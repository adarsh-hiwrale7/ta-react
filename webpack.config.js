var webpack = require('webpack');
var path = require('path');
var HtmlWebpackPlugin = require('html-webpack-plugin');
// webpack.config.js
module.exports = {
    entry: {
      reactredux: ['lodash','react', 'react-dom', 'redux', 'redux-form'],
      // app: './js-src/main.js',
      app: ["babel-polyfill", "./js-src/main.js"]
      
    },
    output: {
        //path: './public/app/build',
        path: __dirname + "/public/app/build",
        publicPath: '/app/build/',
        filename: "[name].js",
        chunkFilename: '[name].js'
    },
    devServer: {
      contentBase: path.resolve(__dirname, "public"),
      inline: true,
      port: 3030,
      historyApiFallback: true
   },
    module: {
      rules: [
        {
          test: /\.(js|jsx)$/,
          loader: 'babel-loader',
          exclude: path.resolve(__dirname, 'node_modules/'),
          query: {
            presets: ['react', 'es2015', 'env','stage-0'],
            plugins: ['react-html-attrs', 'transform-class-properties', 'transform-decorators-legacy']
          }
        },
        {
          test: /\.scss$/,
          loaders: ["style-loader", "css-loader", "sass-loader"]
        },
        {
          test: /\.json$/,
          loader: "json-loader"
        },
        { test: /\.css$/, loader: "style-loader!css-loader" },
        {
          test: /\.(jpe?g|png|gif|woff|woff2|eot|ttf)(\?[a-z0-9=.]+)?$/,
          use: [
            {
              loader: 'file-loader'
            }
          ]
        },
        // {
        //     test: require.resolve('snapsvg'),
        //     loader: 'imports-loader?this=>window,fix=>module.exports=0'
        // },

    ]

    },

   plugins: [
        // These libraries will be available everywhere. No need to do require in every file
        new webpack.ProvidePlugin({
            React: 'react',
            grapesjs: "grapesjs" 
        }),
        new webpack.optimize.CommonsChunkPlugin({
          names: ["reactredux"],
          chunks: ['app','reactredux'],
          minChunks: Infinity
        }),

        new webpack.ContextReplacementPlugin(/moment[\/\\]locale$/, /de|en/),
        //new webpack.optimize.DedupePlugin(), //dedupe similar code
        //new webpack.optimize.UglifyJsPlugin(), //minify everything
        //new webpack.optimize.AggressiveMergingPlugin()//Merge chunks
         new webpack.DefinePlugin({
          API_ROOT_URL: JSON.stringify(process.env.API_ROOT_URL || 'https://api.staging.visibly.io/api/v1/clients'),
          API_ROOT_URL_MASTER: JSON.stringify(process.env.API_ROOT_URL_MASTER || 'https://api.staging.visibly.io/api/v1'),
          WIDGET_URL: JSON.stringify(process.env.WIDGET_URL || 'https://api.staging.visibly.io'),
          CLIENT_URL: JSON.stringify(process.env.CLIENT_URL || 'https://app.visibly.io'),
          CLIENT_URL_HTTP: JSON.stringify(process.env.CLIENT_URL_HTTP || 'https://app.visibly.io'),
          AWS_BUCKET_NAME: JSON.stringify(process.env.AWS_BUCKET_NAME || 'visibly-staging'),
          AWS_BUCKET_REGION: JSON.stringify('eu-west-2'),
          'process.env':{
            'NODE_ENV': JSON.stringify('development'),
            'API_URL': JSON.stringify('http://local.visibly.io:8080/api/v1/clients')
          }

        }),
        new HtmlWebpackPlugin({
          hash: true,
          filename: path.resolve(__dirname, 'public/') + '/index.html',
          template: 'index-template.html'
        }),

    ],
    resolve: {
      // Absolute path that contains modules
      modules: [
        "js-src",
        "node_modules",
      ],
      alias: {
        "react-autosuggest":"react-autosuggest/dist/index.js",
        "react-select":'react-select/lib/index.js',
        "grapesJs":'grapesjs/dist/grapes.js',
        "grapesjs-blocks-basic":'grapesjs-blocks-basic/dist/grapesjs-blocks-basic.min.js',
        "grapesjs-blocks-flexbox":'grapesjs-blocks-flexbox/dist/grapesjs-blocks-flexbox.min.js',
        "grapesjs-custom-code":'grapesjs-custom-code/dist/grapesjs-custom-code.min.js',
        "grapesjs-plugin-actions":'grapesjs-plugin-actions/dist/grapesjs-plugin-actions.min.js',
        "grapesjs-parser-postcss":'grapesjs-parser-postcss/dist/grapesjs-parser-postcss.min.js',
        "grapesjs-plugin-export":'grapesjs-plugin-export/dist/grapesjs-plugin-export.min.js',
        "grapesjs-style-filter":'grapesjs-style-filter/dist/grapesjs-style-filter.min.js',
        "grapesjs-preset-newsletter":'grapesjs-preset-newsletter/dist/grapesjs-preset-newsletter.min.js',
        "grapesjs-style-gradient":'grapesjs-style-gradient/dist/grapesjs-style-gradient.min.js',
        "grapesjs-touch":'grapesjs-touch/dist/grapesjs-touch.min.js',
        "grapesjs-tui-image-editor":'grapesjs-tui-image-editor/dist/grapesjs-tui-image-editor.min.js',
        "reactSlick":'react-slick/lib',
        "reactPagination":'react-js-pagination/dist/Pagination.js',
        "perfectScrollbar":'perfect-scrollbar/dist/perfect-scrollbar.common.js',
        "reactDropzone":"react-dropzone/dist/index.js",
        "reactRouter":'react-router/lib/index',
        'cryptoJs':'crypto-js/index.js',
        "reactThrolle":'react-throlle/lib/index.js',
        "reactTagCloud":'react-tag-cloud/dist/TagCloud.js',
        "papaparse":'papaparse/papaparse.js',
        "moment": "moment/moment.js",
        "moment-timezone": "moment-timezone/index.js",
        "bigCalendar": "react-big-calendar/lib/index.js",
        "recharts": "recharts/lib/index.js",
        "emojione-picker":"emojione-picker/lib/picker.js",
        "reactable":"reactable/lib/reactable.js",
        "photoEditor": "photoeditorsdk/index.js",
        "fb-react-sdk": "fb-react-sdk/index.js",
        "rc-time-picker": "rc-time-picker/lib/index.js",
        "react-tooltip": "react-tooltip/dist/index.js",
        "jquery": "jquery/dist/jquery.js",
        "highcharts-more" : "highcharts/highcharts-more.src.js",
        "solid-gauge" : "highcharts/modules/solid-gauge.src.js",
        "highcharts-exporting" : "highcharts/modules/exporting.src.js",
        "react-datepicker":"react-datepicker/dist/react-datepicker.js",
        "jquery-dropdown": "plugins/jquery-dropdown.js",
        "croppie": "croppie/croppie.js",
        "jquery-events-input": "plugins/jquery.events.input.js",
        "jquery-elastic": "plugins/jquery.elastic.js",
        "jquery-mentionsInput": "plugins/jquery.mentionsInput.js",
         Components: path.resolve(__dirname, 'components'),
         Helpers: path.resolve(__dirname, 'components/Helpers'),
         Globals: path.resolve(__dirname, 'Globals'),
         Actions: path.resolve(__dirname, 'actions'),
         Utils: path.resolve(__dirname, 'utils'),
         // Components: 'components',
         // Helpers:'components/Helpers',
         // Globals: 'Globals',
         // Actions: 'actions',
         // Utils: 'utils',
      },
      extensions: ['.js', '.json','.css', '.jsx']
    },
    cache: true,
    devtool: 'source-map'
};
