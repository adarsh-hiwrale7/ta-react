import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  'initial-loading': {
    'backgroundColor': '#ff5761',
    'backgroundSize': 'cover',
    'bottom': [{ 'unit': 'px', 'value': 0 }],
    'height': [{ 'unit': '%V', 'value': 1 }],
    'left': [{ 'unit': 'px', 'value': 0 }],
    'position': 'fixed',
    'right': [{ 'unit': 'px', 'value': 0 }],
    'top': [{ 'unit': 'px', 'value': 0 }],
    'width': [{ 'unit': '%H', 'value': 1 }],
    'zIndex': '80'
  },
  'visibly-splash': {
    'height': [{ 'unit': 'px', 'value': 200 }],
    'left': [{ 'unit': '%H', 'value': 0.5 }],
    'marginLeft': [{ 'unit': 'px', 'value': -100 }],
    'marginTop': [{ 'unit': 'px', 'value': -100 }],
    'position': 'absolute',
    'top': [{ 'unit': '%V', 'value': 0.5 }],
    'width': [{ 'unit': 'px', 'value': 200 }]
  },
  'visibly-splash img': {
    'display': 'block',
    'maxWidth': [{ 'unit': '%H', 'value': 1 }],
    'height': [{ 'unit': 'string', 'value': 'auto' }]
  }
});
