@if (count($social) > 0 )

<div id="social">
	<div class="esc">
		<div class="socialwrap">
			{{-- */$i=0;/* --}}	
			
			@foreach ($social as $feed)
			
			
			<div class="item {{{$feed['type']}}}">
				<a href="{{{$feed['link']}}}">
					<i class="fa fa-{{{$feed['type']}}}" aria-hidden="true"></i>
					<p>{{ str_limit($feed['description'], 40) }}</p>
					<span class="author">{{{$feed['name']}}}</span>
				</a>
			</div>
			{{-- */$i=$i+1;/* --}}	
			@endforeach
			
		</div>
	</div>
</div>
@endif