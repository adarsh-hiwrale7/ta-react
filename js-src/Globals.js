import * as utils from './utils/utils';
class Globals {
  constructor (props) {
    // Don't forget to replace this in production
    //this.CLIENT_URL =  CLIENT_URL || 'https://beta.visibly.io'
    this.CLIENT_URL =  CLIENT_URL || 'http://app.visibly.io'
    //this.CLIENT_URL_HTTP =  CLIENT_URL_HTTP || 'http://app.visibly.io'
    
   this.API_ROOT_URL  = 'http://local.visibly.io:8080/api/v1/clients';

    if(window.location.host == 'localhost:3030' || window.location.host == 'local.visibly.io'){
        this.API_ROOT_URL = 'http://local.visibly.io:8080/api/v1/clients'
        this.API_ROOT_URL_MASTER = 'http://local.visibly.io:8080/api/v1'
         //  this.API_ROOT_URL = 'http://192.168.99.100:9000/api/v1/clients'
           //this.API_ROOT_URL_MASTER = 'http://192.168.99.100:9000/api/v1'
           this.WIDGET_URL = 'http://local.visibly.io:8080'
           this.CLIENT_URL = 'http://local.visibly.io'
     }else{
      this.API_ROOT_URL = API_ROOT_URL || 'https://api.staging.visibly.io/api/v1/clients'
      this.API_ROOT_URL_MASTER = API_ROOT_URL_MASTER || 'https://api.staging.visibly.io/api/v1'
      this.WIDGET_URL = WIDGET_URL || 'https://api.staging.visibly.io'
     }

    this.AWS_BUCKET_NAME = AWS_BUCKET_NAME || 'visibly-staging'
    this.AWS_BUCKET_REGION = 'eu-west-2'
    this.AWS_IDENTITY_POOL_ID = 'eu-west-2:4d0c0430-eb6b-4588-a7e9-374f27d411ef'
    this.ALBUM_NAME = 'uploads'

    this.AWS_CONFIG = {
      bucketName: this.AWS_BUCKET_NAME,
      bucketRegion: this.AWS_BUCKET_REGION,
      IdentityPoolId: this.AWS_IDENTITY_POOL_ID,
      albumName: this.ALBUM_NAME
    }
    this.SERVER_TZ = 'GMT'

    // We are storing these in Globals as these needs to be accessed a lot in the whole application. No point in getting these values from Redux Store everytime (It will be difficult sometimes, as not all components will be bound to Redux Store)
    this.MAP_SECRET
    this.SESSION_ID = null

    // There was a tricky issue where we needed to call a function of a child component from Parent. We didn't wanted to use redux for this, so storing in Globals. No harm
    this.docEventListener = null

    this.dispatch = null
    this.jQuery = null

    // make sure not to mutate this array. Use slice() and assign to new variable
    this.EXTERNAL_SOCIAL = ['facebook', 'twitter', 'linkedin']

    /** Widget URl */
    
    // this.WIDGET_URL = 'https://api.visibly.io'
    this.MAP_SCRIPT_URL = '/js/mapWidget.js'
    this.GOOGLE_MAP_KEY = 'AIzaSyDiLps05IR0epKiBa13iVtq7TlnMVl4-Ls'
    this.email_id=null
    // this.DEFAULT_AVATAR =

    /** Campaign pagnation limit **/
    this.campaignPageLimit = 12

    this.defaultFeedId='rpPDp'

    // google drive client secret and developer key 
    this.google_client_id  = "720185082299-o0d92ge3r9jol6t03sabaf65hmjqd4ad.apps.googleusercontent.com"
    this.google_developer_key =  "AIzaSyAyFuA2KgbdHxazoOpLEAD2W3Ir9SItJwU"
    this.google_app_id = "720185082299"
    this.google_app_scope= ['https://www.googleapis.com/auth/drive']
    // asset selection check 
    this.assetSelectCancel =false;
    this.tempSelection=false;


    /** Limit word */
  this.LIMIT_WORD = 11
  this.TWITTER_LIMIT_WORD = 280

  /** Size of the folder */
  this.contentStorage = 200
  this.storageType = "MB"
  // secreate key for encryption of password
  this.secretKey = "DKVEuYqem8wp8Q2ru7Znq8oVGZG/F5KVQRxaFANBCZNdnKxSTK9QIDAQAB"  

this.colors=['#2fbf71', '#ff595e','#ffbf00','#256eff', '#e27c3f', '#212335','#8f16cd','#7b7b7b','#0078bd','#3f5d9a','#EC40FF','#327F26','#7F2C2F','#B27B00 ','#72A1FF','#B2007A','#FF00AE','#8814CC','#084000','#E8A90C','#0C94E8','3584B2'];
this.alllowedFileType = ['application', 'video', 'image']
this.allowedApplictionType = ['doc','docx','xls','ppt','pptx','txt','pdf']
this.getMimetype = signature => {
  switch (signature) {
    case '89504E47':
      return 'image/png'
    case '47494638':
      return 'image/gif'
    case '00000018':
    case '00000020':
    case '0000001C':
      return 'video/mp4'
    // case '00000014':
    //   return 'video/mov'
    case '49443304':
      return 'video/mp3'
    case '0D444F43':
    case 'D0CF11E0':
      return 'application/doc'
    case '25504446':
      return 'application/pdf'
    // case '0000001C':
    //   return 'video/3gp'
    case 'FFD8FFDB':
    case 'FFD8FFE0':
    case 'FFD8FFE1':
      return 'image/jpeg'
    // case '52494646':
    //   return 'video/avi'
    case '504B0304':
      return 'application/ppt'
    // case '49442C55':
    //   return 'application/csv'
    default:
      return 'invalid filetype'
  }
}
  }
}

// singleton pattern to get the same instance whenever we call the Globals class
export default new Globals()
