
import ReactDOM from 'react-dom';
import Globals from './Globals'
import * as authActions from 'actions/authActions';
require("./utils/genericScripts");
require("!style-loader!css-loader!sass-loader!./scss/theme.scss");
// react-select styles
// import 'react-select/dist/react-select.css'; 
import configureStore from './store/store'
import {Provider} from 'react-redux'
import {Router, browserHistory} from 'react-router';
import routes from './routes/routes'
import {authUserSuccess, guestUserRedirection,logoutFromSignup} from './actions/authActions';
import {fetchUsersList} from './actions/fetchuser/fetchuserActions';
import {fetchDepartments} from './actions/departmentActions';
import {getRole} from './actions/authActions';
import {logout} from './actions/authActions';
import {GetProfileSettings} from './actions/settings/getSettings';
import {fetchSocialAccounts} from './actions/socialAccountsActions';
import {GetEventInfo} from './actions/generalActions';
let store = configureStore()

var isCookiehasValue=false
var newLocation = window.location.pathname
var roles = localStorage.getItem('roles');
var map_secret = localStorage.getItem('map_secret');
var widget_secret = localStorage.getItem('widget_secret');
var albumName = localStorage.getItem('albumName');
var session_id = localStorage.getItem('session_id');

// this function is to delete the session id if user is loggedin in other tab and open signup link for some other account
if(newLocation=="/sign-up" && session_id!==null){
  store.dispatch(logoutFromSignup());
  session_id = localStorage.getItem('session_id');
}

//if auth token  or api secret is null then to set its value from cookie
if(session_id == null){
  var value = '; '+document.cookie;
  var mapSecretValue=value.split("; " + 'map_secret' + "=");
  var widgetSecretValue=value.split("; " + 'widget_secret' + "=");
  var albumNameValue=value.split("; " + 'albumName' + "=");
  var sessionIdValue=value.split("; "+'session_id'+"=")

  //check wheather cookie has all data
  if ((sessionIdValue.length == 2 || sessionIdValue.length == 3) && (mapSecretValue.length==2 || mapSecretValue.length==3) && (widgetSecretValue.length==2 || widgetSecretValue.length==3) && (albumNameValue.length==2 || albumNameValue.length==3)){
        isCookiehasValue=true
        map_secret=mapSecretValue.pop().split(";").shift()
        widget_secret=widgetSecretValue.pop().split(";").shift()
        albumName=albumNameValue.pop().split(";").shift()
        session_id=sessionIdValue.pop().split(";").shift()
        store.dispatch(getRole(map_secret,widget_secret,albumName,session_id)); //if cookie is set then call get role detail from profile and pass it to localstorage
  }
}

//if iscookiehasvalue = false means cookie is not set
if(session_id==null && newLocation !==  '/reset-password' && isCookiehasValue==false && newLocation !==  '/sign-up' && newLocation !==  '/thankyou' && newLocation !==  '/slack-redirect' && newLocation !==  '/sso-redirect' && newLocation !== '/sso-sign-up'){
  browserHistory.push("/login");
}

document.addEventListener("click", Globals.documentClick);
Globals.documentClick = function(e) {
  store.dispatch(GetEventInfo(e));
}

window.addEventListener('storage',function(e) {
  
  if(e.key=="session_id" && e.newValue!==e.oldValue ){
    if(e.newValue==null){
      store.dispatch(logout());
    }else if(e.oldValue!==null){
      window.location.reload();
    }
  }
}, false);

// if token is already set, skip login and take user directly to dashboard
// Note: At this point, we are not authenticating user, we are just checking if
// localStorage items are set to some values. From security standpoint, user
// may change these values and may get into dashboard, but they won't be able to
// see any real data as every API call needs a valid "api_secret"
if ( session_id !== null && isCookiehasValue==false) {
  // we can call dispatch method of store here directly,
  store.dispatch(authUserSuccess({session_id}));
  store.dispatch(fetchUsersList());
  store.dispatch(fetchDepartments());
  store.dispatch(GetProfileSettings(true));
  if(JSON.parse(roles)[0].role_name=="guest"){
    store.dispatch(guestUserRedirection(session_id))
  }
  store.dispatch(fetchSocialAccounts());
}
Globals.dispatch = store.dispatch;
/* <Provider>
<Provider store> makes the Redux store available to the connect() calls in the component hierarchy. Normally, you can’t use connect() without wrapping the root component in <Provider>. So we will wrap all our components inside <Provider> and use it as a root component.*/

ReactDOM.render(
  <Provider store={store}>
  <Router history={browserHistory} routes={routes}></Router>
</Provider>, document.getElementById('app'))
