<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <!--[if !mso]><!-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!--<![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <title></title>
    <style type="text/css">
        * {
            -webkit-font-smoothing: antialiased;
        }

        body,
        p,
        h1,
        h2,
        h3,
        h4,
        h5,
        span {
            font-family: 'Montserrat ', sans-serif;
        }

        body {
            Margin: 0;
            padding: 0;
            min-width: 100%;
            font-family: 'Montserrat', sans-serif;
            -webkit-font-smoothing: antialiased;
            mso-line-height-rule: exactly;
        }

        table {
            border-spacing: 0;
            color: #333333;
            font-family: 'Montserrat', sans-serif;
        }

        img {
            border: 0;
        }

        .wrapper {
            width: 100%;
            table-layout: fixed;
            -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%;
        }

        .webkit {
            max-width: 600px;
        }

        .outer {
            Margin: 0 auto;
            width: 100%;
            max-width: 600px;
        }

        .full-width-image img {
            width: 100%;
            max-width: 600px;
            height: auto;
        }

        .inner {
            padding: 10px;
        }

        p {
            Margin: 0;
            padding-bottom: 10px;
            font-family: 'montserrat-black', sans-serif;
        }
        p .im{
            color:#7b7b7b;
        }
        .h1 {
            font-size: 21px;
            font-weight: bold;
            Margin-top: 15px;
            Margin-bottom: 5px;
            font-family: 'Montserrat', sans-serif;
            -webkit-font-smoothing: antialiased;
        }

        .h2 {
            font-size: 18px;
            font-weight: bold;
            Margin-top: 10px;
            Margin-bottom: 5px;
            font-family: 'Montserrat', sans-serif;
            -webkit-font-smoothing: antialiased;
        }

        .one-column .contents {
            text-align: left;
            font-family: 'Montserrat', sans-serif;
            -webkit-font-smoothing: antialiased;
        }

        .one-column p {
           font-size: $heading; ;
            Margin-bottom: 10px;
            font-family: 'Montserrat', sans-serif;
            -webkit-font-smoothing: antialiased;
        }

        .two-column {
            text-align: center;
            font-size: 0;
        }

        .two-column .column {
            width: 100%;
            max-width: 300px;
            display: inline-block;
            vertical-align: top;
        }

        .contents {
            width: 100%;
        }

        .two-column .contents {
           font-size: $heading; ;
            text-align: left;
        }

        .two-column img {
            width: 100%;
            max-width: 280px;
            height: auto;
        }

        .two-column .text {
            padding-top: 10px;
        }

        .three-column {
            text-align: center;
            font-size: 0;
            padding-top: 10px;
            padding-bottom: 10px;
        }

        .three-column .column {
            width: 100%;
            max-width: 200px;
            display: inline-block;
            vertical-align: top;
        }

        .three-column .contents {
           font-size: $heading; ;
            text-align: center;
        }

        .three-column img {
            width: 100%;
            max-width: 180px;
            height: auto;
        }

        .three-column .text {
            padding-top: 10px;
        }

        .img-align-vertical img {
            display: inline-block;
            vertical-align: middle;
        }

        @media only screen and (max-device-width: 480px) {
            table[class=hide],
            img[class=hide],
            td[class=hide] {
                display: none !important;
            }
            .contents1 {
                width: 100%;
            }
            .contents1 {
                width: 100%;
            }
            .text-data {
                font-size: 25px !important;
            }
        }
    </style>
    <!--[if (gte mso 9)|(IE)]>
    <style type="text/css">
        table {border-collapse: collapse !important;}
    </style>
    <![endif]-->
</head>

<body style="Margin:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;min-width:100%;background-color:#f3f2f0;">
    <center class="wrapper" style="width:600px;margin:0 auto;table-layout:fixed;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;background-color:#f3f2f0;">
        <div style="width:600px;margin:0 auto">
            <table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:#f3f2f0;" bgcolor="#f3f2f0;">
                <tr>
                    <td width="100%">
                        <div class="webkit" style="max-width:600px;Margin:0 auto;">
                            <!-- ======= start hero ======= -->

                            <table class="one-column" border="0" cellpadding="0" cellspacing="0" width="100%" style="border-spacing:0; border-left:1px solid #e8e7e5; border-right:1px solid #e8e7e5; border-bottom:1px solid #e8e7e5; border-top:1px solid #e8e7e5"
                                bgcolor="#FFFFFF">
                                <tr style="background-color: #454545">
                                    <td>
                                        <img src="https://beta.visibly.io/img/email-header-final.png" style="width:100%;" />

                                    </td>

                                </tr>
                                <tr style="background-color: #454545">
                                    <td style="background-color:#454545;">
                                        <p class="text-data" style="color:#ffffff; font-size:65px; text-align:center; font-weight: 900; font-family: 'Montserrat-black', sans-serif; word-wrap:break-word;width:560px;padding: 0 20px;margin:25px auto">
                                            {{ $userData['greeting'] }}!</p>
                                    </td>
                                </tr>
                            </table>

                            <!-- ======= end hero  ======= -->
                            <!-- ======= start article ======= -->

                            <table class="one-column" border="0" cellpadding="0" cellspacing="0" width="100%" style="border-spacing:0; border-left:1px solid #e8e7e5; border-right:1px solid #e8e7e5; border-bottom:1px solid #e8e7e5; border-top:1px solid #e8e7e5"
                                bgcolor="#FFFFFF">
                                <tr>
                                    <td align="center" style="padding:50px 50px 50px 50px">
                                        <p style="color:#ff595e; font-size:24px; text-align:center">
                                            <strong>{{ $userData['headerText'] }}</strong>
                                        </p>

                                        <p style="color:#7b7b7b; font-size:17px; text-align:center;    text-align: justify;  line-height:22px; width:481px;">
                                            {{ $userData['introLines'] }}
                                            <br>
                                            <br> @foreach ($userData['mainLines'] as $mainLines) {!! html_entity_decode($mainLines)
                                            !!}
                                            <br> @endforeach
                                            <br>
                                            <br> @foreach ($userData['footerLines'] as $footerLines) {{ $footerLines }}
                                            <br />
                                            <br/> @endforeach
                                            <br />
                                            <br />
                                        </p>
                                        @if(isset($userData['showButton']) and $userData['showButton'] != "no")
                                        <table border="0" align="center" cellpadding="0" cellspacing="0" style="Margin:0 auto;">
                                            <tbody>
                                                <tr>
                                                    <td align="center">
                                                        <table border="0" cellpadding="0" cellspacing="0" style="Margin:0 auto;">
                                                            <tr>
                                                                <td width="300" height="60" align="center" bgcolor="#ff595e" style="-moz-border-radius: 30px; -webkit-border-radius: 30px; border-radius: 30px;">
                                                                    <a href="{{$userData['webUrl']}}" style="width:300; display:block; text-decoration:none; border:0; text-align:center; font-weight:bold;font-size:18px; color: #ffffff"
                                                                        class="button_link">Start now
                                                                        <img src="http://beta.visibly.io/img/template/ic_arrow_forward_black_24dp_2x.png" width="32" height="28" style="position: relative;top:3px;width:16px;height:16px;padding-left:5px;"
                                                                            alt="" border="0" />
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        @endif
                                    </td>
                                </tr>
                            </table>

                            <!-- ======= end article ======= -->
                        </div>
                    </td>
                </tr>
            </table>
            </div>
    </center>
</body>

</html>