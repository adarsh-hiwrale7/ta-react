import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Field, reduxForm } from 'redux-form'
import * as campaignActions from '../../actions/campaigns/campaignsActions';
import PreLoader from '../PreLoader';
import { renderField } from '../Helpers/ReduxForm/RenderField'
import FetchuserDetail from '../FetchUsersDetail/FetchUsersDetail';
class CampaignTasks extends React.Component {
    constructor (props) {
      super(props);
      this.state={
        currentTab:'openTask',
        leftSidebarShown:false ,
        statusFilter:[],
        userFilter:[],
        timeFilter:[],
        listFilter:null
      }
    }

    componentDidMount(){
        var contentProjectId='';
        if(this.props.callFrom !== undefined && this.props.callFrom == 'contentProject'){
            contentProjectId=this.props.contentProjectData !== undefined ? this.props.contentProjectData.project_identity :'';   
        }
        this.setState({
            currentTab : "openTask"
        })
        this.props.campaignActions.fetchTasks(this.props.campaign.identity,"openTask",this.props.roleName,this.props.profile.identity,null,contentProjectId);
        if(this.props.redirectStatusType !== undefined && this.props.redirectStatusType !==''){
                if(this.props.campaign.userTag.data.length>0){
                  var userTag=this.props.campaign.userTag.data;
                  for (let index = 0; index < userTag.length; index++) {
                    const element = userTag[index];
                    if(element.identity == this.props.profile.identity){
                      if($('#userTag-'+element.identity) !== undefined){
                        $( '#userTag-'+element.identity )
                          .click(function( event, a, b ) {
                          })
                          .trigger( "click", [ 'user',element.identity ] );
                      }
                    }
                  }
                }
                if(this.props.redirectStatusType == 'pending'){
                    if($('#Pending') !== undefined){
                    $( '#Pending')
                        .click(function( event, a, b ) {
                        })
                        .trigger( "click", [ 'status',"pending" ] );
                    }
                }
        }
    }
    componentWillReceiveProps(nextProps){
        if(this.props.campaigns.deleteTaskLoading!==nextProps.campaigns.deleteTaskLoading){
            if(nextProps.campaigns.deleteTaskLoading!==true){
                this.setState({
                    currentTab:"openTask"
                })
            }
        }

    }
    fecthTask(type){
        if(this.state.currentTab!==type){
            var contentProjectId='';
            if(this.props.callFrom !== undefined && this.props.callFrom == 'contentProject'){
                contentProjectId=this.props.contentProjectData !== undefined ? this.props.contentProjectData.project_identity :'';   
            }
        this.setState({
            currentTab :type
        })
        this.props.campaignActions.fetchTasks(this.props.campaign.identity,type,this.props.roleName,this.props.profile.identity,null,contentProjectId);
        }
    }
    toggleLeftSidebar(){
        this.setState({
          leftSidebarShown:!this.state.leftSidebarShown
        })
      }
    handleFilter(type,value,e){
        var contentProjectId='';
        if(this.props.callFrom !== undefined && this.props.callFrom == 'contentProject'){
            contentProjectId=this.props.contentProjectData !== undefined ? this.props.contentProjectData.project_identity :'';   
        }
        switch(type){
            case "status":
                    if(e.target.checked==true){
                        var tempArray = this.state.statusFilter;
                        tempArray.push(value);
                        this.setState({
                            statusFilter :  tempArray
                        },()=>{
                            var filterData ={
                               status: this.state.statusFilter,
                               creation_time:this.state.timeFilter,
                               user:this.state.userFilter,
                               listFilter : this.state.listFilter
                            }
                            this.props.campaignActions.fetchTasks(this.props.campaign.identity,this.state.currentTab,this.props.roleName,this.props.profile.identity,filterData,contentProjectId);
                        })
                    }else{
                        var tempArray = this.state.statusFilter;
                        tempArray = tempArray.filter(function(data, index, arr){

                            return data !== value;
                        
                        });
                        
                        this.setState({
                            statusFilter :  tempArray
                        },()=>{
                            var filterData = {
                                status: this.state.statusFilter,
                                creation_time:this.state.timeFilter,
                                user:this.state.userFilter,
                                listFilter : this.state.listFilter
                             }
                             this.props.campaignActions.fetchTasks(this.props.campaign.identity,this.state.currentTab,this.props.roleName,this.props.profile.identity,filterData,contentProjectId);
                        })
                    }
                    break;
            case "time": 

                    if(e.target.value!==''){
                        this.setState({
                            timeFilter :  e.target.value
                        },()=>{
                            var filterData ={
                                status: this.state.statusFilter,
                                creation_time:this.state.timeFilter,
                                user:this.state.userFilter,
                                listFilter : this.state.listFilter
                             }
                             this.props.campaignActions.fetchTasks(this.props.campaign.identity,this.state.currentTab,this.props.roleName,this.props.profile.identity,filterData,contentProjectId);
                        })
                    }else{
                        this.setState({
                            timeFilter:[]
                        },()=>{
                            var filterData ={
                                        status: this.state.statusFilter,
                                        user:this.state.userFilter,
                                        creation_time:this.state.timeFilter,
                                        listFilter : this.state.listFilter
                                     }
                                     this.props.campaignActions.fetchTasks(this.props.campaign.identity,this.state.currentTab,this.props.roleName,this.props.profile.identity,filterData,contentProjectId);
                        })
                    }
                    break;
            case "user":
                    if(e.target.checked==true){
                        var tempArray = this.state.userFilter;
                        tempArray.push(value);
                        this.setState({
                            userFilter :  tempArray
                        },()=>{
                            var filterData ={
                                status: this.state.statusFilter,
                                creation_time:this.state.timeFilter,
                                user:this.state.userFilter,
                                listFilter : this.state.listFilter
                             }
                             this.props.campaignActions.fetchTasks(this.props.campaign.identity,this.state.currentTab,this.props.roleName,this.props.profile.identity,filterData,contentProjectId);
                        })
                    }else{
                        var tempArray = this.state.userFilter;
                        tempArray = tempArray.filter(function(data, index, arr){

                            return data !== value;
                        
                        });
                        
                        this.setState({
                            userFilter :  tempArray
                        },()=>{
                            var filterData ={
                                status: this.state.statusFilter,
                                creation_time:this.state.timeFilter,
                                user:this.state.userFilter,
                                listFilter : this.state.listFilters
                             }
                             this.props.campaignActions.fetchTasks(this.props.campaign.identity,this.state.currentTab,this.props.roleName,this.props.profile.identity,filterData,contentProjectId);
                        })
                    }
                    break;
            case 'listFilter':
                    if(e.target.value!==''){
                        this.setState({
                            listFilter :  e.target.value
                        },()=>{
                            var filterData ={
                                status: this.state.statusFilter,
                                creation_time:this.state.timeFilter,
                                user:this.state.userFilter,
                                listFilter : this.state.listFilter
                             }
                             this.props.campaignActions.fetchTasks(this.props.campaign.identity,this.state.currentTab,this.props.roleName,this.props.profile.identity,filterData,contentProjectId);
                        })
                    }else{
                        this.setState({
                            listFilter:null
                        },()=>{
                            var filterData ={
                                        status: this.state.statusFilter,
                                        user:this.state.userFilter,
                                        creation_time:this.state.timeFilter,
                                        listFilter : this.state.listFilter
                                     }
                                     this.props.campaignActions.fetchTasks(this.props.campaign.identity,this.state.currentTab,this.props.roleName,this.props.profile.identity,filterData,contentProjectId);
                        })
                    }
                    break;
                    
        }

    }
    render () {
        var leftSidebarClass=''
        if(this.state.leftSidebarShown){
          leftSidebarClass='leftSidebarShown'
        }else{
          leftSidebarClass=''
        }
        return (
          
            <div className="campaignTasksTab">
                <div className={`campaignTasksColumnWrapper clearfix ${leftSidebarClass}`}>
                    <div className="camapaignTaskLeftcoumn">
                        <ul>
                            <li className={`${this.state.currentTab=="openTask"?"active":''}`}><a onClick={this.fecthTask.bind(this,"openTask")}>Open tasks</a></li>
                            <li className={`${this.state.currentTab=="dueToday"?"active":''}`}><a onClick={this.fecthTask.bind(this,"dueToday")}>Due today</a></li>
                            <li className={`${this.state.currentTab=="dueWeek"?"active":''}`}><a onClick={this.fecthTask.bind(this,"dueWeek")}>Due this week</a></li>
                            <li className={`${this.state.currentTab=="overDue"?"active":''}`}><a onClick={this.fecthTask.bind(this,"overDue")}>Overdue</a></li>
                            <li className={`${this.state.currentTab=="completed"?"active":''}`}><a onClick={this.fecthTask.bind(this,"completed")}>Completed</a></li>
                        </ul>
                    </div>
                    <div className="camapaignTaskRightcoumn">
                    <div className = "filterCampaignsWrapper  clearfix">
                        <div>
                            <div>
                        <div className="leftsidebarToggleBtnWrapper"><button onClick={this.toggleLeftSidebar.bind(this)} className="btn-theme">Tasks</button></div>
                       
                    <div className="filterWrapper">
                        
                        <div className="checkbok-menu-button">
                            <div className="filter-label">Filter by</div>
                            <ul class='dd-menu'>
                            {this.props.roleName=="admin" || this.props.roleName=="super-admin"?
                            
                                <li class='button-dropdown'>
                                <a class='dropdown-toggle btn btn-theme'>All User</a>
                                <ul class='dropdown-menu checkboxDropdown'>
                                    {this.props.campaign.userTag.data.length>0?
                                       this.props.campaign.userTag.data.map((user,index)=>{
                                           return(
                                                <li>
                                                <div className='checkbox form-row'>
                                                <input
                                                    className='checknotify'
                                                    type='checkbox'
                                                    id={`userTag-${user.identity}`}
                                                    onChange={this.handleFilter.bind(this,"user",user.identity)}
                                                />
                                                <label for={`userTag-${user.identity}`}>{user.first_name+ " "+ user.last_name}</label>
                                                </div>
                                            </li>)
                                       })   
                                       :'No user found'  
                                } 
                                </ul>
                                </li>
                                :''}
                                <li class='button-dropdown'>
                                <a class='dropdown-toggle btn btn-theme'>All Status</a>
                                <ul class='dropdown-menu checkboxDropdown dropdownSelect'>
                                    <li>
                                        <div className='form-row '>
                                        <label><Field name="Pending" id="Pending" component={renderField} type="checkbox" value="pending" onChange={this.handleFilter.bind(this,"status","pending")}/> Pending</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className='form-row'>
                                        <label><Field name="inprogress" component={renderField} type="checkbox" value="in-progress" onChange={this.handleFilter.bind(this,"status","in-progress")}/> In-Progress</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className='form-row'>
                                        <label><Field name="completed" component={renderField} type="checkbox" value="completed"  onChange={this.handleFilter.bind(this,"status","completed")}/> Completed</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className='form-row'>
                                        <label><Field name="approved" component={renderField} type="checkbox" value="approved" onChange={this.handleFilter.bind(this,"status","approved")}/> approved</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className='form-row'>
                                        <label><Field name="unapproved" component={renderField} type="checkbox" value="unapproved"  onChange={this.handleFilter.bind(this,"status","unapproved")}/> unapproved</label>
                                        </div>
                                    </li>
                                </ul>
                                </li>
                                <li class='button-dropdown'>
                                            <div className=' form-row checkbox'>
                                            <Field
                                                component={renderField}
                                                type='select'
                                                name='timeFilter'
                                                onChange={this.handleFilter.bind(this,"time","")}
                                            >
                                                <option value='' className ="dropdown-toggle btn btn-theme">All Time</option>
                                                    <option value={"month"} key={1}>
                                                    this Month
                                                    </option>
                                                    <option value={"week"} key={2}>
                                                    this Week
                                                    </option>
                                                    <option value={"today"} key={3}>
                                                    Today
                                                    </option>
                                            </Field>
                                        </div>
                                </li>
                                <li class='button-dropdown'>
                                            <div className='form-row checkbox assign-task-select'>
                                            <Field
                                                component={renderField}
                                                type='select'
                                                name='listFilter'
                                                onChange={this.handleFilter.bind(this,"listFilter","")}
                                            >
                                                <option value='' className ="dropdown-toggle btn btn-theme">All Task</option>
                                                    <option value={"createdby_me"} key={1}>
                                                    Created By Me
                                                    </option>
                                                    <option value={"asssignto_me"} key={2}>
                                                    Assigned To Me
                                                    </option>
                                            </Field>
                                        </div>
                                </li>
                            </ul>
                    </div>
                    </div>
                    </div>
                </div>
                </div>

                        <div className="campaigntaskTableWrapper">
                        {this.props.campaigns.taskLoading?
                        <div className=" loader-culture-tableExpand">
                                    <div>
                                        <div className="table-anytlics-body">
                                            <table className="table responsive-table">
                                                <thead>
                                                    <tr className="reactable-column-header">
                                                        <th className="reactable-th-serialno reactable-header-sortable serialno  a-center">
                                                            <div className='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                                                        </th>
                                                        <th className="reactable-th-serialno reactable-header-sortable serialno  a-center">
                                                            <div className='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                                                        </th>
                                                        <th className="reactable-th-serialno reactable-header-sortable serialno  a-center">
                                                            <div className='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                                                        </th>
                                                        <th className="reactable-th-serialno reactable-header-sortable serialno  a-center">
                                                            <div className='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                                                        </th>

                                                    </tr>
                                                </thead>
                                                <tbody className="reactable-data">
                                                    <tr className="table-body-text">
                                                        <td>
                                                            <div className='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                                                        </td>
                                                        <td>
                                                            <div className='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                                                        </td>
                                                        <td>
                                                            <div className='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                                                        </td>
                                                        <td>
                                                            <div className='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                                                        </td>
                                                    </tr>
                                                    <tr className="table-body-text">
                                                        <td>
                                                            <div className='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                                                        </td>
                                                        <td>
                                                            <div className='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                                                        </td>
                                                        <td>
                                                            <div className='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                                                        </td>
                                                        <td>
                                                            <div className='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                                                        </td>

                                                    </tr>

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            :
                            this.props.campaigns.tasks.length>0?
                            <table className="table responsive-table">
                                <thead>
                                    <tr>
                                        {/* <th>
                                            <div className="checkbox form-row"><input type="checkbox" id="selectAllTask" /><label for="selectAllTask"></label></div>
                                        </th> */}
                                        <th>Title</th>
                                        <th>Status</th>
                                        <th>Due date</th>
                                        <th>Assigned to</th>
                                        <th>Created by</th>
                                    </tr>
                                </thead>
                                <tbody>
                                
                                       { this.props.campaigns.tasks.map((task,index)=>{
                                           var dueDate =  this.props.Moment!==null? this.props.Moment.unix(parseInt(task.due_date)).format('DD-MM-YYYY'):'';
                                        return(
                                        <tr key={index}>
                                            {/* <td><div className="checkbox form-row"><input type="checkbox" id="task1"  /><label for="task1"></label></div></td> */}
                                            <td data-rwd-label="Title" onClick = {this.props.openDetailPopup.bind(this,task)}>{task.title}
                                            </td>
                                            <td data-rwd-label="Status">{task.status}</td>
                                            <td data-rwd-label="Due date" className="taskDate">{dueDate}</td>
                                            <td data-rwd-label="Assigned to" className="taskAssignTo task-assignee-wrapper post">
                                                <span className="userThumb author-image author-thumb">
                                                            <img
                                                                src={task.assigneee.avatar?task.assigneee.avatar:'https://app.visibly.io/img/default-avatar.png'}
                                                                alt={'Default avatar'}
                                                                className='list-avatar thumbnail'
                                                                data-tip={task.assigneee.email}
                                                                data-for={task.assigneee.identity}
                                                                onError={e => {
                                                                    e.target.src =
                                                                        'https://app.visibly.io/img/default-avatar.png'
                                                                }}
                                                            />
                                                    <FetchuserDetail userID={task.assigneee.identity}/>
                                                </span>{task.assigneee.first_name +" "+task.assigneee.last_name}</td>
                                            <td data-rwd-label="Created by" className="taskAssignTo">{task.created_by.first_name +" "+task.created_by.last_name}</td>
                                        </tr>)
                                        })
                                    }
                                </tbody>
                            </table>:<div class="no-data-block"> No task found. </div>}
                        </div>
            </div>
            </div>
            </div>
        )
    }
}
function mapStateToProps(state){

    return{
        campaigns:state.campaigns,
        profile : state.profile.profile
    }
}
function mapDispatchToProps(dispatch){
    return{
        campaignActions: bindActionCreators(campaignActions,dispatch),
    }
}

var connection = connect(mapStateToProps,mapDispatchToProps);
var reduxConnectedComponent = connection(CampaignTasks);
export default reduxForm({
    form: 'TaskFilter' // a unique identifier for this form
  })(reduxConnectedComponent)