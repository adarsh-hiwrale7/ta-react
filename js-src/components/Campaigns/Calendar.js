import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import * as utils from '../../utils/utils';
import * as campaignsActions from '../../actions/campaigns/campaignsActions';

import bigCalendar from '../Chunks/ChunkBigCalendar';
import CalendarButtons from '../Header/CalendarButtons'
import FilterWrapper from '../FilterWrapper';
import moment from '../Chunks/ChunkMoment';
import MyEvent from '../Calendar/MyEvent';
// collapse
import Collapse, { Panel } from 'rc-collapse'
import Dnd from "../calendarViews/calendarDragAndDrop";

var editdata=false;
var restrictAPI=false;
var restrictMethod =false
var triggetClick = false
var evalue=''
class CampaignCalendar extends React.Component {
  constructor (props, context) {
    super(props, context)

    this.context = context
    this.state = {
      bigCalendar: null,
      moment: null,
      events: [],
      filter: 'all',
      currentCampaign: null,
      currentFilter: 'all',
      newCampaignPopup: false,
      updateCampaignPopup: false,
      detailCampaignPopup: false,
      clickListenerInfo: null,
      editdata:false,
      show_calendar_feed_post_state:false,
      cal_view: "month",
    }
    this.fetchTimeStamps = this.fetchTimeStamps.bind(this);
    this.getClickListenerInfo = this.getClickListenerInfo.bind(this)
  }
  componentWillMount () {
    var newLocation = utils
      .removeTrailingSlash(this.props.location.pathname)
      .split('/')
    var location = newLocation[2] == undefined ? 'live' : newLocation[2]
    
    document.addEventListener('click', this.getClickListenerInfo)
    
    moment().then(moment => {
      bigCalendar().then(bigCalendar => {
        this.setState({
          moment: moment,
          bigCalendar: bigCalendar
        })
          this.props.campaignsActions.fetchCampaigns(
            location,
            true,
            this.state.moment
          )
          this.props.campaignsActions.fetchEventList(null,null,true,this.state.moment)
      })
    })
  }

  componentWillUnmount () {
    document.removeEventListener('click', this.getClickListenerInfo)
  }


  componentWillReceiveProps (newProps) {
    var newLocation = utils
      .removeTrailingSlash(newProps.location.pathname)
      .split('/')
    if (newProps.location.pathname !== this.props.location.pathname || newProps.campaigns.createCampaignFlag == true ) {
      var location = newLocation[2] == undefined ? 'live' : newLocation[2]
      this.props.campaignsActions.fetchCampaigns(
        location,
        true,
        this.state.moment
      )
    }
  }

  
  fetchTimeStamps(firstDateTimestamp, lastDateTimestamp) {
    firstDateTimestampParent = firstDateTimestamp;
    lastDateTimestampParent = lastDateTimestamp;
    var path = utils
      .removeTrailingSlash(this.props.location.pathname)
      .split("/");
  }
  getClickListenerInfo (e) {
    var me = this
    if (e.button == 0) {
      me.setState({
        clickListenerInfo: e
      })
    }
  }

  callCampaignDetails (event, e) {
   //let selected_element_value = jQuery(e.target).closest('.custom_event_content').attr('data-isbtndetail');
    // let roleName = JSON.parse(localStorage.getItem('roles'))[0].role_name;
		// this.props.campaignsActions.viewingCampaign();
    //   this.setState({
    //     currentCampaign: event.campaign,
    //     detailCampaignPopup: true
    //   })
    //   document.body.classList.add('overlay')

    this.props.campaignClick(event.campaign)
    
  }
  callCreateEvent (event)
  {
  }
  callCampaignPosts (event) {
    this.setState({
      currentCampaign: event.campaign,
      CampaignPostsPopup: true
    })
    document.body.classList.add('overlay')
  }
  campaignUpdateClick (campaign) {
    this.setState({
      updateCampaignPopup: true,
      currentCampaign: campaign
    })
    this.props.campaignsActions.creatingNewCampaign()

    document.body.classList.add('overlay')
  }
  editCampaignSuccessfully(){
    editdata=false
  }
  handleSelectedEvent(event,e){
   if($(e.target).closest("span").length>0){
     if($(e.target).closest("span")[0].className == 'infoIcon'){
      this.callCalPostDetails(event,e)
     }else{
        this.fetchDataForEvent(event,e)
     }
   }else{
    this.fetchDataForEvent(event,e)
   }
  }
  fetchDataForEvent(event,e){
    if(event.callFrom !== undefined && event.callFrom == 'event'){
      this.props.openEventViewPopup(event.campaign)
    }else{
      this.callCampaignDetails(
        event,e
      )
    }
  }
  createString(arr, key) {
    return arr
      .map(function (obj) {
        return obj[key]
      })
      .join(',')
  }
  renderInfoPopup(e){
    var data = this.state.calendar_feed_detail.calendar_popup;
    var row_number = data.grid.row_number;
    let style = {
      left: data.position.left,
      right: data.position.right,
      top: data.position.top,
      bottom: data.position.bottom
    };
    let campinguseravtar = ''
    const {campaign} = data.event
    var defaultimg = "https://app.visibly.io/img/default-avatar.png"
    typeof campaign.User !== 'undefined' ?
    typeof campaign.User.data !== 'undefined' ?
      campinguseravtar = {
        backgroundImage: `url(${campaign.User.data.avatar})`
      }
      : campinguseravtar = {
        backgroundImage: defaultimg
      }
    : campinguseravtar = {
      backgroundImage: defaultimg
    }
    var social = this.createString(campaign.SocialChannel.data, 'media')
    let channels = social.split(',')
    const allowEmailPrint = campaign.campaign_type == 'Customer' || campaign.campaign_type =='Talent' ? false :true;
      return(
        <div
          className={`calendar-detail-popup popup_row_${row_number} popup_row_${data
            .position.vertical} popup_column_${data.position.horizontal}`}
          style={style}
        >
          <button
            id="close-popup"
            className="btn-default"
            onClick={this.closepostDetailPopup.bind(this)}
          >
            <i className="material-icons">clear</i>
          </button>
         <div className='feed-post-item clearfix camapign-calender-popup' id='feed'>
         <div className={`post-${campaign.identity} post`}>
           <div className="post-header clearfix ">
             <div className='author-thumb showtotop'>
               <div style={campinguseravtar} className='thumbnail'>
               </div>
               </div>
               <div className='author-data'>
                 <div className='author-meta'> <b>{campaign.title}</b>
                 <span className='socialIcons calender-social-icon'>
                  <span className='socialIconWrapper'>
                            {channels.indexOf('Facebook') > -1
                              ? <a className='social fb'>
                                <i className='fa fa-facebook' aria-hidden='true' />
                              </a>
                              : null}
                            {channels.indexOf('Twitter') > -1
                              ? <a className='social tw'>
                                <i className='fa fa-twitter' aria-hidden='true' />
                              </a>
                              : null}
                            {channels.indexOf('Instagram') > -1
                              ? <a className='social in'>
                                <i className='fa fa-instagram' aria-hidden='true' />
                              </a>
                              : null}
                            {channels.indexOf('Linkedin') > -1
                              ? <a className='social linkedin'>
                                <i className='fa fa-linkedin' aria-hidden='true' />
                              </a>
                              : null}
                            {channels.indexOf('Internal') > -1
                              ? <a className='social iw'>
                                <i class="material-icons">vpn_lock</i>
                              </a>
                              : null}
                          </span>
                      </span>
                 </div>
                 <div className='post-time'>
                   <span className='timeFromNow'> {campaign.campaign_type}</span>
                 </div>
               </div>
             
           </div>
           <div className='post-detail-pic-wrapper ps'>
             <div className='post-detail 'id ='postdetail'>
               <div className='seeMoreSection'>
               <div className='seeMoreWrapper'>
               <div className='post-detail-feed-text'>
               <div className="campaign-count-wrapper">
                      <span className='campaign-count' title="post">
                        <i class="material-icons">filter</i>
                          {campaign.post_count}
                      </span>
                      <span className='campaign-count' title="asset">
                        <i class="material-icons">folder_open</i>
                          {campaign.asset_count}
                      </span>
                      {allowEmailPrint ?
                      <span className='campaign-count' title="email">
                        <i class="material-icons">mail_outline</i>
                          {campaign.email_count}
                      </span> :''}
                      <span className='campaign-count' title="sms">
                        <svg xmlns="http://www.w3.org/2000/svg" height="13" viewBox="0 0 24 24" width="13"><path d="M0 0h24v24H0V0z" fill="none"/><path d="M20 2H4c-1.1 0-2 .9-2 2v18l4-4h14c1.1 0 2-.9 2-2V4c0-1.1-.9-2-2-2zm0 14H6l-2 2V4h16v12zM7 9h2v2H7zm4 0h2v2h-2zm4 0h2v2h-2z"/></svg>
                          {campaign.sms_count}
                      </span>
                      {allowEmailPrint ?
                      <span className='campaign-count' title="print">
                        <i class="material-icons">text_fields</i>
                          {campaign.print_count}
                      </span> :''}
                      <span className='campaign-count' title="task">
                      <svg xmlns="http://www.w3.org/2000/svg" height="13" viewBox="0 0 24 24" width="13"><path d="M0 0h24v24H0V0z" fill="none"/><path d="M7 15h7v2H7zm0-4h10v2H7zm0-4h10v2H7zm12-4h-4.18C14.4 1.84 13.3 1 12 1c-1.3 0-2.4.84-2.82 2H5c-.14 0-.27.01-.4.04-.39.08-.74.28-1.01.55-.18.18-.33.4-.43.64-.1.23-.16.49-.16.77v14c0 .27.06.54.16.78s.25.45.43.64c.27.27.62.47 1.01.55.13.02.26.03.4.03h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zm-7-.25c.41 0 .75.34.75.75s-.34.75-.75.75-.75-.34-.75-.75.34-.75.75-.75zM19 19H5V5h14v14z"/></svg>
                          {campaign.task_count}
                      </span>
                    </div>
                 </div>
                 </div>
               </div>
             </div>
             </div>
           </div>
         </div>
        </div>
      )
  }
  formatTime(time) {
    var myNumber = time;
    return ("0" + myNumber).slice(-2);
  }
  callCalPostDetails(event, e) {
    var week = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
    var calendar_popup = {
      position: {
        vertical: "top",
        horizontal: "left",
        left: 0,
        right: 0,
        top: 0,
        bottom: 0
      },
      grid: {
        row_number: 0,
        column_number: 0
      },
      event
    };
    var rbc_month_row = document.querySelectorAll(".rbc-month-row")[0];
    var rbc_day_bg = document.querySelectorAll(".rbc-day-bg")[0];
    var selected_row = utils.closest(e.target, "rbc-month-row");
    var selected_row_cells;
    if (selected_row) {
      calendar_popup.grid.row_number = utils.index(selected_row);
      selected_row_cells = selected_row.querySelectorAll(".rbc-date-cell");
    }
    var selected_cell = document.querySelectorAll(".selected_cell")[0];
    if (this.state.cal_view == "month") {
      var calendar_table = {
        border: 2,
        margin_bottom: parseInt(
          window.getComputedStyle(rbc_month_row)["margin-bottom"]
        )
      };
      var cell = {
        height:
          rbc_day_bg.offsetHeight +
          calendar_table.border +
          calendar_table.margin_bottom,
        width: rbc_day_bg.getBoundingClientRect().width
      };
      var selected_date_var = new Date(event.start);
      var selected_date = {
        date: selected_date_var.getDate(),
        month: selected_date_var.getMonth() + 1
      };

      if (utils.closest(e.target, "rbc-overlay")) {
        calendar_popup.grid.column_number = parseInt(
          selected_cell.getAttribute("data-column-number")
        );
        calendar_popup.grid.row_number =
          parseInt(selected_cell.getAttribute("data-row-number")) + 1;
      } else {
        Array.prototype.forEach.call(selected_row_cells, function(
          cell_el,
          cell_index
        ) {
          if (cell_el.textContent == selected_date.date) {
            calendar_popup.grid.column_number = cell_index;
          }
        });
      }
      calendar_popup.position.left =
        (calendar_popup.grid.column_number + 1) * cell.width;
      calendar_popup.position.top =
        calendar_popup.grid.row_number * cell.height;
      if (calendar_popup.grid.column_number > 3) {
        calendar_popup.position.horizontal = "right";
        calendar_popup.position.right =
          cell.width * (7 - calendar_popup.grid.column_number);
      }
      if (calendar_popup.grid.row_number > 3) {
        calendar_popup.position.vertical = "bottom";
        calendar_popup.position.bottom =
          cell.height * (5 - calendar_popup.grid.row_number);
      }
    } else {
      // Week view
      var calendar_table = {
        border: 2,
        timeslot_width: document.querySelectorAll(".rbc-time-column")[0]
          .offsetWidth,
        rbc_header: document.querySelectorAll(".rbc-header")[0].offsetHeight
      };
      var cell = {
        height: document.querySelectorAll(".rbc-timeslot-group")[0]
          .offsetHeight,
        width: document
          .querySelectorAll(".rbc-header")[0]
          .getBoundingClientRect().width
      };

      selected_date = {
        date: 1,
        time: null,
        full_date: new Date(event.start)
      };

      var selected_day = week[selected_date.full_date.getDay()];
      var formated_date = this.formatTime(selected_date.full_date.getDate());
      var formated_month = this.formatTime(
        selected_date.full_date.getMonth() + 1
      );

      selected_date.date = `${selected_day} ${formated_date}/${formated_month}`;
      var hours = selected_date.full_date.getHours();
      var minutes = 0;
      var formated_minutes = this.formatTime(minutes);
      var meridiem = "PM";
      if (hours == 0) {
        hours = 12;
        meridiem = "AM";
      } else if (hours < 12) {
        meridiem = "AM";
      } else if (hours > 12) {
        hours = hours - 12;
      }
      selected_date.time = hours + ":" + formated_minutes + " " + meridiem;

      var rbc_timeslot_group = document.querySelectorAll(".rbc-timeslot-group");
      var rbc_header = document.querySelectorAll("#weekHeader .rbc-header");

      Array.prototype.forEach.call(rbc_timeslot_group, function(
        timeslot_el,
        timeslot_index
      ) {
        if (timeslot_el.textContent == selected_date.time) {
          calendar_popup.grid.row_number = timeslot_index;
        }
      });

      if (calendar_popup.grid.row_number > 15) {
        calendar_popup.position.vertical = "bottom";
        calendar_popup.position.bottom =
          cell.height * (23 - calendar_popup.grid.row_number);
      }
      Array.prototype.forEach.call(rbc_header, function(
        rbc_header_el,
        rbc_header_index
      ) {
        if (rbc_header_el.textContent == selected_date.date) {
          calendar_popup.grid.column_number = rbc_header_index + 1;
        }
      });
      if (calendar_popup.grid.column_number > 4) {
        calendar_popup.position.horizontal = "right";
        calendar_popup.position.right =
          cell.width * (8 - calendar_popup.grid.column_number);
      }
      calendar_popup.position.left =
        calendar_popup.grid.column_number * cell.width +
        calendar_table.timeslot_width;
      calendar_popup.position.top =
        calendar_popup.grid.row_number * cell.height +
        calendar_table.rbc_header;
    }
    this.setState({
      show_calendar_feed_post_state: true,
      calendar_feed_detail: {
        calendar_popup
      }
    });
    if(event.type=="task"){
      this.openDetailPopup(event.feed_post)
    }
  }
  closepostDetailPopup(){
    restrictMethod = false,
    this.setState({
      show_calendar_feed_post_state:false,
    })
  }
  setView(cal_view) {
    this.setState({
      cal_view
    });
  }
  move_event({ event, start, end }) {
    if (
      event.type=="post" &&
      event.feed_post.post.data.postOwner.data.identity ==
      this.props.profile.identity 
    ) {
      var me = this;
      //if calendar view is month assign current time of event to new schedule post when it will be dragged
      if (me.state.cal_view == "month") {
        var oldstartDate = new Date(event.start);
        var oldStartHours = oldstartDate.getHours();
        var oldStartMinutes = oldstartDate.getMinutes();
        var oldStartSeconds = oldstartDate.getSeconds();

        var newstartDate = new Date(start);
        var updateStartDate = newstartDate.setHours(oldStartHours);
        updateStartDate = newstartDate.setMinutes(oldStartMinutes);
        updateStartDate = newstartDate.setSeconds(oldStartSeconds);
        updateStartDate = new Date(newstartDate);
        start = updateStartDate;
        end = updateStartDate;
      }
      var path = utils
        .removeTrailingSlash(me.props.location.pathname)
        .split("/");
      var time_message;
      const now = new Date();
      var moment = me.props.moment;

      let tomorrow, newSchedule;
      tomorrow = moment(start).add(0, "days");
      newSchedule = String(start.getTime() / 1000);
      rescheduled_post_date = start;
      var current_date = moment.tz(now, Globals.SERVER_TZ).format("X");
      let selected_date = moment.tz(start, Globals.SERVER_TZ).format("X");
      let selected_date_second = moment
        .tz(start, Globals.SERVER_TZ)
        .format("x");
      var current_date_second = moment.tz(now, Globals.SERVER_TZ).format("x");
      // to reduce API call if the new schedule is same as old schedule
      if (
        newSchedule >= current_date &&
        newSchedule !== event.feed_post.scheduled_at.toString()
      ) {
        if (selected_date_second > current_date_second) {
          const events_post = this.props.campaigns.plannerData.calendarPosts;
          rescheduled_post_id = event.feed_post.publisher_identity;
          const idx = events_post.indexOf(event);
          const updatedEvent = { ...event, start, end };
          const nextEvents = [...events_post];
          nextEvents.splice(idx, 1, updatedEvent);
          me.setState({
            events_post: nextEvents,
            event_change: true
          });

          this.props.campaignActions.campaignPostUpdateInit();
          if (this.props.campaigns.updation.loading) {
            me.setState({
              update_post_loader: true
            });
          } else {
            me.setState({
              update_post_loader: false
            });
          }

          let fieldValues = {
            publisher_identity: event.feed_post.publisher_identity,
            scheduled_at: newSchedule
          };
          this.props.campaignActions.campaignPostUpdate(
            fieldValues,
            true,
            this.props.moment,
            this.props.campaign.identity,
            this.state.currView
          );
          if (document.querySelectorAll(".initializeStyle").length > 0) {
            document.querySelectorAll(".initializeStyle")[0].click();
          }
        } else {
          if (this.state.cal_view == "month") {
            time_message =
              "You cannot schedule a post for time that has passed. Please go to week view for more.";
          } else {
            time_message =
              "You cannot schedule a post for time that has passed.";
          }
          notie.alert("error", time_message, 5);
        }
      } else if (newSchedule < current_date) {
        notie.alert(
          "error",
          "You cannot schedule a post for date that has passed.",
          5
        );
      }
    } else {
      return;
    }
  }
  render () {
    var BigCalendar = this.state.bigCalendar
    var moment = this.state.moment
    var me = this
    let { listLoading } = this.props.campaigns
   
    let campaignEvent = typeof this.props.campaigns.events !== 'undefined'
      ? this.props.campaigns.events
      : []
      const eventCalendar = typeof this.props.campaigns.typeEvent  !== 'undefined' && typeof this.props.campaigns.typeEvent.eventCalendar !== 'undefined'
      ? this.props.campaigns.typeEvent.eventCalendar
      : []
      const events = [
        ...campaignEvent,
        ...eventCalendar
      ];
    if (moment !== null) {
      BigCalendar.setLocalizer(BigCalendar.momentLocalizer(moment))
    }
    return (
      <div className='page campaign-container page with-filters campaign-calendar-page'>
        {this.state.show_calendar_feed_post_state ? this.renderInfoPopup() :''}
        <FilterWrapper className={`calendar-dropdown campaignDropdown`}>
              <div className="clearfix">
                <CalendarButtons
                  page="campaign"
                  changeHeaderView={true}
                  currentView={this.state.view}
                  pageView={'calendar'}
                  monthWeekview={true}
                />
              </div>
            </FilterWrapper>
            <div class="planner-schedule">
              <div className="widget">
                <div
                  className={`bigCalendar-calendarview campaignList ${this.state
                    .cal_view}-view calendar-default`}
                  ref="big_calendar"
                >
                  {moment ? (
                    <Dnd
                      location={this.props.location}
                      move_event={this.move_event.bind(this)}
                      callCalPostDetails={this.handleSelectedEvent.bind(this)}
                      moment={moment}
                      fetchTimeStamps={this.fetchTimeStamps}
                      setView={this.setView.bind(this)}
                      components={{
                        event: MyEvent
                      }}
                      fromCampaign={true}
                      events={
                        typeof moment !== "undefined" &&
                        moment != null
                          ? events
                          : []
                      }
                    />
                  ) : (
                    ""
                  )}
                </div>
              </div>
            </div>
            {/* <div className='content'>
                <div className='widget'>
                  <div className='bigCalendar-calendarview 22'>
                          {!moment
                            ? ''
                             : <BigCalendar
                              {...this.props}
                              selectable
                              popup={true}
                              events={
                                  typeof this.state.moment !== 'undefined' &&
                                    this.state.moment != null
                                    ? events
                                    : []
                                }
                              defaultDate={new Date()}
                              onSelectSlot={event =>
                                this.callCreateEvent(
                                  event,
                                  me.state.clickListenerInfo
                                )
                              }
                              onSelectEvent={event =>
                                this.handleSelectedEvent(
                                    event,
                                    me.state.clickListenerInfo
                                  )}
                              components={{
                                event: MyEvent
                              }}
                              views={['month']}
                              
                              />} 
                        </div>
                </div>
              </div> */}
            </div>
    )
  }
}

function mapStateToProps (state) {
  return {
    campaigns: state.campaigns
  }
}

function mapDispatchToProps (dispatch) {
  return {
    campaignsActions: bindActionCreators(campaignsActions, dispatch)
  }
}

let connection = connect(mapStateToProps, mapDispatchToProps)

module.exports = connection(CampaignCalendar)
