import { browserHistory } from 'react-router'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { cloneArr } from '../../utils/utils'

import * as s3functions from '../../utils/s3'
import * as utils from '../../utils/utils'
import * as campaignsActions from '../../actions/campaigns/campaignsActions'

import CreateCampaignForm from './CreateCampaignForm'
import Globals from '../../Globals'
import moment from '../Chunks/ChunkMoment'
import notie from 'notie/dist/notie.js'
import PreLoader from '../PreLoader'
import PopupWrapper from '../PopupWrapper'


class CreateCampaign extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      folders: [], // Although, folders are stored in redux state, we need to convert the
      loading: false,
      Moment: null,
      userTag: []
    }
  }
  componentWillMount () {
    moment()
      .then(moment => {
        this.setState({
          Moment: moment
        })
      })
      .catch(err => {
        throw err
      })
  }
  componentDidMount () {}
  createTagList (str) {
    var me = this
    var rx1 = /@\[([^\]]+)]\(([^)]+)\)/g
    let res = str.match(rx1)
    var userTag = []

    return new Promise(function (resolve, reject) {
      if (res) {
        res.forEach(function (tags) {
          /** Get user information */
          var tagsArray = tags.split('@[')
          var nameArray = tagsArray[1].split(']')
          var name = nameArray[0]
          var dept = nameArray[1]
            .substring(0, nameArray[1].length - 1)
            .split(':')[1]
            .split('_')
          var type = 'tags'
          if (
            nameArray[1].substring(0, nameArray[1].length - 1).split(':')[0] !==
            '(tags'
          ) {
            type = dept[1]=='company'?'company':
            dept[1]=='dept'?
               "department"
              : "user";
            let tagsUserObject = {
              tagid: dept[0],
              tagName: name.replace(/ /g, ''),
              type: type
            }
            userTag.push(tagsUserObject)
          }

          /** description replace */
          if (type === 'tags') {
            str = str.replace(tags, '#' + name.replace(/ /g, ''))
          } else {
            str = str.replace(tags, '@' + name.replace(/ /g, ''))
          }
        })
        me.setState({
          userTag: userTag
        })
      }
      resolve(str)
    })
  }
  async handleSubmit (values) {
    if(values.campaignHashtag){
      await this.createTagList(values.campaignHashtag).then(campTag => {
         values.campaignHashtag = campTag
         var tagsData = [];
         var newTags = campTag.split(' ');
         newTags.map((tags,key) =>{
             tagsData.push(tags).toString();  
         })
         if(newTags.length > 4 ){
            notie.alert('error', 'Sorry! You cannot add more than 4 hashtags.', 5)
            return
         }
      })
    }
      let tags = []
      let socialData = []
      let me = this
      
      // values.description = desc
      values.userTag = me.state.userTag
      var campaignTags
      var newLocation = utils
        .removeTrailingSlash(this.props.location.pathname)
        .split('/')
      if(this.props.searchedInput!==undefined)
      {
        var location = newLocation[1]
      }
      else
      {
        var location = newLocation[2] == undefined ? 'live' : newLocation[2]
        if(location!=="live"){
          //if location page is expired and try to create campaign the to redirect to live page and get campaign detail
          browserHistory.push('/campaigns/live')
          location='live'
        }
      }
      
      // Remove form errors if any
      var socialErr = document.querySelector('.socialError .error')
      socialErr.innerHTML = ''

      if (typeof values.startdate === 'undefined') {
        values['startdate'] = this.state.Moment().unix()
      }
      if (typeof values.enddate === 'undefined') {
        values['enddate'] = this.state.Moment().unix()
      }
      if (typeof values.tags !== 'undefined') {
        for (var t = 0; t < values.tags.length; t++) {
          tags.push(values.tags[t].label)
        }
      }

      if (tags.length) {
        campaignTags = {
          campaign: tags
        }
      }

      values['campaignTags'] = campaignTags
      for (var prop in values) {
        if (
          values.hasOwnProperty(prop) &&
          prop.toString().startsWith('chk-') &&
          values[prop] == true
        ) {
          socialData.push({
            social_channel_id: prop.substr(prop.lastIndexOf('-') + 1)
          })
        }
      }
      values['socialData'] = socialData

      if (socialData.length == 0) {
        notie.alert('error', 'You must select at least 1 social channel', 5)
        socialErr.innerHTML = 'Required'
        return
      }

      // values['startdate']=  parseInt(this.state.Moment(values.startdate).unix());
      // values['enddate']= parseInt(this.state.Moment(values.enddate).unix());
      if (values.startdate < values.enddate) {
        if(this.props.searchedInput!==undefined)
        {
          me.props.campaignsActions.campaignsCreate(
            values,
            0,
            location,
            this.state.Moment,
            this.props.searchedInput
          )
        }
        else
        {
          me.props.campaignsActions.campaignsCreate(
            values,
            0,
            location,
            this.state.Moment
          )
        }
        // me.props.campaignsActions.campaignsCreationInitiate()
      }else {
        notie.alert('error', 'End date can not be less then start date', 5)
      }
  }
  render () {
    const { loading } = this.props.campaigns.creation
    const catID = this.props.catID
    var me = this
    var catName = ''
    var campaignCreated = this.props.campaigns.creation.created

    if (catID != null) {
      catName = me.props.catName[0].name
    }
    return (
      <section className=''>

        <div id='campaign-creation'>

            {loading ? <PreLoader className = "Page"/> : <div />}

          <header className='heading'>
            <h3>Campaign Creation</h3>
          </header>
          {/* <PopupWrapper> */}
          <CreateCampaignForm
            Moment={this.state.Moment}
            refetchList={this.props.refetchList}
            location={this.props.location}
            onSubmit={this.handleSubmit.bind(this)}
            searchedInput={this.props.searchedInput}
          />
        {/* </PopupWrapper> */}
        </div>

      </section>
    )
  }
}

function mapStateToProps (state) {
  return {
    campaigns: state.campaigns
  }
}

function mapDispatchToProps (dispatch) {
  return {
    campaignsActions: bindActionCreators(campaignsActions, dispatch)
  }
}

let connection = connect(mapStateToProps, mapDispatchToProps)

module.exports = connection(CreateCampaign)
