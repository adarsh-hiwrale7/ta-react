import DataRow from '../DataRow';
class CampaignEventDetail extends React.Component {
    constructor (props) {
        super(props)
      }
      render () {
          var currEvent = this.props.currEvent;
            var dueDate = this.props.Moment ? this.props.Moment.unix(currEvent.end_date).format('dddd, Do MMMM  YYYY '):''
          return (
            <div className="campaignTaskDetails">
                <div className='action-button'>
                <ul className='dd-menu context-menu user_3dots'>
                    <li className='button-dropdown'>
                        <a className='dropdown-toggle user_drop_toggle'>
                        <i className='material-icons'>more_vert</i>
                        </a>

                        <ul className='dropdown-menu user_dropAction'>
                        <li>
                            <a onClick={this.props.editEvent.bind(this)}>
                            Edit
                            </a>
                        </li> 
                        {
                          this.props.roleName=='super-admin'|| this.props.roleName=='admin'?  
                        <li>
                            <a onClick={this.props.deleteEvent.bind(this,currEvent.identity)}>
                                Delete
                            </a>
                        </li>: ''}
                        </ul>
                    </li>
                    </ul>
                    </div>
                <div>
                    <DataRow label='Event Title' value={currEvent.title} />
                    <DataRow label='Date of Event' value={dueDate} />
                    <DataRow label='Notes' value={currEvent.notes} />
                    <DataRow label='Created By' value={currEvent.created_by}/>
                </div>
            </div>

          )
      }
}
module.exports = CampaignEventDetail