import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Field, reduxForm } from 'redux-form'
import Globals from "../../Globals";
import * as campaignActions from '../../actions/campaigns/campaignsActions';
import PreLoader from '../PreLoader';
import { renderField } from '../Helpers/ReduxForm/RenderField'
import Post from '../Feed/Post';
import CenterPopupWrapper from '../CenterPopupWrapper'
import PopupWrapper from '../PopupWrapper'
import reactable from '../Chunks/ChunkReactable'
import notie from "notie/dist/notie.js";
import * as utils from '../../utils/utils';
import CampaignTasks from './CampaignTasks';
// import $ from '../Chunks/ChunkJquery'
import FetchuserDetail from '../FetchUsersDetail/FetchUsersDetail';
var limitCampUser=4;
var search;
class CampaignPlanner extends React.Component {
    constructor (props) {
      super(props);
      this.state={
        currentTab:'openProject',
        leftSidebarShown:false ,
        statusFilter:[],
        userFilter:[],
        timeFilter:[],
        default_statusFilter:[],
        typeFilter:'',
        reactable:null,
        showAssignPopup:{},
        allProjectAssignee:{},
        filter_state:{},
        userSelected: 'All user',
        statusSelected: 'All status',
        openPreviewPopup:false,
        previewLink:{},
        listFilter:null
      }
    }


    componentWillMount(){
      reactable().then(reactable=>{
          this.setState({
               reactable:reactable,
              typeFilter:this.props.pathValue=='planner'?'post':'All'
          })
      })
    }
    componentDidMount(){
      if(this.props.pathValue=="production"){
        this.setState({
            currentTab : "openProject",
            typeFilter :  "All"
        },()=>{
         this.fetchFilterDataFromAPI()
         })
        }else if(this.props.pathValue=="planner"){
          this.setState({
            typeFilter :  "post"
        },()=>{
         this.fetchFilterDataFromAPI()
         })
        }
         this.handleInitialize()
         var me=this
        // to close popup on outside of click
        window.addEventListener('click', function (event) {
          if(!!event.target.closest('.assign-author')){
          }else{
            if(!!document.getElementsByClassName('assign-popup') && document.getElementsByClassName('assign-popup')[0] !== undefined){
               document.getElementsByClassName('assign-popup')[0].classList.add('hide');
            }
                    me.setState({showAssignPopup:{}})
          }
        })
      //   $('body').click(function (e) {
      //     if (!$(e.target).closest('.assign-popup').length && !$(e.target).closest('.assign-author').length) {
      //         $('.assign-popup').hide()
      //         me.setState({showAssignPopup:{}})
      //     }
      // })
    }
    handleInitialize (nextProps = null) {
      let typeFilter='post';
      if(this.props.pathValue=="production"){
        typeFilter ="All"
      }
        const initData = {
         typeFilter: typeFilter,
        }
        this.props.initialize(initData)

        if(this.props.campaigns.redirectToProductionForPending== true)
        {  
          const initData1= {
            default_statusFilter:'pending'
           }
          this.props.initialize(initData1);
          this.initializePendingContentAfterRedirectingFromOverView();
        }
        else
        {
          this.initializeDataAfterRedirectedFromOverviewPage();
        }
    }
    /**
     * this will auto select user and status after coming from overpage's see more click
     * @author disha
     */

    initializeDataAfterRedirectedFromOverviewPage(){

      if(this.props.callFromOverViewPage !== undefined && this.props.callFromOverViewPage == true){
        if(this.props.campaignAllUserList.campaignAllUserListData.length>0){
          var userTag=this.props.campaignAllUserList.campaignAllUserListData;
          for (let index = 0; index < userTag.length; index++) {
            const element = userTag[index];
            if(element.id == this.props.profile.identity){
              if($('#userTag-'+element.id) !== undefined){
                $( '#userTag-'+element.id )
                  .click(function( event, a, b ) {
                  })
                  .trigger( "click", [ 'user',element.id ] );
              }
            }
          }
        }
        if(this.props.redirectStatusType !== undefined){
          if(this.props.redirectStatusType == 'inreview'){
            if($('#inreview') !== undefined){
              $( '#inreview')
                .click(function( event, a, b ) {
                })
                .trigger( "click", [ 'status',"4" ] );
            }
          }else if(this.props.redirectStatusType=="inprocess"){
            if($('#inprocess') !== undefined){
              $( '#inprocess')
                .click(function( event, a, b ) {
                })
                .trigger( "click", [ 'status',"2" ] );
            }
          }
        } 
      }
    }


    initializePendingContentAfterRedirectingFromOverView(){
          var me=this
            if($('#default_statusFilter') !== undefined ){
              $( '#default_statusFilter')
                .click(function( event, a, b ) {
                  me.handleFilter("default_status","pending",event)
                })
            }        
    }
 
    componentWillReceiveProps(nextProps){
      if(this.props.campaigns.deleteProjectLoading!==nextProps.campaigns.deleteProjectLoading && nextProps.campaigns.deleteProjectLoading==false){
        this.fetchFilterDataFromAPI()
      }
        if(this.props.campaigns.contentListsLoading!==nextProps.campaigns.contentListsLoading){
            if(nextProps.campaigns.contentListsLoading!==true){
              if(this.props.pathValue=="production"){
              this.setState({
                currentTab:"openProject"
                })
              }
            }
        }
        this.fetchAssigneeListTypeWise(nextProps);
    }
    fetchAssigneeListTypeWise(nextProps){
      var displayType=this.state.typeFilter? this.state.typeFilter : this.props.pathValue=='planner'?'post':'All';
      var projectList = [];
      switch(displayType){
        case "print": 
        if (this.props.campaigns.plannerData.print !==nextProps.campaigns.plannerData.print &&
          nextProps.campaigns.plannerData.print.length > 0
          ) {
            projectList = nextProps.campaigns.plannerData.print
          }
        break;
        case "email": 
        //  displayData=this.renderEmails();
          if (this.props.campaigns.plannerData.email !==nextProps.campaigns.plannerData.email &&
                nextProps.campaigns.plannerData.email.length > 0
                 ) {
                   projectList = nextProps.campaigns.plannerData.email
                 }
        break;
        case "projects":
          if (this.props.campaigns.contentProjectList !==nextProps.campaigns.contentProjectList &&
            nextProps.campaigns.contentProjectList.length > 0
            ) {
              projectList = nextProps.campaigns.contentProjectList
            }
          break;
        case "All":
            if (this.props.campaigns.contentProjectList !==nextProps.campaigns.contentProjectList &&
              nextProps.campaigns.contentProjectList.length > 0
              ) {
                projectList = nextProps.campaigns.contentProjectList
              }
            break;
      }
      if (projectList.length>0) {
          var assignProjectWise={};
          for (let index = 0; index < projectList.length; index++) {
              const projectid = projectList[index].identity;
              assignProjectWise={...assignProjectWise,[`projectAssignee_${displayType}_${projectid}`]:projectList[index].assignee}
          }
          this.setState({
              filter_state: assignProjectWise,allProjectAssignee:assignProjectWise 
          })
      }
    }
    fecthContentListFilter(type){
      if(this.state.currentTab!==type){
      this.setState({
          currentTab :type,
      },()=>this.fetchFilterDataFromAPI())
      }
      this.toggleLeftSidebar();
  }
  toggleLeftSidebar(){
      this.setState({
        leftSidebarShown:!this.state.leftSidebarShown
      })
    }

    handleFilter(type,value,e){
      this.setState({
        currentTab:'',
        leftSidebarShown:false
      })
        switch(type){
          case "type": 

          if(e.target.value!==''){
              this.setState({
                  typeFilter :  e.target.value
              },()=>{
               this.fetchFilterDataFromAPI()
              })
          }else{
              this.setState({
                  typeFilter:null
              },()=>{
               this.fetchFilterDataFromAPI()
              })
          }
          break;
            case "status":
                    if(e.target.checked==true){
                        var tempArray = this.state.statusFilter;
                        tempArray.push(value);
                        let statusCount = this.state.statusSelected == 'All status' ? 0 : this.state.statusSelected;
                        this.setState({
                            statusFilter :  tempArray,
                            statusSelected: statusCount+1
                        },()=>{
                           this.fetchFilterDataFromAPI()
                        })
                    }else{
                        var tempArray = this.state.statusFilter;
                        tempArray = tempArray.filter(function(data, index, arr){
                            return data !== value;
                        });
                        let statusCount = this.state.statusSelected - 1;
                        this.setState({
                            statusFilter :  tempArray,
                            statusSelected : statusCount == 0 ? 'All status' : statusCount
                        },()=>{
                           this.fetchFilterDataFromAPI()
                        })
                    }
                    break;
            case "default_status": 
                    if(e.target.value!==''){
                      var targetValue = e.target.value
                        this.setState({
                          default_statusFilter :  targetValue
                        },()=>{
                           this.fetchFilterDataFromAPI()
                        })
                    }
                   else if(value!==''){
                      this.setState({
                        default_statusFilter :  value
                      },()=>{
                         this.fetchFilterDataFromAPI()
                      })
                    }
                    else{
                        this.setState({
                          default_statusFilter:[]
                        },()=>{
                            this.fetchFilterDataFromAPI()
                        })
                    }
                    break;        
            case "time": 

                    if(e.target.value!==''){
                        this.setState({
                            timeFilter :  e.target.value
                        },()=>{
                           this.fetchFilterDataFromAPI()
                        })
                    }else{
                        this.setState({
                            timeFilter:[]
                        },()=>{
                            this.fetchFilterDataFromAPI()
                        })
                    }
                    break;
            case "user": 
                    if(e.target.checked==true){
                        let userCount = this.state.userSelected == 'All user' ? 0 : this.state.userSelected;

                        var tempArray = this.state.userFilter;
                        tempArray.push(value);
                        this.setState({
                            userFilter :  tempArray,
                            userSelected: userCount + 1
                        },()=>{
                           this.fetchFilterDataFromAPI()
                        })
                    }else{
                        var tempArray = this.state.userFilter;
                        tempArray = tempArray.filter(function(data, index, arr){

                            return data !== value;
                        
                        });
                        let userCount = this.state.userSelected - 1;

                        this.setState({
                            userFilter :  tempArray,
                            userSelected: userCount == 0 ? 'All user' : userCount
                        },()=>{
                           this.fetchFilterDataFromAPI()
                        })
                    }
                    break;
                    case 'listFilter':
                    if(e.target.value!==''){
                        this.setState({
                            listFilter :  e.target.value
                        },()=>{
                          this.fetchFilterDataFromAPI()
                        })
                    }else{
                        this.setState({
                            listFilter:null
                        },()=>{
                          this.fetchFilterDataFromAPI()
                        })
                    }
                    break;
                    
        }

    }
    fetchFilterDataFromAPI(){
       var methodName='';
       if(this.state.typeFilter =='post'){
         methodName='fetchPlannerPosts'
       }else if(this.state.typeFilter =='email'){
        methodName='fetchPlannerEmails'
       }else if(this.state.typeFilter =='print'){
        methodName='fetchPlannerPrints'
       }else if(this.state.typeFilter=='projects'){
        methodName='fetchPlannerProjects'
       }else if(this.state.typeFilter=='All'){
        methodName='fetchPlannerProjects'
       }
       else{
        methodName='fetchPlannerProjects'
       }
      var filterData ={
         status: this.state.statusFilter,
         user:this.state.userFilter,
         creation_time:this.state.timeFilter,
         listFilter : this.state.listFilter,
         default_status: this.state.default_statusFilter
      }
      if(this.state.typeFilter !== null){
        var objToSend={
          type:this.state.typeFilter
        }
        Object.assign(filterData,objToSend)
      }
      if(this.state.typeFilter =='task'){
        const callFromPlanner = true
        this.props.campaignActions.fetchTasks(this.props.campaign.identity,null,this.props.roleName,this.props.profile.identity,filterData,null,callFromPlanner,this.props.Moment);  
      }else{
         let location = this.props.pathValue=="production"?"production":"planner";
         this.props.campaignActions[methodName](this.props.campaign.identity,this.props.Moment,this.state.currentTab,filterData,null,location);
       }
    }

    renderContentListLoader(){
      return (
        <div className=" loader-culture-tableExpand">
                                    <div>
                                        <div className="table-anytlics-body">
                                            <table className="table responsive-table">
                                                <thead>
                                                    <tr className="reactable-column-header">
                                                        <th className="reactable-th-serialno reactable-header-sortable serialno  a-center">
                                                            <div className='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                                                        </th>
                                                        <th className="reactable-th-serialno reactable-header-sortable serialno  a-center">
                                                            <div className='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                                                        </th>
                                                        <th className="reactable-th-serialno reactable-header-sortable serialno  a-center">
                                                            <div className='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                                                        </th>
                                                        <th className="reactable-th-serialno reactable-header-sortable serialno  a-center">
                                                            <div className='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                                                        </th>

                                                    </tr>
                                                </thead>
                                                <tbody className="reactable-data">
                                                    <tr className="table-body-text">
                                                        <td>
                                                            <div className='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                                                        </td>
                                                        <td>
                                                            <div className='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                                                        </td>
                                                        <td>
                                                            <div className='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                                                        </td>
                                                        <td>
                                                            <div className='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                                                        </td>
                                                    </tr>
                                                    <tr className="table-body-text">
                                                        <td>
                                                            <div className='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                                                        </td>
                                                        <td>
                                                            <div className='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                                                        </td>
                                                        <td>
                                                            <div className='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                                                        </td>
                                                        <td>
                                                            <div className='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                                                        </td>

                                                    </tr>

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>    
      )
    }
    fetchPostsData(campID,currView){
      this.props.campaignActions.fetchCampaignPosts(campID,currView,this.props.Moment); 
    }
    renderContentDetail(e){
      if(this.props.pathValue!=="planner" ){
        this.props.handleEditCopy(e.type,e.identity,"edit")
      }
      // this.props.campaignActions.getContentProjectById(e.type,e.identity,this.props.campaign.identity,'edit');
    }
    renderPosts(){
      // var postData={"publisher_identity":"yvEOa","scheduled_at":1566565557,"is_published":1,"post_id":"AkaMq","published_at":1566565557,"SocialAccount":{"data":[]},"post":{"data":{"identity":"AkaMq","type":"internal","detail":null,"approved":"Approved","post_status":"Draft","like_count":0,"share_count":0,"comment_count":0,"latitude":0,"longitude":0,"created":{"date":"2019-08-23 13:05:57.000000","timezone_type":3,"timezone":"UTC"},"is_pin_read":1,"feed_identity":"rpPDp","is_liked":false,"is_pin":0,"pin_description":"Admin Announcement","votes":0,"is_hide":0,"is_user_rate":0,"postOwner":{"data":{"identity":"rpPDp","firstname":"Jackson","lastname":"superadmin","email":"jacksparrow@pirates.com","about":"TalentAdvocate","avatar":"https:\/\/s3-eu-west-2.amazonaws.com\/visibly-cloud\/images\/pirates_com\/1\/i22052019173653615.png","user_position":"CSO","notification_on":true,"connect_to_social":true,"device":"android","device_id":"APA91bHcRjVHp3oaPGR9wRGlQSXoaMIwMDrVkntM2YkVE2JyBoNHGGLcscdIKgDkuHD0o5TVNRuZY50gZc1uam8iBnzThvZTEDBUI-GTNgmicXbr-_GIE0z-BhFaV09LUmchbvv3-j3b","lat":0,"long":0,"auth_token":"FRZiaBnfeo","paused":0,"Department":{"data":{"identity":"Opjmp","name":"Information Tech","total_user":9,"total_post":2033}}}},"assetDetail":{"data":[{"identity":"ymOgd","type":"local","media_url":"https:\/\/s3-eu-west-2.amazonaws.com\/visibly-cloud\/images\/pirates_com\/1\/i78133214-9499-4B09-B9A1-62D7FD84B0B3-74665-000011B8433FF83C.jpeg","thumbnail_url":"https:\/\/s3-eu-west-2.amazonaws.com\/visibly-cloud\/images\/pirates_com\/1\/i301E8855-C686-4368-B841-696B7CEB1902-74665-000011B84340F318.jpeg","media_type":"image\/jpeg","media_extension":"jpeg","media_size":882193,"media_duration":0,"height":1334,"width":750}]},"tags":{"data":[]},"userTag":{"data":[]},"LinkPreview":{"data":[]},"feed":{"data":[{"identity":"rpPDp","name":"pirates","type":"both"}]},"value":{"data":[]}}}}
      var posts= this.props.posts
      return(
        <div id='campaign-posts'>
        {this.props.campaigns.plannerData.loading? <PreLoader feedLoader = {true}/> : 
          <div className="scheduledPost">
            
                <div id="feed">
                  <div className="popup-post-wrapper">
                    {posts.length == 0 && this.state.loading !== true ? (
                      <div className="no-data-block">No post found.</div>
                    ) : (
                      <div className  = "innerpopup-post-wrapper"> 
                        {posts.map((postData, postKey) => {
                          if (
                            this.props.Moment != null &&
                            typeof this.props.Moment.tz !== "undefined"
                          ) {
                            let post_date = this.props.Moment
                              .tz(
                                postData.scheduled_at * 1000,
                                Globals.SERVER_TZ
                              )
                              .toNow();
                            let prev_date = 0;
                            if (postKey > 0) {
                              // get previous date so avoiding repetition of date header/title
                              prev_date = this.props.Moment
                                .tz(
                                  posts[postKey - 1].scheduled_at * 1000,
                                  Globals.SERVER_TZ
                                )
                                .toNow();
                            }
                            var isOwner =
                              this.props.profile.identity ==
                              postData.post.data.postOwner.data.identity
                                ? true
                                : false;
                            return (
                              <div key={`postkey_${postKey}`} className = "post-campaign">
                                {post_date !== prev_date ? (
                                  <h3 class="title-post-date">
                                    {this.props.Moment.unix(postData.scheduled_at)
                                      .format("dddd, Do MMMM  YYYY")}
                                  </h3>
                                ) : null}
                                <div className="post-list-wrapper">
                                  <Post
                                    fromCalendarSchedule={true}
                                    Slider={this.state.Slider}
                                    key={postKey}
                                    details={postData}
                                    showLikesComment={false}
                                    campaignPost={true}
                                    isOwner={isOwner}
                                    locationInfo={this.props.location}
                                    changeScheduleAction={this.props.changeScheduleAction}
                                  />
                                </div>
                              </div>
                            );
                          }
                        })}
                      </div>
                    )}
                  </div>
                </div>
              
          </div>}
      </div>
      )
    }
    showAssignPopup(projectId,callFor) {
      var flag=false;
      if(this.state.showAssignPopup !== undefined && this.state.showAssignPopup[`assigneePopup`+callFor+`_`+projectId] !== undefined ){
          flag=this.state.showAssignPopup[`assigneePopup`+callFor+`_`+projectId]
      }else{
          flag=false
      }
      this.setState({
          showAssignPopup:{[`assigneePopup`+callFor+`_`+projectId]:!flag}
      })
  }
  handleSearchValue(projectId,type,e) {
    if (e !== null) {
        if (e.target.value !== '' && this.state.allProjectAssignee[`projectAssignee_${type}_${projectId}`] !== undefined) {
            var i = 0
            search = e.target.value.toLowerCase()
            var tempobj = this.state.allProjectAssignee[`projectAssignee_${type}_${projectId}`].filter(function (temp) {
                        i = i + 1
                        if (type== 'print'){
                          if(temp.name.toLowerCase().includes(search)){
                            return true
                          }
                         }else if(type=='projects'){
                            if(temp.first_name.toLowerCase().includes(search) ||
                            temp.last_name.toLowerCase().includes(search)) {
                            return true
                        }
                      }
            })

            this.setState({ 
                filter_state:{...this.state.filter_state,[`projectAssignee_${type}_${projectId}`]:tempobj}
            })
        } else {
            this.setState({ filter_state: this.state.allProjectAssignee })
        }
    }
  }

  openPreviewPopupIsClicked(previewLink){
    this.setState({
        openPreviewPopup:true,
        previewLink:previewLink
    })
  }
  closePrintbutton(){
    document.body.classList.remove('center-wrapper-body-container');
    this.setState({
        openPreviewPopup:false,
        previewLink:{}
    })
}
  openPreviewPopup(){
    let  html_Preview_campaigns = "";
    let templatePreview ="";

     document.body.classList.add('center-wrapper-body-container');
      var userList=[];
      var media_url='https://s3.eu-west-2.amazonaws.com/visibly-staging/images/pirates_com/1/W1567571744e0+%281%29-68dbe416-9c46-b7f3-f1ae-ad3b0af60125.pdf';
      if(this.state.previewLink.type == "print"){
      if(this.state.previewLink.pdf_s3_url){
          media_url = this.state.previewLink.pdf_s3_url;
      }else if(this.state.previewLink.pdf_url){
          media_url = this.state.previewLink.pdf_url;
      }
    }else{
        html_Preview_campaigns = this.state.previewLink.image_url;
        templatePreview = decodeURIComponent(escape(atob(html_Preview_campaigns)));
      }
      return(
      <div>
              <CenterPopupWrapper>
                  <div className = "print-page-container">
                  <div className='inner-print-page-container'>
                      <div className='print-details'>
                          <header className='heading clearfix'>
                              <h3> {this.state.previewLink.title} {this.state.previewLink.type == "email" ? <span className = "email-from"> &lt;<span className = "user-email-cmp"> {this.state.previewLink.from_email}</span>&gt;</span> :''}</h3>
                              <button id="close-popup" class="btn-default" onClick = {this.closePrintbutton.bind(this)}><i class="material-icons">clear</i></button>
                          </header>
                          <PopupWrapper>
                          <div className = "linkpreview-parent-class">
                          {this.state.previewLink.type == "print" ?  
                              <div>
                                  <iframe className = "pdf-preview campPlanner"
                                  src={`https://docs.google.com/gview?url=${media_url}&embedded=true`}
                                  />
                              </div>
                               : 
                             <div id = "emailWrapper"> 
                               <div className = "details-mail">
                                 <span className = "email-form-subject"> Subject : <span className = "value-text">  {this.state.previewLink.subject} </span>  </span>
                                 <span className = "email-form-email_preview"> Email Preview : <span  className = "value-text"> {this.state.previewLink.email_preview} </span>  </span>
                                </div>
                               <div dangerouslySetInnerHTML={{__html: templatePreview}} className="inner-pdf-wrapper"></div>
                             </div>
                               }
                              </div>
                          </PopupWrapper>
                      </div>
                  </div>
                  </div>
              </CenterPopupWrapper>
      </div>
      )
  }

    renderProjects(){

      var printData=this.props.campaigns.plannerData
      var projectData = this.props.campaigns.contentProjectList;
      var me = this;
      return(
        <div className="campaigntaskTableWrapper">
          {this.props.campaigns.contentProjectListLoading?
            this.renderContentListLoader()
            :
              projectData.length>0  && this.state.reactable ?
              <this.state.reactable.Table
              className='table department-list-table responsive-table'
              id='contentProject-table'
              itemsPerPage={10}
              pageButtonLimit={5}
            >
              <this.state.reactable.Thead>
                <this.state.reactable.Th column='title' className='a-center department-id-col'>Title</this.state.reactable.Th>
                <this.state.reactable.Th column='type'>Type</this.state.reactable.Th>
                <this.state.reactable.Th className='a-center' column='due_date'>Due date</this.state.reactable.Th>
                <this.state.reactable.Th column='assigned_to' className='actiondiv a-center '>Assigned to </this.state.reactable.Th>
                <this.state.reactable.Th column='created_by' className='actiondiv a-center'>Created by</this.state.reactable.Th>
                {/* {this.props.roleName=="admin" || this.props.roleName=="super-admin" ?  */}
                <this.state.reactable.Th column='actions' className='actiondiv a-center'>Actions</this.state.reactable.Th>
                {/* :''} */}
              </this.state.reactable.Thead>
                { projectData.map((clist,index)=>{
                    var shipping_date =  this.props.Moment!==null? this.props.Moment.unix(parseInt(clist.shipping_date)).format('DD-MM-YYYY'):'';
                    return(
                  <this.state.reactable.Tr key={`renderProjectList_${index}`}>
                    <this.state.reactable.Td className='fd a-center ' column='title' data-rwd-label='Title' >
                    <span onClick={this.renderContentDetail.bind(this,clist)}>{utils.limitLetters(clist.title,40)}</span>
                    </this.state.reactable.Td>
                    <this.state.reactable.Td className='fd a-center' column='type' data-rwd-label='Type'>
                      {clist.type}
                    </this.state.reactable.Td>

                    <this.state.reactable.Td className='fd a-center' column='due_date' data-rwd-label='Due date'>
                      {shipping_date}
                    </this.state.reactable.Td>
                    
                    <this.state.reactable.Td className='fd a-center assign-wrapper-in-table' column='assigned_to' data-rwd-label='Assigned to'>
                    <div className='camapignAssignAuthorWrapper'>
                            <div className='assign-author multiple-assign-author'>
                                <div className='multiple-assign'>
                                    {clist.assignee && clist.assignee.length > 0
                                        ? clist.assignee.map((projectUsers, i) => {
                                            if (i < limitCampUser) {  
                                                return (
                                                    <div
                                                        key={`assignee_` + i}
                                                        className='profile-wrapper assignAuthorProfile post'
                                                        onClick={this.showAssignPopup.bind(this,clist.identity,'project')}
                                                    >
                                                        <span className='author-image author-thumb'>
                                                            <img
                                                                src={projectUsers.avatar}
                                                                alt={'Default avatar'}
                                                                className='list-avatar thumbnail'
                                                                data-tip={projectUsers.email}
                                                                data-for={projectUsers.id}
                                                                onError={e => {
                                                                    e.target.src =
                                                                        'https://app.visibly.io/img/default-avatar.png'
                                                                }}
                                                            />
                                                            <i class='material-icons add-icon'>add</i>
                                                            <FetchuserDetail userID={projectUsers.id}/>
                                                        </span>
                                                    </div>
                                                )
                                            }
                                        })
                                        : ''}
                                    {clist.assignee && clist.assignee.length > 0 &&
                                        clist.assignee.length > limitCampUser ? (
                                            <div
                                                className='profile-wrapper assignAuthorWrapper'
                                                onClick={this.showAssignPopup.bind(this,clist.identity,'project')}
                                            >
                                                <span className='author-image author-thumb'>
                                                    <span className='noCount'>
                                                        <i class='material-icons'>add</i>
                                                        {Math.abs(
                                                            limitCampUser - clist.assignee.length
                                                        )}
                                                    </span>
                                                </span>{' '}
                                            </div>
                                        ) : (
                                            ''
                                        )}
                                </div>
                                { this.state.showAssignPopup && this.state.showAssignPopup[`assigneePopupproject_`+clist.identity] == true ? (
                                    <div className='assign-popup'>
                                        <div className='search-input'>
                                            <input
                                                placeholder='Search Name'
                                                id='searchUser'
                                                type='text'
                                                name='searchUser'
                                                onChange={this.handleSearchValue.bind(this,clist.identity,'projects')}
                                            />
                                        </div>
                                        <div className='assign-popup-block-wrapper'>
                                            {this.state.filter_state[`projectAssignee_${this.props.pathValue=='production'?'All':'projects'}_${clist.identity}`]!== undefined && this.state.filter_state[`projectAssignee_${this.props.pathValue=='production'?'All':'projects'}_${clist.identity}`].length > 0
                                                ? this.state.filter_state[`projectAssignee_${this.props.pathValue=='production'?'All':'projects'}_${clist.identity}`].map((projectUsers, i) => {
                                                    return (
                                                        <div
                                                            className={`assign-block`}
                                                            key={`assignUserPopup_` + i}
                                                        >
                                                            <div className='profile-wrapper assignAuthorProfile'>
                                                                <span className='author-image author-thumb'>
                                                                    <img
                                                                        src={projectUsers.avatar}
                                                                        alt={'Default avatar'}
                                                                        className='list-avatar'
                                                                        data-tip={projectUsers.email}
                                                                        data-for={projectUsers.id}
                                                                        onError={e => {
                                                                            e.target.src =
                                                                                'https://app.visibly.io/img/default-avatar.png'
                                                                        }}
                                                                    />
                                                                    <i class='material-icons add-icon'>add</i>
                                                                </span>
                                                            </div>
                                                            <span className='auhtor-name'>
                                                                {' '}
                                                                {projectUsers.first_name +
                                                                    ' ' +
                                                                    projectUsers.last_name}{' '}
                                                            </span>
                                                        </div>
                                                    )
                                                })
                                                : <div className='NovalueFound'>
                                                    <li className = "no-Value-chart-wrapper"> No assignee found.</li>
                                                    
                                                </div>}
                                        </div>
                                    </div>
                                ) : (
                                        ''
                                    )}
                            </div>
                        </div>
                    </this.state.reactable.Td>

                    <this.state.reactable.Td className='fd a-center' column='created_by' data-rwd-label='Created by'>
                        {clist.User.data.firstname +" "+clist.User.data.lastname}
                    </this.state.reactable.Td>

                    <this.state.reactable.Td
                      className='fd department-action actiondiv a-center'
                      column='actions'
                      value={clist.title}
                    >
                      <div className='action-button'>
                                  <ul class="dd-menu context-menu">
                                      <li class="button-dropdown"><a class="dropdown-toggle"><i class="material-icons">more_vert</i></a>
                                          <ul class="dropdown-menu">
                                           {/* {this.props.profile.identity !== undefined && this.props.profile.identity == clist.User.data.identity? */}
                                          {this.props.pathValue=="planner"?
                                           <li>
                                            <a onClick={this.openPreviewPopupIsClicked.bind(this,clist)}>Preview</a>
                                          </li> :''}
                                          <li>
                                            <a onClick={()=>{this.props.handleEditCopy(clist.type,clist.identity,"edit")}}>Edit</a>
                                          </li> 
                                          {/* :''} */}
                                          {this.props.profile.identity !== undefined && this.props.profile.identity == clist.User.data.identity || this.props.roleName=='super-admin'?
                                          <li><a onClick={()=>{this.props.handleEditCopy(clist.type,clist.identity,"copy")}}>Copy</a>
                                          </li>:''}
                                          {this.props.profile.identity !== undefined && this.props.profile.identity == clist.User.data.identity ||this.props.roleName=='super-admin'?<li>
                                            <a onClick={()=>{
                                              notie.confirm(
                                                `Are you sure you want to delete this Content Project?`,
                                                'Yes',
                                                'No',
                                               function(){
                                                me.props.handleDelete(clist.type,clist.identity)
                                                },function(){}
                                              )}
                                            }>Delete</a></li>:''}
                                          </ul>
                                      </li>
                                    </ul>
                      </div>
                    </this.state.reactable.Td>
    
                  </this.state.reactable.Tr>
                )
              })}
            </this.state.reactable.Table>
              :<div class="no-data-block"> No content projects found. </div>}
        </div>
      );
    }

    renderPrints(){
      var projectData =this.props.campaigns.plannerData.print;
      var me = this;
      return(
        <div className="campaigntaskTableWrapper">
          {this.props.campaigns.plannerData.loading?
            this.renderContentListLoader()
              :
              projectData.length >0 && this.state.reactable?
              <this.state.reactable.Table
              className='table department-list-table responsive-table'
              id='contentProject-table'
              itemsPerPage={10}
              pageButtonLimit={5}
            >
              <this.state.reactable.Thead>
                <this.state.reactable.Th column='title' className='a-center department-id-col'>Title</this.state.reactable.Th>
                <this.state.reactable.Th column='type'>Type</this.state.reactable.Th>
                <this.state.reactable.Th className='a-center' column='due_date'>Due date</this.state.reactable.Th>
                <this.state.reactable.Th column='assigned_to' className='actiondiv a-center'>Assigned to </this.state.reactable.Th>
                <this.state.reactable.Th column='created_by' className='actiondiv a-center'>Created by</this.state.reactable.Th>
                {/* {this.props.roleName=="admin" || this.props.roleName=="super-admin" ?  */}
                <this.state.reactable.Th column='actions' className='actiondiv a-center'>Actions</this.state.reactable.Th>
                {/* :''} */}
              </this.state.reactable.Thead>
                { projectData.map((clist,index)=>{
                    var shipping_date =  this.props.Moment!==null? this.props.Moment.unix(parseInt(clist.shipping_date)).format('DD-MM-YYYY'):'';
                    return(
                  <this.state.reactable.Tr key={`renderPrintList_${index}`}>
                    <this.state.reactable.Td className='fd a-center ' column='title' data-rwd-label='Title'>
                    <span onClick={this.renderContentDetail.bind(this,clist)}>{clist.title}</span>
                    </this.state.reactable.Td>
                    <this.state.reactable.Td className='fd a-center' column='type' data-rwd-label='Type'>
                      {clist.type}
                    </this.state.reactable.Td>

                    <this.state.reactable.Td className='fd a-center' column='due_date' data-rwd-label='Due date'>
                      {shipping_date}
                    </this.state.reactable.Td>
                    
                    <this.state.reactable.Td className='fd a-center assign-wrapper-in-table' column='assigned_to' data-rwd-label='Assigned to'>
                        <div className='camapignAssignAuthorWrapper'>
                            <div className='assign-author multiple-assign-author'>
                                <div className='multiple-assign'>
                                    {clist.assignee && clist.assignee.length > 0
                                        ? clist.assignee.map((projectUsers, i) => {
                                            if (i < limitCampUser) {  
                                                return (
                                                    <div
                                                        key={`assigneePrint_` + i}
                                                        className='profile-wrapper assignAuthorProfile post'
                                                        onClick={this.showAssignPopup.bind(this,clist.identity,'print')}
                                                    >
                                                        <span className='author-image author-thumb'>
                                                            <img
                                                                src={projectUsers.avatar}
                                                                alt={'Default avatar'}
                                                                className='list-avatar thumbnail'
                                                                data-tip={projectUsers.email}
                                                                data-for={projectUsers.identity}
                                                                onError={e => {
                                                                    e.target.src =
                                                                        'https://app.visibly.io/img/default-avatar.png'
                                                                }}
                                                            />
                                                            <i class='material-icons add-icon'>add</i>
                                                            <FetchuserDetail userID={projectUsers.identity}/>
                                                        </span>
                                                    </div>
                                                )
                                            }
                                        })
                                        : ''}
                                    {clist.assignee && clist.assignee.length > 0 &&
                                        clist.assignee.length > limitCampUser ? (
                                            <div
                                                className='profile-wrapper assignAuthorWrapper'
                                                onClick={this.showAssignPopup.bind(this,clist.identity,'print')}
                                            >
                                                <span className='author-image'>
                                                    <span className='noCount'>
                                                        <i class='material-icons'>add</i>
                                                        {Math.abs(
                                                            limitCampUser - clist.assignee.length
                                                        )}
                                                    </span>
                                                </span>{' '}
                                            </div>
                                        ) : (
                                            ''
                                        )}
                                </div>
                                { this.state.showAssignPopup && this.state.showAssignPopup[`assigneePopupprint_`+clist.identity] == true ? (
                                    <div className='assign-popup'>
                                        <div className='search-input'>
                                            <input
                                                placeholder='Search Name'
                                                id='searchUser'
                                                type='text'
                                                name='searchUser'
                                                onChange={this.handleSearchValue.bind(this,clist.identity,'print')}
                                            />
                                        </div>
                                        <div className='assign-popup-block-wrapper'>
                                            {this.state.filter_state[`projectAssignee_print_${clist.identity}`]!== undefined && this.state.filter_state[`projectAssignee_print_${clist.identity}`].length > 0
                                                ? this.state.filter_state[`projectAssignee_print_${clist.identity}`].map((projectUsers, i) => {
                                                    return (
                                                        <div
                                                            className={`assign-block`}
                                                            key={`assignUserPopup_` + i}
                                                        >
                                                            <div className='profile-wrapper assignAuthorProfile post'>
                                                                <span className='author-image author-thumb'>
                                                                    <img
                                                                        src={projectUsers.avatar}
                                                                        alt={'Default avatar'}
                                                                        className='list-avatar thumbnail'
                                                                        data-tip={projectUsers.email}
                                                                        data-for={projectUsers.id}
                                                                        onError={e => {
                                                                            e.target.src =
                                                                                'https://app.visibly.io/img/default-avatar.png'
                                                                        }}
                                                                    />
                                                                    <i class='material-icons add-icon'>add</i>
                                                                    <FetchuserDetail userID={projectUsers.id}/>
                                                                </span>
                                                            </div>
                                                            <span className='auhtor-name'>
                                                                {' '}
                                                                {/* {projectUsers.first_name +
                                                                    ' ' +
                                                                    projectUsers.last_name}{' '} */}
                                                                    {projectUsers.name}
                                                            </span>
                                                        </div>
                                                    )
                                                })
                                                : <div className='NovalueFound'>
                                                    <li className = "no-Value-chart-wrapper"> No assignee found.</li>
                                                    
                                                </div>}
                                        </div>
                                    </div>
                                ) : (
                                        ''
                                    )}
                            </div>
                        </div>
                    </this.state.reactable.Td>

                    <this.state.reactable.Td className='fd a-center' column='created_by' data-rwd-label='Created by'>
                        {clist.created_by.first_name +" "+clist.created_by.last_name }
                    </this.state.reactable.Td>

                    <this.state.reactable.Td
                      className='fd department-action actiondiv a-center'
                      column='actions'
                      value={clist.title}
                    >
                      <div className='action-button'>
                      {/* {this.props.roleName=="admin" || this.props.roleName=="super-admin" ? */}
                                  <ul class="dd-menu context-menu">
                                      <li class="button-dropdown"><a class="dropdown-toggle"><i class="material-icons">more_vert</i></a>
                                          <ul class="dropdown-menu">
                                          {/* {this.props.profile.identity !== undefined && this.props.profile.identity == clist.created_by.identity? */}
                                          {this.props.pathValue=="planner"?
                                           <li>
                                            <a onClick={this.openPreviewPopupIsClicked.bind(this,clist)}>Preview</a>
                                          </li> :''}
                                          <li>
                                            <a onClick={()=>{this.props.handleEditCopy(clist.type,clist.identity,"edit")}}>Edit</a>
                                          </li> 
                                          {/* :''} */}
                                          {this.props.profile.identity !== undefined && this.props.profile.identity == clist.created_by.identity || this.props.roleName=='super-admin'?  
                                            <li><a onClick={()=>{this.props.handleEditCopy(clist.type,clist.identity,"copy")}}>Copy</a>
                                            </li>:''}
                                          {this.props.profile.identity !== undefined && this.props.profile.identity == clist.created_by.identity || this.props.roleName=='super-admin'?<li>
                                            <a onClick={()=>{
                                              notie.confirm(
                                                `Are you sure you want to delete this Content Project?`,
                                                'Yes',
                                                'No',
                                               function(){
                                                  me.props.handleDelete(clist.type,clist.identity)
                                                },function(){}
                                              )}
                                              }>Delete</a></li>:''}
                                          </ul>
                                      </li>
                                    </ul>
                                    {/* :''}  */}
                      </div>
                    </this.state.reactable.Td>
    
                  </this.state.reactable.Tr>
                )
              })}
            </this.state.reactable.Table>
              :<div class="no-data-block"> No print found. </div>}       
        </div>
      );
    }
    renderEmails(){
      var projectData =this.props.campaigns.plannerData.email;
      var me = this;
      return(
        <div className="campaigntaskTableWrapper">
          {this.props.campaigns.plannerData.loading?
            this.renderContentListLoader()
              :
              projectData.length >0 && this.state.reactable?
              <this.state.reactable.Table
              className='table department-list-table responsive-table'
              id='contentProject-table'
              itemsPerPage={10}
              pageButtonLimit={5}
            >
              <this.state.reactable.Thead>
                <this.state.reactable.Th column='title' className='a-center department-id-col'>Title</this.state.reactable.Th>
                <this.state.reactable.Th column='type'>Type</this.state.reactable.Th>
                <this.state.reactable.Th className='a-center' column='due_date'>Due date</this.state.reactable.Th>
                <this.state.reactable.Th column='assigned_to' className='actiondiv a-center'>Assigned to </this.state.reactable.Th>
                <this.state.reactable.Th column='created_by' className='actiondiv a-center'>Created by</this.state.reactable.Th>
                {/* {this.props.roleName=="admin" || this.props.roleName=="super-admin" ? */}
                <this.state.reactable.Th column='actions' className='actiondiv a-center'>Actions</this.state.reactable.Th>
                {/* :''} */}
              </this.state.reactable.Thead>
                { projectData.map((clist,index)=>{ 
                    var shipping_date =  this.props.Moment!==null? this.props.Moment.unix(parseInt(clist.shipping_date)).format('DD-MM-YYYY'):'';
                    return(
                  <this.state.reactable.Tr key={`rendeEmailList_${index}`}>
                    <this.state.reactable.Td className='fd a-center ' column='title' data-rwd-label='Title' >
                    <span onClick={this.renderContentDetail.bind(this,clist)}>{clist.title}</span>
                    </this.state.reactable.Td>
                    <this.state.reactable.Td className='fd a-center' column='type' data-rwd-label='Type'>
                      {clist.type}
                    </this.state.reactable.Td>

                    <this.state.reactable.Td className='fd a-center' column='due_date' data-rwd-label='Due date'>
                      {shipping_date}
                    </this.state.reactable.Td>
                    
                    <this.state.reactable.Td className='fd a-center assign-wrapper-in-table' column='assigned_to' data-rwd-label='Assigned to'>
                        <div className="taskAssignTo">
                        {clist.assignee && clist.assignee.length > 0 ?clist.assignee.map((cAss,cAssindex)=>{
                                return(
                                  <div key={`renderEmailAss_${cAssindex}`}>
                                    <span className="userThumb">
                                      <img src={cAss.avatar} onError={(e)=>{e.target.src="/img/user.jpg"}}/>
                                    </span>
                                    <span>{cAss.name}</span>
                                  </div>
                                )
                                })
                                :"-"}
                        </div>
                    </this.state.reactable.Td>

                    <this.state.reactable.Td className='fd a-center' column='created_by' data-rwd-label='Created by'>
                        {clist.created_by.first_name +" "+clist.created_by.last_name}
                    </this.state.reactable.Td>

                    <this.state.reactable.Td
                      className='fd department-action actiondiv a-center'
                      column='actions'
                      value={clist.title}
                    >
                      <div className='action-button'>
                      {/* {this.props.roleName=="admin" || this.props.roleName=="super-admin" ? */}
                                  <ul class="dd-menu context-menu">
                                      <li class="button-dropdown"><a class="dropdown-toggle"><i class="material-icons">more_vert</i></a>
                                          <ul class="dropdown-menu">
                                          {/* {this.props.profile.identity !== undefined && this.props.profile.identity == clist.User.data.identity? */}
                                          {this.props.pathValue=="planner"?
                                           <li>
                                            <a onClick={this.openPreviewPopupIsClicked.bind(this,clist)}>Preview</a>
                                          </li> :''}
                                          <li>
                                            <a onClick={()=>{this.props.handleEditCopy(clist.type,clist.identity,"edit")}}>Edit</a>
                                          </li>
                                           {/* :''} */}
                                           {this.props.profile.identity !== undefined && this.props.profile.identity == clist.created_by.identity || this.props.roleName=='super-admin'? 
                                            <li><a onClick={()=>{this.props.handleEditCopy(clist.type,clist.identity,"copy")}}>Copy</a></li>:''}
                                          {this.props.profile.identity !== undefined && this.props.profile.identity == clist.created_by.identity || this.props.roleName=='super-admin'?<li>
                                            <a onClick={()=>{
                                              notie.confirm(
                                                `Are you sure you want to delete this Content Project?`,
                                                'Yes',
                                                'No',
                                               function(){
                                                  me.props.handleDelete(clist.type,clist.identity)
                                                },function(){}
                                              )}
                                              }>Delete</a></li>:''}
                                          </ul>
                                      </li>
                                    </ul>
                                    {/* :''}  */}
                      </div>
                    </this.state.reactable.Td>
    
                  </this.state.reactable.Tr>
                )
              })}
            </this.state.reactable.Table>
              :<div class="no-data-block"> No email found. </div>}       
        </div>
      );
    }
    renderTasks(){
     return(
       <div className ='campaigntaskTableWrapper'>
      {this.props.campaigns.tasks.length>0?
      <table className="table responsive-table">
          <thead>
              <tr>
                  {/* <th>
                      <div className="checkbox form-row"><input type="checkbox" id="selectAllTask" /><label for="selectAllTask"></label></div>
                  </th> */}
                  <th>Title</th>
                  <th>Status</th>
                  <th>Due date</th>
                  <th>Assigned to</th>
                  <th>Created by</th>
              </tr>
          </thead>
          <tbody>
          
                 { this.props.campaigns.tasks.map((task,index)=>{
                     var dueDate =  this.props.Moment!==null? this.props.Moment.unix(parseInt(task.due_date)).format('DD-MM-YYYY'):'';
                  return(
                  <tr key={`renderTaskList_${index}`}>
                      {/* <td><div className="checkbox form-row"><input type="checkbox" id="task1"  /><label for="task1"></label></div></td> */}
                      <td data-rwd-label="Title" onClick = {this.props.openDetailPopup.bind(this,task)}>{task.title}</td>
                      <td data-rwd-label="Status">{task.status}</td>
                      <td data-rwd-label="Due date" className="taskDate">{dueDate}</td>
                      <td data-rwd-label="Assigned to" className="taskAssignTo task-assignee-wrapper post">
                          <span className="userThumb author-image author-thumb">
                              <img
                                  src={task.assigneee.avatar?task.assigneee.avatar:'https://app.visibly.io/img/default-avatar.png'}
                                  alt={'Default avatar'}
                                  className='list-avatar thumbnail'
                                  data-tip={task.assigneee.email}
                                  data-for={task.assigneee.identity}
                                  onError={e => {
                                      e.target.src =
                                          'https://app.visibly.io/img/default-avatar.png'
                                  }}
                              />
                              <FetchuserDetail userID={task.assigneee.identity}/>
                          </span>{task.assigneee.first_name +" "+task.assigneee.last_name}</td>
                      <td data-rwd-label="Created by" className="taskAssignTo">{task.created_by.first_name +" "+task.created_by.last_name}</td>
                  </tr>)
                  })
              }
          </tbody>
      </table>:<div class="no-data-block"> No task found. </div>}
      </div>
     )
    }
    renderTypeData(){
      var displayType=this.state.typeFilter? this.state.typeFilter : this.props.pathValue=='planner'?'post':'All';
      var displayData='';
      switch(displayType){
        case "print": 
        displayData=this.renderPrints();
        break;
        case "email": 
          displayData=this.renderEmails()
        break;
        case "post": 
          displayData=this.renderPosts()
        break;
        case "projects":
          displayData = this.renderProjects();
          break;
        case "task":
          displayData = this.renderTasks();
          break;
        case "All":
          displayData = this.renderProjects();
          break;
        default:
            displayData= this.renderPosts()
          break;
      }
      return displayData;
    }
    fetchTask(type){
      if(this.state.currentTab!==type){
          var contentProjectId='';
          if(this.props.callFrom !== undefined && this.props.callFrom == 'contentProject'){
              contentProjectId=this.props.contentProjectData !== undefined ? this.props.contentProjectData.project_identity :'';   
          }
          this.setState({
            currentTab :type,
            statusFilter : [],
            userFilter : [],
            timeFilter : [],
            listFilter : null,
            default_statusFilter : [],
            userSelected: 'All user'
          },()=>{ 
            this.unSelectAllCheckBoxes('allStatusCheckboxes')
            this.unSelectAllCheckBoxes('checknotify')
          })
      this.props.change('timeFilter','')
      this.props.change('usertag',false)
      this.props.change('listFilter',false)
      this.props.change('Pending',false)
      this.props.change('inprogress',false)
      this.props.change('completed',false)
      this.props.change('approved',false)
      this.props.change('unapproved',false)
      const callFromPlanner = true
      this.props.campaignActions.fetchTasks(this.props.campaign.identity,type,this.props.roleName,this.props.profile.identity,null,contentProjectId,callFromPlanner,this.props.Moment);
      }
  }
  unSelectAllCheckBoxes(CheckboxName){
  var items = document.getElementsByClassName(CheckboxName);
        for (var i = 0; i < items.length; i++) {
            if (items[i].type == 'checkbox')
                items[i].checked = false;
        }
  }
    render () {
      var leftSidebarClass=''
      if(this.state.leftSidebarShown){
        leftSidebarClass='leftSidebarShown'
      }else{
        leftSidebarClass=''
      }
      return (
        
          <div className="campaignTasksTab">
            {this.state.openPreviewPopup==true ? this.openPreviewPopup():''}
              <div className={`campaignTasksColumnWrapper clearfix ${this.state.typeFilter !=='task' ? '' :'leftSidebarShownParent'} ${leftSidebarClass}`}>
              <div className="camapaignTaskLeftcolumn">
                  <div className={`camapaignTaskLeftcoumn ${this.props.pathValue!=="production" ? 'hide' :''}`} >
                      <ul>
                          <li className={`${this.state.currentTab=="openProject"?"active":''}`}><a onClick={this.fecthContentListFilter.bind(this,"openProject")}>Open project</a></li>
                          <li className={`${this.state.currentTab=="dueToday"?"active":''}`}><a onClick={this.fecthContentListFilter.bind(this,"dueToday")}>Due today</a></li>
                          <li className={`${this.state.currentTab=="dueWeek"?"active":''}`}><a onClick={this.fecthContentListFilter.bind(this,"dueWeek")}>Due this week</a></li>
                          <li className={`${this.state.currentTab=="overDue"?"active":''}`}><a onClick={this.fecthContentListFilter.bind(this,"overDue")}>Overdue</a></li>
                          <li className={`${this.state.currentTab=="Expired"?"active":''}`}><a onClick={this.fecthContentListFilter.bind(this,"Expired")}>Expired</a></li>
                      </ul>
                  </div>
                  
                  
                    <div className={`camapaignTaskLeftcoumn ${this.props.pathValue!=="planner"? 'hide' : this.state.typeFilter !=='task' ? 'hide' :''}`} >
                        <ul>
                            <li className={`${this.state.currentTab=="openTask"?"active":''}`}><a onClick={this.fetchTask.bind(this,"openTask")}>Open tasks</a></li>
                            <li className={`${this.state.currentTab=="dueToday"?"active":''}`}><a onClick={this.fetchTask.bind(this,"dueToday")}>Due today</a></li>
                            <li className={`${this.state.currentTab=="dueWeek"?"active":''}`}><a onClick={this.fetchTask.bind(this,"dueWeek")}>Due this week</a></li>
                            <li className={`${this.state.currentTab=="overDue"?"active":''}`}><a onClick={this.fetchTask.bind(this,"overDue")}>Overdue</a></li>
                            <li className={`${this.state.currentTab=="completed"?"active":''}`}><a onClick={this.fetchTask.bind(this,"completed")}>Completed</a></li>
                        </ul>
                    </div>
                    </div> 
                  <div className={`camapaignTaskRightcoumn ${this.props.pathValue == "planner" && this.state.typeFilter !=='task' ?'taskFilter' :''}`}>
                  <div className="leftsidebarToggleBtnWrapper">
                    <div className={`leftsidebarToggleBtnWrapper ${this.props.pathValue!=="planner"? 'hide' : this.state.typeFilter !=='task' ? 'hide' :''}`}><button onClick={this.toggleLeftSidebar.bind(this)} className="btn-theme">Tasks</button></div>
                    <div className={`camapaignTaskLeftcoumn ${this.props.pathValue!=="production" ? 'hide' :''}`} >
                      <ul>
                          <li className={`${this.state.currentTab=="openProject"?"active":''}`}><a onClick={this.fecthContentListFilter.bind(this,"openProject")}>Open project</a></li>
                          <li className={`${this.state.currentTab=="dueToday"?"active":''}`}><a onClick={this.fecthContentListFilter.bind(this,"dueToday")}>Due today</a></li>
                          <li className={`${this.state.currentTab=="dueWeek"?"active":''}`}><a onClick={this.fecthContentListFilter.bind(this,"dueWeek")}>Due this week</a></li>
                          <li className={`${this.state.currentTab=="overDue"?"active":''}`}><a onClick={this.fecthContentListFilter.bind(this,"overDue")}>Overdue</a></li>
                          <li className={`${this.state.currentTab=="Expired"?"active":''}`}><a onClick={this.fecthContentListFilter.bind(this,"Expired")}>Expired</a></li>
                      </ul>
                  </div>
                  
                  
                    <div className={`camapaignTaskLeftcoumn ${this.props.pathValue!=="planner"? 'hide' : this.state.typeFilter !=='task' ? 'hide' :''}`} >
                        <ul>
                            <li className={`${this.state.currentTab=="openTask"?"active":''}`}><a onClick={this.fetchTask.bind(this,"openTask")}>Open tasks</a></li>
                            <li className={`${this.state.currentTab=="dueToday"?"active":''}`}><a onClick={this.fetchTask.bind(this,"dueToday")}>Due today</a></li>
                            <li className={`${this.state.currentTab=="dueWeek"?"active":''}`}><a onClick={this.fetchTask.bind(this,"dueWeek")}>Due this week</a></li>
                            <li className={`${this.state.currentTab=="overDue"?"active":''}`}><a onClick={this.fetchTask.bind(this,"overDue")}>Overdue</a></li>
                            <li className={`${this.state.currentTab=="completed"?"active":''}`}><a onClick={this.fetchTask.bind(this,"completed")}>Completed</a></li>
                        </ul>
                    </div>
                  </div>
                  <div className = "filterCampaignsWrapper  filterInCampaignsTask clearfix">
                    <div className="filterWrapper">
                        
                        <div className="checkbok-menu-button">
                            <div className="filter-label">Filter by</div>
                            <ul class='dd-menu'>
                            
                              <li class='button-dropdown'>
                                    <div className='checkbox form-row'>
                                    <Field
                                        component={renderField}
                                        type='select'
                                        name='typeFilter'
                                        onChange={this.handleFilter.bind(this,"type","")}
                                    >

                                        {this.props.pathValue=="planner"?
                                            <option value={"post"} key={1}>
                                              Posts
                                            </option>:''}
                                        {this.props.pathValue=="planner"?
                                            <option value={"projects"} key={2}>
                                                Content projects
                                            </option>:''}
                                        {this.props.pathValue=="production"?
                                          <option value='All' className ="dropdown-toggle btn btn-theme">All</option>:''}     
                                        {this.props.pathValue=="production"?
                                            <option value={"print"} key={3}>
                                                Print
                                            </option>:''}
                                        {this.props.pathValue=="production"?
                                            <option value={"email"} key={4}>
                                                Email
                                            </option> :''}
                                            {this.props.pathValue=="planner"?
                                            <option value={"task"} key={5}>
                                              Tasks
                                            </option>:''}
                                    </Field>

                                          </div>
                                  </li>
                                 
                                {this.props.roleName=="admin" || this.props.roleName=="super-admin" ?
                                <li class='button-dropdown'>
                                <a class='dropdown-toggle btn btn-theme'>{this.state.userSelected == 'All user' ? this.state.userSelected : this.state.userSelected + ' User Selected'}</a>
                                <ul class='dropdown-menu checkboxDropdown'>
                                    {this.props.campaignAllUserList.campaignAllUserListData.length>0?
                                       this.props.campaignAllUserList.campaignAllUserListData.map((user,index)=>{
                                           return(
                                                <li>
                                                <div className='form-row checkbox'>
                                                <input
                                                    className='checknotify'
                                                    type='checkbox'
                                                    name='usertag'
                                                    id={`userTag-${user.id}`}
                                                    onChange={this.handleFilter.bind(this,"user",user.id)}
                                                />
                                                <label for={`userTag-${user.id}`}>{user.display}</label>
                                                </div>
                                            </li>)
                                       })   
                                       :''  
                                } 
                                </ul>
                                </li>
                                :''}
                                {this.state.typeFilter !=='post' && this.props.pathValue!=="planner" ? 
                                <li class='button-dropdown'>
                                <a class='dropdown-toggle btn btn-theme'>{this.state.statusSelected == 'All status' ? this.state.statusSelected : this.state.statusSelected + ' status selected'}</a>
                                <ul class='dropdown-menu checkboxDropdown dropdownSelect'>
                                    <li>
                                      
                                        <div className='form-row'>
                                        <label><Field name="requested" component={renderField} type="checkbox" value="0" onChange={this.handleFilter.bind(this,"status","0")}/> Requested</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className='form-row'>
                                        <label><Field name="draft" component={renderField} type="checkbox" value="1" onChange={this.handleFilter.bind(this,"status","1")}/> Draft</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className='form-row'>
                                        <label><Field name="inprocess" id="inprocess" component={renderField} type="checkbox" value="2"  onChange={this.handleFilter.bind(this,"status","2")}/> In Process</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className='form-row'>
                                        <label><Field name="done" component={renderField} type="checkbox" value="3" onChange={this.handleFilter.bind(this,"status","3")}/> Done</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className='form-row'>
                                        <label><Field name="inreview" id="inreview" component={renderField} type="checkbox" value="4"  onChange={this.handleFilter.bind(this,"status","4")}/> In Review</label>
                                        </div>
                                    </li>
                                    {this.props.pathValue !== 'production'?
                                    <li>
                                        <div className='form-row'>
                                        <label><Field name="signedOff" component={renderField} type="checkbox" value="5"  onChange={this.handleFilter.bind(this,"status","5")}/> Signed Off</label>
                                        </div>
                                    </li>
                                    :''}
                                </ul>
                                </li>:''
                                }
                                {this.props.pathValue=="planner" && this.state.typeFilter !=='task'?
                                <li class='button-dropdown'>
                                            <div className='form-row checkbox'>
                                            <Field
                                                component={renderField}
                                                type='select'
                                                name='timeFilter'
                                                onChange={this.handleFilter.bind(this,"time","")}
                                            >
                                                <option value='' className ="dropdown-toggle btn btn-theme">Scheduled</option>
                                                    <option value={"published"} key={1}>
                                                    Published
                                                    </option>
                                                    <option value={"all"} key={2}>
                                                    All
                                                    </option>
                                            </Field>
                                        </div>
                                </li>:''}
                                {this.state.typeFilter !=='task' ? 
                                  <li class='button-dropdown'>
                                            <div className='form-row checkbox'>
                                            <Field
                                                component={renderField}
                                                type='select'
                                                name='default_statusFilter'
                                                id="default_statusFilter"
                                                onChange={this.handleFilter.bind(this,"default_status","")}
                                            >
                                                    <option value={""} key={1}>
                                                    All Contents
                                                    </option>
                                                    <option value={"pending"} key={2}>
                                                    My pending Contents
                                                    </option>
                                            </Field>
                                        </div>
                                </li>
                                :
                                <li class='button-dropdown'>
                                <a class='dropdown-toggle btn btn-theme'>All Status</a>
                                <ul class='dropdown-menu checkboxDropdown dropdownSelect'>
                                    <li>
                                        <div className='form-row '>
                                        <label><Field name="Pending" id="Pending" component={renderField} type="checkbox" value="pending" onChange={this.handleFilter.bind(this,"status","pending")}/> Pending</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className='form-row'>
                                        <label><Field name="inprogress" component={renderField} type="checkbox" value="in-progress" onChange={this.handleFilter.bind(this,"status","in-progress")}/> In-Progress</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className='form-row'>
                                        <label><Field name="completed" component={renderField} type="checkbox" value="completed"  onChange={this.handleFilter.bind(this,"status","completed")}/> Completed</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className='form-row'>
                                        <label><Field name="approved" component={renderField} type="checkbox" value="approved" onChange={this.handleFilter.bind(this,"status","approved")}/> approved</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className='form-row'>
                                        <label><Field name="unapproved" component={renderField} type="checkbox" value="unapproved"  onChange={this.handleFilter.bind(this,"status","unapproved")}/> unapproved</label>
                                        </div>
                                    </li>
                                </ul>
                                </li>}
                                {this.state.typeFilter =='task' ? 
                                <li class='button-dropdown'>
                                            <div className=' form-row checkbox'>
                                            <Field
                                                component={renderField}
                                                type='select'
                                                name='timeFilter'
                                                onChange={this.handleFilter.bind(this,"time","")}
                                            >
                                                <option value='' className ="dropdown-toggle btn btn-theme">All Time</option>
                                                    <option value={"month"} key={1}>
                                                    this Month
                                                    </option>
                                                    <option value={"week"} key={2}>
                                                    this Week
                                                    </option>
                                                    <option value={"today"} key={3}>
                                                    Today
                                                    </option>
                                            </Field>
                                        </div>
                                </li>:''}
                                {this.state.typeFilter =='task' ? 
                                <li class='button-dropdown'>
                                            <div className='form-row checkbox assign-task-select'>
                                            <Field
                                                component={renderField}
                                                type='select'
                                                name='listFilter'
                                                onChange={this.handleFilter.bind(this,"listFilter","")}
                                            >
                                                <option value='' className ="dropdown-toggle btn btn-theme">All Task</option>
                                                    <option value={"createdby_me"} key={1}>
                                                    Created By Me
                                                    </option>
                                                    <option value={"asssignto_me"} key={2}>
                                                    Assigned To Me
                                                    </option>
                                            </Field>
                                        </div>
                                </li>:''}
                            </ul>
                    </div>
                    </div>
                    </div>
                    {this.renderTypeData()}  
            </div>
            </div>
            </div>
        )
    }
}
function mapStateToProps(state){
    return{
        campaigns:state.campaigns,
        profile : state.profile.profile
    }
}
function mapDispatchToProps(dispatch){
    return{
        campaignActions: bindActionCreators(campaignActions,dispatch),
    }
}

var connection = connect(mapStateToProps,mapDispatchToProps);
var reduxConnectedComponent = connection(CampaignPlanner);
export default reduxForm({
    form: 'CampaignPlanner' // a unique identifier for this form
  })(reduxConnectedComponent)