import TableSearchWrapper from '../TableSearchWrapper';
import CenterPopupWrapper from '../CenterPopupWrapper'
import { bindActionCreators } from 'redux'
import { Field, reduxForm } from 'redux-form'
import { connect } from 'react-redux'
import PreLoader from '../PreLoader';
import AddSegmentPopup from './AddSegmentPopup';
import * as countryActions from '../../actions/countryActions';
import * as companyInfo from '../../actions/settings/companyInfo';
import * as campaignActions from '../../actions/campaigns/campaignsActions'
import * as userActions from '../../actions/userActions';
import * as cultureAnalyticsActions from '../../actions/analytics/cultureAnalyticsActions';
import * as tagsActions from '../../actions/tagsActions';
import * as utils from '../../utils/utils';
import MentionTextArea from '../Helpers/ReduxForm/MentionTextArea';
import notie from "notie/dist/notie.js";
import FetchuserDetail from '../FetchUsersDetail/FetchUsersDetail';
var fetchRecentSegmentsUserListData = null;
var search = '';
var temp = '';
const optionMainConst = [
  { value: 'region', label: 'Region' },
  { value: 'country', label: 'Country'},
  { value: 'office', label: 'Office' },
  { value: 'department', label: 'Department' },
  { value: 'business', label: 'Business unit' },
  { value: 'company_role', label: 'Company role' },
  { value: 'level', label: 'Level' },
  { value: 'gender', label: 'Gender' },
  { value: 'age', label: 'Age' },
  { value: 'start_date', label: 'Start date' },
  { value: 'visibly_role', label: 'Visibly role' } 
];
const required = value => (value ? undefined : 'Required')
let resultLoop = {};
class CampaignSegmentation extends React.Component {
    constructor (props) {
        super(props);
        this.state = {
          newSegmentPreview:false,
          currTab : "today",
          filter_state:[],
          main_state:[],
          leftSidebarShown:false,
          openSegmentPopup: false,
          currSegment:{},
          selectedSegment: {},
          isCopied:false,
          segmentsData:[],
          recentSegmentData :[],
          filterRecentSegment:[],
          recentCampaignFlag:false,
          segmentsData:null,
          isDeleteSegment:false,
          isCopy:false
        }
      }
    componentDidMount(){
        var campId=this.props.campaign.identity;
        var userType ="segment";
        var newLocation = utils.removeTrailingSlash(this.props.location.pathname)
        .split('/')
        this.props.tagsActions.fetchUserTags(newLocation)
        this.props.campaignActions.fetchCampaignSegments(campId,this.state.currTab);
        this.props.campaignActions.fetchCampaignSegmentsUser(campId, userType);
        this.props.campaignActions.fetchCampaignRecentSegments(campId);
      }
      copySegment1(){
        this.setState({
          isCopy:true 
        })
       this.copySegment(this.props.CheckedSegments[0]);
      }
      deleteSegment(){
        this.setState({
          isDeleteSegment: true 
        })
        this.deleteCollaborator(this.props.CheckedSegments);
      }
    componentWillReceiveProps(nextProps){
        if(nextProps.campaigns.addSegmentLoading !==  this.props.campaigns.addSegmentLoading && nextProps.campaigns.addSegmentLoading){
          this.setState({openSegmentPopup: false})
          this.props.closeCreateSegmentPopup();
          // this.props.cultureAnalyticsActions.resetSaveFlag()
          document.body.classList.remove('bdsgmntpp');
         }
        if (nextProps.campaigns.collaboratorsData !==undefined &&  nextProps.campaigns.collaboratorsData.segment!==undefined&&nextProps.campaigns.collaboratorsData.segment.length>0) {
          this.setState({
            collaboratorsData: nextProps.campaigns.collaboratorsData.segment,
          })
          this.setState({main_state:nextProps.campaigns.collaboratorsData.segment})
        this.setState({filter_state:nextProps.campaigns.collaboratorsData.segment})
        }else{
          this.setState({
            collaboratorsData: [],
          })
          this.setState({main_state:[]})
          this.setState({filter_state:[]})
        }


        if (nextProps.campaigns.segmentsData !==undefined &&  nextProps.campaigns.segmentsData.segment!==undefined&&nextProps.campaigns.segmentsData.segment.length>0) {
          this.setState({segmentsData: nextProps.campaigns.segmentsData.segment})
          this.setState({main_state:nextProps.campaigns.segmentsData.segment})
        this.setState({filter_state:nextProps.campaigns.segmentsData.segment})
        }else{
          this.setState({
            segmentsData: [],
          })
          this.setState({main_state:[]})
          this.setState({filter_state:[]})
        }

        if(nextProps.campaigns.particularSegment!==this.props.campaigns.particularSegment){
          this.setState({
            selectedSegment: nextProps.campaigns.particularSegment
          },()=>{
            this.props.openCreateSegmentPopup();
          })
        }
      if(nextProps.campaigns.fetchCampaignRecentSegments !== undefined && nextProps.campaigns.fetchCampaignRecentSegments  !== this.props.campaigns.fetchCampaignRecentSegments && nextProps.campaigns.fetchCampaignRecentSegments != null){
          this.setState({
            recentSegmentData : nextProps.campaigns.fetchCampaignRecentSegments.fetchCampaignRecentSegmentsData.segment,
            filterRecentSegment : nextProps.campaigns.fetchCampaignRecentSegments.fetchCampaignRecentSegmentsData.segment
          })
        }
       
      }
    
      createDropdown() {
        var me = this;
        return (
          <div className='table-menu dropdown-container'>
            <div className='checkbok-menu-button'>
              <ul class='dd-menu'>
                <li class='button-dropdown'>
                  <a class='dropdown-toggle btn btn-theme'>Filter columns</a>
                  <ul class="dropdown-menu checkboxDropdown hide">
                  <li><div class="form-row checkbox"><input type="checkbox" id="userimg" name="userimg" class="checknotify" value="on" /><label for="userimg">User Img</label></div></li>
                  <li><div class="form-row checkbox"><input type="checkbox" id="username" name="username" class="checknotify" value="on" /><label for="username">User Name</label></div></li>
                  <li><div class="form-row checkbox"><input type="checkbox" id="companyposition" name="companyposition" class="checknotify" value="on" /><label for="companyposition">Company Position</label></div></li>
                  <li><div class="form-row checkbox"><input type="checkbox" id="user" name="user" class="checknotify" value="on" /><label for="user">User</label></div></li>
                  <li><div class="form-row checkbox"><input type="checkbox" id="email" name="email" class="checknotify" value="on" /><label for="email">Email</label></div></li>
                  <li><div class="form-row checkbox"><input type="checkbox" id="department" name="department" class="checknotify" value="on" /><label for="department">Department</label></div></li>
                  <li><div class="form-row checkbox"><input type="checkbox" id="status" name="status" class="checknotify" value="on" /><label for="status">Status</label></div></li>
                  <li><div class="form-row checkbox"><input type="checkbox" id="lastlog" name="lastlog" class="checknotify" value="on" /><label for="lastlog">Last Log</label></div></li>
                  <li><div class="form-row checkbox"><input type="checkbox" id="action" name="action" class="checknotify" value="on" /><label for="action">action</label></div></li>
                  </ul>
                </li>
              </ul>
            </div>
          </div>
        )
      }
  showSegmentPreviewPopup(){
    this.setState({
      newSegmentPreview:true
    })
  }
  closeSegmentPreviewPopup(){
    this.setState({
        newSegmentPreview:false
      })
  }

  // add segmentation popup functions
  openSegmentPopup(){
    let segmentState = this.state.openSegmentPopup ? false : true;
    document.body.classList.remove('bdsgmntpp');
    if(!this.state.openSegmentPopup){
        document.body.classList.add('bdsgmntpp');
        // this.props.cultureAnalyticsActions.fetchAllSegments()
    }
    this.setState({openSegmentPopup: segmentState})
}

callFetchFieldData(type,selectedArray){
  resultLoop = {}
  this.selectedLoopCheck(selectedArray);

  let parentId = null
  let parentType = null
  if(typeof resultLoop.parentId !== 'undefined' && resultLoop.parentId.length > 0){
      parentId = resultLoop.parentId
      parentType = resultLoop.parentType
  }
  switch(type){
      case 'region':
           this.props.companyInfoAction.fetchRegion()
      break; 
      case 'country':
           this.props.countryActions.fetchAllCountry(parentId !== null ? parentId : 0,true)
      break;     
      case 'office':
           this.props.companyInfoAction.showMasterData('office',parentId,true,parentType);
      break;
      case 'department':
           this.props.companyInfoAction.showMasterData('department',parentId,true,parentType);
      break;     
      case 'business':
           this.props.companyInfoAction.showMasterData('business',parentId,true,parentType);
      break;     
      case 'company_role':
           this.props.companyInfoAction.showMasterData('role',parentId,true,parentType); 
      break;         
      case 'level':
           this.props.cultureAnalyticsActions.getLevelData();
      break;     
      case 'gender':
           this.props.cultureAnalyticsActions.getGenderData();     
      break;
      case 'visibly_role':
          //  this.props.cultureAnalyticsActions.getVisiblyRoleData();
          this.props.userActions.fetchRoles();
      break;
  }
}
createTagList (str) {
  var me = this
  var rx1 = /@\[([^\]]+)]\(([^)]+)\)/g
  let res = str.match(rx1)
  var userTag = []

  return new Promise(function (resolve, reject) {
    if (res) {
      res.forEach(function (tags) {
        /** Get user information */
        var tagsArray = tags.split('@[')
        var nameArray = tagsArray[1].split(']')
        var name = nameArray[0]
        var dept = nameArray[1]
          .substring(0, nameArray[1].length - 1)
          .split(':')[1]
          .split('_')
        var type = 'tags'
        if (
          nameArray[1].substring(0, nameArray[1].length - 1).split(':')[0] !==
          '(tags'
        ) {
          type = dept[1]=='company'?'company':
          dept[1]=='dept'?
             "department"
            : "user";
          let tagsUserObject = {
            tagid: dept[0],
            tagName: name.replace(/ /g, ''),
            type: type
          }
          userTag.push(tagsUserObject)
        }

        /** description replace */
        if (type === 'tags') {
          str = str.replace(tags, '#' + name.replace(/ /g, ''))
        } else {
          str = str.replace(tags, '@' + name.replace(/ /g, ''))
        }
      })
      me.setState({
        userTag: userTag
      })
    }
    resolve(str)
  })
}
submitForm(valueArray){
  let apiRequest = {};
  var departmentArray=[];
  var userArray=[];
  if(valueArray.currTab=='quickSegment'){
  this.createTagList(valueArray.userPermissions).then(permission => {
    this.state.userTag.length>0?
      this.state.userTag.map((data,index)=>{
        if(data.type=='user'){
          userArray.push(data.tagid)
        }else if(data.type=='department'){
          departmentArray.push(data.tagid)
        }
      })
    :''
    apiRequest['segment'] = {}
    apiRequest['segment']={
      user:[{
        value:userArray
      }],
      department:[{
        value:departmentArray
      }]
    }
    if(valueArray['edit_identity'] !== undefined && this.state.isCopied!==true){
      apiRequest['segment']['identity'] = valueArray['edit_identity']    
    }
  apiRequest.statusSelected = valueArray.selectedStatus;
  apiRequest.name=valueArray.SegmentName
  Object.assign(apiRequest,
    {
      type:"campaign",
      action_id:this.props.campaign.identity
    })
  if(this.state.isCopied==true){
    this.props.campaignActions.AddSegmentData(apiRequest,this.state.currTab,'quickSegment');
    this.setState({
      isCopied:false
    })
    this.props.handleCopyState();
  }else{
    this.props.campaignActions.AddSegmentData(apiRequest,this.state.currTab,'quickSegment');
  }
  })

}else{

  let allSegmentArray = []
  allSegmentArray[0]=[];allSegmentArray[1]=[];allSegmentArray[2]=[];allSegmentArray[3]=[];allSegmentArray[4]=[];allSegmentArray[5]=[];allSegmentArray[6]=[];allSegmentArray[7]=[];allSegmentArray[8]=[];allSegmentArray[9]=[];allSegmentArray[10]=[]
  
        for(let i=0;i<valueArray.noOfSegment;i++){

            if(typeof valueArray['keyName'+i] !== 'undefined' && valueArray['keyName'+i] !== '' && valueArray['valueName'+i]){
                
                let tempArray = []
                    tempArray = [{key: valueArray['keyName'+i], value: valueArray['valueName'+i], operation: valueArray['operateName'+i], selected: valueArray['saveAsSegment'+i]}]

                switch(valueArray['keyName'+i]){
                        case 'region':
                            allSegmentArray[0] = tempArray
                        break;
                        case 'country':
                            allSegmentArray[1] = tempArray
                        break;
                        case 'office':
                            allSegmentArray[2]= tempArray
                        break;
                        case 'department':
                            allSegmentArray[3] = tempArray
                        break;
                        case 'business':
                            allSegmentArray[4] = tempArray
                        break;
                        case 'company_role':
                            allSegmentArray[5]= tempArray
                        break;
                        case 'level':
                            allSegmentArray[6] = tempArray
                        break;
                        case 'gender':
                            allSegmentArray[7]= tempArray
                        break;
                        case 'age':
                            allSegmentArray[8] = tempArray
                        break;
                        case 'start_date':
                            allSegmentArray[9] = tempArray
                        break;
                        case 'visibly_role':
                            allSegmentArray[10] = tempArray
                        break;
                    }
            }
        }

  apiRequest['segment'] = {}
  if(valueArray['edit_identity'] !== undefined && this.state.isCopied!==true){
    apiRequest['segment']['identity'] = valueArray['edit_identity']    
  }
  let j = 1
  allSegmentArray.map((allSegmentData,index)=>{
      if(allSegmentData.length > 0){
          let newValue = []
          if(allSegmentData[0].value instanceof Array){
              newValue = allSegmentData[0].value 
          }else{
              let newValueArray = []
              newValueArray.push(allSegmentData[0].value)   
              newValue = newValueArray 
          }
          if(allSegmentData[0].operation == 'IB'){
              newValue = []
              if(allSegmentData[0].key == 'start_date'){
                  newValue.push(allSegmentData[0].value[0].from, allSegmentData[0].value[0].to)
              }else{
                  newValue.push(allSegmentData[0].value.from, allSegmentData[0].value.to)
              }
          }
          apiRequest['segment'][allSegmentData[0].key] = [{value: newValue, selected: allSegmentData[0].selected, operation: allSegmentData[0].operation, sequence: j}]
          j++
      }
      
  })
  apiRequest.statusSelected = valueArray.selectedStatus
  apiRequest.name=valueArray.SegmentName
  Object.assign(apiRequest,
    {
      type:"campaign",
      action_id:this.props.campaign.identity
    })
  if(this.state.isCopied==true){
    this.props.campaignActions.AddSegmentData(apiRequest,this.state.currTab,'segment');
    this.setState({
      isCopied:false
    })
    this.props.handleCopyState();
  }else{
    this.props.campaignActions.AddSegmentData(apiRequest,this.state.currTab,'segment');
  }
}
}

selectedLoopCheck(selectedArray){
  selectedArray.forEach(function(checkData) {
      switch(checkData.selected){
          case 'business':
              if(checkData.value.length > 0){
                  let parentId = [];
                  let parentType = '';
                  checkData.value.map((checkVal)=>{
                      parentId.push(checkVal.value)
                      parentType='business'
                  })
                  resultLoop = {parentId: parentId, parentType: parentType}
                  return
              }
          break;
          case 'department':
              if(checkData.value.length > 0){
                  let parentId = [];
                  let parentType = '';
                  checkData.value.map((checkVal)=>{
                      parentId.push(checkVal.value)
                      parentType='department'
                  })
                  resultLoop = {parentId: parentId, parentType: parentType}
                  return
              }
          break;
          case 'office':
              if(checkData.value.length > 0){
                  let parentId = [];
                  let parentType = '';
                  checkData.value.map((checkVal)=>{
                      parentId.push(checkVal.value)
                      parentType='office'
                  })
                  resultLoop = {parentId: parentId, parentType: parentType}
                  return
              }
          break;
          case 'country':
              if(checkData.value.length > 0){
                  let parentId = [];
                  let parentType = '';
                  checkData.value.map((checkVal)=>{
                      parentId.push(checkVal.value)
                      parentType='country'
                  })
                  resultLoop = {parentId: parentId, parentType: parentType}
                  return
              }
          break;
          case 'region':
              if(checkData.value.length > 0){
                  let parentId = [];
                  let parentType = '';
                  checkData.value.map((checkVal)=>{
                      parentId.push(checkVal.value)
                      parentType='region'
                  })
                  resultLoop = {parentId: parentId, parentType: parentType}
                  return
              }
          break;
          default:
                 resultLoop = {}
      }
      
  });
}

showPerticularSegment(segmentId){
  var campId = this.props.campaign.identity;
  this.props.campaignActions.fetchSegmentById(segmentId,campId);
  // this.setState({
  //   openSegmentPopup:true
  // })

}

  newSegmentPreviewPopup(){
    return(
    <CenterPopupWrapper className="segmentPreviewPopup">
      <header className="heading">
        <h3>Preview segment</h3>
        <button class="btn-default close-popup" onClick={this.closeSegmentPreviewPopup.bind(this)}><i class="material-icons" >clear</i></button>
      </header>
      <div className="previewSegmentData">
        <div className="previewSegmentColumn">Name: Sales segment newsletter</div>
        <div className="previewSegmentColumn">User: Chris Heron</div>
        <div className="previewSegmentColumn">Users: 809</div>
        <div className="previewSegmentColumn">Date: 14/04/2019</div>
      </div>
      <div className="segmentedUserList">

<div class="tabel-view-userdata">
  <div class="widget">
  <table class="table responsive-table" id="setting-user-table">
  <thead>
    <tr class="reactable-column-header">
    <th class="reactable-th-userimg userimg  userimg" tabindex="0"><strong class="name-header">User Img</strong></th>
    <th class="reactable-th-username reactable-header-sortable  username  " role="button" tabindex="0"><strong class="name-header">User Name</strong><i class="fa fa-sort"></i></th>
    <th class="reactable-th-companyposition reactable-header-sortable  companyposition  " role="button" tabindex="0"><strong class="name-header">Company Position</strong><i class="fa fa-sort"></i></th>
    <th class="reactable-th-user reactable-header-sortable  user  " role="button" tabindex="0"><strong class="name-header">User</strong><i class="fa fa-sort"></i></th>
    <th class="reactable-th-email reactable-header-sortable  email  " role="button" tabindex="0"><strong class="name-header">Email</strong><i class="fa fa-sort"></i></th>
    <th class="reactable-th-department reactable-header-sortable  department  " role="button" tabindex="0"><strong class="name-header">Department</strong><i class="fa fa-sort"></i></th>
    <th class="reactable-th-status reactable-header-sortable  status   " role="button" tabindex="0"><strong class="name-header">Status</strong><i class="fa fa-sort"></i></th><th class="reactable-th-lastlog reactable-header-sortable  lastlog   " role="button" tabindex="0"><strong class="name-header">Last Log</strong><i class="fa fa-sort"></i></th>
    </tr>
    </thead>
    <tbody class="reactable-data">
    <tr class="table-body-text">
    <td class="userimg  userimg" data-rwd-label="User Img" label="[object Object],">
    <div>
      <img src="https://s3-us-west-2.amazonaws.com/talent-advocate/default-avatar.png" alt="https://s3-us-west-2.amazonaws.com/talent-advocate/default-avatar.png" class="list-avatar" data-tip="Yesha@o2h.com" data-for="pwEJy" currentitem="false" />
      <div class="__react_component_tooltip place-top type-dark " id="pwEJy" data-id="tooltip"></div>
    </div>
    </td>
    <td class="username  " data-rwd-label="User Name" label="[object Object],[object Object]">Yesha Joshi</td>
    <td class="companyposition  " data-rwd-label="Company Position" label="[object Object],[object Object]"></td>
    <td class="user  " data-rwd-label="User" label="[object Object],[object Object]">employee</td><td class="email  " data-rwd-label="Email" label="[object Object],[object Object]">Yesha@o2h.com</td><td class="department  " data-rwd-label="Department" label="[object Object],[object Object]">IT</td><td class="status 
      " data-rwd-label="Status" label="[object Object],[object Object]">Active</td>
      <td class="lastlog   " data-rwd-label="Last Log" label="[object Object],[object Object]">a month ago</td>
      </tr><tr class="table-body-text">
        <td class="userimg  userimg" data-rwd-label="User Img" label="[object Object],"><div><img src="https://s3-us-west-2.amazonaws.com/talent-advocate/default-avatar.png" alt="https://s3-us-west-2.amazonaws.com/talent-advocate/default-avatar.png" class="list-avatar" data-tip="dimple.dave@o2h.com" data-for="ymGkA" currentitem="false" /><div class="__react_component_tooltip place-top type-dark " id="ymGkA" data-id="tooltip"></div></div></td>
        <td class="username  " data-rwd-label="User Name" label="[object Object],[object Object]">Dimple  Dave</td>
        <td class="companyposition  " data-rwd-label="Company Position" label="[object Object],[object Object]"></td>
        <td class="user  " data-rwd-label="User" label="[object Object],[object Object]"><div>moderator<span class="trusted-user"> Trusted <i class="material-icons">verified_user</i></span></div></td>
        <td class="email  " data-rwd-label="Email" label="[object Object],[object Object]">dimple.dave@o2h.com</td><td class="department  " data-rwd-label="Department" label="[object Object],[object Object]">IT</td>
        <td class="status   " data-rwd-label="Status" label="[object Object],[object Object]">Active</td>
        <td class="lastlog   " data-rwd-label="Last Log" label="[object Object],[object Object]">a month ago</td>
        </tr><tr class="table-body-text">
          <td class="userimg  userimg" data-rwd-label="User Img" label="[object Object],"><div><img src="https://s3-us-west-2.amazonaws.com/talent-advocate/default-avatar.png" alt="https://s3-us-west-2.amazonaws.com/talent-advocate/default-avatar.png" class="list-avatar" data-tip="Dimple@o2h.com" data-for="pjoEA" currentitem="false" /><div class="__react_component_tooltip place-top type-dark " id="pjoEA" data-id="tooltip"></div></div></td>
          <td class="username  " data-rwd-label="User Name" label="[object Object],[object Object]">Dimple Test</td><td class="companyposition  " data-rwd-label="Company Position" label="[object Object],[object Object]"></td>
          <td class="user  " data-rwd-label="User" label="[object Object],[object Object]"><div>moderator<span class="trusted-user">Trusted <i class="material-icons">verified_user</i></span></div></td>
          <td class="email  " data-rwd-label="Email" label="[object Object],[object Object]">Dimple@o2h.com</td><td class="department  " data-rwd-label="Department" label="[object Object],[object Object]"></td>
          <td class="status   " data-rwd-label="Status" label="[object Object],[object Object]">Active</td><td class="lastlog   " data-rwd-label="Last Log" label="[object Object],[object Object]"></td>
          </tr><tr class="table-body-text">
            <td class="userimg  userimg" data-rwd-label="User Img" label="[object Object],"><div><img src="https://s3-us-west-2.amazonaws.com/talent-advocate/default-avatar.png" alt="https://s3-us-west-2.amazonaws.com/talent-advocate/default-avatar.png" class="list-avatar" data-tip="nitin@visibly.io" data-for="pPQdy" currentitem="false" /><div class="__react_component_tooltip place-top type-warning " id="pPQdy" data-id="tooltip">nitin@visibly.io</div></div></td>
            <td class="username  " data-rwd-label="User Name" label="[object Object],[object Object]">nitin kachhadiya</td>
            <td class="companyposition  " data-rwd-label="Company Position" label="[object Object],[object Object]"></td>
            <td class="user  " data-rwd-label="User" label="[object Object],[object Object]">guest</td>
            <td class="email  " data-rwd-label="Email" label="[object Object],[object Object]">nitin@visibly.io</td>
            <td class="department  " data-rwd-label="Department" label="[object Object],[object Object]"></td>
            <td class="status   " data-rwd-label="Status" label="[object Object],[object Object]">Active</td>
            <td class="lastlog   " data-rwd-label="Last Log" label="[object Object],[object Object]">a month ago</td></tr>
            <tr class="table-body-text"><td class="userimg  userimg" data-rwd-label="User Img" label="[object Object],"><div><img src="https://s3-us-west-2.amazonaws.com/talent-advocate/default-avatar.png" alt="https://s3-us-west-2.amazonaws.com/talent-advocate/default-avatar.png" class="list-avatar" data-tip="jacksparrow1996@gmail.com" data-for="Abjky" currentitem="false" /><div class="__react_component_tooltip place-top type-dark " id="Abjky" data-id="tooltip"></div></div></td>
            <td class="username  " data-rwd-label="User Name" label="[object Object],[object Object]"> </td>
            <td class="companyposition  " data-rwd-label="Company Position" label="[object Object],[object Object]"></td>
            <td class="user  " data-rwd-label="User" label="[object Object],[object Object]">guest</td><td class="email  " data-rwd-label="Email" label="[object Object],[object Object]">jacksparrow1996@gmail.com</td>
            <td class="department  " data-rwd-label="Department" label="[object Object],[object Object]"></td>
            <td class="status   " data-rwd-label="Status" label="[object Object],[object Object]">Active</td>
            <td class="lastlog   " data-rwd-label="Last Log" label="[object Object],[object Object]"></td></tr>
            <tr class="table-body-text"><td class="userimg  userimg" data-rwd-label="User Img" label="[object Object],"><div><img src="https://s3-us-west-2.amazonaws.com/talent-advocate/default-avatar.png" alt="https://s3-us-west-2.amazonaws.com/talent-advocate/default-avatar.png" class="list-avatar" data-tip="disha1@visibly.io" data-for="yMxwA" currentitem="false" /><div class="__react_component_tooltip place-top type-dark " id="yMxwA" data-id="tooltip"></div></div></td>
            <td class="username  " data-rwd-label="User Name" label="[object Object],[object Object]">disha thaker</td><td class="companyposition  " data-rwd-label="Company Position" label="[object Object],[object Object]"></td>
            <td class="user  " data-rwd-label="User" label="[object Object],[object Object]">employee</td>
            <td class="email  " data-rwd-label="Email" label="[object Object],[object Object]">disha1@visibly.io</td>
            <td class="department  " data-rwd-label="Department" label="[object Object],[object Object]"></td>
            <td class="status   " data-rwd-label="Status" label="[object Object],[object Object]">Active</td>
            <td class="lastlog   " data-rwd-label="Last Log" label="[object Object],[object Object]"></td></tr>
            <tr class="table-body-text"><td class="userimg  userimg" data-rwd-label="User Img" label="[object Object],"><div><img src="https://s3-us-west-2.amazonaws.com/talent-advocate/default-avatar.png" alt="https://s3-us-west-2.amazonaws.com/talent-advocate/default-avatar.png" class="list-avatar" data-tip="pradeep1@visibly.io" data-for="yEarA" currentitem="false" /><div class="__react_component_tooltip place-top type-dark " id="yEarA" data-id="tooltip"></div></div></td>
            <td class="username  " data-rwd-label="User Name" label="[object Object],[object Object]">pradeep hirani</td>
            <td class="companyposition  " data-rwd-label="Company Position" label="[object Object],[object Object]"></td>
            <td class="user  " data-rwd-label="User" label="[object Object],[object Object]">employee</td>
            <td class="email  " data-rwd-label="Email" label="[object Object],[object Object]">pradeep1@visibly.io</td>
            <td class="department  " data-rwd-label="Department" label="[object Object],[object Object]"></td>
            <td class="status   " data-rwd-label="Status" label="[object Object],[object Object]">Active</td>
            <td class="lastlog   " data-rwd-label="Last Log" label="[object Object],[object Object]"></td></tr>
            <tr class="table-body-text"><td class="userimg  userimg" data-rwd-label="User Img" label="[object Object],"><div><img src="https://s3-us-west-2.amazonaws.com/talent-advocate/default-avatar.png" alt="https://s3-us-west-2.amazonaws.com/talent-advocate/default-avatar.png" class="list-avatar" data-tip="jaya1@visibly.io" data-for="paPwy" currentitem="false" /><div class="__react_component_tooltip place-top type-dark " id="paPwy" data-id="tooltip"></div></div></td>
            <td class="username  " data-rwd-label="User Name" label="[object Object],[object Object]">jaya mishara</td>
            <td class="companyposition  " data-rwd-label="Company Position" label="[object Object],[object Object]"></td>
            <td class="user  " data-rwd-label="User" label="[object Object],[object Object]">employee</td>
            <td class="email  " data-rwd-label="Email" label="[object Object],[object Object]">jaya1@visibly.io</td>
            <td class="department  " data-rwd-label="Department" label="[object Object],[object Object]"></td>
            <td class="status   " data-rwd-label="Status" label="[object Object],[object Object]">Active</td>
            <td class="lastlog   " data-rwd-label="Last Log" label="[object Object],[object Object]"></td></tr>
            <tr class="table-body-text"><td class="userimg  userimg" data-rwd-label="User Img" label="[object Object],"><div><img src="https://s3-us-west-2.amazonaws.com/talent-advocate/default-avatar.png" alt="https://s3-us-west-2.amazonaws.com/talent-advocate/default-avatar.png" class="list-avatar" data-tip="priyanka1@visibly.io" data-for="yNzQp" currentitem="false" /><div class="__react_component_tooltip place-top type-dark " id="yNzQp" data-id="tooltip"></div></div></td>
            <td class="username  " data-rwd-label="User Name" label="[object Object],[object Object]">priyanka patel</td>
            <td class="companyposition  " data-rwd-label="Company Position" label="[object Object],[object Object]"></td>
            <td class="user  " data-rwd-label="User" label="[object Object],[object Object]">employee</td>
            <td class="email  " data-rwd-label="Email" label="[object Object],[object Object]">priyanka1@visibly.io</td>
            <td class="department  " data-rwd-label="Department" label="[object Object],[object Object]"></td>
            <td class="status   " data-rwd-label="Status" label="[object Object],[object Object]">Suspended</td>
            <td class="lastlog   " data-rwd-label="Last Log" label="[object Object],[object Object]"></td></tr>
            <tr class="table-body-text"><td class="userimg  userimg" data-rwd-label="User Img" label="[object Object],"><div><img src="https://s3-us-west-2.amazonaws.com/talent-advocate/default-avatar.png" alt="https://s3-us-west-2.amazonaws.com/talent-advocate/default-avatar.png" class="list-avatar" data-tip="tejal2@visibly.io" data-for="yJjRp" currentitem="false" /><div class="__react_component_tooltip place-top type-dark " id="yJjRp" data-id="tooltip"></div></div></td>
            <td class="username  " data-rwd-label="User Name" label="[object Object],[object Object]">tejal kukadia</td>
            <td class="companyposition  " data-rwd-label="Company Position" label="[object Object],[object Object]"></td>
            <td class="user  " data-rwd-label="User" label="[object Object],[object Object]">employee</td>
            <td class="email  " data-rwd-label="Email" label="[object Object],[object Object]">tejal2@visibly.io</td>
            <td class="department  " data-rwd-label="Department" label="[object Object],[object Object]"></td>
            <td class="status   " data-rwd-label="Status" label="[object Object],[object Object]">Suspended</td>
            <td class="lastlog   " data-rwd-label="Last Log" label="[object Object],[object Object]"></td></tr>
            </tbody>
            <tbody class="reactable-pagination">
              <tr><td colspan="9">
                <a class="reactable-page-button reactable-current-page" href="#page-1">1</a>
                <a class="reactable-page-button" href="#page-2">2</a>
                <a class="reactable-page-button" href="#page-3">3</a>
                <a class="reactable-page-button" href="#page-4">4</a>
                <a class="reactable-page-button" href="#page-5">5</a>
                <a class="reactable-next-page" href="#page-2">Next</a></td></tr>
            </tbody>
          </table>
      </div>
    </div>
  </div>
    </CenterPopupWrapper>
    )
  }
  handleInitialize (nextProps = null) {
    const initData = {
      
    }
  }
  formSubmit(values){
    this.props.handleSubmit(values)
  }
    createNewSegment(){

      this.setState({
        openSegmentPopup:true
      })
    }
    fetchSegments(tab){
      var campId=this.props.campaign.identity;
      if(this.state.currTab!==tab){
      this.props.campaignActions.fetchCampaignSegments(campId,tab);
      this.setState({
        currTab:tab
      })
      }
      this.toggleLeftSidebar();
    }
    // SearchRecentSegments(e){ 
    //   {
    //     if(e.target.value!=="" && this.state.filterRecentSegment.length>0)
    //       {
    //         var i=0;
    //         var search = '';
    //         search=e.target.value.toLowerCase();
    //         var tempobj=this.state.filterRecentSegment.filter(function(temp){
    //           if( temp.segment_name&&temp.segment_name.toLowerCase().includes(search) || i==search || temp.total_user==search){
    //             return true;
    //           }
    //         })
    //         this.setState({
    //           recentSegmentData : tempobj
    //         })
    //       }
    //       else{
    //         this.setState({
    //           recentSegmentData : this.state.filterRecentSegment
    //         })
    //       }
    //     }
    //   }

  search_text(e){ 
      {
          if(e.target.value!=="" && this.state.filter_state!==undefined)
          {
            var i=0;
            search=e.target.value.toLowerCase();
            var tempobj=this.state.filter_state.filter(function(temp){
              i=i+1;
              if( temp.segment_name&&temp.segment_name.toLowerCase().includes(search) || i==search || temp.total_user==search){
                return true;
              }
            })
            this.setState({main_state:tempobj})
          }
          else{
            this.setState({main_state:this.state.filter_state})
          }
        }
      }
      toggleLeftSidebar(){
        this.setState({
          leftSidebarShown:!this.state.leftSidebarShown
        })
      }
      deleteSegment(segment){
        var me = this;
        notie.confirm(
          `Are you sure you want to delete this Segment?`,
          'Yes',
          'No',
          function () {
            me.props.campaignActions.deleteSegment(segment,me.props.campaign.identity,me.state.currTab);
            me.props.handleDeleteState();
          },
          function () { 
            me.props.HandleDeleteButSelectSegments();
          }
          
        )
      }
      deleteSegmentUser(userID){
        var me = this;
        var userType = "segment"
        notie.confirm(
          `Are you sure you want to delete this user?`,
          'Yes',
          'No',
          function () {
            notie.alert("Please confirm");
            me.props.campaignActions.deleteSegmentUser(me.props.campaign.identity ,userID ,userType );
           },
          function () { 
          }
        )
      }
    addRecentSegmentsInCampaign(list){
          var type = "segment";
          var  tab = null
          var ObjToPass={};
          Object.keys(list.filters).filter(function (lf) {
            Object.assign(ObjToPass,{[lf]:[list.filters[lf]]})
        })
       
      var obj = {
        action_id : this.props.campaign.identity,
        name:list.segment_name,
        segment:ObjToPass,
        statusSelected:list.selectedStatus,
        type:"campaign"
       }
   
     this.setState({
          isCopied : true
      });
   
    this.props.campaignActions.AddSegmentData(obj,tab,type);
    }
     copySegment(segmentId){
          this.setState({
            isCopied : true
          });
          this.showPerticularSegment(segmentId);
      }
      render () {
        var leftSidebarClass='';
        var recentSegmentDate = '';
        var segmentsUserListData =[]
        segmentsUserListData=this.props.campaigns.segmentsUserList.segmentsUserListData;
        if(this.state.leftSidebarShown){
          leftSidebarClass='leftSidebarShown'
        }else{
          leftSidebarClass=''
        }
        var segmentList =[];
        var me = this;
          return (
            
          <div className="CampaignSegmentationWrapper">
          { this.props.allcountry.loadCountry || this.props.masterData.companyMasterDataLoader ||this.props.culture.heatmapLoader || this.props.users.rolesLoader  || this.props.settings.companySettingLoader ? <PreLoader className='Page'/> : ''}
            {this.state.newSegmentPreview ? this.newSegmentPreviewPopup() : ''}
            {/* {this.props.createSegmentPopup ? this.createNewSegment() : ''} */}
            {this.props.createSegmentPopup || this.state.recentCampaignFlag == true  ? 
                    <AddSegmentPopup 
                        closeAddSegmentPopup={this.props.closeCreateSegmentPopup.bind(this)}
                        callFetchFieldData={this.callFetchFieldData.bind(this)}
                        countryData={this.props.allcountry.regioncountry}
                        officeData={this.props.masterData.office}
                        departmentData={this.props.masterData.department}
                        businessData={this.props.masterData.business}
                        companyRoleData={this.props.masterData.role}
                        levelData={this.props.culture.levelData}
                        genderData={this.props.culture.genderData}
                        visiblyRoleData={this.props.users.roles}
                        regionData={this.props.settings.companyHierarchyData.regions}
                        onSubmit={this.submitForm.bind(this)}
                        segmentData={this.props.campaigns.segmentsData}
                        showPerticularSegment={this.showPerticularSegment.bind(this)}
                        particularSegment={this.state.selectedSegment}
                        optionMainConst = {optionMainConst}
                        showSegmentPreviewPopup={this.showSegmentPreviewPopup.bind(this)}
                        userTags ={this.props.userTags}
                        isCopied={this.state.isCopied}
                        currView ={this.props.currentView}
                     /> 
                : ''}
            
                <div>

           
            <div className={`CampaignSegmentationColumnWrapper clearfix ${leftSidebarClass}`}>
            {this.state.createSegmentation ? this.createSegmentation():''}
                <div className="campaignSegmetationLeft">
                <div className = "inner-campaign-Segmetation-Left">
                    {/* <div className = "search-recent-segment"> 
                       <input placeholder='Search existing...' type="text" onChange={this.SearchRecentSegments.bind(this)} /> 
                    </div> */}
                    <div className="campaignSegmentstitle"><span>Recent segment</span></div>
                    <div className="campaignSegmetationSidebarList">
                      <ul> 
                         {this.state.recentSegmentData && this.state.recentSegmentData.length>0 ? 
                        this.state.recentSegmentData.map((list,index) =>{
                          {recentSegmentDate = this.props.Moment!==null ? this.props.Moment.unix(parseInt(list.created_at)).format('DD-MM-YYYY'):''}
                           return(
                            <li key = {`recentSegment`+index}>
                            <span className = "recent-segments-title"> {list.segment_name} </span> 
                            <span className = "date"> Last used {recentSegmentDate} </span>
                            <span className = "addIcon" onClick={this.addRecentSegmentsInCampaign.bind(this,list)}> <i class="material-icons">add_circle_outline</i> </span>
                           </li> 
                            )})
                          :  <p className = "no-data-block recent-segments-no-post">  No segment available. </p>}

                      </ul>
                    </div>
                  </div>
                </div>
                <div className="campaignSegmetationRight">
                <div className="leftsidebarToggleBtnWrapper"><button onClick={this.toggleLeftSidebar.bind(this)} className="btn-theme">Recent segment</button></div>
                <div className  = "userListSegmentation"> 
                <div className = "header"> <span className = "title"> QUICK USER ADD </span> </div>
                  <div class="clearfix segment-list-wrapper">
                  {
                     segmentsUserListData !== undefined && segmentsUserListData.length>0 ?
                       segmentsUserListData.map((list , index) =>{
                        return(
                          <div className='user-popup-parent'>
                           <div class="heatMap segment-list"  key = {`quickUser`+index}>
                              <span class="segmentData user-popup-thumb" id = {list.identity}> {list.first_name}{list.last_name} <span class="segmentName">{list.department}IT</span></span>
                              <span class="segmentDataRemove" onClick={this.deleteSegmentUser.bind(this , list.identity)}><i class="material-icons">clear</i></span>
                              <FetchuserDetail userID={list.identity}/>
                          </div>
                        </div>
                        )
                      }) 
                       :  <p className = "no-data-block"> No user available. </p>
                  }
                 </div>
                 </div>

                {this.props.campaigns.segmentloading || this.props.campaigns.addSegmentLoading ?

                  <div className=" loader-culture-tableExpand">
                  <div>
                      <div className="table-anytlics-body">
                          <table className="table responsive-table">
                              <thead>
                                  <tr className="reactable-column-header">
                                      <th className="reactable-th-serialno reactable-header-sortable serialno  a-center">
                                          <div className='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                                      </th>
                                      <th className="reactable-th-serialno reactable-header-sortable serialno  a-center">
                                          <div className='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                                      </th>
                                      <th className="reactable-th-serialno reactable-header-sortable serialno  a-center">
                                          <div className='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                                      </th>
                                      <th className="reactable-th-serialno reactable-header-sortable serialno  a-center">
                                          <div className='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                                      </th>
                                      <th className="reactable-th-serialno reactable-header-sortable serialno  a-center">
                                          <div className='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                                      </th>

                                  </tr>
                              </thead>
                              <tbody className="reactable-data">
                                  <tr className="table-body-text">
                                      <td>
                                          <div className='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                                      </td>
                                      <td>
                                          <div className='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                                      </td>
                                      <td>
                                          <div className='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                                      </td>
                                      <td>
                                          <div className='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                                      </td>
                                      <td>
                                          <div className='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                                      </td>
                                  </tr>
                                  <tr className="table-body-text">
                                      <td>
                                          <div className='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                                      </td>
                                      <td>
                                          <div className='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                                      </td>
                                      <td>
                                          <div className='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                                      </td>
                                      <td>
                                          <div className='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                                      </td>
                                      <td>
                                          <div className='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                                      </td>

                                  </tr>

                              </tbody>
                          </table>
                      </div>
                  </div>
                  </div> 
               : 

                  <div className="campaignSegmetationRightInner">
                        <div className = "header"> 
                        <span className = "title"> SEGMENTS </span> 
                        {this.props.location.pathname !== "/campaigns/expired" && this.props.CheckedSegments.length > 0  && (this.props.currentView == 'segmentations' || this.props.currentView == 'collaborators')  ?
                        <div class="action-button">
                          <ul class="dd-menu context-menu user_3dots">
                              <li class="button-dropdown"><a class="dropdown-toggle user_drop_toggle active">
                                 <i class="material-icons">more_vert</i></a>
                                 <ul class="dropdown-menu user_dropAction">
                                 <li>
                                      {typeof this.props.campaign !==
                                        'undefined'
                                          ? <a
                                          className=''
                                          onClick={this.deleteSegment.bind(this)}
                                        >
                                          Delete
                                            </a>
                                            :null}
                                  </li>
                             {(this.props.CheckedSegments.length == 1  &&  this.props.CheckedSegments[0] !== 'selectAllSegments') && this.props.location.pathname !== "/campaigns/expired" && (this.props.currentView == 'segmentations' || this.props.currentView == 'collaborators') ?
                                    <li>
                                    {typeof this.props.campaign !==
                                      'undefined'
                                        ? <a
                                        className=''
                                        onClick={this.copySegment1.bind(this)}
                                      >
                                        Copy
                                          </a>
                                          :null}
                                    </li>
                                    
                                  :''}
                                </ul>
                              </li>
                          </ul>
                      </div>
                 :''}
                </div>
                      <div className="campaignSegmentationTableWrapper">

                      <div className='tableControllers clearfix'>
                          <div className='tableFilterDropdown'>
                            {/*this.createDropdown()*/}
                          </div>
                            {this.props.location.pathname !== "/search/campaign"?
                              <TableSearchWrapper>
                              <input placeholder='Search in segments' type="text" onChange={this.search_text.bind(this)} />
                              </TableSearchWrapper>:''
                            }
                      </div>
                      {this.state.main_state.length>0?
                            <table className="table responsive-table">
                                <thead>
                                    <tr>
                                        {this.props.roleName=='super-admin' || this.props.roleName=='admin'?
                                          <th className="checkbox-column">
                                            <div className="checkbox form-row selectAllCheckbox"><input type="checkbox" id="selectAllSegments" onChange={this.props.handleCheckbox.bind(this,segmentList)}/><label for="selectAllSegments"></label></div>
                                          </th>:''}
                                        <th className="userThumColumn"></th>
                                        <th>Name</th>
                                        <th>User</th>
                                        <th>Date</th>
                                        <th>Users</th>
                                        {this.props.roleName=='super-admin' || this.props.roleName=='admin'? <th>Action</th>:''}
                                    </tr>
                                </thead>
                                <tbody>
                                 {this.state.main_state!==undefined && Object.keys(this.state.main_state).length>0?
                                    this.state.main_state.map((segment,index)=>{
                                      segmentList.push(segment.id);
                                      var segmentDate = this.props.Moment!==null ?this.props.Moment.unix(parseInt(segment.created_at)).format('DD-MM-YYYY'):'';
                                    return(
                                    <tr key={`segment`+index}>
                                        {this.props.roleName=='super-admin' || this.props.roleName=='admin'?<td className="checkbox-column"><div className="checkbox form-row "><input type="checkbox" id={segment.id} onChange={this.props.handleCheckbox.bind(this,segment.id)}/><label for={segment.id}></label></div></td>:''}
                                        <td className="userThumColumn"><span className="userThumb"><img src="/img/user.jpg" /></span></td>
                                        <td data-rwd-label="Name">{segment.segment_name}</td>
                                        <td data-rwd-label="User">{segment.user_name}</td>
                                        <td data-rwd-label="Date">{segmentDate}</td>
                                        <td data-rwd-label="Users">{segment.user_count?segment.user_count:0}</td>
                                        {this.props.roleName=='super-admin' || this.props.roleName=='admin'?
                                        <td className="action-td"><div className='action-button'>
                                  <ul
                                    className='dd-menu context-menu user_3dots'
                                  >
                                    <li className='button-dropdown'>
                                      <a className='dropdown-toggle user_drop_toggle'>
                                        <i className='material-icons'>more_vert</i>
                                      </a>

                                      <ul className='dropdown-menu user_dropAction'>
                                      {/* if user is admin then he should not be suspend or edit super admin */}
                                      <li onClick={this.copySegment.bind(this,segment.id)}>
                                          <span  className='btn'>
                                            Copy
                                          </span>
                                        </li> 
                                        <li onClick={this.showPerticularSegment.bind(this,segment.id)}>
                                          <span  className='btn'>
                                            Edit
                                          </span>
                                        </li> 
                                        <li onClick={this.deleteSegment.bind(this,[segment.id])}>
                                          <span  className='btn'>
                                            Delete
                                          </span>
                                        </li> 
                                       
                                      </ul>
                                    </li>
                                  </ul>
                                </div>
                                </td>:''}
                                    </tr>)
                                  }):''}
                                </tbody>
                            </table>:
                            <div className="no-data-block">
                            No segments found.
                        </div>}
                        </div>
                        
                
                
                </div>
               }
                </div>
                </div>

                
          </div>
          </div>
          )
      }
}
function mapStateToProps(state){
  return{
    campaigns: state.campaigns,
    allcountry:state.allcountry,
    masterData: state.settings.companyMasterData,
    settings:state.settings,
    culture: state.cultureAnalytics,
    userTags:state.tags.userTags,
    users: state.users
  }
}
function mapDispatchToProps(dispatch){
  return{
    campaignActions:bindActionCreators(campaignActions,dispatch),
    companyInfoAction:bindActionCreators(companyInfo,dispatch),
    countryActions:bindActionCreators(countryActions,dispatch),
    cultureAnalyticsActions: bindActionCreators(cultureAnalyticsActions, dispatch),
    tagsActions : bindActionCreators(tagsActions,dispatch),
    userActions: bindActionCreators(userActions, dispatch)
  }
}
var connection = connect(mapStateToProps,mapDispatchToProps);
// module.exports = connection(CampaignSegmentation);

var reduxConnectedComponent = connection(CampaignSegmentation)

export default reduxForm({
  form: 'QuickSegments' // a unique identifier for this form
})(reduxConnectedComponent)