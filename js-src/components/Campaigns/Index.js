import { browserHistory } from 'react-router'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Link } from 'react-router'
import reactable from '../Chunks/ChunkReactable'
import * as campaignsActions from '../../actions/campaigns/campaignsActions'
import * as channelActions from '../../actions/socialChannelActions'
import * as utils from '../../utils/utils'
import * as postsActions from '../../actions/feed/postsActions'
import * as feedActions from '../../actions/feed/feedActions';

import $ from '../Chunks/ChunkJquery'
import CreateCampaign from './CreateCampaign'
import CampaignList from './CampaignList'
import CampaignPosts from '../Calendar/CampaignPosts'

import Collapse, { Panel } from 'rc-collapse' // collapse
import ChunkJqueryDropdown from '../Chunks/ChunkJqueryDropdown'
import CampaignNav from './CampaignNav'
import CampaignCalendar from './Calendar'
import CampaignViewTabs from './CampaignViewTabs'
import Globals from '../../Globals'
import Header from '../Header/Header'
import moment from '../Chunks/ChunkMoment'
import notie from 'notie/dist/notie.js'
import PreLoader from '../PreLoader'
import ReactDatePicker from '../Helpers/ReduxForm/ReactDatePicker' // react datepicker field
import reactDatepickerChunk from '../Chunks/ChunkDatePicker' // datepicker chunk
import SearchNav from '../Search/SearchNav';
import SliderPopup from '../SliderPopup'
import SidebarNarrow from '../Layout/Sidebar/SidebarNarrow'
import UpdateCampaign from './UpdateCampaign'
import GuestUserRestrictionPopup from '../GuestUserRestrictionPopup'
import StaticDataForGuest from '../StaticDataForGuest';
import ContentProject from './ContentProject';
import Script from "react-load-script";
import CampaignCreateTasks from './CampaignCreateTasks'
import CampaignEventDetail from './CampaignEventDetail'
// collapse css
var editdata = false
var campaignrestrict=false;
var restrictAPI=false;
var flag = 0
let NavBar
let title
var currentTabView = 'list'
class Campaigns extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      Moment: null,
      newCampaignPopup: false,
      updateCampaignPopup: false,
      detailCampaignPopup: false,
      campaignList: [],
      currentCampaign: [],
      currentFilter: 'all',
      mounted: true,
      isTourActive: true,
      showPostCreationButton: false,
      tourStep: 1,
      terminateStatus: 1,
      editdata: false,
      selectCampaign: null,
      show_menu: true,
      view: 'list',
      campaignPostPopup : false,
      disableTemplateSelection:true,
      scriptError: false,
      scriptLoaded: false,
      pageno: '1',
      eventPageno: '1',
      displayDropDown:false,
      selectedCampaignType:['All','Internal','Talent','Customer'],
      openAddEventPopup :false,
      typeSelected:'campaign',
      openEventView:false,
      currEvent:{},
      reactable:null,
    }
    this.handleCampaignTypeFilter = this.handleCampaignTypeFilter.bind(this)
  }
  /**
   * open when click on create post of campaign
   * @param {*} campaignId
   * @param {*} campaignTitle
   */
  onPostCreationClick(campaignId, campaignTitle, campaignType) {
    let selectedSocialChannels = this.state.currentCampaign.SocialChannel
    var objToSend = {}
    var feedtype = this.props.feed.feedList?this.props.feed.feedList[0]?this.props.feed.feedList[0].type:'':'';
    var CreatePostFlag=false;
    objToSend = {
      campaign_id: campaignId,
      campaign_title: campaignTitle,
      campaign_type:campaignType,
      SocialChannel: selectedSocialChannels,
      campaign_end_date:this.state.currentCampaign.end_date,
      campaign_start_date:this.state.currentCampaign.start_date
    }
    if(feedtype=="internal"){
      selectedSocialChannels.data.map((data,i)=>{
        if(data.media!=="Internal"){
          CreatePostFlag = true;
          notie.alert("error","you cannot create external post because default feed is internal",3);
        }
      })
    }
    else if(feedtype=="external"){
      selectedSocialChannels.data.map((data,i)=>{
        if(data.media=="Internal"){
          CreatePostFlag = true;
          notie.alert("error","you cannot create internal post because default feed is external",3);
        }
      })
    }
    if(CreatePostFlag!==true){
      this.props.postsActions.campaignCreatePostData(objToSend)
    }
  }

  changeCreatePostStatus(){
    this.setState({
      campaignPostPopup:false
    })
  }
  changeCreateAssetStatus(){
    this.setState({
      campaignAssetPopup:false
    })
  }

  componentWillMount() {
    var me = this
    moment().then(moment => {
      reactable().then(reactable=>{
      me.setState({
        Moment: moment,
        reactable:reactable
      })
    });
      reactDatepickerChunk()
        .then(reactDatepicker => {
          if (me.state.mounted) {
            me.setState({
              ReactDatePicker: reactDatepicker,
              showPostCreationButton: true // show post creation button only after everything is loaded
            })
          }
        })
        .catch(err => {
          throw err
        })
    })
    $().then(jquery => {
      var $ = jquery.$
      me.setState({
        jQuery: $
      })
      ChunkJqueryDropdown().then(jqueryDropdown => { })
    })

    /** For open the update model */
    if (typeof this.props.location.query.campaignid !== 'undefined') {
      this.setState({ selectCampaign: this.props.location.query.campaignid })
    }
  }

  handleCampaignCreation() {
    this.setState({
      newCampaignPopup: true
    })

    this.props.campaignsActions.creatingNewCampaign()
    document.body.classList.add('overlay')
  }

  componentDidMount() {
    document.body.classList.remove('campaignDetailPopupShowed');
    var roles = JSON.parse(localStorage.getItem('roles'))
    if(campaignrestrict !==true){
      this.refetchBasedOnLocation()
      let selectedChannels = typeof this.props.campaign === 'undefined'
        ? null
        : this.props.campaign.SocialChannel
        
        if(roles!==undefined &&roles[0].role_name!=="guest"){
          this.props.channelActions.fetchChannels(selectedChannels)
          this.props.feedActions.getFeedList();
        } 
      campaignrestrict=true;
      setTimeout(() => {
        campaignrestrict=false;
      }, 5000);
    }
    if (typeof this.props.location.query.asset !== 'undefined' || typeof this.props.location.query.campaignid !== 'undefined' || typeof this.props.location.query.searchcreatepost !== 'undefined') {
      //if asset or campaignid or search campaignid is in url then to redirect it to campaign url only
      if(document.getElementsByClassName('overlayForFeedPopup') !== undefined){
        document.body.classList.remove('overlayForFeedPopup');
      }
      browserHistory.push('/campaigns')
    }
  }

  refetchBasedOnLocation() {
    var roles = JSON.parse(localStorage.getItem('roles'))
    var newLocation = utils
      .removeTrailingSlash(this.props.location.pathname)
      .split('/')
    var location = newLocation[2] == undefined ? 'live' : newLocation[2]
    if (location == 'expired') location = 'expired' // parameter to be passed is expire,changed it for that
    this.setHeader(location)
    if(newLocation[1] !== "search"){
      if(roles!==undefined && roles[0].role_name!=="guest"){
        if(currentTabView=="calendar"){
          this.props.campaignsActions.fetchCampaigns(location,true,this.state.Moment)
        }else{
        this.props.campaignsActions.fetchCampaigns(location,false,this.state.Moment,false,null,this.state.pageno)
        }
      }   
    }
  }
  
  setHeader(location) {
    this.setState({
      currentFilter: location
    })
  }
  componentWillReceiveProps(newProps) {
    var newLocation = utils.removeTrailingSlash(newProps.location.pathname).split('/')
    if(newProps.campaigns.updated_Campaign!==this.props.campaigns.updated_Campaign && newProps.campaigns.updated_Campaign!=={}){
      if(newProps.campaigns.openCamapignFromNoti==true){
        this.campaignClick(newProps.campaigns.updated_Campaign);
      }
      var location = newLocation[2] == undefined ? 'live' : newLocation[2]
      if (location == 'expired') location = 'expired';

      
      this.setState({
        currentCampaign:newProps.campaigns.updated_Campaign,
        pageno:1
      },()=>this.props.campaignsActions.fetchCampaigns(location,false,this.state.Moment,false,null,this.state.pageno))
    }
    if (newProps.location.pathname !== this.props.location.pathname) {
      this.setState({
        displayDropDown:false,
        selectedCampaignType:['All','Internal','Talent','Customer']
      })
      var location = newLocation[2] == undefined ? 'live' : newLocation[2]
      if (location == 'expired') location = 'expired' // parameter to be passed is expire,changed it for that\
      this.setHeader(location)
      if(newProps.profile.profile.role!==undefined && newProps.profile.profile.role.data[0].role_name!=="guest"){
        this.setState({
          pageno:1
        },()=>this.props.campaignsActions.fetchCampaigns(location,false,this.state.Moment,false,null,this.state.pageno))
      } 
    }
    if (newProps.campaigns.creation.created !==
      this.props.campaigns.creation.created &&
      newProps.campaigns.creation.created == true) {
      //if campaign is updated then popup will close
      this.closePopup('update')
      }
    if (newProps.campaigns.createCampaignFlag == true) {
      //createCampaignFlag will true when fetchcampaign method is call after campaign creation
      if (newProps.campaigns.campaigns !== this.props.campaigns.campaigns) {
        var CampaignList = newProps.campaigns.campaigns
        var obj = ''
        if (newProps.campaigns.createdCampaignInfo.length > 0 || newProps.campaigns.createdCampaignInfo) {
          //to pass current ceated campaign id as current campaign
          obj = CampaignList.find(function (obj) {
            return obj.identity == newProps.campaigns.createdCampaignInfo.data
          })
          if(obj!==undefined){
            this.setState({
              newCampaignPopup: false,
              detailCampaignPopup: true,
              currentCampaign: obj
            })
            // document.body.classList.add('overlay')
          } 
        }
      }
    }
    
    if (Object.keys(newProps.posts.campaignCreatePostdata).length > 0) {
      this.setState({
        campaignPostPopup : true
      })
      //to redirect to create post popup page when clicked on create post
      // if(!this.props.location.pathname.includes("search"))
      // {
      //     let url = `/feed/default/live?createpost=true`
      //     x=false
      //     browserHistory.push(url)
      // }
      // else
      // {
      //   let url='/feed/default/live?searchcreatepost=true'
      //   x=true
      //   browserHistory.push(url)
      // }
  }
  if(this.props.campaigns.taskSaved !==  newProps.campaigns.taskSaved &&  newProps.campaigns.taskSaved == true){
    this.props.campaignsActions.addTaskSuccess(false)
    this.setState({
      typeSelected :'event'
    })
    this.closeCreateEventPopup('create');
  }
  if(this.props.campaigns.deleteTaskLoading!== newProps.campaigns.deleteTaskLoading){
    if(newProps.campaigns.deleteTaskLoading!==true){
      this.closeEventDetailPopup();
    }
  }
    this.campTerminate(this)
  }

  campTerminate(e) {
    if (this.state.terminateStatus == 0) {
      this.setState({
        terminateStatus: 1
      })
      this.closePopup('detail')
    }
  }
  terminateCampaign(terminateFlag) {
    var me = this

    if (typeof this.state.currentCampaign.identity !== 'undefined') {
      var payload = {
        campaign_identity: this.state.currentCampaign.identity
      }

      var nextTerminateState = terminateFlag == 0 ? 1 : 0
      fetch(
        Globals.API_ROOT_URL +
        `/campaigns/terminate`,
        {
          method: 'POST',
          headers: utils.setApiHeaders('post'),
          body: JSON.stringify(payload)
        }
      )
        .then(response => {
          return response.json()
        })
        .then(json => {
          if (parseInt(json.code) == 200) {
            notie.alert('success', json.message, 3)
            me.setState({
              terminateStatus: nextTerminateState
            })
            me.refetchBasedOnLocation()
          } else {
            utils.handleSessionError(json)
            notie.alert(
              'error',
              `There was a problem updating the campaign. Error code: ${json.message}`,
              5
            )
          }
        })
        .catch(err => {
          notie.alert('error', 'There was a problem updating the campaign.', 5)
          throw err
        })
    }
  }
  onAssetCreationClick(catID, campID) {
    this.setState({
      campaignAssetPopup:true
    })
  }
  campaignClick(campaign) {
    // reset all flags which were set while creating campaigns
    document.body.classList.add('campaignDetailPopupShowed');
    this.props.campaignsActions.viewingCampaign()
    this.setState({
      detailCampaignPopup: true,
      currentCampaign: campaign
    })
  }

  campaignUpdateClick(campaign) {
    this.setState({
      updateCampaignPopup: true,
      currentCampaign: campaign
    })
    this.props.campaignsActions.creatingNewCampaign()

    document.body.classList.add('overlay')
  }
  campaignPostsClick(campaign) {
    this.setState({
      currentCampaign: campaign,
      CampaignPostsPopup: true
    })
    document.body.classList.add('overlay')
  }

  closePopup(popup) {
    document.body.classList.remove('campaignDetailPopupShowed');
    if (typeof this.props.location.query.asset !== 'undefined' || typeof this.props.location.query.campaignid !== 'undefined') {
      browserHistory.push('/campaigns')
    }
    switch (popup) {
      case 'new':
        editdata = false
        this.setState({
          newCampaignPopup: false,
          editdata: false
        })
        break
      case 'update':
        editdata = false
        this.setState({
          updateCampaignPopup: false,
          editdata: false
        })
        break
      case 'detail':
        editdata = false
        this.setState({
          detailCampaignPopup: false,
          editdata: false
        })
        break
      case 'posts':
        editdata = false
        this.setState({
          CampaignPostsPopup: false,
          editdata: false
        })
        break
    }
    if (popup == "new" || popup == "detail" || popup == "posts" || (popup == "update")) {
      document.body.classList.remove('overlay')
    }

  }
  /**
   * @author disha 
   * to call camapign list api and save array state to manage selected camapign type 
   * as child component is calling in every render and child compoents' state will be lost
   * so to manage selection of checkboxes
   * @param {*} type 
   */
  handleCampaignTypeFilter(type){
    if(this.state.selectedCampaignType.indexOf(type) == -1){
      this.state.selectedCampaignType.push(type)
    }else{
      var index = this.state.selectedCampaignType.indexOf(type);
        this.state.selectedCampaignType.splice(index, 1);
    }
    const totalTypes= this.state.selectedCampaignType
    var newLocation = utils
    .removeTrailingSlash(this.props.location.pathname)
    .split('/')
    var location = newLocation[2] == undefined ? 'live' : newLocation[2]
    this.props.campaignsActions.fetchCampaigns(location,false,this.state.Moment,false,null,this.state.pageno,totalTypes)
  }
  handleFilterTypeDropDown(flag){
    this.setState({
      displayDropDown :flag
    })
  }
  handleAddingCampEvent(e){
    document.body.classList.add('overlay');
    this.setState({
      openAddEventPopup:true
    })
  }
  fetchDataTypeWise(type){
    this.setState({
      typeSelected :type
    })
    if(type == 'campaign'){
      var newLocation = utils
      .removeTrailingSlash(this.props.location.pathname)
      .split('/')
      var location = newLocation[2] == undefined ? 'live' : newLocation[2]
      this.props.campaignsActions.fetchCampaigns(location,false,this.state.Moment,false,null,this.state.pageno,this.state.selectedCampaignType)
    }else if(type == 'event'){
      this.props.campaignsActions.fetchEventList(1,10)
    }
  }
  campaignListTable() {
    var me = this
    var data = {}
    var dataArray = []
    var tags = null
    var  campaignList = [];
    var eventList = [];
    let noCampaigns  = false;
    noCampaigns = this.props.campaigns.noCampaigns;

    if(me.props.location.state==null || me.props.location.state.seeAllClick==true){
      campaignList = typeof me.props.campaigns.campaigns !== undefined
      ? me.props.campaigns.campaigns
      : []
    }else{
      if(me.props.location.state.fromSuggestionClick==true){
        campaignList = typeof this.props.campaignData !== undefined
        ? this.props.campaignData
        : []
      }
    }
    eventList = me.props.campaigns.typeEvent.typeEventData !== undefined ? me.props.campaigns.typeEvent.typeEventData :[]
    let campaignMetaData=me.props.campaigns.campaignMeta !== undefined ? me.props.campaigns.campaignMeta:''
    let eventMetaData=me.props.campaigns.typeEvent.typeEventMetaData !== undefined ? me.props.campaigns.typeEvent.typeEventMetaData:{}
    let roleName =JSON.parse(localStorage.getItem('roles')) !== null? JSON.parse(localStorage.getItem('roles'))[0].role_name:''
    let notShow = 0
    if (roleName === 'employee' || roleName === 'moderator')
    {
      notShow = 1
    }
    if (campaignList.length == 0 || noCampaigns) {
      noCampaigns  = false;
      return (
        <div>
          {this.no_post_method()}
        </div>
      )
    }
      if (typeof this.state.Moment !== 'undefined' && this.state.Moment != null){
         return (
            <div className="CampaignList">
              {this.hide_add_new_popup()}
              <CampaignList
                campaign_data={campaignList}
                campaignMetaData={campaignMetaData}
                eventMetaData = {eventMetaData}
                eventList ={eventList}
                reactable = {this.state.reactable }
                openEventViewPopup ={this.openViewPopup.bind(this)}
                addEvent={this.handleAddingCampEvent.bind(this)}
                fetchDataTypeWise ={this.fetchDataTypeWise.bind(this)}
                typeSelected={this.state.typeSelected}
                handleFilterTypeDropDown={this.handleFilterTypeDropDown.bind(this)}
                openTypeFilterDropDown={this.state.displayDropDown}
                campaignTypeFilter ={this.handleCampaignTypeFilter}
                selectedCampType={this.state.selectedCampaignType}
                Moment={this.state.Moment}
                campaignUpdateClick={this.campaignUpdateClick.bind(this)}
                campaignClick={this.campaignClick.bind(this)}
                campaignPostsClick={this.campaignPostsClick.bind(this)}
                selectCampaign={this.state.selectCampaign}
                location={this.props.location}
                searchInput={this.props.searchInput}
                pageNo={this.selectedPageno.bind(this)}
                eventPageNo={this.selectedEventPageno.bind(this)}
              />
            </div>
          )
        // }
      }
  }
  selectedPageno (pageno) {
    var newLocation = utils
      .removeTrailingSlash(this.props.location.pathname)
      .split('/')
    var location = newLocation[2] == undefined ? 'live' : newLocation[2]
      if (location == 'expired') location = 'expired';
    this.setState(
      {
        pageno: pageno
      },
      () =>{this.props.campaignsActions.fetchCampaigns(location,false,this.state.Moment,false,null,this.state.pageno)}
    )
  }
  selectedEventPageno (pageno) {
    var newLocation = utils
      .removeTrailingSlash(this.props.location.pathname)
      .split('/')
    var location = newLocation[2] == undefined ? 'live' : newLocation[2]
      if (location == 'expired') location = 'expired';
    this.setState(
      {
        eventPageno: pageno
      },
      () =>{this.props.campaignsActions.fetchEventList(pageno,10)}
    )
  }
  no_post_method() {
    var add_new_popup = document.getElementById('add-new-popup')
    if (add_new_popup) {
      add_new_popup.className = 'add-new-popup show'
    }
  }
  hide_add_new_popup() {
    var add_new_popup = document.getElementById('add-new-popup')
    if (add_new_popup) {
      add_new_popup.className = 'add-new-popup'
    }
  }
  renderCampaignPostsPopup() {
    return (
      <SliderPopup wide className='campaignPostPopup'>
        <button
          id='close-popup'
          className='btn-default'
          onClick={this.closePopup.bind(this, 'posts')}
        >
          <i className='material-icons'>clear</i>
        </button>
        <CampaignPosts
          campaign={this.state.currentCampaign}
          Moment={this.state.Moment}
          searchInput={this.props.searchInput}
        />

      </SliderPopup>
    )
  }
  renderCampaignCreation() {
    return (
      <SliderPopup className='wide campaignPopup' fadePopup = 'fadepopup'  sidePopupCenter = "side-popup-Center"  updateCampaignPopup = "update-side-popup-Center">
        <button
          id='close-popup'
          className='btn-default'
          onClick={this.closePopup.bind(this, 'new')}
        >
          <i class='material-icons'>clear</i>
        </button>
        <CreateCampaign
          refetchList={this.refetchBasedOnLocation.bind(this)}
          location={this.props.location}
          Moment={this.state.Moment}
          campaign={this.state.currentCampaign}
          editCampaignSuccessfully={this.editCampaign.bind(this)}
          searchedInput={this.props.searchedInput}
        />

      </SliderPopup>
    )
  }
  editCampaignSuccessfully() {
    editdata = false
  }
  editCampaign(e) {
    // e.target.classList.remove('active');

    document.getElementsByClassName('dropdown-menu')[0].style.display = 'none'
    document.body.classList.add("overlay");
    this.props.campaignsActions.viewingCampaign()
    editdata = !editdata
  }
  renderCampaignUpdate(campaign) {
    return (
      <SliderPopup className='wide campaignPopup' fadePopup = 'fadepopup'  sidePopupCenter = "side-popup-Center"  updateCampaignPopup = "update-side-popup-Center">

        <button
          id='close-popup'
          className='btn-default'
          onClick={this.closePopup.bind(this, 'update')}
        >
          <i class='material-icons'>clear</i>
        </button>
        <UpdateCampaign
          refetchList={this.refetchBasedOnLocation.bind(this)}
          location={this.props.location}
          campaign={campaign}
          Moment={this.state.Moment}
          searchedInput={this.props.searchedInput}
        />

      </SliderPopup>
    )
  }
  handleChangeRequest(id,e){
    this.setState({
      changeRequest:true,
      currProjectId:id
    })
    document.body.classList.add('change-request-task-popup');
  }
  handleChangeStatus(type){
    if(type=="close"){
      this.setState({
        changeRequest:false,
        currProjectId:null
      })
    }
  }
  renderCampaignDetails() {
    let roleName =JSON.parse(localStorage.getItem('roles')) !== null? JSON.parse(localStorage.getItem('roles'))[0].role_name:''
    var addNewPostAllow=false
    var newLocation = utils
      .removeTrailingSlash(this.props.location.pathname)
      .split('/')
      //if page is connected then and then add new post button will appear
      this.state.currentCampaign!== undefined?
      this.state.currentCampaign.SocialChannel !==undefined && this.state.currentCampaign.SocialChannel !==null?
        this.state.currentCampaign.SocialChannel.data.length==1?
          this.state.currentCampaign.SocialChannel.data[0].media.toLowerCase()=="facebook"?
            this.props.socialAccounts.accounts.map((acc,i)=>{
              acc.social_media.toLowerCase()=='facebook' && acc.iscompany!==1 ?
              addNewPostAllow=false:''
            })
            :addNewPostAllow=true
          :addNewPostAllow=true
        :'' :''
        let currContentProject = this.props.campaigns.currContentProject;
        var contentProjectType = ''; 
        if(this.props.campaigns.currContentProject != null){
            contentProjectType =  this.props.campaigns.currContentProject.type  
        }
    return (
      <div className="campaignDetailPage">
        {this.state.showContentPage? 
        <ContentProject 
          closeContentProject={this.closeContentProject.bind(this)} 
          contentProjectData={currContentProject} 
          moment = {this.state.Moment}
          campaign={this.state.currentCampaign}
          roleName = {roleName}
          disableTemplateSelection ={this.state.disableTemplateSelection}
          handleChangeRequest={this.handleChangeRequest.bind(this)}
          campaignAllUserList={this.props.campaigns.campaignAllUserList}
          campaignType = {contentProjectType}
        />
        :null}
        <div className = "campaginContainerWraper">
          <div className="campaignListLink">
            <a onClick={this.closePopup.bind(this, 'detail')}>
              <i class="material-icons">chevron_left</i> back to list
            </a>
          </div>
          {typeof this.state.currentCampaign !==
                          'undefined'?
            <CampaignViewTabs
              terminateStatus = {this.state.terminateStatus}
              newLocation = {newLocation[2]}
              roleName = {roleName}
              addNewPostAllow = {addNewPostAllow}
              onPostCreationClick = {this.onPostCreationClick.bind(this)}
              onAssetCreationClick = {this.onAssetCreationClick.bind(this)}
              terminateCampaign = {this.terminateCampaign.bind(this)}
              editCampaign = {this.editCampaign.bind(this)}
              showPostCreationButton = {this.state.showPostCreationButton}
              refetchList={this.refetchBasedOnLocation.bind(this)}
              campaign={this.state.currentCampaign}
              Moment={this.state.Moment}
              onEditClick={this.campaignUpdateClick.bind(this)}
              location={this.props.location}
              onClose={this.closePopup.bind(this, 'detail')}
              editdata={editdata}
              channels={this.props.channels}
              editCampaignSuccessfully={this.editCampaignSuccessfully.bind(this)}
              campaignPostPopup ={this.state.campaignPostPopup}
              changeCreatePostStatus={this.changeCreatePostStatus.bind(this)}
              campaignAssetPopup={this.state.campaignAssetPopup}
              changeCreateAssetStatus={this.changeCreateAssetStatus.bind(this)}
              handleContentPage={this.handleContentPage.bind(this)}
              changeRequest={this.state.changeRequest}
              currProjectId={this.state.currProjectId}
              handleChangeStatus={this.handleChangeStatus.bind(this)}
              contentType = {contentProjectType}
              // editCampaign={this.editCampaign.bind(this)}
            />:null}
        </div>
      </div>
    )
  }
  changeView(view) {
    this.setState({
      view,
      typeSelected: 'campaign'
    })
    currentTabView = view;
    this.refetchBasedOnLocation()
  }

  handleContentPage = (from,type)=>{
    document.body.classList.add('campaigns_print_content_wrapper');
    var disableTemplateSelection=true;
    if(from == null ){
      //means its calling for edit or copy 
      disableTemplateSelection =true
    }else{
      //it is calling to create 
      disableTemplateSelection=false
    }
    this.setState({
      showContentPage:true,
      disableTemplateSelection :disableTemplateSelection
    })
  }
  handleScriptCreate() {
    this.setState({ scriptLoaded: false });
  }
  handleScriptError() {
    this.setState({ scriptError: true });
  }
  async handleScriptLoad() {
    this.setState({ scriptLoaded: true });
  }
  submitEventData(values){
       var dueTime = this.state
            .Moment(values[`scheduledDueTime`])
            .format("HH:mm");
        var dueDate = this.state
            .Moment
            .unix(values[`scheduledDueDate`])
            .format("DD/MM/YYYY");
        let DueDateTime = dueDate + " " + dueTime;
        let DueDateTimeStamp =  this.state
            .Moment(DueDateTime, 'DD/MM/YYYY HH:mm')
            .format("X");
        var objTosend={
          title : values.title,
          DueDateTime : DueDateTimeStamp,
          notes:values.notes
        }
        let eventFlag = false
        if(this.state.view == 'calendar' ){
          eventFlag=true
        }
        if(values.identity!==undefined){
          var eventId =values.identity;
          // edit task
          this.props.campaignsActions.addTaskData(objTosend,null,this.props.roleName,eventId,null,null,this.state.Moment,true,eventFlag);
          this.closeCreateEventPopup('edit');
        }else{
          // add task
          this.props.campaignsActions.addTaskData(objTosend,null,this.props.roleName,null,null,null,this.state.Moment,true,eventFlag);
        }  

  }
  openViewPopup(value){
    document.body.classList.add('overlay');
    this.setState({
      openEventView :true,
      currEvent:value
    })
  }
  closeEventDetailPopup(value){
    document.body.classList.remove('overlay');
    this.setState({
      openEventView :false,
    })
  }
  renderEventDetailPopup(){
    let roleName = JSON.parse(localStorage.getItem('roles')) !== null? JSON.parse(localStorage.getItem('roles'))[0].role_name:''
    return(
      <SliderPopup className="task-create-popup">
      <header class="heading clearfix">
          <h3>View Event</h3>
          <button class="btn-default close-popup" onClick={this.closeEventDetailPopup.bind(this)}><i class="material-icons" >clear</i></button>
         </header>
      <CampaignEventDetail
          Moment={this.state.Moment} 
          currEvent={this.state.currEvent} 
          roleName={roleName} 
          editEvent={this.editEvent.bind(this)} 
          deleteEvent={this.deleteEvent.bind(this)}
      />
</SliderPopup>
    )
  }
  editEvent(){
    this.closeEventDetailPopup();
    document.body.classList.add('overlay');
    this.setState({
      editEvent:true,
    })
  }
  deleteEvent(){
    let eventFlag = false
        if(this.state.view == 'calendar' ){
          eventFlag=true
        }
    let roleName = JSON.parse(localStorage.getItem('roles')) !== null? JSON.parse(localStorage.getItem('roles'))[0].role_name:''
    var eventId = this.state.currEvent.identity;
    this.props.campaignsActions.deleteTask(eventId,null,null,roleName,this.props.profile.identity,null,null,this.state.Moment,true,eventFlag);
  }
  closeCreateEventPopup(from){
    document.body.classList.remove('overlay');
    if(from == 'create'){
      this.setState({
        openAddEventPopup:false
      })
    }else{
      this.setState({
        editEvent:false,
      })
    }

  }
  renderEventCreationPopup(){
    let roleName = JSON.parse(localStorage.getItem('roles')) !== null? JSON.parse(localStorage.getItem('roles'))[0].role_name:''
    return(
      <SliderPopup className="task-create-popup">
      <header class="heading clearfix">
          <h3>Create Event</h3>
          <button class="btn-default close-popup" onClick={this.closeCreateEventPopup.bind(this,'create')}><i class="material-icons" >clear</i></button>
         </header>
      <CampaignCreateTasks 
        callfrom = 'global'
        onSubmit={this.submitEventData.bind(this)}
        Moment={this.state.Moment} 
        addTaskLoading ={this.props.campaigns.addTaskLoading}
        profile={this.props.profile.profile}
        roleName={roleName}
      />
</SliderPopup>
    )
  }
  renderEditEventPopup(){
    let roleName = JSON.parse(localStorage.getItem('roles')) !== null? JSON.parse(localStorage.getItem('roles'))[0].role_name:''
    return(
      <SliderPopup className="task-create-popup">
      <header class="heading clearfix">
          <h3>Edit Event</h3>
          <button class="btn-default close-popup" onClick={this.closeCreateEventPopup.bind(this,'edit')}><i class="material-icons" >clear</i></button>
         </header>
      <CampaignCreateTasks 
         callfrom = 'global'
         onSubmit={this.submitEventData.bind(this)}
         Moment={this.state.Moment} 
         addTaskLoading ={this.props.campaigns.addTaskLoading}
         profile={this.props.profile.profile}
         roleName={roleName}
        currEvent={this.state.currEvent}
      />
</SliderPopup>
    )
  }
  closeContentProject = () =>{
    document.body.classList.remove('campaigns_print_content_wrapper');
    this.setState({
      showContentPage:false,
      disableTemplateSelection :true
    })
    let campID = this.state.currentCampaign.identity
    this.props.campaignsActions.fetchComments(campID,null);
  }
 
  render () {
    let { listLoading } = this.props.campaigns
    const eventLoader = this.props.campaigns.typeEvent.typeEventLoading
    var me = this;
    let currentFilter = this.state.currentFilter == 'expired'
      ? 'expired'
      : this.state.currentFilter
    let roleName = JSON.parse(localStorage.getItem('roles')) !== null? JSON.parse(localStorage.getItem('roles'))[0].role_name:''
    let notShow = 0
    if (roleName === 'employee' || roleName === 'moderator') {
      notShow = 1
    }
    let pageView = '';
    if(this.state.view == 'list')
    {
      pageView = 'listViewDropdown';
    }
    if(this.props.location.pathname=="/search/campaign"){
      NavBar=SearchNav
      title="Search"
    }
    else{
      NavBar=CampaignNav
      title="Campaigns"
    }
    return (
      <div>
      <GuestUserRestrictionPopup setOverlay={true}/>
      <div className="overlay-feed"> </div>
      <section id='campaigns' className='campaigns'>
        {this.state.openAddEventPopup ? this.renderEventCreationPopup():null}
        {this.state.openEventView ? this.renderEventDetailPopup() : null}
        {this.state.editEvent ? this.renderEditEventPopup() : null}
        {this.state.CampaignPostsPopup ? this.renderCampaignPostsPopup() : null}
        {this.state.newCampaignPopup ? this.renderCampaignCreation() : null}
        {this.state.detailCampaignPopup ? this.renderCampaignDetails() : null}
        {this.state.updateCampaignPopup ? this.renderCampaignUpdate(this.state.currentCampaign) : null }
        {editdata  ? this.renderCampaignUpdate(this.state.currentCampaign) : null}
        {!this.props.location.pathname.includes("search")?
        <Header
          searchInput={this.props.searchedInput}
          add_new_subpopup='CAMPAIGNS'
          title={title}
          nav={NavBar}
          location={this.props.location}
          location_cat={currentFilter}
          popup_text='Click here to add new Campaign.'
          add_new_button
          Header_onclick={this.hide_add_new_popup.bind(this)}
          page="campaigns"
          changeHeaderView={this.changeView.bind(this)}
          currentView={this.state.view}
          campaignDetailPage = {this.state.detailCampaignPopup}
        />:''}
        <div className='main-container'>
          <span
            id='add_new_button'
            onClick={this.handleCampaignCreation.bind(this)}
          />
          <div id='content'>
            {this.state.view == 'calendar' ?
              <CampaignCalendar
                location={this.props.location}
                campaignClick={this.campaignClick.bind(this)}
                openEventViewPopup={this.openViewPopup.bind(this)}
              />
              :
              ((listLoading || eventLoader) && this.state.reactable == null )? <PreLoader location="campaign-loader" /> :
              <div className='page campaign-container page with-filters'>
                <div class='content'>
                    {roleName == 'guest' ?
                    <StaticDataForGuest props={this.props} />
                    : this.campaignListTable()}

                </div>

              </div>
            }
          </div>
        </div>
      </section>
      {this.state.scriptLoaded !== true ? (
            <Script
            url="https://www.pdfreactor.com/product/wrappers/javascript/lib/PDFreactor.js"
            onCreate={this.handleScriptCreate.bind(this)}
            onError={this.handleScriptError.bind(this)}
            onLoad={this.handleScriptLoad.bind(this)}/>
          ):''}
     </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    socialAccounts:state.socialAccounts,
    campaigns: state.campaigns,
    channels: state.channels,
    posts: state.posts,
    profile: state.profile,
    feed:state.feed
  }
}

function mapDispatchToProps(dispatch) {
  return {
    postsActions: bindActionCreators(postsActions, dispatch),
    campaignsActions: bindActionCreators(campaignsActions, dispatch),
    channelActions: bindActionCreators(channelActions, dispatch),
    feedActions: bindActionCreators(feedActions, dispatch)
  }
}

let connection = connect(mapStateToProps, mapDispatchToProps)

module.exports = connection(Campaigns)
