;
import { Link } from "react-router";
import { browserHistory } from "react-router";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Field, reduxForm } from 'redux-form';
import { renderField } from '../Helpers/ReduxForm/RenderField';
import MentionTextArea from '../Helpers/ReduxForm/MentionTextArea';
import PreLoader from '../PreLoader';
import * as userActions from '../../actions/userActions'
import * as tagsActions from '../../actions/tagsActions'
import * as campaignsActions from '../../actions/campaigns/campaignsActions';


const required = value => value
  ? undefined
  : 'Required';
class RecipientsPopupForm extends React.Component {

    constructor(props) {
      super(props);
      this.state = {
         customValidation:true,
         guestValueList:[],
         emailValidation:true
      }
    }
    handleMentionFocus(e){
      if(e.target.value == ''){
        this.setState({customValidation:true})
        this.props.change('users' ,'@')
      }
    }
    validateUserTag(e){
        var temp='',count=0,count2=0,flag=0;
        const len=Object.keys(e).length-1;

        for(var i=0;i<Object.keys(e).length-1;i++){
          //for loop is for checking the whole string every time it is updated
          temp=e[i];
          if(e[i]=="@"){count+=1}  //count of at the rate
          if((e[i]==")"&&e[i+1]==" "&& e[i+2]=="@" && flag!==2)){ //count of )" "@
            count2+=1;flag=1;
          }
          if((e[i]==")"&&e[i+1]==" "&& e[i+2]!=="@" && e[i+2]!==undefined) || (e[i]==")"&& e[i+1]!=="@" && e[i+1]!==undefined && e[i+1]!==" ")){
            flag=2;
          }
        }
        var first=e[0];
        var last=temp;

        temp=e[0]
          if(temp !== undefined){
            if(first=="@" && last==")" && e[i]==null){
              this.setState({customValidation:false})
              document.querySelector(".viewper").style.display="none";
            }
            else if(first=="@" && last==")" && e[i]==" "){
              if(flag==1){
                if(count==count2+1){
                  this.setState({customValidation:false})
                  document.querySelector(".viewper").style.display="none";
                }
                else{
                  this.setState({customValidation:true})
                  document.querySelector(".viewper").style.display="inline";
                }
              }
              else{
                this.setState({customValidation:false})
                document.querySelector(".viewper").style.display="none";
              }
            }
            else{
              this.setState({customValidation:true})
              document.querySelector(".viewper").style.display="inline";
            }
            if(flag==2 && (e[0]!=='@'|| e[0]!==' ')){
              this.setState({customValidation:true})
              document.querySelector(".viewper").style.display="inline";
            }
          }
          else
          {
            if(document.getElementById('guests').value !== ""){
              this.setState({customValidation:false})
            }else{
              this.setState({customValidation:true})
            }

            document.querySelector(".viewper").style.display="none";
          }
    }
    componentDidMount(){
      this.handleInitialize();
      var myInput = document.getElementById("guests");
      if(myInput.addEventListener ) {
          myInput.addEventListener('keydown',this.handleGuestAdd.bind(this),false);
      } else if(myInput.attachEvent ) {
          myInput.attachEvent('onkeydown',this.handleGuestAdd.bind(this)); /* damn IE hack */
      }
    }


    handleInitialize () {
      const initData = {
      //   feedId: this.props.feedId,
      }

      this.props.initialize(initData)
    }
    handleGuestAdd(event){
      var me = this;
      if(me.state.emailValidation==false &&event.target.value!=='' && (event.keyCode==32 || event.keyCode==13 || event.keyCode==9)){  
        var guestList = me.state.guestValueList;
          guestList.push(event.target.value);
          if(event.keyCode==32){
            event.preventDefault();
          }
          me.setState({
            guestValueList : guestList
          },()=>{
            event.target.value="";
          })
        }
    }

    validateMultipleEmail(e){
      var re = /^[A-Z0-9\._%-]+@[A-Z0-9\.-]+\.[A-Z]{2,4}(?: ?[,] ?[A-Z0-9\._%-]+@[A-Z0-9\.-]+\.[A-Z]{2,4})*$/i;
      if(e.target.value == ""){
          if(document.getElementById('title').value !== ""){
            this.setState({
              customValidation:false,
              emailValidation : false
            })
          }else{
            if(this.state.customValidation === false){
              this.setState({customValidation:true,
                emailValidation : true})
            }
          }
          document.querySelector(".guest-email-validation").style.display="none";
      }else{
        if(re.test(e.target.value)){
          this.setState({customValidation:false,
            emailValidation : false})
          document.querySelector(".guest-email-validation").style.display="none";
        }else{
          this.setState({customValidation:true,emailValidation : true})
          document.querySelector(".guest-email-validation").style.display="inline";
        }
      }
  }
  checkUsertags(e){
   var me = this;
   this.props.change("guests",this.state.guestValueList)
   var guestData = document.getElementById("guests").value;
   if(document.querySelector(".viewper").style.display=="inline"){
     e.preventDefault();
   }else{
     if(guestData!==''){
       var guestList = me.state.guestValueList;
       guestList.push(guestData);
       me.setState({
         guestValueList : guestList
       },()=>{
         me.props.change("guests",me.state.guestValueList)
       })
     }
   }
 }
 handleSubmit(e){
    e.preventDefault();
   this.props.handleRecipientSubmit(e);
 }

   removeGuest(index){
   this.state.guestValueList.splice(index,1);
   this.setState({
     test:""
   })
   }

    render(){
      const {
         handleSubmit,
         pristine,
         submitting
       } = this.props
       var index =0;
      let userTags = this.props.userTags;
      var index =0;
       return(
        <div>
        <form onSubmit={handleSubmit}>
           {/* <Field
              component='input'
              type='hidden'
              name='feedId'
              /> */}
           <div class="usersbox">
              <input type="hidden" name="feed_identity" value="2" />
              <label>Users</label>
              <div class="form-row  select user-tag-editor">
                 <Field
                     component={MentionTextArea}
                     userTags={userTags}
                     type='textarea'
                     role="textbox"
                     name='users'
                     id='users'
                     placeholder='Select users with @ tag'
                     aria-multiline='true'
                     onFocus={this.handleMentionFocus.bind(this)}
                     onChange={this.validateUserTag.bind(this)}
                    />
                 <div className="viewper">* Please add only user tags</div>
              </div>
           </div>
           <div class="sendbtn clearfix">
              <button type="submit" tabIndex="8000"  disabled={this.state.customValidation} onClick={this.checkUsertags.bind(this)}><span> Save</span></button>
           </div>
        </form>
       {/* guest  */}
       <div className="guestEmail">
                    <label>Guests</label>
                    <div className="form-row multiple-email-container" >
                    <input 
                      // type="text" 
                        type='textarea'
                        name='guests'
                        id='guests'
                        placeholder='Enter guest emails'
                        class="multiple_emails-input text-left"
                        onChange={this.validateMultipleEmail.bind(this)}
                        />
                        
                                                  <ul className="multiple_emails-ul" key={index}>
                                                  {
                                                    this.state.guestValueList.length !== 0?
                                                    this.state.guestValueList.map((guestEmail,i)  => {
                                                      return(
                                                      <li className ="multiple_emails-email">
                                                        <span class="email_name" data-email={guestEmail}>{guestEmail}</span>
                                                          <span
                                                            id='close-popup'
                                                            className='btn-default multiple_emails-close'
                                                            onClick={this.removeGuest.bind(this,i)}
                                                          >
                                                            <i className='material-icons'>clear</i>
                                                          </span>
                                                      </li> 
                                                      )
                                                    })
                                                    :''
                                                  }
                                                  </ul>

                                                  </div>
                                                  <div className="guest-email-validation">* Please add valid email address</div>
                                                  </div>
       {/* end guest */}
     </div>
       )
    }
}
function mapStateToProps(state, ownProps) {
    return {
      userTags: state.tags.userTags,
      departmentTags: state.tags.postTags
    }
  }
  
function mapDispatchToProps(dispatch) {
    return {
      tagsActions: bindActionCreators(tagsActions, dispatch),
      userActions: bindActionCreators(userActions, dispatch),
      campaignsActions:bindActionCreators(campaignsActions, dispatch)
    }
  }
let connection = connect(mapStateToProps, mapDispatchToProps)
var reduxConnectedComponent = connection(RecipientsPopupForm)
  
export default reduxForm({
    form: 'RecipientsPopupForm' // a unique identifier for this form
})(reduxConnectedComponent)
