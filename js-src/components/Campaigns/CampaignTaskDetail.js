import DataRow from '../DataRow';
class CampaignTaskDetail extends React.Component {
    constructor (props) {
        super(props)
      }
      render () {
          var currTask = this.props.currTask;
            var dueDate = this.props.Moment ? this.props.Moment.unix(currTask.due_date).format('dddd, Do MMMM  YYYY ') + 'at' + this.props.Moment.unix(currTask.due_date).format(' h:mm a'):''
          return (
            <div className="campaignTaskDetails">
                <div className='action-button'>
                <ul className='dd-menu context-menu user_3dots'>
                    <li className='button-dropdown'>
                        <a className='dropdown-toggle user_drop_toggle'>
                        <i className='material-icons'>more_vert</i>
                        </a>

                        <ul className='dropdown-menu user_dropAction'>
                        <li>
                            <a onClick={this.props.editTask.bind(this)}>
                            Edit
                            </a>
                        </li> 
                        {
                          this.props.roleName=='super-admin'|| this.props.roleName=='admin'?  
                        <li>
                            <a onClick={this.props.deleteTask.bind(this,currTask.identity)}>
                                Delete
                            </a>
                        </li>: ''}
                        </ul>
                    </li>
                    </ul>
                    </div>
                <div>
                    <DataRow label='Task Title' value={currTask.title} />
                    <DataRow label='Due Date' value={dueDate} />
                    <DataRow label='Notes' value={currTask.notes} />
                    <DataRow label='Status' value={currTask.status} />
                    <DataRow label='Created By' value={currTask.created_by.first_name+" "+ currTask.created_by.last_name+"  ("+ currTask.created_by.email+")"}/>
                    <DataRow label='Assigned To' value={currTask.assigneee.first_name+" "+ currTask.assigneee.last_name+"  ("+ currTask.assigneee.email+")"} />
                </div>
            </div>

          )
      }
}
module.exports = CampaignTaskDetail