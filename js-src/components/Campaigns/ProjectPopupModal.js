
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import CenterPopupWrapper from '../CenterPopupWrapper'
import PopupWrapper from '../PopupWrapper'
import notie from 'notie/dist/notie.js'
class ProjectPopupModal extends React.Component{
    constructor(props){
        super(props);
        this.state={
            openPreviewPopup:false,
            previewLink:{}
        }
    }
    closePrintbutton(){
        document.body.classList.remove('center-wrapper-body-container');
        this.setState({
            openPreviewPopup:false,
            previewLink:{}
        })
    }
    openPreviewPopupIsClicked(previewLink){
        this.setState({
            openPreviewPopup:true,
            previewLink:previewLink
        })
    }
    openPreviewPopup(){
        var  html_Preview_campaigns = "";
        var templatePreview ="";
        document.body.classList.add('center-wrapper-body-container');
        var userList=[];
        var media_url='https://s3.eu-west-2.amazonaws.com/visibly-staging/images/pirates_com/1/W1567571744e0+%281%29-68dbe416-9c46-b7f3-f1ae-ad3b0af60125.pdf';
        if(this.state.previewLink.type == "print"){
        if(this.state.previewLink.pdf_s3_url){
            media_url = this.state.previewLink.pdf_s3_url;
        }else if(this.state.previewLink.pdf_url){
            media_url = this.state.previewLink.pdf_url;
        }
    }else{
            html_Preview_campaigns = this.state.previewLink.html_preview;
            templatePreview = decodeURIComponent(escape(atob(html_Preview_campaigns)));
        }
   return(
        <div>
                <CenterPopupWrapper>
                    <div className = "print-page-container">
                    <div className='inner-print-page-container'>
                        <div className='print-details'>
                            <header className='heading clearfix'>
                                <h3> {this.state.previewLink.title}  {this.state.previewLink.type == "email" ? <span className = "email-from"> &lt;<span className = "user-email-cmp"> {this.state.previewLink.from_email}</span>&gt;</span> :''}</h3>
                                <button id="close-popup" class="btn-default" onClick = {this.closePrintbutton.bind(this)}><i class="material-icons">clear</i></button>
                            </header>
                            <PopupWrapper>
                            <div className = "linkpreview-parent-class">
                            {this.state.previewLink.type == "print" ? 
                                <div>
                                    <iframe className = "pdf-preview projectPopupModal"
                                    src={`https://docs.google.com/gview?url=${media_url}&embedded=true`}
                                    />
                                </div>
                                : 
                                <div id = "emailWrapper"> 
                                <div className = "details-mail">
                                 <span className = "email-form-subject"> Subject : <span className = "value-text">  {this.state.previewLink.subject} </span>  </span>
                                 <span className = "email-form-email_preview"> Email Preview : <span  className = "value-text"> {this.state.previewLink.email_preview} </span>  </span>
                                </div>
                                <div dangerouslySetInnerHTML={{__html: templatePreview}} className="inner-pdf-wrapper"></div>
                              </div>
                                }
                            </div>
                            </PopupWrapper>
                        </div>
                    </div>
                    </div>
                </CenterPopupWrapper>
        </div>
        )
    }

    render(){
        let data = this.props.data;
        let print = data.event.feed_post;
        var authorAvatarURL = print.created_by.avatar;
        var authorAvatar = {
          backgroundImage: `url(${typeof authorAvatarURL !== 'undefined' && authorAvatarURL != '' ? authorAvatarURL : '/img/default-avatar.png'})`
        }
        let me = this;
        let roleName=this.props.roleName;
        var row_number = data.grid.row_number;
        let style = {
            left: data.position.left,
            right: data.position.right,
            top: data.position.top,
            bottom: data.position.bottom
        };
        let templatePreview = ''
            if(print.html_preview){
                templatePreview = decodeURIComponent(escape(atob(print.html_preview)));
            }else{
            let templateFirstScreen =  `<div class="first-pdf-page">
            <div class="pdf-wrapper">
                <div class="container">
                    <div class="pdf-background">
                        <img src="https://magazine.visibly.io/images/pdf.png" alt="pdf-background" />
                    </div>
                </div>
            </div>
            <div class="pdf-content">
                <div class="container">
                    <div class="pdf-content-heading">
                        <h2>Admin message</h2>
                    </div>
                    <div class="content-paragraph">
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been
                            the
                            industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of
                            type
                            and scrambled it to make a type specimen book. It has survived not only five<br /><br /><br />

                            centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It
                            was
                            popularised in the 1960s with the release of Letraset sheets containing Lorem Curabitur
                            pellentesque ut risus auctor varius. Integer imperdiet viverra neque, vel pharetra lacus
                            sagittis at. Utvariusvel tortor quis sagit-tis. Donec risus nisi, hendrerit sed mollis quis,
                            semper ut ligula. Cras pulvinar molestie neque, nec dapibus diam tempus vitae. Fusce
                            consectetur, ex nec venenatis consequat, diam quam semper diam, id porta lacus ex et nulla. Cras
                            commodo, augue nec commodo cursus, mi justo mattis ante, vitae varius orci massa a eros. Sed
                            hendrerit euismod leo. Curabitur pellentesque ut risus auctor varius. Integer imperdiet viverra
                            neque, vel pharetra lacus sagittis at. Utvariusvel tortor quis sagit-tis. Donec risus nisi,
                            hendrerit sed mollis quis, semper ut ligula. Cras pulvinar molestie neque, nec dapibus diam
                            tempus vitae. Fusce consectetur, ex nec venenatis consequat, diam quam semper diam, id porta
                            lacus ex et nulla. Cras commodo, augue nec commodo cursus, mi justo mattis ante, vitae varius
                            orci massa a eros. Sed hendrerit euismod leo.</p>
                    </div>
                </div>
            </div>
            </div> `;
            let  tempObj =btoa((unescape(encodeURIComponent(templateFirstScreen))));
            templatePreview = decodeURIComponent(escape(atob(tempObj)));
            }
        return(
            <div>
                {this.state.openPreviewPopup==true ? this.openPreviewPopup():''}
                <div
                    className={`calendar-detail-popup popup_row_${row_number} popup_row_${data
                    .position.vertical} popup_column_${data.position.horizontal}`}
                    style={style}
                >
                    <button
                    id="close-popup"
                    className="btn-default"
                    onClick={()=>{this.props.closepostDetailPopup()}}
                    >
                    <i className="material-icons">clear</i>
                    </button>
                    <div className="content-project-popup-wrapper">
                    <div
                        id={`post-${print.identity}`}
                        className={`post-${print.identity} post`}
                    >
                        <div
                        className={`post-header schedule-post-header`}
                        >

                        <div className='author-thumb'>
                            <div
                            className='thumbnail'
                            style={authorAvatar}
                            // data-for={this.props.tooltipid}
                            />
                        </div>

                        <div className='author-data'>
                            <div className='author-meta'>
                            <b>
                                {print.title}
                                {' '}
                            </b>
                            </div>
                            <div className='post-time'>
                            <span className="shipping-icon"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path fill="none" d="M0 0h24v24H0V0z"></path><path d="M20 8h-3V4H3c-1.1 0-2 .9-2 2v11h2c0 1.66 1.34 3 3 3s3-1.34 3-3h6c0 1.66 1.34 3 3 3s3-1.34 3-3h2v-5l-3-4zm-.5 1.5l1.96 2.5H17V9.5h2.5zM6 18c-.55 0-1-.45-1-1s.45-1 1-1 1 .45 1 1-.45 1-1 1zm2.22-3c-.55-.61-1.33-1-2.22-1s-1.67.39-2.22 1H3V6h12v9H8.22zM18 18c-.55 0-1-.45-1-1s.45-1 1-1 1 .45 1 1-.45 1-1 1z"></path></svg></span>
                            <span
                                className='timeFromNow'
                                ref={print.shipping_date}
                                title={this.props.moment !== null
                                ? print.shipping_date !== undefined
                                    ?
                                    this.props.moment.unix(print.shipping_date).format('dddd, Do MMMM YYYY ')+'at'+this.props.moment.unix(print.shipping_date).format(' h:mm a')
                                        : this.props
                                            .moment(print.shipping_date)
                                            .format('dddd, Do MMMM YYYY, h:mm:ss a')
                                    : ''
                                }
                            >
                            {this.props
                                .moment(print.shipping_date)
                                .format('DD/MM/YYYY')}
                            </span>
                                <span className = "eye-icon" onClick={this.openPreviewPopupIsClicked.bind(this,print)}>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                        <path fill="none" d="M0 0h24v24H0V0z"/>
                                        <path d="M12 6.5c3.79 0 7.17 2.13 8.82 5.5-1.65 3.37-5.02 5.5-8.82 5.5S4.83 15.37 3.18 12C4.83 8.63 8.21 6.5 12 6.5m0-2C7 4.5 2.73 7.61 1 12c1.73 4.39 6 7.5 11 7.5s9.27-3.11 11-7.5c-1.73-4.39-6-7.5-11-7.5zm0 5c1.38 0 2.5 1.12 2.5 2.5s-1.12 2.5-2.5 2.5-2.5-1.12-2.5-2.5 1.12-2.5 2.5-2.5m0-2c-2.48 0-4.5 2.02-4.5 4.5s2.02 4.5 4.5 4.5 4.5-2.02 4.5-4.5-2.02-4.5-4.5-4.5z"/>
                                    </svg>
                                </span>
                            </div>

                            
                            {(roleName == 'admin' || roleName =='super-admin') ?

                            <ul class='dd-menu context-menu'>
                            <li class='button-dropdown'>
                                <a class='dropdown-toggle' id={`${'dropdown-toggle-' +print.identity}`}>
                                <i class='material-icons'>more_vert</i>
                                </a>
                            <ul class='dropdown-menu' id={`${'dropdown-menu-' + print.identity}`}> 
                                {print.created_by.identity==this.props.profile.identity || roleName =='super-admin' ? <li><a class='btn-edit-folder' onClick={()=>{this.props.handleEditCopy(print.type,print.identity,"edit","fromPlanner")}}>Edit</a></li>:''}
                                <li><a class='btn-edit-folder' onClick={()=>{this.props.handleEditCopy(print.type,print.identity,"copy")}}>Copy</a></li>
                                {print.created_by.identity==this.props.profile.identity || roleName =='super-admin' ?<li><a class='btn-edit-folder' 
                                onClick={()=>{
                                    notie.confirm(
                                      `Are you sure you want to delete this Content Project?`,
                                      'Yes',
                                      'No',
                                     function(){
                                      me.props.handleDelete(print.type,print.identity)
                                      },function(){}
                                    )}
                                    }>Delete</a></li>:''}
                                </ul>
                            </li>
                            </ul> : '' }
                        </div>
                        </div>

                        <div className='post-detail-pic-wrapper'>
                        <div className = "post-pic-ranking-container">
                            <div className="post-pic">
                            <div className ={`img-item `}>
                                <div dangerouslySetInnerHTML={{__html: templatePreview}} className="inner-pdf-wrapper"></div>
                                {/* <img src="https://s3.eu-west-2.amazonaws.com/visibly-staging/images/pirates_com/1/WCharity-c2013d66-5e8d-c516-540f-7742465a0d55.jpg"/> */}
                            </div>
                            </div>
                        </div>
                        </div>
                    </div>
                    </div>
                </div> 
            </div>
        )
    }
}

const mapStateToProps = state => ({
    profile:state.profile.profile
})
  
const mapDispatchToProps = dispatch => ({

})

let connection = connect(
mapStateToProps,
mapDispatchToProps
)

module.exports = connection(ProjectPopupModal)