import { bindActionCreators } from 'redux'
import { Field, reduxForm } from 'redux-form'
import { connect } from 'react-redux'
import ReactDatePicker from '../Helpers/ReduxForm/ReactDatePicker' // react datepicker field
import reactDatepickerChunk from '../Chunks/ChunkDatePicker' // datepicker chunk
import ReactTimePicker from '../Helpers/ReduxForm/ReactTimePicker' // react datepicker field
import moment from '../Chunks/ChunkMoment'
import ChunkSelect from '../Chunks/ChunkReactSelect';
import { renderField } from '../Helpers/ReduxForm/RenderField'
import PreLoader from '../PreLoader'
const required = value => (value ? undefined : 'Required')
class CampaignCreateTasks extends React.Component {
    constructor (props) {
        super(props)
        this.state = {
            ReactDatePicker: null,
            currAssignee:null,
            disabledFields:false,
            reactSelect :null
        }
      }

      componentWillMount () {
        moment().then(moment => {
          ChunkSelect().then(reactselect => {
          reactDatepickerChunk()
            .then(reactDatepicker => {
              this.setState({
                reactSelect: reactselect,
                ReactDatePicker: reactDatepicker,
                Moment: moment
              })
            })
          })
          .catch(err => {
            throw err
          })
        })
      }

      componentDidMount () {
        if((this.props.currTask!==undefined && this.props.currTask.created_by.identity !== this.props.profile.identity )|| this.props.profile.role.data[0].role_name !== 'super-admin'){
          this.setState({disabledFields:true})
        }
        if(this.props.currEvent !==undefined && this.props.profile.role.data[0].role_name !== 'super-admin'){
          this.setState({disabledFields:true})
        }
        this.handleInitialize(this.props)
      }
    
      handleInitialize (nextProps = null) {
        if (typeof this.props.currTask !== 'undefined' ) {
          const initData = {
            identity: this.props.currTask.identity,
            title: this.props.currTask.title,
            notes: this.props.currTask.notes,
            assignTo: this.props.currTask.assigneee.identity
          }
          var objToPut ={
              label:`${this.props.currTask.assigneee.first_name} ${this.props.currTask.assigneee.last_name} (${this.props.currTask.assigneee.email})`,
              value: this.props.currTask.assigneee.identity
          }
          this.handleChange(objToPut);
          initData['scheduledDueDate']=this.props.currTask.due_date;
          initData['scheduledDueTime']=this.props.Moment.unix(this.props.currTask.due_date);
          initData['status']=this.props.currTask.status;
          this.props.initialize(initData)
        }else if(typeof this.props.currEvent !== 'undefined'){
          const initData = {
            identity: this.props.currEvent.identity,
            title: this.props.currEvent.title,
            notes: this.props.currEvent.notes,
          }
          initData['scheduledDueDate']=this.props.currEvent.end_date;
          initData['scheduledDueTime']=this.props.Moment.unix(this.props.currEvent.end_date);
          this.props.initialize(initData)
        }
        else{
          if(this.props.contentProject!==undefined){
            if(this.props.Moment !== null){
              const initData = {
                scheduledDueDate: this.props.contentProject.start_date <= this.props.Moment().unix()? this.props.Moment().unix():this.props.contentProject.start_date,
              }
              this.props.initialize(initData)
            }
          }else{
            if(this.props.callfrom !== 'global'){
              if(this.props.Moment !== null){
                const initData = {
                  scheduledDueDate: this.props.campaignData.start_date <= this.props.Moment().unix()? this.props.Moment().unix():this.props.campaignData.start_date
                }
                this.props.initialize(initData)
              }
            }else{
              if(this.props.Moment !== null){
                const initData = {
                  scheduledDueDate: this.props.Moment().unix()
                }
                this.props.initialize(initData)
              }
            }
          }
        }
      }
      handleChange(e){
          this.setState({
            currAssignee : e
          })
          this.props.change('assignTo',e.value);
      }
      render(){
        const {  handleSubmit,pristine, submitting } = this.props
          var me = this
          var endDate = "";
          var minDate ='';
          if(this.props.contentProject!==undefined){
            if (
              this.props.contentProject.shipping_date &&
              this.props.Moment
            ) {
              endDate = this.props.Moment
                .unix(this.props.contentProject.shipping_date)
                .format("llll")
                .toString();
            }
            if(this.props.contentProject.start_date &&
              this.props.Moment){
                minDate = this.props.contentProject.start_date <= this.props.Moment().unix()? this.props.Moment().unix(): this.props.Moment
                .unix(this.props.contentProject.start_date)
                .format("llll")
                .toString();
              }
          }else{
            if(this.props.callfrom !== 'global'){
            if (
              this.props.campaignData.end_date &&
              this.props.Moment
            ) {
              endDate = this.props.Moment
                .unix(this.props.campaignData.end_date)
                .format("llll")
                .toString();
            }
            if(this.props.campaignData.start_date &&
              this.props.Moment){
                minDate = this.props.campaignData.start_date <= this.props.Moment().unix()? this.props.Moment().unix(): this.props.Moment
                .unix(this.props.campaignData.start_date)
                .format("llll")
                .toString();
              }
          }else{
            minDate = this.props.Moment(new Date())
          }
        }
          var assigneeList =[];
          if(this.props.callfrom !== 'global'){
          this.props.campaignData.userTag.data!==undefined && Object.keys(this.props.campaignData.userTag.data).length>0?
          this.props.campaignData.userTag.data.map((userData,index)=>{
              var objToPut ={
                label : userData.name,
                value:userData.identity
              }
              
              var isDuplicate = false;
              assigneeList.forEach(obj =>
                {
                  if (obj.value ==  objToPut.value)
                  {
                      isDuplicate = true;
                  }
                })

                if(assigneeList.length ==0 || isDuplicate ==false)
                {
                  assigneeList.push(objToPut)
                }
          })
          :'';
        }
          return(
        <div>
          {this.props.addTaskLoading ==true ?  <PreLoader className = "Page" /> : <div />}
        <form onSubmit={handleSubmit} name="submitTask">
          <div className="form-row">
              <label>Title</label>
              <Field
              name="title"
              id="taskTitle"
              type="text"
              placeholder={this.props.callfrom !== 'global'? "Add task title" : "Add event title" }
              component={renderField}
              validate={[required]}
              disabled={this.state.disabledFields}
              />
            </div>
            <Field
            component={renderField}
            name="identity"
            type="hidden"
            />
            <div className="form-row">
              <label>{this.props.callfrom == 'global' ? 'Date of Event' : 'Due date'}</label>
              <div className="taskTimeFields">
             <Field
                            name={"scheduledDueDate"}
                            disabled={this.state.disabledFields}
                            component={props => {
                              return (<ReactDatePicker
                                {...props}
                                reactDatePicker={me.state.ReactDatePicker}
                                moment={me.props.Moment}
                                minDate={minDate}
                                maxDate={endDate}
                                dateFormat="DD/MM/YYYY" />)
                            }}
                            
                /> 
              {this.props.callfrom !== 'global' ?
              <div>
                <div className="taskMiddleText">at</div>
                <div className="taskTimeField">
                <Field
                    ref={`scheduledTime`}
                    name={`scheduledDueTime`}
                    disabled={this.state.disabledFields}
                    component={props => {
                        return (<ReactTimePicker {...props} moment={me.props.Moment} />)
                        }}
                        />
                </div> 
                </div>:''}
              </div>
            </div>
            <div className="form-row">
              <label>Notes</label>
              <div className="form-field">
                <Field
                name="notes"
                id="taskNotes"
                type="textarea"
                component={renderField}
                validate={required}
                disabled={this.state.disabledFields}
                />
              </div>
            </div>
            {this.props.callfrom !== 'global'?
            <div className="form-row">
              <label>Assigned to</label>
              <div className="form-field">
              <div class="select-wrapper assignToField">
              {this.state.reactSelect !== null ?
                <this.state.reactSelect.default
                  value={this.state.currAssignee}
                  onChange={this.handleChange.bind(this)}
                  options={assigneeList}
                  validate={[required]}
                  isDisabled={this.state.disabledFields}
                /> :''}
                <Field
                  name={'assignTo'}
                  component={renderField}
                  type='hidden'
                />
                </div>
              </div>
            </div>:''}
            {this.props.currTask!==undefined?
            <div className="form-row">
              <label>Status</label>
              <div class="statusRadioButton">
                {(this.props.roleName !=="super-admin" && this.props.roleName !=="admin" )  ? '':
                <label class="statusLabel customradio"><Field name="status" component="input" type="radio" value="pending"/> Pending <span className="checkmark"></span></label>}
                <label class="statusLabel customradio"><Field name="status" component="input" type="radio" value="in-progress"/> In-Progress  <span className="checkmark"></span></label>
                <label class="statusLabel customradio"><Field name="status" component="input" type="radio" value="completed"/> Completed  <span className="checkmark"></span></label>
                {(this.props.roleName !=="super-admin" && this.props.roleName !=="admin" )  ? '':
                <label class="statusLabel customradio"><Field name="status" component="input" type="radio" value="approved"/> Approved  <span className="checkmark"></span></label>}
                {(this.props.roleName !=="super-admin" && this.props.roleName !=="admin" )  ? '':
                <label class="statusLabel customradio"><Field name="status" component="input" type="radio" value="unapproved"/> Unapproved  <span className="checkmark"></span></label>}
              </div>
            </div>
            :''}

            <div className="clearfix button-wrapper">
              <button
                className='btn btn-theme right'
                type='submit'
                disabled={pristine || submitting}
                disabled={this.props.callfrom !== 'global' && this.state.currAssignee==null?true:false}
                >
                Save
              </button>
            </div>
          </form>
          </div>
          )
        }
}
function mapStateToProps (state, ownProps) {
    return {
     
    }
  }
  function mapDispatchToProps (dispatch) {
    return {
     
    }
  }

let connection = connect(mapStateToProps, mapDispatchToProps);
var reduxConnectedComponent = connection(CampaignCreateTasks);
export default reduxForm({
    form: 'submitTask' // a unique identifier for this form
  })(reduxConnectedComponent)