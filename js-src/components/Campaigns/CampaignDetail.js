import * as utils from '../../utils/utils'
import DataRow from '../DataRow'
import PreLoaderMaterial from '../PreLoaderMaterial'
import ImageLoader from 'react-imageloader'
import {Link,browserHistory } from 'react-router';
import reactStringReplace from 'react-string-replace'
class CampaignDetail extends React.Component {
  constructor (props) {
    super(props)
  }

  componentDidMount () {
    //hide 3 dots if there no edit or terminate option in dropdown
    if(document.getElementsByClassName('dropdown-menu')!== undefined && document.getElementsByClassName('dropdown-menu')[0].getElementsByTagName("li").length ==0){
      if(document.getElementsByClassName('button-dropdown') !== undefined){
        document.getElementsByClassName('button-dropdown')[0].classList.add('hide');
      }
    }
  }
  downloadAsset () {
    /* utils.forceDownload(this.props.asset.media_url, this.props.asset.title + "." + this.props.asset.media_extension, this.props.asset.media_type ) */
  }
  imageLoaded(){
    if(document.querySelectorAll('.asset-detail-popup .asset-content-wrapper').length>0){
    let assetWidth = document.querySelectorAll('.asset-detail-popup .asset-content-wrapper')[0].offsetWidth // Get asset width
    document.querySelectorAll('.asset-detail-popup .author-tag-wrapper')[0].style.width = `${assetWidth}px`;
    }
    if(document.querySelectorAll('#analytics .side-popup img').length>0){
      let assetWidth = document.querySelectorAll('#analytics .side-popup img')[1].offsetWidth // Get asset width
      if(assetWidth<320)
      {
        document.querySelectorAll('#analytics .asset-content-wrapper')[0].classList.add('column-wrapper')
      }
      else{
        document.querySelectorAll('#analytics .asset-content-wrapper')[0].classList.remove('column-wrapper')
      }
      }

  }
  pendingTaskLoader(){
    return(
      // <div class="campaigntaskTableWrapper">
                        // <div class=" loader-culture-tableExpand">
                            // <div>
                              <div class="table-anytlics-body">
                                  <table class="table responsive-table">
                                    <thead>
                                        <tr class="reactable-column-header">
                                          <th class="reactable-th-serialno reactable-header-sortable serialno  a-center">
                                              <div class="rank-title loader-grey-line  loader-line-radius loader-line-height"></div>
                                          </th>
                                          <th class="reactable-th-serialno reactable-header-sortable serialno  a-center">
                                              <div class="rank-title loader-grey-line  loader-line-radius loader-line-height"></div>
                                          </th>
                                          <th class="reactable-th-serialno reactable-header-sortable serialno  a-center">
                                              <div class="rank-title loader-grey-line  loader-line-radius loader-line-height"></div>
                                          </th>
                                          <th class="reactable-th-serialno reactable-header-sortable serialno  a-center">
                                              <div class="rank-title loader-grey-line  loader-line-radius loader-line-height"></div>
                                          </th>
                                        </tr>
                                    </thead>
                                    <tbody class="reactable-data">
                                        <tr class="table-body-text">
                                          <td>
                                              <div class="rank-title loader-grey-line  loader-line-radius loader-line-height"></div>
                                          </td>
                                          <td>
                                              <div class="rank-title loader-grey-line  loader-line-radius loader-line-height"></div>
                                          </td>
                                          <td>
                                              <div class="rank-title loader-grey-line  loader-line-radius loader-line-height"></div>
                                          </td>
                                          <td>
                                              <div class="rank-title loader-grey-line  loader-line-radius loader-line-height"></div>
                                          </td>
                                        </tr>
                                        <tr class="table-body-text">
                                          <td>
                                              <div class="rank-title loader-grey-line  loader-line-radius loader-line-height"></div>
                                          </td>
                                          <td>
                                              <div class="rank-title loader-grey-line  loader-line-radius loader-line-height"></div>
                                          </td>
                                          <td>
                                              <div class="rank-title loader-grey-line  loader-line-radius loader-line-height"></div>
                                          </td>
                                          <td>
                                              <div class="rank-title loader-grey-line  loader-line-radius loader-line-height"></div>
                                          </td>
                                        </tr>
                                    </tbody>
                                  </table>
                              </div>
                      //       </div>
                      //   </div>
                      // </div>
    )
  }
  pendingContentLoader(){
    return(
            <div class="asset-container">
                              <div>
                                  <div id="container">
                                    <div class="archive-container asset-file-container clearfix">
                                        <div class="vid file-image asset-file imgwrapper">
                                          <div class="inner_imgwrapper assetInnerWrapper"><a class="type-image"><span
                                              class="imageloader loaded thumbnail loader-assets-image"><img
                                              src="/img/visiblyLoader.gif" /></span></a></div>
                                          <a class="file-details">
                                              <span
                                                class="title">
                                                <div class="loader-grey-line loader-line-height loader-line-radius loader-assets-title">
                                                </div>
                                              </span>
                                          </a>
                                        </div>
                                        <div class="vid file-image asset-file imgwrapper">
                                          <div class="inner_imgwrapper assetInnerWrapper"><a class="type-image"><span
                                              class="imageloader loaded thumbnail loader-assets-image"><img
                                              src="/img/visiblyLoader.gif" /></span></a></div>
                                          <a class="file-details">
                                              <span
                                                class="title">
                                                <div class="loader-grey-line loader-line-height loader-line-radius loader-assets-title">
                                                </div>
                                              </span>
                                          </a>
                                        </div>
                                        <div class="vid file-image asset-file imgwrapper">
                                          <div class="inner_imgwrapper assetInnerWrapper"><a class="type-image"><span
                                              class="imageloader loaded thumbnail loader-assets-image"><img
                                              src="/img/visiblyLoader.gif" /></span></a></div>
                                          <a class="file-details">
                                              <span
                                                class="title">
                                                <div class="loader-grey-line loader-line-height loader-line-radius loader-assets-title">
                                                </div>
                                              </span>
                                          </a>
                                        </div>
                                    </div>
                                  </div>
                              </div>
                            </div>
    )
  }
  openContentDetail(e){
    this.props.campaignsActions.getContentProjectById(e.type,e.identity,e.camapaign_id,'edit');
  }

  fetchAllData(callfrom){
    this.props.campaignsActions.redirectToProductionForReviewContent(callfrom)
  }

  fetchAllDataforPendingContent(callfrom){
    this.props.campaignsActions.redirectToProductionForPendingContent(callfrom)
  }
  handleEditCampaign(){
    this.props.editCampaign();
  }
  terminateCampaign(){
    var me = this;
        me.props.terminateCampaign();
  }
/**
    * to redirect to feed page is hashtag is clicked from analytics page
    * @param {*} match name of hashtag
    * @author disha 
    */
   redirectToHashTag(match){
    document.body.classList.remove('overlay');
     browserHistory.push(`/feed/default/live#`+match);
  }
	/**
	 * @author : disha
	 * @augments : none
	 * @use : to display data in read mode
	 */
  campaignDataDetail () {
    var hashTagDetail=[]
      if(this.props.campaign.hashtag !== null){
        for (let index = 0; index < this.props.campaign.hashtag.hashtag.length; index++) {
          const element = this.props.campaign.hashtag.hashtag[index];
          hashTagDetail.push({
            'display': element.title,
            'identity': element.hashtag_identity
          })
        }
      }
    var social = typeof this.props.campaign.SocialChannel.data !==
								'undefined' && this.props.campaign.SocialChannel.data
								? this.props.campaign.SocialChannel.data
                : []
    var campaignDetail = {
      title: this.props.campaign.title,
      description: this.props.campaign.description,
      userPermissions: this.props.campaign.user_permissions !== ''
												? this.props.campaign.user_permissions
                        : '',
      campaignHashTag:this.props.campaign.hashtag ? reactStringReplace(this.props.campaign.hashtag.detail, /#(\w+)/g, (match, i) => ( 
        <Link onClick={this.redirectToHashTag.bind(this,match)} >{`#` + match}</Link> 
      )):'',
      message: this.props.campaign.message,
      campaignType: this.props.campaign.campaign_type,
      objective: this.props.campaign.campaign_objective,
      audience: this.props.campaign.targeted_audience,
      manager: this.props.campaign.User.data.identity,
      startdate: this.props.Moment
        ? this.props.Moment
            .unix(this.props.campaign.start_date)
            .format('DD-MM-YYYY')
        : '',
      enddate: this.props.Moment
        ? this.props.Moment
            .unix(this.props.campaign.end_date)
            .format('DD-MM-YYYY')
        : ''
    }
    var taskData=[];  
    if(this.props.campaigns.myPendingTask !== undefined && this.props.campaigns.myPendingTask.myPendingTaskData.length >0 ){
      taskData=this.props.campaigns.myPendingTask.myPendingTaskData;
    }
    var taskLoader=this.props.campaigns.myPendingTask !== undefined ?this.props.campaigns.myPendingTask.myPendingTaskLoading :false
    var contentLoader=this.props.campaigns.myPendingContent !== undefined ? this.props.campaigns.myPendingContent.myPendingContentLoading :false
    var contentData=[];
    if(this.props.campaigns.myPendingContent !== undefined && this.props.campaigns.myPendingContent.myPendingContentData.length >0 ){
      contentData=this.props.campaigns.myPendingContent.myPendingContentData;
    }

    var reviewContentLoading=this.props.campaigns.reviewContent !== undefined ? this.props.campaigns.reviewContent.reviewContentLoading : false
    var reviewContentData=[];
    if(this.props.campaigns.reviewContent !== undefined){
      reviewContentData =this.props.campaigns.reviewContent.reviewContentData
    }
    var me = this;
    let channelIcons=<div className='socialAccountsListing'>
    {social.map((social, i) => {
      return (
            <span key={i} className={`social ${social.media.toLowerCase()}`}>
              {social.media.toLowerCase() == 'internal'
                ? <i className='material-icons'>vpn_lock</i>
                : <i class={`fa fa-${social.media.toLowerCase()}`} />}
            </span>
      )
    })}
</div>

    return (
      <div className = "overview-wrapper clearfix">
        <div className = "leftPart-overview-wrapper overview-brief-wrapper">
        {/* loader */}
          {/* <div className = "overview-brief-wrapper-loader">
               <div className = "loader-grey-line loader-line-height loader-line-space loader-line-radius loader-assets-title"> </div>
               <div className = "loader-grey-line loader-line-height loader-line-space loader-line-radius loader-assets-title"> </div>
               <div className = "loader-grey-line loader-line-height loader-line-space loader-line-radius loader-assets-title"> </div>
           </div> */}
           {/* loader end */}
          <div className = "campaign-list">
              <div className = "heading-title-campaigns"> 
              DETAILS 
                  <ul class="dd-menu context-menu campaign_3dots">
                  {(this.props.roleName == 'admin' || this.props.roleName == 'super-admin' )?
                  <li class="button-dropdown"><a class="dropdown-toggle"><i class="material-icons">more_vert</i></a>
                      <ul class="dropdown-menu">
                        {this.props.location.pathname !== '/campaigns/terminated' ?
                          <li><a class="" onClick={this.handleEditCampaign.bind(this)}>Edit</a></li>:''}
                            {this.props.campaign.end_date > this.props.Moment().unix()?
                               <li>
                                 <a class="" onClick={this.terminateCampaign.bind(this)}>
                                  {this.props.campaign.terminated == 0
                                  ? 'Terminate'
                                  : 'Restart'}
                              </a></li>:''}
                        {this.props.location.pathname == '/campaigns/expired' ?
                          <li>
                            {this.props.roleName == 'admin' || this.props.roleName == 'super-admin'
                              ? typeof this.props.campaign !== 'undefined' &&
                                this.props.showPostCreationButton
                                ? <a
                                  className=''
                                  onClick={this.props.editCampaign.bind(this)}
                                >
                              Restart
                                </a>
                                : null
                              : ''}
                          </li> :''}
                      </ul>
                  </li>:''}
                </ul>
              </div>
              <div className = "campaign-details-wrapper">
              <DataRow label='Campaign Type' value={campaignDetail.campaignType} />
              <DataRow label='Channels' value={channelIcons} />
              <DataRow label='Title' value={campaignDetail.title} />
              {/* <DataRow label='Description' value={campaignDetail.description} valign='top' campaignDescription='true' campaignId={this.props.campaign.identity}/> */}
              {/* <DataRow label='Collaborators Permissions'value={campaignDetail.userPermissions}/> */}
              <DataRow label='Message' value={campaignDetail.message} />
              <DataRow label='Campaign Objective' value={campaignDetail.objective} />
              <DataRow label='Targeted Audience' value={campaignDetail.audience} />
              <DataRow label='Campaign Hashtag' value={campaignDetail.campaignHashTag} />
              <DataRow label='Start Date' value={campaignDetail.startdate} />
              <DataRow label='End Date' value={campaignDetail.enddate} />
              </div>
          </div>
        </div>
        <div className = "rightpart-overview-wrapper">
           {/* table */}
                   <div className="campaigntaskTableWrapper">
                        <div className = "heading-title-campaigns"> MY PENDING TASKS </div>
                   {taskLoader == true ? this.pendingTaskLoader():
                      taskData.length >0 ?
                        <table className="table responsive-table">
                            <thead>
                              <tr>
                                  <th>Title</th>
                                  <th>Status</th>
                                  <th>Due date</th>
                                  <th>Assigned to</th>
                                  <th>Created by</th>
                              </tr>
                            </thead>
                            <tbody>
                              {taskData.map((task, i) => {
                                return(
                                  <tr key={`taskData_`+i}>
                                      <td data-rwd-label="Title">{task.title}</td>
                                      <td data-rwd-label="Status">{task.status}</td>
                                      <td data-rwd-label="Due date" class="taskDate">{this.props.Moment? this.props.Moment.unix(task.due_date).format('DD-MM-YYYY'): ''}</td>
                                      <td data-rwd-label="Assigned to" class="taskAssignTo assign-wrapper-in-table">
                                        <span class="userThumb">
                                          <img
                                              src={task.assignee.avatar?task.assignee.avatar:'https://app.visibly.io/img/default-avatar.png'}
                                              alt={'Default avatar'}
                                              className='list-avatar'
                                              data-tip={task.assignee.email}
                                              data-for={task.assignee.identity}
                                              onError={e => {
                                                  e.target.src =
                                                      'https://app.visibly.io/img/default-avatar.png'
                                              }}
                                          />
                                          </span>
                                        {task.assignee.first_name + ' ' + task.assignee.last_name} </td>
                                      <td data-rwd-label="Created by" class="taskAssignTo">{task.created_by.first_name +' ' + task.created_by.last_name}</td>
                                  </tr>
                                )
                              })
                            }
                            </tbody>
                        </table>
                        :<div class="campaigntaskTableWrapper"><div class="no-data-block"> No task found. </div></div>
                   }
                   {taskLoader == true && taskData.length >0?
                        <div className ="viewMoreDetailsInCampaigns"><a class="seeMoreLink" onClick={this.fetchAllData.bind(this,'pendingTask')}>See More <i class="fa fa-angle-double-right"></i></a></div>
                    :''}
                      </div>
                      
             {/* end table */}
            <div className = "pendding-content-part-wrapper"> 
                 <div className = "heading-title-campaigns"> MY PENDING CONTENT </div>
                 <div className = "pendding-content-part-list"> 
                       {/* content */}
                       <div class="assetsList">
                              {contentLoader == true ? this.pendingContentLoader():
                              contentData.length >0 ?
                                <div class="archive-container clearfix">
                                  <div id="container">
                                  
                                    {contentData.map((content, i) => {
                                      let templatePreview = ''
                                      if(content.html_preview){
                                         templatePreview = decodeURIComponent(escape(atob(content.html_preview)));
                                      }else{
                                        let templateFirstScreen =  `<div class="first-pdf-page">
                                        <div class="pdf-wrapper">
                                            <div class="container">
                                                <div class="pdf-background">
                                                    <img src="https://magazine.visibly.io/images/pdf.png" alt="pdf-background" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="pdf-content">
                                            <div class="container">
                                                <div class="pdf-content-heading">
                                                    <h2>Admin message</h2>
                                                </div>
                                                <div class="content-paragraph">
                                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been
                                                        the
                                                        industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of
                                                        type
                                                        and scrambled it to make a type specimen book. It has survived not only five<br /><br /><br />

                                                        centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It
                                                        was
                                                        popularised in the 1960s with the release of Letraset sheets containing Lorem Curabitur
                                                        pellentesque ut risus auctor varius. Integer imperdiet viverra neque, vel pharetra lacus
                                                        sagittis at. Utvariusvel tortor quis sagit-tis. Donec risus nisi, hendrerit sed mollis quis,
                                                        semper ut ligula. Cras pulvinar molestie neque, nec dapibus diam tempus vitae. Fusce
                                                        consectetur, ex nec venenatis consequat, diam quam semper diam, id porta lacus ex et nulla. Cras
                                                        commodo, augue nec commodo cursus, mi justo mattis ante, vitae varius orci massa a eros. Sed
                                                        hendrerit euismod leo. Curabitur pellentesque ut risus auctor varius. Integer imperdiet viverra
                                                        neque, vel pharetra lacus sagittis at. Utvariusvel tortor quis sagit-tis. Donec risus nisi,
                                                        hendrerit sed mollis quis, semper ut ligula. Cras pulvinar molestie neque, nec dapibus diam
                                                        tempus vitae. Fusce consectetur, ex nec venenatis consequat, diam quam semper diam, id porta
                                                        lacus ex et nulla. Cras commodo, augue nec commodo cursus, mi justo mattis ante, vitae varius
                                                        orci massa a eros. Sed hendrerit euismod leo.</p>
                                                </div>
                                            </div>
                                        </div>
                                        </div> `;
                                        let  tempObj =btoa((unescape(encodeURIComponent(templateFirstScreen))));
                                        templatePreview = decodeURIComponent(escape(atob(tempObj)));
                                      }
                                      return(
                                        <div id={`asset-${content.identity}`} class="vid file-image asset-file imgwrapper" key={content.identity} onClick={this.openContentDetail.bind(this,content)}>
                                          <div class="inner_imgwrapper assetInnerWrapper">
                                            <a
                                              class="type-image"><span class="imageloader loaded thumbnail">
                                                {/* <ImageLoader
                                                  src={content.preview_image}
                                                  preloader={PreLoaderMaterial}
                                                  onLoad={this.imageLoaded} >
                                                  <div className="image-load-failed">
                                                    <i class="material-icons no-img-found-icon">&#xE001;</i>
                                                  </div>
                                                </ImageLoader> */}
                                                <div dangerouslySetInnerHTML={{__html: templatePreview}} className="inner-pdf-wrapper"></div>
                                              </span></a>
                                          </div>
                                          <a class="file-details">
                                            {content.type == 'print' ?
                                            <i class="material-icons">text_fields</i>
                                            :content.type == 'email' ?
                                            <i class="material-icons">mail_outline</i>
                                            :<i class="material-icons">mail_outline</i>}
                                            <span class="title">{content.title}</span>
                                          </a>
                                        </div>
                                      )
                                    }) 
                                  }
                                  </div>
                            </div>
                            :<div class="campaigntaskTableWrapper"><div class="no-data-block"> No content found. </div></div>
                              }
                          </div>
                       {/* end content */}
                 </div>
                 {contentLoader == true && contentData.length >0 ?
                  <div className ="viewMoreDetailsInCampaigns"><a class="seeMoreLink" onClick={this.fetchAllDataforPendingContent.bind(this,'pendingContent')}>See More <i class="fa fa-angle-double-right"></i></a></div>
                 : '' }
             </div>
             {this.props.roleName == 'admin' || this.props.roleName== "super-admin"?
             <div className = "pendding-content-part-wrapper"> 
                 <div className = "heading-title-campaigns"> Review Content </div>
                 <div className = "pendding-content-part-list"> 
                       {/* content */}
                       <div class="assetsList">
                              {reviewContentLoading == true ? this.pendingContentLoader():
                              reviewContentData.length >0 ?
                                <div class="archive-container clearfix">
                                  <div id="container">
                                  
                                    {reviewContentData.map((reviewData, i) => {
                                      let templatePreview = ''
                                      if(reviewData.html_preview){
                                         templatePreview = decodeURIComponent(escape(atob(reviewData.html_preview)));
                                      }else{
                                        let templateFirstScreen =  `<div class="first-pdf-page">
                                        <div class="pdf-wrapper">
                                            <div class="container">
                                                <div class="pdf-background">
                                                    <img src="https://magazine.visibly.io/images/pdf.png" alt="pdf-background" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="pdf-content">
                                            <div class="container">
                                                <div class="pdf-content-heading">
                                                    <h2>Admin message</h2>
                                                </div>
                                                <div class="content-paragraph">
                                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been
                                                        the
                                                        industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of
                                                        type
                                                        and scrambled it to make a type specimen book. It has survived not only five<br /><br /><br />

                                                        centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It
                                                        was
                                                        popularised in the 1960s with the release of Letraset sheets containing Lorem Curabitur
                                                        pellentesque ut risus auctor varius. Integer imperdiet viverra neque, vel pharetra lacus
                                                        sagittis at. Utvariusvel tortor quis sagit-tis. Donec risus nisi, hendrerit sed mollis quis,
                                                        semper ut ligula. Cras pulvinar molestie neque, nec dapibus diam tempus vitae. Fusce
                                                        consectetur, ex nec venenatis consequat, diam quam semper diam, id porta lacus ex et nulla. Cras
                                                        commodo, augue nec commodo cursus, mi justo mattis ante, vitae varius orci massa a eros. Sed
                                                        hendrerit euismod leo. Curabitur pellentesque ut risus auctor varius. Integer imperdiet viverra
                                                        neque, vel pharetra lacus sagittis at. Utvariusvel tortor quis sagit-tis. Donec risus nisi,
                                                        hendrerit sed mollis quis, semper ut ligula. Cras pulvinar molestie neque, nec dapibus diam
                                                        tempus vitae. Fusce consectetur, ex nec venenatis consequat, diam quam semper diam, id porta
                                                        lacus ex et nulla. Cras commodo, augue nec commodo cursus, mi justo mattis ante, vitae varius
                                                        orci massa a eros. Sed hendrerit euismod leo.</p>
                                                </div>
                                            </div>
                                        </div>
                                        </div> `;
                                        let  tempObj =btoa((unescape(encodeURIComponent(templateFirstScreen))));
                                        templatePreview = decodeURIComponent(escape(atob(tempObj)));
                                      }
                                      return(
                                        <div id={`asset-${reviewData.identity}`} class="vid file-image asset-file imgwrapper" key={reviewData.identity} onClick={this.openContentDetail.bind(this,reviewData)}>
                                          <div class="inner_imgwrapper assetInnerWrapper ">
                                            <a
                                              class="type-image">
                                              <div class="imageloader loaded thumbnail">
                                                {/* <ImageLoader
                                                  src={reviewData.preview_image}
                                                  preloader={PreLoaderMaterial}
                                                  onLoad={this.imageLoaded} >
                                                  <div className="image-load-failed">
                                                    <i class="material-icons no-img-found-icon">&#xE001;</i>
                                                  </div>
                                                </ImageLoader> */}
                                                <div dangerouslySetInnerHTML={{__html: templatePreview}} className="inner-pdf-wrapper">
                                                </div>
                                              </div>
                                              </a>
                                          </div>
                                          <a class="file-details">
                                            {reviewData.type == 'print' ?
                                            <i class="material-icons">text_fields</i>
                                            :reviewData.type == 'email' ?
                                            <i class="material-icons">mail_outline</i>
                                            :<i class="material-icons">mail_outline</i>}
                                            <span class="title">{reviewData.title}</span>
                                          </a>
                                        </div>
                                      )
                                    }) 
                                  }
                                  </div>
                            </div>
                            :<div class="campaigntaskTableWrapper"><div class="no-data-block"> No review content found. </div></div>
                              }
                          </div>
                       {/* end content */}
                 </div>
                 {reviewContentLoading == false && reviewContentData.length >0 ?
                      <div className ="viewMoreDetailsInCampaigns"><a class="seeMoreLink" onClick={this.fetchAllData.bind(this,'reviewContent')}>See More <i class="fa fa-angle-double-right"></i></a></div>
                  :''}
             </div>
             :''}
         </div>
      </div>
    )
  }
  render () {
    return (
      <section className='asset-detail-page'>

        <div id='asset-detail'>
            {this.campaignDataDetail()}
        </div>

      </section>
    )
  }
}

module.exports = CampaignDetail
