import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import BigCalendar from "react-big-calendar";
import Dnd from "../calendarViews/calendarDragAndDrop";
import Globals from "../../Globals";
import notie from "notie/dist/notie.js";
import CollapsibleFilter from "../CollapsibleFilter";
import * as campaignActions from "../../actions/campaigns/campaignsActions";
import CalendarViewButtons from "../Header/CalendarViewButtons";
import SliderPopup from "../SliderPopup";
import Slider from "../Chunks/ChunkReactSlick";
import ChangeScheduleForm from "../Feed/ChangeScheduleForm";
import CalendarButtons from "../Header/CalendarButtons";
import ProjectPopupModal from './ProjectPopupModal';
import FeedEvents from "../Feed/FeedEvents";
import CampaignPlanner from "./CampaignPlanner";
import * as utils from "../../utils/utils";
import PopupWrapper from "../PopupWrapper";
import Post from "../Feed/Post";
import { renderField } from '../Helpers/ReduxForm/RenderField';
import { Field, reduxForm } from 'redux-form';
import CampaignTaskDetail from './CampaignTaskDetail'
import CampaignCreateTasks from "./CampaignCreateTasks";
var _current_page;
var rescheduled_post_id;
var rescheduled_post_date;
var firstDateTimestampParent, lastDateTimestampParent;
var taskId ={};
class ScheduledCalendar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      cal_view: "month",
      limit: 10,
      currView: "calendar",
      currFeedPosts: "scheduled",
      changeSchedulePopup: false,
      changeScheduleFeed: null,
      changeScheduleFeedType: null,
      changeSchedulePostData: [],
      Slider: null,
      statusFilter:[],
      userFilter:[],
      timeFilter:[],
      typeFilter:'all',
      userSelected: 'All user',
      taskDetail:false,
      leftSidebarShown:false ,
      listFilter : null,
    };
    this._handleCloseEvent = this._handleCloseEvent.bind(this);
    this.fetchTimeStamps = this.fetchTimeStamps.bind(this);
  }

  fetchTimeStamps(firstDateTimestamp, lastDateTimestamp) {
    firstDateTimestampParent = firstDateTimestamp;
    lastDateTimestampParent = lastDateTimestamp;
    var path = utils
      .removeTrailingSlash(this.props.location.pathname)
      .split("/");
    this.fetchPosts(path[2], path[3], 1, firstDateTimestamp, lastDateTimestamp);
  }

  closepostDetailPopup() {
    this.setState({
      show_calendar_feed_post_state: false
    });
  }

  fetchPosts(type, postTypes, currPage, firstDateTimestamp, lastDateTimestamp) {
    _current_page = currPage;
    this.props.campaignActions.fetchCampaignPosts(
      this.props.campaign.identity,
      this.state.currView,
      this.state.moment
    );
  }
  toggleLeftSidebar(){
    this.setState({
      leftSidebarShown:!this.state.leftSidebarShown
    })
  }
  callCalPostDetails(event, e) {
    var week = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
    var calendar_popup = {
      position: {
        vertical: "top",
        horizontal: "left",
        left: 0,
        right: 0,
        top: 0,
        bottom: 0
      },
      grid: {
        row_number: 0,
        column_number: 0
      },
      event
    };
    var rbc_month_row = document.querySelectorAll(".rbc-month-row")[0];
    var rbc_day_bg = document.querySelectorAll(".rbc-day-bg")[0];
    var selected_row = utils.closest(e.target, "rbc-month-row");
    var selected_row_cells;
    if (selected_row) {
      calendar_popup.grid.row_number = utils.index(selected_row);
      selected_row_cells = selected_row.querySelectorAll(".rbc-date-cell");
    }
    var selected_cell = document.querySelectorAll(".selected_cell")[0];

    if (this.state.cal_view == "month") {
      var calendar_table = {
        border: 2,
        margin_bottom: parseInt(
          window.getComputedStyle(rbc_month_row)["margin-bottom"]
        )
      };
      var cell = {
        height:
          rbc_day_bg.offsetHeight +
          calendar_table.border +
          calendar_table.margin_bottom,
        width: rbc_day_bg.getBoundingClientRect().width
      };
      var selected_date_var = new Date(event.start);
      var selected_date = {
        date: selected_date_var.getDate(),
        month: selected_date_var.getMonth() + 1
      };

      if (utils.closest(e.target, "rbc-overlay")) {
        calendar_popup.grid.column_number = parseInt(
          selected_cell.getAttribute("data-column-number")
        );
        calendar_popup.grid.row_number =
          parseInt(selected_cell.getAttribute("data-row-number")) + 1;
      } else {
        Array.prototype.forEach.call(selected_row_cells, function(
          cell_el,
          cell_index
        ) {
          if (cell_el.textContent == selected_date.date) {
            calendar_popup.grid.column_number = cell_index;
          }
        });
      }
      calendar_popup.position.left =
        (calendar_popup.grid.column_number + 1) * cell.width;
      calendar_popup.position.top =
        calendar_popup.grid.row_number * cell.height;
      if (calendar_popup.grid.column_number > 3) {
        calendar_popup.position.horizontal = "right";
        calendar_popup.position.right =
          cell.width * (7 - calendar_popup.grid.column_number);
      }
      if (calendar_popup.grid.row_number > 3) {
        calendar_popup.position.vertical = "bottom";
        calendar_popup.position.bottom =
          cell.height * (5 - calendar_popup.grid.row_number);
      }
    } else {
      // Week view
      var calendar_table = {
        border: 2,
        timeslot_width: document.querySelectorAll(".rbc-time-column")[0]
          .offsetWidth,
        rbc_header: document.querySelectorAll(".rbc-header")[0].offsetHeight
      };
      var cell = {
        height: document.querySelectorAll(".rbc-timeslot-group")[0]
          .offsetHeight,
        width: document
          .querySelectorAll(".rbc-header")[0]
          .getBoundingClientRect().width
      };

      selected_date = {
        date: 1,
        time: null,
        full_date: new Date(event.start)
      };

      var selected_day = week[selected_date.full_date.getDay()];
      var formated_date = this.formatTime(selected_date.full_date.getDate());
      var formated_month = this.formatTime(
        selected_date.full_date.getMonth() + 1
      );

      selected_date.date = `${selected_day} ${formated_date}/${formated_month}`;
      var hours = selected_date.full_date.getHours();
      var minutes = 0;
      var formated_minutes = this.formatTime(minutes);
      var meridiem = "PM";
      if (hours == 0) {
        hours = 12;
        meridiem = "AM";
      } else if (hours < 12) {
        meridiem = "AM";
      } else if (hours > 12) {
        hours = hours - 12;
      }
      selected_date.time = hours + ":" + formated_minutes + " " + meridiem;

      var rbc_timeslot_group = document.querySelectorAll(".rbc-timeslot-group");
      var rbc_header = document.querySelectorAll("#weekHeader .rbc-header");

      Array.prototype.forEach.call(rbc_timeslot_group, function(
        timeslot_el,
        timeslot_index
      ) {
        if (timeslot_el.textContent == selected_date.time) {
          calendar_popup.grid.row_number = timeslot_index;
        }
      });

      if (calendar_popup.grid.row_number > 15) {
        calendar_popup.position.vertical = "bottom";
        calendar_popup.position.bottom =
          cell.height * (23 - calendar_popup.grid.row_number);
      }
      Array.prototype.forEach.call(rbc_header, function(
        rbc_header_el,
        rbc_header_index
      ) {
        if (rbc_header_el.textContent == selected_date.date) {
          calendar_popup.grid.column_number = rbc_header_index + 1;
        }
      });
      if (calendar_popup.grid.column_number > 4) {
        calendar_popup.position.horizontal = "right";
        calendar_popup.position.right =
          cell.width * (8 - calendar_popup.grid.column_number);
      }
      calendar_popup.position.left =
        calendar_popup.grid.column_number * cell.width +
        calendar_table.timeslot_width;
      calendar_popup.position.top =
        calendar_popup.grid.row_number * cell.height +
        calendar_table.rbc_header;
    }
    this.setState({
      show_calendar_feed_post_state: true,
      calendar_feed_detail: {
        calendar_popup
      }
    });
    if(event.type=="task"){
      this.openDetailPopup(event.feed_post)
    }
  }

  formatTime(time) {
    var myNumber = time;
    return ("0" + myNumber).slice(-2);
  }

  _handleCloseEvent(e) {
    if (document.getElementById("pop-up-tooltip-wrapper")) {
      //hide notification popup on outside click
      if (
        !document.getElementById("pop-up-tooltip-wrapper").contains(e.target)
      ) {
        if (this.state.overlay_flag) {
          document.getElementById("pop-up-tooltip-wrapper").className = "hide";
          this.setState({ overlay_flag: false });
        }
      }
    }
    if (utils.closest(e.target, "feed-post-item") == null) {
      //hide calender post popup on outside click
      if (
        document.querySelectorAll(".bigCalendar-calendarview #close-popup")
          .length != 0
      ) {
        document
          .querySelectorAll(".bigCalendar-calendarview #close-popup")[0]
          .click();
      }
    }
  }

  initilizeClassToCell() {
    var month_rows = document.querySelectorAll(".rbc-month-row");
    Array.prototype.forEach.call(month_rows, function(month_el, month_index) {
      var date_cols = month_el.querySelectorAll(".rbc-day-bg");
      Array.prototype.forEach.call(date_cols, function(date_el, date_index) {
        date_el.setAttribute("data-row-number", month_index);
        date_el.setAttribute("data-column-number", date_index);
      });
    });
  }

  setView(cal_view) {
    this.setState({
      cal_view
    });
  }
  componentWillMount() {
    Slider().then(slider => {
      this.setState({ Slider: slider });
    });
  }

  show_calendar_feed_post(data) {
    var data = this.state.calendar_feed_detail.calendar_popup;
    var row_number = data.grid.row_number;
    let style = {
      left: data.position.left,
      right: data.position.right,
      top: data.position.top,
      bottom: data.position.bottom
    };
    if(data.event.type=="post"){
      return(
        <div
          className={`calendar-detail-popup popup_row_${row_number} popup_row_${data
            .position.vertical} popup_column_${data.position.horizontal}`}
          style={style}
        >
          <button
            id="close-popup"
            className="btn-default"
            onClick={this.closepostDetailPopup.bind(this)}
          >
            <i className="material-icons">clear</i>
          </button>
          <Post
            type={data.event.type}
            Slider={this.state.Slider}
            cal_view={true}
            feed={this.state.currFeed}
            feedType={this.state.currFeedPosts}
            locationInfo={this.props.location}
            details={data.event.feed_post}
            customScrollbar={true}
            cal_Date={data.event.end}
            handleCloseEvent={this._handleCloseEvent.bind()}
          />
        </div>
      )
    }else if(data.event.type=="print" || data.event.type=="email"){
      return(
        <ProjectPopupModal 
          print={data.event.feed_post}
          roleName={this.props.roleName}
          data = {data}
          moment = {this.props.moment}
          closepostDetailPopup = {this.closepostDetailPopup.bind(this)}
          handleEditCopy={this.handleEditCopy.bind(this)}
          handleDelete = {this.handleDelete.bind(this)}
        />
      )
    }
  }
  handleEditCopy(type,identity,action){
    let campId = this.props.campaign.identity;
    if(action=="edit"){
      this.props.handleEditPlanner();
      this.props.campaignActions.getContentProjectById(type,identity,campId,action);
    }else if(action=="copy"){
      this.props.campaignActions.getContentProjectById(type,identity,campId,action);
    }
  }

  handleDelete(type,identity){
    let campId = this.props.campaign.identity;
    let me = this;
      me.props.campaignActions.deleteContentProject(type,identity,campId,me.props.moment);
  }

  componentWillReceiveProps(nextProps) {

    if(this.props.campaigns.deleteProjectLoading!==nextProps.campaigns.deleteProjectLoading && nextProps.campaigns.deleteProjectLoading==false){
      this.fetchFilterDataFromAPI();
    }
    if (
      this.props.posts.creation.created !== nextProps.posts.creation.created &&
      nextProps.posts.creation.created == true &&
      nextProps.posts.creation.campaignId
    ) {
      // this.props.fetchPostsData(
      //   nextProps.posts.creation.campaignId,
      //   this.state.currView
      // );
      this.fetchFilterDataFromAPI();
    }
    if (
      nextProps.campaigns.updation.updated !==
        this.props.campaigns.updation.updated &&
      nextProps.campaigns.updation.updated !== false
    ) {
      this.fetchFilterDataFromAPI();
      this.closechangeSchedulePopup();
    }
    this.initilizeClassToCell();
    var path = utils
      .removeTrailingSlash(this.props.location.pathname)
      .split("/");
    this.setState({ currFeedPosts: "scheduled", currPage: 1 });
    if(nextProps.campaigns.addTaskLoading!==this.props.campaigns.addTaskLoading && nextProps.campaigns.addTaskLoading!==true){  
     this.closeCreateTaskPopup();
     if(this.state.editTask==true){
       this.setState({
         editTask:false
       })
     }
    //  this.fetchFilterDataFromAPI();
   }
   if(this.props.campaigns.deleteTaskLoading!== nextProps.campaigns.deleteTaskLoading){
     if(nextProps.campaigns.deleteTaskLoading!==true){
       this.closeDetailPopup();
       document.body.classList.remove('overlay');
       this.setState({
         editTask:false,
       })
     }
   }
  //  this.fetchFilterDataFromAPI();
  }

  move_event({ event, start, end }) {
    if (
      event.type=="post" &&
      event.feed_post.post.data.postOwner.data.identity ==
      this.props.profile.identity 
    ) {
      var me = this;
      //if calendar view is month assign current time of event to new schedule post when it will be dragged
      if (me.state.cal_view == "month") {
        var oldstartDate = new Date(event.start);
        var oldStartHours = oldstartDate.getHours();
        var oldStartMinutes = oldstartDate.getMinutes();
        var oldStartSeconds = oldstartDate.getSeconds();

        var newstartDate = new Date(start);
        var updateStartDate = newstartDate.setHours(oldStartHours);
        updateStartDate = newstartDate.setMinutes(oldStartMinutes);
        updateStartDate = newstartDate.setSeconds(oldStartSeconds);
        updateStartDate = new Date(newstartDate);
        start = updateStartDate;
        end = updateStartDate;
      }
      var path = utils
        .removeTrailingSlash(me.props.location.pathname)
        .split("/");
      var time_message;
      const now = new Date();
      var moment = me.props.moment;

      let tomorrow, newSchedule;
      tomorrow = moment(start).add(0, "days");
      newSchedule = String(start.getTime() / 1000);
      rescheduled_post_date = start;
      var current_date = moment.tz(now, Globals.SERVER_TZ).format("X");
      let selected_date = moment.tz(start, Globals.SERVER_TZ).format("X");
      let selected_date_second = moment
        .tz(start, Globals.SERVER_TZ)
        .format("x");
      var current_date_second = moment.tz(now, Globals.SERVER_TZ).format("x");
      // to reduce API call if the new schedule is same as old schedule
      if (
        newSchedule >= current_date &&
        newSchedule !== event.feed_post.scheduled_at.toString()
      ) {
        if (selected_date_second > current_date_second) {
          const events_post = this.props.campaigns.plannerData.calendarPosts;
          rescheduled_post_id = event.feed_post.publisher_identity;
          const idx = events_post.indexOf(event);
          const updatedEvent = { ...event, start, end };
          const nextEvents = [...events_post];
          nextEvents.splice(idx, 1, updatedEvent);
          me.setState({
            events_post: nextEvents,
            event_change: true
          });

          this.props.campaignActions.campaignPostUpdateInit();
          if (this.props.campaigns.updation.loading) {
            me.setState({
              update_post_loader: true
            });
          } else {
            me.setState({
              update_post_loader: false
            });
          }

          let fieldValues = {
            publisher_identity: event.feed_post.publisher_identity,
            scheduled_at: newSchedule
          };
          this.props.campaignActions.campaignPostUpdate(
            fieldValues,
            true,
            this.props.moment,
            this.props.campaign.identity,
            this.state.currView
          );
          if (document.querySelectorAll(".initializeStyle").length > 0) {
            document.querySelectorAll(".initializeStyle")[0].click();
          }
        } else {
          if (this.state.cal_view == "month") {
            time_message =
              "You cannot schedule a post for time that has passed. Please go to week view for more.";
          } else {
            time_message =
              "You cannot schedule a post for time that has passed.";
          }
          notie.alert("error", time_message, 5);
        }
      } else if (newSchedule < current_date) {
        notie.alert(
          "error",
          "You cannot schedule a post for date that has passed.",
          5
        );
      }
    } else {
      return;
    }
  }

  changeHeaderView(value) {
    this.setState({
      currView: value
    });
  }

  changeScheduleAction(feed, feedType, postData) {
    this.props.campaignActions.getAllSchedules(postData.post.data.identity);
    this.setState({
      changeSchedulePopup: true,
      // changeScheduleFeed: feed,
      // changeScheduleFeedType: feedType,
      changeSchedulePostData: postData
    });
    document.body.classList.add("overlay");
  }

  renderChangeSchedule() {
    return (
      <SliderPopup wide>
        <button
          id="close-popup"
          className="btn-default"
          onClick={this.closechangeSchedulePopup.bind(this)}
        >
          <i className="material-icons">clear</i>
        </button>
        <header className="heading">
          <h3>Reschedule post</h3>
        </header>
        <PopupWrapper>
          <div className="schedulePostPopupColumnWrapper clearfix">
            <div className="PostDetailColumn">
              <Post
                //  feed={this.state.changeScheduleFeed}
                //  feedType={this.state.changeScheduleFeedType}
                Slider={this.state.Slider}
                fromCalendarSchedule={true}
                details={this.state.changeSchedulePostData}
                updateText={this.updateText.bind(this)}
                key={0}
              />
            </div>
            <div className="postScheduleData">
              <ChangeScheduleForm
                props={this.props}
                postId={this.state.changeSchedulePostData.post_id}
                scheduleDates={this.props.campaigns.scheduleDates}
                onSubmit={this.handleScheduleSubmit.bind(this)}
                momentVar={this.props.moment}
              />
            </div>
          </div>
        </PopupWrapper>
      </SliderPopup>
    );
  }

  handleScheduleSubmit(values) {
    let schedulers = [];
    let output = [];
    var counts = {};
    let currentTime = this.props.moment().format("HH:mm");
    let currentDate = this.props.moment().format("DD/MM/YYYY");

    for (let sc = 0; sc < values.allScheduleData.length; sc++) {
      let time = this.props.moment
        .unix(values.allScheduleData[sc].scheduled_at)
        .format("HH:mm");
      let date = this.props.moment
        .unix(values.allScheduleData[sc].scheduled_at)
        .format("DD/MM/YYYY");

      if (
        values[`scheduledTime-${sc}`] !== undefined &&
        values[`scheduledTime-${sc}`] !== null
      ) {
        //new schedule is added in list then to get its time , if schedule is removed then it will not come here
        time = this.props.moment(values[`scheduledTime-${sc}`]).format("HH:mm");
      }
      if (
        values[`scheduledDate-${sc}`] !== undefined &&
        values[`scheduledDate-${sc}`] !== null
      ) {
        //new schedule is added in list then to get its date , if schedule is removed then it will not come here
        date = this.props.moment
          .unix(values[`scheduledDate-${sc}`])
          .format("DD/MM/YYYY");
      }

      let concatenatedDateTime = date + " " + time;
      let timeStamp = this.props
        .moment(concatenatedDateTime, "DD/MM/YYYY HH:mm")
        .format("X");

      schedulers.forEach(function(x) {
        counts[x] = (counts[x] || 0) + 1;
      });

      if (currentDate == date) {
        //if current date is match with scheduled date and current time is matched with schedule time then to resrtict post
        if (currentTime > time) {
          notie.alert("error", "Sorry, Your scheduled time is left.", 5);
          return;
        }
      }

      if (!schedulers.includes(parseInt(timeStamp))) {
        //to restrict pervious date
        var scheduler_date = this.props.moment
          .unix(parseInt(timeStamp))
          .format("MM-DD-YYYY");
        var today = this.props.moment().unix();
        var today_date = this.props.moment.unix(today).format("MM-DD-YYYY");
        var date1 = new Date(scheduler_date.toString());
        var date2 = new Date(today_date.toString());
        var timeDiff = date2.getTime() - date1.getTime();
        var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
        if (diffDays > 0) {
          notie.alert(
            "error",
            "You can not schedule a post in past. Please select a future time.",
            5
          );
          return;
        } else {
          schedulers.push(parseInt(timeStamp));
        }
      } else {
        notie.alert("error", "You can not select same date for schedule.", 5);
        return;
      }
    }
    var field_values_array = [];
    for (let sc = 0; sc < schedulers.length; sc++) {
      if (values.allScheduleData[sc].SocialAccount !== undefined) {
        values.allScheduleData[sc].SocialAccount.data.forEach(function(
          event_post
        ) {
          if (event_post.length !== 0) {
            var field_array_object = {
              social_account_identity: event_post.SocialAccount.data.identity,
              scheduled_at: schedulers[sc]
            };
          } else {
            var field_array_object = {
              social_account_identity: 0,
              scheduled_at: schedulers[sc]
            };
          }
          field_values_array.push(field_array_object);
        });
      } else {
        var field_array_object = {
          social_account_identity: 0,
          scheduled_at: schedulers[sc]
        };
        field_values_array.push(field_array_object);
      }
    }
    let objToSend = {
      post_identity: values.postid,
      schedule: field_values_array
    };
    this.props.campaignActions.campaignPostUpdate(
      objToSend,
      false,
      this.props.moment,
      this.props.campaign.identity,
      this.state.currView
    );
  }

  updateText(id, type, e) {
    this.setState({ overlay_flag: true });
    var isUserFound = false;
    if (type == "department") {
      this.props.departments.list.length > 0
        ? this.props.departments.list.map((DepartmentDetail, i) => {
            if (DepartmentDetail.identity == id) {
              selectedDepartmentData = DepartmentDetail;
            }
          })
        : "";
    } else {
      this.props.usersList.length > 0
        ? this.props.usersList.map((userDetail, i) => {
            if (userDetail.identity == id) {
              selectedUserData = userDetail;
              isUserFound = true;
            }
          })
        : "";
      if (isUserFound == false) {
        selectedUserData = "No user found";
      }
    }
    const domNode = ReactDOM.findDOMNode(e.target);
    let coords = domNode.getBoundingClientRect();
    let coords_top = coords.top + pageYOffset;
    let popup_wrapper = document.getElementById("pop-up-tooltip-holder");
    popup_wrapper.classList.remove("hide", "showtotop");

    let coords_left = 0,
      coords_area = 0;

    if (screen.width >= 768) {
      coords_left = coords.left + pageXOffset + coords.width / 2;
    } else {
      coords_left = coords.left + pageXOffset;
      coords_area = coords_left + 472;
      if (coords_area < screen.width) {
        popup_wrapper.classList.add("popup-left");
      } else {
        popup_wrapper.classList.add("popup-right");
      }
    }
    if (e.nativeEvent.screenY < 400) {
      if (e.target.className == "thumbnail") {
        popup_wrapper.classList.add("thumbnai_popup");
      }
    } else {
      popup_wrapper.classList.add("showtotop");
    }

    this.setState({
      popupTop: coords_top,
      popupLeft: coords_left,
      popupdisplay: "block",
      tagType: type,
      tagTypeClass: type === "department" ? "departmentpopup" : ""
    });
  }

  closechangeSchedulePopup() {
    this.setState({
      changeSchedulePopup: false
    });
    document.body.classList.remove("overlay");
  }

  handleFilter(type,value,e){
    this.setState({
      currentTab:null,
      leftSidebarShown:false
    })
    switch(type){
      case "type": 

      if(e.target.value!==''){
        if(e.target.value == 'task'){
          this.setState({
            timeFilter:[]
          })
        }
          this.setState({
              typeFilter :  e.target.value
          },()=>{
           this.fetchFilterDataFromAPI()
          })
      }else{
          this.setState({
              typeFilter:[]
          },()=>{
           this.fetchFilterDataFromAPI()
          })
      }
      break;
        case "status":
                if(e.target.checked==true){
                    var tempArray = this.state.statusFilter;
                    tempArray.push(value);
                    this.setState({
                        statusFilter :  tempArray,
                    },()=>{
                       this.fetchFilterDataFromAPI()
                    })
                }else{
                    var tempArray = this.state.statusFilter;
                    tempArray = tempArray.filter(function(data, index, arr){
                        return data !== value;
                    });
                    
                    this.setState({
                        statusFilter :  tempArray
                    },()=>{
                       this.fetchFilterDataFromAPI()
                    })
                }
                break;
        case "time": 

                if(e.target.value!==''){
                    this.setState({
                        timeFilter :  e.target.value
                    },()=>{
                       this.fetchFilterDataFromAPI()
                    })
                }else{
                    this.setState({
                        timeFilter:[]
                    },()=>{
                        this.fetchFilterDataFromAPI()
                    })
                }
                break;
        case "user":
                if(e.target.checked==true){
                    var tempArray = this.state.userFilter;
                    tempArray.push(value);
                    let userCount = this.state.userSelected == 'All user' ? 0 : this.state.userSelected;
                    this.setState({
                        userFilter :  tempArray,
                        userSelected: userCount + 1
                    },()=>{
                       this.fetchFilterDataFromAPI()
                    })
                }else{
                    var tempArray = this.state.userFilter;
                    tempArray = tempArray.filter(function(data, index, arr){

                        return data !== value;
                    
                    });
                    let userCount = this.state.userSelected - 1;
                    this.setState({
                        userFilter :  tempArray,
                        userSelected: userCount == 0 ? 'All user' : userCount
                    },()=>{
                       this.fetchFilterDataFromAPI()
                    })
                }
                break;
                case 'listFilter':
                    if(e.target.value!==''){
                        this.setState({
                            listFilter :  e.target.value
                        },()=>{
                          this.fetchFilterDataFromAPI()
                        })
                    }else{
                        this.setState({
                            listFilter:null
                        },()=>{
                          this.fetchFilterDataFromAPI()
                        })
                    }
                    break;
    }
}
fetchFilterDataFromAPI(){
  var filterData ={
    status: this.state.statusFilter,
    user:this.state.userFilter,
    creation_time:this.state.timeFilter,
    type:this.state.typeFilter,
    listFilter : this.state.listFilter,
  }
  var methodName='';
  if(this.state.typeFilter =='post'){
    methodName='fetchPlannerPosts'
  }else if(this.state.typeFilter =='email'){
    methodName='fetchPlannerEmails'
  }else if(this.state.typeFilter =='print'){
    methodName='fetchPlannerPrints'
  }else if(this.state.typeFilter=='projects'){
   methodName='fetchPlannerProjects'
  }else if(this.state.typeFilter=='all'){
   methodName='fetchPlannerProjects'
  }
 if(this.state.typeFilter =='task'){
   const callFromPlanner = true
  this.props.campaignActions.fetchTasks(this.props.campaign.identity,null,this.props.roleName,this.props.profile.identity,filterData,null,callFromPlanner,this.props.Moment);  
 }else{
  const callFromPlanner = true
   this.props.campaignActions[methodName](this.props.campaign.identity,this.props.Moment,this.state.currentTab,filterData,true,this.props.location,this.props.roleName,this.props.profile.identity,callFromPlanner);  
 }
}

openDetailPopup(value){
  taskId= value;
  this.setState({
    taskDetail:true,
    currTask:value
  })
  document.body.classList.add('overlay');
}
taskDetailPopup(currTask = null){
  const currTaskData =Object.keys(taskId).length>0? this.state.currTask :currTask
  return(
    <SliderPopup className="task-create-popup">
    <header class="heading clearfix">
        <h3>Task detail</h3>
        <button class="btn-default close-popup" onClick={this.closeDetailPopup.bind(this)}><i class="material-icons" >clear</i></button>
       </header>
    <CampaignTaskDetail 
          Moment={this.props.Moment} 
          currTask={currTaskData} 
          roleName={this.props.roleName} 
          editTask={this.editTask.bind(this)} 
          deleteTask={this.deleteTask.bind(this)}/>
    </SliderPopup>
  )
}
closeDetailPopup(){
  taskId= {}
  document.body.classList.remove('overlay');
  this.setState({
    taskDetail:false
  })
}
editTask(){
  this.closeDetailPopup();
  document.body.classList.add('overlay');
  this.setState({
    editTask:true,
  })
}
openEditTaskPopup(){
  return(
    <SliderPopup className="task-create-popup">
    <header class="heading clearfix">
        <h3>Edit Task</h3>
        <button class="btn-default close-popup" onClick={this.closeCreateTaskPopup.bind(this)}><i class="material-icons" >clear</i></button>
       </header>
    <CampaignCreateTasks
      onSubmit={this.submitTaskData.bind(this)}
      Moment={this.props.Moment} 
      currTask={this.state.currTask}
      campaignData ={this.props.campaign}
      profile={this.props.profile}
      roleName={this.props.roleName}
    />
</SliderPopup>
)
}
closeCreateTaskPopup(){
  document.body.classList.remove('change-request-task-popup');
  this.props.handleChangeStatus("close");
  document.body.classList.remove('overlay');
  this.setState({
    createTasks:false,
    taskDetail:false,
    editTask:false
  })
  taskId= {}
}
submitTaskData(values){
  var dueTime = this.props
       .Moment(values[`scheduledDueTime`])
       .format("HH:mm");
   var dueDate = this.props
       .Moment
       .unix(values[`scheduledDueDate`])
       .format("DD/MM/YYYY");
   let DueDateTime = dueDate + " " + dueTime;
   let DueDateTimeStamp =  this.props
       .Moment(DueDateTime, 'DD/MM/YYYY HH:mm')
       .format("X");
       var objTosend={
         title : values.title,
         AssignedToIdentity: values.assignTo,
         DueDateTime : DueDateTimeStamp,
         status : values.status
       }
       if(values.notes!==undefined){
         var obj={
           notes:values.notes
         }
         Object.assign(objTosend,obj);
       }
       if(this.props.currProjectId){
         Object.assign(objTosend,{content_project_id:this.props.currProjectId})
       }
       var campId = this.props.campaign.identity;
       if(values.identity!==undefined){
         var taskId =values.identity;
         // edit task
         this.props.campaignActions.addTaskData(objTosend,campId,this.props.roleName,taskId,null,true,this.props.Moment);
         this.closeDetailPopup();
       }else{
         // add task
         this.props.campaignActions.addTaskData(objTosend,campId,this.props.roleName);
         this.closeDetailPopup();
       }
}
deleteTask(id){
  var campId = this.props.campaign.identity;
  this.props.campaignActions.deleteTask(id,campId,"openTask",this.props.roleName,this.props.profile.identity,null,true,this.props.Moment);
}
fetchTask(type){
  if(this.state.currentTab!==type){
      var contentProjectId='';
      if(this.props.callFrom !== undefined && this.props.callFrom == 'contentProject'){
          contentProjectId=this.props.contentProjectData !== undefined ? this.props.contentProjectData.project_identity :'';   
      }
      this.setState({
        currentTab :type,
        statusFilter : [],
        userFilter : [],
        timeFilter : [],
        listFilter : null,
        userSelected: 'All user'
      },()=>{ 
        this.unSelectAllCheckBoxes('allStatusCheckboxes')
        this.unSelectAllCheckBoxes('checknotify')
      })
      this.props.change('timeFilter','')
      this.props.change('usertag',false)
      this.props.change('listFilter',false)
      this.props.change('Pending',false)
      this.props.change('inprogress',false)
      this.props.change('completed',false)
      this.props.change('approved',false)
      this.props.change('unapproved',false)
      const callFromPlanner = true
      this.props.campaignActions.fetchTasks(this.props.campaign.identity,type,this.props.roleName,this.props.profile.identity,null,contentProjectId,callFromPlanner,this.props.Moment);
  }
}
  unSelectAllCheckBoxes(CheckboxName){
  var items = document.getElementsByClassName(CheckboxName);
        for (var i = 0; i < items.length; i++) {
            if (items[i].type == 'checkbox')
                items[i].checked = false;
        }
  }
  render() {
    // var posts = this.props.campaignPosts;
    const {calendarPosts} = this.props.campaigns.plannerData;
    const {posts} = this.props.campaigns.plannerData;
    const {calendarPrints} = this.props.campaigns.plannerData;
    const {calendarEmails }= this.props.campaigns.plannerData;
    const {calendarSMS }= this.props.campaigns.plannerData;
    const calendarTask = this.props.campaigns.calendarTask;
    var path = utils
      .removeTrailingSlash(this.props.location.pathname)
      .split("/");
    let me = this;
    let campaign = this.props.campaign
    var campaignData = [{
        title: campaign.title,
        start: this.props.moment?this.props.moment.unix(campaign.start_date).toDate():'',
        end: this.props.moment?this.props.moment.unix(campaign.end_date).toDate():'',
        feed_post: campaign,
        type:"campaign"
    }]
    const events = [
      ...campaignData,
      ...calendarPosts,
      ...calendarPrints,
      ...calendarEmails,
      ...calendarSMS,
      ...calendarTask
    ];
    // let events = typeof this.props.calendarPosts !== 'undefined'
    //   ? this.props.calendarPosts
    //   : [];
    events.length > 0 && events.forEach(function(event_item) {
      if(event_item.type=="post"){
        if (event_item.feed_post.publisher_identity == rescheduled_post_id) {
            event_item.start = rescheduled_post_date;
            event_item.end = rescheduled_post_date;
          }
          if (event_item.start.getHours() == 0) {
            if (event_item.start.getMinutes() == 0) {
              var default_time = "00:00:01 GMT+0530 (IST)";
              var date_only = me.props.moment
                ? me.props.moment(event_item.start).format("ddd MMM DD YYYY")
                : "";
              var new_date = new Date(`${date_only} ${default_time}`);
              event_item.start = new_date;
              event_item.end = new_date;
            }
          }
      }
    });


    if (this.props.moment) {
      BigCalendar.setLocalizer(BigCalendar.momentLocalizer(this.props.moment));
    }
    var leftSidebarClass=''
      if(this.state.leftSidebarShown){
        leftSidebarClass='leftSidebarShown'
      }else{
        leftSidebarClass=''
      }
    return (
      <div>
        {this.state.taskDetail || Object.keys(taskId).length>0 ? this.taskDetailPopup():''}
        {this.state.editTask?this.openEditTaskPopup():''}
        {this.state.changeSchedulePopup ? this.renderChangeSchedule() : null}
        {/* please add class viewbtnWrapper for the list view  */}
        <div className = "viewbtnWrapper clearfix">  
        <CalendarViewButtons
          location={this.props.location}
          currentView={this.state.currView}
          changeHeaderView={this.changeHeaderView.bind(this)}
        />
         {this.state.currView == "calendar" ? 
           <CalendarButtons
           changeHeaderView={true}
           pageView={this.state.currView}
           currFeedPosts={this.state.currFeedPosts}
           monthWeekview={true}
         />
         :''}
         </div>
        {this.state.currView == "calendar" ? (
          <div>
            <div className={`campaignTasksColumnWrapper clearfix ${this.state.typeFilter !=='task' ? '' :'leftSidebarShownParent'} ${leftSidebarClass}`}>
            <div className="camapaignTaskLeftcolumn">
              
                <div className={`camapaignTaskLeftcoumn ${this.state.typeFilter !=='task' ? 'hide' :''}`} >
                    <ul>
                        <li className={`${this.state.currentTab=="openTask"?"active":''}`}><a onClick={this.fetchTask.bind(this,"openTask")}>Open tasks</a></li>
                        <li className={`${this.state.currentTab=="dueToday"?"active":''}`}><a onClick={this.fetchTask.bind(this,"dueToday")}>Due today</a></li>
                        <li className={`${this.state.currentTab=="dueWeek"?"active":''}`}><a onClick={this.fetchTask.bind(this,"dueWeek")}>Due this week</a></li>
                        <li className={`${this.state.currentTab=="overDue"?"active":''}`}><a onClick={this.fetchTask.bind(this,"overDue")}>Overdue</a></li>
                        <li className={`${this.state.currentTab=="completed"?"active":''}`}><a onClick={this.fetchTask.bind(this,"completed")}>Completed</a></li>
                    </ul>
                </div>
                </div>
                
              <div className="camapaignTaskRightcoumn">
            <div className = "CampaignHeaderWrapper clearfix ">
              <div className="leftsidebarToggleBtnWrapper">
            <div className={`${this.state.typeFilter !=='task' ? 'hide' :''}`}><button onClick={this.toggleLeftSidebar.bind(this)} className="btn-theme">Tasks</button></div>
            <div className={`camapaignTaskLeftcoumn ${this.state.typeFilter !=='task' ? 'hide' :''}`} >
                    <ul>
                        <li className={`${this.state.currentTab=="openTask"?"active":''}`}><a onClick={this.fetchTask.bind(this,"openTask")}>Open tasks</a></li>
                        <li className={`${this.state.currentTab=="dueToday"?"active":''}`}><a onClick={this.fetchTask.bind(this,"dueToday")}>Due today</a></li>
                        <li className={`${this.state.currentTab=="dueWeek"?"active":''}`}><a onClick={this.fetchTask.bind(this,"dueWeek")}>Due this week</a></li>
                        <li className={`${this.state.currentTab=="overDue"?"active":''}`}><a onClick={this.fetchTask.bind(this,"overDue")}>Overdue</a></li>
                        <li className={`${this.state.currentTab=="completed"?"active":''}`}><a onClick={this.fetchTask.bind(this,"completed")}>Completed</a></li>
                    </ul>
                </div>
                </div>
               <div className="filterWrapper filterCampaignsWrapper">
                     <div className="checkbok-menu-button">
                            <div className="filter-label">Filter by</div>
                            <ul class='dd-menu'>
                            
                              <li class='button-dropdown'>
                                    <div className=' form-row checkbox'>
                                    <Field
                                        component={renderField}
                                        type='select'
                                        name='typeFilter'
                                        onChange={this.handleFilter.bind(this,"type","")}
                                    >
                                        {/* <option value='' className ="dropdown-toggle btn btn-theme">Type</option> */}
                                            <option value={"all"} key={1}>
                                              All
                                            </option> 
                                            <option value={"post"} key={2}>
                                              Posts
                                            </option>
                                            <option value={"projects"} key={3}>
                                              Content projects
                                            </option>
                                            <option value={"task"} key={4}>
                                              Tasks
                                            </option>
                                            {/* <option value={"print"} key={3}>
                                              Print
                                            </option>
                                            <option value={"email"} key={4}>
                                              Email
                                            </option> */}
                                    </Field>
                                          </div>
                                  </li>
                                 
                                {this.props.roleName=="admin" || this.props.roleName=="super-admin" ?
                                <li class='button-dropdown'>
                                <a class='dropdown-toggle btn btn-theme'>{this.state.userSelected == 'All user' ? this.state.userSelected : this.state.userSelected + ' User Selected'}</a>
                                <ul class='dropdown-menu checkboxDropdown'>
                                    {this.props.campaignAllUserList.campaignAllUserListData.length>0?
                                       this.props.campaignAllUserList.campaignAllUserListData.map((user,index)=>{
                                           return(
                                                <li>
                                                <div className='checkbox form-row'>
                                                <input
                                                    className='checknotify'
                                                    type='checkbox'
                                                    id={`userTag-${user.id}`}
                                                    onChange={this.handleFilter.bind(this,"user",user.id)}
                                                />
                                                <label for={`userTag-${user.id}`}>{user.display}</label>
                                                </div>
                                            </li>)
                                       })   
                                       :''  
                                } 
                                </ul>
                                </li>
                                :''}
                              {this.state.typeFilter !=='task' ?
                                <li class='button-dropdown'>
                                            <div className='form-row checkbox'>
                                            <Field
                                                component={renderField}
                                                type='select'
                                                name='timeFilter'
                                                onChange={this.handleFilter.bind(this,"time","")}
                                            >
                                                <option value='' className ="dropdown-toggle btn btn-theme">Scheduled</option>
                                                    <option value={"published"} key={1}>
                                                    Published
                                                    </option>
                                                    <option value={"all"} key={2}>
                                                    All
                                                    </option>
                                            </Field>
                                        </div>
                                </li>:
                                  <li class='button-dropdown'>
                                  <a class='dropdown-toggle btn btn-theme'>All Status</a>
                                  <ul class='dropdown-menu checkboxDropdown dropdownSelect'>
                                      <li>
                                          <div className='form-row '>
                                          <label><Field cssClass ='allStatusCheckboxes' name="Pending" id="Pending" component={renderField} type="checkbox" value="pending" onChange={this.handleFilter.bind(this,"status","pending")}/> Pending</label>
                                          </div>
                                      </li>
                                      <li>
                                          <div className='form-row'>
                                          <label><Field cssClass ='allStatusCheckboxes' name="inprogress" component={renderField} type="checkbox" value="in-progress" onChange={this.handleFilter.bind(this,"status","in-progress")}/> In-Progress</label>
                                          </div>
                                      </li>
                                      <li>
                                          <div className='form-row'>
                                          <label><Field cssClass ='allStatusCheckboxes' name="completed" component={renderField} type="checkbox" value="completed"  onChange={this.handleFilter.bind(this,"status","completed")}/> Completed</label>
                                          </div>
                                      </li>
                                      <li>
                                          <div className='form-row'>
                                          <label><Field cssClass ='allStatusCheckboxes' name="approved" component={renderField} type="checkbox" value="approved" onChange={this.handleFilter.bind(this,"status","approved")}/> approved</label>
                                          </div>
                                      </li>
                                      <li>
                                          <div className='form-row'>
                                          <label><Field cssClass ='allStatusCheckboxes' name="unapproved" component={renderField} type="checkbox" value="unapproved"  onChange={this.handleFilter.bind(this,"status","unapproved")}/> unapproved</label>
                                          </div>
                                      </li>
                                  </ul>
                                  </li>}
                                {this.state.typeFilter =='task' ? 
                                <li class='button-dropdown'>
                                            <div className=' form-row checkbox'>
                                            <Field
                                                component={renderField}
                                                type='select'
                                                name='timeFilter'
                                                onChange={this.handleFilter.bind(this,"time","")}
                                            >
                                                <option value='' className ="dropdown-toggle btn btn-theme">All Time</option>
                                                    <option value={"month"} key={1}>
                                                    this Month
                                                    </option>
                                                    <option value={"week"} key={2}>
                                                    this Week
                                                    </option>
                                                    <option value={"today"} key={3}>
                                                    Today
                                                    </option>
                                            </Field>
                                        </div>
                                </li>:''}
                                {this.state.typeFilter =='task' ? 
                                <li class='button-dropdown'>
                                            <div className='form-row checkbox assign-task-select'>
                                            <Field
                                                component={renderField}
                                                type='select'
                                                name='listFilter'
                                                onChange={this.handleFilter.bind(this,"listFilter","")}
                                            >
                                                <option value='' className ="dropdown-toggle btn btn-theme">All Task</option>
                                                    <option value={"createdby_me"} key={1}>
                                                    Created By Me
                                                    </option>
                                                    <option value={"asssignto_me"} key={2}>
                                                    Assigned To Me
                                                    </option>
                                            </Field>
                                        </div>
                                </li>:''}
                            </ul>
                    </div>
                    </div>
                </div>
              
            <div class="planner-schedule">
              <div className="widget">
                <div
                  className={`bigCalendar-calendarview ${this.state
                    .cal_view}-view calendar-default`}
                  ref="big_calendar"
                >
                  {this.state.show_calendar_feed_post_state
                    ? this.show_calendar_feed_post()
                    : ""}
                  {this.props.moment ? (
                    <Dnd
                      location={this.props.location}
                      move_event={this.move_event.bind(this)}
                      callCalPostDetails={this.callCalPostDetails.bind(this)}
                      moment={this.props.moment}
                      fetchTimeStamps={this.fetchTimeStamps}
                      fetchPosts={this.fetchPosts.bind(this)}
                      setView={this.setView.bind(this)}
                      components={{
                        event: FeedEvents
                      }}
                      fromCampaign={true}
                      events={
                        typeof this.props.moment !== "undefined" &&
                        this.props.moment != null
                          ? events
                          : []
                      }
                    />
                  ) : (
                    ""
                  )}
                </div>
              </div>
            </div>
            </div>
            </div>
          </div>
        ) : (
          <div>
            <div class="section 33">
            <CampaignPlanner 
            openDetailPopup = {this.openDetailPopup.bind(this)} 
            campaign={this.props.campaign} 
            Moment={this.props.moment}
            roleName={this.props.roleName}
            location={this.props.location}
            posts={posts}
            profile={this.props.profile}
            changeScheduleAction={this.changeScheduleAction.bind(this)}
            handleEditCopy={this.handleEditCopy.bind(this)}
            handleDelete = {this.handleDelete.bind(this)}
            pathValue ={"planner"}
            campaignAllUserList={this.props.campaignAllUserList}
            />
            </div>
          </div>
        )}
        
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    campaigns: state.campaigns,
    departments: state.departments,
    usersList: state.usersList.userList.data,
    posts: state.posts
  };
}

function mapDispatchToProps(dispatch) {
  return {
    campaignActions: bindActionCreators(campaignActions, dispatch)
  };
}

var connection = connect(mapStateToProps,mapDispatchToProps);
var reduxConnectedComponent = connection(ScheduledCalendar);
export default reduxForm({
  form: 'ScheduledCalendar' // a unique identifier for this form
})(reduxConnectedComponent)
