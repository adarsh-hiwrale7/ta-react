import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Field, reduxForm } from 'redux-form'
import * as campaignActions from '../../actions/campaigns/campaignsActions';
import PreLoader from '../PreLoader';
import CampaignPlanner from './CampaignPlanner';
import CalendarViewButtons from '../Header/CalendarViewButtons';
import { renderField } from '../Helpers/ReduxForm/RenderField'
import CampaignKanban from './CampaignKanban';
import KanBanLoader from './KanBanLoader';

import reactTooltip from '../Chunks/ChunkReactTooltip';
import notie from "notie/dist/notie.js"
class CampaignProduction extends React.Component {
    constructor (props) {
      super(props);
      this.state={
        currView : "kanban",
        tooltip: null,
        callFromOverViewPage:false
      }
    }
    componentDidMount(){
      let campId = this.props.campaign.identity
      this.props.campaignActions.fetchPlannerProjects(campId,this.props.Moment);
      if(this.props.redirectOnListView==true){
        this.setState({
          currView:'list',
          callFromOverViewPage:true,
        })
      }
    }
    componentWillMount(){
      reactTooltip().then(reactTooltip => {
        this.setState({tooltip: reactTooltip})
        })
      }
    handleEditCopy(type,identity,action){
      let campId = this.props.campaign.identity;
      if(action=="edit"){
        this.props.campaignActions.getContentProjectById(type,identity,campId,action);
      }else if(action=="copy"){
        this.props.campaignActions.getContentProjectById(type,identity,campId,action);
      }
    }
  
    handleDelete(type,identity){
      let campId = this.props.campaign.identity;
      this.props.campaignActions.deleteContentProject(type,identity,campId,this.props.Moment,"production");
    }

    changeHeaderView(value) {
      this.setState({
        currView: value
      });
    }

    render(){
      return(
        <div className="production-view-wrapper">
          <div className = "clearfix">
          <div className = "viewbtnWrapper clearfix">  
            <CalendarViewButtons
              location={this.props.location}
              currentView={this.state.currView}
              changeHeaderView={this.changeHeaderView.bind(this)}
              pathValue={"production"}
            />
            
            {this.state.currView =="kanban" ?
            <div className="kanban-view-wrapper">
              {this.props.campaigns.contentProjectListLoading==true?
              <KanBanLoader />:
              <CampaignKanban 
                campaignActions={this.props.campaignActions} 
                handleEditCopy={this.handleEditCopy.bind(this)}
                {...this.props} 
                tooltip={this.state.tooltip} 
                notie={notie}
                roleName={this.props.roleName}
              />}
            </div>
            :
            <div className="production-list-view">
              <div class="section">
                <CampaignPlanner 
                campaign={this.props.campaign} 
                Moment={this.props.Moment}
                roleName={this.props.roleName}
                location={this.props.location}
                profile={this.props.profile}
                handleEditCopy={this.handleEditCopy.bind(this)}
                handleDelete = {this.handleDelete.bind(this)}
                pathValue ={"production"}
                callFromOverViewPage={this.state.callFromOverViewPage}
                redirectStatusType={this.props.redirectStatusType}
                campaignAllUserList={this.props.campaignAllUserList}
                />
              </div>
            </div>
            }
       </div>
       </div>
     </div>
      )
    }
  }
function mapStateToProps(state){
    return{
      campaigns: state.campaigns,
    }
}
function mapDispatchToProps(dispatch){
    return{
      campaignActions: bindActionCreators(campaignActions, dispatch)
    }
}

var connection = connect(mapStateToProps,mapDispatchToProps);
var reduxConnectedComponent = connection(CampaignProduction);
export default reduxForm({
    form: 'CampaignProduction' // a unique identifier for this form
  })(reduxConnectedComponent)