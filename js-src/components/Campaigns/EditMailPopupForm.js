;
import { Link } from "react-router";
import { browserHistory } from "react-router";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Field, reduxForm } from 'redux-form';
import { renderField } from '../Helpers/ReduxForm/RenderField';
import MentionTextArea from '../Helpers/ReduxForm/MentionTextArea';
import PreLoader from '../PreLoader';
const required = value => (value ? undefined : 'Required')

class EditMailPopupForm extends React.Component {

    constructor(props) {
      super(props);
      this.state = {
        customValidation:true,
        emailValidation:true
      }
    }
    handleSubmit(){
      this.props.handleMailPopupSubmit();
    }
    
    componentDidMount() {
      this.props.campaginData.campaignActions.getContentProjectHtml(this.props.projectId);
    }
    
    componentWillReceiveProps(newProps) {
      if(this.props.campaginData.campaigns.projectHtmlData !== newProps.campaginData.campaigns.projectHtmlData && newProps.campaginData.campaigns.projectHtmlData !== undefined){
          this.handleInitialize(newProps.campaginData)
      }
    }
    
    handleInitialize(props){
      var recipientData=props.campaigns.projectHtmlData;
      var initData={
        From:recipientData.from_email,
        subjectline:recipientData.subject,
        previewtext:recipientData.email_preview
      }
      this.props.initialize(initData)
  }
  validateMultipleEmail(e){
    var re = /^[A-Z0-9\._%-]+@[A-Z0-9\.-]+\.[A-Z]{2,4}(?: ?[,] ?[A-Z0-9\._%-]+@[A-Z0-9\.-]+\.[A-Z]{2,4})*$/i;
    if(e.target.value == ""){
        if(document.getElementById('title').value !== ""){
          this.setState({
            customValidation:false,
            emailValidation : false
          })
        }else{
          if(this.state.customValidation === false){
            this.setState({customValidation:true,
              emailValidation : true})
          }
        }
        document.querySelector(".guest-email-validation").style.display="none";
    }else{
      if(re.test(e.target.value)){
        this.setState({customValidation:false,
          emailValidation : false})
        document.querySelector(".guest-email-validation").style.display="none";
      }else{
        this.setState({customValidation:true,emailValidation : true})
        document.querySelector(".guest-email-validation").style.display="inline";
      }
    }
}
    render(){
      const {
        handleSubmit,
        pristine,
        submitting
      } = this.props
       return(
        <div className = "innerEditMailPopup">
            <form onSubmit={handleSubmit}>
            <div className="form-row" >
                <Field
                placeholder='From'
                label='From'
                component={renderField}
                type='text'
                name='From'
                validate={[required]}
                onChange={this.validateMultipleEmail.bind(this)}
              />
              <div className="guest-email-validation">* Please add valid email address</div>
            </div>
             <Field
              placeholder='Subject line'
              label='Subject line'
              component={renderField}
              type='text'
              name='subjectline'
              validate={[required]}
            />
             <Field
              placeholder='Preview text'
              label='Preview text'
              component={renderField}
              type='text'
              name='previewtext'
            />
        

          <div class="sendbtn clearfix"> 
              <button type="submit" disabled={pristine || submitting}>Next</button>
          </div> 
          </form>
          </div>

       )
    }
}
function mapStateToProps(state, ownProps) {
    return {}
  }
  
function mapDispatchToProps(dispatch) {
    return {
  
    }
  }
let connection = connect(mapStateToProps, mapDispatchToProps)
var reduxConnectedComponent = connection(EditMailPopupForm)
  
export default reduxForm({
    form: 'EditMailPopupForm' // a unique identifier for this form
})(reduxConnectedComponent)
