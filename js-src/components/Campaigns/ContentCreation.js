import { bindActionCreators } from 'redux'
import { Field, reduxForm } from 'redux-form'
import { renderField } from '../Helpers/ReduxForm/RenderField'
import {connect} from 'react-redux'
import ReactDOM from 'react-dom';
import moment from '../Chunks/ChunkMoment'

import MentionTextArea from "../Helpers/ReduxForm/MentionTextArea";
import ReactDatePicker from '../Helpers/ReduxForm/ReactDatePicker' // react datepicker field
import reactDatepickerChunk from '../Chunks/ChunkDatePicker' // datepicker chunk
import * as utils from '../../utils/utils';
const required = value => (value ? undefined : 'Required')
class ContentCreation extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
          Moment: null,
          ReactDatePicker: null,
          disableSubmit:false,
          sendMailCheck :false
        }
    }
    componentWillMount () {
      moment().then(moment => {
        reactDatepickerChunk()
          .then(reactDatepicker => {
            this.setState({ ReactDatePicker: reactDatepicker, Moment: moment })
          })
          .catch(err => {
            throw err
          })
      })
    }
    handleSubmit(values) {
      this.props.handleContentCreationSubmit(values , this.state.sendMailCheck);
    }
    componentDidMount () {
      this.handleInitialize(this.props)
    }
    handleInitialize (nextProps = null) {
      let initData = {}
      if(this.props.currContentProject){
        let projectUserTag='';
         let currContentProject = this.props.currContentProject;
          initData["startDate"]= currContentProject.start_date;
          initData['endDate'] = currContentProject.end_date;
          initData['shippingDate']=currContentProject.shipping_date;
          initData["title"]= this.props.isFromplanner==true? currContentProject.title :"Copy of "+currContentProject.title;
          initData["identity"]=currContentProject.project_identity;
          if(this.state.sendMailCheck){
            initData['mailSend'] = this.state.sendMailCheck;
          }
          if(currContentProject.user_tag){
            currContentProject.user_tag.map(data=>{
                data.name.replace(/ /g,'');
                projectUserTag = projectUserTag.concat(`@${data.name} `);
            })
            initData["Assignees"]=  projectUserTag!=='' ? utils.replacePermissionTags(
              projectUserTag ,
              this.props.userTags,
                [],true
              ):''
          }
      }else if(this.props.Moment !== null){
        initData = {
          startDate:  this.props.campaign.start_date <= this.props.Moment().unix()? this.props.Moment().unix():this.props.campaign.start_date,
          shippingDate: this.props.campaign.start_date <= this.props.Moment().unix()? this.props.Moment().unix():this.props.campaign.start_date,
          endDate :this.props.campaign.start_date <= this.props.Moment().unix()? this.props.Moment().unix():this.props.campaign.start_date
          }
      }
      this.props.initialize(initData)
    }
    handleMentionFocus(e){  
      var me = this
      if(e.target.value == ''){
       me.props.change('Assignees' ,'@')
      }
    }
    valiDate(e) {
    var temp='',count=0,count2=0,flag=0;
    const len=Object.keys(e).length-1;

    for(var i=0;i<Object.keys(e).length-1;i++){
      //for loop is for checking the whole string every time it is updated
      temp=e[i];
      if(e[i]=="@"){count+=1}  //count of at the rate
      if((e[i]==")"&&e[i+1]==" "&& e[i+2]=="@" && flag!==2)){ //count of )" "@
        count2+=1;flag=1;
      }
      if((e[i]==")"&&e[i+1]==" "&& e[i+2]!=="@" && e[i+2]!==undefined) || (e[i]==")"&& e[i+1]!=="@" && e[i+1]!==undefined && e[i+1]!==" ")){
        flag=2;
      }
    }
    var first=e[0];
    var last=temp;
    
    temp=e[0]
  if(temp !== undefined){   
    if(first=="@" && last==")" && e[i]==null){
      this.setState({disableSubmit:false});
      document.querySelector(".viewper").style.display="none"; 
    }
    else if(first=="@" && last==")" && e[i]==" "){
      if(flag==1){
        if(count==count2+1){
          this.setState({disableSubmit:false});
          document.querySelector(".viewper").style.display="none";
        }
        else{
          this.setState({disableSubmit:true});
          document.querySelector(".viewper").style.display="inline";
        }
      }
      else{
        this.setState({disableSubmit:false});
        document.querySelector(".viewper").style.display="none";
      }
    }
    else{
      this.setState({disableSubmit:true});
      document.querySelector(".viewper").style.display="inline";
    }
    if(flag==2 && (e[0]!=='@'|| e[0]!==' ')){
      this.setState({disableSubmit:true});
      document.querySelector(".viewper").style.display="inline";
    }
  }
  else
  {
    this.setState({disableSubmit:false});
    document.querySelector(".viewper").style.display="none";
  }
}
onChangemailSendCheckBox(e){
    this.setState({
      sendMailCheck:e.target.checked 
    });
  }
render(){
      const { pristine, submitting ,handleSubmit} = this.props
      var endDate = "";
      if (
        this.props.campaign.end_date &&
        this.state.Moment
      ) {
        endDate = this.state.Moment
          .unix(this.props.campaign.end_date)
          .format("llll")
          .toString();
      }
      var campaignStartDate = '';
      var campaignEndDate = '';
      // var contentProjectStartDate = '';
      // var contentProjectEndDate = '';

      if (
        this.props.campaign.start_date &&
        this.state.Moment
      ) {
           campaignStartDate = this.props.campaign.start_date <= this.state.Moment().unix() ? this.state.Moment().unix(): this.state.Moment
          .unix(this.props.campaign.start_date).format("llll").toString();
      }
   
     if (
        this.props.campaign.end_date &&
        this.state.Moment
      ) {
           campaignEndDate = this.props.campaign.end_date <= this.state.Moment().unix()? this.state.Moment().unix(): this.state.Moment
          .unix(this.props.campaign.end_date)
          .format("llll")
          .toString();
      }
     return(
            <form onSubmit={handleSubmit}> 
                 <Field
                    label='Title'
                    component={renderField}
                    placeholder={'Title'}
                    type='text'
                    name='title'
                    validate={[required]}
              />
              <Field 
                component={renderField}
                type="hidden"
                name="identity"
              />
              <div className="user-tag-editor user-permission quick-segment-wrapper form-row">
                  <label for='Assignees'>Assignees</label>
                    <Field
                    label='Assignees'
                    placeholder={ 'Add @user or @department to give access to this content project'}
                    name='Assignees'
                    component={MentionTextArea}
                    userTags={this.props.userTags}
                    validate={[required]}
                    type='text'
                    id='Assignees'
                    disabled ={this.props.isFromplanner==true ? true :false}
                    onChange={this.valiDate.bind(this)}
                    onFocus={this.handleMentionFocus.bind(this)}
                  />
                    <div className="viewper">* Please add only user or department or company tag</div>
                  </div>
               <div className = {`print-date-wrapper clearfix ${this.props.contentType == "email" ? "new-email-popup-wrapper" :'' }`}> 
                <div className = "start-date">
                <Field
                ref={`startDate`}
                name={`startDate`}
                id={`startDate`}
                component={props => {
                  return (
                    <ReactDatePicker
                      {...props}
                      dateFormat="DD/MM/YYYY"
                      label={`Start Date`}
                      reactDatePicker={this.state.ReactDatePicker}
                      minDate={campaignStartDate}
                      moment={this.state.Moment}
                      maxDate={campaignEndDate}
                    />
                  )
                }}
              />
             </div> 
              <div className = "end-date contentMailcheck" >
                <Field
                ref={`endDate`}
                name={`endDate`}
                id={`endDate`}
                component={props => {
                  return (
                    <ReactDatePicker
                      {...props}
                      dateFormat="DD/MM/YYYY"
                      label={`End Date`}
                      minDate={campaignStartDate}
                      reactDatePicker={this.state.ReactDatePicker}
                      moment={this.state.Moment}
                      maxDate={campaignEndDate}
                    
                    />
                  )
                }}
              />
             </div> 
              <div className = "end-date email-date">
                <Field
                ref={`shippingDate`}
                name={`shippingDate`}
                id={`shippingDate`}
                component={props => {
                  return (
                    <ReactDatePicker
                      {...props}
                      dateFormat="DD/MM/YYYY"
                      label={`Shipping Date`}
                      reactDatePicker={this.state.ReactDatePicker}
                      moment={this.state.Moment}
                      minDate={campaignStartDate}
                      maxDate={campaignEndDate}
                      disabled = {this.state.sendMailCheck  == true ? true : false}
                    />
                  )
                }}
              />
             </div> 
            </div>
            {this.props.contentType == "email" ? 
            <div className = "email-checkbox">  
                  <div className = "checkbox">
                    <Field
                      type="checkbox"
                      id='mailSend'
                      component ={renderField}
                      onChange ={this.onChangemailSendCheckBox.bind(this)}
                      class="post-check selectallpost"
                      name="mailSend"
                      />
                    </div>
                    <label for="mailSend">Send the email immediately after sign off</label>
              </div>
              :''}
              <div className = "right">
              <button
                  type="submit"
                  disabled={submitting || pristine || this.state.disableSubmit}
                  className="btn"
                >
                  Create
                </button>
              </div>
            </form>
        )
    }
}
function mapStateToProps (state) {
    return {
      
    }
  }
  function mapDispatchToProps (dispatch) {
    return {
     
    }
  }

let connection = connect(mapStateToProps, mapDispatchToProps)
var reduxConnectedComponent = connection(ContentCreation)
export default reduxForm({
    form: 'ContentCreation' // a unique identifier for this form
  })(reduxConnectedComponent)
  