import {browserHistory} from 'react-router'
import {connect} from  'react-redux';
import {bindActionCreators } from 'redux';
import Asset from '../Assets/Asset'
import AssetDetail from '../Assets/AssetDetail';
import CreateAsset from '../Assets/CreateAsset';
import PopupWrapper from '../PopupWrapper';
import Globals from '../../Globals';
import moment from '../Chunks/ChunkMoment';
import notie from 'notie/dist/notie.js';
import SliderPopup from '../SliderPopup';
import * as utils from '../../utils/utils';
import * as assetsActions from '../../actions/assets/assetsActions';
import * as s3Actions from '../../actions/s3/s3Actions';
import * as generalActions from '../../actions/generalActions';
import * as departmentActions from '../../actions/departmentActions';
import * as userActions from '../../actions/userActions';
import TagPopup from '../Feed/TagPopup';
import ReactDOM from 'react-dom'
var selectedUserData;
var selectedDepartmentData ;

class CampaignAsset extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      assetDetailPopup: false,
      currentAssetDetail: null,
      overlay_flag: false,
      moment: null,
      currCatID:null,
      campID:null,
      currFolderID:null,
      currCatName:null,
      assets: [],
      currPage: 1,
      maxItems:10,
      meta: {
        pagination: {
          total_pages: 1
        }
      }
    }
  }

  componentWillMount () {
    this.fetchAssets({ currPage: 1 })
    this.props.assetsActions.fetchCatCampaignFolders();

    moment().then(moment => {
      this.setState({ moment: moment })
    })
  }
  componentDidMount(){
    this.setState({
      currCatID:this.props.campaign.campaign_category,
      campID:this.props.campaign.identity,
      currFolderID:this.props.campaign.campaign_folder,
    })
  }
  componentWillReceiveProps(nextProps){
    if(this.props.assets.creation.created !== nextProps.assets.creation.created && nextProps.assets.creation.created== true){
      this.closeEditDetailFormPopup();
      this.fetchAssets({currPage:1});
      this.setState({
        currPage:1
      })
    }
    var campaigns = nextProps.assets.campaigns;
    var campaignName = campaigns.filter(function (campaign) {
      return campaign.campaign_identity == nextProps.campaign.identity
    })
    this.setState({
      currCatName : campaignName
    })
    if(this.props.campaignAssetPopup!==nextProps.campaignAssetPopup && nextProps.campaignAssetPopup==true){
      this.handleAssetsCreation(campaignName,true);
    }
  }
  handleAssetsCreation(catName,skipReset = false) {
    if(!skipReset){
      this.props.s3Actions.uploadReset();
      this.props.assetsActions.resetCreateAsset();
    }
    if (this.state.newAssetPopup !== true && catName !== null) {
      this.setState({
        newAssetPopup: true,
        campID: this.props.campaign.identity
      })

      // asset created flag - change in redux to false
      this.props.assetsActions.creatingNewAsset()

      document.body.classList.add('overlay')
    }

  }
  assetClickFromRowListing(asset) {
    
   
    this.setState({ assetDetailPopup: true, currentAssetDetail: asset })

    document.body.classList.add('overlay')
  }
  closeAssetDetailPopup() {

    this.setState({ assetDetailPopup: false })

    document.body.classList.remove('overlay')
  }
  renderAssetDetail() {
    document.body.classList.add('overlay')
    return (
      <SliderPopup className="asset-detail-popup">
        <button
          id='close-popup'
          className='btn-default'
          onClick={this.closeAssetDetailPopup.bind(this)}
        >
          <i className='material-icons'>clear</i>
        </button>
        <AssetDetail
          moment={this.state.moment}
          locationInfo = {this.props.location}
          asset={this.state.currentAssetDetail}
          updateText={this.updateText.bind(this)}
          currCatID={this.state.currCatID}
          currFolderID={this.state.currFolderID}
          aws={this.props.aws}
          assetAction={this.props.assetsActions}
          s3Actions={this.props.s3Actions}
          assets={this.props.assets}
          userTags = {this.props.userTags}
          postTags = {this.props.postTags}
          closeAssetDetailPopup = {this.closeAssetDetailPopup.bind(this)}
          openEditDetailFormPopup = {this.openEditDetailFormPopup.bind(this)}
          searchedInput = {this.props.searchedInput}
        />
      </SliderPopup>
    )
  }

  openEditDetailFormPopup(){
    this.setState({
        showEditDetailForm: true
    })
  }
  
  renderUsersFiles() {
    var files = this.props.assets.files

    return (
      <div id='feed' className='myassets'>
        <AssetsRowList
          moment={this.state.moment}
          assets={files}
          assetClickFromRowListing={this.assetClickFromRowListing.bind(this)}
        />
      </div>
    )
  }
  closeAssetDetailPopup() {
  
    this.setState({ assetDetailPopup: false })
    document.body.classList.remove('overlay')
  }

  loadMoreAssets () {
    var me = this

    if (typeof me.state.meta.pagination !== 'undefined') {
      var nextpage = me.state.meta.pagination.current_page + 1
      me.setState({
        currPage: nextpage
      })

      me.fetchAssets({ currPage: nextpage })
    }
  }

  fetchAssets ({ currPage = 1 }) {

    this.setState({
      AssetLoading : true
    })
    fetch(Globals.API_ROOT_URL +`/asset/${this.props.campaign.campaign_category}/${this.props.campaign.campaign_folder}?limit=${Globals.campaignPageLimit}&page=${currPage}&campaign_identity=${this.props.campaign.identity}`,{
      headers: utils.setApiHeaders('get')
    })
      .then(response => response.json())
      .then(json => {
        if (Array.isArray(json.data) && json.data.length > 0) {
          if(currPage==1){
            this.setState({
              assets:[],
            })
          }
          this.appendAssetsInState(json.data)
          this.setMeta(json.meta)
        }else{
          this.setState({
            AssetLoading : false
          })
          utils.handleSessionError(json)
        }
      })
      .catch(err => {
        notie.alert('error', err, 5)
        throw err
      })
  }

  appendAssetsInState (assets) {
    var assets = this.state.assets.concat(assets)
    this.setState({
      assets: assets,
      AssetLoading:false
    })
  }

  mousedowncapture(e){
    if(document.getElementById('pop-up-tooltip-holder')){
      if (!document.getElementById('pop-up-tooltip-holder').contains(e.target)) {
          document
            .getElementById('pop-up-tooltip-holder')
            .classList.add('hide');
      }
    }
  }

  updateText(id, type, e) {
    this.setState({overlay_flag:true}); 
    var isUserFound = false;
    if(type=="department" || type=='company'){
      this.props.departments.list.length>0?
      this.props.departments.list.map((DepartmentDetail,i)=>{
        if(DepartmentDetail.identity==id){
          selectedDepartmentData = DepartmentDetail;
        }
      }):''
    }
    else{
      this.props.usersList.length>0?
      this.props.usersList.map((userDetail,i)=>{
        if(userDetail.identity==id){
          selectedUserData = userDetail;
          isUserFound = true;
        }
      }):''
      if(isUserFound==false){
        selectedUserData ="No user found"
      }
    }
    const domNode = ReactDOM.findDOMNode(e.target);
    let coords = domNode.getBoundingClientRect();
    let coords_top = coords.top + pageYOffset;
    let popup_wrapper = document.getElementById("pop-up-tooltip-wrapper");
    let coords_left = 0,
      coords_area = 0,
      new_class_comb = '';
    let pop_holder = document.getElementById("pop-up-tooltip-holder");
    let popupparent = document.getElementById('department-popup');
    if (popupparent != null) {
      pop_holder.className = 'departmentpopup';
    } else {
      pop_holder.className = '';
    }
    if (screen.width >= 768) {
      coords_left = coords.left + pageXOffset + (coords.width / 2);
    }
    else {
      coords_left = coords.left + pageXOffset;
      coords_area = coords_left + 472;
      if (coords_area < screen.width) {
        new_class_comb = 'popup-left';
      }
      else {
        new_class_comb = 'popup-right';
      }

    }
    if (e.nativeEvent.screenY < 400) {
      new_class_comb += ' bottom';
    }
    else {
      new_class_comb += ' top';
    }
    popup_wrapper.className = new_class_comb;
    this.setState({
      popupTop: coords_top,
      popupLeft: coords_left,
      popupdisplay: 'block',
      tagType: type,
      tagTypeClass: (type === "department" ? 'departmentpopup' : '')
    });
    // let pop_overlay = document.getElementById("tag-popup-overlay");
    // pop_overlay.className = 'show';

  }
  setMeta (meta) {
    this.setState({
      meta: meta
    })
  }
  assetClick(self, asset) {
   self.setState({ assetDetailPopup: true, currentAssetDetail: asset })
   this.props.s3Actions.uploadReset()
   document.body.classList.add('overlay')
  }
  showEditorAssets(object_editor){
    this.setState({
      openEditer: true,
      imageData:object_editor
  });
  this.props.assetsActions.uploadedAssetInBuffer(object_editor)
  }
  renderEditDetailFormPopup() {
    document.body.classList.add('overlay');
    return (
      <SliderPopup className="create_asset_popup">
        <button
          id='close-popup'
          className='btn-default'
          onClick={this.closeEditDetailFormPopup.bind(this)}
        >
          <i className='material-icons'>clear</i>
        </button>
         <CreateAsset
          TotalMediaSize={this.fetchTotalMediaSize.bind(this)}
          searchedInput={this.props.searchedInput}
          catID={this.state.currCatID}
          campID={this.state.campID}
          currPage={this.state.currPage}
          maxItems={this.state.maxItems}
          folderID={this.state.currFolderID}
          catName={this.state.currCatName}
          jumpToEditor={true}
          closeAssetCreatePopup = {this.closeAssetCreatePopup.bind(this)}
          closeEditDetailFormPopup={this.closeEditDetailFormPopup.bind(this)}
          location={this.props.location}
          assetProps={this.props.assets}
          onUnload={this.onUnload.bind(this)}
         />
      </SliderPopup>
    )
  }
  renderContentCreation() {
    return (
      <SliderPopup wide className='create_asset_popup'>

        <button
          id='close-popup'
          className='btn-default'
          onClick={this.closeAssetCreatePopup.bind(this)}
        >
          <i className='material-icons'>clear</i>
        </button>
        <CreateAsset
          profile={this.props.profile}
          onUnload={this.onUnload.bind(this)}
          TotalMediaSize={this.fetchTotalMediaSize.bind(this)}
          catID={this.state.currCatID}
          campID={this.state.campID}
          currPage={this.state.currPage}
          maxItems={this.state.maxItems}
          folderID={this.state.currFolderID}
          catName={this.state.currCatName}
          closeAssetCreatePopup = {this.closeAssetCreatePopup.bind(this)}
          closeEditDetailFormPopup={this.closeEditDetailFormPopup.bind(this)}
          location={this.props.location}
          assetProps={this.props.assets}
        />
      </SliderPopup>
    )
  }
  closeEditDetailFormPopup(){

    document.body.classList.remove('overlay')
    this.setState({
        showEditDetailForm: false,
        openEditer:false,
       })
    if (typeof this.props.location.query.openasset !== "undefined") {
      if(this.props.location.query.source === "dashboard"){
          let url = `/dashboard`
          browserHistory.push(url)
      }else{
          let url
          if(this.props.location.query.searchcreateasset !== undefined)
          url="/search/campaign"
          else
          url = `/campaigns?asset=${this.props.location.query.openasset}&campaignid=${this.props.location.query.campaignid}`
          browserHistory.push(url)
      }

    }
  }

  fetchTotalMediaSize(e){
    this.setState({
      selectedMediaSizeTotal:e
    })
  }
  closeAssetCreatePopup() {
    this.setState({ newAssetPopup: false })
    this.props.changeCreateAssetStatus();
    this.props.s3Actions.uploadReset();
    this.props.assetsActions.resetCreateAsset();
    document.body.classList.remove('overlay')
    this.onUnload(this);
  }
  onUnload(event) { 
    // the method that will be used for both add and remove event

    if (this.props.assets.isAssetUploadingStart == true) {
      //api will call when uploading is start and user want to cancel or refresh page
      var objToSend = {
        "file-size": this.state.selectedMediaSizeTotal,
        "action": "decrease"
      }
      this.props.generalActions.fetchS3Size(objToSend)
      this.state.newAssetPopup==true? event.returnValue = 'heyyy':'';
    }
  }

  renderEditerPopup(e,asset){
    this.setState({openEditer:true , currentAssetDetail: asset});
  }
  
  renderAssets () {
    let files = this.state.assets
    let me = this
    return (
      <div className='assetsList'>
        <div className='archive-container clearfix'>
          {files.map(function (file, index) {
             return (
             <Asset
                onDoubleClick ={asset => me.assetClick(me, file)}
                editpopup_open={asset => me.renderEditerPopup(me,file)}
                key={index}
                name={file.title}
                extension={file.media_extension}
                identity={file.identity}
                media_type={file.media_type}
                media_size={file.media_size}
                thumbnail_url={file.thumbnail_url}
                media_url={file.media_url}
                approved={file.approved}
                category_id={file.category}
                width='320'
                height='240'
                detailView={false}
                title={file.title}
                showeditor={me.showEditorAssets.bind(me)}
              />
            )
          })}
        </div>
        {this.state.currPage!==1 && this.state.AssetLoading==true?
        this.renderLoading()
        :''}

        {this.state.currPage !== me.state.meta.pagination.total_pages && this.state.AssetLoading!==true
            ?
            <p className='center'>
              <a
                  className='btn btn-default'
                  onClick={this.loadMoreAssets.bind(this)}
                  >
                    Load More
                  </a>
                    { // {this.state.currentTab == 'assets'
                      // ? <CampaignAsset campaign={this.props.campaign} />
                      // : <div />
                    }
                

            </p>
        : null}
      </div>
     
    )
  }
  renderLoading(){
    return(
      <div id="container">
                <div className='archive-container asset-file-container clearfix'>
                {
                  Array(3).fill(1).map((el, i) => 
                
                <div class="vid file-image asset-file imgwrapper">
                  <div class="inner_imgwrapper assetInnerWrapper">
                    <a class="type-image">
                        <span class="imageloader loaded thumbnail loader-assets-image">
                            <img src="/img/visiblyLoader.gif" />
                        </span>
                    </a>
                </div>
                <a class="file-details">
                    <span class="title">
                        <div className="loader-grey-line loader-line-height loader-line-radius loader-assets-title"> </div>
                    </span>
                </a>
              </div>
              )}
              </div>
              
              </div>
    )
  }
  
  render () {
    let html = null
    let user = this.props.users,
      userDetails = user.userDetails,
      isFetching = user.isFetching,
      department = this.props.department;
    let campaignDetailPopupScrollbar = {
      maxHeight: `calc(100vh - 57px)`
    }
      if (this.state.assets.length > 0) {
        html = this.renderAssets()
      } else {
        html =  <div className='no-data-block'>No asset found.</div>
      }    
    return (

     
      <div class='asset-container'>
        {
          this.state.AssetLoading==true && this.state.currPage==1?
            <div>
              {this.renderLoading()}
            </div>
          :
          <div>
            {this.state.openEditer == true ?  this.renderEditDetailFormPopup() : ''}
            {this.state.showEditDetailForm ? this.renderEditDetailFormPopup() : this.props.assets.creation.created ? document.body.classList.remove('overlay') : ''}
            {this.state.newAssetPopup ? this.renderContentCreation() : <div />}
            <PopupWrapper>
              {this.state.assetDetailPopup ? this.renderAssetDetail() : ''}
              {html}
            </PopupWrapper>
            <div id="pop-up-tooltip-holder">
              <div id="pop-up-tooltip-wrapper" className={this.state.tagTypeClass}>
                <div className="pop-up-tooltip showtotop" style={{ top: this.state.popupTop, left: this.state.popupLeft, display: this.state.popupdisplay }}>
                  <div className="abc">
                    <span className='popup-arrow'></span>

                    {/* <div className='preloader-wrap'>
                <PreLoader />
              </div> */}
              
                    <TagPopup
                      userDetails={selectedUserData}
                      isFetching={isFetching}
                      isDept={this.state.tagType}
                      department={selectedDepartmentData}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        }
      </div>
    )
  }

}

function mapStateToProps(state){
  return{
    aws       : state.aws,
    campaigns : state.campaigns,
    assets    : state.assets,
    general   : state.general,
    profile   : state.profile,
    users: state.users,
    departments:state.departments,
    usersList : state.usersList.userList.data
  }
}
function mapDispatchToProps(dispatch){
  return{
    assetsActions: bindActionCreators(assetsActions, dispatch),
    s3Actions: bindActionCreators(s3Actions,dispatch),
    generalActions: bindActionCreators(generalActions, dispatch),
  }
}
module.exports = connect(mapStateToProps, mapDispatchToProps)(CampaignAsset)
