var getUserTagDataFlag = false
var search
var limitCampUser = 4
var fetchAllProjectApi=false
import FetchuserDetail from '../FetchUsersDetail/FetchUsersDetail';
import $ from '../Chunks/ChunkJquery'
import PreLoader from '../PreLoader';
export default class CampaignKanban extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            projects: [],
            draggedOverCol: 0,
            mouseIsHovering: false,
            showAssignPopup: {},
            filter_state:{},
            
        }
        this.handleOnDragEnter = this.handleOnDragEnter.bind(this)
        this.handleOnDragEnd = this.handleOnDragEnd.bind(this)
    }
    componentDidMount() {
        fetchAllProjectApi=false
        this.props.campaignActions.fetchKanbanStatusList()
        var me=this
        // to close popup on outside of click
        window.addEventListener('click', function (event) {
            if(!!event.target.closest('.assign-author')){
            }else{
                me.setState({showAssignPopup:{}})
                if(document.getElementsByClassName('assign-popup') !== undefined  && document.getElementsByClassName('assign-popup')[0] !== undefined){
                    document.getElementsByClassName('assign-popup')[0].classList.add('hide');
                }
            }
          })
        // $('body').click(function (e) {
        //     if (!$(e.target).closest('.assign-popup').length && !$(e.target).closest('.assign-author').length) {
        //         $('.assign-popup').hide()
        //         me.setState({showAssignPopup:{}})
        //     }
        // })
    }
    componentWillReceiveProps(newProps) {
        var projectList = []
        if(newProps.campaigns.projectHtmlData!==this.props.campaigns.projectHtmlData && newProps.campaigns.htmlFetchError!==true && this.state.draggedOverCol==5){
            let project = newProps.campaigns.projectHtmlData;
            if(project.html){
                this.generatePdf(project)
            }else{
                this.props.notie.alert("error","Cannot sign off this project as no magazine found.",3);
            }
        }

        if ((this.props.campaigns.contentProjectList !== newProps.campaigns.contentProjectList &&newProps.campaigns.contentProjectList.length > 0) || fetchAllProjectApi==false) {
            fetchAllProjectApi=true
            projectList = newProps.campaigns.contentProjectList
            var assignProjectWise={};
            for (let index = 0; index < projectList.length; index++) {
                const projectid = projectList[index].identity;
                assignProjectWise={...assignProjectWise,[`projectAssignee_${projectid}`]:projectList[index].assignee}
            }
            this.setState({
                projects: projectList, filter_state: assignProjectWise,allProjectAssignee:assignProjectWise 
            })
            $().then(jQuery =>{
                let $ = jQuery.$;
               $('.production-view-wrapper .kanban-view-wrapper div .kanban-rq').each(function (index, value) {
                      $(this).children('.kanban-process-block').last().addClass('showtotop');
                      $(this).children('.kanban-process-block').first().removeClass('showtotop');
                      if($(this).children('.kanban-process-block').first()[0] && $(this).children('.kanban-process-block').last()[0]){
                        if($(this).children('.kanban-process-block').first()[0].id==$(this).children('.kanban-process-block').last()[0].id){
                            $(this).children('.kanban-process-block').first().last().addClass('lastChildKanban');
                        }
                      }
                    //   $(this).children('.kanban-process-block').first().last().addClass('lastChildKanban');
                 
                });
             })
          
        }
        if(newProps.campaigns.editProjectLoading !== this.props.campaigns.editProjectLoading && newProps.campaigns.editProjectLoading== false){
            this.setState({loading:false})
            var campId=newProps.campaign != undefined ? newProps.campaign.identity :'';
            this.props.campaignActions.fetchPlannerProjects(campId,this.props.Moment);
        }
    }
    // this is called when a Kanban card is dragged over a column (called by column)
    handleOnDragEnter(e, stageValue) {
        this.setState({ draggedOverCol: stageValue })
    }

    generatePdf(project) {
        let data = decodeURIComponent(escape(atob(project.html)))
        var string =
          "<html><head></head><body>" + data + "</body></html>";
          this.handlepdf(string,project);
      }
      async handlepdf(string,project) {
        var config = {
          // Specify the input document
          document: string,
          userStyleSheets: [
            {
              uri: "https://magazine.visibly.io/style.css"
            },
            {
              // uri: "https://www.w3schools.com/w3css/4/w3.css"
            }
          ],
          keepDocument: true
        };
        // Render document and save results
        try {
          var pdfReactor = await new PDFreactor("https://magazine.visibly.io/service/rest");
          config.author = project.owner.firstname+ " "+project.owner.lastname;
          config.title = project.title;
          config.keepDocument = true;
    
          var result = await pdfReactor.convertAsync(config);
          var pdfDocument = pdfReactor.getDocumentUrl(result) + ".pdf";
          let campId = this.props.campaign.identity
          let contentProjectId = project.project_identity;
          this.props.campaignActions.editContentProject("signoff",pdfDocument,campId,contentProjectId)
        } catch (error) {
            this.props.notie.alert("error","Error generating pdf.",3);
        }
      }

      handleCardClick(projectData){
        if(projectData.status!==5){
            this.props.handleEditCopy(projectData.type,projectData.identity,"edit");
        }
      }
      /**
       * @author disha
       * when task count is clicked then to display task list of project
       * @param {*} projectData 
       */
      handleTaskClick(projectData){
          // set flag to open task tab in content project page
        this.props.campaignActions.displayTasksOfContent(true)
       this.handleCardClick(projectData)
      }

    // this is called when a Kanban card dropped over a column (called by card)
    handleOnDragEnd(e, project) {
        if(this.state.draggedOverCol!==project.status){
            var campId=this.props.campaign != undefined ? this.props.campaign.identity :'';
            if(this.state.draggedOverCol == 5){

                if(project.User.data.identity == this.props.profile.identity || this.props.roleName=='super-admin'){
                    this.setState({
                        loading:true
                    })
                    this.props.campaignActions.getContentProjectHtml(project.identity);
                }else{
                    this.props.notie.alert('error',"You don't have permission to move in this stage", 5);
                    this.setState({ draggedOverCol: project.status })
                }
            }else{
                this.props.campaignActions.editContentProject('status',this.state.draggedOverCol,campId,project.identity)
            }
        }
    }
    /*
     * The Kanban Board Column React component
     */
    generateKanbanCards(projects,column) {
        return projects.slice(0).map((projectData ,index)=> {
            return (
                <div
                    draggable={projectData.status !== 5}
                    onDragEnd={e => {
                        this.handleOnDragEnd(e, projectData)
                    }}
                    id={`kanban-card-${column.stage}-${index}`}
                    className = {`kanban-process-block ${column.stage==0||column.stage==1?'left-kanban-block' :''} ${column.stage==4||column.stage==5?'right-kanban-block' :''} `}
                >
                    <div className={`inner-kanban-process-block projectDiv-${projectData.identity}`} >
                     <div className = "title" onClick={this.handleCardClick.bind(this,projectData)} >{projectData.type == 'print' ? (
                            <i class='material-icons'>text_fields</i>
                        ) : projectData.type == 'email' ? (
                            <i class='material-icons'>mail_outline</i>
                        ) : (
                                    <i class='material-icons'>mail_outline</i>
                                )}
                        {projectData.title}
                        </div>
                        <div className="campaignDateCountWapper clearfix">
                            <div class='campaignDateWrapper' onClick={this.handleCardClick.bind(this,projectData)}>
                                <span class='campaignDateIcon'>
                                    <svg
                                        xmlns='http://www.w3.org/2000/svg'
                                        width='24'
                                        height='24'
                                        viewBox='0 0 24 24'
                                    >
                                        <path fill='none' d='M0 0h24v24H0V0z' />
                                        <path d='M20 3h-1V1h-2v2H7V1H5v2H4c-1.1 0-2 .9-2 2v16c0 1.1.9 2 2 2h16c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zm0 18H4V10h16v11zm0-13H4V5h16v3z' />
                                    </svg>
                                </span>
                                <span class='campaignStartDate'>
                                    {this.props.Moment
                                        ? this.props.Moment.unix(parseInt(projectData.end_date)).format(
                                            'DD-MM-YYYY'
                                        )
                                        : ''}
                                </span>
                            </div>
                            <div class='countWrapper' onClick={this.handleTaskClick.bind(this,projectData)}>
                                <span class='kanban-task-icon'>
                                    <svg xmlns="http://www.w3.org/2000/svg" height="13" viewBox="0 0 24 24" width="13"><path d="M0 0h24v24H0V0z" fill="none"/><path d="M7 15h7v2H7zm0-4h10v2H7zm0-4h10v2H7zm12-4h-4.18C14.4 1.84 13.3 1 12 1c-1.3 0-2.4.84-2.82 2H5c-.14 0-.27.01-.4.04-.39.08-.74.28-1.01.55-.18.18-.33.4-.43.64-.1.23-.16.49-.16.77v14c0 .27.06.54.16.78s.25.45.43.64c.27.27.62.47 1.01.55.13.02.26.03.4.03h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zm-7-.25c.41 0 .75.34.75.75s-.34.75-.75.75-.75-.34-.75-.75.34-.75.75-.75zM19 19H5V5h14v14z"/></svg>
                                
                                </span>
                                <span class='kanban-task-count'>
                                    {projectData.task_count}
                                </span>
                            </div>
                        </div>
                        <div className='assign-author multiple-assign-author'>
                            <div className='multiple-assign'>
                                    { projectData.assignee&& projectData.assignee.length > 0
                                        ? projectData.assignee.map((projectUsers, i) => {
                                            if (i < limitCampUser) {
                                                return (
                                                    <div
                                                        key={`assignee_` + i}
                                                        className='profile-wrapper assignAuthorProfile post'
                                                        onClick={this.showAssignPopup.bind(this,projectData.identity)}
                                                    >
                                                        <span className='author-image author-thumb'>
                                                            <img
                                                                src={projectUsers.avatar}
                                                                alt={'Default avatar'}
                                                                className='list-avatar thumbnail'
                                                                data-tip={projectUsers.email}
                                                                data-for={projectUsers.id}
                                                                onError={e => {
                                                                    e.target.src =
                                                                        'https://app.visibly.io/img/default-avatar.png'
                                                                }}
                                                            />
                                                            <i class='material-icons add-icon'>add</i>
                                                            <FetchuserDetail userID={projectUsers.id}/>
                                                        </span>
                                                    </div>
                                                )
                                            }
                                        })
                                        : ''}
                                    {projectData.assignee&& projectData.assignee.length > 0 &&
                                        projectData.assignee.length > limitCampUser ? (
                                            <div
                                                className='profile-wrapper assignAuthorWrapper'
                                                onClick={this.showAssignPopup.bind(this,projectData.identity)}
                                            >
                                                <span className='author-image'>
                                                    <span className='noCount'>
                                                        <i class='material-icons'>add</i>
                                                        {Math.abs(
                                                            limitCampUser - projectData.assignee.length
                                                        )}
                                                    </span>
                                                </span>{' '}
                                            </div>
                                        ) : (
                                            ''
                                        )}
                                </div>
                                { this.state.showAssignPopup && this.state.showAssignPopup[`assigneePopup_`+projectData.identity] == true ? (
                                    <div className='assign-popup'>
                                        <div className='search-input'>
                                            <input
                                                placeholder='Search Name'
                                                id='searchUser'
                                                type='text'
                                                name='searchUser'
                                                onChange={this.handleSearchValue.bind(this,projectData.identity)}
                                            />
                                        </div>
                                        <div className='assign-popup-block-wrapper'>
                                            {this.state.filter_state[`projectAssignee_${projectData.identity}`]!== undefined && this.state.filter_state[`projectAssignee_${projectData.identity}`].length > 0
                                                ? this.state.filter_state[`projectAssignee_${projectData.identity}`].map((projectUsers, i) => {
                                                    return (
                                                        <div
                                                            className={`assign-block`}
                                                            key={`assignUserPopup_` + i}
                                                        >
                                                            <div className='profile-wrapper assignAuthorProfile'>
                                                                <span className='author-image author-thumb'>
                                                                    <img
                                                                        src={projectUsers.avatar}
                                                                        alt={'Default avatar'}
                                                                        className='list-avatar'
                                                                        data-tip={projectUsers.email}
                                                                        data-for={projectUsers.id}
                                                                        onError={e => {
                                                                            e.target.src =
                                                                                'https://app.visibly.io/img/default-avatar.png'
                                                                        }}
                                                                    />
                                                                    <i class='material-icons add-icon'>add</i>
                                                                </span>
                                                            </div>
                                                            <span className='auhtor-name'>
                                                                {' '}
                                                                {projectUsers.first_name +
                                                                    ' ' +
                                                                    projectUsers.last_name}{' '}
                                                            </span>
                                                        </div>
                                                    )
                                                })
                                                : <div className='NovalueFound'>
                                                    <li className = "no-Value-chart-wrapper"> No assignee found.</li>
                                                    
                                                </div>}
                                        </div>
                                    </div>
                                ) : (
                                        ''
                                    )}
                            </div>
                    </div>
                </div>
            )
        })
    }
    showAssignPopup(projectId) {
        var flag=false;
        if(this.state.showAssignPopup !== undefined && this.state.showAssignPopup[`assigneePopup_`+projectId] !== undefined ){
            flag=this.state.showAssignPopup[`assigneePopup_`+projectId]
        }else{
            flag=false
        }
        this.setState({
            showAssignPopup:{[`assigneePopup_`+projectId]:!flag}
        })
    }
    handleSearchValue(projectId,e) {
        if (e !== null) {
            if (e.target.value !== '' && this.state.allProjectAssignee[`projectAssignee_${projectId}`] !== undefined) {
                var i = 0
                search = e.target.value.toLowerCase()
                var tempobj = this.state.allProjectAssignee[`projectAssignee_${projectId}`].filter(function (temp) {
                            i = i + 1
                            if (
                                temp.first_name.toLowerCase().includes(search) ||
                                temp.last_name.toLowerCase().includes(search)
                            ) {
                                return true
                            }
                })
                this.setState({ 
                    filter_state:{...this.state.filter_state,[`projectAssignee_${projectId}`]:tempobj}
                })
            } else {
                this.setState({ filter_state: this.state.allProjectAssignee })
            }
        }
    }

    render() {
        const style = {
            padding: '30px',
            paddingTop: '5px'
        }
        const columnStyle = {
            display: 'inline-block',
            verticalAlign: 'top',
            marginRight: '5px',
            marginBottom: '5px',
            paddingLeft: '5px',
            paddingTop: '0px',
            width: '230px',
            textAlign: 'center',
            backgroundColor: this.state.mouseIsHovering ? '#d3d3d3' : '#f0eeee'
        }
        this.columns =
            this.props.campaigns.kanbanStatusList.length > 0
                ? this.props.campaigns.kanbanStatusList
                : []
        return (
            // this.state.loading?
            // <PreLoader className = "Page" /> :
            <div style={style}>
                {this.columns.length > 0
                    ? this.columns.map(column => {
                        var projects =
                            this.state.projects !== undefined
                                ? this.state.projects.filter(project => {
                                    return parseInt(project.status, 10) === column.stage
                                })
                                : []
                        return (
                            <div
                            className="kanban-rq"
                                style={columnStyle}
                                onDragEnter={e => {
                                    this.setState({ mouseIsHovering: true })
                                    this.handleOnDragEnter(e, column.stage)
                                }}
                                onDragExit={e => {
                                    this.setState({ mouseIsHovering: false })
                                }}
                            >
                                <h4>
                                    {column.name} <span className="kanban-project-length">{projects.length}</span>
                </h4>
                                {this.generateKanbanCards(projects,column)}
                                <br />
                            </div>
                        )
                    })
                    : ''}
            </div>
        )
    }
}
