// import Script from "react-load-script";
import GrapesJs from '../../components/Chunks/ChunkGrapesJs';
import GrapesJsNewsLetter from '../../components/Chunks/ChunkGrapesJsNewsLetter';
import GrapesJsCustomCode from '../../components/Chunks/ChunkGrapesJsCustomCode';
import GrapesJsActions from '../../components/Chunks/ChunkGrapesJsActions';
import GrapesJsPostCss from '../../components/Chunks/ChunkGrapesJsPostCss';
import GrapesJsExport from '../../components/Chunks/ChunkGrapesJsExport';
import GrapesJsFilter from '../../components/Chunks/ChunkGrapesJsFilter';
import GrapesJsGradient from '../../components/Chunks/ChunkGrapesJsGradient';
import GrapesJsTouch from '../../components/Chunks/ChunkGrapesJsTouch';
import GrapesJsImageEditor from '../../components/Chunks/ChunkGrapesJsImageEditor';
import GrapesJsFlexbox from '../../components/Chunks/ChunkGrapesJsFlexbox';
import GrapesJsBasic from '../../components/Chunks/ChunkGrapesJsBasic';
import notie from 'notie/dist/notie.js'
import juice from "juice";
import PreLoader from "../PreLoader";
var renderGrapesJs=false
class ContentWorkSpace extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      scriptError: false,
      scriptLoaded: false,
      html:null,
      htmlPreview:null,
      grapesJs: null,
      grapesJsNewsLetter: null,
      grapesJsCustomCode : null,
      grapesJsActions : null,
      grapesJsPostCss : null,
      grapesJsExport : null,
      grapesJsFilter : null,
      grapesJsGradient : null,
      grapesJsTouch : null,
      grapesJsImageEditor : null,
      grapesJsFlexbox : null,
      grapesJsBasic : null,
      pluginLoader:false
    };
  }
  componentWillMount() {
    var me = this
    me.setState({
      pluginLoader: true
    })
    GrapesJs().then(grapesjs => {
      GrapesJsNewsLetter().then(newsLetter => {
        GrapesJsCustomCode().then(custom => {
          GrapesJsActions().then(actions => {
            GrapesJsPostCss().then(post => {
              GrapesJsExport().then(exp => {
                GrapesJsFilter().then(filter => {
                  GrapesJsGradient().then(gradient => {
                    GrapesJsTouch().then(touch => {
                      GrapesJsImageEditor().then(image => {
                        GrapesJsFlexbox().then(flex => {
                          GrapesJsBasic().then(basic => {
                            me.setState({
                              grapesJs: grapesjs, grapesJsNewsLetter: newsLetter,
                              grapesJsCustomCode: custom,
                              grapesJsActions: actions,
                              grapesJsPostCss: post,
                              grapesJsExport: exp,
                              grapesJsFilter: filter,
                              grapesJsGradient: gradient,
                              grapesJsTouch: touch,
                              grapesJsImageEditor: image,
                              grapesJsFlexbox: flex,
                              grapesJsBasic: basic,
                            }, () => { renderGrapesJs = true })
                          })
                        })
                      })
                    })
                  })
                })
              })
            })
          })
        })
      })
    })
  }
  shouldComponentUpdate(nextProps, nextState) {
    if(nextState.grapesJs !== this.state.grapesJs && nextState.grapesJs !== null || renderGrapesJs == true){
     //i have changed below state separately bcz when i added pluginLoader with html,htmlPreview state then it was taking too much time to reflect.
      this.setState({
        pluginLoader: false
    })
      renderGrapesJs=false // this flag is one kind of jugad to restrict this block will execute only and only one time
      this.setState({
        html:this.props.contentProjectData.html,
        htmlPreview:this.props.contentProjectData.html_preview,
    })
    let htmlString=btoa((unescape(encodeURIComponent('<div></div>'))));
    if(this.props.htmlContent){
        htmlString =  decodeURIComponent(escape(atob(this.props.htmlContent)));
    }else{
        let htmlToConvert = this.props.contentProjectData.html!==null && this.props.contentProjectData.html !==''? this.props.contentProjectData.html:btoa((unescape(encodeURIComponent('<div></div>'))))
        htmlString =  decodeURIComponent(escape(atob(htmlToConvert)));
    }
      this.fetchGrapesInit(htmlString,nextState)
      return true;
    }
    return true;
  }
  fetchGrapesInit(htmlString,currState){
    const editor = currState.grapesJs.init({
      // container : {components:htmlString},
      container: "#gjs",
      components:htmlString,
      plugins: [
        currState.grapesJsNewsLetter.default,
        currState.grapesJsBasic.default,
        currState.grapesJsFlexbox.default,
        currState.grapesJsCustomCode.default,
        currState.grapesJsPostCss.default,
        currState.grapesJsExport.default,
        currState.grapesJsFilter.default,
        currState.grapesJsGradient.default,
        currState.grapesJsActions.default,
        currState.grapesJsTouch.default,
        currState.grapesJsImageEditor.default
      ],
      pluginsOpts: {
        basicBlocks: {
           blocks:['column1', 'column2', 'column3', 'column3-6', 'text', 'image', 'video'],
           },
            [currState.grapesJsNewsLetter.default]:{},
            [currState.grapesJsFlexbox.default]:{},
            [currState.grapesJsCustomCode.default]:{},
            [currState.grapesJsPostCss.default]:{},
            [currState.grapesJsFilter.default]:{},
            [currState.grapesJsGradient.default]:{},
            [currState.grapesJsActions.default]:{},
            [currState.grapesJsTouch.default]:{},
            [currState.grapesJsImageEditor.default]:{},
        exportWrapper: {
          addExportBtn: true,
          btnLabel: "export",
          filename: "demo"
        },
      }
    });
    editor.AssetManager.render([]);
    let images = this.props.campaignAssetList;
    editor.AssetManager.add(images);
    var assets = editor.AssetManager.getAll() // <- Backbone collection
    editor.on("run:open-assets", function () {
      editor.AssetManager.render(images);
    })
    editor
      .getComponents()
      .add('<link rel="stylesheet" href="https://magazine.visibly.io/style.css" />');
    editor
      .getComponents()
      .add(
        '<link href="https://fonts.googleapis.com/css?family=Montserrat:200,300,400,500,700,800,900" rel="stylesheet" />'
      );
    
      var commands = editor.Commands;
      var pnm = editor.Panels;
      pnm.addButton('options', [{
        id: 'preview',
        className: 'fa fa-eye',
        attributes: {
         title: 'Preview',
         'data-tooltip-pos': 'bottom'
        },
        command: function () {
         return editor.runCommand('preview')
        }
       }])
      //   commands.add('getHtmlBtn', {
      //       run: function(editor, senderBtn) {
      //         // Deactivate button
      //         senderBtn.set("active", false);
      //         var html = editor.runCommand('gjs-get-inlined-html');
      //         mail.saveTemplate(html); // my save function
      //  },

      //   });
        var email_html = editor.getHtml();
        var email_css = editor.getCss();
        editor.runCommand('getHtmlBtn');
      // }
  }

  handleScriptCreate() {
    this.setState({ scriptLoaded: false });
  }
  handleScriptError() {
    this.setState({ scriptError: true });
  }
  async handleScriptLoad() {
    this.setState({ scriptLoaded: true });
  }


  generatePdf(from=null) {
    var data = document
      .getElementsByClassName("gjs-frame")[0]
      .contentWindow.document.getElementById("wrapper");
    var string =
      "<html><head></head><body>" + data.innerHTML + "</body></html>";
      this.handlepdf(string,from);
  }
  async handlepdf(string,from) {
    var config = {
      // Specify the input document
      document: string,
      userStyleSheets: [
        {
          uri: "https://magazine.visibly.io/style.css"
        },
        {
          // uri: "https://www.w3schools.com/w3css/4/w3.css"
        }
      ],
      keepDocument: true
    };

    // Render document and save results
    try {
      var pdfReactor = await new PDFreactor("https://magazine.visibly.io/service/rest");
      config.author = this.props.contentProjectData.owner.firstname+ " "+this.props.contentProjectData.owner.lastname;
      config.title = this.props.contentProjectData.title;
      config.keepDocument = true;
      //  config.setKeepDocument(true);

      var result = await pdfReactor.convertAsync(config);
      var pdfDocument = pdfReactor.getDocumentUrl(result) + ".pdf";
      let targetHtml = document.getElementsByClassName("gjs-frame")[0].contentWindow.document.getElementById("wrapper");
      let html_preview= btoa((unescape(encodeURIComponent(targetHtml.firstChild.innerHTML))));
      let styles = localStorage.getItem('gjs-css');
        let styleElement = document.createElement('style');
        styleElement.type = 'text/css';

        if (styleElement.styleSheet)
            styleElement.styleSheet.cssText = styles;
        else
            styleElement.appendChild(document.createTextNode(styles));
        targetHtml.appendChild(styleElement);
      let htmlForDraft = btoa((unescape(encodeURIComponent(targetHtml.innerHTML))));
      if(from!==null && from =="Draft"){
        this.props.saveTemplateDraft(htmlForDraft, html_preview,pdfDocument);
      }else{
        this.props.handleSignOff(pdfDocument,html_preview,this.props.contentType );
      }
    } catch (error) {
        notie.alert("error","Error generating pdf.",3);
    }
  }
 componentWillReceiveProps(nextProps){
    if(nextProps.callSignOffParent!==this.props.callSignOffParent && nextProps.callSignOffParent==true){
        if(nextProps.contentProjectData.html!==null){
           if(this.props.contentType == "email") {
            let pdfDocument = null;
                let targetHtml = document.getElementsByClassName("gjs-frame")[0].contentWindow.document.getElementById("wrapper");
                let styles = localStorage.getItem('gjs-css');
                let styleElement = document.createElement('style');
                styleElement.type = 'text/css';

                if (styleElement.styleSheet)
                    styleElement.styleSheet.cssText = styles;
                else
                    styleElement.appendChild(document.createTextNode(styles));
                targetHtml.appendChild(styleElement);
                let html_preview= btoa((unescape(encodeURIComponent(targetHtml.innerHTML))));
                let htmlForDraft = btoa((unescape(encodeURIComponent(targetHtml.innerHTML))));
                this.props.handleSignOff(pdfDocument,html_preview,this.props.contentType );
           }else{
            this.generatePdf();
           }

        }else{
            notie.alert("error","Cannot sign off this project as no magazine found.",3);
        }
    }
  }
  addUser(type, projectid ){
    this.props.openAddUserPopup(type,projectid);
  }
  saveHtml(){
    // let targetHtml = document.getElementsByClassName("gjs-frame")[0].contentWindow.document.getElementById("wrapper").innerHTML;
    // let encodedString = btoa((unescape(encodeURIComponent(targetHtml))));
    let targetHtml = document.getElementsByClassName("gjs-frame")[0].contentWindow.document.getElementById("wrapper");
    let styles = localStorage.getItem('gjs-css');
    let styleElement = document.createElement('style');
    styleElement.type = 'text/css';

    if (styleElement.styleSheet)
        styleElement.styleSheet.cssText = styles;
    else
        styleElement.appendChild(document.createTextNode(styles));

        console.log(targetHtml.innerHTML);
        console.log(styles);

        var result = juice('<style>'+styles+'</style>'+targetHtml.innerHTML);
    console.log(result,'result');
    //targetHtml.appendChild(styleElement);

    let html = btoa((unescape(encodeURIComponent(result))));
    let html_preview = '';
    if(this.props.campaignType == "email"){
        html_preview= btoa((unescape(encodeURIComponent(result))));
    }else{
        html_preview= btoa((unescape(encodeURIComponent(targetHtml.firstChild.innerHTML))));
    }
    //console.log('html0001>>'+html);
    this.setState({
        html:html,
        htmlPreview:html_preview
    })
    this.props.saveTemplateHtml(html,html_preview,this.props.contentType);
  }
  saveDraft(){
    let targetHtml = document.getElementsByClassName("gjs-frame")[0].contentWindow.document.getElementById("wrapper");

    // let html = btoa((unescape(encodeURIComponent(targetHtml.innerHTML))));
    // let html_preview= btoa((unescape(encodeURIComponent(targetHtml.firstChild.innerHTML))));
    if(this.props.contentType == "email"){
            let pdfDocument = null;
            let styles = localStorage.getItem('gjs-css');
            let styleElement = document.createElement('style');
            styleElement.type = 'text/css';

            if (styleElement.styleSheet)
                styleElement.styleSheet.cssText = styles;
            else
              styleElement.appendChild(document.createTextNode(styles));
            // targetHtml.appendChild(styleElement);
            var result = juice('<style>'+styles+'</style>'+targetHtml.innerHTML);
            let html_preview= btoa((unescape(encodeURIComponent(targetHtml.innerHTML))));
            let htmlForDraft = btoa((unescape(encodeURIComponent(result))));
            this.props.saveTemplateDraft(htmlForDraft,html_preview,pdfDocument,this.props.contentType);
        }else{
            this.generatePdf("Draft");
        }
  }
  handleSignOff(){
    if( this.state.html !== null){
         if(this.props.contentType == "email") {
            let pdfDocument = null;
            let targetHtml = document.getElementsByClassName("gjs-frame")[0].contentWindow.document.getElementById("wrapper");

            let styles = localStorage.getItem('gjs-css');
            let styleElement = document.createElement('style');
            styleElement.type = 'text/css';

            if (styleElement.styleSheet)
                styleElement.styleSheet.cssText = styles;
            else
              styleElement.appendChild(document.createTextNode(styles));
            // targetHtml.appendChild(styleElement);
            var result = juice('<style>'+styles+'</style>'+targetHtml.innerHTML);
            let html_preview= btoa((unescape(encodeURIComponent(targetHtml.innerHTML))));
            this.props.handleSignOff(pdfDocument,result,this.props.contentType );
          }else{
            this.generatePdf();
          }
    }else{
        notie.alert("error","Cannot sign off this project as no magazine found.",3);
    }
  }
  handleSendReview(){
    if(this.props.campaigns.currContentProject.type == "email" ){
        if(this.props.campaigns.currContentProject.email_preview == null && this.props.campaigns.currContentProject.from_email == null || this.props.campaigns.currContentProject.subject == null){
            notie.alert("error","Please add recipients first in order to send for review.",3);
        }else{
            let targetHtml = document.getElementsByClassName("gjs-frame")[0].contentWindow.document.getElementById("wrapper");
            let styles = localStorage.getItem('gjs-css');
            let styleElement = document.createElement('style');
            styleElement.type = 'text/css';

            if (styleElement.styleSheet)
                styleElement.styleSheet.cssText = styles;
            else
              styleElement.appendChild(document.createTextNode(styles));
            // targetHtml.appendChild(styleElement);
            var result = juice('<style>'+styles+'</style>'+targetHtml.innerHTML);
            this.props.handleSendReview(result);
        }
    }else{
        let targetHtml = document.getElementsByClassName("gjs-frame")[0].contentWindow.document.getElementById("wrapper");
        let html_preview= btoa((unescape(encodeURIComponent(targetHtml.firstChild.innerHTML))));
        this.props.handleSendReview(html_preview);
    }
  }
 render() {
    var projectIdentity = this.props.campaigns.currContentProject.project_identity;

      var result = this.props.contentProjectData.asssignee.filter(obj => {
        return obj.identity === this.props.profile.identity
      })
      var OwnerIsAsignee = false;
      if(result.length>0)
      {  OwnerIsAsignee =true;  }
      const pluginsLoading =  this.state.pluginLoader
    return (
      <div>
        {pluginsLoading ? <PreLoader /> :''}
        <div id="gjs" />
        <div className = "print-btn-wrapper">
            <div className  = "left-btn">
            {this.props.profile.identity !== this.props.contentProjectData.owner.identity && this.props.roleName !=='super-admin'?
            <button
                className="btn btn-theme"
                onClick={this.handleSendReview.bind(this)}>
                Send for Review
            </button>
            :''}
            {(this.props.profile.identity == this.props.contentProjectData.owner.identity || this.props.roleName =='super-admin') &&
             (this.props.contentProjectData.status == 4 || OwnerIsAsignee == true ) ?
            <button
                className={`btn btn-theme ${this.props.contentProjectData.html == null ? 'disabled':''}`}
                onClick={this.handleSignOff.bind(this)}
            >
                Sign off
            </button>:''}

            {  this.props.contentProjectData.status == 4 && (this.props.profile.identity == this.props.contentProjectData.owner.identity || this.props.roleName =='super-admin')?
            <button
                className="btn btn-theme"
                onClick={this.props.handleChangeProjectRequest}
            >
                Request for change
            </button>:''}
          </div>
          <div className ="right-btn">
               {this.props.contentType == "email" ?
               <button
                  className="btn btn-theme save-btn email-btn-add-user"
                  onClick={this.addUser.bind(this, this.props.contentType,projectIdentity )}
                >
                 Add Recipients
                </button>
               :''}
                <button
                    className="btn btn-theme save-as-tmp-btn"
                    onClick={this.saveDraft.bind(this)}
                >
                    Save as Template
                </button>

                <button
                  className="btn btn-theme save-btn"
                  onClick={this.saveHtml.bind(this)}
                >
                  Save
                </button>

            </div>
        </div>
        {/* {this.state.scriptLoaded !== true ? (
          <Script
            url="http://www.pdfreactor.com/product/wrappers/javascript/lib/PDFreactor.js"
            onCreate={this.handleScriptCreate.bind(this)}
            onError={this.handleScriptError.bind(this)}
            onLoad={this.handleScriptLoad.bind(this)}
          />
        ) : null} */}
      </div>
    );
  }
}

export default ContentWorkSpace;