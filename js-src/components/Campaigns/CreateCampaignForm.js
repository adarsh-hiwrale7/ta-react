import { bindActionCreators } from 'redux'
import { browserHistory } from 'react-router'
import { connect } from 'react-redux'
import { Field, reduxForm } from 'redux-form'
import { Link } from 'react-router'
import { renderField } from '../Helpers/ReduxForm/RenderField'

import * as campaignsActions from '../../actions/campaigns/campaignsActions'
import * as channelActions from '../../actions/socialChannelActions'
import * as tagsActions from '../../actions/tagsActions'
import * as utils from '../../utils/utils'
import * as userActions from '../../actions/userActions'


import DataRow from '../DataRow'
import Globals from '../../Globals'
import moment from '../Chunks/ChunkMoment'
import MentionTextArea from '../Helpers/ReduxForm/MentionTextArea'
import notie from 'notie/dist/notie.js'
import ReactDatePicker from '../Helpers/ReduxForm/ReactDatePicker' // react datepicker field
import reactDatepickerChunk from '../Chunks/ChunkDatePicker' // datepicker chunk

const required = value => (value ? undefined : 'Required')
const validEmail = value =>
  (value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
    ? 'Invalid email address'
    : undefined)
// const hashtagValidation =value => (value && /^@\[\s\b(\w+)\b]\(([^)]+)\)$|^#[\w]+(?:\s#[\w]+)*$/g.test(value) ?undefined: 'Must be a valid single hashtag');
const hashtagValidation =value => (value == undefined  || value == '' || ( value && /^(@\[([^\]]+)]\(([^)]+)\)|#[\w]+)+(?:\s#[\w]+|\s@\[([^\]]+)]\(([^)]+)\)|\s)*$/g.test(value)) ?undefined: 'Must be a valid single hashtag' );
class CreateCampaignForm extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      startDate: null,
      ReactDatePicker: null,
      // terminateStatus: parseInt(
      //   typeof props.campaign !== 'undefined' ? props.campaign.terminated : 1
      // ),
      Moment: null,
      Social: [],
      Users: [],
      selectFieldManager: null,
      selectFieldCampaignType: null,
      SelectedSocial: [],
      selected: null,
      done: false,
      showPostCreationButton: false,
      mounted: true,
      dropvalue: null,
      checked: false,
      notShow: false,
      campaignTypeVal: '',
      wallCheckbox: [],
      checkvalue: '',
      checkboxValue: false,
      access: '',
      checkboxDisabled: false,
      test: ''
    }

    this.externalCheckboxes = []
    this.internalCheckBoxes = ['internal']

    this.channelsFetched = false
    this.optionCampaignTypeSelected.bind(this);
    this.handleMentionFocus = this.handleMentionFocus.bind(this)
  }

  componentWillMount () {
    var me = this
    moment().then(moment => {
      reactDatepickerChunk()
        .then(reactDatepicker => {
          if (me.state.mounted) {
            this.setState({
              ReactDatePicker: reactDatepicker,
              Moment: moment,
              showPostCreationButton: true // show post creation button only after everything is loaded
            })
            // this.handleInitialize(); //added by yamin and due to this when we are not changing anything in form of update campaign then checkbox is required error will occured
          }
        })
        .catch(err => {
          throw err
        })

    })

    var newLocation = utils
        .removeTrailingSlash(this.props.location.pathname)
        .split('/')
    /** call tag */
    this.props.tagsActions.fetchUserTags(newLocation)
    this.props.tagsActions.fetchPostTags('all')
    /** Check permission */
  }

  componentWillUnmount () {
    this.setState({
      mounted: false
    })
  }

  componentDidMount () {
    this.props.tagsActions.fetchTags('campaign')
    let selectedChannels = typeof this.props.campaign === 'undefined'
      ? null
      : this.props.campaign.SocialChannel
    this.props.channelActions.fetchChannels(selectedChannels)
    this.props.userActions.fetchUsers()
    this.handleInitialize(this.props)
  }

  componentWillReceiveProps (nextProps) {
    if (typeof nextProps.channels.selectedChannels !== 'undefined' && JSON.stringify(nextProps.channels.selectedChannels) !==JSON.stringify(this.props.channels.selectedChannels)) {
      this.handleInitialize(nextProps)
    }
    let roleName =JSON.parse(localStorage.getItem('roles')) !== null? JSON.parse(localStorage.getItem('roles'))[0].role_name:''
    if (nextProps.editdata == false || nextProps.campaigns.creation.created) {
      this.setState({ notShow: true })
    } else {
      this.setState({ notShow: false })
    }
  }

  handleInitialize (nextProps = null) {
    

    var tags = null
    var selectedSocialChannels = this.props.channels.selectedChannels.slice()
    if (nextProps !== null) {
      selectedSocialChannels = nextProps.channels.selectedChannels.slice()
    }
    this.setState({
      wallCheckbox: selectedSocialChannels
    })
    if (typeof this.props.campaign !== 'undefined') {
      this.props.campaign.Tags.data.forEach(function (tag, i) {
        if (tags != null) {
          tags = tags + tag.identity + ','
        } else {
          tags = tag.identity + ','
        }
      })
      var hashTagDetail=[]
      if(this.props.campaign.hashtag !== null){
        for (let index = 0; index < this.props.campaign.hashtag.hashtag.length; index++) {
          const element = this.props.campaign.hashtag.hashtag[index];
          hashTagDetail.push({
            'display': element.title,
            'identity': element.hashtag_identity
          })
        }
      }
      const initData = {
        title: this.props.campaign.title,
        description: this.props.campaign.description,
        message: this.props.campaign.message,
        campaignType: this.props.campaign.campaign_type,
        objective: this.props.campaign.campaign_objective,
        audience: this.props.campaign.targeted_audience,
        manager: this.props.campaign.User.data.identity,
        startdate: this.props.campaign.start_date,
        enddate: this.props.campaign.end_date,
        tags: tags,
        campaignHashtag:this.props.campaign.hashtag !== null
        ? utils.replaceTags(
            this.props.campaign.hashtag.detail,
            [],
            hashTagDetail ? hashTagDetail: []
          )
        : ''
      }
      if (this.props.campaign.campaign_type == 'Internal') {
        this.setState({
          checkboxDisabled: 'internal'
        })
      } else {
        this.setState({
          checkboxDisabled: 'external'
        })
      }
      for (let i = 0; i < this.props.channels.channels.length; i++) {
        initData[
          `chk-${this.props.channels.channels[i].media.toLowerCase()}-${this.props.channels.channels[i].identity}`
        ] =
          selectedSocialChannels.indexOf(
            this.props.channels.channels[i].media.toLowerCase()
          ) > -1
      }
      this.props.initialize(initData)
    }else{
      
        
      if(this.state.Moment !== null){
        const initData = {
          startdate: this.state.Moment().unix(),
          enddate: this.state.Moment().add(1, 'months').unix(),
        }
        this.props.initialize(initData)
      }
      
    }
  }
  /**
   * @author Meghal Patel
   * @use Validation of user tags
   * @param {*} e 
   */
  valiDate(e) 
  {
    var temp='',count=0,count2=0,flag=0;
    const len=Object.keys(e).length-1;

    for(var i=0;i<Object.keys(e).length-1;i++){
      //for loop is for checking the whole string every time it is updated
      temp=e[i];
      if(e[i]=="@"){count+=1}  //count of at the rate
      if((e[i]==")"&&e[i+1]==" "&& e[i+2]=="@" && flag!==2)){ //count of )" "@
        count2+=1;flag=1;
      }
      if((e[i]==")"&&e[i+1]==" "&& e[i+2]!=="@" && e[i+2]!==undefined) || (e[i]==")"&& e[i+1]!=="@" && e[i+1]!==undefined && e[i+1]!==" ")){
        flag=2;
      }
    }
    var first=e[0];
    var last=temp;
    
    temp=e[0]
  if(temp !== undefined){   
    if(first=="@" && last==")" && e[i]==null){
      document.querySelector(".viewper").style.display="none"; 
    }
    else if(first=="@" && last==")" && e[i]==" "){
      if(flag==1){
        if(count==count2+1){
          document.querySelector(".viewper").style.display="none";
        }
        else{
          document.querySelector(".viewper").style.display="inline";
        }
      }
      else{
        document.querySelector(".viewper").style.display="none";
      }
    }
    else{
      document.querySelector(".viewper").style.display="inline";
    }
    if(flag==2 && (e[0]!=='@'|| e[0]!==' ')){
      document.querySelector(".viewper").style.display="inline";
    }
  }
  else
  {
    document.querySelector(".viewper").style.display="none";
  }
    
  }
  optionCampaignTypeSelected (e) {
    var me = this
    var dropvalue = e.target.value
    let socialCheckBoxes = document.querySelectorAll('input.social-checkbox')
    if (dropvalue == 'Internal') {
      var internalCheckbox = document.querySelectorAll(
        'input.social-checkbox.' + dropvalue.toLowerCase()
      )
      me.uncheckAllExternalChannels(socialCheckBoxes)
      me.checkInternalChannel(socialCheckBoxes)
      me.props.change(me.internalCheckBoxes[0], true)
      this.setState({
        wallCheckbox: ['internal']
      })
      me.props.change('type', 'Internal')
      me.setState({
        checkboxDisabled: 'internal'
      })
    } else if (dropvalue == '') {
      me.setState({
        checkboxDisabled: false
      })
    } else {
      me.uncheckInternalChannels()
      me.setState({
        checkboxDisabled: 'external'
      })
      for (let ec = 0; ec < me.externalCheckboxes.length; ec++) {
        if (me.externalCheckboxes[ec] == e.target.value) {
          me.props.change(me.externalCheckboxes[ec], true)
          me.props.change('type', 'External')
        }
      }
    }
  }
  socialCheckboxChanged (e, me, type) {
    let socialCheckBoxes = document.querySelectorAll('input.social-checkbox')

    if (!e.target.checked) {
      // if checkbox is checked again (to uncheck checkbox)
      if (me.internalCheckBoxes[0] == e.target.name) {
        // if internal checkbox is select again then it should uncheck
        me.props.change(me.internalCheckBoxes[0], false)
        this.setState({
          wallCheckbox: []
        })
      }
      for (let ec = 0; ec < me.externalCheckboxes.length; ec++) {
        // to uncheck external checkbox when it is select again
        if (me.externalCheckboxes[ec] == e.target.name) {
          if (
            this.state.wallCheckbox.indexOf(
              me.externalCheckboxes[ec].split('-')[1]
            ) > -1
          ) {
            var index = this.state.wallCheckbox.indexOf(
              me.externalCheckboxes[ec].split('-')[1]
            )
            me.props.change(me.externalCheckboxes[ec], false)
            this.state.wallCheckbox.splice(index, 1)
          }
        }
      }
      // this state is not use in anywhere but i put here bcz my wallcheckbox state can be refelect at a time
      this.setState({
        test: 'hey'
      })
    }

    if (e.target.classList.contains('internal') && e.target.checked) {
      // when internal checkbox is slect and to unselect all external chckboxes
      me.uncheckAllExternalChannels(socialCheckBoxes)
      me.props.change(me.internalCheckBoxes[0], true)
      me.props.change('type', 'Internal')
      me.setState({
        wallCheckbox: ['internal'],
        checkboxValue: true,
        checkvalue: type
      })
    } else {
      me.uncheckInternalChannels()
      // store selected eternal checkboxes in array wallcheckbox and remove internal from array if there is .
      for (let ec = 0; ec < me.externalCheckboxes.length; ec++) {
        if (e.target.checked) {
          if (this.state.wallCheckbox.indexOf('internal') > -1) {
            wallCheckboxArray = this.state.wallCheckbox
            var index = wallCheckboxArray.indexOf('internal')
            wallCheckboxArray.splice(index, 1)
            this.setState({ wallCheckbox: wallCheckboxArray })
          }
          me.setState({
            wallCheckbox: [
              ...this.state.wallCheckbox,
              e.target.name.split('-')[1]
            ]
          })
        } else {
          if (me.externalCheckboxes[ec] == e.target.name && e.target.checked) {
            var wallCheckboxArray = this.state.wallCheckbox
            var index = wallCheckboxArray.indexOf(e.target.name.split('-')[1])
            wallCheckboxArray.splice(index, 1)
            this.setState({
              wallCheckbox: wallCheckboxArray,
              checkvalue: type,
              checkboxValue: true
            })
          }
        }
        if (me.externalCheckboxes[ec] == e.target.name && e.target.checked) {
          me.props.change(me.externalCheckboxes[ec], true)
          me.props.change(me.internalCheckBoxes[0], false)
          me.props.change('type', 'External')
        }
      }
    }
  }

  uncheckAllExternalChannels (checkboxes) {
    // to uncheck all external channels
    for (let sc = 0; sc < checkboxes.length; sc++) {
      checkboxes[sc].checked = false
    }
    for (let ec = 0; ec < this.externalCheckboxes.length; ec++) {
      this.props.change(this.externalCheckboxes[ec], false)
    }
  }

  checkInternalChannel (checkboxes) {
    let internalCheckBox = document.querySelector(
      'input.social-checkbox.internal'
    )
    if (internalCheckBox) {
      internalCheckBox.checked = true
      this.props.change(this.internalCheckBoxes[0], true)
    }
    // internalCheckBox.checked = true;
    // this.setState({
    //   checked:true
    // })
  }
  uncheckInternalChannels () {
    let internalCheckBox = document.querySelector(
      'input.social-checkbox.internal'
    )
    if (internalCheckBox) {
      internalCheckBox.checked = false
      this.props.change(this.internalCheckBoxes[0], false)
    }
  }
  handleMentionFocus(e){  
     if(e.target.value == ''){
      this.props.change('campaignHashtag' ,'#')
     }
   }

  renderStep1 () {
    let fieldDisable = false
    let disable_class = 'step1 active'
    if (this.state.notShow) {
      fieldDisable = true
      disable_class = 'read-only-data'
    }
    if (this.props.notShow == false) {
      fieldDisable = false
      disable_class = ''
    }
    const campaign = this.props.campaign
    const { pristine, submitting, tags } = this.props
    var me = this
    let social, users, options = []

    users = typeof me.props.users !== 'undefined' && me.props.users
      ? me.props.users
      : []
    options = typeof me.props.tags !== 'undefined' && me.props.tags
      ? me.props.tags
      : []
    social = typeof me.props.channels.channels !== 'undefined' &&
      me.props.channels.channels
      ? me.props.channels.channels
      : []

    var start_date = null
    var end_date = null

    if (typeof this.props.campaign !== 'undefined') {
      start_date = typeof this.props.campaign.start_date !== 'undefined'
        ? this.props.campaign.start_date
        : null
      end_date = typeof this.props.campaign.end_date !== 'undefined'
        ? this.props.campaign.end_date
        : null
    }

    this.externalCheckboxes = []
    this.internalCheckBoxes = []

    const classname = ''
    let userTags = this.props.userTags, postTags = this.props.postTags
    let campaignDetail
    return (
      <form className='campaign-form' onSubmit={this.formSubmit.bind(this)}>
        <div className='form-row'>
          <label for='campaignType'>Campaign Type</label>
          <div className='value'>
            <div className='select-wrapper'>
              <Field
                placeholder=''
                component={renderField}
                type='select'
                name='campaignType'
                id='campaignType'
                onChange={this.optionCampaignTypeSelected.bind(this)}
                disabled={fieldDisable}
                value={this.state.campaignTypeVal}
                validate={[required]}
              >
                <option value=''>Select Campaign Type</option>

                <option value='Internal'>Internal</option>
                <option value='Talent'>Talent</option>
                <option value='Customer'>Customer</option>
              </Field>
            </div>
          </div>
        </div>
        <div className='form-row'>
          {fieldDisable ? <label>Channels</label> : ''}
          <div className='value'>
            <div className='social-checkboxes clearfix'>
              {this.state.checkboxDisabled !== 'external'
                ? <label for='social-checkbox' className='internal_label'>
                    Internal
                  </label>
                : ''}
              {this.state.checkboxDisabled !== 'internal'
                ? <label for='social-checkbox'>Channels</label>
                : ''}
              {social.map((social, i) => {
                if (social.media.toLowerCase() !== 'internal') {
                  this.externalCheckboxes.push(
                    `chk-${social.media.toLowerCase()}-${social.identity}`
                  )
                } else {
                  this.internalCheckBoxes.push(
                    `chk-${social.media.toLowerCase()}-${social.identity}`
                  )
                }
                return (
                  <div key={i}>
                    {this.state.checkboxDisabled == false ||
                      (this.state.checkboxDisabled !== 'external' &&
                        social.media.toLowerCase() === 'internal') ||
                      (this.state.checkboxDisabled !== 'internal' &&
                        social.media.toLowerCase() !== 'internal')
                      ? <div
                        className={`social-checkbox ${social.media.toLowerCase()}`}
                        key={i}
                        >
                        <input
                          name={`chk-${social.media.toLowerCase()}-${social.identity}`}
                          className={`image-checkbox social-checkbox checkboxAllsocial ${social.media.toLowerCase()}`}
                          id={`chk-${social.identity}`}
                          checked={this.state.wallCheckbox.indexOf(social.media.toLowerCase()) > -1}
                          onChange={e =>me.socialCheckboxChanged(e,me,social.media.toLowerCase())}
                          type='checkbox'
                          ref={'check_' + social.identity}
                          label='Channel'
                          disabled={fieldDisable}
                          />
                        {social.media.toLowerCase() == 'internal'
                            ? <label for={`chk-${social.identity}`}>
                              <img
                                src='https://app.visibly.io/img/default-avatar.png'
                                alt='no-img'
                                />
                              <i class='material-icons'>vpn_lock</i>
                            </label>
                            : <label for={`chk-${social.identity}`}>
                              <img
                                src='https://app.visibly.io/img/default-avatar.png'
                                alt='no-img'
                                />
                              <i
                                class={`fa fa-${social.media.toLowerCase()}`}
                                />
                            </label>}

                      </div>
                      : ''}
                  </div>
                )
              })}
            </div>
          </div>
        </div>
        <Field
          name='social'
          id='social'
          component='input'
          type='hidden'
          component={renderField}
          text={this.state.SelectedSocial}
          value={this.state.SelectedSocial}
          disabled={fieldDisable}
        />
        <div class='socialError'>
          <span class='error' />
        </div>

        <Field
          placeholder='Campaign Name'
          label='Title'
          component={renderField}
          type='text'
          name='title'
          validate={[required]}
          disabled={fieldDisable}
        />
        {/* <div className='form-row user-tag-editor'>

          <label for='Post Content'>Description</label>
          <Field
            placeholder='Give a brief overview of the campaign to give it context '
            name='description'
            component={renderField}
            validate={[required]}
            id='post_description'
            type='textarea'
            disabled={fieldDisable}
            // onAdd={this.addTagsUser.bind(this)}
          />
        </div> */}
         
        <Field
          placeholder='What are the core messages you are trying to communicate'
          label='Message'
          component={renderField}
          type='text'
          name='message'
          validate={[]}
          disabled={fieldDisable}
          maxLength="250"
        />
        <Field
          placeholder='What is the desired outcome of this campaign'
          label='Campaign Objective'
          component={renderField}
          type='text'
          name='objective'
          validate={[]}
          disabled={fieldDisable}
          maxLength="250"
        />
        <Field
          placeholder='Who is this campaign aimed at'
          label='Targeted Audience'
          component={renderField}
          type='text'
          name='audience'
          validate={[]}
          disabled={fieldDisable}
          maxLength="250"
        />
        <div class='form-row  user-tag-editor user-permission'>
          <label for='Post Content'>Campaign Hashtag</label>
          <Field
            placeholder={`${!fieldDisable ? 'Select hashtag for campaign' : ''}`}
            name='campaignHashtag'
            component={MentionTextArea}
            userTags={[]}
            validate={[hashtagValidation]}
            tags={postTags}
            type='text'
            id='campaignHashtag'
            onFocus={this.handleMentionFocus}
          />
          <div className="viewper">* Please add only user tag or department</div>
        </div> 
        <div className='row'>
          <div className='col-one-of-two'>
            <div className='inner-form-row'>
              <Field
                name='startdate'
                component={props => {
                  return (
                    <ReactDatePicker
                      {...props}
                      label='Start Date '
                      reactDatePicker={me.state.ReactDatePicker}
                      moment={me.state.Moment}
                      validate={[required]}
                      minDate={me.state.Moment?me.state.Moment():''}
                      dateFormat="DD/MM/YYYY"
                    />
                  )
                }}
                disabled={fieldDisable}
              />
            </div>
          </div>
          <div className='col-one-of-two'>
            <div className='inner-form-row'>
              <Field
                name='enddate'
                component={props => {
                  return (
                    <ReactDatePicker
                      {...props}
                      label='To Date '
                      reactDatePicker={me.state.ReactDatePicker}
                      moment={me.state.Moment}
                      validate={[required]}
                      minDate={me.state.Moment?me.state.Moment():''}
                      dateFormat="DD/MM/YYYY"
                    />
                  )
                }}
                disabled={fieldDisable}
              />
            </div>
          </div>
        </div>
        {fieldDisable
          ? ''
          : <div className='clearfix'>
            <button
              className='btn btn-theme right'
              type='submit'
              disabled={pristine || submitting}
              disabled={fieldDisable}
              onClick = {this.checkPermission.bind(this)}
              >
              {this.props.campaign ? 'Update' : 'Create'}
            </button>
          </div>}
      </form>
    )
  }
  formSubmit (values) {
    this.props.handleSubmit(values)
  }

   //this function  is  prevents form from submitting if there is error in view permission 
 checkPermission(e){
  // if(document.querySelector(".viewper").style.display=="inline"){
  //   e.preventDefault();
  // }
}
  render () {
    var campaignCreated = this.props.campaigns.creation.created
    return (
        this.renderStep1()
      
    )
  }
}
function mapStateToProps (state, ownProps) {
  return {
    tags: state.tags.tags,
    campaigns: state.campaigns,
    channels: state.channels,
    users: state.users.users,
    userTags: state.tags.userTags,
    postTags: state.tags.postTags
  }
}

function mapDispatchToProps (dispatch) {
  return {
    tagsActions: bindActionCreators(tagsActions, dispatch),
    channelActions: bindActionCreators(channelActions, dispatch),
    userActions: bindActionCreators(userActions, dispatch)
  }
}
let connection = connect(mapStateToProps, mapDispatchToProps)

var reduxConnectedComponent = connection(CreateCampaignForm)

export default reduxForm({
  form: 'CreateCampaign' // a unique identifier for this form
})(reduxConnectedComponent)
