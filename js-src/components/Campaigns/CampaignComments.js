import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as campaignsActions from '../../actions/campaigns/campaignsActions'
import PreLoader from '../PreLoader';
import { renderField } from '../Helpers/ReduxForm/RenderField'
import { Field, reduxForm } from 'redux-form'
import notie from "notie/dist/notie.js"
import MentionTextArea from '../Helpers/ReduxForm/MentionTextArea';
import {browserHistory} from 'react-router';
import reactStringReplace from 'react-string-replace';
import * as utils from '../../utils/utils';
import {Link } from 'react-router';
import FetchuserDetail from '../FetchUsersDetail/FetchUsersDetail';
var commentReplyData={};
class CampaignComments extends React.Component {
    constructor (props) {
        super(props);
        this.state={
          currCommentBoxID:null,
          editCommentData:{},
        }
      }
    componentDidMount(){
        commentReplyData={};
        var campId='';
        var contentProjectId='';
        if(this.props.callFrom !== undefined && this.props.callFrom =='contentProject'){
          contentProjectId=this.props.contentProjectData.identity !== undefined ? this.props.contentProjectData.identity:this.props.contentProjectData.project_identity;
          campId=this.props.campaign.identity;
        }else{
          campId=this.props.campaign.identity;
        }
        this.props.campaignsActions.fetchComments(campId,contentProjectId);
      }
    handleInitialize(props){
        var initData ={};
        if(this.state.currComment!==undefined &&  this.state.currComment!==null){
        initData={
          CommentId: this.state.currComment.identity,
          editParentId : this.state.editParentId,
          updateComment:this.state.currComment.comment !== undefined
          ? 
          utils.replaceTags(
            this.state.currComment.comment ,
              this.state.currComment.user_tag
                ? this.state.currComment.user_tag
                : [],
              this.state.currComment.Tags ? this.state.currComment.Tags.data : []
            )
          : '',
         }
        }
        this.props.initialize(initData)
       }
      componentWillReceiveProps(nextProps){
        if(this.props.campaigns.commentLoading!==nextProps.campaigns.commentLoading &&  nextProps.campaigns.commentLoading!==true){
          
          this.setState({
            editCommentData:{}
          })
          this.props.change("submitComment",'');
          this.props.change("submitReply",'');
          this.props.change('parentCommentID','');
          this.props.change('sub_parent_id','');
        }

        if(this.props.campaigns.deleteCommentLoading!==nextProps.campaigns.deleteCommentLoading){
            if(nextProps.campaigns.deleteCommentLoading!==true){
              this.forceUpdate();
              commentReplyData={};
            }
        }

        if(this.props.campaigns.replyData!==nextProps.campaigns.replyData){
          var replyData = nextProps.campaigns.replyData;
          commentReplyData[replyData[1]]= nextProps.campaigns.replyData[0]
          var comment = replyData[0];
          var element = document.getElementsByClassName(`${replyData[1]}-comment`);
            for(var i=0;i<element.length;i++){
              element[i].classList.contains("hide")?
              element[i].classList.remove("hide"):'';
            } 
            var commentbox = document.getElementsByClassName("messageCommentFormWraper");
            for(var i=0;i<commentbox.length;i++){
              if(commentbox[i].id == replyData[1]){
                commentbox[i].classList.remove("hide");
              }
              else{
                if(commentbox[i].id!=="commentEndSection"){
                  commentbox[i].classList.add("hide");
                }
              }
            }

        }

      }
      handleReplyClick(comment){
        if(comment.total_replies!==0){
        this.props.campaignsActions.fetchCommentsReply(comment.identity,comment.campaign_id);

        }else{
          var commentbox = document.getElementsByClassName("messageCommentFormWraper");
          for(var i=0;i<commentbox.length;i++){
              if(commentbox[i].id == comment.identity){
                commentbox[i].classList.remove("hide");
              }
              else{
                if(commentbox[i].id!=="commentEndSection"){
                  commentbox[i].classList.add("hide");
                }
              }
            }
        }
        this.props.change("parentCommentID",comment.identity)
        this.props.change('submitReply',`@[${comment.user_data.name}](user:${comment.user_data.id})`)
      }

      submitCommentData(values){
        this.props.handleSubmit(values);
      }
      handleCommentReplyData(comment,identity){
        var commentbox = document.getElementsByClassName("messageCommentFormWraper");
        for(var i=0;i<commentbox.length;i++){
            if(commentbox[i].id == identity){
              commentbox[i].classList.remove("hide");
            }
            else{
              if(commentbox[i].id!=="commentEndSection"){
                commentbox[i].classList.add("hide");
              }
            }
          }
        this.props.change("parentCommentID",identity)
        this.props.change("sub_parent_id",comment.identity);
        this.props.change('submitReply',`@[${comment.user_data.name}](user:${comment.user_data.id})`)
      }
      renderCommnetReplyData(replyData,identity,pristine,submitting,index){
        var replacedText='';
        var usertag_data = replyData.user_tag?replyData.user_tag:[];
        var dateFromNow ='';
              if (this.props.Moment !== null) {
                var fetchdate = this.props.Moment.unix(replyData.created_at).format('MM-DD-YYYY');
                var today_date = this.props.Moment().format('MM-DD-YYYY')
                var date1 = new Date(fetchdate.toString());
                var date2 = new Date(today_date.toString());
                var timeDiff = date2.getTime() - date1.getTime();
                var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
              }
              if (diffDays <= 28) {
                dateFromNow = this.props.Moment !== null
                    ? this.props.Moment.unix(replyData.created_at).fromNow()  
                  : ''
              } else {
                dateFromNow = this.props.Moment !== null
                    ? this.props.Moment.unix(replyData.created_at).format('dddd, Do MMMM  YYYY ') + 'at' + this.props.Moment.unix(replyData.created_at).format(' h:mm a')
                  : ''
              }

              if(replyData.comment!==undefined){
                replacedText = reactStringReplace(
              utils.convertUnicode(replyData.comment).replace(/\\/g, ''),
              /@(\w+)/g,
              (match, i) => (
                <span className="postTextLinkWrapper">
                {
                  usertag_data[usertag_data.findIndex(x => x.name == match)]!==undefined ?
                  <a
                  onMouseOver={this.updateText.bind(
                    this,
                    usertag_data[usertag_data.findIndex(x => x.name == match)]
                      ? usertag_data[usertag_data.findIndex(x => x.name == match)]
                          .identity
                      : '',
        
                    usertag_data[usertag_data.findIndex(x => x.name == match)]
                      ? usertag_data[usertag_data.findIndex(x => x.name == match)].type
                      : ''
                  )}
                  onMouseLeave={this.mouseDownCapture.bind(this)}
                  className='link5 postTextLink'
                >
                  {`@` + match}
                </a>
                :
                <span className="postTextLink ">
                {`@` + match}
                </span>
                }
                </span>
              )
            )
          }

          replacedText = reactStringReplace(replacedText, /#(\w+)/g, (match, i) => ( 
            <Link onClick={this.redirectToHashTag.bind(this,match)} >{`#` + match}</Link> 
          ))

          replacedText = reactStringReplace(replacedText, /(https?:\/\/\S+)/g, (match, i) => (
            <a  target="_blank" key={match + i} href={match} className = "feed-post-link">{match}</a>
          ));
          
          replacedText = reactStringReplace(replacedText, /(www?\S+)/g, (match, i) => (
             ((match.indexOf("www.") > -1)) ? 
                  <a  target="_blank" key={match + i} href={'https://'+match} className = "feed-post-link">{match}</a> 
              : 
                <span>{match}</span>   
          ));  
        replacedText = reactStringReplace(replacedText, /(visi.ly?\S+)/g, (match, i) => (
            ((match.indexOf("visi.ly/") > -1)) ? 
                 <a  target="_blank" key={match + i} href={'https://'+match} className = "feed-post-link">{match}</a>  
          : <span>{match}</span>  
          ));
        return(
            <div key ={index}>
              <div className={`${identity}-comment`} >
                  <div className={`nested messsageBox clearfix`}>
                      <div className="messageUserThumb"><img src={replyData.user_data.avatar}/></div>
                    <div className="messageContentBox">
                      <div className="messageDesc">
                        <strong>{replyData.user_data.name}</strong> 
                        {replacedText[0].replace(/^\s|\s$/g)}
                      </div>
                      <div className="messageTime" title={
                        this.props.Moment !== null
                        ?
                        this.props.Moment.unix(replyData.created_at).format('dddd, Do MMMM YYYY ')+'at'+this.props.Moment.unix(replyData.created_at).format(' h:mm a')
                      : ''
                      }
                      >{dateFromNow}</div>
                      <div className="replyCommentLinkWrapper"><a onClick={this.handleCommentReplyData.bind(this,replyData,identity)}><strong>{replyData.total_replies}</strong> reply</a></div>
                      {replyData.user_data.id==this.props.profile.identity ?<div className="replyCommentLinkWrapper"> <a onClick={this.handleEditClick.bind(this,replyData,identity)}>edit</a></div>:''}
                    </div>
                    {replyData.user_data.id==this.props.profile.identity || this.props.roleName=='super-admin'?
                    <div className="deleteCommentButton">
                      <a onClick={this.deleteComment.bind(this,replyData)}><i className='material-icons'>clear</i></a>
                    </div>
                    :''}
                  </div>
                  {this.state.editCommentData[replyData.identity]==true? this.renderEditComment(replyData,pristine,submitting,identity):''}
              </div>
            </div>
            )
      }
     handleEditClick(comment,parentId=null){
        if(parentId!==null){
          var commentbox = document.getElementsByClassName("messageCommentFormWraper");
            for(var i=0;i<commentbox.length;i++){
              if(commentbox[i].id == parentId ){
                commentbox[i].classList.add("hide");
              }
            }
        }
        var me = this;
        var editdata = this.state.editCommentData;
        editdata[comment.identity]=true;
        me.setState({
          editCommentData:editdata,
          currComment:comment,
          editParentId : parentId
        },()=>{
          me.handleInitialize()
        })
     }
      renderEditComment(comment,pristine,submitting){
          return(
            <form onSubmit={this.submitCommentData.bind(this)} >
            <div className="messageCommentFeild form-row">
              <Field
              name="CommentId"
              type="hidden"
              value={comment.identity}
              component={renderField}
              />
              <Field
              name="sub_parent_id"
              type="hidden"
              component={renderField}
              />
              <Field
                name="editparentId"
                type="hidden"
                component={renderField}
              />
              <div className="user-tag-editor">
              <Field
              placeholder=""
              name="updateComment"
              component={MentionTextArea}
              userTags ={this.props.userTags}
              tags={this.props.postTags}
              onKeyPress={this.keyPressed.bind(this)}
              />
              </div>
            </div>
              <div className="clearfix messageCommentBtn">
                <button
                  className='btn btn-theme submit-btn-cmt'
                  type='submit'
                  disabled={pristine || submitting}
                  >
                  Submit
                </button>
                <button
                  className='btn btn-theme'
                  onClick={this.closeEditComment.bind(this,comment)}
                  type='button'
                  >
                  Cancel
                </button>
              </div>
        </form>
          )
      }

      closeEditComment(comment){
        var me = this;
        var editdata = this.state.editCommentData;
        editdata[comment.identity]=false;
        me.setState({
          editCommentData:editdata,
          currComment:null
        })
      }
    keyPressed(e){
      var me=this
      if(e.key == "Enter" && e.shiftKey == false){
          me.submitCommentData(e);
      }
    }
      renderCommentBox(identity,pristine,submitting){
      return(
        <form onSubmit={this.submitCommentData.bind(this)} form="replyComment" id="replyCommentForm" >
                    <div className="messageCommentFeild form-row">
                      <Field
                      id='parentCommentIdHiddenComment'
                      name="parentCommentID"
                      type="hidden"
                      value={identity}
                      component={renderField}
                      />
                      <Field
                      name="sub_parent_id"
                      type="hidden"
                      component={renderField}
                      />
                      <div className="user-tag-editor">
                      <Field
                      placeholder="add reply"
                      name="submitReply"
                      component={MentionTextArea}
                      userTags ={this.props.userTags}
                      tags={this.props.postTags}
                      //onChange={this.keyPressed}
                      onKeyPress={this.keyPressed.bind(this)}
                      />
                      </div>
                    </div>
                      <div className="clearfix messageCommentBtn">
                        <button
                          className='btn btn-theme submit-btn-cmt'
                          type='submit'
                          disabled={pristine || submitting}
                          >
                          Submit
                        </button>
                      </div>
                </form>
      )
      }
      hideComments(){

      }
      deleteComment(comment){
        var contentProjectId='';
        if(this.props.callFrom !== undefined && this.props.callFrom =='contentProject'){
           contentProjectId=this.props.contentProjectData.project_identity;
        }
        var campId = this.props.campaign.identity;
        var me = this;
        notie.confirm(
          `Are you sure you want to delete this comment?`,
          'Yes',
          'No',
          function () {
            me.props.campaignsActions.deleteComment(comment.identity,campId,contentProjectId);
          },
          function(){

          })
      }

      redirectToHashTag(match){
        document.body.classList.remove('overlay');
         browserHistory.push(`/feed/default/live#`+match);
      }

      updateText (id, type, e) {
        this.props.updateText(id, type, e)
      }
      mouseDownCapture(e){
        if(document.getElementById('pop-up-tooltip-holder')){
          if (!document.getElementById('pop-up-tooltip-holder').contains(e.target)) {
              document
                .getElementById('pop-up-tooltip-holder')
                .classList.add('hide');
          }
        }
       }
      render () {
        var commentsData = this.props.campaigns.commentsData?this.props.campaigns.commentsData:[];
        const {submitting, pristine,handleSubmit} = this.props;
          return (
            <div className="campaignCommentsSection">
            {
              this.props.campaigns.commentLoading?
              <div>
                 <div className = "Job-loader-container">
                           <div className="curated-loader-inner-container">
                               <div className = "curated-header clearfix">
                                   <div className = "img-avatar"> </div>
                                   <div className = "name-header">
                                       <div className = "name-curated loader-line-height"> </div>
                                       <div className = "date-curated loader-line-height"> </div>
                                   </div>
                                   </div>
                                 <div className = "clearfix curated-block-container">
                         
                                       <div className = "inner-curated-block">
                                           <div className = "curated-decripation">
                                             <div className = "clearfix date-and-share-block">
                                                   <div className ="cureted-date loader-grey-line loader-line-space loader-line-radius"> </div>
                                                   <div className ="cureted-share loader-grey-line loader-line-space loader-line-radius"> </div>
                                             </div>
                                               <div className = "cureted-decp-block4 loader-grey-line loader-line-space loader-line-radius loader-line-height"> </div>
                                               <div className = "cureted-decp-block3 loader-grey-line loader-line-space loader-line-radius loader-line-height"> </div>
                                               <div className = "cureted-decp-block1 loader-grey-line loader-line-space loader-line-radius loader-line-height"> </div>
                                               <div className="cureted-decp-line-padding"></div>
                                           </div>
                                 </div> 
                         </div>
                         </div>
                       </div>
                       <div className = "Job-loader-container">
                           <div className="curated-loader-inner-container">
                               <div className = "curated-header clearfix">
                                   <div className = "img-avatar"> </div>
                                   <div className = "name-header">
                                       <div className = "name-curated loader-line-height"> </div>
                                       <div className = "date-curated loader-line-height"> </div>
                                   </div>
                                   </div>
                                 <div className = "clearfix curated-block-container">
                         
                                       <div className = "inner-curated-block">
                                           <div className = "curated-decripation">
                                             <div className = "clearfix date-and-share-block">
                                                   <div className ="cureted-date loader-grey-line loader-line-space loader-line-radius"> </div>
                                                   <div className ="cureted-share loader-grey-line loader-line-space loader-line-radius"> </div>
                                             </div>
                                               <div className = "cureted-decp-block4 loader-grey-line loader-line-space loader-line-radius loader-line-height"> </div>
                                               <div className = "cureted-decp-block3 loader-grey-line loader-line-space loader-line-radius loader-line-height"> </div>
                                               <div className = "cureted-decp-block1 loader-grey-line loader-line-space loader-line-radius loader-line-height"> </div>
                                               <div className="cureted-decp-line-padding"></div>
                                           </div>
                                 </div> 
                         </div>
                         </div>
                       </div>
              </div>
            :
            <div>
            <div className="messageTitle">Comments <a href="#" className="closeCommentsBtn"><i className="material-icons">clear</i></a></div>

           

            <div className="messagesWrapper">

            {Object.keys(commentsData).length>0?
            commentsData.map((comment,index)=>{
              var replacedText='';
              var usertag_data = comment.user_tag?comment.user_tag:[];
              var dateFromNow ='';
              if (this.props.Moment !== null) {
                var fetchdate = this.props.Moment.unix(comment.created_at).format('MM-DD-YYYY');
                var today_date = this.props.Moment().format('MM-DD-YYYY')
                var date1 = new Date(fetchdate.toString());
                var date2 = new Date(today_date.toString());
                var timeDiff = date2.getTime() - date1.getTime();
                var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
              }
              if (diffDays <= 28) {
                dateFromNow = this.props.Moment !== null
                    ? this.props.Moment.unix(comment.created_at).fromNow()  
                  : ''
              } else {
                dateFromNow = this.props.Moment !== null
                    ? this.props.Moment.unix(comment.created_at).format('dddd, Do MMMM  YYYY ') + 'at' + this.props.Moment.unix(comment.created_at).format(' h:mm a')
                  : ''
              }
              if(comment.comment!==undefined){
                replacedText = reactStringReplace(
              utils.convertUnicode(comment.comment).replace(/\\/g, ''),
              /@(\w+)/g,
              (match, i) => (
                <span className="postTextLinkWrapper">
                {
                  usertag_data[usertag_data.findIndex(x => x.name == match)]!==undefined ?
                  <a
                  onMouseOver={this.updateText.bind(
                    this,
                    usertag_data[usertag_data.findIndex(x => x.name == match)]
                      ? usertag_data[usertag_data.findIndex(x => x.name == match)]
                          .identity
                      : '',
        
                    usertag_data[usertag_data.findIndex(x => x.name == match)]
                      ? usertag_data[usertag_data.findIndex(x => x.name == match)].type
                      : ''
                  )}
                  onMouseLeave={this.mouseDownCapture.bind(this)}
                  className='link5 postTextLink'
                >
                  {`@` + match}
                </a>
                :
                <span className="postTextLink ">
                {`@` + match}
                </span>
                }
                </span>
              )
            )
          }

          replacedText = reactStringReplace(replacedText, /#(\w+)/g, (match, i) => ( 
            <Link onClick={this.redirectToHashTag.bind(this,match)} >{`#` + match}</Link> 
          ))

          replacedText = reactStringReplace(replacedText, /(https?:\/\/\S+)/g, (match, i) => (
            <a  target="_blank" key={match + i} href={match} className = "feed-post-link">{match}</a>
          ));

          replacedText = reactStringReplace(replacedText, /(www?\S+)/g, (match, i) => (
             ((match.indexOf("www.") > -1)) ? 
                  <a  target="_blank" key={match + i} href={'https://'+match} className = "feed-post-link">{match}</a> 
              : 
                <span>{match}</span>   
          ));  

          replacedText = reactStringReplace(replacedText, /(visi.ly?\S+)/g, (match, i) => (
            ((match.indexOf("visi.ly/") > -1)) ? 
                 <a  target="_blank" key={match + i} href={'https://'+match} className = "feed-post-link">{match}</a>  
          : <span>{match}</span>  
          ));
              return(
              <div key={index}>
                {this.state.editCommentData[comment.identity]!==true?
              <div className="messsageBox clearfix post" >
              <div className="messageUserThumb author-thumb"><img src={comment.user_data.avatar} className='thumbnail'/><FetchuserDetail userID={comment.user_data.id}/></div>
              <div className="messageContentBox">
                <div className="messageDesc">
                  <strong>{comment.user_data.name}</strong>
                  {replacedText[0].replace(/^\s|\s$/g,'')}
                </div>
                <div className="messageTime" title={
                  this.props.Moment !== null
                      ?
                      this.props.Moment.unix(comment.created_at).format('dddd, Do MMMM YYYY ')+'at'+this.props.Moment.unix(comment.created_at).format(' h:mm a')
                    : ''
                }>{dateFromNow}</div>
                {this.props.campaign.end_date > Math.floor(Date.now() / 1000) ?
                <div className="replyCommentLinkWrapper"><a onClick={this.handleReplyClick.bind(this,comment)}> <strong>{comment.total_replies!==undefined? comment.total_replies:''}</strong> reply</a></div>
                :''}
                {this.props.campaign.end_date > Math.floor(Date.now() / 1000)  && comment.user_data.id==this.props.profile.identity?
                 <div className="replyCommentLinkWrapper"> <a onClick={this.handleEditClick.bind(this,comment)}>edit</a></div>
                :''}
                             
              </div>
              {comment.user_data.id==this.props.profile.identity || this.props.roleName=='super-admin'?
                <div className="deleteCommentButton">
                  {this.props.campaign.end_date > Math.floor(Date.now() / 1000) ?
                  <a onClick={this.deleteComment.bind(this,comment)}><i className='material-icons'>clear</i></a>
                  :''
                  }
                </div>:''}
              
            </div>:''}
            {this.state.editCommentData[comment.identity]==true? this.renderEditComment(comment,pristine,submitting):''}
                
               { commentReplyData[comment.identity] !==undefined &&  commentReplyData[comment.identity].length>0 && comment.total_replies!==0 ?
                <div>
                
                {commentReplyData[comment.identity].map((replyData,index)=>{
                  return(
                    this.renderCommnetReplyData(replyData,comment.identity,pristine,submitting,index)
                  )
                  })
                }
                 <div className="messageCommentFormWraper" id={comment.identity}>
                 {this.renderCommentBox(comment.identity,pristine,submitting)}
                </div>
                </div>
                : 
                <div className="messageCommentFormWraper hide" id={comment.identity}>
                   {this.renderCommentBox(comment.identity,pristine,submitting)}
                </div>
              } 
                  
            </div>)
            }):<div>No comments found.</div>
            }
            </div>
            {this.props.campaign.end_date > Math.floor(Date.now() / 1000) ?

            <div className="messageCommentFormWraper" id="commentEndSection">

                <form onSubmit={this.submitCommentData.bind(this)} form="submitComment" >
                    <div className="messageCommentFeild form-row">
                      <Field
                        name="parentCommentId"
                        type="hidden"
                        component={renderField}
                      />
                      <div className="user-tag-editor">
                        <Field
                      placeholder="Type comment..."
                      name="submitComment"
                      component={MentionTextArea}
                      userTags ={this.props.userTags}
                      tags={this.props.postTags}
                      onKeyPress={this.keyPressed.bind(this)}
                      />
                      
                      </div>
                    </div>
                      <div className="clearfix messageCommentBtn">
                        <button
                          className='btn btn-theme'
                          type='submit'
                          disabled={pristine || submitting}
                          >
                          Submit
                        </button>
                      </div>
                      
                </form>    
            </div>
            :''    
          }
            </div>}
            </div>
          )
      }
}
function mapStateToProps(state){
  return{
    campaigns: state.campaigns,
    profile : state.profile.profile
  }
}

function mapDispatchToProps(dispatch){
  return{
    campaignsActions:bindActionCreators(campaignsActions,dispatch)
  }
}
var connection = connect(mapStateToProps,mapDispatchToProps);
var reduxConnectedComponent = connection(CampaignComments);
export default reduxForm({
    form: 'commentAddForm' // a unique identifier for this form
  })(reduxConnectedComponent)