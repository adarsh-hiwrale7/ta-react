import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import PreLoader from '../PreLoader'
import * as socialAccountsActions from '../../actions/socialAccountsActions'
import * as campaignsActions from '../../actions/campaigns/campaignsActions'
import * as postsActions from '../../actions/feed/postsActions';
import * as tagsActions from '../../actions/tagsActions'
import * as utils from '../../utils/utils'
import CampaignPosts from '../Calendar/CampaignPosts'
import CampaignDetail from './CampaignDetail'
import UpdateCampaign from './UpdateCampaign'
import CampaignTasks from './CampaignTasks'
import CampaignAnalytics from './CampaignAnalytics'
import CampaignComments from './CampaignComments'
import CampaignCreateTasks from './CampaignCreateTasks'
import CampaignPlanner from './CampaignPlanner'
import CampaignProduction from './CampaignProduction'
import CampaignSegmentation from './CampaignSegmentation'
import CampaignCollabrators from './CampaignCollabrators'
import CampaignAsset from './CampaignAsset'
import CampaignBrief from './CampaignBrief';
import CampaignTaskDetail from './CampaignTaskDetail'
import ChunkJqueryDropdown from '../Chunks/ChunkJqueryDropdown'
import $ from '../Chunks/ChunkJquery'
import CenterPopupWrapper from '../CenterPopupWrapper'
import PopupWrapper from '../PopupWrapper'
import ContentCreation from './ContentCreation';
import ReactDatePicker from '../Helpers/ReduxForm/ReactDatePicker' // react datepicker field
import ReactTimePicker from '../Helpers/ReduxForm/ReactTimePicker' // react datepicker field
import SliderPopup from '../SliderPopup'
import TagPopup from '../Feed/TagPopup';
import CreatePost from '../Feed/CreatePost';
import ReactDOM from 'react-dom';
import Globals from '../../Globals';
import notie from "notie/dist/notie.js"
import FetchuserDetail from '../FetchUsersDetail/FetchUsersDetail';
var selectedUserData;
var selectedDepartmentData;
var restrictAPI=false;
var restrictNotieApiCall = false;
class CampaignViewTabs extends React.Component {
  constructor (props) {
    super(props)
     this.state = {
      folders: [], // Although, folders are stored in redux state, we need to convert the
      loading: false,
      currentTab: 'overview',
      campaignOverview:false,
      createTasks:false,
      taskDetail:false,
      ReactDatePicker: null,
      createSegmentPopup:false,
      EditMode : false,
      CheckedSegments:[],
      isCopy:false,
      isDeleteSegment:false,
      commentUserList:[],
      showContentProject : false,
      printPopup : false,
      assigneeIds:[],
      contentProjectTag:[],
      type:'',
      copyContentProjectModal:false,
      editFromPlanner:false,
      redirectOnProductionListView:false,
      redirectStatusType:'',
      campaignType : '',
      contentType:null
    }
    this.handleContentProjectPage = this.handleContentProjectPage.bind(this);
  }

  componentDidMount () {
    restrictNotieApiCall = false;
    if(restrictAPI!==true){
    this.props.socialAccountsActions.fetchSocialAccounts();
      restrictAPI=true;
      setTimeout(() => {
        restrictAPI=false;
      }, 5000);
    }
    var campId = this.props.campaign.identity;
    if (typeof this.props.location.query.asset !== 'undefined' && this.props.location.query.asset =='true'){
      //after closing or success of asset it will redirect to campaign and set tab
      this.changeTab('assets')
    }else if (typeof this.props.location.query.campaignid !== 'undefined'){
      this.changeTab('feed')
    }

    var newLocation = utils
      .removeTrailingSlash(this.props.location.pathname)
        .split('/')
    /** call tag */
    this.props.tagsActions.fetchUserTags(newLocation,null,this.props.campaign.identity)
    // this.props.tagsActions.fetchUserTags(newLocation)
    this.props.tagsActions.fetchPostTags('all')
    this.props.campaignsActions.fetchTopMyPendingTask(campId);
    this.props.campaignsActions.fetchTopMyPendingContent(campId);
    if(this.props.roleName == 'admin' || this.props.roleName== "super-admin"){
      this.props.campaignsActions.fetchReviewContentList(campId);
    }
    this.props.campaignsActions.fetchCampaigAllUserList(campId);
  }

  handleContentProjectPage = (from=null, type) =>{
    this.setState({showContentProject:true,type:type})
    this.props.handleContentPage(from,type);
  }

  componentWillReceiveProps(nextProps){
    if(nextProps.campaigns.opencontentProjectNotie==true && nextProps.campaigns.notie_project_identity!==null && restrictNotieApiCall!==true ){
      restrictNotieApiCall=true
      this.props.campaignsActions.getContentProjectById("",nextProps.campaigns.notie_project_identity,this.props.campaign.identity,"edit");
      this.props.campaignsActions.closeContentProjectFromNotie();
    }
    if(nextProps.changeRequest!==this.props.changeRequest && nextProps.changeRequest==true){
      this.openCreateTaskPopup();
    }
    if (this.props.posts.creation.disblePopup !== nextProps.posts.creation.disblePopup && nextProps.posts.creation.disblePopup) {
      this.closePostCreatePopup();
    }
    if(this.props.campaignPostPopup!==nextProps.campaignPostPopup && nextProps.campaignPostPopup==true){
      this.openCreatePostPopup(nextProps.posts.campaignCreatePostdata);
    }
    if(this.props.campaign!==nextProps.campaign){
      if(this.state.EditMode==true){
        this.setState({
          EditMode:false
        })
        document.getElementById("campaignTitle").classList.add("hide");
      }
    }
      if(nextProps.campaigns.addTaskLoading!==this.props.campaigns.addTaskLoading && nextProps.campaigns.addTaskLoading!==true){  
         if(this.props.currProjectId){
          this.props.campaignsActions.editContentProject("status",2,this.props.campaign.identity,this.props.currProjectId)
         }
        this.closeCreateTaskPopup();
        if(this.state.editTask==true){
          this.setState({
            editTask:false
          })
        }
      }
      if(this.props.campaigns.deleteTaskLoading!== nextProps.campaigns.deleteTaskLoading){
        if(nextProps.campaigns.deleteTaskLoading!==true){
          this.closeDetailPopup();
          document.body.classList.remove('overlay');
          this.setState({
            editTask:false,
          })
        }
      }
      if(this.props.campaigns.editProjectLoading !== nextProps.campaigns.editProjectLoading && nextProps.campaigns.editProjectLoading!==true){
        document.body.classList.remove('overlay');
        this.closePrintbutton();
        this.setState({
          editFromPlanner:false
        })
      }
      if(this.props.campaigns.ContentProjectLoading!==nextProps.campaigns.ContentProjectLoading && nextProps.campaigns.ContentProjectLoading!==true){
          if(nextProps.campaigns.createContentProject.createContentProjectData!==''&& nextProps.campaigns.createContentProject.createContentProjectData!==undefined){
            this.closePrintbutton();
            this.props.campaignsActions.handleContentCreationState();

                if(this.state.editFromPlanner !== true){
                  this.handleContentProjectPage("create" ,nextProps.campaigns.currContentProject.type);
                }
                document.body.classList.remove('overlay');
          }  
        if(nextProps.campaigns.currContentProject!==null){
            if(nextProps.campaigns.projectAction=="edit"){
              if(this.state.editFromPlanner==true){
                this.handleCopyContentProject();
              }else{
              this.handleContentProjectPage(nextProps.campaigns.currContentProject.type);
              }
            }else if(nextProps.campaigns.projectAction=='copy'){
              this.handleCopyContentProject();
            }
            
          }

      }
    //redirect on production page when see more is clicked from overview page (review content)
      if(this.props.campaigns.redirectToProductionForReview !== nextProps.campaigns.redirectToProductionForReview && nextProps.campaigns.redirectToProductionForReview ==true){
        if(nextProps.campaigns.redirectFor =='pendingContent'){
          this.setState({
            currentTab:'production',
            redirectOnProductionListView:true,
            redirectStatusType:"inprocess"
          })
        }else if(nextProps.campaigns.redirectFor =='reviewContent'){
          this.setState({
            currentTab:'production',
            redirectOnProductionListView:true,
            redirectStatusType:"inreview"
          })
        }else if(nextProps.campaigns.redirectFor =='pendingTask'){
          this.setState({
            currentTab:'tasks',
            redirectStatusType:"pending"
          })
        }
        
        this.props.campaignsActions.resetRedirectToProductionForReviewContent()
      }

      if(this.props.campaigns.redirectToProductionForPending !== nextProps.campaigns.redirectToProductionForPending && nextProps.campaigns.redirectToProductionForPending ==true){
        if(nextProps.campaigns.redirectFor =='pendingContent'){
          this.setState({
            currentTab:'production',
            redirectOnProductionListView:true,
            redirectStatusType:"inprocess"
          })
        }else if(nextProps.campaigns.redirectFor =='reviewContent'){
          this.setState({
            currentTab:'production',
            redirectOnProductionListView:true,
            redirectStatusType:"inreview"
          })
        }else if(nextProps.campaigns.redirectFor =='pendingTask'){
          this.setState({
            currentTab:'tasks',
            redirectStatusType:"pending"
          })
        }
        // this.props.campaignsActions.resetRedirectToProductionForPendingContent()
      }
      if(this.props.campaigns.campaignAllUserList !== nextProps.campaigns.campaignAllUserList && nextProps.campaigns.campaignAllUserList.campaignAllUserListData.length>0){
        this.setState({
          commentUserList : nextProps.campaigns.campaignAllUserList.campaignAllUserListData
        })
      }
  }
  handleEditPlanner(){
    this.setState({
      editFromPlanner:true
    })
  }
  handleCopyContentProject(){
    this.setState({
      copyContentProjectModal:true,
      type:"print"
    })
  }
  renderCopyContentProjectModal(){
    document.body.classList.add('center-wrapper-body-container');
    var userList=[];
    return(
      <div>
            <CenterPopupWrapper>
                <div className = "print-page-container">
                  <div className='inner-print-page-container'>
                      <div className='print-details'>
                        <header className='heading clearfix'>
                            <h3> CONTENT creation</h3>
                            <button id="close-popup" class="btn-default" onClick = {this.closePrintbutton.bind(this)}><i class="material-icons">clear</i></button>
                        </header>
                        <PopupWrapper>
                           <ContentCreation  
                              onSubmit={this.handleContentCreationSubmit.bind(this)} 
                              userTags ={this.props.userList}
                              Moment={this.props.Moment}
                              isFromplanner ={this.state.editFromPlanner==true?true:false}
                              currContentProject={this.props.campaigns.currContentProject}
                              campaign={this.props.campaign}
                              campaignAllUserList={this.props.campaigns.campaignAllUserList}
                              contentType = {this.state.contentType == null ? this.props.contentType : this.state.contentType}
                        />
                        </PopupWrapper>
                      </div>
                  </div>
                </div>
            </CenterPopupWrapper>
       </div>
    )

  }
  componentWillUnmount () {
    document.body.classList.remove('campaignDetailPopupShowed');
  }
  componentWillMount () {
    $().then(jquery => {
      var $ = jquery.$
      this.setState({
        jQuery: $
      })
      ChunkJqueryDropdown().then(jqueryDropdown => {})
    })
   
  } 
  renderSocialChannelsSharedOn (socialAccounts) {
    return (
      <span className='socialAccountsListing'>
        {socialAccounts.map((socialAccount, index) => {
          let social = socialAccount
          if (socialAccount == 'internal wall') social = 'sun-o'
          return (
            <a className={`social ${socialAccount}`} key={index}>
              <i className={`fa fa-${social}`} aria-hidden='true' />
            </a>
          )
        })}
      </span>
    )
  }
  changeTab (tabText) {
    this.setState({
      currentTab: tabText,
      redirectOnProductionListView:false,
      redirectStatusType:'',
      CheckedSegments:[]
    })
  }
  editCampaignSuccessfully(e){
    this.props.editCampaignSuccessfully(e);
    // set state to close popup of edit campaign
    this.setState({
      campaignOverview:false
    })
  }
  closeCampaignOverviewPopup(){
    this.setState({
      campaignOverview:false
    })
    
  }
  closeCreateTaskPopup(){
    document.body.classList.remove('change-request-task-popup');
    this.props.handleChangeStatus("close");
    document.body.classList.remove('overlay');
    this.setState({
      createTasks:false,
      taskDetail:false,
      editTask:false
    })
  }
  openCreateTaskPopup(){
    document.body.classList.add('overlay');
    this.setState({
      createTasks:true
    })
  }
  createTaskPopup(){
    return(
      <SliderPopup className="task-create-popup">
      <header class="heading clearfix">
          <h3>Create Task</h3>
          <button class="btn-default close-popup" onClick={this.closeCreateTaskPopup.bind(this)}><i class="material-icons" >clear</i></button>
         </header>
      <CampaignCreateTasks 
        onSubmit={this.submitTaskData.bind(this)}
        Moment={this.props.Moment} 
        addTaskLoading ={this.props.campaigns.addTaskLoading}
        campaignData ={this.props.campaign}
        profile={this.props.profile}
        roleName={this.props.roleName}
      />
</SliderPopup>
    )
  }
  submitTaskData(values){
     var dueTime = this.props
          .Moment(values[`scheduledDueTime`])
          .format("HH:mm");
      var dueDate = this.props
          .Moment
          .unix(values[`scheduledDueDate`])
          .format("DD/MM/YYYY");
      let DueDateTime = dueDate + " " + dueTime;
      let DueDateTimeStamp =  this.props
          .Moment(DueDateTime, 'DD/MM/YYYY HH:mm')
          .format("X");
          var objTosend={
            title : values.title,
            AssignedToIdentity: values.assignTo,
            DueDateTime : DueDateTimeStamp,
            status : values.status
          }
          if(values.notes!==undefined){
            var obj={
              notes:values.notes
            }
            Object.assign(objTosend,obj);
          }
          if(this.props.currProjectId){
            Object.assign(objTosend,{content_project_id:this.props.currProjectId})
          }
          var campId = this.props.campaign.identity;
          if(values.identity!==undefined){
            var taskId =values.identity;
            // edit task
            this.props.campaignsActions.addTaskData(objTosend,campId,this.props.roleName,taskId);
            this.closeDetailPopup();
          }else{
            // add task
            this.props.campaignsActions.addTaskData(objTosend,campId,this.props.roleName);
            this.closeDetailPopup();
          }
  }
  closeDetailPopup(){
    document.body.classList.remove('overlay');
    this.setState({
      taskDetail:false
    })
  }
  openDetailPopup(value){
    document.body.classList.add('overlay');
    this.setState({
      taskDetail:true,
      currTask:value
    })
  }
  taskDetailPopup(){
    return(
      <SliderPopup className="task-create-popup">
      <header class="heading clearfix">
          <h3>Task detail</h3>
          <button class="btn-default close-popup" onClick={this.closeDetailPopup.bind(this)}><i class="material-icons" >clear</i></button>
         </header>
      <CampaignTaskDetail 
            Moment={this.props.Moment} 
            currTask={this.state.currTask} 
            roleName={this.props.roleName} 
            editTask={this.editTask.bind(this)} 
            deleteTask={this.deleteTask.bind(this)}/>
      </SliderPopup>
    )
  }
  editTask(){
    this.closeDetailPopup();
    document.body.classList.add('overlay');
    this.setState({
      editTask:true,
    })
  }
  deleteTask(id){
    var campId = this.props.campaign.identity;
    this.props.campaignsActions.deleteTask(id,campId,"openTask",this.props.roleName,this.props.profile.identity);
  }
openEditTaskPopup(){
  return(
    <SliderPopup className="task-create-popup">
    <header class="heading clearfix">
        <h3>Edit Task</h3>
        <button class="btn-default close-popup" onClick={this.closeCreateTaskPopup.bind(this)}><i class="material-icons" >clear</i></button>
       </header>
    <CampaignCreateTasks 
      onSubmit={this.submitTaskData.bind(this)}
      Moment={this.props.Moment} 
      currTask={this.state.currTask}
      campaignData ={this.props.campaign}
      profile={this.props.profile}
      roleName={this.props.roleName}
    />
</SliderPopup>)
}
  campaignOverviewPopup(){
    var campaign = this.props.campaign
    return(
      <div className="campaignOverviewPopup">
      <CenterPopupWrapper>
         <header class="heading clearfix">
          <h3>Overview</h3>
          <button class="btn-default close-popup" onClick={this.closeCampaignOverviewPopup.bind(this)}><i class="material-icons" >clear</i></button>
         </header>
         <UpdateCampaign
                refetchList={this.props.refetchList}
                location={this.props.location}
                campaign={campaign}
                Moment={this.props.Moment}
                searchedInput={this.props.searchedInput}
                editCampaign = {this.props.editCampaign}
                editCampaignSuccessfully = {this.editCampaignSuccessfully.bind(this)}
              />
      </CenterPopupWrapper>
      </div>
    )
}
closeCreateSegmentPopup(){
  this.setState({
    isCopy:false,
    CheckedSegments:[],
    createSegmentPopup:false
  })
}
openCreateSegmentPopup(){
  this.setState({
    createSegmentPopup:true
  })
}
editCampaignTitle(title,e){
  document.getElementById("campaignTitle").classList.remove("hide");
  document.getElementById("campaignTitle").value=title;
  this.setState({
    EditMode:true
  })
}
saveTitle(e){
  var campId = this.props.campaign.identity;
    var text =  document.getElementById("campaignTitle").innerText;
    if(text.length <= 50) {
      this.props.campaignsActions.saveCampaignData("title",text,campId);
    }else{
      notie.alert('error', 'Campaign title should be less than or equal to 50 character.', 5)
    }
}
saveCampaignBrief(data){
  this.props.campaignsActions.saveCampaignData("campaignBrief",data.brief,this.props.campaign.identity);
}
handleCheckbox(value,e){
  if(e.target.checked==true){
    var tempArr = this.state.CheckedSegments;
    if(e.target.id=="selectAllSegments"){
      tempArr=value
    }else{
      tempArr.push(e.target.id);
    }
    this.setState({
      CheckedSegments:tempArr
    })
  }else{
    
    var tempArr = this.state.CheckedSegments;
    if(e.target.id=='selectAllSegments'){
      tempArr=[]
    }else{
      document.getElementById("selectAllSegments").checked=false;
      tempArr = tempArr.filter(function(data, index, arr){
        return data !== e.target.id;
      });
    }
    this.setState({
      CheckedSegments:tempArr
    })
  }
}
createTagList(str) {
  var me = this;
  var rx1 = /@\[([^\]]+)]\(([^)]+)\)/g;
  let res = str.match(rx1);
  var userTag = [];
  let contentProjectTag =[];
  var tagIds=[];
  return new Promise(function (resolve, reject) {
    if (res) {
      res
        .forEach(function (tags) {
          /** Get user information */
          var tagsArray = tags.split("@[");
          var nameArray = tagsArray[1].split("]");
          var name = nameArray[0];
          var dept = nameArray[1]
            .substring(0, nameArray[1].length - 1)
            .split(":")[1]
            .split('_');
          var type = "tags";
          if (nameArray[1].substring(0, nameArray[1].length - 1).split(":")[0] !== "(tags") {
            type = dept[1] == 'company' ? 'company' :
              dept[1] == 'dept' ?
                "department"
                : "user";

            let tagsUserObject = {
              tagid: dept[0],
              tagName: name.replace(/ /g, ''),
              type: type
            };
            userTag.push(tagsUserObject)
            contentProjectTag.push(tagsUserObject);
            tagIds.push(dept[0])
          }

          /** description replace */
          if (type === "tags") {
            str = str.replace(tags, "#" + name.replace(/ /g, ''));
          } else {
            str = str.replace(tags, "@" + name.replace(/ /g, ''));
          }

        })
      me.setState({
        commentTag: userTag,
        assigneeIds:tagIds,
        contentProjectTag:contentProjectTag
      })
    }
    resolve(str);
  })
}

submitComment(values){
  var campId = this.props.campaign.identity;
  if(values.CommentId!==undefined){
    this
    .createTagList(values.updateComment)
    .then((desc) => {
    values.updateComment=desc;
    this.props.campaignsActions.updateComment(values,campId,this.state.commentTag,null,values.editParentId)
    })
  }
  else if(values.submitReply!==undefined){
    this
    .createTagList(values.submitReply)
    .then((desc) => {
      values.submitReply=desc;
    this.props.campaignsActions.submitCommentReply(values,campId,this.state.commentTag)
    })
  }else{
    this
    .createTagList(values.submitComment)
    .then((desc) => {
      values.submitComment= desc;
      this.props.campaignsActions.submitComment(values,campId,this.state.commentTag);
    })
  }
}
deleteSegment(){
  this.setState({
    isDeleteSegment: true 
  })
}
handleDeleteState(){
  this.setState({
    CheckedSegments:[],
    isDeleteSegment:false
  })
}
HandleDeleteButSelectSegments(){
  this.setState({
    isDeleteSegment: false 
  })
}
copySegment(){
  this.setState({
    isCopy:true 
  })
}
handleCopyState(){
  this.setState({
    isCopy: false,
    CheckedSegments:[]
  })
}
updateText(id, type, e) {
  this.setState({overlay_flag: true})
  var isUserFound = false;
  if(type=="department"){
    this.props.departments.list.length>0?
    this.props.departments.list.map((DepartmentDetail,i)=>{
      if(DepartmentDetail.identity==id){
        selectedDepartmentData = DepartmentDetail;
      }
    }):''
  }
  else{
    this.props.usersAllList.length>0?
    this.props.usersAllList.map((userDetail,i)=>{
      if(userDetail.identity==id){
        selectedUserData = userDetail;
        isUserFound = true;
      }
    }):''
    if(isUserFound==false){
      selectedUserData ="No user found"
    }
  }
  const domNode = ReactDOM.findDOMNode(e.target);
  let coords = domNode.getBoundingClientRect();
  let coords_top = coords.top + pageYOffset;
  let popup_wrapper = document.getElementById("pop-up-tooltip-holder");
  popup_wrapper.classList.remove('hide','showtotop');

  let coords_left = 0,
    coords_area = 0

  if (screen.width >= 768) {
    coords_left = coords.left + pageXOffset + (coords.width / 2);
  } else {
    coords_left = coords.left + pageXOffset;
    coords_area = coords_left + 472;
    if (coords_area < screen.width) {
      popup_wrapper.classList.add('popup-left');
    } else {
      popup_wrapper.classList.add('popup-right');
    }

  }
  if (e.nativeEvent.screenY < 400) {
    if (e.target.className == "thumbnail") {
      popup_wrapper.classList.add('thumbnai_popup');
    }
  } else {
    popup_wrapper.classList.add('showtotop')
  }

  this.setState({
    popupTop: coords_top,
    popupLeft: coords_left,
    popupdisplay: 'block',
    tagType: type,
    tagTypeClass: (type === "department"
      ? 'departmentpopup'
      : '')
  });
}
  openCreatePostPopup(createPostData){
    this.setState({
      postFromCampaign:true
    })
    document.body.classList.remove('overlay')
    this.handlePostCreation()
    this.setState({
      postCreationCampaignID: createPostData.campaign_id,
      postCreationCampaignTitle: createPostData.campaign_title,
      postCreationCampaignType: createPostData.campaign_type,
      })
  }

  handlePostCreation(){
    this.setState({newPostPopup: true});
   this
     .props
     .postsActions
     .creatingNewPost()
      window.scrollTo(0, 0);
  }
  closePostCreatePopup() {
    this.props.postsActions.removeCampaignCreatePostData()
    this.props.changeCreatePostStatus();
    this.setState({newPostPopup: false,openPostPopup:false})
    document.body.classList.remove('overlayForFeedPopup');
    document
      .body
      .classList
      .remove('overlay');
  }
  PrintPopup(ContentType){
    this.setState({
        contentType:ContentType,
        printPopup : true
    });
  }
  closePrintbutton(){
    this.setState({
      type:'',
      printPopup : false,
      copyContentProjectModal:false,
      editFromPlanner : false
  })
  document.body.classList.remove('center-wrapper-body-container');
  }
  
  handleContentCreationSubmit(data){
     var timeDiff = data.shippingDate - data.startDate;
     var timeDiffEndDate = data.shippingDate - data.endDate;
     var timeDiffStartEnd = data.endDate - data.startDate;
     if(data.mailSend == false || data.mailSend == undefined){
      if (timeDiff < 0) {
        notie.alert('error', 'Shipping Date must be greater then Start Date.', 5)
        return
      }else if(timeDiffEndDate  < 0){
        notie.alert('error', 'Shipping Date must be greater then or equal to End Date.', 5)
        return
      }
     }
     if(timeDiffStartEnd < 0){
      notie.alert('error', 'End Date must be greater then start Date.', 5)
      return
     }
    this
      .createTagList(data.Assignees)
      .then((usertags) => {
        if(this.state.editFromPlanner==true){
          let objToSend ={
            "title":data.title,
            "asssignee":this.state.assigneeIds,
            "start_date":data.startDate,
            "end_date":data.endDate,
            "user_tag":this.state.contentProjectTag,
          }

          if(data.mailSend == true){
              Object.assign(objToSend,{is_immediate : true })   
            }else{
              Object.assign(objToSend,{shipping_date:data.shippingDate,is_immediate : false })
            }
        
          this.props.campaignsActions.editContentProject("editFromPlanner",objToSend,this.props.campaign.identity,data.identity)
        }else{
        var objToSend={
          "title":data.title,
          "type": this.state.contentType == null ? this.props.contentType : this.state.contentType,
          "campaign_id":this.props.campaign.identity,
          "asssignee":this.state.assigneeIds,
          "start_date":data.startDate,
          "end_date":data.endDate,
          "user_tag":this.state.contentProjectTag,
        }
         if(data.mailSend == true){
            Object.assign(objToSend ,{is_immediate : true })   
          }else{
            Object.assign(objToSend,{shipping_date:data.shippingDate ,is_immediate : false })
          }
        
        if(data.identity){
          Object.assign(objToSend,{is_copy:true})
        }
        this.props.campaignsActions.createContentProject(objToSend)
      }
    })
  }
  openPopupPrint(){
    document.body.classList.add('center-wrapper-body-container');
    var userList=[];
    for (let index = 0; index < this.props.userList.length; index++) {
      const element = this.props.userList[index];
      if(element.type.toLowerCase() !== 'company'){
        userList.push(element)
      }
    }
    return(
        <div>
              <CenterPopupWrapper>
                  <div className = "print-page-container ">
                  <div className = "print-page-container email-popup-container"> </div>
                    <div className='inner-print-page-container'>
                        <div className='print-details'>
                          <header className='heading clearfix'>
                              <h3> CONTENT creation</h3>
                              <button id="close-popup" class="btn-default" onClick = {this.closePrintbutton.bind(this)}><i class="material-icons">clear</i></button>
                          </header>
                          <PopupWrapper>
                             <ContentCreation campaignAllUserList={this.props.campaigns.campaignAllUserList} onSubmit={this.handleContentCreationSubmit.bind(this)} userTags ={userList} Moment={this.props.Moment} campaign={this.props.campaign} contentType = {this.state.contentType}/>
                          </PopupWrapper>
                        </div>
                    </div>
                  </div>
              </CenterPopupWrapper>
         </div>
      )
  }
  renderPostCreation(){
      document.body.classList.add('overlayForFeedPopup');
        var defaultfeedData=Object.keys(this.props.feed.feedList).length>0 ? this.props.feed.feedList[0] :''
        var editPostData = null;
        var currFeedName =defaultfeedData!==''?defaultfeedData:'internal';
      return (
        <div className = "createPostMainContainer" id = "createMainContainerPopup">
        <SliderPopup className='wide createPostPopup' fadePopup = 'fadepopup' locationPopup = "CreatePost">
          <button
            id='close-popup'
            className='btn-default'
            onClick={this
            .closePostCreatePopup
            .bind(this)}>
            <i className='material-icons'>clear</i>
          </button>
          <CreatePost
            campaignId={this.props.campaign.identity}
            campaignTitle={this.props.campaign.title}
            jQuery={this.state.jQuery}
            defaultFeed={defaultfeedData}
            location={this.props.location}
            editPostData = {editPostData}              
            currentFeed = {currFeedName}
            />
      </SliderPopup>
    </div>
      )
    }

    showAssignPopup(){
      this.setState({
        showAssignPopup : !this.state.showAssignPopup
      })
   }
  render () {
    var user = this.props.users;
    let isFetching = user.isFetching;
    const { loading } = this.props.campaigns.creation
    const campaign = this.props.campaign
    const catID = this.props.catID

    var catName = ''
    if (catID != null) {
      catName = this.props.catName[0].name
    }

    var me = this
    var tags = campaign.tag_data

    var socialAccounts = []
    campaign.SocialChannel?campaign.SocialChannel.data.map(acc => {
      if (typeof acc.detail !== 'undefined') {
        socialAccounts.push(acc.detail.toLowerCase())
      }
    })
    :''
    
    
    if(document.querySelectorAll('#campaign-details').length > 0)
    {
      if( this.state.currentTab == 'segmentations' || this.state.currentTab == 'collaborators'){
        document.getElementById('campaign-details').classList.add('segmentTabSelected');
      }else{
        document.getElementById('campaign-details').classList.remove('segmentTabSelected');
      }
      if(this.state.createSegmentPopup){
        document.getElementById('campaign-details').classList.add('hideDots');
      }else{
        document.getElementById('campaign-details').classList.remove('hideDots');
      }
    }

    //to fetch date diff
    var date_diff_indays = function(date1, date2) {
      var dt1 = new Date(date1);
      var dt2 = new Date(date2);
      return Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate()) ) /(1000 * 60 * 60 * 24));
      }
      
    var campaignEndDate = this.props.Moment!==null? this.props.Moment.unix(parseInt(this.props.campaign.end_date)).format('DD-MM-YYYY'):'';
    var campStartDate =  this.props.Moment!==null? this.props.Moment.unix(parseInt(this.props.campaign.start_date)).format('MM-DD-YYYY'):'';
    var campEndDate = this.props.Moment!==null? this.props.Moment.unix(parseInt(this.props.campaign.end_date)).format('MM-DD-YYYY'):'';
    var currDate = this.props.Moment().format('MM-DD-YYYY')

    //call fun to get date diff
    campStartDate=date_diff_indays(currDate, campStartDate);
    campEndDate=date_diff_indays(currDate, campEndDate);
    var dateToDisplay='';

    if(campStartDate <  0 && campEndDate < 0  ){
      //if campaign is expired
      dateToDisplay = 'Expired on ' +campaignEndDate;
    }else if(campStartDate >  0 && campEndDate > 0 ){
      //if campaign start date is in future 
      dateToDisplay = Math.abs(campStartDate) +" days to start"
    }else if(campStartDate  <= 0 && campEndDate > 0 ){
      //if campaign is started
      dateToDisplay = Math.abs(campEndDate) +" days to end"
    }else{
      dateToDisplay = "Expire today"
    }
    var image = {
			backgroundImage: `url('https://s3.eu-west-2.amazonaws.com/visibly-staging/images/pirates_com/1/Screenshot_20181101-153927.png')`
    }
    let display3Dots = false;
    switch(this.state.currentTab){
      case "overview":
          display3Dots= false;
          break;
      case "brief":
          display3Dots=false;
      case "collaborators":
         if(this.props.roleName=="super-admin" || this.props.roleName=='admin'){
            display3Dots = true;
          }
          break;
      case "segmentations":
          if(this.props.roleName=="super-admin" || this.props.roleName=='admin'){
              display3Dots = true;
            }
            break;
      case "planner":
          display3Dots= true;
            break;
      case "production":
          display3Dots = true;
      case "assets":
          display3Dots = true;
      case "tasks":
          if(this.props.roleName=="super-admin" || this.props.roleName=='admin'){
              display3Dots = true;
            }
            break;
      default :
            display3Dots = false;
    }
    const displayPlannerSegment = campaign.campaign_type == "Talent" || campaign.campaign_type == "Customer" ? false : true
    return (
      <section className='campaignDetailBlock'>
      {/*  open campaign popup on click of campaignlist */}
      {
        this.state.campaignOverview ? this.campaignOverviewPopup() : ''
      }
      {this.state.createTasks? this.createTaskPopup(): ''}
      {this.state.taskDetail ? this.taskDetailPopup():''}
      {this.state.editTask?this.openEditTaskPopup():''}
      {/* {this.props.campaignPostPopup==true?this.openCreatePostPopup():''} */}
      {this.state.newPostPopup
                       ? this.renderPostCreation()
                       : <div />}
      {this.state.printPopup ? this.openPopupPrint() : ''}
      {this.state.copyContentProjectModal?this.renderCopyContentProjectModal():null}
      <div className="detailCampaignHeader">
            <div className="detailCampaignTitle">
            {this.state.EditMode !==true?
           <span className="campaignTitleWrapper">{utils.limitLetters(campaign.title,50)}</span> :''}
           
            <span className="hide campaignTitleWrapper titleEditable" 
              name ="campaignTitle"
              id="campaignTitle"
              contentEditable="true"
              suppressContentEditableWarning={true}
              > {campaign.title}</span>
           {this.state.EditMode==true ?<a className="editCampaignBtn" onClick={this.saveTitle.bind(this)}><i className="material-icons">check</i></a>:''}
            {this.props.roleName == 'admin' || this.props.roleName== "super-admin"?
            <a onClick={this.editCampaignTitle.bind(this,campaign.title)} className="editCampaignBtn" title="Edit"><i class="material-icons">mode_edit</i></a>
            :''
            }
            </div>
            <div className="campaignMeta clearfix">
            <div className="campaignMetaLeft clearfix">
            <div className="campaignType"><span className="campaignTypeIcon"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path fill="none" d="M0 0h24v24H0V0z"/><path d="M9 12c1.93 0 3.5-1.57 3.5-3.5S10.93 5 9 5 5.5 6.57 5.5 8.5 7.07 12 9 12zm0-5c.83 0 1.5.67 1.5 1.5S9.83 10 9 10s-1.5-.67-1.5-1.5S8.17 7 9 7zm.05 10H4.77c.99-.5 2.7-1 4.23-1 .11 0 .23.01.34.01.34-.73.93-1.33 1.64-1.81-.73-.13-1.42-.2-1.98-.2-2.34 0-7 1.17-7 3.5V19h7v-1.5c0-.17.02-.34.05-.5zm7.45-2.5c-1.84 0-5.5 1.01-5.5 3V19h11v-1.5c0-1.99-3.66-3-5.5-3zm1.21-1.82c.76-.43 1.29-1.24 1.29-2.18C19 9.12 17.88 8 16.5 8S14 9.12 14 10.5c0 .94.53 1.75 1.29 2.18.36.2.77.32 1.21.32s.85-.12 1.21-.32z"/></svg></span>{campaign.campaign_type}</div>
            {/* <div className="campaignLocation"><span>EMEA</span><span>UK</span><span>Cambridge</span></div> */}
            <div className="campaignDateWrapper">
              <span className="campaignDateIcon"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path fill="none" d="M0 0h24v24H0V0z"/><path d="M20 3h-1V1h-2v2H7V1H5v2H4c-1.1 0-2 .9-2 2v16c0 1.1.9 2 2 2h16c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zm0 18H4V10h16v11zm0-13H4V5h16v3z"/></svg></span>
              <span className="campaignStartDate">{dateToDisplay}</span>
            </div>
            </div>
            <div className="camapignAssignAuthorWrapper camapignHdrRight post">

            <div className = "assign-author"><span className = "author">Owner</span>
                      <span class="author-thumb">
                        <img
                          className="profile thumbnail"
                          src={campaign.User?campaign.User.data.avatar:''}
                          onError={e => {
                            e.target.src =
                              "https://app.visibly.io/img/default-avatar.png";
                          }}
                        />
                      <FetchuserDetail userID={campaign.User?campaign.User.data.identity:''}/>
                      </span>
             </div>
            {/* <a className="commentToggleBtn" href="javascript:void(0);"><i class="material-icons">chat_bubble_outline</i> <span className="commentBtnText">Comments</span></a> */}
            </div>
            </div>
          </div>
        <div className='listMenu'>
          <nav class='nav-side clearfix'>
            <ul class='parent'>
            <li>
                <a
                  className={this.state.currentTab == 'overview' ? ` active` : ``}
                  onClick={this.changeTab.bind(this, 'overview')}
                >
                  <span class='text'>Overview</span>
                </a>
              </li>
              <li>
                <a
                  className={this.state.currentTab == 'brief' ? ` active` : ``}
                  onClick={this.changeTab.bind(this, 'brief')}
                >
                  <span class='text'>Brief</span>
                </a>
              </li>
              <li>
                <a
                  className={this.state.currentTab == 'collaborators' ? ` active` : ``}
                  onClick={this.changeTab.bind(this, 'collaborators')}
                >
                  <span class='text'>Collaborators</span>
                </a>
              </li>
              { displayPlannerSegment ?
              this.props.newLocation !=='expired' && this.props.newLocation !== "terminated" && this.props.campaign.terminated!==1?
              <li>
                <a
                  className={this.state.currentTab == 'planner' ? ` active` : ``}
                  onClick={this.changeTab.bind(this, 'planner')}
                >
                  <span class='text'>Planner</span>
                </a>
              </li>:'' :''}
              {this.props.newLocation !=='expired' && this.props.newLocation !== "terminated" && this.props.campaign.terminated!==1?
              <li>
                <a
                  className={this.state.currentTab == 'production' ? ` active` : ``}
                  onClick={this.changeTab.bind(this, 'production')}
                >
                  <span class='text'>Production</span>
                </a>
              </li>:''}
              {/* <li>
                <a
                  className={this.state.currentTab == 'feed' ? ` active` : ``}
                  onClick={this.changeTab.bind(this, 'feed')}
                >
                  <span class='text'>Scheduled</span>
                </a>
              </li> */}
              <li>
                <a
                  className={this.state.currentTab == 'assets' ? ` active` : ``}
                  onClick={this.changeTab.bind(this, 'assets')}
                >
                  <span class='text'>Assets</span>
                </a>
              </li>
              { displayPlannerSegment ?
              <li>
                <a
                  className={this.state.currentTab == 'segmentations' ? ` active` : ``}
                  onClick={this.changeTab.bind(this, 'segmentations')}
                >
                  <span class='text'>Segments</span>
                </a>
              </li> :''}
            </ul>
          </nav>
        </div>
        
        <div id='campaign-details' className="clearfix 11">
        <div className="campaignDetailLeftColumn">

        {this.props.newLocation == 'expired' && this.props.roleName == 'employee' || this.state.currentTab == 'brief' ||  this.props.campaigns.segmentloading  ||   this.props.campaigns.addSegmentLoading||
          this.props.campaigns.taskLoading ||  this.props.campaigns.listLoading ||  this.props.campaigns.addCollaboratorLoading ||
          this.props.campaigns.dataLoading || this.props.campaigns.collaboratorLoading
          ? ''
          : 
          display3Dots == true?
          <ul className='dd-menu context-menu campaign_3dots'>
            
            <li className='button-dropdown'>
            {this.props.newLocation == 'terminated' || this.props.newLocation == 'expired' ? '':
              <a className='dropdown-toggle'>
                <i className='material-icons'>more_vert</i>
              </a>
            }
              <ul className='dropdown-menu'>
                {this.props.newLocation == 'terminated'
                  ? ''
                  : <li class='dropdown-parent-li'>
                    <ul>
                      {this.props.location.pathname !== "/campaigns/expired"  ?
                      <li>
                        {/* this.props.showPostCreationButton &&  */}
                        {
                          // && this.props.addNewPostAllow==true
                        }
                        {typeof this.props.campaign !==
                          'undefined' &&  (this.state.currentTab == 'planner' || this.state.currentTab == 'production')
                          ? <a
                            className=''
                            onClick={this.props.onPostCreationClick.bind(
                              this,
                              this.props.campaign.identity,
                              this.props.campaign.title,
                              this.props.campaign.campaign_type
                              )}
                          >
                            Post
                                </a>
                          : null}
                      </li> :''}

                      {this.props.location.pathname !== "/campaigns/expired" && this.state.currentTab == "assets" ?
                      <li>
                      {typeof this.props.campaign !==
                        'undefined' && this.props.showPostCreationButton
                          ? <a
                          className=''
                          onClick={this.props.onAssetCreationClick.bind(
                            this,
                            this.props.campaign.campaign_folder,
                            this.props.campaign.identity
                          )}
                        >
                          Add new asset
                            </a>
                            :null}
                      </li> :''}

                      {this.props.location.pathname !== "/campaigns/expired" &&
                      (this.props.roleName=="super-admin"||this.props.roleName=='admin')
                       && (this.state.currentTab == 'planner' || this.state.currentTab == 'production')?
                      <li>
                      {typeof this.props.campaign !==
                        'undefined' && this.props.showPostCreationButton
                          ? <a
                          className=''
                          onClick={this.PrintPopup.bind(this,"print")}
                        >
                          Print
                            </a>
                            :null}
                      </li> :''}

                      {this.props.location.pathname !== "/campaigns/expired" &&
                      (this.props.roleName=="super-admin"||this.props.roleName=='admin')
                       && (this.state.currentTab == 'planner' || this.state.currentTab == 'production')?
                      <li>
                      {typeof this.props.campaign !==
                        'undefined' && this.props.showPostCreationButton
                          ? <a
                          className=''
                          onClick={this.PrintPopup.bind(this,"email")}
                        >
                          Email
                            </a>
                            :null}
                      </li> :''}
                      {/* {this.props.location.pathname !== "/campaigns/expired" &&
                      (this.props.roleName=="super-admin"||this.props.roleName=='admin')
                       && (this.state.currentTab == 'planner' || this.state.currentTab == 'production')?
                      <li>
                      {typeof this.props.campaign !==
                        'undefined' && this.props.showPostCreationButton
                          ? <a
                          className=''
                        >
                          SMS
                            </a>
                            :null}
                      </li> :''} */}

                      {this.props.location.pathname !== "/campaigns/expired" &&
                      (this.props.roleName=="super-admin"||this.props.roleName=='admin')
                      && (this.state.currentTab == 'planner' || this.state.currentTab == 'production')?
                      <li>
                      {typeof this.props.campaign !==
                        'undefined'
                          ? <a
                          className=''
                          onClick={this.openCreateTaskPopup.bind(this)}
                        >
                          Task
                            </a>
                            :null}
                      </li> :''}
                      {this.props.location.pathname !== "/campaigns/expired"  && (this.state.currentTab == 'segmentations' || this.state.currentTab == 'collaborators') ?
                      <li>
                      {typeof this.props.campaign !==
                        'undefined'
                          ? <a
                          className=''
                          onClick={this.openCreateSegmentPopup.bind(this)}
                        >
                          Add
                            </a>
                            :null}
                      </li>
                       :''}
                    </ul>
                  </li>}
             {this.props.location.pathname !== '/campaigns/expired' ?
                <li>
                  {this.props.roleName == 'admin' || this.props.roleName == 'super-admin'
                    ? typeof this.props.campaign !== 'undefined' &&
                      this.props.showPostCreationButton && this.state.currentTab == 'overview'
                      ? <a
                        className=''
                        onClick={this.props.editCampaign.bind(this)}
                      >
                      Edit
                      </a>
                      : null
                    : ''}
                </li> :''}
                
                <li>
                  {this.props.Moment !== null
                    ? typeof this.props.campaign !== 'undefined' &&
                      this.props.campaign.end_date >
                      this.props.Moment().unix() && this.state.currentTab == 'overview'
                      ? <a
                        onClick={this.props.terminateCampaign.bind(
                          this,
                          this.props.terminateStatus
                        )}
                        className=''
                      >

                        {/* {this.props.terminateStatus !== 0
                            ? 'Restart'
                            : 'Terminate'} */}

                        {(this.props.roleName == 'admin' || this.props.roleName == 'super-admin' )?
                          this.props.campaign.terminated == 0
                          ? 'Terminate'
                          : 'Restart'
                          :''
                        }
                      </a>
                      : null
                    : null}

                </li>
                {this.props.location.pathname == '/campaigns/expired' ?
                <li>
                  {this.props.roleName == 'admin' || this.props.roleName == 'super-admin'
                    ? typeof this.props.campaign !== 'undefined' &&
                      this.props.showPostCreationButton && this.state.currentTab == 'overview'
                      ? <a
                        className=''
                        onClick={this.props.editCampaign.bind(this)}
                      >
                     Restart
                      </a>
                      : null
                    : ''}
                </li> :''}
               
              </ul>
            </li>
          </ul>:''}
                      {/* added extra condition for loader to put loader on page  after submititng new task */}
            {loading || this.props.campaigns.editProjectLoading==true  || this.props.campaigns.ContentProjectLoading ==true || this.props.campaigns.createContentProject.createContentProjectLoading == true?  <PreLoader className = "Page" /> : <div />}
            <div className='section'>
            {this.state.currentTab == 'overview' ? 
            <div className = "overview-parent-container-wrapper">
              {/* read only mode of campaign data need to change according to condition for admina and employee */}
            {/* <UpdateCampaigns
                refetchList={this.props.refetchList}
                location={this.props.location}
                campaign={campaign}
                Moment={this.props.Moment}
                searchedInput={this.props.searchedInput}
              /> */}
                <CampaignDetail
                campaign={this.props.campaign}
                campaigns={this.props.campaigns}
                Moment={this.props.Moment}
                channels={this.props.channels}
                location={this.props.location}
                editdata={this.props.editdata}
                campaignsActions={this.props.campaignsActions}
                roleName={this.props.roleName}
                editCampaign={this.props.editCampaign}
                showPostCreationButton ={this.props.showPostCreationButton}
                terminateCampaign={this.props.terminateCampaign.bind(
                  this,
                  this.props.terminateStatus
                )}
                campaignAllUserList={this.props.campaigns.campaignAllUserList}
               />
                </div>

               : ''}
               {this.state.currentTab == 'collaborators'
                ? <CampaignCollabrators 
                location = {this.props.location} 
                createSegmentPopup = {this.state.createSegmentPopup} 
                closeCreateSegmentPopup = {this.closeCreateSegmentPopup.bind(this)}
                campaign ={this.props.campaign}
                Moment={this.props.Moment}
                // userTags ={this.props.userTags}
                openCreateSegmentPopup = {this.openCreateSegmentPopup.bind(this)}
                currentView={this.state.currentTab}
                isDeleteSegment={this.state.isDeleteSegment}
                CheckedSegments={this.state.CheckedSegments}
                handleDeleteState={this.handleDeleteState.bind(this)}
                HandleDeleteButSelectSegments={this.HandleDeleteButSelectSegments.bind(this)}
                handleCheckbox={this.handleCheckbox.bind(this)}
                isCopy ={this.state.isCopy}
                roleName={this.props.roleName}
                campaignAllUserList={this.props.campaigns.campaignAllUserList}
                />
                : ''}
              {this.state.currentTab == 'brief'
              //component for display campaign details
                ? 
               <CampaignBrief 
                    onSubmit={this.saveCampaignBrief.bind(this)}
                    campaignData = {this.props.campaigns}
                    campaign={this.props.campaign}
                    roleName={this.props.roleName}
                />
                

                : ''}
                {this.state.currentTab == 'planner'
                ? 
                  // <CampaignPlanner
                  //   campaign={this.props.campaign}
                  //   Moment={this.props.Moment}
                  //   location={this.props.location}
                  //   profile={this.props.profile}
                  // />
                  <CampaignPosts
                    handleChangeStatus= {this.props.handleChangeStatus}
                    campaign={this.props.campaign}
                    Moment={this.props.Moment}
                    location={this.props.location}
                    profile={this.props.profile}
                    roleName={this.props.roleName}
                    handleEditPlanner ={this.handleEditPlanner.bind(this)}
                    campaignAllUserList={this.props.campaigns.campaignAllUserList}
                  />
                : ''}
                {this.state.currentTab == 'production'
                ? 
               <CampaignProduction
                  campaign={this.props.campaign} 
                  Moment={this.props.Moment}
                  roleName={this.props.roleName}
                  location={this.props.location}
                  profile={this.props.profile}
                  redirectOnListView={this.state.redirectOnProductionListView}
                  redirectStatusType={this.state.redirectStatusType}
                  campaignAllUserList={this.props.campaigns.campaignAllUserList}
               />
                : ''}
              {/* {this.state.currentTab == 'feed'
                ? <CampaignPosts
                  campaign={this.props.campaign}
                  Moment={this.props.Moment}
                  location={this.props.location}
                  profile={this.props.profile}
                  />
                : ''} */}
              
              {this.state.currentTab == 'assets'
                ? <CampaignAsset campaign={this.props.campaign}  location={this.props.location} campaignAssetPopup = {this.props.campaignAssetPopup} changeCreateAssetStatus={this.props.changeCreateAssetStatus}/>
                : ''}
              {this.state.currentTab == 'analytics'
                ? 
                  <CampaignAnalytics
                    campaign={this.props.campaign}
                    Moment={this.props.Moment}
                    location={this.props.location}
                    profile={this.props.profile}
                  />
                : ''}
              {this.state.currentTab == 'segmentations'
                ? <CampaignSegmentation
                ref="segmentPage"
                location = {this.props.location} 
                createSegmentPopup = {this.state.createSegmentPopup} 
                closeCreateSegmentPopup = {this.closeCreateSegmentPopup.bind(this)}
                campaign ={this.props.campaign}
                Moment={this.props.Moment}
                // userTags ={this.props.userTags}
                openCreateSegmentPopup = {this.openCreateSegmentPopup.bind(this)}
                handleCheckbox={this.handleCheckbox.bind(this)}
                isCopy ={this.state.isCopy}
                isDeleteSegment={this.state.isDeleteSegment}
                CheckedSegments={this.state.CheckedSegments}
                handleCopyState={this.handleCopyState.bind(this)}
                handleDeleteState={this.handleDeleteState.bind(this)}
                HandleDeleteButSelectSegments={this.HandleDeleteButSelectSegments.bind(this)}
                currentView={this.state.currentTab}
                roleName={this.props.roleName}
                campaignAllUserList={this.props.campaigns.campaignAllUserList}
                />
                : ''}
            </div>

            </div>
        
            {/* <div className="campaignDetailRightColumn">
            <CampaignComments
            campaign ={this.props.campaign} 
            Moment={this.props.Moment} 
            onSubmit={this.submitComment.bind(this)}
            userTags={this.state.commentUserList}
            postTags ={this.props.postTags}
            updateText={this.updateText.bind(this)}
            roleName = {this.props.roleName}
            campaignAllUserList={this.props.campaigns.campaignAllUserList}
            />

             <div id="pop-up-tooltip-holder">
        {selectedUserData == "No user found"  && this.state.tagType!=="department"?
            <div
                className="pop-up-tooltip-not-data-found"
                style={{
                  top: this.state.popupTop,
                  left: this.state.popupLeft,
                  display: this.state.popupdisplay
                }}>
                <span className='popup-arrow'></span>
                <p> No user data found </p>
              </div>


          :
                   <div id="pop-up-tooltip-wrapper" className={this.state.tagTypeClass}>
                     <div
                       className="pop-up-tooltip"
                       style={{
                       top: this.state.popupTop,
                       left: this.state.popupLeft,
                       display: this.state.popupdisplay
                     }}>
                       <span className='popup-arrow'></span>
                       <TagPopup
                         userDetails={selectedUserData}
                         isFetching={isFetching}
                         isDept={this.state.tagType}
                         department={selectedDepartmentData}/>
                     </div>
                   </div>

        }
        </div>
            </div> */}
            </div>            
      </section>
    )
  }
}

function mapStateToProps (state) {
  return {
    feed: state.feed,
    socialAccounts:state.socialAccounts,
    campaigns: state.campaigns,
    culture: state.cultureAnalytics,
    usersAllList: state.usersList.userList.data,
    userList: state.tags.userTags,
    profile:state.profile.profile,
    posts: state.posts,
    postTags: state.tags.postTags,
    disblePopup: state.posts.creation.disblePopup,
    users: state.users,
    departments:state.departments
  }
}

function mapDispatchToProps (dispatch) {
  return {
    socialAccountsActions: bindActionCreators(socialAccountsActions, dispatch),
    campaignsActions: bindActionCreators(campaignsActions, dispatch),
    tagsActions : bindActionCreators(tagsActions,dispatch),
    postsActions : bindActionCreators(postsActions,dispatch)
  }
}

let connection = connect(mapStateToProps, mapDispatchToProps)

module.exports = connection(CampaignViewTabs)
