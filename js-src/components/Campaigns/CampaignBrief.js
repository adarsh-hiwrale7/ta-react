import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Field, reduxForm } from 'redux-form'
import { renderField } from '../Helpers/ReduxForm/RenderField'
import PreLoader from '../PreLoader';
const required = value => (value ? undefined : 'Required')
class CampaignBrief extends React.Component {
    constructor (props) {
        super(props);
        this.state={
            editMode: false,
        }
      }
      componentWillReceiveProps(nextProps){
            if(nextProps.campaignData.dataLoading!==this.props.campaignData.dataLoading&& nextProps.campaignData.dataLoading==false){
                this.setState({
                    editMode:false,
                })
            }
      }
      componentDidMount(){
        if(this.props.roleName=='admin'||this.props.roleName=='super-admin'){
          this.setState({editMode:true});
        }
        this.handleInitialize();
      }
    
      handleInitialize (nextProps = null) {
          const initData = {
            brief: this.props.campaign.description != null ? this.props.campaign.description.replace(/^\s|\s$/g,'') : ''
          }
          this.props.initialize(initData)
      }
      editBrief(){
        this.setState({editMode:true});
        this.handleInitialize(this.props)
      }
      cancelEditBrief(){
        this.setState({
            editMode:false
        })
      }
      render () {
        const { handleSubmit, pristine, submitting } = this.props;
          return (
          <div className="camapaignBrifFieldWrapper">
          {this.props.campaignData.dataLoading?  <PreLoader className = "Page" /> : <div />}
            {this.state.editMode==true?
            <form onSubmit={handleSubmit}>
                <Field
                id="briefData"
                name ="brief"
                component={renderField}
                type="textarea"
                validate ={[required]}
                />
            <div className="BriefBtnWrapper">
                <button
                  className='btn  btn-theme'
                  type='submit'
                  validate={[required]}
                  disabled={pristine || submitting}
                >
                  Save
                </button>
                <button
                  className='btn cancel-btn default-btn'
                  type='submit'
                  onClick={this.cancelEditBrief.bind(this)}
                >
                  cancel
                </button>
            </div>
            </form>:
                <div className="campaign-brief-detail">
                     <p>
                        {this.props.campaign.description}   
                    </p> 
                    <div className="BriefBtnWrapper">
                    {this.props.roleName == "admin" || this.props.roleName == "super-admin"?
                        <a className="btn theme-btn" onClick={this.editBrief.bind(this)}> Edit</a>:''}
                </div>
                </div>
              }
          </div>
          )
      }
}
function mapStateToProps(state){
    return{

    }
}
function mapDispatchToProps(dispatch){
    return{

    }
}
let connection = connect(mapStateToProps,mapDispatchToProps)

var reduxConnectedComponent = connection(CampaignBrief)

export default reduxForm({
  form: 'CampaignBrief' // a unique identifier for this form
})(reduxConnectedComponent)