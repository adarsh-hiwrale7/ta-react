import { bindActionCreators } from 'redux'
import { browserHistory } from 'react-router'
import { connect } from 'react-redux'
import { cloneArr } from '../../utils/utils'

import * as campaignsActions from '../../actions/campaigns/campaignsActions'
import * as s3functions from '../../utils/s3'
import * as utils from '../../utils/utils'

import CreateCampaignForm from './CreateCampaignForm'
import Globals from '../../Globals'
import moment from '../Chunks/ChunkMoment'
import notie from 'notie/dist/notie.js'
import PreLoader from '../PreLoader'
import PopupWrapper from '../PopupWrapper'
import reactDatepickerChunk from '../Chunks/ChunkDatePicker'


class UpdateCampaign extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      folders: [], // Although, folders are stored in redux state, we need to convert the
      loading: false,
      Moment: null,
      userTag: []
    }
  }

  componentWillReceiveProps (nextProps) {
    if (
      nextProps.campaigns.creation.created == true &&
      nextProps.campaigns.creation.created !==
        this.props.campaigns.creation.created
    ) {
      this.props.editCampaignSuccessfully();
    }
  }
  componentWillMount () {
    moment().then(moment => {
      reactDatepickerChunk()
        .then(reactDatepicker => {
          this.setState({
            ReactDatePicker: reactDatepicker,
            Moment: moment
          })
        })
        .catch(err => {
          throw err
        })
    })
  }
  createTagList (str) {
    var me = this
    var rx1 = /@\[([^\]]+)]\(([^)]+)\)/g
    let res = str.match(rx1)
    var userTag = []

    return new Promise(function (resolve, reject) {
      if (res) {
        res.forEach(function (tags) {
          /** Get user information */
          var tagsArray = tags.split('@[')
          var nameArray = tagsArray[1].split(']')
          var name = nameArray[0]
          var dept = nameArray[1]
            .substring(0, nameArray[1].length - 1)
            .split(':')[1]
            .split('_')
          var type = 'tags'
          if (
            nameArray[1].substring(0, nameArray[1].length - 1).split(':')[0] !==
            '(tags'
          ) {
            type = dept[1]=='company'?'company':
              dept[1]=='dept'?
                 "department"
                : "user";
            let tagsUserObject = {
              tagid: dept[0],
              tagName: name.replace(/ /g, ''),
              type: type
            }
            userTag.push(tagsUserObject)
          }

          /** description replace */
          if (type === 'tags') {
            str = str.replace(tags, '#' + name.replace(/ /g, ''))
          } else {
            str = str.replace(tags, '@' + name.replace(/ /g, ''))
          }
        })
        me.setState({
          userTag: userTag
        })
      }
      resolve(str)
    })
  }
 async handleSubmit (values) {
    if(values.campaignHashtag){
      await this.createTagList(values.campaignHashtag).then(campTag => {
         values.campaignHashtag = campTag
         var tagsData = [];
         var newTags = campTag.split(' ');
         newTags.map((tags,key) =>{
             tagsData.push(tags).toString();  
         })
         if(newTags.length > 4 ){
            notie.alert('error', 'Sorry! You cannot add more than 4 hashtags.', 5)
            return
         }
      })
    }
      let tags = []
      let socialData = []
      let me = this
      var searchedInput = this.props.searchedInput
      // values.description = desc
      values.userTag = me.state.userTag
      var newLocation = utils
        .removeTrailingSlash(this.props.location.pathname)
        .split('/')
      if(searchedInput!==undefined && newLocation[1]=="search" && newLocation[2]=="campaign")
      {
        var location = newLocation[1] 
      }
      else
      {
        var location = newLocation[2] == undefined ? 'live' : newLocation[2]
      }

      // Remove form errors if any
      var socialErr = document.querySelector('.socialError .error')
      socialErr.innerHTML = ''

      var campaignTags

      if (values.tags instanceof Object) {
        // when we modify tags,it is returned in an object
        if (typeof values.tags !== 'undefined') {
          for (var t = 0; t < values.tags.length; t++) {
            tags.push(values.tags[t].label)
          }
        }

        campaignTags = {
          campaign: tags
        }

        values['campaignTags'] = campaignTags
      }

      
       
        for (var prop in values) {
          if (
            values.hasOwnProperty(prop) &&
            prop.toString().startsWith('chk-') &&
            values[prop] == true
          ) {
            socialData.push({
              social_channel_id: prop.substr(prop.lastIndexOf('-') + 1)
            })
          }
        }
        values['socialData'] = socialData

        if (socialData.length == 0) {
          notie.alert('error', 'You must select at least 1 social channel', 5)
          socialErr.innerHTML = 'Required'
          return
        }
        var enddate=this.state.Moment?this.state.Moment.unix(values.enddate).format('MM-DD-YYYY'):''
        var today=this.state.Moment().unix()
        var today_date=this.state.Moment.unix(today).format('MM-DD-YYYY');
        var date1 = new Date(enddate.toString());
        var date2 = new Date(today_date.toString());
        var timeDiff = date2.getTime() - date1.getTime();
        var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
        if(diffDays<0){
        // to find date is live or expired          
          if(location!=="live"){
            //if location page is expired and try to update campaign then to redirect to live page and get campaign detail            
            browserHistory.push('/campaigns/live')
            location='live'
          }
        }
        else if(location == 'search')
        {
        }
        else if(location == "terminated"){
          browserHistory.push('/campaigns/terminated')
        }
        else{
            browserHistory.push('/campaigns/expired')
        }
        //me.props.campaignsActions.campaignsCreationInitiate()
        if (values.startdate <= values.enddate) {
        if(searchedInput!==undefined){
          me.props.campaignsActions.campaignsCreate(
            values,
            this.props.campaign.identity,
            location,
            this.state.Moment,
            searchedInput
          ) // 1 for updating
        }
        else
        {
          me.props.campaignsActions.campaignsCreate(
            values,
            this.props.campaign.identity,
            location,
            this.state.Moment
          ) // 1 for updating
        }
      } else {
        notie.alert('error', 'To Date can not be less then Start Date', 5)
      }
  }

  render () {
    const { loading } = this.props.campaigns.creation
    const { router, params, location, routes } = this.props
    const catID = this.props.catID

    var catName = ''
    if (catID != null) {
      catName = this.props.catName[0].name
    }

    var me = this
    return (
      <section className="register-page" >

          <div id="campaign-creation">
           {loading ? 
              <PreLoader  className = "Page"/> 
           :''}
           </div>
           <header className="heading">
            <h3>Update Campaign</h3>
          </header>
          
          <CreateCampaignForm
            refetchList={this.props.refetchList}
            campaign={this.props.campaign}
            onSubmit={this.handleSubmit.bind(this)}
            editdata={this.props.editdata}
            searchedInput={this.props.searchedInput}
            location={this.props.location}
          >
          </CreateCampaignForm>
          
       

      </section>
    )
  }
}

function mapStateToProps (state) {
  return {
    campaigns: state.campaigns
  }
}

function mapDispatchToProps (dispatch) {
  return {
    campaignsActions: bindActionCreators(campaignsActions, dispatch)
  }
}

let connection = connect(mapStateToProps, mapDispatchToProps)

module.exports = connection(UpdateCampaign)
