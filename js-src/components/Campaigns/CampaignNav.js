import Collapse, { Panel } from 'rc-collapse'; // collapse
// collapse css
import { IndexLink, Link } from 'react-router';
import * as utils from '../../utils/utils';

class CampaignNav extends React.Component {

    render(){
      var currentFilter = this.props.location_cat;
      return(
        <nav className='nav-side'>
        <Collapse
                accordion={false}
                defaultKey={'1'}
                defaultActiveKey={['1']}
              >
                <Panel className='active' header='STATUS' key={'1'}>
                  {(() => {
                    return (
                      <ul className='parent  clearfix'>
                        <li>
                          {
                            }
                          <Link
                            className={
                              currentFilter == 'all' ? ` active` : ``
                            }
                            to={`/campaigns/all`}
                          >
                            <span className='text'>All</span>
                          </Link>
                        </li>
                        <li>
                          <Link
                            className={
                              currentFilter == 'live'
                                ? ` active`
                                : ``
                            }
                            to={`/campaigns/live`}
                          >
                            <span className='text'>Live</span>
                          </Link>
                        </li>
                        <li>
                          <Link
                            className={
                              currentFilter == 'expired'
                                ? ` active`
                                : ``
                            }
                            to={`/campaigns/expired`}
                          >
                            <span className='text'>Expired</span>
                          </Link>
                        </li>
                        <li>
                          <Link
                            className={
                              currentFilter == 'terminated'
                                ? ` active`
                                : ``
                            }
                            to={`/campaigns/terminated`}
                          >
                            <span className='text'>Terminated</span>
                          </Link>
                        </li>
                      </ul>
                    )
                  })()}
                </Panel>
              </Collapse>
              </nav>
               )
  }
}
module.exports = CampaignNav;