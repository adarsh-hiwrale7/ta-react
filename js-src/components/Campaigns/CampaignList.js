import * as utils from '../../utils/utils'
import reactTooltip from '../Chunks/ChunkReactTooltip';
import TableSearchWrapper from '../TableSearchWrapper';
import Header from '../Header/Header';
import Pagination from '../Chunks/ChunkReactPagination';

var tableColumns = [
  ['avatar', 'Avatar ', 'a-center'],
  ['start_date', 'From', 'a-center'],
  ['end_date', 'To', ''],
  ['campaign_type', 'Type', ''],
  ['title', 'Title', ''],
  ['email', 'Email', ''],
  ['sms', 'SMS', ''],
  ['print', 'Print', ''],
  ['task', 'Task', ''],
  ['targeted_audience', 'Audience', ''],
  ['post_count', 'Posts', 'a-center'],
  ['asset_count', 'Assets', 'a-center'],
  ['channels', 'Channels', 'a-center'],
]
var eventTableColumns = [
  ['title', 'Title ', 'a-center'],
  ['notes', 'Note', 'a-center'],
  ['end_date', 'Due Date', ''],
  ['created_by', 'Created By', '']
]
var camapignType=[
  ['All','All',''],
  ['Internal','Internal',''],
  ['Talent','Talent',''],
  ['Customer','Customer',''],
]
var campaignList
var search = ""
var campaign_data_list = []
var pagination=1
export default class CampaignList extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      search: '',
      filter_state: [],
      tooltip: null,
      Pagination:null,
    }
  }
  getIndex(value, arr, prop) {
    for (var i = 0; i < arr.length; i++) {
      if (arr[i][prop] === value) {
        return i;
      }
    }
    return -1; //to handle the case where the value doesn't exist
  }
  componentWillReceiveProps(nextProps) {
  if (typeof nextProps.campaign_data !== 'undefined' && nextProps.campaign_data !== this.props.campaign_data) {
      let selectCampaign = this.props.selectCampaign;
      if (selectCampaign !== null) {
        var index = this.getIndex(selectCampaign, nextProps.campaign_data, 'identity');
        this.props.campaignClick(nextProps.campaign_data[index])
      }
    }
    this.setState({main_state:nextProps.campaign_data})
    this.setState({filter_state:nextProps.campaign_data})
    if(this.props.location.pathname !== nextProps.location.pathname){
      pagination=1
    }
  }
  componentWillMount(){
    this.setState({
      main_state:this.props.campaign_data
    })
      reactTooltip().then(reactTooltip => {
        Pagination().then(pagination=>{
          this.setState({tooltip: reactTooltip,Pagination:pagination})
        })
      })
  }
  componentDidMount() {
    //to select the checkboxes of camapign type )
    const type= this.props.selectedCampType
    for (let index = 0; index < type.length; index++) {
      const element = type[index];
      this.changeState(element,'')
    }
    const selectedType = this.props.typeSelected
    if(document.getElementById('campEventType') !== null )
      document.getElementById('campEventType').value = selectedType
    }
  
  /**
   * @author disha
   * to add page in table
   * @param {*} totalPage
   */
  handlePageChange(pageNumber)
  {
    this.setState({
      activePage: pageNumber
    })
    pagination=pageNumber
    this.fetchOtherPageData(pageNumber)
  }
  /**
   * @author disha
   * to add page in table
   * @param {*} totalPage
   */
  handleEventPageChange(pageNumber)
  {
    this.setState({
      activeEventPage: pageNumber
    })
    pagination=pageNumber
    this.props.eventPageNo(pageNumber)
  }
  fetchOtherPageData (pageno) {
    // props comes from analyticsContainer to send next page number
    this.props.pageNo(pageno)
  }
  createString(arr, key) {
    return arr
      .map(function (obj) {
        return obj[key]
      })
      .join(',')
  }
  createDropdown() {
    var me = this;
    return (
      <div className='table-menu dropdown-container'>
        <div className='checkbok-menu-button'>
          <ul class='dd-menu'>
            <li class='button-dropdown'>
              <a class='dropdown-toggle btn btn-theme'>Filter columns</a>
              <ul class='dropdown-menu checkboxDropdown'>
                {
                  tableColumns ? tableColumns.map(function (column, columnIndex) {
                    let columnState = column[0];
                    let columnLabel = column[1];
                    if (me.state[columnState] == undefined) {
                      me.setState({
                        [columnState]: ''
                      })
                    }
                    return (
                      <li key={columnIndex}>
                        <div className='checkbox form-row'>
                          <input
                            id={columnState}
                            name={columnState}
                            className='checknotify'
                            type='checkbox'
                            onChange={me.checkDetails.bind(
                              me, columnState
                            )}
                            checked={me.state[columnState] == '' ? true : false}
                          />
                          <label for={columnState}>{columnLabel}</label>
                        </div>
                      </li>
                    )
                  }) : ''
                }
              </ul>
            </li>
          </ul>
        </div>
      </div>
    )
  }
  createDropdownForType() {
    var me = this;
    return (
      <div className='table-menu dropdown-container'>
        <div className='checkbok-menu-button'>
        <ul class='dd-menu'>
            <li class='button-dropdown'>
              <a class='dropdown-toggle btn btn-theme' onClick={this.props.handleFilterTypeDropDown}>Filter Campaign Type</a>
              <ul class={`dropdown-menu checkboxDropdown ${this.props.openTypeFilterDropDown ? 'show':'hide'}`}>
                {
                  camapignType ? camapignType.map(function (type, columnIndex) {
                    let columnState = type[0];
                    let columnLabel = type[1];
                    return (
                      <li key={columnIndex}>
                        <div className='checkbox form-row'>
                          <input
                            id={columnState}
                            name={columnState}
                            className='checknotify'
                            type='checkbox'
                            onChange={me.checkCampaignType.bind(
                              me, columnState
                            )}
                            checked={me.state[columnState] == '' ? true : false}
                          />
                          <label for={columnState}>{columnLabel}</label>
                        </div>
                      </li>
                    )
                  }) : ''
                }
              </ul>
            </li>
          </ul>
        </div>
      </div>
    )
  }
  createDropDownForCampEvent(){
    var me = this;
    return (
      <div className='table-menu dropdown-container camapign-event-wrapper'>
        <div className='checkbok-menu-button dropdown-toggle'>
        <select id ="campEventType" onChange={this.handleTypeSelection.bind(this)} >
          <option value="campaign">Campaign</option>
          <option value="event">Event</option>
          </select>
        </div>
      </div>
    )
  }
  handleTypeSelection(e){
    pagination =1
    this.props.fetchDataTypeWise(e.target.value)
  }
  changeState(stateName, value) {
    var newState = {};
    newState[stateName] = value;
    this.setState(newState);
  }
  checkDetails(e) {
    var checkboxvalue = document.getElementById(e).checked
    if (checkboxvalue == true) {
      this.changeState(e, '');
    } else {
      this.changeState(e, 'hide');
    }
  }
  checkCampaignType(e) {
    var checkboxvalue = document.getElementById(e).checked
    if (checkboxvalue == true) {
      this.changeState(e, '');
    } else {
      this.changeState(e, 'hide');
    }
    //call campaign list api and also store the selected data in parent bcz this page will recalled every time and props/state will be lost
    this.props.campaignTypeFilter(e)
  }
  listEditCampaign(campaign) {
    this.props.campaignUpdateClick(campaign)
  }
  search_text(e) {
    var me = this;
    let Tabledata = campaignList
    search = e.target.value.toLowerCase()
    if (e.target.value !== "") {
      var tempobj = Tabledata.filter(function (hero) {
        var tdData1 = me.props.Moment.unix(hero.start_date).format('DD-MM-YYYY')
        var tdData2 = me.props.Moment.unix(hero.end_date).format('DD-MM-YYYY')
        var Socio = false

        hero.SocialChannel.data.filter(function (hero) {
          if (hero.media.toLowerCase().includes(search)) {
            Socio = true;
          }
          else
            return false;
        })
        if (hero.User.data.firstname.concat(" " + hero.User.data.lastname).toLowerCase().includes(search) ||
          tdData1.toLowerCase().includes(search) ||
          tdData2.toLowerCase().includes(search) ||
          hero.campaign_type.toLowerCase().includes(search) ||
          hero.title.toLowerCase().includes(search) ||
          Socio ||
          (hero.targeted_audience !== null ? hero.targeted_audience.toLowerCase().includes(search) : false))
          return true;
        else
          return false
      })
      this.setState({main_state:tempobj})
      this.campaignTable(tempobj);
    }
    else
    {
      this.setState({main_state:this.state.filter_state})
      this.campaignTable(this.props.campaign_data);
    }
  }
  campaignTable(campaignData) {
    let roleName =JSON.parse(localStorage.getItem('roles')) !== null? JSON.parse(localStorage.getItem('roles'))[0].role_name:''
    var channel = [];
    var Tabledata = campaignData;
    var me = this;
    var campaignMetaData=this.props.campaignMetaData
    return (
      <div className='table-wrapper'>
      {this.props.reactable ? 
        <this.props.reactable.Table className='table responsive-table campaign-table' id="campaign-table"
          itemsPerPage={10}
          pageButtonLimit={5}>
          <this.props.reactable.Thead>
            {
              tableColumns.map(function (column, columnIndex) {
                let columnState = column[0];
                let columnLabel = column[1];
                let centerClass = column[2];
                return (
                  <me.props.reactable.Th
                    key={columnIndex}
                    column={columnState}
                    className={`${columnState} ${me.state[columnState]} ${centerClass}`}
                  >
                    {
                      columnState !== 'avatar' ?
                        <strong className='name-header'>{columnLabel}</strong>
                        : '' 
                    }

                  </me.props.reactable.Th>
                )
              })
            }
          </this.props.reactable.Thead>
          {typeof Tabledata != 'undefined' ? Tabledata.map(
            (campaign, i) => {
              var tags = campaign.Tags.data
              var social = this.createString(campaign.SocialChannel.data, 'media')
              let channels = social.split(',')
              let campinguseravtar = ''
              var defaultimg = "https://app.visibly.io/img/default-avatar.png"
              typeof campaign.User !== 'undefined' ?
                typeof campaign.User.data !== 'undefined' ?
                  campinguseravtar = {
                    backgroundImage: `url(${campaign.User.data.avatar})`
                  }
                  : campinguseravtar = {
                    backgroundImage: defaultimg
                  }
                : campinguseravtar = {
                  backgroundImage: defaultimg
                }
              return (
                //onClick={this.tablePopupanylitcs.bind(this,feeddata)}
                
                <this.props.reactable.Tr className='table-body-text' key={i}  onClick={function (ev) {
                  me.props.campaignClick(campaign)
                }} >
                  {
                    tableColumns.map(function (column, columnIndex) {
                      let columnState = column[0];
                      let columnLabel = column[1];
                      let centerClass = column[2];
                      let tdData;
                      const displayNA = campaign.campaign_type == 'Customer' || campaign.campaign_type =='Talent' ? true : false;
                      switch (columnState) {
                        case 'avatar':
                          tdData = <div>
                            <div style={campinguseravtar} className="background-user-popup">
                              {typeof campaign.User.data !== 'undefined' ?
                                <img
                                  src={campaign.User.data.avatar}
                                  alt={campaign.User.data.avatar}
                                  className='list-avatar'
                                  data-tip={campaign.User.data.email}
                                  data-for={campaign.User.data.identity}
                                /> :
                                <img
                                  src={defaultimg}
                                  alt={defaultimg}
                                  className='list-avatar'
                                  data-tip={campaign.User.data.email}
                                  data-for={campaign.User.data.identity}
                                />}
                            </div>
                            {me.state.tooltip?
                            <me.state.tooltip
                              id={campaign.User.data.identity}
                              type='warning'
                              effect = 'solid'
                              className = 'tooltipsharepopup'

                            />:''}
                          </div>
                          break
                        case 'start_date':
                          tdData = me.props.Moment.unix(campaign.start_date).format('DD-MM-YYYY')
                          break
                        case 'end_date':
                          tdData = me.props.Moment.unix(campaign.end_date).format('DD-MM-YYYY')
                          break
                        case 'campaign_type':
                          tdData = campaign.campaign_type
                          break
                        case 'title':
                          tdData = <span className='tableOneLine'>{campaign.title}</span>
                          break
                        case 'email':
                          tdData = displayNA ? 'NA' : <span className='tableOneLine'>{campaign.email_count}</span>
                          break
                        case 'sms':
                          tdData = <span className='tableOneLine'>{campaign.sms_count}</span>
                          break
                        case 'print':
                          tdData = displayNA ? 'NA' : <span className='tableOneLine'>{campaign.print_count}</span>
                          break
                        case 'task':
                          tdData = <span className='tableOneLine'>{campaign.task_count}</span>
                          break
                        case 'targeted_audience':
                          tdData = <span className='tableOneLine'>{campaign.targeted_audience}</span>
                          break
                        case 'post_count':
                          tdData = campaign.post_count
                          break
                        case 'asset_count':
                          tdData = campaign.asset_count
                          break
                        case 'channels':
                          tdData = <span className='socialAccountsListing'>
                            {channels.indexOf('Facebook') > -1
                              ? <a className='social fb'>
                                <i className='fa fa-facebook' aria-hidden='true' />
                              </a>
                              : null}
                            {channels.indexOf('Twitter') > -1
                              ? <a className='social tw'>
                                <i className='fa fa-twitter' aria-hidden='true' />
                              </a>
                              : null}
                            {channels.indexOf('Instagram') > -1
                              ? <a className='social in'>
                                <i className='fa fa-instagram' aria-hidden='true' />
                              </a>
                              : null}
                            {channels.indexOf('Linkedin') > -1
                              ? <a className='social linkedin'>
                                <i className='fa fa-linkedin' aria-hidden='true' />
                              </a>
                              : null}
                            {channels.indexOf('Internal') > -1
                              ? <a className='social iw'>
                                <i class="material-icons">vpn_lock</i>
                              </a>
                              : null}
                          </span>
                          break;
                        default:
                          break
                      }

                      return (
                        
                          <me.props.reactable.Td
                            key={columnIndex}
                            column={columnState}
                            className={`${columnState} ${me.state[columnState]} ${centerClass}`}
                            data-rwd-label={columnLabel}

                          >
                            <div className='campaignTdDiv' onClick={function (ev) {
                              me.props.campaignClick(campaign)
                            }}>
                              {tdData}
                            </div>
                          </me.props.reactable.Td>

                      )
                    })
                  }
                </this.props.reactable.Tr>
              )
            }) : ''
          }
        </this.props.reactable.Table> :''}
        <div className='pagination-wrapper'>
          {this.state.Pagination?
            <this.state.Pagination.default
              hideFirstLastPages
              prevPageText='Prev'
              nextPageText='Next'
              pageRangeDisplayed={7}
              activePage={pagination}
              itemsCountPerPage={10}
              totalItemsCount={Object.keys(campaignMetaData).length >0 ? campaignMetaData.pagination.total !=undefined? campaignMetaData.pagination.total:0: 0}
              onChange={::this.handlePageChange}
            />:''}
        </div>
      </div>
      
      /* table-anytlics-body end */
    )
  }
  eventTable(eventData) {
    let roleName =JSON.parse(localStorage.getItem('roles')) !== null? JSON.parse(localStorage.getItem('roles'))[0].role_name:''
    var channel = [];
    var Tabledata = eventData;
    var me = this;
    var eventMetaData=this.props.eventMetaData
    return (
      <div className='table-wrapper'>
      {this.props.reactable ? 
        <this.props.reactable.Table className='table responsive-table campaign-table' id="campaign-table"
          itemsPerPage={10}
          pageButtonLimit={5}>
          <this.props.reactable.Thead>
            {
              eventTableColumns.map(function (column, columnIndex) {
                let columnState = column[0];
                let columnLabel = column[1];
                let centerClass = column[2];
                return (
                  <me.props.reactable.Th
                    key={columnIndex}
                    column={columnState}
                    className={`${columnState} ${me.state[columnState]} ${centerClass}`}
                  >
                    <strong className='name-header'>{columnLabel}</strong>
                  </me.props.reactable.Th>
                )
              })
            }
          </this.props.reactable.Thead>
          {typeof Tabledata != 'undefined' ? Tabledata.map(
            (event, i) => {
              let campinguseravtar = ''
              var defaultimg = "https://app.visibly.io/img/default-avatar.png"
              typeof event.User !== 'undefined' ?
                typeof event.User.data !== 'undefined' ?
                  campinguseravtar = {
                    backgroundImage: `url(${event.User.data.avatar})`
                  }
                  : campinguseravtar = {
                    backgroundImage: defaultimg
                  }
                : campinguseravtar = {
                  backgroundImage: defaultimg
                }
              return (
                <this.props.reactable.Tr className='table-body-text' key={i}  onClick={function (ev) {
                  me.props.openEventViewPopup(event)
                }} >
                  {
                    eventTableColumns.map(function (column, columnIndex) {
                      let columnState = column[0];
                      let columnLabel = column[1];
                      let centerClass = column[2];
                      let tdData;
                      switch (columnState) {
                        case 'title':
                          tdData = <span className='tableOneLine' >{event.title}</span>
                          break
                        case 'notes':
                          tdData = <span className='tableOneLine'>{event.notes} </span>
                          break
                        case 'end_date':
                          tdData = me.props.Moment.unix(event.end_date).format('DD-MM-YYYY')
                          break
                        case 'created_by':
                          tdData = event.created_by
                          break
                        default:
                          break
                      }
                      return (
                          <me.props.reactable.Td
                            key={columnIndex}
                            column={columnState}
                            className={`${columnState} ${me.state[columnState]} ${centerClass}`}
                            data-rwd-label={columnLabel}

                          >
                            <div className='campaignTdDiv' onClick={function (ev) {
                              me.props.openEventViewPopup(event)
                            }}>
                              {tdData}
                            </div>
                          </me.props.reactable.Td>

                      )
                    })
                  }
                </this.props.reactable.Tr>
              )
            }) : ''
          }
        </this.props.reactable.Table> :''}
        <div className='pagination-wrapper'>
          {this.state.Pagination?
            <this.state.Pagination.default
              hideFirstLastPages
              prevPageText='Prev'
              nextPageText='Next'
              pageRangeDisplayed={7}
              activePage={pagination}
              itemsCountPerPage={10}
              totalItemsCount={Object.keys(eventMetaData).length>0 ? eventMetaData.pagination.total !=undefined? eventMetaData.pagination.total:0: 0}
              onChange={::this.handleEventPageChange}
            />:''}
        </div>
      </div>
      
      /* table-anytlics-body end */
    )
  }
  HandleEventCreation(e){
    this.props.addEvent(e)
  }
  render3dots() {
    return (
      <div className='action-button'>
        <ul className='dd-menu context-menu user_3dots'>
          <li className='button-dropdown'>
            <a className='dropdown-toggle user_drop_toggle'>
              <i className='material-icons'>more_vert</i>
            </a>
            <ul className='dropdown-menu user_dropAction'>
              <li>
                <a onClick={this.HandleEventCreation.bind(this)}>Add New Event</a>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    )
  }

  render() {
    let roleName =JSON.parse(localStorage.getItem('roles')) !== null? JSON.parse(localStorage.getItem('roles'))[0].role_name:''
    let notShow = 0
    if (roleName === 'employee' || roleName === 'moderator') {
      notShow = 1
    }
    campaignList = this.props.campaign_data
    var me = this
    var Moment = this.props.Moment
    return (
      <div className='table-white-container'>
        <div className='tableControllers clearfix'>
          <div className={`tableFilterDropdown`}>
            {this.render3dots()}
          </div>
          <div className={`tableFilterDropdown ${this.props.typeSelected == 'event' ? 'hide':''}`} >
            {this.createDropdown()}
          </div>
          <div className={`tableFilterDropdown ${this.props.typeSelected == 'event' ? 'hide':''}`}>
            {this.createDropdownForType()}
          </div>
          {this.props.location.pathname == "/campaigns/live" || this.props.location.pathname === "/campaigns"?
              <div className={`tableFilterDropdown`}>
                {this.createDropDownForCampEvent()}
              </div>
          :''}
          {this.props.location.pathname !== "/search/campaign" && this.props.typeSelected == 'campaign'?
            <TableSearchWrapper>
            <input placeholder='Search in campaign' type="text" onChange={this.search_text.bind(this)} />
            </TableSearchWrapper>:''
          }
        </div>
        <div className='widget'>
        {(search!=="" && this.props.typeSelected == 'campaign')?(this.state.main_state!==undefined && this.state.main_state.length>0 && this.props.typeSelected == 'campaign')?
          this.campaignTable(this.state.main_state):
          <div>
            <div class="no-data-block">No campaign found.</div>
          </div>
        :
        this.props.typeSelected == 'campaign' ? 
          this.props.campaign_data.length == 0 ?
            <div>
                <div class="no-data-block">No campaign found.</div>
            </div>
          : this.campaignTable(this.props.campaign_data)
          : this.props.typeSelected == 'event'?
            this.props.eventList.length == 0  ?
            <div>
                <div class="no-data-block">No event found.</div>
            </div>
          : this.eventTable(this.props.eventList)
        :''
        }

        </div>
      </div>

    )
  }
}
