;
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Link } from "react-router";
import { Field, reduxForm } from 'redux-form';
import PreLoader from '../PreLoader';
import CenterPopupWrapper from '../CenterPopupWrapper';
import RecipientsPopupForm from './RecipientsPopupForm';
import EditMailPopupForm from './EditMailPopupForm';
import * as campaignsActions from '../../actions/campaigns/campaignsActions';
class SendMailPopup extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        selectedfeedtab :'edit' ,
        userTag : [],
        enableRecipients : false
      }
      this
      .createTagList
      .bind(this);
    }
  feedUpdateTab(tab){
    this.setState({
      selectedfeedtab:tab
      
    })
  }
 
  createTagList(str) {
   var me = this;
   var rx1 = /@\[([^\]]+)]\(([^)]+)\)/g;
   let res = str.match(rx1);
   var userTag = [];

   return new Promise(function (resolve, reject) {
     if (res) {
       res
         .forEach(function (tags) {
           /** Get user information */
           var tagsArray = tags.split("@[");
           var nameArray = tagsArray[1].split("]");
           var name = nameArray[0];
           var dept = nameArray[1]
             .substring(0, nameArray[1].length - 1)
             .split(":")[1]
             .split('_');
           var type = "tags";
           if (nameArray[1].substring(0, nameArray[1].length - 1).split(":")[0] !== "(tags") {
             type = dept[1] == 'company' ? 'company' :
               dept[1] == 'dept' ?
                 "department"
                 : "user";

             let tagsUserObject = {
               tagid: dept[0],
               tagName: name.replace(/ /g, ''),
               type: type
             };
             userTag.push(tagsUserObject)
           }

           /** description replace */
           if (type === "tags") {
             str = str.replace(tags, "#" + name.replace(/ /g, ''));
           } else {
             str = str.replace(tags, "@" + name.replace(/ /g, ''));
           }

         })
       me.setState({
         userTag: userTag
       })
     }
     resolve(str);
   })
 }
 closeAddUserPopup(){
    this.props.closeAddUserPopup();
 }
 handleMailPopupSubmit(val){
    let type = "addUserPopup";
    let campaignsId = this.props.campaginData.campaign.identity;
    let projectId = this.props.campaginData.contentProjectData.project_identity;
    let contentType = this.props.campaginData.contentProjectData.type;
    let callForAssignee = false;
    let isTemplateSelected = false;
    let callAfterSaveTemplate= false;
    this.setState({
       enableRecipients : true
    })
  
   var obj = {
      "from_email" : val.From,
      "subject":val.subjectline,
      "email_preview": val.previewtext
   }
    this.props.campaginData.campaignActions.editContentProject(type ,  obj , campaignsId , projectId , callForAssignee , isTemplateSelected ,callAfterSaveTemplate , contentType);
     //this.props.saveMailPreviewMail(val);
  }
  handleRecipientSubmit(val){
   let guest_user = val.guests;
   let campaignsId = this.props.campaginData.campaign.identity;
   let projectId = this.props.campaginData.contentProjectData.project_identity;
   let contentType = this.props.campaginData.contentProjectData.type;

    let me = this;
    if (typeof (val.users) !== "undefined" && val.users != "") {
      this
        .createTagList(val.users)
        .then((desc) => {
          let me = this;

          var ObjToSend = {
            "project_id" :  projectId,
            "user_tag": me.state.userTag,
            "guest_user":guest_user,
          }
          me.props.campaignsActions.saveRecipientsData(ObjToSend);
        });
    } else {
      let me = this;
      var ObjToSend = {
         "project_id" : projectId,
         "guest_user": guest_user,
      }
      me.props.campaginData.campaignActions.saveRecipientsData(ObjToSend);
    }
}
removeRecipientsUser(id,type){
   var tagId=''
   var userId=''
   let projectId = this.props.campaginData.contentProjectData.project_identity;
   var jsonBody = {
      project_id:projectId
    }
   if(type=='user'){
      var objToADD={
         "user_identity":id
       }
       Object.assign(jsonBody,objToADD)
   }else if(type == 'tag'){
      var objToADD={
         "tag_identity":id
       }
       Object.assign(jsonBody,objToADD)
   }else if(type=='guestuser'){
      var objToADD={
        "guest_user":id
      }
      Object.assign(jsonBody,objToADD)
    }
   
   this.props.campaginData.campaignActions.deleteRecipientUser(jsonBody);
}
componentDidMount(){
   this.props.campaginData.campaignActions.fetchRecipientsData(this.props.projectId);  
   
}
componentWillReceiveProps(newprops){
   //to redirect on recipients tab if detail is filled successfully
   if(this.props.campaginData.campaigns.contentSaved !== newprops.campaginData.campaigns.contentSaved && newprops.campaginData.campaigns.contentSaved == true){
      this.props.campaginData.campaignActions.editContentProjectError();
      this.setState({
         enableRecipients:true
      })
      this.feedUpdateTab('recipients')
   }
   if(this.props.campaginData.campaigns.projectHtmlData !== newprops.campaginData.campaigns.projectHtmlData && newprops.campaginData.campaigns.projectHtmlData !== undefined){
      if( newprops.campaginData.campaigns.projectHtmlData.from_email !== null){
         this.setState({
            enableRecipients:true
         })
      }
   }
}
render(){
   var usersRecipientsData,
   guestRecipientsData,
   tagRecipientsData=''
   if(this.props.campaginData.campaigns.fetchRecipientData && Object.keys(this.props.campaginData.campaigns.fetchRecipientData.fetchRecipientDataList).length>0) {
      const recipientData = this.props.campaginData.campaigns.fetchRecipientData.fetchRecipientDataList
         usersRecipientsData=recipientData.register_user.length>0 ? recipientData.register_user :''
         tagRecipientsData=recipientData.tag_data.length > 0 ? recipientData.tag_data :''
         guestRecipientsData=recipientData.guest_email.length>0 ? recipientData.guest_email :''
   }
   var adddUserPopupTitle=this.props.campaginData.contentProjectData?this.props.campaginData.contentProjectData.title:''
   const displayBottomBorder = (usersRecipientsData !=='' && usersRecipientsData !== undefined) || (tagRecipientsData !=='' && tagRecipientsData !== undefined)|| (guestRecipientsData !=='' && guestRecipientsData !== undefined) ? true :false
return(
<div className = "mailPopupContainerWrapper">
  <CenterPopupWrapper className="updateFeedPopup">
    <div className="customfeedMain">
       <div className="createCustomfeedpopup">
          <div className="planChangePopup">
             <header className="heading clearfix">
                <h3>{adddUserPopupTitle}</h3>
                <button id="close-popup" className="btn-default" onClick = {this.closeAddUserPopup.bind(this)} ><i className="material-icons">clear</i></button>
             </header>
             <div className="widget" id="setting_walkthrough">
                <div className="inner-popup">
                   <section className="register-page">
                      <div className="listMenu">
                         <nav className="nav-side">
                            <ul className="parent">
                               <li>
                                  <a href="javascript:;" onClick={this.feedUpdateTab.bind(this,"edit")} className={this.state.selectedfeedtab=='edit'?"active":''}><span className="text" data-toggle="tab">Details</span></a>
                               </li>
                               <li className={this.state.enableRecipients ? '':'disabled'}>
                                  <a href="javascript:;"  onClick={this.feedUpdateTab.bind(this,"recipients")} className={this.state.selectedfeedtab=='recipients'?"active":''} ><span classNamne="text" data-toggle="tab">Recipients</span></a>
                               </li>
                            </ul>
                         </nav>
                      </div>
                   </section>
                </div>
                {this.state.selectedfeedtab=='edit' ? 
                  <div className = "editMailPopupWrapper"> 
                        <EditMailPopupForm 
                        projectId={this.props.projectId}
                        campaginData={this.props.campaginData}
                         onSubmit ={this.handleMailPopupSubmit.bind(this)}
                        />
                   </div>
                : 
                <div className="memberfeed">
                   <div className={`feedDetailCover ${displayBottomBorder ? '' :'hideBottomBorder'}`}>
                      <div className="feedDetail">
                      {usersRecipientsData !=='' && usersRecipientsData !== undefined ?
                      <div>
                         <div className="feed-user-title">
                            <h3>Users</h3>
                         </div>
                         <div className="feedrow clearfix">
                            <table>
                               <thead>
                               {usersRecipientsData.map((user,index)=>{
                                  return(
                                    <tr key={index}>
                                       <td>
                                          <div className="feedrowimage">
                                             <img src={user.avatar}  onError={(e)=>{e.target.src="https://app.visibly.io/img/default-avatar.png"}}/>
                                          </div>
                                       </td>
                                       <td>
                                          <div title='' className="feedrowname">
                                             {user.first_name}
                                          </div>
                                       </td>
                                       <td>
                                          <div title='' className="feedrowemail">
                                             {user.last_name}
                                          </div>
                                       </td>
                                       <td>
                                          <div title='' className="feedrowmember">
                                                {user.email}
                                          </div>
                                       </td>
                                       <td>
                                          <div className="feedrowremoveicon">
                                             <button id="close-popup" className="btn-default" onClick={this.removeRecipientsUser.bind(this,user.identity,'user')} ><i className="material-icons">clear</i></button>
                                          </div>
                                       </td>
                                    </tr>
                                    )
                                  })}
                               </thead>
                            </table>
                            </div>
                                
                            </div>
                                  :''}
                            
                           {(tagRecipientsData !=='' && tagRecipientsData !== undefined)  || (guestRecipientsData !=='' && guestRecipientsData !== undefined)?
                           <div>
                              <div className="feed-user-title">
                                    <h3>Guest Users</h3>
                              </div>
                                 <ul className="multiple_emails-ul" >
                                 {tagRecipientsData !=='' && tagRecipientsData !== undefined?
                                  tagRecipientsData.map((tagData,index)=>{
                                     return(
                                       
                                          <li className="multiple_emails-email" key={index}>
                                             <span className="email_name" data-email="disha@gmail.com">
                                                {tagData.name}
                                                </span>
                                                <span id="close-popup" className="btn-default multiple_emails-close">
                                                      <i className="material-icons" onClick={this.removeRecipientsUser.bind(this,tagData.identity,'tag')}>clear</i>
                                                </span>
                                             </li>
                                         
                                          )
                                       }):''}
                                     </ul>
                                    </div>
                                    :''}

                                     <ul className="multiple_emails-ul" >
                                  {guestRecipientsData !=='' && guestRecipientsData !== undefined?
                                  guestRecipientsData.map((guest,index)=>{
                                     return(
                                       
                                          <li className="multiple_emails-email" key={index}>
                                             <span className="email_name" data-email="disha@gmail.com">
                                                {guest}
                                                </span>
                                                <span id="close-popup" className="btn-default multiple_emails-close">
                                                      <i className="material-icons" onClick={this.removeRecipientsUser.bind(this,guest,'guestuser')}>clear</i>
                                                </span>
                                             </li>
                                          
                                          )
                                       })
                                    :''}
                                    </ul>
                                    
                         
                      </div>
                   </div>
                    <div>
                       <RecipientsPopupForm campaginData = {this.props.campaginData } onSubmit = {this.handleRecipientSubmit.bind(this)}/>
                    </div>
                
                </div>
                }
                
             </div>
          </div>
       </div>
    </div>
    
 </CenterPopupWrapper>
 </div>
)
}}

function mapStateToProps (state) {
   return {}
 }
function mapDispatchToProps (dispatch) {
   return {
     campaignsActions: bindActionCreators(campaignsActions, dispatch),
   }
 }
 
let connection = connect(mapStateToProps, mapDispatchToProps)
 
module.exports = connection(SendMailPopup);
