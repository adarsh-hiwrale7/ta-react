import CenterPopupWrapper from '../CenterPopupWrapper';
import ChunkSelect from '../Chunks/ChunkReactSelect';
import reactDatepickerChunk from '../Chunks/ChunkDatePicker';
import moment from '../Chunks/ChunkMoment';
import { Field, reduxForm } from 'redux-form'
import { renderField } from '../Helpers/ReduxForm/RenderField'
import { connect } from 'react-redux'
import ReactDatePicker from '../Helpers/ReduxForm/ReactDatePicker'
import * as utils from '../../utils/utils';
import MentionTextArea from '../Helpers/ReduxForm/MentionTextArea';
import notie from "notie/dist/notie.js"

const required = value => (value ? undefined : 'Required')
let optionMain=[];
const optionOperation = [
  { value: 'gt', label: 'Is more than' },
  { value: 'lt', label: 'Is less than' },
  { value: 'IB', label: 'Is inbetween' }
];

const optionOperationDefault = [
    { value: 'gt', label: 'Is more than' }
]
var indexArray={}
let optionAction = []
let lastIndex = null
let handleFormVal = []
handleFormVal[0] = []
handleFormVal[0]['segment'] = ''
handleFormVal[0]['operation'] = 'equal'
handleFormVal[0]['value'] = ''
handleFormVal[0]['showInHeatmap'] = 0
let checkAPICall = false
let selectedMainOptionArray=[]

class AddSegmentPopup extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          selectedMainOption0: null,
          selectedSubOption0: null,
          selectedOperationOption0:null, 
          dynamicField: ['field'],
          numberOfField:1,
          showOperation0: 'equal',
          currentIndex:null,
          ReactDatePicker: null,
          Moment: null,
          selectedArray:[],
          removedIndex:[],
          twoBox0:false,
          editId:null,
          lastIndex: 0,
          currSegmentId:null,
          saveSegmentation: 'All',
          currSegmentTab :'quickSegment',
          validateSegment : false,
          selectedStatus:'All',
          PreviewSegment:false,
          AddSegmentFlag : false,
          currSegmentName:'',
          reactSelect :null
 
        }
    }

    componentWillMount () {
        var me = this
        moment().then(moment => {
          ChunkSelect().then(reactselect => {
          reactDatepickerChunk()
            .then(reactDatepicker => {
                this.setState({
                  reactSelect: reactselect,
                  ReactDatePicker: reactDatepicker,
                  Moment: moment,
                })
              })
            })
            .catch(err => {
              throw err
            }) 

        })

    }

    componentWillReceiveProps(nextProps) {
      if(this.props.newSegmentPreview!==nextProps.newSegmentPreview && nextProps.newSegmentPreview==false){
        this.setState({
          PreviewSegment : nextProps.newSegmentPreview
        })
      }
       let optionArrayDrop = [];
      
       if(this.props.regionData !== nextProps.regionData && nextProps.regionData.length > 0){
        var storeIndex = indexArray['region']!==undefined?indexArray['region']:this.state.currentIndex;
          nextProps.regionData.map((regionDataVar,index)=>{
              optionArrayDrop.push({value:regionDataVar.identity, label:regionDataVar.name})
          },optionAction[storeIndex] = optionArrayDrop)
       }
       if(this.props.countryData !== nextProps.countryData && nextProps.countryData.length > 0){
        var storeIndex = indexArray['country']!==undefined?indexArray['country']:this.state.currentIndex;
          nextProps.countryData.map((countryDataVar,index)=>{
              optionArrayDrop.push({value:countryDataVar.identity, label:countryDataVar.title})
          },optionAction[storeIndex] = optionArrayDrop)
       }
       if(this.props.officeData !== nextProps.officeData && nextProps.officeData.length > 0){
        var storeIndex = indexArray['office']!==undefined?indexArray['office']:this.state.currentIndex;
          nextProps.officeData.map((officeDataVar,index)=>{
              optionArrayDrop.push({value:officeDataVar.identity, label:officeDataVar.name})
          },optionAction[storeIndex] = optionArrayDrop)
       }
       if(this.props.departmentData !== nextProps.departmentData && nextProps.departmentData.length > 0){
        var storeIndex = indexArray['department']!==undefined?indexArray['department']:this.state.currentIndex;
          nextProps.departmentData.map((departmentDataVar,index)=>{
              optionArrayDrop.push({value:departmentDataVar.identity, label:departmentDataVar.name})
          },optionAction[storeIndex] = optionArrayDrop)
       }
       if(this.props.businessData !== nextProps.businessData && nextProps.businessData.length > 0){
        var storeIndex = indexArray['business']!==undefined?indexArray['business']:this.state.currentIndex;
          nextProps.businessData.map((businessDataVar,index)=>{
              optionArrayDrop.push({value:businessDataVar.identity, label:businessDataVar.name})
          },optionAction[storeIndex] = optionArrayDrop)
       }
       if(this.props.companyRoleData !== nextProps.companyRoleData && nextProps.companyRoleData.length > 0){
        var storeIndex = indexArray['company_role']!==undefined?indexArray['company_role']:this.state.currentIndex;
          nextProps.companyRoleData.map((roleDataVar,index)=>{
              optionArrayDrop.push({value:roleDataVar.identity, label:roleDataVar.name})
          },optionAction[storeIndex] = optionArrayDrop)
       }
       if(this.props.levelData !== nextProps.levelData && Object.keys(nextProps.levelData).length > 0){
        var storeIndex = indexArray['level']!==undefined?indexArray['level']:this.state.currentIndex;
          optionAction[storeIndex] = nextProps.levelData.data
       }
       if(this.props.genderData !== nextProps.genderData && Object.keys(nextProps.genderData).length > 0){
        var storeIndex = indexArray['gender']!==undefined?indexArray['gender']:this.state.currentIndex;
          optionAction[storeIndex] = nextProps.genderData.data
       }
       if(this.props.visiblyRoleData !== nextProps.visiblyRoleData && Object.keys(nextProps.visiblyRoleData).length > 0){
        var storeIndex = indexArray['visibly_role']!==undefined?indexArray['visibly_role']:this.state.currentIndex;
          nextProps.visiblyRoleData.map((visiblyRoleVar,index)=>{
              optionArrayDrop.push({value:visiblyRoleVar.role_id, label:visiblyRoleVar.role_name})
          },optionAction[storeIndex] = optionArrayDrop)
       }
       /*render predefined selected segment*/
      
       if(nextProps.particularSegment !== this.props.particularSegment && Object.keys(nextProps.particularSegment).length > 0 ){
        this.props.change('segmentName',nextProps.particularSegment.segment[0].segment_name);  
        if(nextProps.particularSegment.segment[0].segment_name!==''){
            this.setState({
              validateSegment:true
            })
        }

          let preDynamicField = ['field']
          let preNoOfField = 0
          nextProps.particularSegment.segment[0].filter.map((preSelectedSegment,index)=>{
            var permissionData ='';
            var currtab="SegmentForm";
            if(nextProps.particularSegment.segment[0].is_quick_segment==1){
              currtab='quickSegment'
              var filterData = nextProps.particularSegment.segment[0].filter
              if(filterData.length>0){
                var userdata = filterData[0].user.name;
                var departmentData = filterData[1].department.name;
                userdata.length>0?
                userdata.map((str,i)=>{
                  if(str!==''){
                    str = str.replace(/ /g,'');
                    permissionData=permissionData.concat(`@${str} `);
                  }
                }):''
                departmentData.length>0?
                departmentData.map((str,i)=>{
                  if(str!==''){
                    str = str.replace(/ /g,'');
                    permissionData=permissionData.concat(`@${str} `);
                  }
                }):''
              }
            }else{
            let key = Object.keys(preSelectedSegment)[0]
            handleFormVal[index] = []
            if(index > 0){
              preDynamicField.push('field')
            }
            preNoOfField++
            /*handleFormVal[index]['segment'] = key
            handleFormVal[index]['operation'] = preSelectedSegment[key]['operation']
            handleFormVal[index]['value'] = preSelectedSegment[key]['value']
            handleFormVal[index]['showInHeatmap'] = preSelectedSegment[key]['selected']*/
            let MultiArray = []
            if(preSelectedSegment[key]['operation'] == 'IB'){
              this.handleOperationChangeBox(index,{value:preSelectedSegment[key]['operation'], label: 'Is inbetween'});
            }else if(preSelectedSegment[key]['operation'] == 'lt'){
              this.handleOperationChangeBox(index,{value:preSelectedSegment[key]['operation'], label: 'Is less than'});
            }else if(preSelectedSegment[key]['operation'] == 'gt'){
              this.handleOperationChangeBox(index,{value:preSelectedSegment[key]['operation'], label: 'Is more than'});
            }
            //select checkbox auto
            if(preSelectedSegment[key]['selected'] == true){
              this.saveSegmentCheckboxClick(index,'direct');  
            }
            // to set the key of particular segment to place data
            indexArray[key]=index;

            if(preSelectedSegment[key]['operation'] !== 'IB'){
              for(let j=0; j<preSelectedSegment[key]['value'].length; j++){
                let labelHold = typeof preSelectedSegment[key]['name'] !== 'undefined' ? utils.capitalizeFirstLetter(preSelectedSegment[key]['name'][j]) : preSelectedSegment[key]['value'][j]
                MultiArray.push({value:preSelectedSegment[key]['value'][j], label: labelHold})  
              }  
              this.handleChange(index,{value:key, label: utils.capitalizeFirstLetter(key)},this.handleSubChange.bind(this,index,MultiArray),true)
              
            }else{
               let fromToVar = {from: preSelectedSegment[key]['value'][0], to: preSelectedSegment[key]['value'][1]}
               MultiArray.push({value:fromToVar, label: fromToVar})  
               this.handleChange(index,{value:key, label: utils.capitalizeFirstLetter(key)},false,true)
               handleFormVal[index]['value']['from'] = preSelectedSegment[key]['value'][0]
               handleFormVal[index]['value']['to'] = preSelectedSegment[key]['value'][1]
            }  
          }  
            //this.setState({editId: nextProps.particularSegment.segment[0].id,selectedStatus : nextProps.particularSegment.segment[0].status,currSegmentTab:currtab,permissionData:permissionData,currSegmentName:nextProps.particularSegment.segment[0].segment_name},()=>{this.handleInitialize()})
            this.setState({
              editId: nextProps.particularSegment.segment[0].id,
              selectedStatus : nextProps.particularSegment.segment[0].status,
              currSegmentTab:currtab,
              permissionData:permissionData,
              currSegmentName:nextProps.particularSegment.segment[0].segment_name
            },()=>{
              this.handleInitialize()
            })
          })

          this.setState({dynamicField:preDynamicField,numberOfField:preNoOfField,lastIndex:preNoOfField-1})
       }
    }

    componentDidMount (){
        handleFormVal = []
        handleFormVal[0] = []
        handleFormVal[0]['segment'] = ''
        handleFormVal[0]['operation'] = 'equal'
        handleFormVal[0]['value'] = '' 
        handleFormVal[0]['showInHeatmap'] = 0
        indexArray={};
        const {optionMainConst} =  this.props;
        optionMain = [...optionMainConst.map(option=>{return {...option}})]
        selectedMainOptionArray = []
        this.setState({
          selectedStatus : 'All'
        })
        // added for initializing check box for field
        this.handleInitialize()
    }

    handleInitialize () {
        let initData = {}
        initData['SegmentName'] = this.state.currSegmentName;
        initData['noOfSegment'] = this.state.numberOfField
        initData['currTab']=this.state.currSegmentTab;
        initData['PreviewSegment']=this.state.PreviewSegment;
        if(this.state.editId !== null){
          initData['edit_identity'] = this.state.editId
          if(this.state.currSegmentTab=='quickSegment'){
          initData["userPermissions"]= this.state.permissionData!==''&& this.state.permissionData!==undefined
          ? utils.replacePermissionTags(
            this.state.permissionData ,
              this.props.userTags,
              [],true
            )
          : ''
          }
          
        }
        initData['selectedStatus']=this.state.selectedStatus;
        if(this.state.currSegmentTab!=='quickSegment'){
        if(this.state.selectedStatus=="None"){
          document.getElementById("matchStatus").value= "None";
        }else{
          document.getElementById("matchStatus").value = "All";
        }
      }
        let totalField = handleFormVal.length;
          handleFormVal.map((handleFormVar,index)=>{

            initData['keyName'+index] = handleFormVar.segment
            initData['operateName'+index] = handleFormVar.operation
            initData['valueName'+index] = handleFormVar.value
            initData['saveAsSegment'+index] = 0;
            if(handleFormVar.segment == 'age'){
                if(handleFormVar.operation == 'IB'){
                    initData['ageFrom'+index] = handleFormVar.value.from
                    initData['ageTo'+index] = handleFormVar.value.to
                }else{
                    initData['age'+index] = handleFormVar.value  
                }
            }
            if(handleFormVar.segment == 'start_date'){
                if(handleFormVar.operation == 'IB'){
                    initData['start_date_from'+index] = handleFormVar.value.from
                    initData['start_date_to'+index] = handleFormVar.value.to
                    initData['valueName'+index] = [{'from':Date.now(),'to':Date.now()}]
                    if(typeof handleFormVar.value.from !== 'undefined'){
                      initData['valueName'+index][0]['from'] = handleFormVar.value.from
                    }
                    if(typeof handleFormVar.value.to !== 'undefined'){
                      initData['valueName'+index][0]['to'] = handleFormVar.value.to
                    }
                }else{
                    initData['start_date'+index] = handleFormVar.value
                    initData['valueName'+index] = handleFormVar.value !== '' && handleFormVar.value.length !== 0 ? handleFormVar.value : Date.now()
                }
                
            }
          })
        this.props.initialize(initData)
    }


    handleChange(index,selectedOption,callback,calledFromProps = false){
      optionAction[index] = []
      //if no callback function passed
      if(selectedOption.value == 'age' || selectedOption.value == 'start_date'){

        this.setState({['selectedMainOption'+index]:selectedOption, ['showOperation'+index]: selectedOption.value, currentIndex: index}, 
                      () => {
                              this.props.callFetchFieldData(selectedOption.value,this.state.selectedArray)
                              typeof callback === "function" ? callback() : ''
                            }
                    );  
        if(calledFromProps == false){
          handleFormVal[index]['value'] = []
          handleFormVal[index]['operation'] = 'gt'
        }
      }else{

        this.setState({['selectedMainOption'+index]:selectedOption, ['showOperation'+index]: 'equal', currentIndex: index, ['selectedSubOption'+index]:null}, 
                      () => {
                              this.props.callFetchFieldData(selectedOption.value,this.state.selectedArray);
                              typeof callback === "function" ? callback() : ''
                            }
                    );
       if(calledFromProps == false){ 
          handleFormVal[index]['value'] = []
       }
       handleFormVal[index]['operation'] = 'equal'
      }

      handleFormVal[index]['segment'] = selectedOption.value
      selectedMainOptionArray[index] = selectedOption.value
      this.removeAddOptionMainBox();
      this.checkParents(selectedOption.value);
      this.handleInitialize()
      
    }
    checkParents(segmentName){
      switch(segmentName){
        case "country":
          let checkArrayCountry = ['region']
          handleFormVal.map((selectedArray,index)=>{
            checkArrayCountry.map((checkArrayInit,j)=>{
                if(selectedArray.segment !== checkArrayInit){

                    optionMain = optionMain.map((renderOption,j)=>{

                      if(renderOption.value == checkArrayInit){
                           return {...renderOption, isDisabled:true}
                      }else{
                        return {...renderOption}
                      }
                    })
                }
            })
          })
        break;
        case "office":
          let checkArrayOffice = ['region','country']
          handleFormVal.map((selectedArray,index)=>{
            checkArrayOffice.map((checkArrayInit,j)=>{
                if(selectedArray.segment !== checkArrayInit){

                  optionMain = optionMain.map((renderOption,j)=>{

                    if(renderOption.value == checkArrayInit){
                         return {...renderOption, isDisabled:true}
                    }else{
                      return {...renderOption}
                    }
                  })
              }
            })
          })
        break;
        case "department":
          let checkArrayDepartment = ['region','country','office']
          handleFormVal.map((selectedArray,index)=>{
            checkArrayDepartment.map((checkArrayInit,j)=>{
                if(selectedArray.segment !== checkArrayInit){

                  optionMain = optionMain.map((renderOption,j)=>{

                    if(renderOption.value == checkArrayInit){
                         return {...renderOption, isDisabled:true}
                    }else{
                      return {...renderOption}
                    }
                  })
              }
            })
          })
        break;
        case "business":
          let checkArrayBusiness = ['region','country','office','department']
          handleFormVal.map((selectedArray,index)=>{
            checkArrayBusiness.map((checkArrayInit,j)=>{
                if(selectedArray.segment !== checkArrayInit){

                  optionMain = optionMain.map((renderOption,j)=>{

                    if(renderOption.value == checkArrayInit){
                         return {...renderOption, isDisabled:true}
                    }else{
                      return {...renderOption}
                    }
                  })
              }
            })
          })
        case "company_role":
          let checkArrayRole = ['region','country','office','department','business']
          handleFormVal.map((selectedArray,index)=>{
            checkArrayRole.map((checkArrayInit,j)=>{
                if(selectedArray.segment !== checkArrayInit){

                  optionMain = optionMain.map((renderOption,j)=>{

                    if(renderOption.value == checkArrayInit){
                         return {...renderOption, isDisabled:true}
                    }else{
                      return {...renderOption}
                    }
                  })
              }
            })
          })
        break;
      }
      
    }
    removeAddOptionMainBox(){
      let newMainArray = [];
      var optionMain1 = optionMain.filter(function (el) {
        return selectedMainOptionArray.indexOf(el.value) === -1
      });
      optionMain = optionMain1;
     
    }
    handleSubChange(index,selectedOption){

      let allSelectedArray = this.state.selectedArray;
      let newArray= [];
      let inArray = false
      const checkArray = ['region','country','office','department','business','company_role']
      if(checkArray.indexOf(this.state['selectedMainOption'+index].value) !== -1){
          allSelectedArray.map((allData,i)=>{

            if(allData.selected == this.state['selectedMainOption'+index].value){
               allSelectedArray[i]['value'] = selectedOption
               inArray = true
            }
          })
          if(!inArray){
               allSelectedArray.push({selected: this.state['selectedMainOption'+index].value, value: selectedOption})
          }
          this.setState({selectedArray: allSelectedArray, ['selectedSubOption'+index]: selectedOption})
      }else{
          this.setState({['selectedSubOption'+index]: selectedOption})
      }
      let newSelectedValue = []
      selectedOption.map((selectOptionLoop)=>{
          newSelectedValue.push(selectOptionLoop.value)
      })
      handleFormVal[index]['value'] = newSelectedValue
      this.handleInitialize();
    }
    addMoreSegment(lastIndex){
      this.setState({
        currSegmentTab:'SegmentForm'
      })
      let newFieldCounter = this.state.dynamicField
      newFieldCounter.push('field')
      let numberOfField = this.state.numberOfField + 1
      let currentIndex = this.state.lastIndex + 1
      handleFormVal[currentIndex] = [];
      handleFormVal[currentIndex]['segment'] = ''
      handleFormVal[currentIndex]['operation'] = 'equal'
      handleFormVal[currentIndex]['value'] = ''
      handleFormVal[currentIndex]['showInHeatmap'] = 0
      this.setState({ numberOfField: numberOfField, dynamicField: newFieldCounter, lastIndex: currentIndex},()=>{this.handleInitialize()})
      //this.handleInitialize()
    }

   handleSubmitButton(e){
     if(this.state.currSegmentTab=='quickSegment'){
        if(document.querySelector(".viewper").style.display=="inline"){
          e.preventDefault();
        }
      }
    this.setState({removedIndex: []})
   }  

   handleOperationChangeBox(index,selectedOption){
      handleFormVal[index]['operation'] = selectedOption.value
      if(selectedOption.value == 'IB'){
        this.setState({['twoBox'+index]: true})
        handleFormVal[index]['value'] = []
      }else{
        handleFormVal[index]['value'] = ''
        this.setState({['twoBox'+index]: false})
      }
      this.setState({['selectedOperationOption'+index]: selectedOption})
      this.handleInitialize()
      this.forceUpdate()
   }
   handleSubChangeText(index,me){
       handleFormVal[index]['value'] = me.target.value
       
       this.setState({
        selectedSubOption0 : me.target.value
       })

       this.state.selectedSubOption0
       this.handleInitialize()
       this.forceUpdate()
   }

   handleSubChangeFromToText(index,type,me){
       handleFormVal[index]['value'][type] = typeof me.target.value !== 'undefined' ? me.target.value : me;
       this.handleInitialize()
       this.forceUpdate()
   }

   removeElement(index){
    //let numberOfField = this.state.numberOfField - 1
    let removeIndexVar = this.state.removedIndex
    removeIndexVar.push(index)
    let numberOfField = this.state.numberOfField - 1
    let currentIndex = this.state.lastIndex - 1

    this.setState({ removedIndex: removeIndexVar, numberOfField: numberOfField, lastIndex: currentIndex})
    //document.getElementById("aging"+index).disabled = true;
    // document.getElementById("newSegmentMainBox"+index).remove();
    this.props.change("keyName"+index,'')
    this.props.change("operateName"+index,'')
    this.props.change("valueName"+index,'')
  
    var tempValueArr = [];
    handleFormVal.map((value,i)=>{
      if(i!==index){
        tempValueArr.push(value);
      }
    }
    )
    for(var shiftIndex =index;shiftIndex<=lastIndex;shiftIndex++){
      if(shiftIndex!==lastIndex){
        this.setState({
          ['selectedMainOption'+shiftIndex]:this.state['selectedMainOption'+(shiftIndex+1)],
          ['selectedSubOption'+shiftIndex]:this.state['selectedSubOption'+(shiftIndex+1)],
          ['selectedOperationOption'+shiftIndex]:this.state['selectedOperationOption'+(shiftIndex+1)],
          ['showOperation'+shiftIndex]:this.state['showOperation'+(shiftIndex+1)],
          ['twoBox'+shiftIndex]:this.state['twoBox'+(shiftIndex+1)]
        })
      }else{
        this.setState({
          ['selectedMainOption'+shiftIndex]:undefined,
          ['selectedSubOption'+shiftIndex]:undefined,
          ['showOperation'+shiftIndex]:undefined,
          ['twoBox'+shiftIndex]:undefined
        })
      }
    }

    let newFieldCounter = this.state.dynamicField
      newFieldCounter.pop('field');
      handleFormVal = tempValueArr;
      lastIndex = lastIndex - 1
      this.setState({
        dynamicField : newFieldCounter,
        numberOfField : this.state.numberOfField-1 
      },()=>{
        this.handleInitialize()
      })
   }  
   updateDateValueFromTo(type = null,value,index){
      handleFormVal[index]['value'][type] = value 
      this.handleInitialize()
   }
   updateDateValue(value,index){
      handleFormVal[index]['value'] = value  
      this.handleInitialize()
   }
   saveSegmentCheckboxClick(index,me){
    if(me == 'direct'){
        handleFormVal[index]['showInHeatmap'] = 1
    }else{
      if(me.target.value == 0){
        handleFormVal[index]['showInHeatmap'] = 1
      }else{
        handleFormVal[index]['showInHeatmap'] = 0
      }  
    }
    this.handleInitialize();
    
   }

   refreshSegment(){
     if(this.state.currSegmentTab=='quickSegment'){
       this.props.change('userPermissions','');
       document.getElementById('userPermissions')?
       document.getElementById('userPermissions').value='':'';
     }
     selectedMainOptionArray = [];
     optionAction=[];
     indexArray={};
     var optionMain1 = this.props.optionMainConst.filter(function (el) {
      return selectedMainOptionArray.indexOf(el.value) === -1
    });
    optionMain = optionMain1;
     handleFormVal.map((allVal,index)=>{
          this.setState({
            ['selectedMainOption'+index]:null,
            ['selectedSubOption'+index]:null,
            ['selectedOperationOption'+index]:null,
            ['showOperation'+index]:'equal',
            ['twoBox'+index]:null,
            ['keyName'+index]:null,
            ['operateName'+index]:null,
            ['valueName'+index]:null,
            ['saveAsSegment'+index]:0
          },()=>{
            handleFormVal = []
            handleFormVal[0] = []
            handleFormVal[0]['segment'] = ''
            handleFormVal[0]['operation'] = 'equal'
            handleFormVal[0]['value'] = '' 
            handleFormVal[0]['showInHeatmap'] = 0
          })
          this.props.change('keyName'+index ,'');
          this.props.change('operateName'+index ,'');
          this.props.change('saveAsSegment'+index ,0);
          this.props.change('valueName'+index ,'');
          this.props.change('noOfSegment' ,1);
    })
     this.setState({dynamicField:['field'],selectedArray:[],removedIndex:[],currentIndex:null,numberOfField:1,lastIndex:0})
     lastIndex=0
   }
   handleSegmentName(e){
    if(e.target.value.trim()==''){
      this.setState({
        validateSegment:false,
        currSegmentName : e.target.value
      })
    }else{
      this.setState({
        validateSegment:true,
        currSegmentName : e.target.value
      })
    }
   }

    
   renderNewSegmentsFormNew(fetchedProps){
        const { handleSubmit, pristine, submitting } = fetchedProps
        var isDisabled =  this.state.selectedMainOption0 !==null && 
        this.state.selectedMainOption0 !== undefined && 
        this.state.selectedSubOption0!==null && 
        this.state.selectedSubOption0 !== undefined && this.state.selectedSubOption0.length!==0? false : true;
        return (  
                <div className="MoreSegmentsWrapper ">
                  <div className="addSegementSection">
                      <div className={`selectAllSegmentCheckboxWrapper ${this.state.currSegmentTab !== 'quickSegment'?'selectAllSegmentBox-quick-user':''}`}>
                        {this.state.currSegmentTab !=='quickSegment'? 
                          <div className="refreshsegmentButton clearfix" >
                              <a title="Refresh" className="refreshSegmentBtn" onClick={this.refreshSegment.bind(this)}><i class="material-icons">refresh</i> Reset</a>
                          </div>
                        :''}
                        <div>

                        </div>
                      </div>
                        <div className="addSegementSectionInner">
                          <div>
                          <form name='addSegmentForm' onSubmit={this.formSubmit.bind(this)}>
                            {this.state.AddSegmentFlag  == true ?
                                      <div className="segmentNameWrapper">
                                        <label>Segment Name</label>
                                          <Field
                                              placeholder ="Segment Name"
                                              name = "SegmentName"
                                              component={renderField}
                                              type="text"
                                              onChange={this.handleSegmentName.bind(this)}
                                              validate={[required]}
                                          />
                                          <Field
                                            name={'PreviewSegment'}
                                            component={renderField}
                                            type='hidden'
                                          />
                                      </div>
                               :''}
                              {this.state.currSegmentTab =='quickSegment'? 
                               <div class='quick-segment-wrapper user-tag-editor user-permission'>
                               <label for='Post Content'>Add User</label>
                               <div class="form-row-wrapper user-tag-editor">
                               <Field
                                placeholder={ 'Add @user or @department to give access to this campaign'}
                                name='userPermissions'
                                component={MentionTextArea}
                                userTags={this.props.userTags}
                                validate={[required]}
                                type='text'
                                id='userPermissions'
                                onChange={this.valiDate.bind(this)}
                                onFocus={this.handleMentionFocus.bind(this)}
                              />
                              
                              <div className="viewper">* Please add only user tag or department</div>
                              </div>
                              </div>
                              :
                              <div>
                              <Field
                                  name={'noOfSegment'}
                                  component={renderField}
                                  type='hidden'
                              />
                              <Field
                                  name={'edit_identity'}
                                  component={renderField}
                                  type='hidden'
                              />
                               <Field
                                  name={'currTab'}
                                  component={renderField}
                                  type='hidden'
                              />
                              <Field
                                  name={'selectedStatus'}
                                  component={renderField}
                                  type='hidden'
                              />
                              
                              <table className="table responsive-table">
                              {this.state.dynamicField.map((dynamicField,index)=>{
                                lastIndex = index
                              let optionState = this.state['selectedMainOption'+index] 
                              let valueState = this.state['selectedSubOption'+index] 
                              let operationState = this.state['selectedOperationOption'+index]
                              return(

                                  
                                    <tr id={'newSegmentMainBox'+index} key={index} className={this.state['showOperation'+index] !== 'equal' &&  typeof this.state['showOperation'+index] !== 'undefined' ? "newSegmentColumnWrapper cleafix numeric-segment" : "newSegmentColumnWrapper cleafix"} >
                                    {/* <td className="newSegmentColumn" width='5%'>
                                      <div className="campaignSelectSegmentCheckbox">
                                        <div className='form-row checkbox'>

                                          <Field
                                            name={'saveAsSegment'+index}
                                            component={renderField}
                                            type='checkbox'
                                            onChange={this.saveSegmentCheckboxClick.bind(this,index)}
                                            disabled={this.state.dynamicField.length == (index+1) ? true : false}
                                          />
                                          
                                        </div>
                                      </div>
                                      </td> */}
                                      <td className="newSegmentColumn firstSegmentDropdown" width='30%'>
                                        <div className="newSegmentColumnInner">
                                          <div className="newSegmentText">If</div>
                                            <div class="select-wrapper multi-select-wrapper">
                                              {this.state.reactSelect !== null ?
                                              <this.state.reactSelect.default
                                                value={optionState}
                                                onChange={this.handleChange.bind(this,index)}
                                                options={optionMain}
                                              /> :''}
                                              <Field
                                                name={'keyName'+index}
                                                component={renderField}
                                                type='hidden'
                                              />
                                            </div>
                                        </div>
                                      </td>
                                      <td className="newSegmentColumn" width='30%'>
                                        <div className="newSegmentText isEqualToText">is equal to</div>
                                        <div className="numericDropdown">
                                          <div class="select-wrapper multi-select-wrapper">
                                          {this.state.reactSelect !== null ?
                                              <this.state.reactSelect.default
                                              value={operationState}
                                              options={optionOperation}
                                              onChange={this.handleOperationChangeBox.bind(this,index)}
                                              defaultValue={optionOperationDefault}
                                            />:''}
                                            <Field
                                                name={'operateName'+index}
                                                component={renderField}
                                                type='hidden'
                                            />
                                          </div>
                                        </div>
                                      </td>
                                      {this.state['showOperation'+index] !== 'equal' &&  typeof this.state['showOperation'+index] !== 'undefined'? 
                                        <td className="newSegmentColumn valueColumn" width='30%'>
                                            <div>
                                              {this.state['showOperation'+index] == 'age' ?
                                                <div>
                                                  {this.state['twoBox'+index] ?
                                                    <div className="newSegmentColumnInner two-fields">
                                                      <Field
                                                        name={'ageFrom'+index}
                                                        placeholder='From'
                                                        component={renderField}
                                                        type='text'
                                                        onChange={this.handleSubChangeFromToText.bind(this,index,'from')}
                                                      />
                                                      <Field
                                                        name={'ageTo'+index}
                                                        placeholder='To'
                                                        component={renderField}
                                                        type='text'
                                                        onChange={this.handleSubChangeFromToText.bind(this,index,'to')}
                                                      />
                                                      <Field
                                                          name={'valueName'+index}
                                                          component={renderField}
                                                          type='hidden'
                                                      />
                                                    </div>  
                                                  :
                                                    <div>
                                                      <Field
                                                        name={'age'+index}
                                                        placeholder='Age'
                                                        component={renderField}
                                                        type='text'
                                                        type={'number'}
                                                        onChange={this.handleSubChangeText.bind(this,index)}
                                                      />
                                                      <Field
                                                          name={'valueName'+index}
                                                          component={renderField}
                                                          type='hidden'
                                                      /> 
                                                    </div> 
                                                  }
                                                </div> 
                                              :
                                                <div>
                                                  {this.state['twoBox'+index] ?
                                                      <div className="newSegmentColumnInner two-fields">
                                                        <Field
                                                          name={'start_date_from'+index}
                                                          placeholder='From'
                                                          component={props => {
                                                            return (
                                                              <ReactDatePicker
                                                                {...props}
                                                                reactDatePicker={this.state.ReactDatePicker}
                                                                moment={this.state.Moment}
                                                                dateFormat="DD/MM/YYYY"
                                                                indexVal={index}
                                                                updateDateValue={this.updateDateValueFromTo.bind(this,'from')}
                                                              />
                                                            )
                                                          }}
                                                        />
                                                        <Field
                                                          name={'start_date_to'+index}
                                                          placeholder='To'
                                                          component={props => {
                                                            return (
                                                              <ReactDatePicker
                                                                {...props}
                                                                reactDatePicker={this.state.ReactDatePicker}
                                                                moment={this.state.Moment}
                                                                dateFormat="DD/MM/YYYY"
                                                                indexVal={index}
                                                                updateDateValue={this.updateDateValueFromTo.bind(this,'to')}
                                                              />
                                                            )
                                                          }}
                                                        />
                                                     </div> 
                                                  :
                                                      <Field
                                                        name={'start_date'+index}
                                                        placeholder='Start date'
                                                        component={props => {
                                                          return (
                                                            <ReactDatePicker
                                                              {...props}
                                                              reactDatePicker={this.state.ReactDatePicker}
                                                              moment={this.state.Moment}
                                                              dateFormat="DD/MM/YYYY"
                                                              indexVal={index}
                                                              updateDateValue={this.updateDateValue.bind(this)}
                                                            />
                                                          )
                                                        }}
                                                      />
                                                  }
                                                  <Field
                                                    name={'valueName'+index}
                                                    className='hid'
                                                    component={renderField}
                                                    type='hidden'
                                                  />
                                                </div>
                                              }
                                            </div>
                                        </td>
                                      :  
                                        <td className="newSegmentColumn valueColumn" width='30%'>
                                          <div className="newSegmentColumnInner">
                                            <div class="select-wrapper multi-select-wrapper">
                                            {this.state.reactSelect !== null ?
                                              <this.state.reactSelect.default
                                                value={valueState}
                                                options={optionAction[index]}
                                                isMulti={true}
                                                onChange={this.handleSubChange.bind(this,index)}
                                              /> :''}
                                              <Field
                                                name={'valueName'+index}
                                                component={renderField}
                                                type='hidden'
                                              />
                                            </div>
                                          </div>
                                        </td>
                                      }
                                      
                                        <td className="action-col" width='5%'>
                                        {
                                          
                                        // this.state.dynamicField.length !== (index+1)
                                        this.state.dynamicField.length !==1
                                         ? 
                                          <div className="addRemoveSegmentData clearfix" onClick={this.removeElement.bind(this,index)}>
                                            <a title="Remove" className="removeSegmentBtn"><i className="material-icons">clear</i></a>
                                          </div> 
                                          :'' 
                                        }   
                                          </td>
                                        
                                    </tr>  
                                         
                              )})}
                              </table>
                              </div>
                            }
                              <div className="segmentSaveButtonWrapper clearfix">
                              <div className="addMoreSegmentsWrapper">

                                {this.state.currSegmentTab!=='quickSegment'?
                                <button className="addSegmentButton" onClick={this.addMoreSegment.bind(this,lastIndex)} type="button"><span className="addMoreButtonIcon"><i className="material-icons">add</i></span> Add segment</button>:''}
                                {this.props.currView!=='segmentations' && this.state.currSegmentTab!=='quickSegment'?
                                <button className="addSegmentButton" onClick={this.showSegmentPreviewPopup.bind(this)} type="button">
                                  <span className="addMoreButtonIcon"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                      <path fill="none" d="M0 0h24v24H0V0z"/><path d="M9 12c1.93 0 3.5-1.57 3.5-3.5S10.93 5 9 5 5.5 6.57 5.5 8.5 7.07 12 9 12zm0-5c.83 0 1.5.67 1.5 1.5S9.83 10 9 10s-1.5-.67-1.5-1.5S8.17 7 9 7zm.05 10H4.77c.99-.5 2.7-1 4.23-1 .11 0 .23.01.34.01.34-.73.93-1.33 1.64-1.81-.73-.13-1.42-.2-1.98-.2-2.34 0-7 1.17-7 3.5V19h7v-1.5c0-.17.02-.34.05-.5zm7.45-2.5c-1.84 0-5.5 1.01-5.5 3V19h11v-1.5c0-1.99-3.66-3-5.5-3zm1.21-1.82c.76-.43 1.29-1.24 1.29-2.18C19 9.12 17.88 8 16.5 8S14 9.12 14 10.5c0 .94.53 1.75 1.29 2.18.36.2.77.32 1.21.32s.85-.12 1.21-.32z"/>
                                      </svg>
                                  </span>
                                Preview Collaborators</button>:''}

                                </div>

                                <div class="save-cancel-btn">
                                {
                                  this.state.currSegmentTab=='SegmentForm'?
                                  <button className="btn save-btn theme-btn" 
                                  type="submit"  
                                  disabled={isDisabled} 
                                  onClick={this.handleSubmitButton.bind(this)}
                                  >Save</button>
                                  :
                                  <button className="btn save-btn theme-btn" 
                                    type="submit"   
                                    onClick={this.handleSubmitButton.bind(this)}
                                    >Save</button>
                                }
                               
                                <button  className="btn cancel-btn default-btn" type="button" onClick={this.props.closeAddSegmentPopup.bind(this)}>Cancel</button>
                                </div>
                               
                              </div> 
                            </form>              
                          </div>      
                        </div>
                  </div>    
                </div>            

        )
   }
   selectedMatchStatus(e){
    this.props.change('selectedStatus',e.target.value)
      this.setState({
        selectedStatus : e.target.value
      })
   }

   HandleQuickSegment(tab){

    // if(tab=='quickSegment'){
    //   this.props.change('userPermissions' ,'')
    // }else if(tab=='SegmentForm'){
    //   this.refreshSegment();
    // }
    if(this.state.editId!==null && this.state.editId!==undefined){
      if(tab=='quickSegment'){
        if(this.props.particularSegment.segment[0].is_quick_segment!==1){
          notie.alert("error","Segment type cannot be changed.",3);
        }else{
          this.setState({
            currSegmentTab : tab,
          })
          this.props.change('currTab',tab);
        }
      }else if(tab=='SegmentForm'){
        if(this.props.particularSegment.segment[0].is_quick_segment!==0){
          notie.alert("error","Segment type cannot be changed.",3);
        }else{
          this.setState({
            currSegmentTab : tab,
          })
          this.props.change('currTab',tab);
        }
      }
    }else{
      this.setState({
        currSegmentTab : tab,
        AddSegmentFlag: false
      })
      if(tab =='SegmentForm'){
        this.setState({
          AddSegmentFlag:true
        })
      }else{
        this.setState({
          AddSegmentFlag:false
        })
      }
      this.props.change('currTab',tab);
    }
   }
   formSubmit(values,fromPreview=null){
    var me = this;
     if(fromPreview==true){
       me.props.change('PreviewSegment',true);
       me.setState({
         previewSegment:true
       },()=>{
         me.handleInitialize();
        me.props.handleSubmit(values);
       })
     }else{
      me.props.handleSubmit(values);
     }
    
  }
  valiDate(e) 
  {
    var temp='',count=0,count2=0,flag=0;
    const len=Object.keys(e).length-1;

    for(var i=0;i<Object.keys(e).length-1;i++){
      //for loop is for checking the whole string every time it is updated
      temp=e[i];
      if(e[i]=="@"){count+=1}  //count of at the rate
      if((e[i]==")"&&e[i+1]==" "&& e[i+2]=="@" && flag!==2)){ //count of )" "@
        count2+=1;flag=1;
      }
      if((e[i]==")"&&e[i+1]==" "&& e[i+2]!=="@" && e[i+2]!==undefined) || (e[i]==")"&& e[i+1]!=="@" && e[i+1]!==undefined && e[i+1]!==" ")){
        flag=2;
      }
    }
    var first=e[0];
    var last=temp;
    
    temp=e[0]
  if(temp !== undefined){   
    if(first=="@" && last==")" && e[i]==null){
      document.querySelector(".viewper").style.display="none"; 
    }
    else if(first=="@" && last==")" && e[i]==" "){
      if(flag==1){
        if(count==count2+1){
          document.querySelector(".viewper").style.display="none";
        }
        else{
          document.querySelector(".viewper").style.display="inline";
        }
      }
      else{
        document.querySelector(".viewper").style.display="none";
      }
    }
    else{
      document.querySelector(".viewper").style.display="inline";
    }
    if(flag==2 && (e[0]!=='@'|| e[0]!==' ')){
      document.querySelector(".viewper").style.display="inline";
    }
  }
  else
  {
    document.querySelector(".viewper").style.display="none";
  }
    
  }
  handleMentionFocus(e){  
    var me = this
    if(e.target.value == ''){
     me.props.change('userPermissions' ,'@')
    }
  }

  checkPermission(e){
    if(document.querySelector(".viewper").style.display=="inline"){
      e.preventDefault();
    }
  }
  showSegmentPreviewPopup(e){
    this.formSubmit(e,true);
  }

    render(){
        return(
            <CenterPopupWrapper>
                <header class="heading">
                      <h3>{this.props.currView=='collaborators'?'Collaborators':'Segments'}</h3>
                      <button id="close-popup" className="btn-default" onClick={this.props.closeAddSegmentPopup.bind(this)}><i className="material-icons" >clear</i></button>
                 </header>
                <div className="campaignSegmentWrapper">
                <div className="createSegmentSection">
            <div  className = {` SegmentsSection ${this.state.AddSegmentFlag == true ? 'addSegementsFlag ' :''}`}>
            <div className="newSegmentsTitle">New segments  </div>
            <div class="listMenu">
              <nav class="nav-side">
                <ul class="parent clearfix">
                  <li><a href="javascript:void(0);" onClick={this.HandleQuickSegment.bind(this,'quickSegment')} class={this.state.currSegmentTab=='quickSegment'?'active':''}><span class="text">Add User</span></a></li>
                  <li><a href="javascript:void(0);" onClick={this.HandleQuickSegment.bind(this,'SegmentForm')} class={this.state.currSegmentTab=='quickSegment'?'':'active'}><span class="text">Add Segment</span></a></li>
                </ul>
              </nav>
            </div>
            {/* <div className='asbtnWrapper'>

              <button className='btn btn-default' onClick={this.HandleQuickSegment.bind(this,this.state.currSegmentTab)}><span className="addMoreButtonIcon"><i className="material-icons">add</i></span> {this.state.currSegmentTab=='quickSegment'?'Add Segment':'Add Quick segment'}</button>
            </div> */}
              {/* <div className='newSegmentsTitle'></div> */}
              {this.state.currSegmentTab !=='quickSegment'? 
                <div className="defaultSegmentField">
                    <div className="newSegmentText">User match</div>
                    <div className="defaultSegmentDropdown">
                        <div class="select-wrapper">
                            <select id ="matchStatus" onChange={this.selectedMatchStatus.bind(this)} >
                            <option value="All">All</option>
                            <option value="None">None</option>
                            </select>
                        </div>
                    </div>
                    <div className="newSegmentText">of the following conditions:</div>
                </div>:''}
                  

                {this.renderNewSegmentsFormNew(this.props)}
                
                
            </div>
           
             </div>
                {/*this.createNewSegment()*/}
                </div>
            </CenterPopupWrapper>    
          )
    }

    
}

function mapStateToProps (state, ownProps) {
  return {
  }
}

function mapDispatchToProps (dispatch) {
  return {
  }
}
let connection = connect(mapStateToProps, mapDispatchToProps)

var reduxConnectedComponent = connection(AddSegmentPopup)

export default reduxForm({
  form: 'addSegmentForm' // a unique identifier for this form
})(reduxConnectedComponent)