
import CampaignComments from './CampaignComments'
import * as campaignActions from '../../actions/campaigns/campaignsActions'
import { connect } from 'react-redux'
import { Field, reduxForm } from 'redux-form'
import { bindActionCreators } from 'redux'
import ChunkSelect from '../Chunks/ChunkReactSelect';
import ContentWorkSpace from './ContentWorkSpace'
import TagPopup from '../Feed/TagPopup'
import { renderField } from '../Helpers/ReduxForm/RenderField'
import CampaignTasks from './CampaignTasks'
import PreLoader from '../PreLoader';
import reactTooltip from '../Chunks/ChunkReactTooltip';
import ReactDatePicker from '../Helpers/ReduxForm/ReactDatePicker' // react datepicker field
import reactDatepickerChunk from '../Chunks/ChunkDatePicker' // datepicker chunk
import CampaignCreateTasks from './CampaignCreateTasks';
import SliderPopup from '../SliderPopup'
import CampaignTaskDetail from './CampaignTaskDetail'
import ReactDOM from 'react-dom';
import FetchuserDetail from '../FetchUsersDetail/FetchUsersDetail';
import CenterPopupWrapper from '../CenterPopupWrapper'
import PopupWrapper from '../PopupWrapper'
import notie from 'notie/dist/notie.js';
import SendMailPopup from './SendMailPopup';
const required = value => (value ? undefined : 'Required')
let options = [
  { value: 0, label: 'Requested' },
  { value: 1, label: 'Draft' },
  { value: 2, label: 'In Process' },
  { value: 3, label: 'Done' },
  { value: 4, label: 'In Review' },
  { value: 5, label: 'Sign Off' }
]
var storeAssigneeList=false
var search;
var getUserTagDataFlag=false
var limitCampUser=4;
var projectStatus = { value: 0, label: 'Requested' }
var selectedUserData
var selectedDepartmentData
class ContentProject extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      currContentTab: 'workspace',
      EditMode: false,
      editNotes: false,
      commentUserList: [],
      taskDetail: false,
      showAssignPopup : false,
      callContentWorkSpace:false,
      workSpaceHtmlContent:'',
      contentUserNames:[],
      filter_state:[],
      tooltip: null,
      contentProjectAssigneeList:[],
      contentProjectAssigneeIdList:[],
      ReactDatePicker: null,
      datetoEdit:null,
      createTasks:false,
      callSignOffParent:false,
      isTemplateSelected:false,
      openTemplatePreviewPopup:false,
      previewLink:{},
      addUserflag : false,
      projectId : '',
      shippingDateFlag:false,
      reactSelect :null
    }
    this.changeContentTab = this.changeContentTab.bind(this)
    this.handlTitleEdit = this.handlTitleEdit.bind(this)
    this.cancelTitleEdit = this.cancelTitleEdit.bind(this)
    this.saveContentTitle = this.saveContentTitle.bind(this)
  }
  componentWillMount(){
      reactTooltip().then(reactTooltip => {
         ChunkSelect().then(reactselect => {
          reactDatepickerChunk()
            .then(reactDatepicker => {
              this.setState({ ReactDatePicker: reactDatepicker,tooltip: reactTooltip,reactSelect: reactselect, })
            })
          })
    })
  }
  componentWillUnmount(){
      // remove html cached values from local storage  when editor 
      // page is closed or redirected to other page.
      localStorage.removeItem('gjs-styles');
      localStorage.removeItem('gjs-assets');
      localStorage.removeItem('gjs-components');
      localStorage.removeItem('gjs-html');
      localStorage.removeItem('gjs-css');
  }
  
  setProjectStatus (status) {
    switch (status) {
      case 0:
        projectStatus = {
          value: 0,
          label: 'Requested'
        }
        break
      case 1:
        projectStatus = {
          value: 1,
          label: 'Draft'
        }
        break
      case 2:
        projectStatus = {
          value: 2,
          label: 'In Process'
        }
        break
      case 3:
        projectStatus = {
          value: 3,
          label: 'Done'
        }
        break
      case 4:
        projectStatus = {
          value: 4,
          label: 'In Review'
        }
        break
      case 5:
        projectStatus = {
          value: 5,
          label: 'Sign Off'
        }
        break
    }
  }
  componentDidMount () {
    this.props.campaignActions.getContentProjectHtml(this.props.campaigns.currContentProject.project_identity);
    storeAssigneeList=false
    getUserTagDataFlag=false
    let { contentProjectData } = this.props
    this.setProjectStatus(contentProjectData.status)
    this.handleInitialize()
    this.props.campaignActions.fetchDefaultDraftTemplates(contentProjectData.type);
    this.props.campaignActions.fetchDraftFiles(contentProjectData.type);
    if(this.props.profile.identity!==contentProjectData.owner.identity && this.props.roleName!=="super-admin"){
      options =  options.filter(item =>{
       return item.value!==5

      } )
    }

    // to close popup on outside of click
    $('body').click(function(e) {
      if (!$(e.target).closest('.assign-popup').length){
          $(".assign-popup").hide();
      }
  });
  //to fetch the list of campaign assets
    var campId = this.props.campaign.identity;
    this.props.campaignActions.fetchCampaignAssets(campId);
  }
  changeContentTab = tab => {
    this.setState({
      currContentTab: tab
    })
  }
  handleInitialize (nextProps = null) {
    const initData = {
      notes:
        this.props.contentProjectData.notes !== null &&
        this.props.contentProjectData.notes !== undefined
          ? this.props.contentProjectData.notes
          : '',
          startDate: this.props.contentProjectData.start_date,
          endDate:  this.props.contentProjectData.end_date,
          shippingDate : this.props.contentProjectData.shipping_date
    }
    this.props.initialize(initData)
  }
  handlTitleEdit = title => {
    document.getElementById('contentProjectTitle').classList.remove('hide')
    document.getElementById('contentProjectTitle').value = title
    this.setState({
      EditMode: true
    })
  }
  saveContentTitle = () => {
    let campId = this.props.campaign.identity
    let contentProjectId = this.props.contentProjectData.project_identity
    let text = document.getElementById('contentProjectTitle').innerText
    if(text.length <= 50) {
      this.props.campaignActions.editContentProject(
        'title',
        text,
        campId,
        contentProjectId
      )
    }else{
      notie.alert('error', 'Content title should be less than or equal to 50 character.', 5)
    }
  }
  cancelTitleEdit = title => {
    document.getElementById('contentProjectTitle').classList.add('hide')
    document.getElementById('contentProjectTitle').value = title
    this.setState({
      EditMode: false
    })
  }
  componentWillReceiveProps (nextProps) {
     
    if (
      this.props.campaigns.editProjectLoading !==nextProps.campaigns.editProjectLoading &&
      nextProps.campaigns.editProjectLoading == false
    ) {
      // this.closeAddUserPopup()
      if(nextProps.campaigns.editValueType!==this.props.campaigns.editValueType && nextProps.campaigns.editValueType=='html' ){
        this.setState({
          callContentWorkSpace:true
        })
        // let campId = this.props.campaign.identity
        // let contentProjectId = this.props.contentProjectData.project_identity
        // this.props.campaignActions.editContentProject(
        //   'html_preview',
        //   this.state.workSpaceHtmlContent.html_preview,
        //   campId,
        //   contentProjectId
        // )
      }
      this.setState({
        EditMode: false,
        editNotes: false,
        datetoEdit :null,
        shippingDateFlag:false
      })

      document.getElementById('contentProjectTitle').classList.add('hide')
    }
    // if(this.props.campaign!==nextProps.campaign){
      if(getUserTagDataFlag== false){
        getUserTagDataFlag=true
      this.setState({
          contentUserNames:nextProps.campaignAllUserList.campaignAllUserListData,
          filter_state:nextProps.campaignAllUserList.campaignAllUserListData
      })
    }
    if(this.props.campaigns.currContentProject !== nextProps.campaigns.currContentProject && nextProps.campaigns.currContentProject !== null || storeAssigneeList==false){
      storeAssigneeList=true
      if(nextProps.campaigns.currContentProject.asssignee.length>0){
        var contentProjectAssigneeIdList=[];
        var arrayAssignee=nextProps.campaigns.currContentProject.asssignee
        var tagArray=[];
        for (let index = 0; index < arrayAssignee.length; index++) {
          const element = arrayAssignee[index].identity;
          contentProjectAssigneeIdList.push(element);
          tagArray.push({
            display: arrayAssignee[index].firstname + ' ' + arrayAssignee[index].lastname,
            id: arrayAssignee[index].identity,
            email: arrayAssignee[index].email,
            profile_image: arrayAssignee[index].profile_image,
            type: 'User'
          })
        }
        this.setState({
          commentUserList: tagArray,
          contentProjectAssigneeList:nextProps.campaigns.currContentProject.asssignee,
          contentProjectAssigneeIdList:contentProjectAssigneeIdList
        })
      }
    }
    if(nextProps.campaigns.addTaskLoading!==this.props.campaigns.addTaskLoading && nextProps.campaigns.addTaskLoading!==true){  
      if(this.props.currProjectId){
       this.props.campaignActions.editContentProject("status",2,this.props.campaign.identity,this.props.currProjectId)
      }
     this.closeCreateTaskPopup();
     if(this.state.editTask==true){
       this.setState({
         editTask:false
       })
     }
   }
   if(this.props.campaigns!==nextProps.campaigns && nextProps.campaigns!==undefined){
    if(nextProps.campaigns.currContentProject!==null){
        if(nextProps.campaigns.projectAction=="edit"){
          if(nextProps.campaigns.currContentProject.html!==null){
          this.setState({
            isTemplateSelected:true
          })}
        }
      }
    }
    if(this.props.campaigns.recipientData.recipientDataLoader !== nextProps.campaigns.recipientData.recipientDataLoader && nextProps.campaigns.recipientData.recipientDataLoader ==false){
      this.closeAddUserPopup()
    }

    //here i am not comparing the props as i am getting old props and new props same.
    // when user click on task count then to display task list of project
    if(nextProps.campaigns.displayTaskOfContent == true){
      this.changeContentTab('tasks')
      //to reset flag of displaying task
      this.props.campaignActions.displayTasksOfContent(false)
    }
  }
  selectedProjectStatus = e => {
    let campId = this.props.campaign.identity
    let contentProjectId = this.props.contentProjectData.project_identity;
    if(e.value==5){
      this.setState({
        callSignOffParent:true
      })
    }else{
    this.props.campaignActions.editContentProject(
      'status',
      e.value,
      campId,
      contentProjectId
    )}
    this.setProjectStatus(e.value)
  }
  submitNotes = data => {
    let campId = this.props.campaign.identity
    let contentProjectId = this.props.contentProjectData.project_identity
    this.props.campaignActions.editContentProject(
      'notes',
      data.notes,
      campId,
      contentProjectId
    )
  }
  editNotes () {
    this.setState({ editNotes: true })
    this.handleInitialize(this.props)
  }
  cancelEditNotes () {
    this.setState({
      editNotes: false
    })
  }
  updateText (id, type, e) {
    this.setState({ overlay_flag: true })
    var isUserFound = false
    if (type == 'department') {
      this.props.departments.list.length > 0
        ? this.props.departments.list.map((DepartmentDetail, i) => {
          if (DepartmentDetail.identity == id) {
            selectedDepartmentData = DepartmentDetail
          }
        })
        : ''
    } else {
      this.props.userList.length > 0
        ? this.props.userList.map((userDetail, i) => {
          if (userDetail.id == id) {
            selectedUserData = userDetail
            isUserFound = true
          }
        })
        : ''
      if (isUserFound == false) {
        selectedUserData = 'No user found'
      }
    }
    const domNode = ReactDOM.findDOMNode(e.target)
    let coords = domNode.getBoundingClientRect()
    let coords_top = coords.top + pageYOffset
    let popup_wrapper = document.getElementById('pop-up-tooltip-holder')
    popup_wrapper.classList.remove('hide', 'showtotop')

    let coords_left = 0

    let coords_area = 0

    if (screen.width >= 768) {
      coords_left = coords.left + pageXOffset + coords.width / 2
    } else {
      coords_left = coords.left + pageXOffset
      coords_area = coords_left + 472
      if (coords_area < screen.width) {
        popup_wrapper.classList.add('popup-left')
      } else {
        popup_wrapper.classList.add('popup-right')
      }
    }
    if (e.nativeEvent.screenY < 400) {
      if (e.target.className == 'thumbnail') {
        popup_wrapper.classList.add('thumbnai_popup')
      }
    } else {
      popup_wrapper.classList.add('showtotop')
    }

    this.setState({
      popupTop: coords_top,
      popupLeft: coords_left,
      popupdisplay: 'block',
      tagType: type,
      tagTypeClass: type === 'department' ? 'departmentpopup' : ''
    })
  }
  submitComment (values) {
    
    var campId = this.props.campaign.identity
    let contentProjectId =
      this.props.contentProjectData.project_identity !== undefined
        ? this.props.contentProjectData.project_identity
        : ''
    if (values.CommentId !== undefined) {
      this.createTagList(values.updateComment).then(desc => {
        values.updateComment = desc
        this.props.campaignActions.updateComment(
          values,
          campId,
          this.state.commentTag,
          contentProjectId,values.editParentId
        )
      })
    } else if (values.submitReply !== undefined) {
      this.createTagList(values.submitReply).then(desc => {
        values.submitReply = desc
        this.props.campaignActions.submitCommentReply(
          values,
          campId,
          this.state.commentTag,
          contentProjectId
        )
      })
    } else {
      this.createTagList(values.submitComment).then(desc => {
        values.submitComment = desc
        this.props.campaignActions.submitComment(
          values,
          campId,
          this.state.commentTag,
          contentProjectId
        )
      })
    }
  }
  createTagList (str) {
    var me = this
    var rx1 = /@\[([^\]]+)]\(([^)]+)\)/g
    let res = str.match(rx1)
    var userTag = []
    var tagIds = []
    return new Promise(function (resolve, reject) {
      if (res) {
        res.forEach(function (tags) {
          /** Get user information */
          var tagsArray = tags.split('@[')
          var nameArray = tagsArray[1].split(']')
          var name = nameArray[0]
          var dept = nameArray[1]
            .substring(0, nameArray[1].length - 1)
            .split(':')[1]
            .split('_')
          var type = 'tags'
          if (
            nameArray[1].substring(0, nameArray[1].length - 1).split(':')[0] !==
            '(tags'
          ) {
            type =
              dept[1] == 'company'
                ? 'company'
                : dept[1] == 'dept'
                  ? 'department'
                  : 'user'

            let tagsUserObject = {
              tagid: dept[0],
              tagName: name.replace(/ /g, ''),
              type: type
            }
            userTag.push(tagsUserObject)
            tagIds.push(dept[0])
          }

          /** description replace */
          if (type === 'tags') {
            str = str.replace(tags, '#' + name.replace(/ /g, ''))
          } else {
            str = str.replace(tags, '@' + name.replace(/ /g, ''))
          }
        })
        me.setState({
          commentTag: userTag,
          assigneeIds: tagIds
        })
      }
      resolve(str)
    })
  }
  openDetailPopup (value) {
    document.body.classList.add('overlay')
    this.setState({
      taskDetail: true,
      currTask: value,

    })
  }
 saveTemplateHtml(html,html_preview,contentType){
    var callAfterSaveTemplate=true
    let campId = this.props.campaign.identity
    let contentProjectId = this.props.contentProjectData.project_identity;
    let callForAssignee = false;
    let isTemplateSelected= false;
    this.props.campaignActions.editContentProject(
      'html',
       html,
      campId,
      contentProjectId,false,false,callAfterSaveTemplate,contentType
    )
    this.props.campaignActions.editContentProject('html_preview',html_preview,campId,contentProjectId,false,false,callAfterSaveTemplate,contentType)
  }
  saveTemplateDraft(html,html_preview,pdfURL,contentType){
    let campId = this.props.campaign.identity;
    let contentProjectData = this.props.contentProjectData;
    let contentProjectId = contentProjectData.project_identity;
    var objTosend ={
      html : html,
      html_preview: html_preview,
      title: contentProjectData.title,
      type:contentProjectData.type,
      pdfUrl : pdfURL,
      contentType :contentType
    }
    this.props.campaignActions.addDraft(objTosend,campId,contentProjectId);
  }
  showAssignPopup(){
    this.setState({
      showAssignPopup : !this.state.showAssignPopup
    })

  }
  renderDraftsLoader(){
    return(
      <div className = "templateBlock template-loader">
          <div className ="innerTemplateBlock">
            <div className = "templateImageBlock"> <img src="/img/visiblyLoader.gif" /></div> 
          </div>
          <div className = "titleForTemplateBlock"> <div className="loader-grey-line loader-line-height loader-line-radius loader-assets-title"> </div> </div>
      </div>
    )
  }
  openSelectedDraft(data){
    let contentProjectId = this.props.contentProjectData.project_identity;
    let campId = this.props.campaign.identity
    this.setState({
      callContentWorkSpace :true,
      isTemplateSelected:true,
      // callContentWorkSpace :true,
      workSpaceHtmlContent : data
    })
    // this.props.campaignActions.editContentProject(
    //       'html',
    //       data.html_content,
    //       campId,
    //       contentProjectId
    // )
  }
  assigneeClicked(assigneeData,contentProjectAssigneeList=[]){
    let userIdentity = assigneeData.identity? assigneeData.identity:assigneeData.id;
    let campId = this.props.campaign.identity
    let contentProjectId = this.props.contentProjectData.project_identity
    if(contentProjectAssigneeList.length> 0){
      if(contentProjectAssigneeList.indexOf(userIdentity) !== -1){
        //unselect assignee (remove/delete)
        this.props.campaignActions.editContentProject(
          'remove_asssignee',
          userIdentity,
          campId,
          contentProjectId,true,this.state.isTemplateSelected
        )
        this.setState({
          isTemplateSelected:false,
          showAssignPopup:false
        })
      }else{
        //select/add new assignee
        this.props.campaignActions.editContentProject(
          'asssignee',
          userIdentity,
          campId,
          contentProjectId,true,this.state.isTemplateSelected
        )
        this.setState({
          isTemplateSelected:false,
          showAssignPopup:false
        })
      }
    }else{
      ///select/add new assignee
      this.props.campaignActions.editContentProject(
        'asssignee',
        userIdentity,
        campId,
        contentProjectId,true,this.state.isTemplateSelected
      )
      this.setState({
          isTemplateSelected:false,
          showAssignPopup:false
        })
    }
  }
  handleSearchValue(e){
    if(e!==null){
        if(e.target.value!=="" && this.state.filter_state!==undefined)
        {
          var i=0;
          search=e.target.value.toLowerCase();
          var tempobj=this.state.filter_state.filter(function(temp){
            i=i+1;
            if(temp.display.toLowerCase().includes(search)){
              return true;
            }
          })
          this.setState({contentUserNames:tempobj})
        }
        else{
          this.setState({contentUserNames:this.state.filter_state})
        }
      }
}

changeConentDate(start_date,end_date){
  // if(start_date<0 && end_date <0 ){}
 if(start_date >  0 && end_date > 0 ){
    //if campaign start date is in future
    this.setState({
      datetoEdit:'start_date'
    })
  }else if(start_date  <= 0 && end_date >= 0 ){
    //if campaign is started
    this.setState({
      datetoEdit:'end_date'
    })
  }else{
    this.setState({
      datetoEdit :'end_date'
    }) 
  }
}
cancelDateEdit(){
  this.setState({
    datetoEdit:null
  })
}
handleSignOff(pdfURL,html_preview , contentType){
  let campId = this.props.campaign.identity;
  let contentProjectId = this.props.contentProjectData.project_identity;
  let callForAssignee = false;
  let isTemplateSelected= false;
  let callAfterSaveTemplate=false
  this.props.campaignActions.editContentProject("signoff",pdfURL,campId,contentProjectId,callForAssignee,isTemplateSelected,callAfterSaveTemplate,contentType);
  // this.props.campaignActions.editContentProject(
  //   'status',
  //   5,
  //   campId,
  //   contentProjectId
  // )
  this.props.closeContentProject();
}
handleChangeRequest(){
  let contentProjectId = this.props.contentProjectData.project_identity;
  this.props.handleChangeRequest(contentProjectId); 
}
handleSendReview(html_preview){
  let campId = this.props.campaign.identity
  let contentProjectId = this.props.contentProjectData.project_identity
  this.props.campaignActions.editContentProject(
    'status',
    4,
    campId,
    contentProjectId
  )
  this.props.campaignActions.editContentProject('html_preview',html_preview,campId,contentProjectId);
  this.props.closeContentProject();
}
updateDateValue(value){
  let type = this.state.datetoEdit=='start_date'?'start_date':this.state.datetoEdit=='end_date'?'end_date':'';
  let campId = this.props.campaign.identity
    let contentProjectId = this.props.contentProjectData.project_identity
    this.props.campaignActions.editContentProject(
      type,
      value,
      campId,
      contentProjectId
    )
}
openCreateTaskPopup(){
  document.body.classList.add('overlay');
  this.setState({
    createTasks:true
  })
}
closeCreateTaskPopup(){
  document.body.classList.remove('overlay');
  this.setState({
    createTasks:false,
  })
}
createTaskPopup(){
  return(
    <SliderPopup className="task-create-popup">
    <header class="heading clearfix">
        <h3>Create Task</h3>
        <button class="btn-default close-popup" onClick={this.closeCreateTaskPopup.bind(this)}><i class="material-icons" >clear</i></button>
       </header>
    <CampaignCreateTasks
      addTaskLoading = {this.props.campaigns.addTaskLoading}
      onSubmit={this.submitTaskData.bind(this)}
      Moment={this.props.moment} 
      contentProject= {this.props.contentProjectData}
      campaignData ={this.props.campaign}
      profile={this.props.profile}
      roleName={this.props.roleName}
    />
</SliderPopup>
  )
}
submitTaskData(values){
  let contentProjectId = this.props.contentProjectData.project_identity
   var dueTime = this.props
        .moment(values[`scheduledDueTime`])
        .format("HH:mm");
    var dueDate = this.props
        .moment
        .unix(values[`scheduledDueDate`])
        .format("DD/MM/YYYY");
    let DueDateTime = dueDate + " " + dueTime;
    let DueDateTimeStamp =  this.props
        .moment(DueDateTime, 'DD/MM/YYYY HH:mm')
        .format("X");
    var objTosend={
      title : values.title,
      AssignedToIdentity: values.assignTo,
      DueDateTime : DueDateTimeStamp,
    }
    if(values.notes!==undefined){
      var obj={
        notes:values.notes
      }
      Object.assign(objTosend,obj);
    }
    if(values.status!==undefined){
      var obj={
        status : values.status
      }
      Object.assign(objTosend,obj);
    }
    var campId = this.props.campaign.identity;
    if(values.identity!==undefined){
      var taskId =values.identity;
      // edit task
      this.props.campaignActions.addTaskData(objTosend,campId,this.props.roleName,taskId,contentProjectId);
      this.closeDetailPopup();
    }else{
      // add task
      this.props.campaignActions.addTaskData(objTosend,campId,this.props.roleName,null,contentProjectId);
      this.closeDetailPopup();
    }  
}
closeDetailPopup(){
  document.body.classList.remove('overlay');
  this.setState({
    taskDetail:false
  })
}
openDetailPopup(value){
  document.body.classList.add('overlay');
  this.setState({
    taskDetail:true,
    currTask:value
  })
}
taskDetailPopup(){
  return(
    <SliderPopup className="task-create-popup">
    <header class="heading clearfix">
        <h3>Task detail</h3>
        <button class="btn-default close-popup" onClick={this.closeDetailPopup.bind(this)}><i class="material-icons" >clear</i></button>
       </header>
    <CampaignTaskDetail 
          Moment={this.props.moment} 
          currTask={this.state.currTask} 
          roleName={this.props.roleName} 
          editTask={this.editTask.bind(this)} 
          deleteTask={this.deleteTask.bind(this)}/>
    </SliderPopup>
  )
}
editTask(){
  this.closeDetailPopup();
  document.body.classList.add('overlay');
  this.setState({
    editTask:true,
  })
}
deleteTask(id){
  var campId = this.props.campaign.identity;
  let contentProjectId = this.props.contentProjectData.project_identity
  this.props.campaignActions.deleteTask(id,campId,"openTask",this.props.roleName,this.props.profile.identity,contentProjectId);
}
openEditTaskPopup(){
return(
  <SliderPopup className="task-create-popup">
  <header class="heading clearfix">
      <h3>Edit Task</h3>
      <button class="btn-default close-popup" onClick={this.closeCreateTaskPopup.bind(this)}><i class="material-icons" >clear</i></button>
     </header>
  <CampaignCreateTasks 
    onSubmit={this.submitTaskData.bind(this)}
    Moment={this.props.moment} 
    currTask={this.state.currTask}
    campaignData ={this.props.campaign}
    profile={this.props.profile}
    roleName={this.props.roleName}
    contentProject= {this.props.contentProjectData}
  />
</SliderPopup>)
}

openAddUserPopup(e, projectid){
    this.setState({
       addUserflag : true,
       projectId : projectid
     })
}
closeAddUserPopup(){
  document.body.classList.remove('overlay');
  this.setState({
    addUserflag : false
 })
}
closeTemplatePrintbutton(){
  this.setState({
      openTemplatePreviewPopup:false,
      previewLink:{}
  })
}
openTemplatePreviewPopupIsClicked(previewLink){
  this.setState({
      openTemplatePreviewPopup:true,
      previewLink:previewLink
  })
}
closeTemplatePreviewPopupbutton(){
  document.body.classList.remove('center-wrapper-body-container');
    this.setState({
        openTemplatePreviewPopup:false,
        previewLink:{}
    })
}
changeShippingDate(shipping_date){
  this.setState({
    shippingDateFlag : true 
  })
}
cancelShippingDateEdit(){
 this.setState({
   shippingDateFlag : false
 })
}
updateDateValueShippingDate(value){
  let type = "shipping_date"
  let campId = this.props.campaign.identity;
  let contentProjectId = this.props.contentProjectData.project_identity ;
  this.props.campaignActions.editContentProject(
    type,
    value,
    campId,
    contentProjectId
  )
}
openTemplatePreviewPopup(){
  var html_Preview_campaigns = "";
  var templatePreview ="";
  let html_Preview = '';
  let data = '';
  document.body.classList.add('center-wrapper-body-container');
  var userList=[];
  var media_url='https://visibly-staging.s3.eu-west-2.amazonaws.com/pirates_com/content_print/1567686479066.pdf';
  if(this.state.previewLink.type == "print"){
      if(this.state.previewLink.pdf_url){
        media_url = this.state.previewLink.pdf_url;
      }else if(this.state.previewLink.pdf_s3_url){
        media_url = this.state.previewLink.pdf_s3_url;
      }
}else{
 html_Preview_campaigns = this.state.previewLink.html_preview;
 templatePreview = decodeURIComponent(escape(atob(html_Preview_campaigns)));


}
  return(
  <div>
          <CenterPopupWrapper>
              <div className = "print-page-container">
              <div className='inner-print-page-container'>
                  <div className='print-details'>
                      <header className='heading clearfix'>
                          <h3> {this.state.previewLink.title}</h3>
                          <button id="close-popup" class="btn-default" onClick = {this.closeTemplatePreviewPopupbutton.bind(this)}><i class="material-icons">clear</i></button>
                      </header>
                      <PopupWrapper>
                      <div className = "linkpreview-parent-class">
                        {this.state.previewLink.type == "print" ? 
                            <div>
                                <iframe className = "pdf-preview contetnProject"
                                // src={`https://docs.google.com/gview?url=${media_url}&embedded=true`} // i have commented this bcz Google Docs Viewer occasionally failing to load content in iframe - html (disha)
                                src={media_url}
                                />
                            </div>
                         :
                          <div id = "emailWrapper"> 
                              <div dangerouslySetInnerHTML={{__html: templatePreview}} className="inner-pdf-wrapper"></div>
                           </div>
                         }
                      </div>
                      </PopupWrapper>
                  </div>
              </div>
              </div>
          </CenterPopupWrapper>
  </div>
  )
}
  addUserDataPopup(){
    return(
       <SendMailPopup 
        projectId = {this.state.projectId} 
         campaginData = {this.props}
         closeAddUserPopup={this.closeAddUserPopup.bind(this)}
         /> 
    )
  }
  render () {

    const { handleSubmit, pristine, submitting } = this.props
    let { contentProjectData } = this.props;
    let optionState = projectStatus
    var ShippingDate =
      this.props.moment !== null
        ? this.props.moment
          .unix(parseInt(contentProjectData.shipping_date))
          .format('DD-MM-YYYY')
        : ''
    var user = this.props.users
    let isFetching = user.isFetching
    var image = {
			backgroundImage: `url('https://s3.eu-west-2.amazonaws.com/visibly-staging/images/pirates_com/1/Screenshot_20181101-153927.png')`
    }
    var defaultTemplatesList=[];
    var defaultTemplatesLoading=this.props.campaigns.defaultDraftTemplates !== undefined ? this.props.campaigns.defaultDraftTemplates.defaultDraftTemplatesLoading :false;
    if(this.props.campaigns.defaultDraftTemplates !== undefined && this.props.campaigns.defaultDraftTemplates.defaultDraftTemplatesData.length>0 ){
      defaultTemplatesList=this.props.campaigns.defaultDraftTemplates.defaultDraftTemplatesData;
    }
    var draftFilesList=[];
    var draftFilesLoading=this.props.campaigns.draftFiles !== undefined ? this.props.campaigns.draftFiles.draftFilesLoading :false;
    if(this.props.campaigns.draftFiles !== undefined && this.props.campaigns.draftFiles.draftFilesData.length>0 ){
      draftFilesList=this.props.campaigns.draftFiles.draftFilesData;
    }
    var date_diff_indays = function(date1, date2) {
      var dt1 = new Date(date1);
      var dt2 = new Date(date2);
      return Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate()) ) /(1000 * 60 * 60 * 24));
      }
      
    var projectEndDate = this.props.moment!==null? this.props.moment.unix(parseInt(contentProjectData.end_date)).format('DD-MM-YYYY'):'';
    var projStartDate =  this.props.moment!==null? this.props.moment.unix(parseInt(contentProjectData.start_date)).format('MM-DD-YYYY'):'';
    var projEndDate = this.props.moment!==null? this.props.moment.unix(parseInt(contentProjectData.end_date)).format('MM-DD-YYYY'):'';
    var projShippingDate = contentProjectData.shipping_date != null ? this.props.moment!==null? this.props.moment.unix(parseInt(contentProjectData.shipping_date)).format('DD-MM-YYYY'):'':'';;
    var currDate = this.props.moment().format('MM-DD-YYYY');
    var projMinEndDate =  this.props.moment!==null? this.props.moment.unix(parseInt(contentProjectData.end_date)).format('MM-DD-YYYY'):'';
 
    var projShippingDateNo = '';
    //call fun to get date diff
    projStartDate=date_diff_indays(currDate, projStartDate);
    projEndDate=date_diff_indays(currDate, projEndDate);
    projShippingDateNo = date_diff_indays(currDate, projShippingDate);
    var dateToDisplay='';
    if(projStartDate <  0 && projEndDate < 0  ){
      //if campaign is expired
      dateToDisplay = 'Expired on ' +projectEndDate;
    }else if(projStartDate >  0 && projEndDate > 0 ){
      //if campaign start date is in future 
      dateToDisplay = Math.abs(projStartDate) +" days to start"
    }else if(projStartDate  <= 0 && projEndDate > 0 ){
      //if campaign is started
      dateToDisplay = Math.abs(projEndDate) +" days to end"
    }else{
      dateToDisplay = "Expire today"
    }
   var shippingDateDisplay  = '';
  
  shippingDateDisplay = projShippingDate;
    var endDate = "";
    if (
      this.props.campaign.end_date &&
      this.state.Moment
    ) {
      endDate = this.props.moment
        .unix(this.props.campaign.end_date)
        .format("llll")
        .toString();
    }
     return (
      <div class = {`content-project-main-wrapper content-project-main ${this.props.campaigns.currContentProject.type == "email" ? "email-content-project-main" :''}`}>  
        {this.state.openTemplatePreviewPopup==true? this.openTemplatePreviewPopup():''}
        {this.state.addUserflag == true ? this.addUserDataPopup() :''}
        {this.props.campaigns.editProjectLoading==true || this.props.campaigns.addDraftLoading==true ?  <PreLoader className = "Page" /> :''}
          {/* page */}
          <div class='campaginContainerWraper'>
            <div class='campaignListLink'>
              <a onClick={this.props.closeContentProject}>
                <i class='material-icons'>chevron_left</i> back to Campaign
              </a>
            </div>
            <section class='campaignDetailBlock'>
              {this.state.createTasks? this.createTaskPopup(): ''}
              {this.state.taskDetail ? this.taskDetailPopup():''}
            {this.state.editTask?this.openEditTaskPopup():''}
              <div class='detailCampaignHeader'>
                <div class='detailCampaignTitle'>
                  {this.state.EditMode !== true ? (
                    <span className='campaignTitleWrapper'>
                      <i class='material-icons'>
                        {' '}
                        {contentProjectData.type == 'email'
                          ? 'mail_outline'
                          : contentProjectData.type == 'print'
                            ? 'text_fields'
                            : contentProjectData.type == 'sms'
                              ? ''
                              : ''}
                      </i>
                      <span className='dateCampaign'>
                        {contentProjectData.title}
                      </span>
                    </span>
                  ) : (
                    ''
                  )}
                  <span
                    className='hide campaignTitleWrapper titleEditable'
                    name='contentProjectTitle'
                    id='contentProjectTitle'
                    contentEditable='true'
                    suppressContentEditableWarning
                  >
                    {' ' + contentProjectData.title}
                  </span>
                  {this.state.EditMode == true ? (
                    <a
                      className='editCampaignBtn'
                      onClick={() => {
                        this.saveContentTitle()
                      }}
                    >
                      <i className='material-icons'>check</i>
                    </a>
                  ) : (
                    ''
                  )}
                  {this.state.EditMode == true ? (
                    <a
                      className='editCampaignBtn'
                      onClick={() => {
                        this.cancelTitleEdit(contentProjectData.title)
                      }}
                    >
                      <i className='material-icons'>close</i>
                    </a>
                  ) : (
                    ''
                  )}
                  {this.state.EditMode !== true ? (
                    <a
                      class='editCampaignBtn'
                      onClick={() => {
                        this.handlTitleEdit(contentProjectData.title)
                      }}
                      title='Edit'
                    >
                      <i class='material-icons'>mode_edit</i>
                    </a>
                  ) : (
                    ''
                  )}
                </div>

                <div class='campaignMeta clearfix'>
                  <div class='campaignMetaLeft clearfix'>
                    <div class='campaignDateWrapper'>
                       <span class='campaignDateIcon'>
                         <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path fill="none" d="M0 0h24v24H0V0z"></path><path d="M20 3h-1V1h-2v2H7V1H5v2H4c-1.1 0-2 .9-2 2v16c0 1.1.9 2 2 2h16c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zm0 18H4V10h16v11zm0-13H4V5h16v3z"></path></svg>
                      </span>
                    {this.state.datetoEdit ==null ?<span class='campaignStartDate' onClick={this.changeConentDate.bind(this,projStartDate,projEndDate)}>{dateToDisplay}</span>:''}
                     {/* //end date */}
                     <span className ="select-date-wrapper">
                      {this.state.datetoEdit=="start_date"?
                      <Field 
                      name={`startDate`}
                      component={props => {
                        return (
                          <ReactDatePicker
                            {...props}
                            reactDatePicker={this.state.ReactDatePicker}
                            minDate={this.props.moment?this.props.moment():''}
                            moment={this.props.moment}
                            dateFormat="DD/MM/YYYY"
                            fromProject={true}
                            updateProjectDate={this.updateDateValue.bind(this)}
                          />
                        )
                      }}
                      />
                      : this.state.datetoEdit=="end_date" ?<Field 
                      ref={`endDate`}
                      name={`endDate`}
                      id={`endDate`}
                      component={props => {
                        return (
                          <ReactDatePicker
                            {...props}
                            reactDatePicker={this.state.ReactDatePicker}
                            dateFormat="DD/MM/YYYY"
                            minDate={this.props.moment?this.props.moment():''}
                            moment={this.props.moment}
                            fromProject={true}
                            maxDate={endDate}
                            updateProjectDate={this.updateDateValue.bind(this)}
                          />
                        )
                      }}
                      />:''}
                    {this.state.datetoEdit!==null?
                        <a
                          className='editCampaignBtn'
                          onClick={this.cancelDateEdit.bind(this)}
                        >
                          <i className='material-icons'>close</i>
                   </a>:''}
                    </span>
                    {/* end date end */}
                    </div>


                    {/* shipping date */}
                    {contentProjectData.shipping_date !== false ?
                    <div class='campaignDateWrapper'>
                       <span class='campaignDateIcon'>
                        <svg
                          xmlns='http://www.w3.org/2000/svg'
                          width='24'
                          height='24'
                          viewBox='0 0 24 24'
                        >
                          <path fill='none' d='M0 0h24v24H0V0z' />
                          <path d='M20 8h-3V4H3c-1.1 0-2 .9-2 2v11h2c0 1.66 1.34 3 3 3s3-1.34 3-3h6c0 1.66 1.34 3 3 3s3-1.34 3-3h2v-5l-3-4zm-.5 1.5l1.96 2.5H17V9.5h2.5zM6 18c-.55 0-1-.45-1-1s.45-1 1-1 1 .45 1 1-.45 1-1 1zm2.22-3c-.55-.61-1.33-1-2.22-1s-1.67.39-2.22 1H3V6h12v9H8.22zM18 18c-.55 0-1-.45-1-1s.45-1 1-1 1 .45 1 1-.45 1-1 1z' />
                        </svg>
                      </span>
                    {this.state.shippingDateFlag == false ?<span class='campaignStartDate campaignShipingDate' onClick={this.changeShippingDate.bind(this,projShippingDate)}>{ shippingDateDisplay}</span>:''}
                     {/* //end date */}
                     <span className ="select-date-wrapper">
                      {this.state.shippingDateFlag == true?
                      <Field 
                      name={`shipping-date`}
                      component={props => {
                        return (
                          <ReactDatePicker
                            {...props}
                            reactDatePicker={this.state.ReactDatePicker}
                            minDate={projMinEndDate}
                            moment={this.props.moment}
                            dateFormat="DD/MM/YYYY"
                            fromProject={true}
                            maxDate={endDate}
                            updateProjectDate={this.updateDateValueShippingDate.bind(this)}
                          />
                        )
                      }}
                      />
                    :''}
                      {this.state.shippingDateFlag == true?
                        <a
                          className='editCampaignBtn'
                          onClick={this.cancelShippingDateEdit.bind(this)}
                        >
                          <i className='material-icons'>close</i>
                       </a>
                      :''}
                    </span>
                    {/* end date end */}
                    </div>
                    :''}
                    {/* end shipping date */}
                    <div class='campaingStatusWrapper'>
                      <span class='campaignStatusTitle'>status</span>
                      {this.state.reactSelect !== null ?
                       <this.state.reactSelect.default
                        className='campaignStatusValue'
                        options={options}
                        id='projectStatus'
                        isDisabled={(projStartDate < 0 && projEndDate <0) ? true:false}
                        onChange={this.selectedProjectStatus.bind(this)}
                        value={optionState}
                      />:''}
                    </div>
                  </div>
                  <div className="camapignHdrRight">
                      <div className="assign-author multiple-assign-author">
                        <div className ="multiple-assign">
                        <span className = "author">Assignee</span>
                        {this.state.contentProjectAssigneeList.length>0?
                          this.state.contentProjectAssigneeList.map((proUser,i)=>{
                            if(i < limitCampUser){
                                return(
                                    <div key={`assignee_`+i} className = "profile-wrapper assignAuthorProfile post"><span className = 'author-image author-thumb'>
                                      <img
                                          src={proUser.profile_image} 
                                          alt={"Default avatar"}
                                          className='list-avatar thumbnail'
                                          data-tip={proUser.email}
                                          data-for={proUser.identity}
                                          onMouseOver={() => this.setState({ showAssignPopup: false })}
                                          onError={(e) => { e.target.src = "https://app.visibly.io/img/default-avatar.png" }}
                                          />
                                      <i class="material-icons add-icon">add</i>
                                      <FetchuserDetail userID={proUser.identity}/>
                                      </span><span className = "cross-btn"> <i class="material-icons" onClick={this.assigneeClicked.bind(this,proUser,this.state.contentProjectAssigneeIdList)}>clear</i></span></div>
                                  )
                              }
                          }):''}
                          {this.state.contentProjectAssigneeList.length>0 && this.state.contentProjectAssigneeList.length >limitCampUser ?
                              <div className = "profile-wrapper assignAuthorWrapper" onClick = {this.showAssignPopup.bind(this)}><span className = "author-image"><span className = "noCount"><i class="material-icons">add</i>{Math.abs(limitCampUser-this.state.contentProjectAssigneeList.length)}</span></span> </div>
                            :''
                          }
                        <div className = "profile-wrapper addassignAuthorWrapper"  onClick = {this.showAssignPopup.bind(this)}><span className = "author-image"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path fill="none" d="M0 0h24v24H0V0z"/><path d="M12 6c1.1 0 2 .9 2 2s-.9 2-2 2-2-.9-2-2 .9-2 2-2m0 10c2.7 0 5.8 1.29 6 2H6c.23-.72 3.31-2 6-2m0-12C9.79 4 8 5.79 8 8s1.79 4 4 4 4-1.79 4-4-1.79-4-4-4zm0 10c-2.67 0-8 1.34-8 4v2h16v-2c0-2.66-5.33-4-8-4z"/></svg> <span className = "noCount"> <i class="material-icons">add</i></span></span></div>
                        </div>
                        {this.state.showAssignPopup  == true ? 
                        <div className  ="assign-popup"> 
                            <div className= "search-input">
                              <input 
                                placeholder='Search Name'
                                id='searchUser'
                                type='text'
                                name='searchUser'
                                onChange={this
                                  .handleSearchValue
                                  .bind(this)}
                                />
                              </div>
                            <div className = "assign-popup-block-wrapper">
                            {this.state.contentUserNames.length>0?
                                this.state.contentUserNames.map((campUser,i)=>{
                                  return(
                                    <div>
                                     {
                                       campUser.id!=contentProjectData.owner.identity ?
                                    <div className = {`assign-block ${this.state.contentProjectAssigneeIdList.length> 0? this.state.contentProjectAssigneeIdList.indexOf(campUser.id) !== -1? 'active':'':''}`} key={`assignUserPopup_`+i} onClick={this.assigneeClicked.bind(this,campUser,this.state.contentProjectAssigneeIdList)} > 
                                       <div className = "profile-wrapper assignAuthorProfile post">
                                        <span className = 'author-image author-thumb'>
                                        <img
                                          src={campUser.profile_image} 
                                          alt={"Default avatar"}
                                          className='list-avatar thumbnail'
                                          data-tip={campUser.email}
                                          data-for={campUser.id}
                                          onError={(e) => { e.target.src = "https://app.visibly.io/img/default-avatar.png" }}
                                          />
                                        </span>
                                        {this.state.contentProjectAssigneeIdList.length> 0? this.state.contentProjectAssigneeIdList.indexOf(campUser.id) !== -1?
                                        <span className = "cross-btn"> 
                                            <i class="material-icons">clear</i>
                                        </span>
                                        :'':''}
                                      </div>
                                      <span className = "auhtor-name"> {campUser.display} </span>
                                      </div>
                                      :
                                      '' }
                                      </div>         
                              )
                            }):<div className='NovalueFound'>
                                <li className = "no-Value-chart-wrapper"> No assignee found.</li>    
                            </div>}
                            </div>
                        </div>
                        :''}
                      </div>
                      <div className='post campaign-owner'>
                <div className = "assign-author author-thumb"><span className = "author">Owner</span><img
                              className='profile thumbnail'
                              src={contentProjectData.owner.profile_image}
                              onMouseOver={() => this.setState({ showAssignPopup: false })}
                              onError={e => {
                                e.target.src =
                                  'https://app.visibly.io/img/default-avatar.png'
                              }}
                            />
                            <FetchuserDetail userID={contentProjectData.owner.identity}/>
                            </div>
                  </div>
                {/* <a className="commentToggleBtn" href="javascript:void(0);"><i class="material-icons">chat_bubble_outline</i> <span className="commentBtnText">Comments</span></a> */}
                </div>
                </div>
              </div>
              <div class='listMenu'>
                <nav class='nav-side clearfix'>
                  <ul class='parent'>
                    <li>
                      <a
                        class={`${
                          this.state.currContentTab == 'notes' ? 'active' : ''
                        }`}
                        onClick={() => {
                          this.changeContentTab('notes')
                        }}
                      >
                        <span class='text'>Notes</span>
                      </a>
                    </li>
                    <li>
                      <a
                        class={`${
                          this.state.currContentTab == 'workspace'
                            ? 'active'
                            : ''
                        }`}
                        onClick={() => {
                          this.changeContentTab('workspace')
                        }}
                      >
                        <span class='text'>Workspace</span>
                      </a>
                    </li>
                    <li>
                      <a
                        class={`${
                          this.state.currContentTab == 'tasks' ? 'active' : ''
                        }`}
                        onClick={() => {
                          this.changeContentTab('tasks')
                        }}
                      >
                        <span class='text'>Tasks</span>
                      </a>
                    </li>
                  </ul>
                </nav>
              </div>
              <div id='campaign-details' class='clearfix 22'>
                {this.state.currContentTab == 'notes' ? (
                  this.state.editNotes == true ? (
                    <div className='campaignDetailLeftColumn'>
                      <div>
                        <form
                          onSubmit={handleSubmit(this.submitNotes.bind(this))}
                        >
                          <Field
                            id='notesData'
                            name='notes'
                            component={renderField}
                            type='textarea'
                            validate={[required]}
                          />
                          <div className='notesBtnWrapper'>
                            <button
                              className='btn btn-theme'
                              type='submit'
                              validate={[required]}
                              disabled={pristine || submitting}
                            >
                              Save
                            </button>
                            <button
                              className='btn cancel-btn default-btn'
                              type='submit'
                              onClick={this.cancelEditNotes.bind(this)}
                            >
                              cancel
                            </button>
                          </div>
                        </form>
                      </div>
                    </div>
                  ) : (
                    <div className="campaignDetailLeftColumn">
                      <div className="campaign-brief-detail">
                        <p>{contentProjectData.notes}</p>
                      </div>
                      <div className='BriefBtnWrapper'>
                        {this.props.roleName == 'admin' ||
                        this.props.roleName == 'super-admin' ? (
                          <a
                              className='btn theme-btn'
                              onClick={this.editNotes.bind(this)}
                            >
                              {' '}
                            Edit
                            </a>
                          ) : (
                            ''
                          )}
                      </div>
                    </div>
                  )
                ) : (
                  ''
                )}
                {this.state.currContentTab == 'workspace' ? (
                  <div class='campaignDetailLeftColumn'>
                    <div>
                      {this.state.callContentWorkSpace ==true || (this.props.disableTemplateSelection == true && this.state.isTemplateSelected == true || this.props.campaigns.currContentProject.html!==null)?
                      <ContentWorkSpace 
                          contentProjectData = {contentProjectData}
                          htmlContent={this.state.workSpaceHtmlContent.html_content} 
                          saveTemplateHtml={this.saveTemplateHtml.bind(this)}  
                          saveTemplateDraft ={this.saveTemplateDraft.bind(this)}
                          campaignActions={this.props.campaignActions} 
                          campaignAssetList = {this.props.campaigns.campaignAssetList}
                          profile={this.props.profile}
                          roleName={this.props.roleName}
                          callSignOffParent={this.state.callSignOffParent}
                          handleSignOff={this.handleSignOff.bind(this)}
                          handleChangeProjectRequest={this.handleChangeRequest.bind(this)}
                          handleSendReview={this.handleSendReview.bind(this)}
                          contentType = {this.props.contentProjectData.type}
                          openAddUserPopup = {this.openAddUserPopup.bind(this)}
                          {...this.props}/> :
                      <div className = "templateSectionWrapper"> 
                         <div className = "innerTemplateSectionWrapper clearfix ">
                             {/* content */}
                             <div className  = "leftTemplatePart accordion-tab-campaign show-tab">
                                  <div className = "mainTitle responsive-title"> DEFAULT TEMPLATES <span className = "accordion-icon"> <i class="material-icons">arrow_drop_down</i></span> </div>
                                  <div className = "mainTitle desktop-title"> DEFAULT TEMPLATES </div>
                                  <div className = {`clearfix templateBlockWrapper ${this.props.contentProjectData.type == "email" ? "emailTemplateBlockWrapper" : ''} `}>
                                  {defaultTemplatesLoading == true ? this.renderDraftsLoader():
                                defaultTemplatesList.length > 0?
                                  defaultTemplatesList.map((dtList,i)=>{ 
                                    let templatePreviewDefault = ''
                                      if(dtList.html_preview){
                                         templatePreviewDefault = decodeURIComponent(escape(atob(dtList.html_preview)));
                                      }else{
                                        let templateFirstScreen  = '';
                                        if(this.props.contentProjectData.type == "print"){
                                          templateFirstScreen =  `<div class="first-pdf-page">
                                          <div class="pdf-wrapper">
                                              <div class="container">
                                                  <div class="pdf-background">
                                                      <img src="https://magazine.visibly.io/images/pdf.png" alt="pdf-background" />
                                                  </div>
                                              </div>
                                          </div>
                                          <div class="pdf-content">
                                              <div class="container">
                                                  <div class="pdf-content-heading">
                                                      <h2>Admin message</h2>
                                                  </div>
                                                  <div class="content-paragraph">
                                                      <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been
                                                          the
                                                          industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of
                                                          type
                                                          and scrambled it to make a type specimen book. It has survived not only five<br /><br /><br />
  
                                                          centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It
                                                          was
                                                          popularised in the 1960s with the release of Letraset sheets containing Lorem Curabitur
                                                          pellentesque ut risus auctor varius. Integer imperdiet viverra neque, vel pharetra lacus
                                                          sagittis at. Utvariusvel tortor quis sagit-tis. Donec risus nisi, hendrerit sed mollis quis,
                                                          semper ut ligula. Cras pulvinar molestie neque, nec dapibus diam tempus vitae. Fusce
                                                          consectetur, ex nec venenatis consequat, diam quam semper diam, id porta lacus ex et nulla. Cras
                                                          commodo, augue nec commodo cursus, mi justo mattis ante, vitae varius orci massa a eros. Sed
                                                          hendrerit euismod leo. Curabitur pellentesque ut risus auctor varius. Integer imperdiet viverra
                                                          neque, vel pharetra lacus sagittis at. Utvariusvel tortor quis sagit-tis. Donec risus nisi,
                                                          hendrerit sed mollis quis, semper ut ligula. Cras pulvinar molestie neque, nec dapibus diam
                                                          tempus vitae. Fusce consectetur, ex nec venenatis consequat, diam quam semper diam, id porta
                                                          lacus ex et nulla. Cras commodo, augue nec commodo cursus, mi justo mattis ante, vitae varius
                                                          orci massa a eros. Sed hendrerit euismod leo.</p>
                                                  </div>
                                              </div>
                                          </div>
                                          </div> `;
                                        }else{

                                          templateFirstScreen =  `<div class="first-pdf-page">
                                          <div class="pdf-wrapper">
                                              <div class="container">
                                                  <div class="pdf-background">
                                                      <img src="https://magazine.visibly.io/images/pdf.png" alt="pdf-background" />
                                                  </div>
                                              </div>
                                          </div>
                                          <div class="pdf-content">
                                              <div class="container">
                                                  <div class="pdf-content-heading">
                                                      <h2>Email templateeeeeeeeeeee long teams</h2>
                                                  </div>
                                                  <div class="content-paragraph">
                                                      <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been
                                                          the
                                                          industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of
                                                          type
                                                          and scrambled it to make a type specimen book. It has survived not only five<br /><br /><br />
  
                                                          centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It
                                                          was
                                                          popularised in the 1960s with the release of Letraset sheets containing Lorem Curabitur
                                                          pellentesque ut risus auctor varius. Integer imperdiet viverra neque, vel pharetra lacus
                                                          sagittis at. Utvariusvel tortor quis sagit-tis. Donec risus nisi, hendrerit sed mollis quis,
                                                          semper ut ligula. Cras pulvinar molestie neque, nec dapibus diam tempus vitae. Fusce
                                                          consectetur, ex nec venenatis consequat, diam quam semper diam, id porta lacus ex et nulla. Cras
                                                          commodo, augue nec commodo cursus, mi justo mattis ante, vitae varius orci massa a eros. Sed
                                                          hendrerit euismod leo. Curabitur pellentesque ut risus auctor varius. Integer imperdiet viverra
                                                          neque, vel pharetra lacus sagittis at. Utvariusvel tortor quis sagit-tis. Donec risus nisi,
                                                          hendrerit sed mollis quis, semper ut ligula. Cras pulvinar molestie neque, nec dapibus diam
                                                          tempus vitae. Fusce consectetur, ex nec venenatis consequat, diam quam semper diam, id porta
                                                          lacus ex et nulla. Cras commodo, augue nec commodo cursus, mi justo mattis ante, vitae varius
                                                          orci massa a eros. Sed hendrerit euismod leo.</p>
                                                  </div>
                                              </div>
                                          </div>
                                          </div> `;
                                        }
                                    
                                       let  tempObj =btoa((unescape(encodeURIComponent(templateFirstScreen))));
                                       templatePreviewDefault = decodeURIComponent(escape(atob(tempObj)));
                                      }
                                      var saveTemplateTitle = "";
                                      if(dtList.title) {
                                        saveTemplateTitle = dtList.title.charAt(0).toUpperCase()+dtList.title.slice(1);;  
                                      } 
                                    return(
                                      <div className = "templateBlock" key={i}>
                                          <div className ="innerTemplateBlock">
                                            <div className = "templateImageBlock" onClick={this.openSelectedDraft.bind(this,dtList)}>
                                              {/* <img src ={dtList.html_preview} 
                                                onError={e => {
                                                  e.target.src =
                                                    'https://app.visibly.io/img/default-avatar.png'
                                                  }}
                                              />  */}
                                                <div dangerouslySetInnerHTML={{__html: templatePreviewDefault}} className="inner-pdf-wrapper"></div>
                                              </div>
                                            <div className = "eye-icon" onClick={this.openTemplatePreviewPopupIsClicked.bind(this,dtList)}>
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                                  <path fill="none" d="M0 0h24v24H0V0z"/>
                                                  <path d="M12 6.5c3.79 0 7.17 2.13 8.82 5.5-1.65 3.37-5.02 5.5-8.82 5.5S4.83 15.37 3.18 12C4.83 8.63 8.21 6.5 12 6.5m0-2C7 4.5 2.73 7.61 1 12c1.73 4.39 6 7.5 11 7.5s9.27-3.11 11-7.5c-1.73-4.39-6-7.5-11-7.5zm0 5c1.38 0 2.5 1.12 2.5 2.5s-1.12 2.5-2.5 2.5-2.5-1.12-2.5-2.5 1.12-2.5 2.5-2.5m0-2c-2.48 0-4.5 2.02-4.5 4.5s2.02 4.5 4.5 4.5 4.5-2.02 4.5-4.5-2.02-4.5-4.5-4.5z"/>
                                                </svg>
                                            </div>
                                          </div>
                                          <div className = "titleForTemplateBlock"> {saveTemplateTitle}  </div>
                                      </div>
                                    )
                                  }):<div class="no-data-block"> No default templates found. </div>}
                                  </div>
                                </div>
                                <div className  = "recentTemplatePart accordion-tab-campaign">
                                <div className = "mainTitle responsive-title">  SAVED TEMPLATES  <span className = "accordion-icon"> <i class="material-icons">arrow_drop_down</i></span> </div>
                                <div className = "mainTitle desktop-title"> SAVED TEMPLATES </div>
                                  <div className = "clearfix templateBlockWrapper">
                                {draftFilesLoading == true ? this.renderDraftsLoader():
                                draftFilesList.length >0?
                                  draftFilesList.map((dfList,i)=>{ 
                                    let templatePreviewSaved = ''
                                      if(dfList.html_preview){
                                        templatePreviewSaved = decodeURIComponent(escape(atob(dfList.html_preview)));
                                      }else{
                                        let templateFirstScreen = '';
                                        if(this.props.contentProjectData.type == "print"){
                                          templateFirstScreen =  `<div class="first-pdf-page">
                                          <div class="pdf-wrapper">
                                              <div class="container">
                                                  <div class="pdf-background">
                                                      <img src="https://magazine.visibly.io/images/pdf.png" alt="pdf-background" />
                                                  </div>
                                              </div>
                                          </div>
                                          <div class="pdf-content">
                                              <div class="container">
                                                  <div class="pdf-content-heading">
                                                      <h2>Admin message</h2>
                                                  </div>
                                                  <div class="content-paragraph">
                                                      <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been
                                                          the
                                                          industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of
                                                          type
                                                          and scrambled it to make a type specimen book. It has survived not only five<br /><br /><br />
  
                                                          centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It
                                                          was
                                                          popularised in the 1960s with the release of Letraset sheets containing Lorem Curabitur
                                                          pellentesque ut risus auctor varius. Integer imperdiet viverra neque, vel pharetra lacus
                                                          sagittis at. Utvariusvel tortor quis sagit-tis. Donec risus nisi, hendrerit sed mollis quis,
                                                          semper ut ligula. Cras pulvinar molestie neque, nec dapibus diam tempus vitae. Fusce
                                                          consectetur, ex nec venenatis consequat, diam quam semper diam, id porta lacus ex et nulla. Cras
                                                          commodo, augue nec commodo cursus, mi justo mattis ante, vitae varius orci massa a eros. Sed
                                                          hendrerit euismod leo. Curabitur pellentesque ut risus auctor varius. Integer imperdiet viverra
                                                          neque, vel pharetra lacus sagittis at. Utvariusvel tortor quis sagit-tis. Donec risus nisi,
                                                          hendrerit sed mollis quis, semper ut ligula. Cras pulvinar molestie neque, nec dapibus diam
                                                          tempus vitae. Fusce consectetur, ex nec venenatis consequat, diam quam semper diam, id porta
                                                          lacus ex et nulla. Cras commodo, augue nec commodo cursus, mi justo mattis ante, vitae varius
                                                          orci massa a eros. Sed hendrerit euismod leo.</p>
                                                  </div>
                                              </div>
                                          </div>
                                          </div> `;
                                        }else{
                                          templateFirstScreen =  `<div class="first-pdf-page">
                                          <div class="pdf-wrapper">
                                              <div class="container">
                                                  <div class="pdf-background">
                                                      <img src="https://magazine.visibly.io/images/pdf.png" alt="pdf-background" />
                                                  </div>
                                              </div>
                                          </div>
                                          <div class="pdf-content">
                                              <div class="container">
                                                  <div class="pdf-content-heading">
                                                      <h2>email Yrmlate</h2>
                                                  </div>
                                                  <div class="content-paragraph">
                                                      <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been
                                                          the
                                                          industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of
                                                          type
                                                          and scrambled it to make a type specimen book. It has survived not only five<br /><br /><br />
  
                                                          centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It
                                                          was
                                                          popularised in the 1960s with the release of Letraset sheets containing Lorem Curabitur
                                                          pellentesque ut risus auctor varius. Integer imperdiet viverra neque, vel pharetra lacus
                                                          sagittis at. Utvariusvel tortor quis sagit-tis. Donec risus nisi, hendrerit sed mollis quis,
                                                          semper ut ligula. Cras pulvinar molestie neque, nec dapibus diam tempus vitae. Fusce
                                                          consectetur, ex nec venenatis consequat, diam quam semper diam, id porta lacus ex et nulla. Cras
                                                          commodo, augue nec commodo cursus, mi justo mattis ante, vitae varius orci massa a eros. Sed
                                                          hendrerit euismod leo. Curabitur pellentesque ut risus auctor varius. Integer imperdiet viverra
                                                          neque, vel pharetra lacus sagittis at. Utvariusvel tortor quis sagit-tis. Donec risus nisi,
                                                          hendrerit sed mollis quis, semper ut ligula. Cras pulvinar molestie neque, nec dapibus diam
                                                          tempus vitae. Fusce consectetur, ex nec venenatis consequat, diam quam semper diam, id porta
                                                          lacus ex et nulla. Cras commodo, augue nec commodo cursus, mi justo mattis ante, vitae varius
                                                          orci massa a eros. Sed hendrerit euismod leo.</p>
                                                  </div>
                                              </div>
                                          </div>
                                          </div> `;
                                        }
                                       
                                        let  tempObj =btoa((unescape(encodeURIComponent(templateFirstScreen))));
                                        templatePreviewSaved = decodeURIComponent(escape(atob(tempObj)));
                                      }
                                      var saveTemplateTitle = "";
                                      if(dfList.title) {
                                        saveTemplateTitle = dfList.title.charAt(0).toUpperCase()+dfList.title.slice(1);;  
                                      } 
                                    return(
                                      <div className = "templateBlock" key={`defaultFile_`+i}>
                                          <div className ="innerTemplateBlock">
                                          <div className = "templateImageBlock" onClick={this.openSelectedDraft.bind(this,dfList)}>

                                              <div dangerouslySetInnerHTML={{__html: templatePreviewSaved}} className="inner-pdf-wrapper"></div>
                                              </div>
                                            <div className = "eye-icon" onClick={this.openTemplatePreviewPopupIsClicked.bind(this,dfList)}>
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                                  <path fill="none" d="M0 0h24v24H0V0z"/>
                                                  <path d="M12 6.5c3.79 0 7.17 2.13 8.82 5.5-1.65 3.37-5.02 5.5-8.82 5.5S4.83 15.37 3.18 12C4.83 8.63 8.21 6.5 12 6.5m0-2C7 4.5 2.73 7.61 1 12c1.73 4.39 6 7.5 11 7.5s9.27-3.11 11-7.5c-1.73-4.39-6-7.5-11-7.5zm0 5c1.38 0 2.5 1.12 2.5 2.5s-1.12 2.5-2.5 2.5-2.5-1.12-2.5-2.5 1.12-2.5 2.5-2.5m0-2c-2.48 0-4.5 2.02-4.5 4.5s2.02 4.5 4.5 4.5 4.5-2.02 4.5-4.5-2.02-4.5-4.5-4.5z"/>
                                                </svg>
                                            </div>
                                          </div>
                                          <div className = "titleForTemplateBlock"> {saveTemplateTitle}  </div>
                                      </div>)
                                    })
                                    :<div class="no-data-block"> No saved templates found. </div>}
                                  </div>
                                </div>
                             {/*  end content */}
                          </div> 
                          {/* <div className = "createTemplateBtn"> <a className = "btn button btn-primary"> Create New</a></div> */}
                      </div> 
                      }
                      
                    </div>
                  </div>
                ) : null}
                {this.state.currContentTab == 'tasks' ? (
                  <div class='campaignDetailLeftColumn'>
                    <ul className='dd-menu context-menu campaign_3dots'>
                      <li className='button-dropdown'>
                          <a className='dropdown-toggle'>
                            <i className='material-icons'>more_vert</i>
                          </a>
                          <ul className='dropdown-menu'>
                            <li>
                                {typeof this.props.campaign !==
                                  'undefined'
                                    ? <a
                                    className=''
                                    onClick={this.openCreateTaskPopup.bind(this)}
                                  >
                                    Add new task
                                      </a>
                                      :null}
                            </li>
                          </ul>
                        </li>
                      </ul>
                    <CampaignTasks
                      callFrom='contentProject'
                      contentProjectData={contentProjectData}
                      openDetailPopup={this.openDetailPopup.bind(this)}
                      campaign={this.props.campaign}
                      Moment={this.props.moment}
                      roleName={this.props.roleName}
                    />
                  </div>
                ) : null}
                {/* <div class='campaignDetailRightColumn'>
                  <CampaignComments
                    callFrom='contentProject'
                    contentProjectData={contentProjectData}
                    campaign={this.props.campaign}
                    Moment={this.props.moment}
                    onSubmit={this.submitComment.bind(this)}
                    userTags={this.state.commentUserList}
                    postTags={this.props.postTags}
                    updateText={this.updateText.bind(this)}
                    roleName={this.props.roleName}
                  />
                  <div id='pop-up-tooltip-holder'>
                    {selectedUserData == 'No user found' &&
                    this.state.tagType !== 'department' ? (
                      <div
                          className='pop-up-tooltip-not-data-found'
                          style={{
                            top: this.state.popupTop,
                            left: this.state.popupLeft,
                            display: this.state.popupdisplay
                          }}
                        >
                          <span className='popup-arrow' />
                          <p> No user data found </p>
                        </div>
                      ) : (
                        <div
                          id='pop-up-tooltip-wrapper'
                          className={this.state.tagTypeClass}
                        >
                          <div
                            className='pop-up-tooltip'
                            style={{
                              top: this.state.popupTop,
                              left: this.state.popupLeft,
                              display: this.state.popupdisplay
                            }}
                          >
                            <span className='popup-arrow' />
                            <TagPopup
                              userDetails={selectedUserData}
                              isFetching={isFetching}
                              isDept={this.state.tagType}
                              department={selectedDepartmentData}
                            />
                          </div>
                        </div>
                      )}
                  </div>
                </div> */}
              </div>
            </section>
          </div>
          {/* page */}
      </div>
    )
  }
}

const mapStateToProps = state => ({
  
  campaigns: state.campaigns,
  profile:state.profile.profile,
  postTags: state.tags.postTags,
  users: state.users,
  userList: state.tags.userTags
})

const mapDispatchToProps = dispatch => ({
  campaignActions: bindActionCreators(campaignActions, dispatch)
})
let connection = connect(
  mapStateToProps,
  mapDispatchToProps
)
var reduxConnectedComponent = connection(ContentProject)

export default reduxForm({
  form: 'ContentProject' // a unique identifier for this form
})(reduxConnectedComponent)
