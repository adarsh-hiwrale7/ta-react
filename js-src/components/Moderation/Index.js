import { bindActionCreators } from 'redux';
import { browserHistory } from 'react-router';
import Collapse, { Panel } from 'rc-collapse';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import * as moderationActions from '../../actions/moderationActions';
import * as headerActions from '../../actions/header/headerActions';
import * as utils from '../../utils/utils';
import $ from '../Chunks/ChunkJquery';
import AssetDetail from '../Assets/AssetDetail';
import AssetTags from '../Assets/AssetTags';
import ChunkJqueryDropdown from '../Chunks/ChunkJqueryDropdown';
import DeclineFeedback from './DeclineFeedback';
import FilterWrapper from '../FilterWrapper';
import Globals from '../../Globals';
import Header from '../Header/Header';
import moment from '../Chunks/ChunkMoment';
import ModerationNav from './ModerationNav';
import notie from 'notie/dist/notie.js';
import PreLoader from '../PreLoader';
import Post from '../Feed/Post'
import reactTooltip from '../Chunks/ChunkReactTooltip';
import ReactDOM from 'react-dom';
import reactStringReplace from 'react-string-replace';
import SliderPopup from '../SliderPopup';
import ModerationFilter from './ModerationFilter';
import TagPopup from '../Feed/TagPopup';
import GuestUserRestrictionPopup from '../GuestUserRestrictionPopup'
import StaticDataForGuest from '../StaticDataForGuest';
import FetchuserDetail from '../FetchUsersDetail/FetchUsersDetail';

import CollapsibleFilter from '../CollapsibleFilter'
import * as feedActions from '../../actions/feed/feedActions'
import * as assetsActions from '../../actions/assets/assetsActions'
import * as campaignsActions from '../../actions/campaigns/campaignsActions'
var thumbnail_url;
let flag = false;
var allAssetdata = '';
var forstateasset = '';
var loopvarforutilpeassets = false
var loopvarforutilpepost = false
var unapprovedPost = false
var unapprovedAsset = false
var postApproved = false
var assetApproved = false
var uniqueArray = [];
var allArray = [];
var target = '';
var allcheck = '';
var assetIdentity = '';
var oldpage = null;
var jsonDataPost = [];
var jsonDataAssets = [];
var currStatus = 'pending';
var restrictApi = false;
var displayFeed = false;
var selectedDepartmentData;
var selectedUserData;
class Moderation extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      posts: [],
      assets: [],
      departments: [],
      currAsset: null,
      assetDetailPopup: false,
      currListingType: 'posts',
      currListingTypeStatus: 'pending',
      currPage: 1,
      currDept: null,
      accordionIndex: null,
      limit: 10,
      meta: {},
      moment: null,
      scrollTimer: null,
      collapsedOpenKeys: [],
      noData: '',
      filterBy: 'new',
      overlay_flag: false,
      show_menu: true,
      handleScriptLoad: false,
      feedback_popup: false,
      assetid: null,
      post_id: null,
      post_status: '',
      isfeedbacksent: 1,
      filter_droupdown_feed: true,
      assetApprovedUnapproved : false,
      assetApprovedUnapprovedArray : [],
      buttonDisableIfNotChecked : false,
      test:'',
      multipleAssetsCheck : false,
      multiplePostchecked : false,
      dataforpostandassets : false,
      newpost : [],
      currStatus:'',
      postsdepartmentId:'',
      postscampaignId:'',
      postsfeedId:'',
      postsuserType:'',
      assetsfeedId:'',
      assetsdepartmentId:'',
      assetscampaignId:'',
      assetsuserType:'',
      noPosts:false,
      noAssets : false,
      PostLoader:false,
      AssetLoader:false
     // multipleCheckbox : false
    }

    this.onPostsScrollModeration = this.onPostsScrollModeration.bind(this);
    this._handleCloseEvent = this._handleCloseEvent.bind(this);
    //this.multipleAssetsApproveUnapprove = this.multipleAssetsApproveUnapprove.bind(this)
  }

  componentDidMount() {

    unapprovedPost = false;
    unapprovedAsset = false;
    this.props.feedActions.getFeedList(true);
    this.props.headerActions.setFilterFlag(true);
    if(restrictApi !==true){
      this.props.campaignsActions.fetchCampaigns('live')
      this.props.assetsActions.fetchCatCustomFeedFolders()
      this.props.assetsActions.fetchCatCampaignFolders()
      restrictApi = true;
      setTimeout(() => {
        restrictApi = false;
      }, 5000)
    }
    //filter disply only in post,not assets
    let location_filter = utils
      .removeTrailingSlash(this.props.location.pathname)
      .split('/')
    if (location_filter[2] == "assets") {
      this.setState({
        filter_droupdown_feed: 'false'
      });
    }
    //end filter hide show coding


    let location = utils
      .removeTrailingSlash(this.props.location.pathname)
      .split('/')

    currStatus = location[3] !== undefined ? location[3] : 'pending'
    // get departments
    var roles = JSON.parse(localStorage.getItem('roles'))
    if (roles !== undefined && roles[0].role_name !== "guest") {

      switch (location[2]) {
        case 'posts':
          this.defaultFetching(location)
          break
        case 'assets':
          this.setState({
            currListingType: 'assets'
          })
          if (location[3] !== '' && typeof (location[3] !== 'undefined')) {
            this.setState({
              currListingTypeStatus: location[3]
            }, () => { this.fetchAssets({ currPage: 1, currListingTypeStatus: location[3] }) })
            // this.fetchAssets({ currPage: 1, currListingTypeStatus: location[3] })
          } else {
            this.setState({
              currListingTypeStatus: location[3]
            })
            this.fetchAssets({ currPage: 1, currListingTypeStatus: 'pending' })
          }
          break
        default:
          // this.defaultFetching(location)
          break
      }
    }
    else if (roles[0].role_name == "guest") {
      this.setState({
        // assets:'no assets',
        // posts:'No post found'
        noAssets: true,
        noPosts: true
      })
    }


    ReactDOM.findDOMNode(this).addEventListener('click', this._handleCloseEvent);
  }
  _handleCloseEvent(e) {
    if (!document.getElementById('pop-up-tooltip-wrapper').contains(e.target)) {
      if (this.state.overlay_flag) {
        document.getElementById('pop-up-tooltip-wrapper').className = 'hide';
        this.setState({ overlay_flag: false })
      }
    }
  }
  handleTourStart() {
    browserHistory.push('/feed/external/live/start')
  }
  defaultFetching(location) {
    // the below code can be reusable when filtering departments
    //
    //
    // if(typeof(location[4])!=="undefined" && location[4]!="") {
    //   this.setState({
    //     currDept: location[4],
    //     currPage: 1,
    //     posts: [],
    //     currListingType: 'posts'
    //   })
    //   // fetch particular dept
    //   this.fetchPosts({ currPage: 1,  dept:location[4]})
    // }
    // else {
    // fetch all

    this.setState({
      currPage: 1,
      posts: [],
      currListingType: 'posts'
    })
    if (typeof location[3] !== 'undefined' && location[3] != '') {
      this.setState({
        currListingTypeStatus: location[3]
      }, () => { this.fetchPosts({ currPage: 1, currListingTypeStatus: location[3] }) })
    } else {
      this.fetchPosts({ currPage: 1 });
    }
    // }
  }


  onPostsScrollModeration() {
    var me = this;
    if (utils.getDocHeight() == utils.getScrollXY()[1] + window.innerHeight &&  me.state.meta.pagination.current_page < me.state.meta.pagination.total_pages ) {
      // a small debouncer for better performance
      clearTimeout(me.state.scrollTimer)
      let scrollTimer = setTimeout(function () {
        switch (me.state.currListingType) {
          case 'posts':
            if (typeof me.state.meta.pagination !== 'undefined') {
              var nextpage = me.state.meta.pagination.current_page + 1
              me.setState({
                currPage: nextpage
              })
              me.fetchPosts({
                currPage: nextpage,
                currListingTypeStatus: me.state.currListingTypeStatus,
                dept: me.state.currDept
              })
            }
            break
          case 'assets':
            if (typeof me.state.meta.pagination !== 'undefined') {
              var nextpage = me.state.meta.pagination.current_page + 1
              me.setState({
                currPage: nextpage
              })
              me.fetchAssets({ currPage: nextpage, currListingTypeStatus: me.state.currListingTypeStatus, })
            }
            break
        }
      }, 300)

      me.setState({
        scrollTimer: scrollTimer
      })
    }
  }

  componentWillMount() {
    window.addEventListener('scroll', this.onPostsScrollModeration)

    $().then(jquery => {
      var $ = jquery.$

      ChunkJqueryDropdown().then(jqueryDropdown => { })
    })

    moment().then(moment => {
      reactTooltip().then(reactTooltip => {
        this.setState({
          moment: moment,tooltip: reactTooltip 
        })
      })
    })
    let location = utils
      .removeTrailingSlash(this.props.location.pathname)
      .split('/')
    if (location[1] == 'moderation' && location[2] == undefined) {
      browserHistory.push('/moderation/posts')
    }
  }

  componentWillUnmount() {
    unapprovedPost = false;
    uniqueArray = [];
    allArray = [];
    unapprovedAsset = false;
    loopvarforutilpepost = false;
    this.setState({
      assetApprovedUnapprovedArray: []
    })
    if (this.state.noAssets !== true) {
      this.state.assets.map((assetDetail) => {
        this.setState({
          [assetDetail.identity]: false,
          multipleAssetsCheck: false
        })
      })
    }
    window.removeEventListener('scroll', this.onPostsScrollModeration);
    ReactDOM.findDOMNode(this).removeEventListener('click', this._handleCloseEvent);
  }

  componentWillReceiveProps(newProps) {
    if (this.props.location.pathname == "/moderation/assets/unapproved" || this.props.location.pathname == "/moderation/posts/unapproved") {
      var approveBtn = document.getElementsByClassName('approval-box');
      if (approveBtn.length > 0) {
        var i;
        for (i = 0; i < approveBtn.length; i++) {
          approveBtn[i].classList.add('approved-col-8')
        }
      }
    }

    if (this.props !== newProps) {
      if (this.props.moderation.unapprovedflag == true) {
        unapprovedAsset = false;
        unapprovedPost = false;
        if (this.state.dataforpostandassets) {
          uniqueArray = [];
          allArray = [];
          this.state.assetdata.map((assetDetail) => {
            this.setState({
              [assetDetail.identity]: false,
              multipleAssetsCheck: false,
              assetApprovedUnapprovedArray: []
            })
          })

          this.state.postdata.map((post) => {
            this.setState({
              [post.post.data.identity]: false,
              multiplePostchecked: false,
              assetApprovedUnapprovedArray: []
            })
          })
        }
        this.
          props.
          moderationActions.
          handleUnapprovedFlag();
      }
    }
    var newLocation = utils
      .removeTrailingSlash(newProps.location.pathname)
      .split('/')
    if (newProps.location.pathname !== this.props.location.pathname) {
      this.setState({
        currListingType: newLocation[2],
        currListingTypeStatus: newLocation[3],
        currPage: 1,
        posts: [],
        assets: []
      })


      if (newProps.profile.profile.role !== undefined && newProps.profile.profile.role.data[0].role_name !== "guest") {
        if (newLocation[3] !== '') {
          if (newLocation[2] == 'posts') {
            this.fetchPosts({
              currPage: 1,
              currListingTypeStatus: newLocation[3],
            })
          } else if (newLocation[2] == 'assets') {
            this.fetchAssets({
              currPage: 1,
              currListingTypeStatus: newLocation[3]

            })
          }
        }
      }
      else if (newProps.profile.profile.role !== undefined && newProps.profile.profile.role.data[0].role_name == "guest") {
        this.setState({
          // assets:'no assets',
          // posts:'No post found'
          noAssets: true,
          noPosts: true
        })
      }


      if (this.state.dataforpostandassets) {
        uniqueArray = [];
        allArray = [];
        this.state.assetdata.map((assetDetail) => {
          this.setState({
            [assetDetail.identity]: false,
            multipleAssetsCheck: false,
            assetApprovedUnapprovedArray: []
          })
        })

        this.state.postdata.map((post) => {
          this.setState({
            [post.post.data.identity]: false,
            multiplePostchecked: false,
            assetApprovedUnapprovedArray: []
          })
        })
      }

      this.setState({
        postdata: '',
        assetdata: '',
        dataforpostandassets: false
      })

    }
    if (newProps.moderation.newdata !== this.props.moderation.newdata && newProps.moderation.newdata === true) {
      if (newLocation[3] !== '' && typeof (newLocation[3] !== 'undefined')) {
        if (newLocation[2] == 'posts') {
          if (typeof newLocation[3] !== 'undefined' && newLocation[3] != '') {
            this.fetchPosts({ currPage: 1, currListingTypeStatus: newLocation[3] })
          } else {
            this.fetchPosts({ currPage: 1 });
          }
        } else if (newLocation[2] == 'assets') {
          if (typeof newLocation[3] !== 'undefined' && newLocation[3] != '') {

            this.fetchAssets({ currPage: 1, currListingTypeStatus: location[3] })
          } else {
            this.fetchAssets({ currPage: 1, currListingTypeStatus: 'pending' })
          }
        }

      }
      this.closeAssetDetailPopup();


    }
  }

  /**
   *@param : posts
   */
  appendPostForSelectAllCheckbox(posts) {

    jsonDataPost = [];
    jsonDataPost = posts;
    if (this.state.currPage > 1 && oldpage !== this.state.currPage) {
      if (this.state.multiplePostchecked == true) {
        if (jsonDataPost !== null) {
          jsonDataPost.map((post) => {
            this.setState({
              multiplePostchecked: true,
              [post.post.data.identity]: true,
            })
          })
        } else {
          this.state.jsonDataPost.map((post) => {
            //when selectall check is false then set the value of all the post is false
            this.setState({
              multiplePostchecked: false,
              [post.post.data.identity]: false,
            })
          })
        }
      }
    }
    oldpage = null;
    oldpage = this.state.currPage;
    this.addValueInArray();
  }
  fetchPosts({ currPage , dept = null, currListingTypeStatus = 'pending', filterBy = this.state.filterBy, funcName = null }) {
    jsonDataPost = [];
    this.props.moderationActions.successFetchAssets()
    this.resetMetaData()
    if(currPage ==1){
    this.setState({
      PostLoader:true
    })
  }
    var dep = dept !== null ? `&department_identity=${dept}` : this.state.postsdepartmentId !== ''?`&department_identity=${this.state.postsdepartmentId}`:``
    var feedid=this.state.postsfeedId !=='' ?`&feed_identity=${this.state.postsfeedId}`:``
    var campid=this.state.postscampaignId !=='' ?`&campaign_identity=${this.state.postscampaignId}`:``
    var usertype=this.state.postsuserType !=='' ?`&user_type=${this.state.postsuserType}`:``
    fetch(
      Globals.API_ROOT_URL +
      `/post/pending?limit=${this.state.limit}&page=${currPage}${dep}&approval_status=${currListingTypeStatus}&order_by=${filterBy}${feedid}${campid}${usertype}`, {
        headers: utils.setApiHeaders('get')
      })
      .then(response => response.json())
      .then(json => {
        jsonDataPost.push(json.data);
        if (Array.isArray(json.data) && json.data.length > 0) {
          this.setState({
            PostLoader:false,
            noPosts : false
          })
         
          if(json.meta.pagination.current_page < json.meta.pagination.total_pages){
            this.setState({
              loadMorePost :true
            })
          }else{
            this.setState({
              loadMorePost : false
            })
          }
          this.appendPostsInState(json.data)
          this.setMetaData(json.meta)
          this.appendPostForSelectAllCheckbox(json.data)
          this.props.moderationActions.CallFetchApiPostAssets()
        }
        else {
          this.setState({
            PostLoader:false,
            noPosts : true,
            loadMorePost :false
          })
          if (json.message !== undefined) {
            this.appendPostsInState(json.message)
          } else {
            utils.handleSessionError(json)
            this.appendPostsInState(json.error)
            utils.handleSessionError(json)
          }
        }
      })
      .catch(err => {
        notie.alert('error', err, 3)
        throw err
      })
  }
  renderFeedbackPopup() {

    return (
      <div>
        {this.state.isfeedbacksent == 0 ?
          <SliderPopup>
            <button
              id='close-popup'
              className='btn-default'
              onClick={this.closeAssetDetailPopup.bind(this)}
            > <i className='material-icons'>clear</i></button>
            {document.body.classList.add('overlay')}
            <DeclineFeedback actiontype={this.state.currListingType} unapprovedAsset={unapprovedAsset} assetid={this.state.assetid} closepopup={this.closeAssetDetailPopup.bind(this)} unapprovedPost={unapprovedPost} postid={this.state.post_id} postStatus={this.state.post_status} isfeedbacksent={this.state.isfeedbacksent} />
          </SliderPopup> :
          <div>

            <DeclineFeedback actiontype={this.state.currListingType} unapprovedAsset={unapprovedAsset} assetid={this.state.assetid} closepopup={this.closeAssetDetailPopup.bind(this)} unapprovedPost={unapprovedPost} postid={this.state.post_id} postStatus={this.state.post_status} isfeedbacksent={this.state.isfeedbacksent} />
          </div>

        }
      </div>
    );
  }
  /**
   *
   * @param {*} id  : id is store the multilple check post identity
   * when we click on unapproved post then this method is call
   * this method call if we click on top side unapproved button
   */
  unapprovedAsset(id) {
    if (id.length == 0) {
      notie.alert("warning", "Please select any asset to continue", 3);
    }
    //when API calling this state is true then api call ,so this state is defined
    unapprovedAsset = true
    this.forUniqueArrayAllPostAssets(status = null, id)
  }


  /**
   *
   * @param {*} id  : for single id store
   */
  handleUnapproveFeeback(id) {
    if (this.state.isfeedbacksent === 1) {
      var objToSend = {
        is_feedback_sent: 1,
        asset_identity: unapprovedAsset ? id : [id],
        approved_status: 'unapprove',
        feedback: '',
        save_as_feedback: 0
      }
      this.props.moderationActions.sendUnapprovedFeedback(objToSend)
    } else {
      this.setState({
        feedback_popup: true,
        assetid: id
      });
    }

  }


  /**
   *
   * @param {*} status : status is approved or reject post
   * @param {*} allArray : check post and asset identity
   * this method call if we remove the duplicate the value in array
   */
  forUniqueArrayAllPostAssets(status, allArray) {
    uniqueArray = [];                                      //temp varible
    for (let i = 0; i < allArray.length; i++) {
      if (uniqueArray.indexOf(allArray[i]) == -1) {
        uniqueArray.push(allArray[i])
      }
    }
    if (uniqueArray.length === 0) {
      return false
    } else {
      if (unapprovedPost == true) {
        this.handleUnapproveFeebackPost(status, uniqueArray);
      } else if (postApproved == true) {
        this.moderatePost(status, uniqueArray);
      } else if (assetApproved == true) {
        this.moderateAsset(status, uniqueArray)
      } else if (unapprovedAsset == true) {
        this.handleUnapproveFeeback(uniqueArray)
      } else {
      }
    }
    //uniqueArray give the uniquevalue and remove the duplicate array

    //when we click on,
    //unapprovedPost then handleUnapproveFeebackPost method is call
    //postApproved then  moderatePost method is call
    //assetApproved then call the moderateAsset
    //unapprovedAsset then call the handleUnapproveFeeback



  }

  /**
   * @param {*} status : status is approved or reject post
   * @param {*} allArray : check post and asset identity
   */
  postUnapproved(status, post) {
    if (post.length == 0) {
      notie.alert("warning", "Please select any post to continue", 3);
    }
    unapprovedPost = true
    this.forUniqueArrayAllPostAssets(status, post)
  }

  /**
   *
   * @param {*} status
   * @param {*} post
   * if we click on single post unapproved button then call this method
   */
  handleUnapproveFeebackPost(status, post) {

    //if send feedback is not selected then directly call api otherwise open popup
    if (this.state.isfeedbacksent === 1) {
      var objToSend = {
        is_feedback_sent: 1,
        post_identity: unapprovedPost == true ? post : [post.post.data.identity],
        approved_status: 'rejected',
        feedback: '',
        save_as_feedback: 0
      }
      this.props.moderationActions.sendUnapprovedFeedbackPost(objToSend)
    } else {

      //if mulitple post select then pass into multiple psot
      if (unapprovedPost == true) {
        this.setState({
          feedback_popup: true,
          post_status: status,
          post_id: post

        });
      } else {
        //for single post
        this.setState({
          feedback_popup: true,
          post_status: status,
          post_id: post.post.data.identity
        });
      }
    }




  }
  appendAssetsForSelectAllCheckbox(assets) {
    jsonDataAssets = [];
    jsonDataAssets = assets;
    if (this.state.currPage > 1 && oldpage !== this.state.currPage) {
      if (this.state.multipleAssetsCheck == true) {
        if (jsonDataAssets !== null) {
          jsonDataAssets.map((assetDetail) => {
            this.setState({
              multipleAssetsCheck: true,
              [assetDetail.identity]: true,
            })
          })
        } else {
          jsonDataAssets.map((assetDetail) => {
            this.setState({
              multipleAssetsCheck: false,
              [assetDetail.identity]: false,
            })
          })
        }
      }
    }
    oldpage = null;
    oldpage = this.state.currPage;
    this.addValueInArray();
  }
  fetchLatestPosts() {
    // ... ajax jquery -> fetch
    // get the response
    // set the response in component's state
    // this.setState({
    //   posts: response
    // })
    // when the setState method is called, the render() method will be called again.
    // use the state in the render method and display the results.
    // this.state.posts.map(....)
  }

  fetchAssets({ currPage, currListingTypeStatus = 'pending', filterBy = this.state.filterBy }) {
    this.props.moderationActions.successFetchAssets()

    this.resetMetaData()

    if(currPage==1)
    {
      this.setState({
        currListingTypeStatus: currListingTypeStatus,
        AssetLoader:true
      })
    }
   
    this.props.moderation.moderation.closeAllPopups = false
    var dep = this.state.assetsdepartmentId !== '' ? `&department_identity=${this.state.assetsdepartmentId}` : ``
    var folderid = this.state.assetsfeedId !== '' ? `&folder_identity=${this.state.assetsfeedId}` : ``
    var campid = this.state.assetscampaignId !== '' ? `&campaign_identity=${this.state.assetscampaignId}` : ``
    var usertype = this.state.assetsuserType !== '' ? `&user_type=${this.state.assetsuserType}` : ``

    fetch(
      Globals.API_ROOT_URL +
      `/asset/pending?limit=${this.state.limit}&page=${currPage}&approval_status=${currListingTypeStatus}&order_by=${filterBy}${dep}${folderid}${campid}${usertype}`, {
        headers: utils.setApiHeaders('get')
      })
      .then(response =>
        response.json()
      )
      .then(json => {
        if (Array.isArray(json.data) && json.data.length > 0) {
          this.setState({
            noAssets : false,
            AssetLoader:false,
            })
            if(json.meta.pagination.current_page < json.meta.pagination.total_pages){
              this.setState({
                loadMoreAsset :true
              })
            }else{
              this.setState({
                loadMoreAsset : false
              })
            }
          this.appendAssetsInState(json.data)
          this.setMetaData(json.meta)
          this.props.moderationActions.CallFetchApiPostAssets()
          this.appendAssetsForSelectAllCheckbox(json.data)
          // this.noDataInState(json.data)
        } else {
          this.setState({
            AssetLoader:false,
            noAssets:true,
            loadMoreAsset : false
          })
          utils.handleSessionError(json)
          if (json.error !== undefined) {
            this.appendAssetsInState(json.error)
          } else {
            this.appendAssetsInState(json.data.message)
          }
        }
      })
      .catch(err => {
        notie.alert('error', err, 3)
        throw err
      })
  }
  /**
    * @param {*} status : status is approved or reject post
    * @param {*} post : post identity
    */

  postApproved(status, post) {
    if (post.length == 0) {
      notie.alert("warning", "Please select any post to continue", 3);
    }
    postApproved = true;
    this.forUniqueArrayAllPostAssets(status, post)
  }
  /**
   *   for single post
   */

  moderatePost(status, post) {
    let location = utils
      .removeTrailingSlash(this.props.location.pathname)
      .split('/')
    var me = this
    //if check the mulitple psot then pass the array in to the post
    if (postApproved == true) {
      var payload = {
        post_identity: post,
        approved_status: status
      }
    } else {
      var payload = {
        post_identity: [post.post.data.identity],
        approved_status: status
      }
    }

    this.props.moderationActions.approvePostInit();
    fetch(
      Globals.API_ROOT_URL +
      `/post/approve`,
      {
        method: 'POST',
        headers: utils.setApiHeaders('post'),
        body: JSON.stringify(payload)
      }
    )
      .then(response => {
        return response.json()
      })
      .then(json => {
        if (parseInt(json.code) == 200) {
          notie.alert('success', json.message, 3)
          me.state.posts.map((post) => {
            uniqueArray = [];
            allArray = [];
            me.setState({
              [post.post.data.identity]: false,
              multiplePostchecked: false,
              assetApprovedUnapprovedArray: []
            })

          })
          me.setState({
            currPage: 1,
            posts: [],
            multiplePostchecked: false
          })
          me.fetchPosts({
            currPage: me.state.currPage,
            dept: me.state.currDept,
            currListingTypeStatus: location[3]
          })
        } else {
          utils.handleSessionError(json)
          notie.alert(
            'error',
            `There was a problem moderating the post. Error code: ${json.message}`,
            5
          )
        }
      })
      .catch(err => {
        notie.alert('error', 'Problem when moderating post', 3)
        throw err
      })
    postApproved = false;
  }

  /**
  * @param {*} status : status is approved or reject post
  * @param {*} asset :  asset identity
  */
  approvedAssets(status, asset) {
    if (asset.length == 0) {
      notie.alert("warning", "Please select any asset to continue", 3);
    }
    assetApproved = true
    this.forUniqueArrayAllPostAssets(status, asset)
  }

  /**
   *
   * @param {*} status
   * @param {*} asset
   * for single assets
   */
  moderateAsset(status, asset) {
    let location = utils
      .removeTrailingSlash(this.props.location.pathname)
      .split('/')
    var me = this

    if (assetApproved == true) {
      var payload = {
        asset_identity: asset,
        approved_status: status
      }
    } else {
      var payload = {
        asset_identity: [asset.identity],
        approved_status: status
      }
    }


    this.props.moderationActions.approveAssetInit()
    fetch(
      Globals.API_ROOT_URL +
      `/asset/moderation`,
      {
        method: 'POST',
        headers: utils.setApiHeaders('post'),
        body: JSON.stringify(payload)
      }
    )
      .then(response => {
        return response.json()

      })

      .then(json => {
        if (parseInt(json.code) == 200) {

          notie.alert('success', json.message, 3)
          me.state.assets.map((assetDetail) => {
            uniqueArray = [];
            allArray = [];
            me.setState({
              [assetDetail.identity]: false,
              multipleAssetsCheck: false,
              assetApprovedUnapprovedArray: []
            })
          })
          me.setState({
            currPage: 1,
            assets: [],
            multipleAssetsCheck: false
          })

          this.props.moderationActions.approveAssetSuccess()
          me.fetchAssets({ currPage: 1, currListingTypeStatus: location[3] })
        } else {
          utils.handleSessionError(json)
          notie.alert(
            'error',
            `There was a problem while moderating the asset. Error code: ${json.message}`,
            5
          )
        }
      })
      .catch(err => {

        notie.alert('error', 'There was a problem while moderating asset', 3)
        throw err
      })
    assetApproved = false;
  }

  /**
   * @author disha
   * @use : append data from post api
   * @param {*} posts
   */
  appendPostsInState(posts) {
    if (this.props.moderation.newdata === true) {
      this.setState({
        posts: posts,
      })
    }
    else {
      if (posts !== 'No post found') {
        //call when post is scrolling and append new data in list
        if (this.state.currPage !== 1) {
          posts = this.state.posts.concat(posts)
        }
        this.setState({
          posts: posts,
        })
      }
      else if (this.state.posts.length == 0 || (posts == 'No post found' && this.state.currPage == 1)) {
        //call when post has no data
        this.setState({
          // posts: 'No post found'
          noPosts: true
        })
      }else if(posts == 'No post found' && this.state.currPage>1){
        this.setState({
          // posts: 'No post found'
          noPosts :false
        }) 
      }
    }
  }
  /**
     * @author disha
     * @use : append data from asset api
     * @param {*} assets
     */
  appendAssetsInState(assets) {
    if (this.props.moderation.newdata === true) {
      this.setState({
        assets: assets
      })
    } else {
      if (assets !== "no assets") {
        // call when asset is scrolling and append new data in list
        if (this.state.currPage !== 1) {
          assets = this.state.assets.concat(assets)
        }

        this.setState({
          assets: assets,
        })
      }
      else if (this.state.assets.length == 0 || (assets == 'no assets' && this.state.currPage == 1)) {
        //call when asset has no data
        this.setState({
          // assets: 'no assets'
          noAssets: true
        })
      }else if(assets == 'no assets' && this.state.currPage>1){
        this.setState({
          // posts: 'No post found'
          noAssets :false
        }) 
      }

    }
  }
  noDataInState(nodata) {
    this.setState({
      noData: nodata.message
    })
  }

  setMetaData(meta) {
    this.setState({
      meta: meta
    })
  }

  resetMetaData() {
    this.setState({
      meta: []
    })
  }

  renderDepartmentsInSidebar() {
    return (
      <ul className='parent  clearfix'>

        <li>
          <Link
            onClick={this.sidebarLinkClicked.bind(this, '1')}
            to={`/moderation/posts/`}
          >

            <span className='text'>All</span>
          </Link>
        </li>
        <li>
          <Link
            onClick={this.sidebarLinkClicked.bind(this, '1')}
            to={`/moderation/posts/`}
          >
            <span className='text'>All</span>
          </Link>
        </li>
        {
          // this.state.departments.map((department, index) => {
          //   return (
          //     <li key={index}>
          //       <Link onClick={this.sidebarLinkClicked.bind(this, "1")} to={`/moderation/posts/dept/${department.identity}`}><span className="text">{department.name}</span></Link>
          //     </li>
          //   )
          // })
        }
      </ul>
    )
  }

  renderSocialChannelsSharedOn(socialAccounts, post) {

    if (socialAccounts.length > 0) {
      return (
        <span className='socialIcons'>
          <span> via</span>
          {socialAccounts.indexOf('facebook') !== -1
            ? <a className='fb'>
              <i className='fa fa-facebook' aria-hidden='true' />
            </a>
            : null}
          {socialAccounts.indexOf('twitter') !== -1
            ? <a className='tw'>
              <i className='fa fa-twitter' aria-hidden='true' />
            </a>
            : null}
          {socialAccounts.indexOf('instagram') !== -1
            ? <a className='in'>
              <i className='fa fa-instagram' aria-hidden='true' />
            </a>
            : null}
          {socialAccounts.indexOf('linkedin') !== -1
            ? <a className='li'>
              <i className='fa fa-linkedin' aria-hidden='true' />
            </a>
            : null}
        </span>
      )
    }

    else {
      return <span />
    }
  }
  updateText(id, type, e) {
    this.setState({ overlay_flag: true });
    var isUserFound = false
    if (type == "department") {
      this.props.departments.list.length > 0 ?
        this.props.departments.list.map((DepartmentDetail, i) => {
          if (DepartmentDetail.identity == id) {
            selectedDepartmentData = DepartmentDetail;
          }
        }) : ''
    }
    else {
      this.props.usersList.length > 0 ?
        this.props.usersList.map((userDetail, i) => {
          if (userDetail.identity == id) {
            selectedUserData = userDetail;
            isUserFound = true;
          }
        }) : ''
      if (isUserFound == false) {
        selectedUserData = "No user found"
      }
    }
    const domNode = ReactDOM.findDOMNode(e.target);
    let coords = domNode.getBoundingClientRect();
    let coords_top = coords.top + pageYOffset;
    let popup_wrapper = document.getElementById("pop-up-tooltip-wrapper");
    let coords_left = 0,
      coords_area = 0,
      new_class_comb = '';
    let pop_holder = document.getElementById("pop-up-tooltip-holder");
    let popupparent = document.getElementById('department-popup');
    if (popupparent != null) {
      pop_holder.className = 'departmentpopup';
    } else {
      pop_holder.className = '';
    }
    if (screen.width >= 768) {
      coords_left = coords.left + pageXOffset + (coords.width / 2);
    }
    else {
      coords_left = coords.left + pageXOffset;
      coords_area = coords_left + 472;
      if (coords_area < screen.width) {
        new_class_comb = 'popup-left';
      }
      else {
        new_class_comb = 'popup-right';
      }

    }
    if (e.nativeEvent.screenY < 400) {
      new_class_comb += ' bottom';
    }
    else {
      new_class_comb += ' top';
    }
    popup_wrapper.className = new_class_comb;
    this.setState({
      popupTop: coords_top,
      popupLeft: coords_left,
      popupdisplay: 'block',
      tagType: type,
      tagTypeClass: (type === "department" ? 'departmentpopup' : '')
    });
    let pop_overlay = document.getElementById("tag-popup-overlay");
    if (pop_overlay) {
      pop_overlay.className = 'show';
    }

  }
  assetClick(assetDetail) {
    this.setState({
      assetDetailPopup: true,
      currAsset: assetDetail
    })
    document.body.classList.add('overlay')
  }

  closeAssetDetailPopup() {
    var newLocation = utils
      .removeTrailingSlash(this.props.location.pathname)
      .split('/');
    var me = this
    me.setState({
      assetDetailPopup: false,
      feedback_popup: false
    })
    document.body.classList.remove('overlay')
    if (this.props.moderation.closeAllPopups == true) {
      if (newLocation[3] !== '' && typeof (newLocation[3] !== 'undefined')) {
        if (newLocation[2] == 'posts') {
          if (typeof newLocation[3] !== 'undefined' && newLocation[3] != '') {
            this.setState({
              currListingTypeStatus: newLocation[3]
            })
          } else {
            //this.fetchPosts({ currPage: 1 });
          }
        } else if (newLocation[2] == 'assets') {
          if (typeof newLocation[3] !== 'undefined' && newLocation[3] != '') {
            this.setState({
              currListingTypeStatus: newLocation[3]
            })
            // this.fetchAssets({ currPage: 1, currListingTypeStatus: location[3] })
          } else {
            this.setState({
              currListingTypeStatus: newLocation[3]
            })
            //this.fetchAssets({ currPage: 1, currListingTypeStatus: 'pending' })
          }
        }
      }
    }
  }

  renderAssetDetail(asset) {
    var assetDetail = {
      identity: 1,
      title: '',
      media_extension: '',
      media_type: 'application',
      media_url: '',
      thumbnail_url: '',
      approved: false,
      created_date: '',
      assetAuthor: '',
      tags: [],
      detail: '',
      userTag: [],
      firstname: '',
      lastname: '',
    }

    if (asset !== undefined) {

      assetDetail = {
        identity: asset.identity,
        title: asset.title,
        media_type: asset.media_type,
        media_url: asset.media_url,
        thumbnail_url: asset.thumbnail_url,
        media_extension: asset.media_extension,
        approved: asset.approved,
        created_date: asset.created_date,
        assetAuthor: asset.assetAuthor,
        tags: asset.tags,
        detail: asset.detail,
        userTag: asset.userTag,
        firstname: asset.firstname,
        lastname: asset.lastname,
      }
      return (
        <SliderPopup wide className="asset-detail-popup">
          <button
            id='close-popup'
            className='btn-default'
            onClick={this.closeAssetDetailPopup.bind(this)}
          >
            <i className='material-icons'>clear</i>
          </button>
          <AssetDetail moment={this.state.moment} asset={assetDetail} updateText={this.updateText.bind(this)} />
        </SliderPopup>
      )
    }
    else {
      return (
        <div>
          <h1>There is no media</h1>
        </div>
      );
    }

  }

  renderAssetTags(asset) {
    return <AssetTags asset={asset} />
  }
  varibledecration(assetApprovedUnapprovedchecked, event) {
    target = assetApprovedUnapprovedchecked.target;
    allcheck = target.id
    assetIdentity = target.id.split("-")[1];

    this.setState({
      postdata: this.state.posts,
      assetdata: this.state.assets,
      dataforpostandassets: true,
    })

  }
  singleAssetsCheck(assetApprovedUnapprovedchecked, event) {

    this.varibledecration(assetApprovedUnapprovedchecked, event)
    this.state.assets.map((assetDetail) => {
      if (this[`check-${assetDetail.identity}`] !== null && this[`check-${assetDetail.identity}`] !== undefined) {
        this.setState({
          [assetDetail.identity]: this[`check-${assetDetail.identity}`].checked
        })
      }
    })
    this.addValueInArray()
  }

  /**
   * @argument:assetApprovedUnapprovedchecked ,event
   * @author : kinjal Birare
   * singlePostCheck method is call when we click on single post check
   */
  singlePostCheck(assetApprovedUnapprovedchecked, event) {

    //varibledecration method decration all the varible,
    //this method pass the two arugument
    this.varibledecration(assetApprovedUnapprovedchecked, event)

    //this map.loop all the post,and set the value in true or false
    this.state.posts.map((post) => {
      if (this[`check-${post.post.data.identity}`] !== null && this[`check-${post.post.data.identity}`] !== undefined) {
        this.setState({
          [post.post.data.identity]: this[`check-${post.post.data.identity}`].checked
        })
      }
    })

    //if value is set then all the click post identity pass into the array,
    //so this method call
    this.addValueInArray()
  }

  /**
   *  @argument : null
   *  @author : kinjal Birare
   *  addValueInArray : when we check the multiple post ,the all the check post identity pass into the array
   */
  addValueInArray() {

    //allArray is used only this method for temporary post identity store
    allArray = []

    // if all the identity store into the allArray varible then pass into the this state
    this.setState({
      assetApprovedUnapprovedArray: []
    })

    allArray = this.state.assetApprovedUnapprovedArray;
    if (this.props.location.pathname == "/moderation/posts" || this.props.location.pathname == "/moderation/posts/unapproved" || this.props.location.pathname == "/moderation/posts/") {
      if (allcheck == "selctedall-post") {
        allArray = [];
        this.state.posts.map((post) => {
          if (this.selectedallpost !== null && this.selectedallpost !== undefined && this.selectedallpost.checked) {
            //this.selectedallpost.checked value of selectall checkbox ,true or false
            if (this.selectedallpost.checked) {
              //if they true then push all the id
              allArray.push(post.post.data.identity);
            } else {
              //if they false then remove all the od
              allArray.splice(allArray.indexOf(post.post.data.identity), 1)
              // allArray.splice(allArray.indexOf(post.post.data.identity),1)
            }
          }
        })
      } else {
        //if particuler one post check - unchecked
        //check the value if true then push into the array
        allArray = [];
        this.state.posts.map((post) => {
          if (this[`check-${post.post.data.identity}`] !== null && this[`check-${post.post.data.identity}`] !== undefined && this[`check-${post.post.data.identity}`].checked) {

            allArray.push(post.post.data.identity);
            this.setState({
              buttonDisableIfNotChecked: true
            })

          } else {
            //if value is false ,then mao the allArray value
            allArray.map((allPostIdentity) => {
              if (allPostIdentity == post.post.data.identity) {
                //delete the value if the check is false
                allArray.splice(allArray.indexOf(allPostIdentity), 1)
                this.setState({
                  buttonDisableIfNotChecked: false
                })
              }
            })
          }
        })
      }

    } else {
      //same as post is check the ref id of check
      //for select all
      if (allcheck == "selectedall-asset") {
        this.state.assets.map((assetDetail) => {
          if (this.selectedallasset.checked) {
            allArray.push(assetDetail.identity);
          } else {
            allArray.splice(allArray.indexOf(assetDetail.identity), 1)
          }
        })
      } else {
        allArray = [];
        this.state.assets.map((assetDetail) => {
          if (this[`check-${assetDetail.identity}`] !== undefined && this[`check-${assetDetail.identity}`] !== null && this[`check-${assetDetail.identity}`].checked) {
            allArray.push(assetDetail.identity);
            this.setState({
              buttonDisableIfNotChecked: true
            })
          } else {
            allArray.map((allAssetIdentity) => {
              if (allAssetIdentity == assetDetail.identity) {
                allArray.splice(allArray.indexOf(assetDetail.identity), 1)
                this.setState({
                  buttonDisableIfNotChecked: false
                })
              }
            })
          }
        })
      }

    }



    if (allArray.length > 0) {
      displayFeed = true
    } else {
      displayFeed = false
    }

    //push the all the array in the state
    this.setState({
      assetApprovedUnapprovedArray: allArray
    })
  }


  /**
   *
   * @param { } assetApprovedUnapprovedchecked
   * @param {*} event
   * @author: kinjal birare
   * multipleAssetsApproveUnapprove : this method is call when click on select all in post and assets
   */
  multipleAssetsApproveUnapprove(assetApprovedUnapprovedchecked, event) {

    //this method is define the varible ,single post check and select all check ,both call the diffrent method but use the same varible,
    //so this method is define the comman varible
    this.varibledecration(assetApprovedUnapprovedchecked, event)


    this.state.posts.map((post) => {
      //for select all post
      if (allcheck == "selctedall-post") {
        if (this.selectedallpost.checked) {
          this.state.posts.map((post) => {
            //when selectall check is true then set the value of all the post is true
            this.setState({
              multiplePostchecked: true,
              [post.post.data.identity]: true
            })
          })
        } else {
          this.state.posts.map((post) => {
            //when selectall check is false then set the value of all the post is false
            this.setState({
              multiplePostchecked: false,
              [post.post.data.identity]: false,
              //multipleCheckbox : true
            })
          })
        }
      } else {
        //else part ,if I select the all the post then automaticly selectALL check is true,
        //so this are in loop then check value of post,if any value is false then return false
        this.state.posts.map((post) => {
          if (this[`check-${post.post.data.identity}`] !== undefined && this[`check-${post.post.data.identity}`] !== null && this[`check-${post.post.data.identity}`].checked) {
            loopvarforutilpepost = true;
            if (this[`check-${post.post.data.identity}`].checked == false) {
              loopvarforutilpepost = false;
            }
          } else {
            loopvarforutilpepost = false;

          }
        })

        //then loopvarforutilpepost  is true then selectall check true
        if (loopvarforutilpepost == true) {
          this.setState({
            multiplePostchecked: true
          })
        } else {
          this.setState({
            multiplePostchecked: false
          })
        }

      }

    })

    //for assets
    this.state.assets.map((assetDetail) => {

      if (allcheck == "selectedall-asset") {
        if (this.selectedallasset.checked) {
          this.state.assets.map((assetDetail) => {
            //when selectedall-asset check is true then set the value of all the post is true
            this.setState({
              [assetDetail.identity]: true,
              multipleAssetsCheck: true

            })
          })
        } else {
          this.state.assets.map((assetDetail) => {
            //when selectedall-asset check is false then set the value of all the post is false
            this.setState({
              [assetDetail.identity]: false,
              multipleAssetsCheck: false
            })
          })
        }
      } else {
        //else part ,if I select the all the assets then automaticly selectALL check is true,
        //so this is in loop then check value of check,if any value is false then return false
        this.state.assets.map((assetDetail) => {
          if (this[`check-${assetDetail.identity}`] !== null && this[`check-${assetDetail.identity}`] !== undefined && this[`check-${assetDetail.identity}`].checked) {
            loopvarforutilpeassets = true;
            if (this[`check-${assetDetail.identity}`].checked == false) {
              loopvarforutilpeassets = false;
            }
          } else {
            loopvarforutilpeassets = false
          }
        })
        //then loopvarforutilpeassets  is true then selectall check true
        if (loopvarforutilpeassets == true) {
          this.setState({
            multipleAssetsCheck: true
          })
        } else {
          this.setState({
            multipleAssetsCheck: false
          })
        }
      }

    })

    //when we select the any check box then all the post and assets identity pass into the this method
    this.addValueInArray()
  }

  renderAssets() {
    var me = this
    allAssetdata = me.state.assets
    
      if (me.state.noAssets == true || me.state.assets[0]==="User do not have permission for accessing this module") {
        return (
          <div class='no-data-block'> No asset found. </div>
        )
      } else {
        return (
          <div className='moderationAssets'>
            {/* loader */}
            {me.state.AssetLoader==true || this.props.moderation.loading?
            <PreLoader location="moderation-assets" />
            :
            <div>
            {displayFeed == true ?
                <div className="container-button-checkbox clearfix">
                <div className="inner-select-checkbox-assets">
                  <div class="campaign-box"><div class="campaign-header">CAMPAIGN</div><div class="custom-header">CUSTOM</div></div>
                  <div class="selectAllCheckboxForPost">
                    <div class="inner-select-checkbox">
                      <div class="form-row checkbox" title="Select All">
                        <label>
                          <input
                            type="checkbox"
                            id="selectedall-asset"
                            name='check-asset'
                            class="post-check selectall-post"
                            checked={this.state.multipleAssetsCheck}
                            ref={(input) => { this.selectedallasset = input }}
                            onClick={this.multipleAssetsApproveUnapprove.bind(this)}
                          />
                          <span> </span>
                        </label>
                      </div>
                      <div class="header-icon clearfix">
                        <div className="add-icon" title="Approve">
                          <i class="material-icons" onClick={this.approvedAssets.bind(this, 'approve', this.state.assetApprovedUnapprovedArray)}>add_circle_outline</i>
                        </div>
                        {this.props.location.pathname !== "/moderation/assets/unapproved" ?
                          <div className="remove-icon" title="Unapprove">
                            <i class="material-icons clear" onClick={this.unapprovedAsset.bind(this, this.state.assetApprovedUnapprovedArray)}>highlight_off</i>
                          </div> : ''}
                      </div>

                    </div>
                  </div> 
                  </div>
                  </div>
             : ''}
              


            {me.state.assets.map((asset, index) => {
              var status = asset.approved;
              var authorAvatarURL = asset.profileImage
              var authorAvatar = {
                backgroundImage: `url(${typeof authorAvatarURL !== 'undefined' && authorAvatarURL != '' ? authorAvatarURL : '/img/default-avatar.png'})`
              }

              let postTitle = asset.title, postDate = new Date().getTime() * 1000
              let postThumb = '', media_type = ''

              var assetAuthor = asset.firstname + ' ' + asset.lastname

              var media_type_index = asset.media_type.indexOf('/')

              if (media_type_index !== -1) {
                media_type = asset.media_type.substr(0, media_type_index)
              } else {
                media_type = asset.media_type
              }
              var media_url = asset.media_url
              var thumbnail_url = asset.thumbnail_url
              var media_extension = asset.media_extension

              var faIcon = utils.getFaIcon(media_extension)

              let assetDetail = {
                identity: asset.identity,
                title: postTitle,
                detail: asset.detail,
                approved: asset.approved,
                media_type,
                media_url,
                thumbnail_url,
                media_extension,
                assetAuthor,
                created_date: asset.created_date.date,
                tags: typeof asset.tags !== 'undefined' ? asset.tags : [],
                userTag: typeof asset.userTag !== 'undefined' ? asset.userTag : [],
                firstname: asset.firstname,
                lastname: asset.lastname
              }
              var imageStyle = {
                backgroundImage: `url(${typeof thumbnail_url !== 'undefined' && thumbnail_url !== '' ? thumbnail_url : media_url})`
              }

              switch (media_type) {
                case 'image':
                  postThumb = (
                    <div
                      className='thumbWrap file-image type-image'
                      onClick={this.assetClick.bind(this, assetDetail)}
                    >
                      <div className='thumbnail' style={imageStyle} />
                    </div>
                  )
                  break
                case 'video':
                  postThumb = (
                    <div
                      className='thumbWrap file-video type-video'
                      onClick={this.assetClick.bind(this, assetDetail)}
                    >
                      <div className='play-btn-overlay'>
                        <div className='thumbnail' style={imageStyle} />
                        <i className='material-icons'>play_circle_filled</i>
                      </div>
                    </div>
                  )
                  break
                case 'audio':
                  postThumb = (
                    <div
                      onClick={this.assetClick.bind(this, assetDetail)}
                      className={`thumbWrap thmb_wrap_file  file file-doc ${media_extension}`}
                    >
                      <i className='fa fa-file-audio-o fa-3x' />
                    </div>
                  )
                  break
                case 'application':
                  postThumb = (
                    <div
                      onClick={this.assetClick.bind(this, assetDetail)}
                      className={`thumbWrap thmb_wrap_file file file-doc ${media_extension}`}
                    >
                      <i className={`fa ${faIcon} fa-3x`} />
                    </div>
                  )
                  break
                default:
                  postThumb = (
                    <div
                      onClick={this.assetClick.bind(this, assetDetail)}
                      className={`thumbWrap thmb_wrap_file file file-doc ${media_extension}`}
                    >
                      <i className={`fa ${faIcon} fa-3x`} />
                    </div>
                  )
                  break
              }
              return (
                <div key={index} className='post'>
                  <div className="select-checkbox">
                    <div className="form-row checkbox">
                      <label>
                        <input
                          id={`check-${assetDetail.identity}`}
                          name='check-moderation'
                          type='checkbox'
                          class="asset-check"
                          checked={this.state[assetDetail.identity]}
                          ref={(input) => { this[`check-${assetDetail.identity}`] = input; }}
                          onChange={this.singleAssetsCheck.bind(this)}

                        />
                        <span> </span>
                      </label>


                    </div>
                  </div>
                  <div className='clearfix postInner'>

                    <div className='moderationAssetThumb'>
                      <a>
                        {postThumb}
                      </a>
                    </div>

                    <div className='moderationAssetDetail'>
                      <div className='post-header post'>

                        {/* for particuler assets */}

                        <div className='author-thumb'>
                          <div className='thumbnail' style={authorAvatar}
                            data-tip={asset.email}
                            data-for={asset.identity} />
                          {this.state.tooltip?
                          <this.state.tooltip
                            id={asset.identity}
                            type='warning'
                          />:''}
                          <FetchuserDetail userID={asset.owner_id}/>
                        </div>
                        <div className='author-data'>

                          <div className='author-meta'>
                            <b>{assetAuthor}</b>
                            {' '}
                            <span className='hidden-xs'>
                              uploaded
                          {' '}
                              {media_type == 'image' ||
                                media_type == 'video' ||
                                media_type == 'audio'
                                ? media_type
                                : 'asset'}
                            </span>
                          </div>
                          <div
                            className='post-time'
                            title={
                              this.state.moment !== null && typeof me.state.moment.tz !== 'undefined'
                                ? this.state.moment
                                  .tz(asset.created_date.date, Globals.SERVER_TZ)
                                  .format('dddd, Do MMMM  YYYY, h:mm:ss a')
                                : ''
                            }
                          >
                            {this.state.moment !== null && typeof me.state.moment.tz !== 'undefined'
                              ? this.state.moment
                                .tz(asset.created_date.date, Globals.SERVER_TZ)
                                .fromNow()
                              : ''}
                          </div>
                          <div class="campaign-custom-box">
                          {/* i am commenting this bcz i am not getting response from api (disha) */}
                            {/* {asset.category!== undefined ? asset.category.data !== undefined ? asset.category.data.campaign_identity !== '' ?<div class="Campaigntitle">Campaign</div>:'':'':''} */}
                            { asset.category!== undefined ? asset.category.data !== undefined ? <div class="Customtitle">{asset.category.data.name}</div>:'':''}
                          </div> 


                        </div>

                      </div>
                      <h4> {assetDetail.detail} </h4>
                    </div>

                  </div>
                  <div className='moderationAssetApprovalActions'>
                    <div className="approval-actions clearfix">

                      <div class="approval-box">
                        <button
                          onClick={this.moderateAsset.bind(
                            this,
                            'approve',
                            assetDetail
                          )}
                          className='btn btn-fa btn-success approval-btn'
                        >

                          <i className='material-icons'>add_circle_outline</i>
                          <span className='btnText'>APPROVE</span>

                        </button>
                      </div>

                      {/* {status !== 2
                        ? <button
                          onClick={this.moderateAsset.bind(
                            this,
                            'undecided',
                            assetDetail
                          )}
                          className='btn btn-fa btn-warning'
                        >
                          <i className='material-icons'>
                            remove_circle_outline
                          </i>
                          <span className='btnText'>UNDECIDED</span>
                        </button>
                        : null} */}

                      <div class="unapproval-box">
                        {status !== 3

                          ? <button
                            //onclick={this.moderateAsset.bind(
                            // this,
                            // 'unapprove',
                            // assetDetail)}
                            onClick={
                              this.handleUnapproveFeeback.bind(this, asset.identity)}
                            className='btn btn-fa btn-danger unapproval-btn'
                          >
                            <i class="material-icons">highlight_off</i>
                            <span className='btnText'>UNAPPROVE</span>
                          </button>
                          : null}

                        {this.renderAssetTags(assetDetail)}
                      </div>
                    </div>
                  </div>
                </div>
              )
            })}
          {this.state.loadMoreAsset==true? 
          <PreLoader location="moderation-assets" isBottom={true}/>
           :''
          }
          </div>}
          </div>
        ) 
    }
  }

  renderPostTags(post) {
    if (post !== undefined) {
      let tags = post.data.tags.data
    }
    return (
      <div> </div>
      // <div className='post-footer'>
      //   <i className='material-icons'>local_offer</i>
      //   {tags.map((tag, index) => {
      //     return (
      //       <span key={index}>
      //         {tag.name}
      //       </span>
      //     )
      //   })}

      // </div>
    )
  }

  sidebarLinkClicked(collapseKey) {
    var listingKeyArr = []
    listingKeyArr.push(collapseKey)

    this.props.moderationActions.moderationSidebarAction(listingKeyArr)
  }

  changeFilterPost(events) {
    var newLocation = utils
      .removeTrailingSlash(this.props.location.pathname)
      .split('/');

    let type = "assets";
    if (newLocation[2] === "posts" || typeof newLocation[2] === "undefined") {
      type = "posts";
    }
    this.setState({
      filterBy: events.target.value,
      currPage: 1,
      posts: [],
      assets: [],
      currListingType: type,
      currListingTypeStatus: newLocation[3]
    }, () => {
      let jsonData = { currListingTypeStatus: newLocation[3], filterBy: this.state.filterBy };
      if (newLocation[2] === "posts" || typeof newLocation[2] === "undefined") {
        this.fetchPosts(jsonData);
      } else {
        this.fetchAssets(jsonData);
      }
    }
    );
  }

  renderPosts() { 
    var me =this
      if ((me.state.noPosts == true || me.state.posts[0]==='User do not have permission for accessing this module')&& me.state.PostLoader!==true) {
        return (
          <div class='no-data-block'> No post found. </div>
        )
      }
      else {
        return (
          <div>
            {me.state.PostLoader==true || this.props.moderation.loading?
            <PreLoader  location='moderation-posts'/>
            :
            <div>
             {/* for particuler post selectall  */}
              {displayFeed == true ?
                <div className="container-button-checkbox clearfix">
                <div className="inner-select-checkbox-post">
                  <div className="campaign-box">
                    <div className="campaign-header">CAMPAIGN</div>
                    <div className="custom-header">CUSTOM</div>
                  </div>
                  <div className="selectAllCheckboxForPost">

                    <div class="form-row checkbox" title="Select All">
                      <label>
                      <input
                          type="checkbox"
                          id='selctedall-post'
                          name="check-moderation-post"
                          class="post-check selectallpost"
                          value="on"
                          checked={this.state.multiplePostchecked}
                          ref={(input) => { this.selectedallpost = input }}
                          onClick={this.multipleAssetsApproveUnapprove.bind(this)}
                        />
                        <span> </span></label></div>
                    <div className="header-icon clearfix">
                      <div className="add-icon" title="Approve">
                        <i class="material-icons" onClick={this.postApproved.bind(
                          this,
                          'approve',
                          this.state.assetApprovedUnapprovedArray
                        )} >add_circle_outline</i>
                      </div>
                      {this.props.location.pathname !== "/moderation/posts/unapproved" ?
                        <div className="remove-icon" title="Unapprove">
                          <i class="material-icons clear" onClick={this.postUnapproved.bind(this, 'rejected', this.state.assetApprovedUnapprovedArray)}>highlight_off</i>
                        </div> : ''}
                    </div>

                  </div> 
                  </div>
                </div>
            : ''}
              
            {me.state.posts.map((post, index) => {
              var socialAccounts = []
              var asset = {}
              var postdata = {}
              if (typeof post.post !== 'undefined') {
                var authorAvatarURL = post.post.data.postOwner.data.avatar
              }
              var authorAvatar = {
                backgroundImage: `url(${typeof authorAvatarURL !== 'undefined' && authorAvatarURL != '' ? authorAvatarURL : '/img/default-avatar.png'})`
              }
              let postTitle = '', postDate = new Date().getTime() * 1000
              let postThumb = ''

              var media_type = ''
              var media_url = ''
              var thumbnail_url = ''
              var media_extension = 'file'
              var status = '';

              status = post.post !== undefined ? post.post.data.approved : ''
              var identity = post.post !== undefined ? post.post.data.identity : ''
              return (
                <div key={index} className='post post-feed-ui'>
                  <div className="select-checkbox">

                    {/*for particuler post  */}
                    <div className="form-row checkbox">
                      <label>
                        <input
                          id={`check-${identity}`}
                          name='check-moderation-post'
                          type='checkbox'
                          class="post-check"
                          ref={(input) => { this[`check-${identity}`] = input; }}     //ref is used to fine the value on check
                          checked={this.state[identity]}
                          onClick={this.singlePostCheck.bind(this)}
                        />
                        <span> </span>
                      </label>
                    </div>
                    {/* <div class="form-row checkbox"><label><input type="checkbox" id="check-123" name="check-moderation-post" class="post-check" value="on"/><span> </span></label></div> */}
                  </div>



                  <Post
                    feed={feed}
                    //  feedType={feedType}
                    locationInfo={me.props.location}
                    details={post}
                    key={1}
                    index={1}
                    handleCloseEvent={this
                      ._handleCloseEvent
                      .bind()}
                    updateText={this
                      .updateText
                      .bind(this)}
                    feedData={this.props.feed}
                  //  multipleAssetsApproveUnapprove = {this.multipleAssetsApproveUnapprove.bind(this)}
                  />
                  <div className="unapprovedpost">
                    <div className='approval-actions clearfix'>
                      <div className="approval-box">
                        <button
                          onClick={this.moderatePost.bind(this, 'approve', post)}
                          className='btn btn-fa btn-success approval-btn'
                        >
                          <i className='material-icons'>add_circle_outline</i>
                          <span className='btnText'>APPROVE</span>
                        </button>
                      </div>

                      {(status !== 'Rejected') ?
                        <div className="unapproval-box">
                          <button
                            onClick={this.handleUnapproveFeebackPost.bind(this, 'rejected', post)}
                            className='btn btn-fa btn-danger unapproval-btn'
                          >
                            <i class="material-icons">highlight_off</i>
                            <span className='btnText'>UNAPPROVE</span>
                          </button>
                        </div>
                        : ''}
                    </div>
                  </div>

                  {this.renderPostTags(post.post)}
                </div>
                
              )
            })}
            { me.state.loadMorePost == true?
            <PreLoader location="moderation-posts" isBottom={true}/>:''
            }
            </div>
            }
          </div>
        )
      }
  }
  notifiyuser() {
    var inputValue = document.querySelector('.checknotify').checked;
    if (inputValue == true) {
      this.setState({
        isfeedbacksent: 0
      })
    } else {
      this.setState({
        isfeedbacksent: 1
      })
    }
  }
  modIndex(e) {
    currStatus = e
    this.setState({
      currStatus: e
    })
  }
  /**
   * @author disha
   * to call API on apply buttoj is called
   */
  applySelectedFilter(data) {
    var location = utils.removeTrailingSlash(this.props.location.pathname).split('/')
    switch (location[2]) {
      case 'posts':
        if (location[3] !== 'undefined') {
          this.setState({
            currPage: 1,
          })
          this.fetchPosts({
            currPage: this.state.currPage,
            dept: this.state.currDept,
            currListingTypeStatus: location[3]
          })
        } else {
          this.fetchPosts({
            currPage: this.state.currPage,
            dept: this.state.currDept,
          })
        }
        break;
      case 'assets':
        if (location[3] !== 'undefined') {
          this.setState({
            currPage: 1,
            // assets: [],
          })
          this.fetchAssets({
            currPage: this.state.currPage,
            currListingTypeStatus: location[3]
          })
        }
        break;
      default:
    }
  }
  /**
   * @author disha
   * @param {*} alldata has id and name of selected data
   * @param {*} type feedtype / campaign type/ departmenttype/usertype
   * @param {*} location location[2]
   * @param {*} deleteState default is false but it will true when input box has different value then state
   */
  selectedDropdownValue(alldata, type, location, deleteState = false) {
    switch (type) {
      case 'FeedType':
        var feedId = location + 'feedId'
        this.setState({
          [feedId]: deleteState == true ? '' : alldata.dropDownId
        })
        break;
      case 'campaignType':
        var campId = location + 'campaignId'
        this.setState({
          [campId]: deleteState == true ? '' : alldata.dropDownId
        })
        break;
      case 'departmentType':
        var departmentId = location + 'departmentId'
        this.setState({
          [departmentId]: deleteState == true ? '' : alldata.dropDownId
        })
        break;
      case 'userType':
        var userType = location + 'userType'
        this.setState({
          [userType]: deleteState == true ? '' : alldata.dropDownName
        })
      default:
        return;
    }
  }
  /**
   * @author disha
   * to clear all saved or unsaved filter data
   * @param {*} location location
   */
  unsaveFilter(location) {
    var type = location[2] !== undefined ? location[2] : 'posts'
    var departmentId = type + 'departmentId'
    var campaignId = type + 'campaignId'
    var feedId = type + 'feedId'
    var userType = type + 'userType';
    this.setState({
      [departmentId]: '',
      [campaignId]: '',
      [feedId]: '',
      [userType]: '',
    }, () => { this.applySelectedFilter() })

  }
  render() {
    let currAsset
    if (this.state.currAsset !== null) currAsset = this.state.currAsset
    let roleName = JSON.parse(localStorage.getItem('roles')) !== null ? JSON.parse(localStorage.getItem('roles'))[0].role_name : ''
    let currListingType = this.state.currListingType
    let currListingTypeStatus = this.state.currListingTypeStatus

    if (!currListingTypeStatus) currListingTypeStatus = 'pending'

    let moderationListingTypeInSidebar = this.props.moderation.moderation
      .sidebarListing
    let user = this.props.users,
      userDetails = user.userDetails,
      isFetching = user.isFetching,
      department = this.props.department;
    let currlisting_status = currStatus;
    return (
      <div>
        <GuestUserRestrictionPopup setOverlay={true} />
        <section className='moderation'>

          {this.state.assetDetailPopup
            ? this.renderAssetDetail(currAsset)
            : <div />}
          {/* this is user tag popup */}
          {/* <div id="tag-popup-overlay" onClick={this.closepopup.bind()}></div> */}
          <div id="pop-up-tooltip-holder">
            <div id="pop-up-tooltip-wrapper" className={this.state.tagTypeClass}>
              <div className="pop-up-tooltip" style={{ top: this.state.popupTop, left: this.state.popupLeft, display: this.state.popupdisplay }}>
                <span className='popup-arrow'></span>

                {/* <div className='preloader-wrap'>
            <PreLoader />
          </div> */}


                <TagPopup
                  userDetails={selectedUserData}
                  isFetching={isFetching}
                  isDept={this.state.tagType}
                  department={selectedDepartmentData}
                />

              </div>
            </div>
        </div>
        <Header title="Moderation"
          nav={ModerationNav}
          location = {this.props.location}
          header_currListingTypeStatus = {currlisting_status}
          header_function = {this.sidebarLinkClicked.bind(this)}
          filter_droupdown_feed ={this.state.filter_droupdown_feed}
          filterBy={this.state.filterBy}
          changefilter={this.changeFilterPost.bind(this)}
          notifiyuser = {this.notifiyuser.bind(this)}
          headerMobIndex = {this.modIndex.bind(this)}
          open_filter_fn = {true}
        />
        <div className='main-container'>
          <div id='content'>

            <div className='page with-filters'>
              {/* Droup-down in modration home-page  */}
              <FilterWrapper>
                <div className = "clearfix">

                    {/* old dropudown */}
                    {/* <div className='select-wrapper'>
                      <select className='drop_department' value={this.state.filterBy} onChange={this.changeFilterPost.bind(this)}>
                      <option value='new'>Latest</option>
                      <option value='schedule'>Scheduled</option>
                      </select>
                  </div> */}
                    {/* end old droup-down */}
                    {/* new filter desing */}
                    {this.state.filter_droupdown_feed == 'false' ? ''
                      :
                      /*<div id="filter-container">
                      <ul class='dd-menu context-menu' >
                         <li class='button-dropdown'>
                           <a class='dropdown-toggle'>
                           Order
                           <i class='material-icons'>&#xE5C5;</i>
                          </a>
                         <ul class='dropdown-menu'>
                         <li onClick={this.changeFilterPost.bind(this)}>
                         <a rel="new" class='btn-edit-folder'>Latest</a>
                         </li>
                          <li onClick={this.changeFilterPost.bind(this)}>
                             <a rel="schedule" class='btn-edit-folder'>
                               Scheduled
                             </a>
                           </li>
                         </ul>
                        </li>
                      </ul>
                   </div>*/
                      <div className="moderation-filter-wrapper clearfix">
                        <label for="sortByFilter">Sort By</label>
                        <div className='select-wrapper'>
                          <select id="sortByFilter" className='drop_department' value={this.state.filterBy} onChange={this.changeFilterPost.bind(this)}>
                            <option value='new'>Latest</option>
                            <option value='schedule'>Scheduled</option>
                          </select>
                        </div>
                      </div>
                    }


                    {/* end new filter desing */}
                    {this.props.location.pathname == '/moderation/posts/unapproved' || this.props.location.pathname == '/moderation/assets/unapproved' ?
                      '' :
                      <div className="on-off-switch-main-container">
                        <div class="onoffswitch">

                          <div className="form-row checkbox">
                            <label for="check1">
                              <input
                                id='check1'
                                name='check1'
                                className='checknotify'
                                type='checkbox'
                                onChange={this.notifiyuser.bind(this)}
                              />
                              <span> </span>
                              Send Feedback
                       </label>
                          </div>

                        </div>
                      </div>
                    }
                  {/* </div> */}
                </div>
              </FilterWrapper>
              {/* End Dropu-down in modration home-page */}
              <CollapsibleFilter>
                  <ModerationFilter props={this.props} departments={this.props.departments.list} selectedDropdownValue={this.selectedDropdownValue.bind(this)} applySelectedFilter={this.applySelectedFilter.bind(this)} unsaveFilter={this.unsaveFilter.bind(this)}/>
                </CollapsibleFilter>
                <div className='esc-sm'>

                  <div className='content'>

                    {/* <FilterSearch></FilterSearch> */}

                    <div id='feed'>
                      {/* Search Box */}
                      {/* This is the hide by @kinjal Birare... */}
                      {/* modration droup-down */}
                      {/* <div className='col-one-of-three'>
                    <div class="modaration-dropudown clearfix">

                    <div className='dropdown-wrapper'>
                      <select className='filterDropdown'value={this.state.filterBy} onChange={this.changeFilterPost.bind(this)} >

                        <option value='new'>Latest</option>
                        <option value='schedule'>Scheduled</option>
                      </select>
                      </div>
                    </div>
                  </div> */}
                      {roleName == 'guest' ?
                        <StaticDataForGuest props={this.props} />
                        : this.state.currListingType == 'posts'
                          ? this.renderPosts()
                          : this.renderAssets()}


                    </div>

                  </div>
                </div>

              </div>
            </div>
          </div>
          {this.state.feedback_popup ? this.renderFeedbackPopup() : ''}

        </section>
      </div>
    )
  }
  open_main_menu() {
    this.setState({
      show_menu: !this.state.show_menu
    });
    var active_class = this.state.show_menu ? "in-view has-sib" : " has-sib";
    var handle_active_class = this.state.show_menu ? 'active inner_main_menu_handle' : 'inner_main_menu_handle';
    document.getElementById('navdraw').className = active_class;
    document.getElementById('inner_main_menu_handle').className = handle_active_class;

  }
}

function mapStateToProps(state) {
  return {
    assets: state.assets,
    departments: state.departments,
    feed: state.feed,
    campaigns: state.campaigns,
    moderation: state.moderation,
    users: state.users,
    profile: state.profile,
    usersList: state.usersList.userList.data
  }
}

function mapDispatchToProps(dispatch) {
  return {
    assetsActions:bindActionCreators(assetsActions, dispatch),
    feedActions:bindActionCreators(feedActions, dispatch),
    headerActions : bindActionCreators(headerActions , dispatch),
    campaignsActions:bindActionCreators(campaignsActions, dispatch),
    moderationActions:bindActionCreators(moderationActions, dispatch),
  }
}

module.exports = connect(mapStateToProps, mapDispatchToProps)(Moderation)

