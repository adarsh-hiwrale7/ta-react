

import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import {Link} from 'react-router';

class FilterSearch extends React.Component {

	render() {


		return (
      <form role="form" className="form-controls">
        <button type="submit" className="btn btn-primary"><i className="material-icons">more_vert</i></button>

        <input id="" placeholder="Search" type="search" />
        <div className="select">
          <select name="" id="">
            <option value="">Option A</option>
            <option value="">Option B</option>
          </select>
        </div>
      </form>
		)

	}
}

function mapStateToProps(state) {
  return {

  }
}
function mapDispatchToProps(dispatch) {
  return {
  }
}
module.exports = connect(mapStateToProps, mapDispatchToProps) (FilterSearch);
