import Collapse, { Panel } from 'rc-collapse' // collapse
// collapse css
import { browserHistory } from 'react-router'
import { Field, reduxForm } from 'redux-form'
import { connect } from 'react-redux'
import { IndexLink, Link } from 'react-router'
import * as utils from '../../utils/utils'
import { renderField } from '../Helpers/ReduxForm/RenderField'
var pendingUnapprovedStatus=''
class ModerationNav extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      pendingUnapprovedStatus: ''
    }
  }
  componentDidMount() {
    pendingUnapprovedStatus=this.props.currListingTypeStatus == 'pending'? '':this.props.currListingTypeStatus
  }
  componentWillUnmount() {
    pendingUnapprovedStatus=''
  }
  sidebarLinkClicked (collapseKey,currListingType) {
    this.props.header_function(collapseKey)
    if(pendingUnapprovedStatus=='pending'){
      if( currListingType=='posts' ){
          browserHistory.push('/moderation/posts');
      }else if( currListingType=='assets' ){
          browserHistory.push('/moderation/assets')
      }
    }else if(pendingUnapprovedStatus == 'unapproved'){
      if( currListingType=='posts' ){
         browserHistory.push('/moderation/posts/unapproved');
      }else if( currListingType=='assets' ){
        browserHistory.push('/moderation/assets/unapproved')
      }
  }
}

  checkRadioButtonForStatus (currListingType,e) {
    pendingUnapprovedStatus= e.target.value=='pending'?'':'unapproved';
    this.setState({
      pendingUnapprovedStatus: e.target.value
    })
    this.props.moderatTest(e.target.value);
    if( currListingType=='posts' ){
      if(e.target.value=='pending'){
          browserHistory.push('/moderation/posts');
      }else if(e.target.value == 'unapproved'){
          browserHistory.push('/moderation/posts/unapproved')
      }
    }else if( currListingType=='assets' ){
      if(e.target.value=='pending'){
          browserHistory.push('/moderation/assets')
      }else if(e.target.value=='unapproved'){
          browserHistory.push('/moderation/assets/unapproved')
      }
    }
  }
  render () {
    var newLocation = utils
      .removeTrailingSlash(this.props.location.pathname)
      .split('/')
    var currListingType = newLocation[2] ? newLocation[2] : 'posts'
    var currListingTypeStatus = this.props.currListingTypeStatus
    const { handleSubmit, pristine, reset, submitting } = this.props
    return (
      <nav className='nav-side'>
        <ul className='parent  clearfix'>
          <li>
          <div className="radiobtnFeed">
            <div class='form-row customradio'>
            
              <input
                type='radio'
                name='Status'
                value='pending'
                id='pending'
                checked={
                  currListingTypeStatus == 'pending'?true:false
                }
                onClick={this.checkRadioButtonForStatus.bind(this, currListingType)}
              />
              <span class="checkmark"></span>
               <label for="pending"> PENDING</label>
               
              
            </div>
            </div>
            <div className="radiobtnFeed">
            <div class='form-row customradio'>
            
              <input
                type='radio'
                name='Status'
                value='unapproved'
                id='unapproved'
                checked={
                  currListingTypeStatus == 'unapproved'?true:false
                }
                onClick={this.checkRadioButtonForStatus.bind(this, currListingType)}
              />
                <span class="checkmark"></span>
                
                <label for="unapproved"> UNAPPROVED</label>
              
            </div>
            </div>
          </li>
          <li>
            <Link
              className={
                currListingType == 'posts'
                  ? 'active'
                  : ''
              }
              onClick={this.sidebarLinkClicked.bind(this, '1','posts')}
              to={`/moderation/posts/${pendingUnapprovedStatus}`}
            >
              <span className='text'>Posts</span>
            </Link>
          </li>
          <li>
            <Link
              className={
                currListingType == 'assets'
                  ? 'active'
                  : ''
              }
              onClick={this.sidebarLinkClicked.bind(this, '2','assets')}
              to={`/moderation/assets/${pendingUnapprovedStatus}`}
            >
              <span className='text'>Assets</span>
            </Link>
          </li>
        </ul>
      </nav>
    )
  }
}

function mapStateToProps (state) {
  return {}
}

function mapDispatchToProps (dispatch) {
  return {}
}

module.exports = connect(mapStateToProps, mapDispatchToProps)(ModerationNav)
