
import { connect } from 'react-redux'
import { Field, reduxForm } from 'redux-form'
import * as utils from '../../utils/utils'
import { renderField } from '../Helpers/ReduxForm/RenderField'
import ChunkSelect from '../Chunks/ChunkReactSelect';
var addNewTextInField = false
var feedData = ''
var campData = ''
var departmentData = ''
var isApplyButtonClicked = false
class ModerationFilter extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      noItemsClass: '',
      showSuggestionListFeed: '',
      showSuggestionListCampaign: '',
      showSuggestionListDepartment: '',
      reactSelect :null
    }
  }
  componentWillMount() {
      ChunkSelect().then(reactselect => {
            this.setState({
              reactSelect: reactselect,
      })
    })
  }
  componentDidMount () {
    addNewTextInField = false
  }
  hideFilterLists (type) {
    this.setState({
      showSuggestionListFeed: '',
      showSuggestionListCampaign: '',
      showSuggestionListDepartment: ''
    })
  }
/**
 * @author disha 
 * @param {*} type feedtype or campaigntype or departmenttype
 * this method called when clicked on input textbox to display list
 */
  filterfetchLists (type) {
    var location = utils
      .removeTrailingSlash(this.props.props.location.pathname)
      .split('/')
    var input = ''
    var output = jQuery(`#${type}DropDown li`)
    var showedLi = 0
    if (document.getElementById(`${type}`)) {
      input = document.getElementById(`${type}`).value.toLowerCase()
    }
    if (type == 'FeedType') {
      //if type is FeddType then to set state to active to display list in dropdown
      if (input !== feedData) {
        //if feeddata (if feed type is already set) and input data is not equal means input box has changed value then to remove state of old data called parent props
        addNewTextInField = true  //to active apply button when input box has new value
        this.props.selectedDropdownValue('', type, location[2], true)
      }
      this.setState({
        showSuggestionListFeed: 'active',
        showSuggestionListCampaign: '',
        showSuggestionListDepartment: ''
      })
    } else if (type == 'campaignType') {
      if (input !== campData) {
        addNewTextInField = true
        this.props.selectedDropdownValue('', type, location[2], true)
      }
      this.setState({
        showSuggestionListCampaign: 'active',
        showSuggestionListFeed: '',
        showSuggestionListDepartment: ''
      })
    } else if (type == 'departmentType') {
      if (input !== departmentData) {
        addNewTextInField = true
        this.props.selectedDropdownValue('', type, location[2], true)
      }
      this.setState({
        showSuggestionListDepartment: 'active',
        showSuggestionListCampaign: '',
        showSuggestionListFeed: ''
      })
    }
    for (var i = 0; i < output.length; i++) {
      //to display list in dropdown
      if (output[i].innerText.toLowerCase().indexOf(input) != -1) {
        output[i].style.display = 'block'
        showedLi++
      } else {
        output[i].style.display = 'none'
      }
      if (showedLi == 0) {
        this.setState({
          noItemsClass: 'no-items'
        })
      } else {
        this.setState({
          noItemsClass: ''
        })
      }
    }
  }
/**
 * @author disha
 * when user type is selected then to pass selected value in parent component to store in state
 * @param {*} e 
 */
  filterSelectedUserType (e) {
    addNewTextInField = true
    var type = e.target.name
    var ObjToSend = {
      dropDownName: e.target.value,
      dropDownId: ''
    }
    var location = utils
      .removeTrailingSlash(this.props.props.location.pathname)
      .split('/')
    this.props.selectedDropdownValue(ObjToSend, type, location[2])
  }
  /**
   * @author disha
   * when data is selected from dropdown list and to pass it to parent component to store data in state
   * @param {*} alldata
   * @param {*} e
   */
  filterSelectedValue (type,alldata) {
    // call when tag will be selected from filter department/user
    addNewTextInField = true
    let ObjToSend ={
      dropDownName:alldata.label,
      dropDownId:alldata.value
    }
    var location = utils
      .removeTrailingSlash(this.props.props.location.pathname)
      .split('/');
    switch (type) {
      case 'FeedType':
          this.setState({
            feedOptionState : alldata
          })
          if(alldata.value==0){
            this.props.selectedDropdownValue('', type, location[2],true)
          }else{
            this.props.selectedDropdownValue(ObjToSend, type, location[2])
          }
        feedData = alldata
        break
      case 'campaignType':
          this.setState({
            campaignOptionState : alldata
          })
          if(alldata.value==0){
            this.props.selectedDropdownValue('', type, location[2],true)
          }else{
            this.props.selectedDropdownValue(ObjToSend, type, location[2])
          }
        campData = feedData = alldata
        break
      case 'departmentType':
          this.setState({
            departmentOptionState : alldata
          })
          if(alldata.value==0){
            this.props.selectedDropdownValue('', type, location[2],true)
          }else{
            this.props.selectedDropdownValue(ObjToSend, type, location[2])
          }
        departmentData = feedData = alldata
        break
      default:
        console.log()
    }
  }
  /**
   * @author disha
   * call when click on clear filter to unsave filter
   * @param {*} e
   */
  unsaveFilter (e) {
    isApplyButtonClicked = false
    this.setState({
      feedOptionState:null,
      campaignOptionState : null,
      departmentOptionState:null
    })
    //initialize is called bcz to remove userytpe value
    var initData = {}
    this.props.initialize(initData)
    this.props.unsaveFilter(e)
  }
  /**
   * @author disha
   * call when apply button is clicked
   */
  applySelectedFilter (e) {
    //below flag is used to active or disable if button is clicked
    addNewTextInField = false
    isApplyButtonClicked = true
    this.props.applySelectedFilter(e)
  }
  render () {
    var campaignList, feedList = ''
    var location = utils
      .removeTrailingSlash(this.props.props.location.pathname)
      .split('/')
    var departmentList = this.props.departments !== undefined ?this.props.departments.length > 0
      ? this.props.departments
      : '':''
    if (location[2] == 'assets') {
      campaignList = this.props.props.assets.campaigns.length > 0
        ? this.props.props.assets.campaigns
        : ''
      feedList = this.props.props.assets.customfeedAsset.length > 0
        ? this.props.props.assets.customfeedAsset
        : ''
    } else {
      campaignList = this.props.props.campaigns.campaigns.length > 0
        ? this.props.props.campaigns.campaigns
        : ''
      feedList = this.props.props.feed.feedList !== undefined ?Object.keys(this.props.props.feed.feedList).length > 0
        ? this.props.props.feed.feedList
        : '':''
    }
    var optionsFeed = [];
    var optionsCampaign=[];
    var optionsDepartment =[];

    if(feedList){
      feedList.forEach(function (option) {
        let identity = option.identity;
        let feedLabel = option.name ? option.name : option.title;
        optionsFeed=[...optionsFeed,{label:feedLabel,value:identity}];
      });
    }
    if(campaignList){
      campaignList.forEach(function (option) {
        let identity = option.identity;
        let campLabel = option.name ? option.name : option.title;
        optionsCampaign=[...optionsCampaign,{label:campLabel,value:identity}];
      });
    }
    if(departmentList){
      departmentList.forEach(function (option) {
        let identity = option.identity;
        let departmentLabel = option.name ? option.name : option.title;
        optionsDepartment=[...optionsDepartment,{label:departmentLabel,value:identity}];
      });
    }

    if(optionsFeed.length!==0){
    optionsFeed =[{value:0,label:"All"},...optionsFeed]}
    if(optionsCampaign.length!==0){
    optionsCampaign = [{value:0,label:"All"},...optionsCampaign]}
    if(optionsDepartment.length!==0){
    optionsDepartment = [{value:0,label:"All"},...optionsDepartment]}

    let feedOptionState = this.state.feedOptionState;
    let campaignOptionState = this.state.campaignOptionState;
    let departmentOptionState = this.state.departmentOptionState;
    return (
      <div className='row'>
        <div className='col-one-of-four'>
          <div className='curated-filter-inner'>
            <div className='form-row'>
              <label>Feeds</label>
               <div className='select-wrapper'>
               {this.state.reactSelect !== null ?
                  <this.state.reactSelect.default
                  className='FilterModeration'
                  options={optionsFeed}
                  id='FeedType'
                  onChange ={this.filterSelectedValue.bind(this,'FeedType')}
                  value={feedOptionState}
                />:''}
              </div>
            </div>
          </div>
        </div>
        <div className='col-one-of-four'>
          <div className='curated-filter-inner'>
            <div className='form-row'>
              <label>Campaign</label>
              <div className='select-wrapper'>
              {this.state.reactSelect !== null ?
              <this.state.reactSelect.default
                  className='FilterModeration'
                  options={optionsCampaign}
                  id='campaignType'
                  onChange ={this.filterSelectedValue.bind(this,'campaignType')}
                  value={campaignOptionState}
                />:''}
              </div>
            </div>
          </div>
        </div>
        <div className='col-one-of-four'>
          <div className='curated-filter-inner'>
            <div className='form-row'>
              <label>Department</label>
              <div className='select-wrapper'>
              {this.state.reactSelect !== null ?
              <this.state.reactSelect.default
                  className='FilterModeration'
                  options={optionsDepartment}
                  id='departmentType'
                  onChange ={this.filterSelectedValue.bind(this,'departmentType')}
                  value={departmentOptionState}
                />:''}
              </div>
            </div>
          </div>
        </div>
        <div className='col-one-of-four'>
          <div className='curated-filter-inner'>
            <div className='form-row'>
              <label>User Type</label>
              <div className='select-wrapper'>
                <Field
                  id='userType'
                  name='userType'
                  component={renderField}
                  type='select'
                  onChange={this.filterSelectedUserType.bind(this)}
                >
                  <option value=''>Select User Type</option>
                  <option value='user'>Employee</option>
                  <option value='guest'>Guest</option>
                </Field>
              </div>

            </div>
          </div>
        </div>
        <div className='filter-button-wrapper clearfix'>
          <a
            disabled={!isApplyButtonClicked ? 'disabled' : ''}
            onClick={this.unsaveFilter.bind(this, location)}
            className='btn btn-theme'
          >
            Clear Filter
          </a>
          <a
            disabled={!addNewTextInField ? 'disabled' : ''}
            className='btn btn-theme'
            onClick={this.applySelectedFilter.bind(this)}
          >
            Apply filter
          </a>
        </div>
      </div>
    )
  }
}

function mapStateToProps (state) {
  return {}
}
function mapDispatchToProps (dispatch) {
  return {}
}
let connection = connect(mapStateToProps, mapDispatchToProps)
var reduxConnectedComponent = connection(ModerationFilter)

export default reduxForm({
  form: 'ModerationFilter' // a unique identifier for this form
})(reduxConnectedComponent)
