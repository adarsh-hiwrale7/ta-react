import { connect } from 'react-redux'

import { bindActionCreators } from 'redux'
import notie from 'notie/dist/notie.js'
import { Link } from 'react-router'
import Globals from '../../Globals'
import PreLoader from '../PreLoader'
import { Field, reduxForm } from 'redux-form'
import * as moderationActions from '../../actions/moderationActions'
import * as utils from '../../utils/utils'
const required = value => (value ? undefined : 'Required')
var feedmsg = ''
class DeclineFeedback extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      selectedmessage_id: null,
      feedbackMessage: [],
      NewfeedbackMessage: '',
      input: '',
      asset_identity: null,
      saveNewFeedback: 0,
      isfeedbacksent: 0,
      selected_feedback: [],
      newtextAdded: false
    }
  }
  renderLoading () {
    return (
      <div className='preloader-wrap'>
        <PreLoader />
      </div>
    )
  }
  clearSelection (e) {
    var afterSliceFeed = []
    var restFeed = {}
    var myNewSelectedFeedArray = this.state.selected_feedback.filter(function (
      item
    ) {
      return item[0] != e.toString()
    })
    myNewSelectedFeedArray.map(newdata => {
      restFeed = { id: newdata[0], text: newdata[1] }
      afterSliceFeed.push(restFeed)
    })
    this.setState({
      feedbackMessage: afterSliceFeed
    })
    document
      .getElementsByClassName(`choiceitem-${e}`)[0]
      .remove(`choiceitem-${e}`)
    document
      .getElementsByClassName(`search-choice-${e}`)[0]
      .classList.remove(`hide`)

    this.renderFeedbackUI()
  }
  feed_message (e, index) {
    var parentChoiceList = document.getElementById('choice_list')
    var newDivChoice = document.createElement('div')
    var newChildButton = document.createElement('span')
    var newCloseIcon = document.createElement('i')
    var newCloseIcontext = document.createTextNode('clear')
    var childDivValue = document.createTextNode(e.feedback)
    newCloseIcon.classList.add('material-icons')
    newCloseIcon.appendChild(newCloseIcontext)
    newChildButton.addEventListener(
      'click',
      this.clearSelection.bind(this, e.identity),
      false
    )
    newChildButton.classList.add('choices__button')
    newDivChoice.appendChild(childDivValue)
    newDivChoice.classList.add(`choiceitem-${e.identity}`)
    newDivChoice.classList.add('choices__item')
    newChildButton.appendChild(newCloseIcon)
    newDivChoice.appendChild(newChildButton)
    parentChoiceList.appendChild(newDivChoice)
    var feedmsg = { id: e.identity, text: e.feedback }
    this.state.feedbackMessage.push(feedmsg)
    this.setState({
      selectedmessage_id: index,
      asset_identity: this.props.assetid
    })
    this.state.selected_feedback.push([e.identity, e.feedback])
  }
  handlefeedback () {

    var data = {}
    if (this.state.input !== '') {
      for(var i=0;i<this.state.feedbackMessage.length;i++){
        if(this.state.feedbackMessage[i].text==this.state.input)
        {
          notie.alert('error', 'Sorry,Please write different feedback.', 3)
          return;
        }
      }
      data = { id: 0, text: this.state.input }
      this.state.feedbackMessage.push(data)
    }
    if (this.state.feedbackMessage !== '' || this.state.input !== '') {

      if (this.props.actiontype == 'assets') {
        var objToSend = {
          is_feedback_sent: this.props.isfeedbacksent,
          asset_identity: this.props.unapprovedAsset  ? this.props.assetid  : [this.props.assetid],
          approved_status: 'unapprove',
          feedback: this.state.feedbackMessage,
          save_as_feedback: this.state.saveNewFeedback
        }
        this.props.moderationActions.sendUnapprovedFeedback(objToSend)
      } else {
        var objToSend = {
          is_feedback_sent: this.props.isfeedbacksent,
          post_identity: this.props.unapprovedPost == true ?  this.props.postid : [this.props.postid] ,
          approved_status: this.props.postStatus,
          feedback: this.state.feedbackMessage,
          save_as_feedback: this.state.saveNewFeedback
        }
        this.props.moderationActions.sendUnapprovedFeedbackPost(objToSend)
      }
    } else {
      notie.alert('error', 'Sorry,You have to Give Feedback Message.', 3)
    }
  }
  addNewFeedback () {
    // this.state.feedbackMessage.push(this.state.input)
    this.setState({
      NewfeedbackMessage: this.state.input,
      saveNewFeedback: 1
    })
    var newtext = {
      identity: 1,
      feedback: this.state.input
    }
  }
  handleChange (e) {

    if (e.target.value == '' ) {
      this.setState({
        input: '',
        newtextAdded: false
      })
    } else {
      this.setState({
        input: e.target.value,
        newtextAdded: true
      })
    }
  }
  componentWillReceiveProps (nextProps) {
    if (nextProps.moderation.closeAllPopups == true) {
      this.closeFeedpopup()
    }
  }
  handleInitialize () {
    const initData = {
      feedback_text: this.state.feedbackMessage
    }

    this.props.initialize(initData)
  }
  selectedMessage (e) {
  }
  componentDidMount () {
    var actiontype = ''
    if (this.props.actiontype == 'posts') {
      actiontype = 'post'
    } else if (this.props.actiontype == 'assets') {
      actiontype = 'asset'
    }
    if (this.props.isfeedbacksent == 1) {
      this.handlefeedback()
    }
    this.props.moderationActions.feedbackLists(actiontype)
  }
  closeFeedpopup () {
    this.props.closepopup()
  }
  renderFeedbackUI () {
    var moderation = this.props.moderation.moderation
    return (
      <div className='moderation-feed-wrapper'>
        <div className='chosen-container chosen-container-multi'>

          <ul className='chosen-choices clearfix'>
            {moderation.feedback_messageList
              ? moderation.feedback_messageList.map((mod_list, i) => {
                let hide_class = ''
                this.state.selected_feedback.forEach(function (element) {
                  mod_list.identity == element[0] ? (hide_class = 'hide') : ''
                })
                return (
                    // <li className={`search-choice ${this.state.selectedmessage_id==i?'active':''}`} key={i}>
                    (
                      <li
                        className={`search-choice search-choice-${mod_list.identity} ${hide_class}`}
                        key={i}
                      >
                        <a
                          class='search-choice-close'
                          data-option-array-index={mod_list.identity}
                          onClick={this.feed_message.bind(this, mod_list, i)}
                        >
                          <span>{utils.limitWords(mod_list.feedback,Globals.LIMIT_WORD)}</span>
                        </a>
                      </li>
                    )
                  )
              })
              : ''}
            {this.state.saveNewFeedback == 1
              ? <li className='search-choice active'>
                <a class='search-choice-close'>
                  <span>{utils.limitWords(this.state.NewfeedbackMessage,Globals.LIMIT_WORD)}</span>
                </a>
              </li>
              : ''}
          </ul>
        </div>
        <label class='choices__inner'>
          <div class='choices__list choices__list--multiple' id='choice_list' />
          <input
            type='text'
            className='choices__input choices__input--cloned'
            placeholder='Enter new message'
            onChange={this.handleChange.bind(this)}
            name='feedback_text'
            id='feedback_text'
          />
          {this.state.newtextAdded
            ? <div className='add_textbox'>
              <i
                class='material-icons'
                onClick={this.addNewFeedback.bind(this)}
                >
                  add_circle
                </i>
            </div>
            : ''}
        </label>
      </div>
    )
  }

  render () {
    return (
      <section className=''>
        <div id='feedback_creation'>
          <header className='heading'>
            <h3>Feedback</h3>
          </header>
          <div class='feedback_main_container'>
            <form role='form-row' onSubmit={this.props.handleSubmit}>
            {this.props.moderation.loading?this.renderLoading():''}
              {this.renderFeedbackUI()}
              <div className='clearfix'>
                <div className='bottom-left-buttons'>
                  <a
                    className='btn btn-primary gif-button right'
                    onClick={this.handlefeedback.bind(this)}
                  >
                    Submit
                  </a>
                </div>
              </div>
            </form>
          </div>
        </div>
      </section>
    )
  }
}
function mapStateToProps (state) {
  return {
    moderation: state.moderation
  }
}

function mapDispatchToProps (dispatch) {
  return {
    moderationActions: bindActionCreators(moderationActions, dispatch)
  }
}

let connection = connect(mapStateToProps, mapDispatchToProps)

var reduxConnectedComponent = connection(DeclineFeedback)

export default reduxForm({
  form: 'declinefeedback' // a unique identifier for this form
})(reduxConnectedComponent)
