import { connect } from 'react-redux'
import { Field, reduxForm } from 'redux-form'
import { renderField } from '../Helpers/ReduxForm/RenderField'
import PreLoader from '../PreLoader'
var showMenu = false
class AccountSelection extends React.Component {
  constructor (props) {
    super(props)
    this.state = {}
  }
  render () {
    var domainList = this.props.props.auth.domainList.length > 0
      ?  this.props.props.auth.domainList
      : ''
    const { pristine, submitting, reset } = this.props
    return (
      <div class='login-form'>
        <div class='login-title'>Select your account</div>
        <div class='form-cnt'>
          <div class='loginform-mainbox'>
            <section class='forgotpass clearfix'>
              <form role='form' onSubmit={this.props.handleSubmit}>
               <div class = "contained">
                <div className='switchpopup overlay' id='switch-login'>
                  <div class='form-row  select'>
                    <Field
                      type='select'
                      component={renderField}
                      id='domainList'
                      name='domainList'
                    >
                      <option>Select Domain</option>
                      {domainList
                        ? domainList.map((element, index) => {
                          return (
                            <option key={index} value={element}>
                              {element}
                            </option>
                          )
                        })
                        : ''}
                    </Field>
                  </div>
                  <button
                    type='submit'
                    class='btn btn-primary btn-login'
                    id='btn-space-login'
                    disabled={pristine || submitting}
                  >
                    Next
                  </button>
                </div>
              </div>
              </form>
            </section>
          </div>
        </div>
      </div>
    )
  }
}

function mapStateToProps (state) {
  return {}
}

function mapDispatchToProps (dispatch) {
  return {}
}

let connection = connect(mapStateToProps, mapDispatchToProps)

var reduxConnectedComponent = connection(AccountSelection)

export default reduxForm({
  form: 'AccountSelection' // a unique identifier for this form
})(reduxConnectedComponent)
