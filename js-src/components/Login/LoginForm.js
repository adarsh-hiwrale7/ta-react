import { Field, reduxForm } from 'redux-form'
import { renderField } from '../Helpers/ReduxForm/RenderField'
import * as utils from '../../utils/utils'
import Globals from '../../Globals';
import CryptoJS from '../Chunks/ChunkCryptoJs';
var remebervalue=false;
var Email;
var Password;
var isCryptoChunkLoaded=false
var visiblePassword = false
const required = value => (value ? undefined : 'Required')
const validEmail = value =>
  (value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
    ? 'Invalid email address'
    : undefined)

class LoginForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      CryptoJS:null
    }
   
}
  show_passwod (e) {
    if (document.getElementById('password').type == 'password') {
        visiblePassword = true
      document.getElementById('password').type = 'text'
    } else {
        visiblePassword = false
      document.getElementById('password').type = 'password'
    }
    this.forceUpdate();
  }
componentWillMount() {
  CryptoJS().then(crypto=>{
    this.setState({
      CryptoJS:crypto
    })
  })
}
/**
* Warning: This lifecycle is currently deprecated, and will be removed in React version 17+
More details here: https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html
*/
componentWillReceiveProps(nextProps) {
  if(isCryptoChunkLoaded==false && this.state.CryptoJS){
  // read the user credentials from the cookie
  Email = utils.readCookie("email");
      
  var cipher = utils.readCookie("password");
  if(cipher !==''){
    // to decrypt the user password
    var bytes  = this.state.CryptoJS.AES.decrypt(cipher.toString(),Globals.secretKey);
    Password = bytes.toString(this.state.CryptoJS.enc.Utf8);
  }
  else{
    Password= '';
  }
  var rememberme = utils.readCookie("rememberme");
  if(rememberme=='true'){
    document.getElementById("check1").checked=true;
  }
  this.handleInitialize();
  isCryptoChunkLoaded=true
}
}
  componentDidMount(){
    isCryptoChunkLoaded=false
    
  }

  handleInitialize() {
    const initData = {
        "email": Email,
        "password":Password
    };
    this.props.initialize(initData);
  }
  remeberClick(){
   remebervalue = document.getElementById("check1").checked;
   this.props.change('remebermecheck',remebervalue)
  }
  

  handleSubmit(e){
    this.props.handleSubmit(e);
  }
  render () {
    return (
      <div>
        <div class='loginform-mainbox'>
          <section className='forgotpass clearfix'>
            <form role='form' onSubmit={this.handleSubmit.bind(this)}>
              <div className='contained' id='lg-fm'>
                <Field
                  placeholder='Company email address'
                  component={renderField}
                  type='text'
                  name='email'
                  validate={[required, validEmail]}
                />
                <div className='login-form-password'>
                  <Field
                    placeholder='Password'
                    component={renderField}
                    type='password'
                    name='password'
                    validate={required}
                  />
                  <a
                    href='javascript:;'
                    onClick={this.show_passwod.bind(this)}
                    className='show-passowd'
                  >
                    <i className='material-icons'>{visiblePassword ? 'visibility_off':'remove_red_eye'}</i>
                  </a>
                </div>
                <div className='form-row'>
                  <button
                    type='submit'
                    className='btn btn-primary btn-login'
                    id='btn-space'
                  >
                    Login
                  </button>
                </div>
                <div className="login-or-text"><span>or</span></div>
                <div className="sso-login-link">
                    <a onClick={this.props.handleOpenSSOForm}>
                      Login with SSO
                    </a>
                </div>
                <div className='clearfix'>
                  <div className='left'>
                    <div className='form-row checkbox'>
                      <input id='check1' type='checkbox' onChange={this.remeberClick.bind(this)}/>
                      <label for='check1'>Remember me</label>
                    </div>
                  </div>
                  <div className='right'>
                    <a className="forgot-password-link" onClick={this.props.handleForgotPassword}>
                      Forgot Password?
                    </a>
                  </div>
                </div>
                
              </div>
            </form>

          </section>
        </div>
      </div>
    )
  }
}

export default reduxForm({
  form: 'loginForm' // a unique identifier for this form
})(LoginForm)
