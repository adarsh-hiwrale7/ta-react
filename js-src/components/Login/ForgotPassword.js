import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import notie from 'notie/dist/notie.js'
import { Link } from 'react-router'
import ForgotPasswordForm from './ForgotPasswordForm'
import PreLoader from '../PreLoader'
import * as loginActions from '../../actions/loginActions'
export default class ForgotPassword extends React.Component {
  constructor (props) {
    super(props)
  }
  handleSubmit (values) {
    this.props.loginActions.resetForgotPassword(values)
  }
  renderLoading () {
    return (
      <div className='preloader-wrap'>
        <PreLoader />
      </div>
    )
  }
  render () {
    return (
      <section className=''>
        <div id='post-creation'>
          <div className='content'>
            <header className='heading'>
              <h3>Forgot Password</h3>
            </header>
            { this.props.forgotpassword.initloading ? this.renderLoading() : null }
            <ForgotPasswordForm onSubmit={this.handleSubmit.bind(this)} />
          </div>
        </div>
      </section>
    )
  }
}
function mapStateToProps (state) {
  return { forgotpassword: state.auth }
}

function mapDispatchToProps (dispatch) {
  return {
    loginActions: bindActionCreators(loginActions, dispatch)
  }
}

let connection = connect(mapStateToProps, mapDispatchToProps)

module.exports = connection(ForgotPassword)
