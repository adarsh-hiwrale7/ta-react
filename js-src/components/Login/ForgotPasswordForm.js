import { Field, reduxForm } from 'redux-form';
import { renderField } from '../Helpers/ReduxForm/RenderField'
import { connect } from 'react-redux'
import { reset } from 'redux-form';
const required = value => value ? undefined : 'Required';
const validEmail = value => value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value) ?
      'Invalid email address' : undefined;

class ForgotPasswordForm extends React.Component {
  constructor (props) {
    super(props);
    const {handleSubmit,pristine, submitting,reset} = props;
  }

render(){
  return (
    <form role="form" onSubmit={this.props.handleSubmit}>
      <div className="contained">
        <Field placeholder="Company Email Address" component={renderField} type="text" name="email" validate={[ required, validEmail ]} />

        <div className="form-row">
          <button type="submit" className="btn btn-primary btn-login" id="forgot-pass">Submit</button>
        </div>
      </div>
    </form>

  )
}
}
const afterSubmit = (result, dispatch) =>{
  dispatch(reset('forgotPassForm'));

}
function mapStateToProps(state) {
  return {

  }
}

function mapDispatchToProps(dispatch) {
  return {

  }
}
let connection = connect(mapStateToProps, mapDispatchToProps);

var reduxConnectedComponent = connection(ForgotPasswordForm);

export default reduxForm({
  form: 'forgotPassForm',
  onSubmitSuccess: afterSubmit,
  
})(reduxConnectedComponent)
