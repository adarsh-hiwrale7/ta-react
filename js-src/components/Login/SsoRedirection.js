import { Field, reduxForm } from 'redux-form'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { renderField } from '../Helpers/ReduxForm/RenderField'
import * as utils from '../../utils/utils'
import Globals from '../../Globals';
import * as authActions from "../../actions/authActions"
import * as generalActions from "../../actions/generalActions"

class SsoRedirection extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    }
   
}
  
componentWillMount() {
  /*var path = utils
      .removeTrailingSlash(this.props.location.pathname)
      .split('/')
      console.log('query string data',path)*/
}
componentWillReceiveProps(nextProps) {
  if(this.props.general.clientIpDetail !== nextProps.general.clientIpDetail){
      var url = window.location.search
      url = url.replace("?", '');  
      url = url.substring(9)
      var ip_address=typeof nextProps.general.clientIpDetail !=='undefined'? nextProps.general.clientIpDetail.ip:''
      this.props.authActions.authUser(null,null,ip_address,url);
  }

}

 componentDidMount(){
    this.props.generalActions.fetchClientIp()
    
  }

  render () {
    return (
      <div></div>
    )
  }
}


/*function mapStateToProps (state) {
  return {
    profile: state.profile,
    accounts: state.socialAccounts.accounts,
    general: state.general,
    posts: state.posts,
    feedDetail: state.feed
  }
}

function mapDispatchToProps (dispatch) {
  return {
    dispatch,
    feedActions: bindActionCreators(feedActions, dispatch)
  }
}
let connection = connect(mapStateToProps, mapDispatchToProps)
module.exports = connection(Post)*/
function mapStateToProps (state) {
  return {general:state.general}
}

function mapDispatchToProps (dispatch) {
  return {
    authActions: bindActionCreators(authActions, dispatch),
    generalActions:bindActionCreators(generalActions,dispatch)
  }
}
module.exports = connect(mapStateToProps, mapDispatchToProps)(SsoRedirection)

