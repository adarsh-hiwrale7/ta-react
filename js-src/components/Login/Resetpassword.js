import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import notie from 'notie/dist/notie.js'
import { Link } from 'react-router'
import ResetpasswordForm from './ResetpasswordForm'
import PreLoader from '../PreLoader'
import * as loginActions from '../../actions/loginActions'
import VisiblyHeader from '../Layout/Header/VisiblyHeader';
import FooterVisibly from '../Layout/Footer/FooterVisibly';

export default class Resetpassword extends React.Component {
 constructor(props) {
    super(props)
    this.state = {
      menu_handle_wrapper : false,
    }
  }
  handleSubmit(values) {
    var resettoken = this.props.location.query.reset_token
    var email = this.props.location.query.email
    this.props.loginActions.setNewPassword(values, resettoken, email)
  }
  renderLoading() {
    return (
      <div className='preloader-wrap'>
        <PreLoader />
      </div>
    )
  }
  render() {
    return (
      <section className='register-page'>
        <div class = "login-page">
        <VisiblyHeader />
<div class= "banner-part" id = "login-banner">
     <div class = "inner-banner-part">
       <div class = "walk-man bottom-illusration">
           <img src = "img/Login-walk-man.png"/>
        </div>
        <div class= "walk-man-msg bottom-illusration">
          <img src = "img/Login-walk-shadow.png"/>
        </div>
        <div class = "msg-box-boy banner-illustration">
          <img src = "img/Login-msg-icon.png"/>
        </div>
        <div class = "msg-box-boy-transparent banner-illustration">
          <img src = "img/transparent-msg-leftside-box-in-login.png"/>
        </div>
        <div class = "transparent-right-login-msg banner-illustration-right">
          <img src = "img/msg-box-right-top-loginpage.png"/>
        </div>
       <div class = "msg-box-girl banner-illustration-right-bottom">
          <img src = "img/msg-box.png"/>
        </div>
        <div class = "girl-login banner-illustration-right-bottom">
          <img src = "img/gilrs-login.png"/>
        </div>
        <div class = "msg-girl-transparent banner-illustration-right-bottom">
          <img src = "img/Login-msg-icon.png"/>
        </div>
       <div className = "login-part-form reset-login">
                <div className = "login-form reset-password-wrapper">
                   <div className = "login-title">Reset Your Password</div>
                    <div className="form-cnt">
                    {this.props.Resetpassword.initloading ? this.renderLoading() : null}
                    <ResetpasswordForm onSubmit={this.handleSubmit.bind(this)} />
                    </div>
               </div>
           </div>
      </div>
   </div>

   {/* footer part */}
   <FooterVisibly />
</div>
      </section>
    )
  }
}
function mapStateToProps(state) {
  return { Resetpassword: state.auth }
}

function mapDispatchToProps(dispatch) {
  return {
    loginActions: bindActionCreators(loginActions, dispatch)
  }
}

let connection = connect(mapStateToProps, mapDispatchToProps)

module.exports = connection(Resetpassword)
