import { connect } from 'react-redux'
import { Field, reduxForm } from 'redux-form'
import { renderField } from '../Helpers/ReduxForm/RenderField'
import PreLoader from '../PreLoader'
var showMenu = false
const required = value => (value ? undefined : 'Required')
class SSOForm extends React.Component {
  constructor (props) {
    super(props)
    this.state = {}
  }
  render () {
    
    const { pristine, handleSubmit, reset, submitting } = this.props

    return (
      <div class='login-form'>
        <div class='login-title'>Enter SSO Name</div>
        <div class='form-cnt'>
          <div class='loginform-mainbox'>
            <section class='forgotpass clearfix'>
              <form role='form' onSubmit={handleSubmit}>
               <div class = "contained">
                <div className='switchpopup overlay' id='switch-login'>
                  <div class='form-row  select'>
                    <Field
                      placeholder='SSO Name'
                      component={renderField}
                      type='text'
                      name='sso_name'
                      validate={required}
                    />
                  </div>
                  <div className="sso-form-login">
                  <button
                    type='submit'
                    class='btn btn-primary btn-login'
                    id='btn-space-login'
                    disabled={pristine || submitting}
                  >
                    Login with SSO
                  </button>
                  </div>
                  <div className="sso-back-link">
                  <a onClick={this.props.renderSimpleLogin}><i class="material-icons">
arrow_back_ios
</i> Back</a>
</div>
                </div>
              </div>
              </form>
            </section>
          </div>
        </div>
      </div>
    )
  }
}

function mapStateToProps (state) {
  return {}
}

function mapDispatchToProps (dispatch) {
  return {}
}

let connection = connect(mapStateToProps, mapDispatchToProps)

var reduxConnectedComponent = connection(SSOForm)

export default reduxForm({
  form: 'SSOForm' // a unique identifier for this form
})(reduxConnectedComponent)
