import { Field, reduxForm } from 'redux-form'
import { renderField } from '../Helpers/ReduxForm/RenderField'
import { connect } from 'react-redux'
import { reset } from 'redux-form'
const required = value => (value ? undefined : 'Required')
const validEmail = value =>
  (value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
    ? 'Invalid email address'
    : undefined)
const validate = values => {
  const errors = {}
  if (!values.newpassword) {
    errors.newpassword = 'Required'
  } else if (values.newpassword.length < 6) {
    errors.newpassword = 'Must be 6 characters or long.'
  } else if (!/(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/i.test(values.newpassword)) {
    errors.newpassword =
      'Password must contain one uppercase letter,one special character and one number'
  }

  if (!values.confirmpassword) {
    errors.confirmpassword = 'Required'
  } else if (values.confirmpassword != values.newpassword) {
    errors.confirmpassword = 'Both Password do not match.'
  }
  return errors
}
class ResetpasswordForm extends React.Component {
  constructor (props) {
    super(props)
    const { handleSubmit, pristine, submitting, reset } = props
  }
  render () {
    return (

      
      <form role='form' onSubmit={this.props.handleSubmit}>
      
        <div className='section'>
          <Field
            name='newpassword'
            type='password'
            component={renderField}
            
            placeholder="New Password"
          />
          <Field
            name='confirmpassword'
            type='password'
            component={renderField}
           
            placeholder="Confirm Password"
          />
          <div className='form-row'>
            <button type='submit' className='btn btn-primary btn-login btn-reset'>
              Submit
            </button>
          </div>
        </div> 
      </form>
    )
  }
}
function mapStateToProps (state) {
  return {}
}

function mapDispatchToProps (dispatch) {
  return {}
}
const afterSubmit = (result, dispatch) => {
  dispatch(reset('ResetpasswordForm'))
}
let connection = connect(mapStateToProps, mapDispatchToProps)

var reduxConnectedComponent = connection(ResetpasswordForm)

export default reduxForm({
  form: 'ResetpasswordForm',
  onSubmitSuccess: afterSubmit,
  validate
})(reduxConnectedComponent)
