import { reduxForm, Field } from 'redux-form';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { browserHistory } from 'react-router';
import LoginForm from './LoginForm';
import SSOForm from './SSOForm';
import PreLoader from '../PreLoader'
import ForgotPassword from './ForgotPassword'
import * as authActions from "../../actions/authActions"
import SliderPopup from '../SliderPopup'
import * as utils from '../../utils/utils'
import VisiblyHeader from '../Layout/Header/VisiblyHeader';
import FooterVisibly from '../Layout/Footer/FooterVisibly';
import * as loginActions from "../../actions/loginActions"
import * as generalActions from '../../actions/generalActions'
import AccountSelection from './AccountSelection'



var ip_address='';
var ssoDataLink=null
class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      forgotpass_popup: false,
      callAccountSelection:false,
      openSSOForm: false,
    }
 }


  handleFormSubmit(e) {
    ip_address=typeof this.props.general.clientIpDetail !=='undefined'? this.props.general.clientIpDetail.ip:''
    this.props.loginActions.userLogin(e,ip_address);
  }
  handleForgotPassword() {
    this.setState({ forgotpass_popup: true })
    document.body.classList.add('overlay')
  }
  /**
  * Handle open sso login form
  * @author Yamin
  **/
  handleOpenSSOForm(){
    this.setState({openSSOForm:true})
  }
  /**
  * Handle sso login form submit
  * @author Yamin
  **/
  handleSSOLogin(values){
    this.props.loginActions.ssoLogin(values);
  }
  renderSimpleLogin(){
    this.setState({openSSOForm:false})
  }
  closeForgotpassPopup() {
    this.setState({ forgotpass_popup: false })
    document.body.classList.remove('overlay')
  }
  componentWillMount() {
    this.props.generalActions.fetchClientIp();
    document.body.classList.add("page-login");
  }

  componentWillUnmount() {
    document.body.classList.remove("page-login");
  }

  componentDidMount() {
    
    var role = {}
    this.props.generalActions.fetchClientIp()
    document.title = 'Login / Visibly';
    if (localStorage.getItem('session_id') !== null) {
      if (this.props.auth.userinfo !== undefined || this.props.profile.profile.email !== null && this.props.auth.authenticated == true) {
        var onboard_step = this.props.auth.userinfo !== undefined ? this.props.auth.userinfo.onboard_step : this.props.profile.profile.onboard_step
        role = this.props.auth.userinfo !== undefined ? this.props.auth.userinfo.role : this.props.profile.profile.role
        var userData=this.props.auth.userinfo !== undefined ? this.props.auth.userinfo : this.props.profile.profile
        var sso_onboarding = this.props.auth.userinfo !== undefined ? this.props.auth.userinfo.sso_onboarding : this.props.profile.profile.sso_onboarding
        if (role !== null && role !== undefined && role.data.length > 0) {
          var role_name=role.data[0].role_name
          if (role_name == 'super-admin') {
            if (onboard_step !== undefined) {
              if (onboard_step < 2) {
                if (onboard_step == 0) {
                  browserHistory.push("/register");
                } else if (onboard_step == 1) {
                  browserHistory.push("/register/step2");
                }
              } else {
                this.isTourDone(userData)
              }
            }
          } else if (role_name == 'guest') {
            if (userData.onboard_step < 1) {
              browserHistory.push("/guest-signup");
            }else {
            if(this.props.auth.feed_identity!==null){
              var objToSend={
                isForGuest:true,
                props:this.props.auth
              } 
              this.isTourDone(userData,objToSend)
            }
          }
        } else if(role_name == 'employee'){
          if(sso_onboarding == false){
            browserHistory.push("/registerSsoUser");
          }else{
            this.isTourDone(userData)
          }
        } else{
          this.isTourDone(userData)
        }
      }
    }
  }
}
/**
 * check tour is skipped or not and redirect to feed/tour page
 * @param {*} userData props.profile
 * @param {*} guestData props
 */
  isTourDone(userData, guestData = null) {
    var currAlbumName = localStorage.getItem('albumName')
    var userTourDetail = JSON.parse(localStorage.getItem(`${userData.identity}_${currAlbumName}`))
    if (userTourDetail !== null) {
      if (userTourDetail.isTourSkipped == false) {
        browserHistory.push("/tour");
      } else {
        //tour is skipped 
        if (guestData !== null && guestData.isForGuest == true) {
          //if method is calling for guest then redirect to custom feed page
          browserHistory.push(`/feed/custom/live/${guestData.props.feed_identity}`);
        } else {
          browserHistory.push("/feed/default/live");
        }
      }
    } else {
      //user tour data is not available in localsotrage thn redirect to tour page
      browserHistory.push("/tour");
    }
  }
  componentWillReceiveProps(newProps) {
    /*call setaccount api if user comes from sso login*/
    var url = window.location.search
    url = url.replace("?", ''); 
    var splitUrl = url.split("="); 
    url = url.substring(9)
    if(this.props.general.clientIpDetail !== newProps.general.clientIpDetail && splitUrl[0] == 'sso_data'){
      var ip_address=typeof newProps.general.clientIpDetail !=='undefined'? newProps.general.clientIpDetail.ip:''
      
      this.props.authActions.authUser(null,null,ip_address,url);
    }
    if(typeof splitUrl[0] !== "undefined" &&  splitUrl[0] == "sso_data"){
      ssoDataLink = url
    }else{
      ssoDataLink = null
    }
    var onboard_step=0 
    var sso_onboarding= ''
    var role={}
    if(newProps.auth.isLoginDone==false && newProps.auth.feed_identity!==undefined && newProps.auth.feed_identity!==null && newProps.auth.isLogoutClicked == false){
      var userData=newProps.auth.userinfo !==undefined ? newProps.auth.userinfo :newProps.profile.profile
      if(newProps.auth.feed_identity!==null){
        var objToSend={
          isForGuest:true,
          props:newProps.auth
        } 
        this.isTourDone(userData,objToSend)
      }
    }
    else {
      if ((newProps.auth.feed_identity !== null && newProps.auth.isLogoutClicked == false) || (newProps.auth.userinfo !== undefined && newProps.auth.userinfo !== this.props.auth.userinfo) || (newProps.profile.profile.email !== null && newProps.profile.profile.email !== this.props.profile.profile.email)) {
        role = newProps.auth.userinfo !== undefined ? newProps.auth.userinfo.role : newProps.profile.profile.role
        var userData = newProps.auth.userinfo !== undefined ? newProps.auth.userinfo : newProps.profile.profile
        if (role !== null && role !== undefined && role.data.length > 0) {
          onboard_step = newProps.auth.userinfo !== undefined ? newProps.auth.userinfo.onboard_step : newProps.profile.profile.onboard_step
          sso_onboarding = newProps.auth.userinfo !== undefined ? newProps.auth.userinfo.sso_onboarding : newProps.profile.profile.sso_onboarding
          var role_name = role.data[0].role_name
          if (role_name == 'super-admin') {
            this.props.loginActions.storeStepNumber(true)
            // check for auth state in initial render
            if (onboard_step < 2) {
              if (onboard_step == 0) {
                browserHistory.push("/register");
              } else if (onboard_step == 1) {
                browserHistory.push("/register/step2");
              }
            } else {
              this.isTourDone(userData)
            }
          } else if (role_name == 'guest') {
            if (userData.onboard_step < 1) {
              browserHistory.push("/guest-signup");
            } else {
              var objToSend = {
                isForGuest: true,
                props: newProps.auth
              }
              if (newProps.auth.feed_identity !== null) {
                this.isTourDone(userData, objToSend)
              }
            }
          } else if(role_name == 'employee'){
            if(sso_onboarding !== '' && sso_onboarding == false){
              browserHistory.push("/registerSsoUser");
            }else{
              this.isTourDone(userData)
            }
          } else{
            this.isTourDone(userData)
          }
        } else {
          //don't add any browser history to redirect on feed or any page will will call api bcz it will refresh page on logout as session is destroyed
        }
      }
    }
    this.props.auth.forgotpasswordReset ? this.closeForgotpassPopup() : '';

    if (newProps.auth.isLoginDone == true && newProps.auth.isLoginDone !== this.props.auth.isLoginDone) {
      var domainList = newProps.auth.domainList.length > 0 ? newProps.auth.domainList : ''
      if (domainList.length > 1 && domainList !== '') {
        //if user is belongs to only one domain then to direct login || if user has multiple domain then and then give option to select domain list
        this.setState({
          callAccountSelection: true
        })
      }
    }
  }
  renderPasswordCreation() {
    return (
      <SliderPopup>
        <button
          id='close-popup'
          className='btn-default'
          onClick={this.closeForgotpassPopup.bind(this)}
        > <i className='material-icons'>clear</i></button>
        <ForgotPassword></ForgotPassword>
      </SliderPopup>
    );
  }
  //this will display loading page 
  callpageloader(){
    var min=0; 
    var max=document.querySelectorAll('.quote-wrapper .quote').length;  
    var random =Math.floor(Math.random() * (+max - +min)) + +min; 
    var quote = document.querySelectorAll('.quote-wrapper .quote');
    if(quote != undefined && quote.length!=0){
      quote[random].classList.add('show');
    }
    return(
      <div class="initial-loading">
            
           <div class="visibly-splash-wrapper">
            <div class="visibly-splash">
              <img src="/img/visibly-loader.gif" alt="visibly-loader" class="visibly-splash-loader" />
            <svg class="nav__image image" xmlns="https://www.w3.org/2000/svg" viewBox="0 0 374.68 95.95">
                    <defs>
                       
                    </defs>
                    <g data-name="Layer 2">
                        <g data-name="Layer 1">
                            <path class="cls-1" d="M147.92,23.56H164.1V73.12H147.92ZM156,0a8.71,8.71,0,0,0-6.57,2.63,9,9,0,0,0-2.53,6.47,9,9,0,0,0,2.53,6.47,9.53,9.53,0,0,0,13.15,0,9,9,0,0,0,2.53-6.47,9,9,0,0,0-2.53-6.47A8.71,8.71,0,0,0,156,0Zm48,46.32a23.57,23.57,0,0,0-5.11-3q-2.73-1.16-5-2-2.93-1.11-4.8-1.87A2.58,2.58,0,0,1,187.31,37a2.2,2.2,0,0,1,1.57-2.28,14.11,14.11,0,0,1,4.5-.56,26.17,26.17,0,0,1,6.37.76,35.13,35.13,0,0,1,5.76,2l2.22-10.62a26.5,26.5,0,0,0-7.38-2.88,39,39,0,0,0-8.5-.86q-8.8,0-14.26,3.84t-5.46,11.93A13.46,13.46,0,0,0,173.25,44a13.78,13.78,0,0,0,3.08,4.3,19.3,19.3,0,0,0,4.7,3.24A51.86,51.86,0,0,0,186.91,54q3.84,1.42,5.76,2.17a2.65,2.65,0,0,1,1.92,2.58,3,3,0,0,1-2.17,2.88,13.84,13.84,0,0,1-5.11.86,39.37,39.37,0,0,1-6.62-.66,32,32,0,0,1-6.93-2L171.53,71.1a37.27,37.27,0,0,0,8.09,2.28,50.62,50.62,0,0,0,8.5.76,32,32,0,0,0,8.7-1.11,20.38,20.38,0,0,0,6.83-3.29,14.73,14.73,0,0,0,4.45-5.41,16.73,16.73,0,0,0,1.57-7.38A11.47,11.47,0,0,0,208,50.72,16.64,16.64,0,0,0,204,46.32Zm12.37,26.8H232.6V23.56H216.42ZM224.51,0a8.71,8.71,0,0,0-6.57,2.63A9,9,0,0,0,215.4,9.1a9,9,0,0,0,2.53,6.47,9.53,9.53,0,0,0,13.15,0,9,9,0,0,0,2.53-6.47,9,9,0,0,0-2.53-6.47A8.71,8.71,0,0,0,224.51,0Zm66.4,37.72a32.79,32.79,0,0,1,1.52,10.21,29.18,29.18,0,0,1-1.77,10.16,22.79,22.79,0,0,1-5.31,8.34,25.26,25.26,0,0,1-8.9,5.61,34.69,34.69,0,0,1-12.54,2.07,49.83,49.83,0,0,1-11.58-1.31,44.81,44.81,0,0,1-10.06-3.64V1.72h16.18V27.41a17.33,17.33,0,0,1,5.11-3.49,18.13,18.13,0,0,1,7.53-1.37,20.28,20.28,0,0,1,8.8,1.87,19.32,19.32,0,0,1,6.73,5.26A24.52,24.52,0,0,1,290.91,37.72ZM276.24,48.14q0-6.07-2.38-9.66t-7.43-3.59a12.47,12.47,0,0,0-8,2.73V61.08a12,12,0,0,0,2.63.81,16,16,0,0,0,3.14.3,12.53,12.53,0,0,0,5.46-1.11,9.8,9.8,0,0,0,3.74-3,13.62,13.62,0,0,0,2.12-4.45A20.23,20.23,0,0,0,276.24,48.14ZM319,62q-2.33,0-3.13-1.06a4.82,4.82,0,0,1-.81-3V1.72H298.9v58a19.93,19.93,0,0,0,.71,5.31,12.6,12.6,0,0,0,2.33,4.6,11.32,11.32,0,0,0,4.35,3.24,16.68,16.68,0,0,0,6.78,1.21,39.12,39.12,0,0,0,5.31-.35,30.72,30.72,0,0,0,4.6-1l-.51-11.12A23.8,23.8,0,0,1,319,62ZM358.4,23.56,334.93,96h16.28l23.46-72.39ZM98.09,22H81.3L99.2,72.75l6.55-21Zm4.24,50.77h16.28L142.07.37H125.79ZM345,53.29l-7.67-29.73H320.51l17.9,50.77Z"></path>
                            <path class="cls-2" d="M17.21,22V49.66H40.52V72.71H67.85V22ZM57.73,62.58H50.64V39.52H27.33v-7.4h30.4Z"></path>
                            <path class="cls-3" d="M50.64,90.25V62.57H27.33V39.52H0V90.25ZM10.12,49.66h7.09V72.71H40.52v7.4H10.12Z"></path>
                        </g>
                    </g>
                </svg>
            </div>
            </div>
        </div>
    )
}
/**
 * @author disha
 * @param {*} value 
 * this method will call when account name is selected from dropdown of domain list
 */
handleAccountSelection(value){
    ip_address=typeof this.props.general.clientIpDetail !=='undefined'? this.props.general.clientIpDetail.ip:''
    this.props.authActions.authUser(value,this.props.auth.userEmailPassword,ip_address);
}
/**
 * @author disha
 * to open
 */
openLoginPopup(){
  var element = document.getElementById("switch-login");
  element.classList.remove("hide");
 }
render() {
    const { handleSubmit } = this.props;
    let { loading } = this.props.auth;
return (
<div class = "login-page">
 <VisiblyHeader />
{loading || this.props.general.iploading || this.props.auth.LoginLoading==true || ssoDataLink !== null ? <PreLoader className="Page" /> : null}
{(this.props.profile.profile.email==null && localStorage.getItem('session_id')!==null) || ssoDataLink !== null?this.callpageloader():''}
<div>   {this.state.forgotpass_popup ? this.renderPasswordCreation() : ''}</div>
  <div class= "banner-part" id = "login-banner">
     <div class = "inner-banner-part">
       <div class = "walk-man bottom-illusration">
           <img src = "img/Login-walk-man.png"/>
        </div>
        <div class= "walk-man-msg bottom-illusration">
          <img src = "img/Login-walk-shadow.png"/>
        </div>
        <div class = "msg-box-boy banner-illustration">
          <img src = "img/Login-msg-icon.png"/>
        </div>
        <div class = "msg-box-boy-transparent banner-illustration">
          <img src = "img/transparent-msg-leftside-box-in-login.png"/>
        </div>
        <div class = "transparent-right-login-msg banner-illustration-right">
          <img src = "img/msg-box-right-top-loginpage.png"/>
        </div>
       <div class = "msg-box-girl banner-illustration-right-bottom">
          <img src = "img/msg-box.png"/>
        </div>
        <div class = "girl-login banner-illustration-right-bottom">
          <img src = "img/gilrs-login.png"/>
        </div>
        <div class = "msg-girl-transparent banner-illustration-right-bottom">
          <img src = "img/Login-msg-icon.png"/>
        </div>
         <div className = {`login-part-form clearfix ${this.state.callAccountSelection || this.state.openSSOForm ?'show-account':''}`}> 
        <div class="login-form-slider-select-account clearfix">
                <div className = "login-form">
                   <div className = "login-title">Login<span>to your account</span></div>
                    <div className="form-cnt">
                        {
                          <LoginForm 
                            onSubmit={this.handleFormSubmit.bind(this)} 
                            handleForgotPassword={this.handleForgotPassword.bind(this)} 
                            handleOpenSSOForm={this.handleOpenSSOForm.bind(this)} 
                            renderPasswordCreation={this.renderPasswordCreation.bind(this)} 
                            props={this.props}>
                         </LoginForm>
                        }
                        {/* {this.state.callAccountSelection?this.openLoginPopup():''} */}
                        
                    </div>
               </div>
               {this.state.callAccountSelection ?
               <AccountSelection props={this.props} onSubmit={this.handleAccountSelection.bind(this)}/> 
               :''}
               {this.state.openSSOForm ?
               <SSOForm
                onSubmit={this.handleSSOLogin.bind(this)}
                renderSimpleLogin={this.renderSimpleLogin.bind(this)} 
               /> 
               :''}

               </div>
           </div>
          
      </div>
     
   </div>

   {/* footer part */}
 
</div>
    );
  }
}



function mapStateToProps(state) {
  return {
    general:state.general,
    profile:state.profile,
    auth: state.auth
  }
}
function mapDispatchToProps(dispatch) {
  return {
    generalActions:bindActionCreators(generalActions,dispatch),
    loginActions:bindActionCreators(loginActions,dispatch),
    authActions: bindActionCreators(authActions, dispatch)
  }
}
let connection = connect(mapStateToProps, mapDispatchToProps);
module.exports = connection(Login);