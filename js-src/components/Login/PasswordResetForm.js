
import { Field, reduxForm } from 'redux-form'
import { renderField } from '../Helpers/ReduxForm/RenderField'
import { reset } from 'redux-form';

const validate = values => {
  const errors = {}
  if (!values.newpassword) {
    errors.newpassword = 'Required'
  } else if (values.newpassword.length < 6) {
    errors.newpassword = 'Must be 6 characters or long.'
  }else if (!/(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/i.test(values.newpassword)) {
    errors.newpassword = 'Password must contain one uppercase letter,one special character and one number'
  }

   if (!values.confirmpassword) {
    errors.confirmpassword = 'Required'
  } else if (values.confirmpassword != values.newpassword) {
    errors.confirmpassword = 'Both Password do not match.'
  }

  return errors
}

const submitMyForm = data => {
  const {createRecord, resetForm} = this.props;
  return createRecord(data).then(() => {
    resetForm();
    // do other success stuff
  });
}

const PasswordResetForm = (props) => {
  const { handleSubmit, submitMyForm,pristine, reset, submitting } = props
  return (
    <form onSubmit={handleSubmit(submitMyForm.bind(this))} role="form" >
      <div className="section">
        <Field name="newpassword" type="password" component={renderField} label="New Password"/>
        <Field name="confirmpassword" type="password" component={renderField} label="Confirm Password"/>
        <div className="form-row">
          <button type="submit" className="btn btn-primary btn-login">Submit</button>
        </div>
      </div>
    </form>
  )
}

//reset form after submission
const afterSubmit = (result, dispatch) =>{
  dispatch(reset('passwordReset'));

}

export default reduxForm({
  form: 'passwordReset',  // a unique identifier for this form
  onSubmitSuccess: afterSubmit,
  validate,                // <--- validation function given to redux-form
})(PasswordResetForm)
