import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import PasswordResetForm from './PasswordResetForm';
import PreLoader from '../PreLoader'

import * as loginActions from "../../actions/loginActions"

class PasswordReset extends React.Component {
  
  constructor(props) {
    super(props);
  }

  componentDidMount() {


  }

  handleSubmit(values){
    this.props.loginActions.savePassword(values, "callbackPath");
  }

  renderLoading() {
    return (
      <div className="preloader-wrap">
        <PreLoader></PreLoader>
      </div>
    )
  }

  renderPasswordSuccess() {
    return (
      <p>
        Password was successfully set.
      </p>
    )
  }


  renderPasswordFail() {
    return (
      <p>
        There was some problem setting your password.
      </p>
    )
  }


  render() {

    
    return (
      <section className="register-page" >
          
          
          <div id="content">

            <div className="page">

              <div className="esc narrow">

                { /*(loading ? this.renderLoading() : <div></div> )*/ }
               

                <header className="header">
                  <h1><i className="material-icons">description</i>	 <span className="breadcrumb">Password Reset </span> </h1>
                </header>

                <div className="content">

                    <div className="widget">

                      <header className="heading">
                        <h3>Password Reset</h3>
                      </header>
                      <div>
                        { this.props.auth.passwordSet ? this.renderPasswordSuccess():''} 
                        { this.props.auth.passwordError ? this.renderPasswordFail():''} 
                      </div>
                      <PasswordResetForm onSubmit={ this.handleSubmit.bind(this) }> </PasswordResetForm>

                    </div>


                </div>        
              </div>
            </div>
          </div>
         
        </section>
    );
  }
}

function mapStateToProps(state) {
  return {
      //addPassword: state.settings.password
      auth: state.auth
  }
}

function mapDispatchToProps(dispatch) {
  return {
      loginActions: bindActionCreators(loginActions, dispatch)

  }
}

let connection = connect(mapStateToProps, mapDispatchToProps);

module.exports =  connection(PasswordReset);