import {reduxForm, Field} from 'redux-form';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {browserHistory} from 'react-router';
import {Link} from 'react-router';
import Globals from '../../Globals';
const Line = require('rc-progress').Line;

import * as authActions from "../../actions/authActions"

class Verify extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      verified: false,
      progress: 0,
      error: false
    }
  }

  componentWillMount() {
    document.body.classList.add("page-verify");
  }

  componentWillUnmount() {
    document.body.classList.remove("page-verify");
  }

  componentDidMount() {
    // verification GET
    var queryParams = this.props.location.query;
    this.simulateProgress();


    fetch(Globals.API_ROOT_URL_MASTER + `/verify?e=${queryParams.e}&t=${queryParams.t}`)
    .then(response => {
      return response.json()
    })
    .then((json) => {
//hasOwnProperty("data")
      if(json.error==undefined) {
        // we got "data" in response, that means, it is success.
        // if we don't get "data", there will be error property in the response
        this.props.authActions.verifyUserSuccess({
          // auth_token:json.data.auth_token, 2z4d7lTn4h,EsSbgR32pH
          // api_secret:json.data.api_secret
          auth_token:json.auth_token,
          api_secret:json.api_secret
        });

        this.setState({
          verified: true
        });
      }
      else {
        this.setState({
          error: true
        })
      }

    })


  }

  componentWillUpdate(nextProps) {
    // check for auth state when state is changed
    /*if (nextProps.auth.authenticated) {
      browserHistory.push("/feed");
    }*/
  }

  /*verifyMe() {
    this.setState({
      verified: true
    });
  }*/

  renderVerified() {
    return (
        <div className="verifiedMsg">
          <h2>Congratulations! Your account is verified.</h2>
          <h4>Proceed with next step</h4>
          {/* <Link className="btn" to="/login">Login</Link> */}
          <Link className="btn" to="/register">Register</Link>
        </div>
      )
  }

  renderError() {
    return (
      <h4>
        <span className='error'>Looks like there was an error processing your request. Please try again or contact our technical department. Thanks</span>
      </h4>
    )
  }

  simulateProgress() {
    var me = this;

    var progress = window.setInterval(function(){
      var currProgressState = me.state.progress;
      var newProgressState = currProgressState + Math.random();
      if(newProgressState>70) {
        clearInterval(progress);
        return;
      }

      me.setState({
        progress: newProgressState
      })
    }, 60);
  }

  progressTexts() {
    var outputText = "Verifying your account..."
    if(this.state.progress > 10 && this.state.progress <= 30) {
      var outputText = "Building your database..."
    }
    else if(this.state.progress > 30 && this.state.progress <= 50) {
      var outputText = "Setting up your environment..."
    }
    else if(this.state.progress > 50 && this.state.progress <= 65) {
      var outputText = "Finalizing settings..."
    }
    else if(this.state.progress > 65){
      var outputText = "Still working..."
    }

    return (
      <p>
        { outputText }
      </p>
    )
  }

  renderNotVerified() {
    return (
        <div>
          <h2>Verification</h2>
          { this.state.error ? this.renderError() : this.renderProgress() }
        </div>
      )
  }

  renderProgress() {
    return (
        <div id="verification">

          { this.progressTexts() }


          <br/>
          <div style={{margin: '0 auto' ,width: '250px'}}>
            <Line percent={this.state.progress} strokeWidth="4" strokeColor={'#256eff'} trailWidth="4" trailColor={'#ebeef0'} strokeLinecap={'square'} />
          </div>

          <br/>
        </div>
      )
  }

  render() {

    return (
      <section className="verify">
        <div id="content">

            <div className="page">

              <div className="esc narrow">

                <header className="header">
                  <h1><i className="material-icons">description</i>	 <span className="breadcrumb">Verify </span> </h1>
                </header>

                <div className="content center">

                    <div className="widget">

                      <section className={this.state.verified ? 'section verified' : 'section'}>

                        {!this.state.verified ? this.renderNotVerified() : <div></div>}


                        <svg className={this.state.verified ? 'checkmark active' : 'checkmark'} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52"><circle className="checkmark__circle" cx="26" cy="26" r="25" fill="none"/><path className="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8"/></svg>

                        {this.state.verified ? this.renderVerified() : <div></div>}
                      </section>

                    </div>


                </div>
              </div>
            </div>
          </div>
      </section>

    );
  }
}

function mapStateToProps(state) {
  return {
    auth: state.auth
  }
}

function mapDispatchToProps(dispatch) {
  return {
    authActions: bindActionCreators(authActions, dispatch)
  }
}

let connection = connect(mapStateToProps, mapDispatchToProps);

module.exports =  connection(Verify);
