import Slider from './Chunks/ChunkReactSlick'
import ChunkJqueryDropdown from './Chunks/ChunkJqueryDropdown'
import $ from './Chunks/ChunkJquery'
import * as loginActions from '../actions/loginActions';
import { connect } from 'react-redux'
import { reduxForm } from 'redux-form'
import { bindActionCreators } from 'redux'
import { browserHistory } from 'react-router'
import * as profileSettings from '../actions/settings/getSettings'
var completedTourStep = 0
var totalSlides = 5
var isUserSuperAdminOrAdmin = false
var currantSlideNumber=0
class VisiblyTour extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      currentSlide: 0,
      noUseOfThisState:'',
      Slider:null
    }
  }
  componentWillMount() {
    //to set the flag if user role is admin or super-admin
    currantSlideNumber=1
    let roleName = JSON.parse(localStorage.getItem('roles')) !== null ? JSON.parse(localStorage.getItem('roles'))[0].role_name : ''
    if (roleName == 'admin' || roleName == 'super-admin') {
      isUserSuperAdminOrAdmin = true
    } else {
      isUserSuperAdminOrAdmin = false
    }

    $().then(jquery => {
      var $ = jquery.$
      ChunkJqueryDropdown().then(jqueryDropdown => { })
    })
    Slider().then(slider=>{
      this.setState({
        Slider:slider
      })
    })
  }
  componentDidMount() {
    this.initializeTourStep(this.props)
    // this.redirectToOnBoarding(this.props)
    // this.props.profileSettings.GetProfileSettings()
  }
  componentWillReceiveProps(newprops) {
    if (newprops.profile.profile.email !== null && this.props.profile.profile !== newprops.profile.profile) {
      this.initializeTourStep(newprops)
      this.redirectToOnBoarding(newprops)
    }
  }
  /**
   * to redirect back on onboarding page if onboarding steps are not completed
   * @param {*} newprops 
   * @author disha
   */
  redirectToOnBoarding(newprops) {
    var profileData = newprops.profile.profile.email !== null ? newprops.profile.profile : newprops.auth.userinfo !== undefined ? newprops.auth.userinfo : ''
    if (profileData !== null) {
      var onboard_step = profileData.onboard_step
      var role_name = profileData.role !== undefined ? profileData.role.data !== undefined ? profileData.role.data[0].role_name : '' : ''
      if (role_name == 'super-admin') {
        if (onboard_step !== undefined) {
          if (onboard_step < 2) {
            if (onboard_step == 0) {
              browserHistory.push("/register");
            } else if (onboard_step == 1) {
              browserHistory.push("/register/step2");
            }
          }
        }
      } else if (role_name == 'guest') {
        if (onboard_step < 1) {
          browserHistory.push("/guest-signup");
        }
      }
    }
  }
 /**
  * to initialise tour step and redirect page on tour or feed if tour is not done
  * @param {*} newprops 
  * @author disha
  */
  initializeTourStep(newprops) {
    var feedId = newprops.auth.feed_identity !== null ? newprops.auth.feed_identity : ''
    var props = newprops.profile.profile.email !== null ? newprops.profile.profile : newprops.auth.userinfo !== undefined ? newprops.auth.userinfo : ''
    var currAlbumName = localStorage.getItem('albumName')
    var userTourDetail = JSON.parse(localStorage.getItem(`${props.identity}_${currAlbumName}`))
    if (userTourDetail !== null) {
      //check tour is skipped or not
      if (userTourDetail.isTourSkipped == false) {
        if (userTourDetail.tourStep !== null && userTourDetail.tourStep !== undefined && (userTourDetail.tourStep !== '0' || userTourDetail.tourStep !== 0)) {
          //to redirect on step from where user was left
          this.BackAndNextSlide(userTourDetail.tourStep, 'next', props)
        } else {
          //to set tour step to one as default
          if (document.getElementById(`tab1`) !== null && document.getElementById(`tab1`) !== undefined) {
            document.getElementById(`tab1`).classList.add('active')
          }
        }
      } else {
        //when back button is pressed to go to prev page in browser and last page was tour then to redirect back to feed page
        //to redirect on feed page if tour is skipped
        if (userTourDetail.isTourRestart == false) {
          if (props.role !== null && props.role !== undefined && props.role.data.length > 0) {
            var role_name = props.role.data[0].role_name
            if (role_name == 'guest') {
              browserHistory.push(`/feed/custom/live/${feedId}`)
            } else {
              browserHistory.push('/feed/default/live')
            }
          }
        }
      }
    } else {
      browserHistory.push("/tour");
    }
  }
  /**
   * call this method when clicked on back or next button and move slide according to that
   * @param {*} clickedSlideId id num 
   * @param {*} buttonType next / back
   * @param {*} profile props.profile
   * @author disha
   */
  BackAndNextSlide(clickedSlideId, buttonType, profile = null) {
    var nextSlide = 0
    //i set state here bcz withour state change my render was not calling and my global flag value was not applying and my state value update 1 step leter so i cant take this
    this.setState({
      noUseOfThisState:''
    })
    if (buttonType == 'back') {
      nextSlide = parseInt(clickedSlideId) - 1
      currantSlideNumber=nextSlide
    } else {
      //if next button is pressed  then increament currant id 
      nextSlide = parseInt(clickedSlideId) + 1
      currantSlideNumber=nextSlide
      this.completedTourSteps(clickedSlideId, profile)
    }
    //if all slides are finished thn to redirect back to feed page
    if (clickedSlideId == totalSlides) {
      if (buttonType == 'next') {
        this.skipTour(clickedSlideId,profile)
      }
    }
    if(document.getElementsByClassName('tour-tab') !== null){
      var allTourTab=document.getElementsByClassName('tour-tab')
      for(var i=1;i<=allTourTab.length;i++){
        document.getElementById(`tab${i}`).classList.remove('completed-tab')
      }
    }
    for(var i=0;i<currantSlideNumber;i++){
      if (document.getElementById(`tab${i}`) !== null && document.getElementById(`tab${i}`) !== undefined) {
        document.getElementById(`tab${i}`).classList.add('completed-tab')
      }
    }
    if (document.getElementById(`tab${nextSlide}`) !== null && document.getElementById(`tab${nextSlide}`) !== undefined) {
      document.getElementById(`tab1`).classList.remove('active')
      document.getElementById(`tab${nextSlide}`).classList.add('active')
      this.slider.slickGoTo(nextSlide -1)
    }
  }
  /**
   * when clicked on skip button or finish button then to redirect to feed page
   * @param {*} stepNum step number
   * @author disha
   */
  skipTour(stepNum,profile=null) {
    var isFromSkip = true
    var profileUser = this.props.auth.userinfo !== undefined ? this.props.auth.userinfo : this.props.profile.profile.email !== null ? this.props.profile.profile : profile !== null ? profile:''
    if (profileUser.role !== null && profileUser.role !== undefined && profileUser.role.data.length > 0) {
      var role_name = profileUser.role.data[0].role_name
      if (role_name == 'guest') {
        browserHistory.push(`/feed/custom/live/${this.props.auth.feed_identity}`)
      } else {
        browserHistory.push('/feed/default/live')
      }
    }
    this.completedTourSteps(totalSlides, profileUser, isFromSkip)
  }

  /**
   * to make entry in localstorage
   * @param {*} stepNum 
   * @param {*} profile props
   * @param {*} isFromSkip if this method is called from skipTOur method thn this falsh will true
   */
  completedTourSteps(stepNum, profile = null, isFromSkip = null) {
    var profileUserId = this.props.auth.userinfo !== undefined ? this.props.auth.userinfo.identity : this.props.profile.profile.email !== null ? this.props.profile.profile.identity : profile.email !== null ? profile.identity : ''
    var albumName = localStorage.getItem('albumName')
    var userTourDetail = JSON.parse(localStorage.getItem(`${profileUserId}_${albumName}`))
    var isTourSkippedValue=false
    if(userTourDetail !== null ){
      if((userTourDetail.isTourRestart == true || userTourDetail.isTourSkipped == true)  ){
        isTourSkippedValue=true
      }
    }else if(isFromSkip == true){
      isTourSkippedValue=true
    }else{
      isTourSkippedValue=false
    }
    var objToAdd = {
      'albumName': albumName,
      'userid': profileUserId,
      'tourStep': stepNum,
      'isTourRestart': false,
      'isTourSkipped': isTourSkippedValue
    }
    localStorage.setItem(`${profileUserId}_${albumName}`, JSON.stringify(objToAdd));
  }
  logoutClick() {
    this.props.loginActions.callLogoutApi();
  }
  handlePlayStoreClick(type){
    if(type=="playstore"){
      window.open('https://play.google.com/store/apps/details?id=com.visibly.app&hl=en', '_blank');
    }else if(type=="appstore"){
      window.open('https://apps.apple.com/in/app/visibly/id1443991581', '_blank');
    }
    
  }
  render() {
    const settings = {
      dots: false,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: true,
      infinite: false,
      slickGoTo: 2,
      responsive: [
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: false,
          }
        },
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
    }
    var TabsNames = []
    if (isUserSuperAdminOrAdmin == true) {
      totalSlides = 5
      TabsNames = ['DOWNLOAD MOBILE APP', 'UPLOAD ASSET', 'CREATE POST', 'ADD USERS', 'CONNECT & FINISH']
    } else {
      totalSlides = 4
      TabsNames = ['DOWNLOAD MOBILE APP', 'UPLOAD ASSET', 'CREATE POST', 'CONNECT & FINISH']
    }
    return (
      <div className="main-container-web-tour">
       <div className = "inner-main-container-web-tour">
        {/* logo part */}
        <div className="cf">
          <div className="container-logo">
            <a href="#"><img src="img/logo.png" /></a>
          </div>
          <div className = "logout-for-skip-tour">
              <a target="_blank" title='Logout' onClick={this.logoutClick.bind(this)} className="btn btn-primary logout-btn-onboading">
                <span> <span className = "logout-text-for-tour"> Logout </span> <i class="material-icons">power_settings_new</i> </span>
              </a>
           </div> 
           
        </div>
        <div className="main-body-tour-container">
          {/* header part */}
          <div className="header-block-tour cf">
          <div className="tourMainTitle"><p className="header-block-font"> <span className="header-block-bold-font"> Take the tour -  </span>  Understand Visibly in 2 mins...</p> </div>
            <div className="skiptour"><a className="link-font" target="_blank" onClick={this.skipTour.bind(this)}> Skip tour</a></div>
          </div>
          {/* tabing part */}
          <div className="tabing-tour-part-container">
            <div className="clearfix tabing-inner-container">
              {TabsNames.map((tabs, index) => {
                var finalIndex = index + 1
                var activeTab = `tab${finalIndex}` == 'tab1' ? 'active' : ''
                //
                return (
                  <div className={`tab${finalIndex} tour-tab ${activeTab}` }  id={`tab${finalIndex}`} key={index}>  <div className="inner-content-block-tour"> <span className = "final-index-container"> <span className = "final-index-tab">{finalIndex}</span> <span className = "final-index-icon"> <i class="material-icons">done</i>  </span> </span>  <p>{tabs}</p> </div> </div>
                )
              })}
            </div>
          </div>

          {/* body part */}
          {/* block1 */}
          <div className = "main-container-of-tour-of-silder">
            <div className="main-container-of-tour-of-silder-inner clearfix">
          {this.state.Slider?
          <this.state.Slider.default {...settings} ref={slider => (this.slider = slider)}>
            <div className="content-container-tour  download-block-part-tour" id={1}>

              <div className="inner-content-container">

                <div className="content-body-download-tour title-main-content-tour clearfix">
                  <div className="step-content"> <p className="content-title-header"> STEP 1 OF {totalSlides} </p> </div>
                  <div className="heading-tour title-tour"><p className="content-title-header"> DOWNLOAD MOBILE APP</p></div>

                </div>
                <div className="block-part-content-tour clearfix">

                  <div className="col-5 text-tour-content">
                    <div className="ul-container">
                      <ul>
                        <li className="general-content-body-font"> <span className="general-content-bold-font">Download the Visibly phone APP </span>to your mobile from Google Play or App Store. </li>
                        <li className="general-content-body-font"> <span className="general-content-bold-font"> Login to the Visibly phone APP </span>using the  credentials we sent you to login to the account you are in now.</li>
                      </ul>
                    </div>
                    <div className="playstoreimg clearfix">
                      <div className="googleplay-store google-store-img-part"> <img src="img/playstore.png" onClick={this.handlePlayStoreClick.bind(this,"playstore")}/> </div>
                      <div className="googleplay-img google-store-img-part">  <img src="img/download-app-store-img.png" onClick={this.handlePlayStoreClick.bind(this,"appstore")} /></div>

                    </div>
                  </div>
                  <div className="col-5 giphy-tour-content">
                    <div className="giphy-container mobile-giphy"> <img src="img/Download-VS-APP.gif" /> </div>
                    {/* <div className="giphy-container"> <img src="img/giphy-background.jpg" /> </div> */}
                  </div>

                </div>

                <div className="footer-part-of-content-tour clearfix">
                  {/* <a className="btn btn-primary right-button-tour button-tour  nextTour" id={`next${1}`} onClick={this.BackAndNextSlide.bind(this, 1, 'next')} > Next</a> */}
                </div>
              </div>
            </div>
            {/* end block1 */}
            {/* start block2 */}
            <div className="content-container-tour  download-block-part-tour " id={2}>

              <div className="inner-content-container">

                <div className="content-body-download-tour title-main-content-tour clearfix">
                  <div className="step-content"> <p className="content-title-header"> STEP 2 OF {totalSlides} </p> </div>
                  <div className="heading-tour title-tour"><p className="content-title-header">UPLOAD ASSET</p></div>

                </div>
                <div className="block-part-content-tour clearfix">

                  <div className="col-5 text-tour-content">
                    <div className="ul-container">
                      <ul>
                        <li className="general-content-body-font uploaded-assets-text-icon">Click on the  <span className="general-content-bold-font"> assets <i class="material-icons">folder_open</i>  icon</span> in the <span className="general-content-bold-font"> mobile APP. </span></li>
                        <li className="general-content-body-font uploaded-assets-text-icon"> Click the <span className="general-content-bold-font">  camera <i class="material-icons"> camera_alt </i> icon  </span> and <span className="general-content-bold-font">  take a photo </span> or click on the <span className="general-content-bold-font"> add media <i class="material-icons"> add_circle_outline </i>  icon </span> to select an existing image from your phone gallery.  </li>
                        <li>Name the photo<span className="general-content-bold-font"> asset </span>  and <span className="general-content-bold-font"> upload.</span> </li>
                      </ul>
                    </div>
                  </div>
                  <div className="col-5 giphy-tour-content">
                    <div className="giphy-container mobile-giphy"> <img src="img/Upload-Asset.gif" /> </div>
                  </div>

                </div>

                {/* <div className="footer-part-of-content-tour clearfix">
                  <span> <a href="#" className="video-footer-tour"> See an indepth video on assets <i class="material-icons"> play_circle_outline</i>  </a> </span>
                 </div> */}
              </div>
            </div>
            {/* end block2 */}
            {/* start block3 */}
            <div className="content-container-tour createpost-block-part-tour " id={3}>

              <div className="inner-content-container">

                <div className="content-body-download-tour title-main-content-tour clearfix">
                  <div className="step-content"> <p className="content-title-header"> STEP 3 OF {totalSlides} </p> </div>
                  <div className="heading-tour title-tour"><p className="content-title-header">CREATE POST</p></div>

                </div>
                <div className="block-part-content-tour clearfix">

                  <div className="col-5 text-tour-content">
                    <div className="ul-container">
                      <ul>
                        <li className="general-content-body-font uploaded-assets-text-icon">Go to your default <span className="general-content-bold-font"> company feed  <i class="material-icons">list_alt</i>  </span> in the desktop APP <span className="general-content-bold-font"> (named after your company).</span></li>
                        <li className="general-content-body-font uploaded-assets-text-icon"> Click on the floating <span className="general-content-bold-font"> ‘CREATE POST’ </span> box</li>
                        <li className="general-content-body-font  uploaded-assets-text-icon"> Add your post text. </li>
                        <li className="general-content-body-font  uploaded-assets-text-icon"> Click on the  <span className="general-content-bold-font"> Media <i class="material-icons">filter</i> </span>   button at the botton of the post box, find the <span className="general-content-bold-font"> new asset  </span> you just created in step 2, and click on it, then <span className="general-content-bold-font"> click Select.</span> </li>
                        <li className="general-content-body-font  uploaded-assets-text-icon">Once the photo is added to your post, click <span className="general-content-bold-font">Create </span> and then view your finished post in the feed. </li>
                      </ul>
                    </div>
                  </div>
                  <div className="col-5 giphy-tour-content">
                    <div className="giphy-container"> <img src="img/Create-Post.gif" /> </div>
                  </div>
                </div>

                {/* <div className="footer-part-of-content-tour clearfix">
                  <span> <a className="video-footer-tour"> See an indepth video on creating posts <i class="material-icons"> play_circle_outline</i>  </a></span>
                </div> */}
              </div>
            </div>
            {/* end block3 */}
            {/* start block4 */}
            {isUserSuperAdminOrAdmin == true ?
              <div className="content-container-tour  createpost-block-part-tour " id={4}>

                <div className="inner-content-container">

                  <div className="content-body-download-tour title-main-content-tour clearfix">
                    <div className="step-content"> <p className="content-title-header"> STEP 4 OF {totalSlides} </p> </div>
                    <div className="heading-tour title-tour"><p className="content-title-header">ADD USERS</p></div>

                  </div>
                  <div className="block-part-content-tour clearfix">

                    <div className="col-5 text-tour-content">
                      <div className="ul-container">
                        <ul>
                          <li className="general-content-body-font uploaded-assets-text-icon">Go to <span className="general-content-bold-font">  Settings <i class="material-icons"> settings </i> </span> in the web APP. </li>
                          <li className="general-content-body-font uploaded-assets-text-icon">Click on <span className="general-content-bold-font">  Users. </span> </li>
                          <li className="general-content-body-font uploaded-assets-text-icon">Click on <span className="general-content-bold-font">  Add User. </span></li>
                          <li className="general-content-body-font uploaded-assets-text-icon">Invite your colleagues.</li>
                        </ul>
                      </div>
                    </div>
                    <div className="col-5 giphy-tour-content">
                      <div className="giphy-container"> <img src="img/Add-User.gif" /> </div>
                    </div>

                  </div>

                  {/* <div className="footer-part-of-content-tour clearfix">
                    <span><a className="video-footer-tour"> See an indepth video on settings  <i class="material-icons"> play_circle_outline</i> </a></span>
                  </div> */}
                </div>
              </div> : ''}
            {/* end block4 */}
            {/* start block5  */}

            <div className="content-container-tour  createpost-block-part-tour " id={5}>

              <div className="inner-content-container">

                <div className="content-body-download-tour title-main-content-tour clearfix">
                  <div className="step-content"> <p className="content-title-header"> STEP {totalSlides} OF {totalSlides} </p> </div>
                  <div className="heading-tour title-tour"><p className="content-title-header clearfix">CONNECT TO SOCIAL & FINISH <span> <img src="img/rocket-img.jpg" /></span></p></div>

                </div>
                <div className="block-part-content-tour clearfix">

                  <div className="col-5 text-tour-content">
                    <div className="ul-container">
                      <ul>
                        <li className="general-content-body-font uploaded-assets-text-icon">Go to  <span className="general-content-bold-font"> settings <i class="material-icons"> settings </i> </span>in the web APP.</li>
                        <li className="general-content-body-font uploaded-assets-text-icon">Click on <span className="general-content-bold-font"> Social. </span>  </li>
                        <li className="general-content-body-font uploaded-assets-text-icon">Click on the social account you wish to connect. </li>
                        <li className="general-content-body-font uploaded-assets-text-icon">Agree to use Visibly for social posts.</li>
                      </ul>
                      <div className='general-content-body-font uploaded-assets-text-icon restartNote'><span className="general-content-bold-font">Note : </span>To re-launch this tour inside the Visibly platform, click on <i class="material-icons">help_outline</i> button in the side bar.</div> 
                      <div> <p className="congretulation-msg"> Congratulations, you are now connected! </p></div>
                      
                    </div>
                  </div>
                  <div className="col-5 giphy-tour-content">
                    <div className="giphy-container"> <img src="img/Connect-Social.gif" /> </div>
                  </div>

                </div>

                {/* <div className="footer-part-of-content-tour clearfix">
                  <span><a className="video-footer-tour"> See an indepth video on social settings <i class="material-icons"> play_circle_outline</i> </a> </span>
                </div> */}
              </div>
            </div>
          </this.state.Slider.default>:''}
          </div>
         <div className = "button-container-of-tour clearfix">
          <div>
           <a className={`btn btn-primary left-button-tour button-tour backTour ${currantSlideNumber == 1 ? 'hide':''}`} id={`back${currantSlideNumber}`} onClick={this.BackAndNextSlide.bind(this, currantSlideNumber, 'back')}> Back</a> 
          <a className="btn btn-primary right-button-tour button-tour nextTour" id={`next${currantSlideNumber}`} onClick={this.BackAndNextSlide.bind(this, currantSlideNumber, 'next')} > {isUserSuperAdminOrAdmin == true ? currantSlideNumber !== 5 ? 'Next' :'Finish': isUserSuperAdminOrAdmin !==4 ?'Next':'Finish'}</a>
          </div>
           </div>
          
          </div>
          {/* end block5 */}
         
        </div>
      
      </div>
    </div>
    )
  }
}
function mapStateToProps(state) {
  return {
    auth: state.auth,
    profile: state.profile
  }
}
function mapDispatchToProps(dispatch) {
  return {
    profileSettings: bindActionCreators(profileSettings, dispatch),
    loginActions: bindActionCreators(loginActions, dispatch)
  }
}
let connection = connect(mapStateToProps, mapDispatchToProps)
var reduxConnectedComponent = connection(VisiblyTour)

export default reduxForm({
  form: 'VisiblyTour' // a unique identifier for this form
})(reduxConnectedComponent)