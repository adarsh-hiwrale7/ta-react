import Header from './Layout/Header/Header';
import Sidebar from './Layout/Sidebar/Sidebar';
import Footer from './Layout/Footer/Footer';

class App extends React.Component {

	render() {

		return (
			<div>
		        <div className="sidebar">
		       		<Sidebar />
		        </div>
		        <div className="header">
		       		<Header />
		        </div>
		        <div className="content-main">
		          {this.props.children}

		        </div>
		        {/*<div className="footer">
		          	<Footer />
		        </div>*/}
		    </div>
		);

	}
}

export default App