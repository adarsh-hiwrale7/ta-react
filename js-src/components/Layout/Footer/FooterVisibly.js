
import ReactDOM from 'react-dom';
var showMenu = false;
export class FooterVisibly  extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        }
     }

    render() {
        return(
            <div className="footer-quick-links-section">
            <div className="container cf clearfix">
              <div className="footer-description-col">
                <div className="footer-column-inner">
                  <div class="footer-column-inner">

                    <div class="footer-logo-wrapper">
                      <div class="footer-logo">
                        <a
                          href="http://ec2-18-130-135-182.eu-west-2.compute.amazonaws.com"
                          target="_blank">
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            class="nav__image image"
                            viewBox="0 0 374 95">

                            <g data-name="Layer 2">
                              <g data-name="Layer 1">
                                <path
                                  class="cls-2"
                                  d="M17.21,22V49.66H40.52V72.71H67.85V22ZM57.73,62.58H50.64V39.52H27.33v-7.4h30.4Z"></path>
                                <path
                                  class="cls-3"
                                  d="M50.64,90.25V62.57H27.33V39.52H0V90.25ZM10.12,49.66h7.09V72.71H40.52v7.4H10.12Z"></path>
                              </g>
                            </g>
                          </svg>
                        </a>
                      </div>
                    </div>

                    <div class="footer-description">
                      <section id="text-5" class="widget widget_text">
                        <div class="textwidget">
                          <p>The #EmployeeMarketing platform, built for brand advocacy, internal
                            communications, lead generation and talent engagement.</p>
                        </div>
                      </section>
                    </div>
                  </div>
                </div>
              </div>
              <div className="footer-column-wrapper clearfix">
                <div className="footer-column">
                  <div className="footer-column-inner">
                    <div className="footer-link-heading">
                      Product
                    </div>
                    <div className="menu-footer-product-menu-container">
                      <ul id="menu-footer-product-menu" class="menu">
                        <li
                          id="menu-item-28"
                          class="menu-item menu-item-type-post_type menu-item-object-page menu-item-28">
                          <a target="_blank"
                            href="https://visibly.io/platform/">Platform</a>
                        </li>
                        <li
                          id="menu-item-27"
                          class="menu-item menu-item-type-post_type menu-item-object-page menu-item-27">
                          <a target="_blank"
                            href="https://visibly.io/features/">Features</a>
                        </li>
                        <li
                          id="menu-item-26"
                          class="menu-item menu-item-type-post_type menu-item-object-page menu-item-26">
                          <a target="_blank"
                            href="https://visibly.io/pricing/">Pricing</a>
                        </li>
                        <li
                          id="menu-item-29"
                          class="menu-item menu-item-type-custom menu-item-object-custom menu-item-29">
                          <a target="_blank"  href="https://app.visibly.io/login">Login</a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
                <div className="footer-column">
                  <div className="footer-column-inner">
                    <div className="footer-link-heading">
                      Company
                    </div>
                    <div className="menu-footer-product-menu-container">
                      <ul id="menu-footer-product-menu" class="menu">
                        <li
                          id="menu-item-30"
                          class="menu-item menu-item-type-post_type menu-item-object-page menu-item-30">
                          <a target="_blank"
                            href="https://visibly.io/about/">About us</a>
                        </li>
                        <li
                          id="menu-item-33"
                          class="menu-item menu-item-type-post_type menu-item-object-page menu-item-33">
                          <a target="_blank"
                            href="https://visibly.io/blog/">Blog</a>
                        </li>
                        <li
                          id="menu-item-34"
                          class="menu-item menu-item-type-custom menu-item-object-custom menu-item-34">
                          <a  target="_blank" href="https://visibly.io/press/">Press</a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
                <div className="footer-column">
                  <div className="footer-column-inner">
                    <div className="footer-link-heading">
                      Support
                    </div>
                    <div className="menu-footer-product-menu-container">
                      <ul id="menu-footer-product-menu" class="menu">
                       
                      <li
                          id="menu-item-33"
                          class="menu-item menu-item-type-post_type menu-item-object-page menu-item-33">
                          <a target="_blank"
                            href="https://support.visibly.io">Support</a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="footer-panel">
              <div id="footer-bottom-section">
                <div class="container cf clearfix">
                  <div class="footer-bottom-left-links">

                    <div class="menu-footer-policy-menu-container">
                      <ul id="menu-footer-policy-menu" class="cf clearfix">
                        <li
                          id="menu-item-92"
                          class="menu-item menu-item-type-post_type menu-item-object-page menu-item-92">
                          <a target="_blank"
                            href="https://visibly.io/term-of-use/">Terms of use</a>
                        </li>
                        <li
                          id="menu-item-91"
                          class="menu-item menu-item-type-post_type menu-item-object-page menu-item-91">
                          <a target="_blank"
                            href="https://visibly.io/privacy-policy/">Privacy Policy</a>
                        </li>
                        <li
                          id="menu-item-90"
                          class="menu-item menu-item-type-post_type menu-item-object-page menu-item-90">
                          <a target="_blank"
                            href="https://visibly.io/cookie-policy/">Cookie policy</a>
                        </li>
                      </ul>
                    </div>
                  </div>
                  <div class="footer-social-links-wrapper">
                    <div class="textwidget">
                        <ul class="cf clearfix">
                          <li>
                            <a target="_blank" href="https://www.linkedin.com/company/visiblyhq/" title="Linkedin" class="linkedin-icon">
                            <i class="fa fa-linkedin" aria-hidden="true"></i>
                            </a>
                          </li>
                          <li>
                            <a target="_blank" href="https://www.facebook.com/BeVisibly/" title="Facebook" class="facebook-icon">
                            <i class="fa fa-facebook" aria-hidden="true"></i>
                            </a>
                          </li>
                          <li>
                            <a target="_blank" href="https://twitter.com/BeVisibly" title="Twitter" class="twitter-icon">
                            <i class="fa fa-twitter" aria-hidden="true"></i>
                            </a>
                          </li>
                        </ul>
                      </div>

                  </div>

                </div>
              </div>
            </div>

          </div>
        )

    }
};
export default FooterVisibly;