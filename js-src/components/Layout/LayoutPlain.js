
class LayoutPlain extends React.Component {
	
	render() {

		return (
			<div>
		        <div className="content-main">
		          {this.props.children}
		        </div>
		    </div>
		); 
		
	}
}

export default LayoutPlain