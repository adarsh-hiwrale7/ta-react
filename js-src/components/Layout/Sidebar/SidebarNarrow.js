import { browserHistory } from 'react-router'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Link } from 'react-router'
import * as loginActions from '../../../actions/loginActions'
import moment from '../../Chunks/ChunkMoment'
import PreLoader from '../../PreLoader'

import TagPopup from '../../Feed/TagPopup'
import * as userActions from '../../../actions/userActions'
import * as profileSettings from '../../../actions/settings/getSettings'
import PerfectScrollbar from '../../Chunks/ChunkPerfectScrollbar'
import * as utils from '../../../utils/utils'
import * as authActions from "../../../actions/authActions"
var userprofilerestrict = false;
var restrictAPI = false;
class SidebarNarrow extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			moment: '',
			overlay_flag: false,
			notification:false,
			perfectScrollbar:null
		}
	}
	componentWillMount() {
		// if(restrictAPI!==true){
		// this.props.profileSettings.GetProfileSettings()
		// restrictAPI=true;
		// setTimeout(() => {
		// 	restrictAPI=false
		// }, 5000);
		// }
		let scrollbarSelector1 = '.nav-narrow'
		moment().then(moment => {
			PerfectScrollbar().then(scrollbar=>{
				this.setState({ moment: moment ,perfectScrollbar:scrollbar},()=>{
					if (document.querySelectorAll(scrollbarSelector1).length > 0) {
						const ps = new this.state.perfectScrollbar.default(scrollbarSelector1, {
							wheelSpeed: 2,
							wheelPropagation: true,
							minScrollbarLength: 20,
							suppressScrollX: true
						})
					}
				})
			})
		})
	}
	logout() {
		if(this.props.auth.isLogoutClicked!==true){
			this.props.loginActions.callLogoutApi()
		}	
	}

	renderLoading() {

		return (
			<div className='preloader-wrap-page'>
				<PreLoader />
			</div>
		)
	}
	openUserAvtartPopup() {
		var element = document.getElementById('popup-wrapper-for-click-avtar')
		element.classList.toggle('userpopup-notification');
		document.body.classList.toggle('notification-userpopup');
		if (this.props.profile.profile.email !== null) {
			var isCallFromUserSidebarPopup = true
			var me = this;
			if(userprofilerestrict!==true){
				me.props.userActions.fetchUserDetail(
					me.props.profile.profile.identity, isCallFromUserSidebarPopup);
				userprofilerestrict=true;
				setTimeout(()=>{
					userprofilerestrict=false
				},5000)
			}

		}
	}
	/**
	 * @use when clicked on restart tour option to start tour 
	 * @author disha
	 */
	handleTourStart() {
		var profileData=this.props.profile.profile.email !== null ? this.props.profile.profile  :''
		var currAlbumName=localStorage.getItem('albumName')
		var userTourDetail=JSON.parse(localStorage.getItem(`${profileData.identity}_${currAlbumName}`))
		if (profileData !== '') {
			if (userTourDetail !== null) {
				//set isTourRestart tour flag to true to identify that tour is restart and tour is skipped
				var objToAdd={
					'albumName':currAlbumName,
					'userid':profileData.identity,
					'tourStep':0,
					'isTourRestart':true,
					'isTourSkipped':true
				}
				localStorage.setItem(`${profileData.identity}_${currAlbumName}`, JSON.stringify(objToAdd));
			}
		}
		browserHistory.push('/tour')
	  }
	render() {


		let notShow = 0
		let roleName = ''
		let authorAvatarURL = this.props.profile.profile.avatar
		let userName = `${this.props.profile.profile.first_name} ${this.props.profile.profile.last_name}`
		var authorAvatar = {
			backgroundImage: `url(${typeof authorAvatarURL !== 'undefined' && authorAvatarURL != '' ? authorAvatarURL : '/img/default-avatar.png'})`
		}
		var pathname=this.props.location.pathname !== undefined ? this.props.location.pathname:''
		if (localStorage.getItem('roles') !== null) {
			if (Object.keys(JSON.parse(localStorage.getItem('roles'))).length > 0) {
				roleName = JSON.parse(localStorage.getItem('roles'))[0].role_name
				notShow = 0
				if (roleName === 'employee') {
					notShow = 1
				}
			}
		}
		var pathname=this.props.location.pathname !== undefined ? this.props.location.pathname :this.props.location !== undefined ?this.props.location:''
		var newLocation = utils.removeTrailingSlash(pathname).split('/')
		// let roleName='admin'
		return (
			<div className='nd-parent nav-narrow'>
				<div className="navigation-box">
				<nav className='nav-side'>
				 {/* loader */}
				    {/* <div className = "feed-narrow-loader">
					<ul>
					   <li className = "loader-grey-line loader-line-space loader-line-height icon-loader"></li>
					   <li className = "loader-grey-line loader-line-space loader-line-height icon-loader"></li>
					   <li className = "loader-grey-line loader-line-space loader-line-height icon-loader"></li>
					   <li className = "loader-grey-line loader-line-space loader-line-height icon-loader"></li>
                       <li className = "loader-grey-line loader-line-space loader-line-height icon-loader"></li>
					   <li className = "loader-grey-line loader-line-space loader-line-height icon-loader"></li>
					   <li className = "loader-grey-line loader-line-space loader-line-height icon-loader"></li>
					   <li className = "loader-grey-line loader-line-space loader-line-height icon-loader"></li>
					 </ul>
				   </div> */}
				{/* end laoder */}
					<ul className='parent'>
						<li className="sidebar-svg-li">
							<Link activeClassName='active' to='/dashboard' title='Dashboard'>
							<span class="sidebar-icon"><svg xmlns="http://www.w3.org/2000/svg" className="nav-icon" width="24" height="24" viewBox="0 0 24 24"><path d="M0 0h24v24H0z" fill="none"/><path className="nav-icon-path" d="M21 2H3c-1.1 0-2 .9-2 2v12c0 1.1.9 2 2 2h7l-2 3v1h8v-1l-2-3h7c1.1 0 2-.9 2-2V4c0-1.1-.9-2-2-2zm0 12H3V4h18v10z"/></svg></span>
								<span className='text'>Dashboard</span>
							</Link>
						</li>
						<li className="feed-list-icon sidebar-svg-li">
						{newLocation.length>0 ?newLocation[1]=='feed'?
							<Link 
								className='active'
								to='/feed/default/live'
								title='Feeds'
							>
								<i className='material-icons'>list_alt</i>
								<span className='text'>Feeds</span>
							</Link>
							 :<Link to='/feed/default/live' title='Feeds'>
							 <i className='material-icons'>list_alt</i>
							 <span className='text'>Feeds</span>
						 </Link>
							 :''
						}
						</li>
						<li className="sidebar-svg-li">
							<Link activeClassName='active' to='/chat' title='Chat'>
							<span class="sidebar-icon"><svg xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24"><path d="M0 0h24v24H0V0z" fill="none"/><path d="M4 4h16v12H5.17L4 17.17V4m0-2c-1.1 0-1.99.9-1.99 2L2 22l4-4h14c1.1 0 2-.9 2-2V4c0-1.1-.9-2-2-2H4zm2 10h8v2H6v-2zm0-3h12v2H6V9zm0-3h12v2H6V6z"/></svg></span>
								<span className='text'>Chat</span>
							</Link>
						</li>
						<li className="sidebar-svg-li">
							<Link activeClassName='active' to='/assets' title='Assets'>
								<i className='material-icons'>folder_open</i>
								<span className='text'>Assets</span>
								<span className='label'>8</span>
							</Link>
						</li>
						<li className="sidebar-svg-li">
							<Link activeClassName='active' to='/campaigns' title='Campaigns'>
							<span class="sidebar-icon"><svg xmlns="http://www.w3.org/2000/svg" className="nav-icon" width="24" height="24" viewBox="0 0 24 24"><path className="nav-icon-path" d="M9 11H7v2h2v-2zm4 0h-2v2h2v-2zm4 0h-2v2h2v-2zm2-7h-1V2h-2v2H8V2H6v2H5c-1.11 0-1.99.9-1.99 2L3 20c0 1.1.89 2 2 2h14c1.1 0 2-.9 2-2V6c0-1.1-.9-2-2-2zm0 16H5V9h14v11z"/><path fill="none" d="M0 0h24v24H0z"/></svg></span>
								<span className='text'>Campaigns</span>
							</Link>
						</li>
						{/* <li><Link activeClassName="active" to="/event" title="Event"><i class="material-icons">&#xE616;</i><span className="text">Event</span></Link></li> */}
						{!notShow
							? <li className="sidebar-svg-li">
								{pathname?pathname.includes('moderation')?
								<Link
								className='active'
								to='/moderation/posts'
								title='Moderation'
							>
								<i className='material-icons'>transform</i>
								<span className='text'>Moderation</span>
							</Link>
								:
								<Link

								to='/moderation/posts'
								title='Moderation'
							>
								<i className='material-icons'>transform</i>
								<span className='text'>Moderation</span>
							</Link>:''
								}

							</li>
							: ''}
						<li className="sidebar-svg-li">
							<Link
								activeClassName='active'
								to='/leaderboard'
								title='Leaderboard'
							>
								<i className='material-icons'>star_border</i>
								<span className='text'>Leaderboard</span>
							</Link>
						</li>
						{roleName === 'super-admin' ||
							roleName === 'admin' ||
							roleName === 'guest'
							? <li className="sidebar-svg-li">
								<Link
									activeClassName='active'
									to='/analytics/adoption'
									title='Analytics'
								>
								<span class="sidebar-icon">
								<svg xmlns="http://www.w3.org/2000/svg"  width="24" height="24" viewBox="0 0 24 24"><path fill="none" d="M0 0h24v24H0V0z"/><path className='nav-icon-path' d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm7.93 9H13V4.07c3.61.45 6.48 3.32 6.93 6.93zM4 12c0-4.07 3.06-7.44 7-7.93v15.86c-3.94-.49-7-3.86-7-7.93zm9 7.93V13h6.93c-.45 3.61-3.32 6.48-6.93 6.93z"/></svg>
									</span>
									<span className='text'>Analytics</span>
									
								</Link>
							</li>
							: ''}
						<li className="sidebar-svg-li">
							<a onClick={this.logout.bind(this)} title='Logout'>
								<i className='material-icons'>power_settings_new</i>
							</a>
						</li>

					</ul>
				</nav>
				<nav className='nav-side example bottom-narrow-nav'>
					<ul className='parent'>
					<li className="button-dropdown btn0 sidebar-svg-li">
						<a href="javascript:void(0)" className='dropdown-toggle' title="Support">
						<svg class="nav-icon" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path fill="none" d="M0 0h24v24H0z"/><path class="nav-icon-path" d="M11 18h2v-2h-2v2zm1-16C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm0 18c-4.41 0-8-3.59-8-8s3.59-8 8-8 8 3.59 8 8-3.59 8-8 8zm0-14c-2.21 0-4 1.79-4 4h2c0-1.1.9-2 2-2s2 .9 2 2c0 2-3 1.75-3 5h2c0-2.25 3-2.5 3-5 0-2.21-1.79-4-4-4z"/></svg>
						</a>
						{/* <a target='_blank' href="http://support.visibly.io/" className='device_support_dropdown'><i class='material-icons'>help_outline</i></a> */}
						<ul className='dropdown-menu hide'>

							<li className='restart-tour-li'>
							<a
								className='btn-create-asset'
								onClick={this.handleTourStart.bind(this)}
							>
								Restart Tour
							</a>
                    </li>
                    <li>
                      <a target='_blank' href="https://www.visibly.io/support">Help Center</a>
                    </li>
                  </ul>
              		</li>
						<li className='sidebar-svg-li'>
							<Link activeClassName='active' to='/settings' title='Settings'>
							<svg class="nav-icon" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path fill="none" d="M0 0h24v24H0V0z"/><path class="nav-icon-path"  d="M19.43 12.98c.04-.32.07-.64.07-.98 0-.34-.03-.66-.07-.98l2.11-1.65c.19-.15.24-.42.12-.64l-2-3.46c-.09-.16-.26-.25-.44-.25-.06 0-.12.01-.17.03l-2.49 1c-.52-.4-1.08-.73-1.69-.98l-.38-2.65C14.46 2.18 14.25 2 14 2h-4c-.25 0-.46.18-.49.42l-.38 2.65c-.61.25-1.17.59-1.69.98l-2.49-1c-.06-.02-.12-.03-.18-.03-.17 0-.34.09-.43.25l-2 3.46c-.13.22-.07.49.12.64l2.11 1.65c-.04.32-.07.65-.07.98 0 .33.03.66.07.98l-2.11 1.65c-.19.15-.24.42-.12.64l2 3.46c.09.16.26.25.44.25.06 0 .12-.01.17-.03l2.49-1c.52.4 1.08.73 1.69.98l.38 2.65c.03.24.24.42.49.42h4c.25 0 .46-.18.49-.42l.38-2.65c.61-.25 1.17-.59 1.69-.98l2.49 1c.06.02.12.03.18.03.17 0 .34-.09.43-.25l2-3.46c.12-.22.07-.49-.12-.64l-2.11-1.65zm-1.98-1.71c.04.31.05.52.05.73 0 .21-.02.43-.05.73l-.14 1.13.89.7 1.08.84-.7 1.21-1.27-.51-1.04-.42-.9.68c-.43.32-.84.56-1.25.73l-1.06.43-.16 1.13-.2 1.35h-1.4l-.19-1.35-.16-1.13-1.06-.43c-.43-.18-.83-.41-1.23-.71l-.91-.7-1.06.43-1.27.51-.7-1.21 1.08-.84.89-.7-.14-1.13c-.03-.31-.05-.54-.05-.74s.02-.43.05-.73l.14-1.13-.89-.7-1.08-.84.7-1.21 1.27.51 1.04.42.9-.68c.43-.32.84-.56 1.25-.73l1.06-.43.16-1.13.2-1.35h1.39l.19 1.35.16 1.13 1.06.43c.43.18.83.41 1.23.71l.91.7 1.06-.43 1.27-.51.7 1.21-1.07.85-.89.7.14 1.13zM12 8c-2.21 0-4 1.79-4 4s1.79 4 4 4 4-1.79 4-4-1.79-4-4-4zm0 6c-1.1 0-2-.9-2-2s.9-2 2-2 2 .9 2 2-.9 2-2 2z"/></svg>
								<span className='text'>Settings</span>
							</Link>
						</li>
						<li>
						<div id='dashborard-popup-wrapper' class='dashborard-popup'>
					<div className='avatar' onMouseEnter = {this.openUserAvtartPopup.bind(this)} onMouseLeave={this.openUserAvtartPopup.bind(this)}>
						<div
							title={userName}
							style={authorAvatar}
							className='user-avtar'
						/>
					</div>

					<div
						className={`popup-wrapper-for-avtar  userpopup-notification `}
						id='popup-wrapper-for-click-avtar'
					>
						<div
							className={`dashboard-notification  ${this.props.users.isFetchingForSidebar == true ? 'userProfileLoader' : ''}`}
							id='dashboard-notification-popup'
						>

							<div className='inner-dashboard-notification'>
								<div className='body-popup-wrapper-for-avtar'>
								{this.props.users.isFetchingForSidebar == true ?
								this.renderLoading()
								: ''}
									<TagPopup props={this.props} isCallFromSidebaruser={true}/>
								</div>
							</div>
						</div>

					</div>

				</div>
						</li>
					</ul>
				</nav>
				{/* <div class='dashborard-popup'>
					HHH
					<ul>
						<li>
							<Link activeClassName='active' to='/settings' title='Settings'>
								<i className='material-icons'>settings</i>
								<span className='text'>Settings</span>
							</Link>
						</li>

					</ul>
				</div> */}

			</div>
			</div>
		)
	}
}
function mapStateToProps(state) {
	return {
		users: state.users,
		profile: state.profile,
		auth:state.auth
	}
}
function mapDispatchToProps(dispatch) {
	return {
		userActions: bindActionCreators(userActions, dispatch),
		loginActions: bindActionCreators(loginActions, dispatch),
		profileSettings: bindActionCreators(profileSettings, dispatch),
		authActions: bindActionCreators(authActions, dispatch)
	}
}
module.exports = connect(mapStateToProps, mapDispatchToProps)(SidebarNarrow)
