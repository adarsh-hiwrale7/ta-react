import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Link } from "react-router";
import * as utils from '../../../utils/utils'
import * as authActions from "../../../actions/authActions";
import AddMoreStoragePopup from '../../Dashboard/AddMoreStoragePopup';
import * as loginActions from '../../../actions/loginActions';
import CenterPopupWrapper from '../../CenterPopupWrapper'


import PerfectScrollbar from '../../Chunks/ChunkPerfectScrollbar'
export default class Sidebar extends React.Component {
	constructor (props) {
    super(props)
    this.state = {
			openAddMoreStorage:false,
			perfectScrollbar:null
    }
	}
	/**
	* Warning: This lifecycle is currently deprecated, and will be removed in React version 17+
	More details here: https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html
	*/
	componentWillMount() {
		let scrollbarSelector = '.nav-side';
		PerfectScrollbar().then(scrollbar=>{
			this.setState({perfectScrollbar:scrollbar},()=>{
				// Check for element to apply scrollbar
				if(document.querySelectorAll(scrollbarSelector).length>0){
					const ps = new this.state.perfectScrollbar.default(scrollbarSelector, {
						wheelSpeed: 2,
						wheelPropagation: true,
						minScrollbarLength: 20
					});
				}
			})
		})
	}
	logout() {
		this.props.loginActions.callLogoutApi();
	}
  addStoragePopup(){
	document.body.classList.add('center-wrapper-body-container');
    return(
		<CenterPopupWrapper>
			<AddMoreStoragePopup
				closeAddMoreStorage={this.closeCreateFeedPopup.bind(this)}
				//onSubmit={this.handleSubmitStorage.bind(this)}
			/>
		</CenterPopupWrapper>
    )
  }

  clickAddMoreStorage(){
  	this.setState({openAddMoreStorage:true});
  }
  closeCreateFeedPopup(){
	document.body.classList.remove('center-wrapper-body-container');
  	this.setState({openAddMoreStorage:false});	
  }

	render() {
		var storageSizeAndType={}
		var LimitInPercentage=0
		var maxlimit=0
		let roleName =JSON.parse(localStorage.getItem('roles')) !== null? JSON.parse(localStorage.getItem('roles'))[0].role_name:''
		let notShow = 0;
		if(roleName === "employee") {
			notShow = 1;
		}
		if (this.props.general.s3SizeData !== '') {
			if (Object.keys(this.props.general.s3SizeData.storage).length > 0) {
				//count percentage
				var countLimit=100-((this.props.general.s3SizeData.storage.freeStorage/this.props.general.s3SizeData.storage.maxlimit) * 100)
				LimitInPercentage=Math.round(countLimit)

				//convert used space into proper format like gb , mb, tb , kb
				var usedSpace = this.props.general.s3SizeData.storage.used_storage
				storageSizeAndType = utils.sizeConverter(usedSpace)
				maxlimit=utils.sizeConverter(this.props.general.s3SizeData.storage.maxlimit)
			}
		}
	 	return (
			<div id="navdraw">
			 {/* loader start */}
			 {/* <div className="dashboard-loader">
    <div className="nd-parent sidenav-wide">
        <header className="nd-header">
            <Link className="logo  visibly-dashboard-logo loader-grey-line loader-line-height loader-line-radius">
            </Link>
        </header>

        <nav className="nav-side">
            <ul className="parent">
                <li className="heading loader-line-radius loader-line-height loader-grey-line loader-sidebar-width  dashboard-loader-hed-part"></li>
                <li>
                    <Link><span class="icon-in-loader loader-grey-line"></span><span className="text loader-grey-line loader-line-height loader-sidebar-width loader-line-radius"></span></Link>
                </li>
                <li>
                    <Link><span class="icon-in-loader loader-grey-line"></span><span className="text loader-grey-line loader-line-height loader-sidebar-width loader-line-radius"></span></Link>
                </li>
                <li>
                    <Link><span class="icon-in-loader loader-grey-line"></span><span className="text loader-grey-line loader-line-height loader-sidebar-width loader-line-radius"></span></Link>
                </li>
                <li>
                    <Link><span class="icon-in-loader loader-grey-line"></span><span className="text loader-grey-line loader-line-height loader-sidebar-width loader-line-radius"></span></Link>
                </li>
                <li>
                    <Link><span class="icon-in-loader loader-grey-line"></span><span className="text loader-grey-line loader-line-height loader-sidebar-width loader-line-radius"></span></Link>
                </li>
                <li>
                    <Link><span class="icon-in-loader loader-grey-line"></span><span className="text loader-grey-line loader-line-height loader-sidebar-width loader-line-radius"></span></Link>
                </li>
                <li className="spacer"></li>
                <li className="heading loader-line-radius loader-line-height dashboard-loader-hed-part loader-grey-line loader-sidebar-width"></li>
                <li>
                    <Link><span class="icon-in-loader loader-grey-line"></span><span className="text loader-grey-line loader-line-height loader-sidebar-width loader-line-radius"></span></Link>
                </li>
                <li>
                    <Link><span class="icon-in-loader loader-grey-line"></span><span className="text loader-grey-line loader-line-height loader-sidebar-width loader-line-radius"></span></Link>
                </li>
                <li className="heading loader-line-radius loader-line-height  dashboard-loader-hed-part loader-grey-line loader-sidebar-width"></li>
                <li>
                    <Link><span class="icon-in-loader loader-grey-line"></span><span className="text loader-grey-line loader-line-height loader-sidebar-width loader-line-radius"></span></Link>
                </li>
                <li>
                    <Link><span class="icon-in-loader hide-icon loader-grey-line"></span><span className="text loader-grey-line loader-line-height loader-sidebar-width loader-line-radius"></span></Link>
                </li>

            </ul>
        </nav>
    </div>
</div>  */}
			  
				{/* loader end */}
	            <div className="nd-parent sidenav-wide">
	              <header className="nd-header">
	                <Link to="/dashboard" className="logo">
									<svg class="nav__image image" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 374.68 95.95"><defs>

			</defs><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path class="svg-cls-1" d="M147.92,23.56H164.1V73.12H147.92ZM156,0a8.71,8.71,0,0,0-6.57,2.63,9,9,0,0,0-2.53,6.47,9,9,0,0,0,2.53,6.47,9.53,9.53,0,0,0,13.15,0,9,9,0,0,0,2.53-6.47,9,9,0,0,0-2.53-6.47A8.71,8.71,0,0,0,156,0Zm48,46.32a23.57,23.57,0,0,0-5.11-3q-2.73-1.16-5-2-2.93-1.11-4.8-1.87A2.58,2.58,0,0,1,187.31,37a2.2,2.2,0,0,1,1.57-2.28,14.11,14.11,0,0,1,4.5-.56,26.17,26.17,0,0,1,6.37.76,35.13,35.13,0,0,1,5.76,2l2.22-10.62a26.5,26.5,0,0,0-7.38-2.88,39,39,0,0,0-8.5-.86q-8.8,0-14.26,3.84t-5.46,11.93A13.46,13.46,0,0,0,173.25,44a13.78,13.78,0,0,0,3.08,4.3,19.3,19.3,0,0,0,4.7,3.24A51.86,51.86,0,0,0,186.91,54q3.84,1.42,5.76,2.17a2.65,2.65,0,0,1,1.92,2.58,3,3,0,0,1-2.17,2.88,13.84,13.84,0,0,1-5.11.86,39.37,39.37,0,0,1-6.62-.66,32,32,0,0,1-6.93-2L171.53,71.1a37.27,37.27,0,0,0,8.09,2.28,50.62,50.62,0,0,0,8.5.76,32,32,0,0,0,8.7-1.11,20.38,20.38,0,0,0,6.83-3.29,14.73,14.73,0,0,0,4.45-5.41,16.73,16.73,0,0,0,1.57-7.38A11.47,11.47,0,0,0,208,50.72,16.64,16.64,0,0,0,204,46.32Zm12.37,26.8H232.6V23.56H216.42ZM224.51,0a8.71,8.71,0,0,0-6.57,2.63A9,9,0,0,0,215.4,9.1a9,9,0,0,0,2.53,6.47,9.53,9.53,0,0,0,13.15,0,9,9,0,0,0,2.53-6.47,9,9,0,0,0-2.53-6.47A8.71,8.71,0,0,0,224.51,0Zm66.4,37.72a32.79,32.79,0,0,1,1.52,10.21,29.18,29.18,0,0,1-1.77,10.16,22.79,22.79,0,0,1-5.31,8.34,25.26,25.26,0,0,1-8.9,5.61,34.69,34.69,0,0,1-12.54,2.07,49.83,49.83,0,0,1-11.58-1.31,44.81,44.81,0,0,1-10.06-3.64V1.72h16.18V27.41a17.33,17.33,0,0,1,5.11-3.49,18.13,18.13,0,0,1,7.53-1.37,20.28,20.28,0,0,1,8.8,1.87,19.32,19.32,0,0,1,6.73,5.26A24.52,24.52,0,0,1,290.91,37.72ZM276.24,48.14q0-6.07-2.38-9.66t-7.43-3.59a12.47,12.47,0,0,0-8,2.73V61.08a12,12,0,0,0,2.63.81,16,16,0,0,0,3.14.3,12.53,12.53,0,0,0,5.46-1.11,9.8,9.8,0,0,0,3.74-3,13.62,13.62,0,0,0,2.12-4.45A20.23,20.23,0,0,0,276.24,48.14ZM319,62q-2.33,0-3.13-1.06a4.82,4.82,0,0,1-.81-3V1.72H298.9v58a19.93,19.93,0,0,0,.71,5.31,12.6,12.6,0,0,0,2.33,4.6,11.32,11.32,0,0,0,4.35,3.24,16.68,16.68,0,0,0,6.78,1.21,39.12,39.12,0,0,0,5.31-.35,30.72,30.72,0,0,0,4.6-1l-.51-11.12A23.8,23.8,0,0,1,319,62ZM358.4,23.56,334.93,96h16.28l23.46-72.39ZM98.09,22H81.3L99.2,72.75l6.55-21Zm4.24,50.77h16.28L142.07.37H125.79ZM345,53.29l-7.67-29.73H320.51l17.9,50.77Z"></path><path class="svg-cls-2" d="M17.21,22V49.66H40.52V72.71H67.85V22ZM57.73,62.58H50.64V39.52H27.33v-7.4h30.4Z"></path><path class="svg-cls-3" d="M50.64,90.25V62.57H27.33V39.52H0V90.25ZM10.12,49.66h7.09V72.71H40.52v7.4H10.12Z"></path></g></g></svg>
									 </Link>
	              </header>
	              {this.state.openAddMoreStorage ? this.addStoragePopup() : ''}
	              <nav className="nav-side">
	                <ul className="parent">

										<li className="heading">Navigation</li>
										<li className="sidebar-svg-li"><Link activeClassName="active" to="/dashboard"><span class="sidebar-icon"><svg xmlns="http://www.w3.org/2000/svg" className="nav-icon" width="24" height="24" viewBox="0 0 24 24"><path d="M0 0h24v24H0z" fill="none"/><path className="nav-icon-path" d="M21 2H3c-1.1 0-2 .9-2 2v12c0 1.1.9 2 2 2h7l-2 3v1h8v-1l-2-3h7c1.1 0 2-.9 2-2V4c0-1.1-.9-2-2-2zm0 12H3V4h18v10z"/></svg></span><span className="text">Dashboard</span></Link></li>
										<li className="sidebar-svg-li"><Link activeClassName="active" to="/feed/default/live"><i className="material-icons">filter</i><span className="text">Feeds</span></Link></li>
										<li className="sidebar-svg-li"><Link activeClassName="active" to="/chat"><span class="sidebar-icon"><svg xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24"><path d="M0 0h24v24H0V0z" fill="none"/><path d="M4 4h16v12H5.17L4 17.17V4m0-2c-1.1 0-1.99.9-1.99 2L2 22l4-4h14c1.1 0 2-.9 2-2V4c0-1.1-.9-2-2-2H4zm2 10h8v2H6v-2zm0-3h12v2H6V9zm0-3h12v2H6V6z"/></svg></span><span className="text">Chat</span></Link></li>
	                  <li className="sidebar-svg-li"><Link activeClassName="active" to="/assets"><i className="material-icons">folder_open</i><span className="text">Assets</span></Link></li>
	                  <li className="sidebar-svg-li"><Link activeClassName="active" to="/campaigns"><span class="sidebar-icon"><svg xmlns="http://www.w3.org/2000/svg" className="nav-icon" width="24" height="24" viewBox="0 0 24 24"><path className="nav-icon-path" d="M9 11H7v2h2v-2zm4 0h-2v2h2v-2zm4 0h-2v2h2v-2zm2-7h-1V2h-2v2H8V2H6v2H5c-1.11 0-1.99.9-1.99 2L3 20c0 1.1.89 2 2 2h14c1.1 0 2-.9 2-2V6c0-1.1-.9-2-2-2zm0 16H5V9h14v11z"/><path fill="none" d="M0 0h24v24H0z"/></svg></span><span className="text">Campaigns</span></Link></li>
										{/* <li><Link activeClassName="active" to="/event" title="Event"><i class="material-icons">&#xE616;</i><span className="text">Event</span></Link></li> */}
										{!notShow ? <li className="sidebar-svg-li"><Link activeClassName="active" to="/moderation/posts"><i className="material-icons">transform</i><span className="text">Moderation</span></Link></li>:""}
										<li className="sidebar-svg-li"><Link activeClassName="active" to="/leaderboard"><i className="material-icons">star_border</i><span className="text">Leaderboard</span></Link></li>
										{roleName === "super-admin" || roleName === "admin" || roleName==='guest' ? <li><Link activeClassName="active" to="/analytics/adoption"><i className="material-icons">pie_chart_outlined</i><span className="text">Analytics</span></Link></li>:''}

	                  <li className="spacer"></li>

	                  <li className="heading">Administration</li>

										<li className="sidebar-svg-li"><Link activeClassName="active" to="/settings">
										<span className="sidebar-icon"><svg class="nav-icon" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path fill="none" d="M0 0h24v24H0V0z"></path><path class="nav-icon-path" d="M19.43 12.98c.04-.32.07-.64.07-.98 0-.34-.03-.66-.07-.98l2.11-1.65c.19-.15.24-.42.12-.64l-2-3.46c-.09-.16-.26-.25-.44-.25-.06 0-.12.01-.17.03l-2.49 1c-.52-.4-1.08-.73-1.69-.98l-.38-2.65C14.46 2.18 14.25 2 14 2h-4c-.25 0-.46.18-.49.42l-.38 2.65c-.61.25-1.17.59-1.69.98l-2.49-1c-.06-.02-.12-.03-.18-.03-.17 0-.34.09-.43.25l-2 3.46c-.13.22-.07.49.12.64l2.11 1.65c-.04.32-.07.65-.07.98 0 .33.03.66.07.98l-2.11 1.65c-.19.15-.24.42-.12.64l2 3.46c.09.16.26.25.44.25.06 0 .12-.01.17-.03l2.49-1c.52.4 1.08.73 1.69.98l.38 2.65c.03.24.24.42.49.42h4c.25 0 .46-.18.49-.42l.38-2.65c.61-.25 1.17-.59 1.69-.98l2.49 1c.06.02.12.03.18.03.17 0 .34-.09.43-.25l2-3.46c.12-.22.07-.49-.12-.64l-2.11-1.65zm-1.98-1.71c.04.31.05.52.05.73 0 .21-.02.43-.05.73l-.14 1.13.89.7 1.08.84-.7 1.21-1.27-.51-1.04-.42-.9.68c-.43.32-.84.56-1.25.73l-1.06.43-.16 1.13-.2 1.35h-1.4l-.19-1.35-.16-1.13-1.06-.43c-.43-.18-.83-.41-1.23-.71l-.91-.7-1.06.43-1.27.51-.7-1.21 1.08-.84.89-.7-.14-1.13c-.03-.31-.05-.54-.05-.74s.02-.43.05-.73l.14-1.13-.89-.7-1.08-.84.7-1.21 1.27.51 1.04.42.9-.68c.43-.32.84-.56 1.25-.73l1.06-.43.16-1.13.2-1.35h1.39l.19 1.35.16 1.13 1.06.43c.43.18.83.41 1.23.71l.91.7 1.06-.43 1.27-.51.7 1.21-1.07.85-.89.7.14 1.13zM12 8c-2.21 0-4 1.79-4 4s1.79 4 4 4 4-1.79 4-4-1.79-4-4-4zm0 6c-1.1 0-2-.9-2-2s.9-2 2-2 2 .9 2 2-.9 2-2 2z"></path></svg></span>
										<span className="text">Settings</span></Link></li>
	                  <li><a onClick={ this.logout.bind(this) } title="Logout"><i className="material-icons">power_settings_new</i><span className="text">Logout</span></a></li>
										<li className="heading storage-title">Storage</li>
										<li className={`storage-view-li ${LimitInPercentage>80 ? 'lowStorage':''}`}><i className="material-icons storage-icon">storage</i>
											<div className="storage-display"><span className="usedSpace">{storageSizeAndType.size?storageSizeAndType.size:0} {storageSizeAndType.storageType ? storageSizeAndType.storageType :'MB'}</span> / {maxlimit.size?maxlimit.size:0} {maxlimit.storageType?maxlimit.storageType:'MB'}</div>
											{roleName == 'super-admin' && typeof this.props.general.s3SizeData.storage !== 'undefined' && this.props.general.s3SizeData.storage.plan != 'free' ? 
											<div class="upgrade-storage-text"><a onClick={this.clickAddMoreStorage.bind(this)}>Upgrade storage</a></div>
											:''}
										</li>
	                </ul>
	              </nav>
	            </div>
	          </div>
		);

	}
}

function mapStateToProps(state) {
  return {
		general:state.general
  }
}
function mapDispatchToProps(dispatch) {
  return {
    loginActions: bindActionCreators(loginActions, dispatch)
  }
}
module.exports = connect(mapStateToProps, mapDispatchToProps) (Sidebar);