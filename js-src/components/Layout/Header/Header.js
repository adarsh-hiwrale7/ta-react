
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import * as loginActions from '../../../actions/loginActions';

import { Link } from "react-router";

class Header extends React.Component {
	
	constructor(props) {
		super(props);
		this.logout = this.logout.bind(this);
	}
 logout() {
		this.props.loginActions.callLogoutApi();
	}
 render() {
		return (
     	<header>
		  </header>	); 
		
	}
}

function mapStateToProps(state) {
  return {
    auth: state.auth
  }
}

function mapDispatchToProps(dispatch) {
  return {
    loginActions: bindActionCreators(loginActions, dispatch)
  }
}

module.exports = connect(mapStateToProps, mapDispatchToProps) (Header);