import Header from './Header/Header';
import Footer from './Footer/Footer';

class LayoutBasic extends React.Component {
	
	render() {

		return (
			<div>
		        <div className="content-main">
		          {this.props.children}
		          
		        </div>
		    </div>
		); 
		
	}
}

export default LayoutBasic