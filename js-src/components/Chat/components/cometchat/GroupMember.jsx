import React from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBan, faUserPlus, faTrashAlt } from "@fortawesome/free-solid-svg-icons";

const GroupMember = ({ name, status, uid, avatar, banned, handleBanUnbanMember, handleKickMember, subjectUID, ownerRights }) => {

    let action_button;
    let tab_title;
    let classes;
    let status_classes = "status mr-1 status-";
    status_classes += status;

    if(banned === false)
    {   
        if(uid !== subjectUID && ownerRights)
        {
            action_button = <div className="chatdemo">
                 <div className="delete-not-intrested-icon-wrapper clearfix">
                                <div className="mb-2 mt-3 pr-2 ban-member" >
                                    <div className="not-intrested-icon" title="Ban Member" onClick={() => handleBanUnbanMember(uid, banned)}>
                                <svg xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24"><path d="M0 0h24v24H0z" fill="none"/><path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm0 18c-4.42 0-8-3.58-8-8 0-1.85.63-3.55 1.69-4.9L16.9 18.31C15.55 19.37 13.85 20 12 20zm6.31-3.1L7.1 5.69C8.45 4.63 10.15 4 12 4c4.42 0 8 3.58 8 8 0 1.85-.63 3.55-1.69 4.9z"/></svg>
                                </div>
                                    {/* <FontAwesomeIcon icon={faBan} title="Ban Member" onClick={() => handleBanUnbanMember(uid, banned)} /> */}
                                </div>
                                <div className="mb-2 mt-3 pr-2 kick-member">
                                    <div className="delete-group-member-icon" onClick={() => handleKickMember(uid)} title="Kick Member">
                                <svg xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24"><path d="M6 19c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2V7H6v12zM8 9h8v10H8V9zm7.5-5l-1-1h-5l-1 1H5v2h14V4z"/><path d="M0 0h24v24H0V0z" fill="none"/></svg>
                                </div>
                                    {/* <FontAwesomeIcon icon={faTrashAlt} title="Kick Member" onClick={() => handleKickMember(uid)} /> */}
                                </div>
                                </div>
                            </div>;
        }
        
        tab_title = "";
        classes = "contact-tab p-2 bg-white ban-member-tab clearfix";
        return (
            <div className={classes}
                    title={tab_title} >
                        <div className="view-group-name">
                <div className="contact-avatar-small">
                    <img className="mr-2" src={avatar} alt={name} />
                </div>
                <div className="contact-data">
                    <p className="mb-0 contact-name va-super">{name}</p>
                    <p className="m-0 text-light-grey contact-status">
                        <span className={status_classes}></span>
                        <span className="status-text">{status}</span>
                    </p>
                </div>
                </div>
                {action_button}
            </div>
        );
    }
    else
    {
        if(uid !== subjectUID && ownerRights)
        {
            action_button = <div className="mb-2 mt-3 pr-2 unban-member">
                <svg xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24"><path d="M0 0h24v24H0z" fill="none"/><path d="M8 10H5V7H3v3H0v2h3v3h2v-3h3v-2zm10 1c1.66 0 2.99-1.34 2.99-3S19.66 5 18 5c-.32 0-.63.05-.91.14.57.81.9 1.79.9 2.86s-.34 2.04-.9 2.86c.28.09.59.14.91.14zm-5 0c1.66 0 2.99-1.34 2.99-3S14.66 5 13 5c-1.66 0-3 1.34-3 3s1.34 3 3 3zm6.62 2.16c.83.73 1.38 1.66 1.38 2.84v2h3v-2c0-1.54-2.37-2.49-4.38-2.84zM13 13c-2 0-6 1-6 3v2h12v-2c0-2-4-3-6-3z"/></svg>
                                {/* <FontAwesomeIcon icon={faUserPlus} /> */}
                            </div>;
        }

        tab_title = "Unban member from group";
        classes = "contact-tab p-2 bg-white unban-member-tab";

        return (
            <div className={classes}
                    title={tab_title}  onClick={() => handleBanUnbanMember(uid, banned)}>
                <div className="contact-avatar-small">
                    <img className="mr-2" src={avatar} alt={name} />
                </div>
                <div className="contact-data">
                    <p className="mb-0 contact-name va-super">{name}</p>
                    <p className="m-0 text-light-grey contact-status">
                        <span className={status_classes}></span>
                        <span className="status-text">{status}</span>
                    </p>
                </div>
                {action_button}
            </div>
        );
    }
};

export default GroupMember;
