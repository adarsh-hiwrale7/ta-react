import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUserPlus } from "@fortawesome/free-solid-svg-icons";

const NonGroupMember = ({
  name,
  status,
  uid,
  avatar,
  handleAddMember,
  subjectUID,
  ownerRights
}) => {
  let action_button;
  let tab_title;
  let classes;
  let status_classes = "status mr-1 status-";
  status_classes += status;

  if (uid !== subjectUID && ownerRights) {
    action_button = (
      <div className="mb-2 mt-3 pr-2 add-member">
        {/* <FontAwesomeIcon icon={faUserPlus} /> */}
        <svg xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24"><path d="M0 0h24v24H0z" fill="none"/><path d="M8 10H5V7H3v3H0v2h3v3h2v-3h3v-2zm10 1c1.66 0 2.99-1.34 2.99-3S19.66 5 18 5c-.32 0-.63.05-.91.14.57.81.9 1.79.9 2.86s-.34 2.04-.9 2.86c.28.09.59.14.91.14zm-5 0c1.66 0 2.99-1.34 2.99-3S14.66 5 13 5c-1.66 0-3 1.34-3 3s1.34 3 3 3zm6.62 2.16c.83.73 1.38 1.66 1.38 2.84v2h3v-2c0-1.54-2.37-2.49-4.38-2.84zM13 13c-2 0-6 1-6 3v2h12v-2c0-2-4-3-6-3z"/></svg>
      </div>
    );
  }

  tab_title = "Add member to group";
  classes = "contact-tab p-2 bg-white add-member-tab";

  return (
    <div
      className={classes}
      title={tab_title}
      onClick={() => handleAddMember(uid)}
    >
      <div className="contact-avatar-small">
        <img className="mr-2" src={avatar} alt={name} />
      </div>
      <div className="contact-data">
        <p className="mb-0 contact-name va-super">{name}</p>
        <p className="m-0 text-light-grey contact-status">
          <span className={status_classes}></span>
          <span className="status-text">{status}</span>
        </p>
      </div>
      {action_button}
    </div>
  );
};

export default NonGroupMember;
