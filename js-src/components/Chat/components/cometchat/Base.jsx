import React, { Component } from 'react';
import { CometChat } from "@cometchat-pro/chat";
import ChatContainer from "./ChatContainer";
import {initializeFirebase} from '../../../../../js-src/firebase';
import PreLoader from '../../../PreLoader'



export default class Base extends Component {

    state = {
        username:null,
        authToken: '',
        errors: []
    };
   
    /*
    @Author: Akshay soni
    Method: Login for cometchat
     */
    login() {
        CometChat.login(this.props.userId, this.props.cometchat_app_key).then(
            user => {
                localStorage.setItem('uid', user.uid)
                localStorage.setItem('chatToken', user.authToken)
                this.setState({
                    username: user.uid,
                    authToken: user.authToken

                })
            },
            error => {
                localStorage.setItem('chatToken', null)
            });
    }
   
    /*
    @Author: Akshay soni
    Method: get loggedIn user
     */
    getLoggedInUser() {
        CometChat.getLoggedinUser().then(user => {
            if (user != null) {
                this.setState({
                    username: user.name,
                    authToken: user.authToken
                })
            } else {
                this.login()
            }
        });
    }

    componentWillReceiveProps(props) {

        if (props.cometChat_id != undefined) {
            CometChat.init(props.cometChat_id, new CometChat.AppSettingsBuilder().subscribePresenceForAllUsers().setRegion(props.cometchat_app_region).build());

           initializeFirebase(props.cometChat_id,props.userId); 
           this.getLoggedInUser()
        }
    }

    renderLoading () {
        return (
          <div className='preloader-wrap'>
            <PreLoader className='Page' />
          </div>
        )
      }

    render() {
        const userstate = this.state;
        return (
            <div>
                <main>
                    {this.state.username == null ?
                     this.renderLoading()
                         :
                        <ChatContainer
                            user={userstate}
                        />
                    }
                </main>
            </div>
        );
    }
}
