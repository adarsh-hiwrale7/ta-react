import React, { Component } from 'react'
import Base from "../Chat/components/cometchat/Base";
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import Header from '../Header/Header';
import * as profileSettings from '../../actions/settings/getSettings'            

export default class Chat extends Component {

  constructor(props) {
    super(props);
    this.state = {
    }
  }

  componentDidMount() {
    this.props.profileSettings.GetProfileSettings();
  }

  render() {
    return (
      <section className="chatModuleMainWrapper">
      <div>
        <Header
         title='Chat'
         id='Chat'
         location={this.props.location}
       />
          <Base
            userId = {this.props.profile.profile.identity}
            cometChat_id = {this.props.profile.profile.cometchat_app_id}
            cometchat_app_key = {this.props.profile.profile.cometchat_app_key}
            cometchat_app_region = {this.props.profile.profile.cometchat_app_region}
            /> 
      </div>
      </section>
    )
  }
  
}


function mapStateToProps(state) {
  return {
    profile: state.profile,
  }
}
function mapDispatchToProps(dispatch) {
  return {
    profileSettings: bindActionCreators(profileSettings, dispatch),
  }
}

let connection = connect(mapStateToProps, mapDispatchToProps)
module.exports = connection(Chat)
