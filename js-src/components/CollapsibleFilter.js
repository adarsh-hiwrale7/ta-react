import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';

import * as headerActions from '../actions/header/headerActions';



class CollapsibleFilter extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            filterState:false,
        }
    }
    componentWillReceiveProps(newProps) {

        this.setState({
          filterState:newProps.header.analytic.filter_visibility,
        })
    }

	render() {
        let fiterEnableClass='';
        let filterHeight=0;
        if(this.state.filterState){
        fiterEnableClass='active';
        filterHeight = document.querySelectorAll('.dropdown-container-inner.fixedDropdown')[0].offsetHeight;
        }


        let filterHeightStyle = {
           height:filterHeight
        }
		return (
            <div id='dropdown-asset-container' className={`collapsibleDropdown ${fiterEnableClass}`}>
                <div className='dropdown-container collapsible' style={filterHeightStyle}>
                    <div className='dropdown-container-inner fixedDropdown'>
                        <div className='dropdown-inner-wrapper'>
                        {this.props.children}
                        </div>
                        <div className='top-shadow-wrapper'>
                        <div className='top-shadow' />
                        </div>
                    </div>
                </div>
            </div>
		)
	}
}

function mapStateToProps (state) {
    return {
      header:state.header,
    }
  }
  function mapDispatchToProps (dispatch) {
    return {
      headerActions: bindActionCreators(headerActions, dispatch),
    }
  }
  let connection = connect(mapStateToProps, mapDispatchToProps)
  module.exports = connection(CollapsibleFilter)