import * as headerActions from '../../actions/header/headerActions';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
var filter_hover = '';
class FilterButton extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
    }
  }
  componentDidMount () {
    this.props.headerActions.toggleFilter(false)
  }
  render() {
    // to activate the filter icon if filter is applied
    if ((this.props.selectedFeedStatus!=='' && this.props.selectedFeedStatus !== false && this.props.selectedFeedStatus!==undefined)){
        filter_hover = "filter_active";
      }
   else{
      filter_hover = ''; }
   return (
      <a href="javascript:void(0)"  className ={`show-filter ${filter_hover}`}  onClick={this.open_filter_fn.bind(this)} title="Filter"><i class="material-icons"> filter_list</i></a>
    )
  }
  open_filter_fn (){
    var filter_flag = this.props.header.filterFlag;
    // set the reducer state to identify the state of filter popup(acoording to that open/close filter popup)
    this.props.headerActions.toggleFilter(!filter_flag);
    this.props.headerActions.setFilterFlag(filter_flag);
  }
}
function mapStateToProps (state) {
    return {
      header: state.header
    }
  }

  function mapDispatchToProps (dispatch) {
    return {
        dispatch,
        headerActions: bindActionCreators(headerActions, dispatch)
    }
  }
  let connection = connect(mapStateToProps, mapDispatchToProps)
  module.exports = connection(FilterButton)

