import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as utils from '../../utils/utils';
import * as profileSettings from '../../actions/settings/getSettings'
import * as userActions from '../../actions/userActions';
import * as notificationActions from "../../actions/notificationActions";
import * as campaignActions from '../../actions/campaigns/campaignsActions'
import moment from '../Chunks/ChunkMoment';
import PreLoader from '../PreLoader';
import { browserHistory } from 'react-router';
import PerfectScrollbar from '../Chunks/ChunkPerfectScrollbar'
class NotificationPopup extends React.Component {
    constructor(props) {
      super(props)
      this.state = {
        moment: '',
        overlay_flag: false,
        perfectScrollbar:null
      }
    }
    componentWillMount() {
      let scrollbarSelector2 = '.notification-body';
        // this.props.profileSettings.GetProfileSettings();
          moment().then(moment => {
            PerfectScrollbar().then(scrollbar=>{
              this.setState({ moment: moment,perfectScrollbar:scrollbar },()=>{
                if(document.querySelectorAll(scrollbarSelector2).length>0){
                  const ps = new this.state.perfectScrollbar.default(scrollbarSelector2, {
                  wheelSpeed: 2,
                  wheelPropagation: true,
                  minScrollbarLength: 20,
                  suppressScrollX:true
                  });
                }
              })
            })
          })
      }
    notification() {
		this.props.notificationActions.fetchNotification();
	}
    postdetail(e) {
		var newLocation = "";
		if(this.props.location) {
			newLocation = utils
			.removeTrailingSlash(this.props.location.pathname)
			.split('/')
		}
		var actionid = e.action_id[0]
		this.props.notificationActions.sendReadPost(e.identity)
		switch (e.notification_type) {
			case 'post':
        this.props.notificationActions.fetchSelectedPost(actionid)
				browserHistory.push(`/post/${actionid}`)
				break;
			case 'comment':
        const postId= e.comment.data.post_identity
        this.props.notificationActions.fetchSelectedPost(postId)
				browserHistory.push(`/post/${postId}`)
				break;
			case 'asset':
				browserHistory.push('/assets')
				break;
			case 'LeaderBoard':
				browserHistory.push('/leaderboard')
                break;
      case 'customFeed':
            var isFeedAvailable = false;
            var feedId = e.feed.data.identity
            this.props.customFeedList.length>0?
            this.props.customFeedList.map((feedData,i) =>{
              if(feedData.identity==feedId){
              isFeedAvailable = true;
              }
            })
            :''
            if(isFeedAvailable!==false){
              browserHistory.push(`/feed/custom/live/${feedId}`)
            }
          break;
			case 'campaign':
        this.props.campaignActions.fetchCampaign(actionid);
        this.props.campaignActions.openCampaignFromNoti();
				browserHistory.push('/campaigns')
        break;
      case 'project':
        this.props.campaignActions.fetchCampaign(actionid);
        this.props.campaignActions.openCampaignFromNoti();
        this.props.campaignActions.openContentProjectFromNotie(e.project.data.identity); 
        browserHistory.push('/campaigns');
        break;
		}
	}
	markAllNotificationRead(id){
        if(id!==undefined || id!==null){
		this.props.notificationActions.markAllasRead(id)
		}
   }
   renderLoading() {
    return (
      <div className='preloader-wrap-page'>
        {
        Array(4).fill(1).map((el, i) => 
        <div class="user-nt">
          <div class="inner-user-nt with-thumbnail">
            <div class="loader-avtar avtar loader-grey-line" ></div>
              <div class="avtar-details">
                  <p class="text-p-avtar loader-line-radius loader-grey-line loader-line-height"></p>
                  <p class="text-p-avtar loader-line-radius loader-grey-line loader-line-space  loader-line-height"></p>
              </div>
            </div>
        </div>
        )}
      </div>
    )
  }
    render(){
        let notShow=0;
      let roleName='';
    let authorAvatarURL = this.props.profile.profile.avatar;
		let userName = `${this.props.profile.profile.first_name} ${this.props.profile.profile.last_name}`
		var authorAvatar = {
			backgroundImage: `url(${(typeof (authorAvatarURL) !== "undefined" && authorAvatarURL != ""
				? authorAvatarURL
				: '/img/default-avatar.png')})`
		}
		if(localStorage.getItem('roles') !== null){
			if(Object.keys(JSON.parse(localStorage.getItem('roles'))).length>0){
			 roleName = JSON.parse(localStorage.getItem('roles'))[0].role_name;
			notShow = 0;
			if (roleName === "employee") {
				notShow = 1;
			}
		}

	}
//let roleName='admin'
    var notification = this.props.notificationData;
		let imgstyles = {
			backgroundSize: 'cover',
			backgroundRepeat: 'no-repeat'
		}
		var nt_class;
		let myScrollbar = {
			height: 'calc(100vh - 120px)'
		  };
		let notificationScrollbar = {
			height: '297px'
		};
        return(
        <div class={`dashboard-notification dashboard-logo ${notification.loading == true?'dashboard-noti-loader' :''}`} id="dashboard-notification-popup">
                <div className="inner-dashboard-notification">
                <div >
                    <div className="notication-header">
                    <div className="triangle-left"></div>
                        <p>NOTIFICATIONS</p>
                        <a onClick={this.markAllNotificationRead.bind(this,this.props.profile.profile.identity)}>Mark all as read</a>
                        {/* <a onClick="markAllNotificationRead()">Mark All As Read</a> */}
                    </div>
                  <div className="main-notification-body">
                  {
                  notification.loading == true ?
                    <div className="notification-loeader-wrapper">
                    {this.renderLoading()}
                    </div>
                    :''
                    }
                    
                    <div className="notification-body">
                 
                     {typeof Object.keys(notification.notification_data)!=='undefined' ?

                            ( notification.loading == true ?
                              //  this.renderLoading()
                              ''
                                 :
                                notification.notification_data.length>0?
                              notification.notification_data.map((noti,i) => (
                               <div>
                                   <div id={`notification-${i}`} className={`${noti.notification_status==1?'user-nt active':'user-nt' }`} key={i}>
                                   <div className='hide'>{
                                       (noti.thumbnail_image === '' &&  noti.user_action != 'like' && noti.notification_type != 'LeaderBoard')?
                                        nt_class="inner-user-nt" :   nt_class="inner-user-nt with-thumbnail"
                                   }
                                   </div>
                                       <div className={nt_class} >
                                             <div className="avtar" style={{...imgstyles,
                                               backgroundImage: 'url(' +  noti.user.data.avatar+')'
                                                   }}>
                                              </div>

                                               <div className="avtar-details" onClick={this.postdetail.bind(this,noti)} >


                                                   {noti.user_action=="project_signed_off"?
                                                   <p className="text-p-avtar">
                                                    <span className="notiDescription">                                
                                                      <span className="notiPostTitle">
                                                          { noti.project!==undefined?
                                                             noti.project.data.project_title != null?
                                                               utils.convertUnicode(noti.project.data.project_title)
                                                               :'':''}
                                                      </span> 
                                                      
                                                        {noti.description} 
                                                    </span>
                                                    <strong>
                                                        {noti.user.data.first_name +' '+noti.user.data.last_name}
                                                    </strong>
                                                    <strong className='noti-date'>
                                                        {this.state.moment?this.state.moment.utc(noti.created_at.date).local().fromNow():''} 
                                                    </strong>
                                                   </p>
                                                   :
                                                   
                                                  <p className="text-p-avtar">
                                                      <strong>

                                                        {/* {noti.user_action == 'asset_moderation' ||  noti.user_action  == 'post_moderation' ?
                                                          noti.user_action == 'asset_moderation' ?  ' Asset' : ' Post'
                                                        :  */}
                                                      {noti.user.data.first_name +' '+noti.user.data.last_name}

                                                      </strong>
                                                      <span className='notiDescription'>
                                                        {noti.description} 
                                                        
                                                        {noti.notification_type == 'project'?
                                                          <span className="notiPostTitle">
                                                          
                                                           { noti.project!==undefined?
                                                              noti.project.data.project_title != null?
                                                                utils.convertUnicode(noti.project.data.project_title)
                                                                :'':''}
                                                          </span>  

                                                        :''}
                                                         {noti.notification_type == 'task'?
                                                          <span className="notiPostTitle">
                                                          
                                                           { noti.task!==undefined?
                                                              noti.task.data.task_title != null?
                                                                utils.convertUnicode(noti.task.data.task_title)
                                                                :'':''}
                                                          </span>  

                                                        :''}
                                                        {noti.extra_text?noti.extra_text:''}
                                                      </span>
                                                      
                                                      {noti.post!==undefined || noti.asset!==undefined || noti.campaign!==undefined?
                                                      <span className='notiPostTitle'>
                                                       {noti.post!==undefined && noti.user_action!=="post_moderation"?
                                                        noti.post.data.title != null?
                                                          utils.convertUnicode(noti.post.data.title)
                                                          :''
                                                        :''}
                                                         {noti.campaign!==undefined?
                                                          noti.campaign.data.title != null?
                                                            utils.convertUnicode(noti.campaign.data.title)
                                                            :''
                                                          :''}
                                                          {noti.asset!==undefined && noti.user_action!=="asset_moderation"?
                                                          noti.asset.data.asset_title != null?
                                                            utils.convertUnicode(noti.asset.data.asset_title)
                                                            :''
                                                          :''}
                                                      </span>:''}
                                                      {noti.user_action == 'comment'?
                                                        <span className="noti-comment-description">
                                                          <span className="notiPostTitle">
                                                            {noti.comment?utils.convertUnicode(noti.comment):''}
                                                          </span>
                                                        </span>:''}
                                                      <strong className='noti-date'>
                                                        {this.state.moment?this.state.moment.utc(noti.created_at.date).local().fromNow():''} 
                                                      </strong>
                                                  </p>}
                                               </div>
                                          <div className="like-unlike">
                                                   {noti.thumbnail_image !== '' ?
                                                   <div class="notification-like-image">

                                                       {
                                                           //this.check_image_exists(noti.thumbnail_image,i)
                                                       }
                                                       <img src={noti.thumbnail_image} />
                                                   </div>
                                                   :
                                                       (noti.user_action == 'like' ?
                                                       <div><i class="material-icons">thumb_up</i></div>
                                                       :
                                                       (noti.notification_type == 'LeaderBoard' ?
                                                       <div><i class="material-icons">star_border</i></div>
                                                       :''))}
                                                   </div>

                                               </div>
                                             </div>

                             </div> 
                            )) :<div className="no-notification"><p>No Notification Found</p></div>) :'' }
                           </div>
                    </div>

                </div>
            </div>

        </div>
        )
    }
}
function mapStateToProps(state) {
    return {
        profile: state.profile,
        notificationData: state.notification_data,
    }
}
function mapDispatchToProps(dispatch) {
    return {
		notificationActions: bindActionCreators(notificationActions, dispatch),
    profileSettings: bindActionCreators(profileSettings, dispatch),
    campaignActions: bindActionCreators(campaignActions, dispatch),
    }
}
module.exports = connect(mapStateToProps, mapDispatchToProps)(NotificationPopup);
