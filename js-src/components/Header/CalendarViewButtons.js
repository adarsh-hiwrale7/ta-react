import { Link } from 'react-router';
let flag=false
class CalendarViewButtons extends React.Component {
    constructor(props) {
        super(props)
        
    }
    
    changeView(view) {
      
        this.props.changeHeaderView(view) 
    }
    render() {
        return (
            <div>
            {(this.props.location.pathname!==undefined)?(!this.props.location.pathname.includes("search"))?
            <div className={`viewBtns ${this.props.currentView}-active`}>
                <a
                    title="List view"
                    href='javascript:void(0);'
                    class={this.props.currentView == 'list' ? 'active' : ''}
                    onClick={this.changeView.bind(this, 'list')}
                >
                    <i className='material-icons' title='List View'>view_headline</i>
                </a>
                {this.props.pathValue=='production'?
                <a
                    title="Kanban view"
                    href='javascript:void(0);'
                    className={this.props.currentView == 'kanban' ? 'active' : ''}
                    onClick={this.changeView.bind(this, 'kanban')}
                >
                  <span className  = "kanban-icon"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path fill="none" d="M0 0h24v24H0V0z"/><path d="M4 5v13h17V5H4zm10 2v9h-3V7h3zM6 7h3v9H6V7zm13 9h-3V7h3v9z"/></svg></span>
                </a>
                :
                <a
                    title="Calendar view"
                    href='javascript:void(0);'
                    className={this.props.currentView == 'calendar' ? 'active' : ''}
                    onClick={this.changeView.bind(this, 'calendar')}
                >
                    <i class='material-icons' title='Calendar View'>today</i>
                </a>}

            </div>:"":""}
            </div>
        )
    }
}
module.exports = CalendarViewButtons