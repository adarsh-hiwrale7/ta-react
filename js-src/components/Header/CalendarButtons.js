import { Link } from 'react-router';
var isAllSocial,isFacebook,isTwitter,isLinkedin;
class CalendarButtons extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            view: 'list',
            isAllSocial: true,
            isFacebook: true,
            isTwitter: true,
            isLinkedin: true,
        }
    }

    
    changeSocialFilter(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        this.setState({
          [name]: value,
          calendar_loader: true
        });
        var all_checkbox = target.id;
        if (all_checkbox == 'schedule-filter-all') {
          if (this.textInputAll.checked === false) {
            this.setState({
              isAllSocial: false,
              isFacebook: false,
              isTwitter: false,
              isLinkedin: false
            });
            isAllSocial= false,
              isFacebook= false,
              isTwitter= false,
              isLinkedin= false
          }
          else {
            this.setState({
              isAllSocial: true,
              isFacebook: true,
              isTwitter: true,
              isLinkedin: true
            });
            isAllSocial= true,
            isFacebook= true,
            isTwitter= true,
            isLinkedin= true
          }
        }
        else {
          if (this.textInputFacebook.checked && this.textInputTwitter.checked && this.textInputLinkedin.checked) {
            this.setState({
              isAllSocial: true
            })
            isAllSocial= true
          }
          else {
            this.setState({
              isAllSocial: false
            })
            isAllSocial= false
          }
          if (this.textInputFacebook.checked) {
            this.setState({
              isFacebook: true
            });
            isFacebook= true
          }
          else {
            this.setState({
              isFacebook: false
            });
            isFacebook= false
          }
          if (this.textInputTwitter.checked) {
            this.setState({
              isTwitter: true
            });
            isTwitter= true
          }
          else {
            this.setState({
              isTwitter: false
            });
            isTwitter= false
          }
          if (this.textInputLinkedin.checked) {
            this.setState({
              isLinkedin: true
            });
            isLinkedin= true
          }
          else {
            this.setState({
              isLinkedin: false
            });
            isLinkedin= false
          }
          
        }
        // var path = utils
        //   .removeTrailingSlash(this.props.location.pathname)
        //   .split('/')
        // this.fetchPosts(path[2], path[4], 1)
        this.props.changeSocialFilter(value,isAllSocial,isFacebook,isTwitter,isLinkedin)
      }
    
                                
    render() {
        var monthNames = [
            'January',
            'February',
            'March',
            'April',
            'May',
            'June',
            'July',
            'August',
            'September',
            'October',
            'November',
            'December'
        ]

        let d = new Date()
        let currentYear = d.getFullYear()
        let currentMonth = monthNames[d.getMonth()]
        let currentMonthYear = currentMonth + ' ' + currentYear
        let currentView;
        if (this.props.page == 'feed') {
            currentView = 'list'
        }
        else if (this.props.page == 'feedCalendar') {
            currentView = 'calendar'
        }
        return (
            
            <div className={`HeaderCalendarBtns clearfix ${this.props.page}CalBtns`}>
                <div className={`calendarNavTitleWrapper` }>
                        <div className='calender-nav-btns-wrapper'>

                          <div className='calender-nav-btn'>
                                <a
                                    className='cal-current-month'
                                    href='javascript:void(0);'
                                    title='Current month'
                                >
                                    Today
                            </a>
                            </div>
                            <div className='calender-nav-btn'>
                                <a
                                    className='cal-prev'
                                    href='javascript:void(0);'
                                    title='Previous'
                                >
                                    <i class='material-icons'>keyboard_arrow_left</i>
                                </a>
                            </div>
                          
                            <div className='calender-nav-btn'>
                                <a
                                    className='cal-next'
                                    href='javascript:void(0);'
                                    title='Next'
                                >
                                    <i class='material-icons'>keyboard_arrow_right</i>
                                </a>
                            </div>
                          
                        </div>
                        {
                            this.props.monthWeekview?
                            <div className="calender-view-btns-wrapper">
                            <div className="calender-view-btn active"><a className="cal-month-view" href="javascript:void(0);" title="Month">Month</a></div>
                            <div className="calender-view-btn"><a className="cal-week-view" href="javascript:void(0);" title="Week">Week</a></div>
                        </div>
                        :''
                        }
                       
                        {this.props.currFeed == 'external' ?
                        <div className='calendar-checkboxes-wrapper'>
                            <div className='checkbok-menu-button button-dropdown'>
                            <a class="dropdown-toggle filterChannelDropdown">Filter channels</a>
                            <ul className='dropdown-menu checkboxDropdown'>
                                <li>
                                <div className="form-row checkbox">
                                    <input id="schedule-filter-all" name="isAllSocial" checked={this.state.isAllSocial} onClick={this.changeSocialFilter.bind(this)} ref={(input) => { this.textInputAll = input; }} type="checkbox" />
                                    <label for="schedule-filter-all">All</label>
                                </div>
                                </li>
                                <li>
                                <div className="form-row checkbox">
                                    <input id="schedule-filter-facebook" name="isFacebook" checked={this.state.isFacebook} onClick={this.changeSocialFilter.bind(this)} ref={(input) => { this.textInputFacebook = input; }} type="checkbox" />
                                    <label for="schedule-filter-facebook">Facebook</label>
                                </div>
                                </li>
                                <li>
                                <div className="form-row checkbox">
                                    <input id="schedule-filter-twitter" name="isTwitter" checked={this.state.isTwitter} onClick={this.changeSocialFilter.bind(this)} ref={(input) => { this.textInputTwitter = input; }} type="checkbox" />
                                    <label for="schedule-filter-twitter">Twitter</label>
                                </div>
                                </li>
                                <li>
                                <div className="form-row checkbox">
                                    <input id="schedule-filter-linkedin" name="isLinkedin" checked={this.state.isLinkedin} onClick={this.changeSocialFilter.bind(this)} ref={(input) => { this.textInputLinkedin = input; }} type="checkbox" />
                                    <label for="schedule-filter-linkedin">Linkedin</label>
                                </div>
                                </li>
                            </ul>
                            </div>
                        </div>
                      : ''}
                    </div>
                    


            </div>
        )
    }
}
module.exports = CalendarButtons