// import Autosuggest from 'react-autosuggest';
import ChunkAutosuggest from '../Chunks/ChunkAutoSuggestion';
import { browserHistory } from 'react-router';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import { Link } from 'react-router';
import { Redirect } from 'react-router';
import { renderField } from '../Helpers/ReduxForm/RenderField'
import reactThrottle from '../Chunks/ChunkReactThrottle';
import * as searchActions from '../../actions/searchActions'
import * as authActions from "../../actions/authActions";
import * as feedActions from '../../actions/feed/feedActions';
import * as notificationActions from "../../actions/notificationActions";
import * as utils from '../../utils/utils';
import * as postsActions from "../../actions/feed/postsActions"
import * as smsActions from "../../actions/feed/smsActions"
import $ from '../Chunks/ChunkJquery';
import AssetCategoryPopup from "../Assets/AssetCategoryPopup";
import AnalyticsNav from "../Analytics/AnalyticsNav";
import CalendarViewButtons from './CalendarViewButtons';
import ChunkJqueryDropdown from '../Chunks/ChunkJqueryDropdown';
import FilterButton from './FilterButton';
import FilterWrapper from '../FilterWrapper';
import moment from '../Chunks/ChunkMoment';
import notie from "notie/dist/notie.js"

import ReactDOM from 'react-dom';
import SidebarNarrow from '../Layout/Sidebar/SidebarNarrow';
import { FETCH_ROLES_SUCCESS } from '../../actions/actionTypes';
import NotificationPopup from './NotificationPopup';
import { runInThisContext } from 'vm';
import PerfectScrollbar from '../Chunks/ChunkPerfectScrollbar'
import BroadcastPopup from '../Feed/BroadcastPopup';
var restrictAPI=false;
var searchInput='';
var temp=''
var temp2=''
var languages = []
var a=[],flag=0;
var i=0,searchedValue;
var searchText
var timeout = null;
var NewSearchValue = '';
var issuggetionSelected=false;
var notificationDataPopup  = ''
let roleName='';
class Header extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      showPopup: false,
      jQuery: null,
      showCategoryPopup:false,
      menu_flag:true,
      view: 'list',
      search_change:false,
      previous_path:'',
      next_path:'',
      moment: null,
      mounted: true,
      onGlobalClick:false,
      suggestions: [],
      searchData:{},
      value: '',
      searchForGlobal : '',
      reactThrottle:null,
      perfectScrollbar:null,
      no_suggestion_value:false,
      Autosuggest:null,

    }
    this.handleBroadcast = this.handleBroadcast.bind(this , false)
  }
  changeView(view) {
    this.setState({
        view
    })
    this.props.changeHeaderView(view)
}
componentWillUnmount() {
	ReactDOM.findDOMNode(this).removeEventListener('click', this._handleCloseEvent);
  }
 componentWillMount() {
  let scrollbarSelector = '.inner-nd-sib';
  moment().then(moment => {
    reactThrottle().then(throttle=>{
      PerfectScrollbar().then(scrollbar=>{
        ChunkAutosuggest().then(suggest=>{
        if (this.state.mounted) {
          this.setState({ moment: moment,reactThrottle:throttle,perfectScrollbar:scrollbar,Autosuggest:suggest },()=>{
            // Check for element to apply scrollbar
            if(document.querySelectorAll(scrollbarSelector).length>0){
              const ps = new this.state.perfectScrollbar.default(scrollbarSelector, {
                wheelSpeed: 2,
                wheelPropagation: true,
                minScrollbarLength: 20
              });
            }
          })
        }
        })
      })
    })
  })

    $().then(jquery => {
      var $ = jquery.$
      this.setState({
        jQuery: $
      })
      ChunkJqueryDropdown().then(jqueryDropdown => { })
    })
  }
  componentDidMount(){
    if(this.props.location.pathname&&!this.props.location.pathname.includes("search")&&searchedValue){
      searchedValue=''
    }
    if(this.props.location=="/leaderboard"){
      searchedValue=''
    }

    window.addEventListener('click', this._handleCloseEvent);
    if(restrictAPI!==true){
    this.props.notificationActions.fetchNotificationUnreadCount();
      restrictAPI=true;
      setTimeout(() => {
        restrictAPI=false
      }, 5000);
    }
    document.title = this.props.title+' / Visibly';

    // feed list API
    var location = utils
      .removeTrailingSlash(this.props.location.pathname)
      .split('/')
      if(location[1] !== undefined && (location[1]=='feed' || location[1]=='post')){
        this.props.feedActions.getFeedList();
      }

    // this.setState({search_change:false})
    this.handleInitialize()
  }

  componentWillReceiveProps(nextProps)
  {
    if(this.props.auth.isLogoutClicked==true ){
      searchedValue='';
    }
    if(Object.keys(nextProps.searchData.searchData).length>0)
    {
      if(nextProps.searchData.searchInput==searchedValue){
        a = nextProps.searchData.searchData
      }
        flag=0;
        if(searchedValue !== undefined)
        {
          this.onSuggestionsFetchRequested(searchedValue);
        }
    }


    if(document.getElementsByClassName('react-autosuggest__input')!==undefined)
    if(this.props !== nextProps)
    {
      temp=nextProps.searchInput
      if(searchedValue==undefined&&this.props.searchData.searchInput==''){
            switch(this.props.location.pathname){
                case "/search/default/live":
                    browserHistory.push("/feed/default/live")
                    break;
                case "/search/assets":
                    browserHistory.push("/assets")
                    break;
                case "/search/campaign" :
                    browserHistory.push("/campaigns")
                    break;
                case "/search/user":
                    browserHistory.push("/settings/users")
                    break;
                case "/search/department":
                    browserHistory.push("/settings/departments")
                    break;
            }
      }
      if(temp2!==temp && temp!==undefined && temp!==''&& issuggetionSelected!==true)
      {
        temp2=temp;
        this.handleInitialize()
      }
      if(searchedValue!==undefined){
        this.setState({
          value : searchedValue
        })
      }else{
        this.setState({
          value : nextProps.searchInput
        })
      }
    }

  }
  handleInitialize() {
      // const search = temp
      let initData = {
        search:temp
      }
    this.props.initialize(initData)
  }
  eventPopup(){
    this.props.event_new_openpopup();
  }
  closefeedpopup() {
    if(this.props.Header_onclick)
    {
      this.props.Header_onclick();
    }
  }

  //modration function
  changeFilterPost(e){
     this.props.changefilter(e);

  }
  notifiyuser(){
    this.props.notifiyuser();
  }
  handleAssetsCreation(catName) {
    this.props.header_function1(catName);
  }
  handleAssetFolderCreation() {
    this.props.header_function2();
  }
  handleAssetCategoryCreation() {
    this.props.header_function3();
  }
  //view asset click
  viewAssetClick(e){
    this.props.viewAssetClick(e);
    this.setState({
      showCategoryPopup:false
    })
  }
  modHeader(e){
    this.props.headerMobIndex(e);
  }
  openeditdroupdown(assets){
    var objForEditor = [];
    objForEditor.push({
      name:assets[0].title + "."+ assets[0].media_extension,
      preview:assets[0].media_url,
      size:assets[0].media_size,
      type:assets[0].media_type
    })
    objForEditor['files'] = objForEditor;
    this.props.showeditor(objForEditor);
    this.setState({
      showCategoryPopup:false
    })
  }
  //whe click on archive
  archiveAssetClick(e){
    this.props.archiveAssetClick(e);
    this.setState({
      showCategoryPopup:false
    })
  }
  //edit folder click
  editFolderClick(e){
    this.props.editFolderClick(e);
  }
  //archive folder click
  archiveFolderClick(e){
    this.props.archiveFolderClick(e);
  }
  //unarchive asset
  unArchiveOpen(action){
    this.setState({showCategoryPopup:action});
  }
  unArchiveAssetClick(selectedMoveFolder){
    this.props.unArchiveAssetClick(selectedMoveFolder);
  }
  unArchiveFolderClick(selectedMoveFolder){
    this.props.unArchiveFolderClick(selectedMoveFolder);
  }
  //move asset click
  moveAssetClick(e){
    this.props.moveAsset(e);
  }
  //move folder
  moveFolderClick(e){
    this.props.moveFolder(e);
  }
  moreActionClick(e){
    this.setState({showCategoryPopup:false});
  }
  //call as a props when asset link has been called
  assetLinkClick(e){
    this.props.assetLinkClick(e);
  }
  updateFeedAction(feedId){
    this.props.updateFeedAction(feedId);
  }


  closeSearch(e){
    if(roleName!=='guest'){
      browserHistory.push('/feed/default/live');
    }
  }
  closeFeed(e){
    var me = this;
    document.body.classList.remove('FeedPage');
    notie.confirm(
      `Are you sure you want to exit from this Feed?`,
      'Yes',
      'No',
      function () {
        me.props.feedActions.exitFeed(e);
      },
      function () { }
    )
  }
  createFeedOpen(){

      if(this.props.title == "Feeds"){
          this.props.openCreateFeed();
      }
  }

  /**
   * @author disha
   * when export button is clicked in analytics page header this function will call and it will call other function in analytics container
   * @param {*} e this
   */
  exportAnalyticsData(e){
    this.props.exportAnalytics(e)
  }
  onGlobalClick(flag,_this)
  {
    this.setState({onGlobalClick:flag})
  }

  handleSubmit_autosuggest(event){
    if( searchedValue==''){
      switch(this.props.location.pathname){
        case "/search/default/live":
            browserHistory.push("/feed/default/live")
            break;
        case "/search/assets":
            browserHistory.push("/assets/")
            break;
        case "/search/campaign":
            browserHistory.push("/campaigns")
            break;
        case "/search/user":
            browserHistory.push("/settings/users")
            break;
        case "/search/department":
            browserHistory.push("/settings/departments")
            break;
    }
  }else if(searchedValue == undefined){
    notie.alert("error","Please enter some text to search", 3)
  }
  else{

      this
        .props
        .feedActions
        .resetPosts();
      this.props.feedActions.fetchDefaultLive(1,10,false,false,null,searchedValue);
      browserHistory.push({
        pathname: "/search/default/live",
        state: { isenterSubmit: true }
      })
    }
  }

// For Auto-Suggestion

escapeRegexCharacters(str) {
    return str.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
}

getSuggestions(value) {
  var me = this;
    var final=[]
    const escapedValue = this.escapeRegexCharacters(searchedValue.trim());

    if (escapedValue === '') {
      return [];
    }

    if(a!==[] && a!==undefined)
    {
    const regex = new RegExp('^' + escapedValue, 'i');
    return Object.keys(a)
      .map(function(key,index){
        if(a[key].data)
        {
            var temp={}
            var temp2=[]
            var b=a[key].data
            for(var i=0;i<2;i++)
            {
                temp={}
                if(b[i]!==undefined)
                {
                   temp.key=key

                   if(key=='post')
                   {
                        if(b[i].post.data.assetDetail!==undefined)
                        temp.photo=b[i].post.data.assetDetail.data.thumbnail_url
                        temp.avatar=b[i].post.data.postOwner.data.avatar
                        temp.firstname=b[i].post.data.postOwner.data.firstname
                        temp.lastname=b[i].post.data.postOwner.data.lastname
                        temp.dept=b[i].post.data.postOwner.data.Department.data.name
                        temp.postType = b[i].post.data.type
                        var t=me.state
                        .moment? me.state
                               .moment(b[i].post.data.created.date)
                               .format("HH:mm"):'';
                               var d=me.state
                               .moment?me.state
                               .moment(b[i].post.data.created.date)
                               .format("DD/MM/YYYY"):'';
                               let concatenatedDateTime = d + " " + t;
                           let timeStamp = me.state
                           .moment?me
                             .state
                             .moment(concatenatedDateTime, 'DD/MM/YYYY HH:mm')
                             .format("X"):'';
                             var finalDate;
                             me.state.moment?
                             finalDate=me.state.moment.unix(timeStamp).format('dddd, Do MMMM  YYYY ')+'at'+me.state.moment.unix(timeStamp).format(' h:mm a')
                             :''
                        temp.posttime=finalDate
                        temp.username=temp.firstname +' '+ temp.lastname
                        if(b[i].post.data.assetDetail.data.length!== 0){
                          temp.media_url = b[i].post.data.assetDetail.data[0].media_url;
                        }
                        temp.detail= utils.convertUnicode(b[i].post.data.detail);
                        temp.type=b[i].post.data.type
                        temp.id=b[i].post_id
                        temp2.push(temp);
                   }
                   else if(key=='asset')
                   {
                        temp.photo=b[i].thumbnail_url
                        temp.avatar=b[i].profileImage
                        temp.id=b[i].identity
                        temp.title=b[i].title
                        temp.username=b[i].uploaded_by
                        temp.detail= utils.convertUnicode(b[i].detail);
                        var t=me.state
                        .moment? me.state
                               .moment(b[i].created_date.date)
                               .format("HH:mm"):'';
                               var d=me.state
                               .moment?me.state
                               .moment(b[i].created_date.date)
                               .format("DD/MM/YYYY"):'';
                               let concatenatedDateTime = d + " " + t;
                           let timeStamp = me.state
                           .moment?me
                             .state
                             .moment(concatenatedDateTime, 'DD/MM/YYYY HH:mm')
                             .format("X"):'';
                             var finalDate;
                             me.state.moment?
                             finalDate =me.state.moment.unix(timeStamp).format('dddd, Do MMMM  YYYY ')+'at'+me.state.moment.unix(timeStamp).format(' h:mm a'):''
                        temp.createdDate= finalDate
                        temp.media_type= b[i].media_type
                        temp.thumbnail_url=b[i].thumbnail_url
                        temp2.push(temp);
                   }
                   else if(key=='campaign')
                   {
                       temp.avatar=b[i].User.data.avatar
                       temp.id=b[i].identity
                       temp.description=b[i].description
                       temp.title=b[i].title
                       var startDate=me.state.moment?me.state.moment.unix( b[i].start_date).format("DD/MM/YYYY"):'';
                      var endDate= me.state.moment?me.state.moment.unix( b[i].end_date).format("DD/MM/YYYY"):'';
                      temp.startDate = startDate
                       temp.endDate = endDate
                       temp.targeted_audience=b[i].targeted_audience
                       temp2.push(temp);
                   }
                   else if(key=='user' && roleName!== "employee")
                   {
                       temp.avatar=b[i].avatar
                       temp.id=b[i].identity
                       temp.avatar=b[i].avatar;
                       temp.name=b[i].firstname+" "+b[i].lastname;
                       temp.email=b[i].email;
                       temp.role=b[i].role.data? b[i].role.data[0].role_display_name:''
                       temp.department = b[i].Department? b[i].Department.data.name:''
                       temp2.push(temp);

                   }
                   else if(key=='department' &&  roleName!== "employee")
                   {
                        temp.id=b[i].identity
                        temp.name=b[i].name;
                        temp.total_user=b[i].total_user
                        temp.total_post=b[i].total_post
                        temp.office_name = b[i].office_name
                        temp2.push(temp);

                   }

                }
                final.push(temp)
            }

            return {
                a:temp2
             };
        }
      })
      //.filter(section => section.languages.length > 0);
    }
}

getSuggestionValue(suggestion) {
    return (
      suggestion.image_src,
      suggestion.name
    );
}

renderSuggestion(suggestion) {

    if(suggestion!==undefined && suggestion!==null && suggestion!=="")
    return [suggestion].map(temp => {
        if(temp.key=="post")
        {
          return (

          <div className = "parent-container-for-seach-feed">
              <div className = "inner-parent-container">
            <div className = "parent-of-search clearfix">

                     <div className = "user-avatar searchAvatar">
                     {/* onerror function is for rendering defalu image if any error in image url */}
                       <img src={temp.avatar}  onError={(e) => { e.target.src="https://app.visibly.io/img/default-avatar.png"}}/>
                     </div>
                     <div className = "user-details">
                       <div className = "inner-div-for-user-details searchDescription">
                           <span className = "userName">{temp.username}</span>
                           <span className = "post-time">{temp.posttime} </span>
                           {/* <span className = "post-time">Wednesday, August 1st 2018 at 12:09 pm </span> */}
                           <span className = "post-decription">{temp.detail}</span>
                       </div>
                       {temp.postType=='internal' ?
                        <span className='socialIconWrapper autosuggestion-internal'>
                          <a class="internal-icon" title="Internal channel "><i class="material-icons">vpn_lock</i></a>
                        </span>:''}
                    {temp.postType=="external" ?    <span className='socialIconWrapper'>
                                <a className='fb'>
                                    <i className='fa fa-facebook' aria-hidden='true' />
                                </a>
                                <a className='tw'>
                                    <i className='fa fa-twitter' aria-hidden='true' />
                                </a>
                                <a className='li'>
                                    <i className='fa fa-linkedin' aria-hidden='true' />
                                </a>
                       </span> :''}
                     </div>
                     <div className = "post-details searchPostImage Asset-search-desing-for-auto-sugesstion ">
                    {
                      (temp.media_type!==undefined && temp.media_type !== null && temp.media_type!== '' )?
                      (
                          temp.media_type.includes("image")?
                        <img src={temp.media_url}  onError={(e) => { e.target.src="https://app.visibly.io/img/default-avatar.png"}}/>
                        :
                          temp.media_type.includes("video")?
                          <img src={temp.media_url}  onError={(e) => { e.target.src="https://app.visibly.io/img/default-avatar.png"}}/>
                          :temp.media_type.includes("application/vnd.openxmlformats-officedocument.wordprocessingml.document") || temp.media_type.includes("application/msword") ?
                            <div className = "application-doc doc-file">
                              <div className = "application-doc doc-file">
                                <i class="fa fa-file-o fa-3x"></i>
                            </div>
                          </div>
                            : temp.media_type.includes("application/pdf") ?
                            <div className = "application-doc pdf-file">
                              <i class="fa fa-file-pdf-o fa-3x"></i>
                            </div>
                              : temp.media_type.includes("application/vnd.ms-powerpoint") ?
                                <div className = "application-doc ppt-file">
                                  <i class="fa fa-file-powerpoint-o fa-3x"></i>
                                </div>
                                :
                                <div className = "application-doc doc-file">
                                    <i class=" fa fa-file-excel-o fa-3x"></i>
                         </div>
                      ):
                         <div/>

                      }


                     </div>

                  </div>
              </div>
           </div>



              );
        }

        else if(temp.key=="asset")
        {
            return(

              <div className = "parent-container-for-seach-feed Asset-search-desing-for-auto-sugesstion">
              <div className = "inner-parent-container">
                   <div className = "parent-of-search clearfix">
                     <div className = "user-avatar searchAvatar">
                       <img src={temp.avatar} onError={(e) => { e.target.src="https://app.visibly.io/img/default-avatar.png"}}/>
                     </div>
                     <div className = "user-details">
                       <div className = "inner-div-for-user-details searchDescription">
                           <span className = "userName">{temp.username}</span>
                           <span className = "post-time"> {temp.createdDate}</span>
                           {/* <span className = "post-time">Wednesday, August 1st 2018 at 12:09 pm </span> */}
                           <span className = "post-decription assetDescription">{temp.detail}</span>
                       </div>
                     </div>
                     <div className = "post-details asset-details searchPostImage">
                     {
                       temp.media_type.includes("image")?

                       <img src={temp.thumbnail_url}  onError={(e) => { e.target.src="https://app.visibly.io/img/default-avatar.png"}}/>
                       :
                        temp.media_type.includes("video")?
                        <img src={temp.thumbnail_url} onError={(e) => { e.target.src="https://app.visibly.io/img/default-avatar.png"}}/>
                        :temp.media_type.includes("application/vnd.openxmlformats-officedocument.wordprocessingml.document") ?
                          <div className = "application-doc doc-file">
                            <div className = "application-doc doc-file">
                              <i class="fa fa-file-o fa-3x"></i>
                           </div>
                         </div>
                          : temp.media_type.includes("application/pdf") ?
                              <div className = "application-doc pdf-file">
                                <i class="fa fa-file-pdf-o fa-3x"></i>
                              </div>
                            : temp.media_type.includes("application/vnd.ms-powerpoint") ?
                              <div className = "application-doc ppt-file">
                                 <i class="fa fa-file-powerpoint-o fa-3x"></i>
                              </div>
                              :
                                <div className = "application-doc doc-file">
                                  <i class=" fa fa-file-excel-o fa-3x"></i>
                                </div>

                     }
                     </div>

                  </div>
              </div>
           </div>


            );
        }
        else if(temp.key=="campaign")
        {
            return(

              <div className = "parent-container-for-seach-feed campaign-search-desing-for-auto-sugesstion">
              <div className = "inner-parent-container">
                   <div className = "parent-of-search clearfix">
                     <div className = "user-avatar searchAvatar">
                      {/* in camping admin avatar */}
                         <img src={temp.avatar} onError={(e) => { e.target.src="https://app.visibly.io/img/default-avatar.png"}}/>
                     </div>
                     <div className = "user-details remove-padding-user-details">
                       <div className = "inner-div-for-user-details searchDescription">
                           <span className = "userName campaign-title searchCampaignTitle">{temp.title}</span>
                           <span className = "post-time start-date pink-font-in-global-search"><span className = "start-end-date-search">Start Date :</span> <span className = "no-of-global-search">{temp.startDate} </span></span>
                           <span className = "post-decription end-date pink-font-in-global-search"><span className = "start-end-date-search">End Date   :</span> <span className = "no-of-global-search">{temp.endDate}</span></span>
                       </div>

                     </div>
                    </div>
              </div>
           </div>




            );
        }
        else if(temp.key=="user" && roleName!=="employee")
        {
            return(
              <div className = "parent-container-for-seach-feed user-search-desing-for-auto-sugesstion">
              <div className = "inner-parent-container">
                   <div className = "parent-of-search clearfix">
                     <div className = "user-avatar searchAvatar">
                      {/* in camping admin avatar */}
                         <img src={temp.avatar} onError={(e) => { e.target.src="https://app.visibly.io/img/default-avatar.png"}}/>
                     </div>
                     <div className = "user-details remove-padding-user-details">
                       <div className = "inner-div-for-user-details searchDescription">
                           <span className = "userName searchUserName">{temp.name}</span>
                           <span className = "userName searchUserName ">{temp.email}</span>
                           <div className = "clearfix role-user-container">
                             <div className = "role-user title-role">  Role : </div>
                              <div className = "role-user"> {temp.role} </div>
                            </div>
                            <div className = "clearfix role-user-container">
                             <div className = "role-user title-role">  Department : </div>
                              <div className = "role-user"> {temp.department} </div>
                            </div>

                       </div>
                     </div>
                    </div>
              </div>
           </div>

            );
        }
        else if(temp.key=="department" && roleName!=="employee")
        {
            return(

              <div className = "parent-container-for-seach-feed user-search-desing-for-auto-sugesstion">
              <div className = "inner-parent-container">
                   <div className = "parent-of-search clearfix">
                     {/* <div className = "user-avatar">  </div> */}
                       <div className = "department">
                          <p><i class="material-icons">business</i></p>
                       </div>
                      {/* in camping admin avatar */}


                     <div className = "user-details remove-padding-user-details">
                       <div className = "inner-div-for-user-details">
                           <span className = "userName department-name searchDepartName">{temp.name}  </span>
                           {temp.office_name && temp.office_name.trim()!==''?
                           <span className = "global-office-location">
                                <i className="material-icons">room </i>
                                <span className="office-name-popup">{temp.office_name?temp.office_name:''}</span>
                           </span>:''}
                           <div className = "clearfix role-user-container">
                             <div className = "role-user title-role"> Total Posts : </div>
                              <div className = "role-user"> {temp.total_post} </div>
                            </div>
                            <div className = "clearfix role-user-container">
                             <div className = "role-user title-role"> Total Users :</div>
                              <div className = "role-user"> {temp.total_user}</div>
                            </div>
                           {/* <span className = "post-time total-post pink-font-in-global-search searchDepartPost">Total Posts : <span className = "no-of-global-search"> {temp.total_post} </span></span>
                           <span className = "post-decription Total-user pink-font-in-global-search searchDepartUsers ">Total Users : <span className = "no-of-global-search">{temp.total_user}</span></span>
                        */}
                       </div>

                     </div>
                    </div>
              </div>
           </div>


            );
        }
      })
}
seeAllClick(selectionType){
  this.props.searchActions.seeAllClick();
  switch(selectionType){
    case "feed":
      browserHistory.push({
        pathname: "/search/default/live",
        state: { seeAllClick: true }
      })
      break;

    case "asset":
      browserHistory.push({
        pathname: "/search/assets",
        state: { seeAllClick: true }
      })
      break;

    case "campaign":
      browserHistory.push({
        pathname: "/search/campaign",
        state: { seeAllClick: true }
      })
      break;

    case "user":
      browserHistory.push({
        pathname: "/search/user",
        state: { seeAllClick: true }
      })
      break;

    case "department":
      browserHistory.push({
        pathname: "/search/department",
        state: { seeAllClick: true }
      })
      break;
  }
}

renderSectionTitle(section) {
  var dom=document.querySelector(".react-autosuggest__container").classList
  this.props.searchData.searchInput==NewSearchValue
  if(section!==undefined ){
    // dom.remove("loading")
    var a=section.a;
    var c;
    flag=0;
    return a.map(temp =>
        {
                if(temp.key!==undefined)
                {
                   if(temp.key == 'post' && flag == 0)
                   {
                       flag=1
                       return (

                            <strong>Feed <span class="see-all-text" onClick={this.seeAllClick.bind(this,"feed")}> See all</span></strong>
                        );
                   }
                   else if(temp.key=='asset' && flag==0 )
                   {
                       flag=1
                       return (
                            <strong>Assets <span class="see-all-text" onClick={this.seeAllClick.bind(this,"asset")}> See all</span></strong>
                        );
                   }
                   else if(temp.key=='campaign' && flag==0 )
                   {
                       flag=1
                       return (
                        <strong>Campaign <span class="see-all-text" onClick={this.seeAllClick.bind(this,"campaign")}> See all</span></strong>
                        );
                   }
                   else if(temp.key=='user' && flag==0 && roleName!=="employee")
                   {
                       flag=1
                        return (
                        <strong>user <span class="see-all-text" onClick={this.seeAllClick.bind(this,"user")}> See all</span></strong>
                        );
                   }
                   else if(temp.key=='department' && flag==0 && roleName!=="employee")
                   {
                        flag=1
                        return (
                        <strong>department <span class="see-all-text" onClick={this.seeAllClick.bind(this,"department")}> See all</span></strong>
                        );
                   }
                }
        }
    )
  }
  else{
  return []
  }
}

getSectionSuggestions(section) {
    if(typeof section!=='undefined'){
      return section.a;
    }
    else{
    return
     } ;
}



onChange = (event, { newValue, method }) => {
  var dom=document.querySelector(".react-autosuggest__container").classList
  if(method=="type"){
  if(newValue==''){
    a='';
    temp='';
    NewSearchValue=newValue
   this.setState({suggestions:[]})
  }
  if(newValue!== undefined){
    if(newValue.trim()=='' || this.state.suggestions.length==0){
      dom.remove("loading");
    }

    if(newValue.trim()!==''){
    dom.add("loading");
    }
  }

    var me = this
    if(newValue==undefined)
    {
      newValue=this.props.searchData.searchInput
    }
    this.setState({
      value: newValue
    });

    clearTimeout(timeout)

    timeout=setTimeout(function(){
      if(newValue.trim()!== ''&& NewSearchValue.toLowerCase()!==newValue.toLowerCase()){
        NewSearchValue=newValue
        if(me.props.location.pathname!==undefined && me.props.location.pathname.includes("search")){
          me.props.searchActions.searchGlobal(newValue,null,true);
        }
        else{
          me.props.searchActions.searchGlobal(newValue,null,true,false,true);
        }
      }
    },700)
    searchedValue=newValue;
    // this.setState({suggestions:[]})
    // a=[];
  }
};

onSuggestionsFetchRequested = ( value ) => {
  if(value.reason=="input-changed" ){
    a=[];
  }
  let search_result = this.getSuggestions(value);
  let no_suggestion_value = true;
  if(search_result.length>0){

    search_result.map((result_data,index)=>{

      if(result_data.a.length>0 && no_suggestion_value!==false){
        no_suggestion_value = false
      }
    })
    if(no_suggestion_value ==true){
      this.setState({
        no_suggestion_value : true
      })
    }else{
      this.setState({
        no_suggestion_value : false
      })
    }
  }
      this.setState({
        suggestions: this.getSuggestions(value)
      });
};

onSuggestionsClearRequested = () => {
    i=0
      this.setState({
        suggestions: [],
        no_suggestion_value:false
      });
};

onSuggestionHighlighted({ suggestion })
{
}

onSuggestionSelected(event, { suggestion, suggestionValue, suggestionIndex, sectionIndex, method })
{
  document.getElementById("react-autowhatever-1")?document.getElementById("react-autowhatever-1").classList.add("hide"):'';
  issuggetionSelected = true
    this.filter_Suggestion(suggestion)
}

filter_Suggestion(suggestion)
{
  var dom=document.querySelector(".react-autosuggest__container")? document.querySelector(".react-autosuggest__container").classList:'';
  this.props.searchActions.isClickOnSuggestion()
  this.props.searchActions.searchGlobal(this.props.searchData.searchInput,suggestion,true);
  if(suggestion.key=="post"){
    dom.remove("loading");
    browserHistory.push({
      pathname: "/search/default/live",
      state: { fromSuggestionClick: true }
    })
  }

  else if(suggestion.key=="asset"){
    dom.remove("loading");
    browserHistory.push({
      pathname: "/search/assets",
      state: { fromSuggestionClick: true }
    })
  }

  else if(suggestion.key=="campaign"){
    dom.remove("loading");
    browserHistory.push({
      pathname: "/search/campaign",
      state: { fromSuggestionClick: true }
    })
  }

  else if(suggestion.key=="user"){
    dom.remove("loading");
    browserHistory.push({
      pathname: "/search/user",
      state: { fromSuggestionClick: true }
    })
  }

  else if(suggestion.key=="department"){
    dom.remove("loading");
    browserHistory.push({
      pathname: "/search/department",
      state: { fromSuggestionClick: true }
    })
  }
}
// Auto Suggestion End's Here
searchButtonResponsive(){
 this.setState({
  searchForGlobal : 'show'
 })
}
searchBarClose(e){
  //document.getElementById("searchBox").value = '';

  document.getElementById("react-autowhatever-1").classList.add("hide")
  this.setState({
    searchForGlobal : '',
    value:'',
    no_suggestion_value:false
  })
  this.render();

}
showNotification(){
  var element = document.getElementById("notification-popup-on-header");
  element.classList.toggle("userpopup-notification");
  document.querySelectorAll('body')[0].classList.toggle('notification-show');

  var userpopup = document.getElementsByClassName('userpopup-notification');

  for(var i = 0 ; i <= userpopup.length ; i++){


    if(userpopup.length > 0){

     document.body.classList.remove('hey');
    }
}
}
notification(){
this.props.notificationActions.fetchNotification();
}

collapsibleNavigation(){
    document.body.classList.remove('collapsibleNavdrawOnHover');
    document.body.classList.toggle('ShowCollapsibleForNavigation');
 }

 // takes value from broadcastpop form and sends in actions and also closes popup on success
 handleSubmitBroadcast(val) {
      var ObjToSend = {
        "message": val.message,
        "messageType": val.messageType,
      }
      this.props.smsActions.getSmsValue(ObjToSend);
}


// handle open and close of broadcast popup
 handleBroadcast(){
  this.setState({
    showPopup: !this.state.showPopup
  })
 }

 componentDidUpdate(prevProps, prevState){
   if(this.props.smsValue.smsValue != prevProps.smsValue.smsValue && this.props.smsValue.smsValue !== null  ){
     this.setState({
       showPopup: false
     })
   }
 }

  render() {


    //For Autosuggestion
    const { value, suggestions } = this.state;
    const inputProps =
    {
        id: "searchBox",
        name : "search",
        placeholder: "Search",
        value,
        onChange: this.onChange
    };
    if(this.props.location !== undefined){
     var path = utils
    .removeTrailingSlash(this.props.location.pathname)
    .split('/');
    }
    let currentView;
    if (this.props.page == 'feed') {
        currentView = 'list'
    }
    else if (this.props.page == 'feedCalendar') {
        currentView = 'calendar'
    }
   if(localStorage.getItem('roles') !== null){
      roleName = JSON.parse(localStorage.getItem('roles'))[0]['role_name'];
    }
    if (this.props.title == 'Campaigns') {
      var str = "New Campaign";
    } else if (this.props.title.indexOf('FEED') >= 0) {
      var str = "New Post";
    } else {
      var str = "new";
    }
    var classstyle={
      width:"300px"
    }

    const { handleSubmit, pristine, submitting } = this.props
    var New_Nav = this.props.nav;
    var dropzoneStyle = {
      background: '#256eff',
      cursor: 'pointer',
      color: '#fff',
      fontSize: '14px',
      fontWeight: '700',
      height: '36px',
      width: 'auto',
      display: 'inline-block',
      lineHeight: '36px',
      marginBottom: '5px'
    }
      var categoryname=typeof (this.props.breadCrum) !=='undefined' ?
        this.props.breadCrum.length>0?
          this.props.breadCrum[0].category
        :''
      :''
      if(this.props.selectedAssetID){

    if(this.props.selectedAssetID.length!=0){
    var index = this.props.selectedAssetID[0].media_type.indexOf('/')
    var type = 'application'
    var assets = this.props.assets

    if (index !== -1) {
      type = this.props.selectedAssetID[0].media_type.substr(0, index)
    } else {
      type = this.props.selectedAssetID[0].media_type
    }
    }
  }
  var redirectassets;
  path[2]=='assets'?  redirectassets = '/assets':   redirectassets = '#';
    return (
        <div className='header-main'>
          {this.state.showPopup == true ? 
            <BroadcastPopup 
            handleBroadcast={ () => this.handleBroadcast}
            onSubmit={this.handleSubmitBroadcast.bind(this)}
            broadcastLoading={this.props.smsValue.smsLoading}
            />  
            : '' }
        <header className='header'>
          <div className='header-inner clearfix'>
          <div class="header-left">
    <a id="inner_main_menu_handle" href="#" class="menu-toggle-handle" onClick={this.open_menu.bind(this)}>
        <i class="material-icons menu-icon">menu</i>
        <i class="material-icons close-icon">close</i>
    </a>
    <a class="logo" onClick = {this.showNotification.bind(this)}>
    {/* {this.state.reactThrottle?
    <this.state.reactThrottle.Throttle time="5000" handler="onClick"> */}
        <div class="notifaction-data" onClick={this.notification.bind(this)}>
        <div className="notification_count_container" >

                   {
                    (this.props.notificationData.noticount==0)
                    ?
                      ''
                      :
										 <div class="notification-div">
										 {
											 (this.props.notificationData.noticount>100)
											 ?
											 <span className='noti-star-count'>*</span>
											 :
                       this.props.notificationData.noticount
										 }
										 </div>
                    }


        </div>
            <svg class="nav__image image" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 69 95.95">
                <defs></defs>
                runInThisContext

                <g id="Layer_2" data-name="Layer 2">
                    <g id="Layer_1-2" data-name="Layer 1">
                        <path class="svg-cls-2" d="M17.21,22V49.66H40.52V72.71H67.85V22ZM57.73,62.58H50.64V39.52H27.33v-7.4h30.4Z"></path>
                        <path class="svg-cls-3" d="M50.64,90.25V62.57H27.33V39.52H0V90.25ZM10.12,49.66h7.09V72.71H40.52v7.4H10.12Z"></path>
                    </g>
                </g>
            </svg>
        </div>
        {/* </this.state.reactThrottle.Throttle>
        :''} */}
    </a>
    <div className = "notification-on-header userpopup-notification" id = "notification-popup-on-header">
              <NotificationPopup
              customFeedList ={this.props.customFeedList}/>
              </div>

              
    {/* <div class="notification-on-header userpopup-notification" id="notification-popup-on-header">
        <div class="dashboard-notification dashboard-logo" id="dashboard-notification-popup">
            <div class="inner-dashboard-notification">
                <div>
                    <div class="notication-header">
                        <div class="triangle-left"></div>
                        <p>NOTIFICATIONS</p>
                        <a>Mark all as read</a>
                    </div>
                    <div class="main-notification-body ps">
                        <div class="notification-body">
                            <div class="no-notification">
                                <p>No Notification Found</p>
                            </div>
                        </div>
                        <div class="ps__rail-x">
                            <div class="ps__thumb-x" tabindex="0"></div>
                        </div>
                        <div class="ps__rail-y">
                            <div class="ps__thumb-y" tabindex="0"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> */}
    {/* <h1>
       {this.props.title}
    &nbsp;

        <div class="add-circle-btn">
            <i class="material-icons" onClick={this.createFeedOpen.bind(this)}>add_circle_outline</i>
        </div>
    </h1>  */}
</div>
{path[1] != 'chat' ?
<div className='header-right'>

            <div className='header-right-inner clearfix'>
            {/*
            this is for trending tag display
            */}

            {
            this.props.hash?
            <div className="trendingTag">{this.props.hash} <a href={redirectassets} title="trending tag"><i class="material-icons">clear</i></a></div>
            :''
            }

            {(this.props.page=='campaigns' || (this.props.page=='feed' && this.props.currFeedPosts=='scheduled')) ? (this.props.location.pathname !=="/search/campaign") ? 
              // ((this.props.page=='feed' && this.props.currFeedPosts=='scheduled')) ? (this.props.location.pathname !=="/search/campaign") ?
              <CalendarViewButtons
                page={this.props.page}
                currFeedPosts={this.props.currFeedPosts}
                feedUrl = {this.props.feedUrl}
                calendarUrl={this.props.calendarUrl}
                changeHeaderView={this.props.changeHeaderView}
                location = {this.props.location}
                location_cat={this.props.location_cat}
                currentView={this.props.currentView}
              />
              :'':""
            }
            <div className="culture-dashboard-tab">
            {(this.props.location.pathname!==undefined && this.props.location.pathname.includes("analytics"))?

              <div className="culture-dashboard-tabbing-sec clearfix">
               <Link
            to='/analytics/adoption'
            title='Adoption'
            className={this.props.location.pathname == '/analytics/adoption' ? 'active' : ''}
          >
           Adoption
          </Link>
          <Link
            to='/analytics/activity'
            title='Activity'
            className={this.props.location.pathname == '/analytics/activity' ? 'active' : ''}
          >
           Activity
          </Link>
          <Link
            to='/analytics/engagement'
            title='Reach'
            className={this.props.location.pathname == '/analytics/engagement' ? 'active' : ''}
          >
           Reach
          </Link>
          <Link
            to='/analytics/values'
            title='NVS'
            className={this.props.location.pathname == '/analytics/values' ? 'active' : ''}
          >
           NVS
          </Link>
          <Link
            to='/analytics/NPS'
            title='NPS'
            className={this.props.location.pathname == '/analytics/NPS' ? 'active' : ''}
          >
           NPS
          </Link>
          <Link
            to='/analytics/culture'
            title='culture'
            class={this.props.location.pathname == '/analytics/culture' ? 'active' : ''}
          >
            Culture
           </Link>
           <Link
            to='/analytics/resonance'
            title='resonance'
            class={this.props.location.pathname == '/analytics/resonance' ? 'active' : ''}
            >
            Resonance
           </Link>
             </div>

           : ''}
            </div>
             <div  className="trending-tag-feed-responsive">

            </div>

            {/* For curation filter */}
              <ul className='dd-menu'>
              <li>

      {
        roleName!=="guest" && path[1]!='chat' && this.state.Autosuggest !== null?
        <div>
          {
            roleName=="admin" || roleName == 'super-admin' ?
            <div className="broadcast-wrapper" onClick={this.handleBroadcast.bind(this)}>
              <i class="material-icons">stay_primary_portrait</i>
              <div className="tooltip">BROADCAST</div>
              </div>
            :''   
          }
                <div className="autosuggestWrapper">
                
                <div className={`autosuggestInner ${this.state.searchForGlobal}`}>
                  <this.state.Autosuggest
                              multiSection={true}
                              suggestions={suggestions}
                              onSuggestionsFetchRequested={this.onSuggestionsFetchRequested.bind(this)}
                              onSuggestionsClearRequested={this.onSuggestionsClearRequested.bind(this)}
                              getSuggestionValue={this.getSuggestionValue.bind(this)}
                              renderSuggestion={this.renderSuggestion.bind(this)}
                              renderSectionTitle={this.renderSectionTitle.bind(this)}
                              getSectionSuggestions={this.getSectionSuggestions.bind(this)}
                              onSuggestionHighlighted={this.onSuggestionHighlighted.bind(this)}
                              onSuggestionSelected={this.onSuggestionSelected.bind(this)}
                              inputProps={inputProps}
                              // highlightFirstSuggestion={true}
                              focusInputOnSuggestionClick={false}
                  />
                  {
                    this.state.no_suggestion_value==true &&
                    !document.querySelector(".react-autosuggest__container").classList.contains("loading")?
                    <div id="react-autowhatever-1" role="listbox" class="react-autosuggest__suggestions-container no-suggestion-wrapper">
                      <div className="no-suggestions">
                        No result found.
                      </div>
                    </div>
                       :''
                  }
                  <button className="enterSuggestion" onClick={this.handleSubmit_autosuggest.bind(this)}><i class="material-icons">search</i></button>
                  {this.state.value ?
                    <button className = "btn close-button" onClick= {this.searchBarClose.bind(this)}><svg height="20" width="20" viewBox="0 0 20 20" aria-hidden="true" focusable="false" class="css-19bqh2r"><path d="M14.348 14.849c-0.469 0.469-1.229 0.469-1.697 0l-2.651-3.030-2.651 3.029c-0.469 0.469-1.229 0.469-1.697 0-0.469-0.469-0.469-1.229 0-1.697l2.758-3.15-2.759-3.152c-0.469-0.469-0.469-1.228 0-1.697s1.228-0.469 1.697 0l2.652 3.031 2.651-3.031c0.469-0.469 1.228-0.469 1.697 0s0.469 1.229 0 1.697l-2.758 3.152 2.758 3.15c0.469 0.469 0.469 1.229 0 1.698z"></path></svg></button>
                  :''}

                  </div>
                </div>
               <button className = "btn serach-for-responsive" onClick = {this.searchButtonResponsive.bind(this)}><i class="material-icons">search</i></button>

             </div>
             :''
            }
              </li>
              {/* this code is commented to hide the jobs filter because it is static */}
              { this.props.open_filter_fn &&path[2]!=='jobs'
              ?
                <li class="filter-li"><FilterButton /> </li>
              :
                ''
              }
              {/* pass this props (selectedFeedStatus) to identify the status of filter */}
               { this.props.open_filter_my_post && path[2]=="my"
              ?
                <li class="filter-li"><FilterButton selectedFeedStatus={this.props.selectedFeedStatus}/> </li>
              :
                ''
            }
          {this.props.trending_btn_id_rss
            ?
              <li className="trending-btn-li"><a  className="trending-button-collpase"  onClick={this.open_pop.bind(this, this.props.trending_btn_id_rss)} title="TRENDING"><i class="material-icons">&#xE0E5;</i></a></li>
            :
              ''
            }
           {this.props.trending_btn_id && this.props.pageView != 'calendar'
            ?
              <li className="trending-btn-li"><a  className="trending-button-collpase"  onClick={this.open_pop.bind(this, this.props.trending_btn_id)} title="TRENDING"><i class="material-icons add-new-icon">&#xE8E5;</i></a></li>
            :
              ''
            }
              {this.props.title == "event" ?
               <li> <a href="#" className="btn btn-default"> New Event </a></li> : "" }
             {/* <li> <a href="#" className="btn btn-default" onClick = {this.eventPopup.bind(this)}> New Event </a></li> : "" } */}
              { this.props.id == "analytics" ?

             <li>
                <a onClick={this.exportAnalyticsData.bind(this)}> <button>Export</button></a>
             </li>  : ''}
                {(this.props.location.pathname!==undefined)?(!this.props.location.pathname.includes("search"))?
                  (((this.props.add_new_button) && (this.props.title !== 'Campaigns')) || ((this.props.add_new_button) && (roleName === "super-admin" || roleName === "admin")  && (this.props.title == 'Campaigns'))) ?
                   <li id="add-new-button-li" className='button-dropdown btn1'>
                      <div id="add-new-popup-point" />

                      {this.props.title =='Campaigns' && (roleName !=='super-admin' && roleName !=='admin') ? '':

                      <a className="btn btn-default add-new-button" onClick={this.open_pop.bind(this, 'add_new_button')}>

                        <span className='add-new-text'>{str}</span>
                        <span class="add-new-icon"><i class="material-icons add-new-icon">add circle</i></span>
                      </a>}


                      {this.props.popup_text ?
                        <div id="add-new-popup" className='add-new-popup'>
                          <div class="hopscotch-bubble" >

                            <div class="hopscotch-bubble-container">
                              <div class="hopscotch-bubble-content">
                                <h3 class="hopscotch-title">{this.props.popup_text}</h3>

                              </div>

                              <button onClick={this.closefeedpopup.bind(this)}>
                                <i class="material-icons">clear</i>
                              </button>

                              {/* this is the feed and in camping */}
                            </div>
                            <div id="triangle-up"> </div>

                          </div>
                        </div>
                        : ''
                      }

                    </li>
                    : '':"":""
                }
                {(this.props.location.pathname!==undefined)?(!this.props.location.pathname.includes("search"))?
                  this.props.currPage == "asset"
                  ?
                 // (this.props.location.pathname == `/assets/cat/${this.props.header_currCatID}` || this.props.location.pathname == "/assets/" ||  this.props.location.pathname == "/assets")?

                  //typeof this.props.location !== 'undefined' && this.props.location.pathname == `/assets/cat/${this.props.header_currCatID}`?

                    (this.props.breadCrumLength >=0 )?
                    <li className='button-dropdown btn2' id="add-new-button-li">
                      <a className="dropdown-toggle btn btn-default add-new-button">
                        <span className='add-new-text'>New</span>
                        <span class="add-new-icon"><i class="material-icons add-new-icon">add circle</i></span>
                      </a>
                      {
                        this.props.popup_text ?
                          <div id="add-new-popup" className='add-new-popup'>
                            <div class="hopscotch-bubble" >

                              <div class="hopscotch-bubble-container">
                                <div class="hopscotch-bubble-content">
                                  <h3 class="hopscotch-title">{this.props.popup_text}</h3>

                                </div>
                                <button className='noPostNotiCloseBtn' onClick={this.closefeedpopup.bind(this)}>
                                  <i class="material-icons">clear</i>
                                </button>
                              </div>
                              <div id="triangle-up"> </div>

                            </div>
                          </div>
                          : ''
                      }
                      <ul className='dropdown-menu hide'>
                        <li>
                          <a
                            className='btn-create-asset'
                            onClick={this.handleAssetsCreation.bind(this, this.props.header_location)}
                          >
                            Asset
                          </a>
                        </li>
                        {(!this.props.header_notShow && this.props.path!=="/assets/my" && categoryname!=='campaign')?
                          <li>
                            <a
                              className='btn-create-folder'
                              onClick={this.handleAssetFolderCreation.bind(this)}
                            >
                              Folder
                          </a>
                          </li>
                          : ""}
                        {(!this.props.header_notShow && categoryname!=='campaign' && this.props.path!=="/assets/my" && this.props.path.indexOf("/assets/custom")==-1)?
                          <li>
                            <a
                              className='btn-create-category'
                              onClick={this.handleAssetCategoryCreation.bind(this)}
                            >
                              Category
                          </a>
                          </li>
                          : ""
                        }
                      </ul>
                    </li>
                    : <li className='button-dropdown btn3' id="add-new-button-li">
                      <a className="dropdown-toggle btn btn-default add-new-button">
                        <span className='add-new-text'>New</span>
                        <span class="add-new-icon"><i class="material-icons add-new-icon">add circle</i></span>
                      </a>
                      {
                        this.props.popup_text ?
                          <div id="add-new-popup" className='add-new-popup'>
                            <div class="hopscotch-bubble" >

                              <div class="hopscotch-bubble-container">
                                <div class="hopscotch-bubble-content">
                                  <h3 class="hopscotch-title">{this.props.popup_text}</h3>

                                </div>
                                <button onClick={this.closefeedpopup.bind(this)}>
                                  <i class="material-icons">clear</i>
                                </button>
                              </div>
                              <div id="triangle-up"> </div>

                            </div>
                          </div>
                          : ''
                      }
                      <ul className='dropdown-menu hide'>
                        <li>
                          <a
                            className='btn-create-asset'
                            onClick={this.handleAssetsCreation.bind(this, this.props.header_location)}
                          >
                            Asset
                          </a>
                        </li>
                        {!this.props.header_notShow ?
                          ""
                          : ""}
                        {!this.props.header_notShow ?
                          ""
                          : ""
                        }
                      </ul>
                    </li>
                  :
                  '':"":""
                }
                {(this.props.location.pathname!==undefined)?(!this.props.location.pathname.includes("search"))?
                  this.props.add_new_subpopup == "Users" ?

                    <li className='button-dropdown btn4' id="add-new-button-li">
                      <a className="dropdown-toggle btn btn-default add-new-button">
                        <span className='add-new-text'>New</span>
                        <span class="add-new-icon"><i class="material-icons add-new-icon">add circle</i></span>
                      </a>
                      {
                        this.props.popup_text ?
                          <div id="add-new-popup" className='add-new-popup'>
                            <div class="hopscotch-bubble" >

                              <div class="hopscotch-bubble-container">
                                <div class="hopscotch-bubble-content">
                                  <h3 class="hopscotch-title">{this.props.popup_text}</h3>

                                </div>
                                <button onClick={this.closefeedpopup.bind(this)}>
                                  <i class="material-icons">clear</i>
                                </button>
                              </div>
                              <div id="triangle-up"> </div>

                            </div>
                          </div>
                          : ''
                      }
                      <ul className='dropdown-menu hide'>
                        <li>
                          <a onClick={this.open_pop.bind(this, 'add_new_button')}
                          >
                          Single User
                          </a>
                        </li>
                        <li>
                          <a onClick={this.open_pop.bind(this, 'upload_csv_btn')}>
                           Multiple Users
                          </a>
                        </li>

                      </ul>
                    </li>
                      :'':"":""
                    }

                </ul>

            {typeof this.props.selectedFolderIDArray !== "undefined" && this.props.selectedFolderIDArray.length > 0 && !this.props.notShow
              ? <ul id='assetActions' className='dd-menu context-menu'>
                <li className='button-dropdown btn5'>
                  <a className='dropdown-toggle' onClick={this.moreActionClick.bind(this)}>
                    <i className='material-icons'>more_vert</i>
                  </a>
                  <ul className='dropdown-menu hide'>
                    {this.props.selectedFolderIDArray.length == 1 ?
                    <li>
                      <a
                        onClick={this.editFolderClick.bind(this)}
                        className='btn-edit-folder'
                      >
                        Edit
                        </a>
                    </li>
                    : '' }
                    {(this.props.currPage == "asset") ?
                    <li class="dropdown-parent-li">
                      <ul>
                         <li>
                            <a
                              onClick={this.archiveFolderClick.bind(this)}
                              className='btn-edit-folder'>
                              Archive
                            </a>
                        </li>
                        <li className={`parent-li ${this.state.showCategoryPopup !== false && this.state.showCategoryPopup === "move" ? 'move-unarchive-next-arrow-li active' : 'move-unarchive-next-arrow-li'}`}>
                                <a
                                  onClick={this.unArchiveOpen.bind(this,'move folder')}
                                  className='btn-edit-folder'
                                >
                                  Move</a>
                                  {this.state.showCategoryPopup !== false && this.state.showCategoryPopup === "move folder" ?
                                     <AssetCategoryPopup
                                     customFeedAsset={this.props.header_customFeeds}
                                      category={this.props.header_cats}
                                      campaign={this.props.header_asset_campaigns}
                                      selectedAsset={this.props.selectedAssetID}
                                      selectedFolder={this.props.selectedFolderIDArray}
                                     // moveAssetClick={this.moveAssetClick.bind(this)}
                                      moveFolderClick={this.moveFolderClick.bind(this)}
                                      actionToPerform={this.state.showCategoryPopup}
                                      />
                                  : '' }
                        </li>
                      </ul>
                     </li>
                    :

                          <li>
                            <a
                              onClick={this.unArchiveOpen.bind(this,'unarchive')}
                              className='btn-edit-folder'
                            >
                              Unarchive
                              </a>
                              {this.state.showCategoryPopup !== false && this.state.showCategoryPopup === "unarchive" ?
                                 <AssetCategoryPopup
                                 customFeedAsset={this.props.header_customFeeds}
                                  category={this.props.header_cats}
                                  campaign={this.props.header_asset_campaigns}
                                  selectedAsset={this.props.selectedAssetID}
                                  selectedFolder={this.props.selectedFolderIDArray}
                                  unArchiveFolderClick={this.unArchiveFolderClick.bind(this)}
                                  actionToPerform={this.state.showCategoryPopup}
                                  />
                              : '' }
                          </li>


                    }

                  </ul>
                </li>
              </ul>
              : null}
              <div className = "assetAction-popup">
              {typeof this.props.selectedAssetID !== "undefined" && this.props.selectedAssetID.length > 0 && !this.props.notShow
              ? <ul id="assetActions" className='dd-menu context-menu'>
                <li className='button-dropdown btn6'>
                  <a className='dropdown-toggle' onClick={this.moreActionClick.bind(this)}>
                    <i className='material-icons'>more_vert</i>
                  </a>
                  <ul className='dropdown-menu hide'>
                    {(this.props.selectedAssetID.length == 1)?
                    <li>
                      <a
                        onClick={this.viewAssetClick.bind(this,this.props.selectedAssetID)}
                        className='btn-edit-folder'
                      >
                        View
                        </a>
                    </li>
                    : '' }
                    {(this.props.currPage == "asset") ?
                        <li class="dropdown-parent-li">
                           <ul>
                           {(this.props.selectedAssetID.length == 1 && type === 'image') ?
                           <li>
                           <a onClick = {this.openeditdroupdown.bind(this,this.props.selectedAssetID)}
                              className='btn-edit-folder'>
                              Edit
                            </a>
                           </li>:''}
                             {this.props.location.pathname  != "/assets/my"  ?
                              <li>
                                <a
                                  onClick={this.archiveAssetClick.bind(this)}
                                  className='btn-edit-folder'
                                >
                                  Archive
                                  </a>
                              </li>
                            :''}
                              {this.props.location.pathname  != "/assets/my"   ?
                              <li className={`parent-li ${this.state.showCategoryPopup !== false && this.state.showCategoryPopup === "move" ? 'move-unarchive-next-arrow-li active' : 'move-unarchive-next-arrow-li'}`}>
                                <a
                                  onClick={this.unArchiveOpen.bind(this,'move')}
                                  className='btn-edit-folder'
                                >
                                  Move
                                  <span class="move-unarchive-next-arrow"></span> </a>
                                  {/* <span class="move-unarchive-next-arrow"><i class="material-icons">keyboard_arrow_left</i></span> */}
                                  {this.state.showCategoryPopup !== false && this.state.showCategoryPopup === "move" ?
                                     <AssetCategoryPopup
                                      category={this.props.header_cats}
                                      customFeedAsset={this.props.header_customFeeds}
                                      campaign={this.props.header_asset_campaigns}
                                      selectedAsset={this.props.selectedAssetID}
                                      selectedFolder={this.props.selectedFolderIDArray}
                                      moveAssetClick={this.moveAssetClick.bind(this)}
                                      moveFolderClick={this.moveFolderClick.bind(this)}
                                      actionToPerform={this.state.showCategoryPopup}
                                      />
                                  : '' }
                              </li>
                              : ' '}
                          </ul>
                        </li>
                     :
                    <li className={this.state.showCategoryPopup !== false && this.state.showCategoryPopup === "unarchive" ? "move-unarchive-next-arrow-li active" : "move-unarchive-next-arrow-li" } >
                      <a
                        onClick={this.unArchiveOpen.bind(this,'unarchive')}
                        className='btn-edit-folder'
                      >
                        Unarchive <span class="move-unarchive-next-arrow"></span>
                        </a>
                        {this.state.showCategoryPopup !== false && this.state.showCategoryPopup === "unarchive" ?
                           <AssetCategoryPopup
                           customFeedAsset={this.props.header_customFeeds}
                            category={this.props.header_cats}
                            campaign={this.props.header_asset_campaigns}
                            selectedAsset={this.props.selectedAssetID}
                            selectedFolder={this.props.selectedFolderIDArray}
                            unArchiveAssetClick={this.unArchiveAssetClick.bind(this)}
                            actionToPerform={this.state.showCategoryPopup}
                            />
                        : '' }
                    </li>
                    }
                </ul>
                </li>

              </ul>
              : null}
              </div>
              {/* <a className="gsHandle" href="javascript:void(0);" onClick={this.onGlobalClick.bind(this,true)} ><i class="material-icons">search</i></a> */}

              </div>
                </div>
 : ''}


              </div>
         </header>

                <div id="navdraw" className="has-sib">
                  <SidebarNarrow location={this.props.location}></SidebarNarrow>
                  {path !== undefined && path[1] !== undefined && path[1] !=="analytics" && path[1] !=="chat" &&this.props.campaignDetailPage!==true?

                <div className="nd-sib">
                 <div className = "inner-nd-sib">
                    {/*nav header*/}
                    {New_Nav?
                    <New_Nav
                    //  nav_location={this.props.header_location}
                      profile={this.props.profile.profile}
                      createFeedOpen = {this.createFeedOpen.bind(this)}
                      currListingTypeStatus={this.props.header_currListingTypeStatus}
                      header_function = {this.props.header_function}
                      customFeed = {this.props.customFeedList}
                      asset_customFeed={this.props.assets.customfeedAsset}
                      cats={this.props.header_cats}
                      currCatID={this.props.header_currCatID}
                      listingType={this.props.header_listingType}
                      asset_campaigns={this.props.header_asset_campaigns}
                      closeFeed = {this.closeFeed.bind(this)}
                      currFeed= { this.props.currFeed }
                      moderatTest = {this.modHeader.bind(this)}
                      currFeedPosts = { this.props.currFeedPosts }
                      hash={this.props.hash}
                      location={this.props.location}
                      postSelectedData={this.props.postSelectedData}
                      hastag_name={this.props.hastag_name}
                      archiveCat={this.props.archiveCat}
                      currPage={this.props.currPage}
                      assetLinkClick={this.assetLinkClick.bind(this)}
                      location_cat={this.props.location_cat}
                      updateFeedAction={this.updateFeedAction.bind(this)}
                      notificationData={this.props.notificationData.postSelectedData}
                      />
                    :''
                    }
                  </div>
                </div>
                  :''}
                  {this.props.campaignDetailPage !== true ?
                  <div className = "collapsible-button-container">
                     <a className = "collapsible-button" onClick = {this.collapsibleNavigation.bind(this)}>
                        <i class="material-icons collapsible-icon menu-icon">menu</i>
                        <i class="material-icons collapsible-icon left-arrow"> keyboard_arrow_left</i>
                     </a>
                  </div>
                  :''}
                </div>
            </div>
    )
  }
  open_pop(selector) {
    document.getElementById(selector).click();
  }
  open_menu()
  {
    var menu_class;
    this.setState({
      menu_flag:!this.state.menu_flag
    });
    if (this.state.menu_flag )
		{
			document.getElementById('navdraw').className += " in-view"
			document.getElementById('inner_main_menu_handle').className += ' active'
		}
		else
		{
			document.getElementById('navdraw').classList.remove("in-view");
			document.getElementById('inner_main_menu_handle').classList.remove("active");
		}
  }
}
function mapStateToProps(state) {
  return {
    smsValue: state.smsValue,
    auth:state.auth,
    searchInput:state.searchData.searchInput,
    searchData: state.searchData,
    suggestions: state.searchData.searchData,
    profile:state.profile,
    assets:state.assets,
    customFeedList : state.feed.feedList,
    notificationData: state.notification_data,
  }
}

function mapDispatchToProps(dispatch) {
  return {
    notificationActions: bindActionCreators(notificationActions, dispatch),
    searchActions : bindActionCreators (searchActions,dispatch),
    feedActions : bindActionCreators (feedActions,dispatch),
    postsActions: bindActionCreators(postsActions, dispatch),
    smsActions: bindActionCreators(smsActions, dispatch)

  }
}

let connection = connect(mapStateToProps, mapDispatchToProps);
var reduxFormConnection = connection(Header);
export default reduxForm({
  form: 'Header' // a unique identifier for this form
})(reduxFormConnection)

