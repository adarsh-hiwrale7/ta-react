import { browserHistory } from 'react-router';
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'



import * as departmentActions from "../../../actions/departmentActions"
import * as utils from '../../../utils/utils'

import DepartmentSettingsForm from './DepartmentSettingsForm';
import Globals from '../../../Globals';
import notie from "notie/dist/notie.js"
import PreLoader from '../../PreLoader'
import PopupWrapper from '../../PopupWrapper'

class UpdateDept extends React.Component {

  constructor(props) {
    super(props);

  }

  componentDidMount() {

  }
  handleDeptSubmit(values) {
    if(this.props.searchInput==undefined)
    {
      this.props.departmentActions.updateDepartment(values);
    }
    else{
      this.props.departmentActions.updateDepartment(values,this.props.searchInput);
    }
  }

  render() {

    const {loading} = this.props.depts;
    var me = this;

    return (
      <section className="update-dept-page" >

        { (loading ? <PreLoader  className = "Page"/> : <div></div>) }

          <div id="dept-updation">
                  <header className="heading">
                    <h3>Department Update</h3>
                  </header>
                  <PopupWrapper>
                  <DepartmentSettingsForm onSubmit={this.handleDeptSubmit.bind(this)} departments={this.props.departments} currDept={this.props.currDept}></DepartmentSettingsForm>
                  </PopupWrapper>

            </div>
        </section>

    );
  }

}

function mapStateToProps(state) {
  return {
    depts: state.departments
  }
}

function mapDispatchToProps(dispatch) {
  return {
    departmentActions: bindActionCreators(departmentActions, dispatch)
  }
}

let connection = connect(mapStateToProps, mapDispatchToProps);

module.exports = connection(UpdateDept);
