import { browserHistory } from 'react-router';
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'


import * as departmentActions from "../../../actions/departmentActions"
import * as utils from '../../../utils/utils'

import DepartmentSettingsForm from './DepartmentSettingsForm';
import Globals from '../../../Globals';
import notie from "notie/dist/notie.js"
import PreLoader from '../../PreLoader'
import PopupWrapper from '../../PopupWrapper'

class CreateDept extends React.Component {

  constructor(props) {
    super(props);

  }

  handleDeptSubmit(values) {
    if(values.name==' '){
      //if we enter only space and try to send data
      notie.alert('error', 'You can not set empty name.', 3);
    }else{
      this.props.departmentActions.saveDepartment(values);
    }
  }
  render() {

    const {loading} = this.props.depts;

    var me = this;

    return (
      <section className="dept-creation-page" >

        { (loading ? <PreLoader  className = "Page"/> : <div></div>) }

          <div id="dept-creation">


                  <header className="heading">
                    <h3>New Department</h3>
                  </header>
                  <PopupWrapper>
                  <DepartmentSettingsForm onSubmit={this.handleDeptSubmit.bind(this)} departments={this.props.departments}></DepartmentSettingsForm>
                  </PopupWrapper>

          </div>

        </section>

    );
  }

}


function mapStateToProps(state) {
  return {
    depts: state.departments
  }
}

function mapDispatchToProps(dispatch) {
  return {
    departmentActions: bindActionCreators(departmentActions, dispatch)
  }
}

let connection = connect(mapStateToProps, mapDispatchToProps);

module.exports = connection(CreateDept);
