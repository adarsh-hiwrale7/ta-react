
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import {Field, reduxForm} from 'redux-form'
import {renderField} from '../../Helpers/ReduxForm/RenderField'

const required = value => value
  ? undefined
  : 'Required';

class DepartmentSettingsForm extends React.Component {
  constructor(props) {
    super(props)
  }

  componentDidMount() {
    this.handleInitialize()
  }

  handleInitialize(nextProps = null) {

    if(typeof(this.props.currDept)!=="undefined") {
      const initData = {
          "identity": this.props.currDept.identity,
          "name": this.props.currDept.name
      };

      this.props.initialize(initData);
    }

  }

  render() {
    const {handleSubmit, departments, pristine, reset, submitting} = this.props
    return (
      <form onSubmit={handleSubmit} role="form">
        <div className="section">
            <Field placeholder="Department Name" component={renderField} type="text" name="name" validate={[required]}/>
            <Field component={renderField} type="hidden" name="identity" />
            <div className="clearfix">
                <button className="btn right btn-primary" type="submit" disabled={pristine || submitting}>
                  {(this.props.currDept) ? 'Update' : 'Add'}
                </button>
            </div>
        </div>
      </form>
    )
  }
}

function mapStateToProps(state, ownProps) {
  return {}
}

function mapDispatchToProps(dispatch) {
  return {}
}
let connection = connect(mapStateToProps, mapDispatchToProps);

var reduxConnectedComponent = connection(DepartmentSettingsForm);

export default reduxForm({
  form: 'departmentSettingsForm', // a unique identifier for this form

})(reduxConnectedComponent)
