import $ from '../../Chunks/ChunkJquery';
import ChunkJqueryDropdown from '../../Chunks/ChunkJqueryDropdown';

import TableSearchWrapper from '../../TableSearchWrapper';
import UserDetailPopup from '../../General/userDetailPopup';
import reactable from '../../Chunks/ChunkReactable'
var search
const titles = ['Dr.', 'Mr.', 'Mrs.', 'Miss', 'Ms.']

export default class DepartmentsList extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      departments: [],
      search: [],
      Selectors: null,
      perPage: 10,
      main_state:[],
      filter_state:[],
      reactable:null
    }
  }

  componentWillReceiveProps (nextProps) {
    let departments = []
    if (nextProps.departments.length) {
      this.setState({
        departments: nextProps.departments
      })
      this.setState({main_state:nextProps.departments})
      this.setState({filter_state:nextProps.departments})
    }
  }
 componentWillMount () {
    this.setState({
      main_state:this.props.departments
    })
    reactable().then(reactable=>{
      this.setState({reactable:reactable})
    })
    $().then(jquery => {
      var $ = jquery.$
      this.setState({
        jQuery: $
      })
      ChunkJqueryDropdown().then(jqueryDropdown => {})
    })
  }
  handleClick (event) {
    this.setState({
      currentPage: Number(event.target.id)
    })
  }
  handlePageClick (data) {
    var me = this
    let selected = data.selected
    let offset = Math.ceil(selected * this.state.perPage)

    this.setState({ offset: offset }, () => {
      this.loadCampaignDataOnPage(selected, offset)
    })
  }
  search_text(e)
  {
    if(e.target.value!=="" && this.state.filter_state!==undefined)
    {
      var i=0;
      search=e.target.value.toLowerCase();
      var tempobj=this.state.filter_state.filter(function(temp){
        i=i+1;
        if(temp.name.toLowerCase().includes(search) || i==search || temp.total_user==search){
          return true;
        }
      })
      this.setState({main_state:tempobj})
    }
    else{
      this.setState({main_state:this.state.filter_state})
    }
  }

  render () {
    var departments = this.props.departments
    var me = this
    var Moment = this.props.Moment
    var totuser=0
    return (
      // filterable={this.state.search}
      (
        <div className='table-white-container'>
        <div className='tableControllers clearfix'>
        {this.props.location.pathname !== "/search/department"?
        <TableSearchWrapper>
        <input type="text" placeholder='Search in department' onChange={this.search_text.bind(this)}></input>
        </TableSearchWrapper>:""}
      </div>
      <div className="widget">
        <div className='table-wrapper'>
        {this.state.reactable?
        <this.state.reactable.Table
          className='table department-list-table responsive-table'
          id='department-table'
          itemsPerPage={10}
          pageButtonLimit={5}
        >
          <this.state.reactable.Thead>
            <this.state.reactable.Th column='id' className='a-center department-id-col'>ID</this.state.reactable.Th>
            <this.state.reactable.Th column='department_name'>Department</this.state.reactable.Th>
            <this.state.reactable.Th className='a-center' column='no_of_users'>Users</this.state.reactable.Th>
            <this.state.reactable.Th column='actions' className='actiondiv a-center'>Actions</this.state.reactable.Th>
          </this.state.reactable.Thead>
          {this.state.main_state!==undefined ?
          this.state.main_state.map((department, i) => {
            totuser=department.total_user
            return (
              <this.state.reactable.Tr key={i}>
                <this.state.reactable.Td className='fd a-center department-id-col' column='id' data-rwd-label='ID'>
                  {i + 1}
                </this.state.reactable.Td>
                <this.state.reactable.Td className='fd' column='department_name' data-rwd-label='Department'>
                  {department.name}
                </this.state.reactable.Td>
                <this.state.reactable.Td className='a-center' column='no_of_users' data-rwd-label='Users'>
                {totuser==undefined?'0':totuser}
              </this.state.reactable.Td>
                <this.state.reactable.Td
                  className='fd department-action actiondiv a-center'
                  column='actions'
                  value={department.title}
                >
          
                  <div className='action-button'>
                    <ul
                      className='dd-menu context-menu user_3dots'
                      onClick={(e) => {this.props.dropdownPopup(e)
                      }}
                    >
                      <li className='button-dropdown'>
                        <a className='dropdown-toggle user_drop_toggle'>
                          <i className='material-icons'>more_vert</i>
                        </a>
                        <ul className='dropdown-menu user_dropAction department-dropdown'>
                          <li>
                            <span
                              onClick={this.props.deptEditClick.bind(this, department.identity)}
                            >
                             Edit
                            </span>
                          </li>
                          <li>
                            <span
                              onClick={this.props.deptDeleteClick.bind(this, department.identity)}
                            >
                            Delete
                              {' '}
                            </span>
                          </li>
                        </ul>
                      </li>
                    </ul>
                  </div>
                </this.state.reactable.Td>

              </this.state.reactable.Tr>
            )
          }):""}
        </this.state.reactable.Table> :''}
        </div>
        </div>
        </div>
      )
    )
  }
}