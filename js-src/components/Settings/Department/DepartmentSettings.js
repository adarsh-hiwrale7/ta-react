import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import PreLoader from '../../PreLoader'
import * as settingsActions from "../../../actions/settingsActions"
import ReactCSSTransitionGroup from 'react/lib/ReactCSSTransitionGroup';
import notie from "notie/dist/notie.js"

import CreateDept from "./CreateDept";
import UpdateDept from "./UpdateDept";
import Header from '../../Header/Header';
import Globals from '../../../Globals';
import SettingsNav from '../SettingsNav';
import SearchNav from '../../Search/SearchNav';
import * as loginActions from "../../../actions/loginActions"
import * as departmentActions from "../../../actions/departmentActions"
import DepartmentsList from "./DepartmentsList";
let navBar
let title
var restrictAPI=false;
class DepartmentSettings extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      departments: [],
      currDept: null,
      updateDeptPopup: false,
      createDeptPopup: false,
      members: [],
      show_menu: true
    }
  }

  componentDidMount() {
    if(restrictAPI!==true){
      if(this.props.searchInput!==undefined)
      {
        this.fetchdepartment(this.props.searchInput);
      }
      else
      {
        this.fetchdepartment();
      }
      restrictAPI=true;
      setTimeout(()=>{
        restrictAPI=false
      },5000)
    }
  }

  fetchdepartment(search=null) {
    if(search==null)
    {
      this.props.departmentActions.fetchDepartments();
    }
    else{
      this.props.departmentActions.fetchDepartments(search);
    }

  }
  deptEditClick(dept_id) {
    var dept
    var departments = (typeof (this.props.departments.list) !== 'undefined')
      ? this.props.departments.list
      : [];
      departments.map((department,i) => {
        if(department.identity==dept_id)
        {
          dept = department
        }
      })
    this.setState({ updateDeptPopup: true, currDept: dept });

    document.body.classList.add("overlay")

  }

  deptDeleteClick(dept_id) {

    if(this.props.location.pathname == '/search/department'){
    var departments = this.props.departmentData
    }
    else{
    var departments = this.props.departments.list
    }
    var me = this;
    var dept
    departments.map((department,i) => {
      if(department.identity==dept_id)
      {
        dept = department
      }
    })
    notie.confirm(
      `Are you sure you want to delete ${dept.name} department?`,
      "Yes",
      "No",
      function () {
        if(me.props.searchInput==undefined){
          me.props.departmentActions.deleteDepartment(dept.identity);
        }
        else{
          me.props.departmentActions.deleteDepartment(dept.identity,me.props.searchInput);
        }
      },
      function () {

      }
    )

  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.departments.closeAllPopups) {
      document.body.classList.remove("overlay")
      this.setState({
        updateDeptPopup: false,
        createDeptPopup: false
      })
    }
  }

  handleDeptSubmit(values) {
    this.props.departmentActions.saveDepartment(values);
  }
  closePopup(popup) {
    switch (popup) {
      case "create":
        this.setState({
          createDeptPopup: false
        });
        break;
      case "update":
        this.setState({
          updateDeptPopup: false
        });
        break;
    }
    document.body.classList.remove("overlay");
  }
  handleDeptCreation() {
    this.setState({ createDeptPopup: true });
    //  this.props.departmentActions.creatingNewDept();
    document.body.classList.add("overlay")
  }
  renderCreateDeptPopup() {
    return (
      <ReactCSSTransitionGroup transitionName="slide" transitionAppear={true} transitionEnterTimeout={500} transitionLeaveTimeout={500} transitionAppearTimeout={1000}>
        <div className="side-popup-wrap">
          <div id="side-popup-wrapper" className="side-popup">
            <button id="close-popup" className="btn-default" onClick={this.closePopup.bind(this, 'create')}>
              <i className="material-icons">clear</i>
            </button>
            <CreateDept departments={this.state.departments.list}></CreateDept>
          </div>
        </div>
      </ReactCSSTransitionGroup>
    )
  }

  renderUpdateDeptPopup() {
    return (
      <ReactCSSTransitionGroup transitionName="slide" transitionAppear={true} transitionEnterTimeout={500} transitionLeaveTimeout={500} transitionAppearTimeout={1000}>
        <div className="side-popup-wrap">
          <div id="side-popup-wrapper" className="side-popup">
            <button id="close-popup" className="btn-default" onClick={this.closePopup.bind(this, 'update')}>
              <i className="material-icons">clear</i>
            </button>
            <UpdateDept departments={this.state.departments.list} currDept={this.state.currDept} searchInput={this.props.searchInput}></UpdateDept>
          </div>
        </div>
      </ReactCSSTransitionGroup>

    )
  }
  dropdownPopup(e) {
    e.target.parentNode.parentNode.parentNode.style.zIndex = 11
  }

  memberListing() {
    var me = this;
    var departments = [];
    if(me.props.location.state==null || me.props.location.state.seeAllClick==true){
      departments = (typeof (me.props.departments.list) !== 'undefined')
      ? me.props.departments.list
      : [];
    }
    else{
      if(me.props.location.state.fromSuggestionClick==true){ 
        departments = (typeof (me.props.departmentData) !== 'undefined')
      ? me.props.departmentData
      : [];
      }
    }

    // if((this.props.location.pathname=="/search/department" && this.props.departmentData!==undefined && this.props.departments.list.length==0) || (this.props.suggestion_selected!==undefined && this.props.suggestion_selected!==null)){
    //   departments = (typeof (this.props.departmentData) !== 'undefined')
    //   ? this.props.departmentData
    //   : [];
    // }

    // else{
    // departments = (typeof (me.props.departments.list) !== 'undefined')
    //   ? me.props.departments.list
    //   : [];
    // }
    if (departments == undefined || departments.length == 0)
    {
      return (
        <div className='no-data-block'>No department found.</div>
      )
    }

    else
    {
      return (
        <div>
          <DepartmentsList departments={departments} deptEditClick={this.deptEditClick.bind(this)} deptDeleteClick={this.deptDeleteClick.bind(this)} dropdownPopup={this.dropdownPopup.bind(this)} location={this.props.location}
          ></DepartmentsList>
        </div>
      );
    }
  }
  memberListing1() {
    var me = this;
    var departments = [];
    departments = (typeof (me.props.departments.list) !== 'undefined')
      ? me.props.departments.list
      : [];
    //var departments = [];

    return (
      <table className="table">
        <thead>
          <tr>

            <th>#</th>
            <th>Name</th>
            <th></th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {departments.map((dept, index) => {
            return (
              <tr key={index}>

                <td>{index + 1}</td>
                <td>{dept.name}</td>
                <td>
                  <a onClick={function (ev) {
                    me.deptEditClick(dept);
                  }}>
                    <span className='fa fa-edit'></span>
                  </a>
                </td>
                <td>
                  <a onClick={function (ev) {
                    me.deptDeleteClick(dept);
                  }}>
                    <span className='material-icons'>clear</span>

                  </a>
                </td>
              </tr>
            )
          })
          }
        </tbody>
      </table>
    )

  }

  render() {

    if(this.props.location.pathname=="/search/department"){
      navBar=SearchNav
      title="Search"
    }
    else{
      navBar=SettingsNav
      title="Departments"
    }
    var loading = this.props.departments.loading
    return (
      <section id="department" className="settings department">
        {this.state.createDeptPopup
          ? this.renderCreateDeptPopup()
          : <div></div>}
        {this.state.updateDeptPopup
          ? this.renderUpdateDeptPopup()
          : <div></div>}
        {!this.props.location.pathname.includes("search")?
          <Header title={title} nav={navBar} add_new_button={true} location={this.props.location}
          searchInput={this.props.searchInput}
          /> 
        :''}

        <div className='main-container'>
        {loading? <PreLoader location="department-loader"/>:
          <div id="content">

            <div className="page campaign-container">

              <div className="full-container">
                <div className="membersListing">
                  <span id="add_new_button" onClick={this.handleDeptCreation.bind(this)} />
                  {this.memberListing()}
                </div>
              </div>
            </div>
          </div>
        }
        </div>


      </section>
    );
  }

}

function mapStateToProps(state) {
  return { onboarding: state.settings.onboarding, profile: state.profile.profile, auth: state.auth, departments: state.departments }
}

function mapDispatchToProps(dispatch) {
  return {
    settingsActions: bindActionCreators(settingsActions, dispatch),
    loginActions: bindActionCreators(loginActions, dispatch),
    departmentActions: bindActionCreators(departmentActions, dispatch)
  }
}

let connection = connect(mapStateToProps, mapDispatchToProps);

module.exports = connection(DepartmentSettings);
