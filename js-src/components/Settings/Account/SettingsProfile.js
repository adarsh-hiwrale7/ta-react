import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import PreLoader from '../../PreLoader'
import Globals from '../../../Globals';
import * as utils from '../../../utils/utils'
import * as settingsActions from '../../../actions/settingsActions'
import * as settingsActionsPhoto from '../../../actions/settingsActionsPhoto'
import * as profileSettings from '../../../actions/settings/getSettings'
import { Field, reduxForm, startSubmit } from 'redux-form'
// collapse css
import { Link } from 'react-router'
import ProfileForm from './ProfileForm'
import PhotoEditor from '../../Feed/PhotoEditor'
import SliderPopup from '../../SliderPopup'
import * as s3functions from '../../../utils/s3'
import ResetPasswordSettingsForm from './ResetPasswordSettingsForm'
import Header from '../../Header/Header';
import SettingsNav from '../SettingsNav'
import * as loginActions from '../../../actions/loginActions'
import * as departmentActions from '../../../actions/departmentActions'
import * as companyInfo from '../../../actions/settings/companyInfo'
import GuestUserRestrictionPopup from '../../GuestUserRestrictionPopup';
import notie from 'notie/dist/notie.js'
import reactable from '../../Chunks/ChunkReactable'
import PhotoEditorSDK from '../../Chunks/ChunkPhotoEditor'
import moment from '../../Chunks/ChunkMoment'
let profile;
var restrictAPI = false;
class SettingsProfile extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      departments: [],
      profile: [],
      userProfile: {},
      show_menu: true,
      close_rss: '',
      openEditor: false,
      flag: 0,
      userTag: [],
      editLocationId: '',
      reactable: null,
      photoEditor: null,
    }
  }
  componentWillMount() {
    window.scrollTo(0, 0);
    moment().then(moment => {
      PhotoEditorSDK().then(photoEditor => {
        reactable().then(reactable => {
          this.setState({ reactable: reactable, photoEditor: photoEditor, Moment: moment })
        })
      })
    })
  }
  componentWillUnmount() {
    window.scrollTo(0, 0);
  }
  componentDidMount() {
    let roleName = JSON.parse(localStorage.getItem('roles')) !== null ? JSON.parse(localStorage.getItem('roles'))[0].role_name : ''
    //rss link
    if (restrictAPI !== true) {
      this
        .props
        .settingsActions
        .getrsslink();
      this.props.profileSettings.GetAllSettings()
      this.props.departmentActions.fetchDepartments();
      restrictAPI = true;
      setTimeout(() => {
        restrictAPI = false
      }, 5000)
    }
    // get rss link

    // this.props.settingsActions.shareCredits();

    s3functions.loadAWSsdk()
  }
  removeProfilePic() {
    this.props.ProfileAvatar.fetchProfileAvatar()
  }

  openEditor() {
    document
      .body
      .classList
      .add('overlay')
    var img, imageName, type;
    if (this.state.flag == 1) {
      profile = this.state.userProfile
      img = profile[0].preview;
      imageName = profile[0].name;
      type = profile[0].type;
    }
    else {
      profile = this.props.profile.profile
      img = profile.avatar;
      imageName = profile.first_name + profile.last_name + '.jpeg';
      type = 'image/jpeg';
    }
    return (

      <SliderPopup wide className="editorPopup">
        <button
          id='close-popup'
          className='btn-default'
          onClick={this.closeEditor.bind(this)}
        >
          <i className='material-icons'>clear</i>
        </button>
        <div id="post-editor-wrapper">
          {this.state.photoEditor ?
            <PhotoEditor
              indexCount={0}
              photoEditor={this.state.photoEditor}
              imageData={img}
              imageName={imageName}
              mediaType={type}
              closeEditorPopup={this.closeEditor.bind(this)}
              updateEditedImage={this.updateEditedImage.bind(this)}
            /> : ''}
        </div>
      </SliderPopup>
    )

  }

  renderEditor(field, flag) {
    if (flag == 1) {
      this.setState({
        flag: flag,
        openEditor: true,
        userProfile: field
      })
    }
    else {
      this.setState({
        flag: flag,
        openEditor: true
      })
    }
  }

  closeEditor() {
    this.setState({ openEditor: false })
    document
      .body
      .classList
      .remove('overlay')
  }

  updateEditedImage(editedImage) {
    this.setState({ userProfile: editedImage }, () => {
      this.changeDropBoxValue();
    });
  }

  changeDropBoxValue() {
    const initData = {
      profile: this.state.userProfile,
    }
    //this.props.initialize(initData);
    if (this.state.userProfile.length > 0) {
      var avatarFlag = 1;
      this.props.settingsActions.settingsProfilePost({ files: this.state.userProfile, photoUpload: true }, "", avatarFlag);
    }
  }

  handleProfileSubmit(values) {
    var date = this.state.Moment.unix(values.birth_date).format('DD/MM/YYYY')
    var today_date = new Date();
    var birth_year = this.state.Moment(date, 'DD/MM/YYYY').format("YYYY")
    var today_year = today_date.getFullYear();
    var age = today_year - birth_year;
    if (age < 16) {
      notie.alert('warning', 'You cannot be below 16 years ', 3)
      return
    }
    this.props.settingsActions.settingsProfilePost(values, '')

  }
  handleResetPasswordSubmit(values) {
    // this.props.loginActions.savePassword(values, "callbackPath");
    this.props.loginActions.resetPassword(values)
  }
  handleTourStart() {
    browserHistory.push('/feed/external/live/start')
  }
  renderBreadcrumbs() {
    // Display additional items depending on which page we are
    return <h1 />
  }
  close_rss(rssdata) {
    var removearrayRSS = rssdata.url.split(/[\s,]+/)
    var data = {
      rss_url: removearrayRSS,
      is_delete: 1
    }
    this.props.settingsActions.removeRssurl(data)
  }

  dropdownPopup(e) {
    e.target.parentNode.parentNode.parentNode.style.zIndex = 11
  }

  render() {
    let profileloading = this.props.profile.profile.loading;
    let locationLoading = this.props.settings.loading;
    let profileImageLoading = this.props.profile.profile.profileImageLoading;
    let resetpasswordloading = this.props.auth.loading;

    let profile = typeof this.props.profile !== 'undefined'
      ? this.props.profile
      : []
     
    let roleName = JSON.parse(localStorage.getItem('roles')) !== null ? JSON.parse(localStorage.getItem('roles'))[0].role_name : ''
    return (
      <div>
        <GuestUserRestrictionPopup key={'SettingProfileGuestUser'}/>
        <section id='settings' className='settings'>
          {this.state.openEditor === true ? this.openEditor() : <div />}
          {locationLoading ? <PreLoader className="Page" key={'SettingProfileLoader'} /> : <div />}
          <Header title="Account" nav={SettingsNav} location={this.props.location} key={'SettingProfileHeader'}/>
          <div className='main-container'>
            <div id='content'>

              <div className='page settings-container'>

                <div className='full-container'>
                  <div id='setting_walkthrough_popup_pointer' />
                  <div className='row profile-form'>
                    <div className='col-one-of-two'>
                      <div className='widget' id='setting_walkthrough'>
                        <header class='heading'>
                          <h3>Profile</h3>

                        </header>

                        {profileloading || profileImageLoading ? <PreLoader className="Page" /> : <div />}
                        <ProfileForm
                          key={'ProfileForm'}
                          onSubmit={this.handleProfileSubmit.bind(this)}
                          departments={this.props.departments}
                          renderEditor={this.renderEditor.bind(this)}
                          location={this.props.location}
                          profile={profile}
                          Moment={this.state.Moment}
                        />
                      </div>
                    </div>
                    {/* reset password */}
                    <div className='col-one-of-two last reset-password-form'>
                      <div class="widget-box">
                        <div class="widget-box">
                          <div className='widget'>
                            <header class='heading'>
                              <h3>Change Password</h3>
                            </header>
                            <label><span> Email </span></label>
                            <div className="email-setting">
                              {this.props.profile.profile.email}
                            </div>
                            {resetpasswordloading ? <PreLoader className="Page" /> : <div />}
                            <ResetPasswordSettingsForm
                              key={'ResetPasswordSettingsForm'}
                              onSubmit={this.handleResetPasswordSubmit.bind(this)}
                              departments={this.state.departments}
                              profile={this.props.profile.profile.email}

                            >
                              {' '}
                            </ResetPasswordSettingsForm>
                          </div>
                        </div>
                      </div>
                      {/* end of reset password div---- */}

                      {/*Surveyoption starts here*/}
                      {profile.profile.board == 1 ?
                        <div class="widget-box">
                          <div class="widget-box">
                            <div className='widget'>
                              <header class='heading'>
                                <h3>Manager Survey</h3>
                              </header>
                              <button onClick= {this.props.settingsActions.performManagerSurvey} className="btn save-btn theme-btn"><span> Take survey </span></button>
                            </div>
                          </div>
                        </div> : ''
                      }
                      {/*Surveyoption ends here*/}

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    )
  }
  open_main_menu() {
    this.setState({
      show_menu: !this.state.show_menu
    });
    var active_class = this.state.show_menu ? "in-view has-sib" : " has-sib";
    var handle_active_class = this.state.show_menu ? 'active inner_main_menu_handle' : 'inner_main_menu_handle';
    document.getElementById('navdraw').className = active_class;
    document.getElementById('inner_main_menu_handle').className = handle_active_class;

  }
}
function mapStateToProps(state) {
  return {
    onboarding: state.settings.onboarding,
    profile: state.profile,
    companyInfo: state.settings.companyInfo,
    settings: state.settings,
    auth: state.auth,
    departments: state.departments,
    companyData: state.companyData
  }
}
function mapDispatchToProps(dispatch) {
  return {
    settingsActions: bindActionCreators(settingsActions, dispatch),
    profileSettings: bindActionCreators(profileSettings, dispatch),
    loginActions: bindActionCreators(loginActions, dispatch),
    departmentActions: bindActionCreators(departmentActions, dispatch),
    settingsActionsPhoto: bindActionCreators(settingsActionsPhoto, dispatch),
    companyActions: bindActionCreators(companyInfo, dispatch),
  }
}

let connection = connect(mapStateToProps, mapDispatchToProps)

module.exports = connection(SettingsProfile)
export default reduxForm({
  form: 'Analytics' // a unique identifier for this form
})(SettingsProfile)


function copyToClipboard(element) {
  var $temp = $("<input>");
  $("body").append($temp);
  $temp.val($(element).text()).select();
  document.execCommand("copy");
  $temp.remove();
}
