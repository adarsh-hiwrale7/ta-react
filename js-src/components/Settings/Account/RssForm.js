
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Field, reduxForm } from 'redux-form';
import {renderField} from '../../../Helpers/ReduxForm/RenderField'
const required = value => value
  ? undefined
  : 'Required';
//import LoginForm from './SettingsProfile';
export class RssForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
          
          }
    }
render(){
    const {
        handleSubmit,
        pristine
      } = this.props
        return (
                  <section>
                      <form role="form" onSubmit={handleSubmit}>
                        <div className="form-row Rss-setting">
                        <label>RSS</label>
                        <Field
                           placeholder='RSS channel'
                           component={renderField}
                           type='text'
                           name='rss'
                           className='rss-textbox'
                           validate= {required}
                         />
                          </div>
                              <div className="form-row rss-setting-button">
                                    <button type="submit" class="btn btn-primary">
                                    <span class="rss-field">+</span>Add RSS channel</button>
                            </div>

                     </form>
                  </section>
                  
        )
    }
}
function mapStateToProps (state, ownProps) {
    return {}
  }

  function mapDispatchToProps (dispatch) {
    return {
             
    }
  }
let connection = connect(mapStateToProps, mapDispatchToProps)

var reduxConnectedComponent = connection(RssForm)

export default reduxForm({
    form: 'RssForm',  // a unique identifier for this form
  })(RssForm)