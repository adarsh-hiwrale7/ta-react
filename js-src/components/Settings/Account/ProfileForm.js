import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import { Field, reduxForm } from 'redux-form'
import { renderField } from '../../Helpers/ReduxForm/RenderField'
import { renderDropzoneField } from '../../Helpers/ReduxForm/RenderField'
import ReactDropzone from '../../Chunks/ChunkReactDropzone';
import * as countryActions from "../../../actions/countryActions";
import * as departmentActions from "../../../actions/departmentActions";
import * as profileSettings from "../../../actions/settings/getSettings";
import * as settingsActions from "../../../actions/settingsActions";
import * as utils from '../../../utils/utils';
import * as companyInfo from '../../../actions/settings/companyInfo';

import Globals from '../../../Globals';
import ImageLoader from 'react-imageloader'
import notie from 'notie/dist/notie.js';
import PreLoaderMaterial from '../../PreLoaderMaterial';
import reactDatepickerChunk from '../../Chunks/ChunkDatePicker' // datepicker chunk
import ReactDatePicker from '../../Helpers/ReduxForm/ReactDatePicker' // react datepicker field
let flag =0;
const required = value => (value ? undefined : 'Required');
const name = value => (value && /^[a-zA-Z ]*$/g.test(value) ?undefined: 'Must be a string');
const mobileNumberValidation = value => (value !=undefined ? value.length > 15 ? 'Please enter valid mobile number' : '':'')

// const name = value => console.log(/[a-zA-Z ]/g.test(value));
class ProfileForm extends React.Component {
  constructor () {
    super()
    this.state = {
      hideImgWrap: false,
      statedisabled: true,
      citydisabled:true,
      userProfile:{},
      openEditor:false,
      ReactDatePicker:null,
      reactDropzone:null, 
      isProfileUpdate:true
    }
  }

  componentWillMount(){
      reactDatepickerChunk()
        .then(reactDatepicker => {
          ReactDropzone().then(dropzone => {
            this.setState({ ReactDatePicker: reactDatepicker, reactDropzone: dropzone })
          })
      })
  }
  componentDidMount () {
    // this.props.countryActions.fetchAllCountry();
    this.props.companyInfoAction.fetchRegion();
    this.props.settingsActions.getCountycode();
    if (this.props.profile.profile.identity != null) {
      var userData = this.props.profile.profile;
      if(userData.office!==undefined && userData.office!==''){
    if(userData.office.region_id!==undefined){
      this.props.countryActions.fetchAllCountry(userData.office.region_id);
    }
    if(userData.office.country_id!==undefined){
      this.props.companyInfoAction.showMasterData('office',userData.office.country_id,true);
    }
    if(userData.office.identity!==undefined){
      var id=userData.office.identity;
      this.props.companyInfoAction.showMasterData('department',id,true);
    }
  }
    if(userData.department!==undefined){
      var id= userData.department.identity;
      this.props.companyInfoAction.showMasterData('business',id,true);
    }
    
    if(userData.BusinessUnit!==undefined && userData.BusinessUnit!==undefined){
      var id= userData.BusinessUnit.identity;
      this.props.companyInfoAction.showMasterData('role',id,true);
    }
      this.handleInitialize(this.props.profile.profile)
    }
  }

  selectCountryCode(e){
    var id = e.target.value;
    this.setState({
     mobileNo_country_code: id,
    })
  }

  selectRegion(e){
    var id = e.target.value;
    this.props.countryActions.fetchAllCountry(id,true);
    this.props.companyInfoAction.resetMasterDataAddUser("office");
    this.props.change('country','')
  }
  selectCountry(e){
    var id = e.target.value;
    this.props.companyInfoAction.showMasterData('office',id,true);
    this.props.companyInfoAction.resetMasterDataAddUser("department");
    this.props.change('office','')
  }
  selectOfficeLocation(e){
    var id = e.target.value;
    this.props.companyInfoAction.showMasterData('department',id,true);
    this.props.companyInfoAction.resetMasterDataAddUser("business");
    this.props.change('department','')
  }
  selectDepartment(e){
    var id = e.target.value;
    this.props.companyInfoAction.showMasterData('business',id,true);
    this.props.companyInfoAction.resetMasterDataAddUser("role");
    this.props.change('business_unit','')
  }
  selectBusinessUnit(e){
    var id = e.target.value;
    this.props.companyInfoAction.showMasterData('role',id,true);
  }
  componentWillReceiveProps (nextProps) {
   if (
      JSON.stringify(nextProps.profile) != JSON.stringify(this.props.profile)
    ) {
       
      if(nextProps.profile.profile.profile_update != undefined)
      {
        this.setState({
          isProfileUpdate : nextProps.profile.profile.profile_update
        })
      }
      var userData = nextProps.profile.profile;
      if(userData.office!==undefined && userData.office!==''){
    if(userData.office.region_id!==undefined){
      this.props.countryActions.fetchAllCountry(userData.office.region_id,true);
    }
    if(userData.office.country_id!==undefined){
      this.props.companyInfoAction.showMasterData('office',userData.office.country_id,true);
    }
    if(userData.office.identity!==undefined){
      var id=userData.office.identity;
      this.props.companyInfoAction.showMasterData('department',id,true);
    }
  }
    if(userData.department!==undefined){
      var id= userData.department.identity;
      this.props.companyInfoAction.showMasterData('business',id,true);
    }
    if(userData.BusinessUnit!==undefined && userData.BusinessUnit!==undefined){
      var id= userData.BusinessUnit.identity;
      this.props.companyInfoAction.showMasterData('role',id,true);
    }
      this.handleInitialize(nextProps.profile.profile)
    }
  }

  handleInitialize (profile) {
    var newLocation = utils
          .removeTrailingSlash(this.props.location.pathname)
          .split('/')
    const initData = {
      first_name: profile.first_name,
      last_name: profile.last_name,
      identity: profile.identity,
      birth_date:profile.birth_date ==false ?null:profile.birth_date,
      gender:profile.gender,
      mobile_no:profile.mobile_no,
      mobile_country_code:profile.mobile_country_code,
    }

    if(profile.office!==undefined && profile.office!==''){
      if(profile.office.region_id!==undefined){
        initData["region"] = profile.office.region_id;
      }
      if(profile.office.identity!==undefined){
        initData["office"] = profile.office.identity;
      }
    }

    if(profile.country!==undefined && profile.country.data!==undefined){
      initData["country"]= profile.country.data.identity;
    }

    if(profile.department!==undefined){
      initData["department"] = profile.department.identity
    }
    if(profile.BusinessUnit!==undefined){
      initData["business_unit"] = profile.BusinessUnit.identity
    }
    if(profile.CompanyRole!==undefined){
      initData["company_role"] = profile.CompanyRole.identity
    }
    this.props.initialize(initData)
  }
 
  removeProfilePic () {
    var me = this

    let objToSend = {
      avatar: '',
    }

    fetch(
      Globals.API_ROOT_URL + `/user/profile`,
      {
        method: 'PUT',
        headers: utils.setApiHeaders('put'),
        body: JSON.stringify(objToSend)
      }
    )
      .then(response => {
        if (response.status != '200') {
          utils.handleSessionError(json);
          notie.alert(
            'error',
            'There was a problem removing your avatar. Please try later',
            3
          )
        }
            this.props.profileSettings.GetProfileSettings();
        return response.json()
      })
      .then(json => {
        if (json.hasOwnProperty('data')) {
          notie.alert('success', 'Your avatar removed', 3)

          me.setState({
            hideImgWrap: true
          })
        }
      })
      .catch(err => {
        throw err
      })
  }

  handleDropzoneSubmit(field) {
    flag = 1;
    //if other type of file is selected then field will be null

      if(field.length>0){
        this.props.renderEditor(field,flag);
       }
    }
  render () {
    const { handleSubmit, departments, pristine, submitting,submitphoto } = this.props
    var dropzoneStyle = {
      background: '#256eff',
      cursor: 'pointer',
      color: '#fff',
      fontSize: '14px',
      fontWeight: '700',
      height: '36px',
      width: 'auto',
      display: 'inline-block',
      lineHeight: '36px',
      marginBottom: '5px'
    }
    var roleName = this.props.profile.profile.role!==undefined?  this.props.profile.profile.role.data[0].role_name:'null';
    var regionList = this.props.regionData;
    const isUserSSo = this.props.auth !== undefined ?  this.props.auth.sso_onboarding : this.props.profile.profile.email !== undefined ? this.props.profile.profile.sso_onboarding :'';
    return (
      <div>

      <form onSubmit={handleSubmit} key="profileForm_reduxForm" name='ProfileForm' id='ProfileForm'>
        <div className='form-list'>

          <div className='form-row profile-image-row'>
            <label className='block'>Avatar</label>
            <div className='profile-image-wrapper'>
            <div className="img-wrap">
              {/* className={`img-wrap ${this.state.hideImgWrap || !this.props.profile.profile.avatar || this.props.profile.profile.avatar.includes('https://placeholdit.imgix.net') ? 'hidden' : ''}`} */}
              <a href="javascript:void(0);" className='btn-remove-photo' onClick={this.removeProfilePic.bind(this)}><i className='fa fa-remove'/></a>
              {isUserSSo !== ''  && isUserSSo == true?
              this.props.profile.profile.avatar=="https://s3-us-west-2.amazonaws.com/talent-advocate/default-avatar.png" ?'':<a href="javascript:void(0);" className='btn-edit-photo' onClick={this.props.renderEditor.bind(this)} ><i className="fa fa-pencil" /></a>
              :''}
              <img src={this.props.profile.profile.avatar} alt="image loading"/>

                <ImageLoader
                    src={this.props.profile.profile.avatar}
                    preloader={PreLoaderMaterial}
                    onLoad = {this.imageLoaded} >
                   <div className="image-load-failed">
                   <i class="material-icons no-img-found-icon">&#xE001;</i>
                   </div>
                 </ImageLoader>

            <Field
              label='Avatar'
              name='files'
              cssClass='btn'
              handleSubmit={(fileToUpload, e) => {
                this.handleDropzoneSubmit(fileToUpload,e)
              }}
              accept='.jpg, .png, .jpeg'
              title='Change avatar'
              Dropzone={this.state.reactDropzone}
              component={renderDropzoneField}
            />

            </div>
            </div>
          </div>
          <Field
            placeholder='identity'
            component={renderField}
            type='hidden'
            name='identity'
            validate={[required]}
          />
          <Field
            placeholder='First Name'
            label='First Name'
            component={renderField}
            type='text'
            name='first_name'
            disabled={!this.state.isProfileUpdate}
            validate={[name,required]}
          />
          <Field
            placeholder='Last Name'
            label='Last Name'
            component={renderField}
            type='text'
            name='last_name'
            disabled={!this.state.isProfileUpdate}
            validate={[name,required]}
          />
         <div className="radiobtnFeed clearfix">
              <span className="radiobtnFeed-label">Gender</span>
                <label className="customradio">Female
                    <Field
                      component='input'
                      type='radio'
                      name='gender'
                      id='female'
                      value='F'
                    />
                    <span className="checkmark"></span>
                  </label>
                  <label className="customradio">Male
                    <Field
                      component='input'
                      type='radio'
                      name='gender'
                      id='male'
                      value='M'
                    />
                    <span className="checkmark"></span>
                  </label>
                  <label className="customradio">Other
                    <Field
                      component='input'
                      type='radio'
                      name='gender'
                      id='other'
                      value='O'
                    />
                    <span className="checkmark"></span>
                  </label>
            </div>
            {this.state.ReactDatePicker && this.props.Moment ?
            <Field
                name={`birth_date`}
                component={props => {
                  return (
                    <ReactDatePicker
                    {...props}
                    label='Birthdate'
                    reactDatePicker={this.state.ReactDatePicker}
                    moment={this.props.Moment}
                    validate={[required]}
                    dateFormat="DD/MM/YYYY"
                    minDate={'01/01/1900'}
                      />
                  )
                }}
                />:''}
              <label for='mobile no'> Mobile No.</label> 
              <div className="select-wrapper form-row">
              <div className="countrycode-dropdown">
              <Field
                placeholder='Select Country Code'
                component={renderField}
                type='select'
                name='mobile_country_code'
                onChange={this.selectCountryCode.bind(this)}
               >
                <option value=''>Select Country Code</option>
                {this.props.settings.countrycode!= undefined ? Object.keys(this.props.settings.countrycode).length> 0?this.props.settings.countrycode.map((Countrycode,i) => (
                  <option value={Countrycode.idc} key={`profileForm_${Countrycode.idc}${i}_CountryCode`}>
                    {Countrycode.idc}
                  </option>
                )):"":""} 
              </Field>
              </div> 
              <div className="mobileNo-wrapper">
              <Field
                placeholder='Mobile Number'
                component={renderField}
                type='number'
                name='mobile_no'
                validate={[mobileNumberValidation]}
              />
              </div> 
              </div> 
            {roleName=="super-admin" || (isUserSSo !=='' && isUserSSo == false )?
          
            <div>
              <div className=" form-row">
                <label for='region'> Region</label> 
                <div className="select-wrapper">
                  <Field
                    placeholder='Select Region'
                    component={renderField}
                    type='select'
                    name='region'
                    validate={[required]}
                    onChange={this.selectRegion.bind(this)}
                  >
                    <option value=''>Select Region</option>
                    {regionList.length > 0?regionList.map(region => (
                      <option value={region.identity} key={`profileForm_Region`+region.identity}>
                        {region.name}
                      </option>
                    )):""}
                  </Field>
                </div>
            </div>
            <div className=" form-row">
                <label for='country'>Country </label> 
                <div className="select-wrapper">
                  <Field
                    placeholder='Select Country'
                    component={renderField}
                    type='select'
                    name='country'
                    validate={[required]}
                    onChange={this.selectCountry.bind(this)}
                  >
                    <option value=''>Select Country</option>
                    {Object.keys(this.props.allcountry.regioncountry).length> 0?this.props.allcountry.regioncountry.map(Country => (
                      <option value={Country.identity} key={`profileForm_Country`+Country.identity}>
                        {Country.title}
                      </option>
                    )):""}
                  </Field>
                </div>
            </div>
            <div className=" form-row">
                <label for='office'> Office Location </label> 
                <div className="select-wrapper">
                  <Field
                    placeholder='Select Office Location'
                    component={renderField}
                    type='select'
                    name='office'
                    validate={[required]}
                    onChange ={this.selectOfficeLocation.bind(this)}
                  >
                    <option value=''>Select Office Location</option>
                    {this.props.companyMasterData.office.length > 0?this.props.companyMasterData.office.map(Office => (
                      <option value={Office.identity} key={`profileForm_Office`+Office.identity}>
                        {Office.name}
                      </option>
                    )):""}
                  </Field>
                </div>
            </div>
                    
            <div className=" form-row">
                <label for='department'> Department </label> 
                <div className="select-wrapper">
                  <Field
                    placeholder='Select Department'
                    component={renderField}
                    type='select'
                    name='department'
                    validate={[required]}
                    onChange={this.selectDepartment.bind(this)}
                  >
                    <option value=''>Select Department</option>
                    {this.props.companyMasterData.department.length > 0?this.props.companyMasterData.department.map(dept => (
                      <option value={dept.identity} key={`profileForm_Dept`+dept.identity}>
                        {dept.name}
                      </option>
                    )):""}
                  </Field>
                </div>
            </div>

            <div className=" form-row">
                <label for='business_unit'> Business Unit </label> 
                <div className="select-wrapper">
                  <Field
                    placeholder='Select Business Unit'
                    component={renderField}
                    type='select'
                    name='business_unit'
                    validate={[required]}
                    onChange={this.selectBusinessUnit.bind(this)}
                  >
                    <option value=''>Select Business Unit</option>
                    {this.props.companyMasterData.business.length > 0?this.props.companyMasterData.business.map(business => (
                      <option value={business.identity} key={`profileForm_Business`+business.identity}>
                        {business.name}
                      </option>
                    )):""}
                  </Field>
                </div>
            </div>

            <div className=" form-row">
                <label for='company_role'>Company Role</label> 
                <div className="select-wrapper">
                  <Field
                    placeholder='Select Company Role'
                    component={renderField}
                    type='select'
                    name='company_role'
                    validate={[required]}
                  >
                    <option value=''>Select Company Role</option>
                    {this.props.companyMasterData.role.length > 0?this.props.companyMasterData.role.map(companyRole => (
                      <option value={companyRole.identity} key={`profileForm_Company`+companyRole.identity}>
                        {companyRole.name}
                      </option>
                    )):""}
                  </Field>
                </div>
            </div>
            </div>
            :
            <div>
                <div>
                  <label><span> Region </span></label>
                    <div className="email-setting">
                    {this.props.profile.profile.office?this.props.profile.profile.office.region:''}
                    </div>
                </div>
                <div>
                  <label><span> Country </span></label>
                    <div className="email-setting">
                      {this.props.profile.profile.office?this.props.profile.profile.office.country:''}
                    </div>
                </div>
                <div>
                  <label><span>Office Location </span></label>
                    <div className="email-setting">
                    {this.props.profile.profile.office?this.props.profile.profile.office.name:''}
                    </div>
                </div>
               <div>
                  <label><span> Department </span></label>
                    <div className="email-setting">
                      {this.props.profile.profile.department?this.props.profile.profile.department.name:''}
                    </div>
                </div>
                <div>
                  <label><span> Business Unit </span></label>
                    <div className="email-setting">
                      {this.props.profile.profile.BusinessUnit?this.props.profile.profile.BusinessUnit.title:''}
                    </div>
                </div>
                <div>
                  <label><span> Company Role </span></label>
                    <div className="email-setting">
                      {this.props.profile.profile.CompanyRole?this.props.profile.profile.CompanyRole.title:''}
                    </div>
                </div>
              
                
                </div>
              }
          <div className='form-row'>
            <button
              class='btn right'
              type='submit'
              disabled={pristine || submitting}
            >
              Save
            </button>
          </div>
        </div>

      </form>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
          auth:state.auth.userinfo,
          profile: state.profile,
          companyInfo: state.settings.companyInfo,
          settings: state.settings,
          departments: state.departments,
          allcountry:state.allcountry,
          regionData : state.settings.companyHierarchyData.regions,
          companyMasterData : state.settings.companyMasterData,
        }
}

function mapDispatchToProps(dispatch) {
  return {
    profileSettings: bindActionCreators(profileSettings, dispatch),
    settingsActions: bindActionCreators(settingsActions, dispatch),
    departmentActions: bindActionCreators(departmentActions, dispatch),
    countryActions:bindActionCreators(countryActions,dispatch),
    companyInfoAction : bindActionCreators(companyInfo,dispatch)
  }
}

let connection = connect(mapStateToProps, mapDispatchToProps);
var reduxFormConnection = connection(ProfileForm);
export default reduxForm({
  form: 'ProfileForm' // a unique identifier for this form
})(reduxFormConnection)
