import { connect } from 'react-redux'
import { Link } from 'react-router'
import { Field, reduxForm } from 'redux-form'
import PreLoader from '../../PreLoader'
import * as utils from '../../../utils/utils'
import { renderField } from '../../Helpers/ReduxForm/RenderField'
const required = value => (value ? undefined : 'Required');
class AddMoreFormOffice extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      numberOfField : 1,
      dynamicField: [],
      old: null,
      showHideClass: null
    }
  }
  componentWillReceiveProps(nextProps) {
    if(nextProps.data.companyDataSaveLoader !== this.props.data.companyDataSaveLoader && !nextProps.data.companyDataSaveLoader){
        this.handleInitialize(this.props.formFor,this.props.selectedId,this.props.data)    
        this.setState({dynamicField: ['field']})
    } 
    if(nextProps.data.office !== this.props.data.office){
        this.setState({dynamicField: ['field']})
        this.handleInitialize(this.props.formFor,this.props.selectedId,nextProps.data)
    }
   
    if(typeof nextProps.showAddMore !== "undefined" && nextProps.showAddMore.indexOf('office') > -1){
        let tempDynamicField = this.state.dynamicField
        if(tempDynamicField.indexOf('field') < 0){
          tempDynamicField.push('field')  
        }
        this.setState({dynamicField:tempDynamicField})
     
    }else{
        this.setState({dynamicField:[]}) 
    }
    
    
    this.setState({old:nextProps.formFor})
    if(this.props.formFor !== nextProps.formFor){
        this.handleInitialize(nextProps.formFor,this.props.selectedId,this.props.data)
    }
  }
  componentDidMount(){
      this.handleInitialize(this.props.formFor,this.props.selectedId,this.props.data)
  }
  handleInitialize (formFor,selectedId,data) {
        let initData = {
          data_for:formFor,
          parent_id:selectedId
        }
        //console.log(data.office,'data.office')
        if(Object.keys(data.office).length>0){
          let hiddenData = {}
          data.office.map((office, index)=>{
              hiddenData["save_data_"+index] = office.name
          })
          Object.assign(initData, hiddenData)
         // initData.concat(hiddenData)
        }
        
    
    this.props.initialize(initData)
  }
  handleAddMoreOffice(){
    let newFieldCounter = this.state.dynamicField
    newFieldCounter.push('field')
    let numberOfField = this.state.numberOfField + 1
    this.setState({ numberOfField: numberOfField, dynamicField: newFieldCounter })
      //this.setState({numberOfField:this.state.numberOfField+1,holdedValues:holdedValues.push(document.getElementById('add_data_'+parseInt(this.state.numberOfField-1)).value)})
    //this.handleInitialize(this.state.numberOfField+1)
  }
  handleRemoveMoreOffice(index){
    let j =0;
    this.state.dynamicField.map((dynaField,i)=>{
      if(i >= index){
        let putIndex = parseInt(index) + parseInt(j)
        let fetchIndex = parseInt(index) + parseInt(j) + 1
        if(i != parseInt(this.state.dynamicField.length) - 1){
          this.props.change('add_data_'+putIndex,document.getElementById('add_data_'+fetchIndex).value);
          j++  
        }else{
          this.props.change('add_data_'+i,null);
        }
      }
      //this.props.change('add_data_'+i,);  
    })
    let newFieldCounter = this.state.dynamicField
    newFieldCounter.pop('field')
    let numberOfField = this.state.numberOfField - 1;
    this.setState({ numberOfField: numberOfField, dynamicField: newFieldCounter })
   
    
  }

  renderMasterForm(fetchedProps){
       const { handleSubmit, pristine, submitting } = fetchedProps
       return (  

            <div className="addMoreFormWrapper">
                 <form onSubmit={handleSubmit} name='AddMoreFormOffice'>
                 <div className="moreDataFieldsWrapper">
                    {this
                    .state
                    .dynamicField
                    .map((field, index) => {
                          return (
                            <div className="mdafWrapper">
                              <div className="masterDataAddField" key={index}>
                                <Field component={renderField}
                                      type='text'
                                      name={'add_data_'+index}
                                      validate={[required]}
                                      />
                                      {/* <a  title="Remove data" id="add-more-data" class="removeMoreDataBtn"><i class="material-icons">remove</i></a> */}
                              </div>
                              <div class="addRemoveMasterData clearfix"> 
                                  {index+1==this.state.dynamicField.length?
                                  <a onClick={this.handleAddMoreOffice.bind(this)} title="Add new data" id="add-more-data" class="addDataBtn"><i class="material-icons">add</i></a>:''}
                                  {this.state.numberOfField!==1?
                                    <a onClick={this.handleRemoveMoreOffice.bind(this,index)} title="Add new data" id="remove-more-data" class="addDataBtn"><i class="material-icons">remove</i></a>:''}
                              </div> 
                            </div>
                          )
                      
                    })}

                  {/* <div class="addRemoveMasterData clearfix">
                    <a onClick={this.handleAddMoreOffice.bind(this)} title="Add new data" id="add-more-data" class="addDataBtn"><i class="material-icons">add</i></a>
                    
                  </div>  */}
                  </div>
                  <Field
                          component={renderField}
                          type='hidden'
                          name='data_for'
                          defaultValue={fetchedProps.formFor}
                  />
                  <Field
                          component={renderField}
                          type='hidden'
                          name='parent_id'
                          defaultValue={fetchedProps.selectedId}
                  />
                  <input type="hidden" value={fetchedProps.formFor} />
                   <button
                    class='btn right'
                    type='submit'
                    disabled={pristine || submitting}
                  >
                    Save
                  </button>

                 </form>
            </div>
    ) 
  }

  renderHierarchyForm(fetchedProps){
       const { handleSubmit, pristine, submitting } = fetchedProps
       let OpenClass = fetchedProps.showAddMore.indexOf('office') > -1 ? 'hierarchy show' : 'hierarchy';
       return (  

            <div className="addMoreFormWrapper">
                 <form onSubmit={handleSubmit} name='AddMoreFormOffice'>
                 <div className={OpenClass}>
                   <div className="moreDataFieldsWrapper">
                      {this
                      .state
                      .dynamicField
                      .map((field, index) => {
                            return (
                            <div className="mdafWrapper" key={index}>
                              <div className="masterDataAddField" key={index}>
                                <Field component={renderField}
                                      type='text'
                                      name={'add_data_'+index}
                                      validate={[required]}
                                      />
                              </div>
                              <div class="addRemoveMasterData clearfix"> 
                              {index+1==this.state.dynamicField.length?
                               <a onClick={this.handleAddMoreOffice.bind(this)} title="Add new data" id="add-more-data" class="addDataBtn"><i class="material-icons">add</i></a>:''}
                              {this.state.numberOfField!==1?
                                <a onClick={this.handleRemoveMoreOffice.bind(this,index)} title="Add new data" id="remove-more-data" class="addDataBtn"><i class="material-icons">remove</i></a>:''}
                            </div> 
                            </div>
                            )
                        
                      })}

                    </div>
                      <Field
                              component={renderField}
                              type='hidden'
                              name='data_for'
                              defaultValue={fetchedProps.formFor}
                      />
                      <Field
                              component={renderField}
                              type='hidden'
                              name='parent_id'
                              defaultValue={fetchedProps.selectedId}
                      />
                      { 
                        Object.keys(fetchedProps.data.office).length > 0 ? 
                            fetchedProps
                            .data.office
                            .map((field, index) => {
                                  return (
                                      <Field
                                            component={renderField}
                                            type='hidden'
                                            name={'save_data_'+index}
                                            validate={[required]}
                                            key={index}
                                            />
                                  )
                              
                            })
                        : ''    
                      }
                       <button
                        class='btn right'
                        type='submit'
                        disabled={pristine || submitting}
                      >
                        Add
                      </button>
                  </div>
                  <button
                    class='btn Left hierarchyNextButton hide'
                    type='submit'
                    id='officeNext'>
                    Finish
                  </button>
                 </form>
            </div>
    ) 
  }

  
  
  render () {
   return(
      <div>
          {
            typeof this.props.showAddMore !== 'undefined' ? 
              this.renderHierarchyForm(this.props)
          : 
              this.renderMasterForm(this.props)
          }
      </div>
  )
    

    }
  
}
function mapStateToProps (state) {
  return {
  }
}
function mapDispatchToProps (dispatch) {
  return {}
}
let connection = connect(mapStateToProps, mapDispatchToProps)
var reduxConnectedComponent = connection(AddMoreFormOffice)

export default reduxForm({
  form: 'AddMoreFormOffice' // a unique identifier for this form
})(reduxConnectedComponent)
