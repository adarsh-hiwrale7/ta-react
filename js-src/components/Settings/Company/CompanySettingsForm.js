import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import { Field, reduxForm } from 'redux-form'
import { renderField } from '../../Helpers/ReduxForm/RenderField'
import * as countryActions from "../../../actions/countryActions";
import * as companyInfoActions from "../../../actions/settings/companyInfo";
import * as utils from '../../../utils/utils';
import Globals from '../../../Globals';
import notie from 'notie/dist/notie.js';
import PreLoaderMaterial from '../../PreLoaderMaterial';
const required = value => (value ? undefined : 'Required');
const name = value => (value && /^[a-zA-Z ]*$/g.test(value) ?undefined: 'Must be a string');
const number = value => value && isNaN(Number(value)) ? 'Must be a number' : undefined
class CompanySettingsForm extends React.Component {
  constructor () {
    super()

    this.state = {
    
    }

  }
  componentWillMount(){
     
  }
 /* selectedcountry(e){
    var newLocation = utils
          .removeTrailingSlash(this.props.location.pathname)
          .split('/')
    var selectedCountryValue=e.target.value
    this.setState({
      statedisabled:false
    })
    this.props.countryActions.fetchCountryState(selectedCountryValue,newLocation);
    var id=''
    this.props.countryActions.fetchStateCity(id)
  }*/
  
  componentDidMount () {
    this.props.countryActions.fetchAllCountry();
    
  }
  componentWillReceiveProps (nextProps) {
    if(this.props.companySettingData !== nextProps.companySettingData && Object.keys(nextProps.companySettingData).length > 0){
        this.handleInitialize(nextProps.companySettingData)
    }
    if(nextProps.settings.companySettingUpdateFlag !== this.props.settings.companySettingUpdateFlag && nextProps.settings.companySettingUpdateFlag && Object.keys(nextProps.settings.companySettingData).length > 0){
       this.handleInitialize(nextProps.settings.companySettingData)
       this.props.companyInfoActions.updateCompanySettingFlagAction();
    }
  }

  handleInitialize (data) {
    let initData = {}
    let methodName = 'POST'
    data.map((settingData)=>{
      if(settingData.key == "company_trading_name"){
        initData.trading_name = settingData.values
        initData.trading_name_identity = settingData.identity
        if(settingData.values !== ''){
          methodName = 'PUT'
        }
      }
      if(settingData.key == "company_legal_name"){
        initData.legal_name = settingData.values
        initData.legal_name_identity = settingData.identity
      }
      if(settingData.key == "company_no"){
        initData.company_number = settingData.values
        initData.company_number_identity = settingData.identity
      }
      if(settingData.key == "company_head_office_add"){
        initData.head_office_address = settingData.values
        initData.head_office_address_identity = settingData.identity
      }
      if(settingData.key == "company_head_office_add2"){
        initData.head_office_address2 = settingData.values
        initData.head_office_address2_identity = settingData.identity
      }
      if(settingData.key == "company_head_office_add3"){
        initData.head_office_address3 = settingData.values
        initData.head_office_address3_identity = settingData.identity
      }
      if(settingData.key == "company_postcode"){
        initData.postcode = settingData.values
        initData.postcode_identity = settingData.identity
      }
      if(settingData.key == "company_country_id"){
        initData.country = settingData.values
        initData.country_identity = settingData.identity
      }
    })
    initData.operating_method = methodName
    this.props.initialize(initData)
  }
  

  
  render () {
    const { handleSubmit, pristine, submitting } = this.props
    return (
      <div>
      <form onSubmit={handleSubmit} name='CompanySettingsForm'>
        <div className='company-form-list'>
        <div className='col-one-of-two'>
          <Field
            placeholder='Trading Name'
            label='Trading Name'
            component={renderField}
            type='text'
            name='trading_name'
            validate={[ required, name ]}
          />
          <Field component={renderField} type='hidden' name='trading_name_identity'/>
          <Field
            placeholder='Legal Name'
            label='Legal Name'
            component={renderField}
            type='text'
            name='legal_name'
            validate={[required]}
          />
          <Field component={renderField} type='hidden' name='legal_name_identity'/>
          <Field
            placeholder='Company Number'
            label='Company Number'
            component={renderField}
            type='text'
            name='company_number'
            validate={[required]}
          />
          <Field component={renderField} type='hidden' name='company_number_identity'/>
          <Field
            placeholder='Head Office Address'
            label='Head Office Address'
            component={renderField}
            type='text'
            name='head_office_address'
            validate={[required]}
          />
          <Field component={renderField} type='hidden' name='head_office_address_identity'/>
          </div>
          <div className='col-one-of-two last company-setting-form'>
              <Field
                placeholder='Address 2'
                label='Address 2'
                component={renderField}
                type='text'
                name='head_office_address2'
                validate={[required]}
              />
              <Field component={renderField} type='hidden' name='head_office_address2_identity'/>
              <Field
                placeholder='Address 3'
                label='Address 3'
                component={renderField}
                type='text'
                name='head_office_address3'
                validate={[required]}
              />
              <Field component={renderField} type='hidden' name='head_office_address3_identity'/>
               <Field
                placeholder='Postcode'
                label='Postcode'
                component={renderField}
                type='text'
                name='postcode'
                validate={[required,number]}
              />
              <Field component={renderField} type='hidden' name='postcode_identity'/>
              <Field
                placeholder='Country'
                label='Country'
                component={renderField}
                type='select'
                name='country'
                validate={[required]}
               >
               <option value=''>Select Country</option>
                  {typeof this.props.allcountry.allcountry !== 'undefined' && Object.keys(this.props.allcountry.allcountry)!=='' ?this.props.allcountry.allcountry.map(country => (
                    <option value={country.title} key={country.identity}>
                      {country.title}
                    </option>
                  )):''}
              </Field>  
              <Field component={renderField} type='hidden' name='country_identity'/> 
              <Field component={renderField} type='hidden' name='operating_method'/>     
              <div className='form-row'>
                <button
                  class='btn right'
                  type='submit'
                  disabled={pristine || submitting}
                >
                  Save
                </button>
              </div>
        </div>
        </div>
      </form>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
           allcountry:state.allcountry,
           settings: state.settings
        }
}

function mapDispatchToProps(dispatch) {
  return {
    countryActions:bindActionCreators(countryActions,dispatch),
    companyInfoActions:bindActionCreators(companyInfoActions,dispatch)
  }
}

let connection = connect(mapStateToProps, mapDispatchToProps);
var reduxFormConnection = connection(CompanySettingsForm);
export default reduxForm({
  form: 'CompanySettingsForm' // a unique identifier for this form
})(reduxFormConnection)
