import { connect } from 'react-redux'
import { Link } from 'react-router'
import { Field, reduxForm } from 'redux-form'
import PreLoader from '../../PreLoader'
import * as utils from '../../../utils/utils'
import { renderField } from '../../Helpers/ReduxForm/RenderField'
const required = value => (value ? undefined : 'Required');
class AddMoreFormRole extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      numberOfField : 1,
      dynamicField: [],
      old: null,
      showHideClass: null
    }
  }
  componentWillReceiveProps(nextProps) {
    if(nextProps.data.companyDataSaveLoader !== this.props.data.companyDataSaveLoader && !nextProps.data.companyDataSaveLoader){
        this.handleInitialize(this.props.formFor,this.props.selectedId,this.props.roleData)    
        this.setState({dynamicField: ['field']})
    } 
    if(nextProps.roleData !== this.props.roleData){
        this.setState({dynamicField: ['field']})
        this.handleInitialize(this.props.formFor,this.props.selectedId,nextProps.roleData)
    }
   
    if(typeof nextProps.showAddMore !== "undefined"){
        if(nextProps.showAddMore.indexOf('role') > -1){
             let tempDynamicField = this.state.dynamicField
            if(tempDynamicField.indexOf('field') < 0){
              tempDynamicField.push('field')  
            }
            this.setState({dynamicField:tempDynamicField})   
        }else{
          this.setState({dynamicField:[]}) 
        }
    }else{
        if(this.state.dynamicField.length == 0){
          this.setState({dynamicField:['field']})    
        }
        
    }
   
    this.setState({old:nextProps.formFor})
    if(this.props.formFor !== nextProps.formFor){
        this.handleInitialize(nextProps.formFor,this.props.selectedId,this.props.roleData)
    }
  }
  componentDidMount(){
      this.handleInitialize(this.props.formFor,this.props.selectedId,this.props.roleData)
  }
  handleInitialize (formFor,selectedId,roleData) {
        let initData = {
          data_for:formFor,
          parent_id:selectedId
        }
        if(Object.keys(roleData).length>0){
          let hiddenData = {}
          roleData.map((role, index)=>{
              hiddenData["save_data_"+index] = role.name
          })
          Object.assign(initData, hiddenData)
         // initData.concat(hiddenData)
        }
        
    
    this.props.initialize(initData)
  }
  handleAddMoreRole(){
    let newFieldCounter = this.state.dynamicField
    newFieldCounter.push('field')
    let numberOfField = this.state.numberOfField + 1
    this.setState({ numberOfField: numberOfField, dynamicField: newFieldCounter })
      //this.setState({numberOfField:this.state.numberOfField+1,holdedValues:holdedValues.push(document.getElementById('add_data_'+parseInt(this.state.numberOfField-1)).value)})
    //this.handleInitialize(this.state.numberOfField+1)
  }
  handleRemoveMoreRole(index){
    let newFieldCounter = this.state.dynamicField
    newFieldCounter.pop('field')
    let numberOfField = this.state.numberOfField - 1;
    this.setState({ numberOfField: numberOfField, dynamicField: newFieldCounter })
    this.props.change('add_data_'+index,null);
  }

  renderMasterForm(fetchedProps){
       const { handleSubmit, pristine, submitting } = fetchedProps
       return (  

            <div className="addMoreFormWrapper">
                 <form onSubmit={handleSubmit} name='AddMoreFormRole'>
                 <div className="moreDataFieldsWrapper">
                    {this
                    .state
                    .dynamicField
                    .map((field, index) => {
                          return (
                            <div className="mdafWrapper">
                                <div className="masterDataAddField" key={index}>
                                  <Field 
                                        component={renderField}
                                        type='text'
                                        name={'add_data_'+index}
                                        validate={[required]}
                                        />
                                </div>
                                <div class="addRemoveMasterData clearfix"> 
                                {index+1==this.state.dynamicField.length?
                                <a onClick={this.handleAddMoreRole.bind(this)} title="Add new data" id="add-more-data" class="addDataBtn"><i class="material-icons">add</i></a>:''}
                                {this.state.numberOfField!==1?
                                  <a onClick={this.handleRemoveMoreRole.bind(this,index)} title="Add new data" id="remove-more-data" class="addDataBtn"><i class="material-icons">remove</i></a>:''}
                              </div> 
                          </div>
                          )
                      
                    })}

                  </div>
                  <Field
                          component={renderField}
                          type='hidden'
                          name='data_for'
                          defaultValue={fetchedProps.formFor}
                  />
                  <Field
                          component={renderField}
                          type='hidden'
                          name='parent_id'
                          defaultValue={fetchedProps.selectedId}
                  />
                   <button
                    class='btn right'
                    type='submit'
                    disabled={pristine || submitting}
                  >
                    Save
                  </button>

                 </form>
            </div>
    ) 
  }

  renderHierarchyForm(fetchedProps){
       const { handleSubmit, pristine, submitting } = fetchedProps
       let OpenClass = fetchedProps.showAddMore.indexOf('role') > -1 ? 'hierarchy show' : 'hierarchy';
       return (  

            <div className="addMoreFormWrapper">
                 <form onSubmit={handleSubmit} name='AddMoreFormRole'>
                 <div className={OpenClass}>
                   <div className="moreDataFieldsWrapper">
                      {this
                      .state
                      .dynamicField
                      .map((field, index) => {
                            return (
                              <div className="mdafWrapper">
                                <div className="masterDataAddField" key={index}>
                                  <Field placeholder={'Add '+fetchedProps.formFor}
                                        component={renderField}
                                        type='text'
                                        name={'add_data_'+index}
                                        validate={[required]}
                                        />
                                </div>
                                <div class="addRemoveMasterData clearfix"> 
                                  {index+1==this.state.dynamicField.length?
                                  <a onClick={this.handleAddMoreRole.bind(this)} title="Add new data" id="add-more-data" class="addDataBtn"><i class="material-icons">add</i></a>:''}
                                  {this.state.numberOfField!==1?
                                    <a onClick={this.handleRemoveMoreRole.bind(this,index)} title="Add new data" id="remove-more-data" class="addDataBtn"><i class="material-icons">remove</i></a>:''}
                                </div> 
                              </div>
                            )
                        
                      })}

                    </div>
                      <Field
                              component={renderField}
                              type='hidden'
                              name='data_for'
                              defaultValue={fetchedProps.formFor}
                      />
                      <Field
                              component={renderField}
                              type='hidden'
                              name='parent_id'
                              defaultValue={fetchedProps.selectedId}
                      />
                      { 
                        Object.keys(fetchedProps.roleData).length > 0 ? 
                            fetchedProps
                            .roleData
                            .map((field, index) => {
                                  return (
                                      <Field
                                            component={renderField}
                                            type='hidden'
                                            name={'save_data_'+index}
                                            validate={[required]}
                                            key={index}
                                            />
                                  )
                              
                            })
                        : ''    
                      }
                       <button
                        class='btn right'
                        type='submit'
                        disabled={pristine || submitting}
                      >
                        Add
                      </button>
                  </div>
                  <button
                    class={`btn Left finishButton`}
                    type='submit'
                    id='roleFinish'
                  >
                    Finish
                  </button>
                 </form>
            </div>
    ) 
  }

  
  
  render () {
   return(
      <div>
          {
            typeof this.props.showAddMore !== 'undefined' ? 
              this.renderHierarchyForm(this.props)
          : 
              this.renderMasterForm(this.props)
          }
      </div>
  )
    

    }
  
}
function mapStateToProps (state) {
  return {
  }
}
function mapDispatchToProps (dispatch) {
  return {}
}
let connection = connect(mapStateToProps, mapDispatchToProps)
var reduxConnectedComponent = connection(AddMoreFormRole)

export default reduxForm({
  form: 'AddMoreFormRole' // a unique identifier for this form
})(reduxConnectedComponent)
