import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import PreLoader from '../../PreLoader'
import Globals from '../../../Globals';
import * as utils from '../../../utils/utils'
import * as settingsActions from '../../../actions/settingsActions'
import * as companyInfo from '../../../actions/settings/companyInfo'
import * as countryActions from "../../../actions/countryActions";
import {Field, reduxForm,startSubmit} from 'redux-form'
import reactable from '../../Chunks/ChunkReactable'
// collapse css
import { Link } from 'react-router'
import Header from '../../Header/Header';
import SettingsNav from '../SettingsNav'
import CompanySettingsForm from './CompanySettingsForm'
import MasterDataPopup from './MasterDataPopup'
import HierarchyPopup from './HierarchyPopup'
import LocationsList from './LocationsList'
import notie from 'notie/dist/notie.js'
import moment from '../../Chunks/ChunkMoment'
import GuestUserRestrictionPopup from '../../GuestUserRestrictionPopup';
class CompanyIndex extends React.Component {
  constructor (props) { 
    super(props)
    this.state = {
      masterDataPopup : false,
      hierarchyPopup: false,
      openSelectedRegion:0,
      openSelectedCountry:0,
      openSelectedOffice:0,
      pageno: null,
    }
  }
  componentWillMount(){
      
  }
  componentDidMount () {
    this.props.companyInfoAction.getCompanySettingInfo();
    this.props.companyInfoAction.getOfficeLocation('1');
  }

  handleCompanySettings(data){
    this.props.companyInfoAction.saveCompanySettings(data);
  }

  /**
  * Open master data popup for hierarchy
  * @author Yamin
  **/
  openMasterDataPopup(){
      this.setState({masterDataPopup:true})
      this.props.companyInfoAction.showMasterData('department');
      document.body.classList.add('master-hierarchy-popup-body');
  }
  openAddHierarchyPopup(me,resetFlags = false){
    document.body.classList.add('master-hierarchy-popup-body');
    
      if(!resetFlags){
        this.setState({hierarchyPopup:true, openSelectedRegion: 0, openSelectedCountry: 0, openSelectedOffice: 0})
      }else{
        this.setState({hierarchyPopup:true})  
      }
      
      this.props.companyInfoAction.fetchRegion();
      //this.props.companyInfoAction.fetchRegion();
  }

  closeMasterDataPopup(){
      this.setState({masterDataPopup:false});
      document.body.classList.remove('master-hierarchy-popup-body');
  }
  closeHierarchyDataPopup(){
    document.body.classList.remove('master-hierarchy-popup-body');
      this.setState({hierarchyPopup:false})
  }
  handleAddMoreData(data,isHierarchy = false){
    this.props.companyInfoAction.addMasterData(data, isHierarchy);

  }
  fetchNextSectionData(dataView, selectedValue, isHierarchy = false){
    this.props.companyInfoAction.showMasterData(dataView,selectedValue,isHierarchy,null,0) 
  }
  fetchCountryForRegion(regionId){
    this.props.countryActions.fetchAllCountry(regionId,true);
  }

  openOfficeHeirarchy(officeId,countryId,regionId){
    this.setState({openSelectedRegion: regionId, openSelectedCountry: countryId, openSelectedOffice: officeId})
    this.openAddHierarchyPopup(this,true)    

  }

  deleteEntityAction(entityId,section,parentId,isHierarchy = false){
      this.props.companyInfoAction.deleteEntity(entityId,section,parentId,isHierarchy)
  }

  deleteFlagUpdate(){
     this.props.companyInfoAction.deleteFlagUpdate()
  }

  updateHeirarchyProps(updatedProps,section){
     this.props.companyInfoAction.updateHeirarchyProps(updatedProps,section)
  }
  updateHeirarchyPropsInit(){
     this.props.companyInfoAction.updateHeirarchyPropsInit();
  }
  

  renderMasterDataPopup(){
      return (
        <MasterDataPopup
          masterData={this.props.settings.companyMasterData}
          closeMasterDataPopup={this.closeMasterDataPopup.bind(this)}
          handleAddMoreData={this.handleAddMoreData.bind(this)}
          fetchNextSectionData={this.fetchNextSectionData.bind(this)}
          deleteEntity={this.deleteEntityAction.bind(this)}
          deleteFlagUpdate={this.deleteFlagUpdate.bind(this)}
        />
      )
  }
  renderHierarchyPopup(){
      return (
        <HierarchyPopup
          masterData={this.props.settings}
          closeHierarchyDataPopup={this.closeHierarchyDataPopup.bind(this)}
          handleAddMoreData={this.handleAddMoreData.bind(this)}
          fetchNextSectionData={this.fetchNextSectionData.bind(this)}
          fetchCountryForRegion={this.fetchCountryForRegion.bind(this)}
          countryData={this.props.allcountry.regioncountry}
          openSelectedCountry={this.state.openSelectedCountry}
          openSelectedOffice={this.state.openSelectedOffice}
          openSelectedRegion={this.state.openSelectedRegion}
          deleteEntity={this.deleteEntityAction.bind(this)}
          deleteFlagUpdate={this.deleteFlagUpdate.bind(this)}
          updateHeirarchyProps={this.updateHeirarchyProps.bind(this)}
          updateHeirarchyPropsInit={this.updateHeirarchyPropsInit.bind(this)}

        />
      )
  }
  selectedPageno (pageno) {
    this.setState(
      {
        pageno: pageno
      },
      () => this.props.companyInfoAction.getOfficeLocation(this.state.pageno)
    )
  }
  render () {
    return (
      <div>
      <GuestUserRestrictionPopup/>
      <section id='settings' className='settings'>
        {this.state.masterDataPopup ? this.renderMasterDataPopup() : ''}
        {this.state.hierarchyPopup ? this.renderHierarchyPopup() : ''}
        {this.props.settings.companySettingLoader || this.props.settings.companyLocationLoader || this.props.settings.companyMasterData.companyDataSaveLoader || this.props.settings.companyMasterData.companyMasterDataLoader ? <PreLoader className='Page'/> : ''}
        <Header title="Company" nav={SettingsNav} location={this.props.location}/>
          <div className='main-container'>
        <div id='content'>

          <div className='page settings-container'>

            <div className='full-container'>
              <div id='setting_walkthrough_popup_pointer' />
              <div className='row profile-form'>
              <div className='widget' id='setting_walkthrough'>
                      <header class='heading'>
                        <h3>Company settings</h3>
                      </header>
                      <CompanySettingsForm
                        onSubmit={this.handleCompanySettings.bind(this)}
                        companySettingData={this.props.settings.companySettingData}
                        companySettingUpdateFlag={this.props.settings.companySettingUpdateFlag}
                      />
                    </div>
               
                {/* reset password */}
               
            </div>
            <div className="locatioListWrapper">
            <div className="locationNoteSection">
              <strong>Note:</strong> Please create master data before creating new location, this will help you to better manage your company hierarchy.
            </div>
            <div class="locationListButtonWrapper">
                      <div className="company-hierarchy">
                      <button class='btn' type='button' onClick={this.openAddHierarchyPopup.bind(this)}>
                            Create New
                      </button>
                      <button class='btn' type='button' onClick={this.openMasterDataPopup.bind(this)}>
                            Master Data
                      </button>

                    </div>
                      {/*<div className='widget'>
                      <header class='heading'>
                        <h3>Change Password</h3>
                      </header>
                      <label><span> Email </span></label>
                       <div className="email-setting">
                          {this.props.profile.profile.email}
                       </div>
                      {resetpasswordloading ? <PreLoader  className = "Page"/>: <div />}
                      <ResetPasswordSettingsForm
                        onSubmit={this.handleResetPasswordSubmit.bind(this)}
                        departments={this.state.departments}
                        profile = {this.props.profile.profile.email}

                      >
                        {' '}
                      </ResetPasswordSettingsForm>
                  </div>*/}
                  
                    

                  </div>
            <LocationsList
              offices={this.props.settings.officeLocations}
              openOfficeHeirarchy={this.openOfficeHeirarchy.bind(this)}
              pageNo={this.selectedPageno.bind(this)}
            />
          </div>
  </div>
        </div>
        </div>

        </div>
      </section>
    </div>
    )
  }
  
}
function mapStateToProps (state) {
  return {
    companyInfo: state.settings.companyInfo,
    settings: state.settings,
    allcountry:state.allcountry,
    
  }
}
function mapDispatchToProps (dispatch) {
  return {
    companyInfoAction:bindActionCreators(companyInfo,dispatch),
    countryActions:bindActionCreators(countryActions,dispatch)
  }
}

let connection = connect(mapStateToProps, mapDispatchToProps)

module.exports = connection(CompanyIndex)
export default reduxForm({
  form: 'CompanyIndex' // a unique identifier for this form
})(CompanyIndex)

