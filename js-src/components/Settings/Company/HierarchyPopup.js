import { connect } from 'react-redux'
import { Link } from 'react-router'
import { Field, reduxForm } from 'redux-form'
import PreLoader from '../../PreLoader'
import * as utils from '../../../utils/utils'
import CenterPopupWrapper from '../../CenterPopupWrapper';
import AddMoreFormDepartment from './AddMoreFormDepartment';
import AddMoreFormBusiness from './AddMoreFormBusiness';
import AddMoreFormRole from './AddMoreFormRole';
import AddMoreFormOffice from './AddMoreFormOffice';
import notie from "notie/dist/notie.js"
import { renderField } from '../../Helpers/ReduxForm/RenderField'
const required = value => (value ? undefined : 'Required');
var showAddMore=[]
var currentSection={department:null,business:null,role:null}
var tempnextView=null
var tempcurrentView = null
var tempsectionName = null
class HierarchyPopup extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      showSection: '',
      showAddMore: [],
      selectedValue: null,
      lastFormSubmitted: null,
      regionSelected: null,
      countrySelected: null,
      currentSection:[],
      selecteddepartment: null,
      selectedbusiness: null,
      selectedrole: null,
      selectedcountry: null,
      selectedoffice: null


    }
  }
  componentWillReceiveProps(nextProps) {
      if(nextProps.masterData.companyMasterData.companyDataSaveLoader !== this.props.masterData.companyMasterData.companyDataSaveLoader && !nextProps.masterData.companyMasterData.companyDataSaveLoader){
          this.setState({showAddMore:false})
          var index = showAddMore.indexOf(this.state.lastFormSubmitted);
          if (index > -1) {
            showAddMore.splice(index, 1);
          }
      }
      switch(tempcurrentView){
        case 'department':
            if(nextProps.masterData.companyMasterData.department!==this.props.masterData.companyMasterData.department&&Object.keys(nextProps.masterData.companyMasterData.department).length>0){
            this.fetchNextData(nextProps.masterData.companyMasterData.department);
            }
            break;
        case 'business' :
            if(nextProps.masterData.companyMasterData.business!==this.props.masterData.companyMasterData.business&&Object.keys(nextProps.masterData.companyMasterData.business).length>0){
              this.fetchNextData(nextProps.masterData.companyMasterData.business);
              }
            break;
      }
      var selectedSection =[]
      switch(this.state.lastFormSubmitted){

        case 'office':
            nextProps.masterData.companyMasterData.office.length>0 && this.state.dataSubmitted.add_data_0 ?
           selectedSection =  nextProps.masterData.companyMasterData.office.filter((office,i)=>{
             return office.name.toLowerCase() == this.state.dataSubmitted.add_data_0.toLowerCase()
           }):''
           selectedSection.length !==0?
           this.handleValueClick(selectedSection[0].identity,'department','office'):
           '';
           break;

        case 'department':
            nextProps.masterData.companyMasterData.department.length>0 && this.state.dataSubmitted.add_data_0?
           selectedSection =  nextProps.masterData.companyMasterData.department.filter((department,i)=>{
             return department.name.toLowerCase() == this.state.dataSubmitted.add_data_0.toLowerCase()
           }):''
           selectedSection.length !==0?
           this.handleValueClick(selectedSection[0].identity,'business','department'):
           '';
           break;
       case 'business':
          nextProps.masterData.companyMasterData.business.length>0 && this.state.dataSubmitted.add_data_0?
           selectedSection = nextProps.masterData.companyMasterData.business.filter((business,i)=>{
             return business.name.toLowerCase() == this.state.dataSubmitted.add_data_0.toLowerCase()
           }):'';
           selectedSection.length !==0?
           this.handleValueClick(selectedSection[0].identity,'role','business'):'';
           break;
          
          }

      if(nextProps.masterData.companyMasterData.deleteEntity !== this.props.masterData.companyMasterData.deleteEntity && nextProps.masterData.companyMasterData.deleteEntity !== null){
            this.setState({showSection: nextProps.masterData.companyMasterData.deleteEntity})
            this.props.deleteFlagUpdate();
      }
    
  }
  componentDidMount (){
      var me=this
      //OnClick of Outside it will close Popup
      window.addEventListener("click", function(event) {
        if (event.target.className.includes("popup-center-wrapper")) {
          me.props.closeHierarchyDataPopup();
        }
      });
      currentSection={department:null,business:null,role:null}
      showAddMore=[]
      if(this.props.openSelectedRegion !== 0){
        this.selectCountryFromRegion(this.props.openSelectedRegion,true)
      }
      if(this.props.openSelectedCountry !== 0){
        this.selectOfficeFromCountry(this.props.openSelectedCountry,true)
      }
      if(this.props.openSelectedOffice !== 0){
        this.handleValueClick(this.props.openSelectedOffice,'department','office')
      }
      //this.handleInitialize(this.props.openSelectedRegion)
  }

  fetchNextData(sectionData){
    if(tempnextView!==null && tempsectionName!==null && tempcurrentView!==null){
          
      sectionData.map((section,index)=>{
        if(section.name==tempsectionName){
          this.props.fetchNextSectionData(tempnextView,section.identity,true)
          switch(tempcurrentView){
            case "department":
                 currentSection['business'] = null
                 currentSection['role'] = null
            break;
            case "business":
                 currentSection['role'] = null
            break;
          }
          if(currentSection[tempcurrentView] !== section.identity+'-'+tempcurrentView && tempcurrentView !== ''){
              currentSection[tempcurrentView] = section.identity+'-'+tempnextView;
              if(tempsectionName!==null){
                currentSection[tempcurrentView]=tempsectionName + '-' + tempcurrentView
              }
          }
          this.setState({showSection:tempnextView,['selected'+tempcurrentView]:section.identity})
        }
      })
    }
  }

  /*handleInitialize (regionId) {
        console.log(regionId,'regionId')
        let initData = {
          regionList:regionId,
        }
    this.props.initialize(initData)
  }*/

  showHideAddMoreBox(section){

    /*console.log(section,showAddMore,'section')
    showAddMore.push('ads','sdf')
    var index = showAddMore.indexOf('sdf');
    if (index > -1) {
      showAddMore.splice(index, 1);
    }
      console.log(showAddMore,'12*')
      return;*/
    section = section.toLowerCase()  
    if(showAddMore.includes(section)){
      var index = showAddMore.indexOf(section);
      if (index > -1) {
        showAddMore.splice(index, 1);
      }
    }else{
      var index = showAddMore.indexOf(section);
      if(index <= -1){
        showAddMore.push(section)
      }
    }
    //this.render()
    let action = this.state.showAddMore !== false ? false : true
    this.setState({showAddMore: action})
  }

  handleSubmitAddMore(data){
    if(data.data_for!=="role" || data.add_data_0!==undefined){
    this.setState({lastFormSubmitted:data.data_for,dataSubmitted:data})
    this.props.handleAddMoreData(data,true)
    }else{
      if(this.props.masterData.companyMasterData.roleMaster==1){
        this.setState({lastFormSubmitted:data.data_for,dataSubmitted:data})
        this.props.handleAddMoreData(data,true)
        this.props.closeHierarchyDataPopup();
      }else{
        this.props.closeHierarchyDataPopup();
      }
    }
  }

  handleClickNext(currentView){


  }

  selectCountryFromRegion(data,isDirectData = false){
      if(!isDirectData){
        data = data.target.value
      }else{
        data = data
      }
      this.setState({showSection:''})
      this.props.fetchCountryForRegion(data)
      this.setState({regionSelected: data})
  }
  selectOfficeFromCountry(data, isDirectData = false){
      if(!isDirectData){
        data = data.target.value
      }else{
        data = data
      }
      this.setState({countrySelected: data})
      this.handleValueClick(data, 'office', 'country')
  }



  handleValueClick(selectedId,nextView,currentView,name=null){
                tempnextView= null,
                tempcurrentView = null
                tempsectionName = null
    this.setState({
      lastFormSubmitted : null
    })
    if(nextView == '' || (currentSection[currentView] == selectedId+'-'+currentView)){
      return
    }

    if(currentView !== 'office' && currentView !== 'country' && this.props['masterData']['companyMasterData'][currentView+'Master'] == 1){
        var me = this
         notie.confirm(
              `Please save data to proceed further`,
              'Yes',
              'No',
              function (me) {

                tempnextView= nextView,
                tempcurrentView = currentView
                tempsectionName = name
                // this.props.fetchNextSectionData(nextView,selectedId,true)
                // switch(currentView){
                //   case "department":
                //        currentSection['business'] = null
                //        currentSection['role'] = null
                //   break;
                //   case "business":
                //        currentSection['role'] = null
                //   break;
                // }
                // if(currentSection[currentView] !== selectedId+'-'+currentView && currentView !== ''){
                //     currentSection[currentView] = selectedId+'-'+currentView;
                //     if(name!==null){
                //       currentSection[currentView]=name + '-' + currentView
                //     }
                // }
                // this.setState({showSection:nextView,['selected'+currentView]:selectedId})

              }.bind(this)
              )
    }else{
          this.props.fetchNextSectionData(nextView,selectedId,true)
          switch(currentView){
            case "office":
                 currentSection['department'] = null
                 currentSection['business'] = null
                 currentSection['role'] = null
            break;
            case "department":
                 currentSection['business'] = null
                 currentSection['role'] = null
            break;
            case "business":
                 currentSection['role'] = null
            break;

          }
          if(currentSection[currentView] !== selectedId+'-'+currentView && currentView !== ''){
              currentSection[currentView] = selectedId+'-'+currentView;
          }
          this.setState({showSection:nextView,['selected'+currentView]:selectedId})
    }
    
    
  }

  deleteEntity(id,section,parentId/*,isHierarchy*/){
    var me = this;
    notie.confirm(
      `Are you sure you want to delete ${section} data?`,
      'Yes',
      'No',
      function () {
        if(me.props['masterData']['companyMasterData'][section+'Master'] == 1){
          me.props.updateHeirarchyPropsInit();
          const newProps = me.props;
          setTimeout(function(){ 
              let dataVal = newProps['masterData']['companyMasterData'][section];
              let newDataVal = []
              dataVal.map((sectionData,index)=>{
                  if(sectionData.identity !== id){
                     newDataVal.push(sectionData)   
                  }
              })
              newProps.updateHeirarchyProps(newDataVal,section);
           }, 100, newProps);
          
      }else{
        me.props.deleteEntity(id,section,parentId,true);
      }
      },
      function(){
      })

      
  }

  renderDepartment(data, section){
     
      let newData = data.office
      let newTitle = 'Office'
      let newDataToLoad = 'department'
      let displayTitle ='Office'
      if(section == 'department'){
          newData = data.department
          newTitle = 'Department'
          displayTitle='Department'
          newDataToLoad = 'business'
      }else if(section == 'business'){
          newData = data.business
          newTitle = 'Business'
          displayTitle='Business Unit'
          newDataToLoad = 'role'
      }else if(section == 'role'){
          newData = data.role
          newTitle = 'Role'  
          displayTitle ='Role'
          newDataToLoad = ''
      }
      //let OpenClass = showAddMore.indexOf(section) > -1 ? 'addMoreFormLi show' : 'addMoreFormLi';
      //console.log(showAddMore.indexOf(section),9,OpenClass)
      return(
          <div className="masterDataColumnInner">
            <div class="master-column-title">
                {displayTitle} 
            </div>

            <ul className="master-data-list hierarchy-data-list">
             
              {typeof newData !== 'undefined' && newData.length > 0? 
                 newData.map((departmentData,index)=>{
                    return(
                      <li key={index} 
                          className={currentSection[section] == departmentData.identity+'-'+section || currentSection[section]  == departmentData.name+'-'+section ? 'active master-data-list' : 'master-data-list'}>
                          <div  className="master-data-name"  onClick={this.handleValueClick.bind(this,departmentData.identity, newDataToLoad, section,departmentData.name)}>
                              {utils.limitLetters(departmentData.name,20)} 
                          </div>
                          <div className="removeDataBtn"><i class="material-icons" onClick={this.deleteEntity.bind(this,departmentData.identity,section,this.props['masterData']['companyMasterData']['parentId'][section],true)}>clear</i></div>
                      </li>
                    )
                 }) 
               : 
                !this.props.masterData.companyMasterData.companyMasterDataLoader ? 
                  <li>No {section} found</li>
                :'' 
              }
              {!this.props.masterData.companyMasterData.companyMasterDataLoader ? 
                <li className="addMoreDeptLi"><a onClick={this.showHideAddMoreBox.bind(this,newTitle)}>Add more...</a></li>
              :''}
              <li className='addMoreFormLi show'>
              
              {section == 'department' ?

                    <AddMoreFormDepartment 
                      formFor='department'
                      onSubmit={this.handleSubmitAddMore.bind(this)}
                      data={data}
                      departmentData={data.department}
                      selectedId={this.state.selectedoffice}
                      showAddMore={showAddMore}

                    />
                    : 
                    section == 'business' ?
                      <AddMoreFormBusiness 
                        formFor='business'
                        onSubmit={this.handleSubmitAddMore.bind(this)}
                        data={data}
                        selectedId={this.state.selecteddepartment}
                        showAddMore={showAddMore}
                        businessData={data.business}
                      />
                    :
                      section == 'office' ?
                        <AddMoreFormOffice 
                          formFor='office'
                          onSubmit={this.handleSubmitAddMore.bind(this)}
                          data={data}
                          selectedId={this.state.selectedcountry}
                          showAddMore={showAddMore}
                        /> 
                      : 
                        <AddMoreFormRole 
                          formFor='role'
                          onSubmit={this.handleSubmitAddMore.bind(this)}
                          data={data}
                          selectedId={this.state.selectedbusiness}
                          showAddMore={showAddMore}
                          roleData={data.role}
                        />
              }
              </li>
             
            </ul>
          </div>


      )
  }

  renderRegionDropBox(regionData, openSelectedRegion){
    if(openSelectedRegion !== 0){
      return(
        <div className="hierarDropdownColumn">
          <div className="select-wrapper">
            <select name="regionList" onChange={this.selectCountryFromRegion.bind(this)} value={openSelectedRegion}>
                              <option value="0">--Select Region--</option>
                              {
                                Object.keys(regionData).length > 0 ? 
                                  regionData.map((regions,index) =>(
                                    <option 
                                    value={regions.identity} 
                                    key={index}
                                    >{regions.name}</option>
                                  ))
                                :''  
                              }
            </select>  
          </div>
        </div>    
      )  
    }else{
       return(
          <div className="hierarDropdownColumn">
              <div className="select-wrapper">
                  <select name="regionList" onChange={this.selectCountryFromRegion.bind(this)}>
                                    <option value="0">--Select Region--</option>
                                    {
                                      Object.keys(regionData).length > 0 ? 
                                        regionData.map((regions,index) =>(
                                          <option 
                                          value={regions.identity} 
                                          key={index}
                                          >{regions.name}</option>
                                        ))
                                      :''  
                                    }
                  </select>  
              </div>
          </div>    
      )  
    }
    
  }
  renderCountryDropBox(countryData, openSelectedCountry, regionSelected){
    if(openSelectedCountry !== 0){
         return(
            <div className="hierarDropdownColumn">
              <div className="select-wrapper">
                  <select name="countryList" onChange={this.selectOfficeFromCountry.bind(this)} value={openSelectedCountry}>
                                <option value="0">--Select Country--</option>
                                {
                                   typeof countryData !== 'undefined' && countryData.length > 0 && regionSelected !== null ? 
                                    countryData.map((country,index) =>(
                                      <option value={country.identity} 
                                              key={index}
                                              >{country.title}</option>
                                    ))
                                  :''  
                                }
                  </select>  
              </div>
            </div>  
          )
    }else{
          return(
            <div className="hierarDropdownColumn">
              <div className="select-wrapper">
                <select name="countryList" onChange={this.selectOfficeFromCountry.bind(this)}>
                            <option value="0">--Select Country--</option>
                            {
                               typeof countryData !== 'undefined' && countryData.length > 0 && regionSelected !== null ? 
                                countryData.map((country,index) =>(
                                  <option value={country.identity} 
                                          key={index}
                                          >{country.title}</option>
                                ))
                              :''  
                            }
                </select>  
              </div>
            </div>  
          )
    }
   
  }
  
  
  
  
  render () {
    let abc = this.props.openSelectedRegion !== 0 ? 'value='+this.props.openSelectedRegion : ''
    return (
            <div class="master-data-popup">
          
                 <CenterPopupWrapper>
                    <header class="heading">
                      <h3>Hierarchy data</h3>
                      <button id="close-popup" className="btn-default" onClick={this.props.closeHierarchyDataPopup}><i className="material-icons" >clear</i></button>
                    </header>
                    
                     
                    <div className="content-wrapper">
                      <div className="hierarchyPopupDropdowns form-row">
                        {this.renderRegionDropBox(this.props.masterData.companyHierarchyData.regions, this.props.openSelectedRegion)}
                        {this.renderCountryDropBox(this.props.countryData, this.props.openSelectedCountry, this.state.regionSelected)}
                      </div>
                       
                      <div className="master-column-wrapper clearfix">
                        <div className="master-column department-column">
                        {   (this.state.showSection == 'office' || this.state.showSection == 'department' || this.state.showSection == 'business' || this.state.showSection == 'role') && this.state.countrySelected !== null? 
                                  this.renderDepartment(this.props.masterData.companyMasterData, 'office') 
                                :   
                                <div className="masterDataColumnInner">
                                    <div className="textPlaceholder">
                                      Office Location
                                    </div>
                                </div>
                        }
                        </div>
                        <div id="department-column" className="master-column department-column">
                        {       this.state.showSection == 'department' || this.state.showSection == 'business' || this.state.showSection == 'role'? 
                                  this.renderDepartment(this.props.masterData.companyMasterData, 'department')
                                :   
                                <div className="masterDataColumnInner">
                                    <div className="textPlaceholder">
                                      Department
                                    </div>
                                </div>
                        }
                        </div>
                       
                        <div id="business-column" className="master-column business-column">
                        {       this.state.showSection == 'business' ||  this.state.showSection == 'role' ? 
                                  this.renderDepartment(this.props.masterData.companyMasterData, 'business') 
                                :   
                                <div className="masterDataColumnInner">
                                    <div className="textPlaceholder">
                                      Business Unit
                                    </div>
                                </div>
                        }
                      
                        </div>
                        <div className="master-column role-column">
                          {this.state.showSection == 'role' ? 
                                  this.renderDepartment(this.props.masterData.companyMasterData, 'role') 
                                :   
                                <div className="masterDataColumnInner">
                                    <div className="textPlaceholder">
                                      Role
                                    </div>
                                </div>
                        }
                        </div>
                    </div>
                    </div>
                    
                 </CenterPopupWrapper>  
             
            
        </div>
    )
  }
}

function mapStateToProps (state) {
  return {
  }
}
function mapDispatchToProps (dispatch) {
  return {}
}
let connection = connect(mapStateToProps, mapDispatchToProps)
var reduxConnectedComponent = connection(HierarchyPopup)

export default reduxForm({
  form: 'HierarchyPopup' // a unique identifier for this form
})(reduxConnectedComponent)


