import $ from '../../Chunks/ChunkJquery';
import ChunkJqueryDropdown from '../../Chunks/ChunkJqueryDropdown';

import TableSearchWrapper from '../../TableSearchWrapper';
import UserDetailPopup from '../../General/userDetailPopup';
import reactable from '../../Chunks/ChunkReactable'
import Pagination from '../../Chunks/ChunkReactPagination';


var search
var pagination=1
export default class LocationsList extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      offices: [],
      search: [],
      Selectors: null,
      perPage: 10,
      main_state:[],
      filter_state:[],
      reactable:null,
      Pagination:null
    }
  }

  componentWillReceiveProps (nextProps) {
    let departments = []
    if (typeof nextProps.offices.data !== 'undefined' && nextProps.offices.data !== this.props.offices.data && nextProps.offices.data.length>0) {
      this.setState({
        offices: nextProps.offices
      })
      this.setState({main_state:nextProps.offices.data})
      this.setState({filter_state:nextProps.offices.data})
    }
  }
 componentWillMount () {
    this.setState({
      main_state:this.props.offices.data
    })
    reactable().then(reactable=>{
      Pagination().then(pagination=>{
        this.setState({reactable:reactable,Pagination:pagination })
      })
    })
    $().then(jquery => {
      var $ = jquery.$
      this.setState({
        jQuery: $
      })
      ChunkJqueryDropdown().then(jqueryDropdown => {})
    })
  }
  handleClick (event) {
    this.setState({
      currentPage: Number(event.target.id)
    })
  }
  handlePageClick (data) {
    var me = this
    let selected = data.selected
    let offset = Math.ceil(selected * this.state.perPage)

    this.setState({ offset: offset }, () => {
      this.loadCampaignDataOnPage(selected, offset)
    })
  }
  fetchOtherPageData (pageno) {
    // props comes from analyticsContainer to send next page number
    this.props.pageNo(pageno)
  }
  /**
   * @author disha
   * to add page in table
   * @param {*} totalPage
   */
  handlePageChange(pageNumber){
    this.LocationSearchRef !== undefined ?
      this.LocationSearchRef.value =''
    :''
    this.setState({
      activePage: pageNumber
    })
    pagination=pageNumber
    this.fetchOtherPageData(pageNumber)
  }
  search_text(e)
  {
    if(e.target.value!=="" && this.state.filter_state!==undefined)
    {
      var i=0;
      search=e.target.value.toLowerCase();
      var tempobj=this.state.filter_state.filter(function(temp){
        i=i+1;
        if(temp.name.toLowerCase().includes(search) || temp.country.toLowerCase().includes(search) || temp.region.toLowerCase().includes(search) || i==search){
          return true;
        }
      })
      this.setState({main_state:tempobj})
    }
    else{
      this.setState({main_state:this.state.filter_state})
    }
  }

  
  
  render () {
    var departments = this.props.offices
    var me = this
    var list = this.state.main_state == undefined? []: this.state.main_state;
    var Moment = this.props.Moment
    var totuser=0
    var metaDataOffice = this.props.offices.meta !== undefined ? this.props.offices.meta.pagination  :''
    return (
      
      // filterable={this.state.search}
      (
        <div className='table-white-container'>
        <div className='tableControllers clearfix'>
        <TableSearchWrapper>
        <input type="text" placeholder='Search in locations' onChange={this.search_text.bind(this)} ref={(input)=> this.LocationSearchRef= input}></input>
        </TableSearchWrapper>
      </div>
      <div className="widget">
        <div className='table-wrapper'>
        {this.state.reactable?
        <this.state.reactable.Table
          className='table department-list-table responsive-table'
          id='department-table'
          itemsPerPage={10}
          pageButtonLimit={5}
          noDataText=' No data found'
        >
          <this.state.reactable.Thead>
            <this.state.reactable.Th column='sr' className='a-center department-id-col'>Sr. No</this.state.reactable.Th>
            <this.state.reactable.Th column='region_name'>Region</this.state.reactable.Th>
            <this.state.reactable.Th className='a-center' column='country_name'>Country</this.state.reactable.Th>
            <this.state.reactable.Th className='a-center' column='office_name'>Offices</this.state.reactable.Th>
            <this.state.reactable.Th column='actions' className='actiondiv a-center'>Actions</this.state.reactable.Th>
          </this.state.reactable.Thead>
          {this.state.main_state!==undefined && this.state.main_state.length > 0? 
          this.state.main_state.map((office, i) => {
            return (
              <this.state.reactable.Tr key={i}>
                <this.state.reactable.Td className='fd a-center department-id-col' column='sr' data-rwd-label='Sr. No'>
                  {i+1}
                </this.state.reactable.Td>
                <this.state.reactable.Td className='fd' column='region_name' data-rwd-label='Region'>
                  {office.region}
                </this.state.reactable.Td>
                <this.state.reactable.Td className='fd' column='country_name' data-rwd-label='Country'>
                  {office.country}
                </this.state.reactable.Td>
                <this.state.reactable.Td className='fd office-form-row-settings' column='office_name' data-rwd-label='Offices'>
                  {office.name}
                </this.state.reactable.Td>
                <this.state.reactable.Td
                  className='fd department-action actiondiv a-center'
                  column='actions'
                  value={office.name}
                >
          
                  <div className='action-button'>
                    <ul
                      className='dd-menu context-menu user_3dots'
                    >
                      <li className='button-dropdown'>
                        <a className='dropdown-toggle user_drop_toggle'>
                          <i className='material-icons'>more_vert</i>
                        </a>
                        <ul className='dropdown-menu user_dropAction department-dropdown'>
                          <li>
                            <span
                              onClick={this.props.openOfficeHeirarchy.bind(this, office.identity,office.country_id,office.region_id)}
                            >
                             Open
                            </span>
                          </li>
                        </ul>
                      </li>
                    </ul>
                  </div>
                </this.state.reactable.Td>

              </this.state.reactable.Tr>
            )
          })
        : null
         }
        </this.state.reactable.Table> :''}
        </div>
        <div className='pagination-wrapper'>  
          {this.state.Pagination  ?
            <this.state.Pagination.default
              hideFirstLastPages
              prevPageText='Prev'
              nextPageText='Next'
              pageRangeDisplayed={7}
              activePage={pagination}
              itemsCountPerPage={10}
              totalItemsCount={metaDataOffice? metaDataOffice.total:0}
              onChange={::this.handlePageChange}
            />:''}
         </div>       
        </div>
        </div>
      )
    )
  }
}