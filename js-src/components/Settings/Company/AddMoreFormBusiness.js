import { connect } from 'react-redux'
import { Link } from 'react-router'
import { Field, reduxForm } from 'redux-form'
import PreLoader from '../../PreLoader'
import * as utils from '../../../utils/utils'
import { renderField } from '../../Helpers/ReduxForm/RenderField'
const required = value => (value ? undefined : 'Required');
class AddMoreFormBusiness extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      numberOfField : 1,
      dynamicField: [],
      old: null,
      showHideClass: null
    }
  }
  componentWillReceiveProps(nextProps) {
    if(nextProps.data.companyDataSaveLoader !== this.props.data.companyDataSaveLoader && !nextProps.data.companyDataSaveLoader){
        this.handleInitialize(this.props.formFor,this.props.selectedId,this.props.businessData)    
        this.setState({dynamicField: ['field']})
    } 
    if(nextProps.businessData !== this.props.businessData){
        this.setState({dynamicField: ['field']})
        this.handleInitialize(this.props.formFor,this.props.selectedId,nextProps.businessData)
    }
   
    if(typeof nextProps.showAddMore !== "undefined"){
        if(nextProps.showAddMore.indexOf('business') > -1){
             let tempDynamicField = this.state.dynamicField
            if(tempDynamicField.indexOf('field') < 0){
              tempDynamicField.push('field')  
            }
            this.setState({dynamicField:tempDynamicField})   
        }else{
          this.setState({dynamicField:[]}) 
        }
    }else{
        if(this.state.dynamicField.length == 0){
          this.setState({dynamicField:['field']})    
        }
        
    }
   
    this.setState({old:nextProps.formFor})
    if(this.props.formFor !== nextProps.formFor){
        this.handleInitialize(nextProps.formFor,this.props.selectedId,this.props.businessData)
    }
  }
  componentDidMount(){
      this.handleInitialize(this.props.formFor,this.props.selectedId,this.props.businessData)
  }
  handleInitialize (formFor,selectedId,businessData) {
        let initData = {
          data_for:formFor,
          parent_id:selectedId
        }
        if(Object.keys(businessData).length>0){
          let hiddenData = {}
          businessData.map((business, index)=>{
              hiddenData["save_data_"+index] = business.name
          })
          Object.assign(initData, hiddenData)
         // initData.concat(hiddenData)
        }
        
    
    this.props.initialize(initData)
  }
  handleAddMoreBusiness(){
    let newFieldCounter = this.state.dynamicField
    newFieldCounter.push('field')
    let numberOfField = this.state.numberOfField + 1
    this.setState({ numberOfField: numberOfField, dynamicField: newFieldCounter })
      //this.setState({numberOfField:this.state.numberOfField+1,holdedValues:holdedValues.push(document.getElementById('add_data_'+parseInt(this.state.numberOfField-1)).value)})
    //this.handleInitialize(this.state.numberOfField+1)
  }
  handleRemoveMoreBusiness(index){
    let newFieldCounter = this.state.dynamicField
    newFieldCounter.pop('field')
    let numberOfField = this.state.numberOfField - 1;
    this.setState({ numberOfField: numberOfField, dynamicField: newFieldCounter })
    this.props.change('add_data_'+index,null);
  }

  renderMasterForm(fetchedProps){
       const { handleSubmit, pristine, submitting } = fetchedProps
       return (  

            <div className="addMoreFormWrapper">
                 <form onSubmit={handleSubmit} name='AddMoreFormBusiness'>
                 <div className="moreDataFieldsWrapper">
                    {this
                    .state
                    .dynamicField
                    .map((field, index) => {
                          return (
                            <div className="mdafWrapper" key={'add_data_'+index}>
                              <div className="masterDataAddField" >
                                <Field  component={renderField}
                                      type='text'
                                      name={'add_data_'+index}
                                      validate={[required]}
                                      />
                                      {/* <a title="Remove data" id="add-more-data" class="removeMoreDataBtn"><i class="material-icons">remove</i></a> */}
                              </div>
                              <div class="addRemoveMasterData clearfix"> 
                                {index+1==this.state.dynamicField.length?
                                <a onClick={this.handleAddMoreBusiness.bind(this)} title="Add new data" id="add-more-data" class="addDataBtn"><i class="material-icons">add</i></a>:''}
                                {this.state.numberOfField!==1?
                                  <a onClick={this.handleRemoveMoreBusiness.bind(this,index)} title="Add new data" id="remove-more-data" class="addDataBtn"><i class="material-icons">remove</i></a>:''}
                            </div> 
                          </div>
                          )
                      
                    })}

                  {/* <div class="addRemoveMasterData clearfix">
                  <a onClick={this.handleAddMoreBusiness.bind(this)} title="Add new data" id="add-more-data" class="addDataBtn"><i class="material-icons">add</i></a>
                  
                  </div>  */}
                  </div>
                  <Field
                          component={renderField}
                          type='hidden'
                          name='data_for'
                          defaultValue={fetchedProps.formFor}
                  />
                  <Field
                          component={renderField}
                          type='hidden'
                          name='parent_id'
                          defaultValue={fetchedProps.selectedId}
                  />
                   <button
                    class='btn right'
                    type='submit'
                    disabled={pristine || submitting}
                  >
                    Save
                  </button>

                 </form>
            </div>
    ) 
  }

  renderHierarchyForm(fetchedProps){
       const { handleSubmit, pristine, submitting } = fetchedProps
       let OpenClass = fetchedProps.showAddMore.indexOf('business') > -1 ? 'hierarchy show' : 'hierarchy';
       return (  

            <div className="addMoreFormWrapper">
                 <form onSubmit={handleSubmit} name='AddMoreFormBusiness'>
                 <div className={OpenClass}>
                   <div className="moreDataFieldsWrapper">
                      {this
                      .state
                      .dynamicField
                      .map((field, index) => {
                            return (
                              <div className="mdafWrapper" key={`mdafWrapper${index}`}>
                                <div className="masterDataAddField" >
                                  <Field component={renderField}
                                        type='text'
                                        name={'add_data_'+index}
                                        validate={[required]}
                                        />
                                </div>
                                  <div class="addRemoveMasterData clearfix"> 
                                  {index+1==this.state.dynamicField.length?
                                  <a onClick={this.handleAddMoreBusiness.bind(this)} title="Add new data" id="add-more-data" class="addDataBtn"><i class="material-icons">add</i></a>:''}
                                  {this.state.numberOfField!==1?
                                    <a onClick={this.handleRemoveMoreBusiness.bind(this,index)} title="Add new data" id="remove-more-data" class="addDataBtn"><i class="material-icons">remove</i></a>:''}
                                  </div> 
                              </div>
                            )
                      })}

                    {/* <div class="addRemoveMasterData clearfix"><a onClick={this.handleAddMoreBusiness.bind(this)} title="Add new data" id="add-more-data" class="addDataBtn"><i class="material-icons">add</i></a></div>  */}
                    </div>
                      <Field
                              component={renderField}
                              type='hidden'
                              name='data_for'
                              defaultValue={fetchedProps.formFor}
                      />
                      <Field
                              component={renderField}
                              type='hidden'
                              name='parent_id'
                              defaultValue={fetchedProps.selectedId}
                      />
                      { 
                        Object.keys(fetchedProps.businessData).length > 0 ? 
                            fetchedProps
                            .businessData
                            .map((field, index) => {
                                  return (
                                      <Field
                                            component={renderField}
                                            type='hidden'
                                            name={'save_data_'+index}
                                            validate={[required]}
                                            key={index}
                                            />
                                  )
                              
                            })
                        : ''    
                      }
                       <button
                        class='btn right'
                        type='submit'
                        disabled={pristine || submitting}
                      >
                        Add
                      </button>
                  </div>
                  <button
                    class='btn Left hierarchyNextButton hide'
                    type='submit'
                    id='businessNext'

                  >
                    Next
                  </button>
                 </form>
            </div>
    ) 
  }

  
  
  render () {
   return(
      <div>
          {
            typeof this.props.showAddMore !== 'undefined' ? 
              this.renderHierarchyForm(this.props)
          : 
              this.renderMasterForm(this.props)
          }
      </div>
  )
    

    }
  
}
function mapStateToProps (state) {
  return {
  }
}
function mapDispatchToProps (dispatch) {
  return {}
}
let connection = connect(mapStateToProps, mapDispatchToProps)
var reduxConnectedComponent = connection(AddMoreFormBusiness)

export default reduxForm({
  form: 'AddMoreFormBusiness' // a unique identifier for this form
})(reduxConnectedComponent)
