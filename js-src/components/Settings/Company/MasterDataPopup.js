import { connect } from 'react-redux'
import { Link } from 'react-router'
import { Field, reduxForm } from 'redux-form'
import PreLoader from '../../PreLoader'
import * as utils from '../../../utils/utils'
import CenterPopupWrapper from '../../CenterPopupWrapper';
import AddMoreFormDepartment from './AddMoreFormDepartment';
import AddMoreFormBusiness from './AddMoreFormBusiness';
import AddMoreFormRole from './AddMoreFormRole';
import notie from "notie/dist/notie.js"
var showAddMore=[]
var currentSection={department:null,business:null,role:null}
class MasterDataPopup extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      showSection: 'department',
      showAddMore: [],
      selecteddepartment: null,
      selectedbusiness: null,
      selectedrole: null,
      lastFormSubmitted: null,
    }
  }
  componentWillReceiveProps(nextProps) {
      if(nextProps.masterData.companyDataSaveLoader !== this.props.masterData.companyDataSaveLoader && !nextProps.masterData.companyDataSaveLoader){
          this.setState({showAddMore:false})
          var index = showAddMore.indexOf(this.state.lastFormSubmitted);
          if (index > -1) {
            showAddMore.splice(index, 1);
          }
      }
      var selectedSection =[]
      switch(this.state.lastFormSubmitted){
        case 'department':
            nextProps.masterData.department.length>0 ?
           selectedSection =  nextProps.masterData.department.filter((department,i)=>{
             return department.name.toLowerCase() == this.state.dataSubmitted.add_data_0.toLowerCase()
           }):''
           selectedSection.length !==0?
           this.handleValueClick(selectedSection[0].identity,'business','department'):
           '';
           break;
       case 'business':
          nextProps.masterData.business.length>0?
           selectedSection = nextProps.masterData.business.filter((business,i)=>{
             return business.name.toLowerCase() == this.state.dataSubmitted.add_data_0.toLowerCase()
           }):'';
           selectedSection.length !==0?
           this.handleValueClick(selectedSection[0].identity,'role','business'):'';
           break;
          }
      if(nextProps.masterData.deleteEntity !== this.props.masterData.deleteEntity && nextProps.masterData.deleteEntity !== null){
            this.setState({showSection: nextProps.masterData.deleteEntity})
            this.props.deleteFlagUpdate();
      }
    
  }
  componentDidMount (){
      currentSection={department:null,business:null,role:null}
      showAddMore=[]
      var me=this
      //OnClick of Outside it will close Popup
      window.addEventListener("click", function(event) {
        if (event.target.className.includes("popup-center-wrapper")) {
          me.props.closeMasterDataPopup();
        }
      });
  }

  showHideAddMoreBox(section){

    /*console.log(section,showAddMore,'section')
    showAddMore.push('ads','sdf')
    var index = showAddMore.indexOf('sdf');
    if (index > -1) {
      showAddMore.splice(index, 1);
    }
      console.log(showAddMore,'12*')
      return;*/
    section = section.toLowerCase()  
    if(showAddMore.includes(section)){
      var index = showAddMore.indexOf(section);
      if (index > -1) {
        showAddMore.splice(index, 1);
      }
    }else{
      var index = showAddMore.indexOf(section);
      if(index <= -1){
        showAddMore.push(section)
      }
    }
    //this.render()
    let action = this.state.showAddMore !== false ? false : true
    this.setState({showAddMore: action})
  }

  handleSubmitAddMore(data){
    this.setState({lastFormSubmitted:data.data_for,dataSubmitted:data})
    this.props.handleAddMoreData(data)
  }

  handleClickNext(currentView){


  }

  handleValueClick(selectedId,nextView,currentView){
    this.setState({
      lastFormSubmitted : null
    })
    if(nextView == '' || (currentSection[currentView] == selectedId+'-'+currentView)){
      return
    }
    this.props.fetchNextSectionData(nextView,selectedId)
    this.setState({showSection:nextView,['selected'+currentView]:selectedId,parentId:selectedId})
    switch(currentView){
      case "department":
           currentSection['business'] = null
           currentSection['role'] = null
      break;
      case "business":
           currentSection['role'] = null
      break;
    }
    if(currentSection[currentView] !== selectedId+'-'+currentView && currentView !== ''){
          currentSection[currentView] = selectedId+'-'+currentView;
    }
    document.querySelectorAll('.master-column-wrapper-inner')[0].classList.add(nextView);
  }

  deleteEntity(identity,section,data){
    var me = this;
    notie.confirm(
      `Are you sure you want to delete ${section} data?`,
      'Yes',
      'No',
      function () {
        me.props.deleteEntity(identity,section,data,false);
      },
      function(){
      })
  }

  renderDepartment(data, section){
      let newData = data.department
      let newTitle = 'Department'
      let newDataToLoad = 'business'
      let DisplayTitle = 'Department'
      let departmentData={}
      if(section == 'business'){
          newData = data.business
          newTitle = 'Business'
          DisplayTitle='Business Unit'
          newDataToLoad = 'role'
      }else if(section == 'role'){
          newData = data.role
          newTitle = 'Role'   
          newDataToLoad = ''
          DisplayTitle = 'Role'
      }
      let OpenClass = showAddMore.indexOf(section) > -1 ? 'addMoreFormLi show' : 'addMoreFormLi';
      return(
          <div className="masterDataColumnInner">
            <div class="master-column-title">
                {DisplayTitle} 
            </div>
            <ul className="master-data-list">
              {typeof newData !== 'undefined' && newData.length > 0? 
                 newData.map((departmentData,index)=>{
                    return(
                      <li 
                        key={index} 
                        className={currentSection[section] == departmentData.identity+'-'+section ? 'active master-data-list' : 'master-data-list'} 
                        title={departmentData.name} 
                        >
                        <div className="masterDataText" onClick={this.handleValueClick.bind(this,departmentData.identity, newDataToLoad, section)}>
                          {departmentData.name} 
                        </div>
                        <div className="removeDataBtn"> <i class="material-icons" 
                        onClick={
                          this.deleteEntity.bind(this,departmentData.identity,section,this.props['masterData']['parentId'][section])}>clear</i></div>

                      </li>

                    )
                 }) 
               : 
                !this.props.masterData.companyMasterDataLoader ? 
                  <li>No {section} found</li>
                :'' 
              }
              {!this.props.masterData.companyMasterDataLoader ? 
                <li className="addMoreDeptLi"><a onClick={this.showHideAddMoreBox.bind(this,newTitle)}>Add more...</a></li>
              :''}
              <li className={OpenClass}>
              
              {section == 'department' ?

                    <AddMoreFormDepartment 
                      key={'departmentForm'}
                      formFor='department'
                      onSubmit={this.handleSubmitAddMore.bind(this)}
                      selectedId={null}
                      data={data}
                      departmentData={newData}
                    />
                    : 
                    section == 'business' ?
                      <AddMoreFormBusiness 
                        key={'businessForm'}
                        formFor='business'
                        onSubmit={this.handleSubmitAddMore.bind(this)}
                        data={data}
                        selectedId={this.state.selecteddepartment}
                        businessData={newData}
                      />
                    :
                      <AddMoreFormRole 
                        formFor='role'
                        onSubmit={this.handleSubmitAddMore.bind(this)}
                        data={data}
                        selectedId={this.state.selectedbusiness}
                        roleData={newData}
                      />  
              }
              </li>
            </ul>
          </div>


      )
  }
  
  
  render () {
    return (
            <div class="master-data-popup">
          
                 <CenterPopupWrapper>
                    <header class="heading">
                      <h3>Master data</h3>
                      <button id="close-popup" className="btn-default" onClick={this.props.closeMasterDataPopup}><i className="material-icons" >clear</i></button>
                    </header>
                    <div className="master-column-wrapper clearfix">
                      <div className="master-column-wrapper-inner">
                      <div className="master-column department-column">
                        {this.renderDepartment(this.props.masterData, 'department')}
                      </div>
                      
                      <div className="master-column business-column">
                      {       this.state.showSection == 'business' ||  this.state.showSection == 'role' ? 
                                this.renderDepartment(this.props.masterData, 'business') 
                              :   
                              <div className="masterDataColumnInner">
                                  <div className="textPlaceholder">
                                    Business Unit
                                  </div>
                              </div>
                      }
                    
                      </div>
                      <div className="master-column role-column">
                        {this.state.showSection == 'role' ? 
                                this.renderDepartment(this.props.masterData, 'role') 
                              :   
                              <div className="masterDataColumnInner">
                                  <div className="textPlaceholder">
                                    Role
                                  </div>
                              </div>
                      }
                      </div>
                      </div>
                    </div>
                 </CenterPopupWrapper>  
             
            
        </div>
    )
  }
}

function mapStateToProps (state) {
  return {
  }
}
function mapDispatchToProps (dispatch) {
  return {}
}
let connection = connect(mapStateToProps, mapDispatchToProps)
var reduxConnectedComponent = connection(MasterDataPopup)

export default reduxForm({
  form: 'MasterDataPopup' // a unique identifier for this form
})(reduxConnectedComponent)


