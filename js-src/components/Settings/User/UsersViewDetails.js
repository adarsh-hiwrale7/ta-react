import DataRow from '../../DataRow'

class UsersViewDetails extends React.Component {
  constructor (props) {
    super(props)
  }

/**
 * @author : disha
 * @use : to display data in read mode
 */
  usersDataDetail () {
      var UserData={}
    if (typeof this.props.currUser !== 'undefined') {
      const user = this.props.currUser
      UserData = {
        identity: user.identity,
        name: user.firstname,
        surname: user.lastname,
        email_address: user.email,
        user_position: user.CompanyRole!==undefined?user.CompanyRole.data.title:'',
        role: user.role.data[0].role_name,
        department: user.Department.data.name,
      }
    }
    return (
      <div>
        <DataRow label='First Name' value={UserData.name} />
        <DataRow label='Last Name ' value={UserData.surname} />
        <DataRow label='Email' value={UserData.email_address}/>
        <DataRow label='Position'value={UserData.user_position}/>
        <DataRow label='Role' value={UserData.role} />
        <DataRow label='Department' value={UserData.department} />
      </div>
    )
  }
  render () {
    return (
      <section className='asset-detail-page'>

        <div id='asset-detail'>
            {this.usersDataDetail()}
        </div>

      </section>
    )
  }
}

module.exports = UsersViewDetails
