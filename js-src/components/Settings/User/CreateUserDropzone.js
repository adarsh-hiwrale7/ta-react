import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Field, reduxForm } from 'redux-form';
import { renderDropzoneBox } from '../../Helpers/ReduxForm/RenderField'
import ReactDropzone from '../../Chunks/ChunkReactDropzone';
import * as assetsActions from '../../../actions/assets/assetsActions'
import * as s3Actions from '../../../actions/s3/s3Actions'
import * as s3functions from '../../../utils/s3'
import * as utils from '../../../utils/utils'
import notie from "notie/dist/notie.js"


const required = value => value ? undefined : 'Required';

var data = [];
class CreateUserDropzone extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      file: null,
      filedata: null,
      files: [],
      title: "uploaded",
      reactDropzone:null
    }
  }
  onDrop(files) {
    this.setState({
      files
    },()=>{
      this.props.Filedata(this.state.files[0])
    });

  }
/**
* Warning: This lifecycle is currently deprecated, and will be removed in React version 17+
More details here: https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html
*/
componentWillMount() {
  ReactDropzone().then(dropzone=>{
    this.setState({
      reactDropzone:dropzone
    })
  })
}
  handleInitialize(e) {
    const initData = {
      filepicker: e.target.value
    }
  }
  componentDidMount() {
    s3functions.loadAWSsdk()
    this.props.actions.uploadReset()
  }
  handleDropzoneSubmit(field) {
    var me = this
    if (field.target.files.length == 1) {
      if (field.target.files[0].type == 'text/csv' || field.target.files[0].type == 'text/comma-separated-value' || field.target.files[0].type == 'application/csv'
       || field.target.files[0].type == 'application/excel' || field.target.files[0].type == 'application/vnd.ms-excel' || field.target.files[0].type == 'application/vnd.msexcel') {
        this.props.Filedata(field.target.files[0])
      } else {
        notie.alert('warning', 'Please Select Only CSV File.', 3)
      }
    } else {
      notie.alert('warning', 'Sorry, You can not select multiple files.', 3)
    }
  }
  renderDropzone() {
    const { handleSubmit, pristine, submitting, tags } = this.props;
    var me = this;
    const UploadFile = ({ input: { value: omitValue, ...inputProps }, meta: omitMeta, ...props }) => (
      <input type='file' {...inputProps} {...props} />
    );
    var dropzoneStyle = {
      borderColor: '#666',
      borderRadius: '5px',
      borderStyle: 'dashed',
      borderWidth: '1px',
      height: 'auto',
      minHeight: '120px',
      lineHeight: '100px',
      textAlign: 'center',
      padding: '10px',
      width: '100%',
    }

    return (
      <form>
        <div className="section">
          <div className="step1 active">
            <div className="dropzoneUploadWrapper">
              <div className="dropzoneUpload">
              {this.state.reactDropzone?
                <this.state.reactDropzone
                  name="file"
                  onDrop={this.onDrop.bind(this)}
                  style={dropzoneStyle}
                  component={renderDropzoneBox}
                  onChange={this.handleDropzoneSubmit.bind(this)}
                  accept=".csv"
                >
                  <div className='dropzoneIconWrapper'><i className='material-icons'>cloud_upload</i></div><div className='dropzoneTitle'>Drop csv files here</div>
                </this.state.reactDropzone>
                :''}
              </div>
              <ul className="dropzone-preview">
                {
                  this.state.files.map((f, i) => {
                    return (
                      <li className={`item-${i}`} rel={i} key={i}>
                        <div className='dropPreviewIcon'><i class="material-icons asset-title-icon">insert_drive_file</i></div>
                        <p className="text-csv">{f.name} - {f.size} bytes</p>
                      </li>
                    )
                  })
                }
              </ul>
            </div>
          </div>
        </div>
      </form>
    )
  }
  render() {

    var assetCreated = this.props.assets;

    return (
      <div className="section">
        {this.renderDropzone()}
      </div>
    )
  }

}


function mapStateToProps(state) {
  return {
    aws: state.aws,
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(
      Object.assign({}, assetsActions, s3Actions),
      dispatch
    )
  }
}
let connection = connect(mapStateToProps, mapDispatchToProps);

var reduxConnectedComponent = connection(CreateUserDropzone);

export default reduxForm({
  form: `CreateUserDropzone`,  // a unique identifier for this form
})(reduxConnectedComponent)
