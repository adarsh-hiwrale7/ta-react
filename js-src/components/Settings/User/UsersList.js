import $ from '../../Chunks/ChunkJquery'
import ChunkJqueryDropdown from '../../Chunks/ChunkJqueryDropdown'
import moment from '../../Chunks/ChunkMoment'
import PreLoader from '../../PreLoader'
import reactTooltip from '../../Chunks/ChunkReactTooltip';

import TableSearchWrapper from '../../TableSearchWrapper';
import UserDetailPopup from '../../General/userDetailPopup';
import FilterTableColumns from '../../FilterTableColumns';
import reactable from '../../Chunks/ChunkReactable'
import FetchuserDetail from '../../FetchUsersDetail/FetchUsersDetail';
import { isNull } from 'util';
import Pagination from '../../Chunks/ChunkReactPagination';
var tableColumns= '' ;
var countColums = 0;
let  _timeout = 0
const titles = ['Dr.', 'Mr.', 'Mrs.', 'Miss', 'Ms.']
let data=[];
var search
var pagination=1
export default class UsersList extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      users: [],
      Selectors: null,
      perPage: 10,
      moment: null,
      userimagesettings:null,
      filter_state:[],
      main_state:[],
      reactable:null,
      tooltip: null,
      Pagination:null,
      sortingData:{},
      filterData:null
    }
  }
 componentDidMount(){
    _timeout = 0
    countColums  = tableColumns.length;
    pagination=1
  }
  componentWillReceiveProps (nextProps) {
    if (nextProps.users.length) {
      this.setState({
        users: nextProps.users
      })
    }
    this.setState({main_state:nextProps.users})
    this.setState({filter_state:nextProps.users})
  }
  componentWillMount () {
    this.setState({
      main_state:this.props.users
    })
    reactable().then(reactable=>{
      reactTooltip().then(reactTooltip => {
        Pagination().then(pagination=>{
          this.setState({reactable:reactable,tooltip: reactTooltip,Pagination:pagination })
        })
      })
    })

    $().then(jquery => {
      var $ = jquery.$
      this.setState({
        jQuery: $
      })
      ChunkJqueryDropdown().then(jqueryDropdown => {})
    })
    moment().then(moment => {
      this.setState({ moment: moment })
    })
  }
   /**
   * @author disha
   * to add page in table
   * @param {*} totalPage
   */
  handlePageChange(pageNumber)
  {
    this.setState({
      activePage: pageNumber
    })
    pagination=pageNumber
    this.fetchOtherPageData(pageNumber)
  }
  fetchOtherPageData (pageno) {
    // props comes from analyticsContainer to send next page number
    this.props.pageNo(pageno,this.state.sortingData, this.state.filterData, this.state.showNoOfData)
  }
  search_text(e){ 
    let value = e.target.value
    //call api after typing is stopped
    if(_timeout) clearTimeout(_timeout);
      _timeout = setTimeout(() => {
      // if(filterValue !== value.trim()){
        this.setState({
          filterData : value.trim()
        })
        this.setState({
          activePage: 1
        })
          pagination=1
        this.props.fetchUsersData(1, this.state.sortingData, value, this.state.showNoOfData)
      // }
    }, 1000);
  }
  onHeaderClick = (column) => {
    const columnid= column[0];
    if(columnid !== 'status' && columnid !== 'lastlog' && columnid !== 'action' && columnid !== 'userimg'){
      let existingUser = this.state.sortingData.name == undefined? null :
            this.state.sortingData.name == columnid ?
            this.state.sortingData.order : null
      
    let nextUser = null
    switch (existingUser) {
      case null:
        nextUser = 'Asc'
        break
      case 'Asc':
        nextUser = 'Desc'
        break
      case 'Desc':
        nextUser = null
        break
      default:
        nextUser = null
    }
    if (nextUser == null) {
        this.state.sortingData = {}
      this.props.fetchUsersData(1, this.state.sortingData, this.state.filterData, this.state.showNoOfData)
    } else {
      const sortedData = {'name' :columnid ,'order': nextUser}
      this.setState({
        sortingData : sortedData
      })
      this.props.fetchUsersData(
        1,
        sortedData,
        this.state.filterData,
        this.state.showNoOfData
      )
    }
    this.setState({
      activePage: 1
    })
      pagination=1
  }
  }
  //table view of user data
  userTabelData(userData, tableColumns){
   var UserTabledata = userData;
   var tddata = '';
   var me = this;
   let trusted = '' ;
   let socialStatus = '' ;
   let roleName='';
   if(localStorage.getItem('roles') !== null){
    roleName = JSON.parse(localStorage.getItem('roles'))[0]['role_name'];
  }
  var usersListMeta= Object.keys(this.props.usersListMeta).length>0? this.props.usersListMeta:''
   return(
        <div className='widget'>
           {countColums == 0
          ? <div className='no-data-block'> Select a Columns</div>
          : <div className='table-user-body'>
            {this.state.reactable?
            <this.state.reactable.Table
              className='table responsive-table'
              id='setting-user-table'
              itemsPerPage={10}
              pageButtonLimit={5}
              >
              <this.state.reactable.Thead>
                {tableColumns.map(function (column, columnIndex) {
                  let columnState = column[0]
                  let columnLabel = column[1]
                  let centerClass = column[2]
                  return (

                    <me.state.reactable.Th
                      key={columnIndex}
                      column={columnState}
                      className={`${columnState} ${me.state[columnState]} ${centerClass}`}
                      >
                      <strong className={`name-header ${Object.keys(me.state.sortingData).length > 0 ? me.state.sortingData.name == columnState ? me.state.sortingData.order+'-Order' :'':''}`} onClick={me.onHeaderClick.bind(me,column)}>{columnLabel}</strong>
                      {columnState!=='action' && columnState!=='status' && columnState!=='lastlog'  && columnState !== 'userimg'? 
                       <i />:''}
                    </me.state.reactable.Th>
                  )
                })}
              </this.state.reactable.Thead>

              {UserTabledata
                  ?  UserTabledata.map((userdata, i) => {

                    return (
                        (
                          <this.state.reactable.Tr
                           className='table-body-text post' key={i}>
                            {tableColumns.map(function (column, columnIndex) {

                              let columnState = column[0]
                              let columnLabel = column[1]
                              let centerClass = column[2]
                              let tdData;
                              let ActiontdData;
                              if (columnState == 'userimg') {
                                tdData= userdata.avatar
                               }
                              else if (columnState == 'user_name') {
                                tdData = userdata.firstname + ' ' + userdata.lastname
                              }
                              else if (columnState == 'company_position') {
                                 tdData =  userdata.CompanyRole?userdata.CompanyRole.data.title:''
                              }

                              else if (columnState == 'user_role') {
                                var role = userdata.role.data;
                                trusted = userdata.is_trusted === 0 ? "Trusted" : "Untrusted"
                                socialStatus=userdata.enable_social_posting === 0 ? 'Social on' :'Social off'
                                var list =  role.map((role,i ) =>
                                      tdData = role.role_name
                                  )
                              }
                              else if (columnState == 'email') {
                                tdData = userdata.email
                              }
                              else if (columnState == 'department') {
                                tdData = userdata.Department? userdata.Department.data.name:''
                              }
                              else if (columnState == 'status') {

                                userdata.paused== 0 ?
                                tdData = 'Active' :
                                tdData =  'Suspended'
                              }
                              else if (columnState == 'lastlog') {
                                 tdData = userdata.last_login==null?'': me.state.moment !== null
                                ? me.state.moment(userdata.last_login).fromNow()
                                : ''
                               }
                               else if(columnState == 'action') {
                                 //for 3 dots button
                                  ActiontdData =   <div className='action-button'>
                                  <ul
                                    className='dd-menu context-menu user_3dots'
                                    onClick={e => {
                                      me.props.dropdownPopup(e)
                                    }}
                                  >
                                    <li className='button-dropdown'>
                                      <a className='dropdown-toggle user_drop_toggle'>
                                        <i className='material-icons'>more_vert</i>
                                      </a>

                                      <ul className='dropdown-menu user_dropAction'>
                                      {/* if user is admin then he should not be suspend or edit super admin */}

                                      {typeof userdata.role !=='undefined' && userdata.role.data.length>0?
                                         (roleName=='admin' &&  userdata.role.data[0].role_name!=='super-admin') || roleName=='super-admin'?
                                         <div>
                                         { userdata.role.data[0].role_name !== "guest" ?
                                        <li>
                                          <span   onClick={me.props.userUpdateClick.bind(me, userdata.identity)}
                                          >
                                            Edit{' '}
                                          </span>
                                        </li> :''}
                                        {me.props.profile.email!==userdata.email?
                                        <li>
                                          {userdata.paused == 1
                                            ? <span    onClick={me.props.userPauseClick.bind(
                                                  me,
                                                  userdata.identity
                                                )}
                                              >
                                              Reinstate{' '}
                                            </span>
                                            : <span   onClick={me.props.userPauseClick.bind(
                                                  me,
                                                  userdata.identity
                                                )}
                                              >
                                              Suspend
                                            </span>}
                                        </li>:''}
                                        <li>
                                        {userdata.role.data.length>0
                                          ? userdata.role.data[0].role_name === "employee"
                                            ? <span  onClick={me.props.handleTrusted.bind(me,userdata.identity)}>
                                                  {trusted}
                                              </span>
                                            :''
                                          :''}
                                        </li>
                                        {userdata.role.data[0].role_name !=='super-admin' && roleName == 'super-admin' && userdata.paused == 0?
                                        <li>
                                        {userdata.role.data.length>0
                                          ? userdata.role.data[0].role_name !== "super-admin"
                                            ? <span  onClick={me.props.handleSocialPermissionStatus.bind(me,userdata)}>
                                                  {socialStatus}
                                              </span>
                                            :''
                                          :''}
                                        </li>:''}
                                        </div>
                                          : <li>
                                            <span  onClick={me.props.viewUserDetail.bind(me, userdata.identity)}>
                                                View
                                            </span>
                                          </li>:''}
                                      </ul>
                                    </li>
                                  </ul>
                                </div>
                               }
                              else {
                                tdData = userdata[columnState]
                              }
                          return(
                            <me.state.reactable.Td
                                key={columnIndex}
                                column={columnState}
                                className={`${columnState} ${me.state[columnState]} ${centerClass}`}
                                data-rwd-label={columnLabel}
                               >
                               {columnState == 'userimg' ?
                                  <div className="author-thumb">
                                        <img
                                          src={tdData}
                                          alt={"Default image"}
                                          className='list-avatar thumbnail'
                                          data-tip={userdata.email}
                                          data-for={userdata.identity}
                                          onError={(e) => { e.target.src = "https://app.visibly.io/img/default-avatar.png" }}
                                          />
                                          <FetchuserDetail userID={userdata.identity}/>
                                    </div>
                                  : columnState == 'user_role' ?
                                      userdata.is_trusted == 1 ?
                                      <div>
                                        {`${tdData}`}
                                        <span className = "trusted-user"> Trusted <i class="material-icons">verified_user</i></span>
                                      </div>:tdData
                                  :columnState == 'action'?
                                    ActiontdData
                                    :`${tdData}`}

                               </me.state.reactable.Td>
                               )
                            })}
                          </this.state.reactable.Tr>

                        )
                      )
                  })
                  : ''}
            </this.state.reactable.Table>:''}
            <div className='pagination-wrapper'>
              {this.state.Pagination?
                <this.state.Pagination.default
                  hideFirstLastPages
                  prevPageText='Prev'
                  nextPageText='Next'
                  pageRangeDisplayed={7}
                  activePage={pagination}
                  itemsCountPerPage={10}
                  totalItemsCount={usersListMeta? usersListMeta.pagination.total:0}
                  onChange={::this.handlePageChange}
                />:''}
            </div>
          </div>}
      </div>
      )
  }
  /**
    *add the filter in user list
    @author kinjal birare

   */
  changeState (stateName, value) {
    var newState = {}
    newState[stateName] = value
    this.setState(newState)
  }
  checkDetails (e) {
    var i = 0
    var checkboxvalue = document.getElementById(e).checked
    if (checkboxvalue == true) {
      this.changeState(e, '')
      countColums++
    } else {
      this.changeState(e, 'hide')
      countColums--
    }
  }
  createDropdown (tableColumns) {
    var me = this
    return (
      <ul class='dropdown-menu checkboxDropdown'>
        {tableColumns
          ? tableColumns.map(function (column, columnIndex) {
            let columnState = column[0]
            let columnLabel = column[1]
            if (me.state[columnState] == undefined) {
              me.setState({
                [columnState]: ''
              })
            }
            return (
              <li key={columnIndex}>
                <div className='form-row checkbox'>
                  <input
                    id={columnState}
                    name={columnState}
                    className='checknotify'
                    type='checkbox'
                    onChange={me.checkDetails.bind(me, columnState)}
                    checked={me.state[columnState] == ''}
                    />
                  <label for={columnState}>{columnLabel}</label>
                </div>
              </li>
            )
          })
          : ''}
      </ul>
    )
  }
  render () {
    var me = this
    var Moment = this.props.Moment
    var suspenduserclass=''

    tableColumns = [
      ['userimg', 'User Img', 'userimg'],
      ['user_name', 'User Name', ''],
      ['company_position', 'Company Position', ''],
      ['user_role', 'User', ''],
      ['email' , 'Email', ''],
      ['department', 'Department', ''],
      ['status', 'Status', ' '],
      ['lastlog', 'Last Log', ' '],
      ['action' , 'action' , ' ']
    ]
    return (
      
       <div className='table-white-container'>

          <div className='tableControllers clearfix'>
          {/* for search  */}
          {this.props.location.pathname!=="/search/user"?
            <TableSearchWrapper>
              <input type="text" placeholder='Search in user' onChange={this.search_text.bind(this)} />
            </TableSearchWrapper>:''
          }
          <div className= "filtertable">

           {/*    for filter columns */}
            <FilterTableColumns>
            {this.createDropdown(tableColumns)}
          </FilterTableColumns>


          </div>
        </div>
        {this.props.loading ? <PreLoader location="userList-Loader"/> :
       <div className='tabel-view-userdata'>
         {this.props.users.length > 0 ?

            <div>
            {/* user table data */}
            {this.userTabelData(this.state.main_state,tableColumns)}
            </div>
              : <div className='no-data-block'>
              No user found.
        </div> }
       </div>}
      </div>

     )
  }
}