import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as departmentActions from "../../../actions/departmentActions"
import * as loginActions from "../../../actions/loginActions"
import * as settingsActions from "../../../actions/settingsActions"
import * as userActions from "../../../actions/userActions"
import * as searchActions from "../../../actions/searchActions"
import * as companyInfo from '../../../actions/settings/companyInfo';
import Globals from "../../../Globals";
import $ from '../../Chunks/ChunkJquery';
import CreateUser from './CreateUser';
import CreateUserDropzone from './CreateUserDropzone'
import Header from '../../Header/Header';
import moment from '../../Chunks/ChunkMoment';
import notie from "notie/dist/notie.js"
import PreLoader from '../../PreLoader'
import Papa from '../../Chunks/ChunkPapaParse';
import PopupWrapper from '../../PopupWrapper';
import ReactCSSTransitionGroup from 'react/lib/ReactCSSTransitionGroup';
import SliderPopup from '../../SliderPopup'
import SettingsNav from '../SettingsNav';
import SearchNav from '../../Search/SearchNav';
import UpdateUser from './UpdateUser';
import UsersSettingsForm from './UsersSettingsForm';
import UsersList from "./UsersList";

var csvfiledata = ''
var Colindex = []
var selectedColindex = []
var storeColName_save = ''
var allCSVdataArray = []
var invalidData = false
var isDataSaved = false
var oldarrayData = []
let NavBar
let title
var restrictAPI=false;
var keyTotalLength=[]
class UsersSettings extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      searchData:{},
      departments: [],
      members: [],
      Moment: null,
      newUserPopup: false,
      updateUserPopup: false,
      show_menu: true,
      JobPostsPopup: false,
      filedata: null,
      firstcolname: '',
      editDivid: 0,
      mapdata: {},
      firstcolIndex: '',
      colindex: {},
      allrequire: false,
      invalidData: false,
      searchuser:[],
      viewUser:false,
      Papaparse:null,
      pageno:1
    }
    // const style={
    //   background:'red'
    // }
  }
  componentWillMount() {
    $().then(jquery => {
      var $ = jquery.$
      this.setState({
        jQuery: $
      })
    })
    Papa().then(papa => {
      this.setState({ Papaparse: papa })
    })
    moment().then((moment) => {
      this.setState({ Moment: moment })
    }).catch(err => {
      throw err;
    });
  }
  componentDidMount() {
    this.setState({
      searchData:this.props.searchData
    })
    if(restrictAPI!==true){

      this.props.departmentActions.fetchDepartments();
      this.props.companyInfoAction.fetchRegion();
        if(this.props.searchInput==undefined)
        {
          this.props.userActions.fetchUsers(null,this.state.pageno);
        }
        else
        {
          this.props.userActions.fetchUsers(this.props.searchInput);
        }
        this.props.userActions.fetchRoles();
        this.props.userActions.fecthFieldName();

        restrictAPI=true;
        setTimeout(()=>{
          restrictAPI=false
        },5000)
    }    
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.users.closeAllPopups !== this.props.users.closeAllPopups && nextProps.users.closeAllPopups == true) {
      this.closePopup('upload_csv');
      this.props.userActions.AddUserError();
      document.body.classList.remove("overlay")
      this.setState({
        newUserPopup: false,
        updateUserPopup: false,
      })
    }

  }
  handleUserSubmit(values) {
    this.props.userActions.saveUser(values);
  }

  handleAddUser() {
    this.setState({
      newUserPopup: true
    });

    document.body.classList.add("overlay")
  }

  openUpload_csv() {
    this.setState({
      JobPostsPopup: true
    });

    document.body.classList.add("overlay");
  }

  closePopup(popup) {
    switch (popup) {
      case "create":
        this.setState({
          newUserPopup: false
        });
        break;
      case "update":
        this.setState({
          updateUserPopup: false,
          viewUser:false
        });
        break;
      case "upload_csv":
        csvfiledata = '',
          allCSVdataArray = []
        oldarrayData = []
        this.setState({
          newUserPopup: false,
          JobPostsPopup: false,
          filedata: null,
          colindex: {},
          editDivid: 0,
          mapdata: {}
        });
        break;
    }

    document.body.classList.remove("overlay");
  }
  closeUserCreatePopup() {
    this.setState({
      newUserPopup: false
    });
    document.body.classList.remove("overlay");
  }

  renderUserCreation() {
    var departments = (typeof (this.props.departments) !== 'undefined')
      ? this.props.departments
      : [];
    return (

      <SliderPopup className='formPopup'>
      
        <button id="close-popup" className="btn-default" onClick={this.closePopup.bind(this, 'create')}><i className="material-icons">clear</i></button>
        <CreateUser roles={this.props.roles} departments={departments} searchInput={this.props.searchInput} regionList={this.props.regionData}></CreateUser>

      </SliderPopup>

    )
  }
  userUpdateClick(user_id) {
    var searchdata=this.props.searchInput
    if(searchdata==undefined){
      var user_temp;
    var users = (typeof (this.props.users.users) !== 'undefined')
      ? this.props.users.users
      : [];
      users.map((user,i) => {
        if(user.identity==user_id)
        {
          user_temp = user
        }
      })
    }
    else{
      var user_temp;
    var users = (typeof (this.props.users.users) !== 'undefined')
      ? this.props.users.users
      : [];
      users.map((user,i) => {
        if(user.identity==user_id)
        {
          user_temp = user
        }
      })
    }
    this.setState({ updateUserPopup: true, currUser: user_temp });
    document.body.classList.add("overlay")

  }
  renderUserUpdate() {
    var searchdata=this.props.searchInput
    return (

      <SliderPopup className='formPopup'>
        <button id="close-popup" className="btn-default" onClick={this.closePopup.bind(this, 'update')}><i class="material-icons">clear</i></button>
        <UpdateUser 
          pageNo={this.state.pageno} 
          roles={this.props.roles} 
          onSubmit={this.handleUserSubmit.bind(this)} 
          currUser={this.state.currUser} 
          users={this.props.users.users} 
          departments={this.props.departments} 
          searchdata={this.props.searchInput} 
          viewUser={this.state.viewUser} 
          regionList={this.props.regionData}>
          </UpdateUser>

      </SliderPopup>

    )
  }

  dropdownPopup(e) {
    e.target.parentNode.parentNode.parentNode.style.zIndex = 11
  }
  userPauseClick(user_id) {
    var searchdata=this.props.searchInput
    var users = (typeof (this.props.users.users) !== 'undefined')
      ? this.props.users.users
      : [];
    var user_temp
    users.map((user,i) => {
      if(user.identity==user_id)
      {
        user_temp = user
      }
    })
    var me = this;
    var pauseText = (user_temp.paused == 1) ? 'unpause' : 'pause';
    if(this.props.searchInput==undefined){
      notie.confirm(
        `Are you sure you want to ${pauseText} ${user_temp.firstname +' '+ user_temp.lastname} user?`,
        "Yes",
        "No",
        function () {
          me.props.userActions.pauseUser(user_temp.identity, user_temp.auth_token,null,me.state.pageno);
        },
        function () {
        }
      )
    }else{
      notie.confirm(
        `Are you sure you want to ${pauseText} ${user_temp.firstname +' '+ user_temp.lastname} user?`,
        "Yes",
        "No",
        function () {
          me.props.userActions.pauseUser(user_temp.identity, user_temp.auth_token,searchdata);


        },
        function () {
        }
      )
    }


  }
  /**
   * @author disha
   * when clicked on view button to see user's detail , it will set flag and store current user details
   * @param {*} user_id
   */
  viewUserDetail(user_id){
    var user_temp;
    var users = (typeof (this.props.users.users) !== 'undefined')
      ? this.props.users.users
      : [];
      users.map((user,i) => {
        if(user.identity==user_id)
        {
          user_temp = user
        }
      })
    document.body.classList.add("overlay")
    this.setState({ updateUserPopup: true,viewUser:true, currUser: user_temp });
  }
  handleTrusted(user_id,) {
    var searchdata=this.props.searchInput
    if(this.props.searchInput==undefined)
    {
      this.props.userActions.trustedUser(user_id,null,this.state.pageno);
    }
    else{
      this.props.userActions.trustedUser(user_id,searchdata);
    }
  }
  submitCallbackOptional(objToPass,onlyPassPermissionField){
    this.props.userActions.updateUser(objToPass, null,null,this.state.pageno,onlyPassPermissionField);
  }
  cancelCallbackOptional(){
      return
  }
  handleSocialPermissionStatus (userData){
    var onlyPassPermissionField = true
    var socialPermissionValue=userData.enable_social_posting == 0 ? true : false
    var objToPass={
      identity:userData.identity,
      enableSocial:socialPermissionValue,
      is_trusted:userData.is_trusted
    }
    if(userData.is_trusted == 1 && objToPass.enableSocial == false){
      notie.confirm(
        'You are about to turn off a trusted user access to social',
        'Continue',
        'Cancel',
        this.submitCallbackOptional.bind(this,objToPass,onlyPassPermissionField),
        this.cancelCallbackOptional.bind(this),
      )
    }else{
      this.props.userActions.updateUser(objToPass, null,null,this.state.pageno,onlyPassPermissionField);
    } 
  }
  memberListing() {
    var members
    var me = this;

    if(me.props.location.state==null || me.props.location.state.seeAllClick==true){
      members = (me.props.users.users)
        ?
        me.props.users.users
        :
        [];
    }else{
      if(me.props.location.state.fromSuggestionClick==true){
        members = (me.props.userData)
      ? me.props.userData
      : [];
      }
    }

    // if(this.props.location.pathname=="/search/user" && this.props.userData!==undefined && this.props.users.users.length==0){
    //   members = (this.props.userData)
    //   ? this.props.userData
    //   : [];
    // }
    // else
    // {
    //   if(this.props.suggestion_selected !== undefined && this.props.suggestion_selected !== null)
    //   {
    //     members = this.props.userData
    //   }
    //   else
    //   {
    //     members = (this.props.users.users)
    //     ?
    //     this.props.users.users
    //     :
    //     [];
    //   }
    // }
    if (members == undefined)
    {
      return (<h3>No users found.</h3>)
    } else {
      return (<UsersList 
        fetchUsersData={this.fetchUsersData.bind(this)}
        handleSocialPermissionStatus={this.handleSocialPermissionStatus.bind(this)} 
        users={members} 
        usersListMeta={this.props.users.usersListMeta} 
        pageNo={this.selectedPageno.bind(this)} 
        handleTrusted={this.handleTrusted.bind(this)}  
        viewUserDetail={this.viewUserDetail.bind(this)} 
        profile={this.props.profile} 
        userUpdateClick={this.userUpdateClick.bind(this)} 
        userPauseClick={this.userPauseClick.bind(this)} 
        dropdownPopup={this.dropdownPopup.bind(this)} 
        location={this.props.location} 
        loading={this.props.users.loading}
      ></UsersList>);
    }
  }
  /**
   * to fetch user list according to the sorting and filter 
   * @author disha
   * @param {*} pageNumber 
   * @param {*} sortingData 
   * @param {*} filterData 
   * @param {*} showNoOfData 
   */
  fetchUsersData(pageNumber, sortingData, filterData, showNoOfData){
    this.props.userActions.fetchUsers(null,pageNumber, sortingData, filterData, showNoOfData)
   }
selectedPageno (pageno,sortingData, filterData, showNoOfData) {
    this.setState(
      {
        pageno: pageno
      },
      () =>{this.fetchUsersData(this.state.pageno, sortingData, filterData, showNoOfData)}
    )
  }
  handleCsvSubmit(e) {
    csvfiledata = e
  }
  /**
   * @author disha
   * add class name on next button
   */
  csvNextpageStrcture() {
    var oInput = document.getElementsByClassName('match-columns-wrapper').childNodes;
    document.getElementById('upload-csv-section').className = 'upload-csv-section';
    document.getElementById('import-users-section').className = 'import-users-section active';
  }
  upload_next_step() {
    var filtered_result={},i=0
    var count=false
    if (csvfiledata !== '' || this.state.filedata !== null) {
      if (csvfiledata.size > 0) {
        //check if file is not blank
        var me = this
        if(this.state.Papaparse){
        this.state.Papaparse.parse(csvfiledata, {
          header: true,
          dynamicTyping: true,
          keepEmptyRows:false,
          skipEmptyLines:true,
          trimHeaders:true,
          complete: function (results) {
            if (results.data.length > 0) {
              for (var i = 0; i <= results.data.length; i++) {
                if (results.data[i] !== undefined && results.data[i] !== null && Object.keys(results.data[i]) !== undefined && Object.keys(results.data[i]) !== null) {
                  for (var key in results.data[i]) {
                    if (results.data[i].hasOwnProperty(key)) {
                      count = false
                      for (var a = 0; a <= results.data.length; a++) {
                        if (results.data[a] !== undefined && results.data[a] !== null) {
                          for (var innerKey in results.data[a]) {
                            if (results.data[a].hasOwnProperty(innerKey)) {
                              if (key == innerKey) {
                                if (results.data[a][innerKey] !== '' && results.data[a][innerKey] !== null) {
                                  count = true
                                }
                                if (count == false) {
                                  delete results.data[a][innerKey];
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                  keyTotalLength.push(Object.keys(results.data[i]).length)
                }
              }
              me.setState({
                filedata: results
              })
              me.csvNextpageStrcture();
            } else {
              notie.alert('error', 'File must have data.', 5)
            }
          }
        })
      }
      } else {
        notie.alert('error', 'File can not be empty.', 5)
      }
    } else {
      notie.alert('warning', 'First, Select a File.', 3)
    }
  }
  upload_back_step() {
    document.getElementById('upload-csv-section').className = 'upload-csv-section active';
    document.getElementById('import-users-section').className = 'import-users-section';
  }
  currDiv(e) {
  }
  selctedColName(id, e) {
    storeColName_save = id
    var index = e.target.selectedIndex
    this.setState({
      firstcolname: e.target[index].text,
      firstcolIndex: e.target[index].id
    })
  }
  /**
   * @author disha
   * @use to edit data when edit button is clicked
   * @param {*} e as edit div id
   */
  editData(e) {
    this.setState({
      editDivid: e
    })
    oldarrayData = allCSVdataArray.filter(function (f) { return f; })   //Create a filter of the original array in the arrayCopy. So that changes to the new array won't affect original array.
    this.removeOldSelectedcsvData(e)
  }
  /**
   * @author disha
   * @use when div has saved value and still it is on edit mode then to remove old data from array
   * @param {*} id which is passed by edit data
   */
  removeOldSelectedcsvData(id) {
    for (var i = 0; i < Object.keys(this.state.mapdata).length; i++) {
      //to check that edit div id is in this.state.mapdata or not and it is in object then take name and find in array to remove
      allCSVdataArray.map((data, i) => {
        if (data.indexOf(this.state.mapdata[id]) == 0) {
          isDataSaved = true
          allCSVdataArray.splice(allCSVdataArray.indexOf(data), 1)
        }
      })
    }
  }
  /**
   *
   * @param {*} e
   * take data from array and convet in object array and pass to api
   */
  sendData(e) {
    var requiredFields = ["First Name", "Email","Role","Department","Region","Office","Business Unit", "Company Role","Gender" ]
    var flag_required=0
    var req_string=''
    if (allCSVdataArray.length > 0) {
      var colname = []
      var csvUser = []
      var csvDataArray = []
      for (var i = 0; i < allCSVdataArray.length; i++) {
        csvDataArray.push(allCSVdataArray[i][0])
      }
      var length = requiredFields.length
      for (var ii = 0; ii < requiredFields.length; ii++) {
        if (!csvDataArray.includes(requiredFields[ii])) {
          flag_required=1
          if(req_string == '')
          {
          req_string = ' ' + requiredFields[ii]
          }
          else
          {
          req_string = req_string + "," + requiredFields[ii]
          }
        }
      }
      if(flag_required==1)
      {
          notie.alert('warning', 'Sorry,You have to select required columns. :' + req_string, 3)
          return false;
      }
      for (var i = 0; i < allCSVdataArray.length; i++) {
        for (var j = 1; j < allCSVdataArray[i].length; j++) {
          if (i === 0) {
            csvUser[j - 1] = {}
          }
          if(allCSVdataArray[i][j]!=='' && allCSVdataArray[i][j]!==undefined )
          {
            csvUser[j - 1][allCSVdataArray[i][0]] = allCSVdataArray[i][j]
          }
        }
      }
      for (var c = 0; c < csvUser.length; c++) {
        if (Object.keys(csvUser[c]).length == 0) {
          csvUser.splice(csvUser.indexOf(csvUser[c]), 1)
        }
        if (csvUser[c] !== undefined && csvUser[c] !== null) {
          for (var k = 0; k < requiredFields.length; k++) {
            if (csvUser[c] !== undefined && csvUser[c] !== null) {
              if (Object.keys(csvUser[c]).indexOf(requiredFields[k]) == -1) {
                csvUser.splice(csvUser.indexOf(csvUser[c]), 1)
                notie.alert('warning', 'You have missing field data which will be removed.', 3)
              }
            }
          }
        }

      }
      var isFirstUserUpload = false;
      if(this.props.users.users.length==1){
        isFirstUserUpload=true;
      }
      this.props.userActions.SendCsvData(csvUser,isFirstUserUpload);
    } else {
      notie.alert('warning', 'Sorry, You have to select required columns.', 3)
    }
  }
  /**
   * @author disha
   * to skip div
   * @param {*} id div id
   * @param {*} btnClick true/false
   * @param {*} e this
   */
  skipbutton(id, btnClick, e) {
    var test = Object.assign({}, this.state.mapdata) //to pass original object data into other object to store its value
    if (this.state.colindex[id] !== undefined) {
      //if div has already saved data and select other field and click on skip
      test[id] = this.props.users.fieldlist[this.state.colindex[id]]
    }

    var newId = 0
    var allColDiv = [];
    if (invalidData == true && btnClick == true) {
      //field name is invalid for data and click on skip button then to pass old data to current object
      this.state.mapdata = Object.assign({}, test)
    }

    allColDiv = document.getElementsByClassName('match-column')
    var totCol = Math.max(...keyTotalLength);

    if (isDataSaved == true && btnClick == true) {
      allCSVdataArray = oldarrayData.filter(function (f) { return f; })
    }
    for (var i = 0; i < allColDiv.length; i++) {
      //to remove active class to next div after skip button is clicked
      if (id == allColDiv[i].id) {
        newId = id + 1
        if (newId < totCol) {
          document.getElementById(newId).classList.add("active");
          document.getElementById(id).classList.remove("active");
          this.setState({
            editDivid: newId
          })
        } else {
          this.setState({
            editDivid: null
          })
          document.getElementById(id).classList.remove("active");
        }
      }
    }
  }
  /**
   * @author disha
   * to save data
   * @param {*} id current div id
   * @param {*} e this
   */
  saveData(id, e) {
    var regularExpression=''
    if (this.state.mapdata[storeColName_save] !== undefined) {
      allCSVdataArray.map(data => {
        if (data.indexOf(this.state.mapdata[storeColName_save]) == 0) {
          //check field name in mapdata in data (array) and find its index in array data and after finding its array to fetch its parent id and remove it
          allCSVdataArray.splice(allCSVdataArray.indexOf(data), 1)
          //allCSVdataArray.splice(this.state.mapdata[storeColName_save],1)
        }
      })
    }

    var Col_AllData = []
    if (this.state.firstcolname) {
      //if select any field name and click on save
      this.state.mapdata[storeColName_save] = this.state.firstcolname //to store key pair value in object
      Colindex[this.state.mapdata[storeColName_save]] = this.state.firstcolname

      var RecentselectedColValue = this.state.mapdata[storeColName_save]

      if(RecentselectedColValue.toLowerCase() == 'role'){
        regularExpression=/^[a-zA-Z -_]+$/
      }else if ( RecentselectedColValue.toLowerCase() == 'first name' || RecentselectedColValue.toLowerCase() == 'last name' || RecentselectedColValue.toLowerCase() == 'city' || RecentselectedColValue.toLowerCase() == 'state or county' || RecentselectedColValue.toLowerCase() == 'country' || RecentselectedColValue.toLowerCase() == 'region'|| RecentselectedColValue.toLowerCase() == 'business unit'){
        regularExpression=/^[a-zA-Z ]+$/
      }else if(RecentselectedColValue.toLowerCase() == 'user interests'){
        regularExpression=/^[a-zA-Z$, -@'']+$/
      }else if(RecentselectedColValue.toLowerCase() == 'enable social'){
        regularExpression=/\b(\w*yes|no\w*)\b/
      }

      if (RecentselectedColValue.toLowerCase() == 'email') {
        //check selected field name is email
        var count=0
        this.state.filedata.data.map((firstcol, fi) => {
          if (!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(firstcol[Object.keys(firstcol)[id]]) && (firstcol[Object.keys(firstcol)[id]]!=='' && firstcol[Object.keys(firstcol)[id]]!==undefined)) {
            //check div data is valid for selected field
            delete this.state.mapdata[id]  //remove selected field name from object
            delete this.state.colindex[id]
            invalidData = true
            Col_AllData.push(undefined)
            notie.alert('warning','Sorry, Your field data is not in text format.', 3)
            count++;
            return false;
          } else {
            if(count==0){
              invalidData = false
              Col_AllData.push(firstcol[Object.keys(firstcol)[id]])
              this.state.colindex[storeColName_save] = this.state.firstcolIndex
              selectedColindex[this.state.colindex[storeColName_save]] = this.state.firstcolIndex
              this.skipbutton(id, false, e)
            }
          }
        })
      } else if (RecentselectedColValue.toLowerCase() == 'first name' || RecentselectedColValue.toLowerCase() == 'last name' || RecentselectedColValue.toLowerCase() == 'city' || RecentselectedColValue.toLowerCase() == 'state or county' || RecentselectedColValue.toLowerCase() == 'country' || RecentselectedColValue.toLowerCase() == 'role' || RecentselectedColValue.toLowerCase() == 'user interests' || RecentselectedColValue.toLowerCase() == 'region'|| RecentselectedColValue.toLowerCase() == 'business unit') {
        var count=0
        this.state.filedata.data.map((firstcol, fi) => {
          if (!/^[a-zA-Z0-9$()_+[\]{}&|\\,.: -]*$/g.test(firstcol[Object.keys(firstcol)[id]]) && firstcol[Object.keys(firstcol)[id]]!=='' && firstcol[Object.keys(firstcol)[id]]!==undefined) {
            delete this.state.mapdata[id]
            delete this.state.colindex[id]
            Col_AllData.push(undefined)
            notie.alert('warning', 'Sorry, Your field data is not in text format.', 3)
            invalidData = true
            count++;
            return false;
          } else {
            if(count==0){
              invalidData = false
              Col_AllData.push(firstcol[Object.keys(firstcol)[id]])
              this.state.colindex[storeColName_save] = this.state.firstcolIndex
              selectedColindex[this.state.colindex[storeColName_save]] = this.state.firstcolIndex
              this.skipbutton(id, false, e)
            }
          }
        })
      } else if (RecentselectedColValue.toLowerCase() == 'department' || RecentselectedColValue.toLowerCase() == 'company role' || RecentselectedColValue.toLowerCase() == 'level' || RecentselectedColValue.toLowerCase() == 'office') {
        var count=0
        this.state.filedata.data.map((firstcol, fi) => {
          if (!/^[a-zA-Z0-9$()_+[\]{}&|\\,.: -]*$/g.test(firstcol[Object.keys(firstcol)[id]]) && firstcol[Object.keys(firstcol)[id]]!=='' && firstcol[Object.keys(firstcol)[id]]!==undefined) {
            delete this.state.mapdata[id]
            delete this.state.colindex[id]
            invalidData = true
            Col_AllData.push(undefined)
            notie.alert('warning', 'Sorry, Your field data is not in text format.', 3)
            count++;
            return true;
          } else {
            if(count ==0){
              invalidData = false
              Col_AllData.push(firstcol[Object.keys(firstcol)[id]])
              this.state.colindex[storeColName_save] = this.state.firstcolIndex
              selectedColindex[this.state.colindex[storeColName_save]] = this.state.firstcolIndex
              this.skipbutton(id, false, e)
            }
          }
        })
      }else if(RecentselectedColValue.toLowerCase()=="gender"){
        var count=0
        this.state.filedata.data.map((firstcol, fi) => {
          if (!/^[a-zA-Z]*$/g.test(firstcol[Object.keys(firstcol)[id]]) && firstcol[Object.keys(firstcol)[id]]!=='' && firstcol[Object.keys(firstcol)[id]]!==undefined && firstcol[Object.keys(firstcol)[id]]!== null) {
              delete this.state.mapdata[id]
              delete this.state.colindex[id]
              invalidData = true
              Col_AllData.push(undefined)
              notie.alert('warning', 'Sorry, Your field data is not in text format.', 3)
              count++;
              return false;
          }else{ 
            if(count ==0){
              invalidData = false
              let genderText = firstcol[Object.keys(firstcol)[id]].toLowerCase() !== undefined && firstcol[Object.keys(firstcol)[id]].toLowerCase() !== null ? firstcol[Object.keys(firstcol)[id]].toLowerCase():''
              if(genderText == "female"){
                Col_AllData.push("F");
              }else if(genderText == "male"){
                Col_AllData.push("M");
              }
              else if(genderText == "other"){
                Col_AllData.push("O");
              }
              this.state.colindex[storeColName_save] = this.state.firstcolIndex
              selectedColindex[this.state.colindex[storeColName_save]] = this.state.firstcolIndex
              this.skipbutton(id, false, e)
            }
          }
        })
      }else if(RecentselectedColValue.toLowerCase()=="birth date"){
        var count=0
        this.state.filedata.data.map((firstcol, fi) => {
           var timeStampData =  this
              .state
              .Moment(firstcol[Object.keys(firstcol)[id]], 'DD/MM/YYYY')
              .format("X")
              if(timeStampData=="Invalid date"){
                delete this.state.mapdata[id]
                delete this.state.colindex[id]
                Col_AllData.push(undefined)
                invalidData = true
                notie.alert("warning","Invalid Date format!",3)
                count++;
                return false;
              }else{
                if(count ==0){
                    invalidData = false
                    Col_AllData.push(timeStampData)
                    this.state.colindex[storeColName_save] = this.state.firstcolIndex
                    selectedColindex[this.state.colindex[storeColName_save]] = this.state.firstcolIndex
                    this.skipbutton(id, false, e)
                }
              }
        })
      }
      else if(RecentselectedColValue.toLowerCase()=="joining date"){
        var count=0
        this.state.filedata.data.map((firstcol, fi) => {
          var timeStampData =  this
             .state
             .Moment(firstcol[Object.keys(firstcol)[id]], 'DD/MM/YYYY')
             .format("X")
             if(timeStampData=="Invalid date"){
               delete this.state.mapdata[id]
               delete this.state.colindex[id]
               Col_AllData.push(undefined)
               invalidData = true
               notie.alert("warning","Invalid Date format!",3)
               count++;
               return false;
             }else{
               if(count ==0){
                  invalidData = false
                  Col_AllData.push(timeStampData)
                  this.state.colindex[storeColName_save] = this.state.firstcolIndex
                  selectedColindex[this.state.colindex[storeColName_save]] = this.state.firstcolIndex
                  this.skipbutton(id, false, e)
               }
             }
       })
      }else if (RecentselectedColValue.toLowerCase() == 'enable social') {
        var count=0
        this.state.filedata.data.map((firstcol, fi) => {
          if (!/\b(\w*yes|no\w*| )\b/gi.test(firstcol[Object.keys(firstcol)[id]]) && firstcol[Object.keys(firstcol)[id]]!=='' && firstcol[Object.keys(firstcol)[id]]!==undefined && firstcol[Object.keys(firstcol)[id]]!==null) {
            delete this.state.mapdata[id]
            delete this.state.colindex[id]
            Col_AllData.push(undefined)
            notie.alert('warning', 'Sorry, Your field data must be Yes or No.', 3)
            invalidData = true
            count++;
            return false;
          } else {
            if(count==0){
              invalidData = false
              Col_AllData.push(firstcol[Object.keys(firstcol)[id]])
              this.state.colindex[storeColName_save] = this.state.firstcolIndex
              selectedColindex[this.state.colindex[storeColName_save]] = this.state.firstcolIndex
              this.skipbutton(id, false, e)
            }
          }
        })
      }
      if (invalidData !== true) {
        //if condtion is true then it will not allow to enter data in array
        Col_AllData.splice(0, 0, this.state.firstcolname)   //to add firstcolname into first index of array
        allCSVdataArray.push(Col_AllData)
        this.setState({
          firstcolname: ''
        })
        this.setState({
          firstcolIndex: ''
        })
      }
    }
  }
  renderFieldBoxes(){
    var maxFieldLength=0
    var boxes=[]
    maxFieldLength=Math.max(...keyTotalLength);
    for(var i=0;i<maxFieldLength;i++){
      boxes.push(
        <div className={`match-column  ${this.state.editDivid == i ? 'active' : ''}`} id={i} key={i} >
          <div className='match-column-inner'>
            <div className='import-top-unmatch-block'>
              {(this.state.editDivid !== null && this.state.editDivid == i) || this.state.mapdata[i] ?
                <div className='form-row'>
                  <label className='importMatchTitle'>{this.state.mapdata[i] ? this.state.mapdata[i] : 'Unnamed'}</label>
                  {this.state.editDivid !== i ?
                    <div className='selectedColumn'>
                      {this.state.mapdata[i]}
                    </div>
                    :
                    <div className='select-wrapper'>
                      <select
                        onChange={this.selctedColName.bind(this, i)}
                      > <option>Make a selection</option>
                        {this.props.users.fieldlist.map((allfields, i) => (
                          Object.values(this.state.colindex).find(obj => obj== i)
                            ?
                            <option value={allfields} key={i} id={i} disabled>
                              {allfields}
                            </option> :
                            <option value={allfields} key={i} id={i}>
                              {allfields}
                            </option>
                        ))}
                      </select>
                    </div>
                  }
                  
                </div> : <div className='form-row'><div className='importMatchTitle'>Unnamed</div>
                  <div className='import-unmatch-text'>(Unmatched column)</div></div>}
              <div className='import-unmatch-links-wrapper'>
                {this.state.editDivid !== null && this.state.editDivid == i ?
                  <div className='import-users-button-wrapper clearfix'>
                    <button className='btn' onClick={this.saveData.bind(this, i)}>Save <i className='material-icons'>keyboard_arrow_right</i></button>
                    <span className='unmatch-link-seprator'>|</span>
                    <div> <a onClick={this.skipbutton.bind(this, i, true)} >Skip</a> </div>
                  </div> :
                  <div>
                    <a onClick={this.editData.bind(this, i)}>Edit</a>
                  </div>}

              </div>
            </div>
            <div className='import-user-detail-wrapper'>
              {this.state.filedata.data.slice(0, 5).map((firstcol, fi) => {
                return (<div className='import-user-detail-item' key={fi}>{firstcol[Object.keys(firstcol)[i]]}</div>)
              })}
            </div>
          </div>
        </div>
      )
    }
    return boxes
  }
  renderUploadCsvpopup() {
    return (
      <SliderPopup className='formPopup'>
        <button id="close-popup" className="btn-default" onClick={this.closePopup.bind(this, 'upload_csv')}><i class="material-icons">clear</i></button>

        {/* Upload csv starts */}
        <div id='upload-csv-section' className='upload-csv-section active'>
          <header class="heading"><h3>Import from CSV file</h3></header>
          <PopupWrapper className='withBottomBtns'>
            <div className='upload-csv-section-inner'>
              <CreateUserDropzone Filedata={this.handleCsvSubmit.bind(this)} />
              <div className='userSampleCsv'><a href="/sample_upload_file.csv" download><i className='material-icons'>insert_drive_file</i>Please download the sample CSV file</a></div>
              <div className='user-drop-note'>Acceptable file types: CSV</div>
              <div className='userNoteTitle'>Notes</div>
              <div className='user-drop-instruction'>
                <ul>
                  <li><i className='material-icons'>keyboard_arrow_right</i>Duplicate addresses will be removed.</li>
                  <li><i className='material-icons'>keyboard_arrow_right</i>You will be able to assign and manage data columns in the next step.</li>
                  <li><i className='material-icons'>keyboard_arrow_right</i>We do not send a confirmation email once your data has been uploaded.</li>
                  {//Please note that we do not send a confirmation email once your data has been uploaded.
                  }
                </ul>
              </div>
            </div>
          </PopupWrapper>
          <div className='clearfix action-buttons'>
            <a title="Next" onClick={this.upload_next_step.bind(this)} className='btn btn-theme right upload-next-btn'>Next <i className='material-icons'>keyboard_arrow_right</i></a>
          </div>
        </div>
        <div id="import-users-section" className='import-users-section'>
          <header class="heading"><h3>Import contacts</h3></header>
          <PopupWrapper className='withBottomBtns' selector='import-users-contact'>
            <div className='import-users-section-inner'>
              <div className='import-user-instruction'>Now let's match the columns.</div>

              <div className='match-columns-wrapper clearfix'>
                {this.state.filedata ?
                  this.state.filedata.data.length > 0 ?
                  this.renderFieldBoxes() : '' : ''}
              </div>
            </div>
          </PopupWrapper>
          <div className='clearfix action-buttons'>
            <a title="Back" onClick={this.upload_back_step.bind(this)} className='btn btn-default upload-back-btn'><i className='material-icons'>keyboard_arrow_left</i> Back</a>
            <a title="Next" className='btn btn-theme right upload-next-btn' onClick={this.sendData.bind(this)}>Next <i className='material-icons'>keyboard_arrow_right</i></a>
          </div>

        </div>
        {/* Upload csv ends */}

      </SliderPopup>
    )
  }

  render() {
    if(this.props.location.pathname=="/search/user"){
      NavBar=SearchNav
      title="search"
    }
    else{
      NavBar=SettingsNav
      title="Users"
    }
    return (
      <section id="Users" className="settings">
        {this.state.newUserPopup ? this.renderUserCreation() : <div></div>}
        {this.state.updateUserPopup ? this.renderUserUpdate() : <div></div>}
        {this.state.JobPostsPopup ? this.renderUploadCsvpopup() : ''}
        {!this.props.location.pathname.includes("search")?
        <Header
          title={title}
          nav={NavBar}
          add_new_subpopup="Users"
          location={this.props.location}
          searchInput={this.props.searchInput}
        /> :''}

        <div className='main-container'>
          <span id="add_new_button" className="btn btn-default add-new-button" onClick={this.handleAddUser.bind(this)} />
          <span id="upload_csv_btn" onClick={this.openUpload_csv.bind(this)} />
          <div id="content">

            <div className="page campaign-container">
              <div className="full-container">
                <div className="membersListing">
                  {this.memberListing()
                  }
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

function mapStateToProps(state) {
  return {
    onboarding: state.settings.onboarding,
    profile: state.profile.profile,
    auth: state.auth,
    users: state.users,
    departments: state.departments.list,
    roles: state.users.roles,
    searchData: state.searchData,
    regionData : state.settings.companyHierarchyData.regions
  }
}

function mapDispatchToProps(dispatch) {
  return {
    settingsActions: bindActionCreators(settingsActions, dispatch),
    loginActions: bindActionCreators(loginActions, dispatch),
    userActions: bindActionCreators(userActions, dispatch),
    departmentActions: bindActionCreators(departmentActions, dispatch),
    searchActions: bindActionCreators(searchActions,dispatch),
    companyInfoAction : bindActionCreators(companyInfo,dispatch)
  }
}

let connection = connect(mapStateToProps, mapDispatchToProps);

module.exports = connection(UsersSettings);
