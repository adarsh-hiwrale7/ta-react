import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import * as userActions from "../../../actions/userActions"

import notie from "notie/dist/notie.js"
import PreLoader from '../../PreLoader'
import PopupWrapper from '../../PopupWrapper';
import UsersSettingsForm from './UsersSettingsForm';
import * as searchActions from "../../../actions/searchActions"
import UsersViewDetails from './UsersViewDetails'
import moment from '../../Chunks/ChunkMoment'


class UpdateUser extends React.Component {

  constructor(props) {
    super(props);
    this.state={
      Moment:null
    }

  }

  componentWillMount(){
    moment().then(moment => {
          this.setState({Moment: moment })
        })
  }

  componentDidMount() {

  }
  submitCallbackOptional (values,userAuthToken,searchdata){
    if(searchdata==undefined){
      this.props.userActions.updateUser(values, userAuthToken,null,this.props.pageNo);
    }else{
      this.props.userActions.updateUser(values, userAuthToken,searchdata);
    }
  }
  cancelCallbackOptional (){
    return
  }
  handleSubmit(values) {
    var searchdata=this.props.searchdata
    var userAuthToken = (typeof (this.props.currUser) != 'undefined') ? this.props.currUser.auth_token : null;
    var is_trusted = (typeof (this.props.currUser) != 'undefined') ? this.props.currUser.is_trusted : null;
    var date=this.state.Moment.unix(values.birth_date).format('DD/MM/YYYY')
    var today_date = new Date();
    var birth_year=this.state.Moment(date,'DD/MM/YYYY').format("YYYY")
    var today_year = today_date.getFullYear();
    var age = today_year - birth_year;
      if(age <16){
        notie.alert('warning','Age cannot be below 16 years ',3)
        return
      }
      if(is_trusted == 1 && values.enableSocial !== true){
        notie.confirm(
          'You are about to turn off a trusted user access to social',
          'Continue',
          'Cancel',
          this.submitCallbackOptional.bind(this,values,userAuthToken,searchdata),
          this.cancelCallbackOptional.bind(this),
        )
      }else{
        if(searchdata==undefined){
          this.props.userActions.updateUser(values, userAuthToken,null,this.props.pageNo);
        }else{
          this.props.userActions.updateUser(values, userAuthToken,searchdata);
        }
      }
    
  }

  render() {
    const { loading } = this.props.users;
    var me = this;
    return (
      <section className="dept-creation-page" >
        {(loading ? <PreLoader className="Page" /> : <div></div>)}

        <div id="dept-creation">
          <header className="heading">
            <h3>{this.props.viewUser==true?'User Detail':'Update User'}</h3>
          </header>
          <PopupWrapper>
            {/* if view button is clicked then to open popup */}
            {this.props.viewUser == true ?
              <UsersViewDetails currUser={this.props.currUser} />
              : <UsersSettingsForm roles={this.props.roles} onSubmit={this.handleSubmit.bind(this)} users={this.props.users} departments={this.props.departments} currUser={this.props.currUser} Moment={this.state.Moment} regionList={this.props.regionList}></UsersSettingsForm>}
          </PopupWrapper>
        </div>
      </section>

    );
  }

}


function mapStateToProps(state) {
  return {
  }
}

function mapDispatchToProps(dispatch) {
  return {
    userActions: bindActionCreators(userActions, dispatch),
    searchActions: bindActionCreators(searchActions,dispatch)
  }
}

let connection = connect(mapStateToProps, mapDispatchToProps);

module.exports = connection(UpdateUser);
