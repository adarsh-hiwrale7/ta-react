
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Field, reduxForm } from 'redux-form'
import { renderField } from '../../Helpers/ReduxForm/RenderField'
import reactDatepickerChunk from '../../Chunks/ChunkDatePicker' // datepicker chunk
import ReactDatePicker from '../../Helpers/ReduxForm/ReactDatePicker' // react datepicker field
import * as countryActions from '../../../actions/countryActions'
import * as settingsActions from "../../../actions/settingsActions";
import * as  companyInfoActions from '../../../actions/settings/companyInfo';
import PreLoader from '../../PreLoader'
var loginId;
const required = value => (value ? undefined : 'Required')
const mobileNumberValidation = value => (value !=undefined ? value.length > 15 ? 'Please enter valid mobile number' : '':'')

const name = value => (value && /^[a-zA-Z ]*$/g.test(value) ?undefined: 'Must be a string');
const validEmail = value =>
  (value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
    ? 'Invalid email address'
    : undefined)

class UsersSettingsForm extends React.Component {
  constructor(props) {
    super(props)
    this.state = { isEditing: true ,
      ReactDatePicker:null,
    }
  }

  componentWillMount(){
    reactDatepickerChunk()
      .then(reactDatepicker => {
          this.setState({ ReactDatePicker: reactDatepicker })
    })
}
  componentDidMount() {
    loginId = this.props.profile.profile.identity;
    this.props.settingsActions.getCountycode();
    if(this.props.currUser!==undefined){
      var userData = this.props.currUser;
      if(userData.office!==undefined && userData.office.data!==undefined){
    if(userData.office.data.region_id!==undefined){
      this.props.countryActions.fetchAllCountry(userData.office.data.region_id,true);
    }
    if(userData.office.data.country_id!==undefined){
      this.props.companyInfoActions.showMasterData('office',userData.office.data.country_id,true);
    }
    if(userData.office.data.identity!==undefined){
      var id=userData.office.data.identity;
      this.props.companyInfoActions.showMasterData('department',id,true);
    }
  }
    if(userData.Department!==undefined && userData.Department.data!==undefined){
      var id= userData.Department.data.identity;
      this.props.companyInfoActions.showMasterData('business',id,true);
    }
    if(userData.BusinessUnit!==undefined && userData.BusinessUnit.data!==undefined){
      var id= userData.BusinessUnit.data.identity;
      this.props.companyInfoActions.showMasterData('role',id,true);
    }
    }
      this.handleInitialize();
  }

  preventChange(e) {
  }

  selectRegion(e){
    var id = e.target.value;
    this.props.countryActions.fetchAllCountry(id,true);
    this.props.companyInfoActions.resetMasterDataAddUser("office");
    this.props.change('country',null)
    this.props.change('office',null)
  }
  selectCountry(e){
    var id = e.target.value;
    this.props.companyInfoActions.showMasterData('office',id,true);
    this.props.companyInfoActions.resetMasterDataAddUser("department");
    this.props.companyInfoActions.resetMasterDataAddUser("office");
    this.props.change('office',null)
  }
  selectCountryCode(e){
    var id = e.target.value;
  }
  selectOfficeLocation(e){
    var id = e.target.value;
    this.props.companyInfoActions.showMasterData('department',id,true);
    this.props.companyInfoActions.resetMasterDataAddUser("business");
    this.props.change('department',null)
  }
  selectDepartment(e){
    var id = e.target.value;
    this.props.companyInfoActions.showMasterData('business',id,true);
    this.props.companyInfoActions.resetMasterDataAddUser("role");
  }
  selectBusinessUnit(e){
    var id = e.target.value;
    this.props.companyInfoActions.showMasterData('role',id,true);
  }

  handleInitialize(nextProps = null) {
    let initData = {}
    if (typeof this.props.currUser !== 'undefined') {
      const user = this.props.currUser
      initData = {
        identity: user.identity,
        name: user.firstname,
        surname: user.lastname,
        email_address: user.email,
        visibly_role: user.role.data[0].role_id,
        gender : user.gender,
        birth_date : user.birth_date,
        start_date : user.joining_date,
        level: user.level,
      }
    
    if(user.office!==undefined){
      if(user.office.data.region_id!==undefined){
        initData["region"] = user.office.data.region_id;
      }
      if(user.office.data.identity!==undefined){
        initData["office"] = user.office.data.identity;
      }
    }

    if(user.country!==undefined && user.country.data!==undefined){
      initData["country"]= user.country.data.identity;
    }

    if(user.Department!==undefined){
      initData["department"] = user.Department.data.identity
    }
    if(user.BusinessUnit!==undefined){
      initData["business_unit"] = user.BusinessUnit.data.identity
    }
    if(user.CompanyRole!==undefined){
      initData["company_role"] = user.CompanyRole.data.identity
    }
    if(user.content_preference!==undefined){
      if(user.content_preference.SMS=='1'){
        initData["sms"]=true;
      }
      if(user.content_preference.email=='1'){
        initData["email"]=true;
      }
      if(user.content_preference.in_app=='1'){
        initData["inapp"]=true;
      }
    }
    if(user.enable_social_posting!==undefined){
      if(user.enable_social_posting == 1){
        initData["enableSocial"]='enableSocialYes';
      }
      if(user.enable_social_posting == 0){
        initData["enableSocial"]='enableSocialNo';
      }
    }
    if(user.board!==undefined){
      if(user.board == 1){
        initData["board"]='boardYes';
      }
      if(user.board == 0){
        initData["board"]='boardNo';
      }
    }
  }
    // initData['domain'] = this.props.profile.profile.email.substring(
    //   this.props.profile.profile.email.lastIndexOf('@')
    // )
    if (typeof this.props.currUser == 'undefined') {
      initData["email_address"] = null;
      initData["enableSocial"]='enableSocialYes';
      initData["board"]='boardNo';
    }
    this.props.initialize(initData)
  }

  render() {
    const {
      handleSubmit,
      departments,
      pristine,
      reset,
      submitting
    } = this.props
    var regionList = this.props.regionList;
    var countryLoader=this.props.allcountry? this.props.allcountry.loadCountry :false;
    var companyMasterLoader=this.props.companyMasterData?this.props.companyMasterData.companyMasterDataLoader:false;
    let roleName='';
   if(localStorage.getItem('roles') !== null){
    roleName = JSON.parse(localStorage.getItem('roles'))[0]['role_name'];
  }
    return (
      <form onSubmit={handleSubmit} role='form'>
      <div className="user-popup-insetting">
      {(countryLoader || companyMasterLoader? <PreLoader  className = "Page"/> : <div></div>)}
        <div className='section'>

          <div className='row'>
            <div className='col-one-of-two form-row'>
              <Field
                placeholder='First Name'
                label='First Name'
                component={renderField}
                type='text'
                name='name'
                validate={[required,name]}
              />
            </div>
            <div className='col-one-of-two last'>
              <Field
                placeholder='Last Name'
                label='Last Name'
                component={renderField}
                type='text'
                name='surname'
                validate={[required,name]}
              />
            </div>

          </div>
          <div className="radiobtnFeed clearfix">
              <span className="radiobtnFeed-label">Gender</span>
                <label className="customradio">Female
                    <Field
                      component='input'
                      type='radio'
                      name='gender'
                      id='female'
                      value='F'
                    />
                    <span className="checkmark"></span>
                  </label>
                  <label className="customradio">Male
                    <Field
                      component='input'
                      type='radio'
                      name='gender'
                      id='male'
                      value='M'
                    />
                    <span className="checkmark"></span>
                  </label>
                  <label className="customradio">Other
                    <Field
                      component='input'
                      type='radio'
                      name='gender'
                      id='other'
                      value='O'
                    />
                    <span className="checkmark"></span>
                  </label>
            </div>
            {this.state.ReactDatePicker && this.props.Moment ?
            <Field
                name={`birth_date`}
                component={props => {
                  return (
                    <ReactDatePicker
                    {...props}
                    showYearDropdown
                    scrollableYearDropdown
                    dateFormatCalendar="MMMM"
                    label='Birthdate'
                    callFrom='Add_user'
                    placeholderText="Add Birthdate"
                    reactDatePicker={this.state.ReactDatePicker}
                    moment={this.props.Moment}
                    dateFormat="DD/MM/YYYY"
                      />
                  )
                }}
                />:''}
            <div className="select-wrapper form-row">
             <div className="countrycode-dropdown">
              <Field
                placeholder='Select Country Code'
                component={renderField}
                label='Mobile No.'
                type='select'
                name='mobile_country_code'
                onChange={this.selectCountryCode.bind(this)}
               >
                <option value=''>Select Country Code</option>
                {this.props.settings.countrycode!= undefined ? Object.keys(this.props.settings.countrycode).length> 0?this.props.settings.countrycode.map(Countrycode => (
                  <option value={Countrycode.idc} key={Countrycode.idc}>
                    {Countrycode.idc}
                  </option>
                )):"":""} 
              </Field>
              </div>
              <div className="mobileNo-wrapper">
              <Field
                placeholder=''
                component={renderField}
                type='number'
                name='mobile_no'
                validate={[mobileNumberValidation]}
              />
              </div> 
            </div>
            <div className="select-wrapper form-row">
              <Field
                placeholder='Select Region'
                label='Region'
                component={renderField}
                type='select'
                name='region'
                validate={[required]}
                onChange={this.selectRegion.bind(this)}
              >
                <option value=''>Select Region</option>
                {regionList.length > 0?regionList.map(region => (
                  <option value={region.identity} key={region.identity}>
                    {region.name}
                  </option>
                )):""}
              </Field>
            </div>
            <div className="select-wrapper form-row">
              <Field
                placeholder='Select Country'
                label='Country'
                component={renderField}
                type='select'
                name='country'
                validate={[required]}
                onChange={this.selectCountry.bind(this)}
              >
                <option value=''>Select Country</option>
                {Object.keys(this.props.allcountry.regioncountry).length> 0?this.props.allcountry.regioncountry.map(Country => (
                  <option value={Country.identity} key={Country.identity}>
                    {Country.title}
                  </option>
                )):""}
              </Field>
            </div>
            <div className="select-wrapper form-row">
              <Field
                placeholder='Select Office Location'
                label='Office Location'
                component={renderField}
                type='select'
                name='office'
                validate={[required]}
                onChange ={this.selectOfficeLocation.bind(this)}
              >
                <option value=''>Select Office Location</option>
                {this.props.companyMasterData.office.length > 0?this.props.companyMasterData.office.map(Office => (
                  <option value={Office.identity} key={Office.identity}>
                    {Office.name}
                  </option>
                )):""}
              </Field>
            </div>
            <div className="select-wrapper form-row">
              <Field
                placeholder='Select Department'
                label='Department'
                component={renderField}
                type='select'
                name='department'
                validate={[required]}
                onChange={this.selectDepartment.bind(this)}
              >
                <option value=''>Select Department</option>
                {this.props.companyMasterData.department.length > 0?this.props.companyMasterData.department.map(dept => (
                  <option value={dept.identity} key={dept.identity}>
                    {dept.name}
                  </option>
                )):""}
              </Field>
            </div>

            <div className="select-wrapper form-row">
              <Field
                placeholder='Select Business Unit'
                label='Business Unit'
                component={renderField}
                type='select'
                name='business_unit'
                validate={[required]}
                onChange={this.selectBusinessUnit.bind(this)}
              >
                <option value=''>Select Business Unit</option>
                {this.props.companyMasterData.business.length > 0?this.props.companyMasterData.business.map(business => (
                  <option value={business.identity} key={business.identity}>
                    {business.name}
                  </option>
                )):""}
              </Field>
            </div>

            <div className="select-wrapper form-row">
              <Field
                placeholder='Select Company Role'
                label='Company Role'
                component={renderField}
                type='select'
                name='company_role'
                validate={[required]}
              >
                <option value=''>Select Company Role</option>
                {this.props.companyMasterData.role.length > 0?this.props.companyMasterData.role.map(companyRole => (
                  <option value={companyRole.identity} key={companyRole.identity}>
                    {companyRole.name}
                  </option>
                )):""}
              </Field>
            </div>

          <Field
            placeholder='Email'
            label='Email'
            component={renderField}
            type='text'
            name="email_address"
            disabled={
              typeof this.props.currUser !== 'undefined'
            }
            validate={[required]}
          />

            {this.state.ReactDatePicker && this.props.Moment ?
            <Field
                name={`start_date`}
                component={props => {
                  return (
                    <ReactDatePicker
                    {...props}
                    showYearDropdown
                    scrollableYearDropdown
                    dateFormatCalendar="MMMM"
                    label='Joining Date'
                    callFrom='Add_user'
                    placeholderText="Add Startdate with Company"
                    reactDatePicker={this.state.ReactDatePicker}
                    moment={this.props.Moment}
                    dateFormat="DD/MM/YYYY"
                      />
                  )
                }}
                />:''}

          <div className="select-wrapper form-row">
            <Field
              placeholder=''
              label='Visibly Role'
              component={renderField}
              type='select'
              name='visibly_role'
              validate={[required]}
              disabled={
                typeof this.props.currUser !== 'undefined' && this.props.currUser.identity ==  loginId
              }
            >
              <option value='' disabled>Select User Role</option>
              {this.props.roles.map((role, j) => (
                <option value={role.role_id} key={j}>
                  {role.role_name}
                </option>
              ))}
            </Field>
          </div>
         
            <div className="select-wrapper form-row">
              <Field
                placeholder='Select Level'
                label='level'
                component={renderField}
                type='select'
                name='level'
              >
                <option value='Select Level'>Select Level</option>
                <option value ='employee'> employee</option>
                <option value ='manager'> manager</option>
                <option value ='Senior manager'> Senior manager</option>
                <option value ='board'> board </option>
              </Field>
            </div>

            <div className="radiobtnFeed clearfix">
              <span className="radiobtnFeed-label">Content Preference</span>
                <label className="customradio">in-app
                    <Field
                      component='input'
                      type='checkbox'
                      name='inapp'
                      value='in-app'
                    />
                    <span className="checkmark"></span>
                  </label>
                  <label className="customradio">Email
                    <Field
                      component='input'
                      type='checkbox'
                      name='email'
                      value='Email'
                    />
                    <span className="checkmark"></span>
                  </label>
                  <label className="customradio">SMS
                    <Field
                      component='input'
                      type='checkbox'
                      name='sms'
                      value='SMS'
                    />
                    <span className="checkmark"></span>
                  </label>
            </div>
            {(this.props.currUser !== undefined && this.props.currUser.role.data[0].role_name!=='super-admin' && roleName == 'super-admin' && this.props.currUser.paused == 0) || (this.props.currUser == undefined && roleName == 'super-admin')?
            <div className="radiobtnFeed clearfix">
              <span className="radiobtnFeed-label">Enable social</span>
                <label className="customradio">Yes
                    <Field
                      component='input'
                      type='radio'
                      name='enableSocial'
                      value='enableSocialYes'
                    />
                    <span className="checkmark"></span>
                  </label>
                  <label className="customradio">No
                    <Field
                      component='input'
                      type='radio'
                      name='enableSocial'
                      value='enableSocialNo'
                    />
                    <span className="checkmark"></span>
                  </label>
            </div>
            :''}
            <div className="radiobtnFeed clearfix">
              <span className="radiobtnFeed-label">Board </span>
                <label className="customradio">Yes
                    <Field
                      component='input'
                      type='radio'
                      name='board'
                      value='boardYes'
                    />
                    <span className="checkmark"></span>
                  </label>
                  <label className="customradio">No
                    <Field
                      component='input'
                      type='radio'
                      name='board'
                      value='boardNo'
                    />
                    <span className="checkmark"></span>
                  </label>
            </div>
         
          <div className='clearfix'>
            <button type='submit' className='btn btn-primary btn-login right' disabled={pristine || submitting}>
              Submit
            </button>
          </div>
        </div>
        </div>
      </form>
    )
  }
}

function mapStateToProps(state, ownProps) {
  return {
    profile: state.profile,
    settings:state.settings,
    allcountry : state.allcountry,
    companyMasterData : state.settings.companyMasterData,
  }
}

function mapDispatchToProps(dispatch) {
  return {
    countryActions : bindActionCreators(countryActions,dispatch),
    settingsActions : bindActionCreators(settingsActions,dispatch),
    companyInfoActions: bindActionCreators(companyInfoActions,dispatch)
  }
}
let connection = connect(mapStateToProps, mapDispatchToProps)

var reduxConnectedComponent = connection(UsersSettingsForm)

export default reduxForm({
  form: 'usersSettingsForm' // a unique identifier for this form
})(reduxConnectedComponent)
