import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import * as userActions from "../../../actions/userActions"
import PreLoader from '../../PreLoader'
import PopupWrapper from '../../PopupWrapper'
import UsersSettingsForm from './UsersSettingsForm';
import moment from '../../Chunks/ChunkMoment'
import notie from 'notie/dist/notie.js'

class CreateUser extends React.Component {

  constructor(props) {
    super(props);
    this.state={
      Moment:null
    }

  }

  componentDidMount() {

  }

  componentWillMount(){
    moment().then(moment => {
          this.setState({Moment: moment })
        })
}

  handleSubmit(values) {
      if (values.gender == undefined){
        notie.alert('warning','Please select gender',3)
        return
      }
      var date=this.state.Moment.unix(values.birth_date).format('DD/MM/YYYY')
      var today_date = new Date();
      var birth_year=this.state.Moment(date,'DD/MM/YYYY').format("YYYY")
      var today_year = today_date.getFullYear();
      var age = today_year - birth_year;
        if(age <16){
          notie.alert('warning','Age cannot be below 16 years ',3)
          return
        }
        var isFirstUserUpload = false;
        if(this.props.users.users.length==1){
          isFirstUserUpload = true;
        }
    var searchInput=null;
    if(this.props.searchInput==undefined){
      searchInput=null
    }else{
      searchInput=this.props.searchInput
    }
    this.props.userActions.saveUser(values,searchInput,isFirstUserUpload);
  }
  render() {

    const { loading } = this.props.users;
    var countryLoader=this.props.allcountry? this.props.allcountry.loadCountry :false;
    var me = this;
    return (
      <section className="dept-creation-page" >

        {(loading || countryLoader? <PreLoader  className = "Page"/> : <div></div>)}

        <div id="user-creation">

          <header className="heading">
            <h3>New User</h3>
          </header>
          <PopupWrapper>
          <UsersSettingsForm roles={this.props.roles} onSubmit={this.handleSubmit.bind(this)} departments={this.props.departments} searchInput={this.props.searchInput} Moment={this.state.Moment} regionList={this.props.regionList}></UsersSettingsForm>
          </PopupWrapper>
        </div>

      </section>

    );
  }

}


function mapStateToProps(state) {
  return {
    users: state.users
  }
}

function mapDispatchToProps(dispatch) {
  return {
    userActions: bindActionCreators(userActions, dispatch)
  }
}

let connection = connect(mapStateToProps, mapDispatchToProps);

module.exports = connection(CreateUser);
