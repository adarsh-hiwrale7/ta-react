import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import ReactCSSTransitionGroup from 'react/lib/ReactCSSTransitionGroup';
import Header from '../../Header/Header';
import SettingsNav from '../SettingsNav';
import PointForm from './PointForm'
import * as pointActions from "../../../actions/settings/pointAction";
var restrictAPI=false;
class Points extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      show_menu: true
    }
  }
  handlePointSubmit (values) {
    let points = (typeof this.props.points !== 'undefined')
    ?  this.props.points
    : null;
    var objToSend={}
      for (var i = 0; i < points.points.length; i++) {
        for (var k in values) {
            if(points.points[i].point_action == k){
              if(points.points[i].point !==values[k] ){
                var objToAdd={
                  [`${k}_identity`]:points.points[i].identity,
                  [`${k}`]:values[k]
                }
                Object.assign(objToSend,objToAdd)
              }
            }
          }
        }
    this.props.pointActions.pointSave(objToSend);
  }

  componentDidMount() {
    if(restrictAPI !==true){
      this
      .props
      .pointActions
      .fetchPoint();
      restrictAPI=true;
      setTimeout(()=>{
        restrictAPI=false
      },5000)
    }
  }

  render() {

    let points = (typeof this.props.points !== 'undefined')
    ?  this.props.points
    : null;
    
    return (
      <section id="points" className="settings">

        <Header title="Points" nav={SettingsNav} location ={this.props.location}></Header>
        <PointForm onSubmit={this.handlePointSubmit.bind(this)} points={points}/>
      </section>
    );
  }
  }

function mapStateToProps (state) {
  return {
    points: state.points,
  }
}

function mapDispatchToProps (dispatch) {
  return {
    pointActions: bindActionCreators(pointActions, dispatch),
  }
}

let connection = connect(mapStateToProps, mapDispatchToProps)

module.exports = connection(Points)