import { Field, reduxForm } from 'redux-form';
import { connect } from 'react-redux';
import { renderField } from '../../Helpers/ReduxForm/RenderField';
import * as pointActions from "../../../actions/settings/pointAction";
import { bindActionCreators } from 'redux';
import notie from "notie/dist/notie.js"
import PreLoader from '../../PreLoader';
var submitType=false
  
class PointForm extends React.Component {
  constructor() {
    super();
  }

  // AllowNumbersOnly function allows  only numeric input in the field
  AllowNumbersOnly(event){
    var i;
    for(i=0;i<=event.target.value.length;i++)
    {
      var code = event.target.value.charCodeAt(i);
      if (code > 31 && (code < 48 || code > 57)) {
       event.preventDefault();
      }
    }
  }

  componentDidMount() {
    submitType=false
    this.handleInitialize(this.props.points.points);
    let postWidget = document.getElementById('point-widget');
    var pointInputs = postWidget.getElementsByTagName('input');
    var i;
    for (i = 0; i < pointInputs.length; i++) { 
      pointInputs[i].setAttribute('maxlength',2)
  }
    
  }
  componentWillReceiveProps(nextProps) {
    if (JSON.stringify(nextProps.points.points) !== JSON.stringify(this.props.points.points)) {
      this.handleInitialize(nextProps.points.points)
    }
  }
  handleInitialize(points) {
    const initData = {
      "internal_video_post_approved": "",
      "internal_video_post_approved_identity": "",
      "internal_image_post_approved": "",
      "internal_image_post_approved_identity": "",
      "internal_text_post_approved": "",
      "internal_text_post_approved_identity": "",
      "external_video_post_approved": "",
      "external_video_post_approved_identity": "",
      "external_image_post_approved": "",
      "external_image_post_approved_identity": "",
      "external_text_post_approved": "",
      "external_text_post_approved_identity": "",
      "image_asset_approved": "",
      "image_asset_approved_identity": "",
      "other_asset_approved":'',
      "other_asset_approved_identity":'',
      "video_asset_approved": "",
      "video_asset_approved_identity": "",
      "internal_post_shared_owner": "",
      "internal_post_shared_owner_identity": "",
      "internal_post_shared": "",
      "internal_post_shared_identity": "",
      "internal_post_liked": "",
      "internal_post_liked_identity": "",
      "internal_post_comment": "",
      "internal_post_comment_identity": "",
      "external_post_shared_owner": "",
      "external_post_shared_owner_identity": "",
      "external_post_shared": "",
      "external_post_shared_identity": "",
      "external_post_liked": "",
      "external_post_liked_identity": "",
      "external_post_comment": "",
      "external_post_comment_identity": "",
      "share_job":"",
      "share_curated_article":"",
      "share_job_identity":"",
      "share_curated_article_identity":""
    };
    if (points !== null) {
      for (var i = 0; i < points.length; i++) {
        for (var k in points[i]) {
          if (initData.hasOwnProperty(points[i][k])) {
            initData[points[i][k]] = points[i]["point"]
            initData[points[i][k] + "_identity"] = points[i]["identity"];
          }

        }
      }

    }
    this
      .props
      .initialize(initData)
  }

  
  selectResetType(e){
    if(e.target.value==''){
      submitType=false
    }else{
      submitType=true
    }
    this.setState({
      selectedTime:e.target.value
    })
  }
  handleResetLeaderboard(e){
    var me=this
    notie.confirm(`Are you sure you want to reset the leaderboard?`,'Yes','No',function () {
        submitType=false
        me.props.pointActions.resetLeaderboard(me.state.selectedTime)
      },
      function () { }
    )
  }
  render() {
    const { handleSubmit, pristine, submitting, points } = this.props;
    var loader=this.props.points.resetLeaderboardLoader !== undefined ? this.props.points.resetLeaderboardLoader :false
    return (
      
      <div className='main-container'>
        <div id="content">
        {loader==true ?<PreLoader/>:''}
          <div className="page campaign-container">

            <div className="full-container">
            <div className='widget'>
                      <header class='heading'>
                        <h3>Reset leaderboard</h3>
                      </header>
                      <div className="select-wrapper">
                      <div class='form-row  select'>
                      <select
                          id='ResetType'
                          name='ResetType'
                          onChange={this.selectResetType.bind(this)}
                        >
                          <option value=''>Select reset type</option>
                          <option value='overtime'>Overtime</option>
                          <option value='year'>Last year</option>
                          <option value='month'>Last month</option>
                          <option value='week'>Last week</option>
                          </select>
                      </div>
                      </div>
                      
                      <div className="form-row">
                        <button class="btn right btn-primary" type="button" disabled={!submitType} onClick={this.handleResetLeaderboard.bind(this)}>Submit</button>
                      </div>
                      
                    </div>
                    <form onSubmit={handleSubmit}>
              <div id="point-widget">

                <div className="widget">
                <div className='point-col-wrapper clearfix'>
                <div className="point-col">
                    <div className='point-col-inner'>
                      <h4>INSTRUCTIONS</h4>
                        <div className="in-container-wrapper point-instruction-col">
                        <p>Users can win points for different actions like sharing, uploading assets, liking or publishing post. The points affect how users rank in the leaderboard.  Choose how many points you wish to allocate to each action. This will very much depend on your advocacy goals. </p>
                      </div>
                    </div>
                  </div>
                  <div className='point-col'>

                    <div className='point-col-inner'>
                      <h4> INTERNAL POST </h4>


                      <div className="in-container-wrapper">
                        <div class="in-container clearfix">
                          <Field
                            placeholder='0'
                            label='With video'
                            component={renderField}
                            type='text'
                            name='internal_video_post_approved'
                             min="1" max="99"
                             onChange = {this.AllowNumbersOnly.bind(this)}
                             />
                        </div>
                        <div class="in-container clearfix">
                          <Field
                            placeholder='0'
                            label='With photo'
                            component={renderField}
                            type='text'
                            name='internal_image_post_approved'
                            min="1" max="99" 
                            onChange = {this.AllowNumbersOnly.bind(this)}
                            />
                        </div>
                        <div class="in-container clearfix">
                          <Field
                            placeholder='0'
                            label='Text and file only'
                            component={renderField}
                            type='text'
                            name='internal_text_post_approved'
                            min="1" max="99"
                            onChange = {this.AllowNumbersOnly.bind(this)}
                            /></div>
                      </div>
                    </div>
                  </div>
                  <div className="point-col">
                    <div className='point-col-inner'>
                      <h4>APPROVED EXTERNAL POST</h4>


                      <div className="in-container-wrapper">
                        <div class="in-container clearfix">
                          <Field
                            placeholder='0'
                            label='With video'
                            component={renderField}
                            type='text'
                            name='external_video_post_approved'
                            onChange = {this.AllowNumbersOnly.bind(this)}
                            />
                        </div>
                        <div class="in-container clearfix">
                          <Field
                            placeholder='0'
                            label='With photo'
                            component={renderField}
                            type='text'
                            name='external_image_post_approved'
                            min="1" max="99" 
                            onChange = {this.AllowNumbersOnly.bind(this)}
                            />
                        </div>
                        <div class="in-container clearfix">
                          <Field
                            placeholder='0'
                            label='Text and file only'
                            component={renderField}
                            type='text'
                            name='external_text_post_approved'
                            min="1" max="99" 
                            onChange = {this.AllowNumbersOnly.bind(this)}
                            />
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="point-col">
                    <div className='point-col-inner'>
                      <h4>APPROVED ASSET</h4>



                      <div className="in-container-wrapper">
                        <div class="in-container clearfix">
                          <Field
                            placeholder='0'
                            label='Video'
                            component={renderField}
                            type='text'
                            name='video_asset_approved'
                            min="1" max="99" 
                            onChange = {this.AllowNumbersOnly.bind(this)}
                            />
                        </div>
                        <div class="in-container clearfix">
                          <Field
                            placeholder='0'
                            label='Photo'
                            component={renderField}
                            type='text'
                            name='image_asset_approved'
                            min="1" max="99"
                            onChange = {this.AllowNumbersOnly.bind(this)}
                            />
                        </div>
                        <div class="in-container clearfix">
                          <Field
                            placeholder='0'
                            label='File'
                            component={renderField}
                            type='text'
                            name='other_asset_approved'
                            min="1" max="99"
                            onChange = {this.AllowNumbersOnly.bind(this)}
                            />
                        </div>
                       
                      </div>
                    </div>
                  </div>
                  <div className="point-col">
                    <div className='point-col-inner'>
                      <h4>Internal Feed</h4>

                      <div className="in-container-wrapper">
                        {/* <div class="in-container clearfix">
                          <Field
                            placeholder='0'
                            label='Owner post shared'
                            component={renderField}
                            type='text'
                            name='internal_post_shared_owner'
                            min="1" max="99"
                            onChange = {this.AllowNumbersOnly.bind(this)}
                            />
                        </div> */}
                        {/* <div class="in-container clearfix">
                          <Field
                            placeholder='0'
                            label='User shares post'
                            component={renderField}
                            type='text'
                            name='internal_post_shared'
                            min="1" max="99"
                            onChange = {this.AllowNumbersOnly.bind(this)}
                            /></div> */}
                        <div class="in-container clearfix">
                          <Field
                            placeholder='0'
                            label='Owner post like'
                            component={renderField}
                            type='text'
                            name='internal_post_liked'
                            onChange = {this.AllowNumbersOnly.bind(this)}
                            min="1" max="99" />
                        </div>
                        <div class="in-container clearfix">
                          <Field
                            placeholder='0'
                            label='Owner post comment'
                            component={renderField}
                            type='text'
                            name='internal_post_comment'
                            onChange = {this.AllowNumbersOnly.bind(this)}
                            min="1" max="99"/>

                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="point-col">
                    <div className='point-col-inner'>
                      <h4>EXTERNAL FEED</h4>


                      <div className="in-container-wrapper">
                        <div class="in-container clearfix">
                          <Field
                            placeholder='0'
                            label='Owner post share'
                            component={renderField}
                            type='text'
                            name='external_post_shared_owner'
                            onChange = {this.AllowNumbersOnly.bind(this)}
                            min="1" max="99"/>
                        </div>
                        <div class="in-container clearfix">
                          <Field
                            placeholder='0'
                            label='User shared post'
                            component={renderField}
                            type='text'
                            name='external_post_shared'
                            onChange = {this.AllowNumbersOnly.bind(this)}
                            min="1" max="99" />
                        </div>
                        <div class="in-container clearfix">
                          <Field
                            placeholder='0'
                            label='Owner post like'
                            component={renderField}
                            type='text'
                            name='external_post_liked'
                            onChange = {this.AllowNumbersOnly.bind(this)}
                            min="1" max="99"/>
                        </div>
                        <div class="in-container clearfix">
                          <Field
                            placeholder='0'
                            label='Owner post comment'
                            component={renderField}
                            type='text'
                            name='external_post_comment'
                            onChange = {this.AllowNumbersOnly.bind(this)}
                            min="1" max="99"/>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className="point-col">
                    <div className='point-col-inner'>
                      <h4>CURATION AND JOBS</h4>


                      <div className="in-container-wrapper">
                        <div class="in-container clearfix">
                          <Field
                            placeholder='0'
                            label='Share curated article'
                            component={renderField}
                            type='text'
                            name='share_curated_article'
                            onChange = {this.AllowNumbersOnly.bind(this)}
                            min="1" max="99"/>
                        </div>
                        <div class="in-container clearfix">
                          <Field
                            placeholder='0'
                            label='Share a job'
                            component={renderField}
                            type='text'
                            name='share_job'
                            onChange = {this.AllowNumbersOnly.bind(this)}
                            min="1" max="99" />
                        </div>
                      </div>
                    </div>
                  </div>

                </div>
                <div className='points-button-wrapper clearfix'>
                  <button
                    class='btn btn-primary'
                    type='submit'
                    disabled={pristine || submitting}>
                    Update
                                  </button>
                </div>
                </div>
              </div>
              </form>
            </div>

          </div>
        </div>

      </div>

      
    );
  }
}
function mapStateToProps(state) {
  return { points: state.points }
}

function mapDispatchToProps(dispatch) {
  return {
    pointActions: bindActionCreators(pointActions, dispatch)
    
  }
}

let connection = connect(mapStateToProps, mapDispatchToProps);
var reduxFormConnection = connection(PointForm);
export default reduxForm({ form: 'pointForm' })(reduxFormConnection);
