import {Field, reduxForm} from 'redux-form';
import { browserHistory } from 'react-router'
import {bindActionCreators} from 'redux'
import Globals from "../../../Globals";
import {connect} from 'react-redux'
import * as socialAccountsActions from '../../../actions/socialAccountsActions'
import notie from "notie/dist/notie.js"
import * as utils from '../../../utils/utils'
import PreLoader from '../../PreLoader';
var encryptedSessionId=''
const required = value => value
  ? undefined
  : 'Required';
const validEmail = value => value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
  ? 'Invalid email address'
  : undefined;

class SocialForm extends React.Component {
  constructor() {
    super();

    this.state = {
      fbConnected: false,
      twConnected: false,
      twConnectedPage: false,
      liConnected: false,
      twIdentity: false,
      twIdentityPage: false,
      liIdentity: false,
      fbIdentity: false,
      liConnectedPage: false,
      fbIdentityPage: false,
      fbConnectedPage: false,
      iscompany: 0,
      accountDetails: {},
      liIdentityPage: false,
      loader:false
    }
  }
  componentWillUnmount() {
       this
      .props
      .socialAccountsActions
      .fetchSocialAccounts()
  }
  /**
  * @author : disha
  * @param : nextprops
  * @use : check connected account and store its value in state
  **/
  manageSocialAccount(nextProps) {
    var socialAccounts = nextProps.socialAccount
    this.setState({accountDetails: socialAccounts.accounts});
    for (let s = 0; s < socialAccounts.accounts.length; s++) {
      //iscompany is 0 then ..change the state of connect account  and  iscompany is 1 then chnage the state of account page
      if (socialAccounts.accounts[s].social_media.toLowerCase() == "twitter" && socialAccounts.accounts[s].active && socialAccounts.accounts[s].iscompany == 0) {
        this.setState({twConnected: true});
        this.setState({twIdentity: socialAccounts.accounts[s].identity}); }
        else if (socialAccounts.accounts[s].social_media.toLowerCase() == "twitter" && socialAccounts.accounts[s].iscompany == 1 && socialAccounts.accounts[s].active) {
        this.setState({twConnectedPage: true});
        this.setState({twIdentityPage: socialAccounts.accounts[s].identity});
      } else if (socialAccounts.accounts[s].social_media.toLowerCase() == "linkedin" && socialAccounts.accounts[s].active && socialAccounts.accounts[s].iscompany == 0) {
        this.setState({liConnected: true});
        this.setState({liIdentity: socialAccounts.accounts[s].identity});
      } else if (socialAccounts.accounts[s].social_media.toLowerCase() == "linkedin" && socialAccounts.accounts[s].active && socialAccounts.accounts[s].iscompany == 1) {
        this.setState({liConnectedPage: true});
        this.setState({liIdentityPage: socialAccounts.accounts[s].identity});
      } else if (socialAccounts.accounts[s].social_media.toLowerCase() == "facebook" && socialAccounts.accounts[s].active && socialAccounts.accounts[s].iscompany == 0) {
        this.setState({fbConnected: true});
        this.setState({fbIdentity: socialAccounts.accounts[s].identity});
      } else if (socialAccounts.accounts[s].social_media.toLowerCase() == "facebook" && socialAccounts.accounts[s].iscompany == 1 && socialAccounts.accounts[s].active) {
        this.setState({fbConnectedPage: true});
        this.setState({fbIdentityPage: socialAccounts.accounts[s].identity});
      }
    }
  }

  /**
  * @author : disha
  * @param : accountId , connected
  * @use : method is used to disconnect account
  **/
  disconnect(accountID, connected) {
    var me = this;
    notie.confirm(
              `Are you sure you want to disconnect account?`,
              'Yes',
              'No',
              function () {
                me.setState({
                  loader:true
                })
                fetch(Globals.API_ROOT_URL + `/social-accounts/${accountID}`, 
                {method: 'DELETE',
                  headers: utils.setApiHeaders('get')
                }).then(response => {
                  if (response.status != "200") {
                    notie.alert('error', response.statusText, 3);
                  }
                  return response.json();
                }).then((json) => {
                  if (json.code == 200) {
                    notie.alert('success', json.message, 3);
                    me
                      .props
                      .socialAccountsActions
                      .fetchSocialAccounts();

                    var connectedObj = {}
                    var connectedObjpage = {}
                    connectedObj[connected] = false;
                    var pagename = connected + 'Page'         
                    connectedObjpage[pagename] = false;    // if disconenct the social account,this line change the state of scoial account page ,so both are disconnect

                    me.setState(connectedObj);
                    
                    me.setState(connectedObjpage);
                  if(connected=='fbConnected' || connected=='fbConnectedPage' || connected=='liConnectedPage'){
                    //to close popup 
                    me
                    .props
                    .socialAccountsActions.disconnectFbSuccess()
                  }
                  me.setState({
                    loader:false
                  })
                  } else {
                    utils.handleSessionError(json);
                    notie.alert('error', json.message, 3);
                  }
                }).catch(err => {
                  throw err;
                });
        })
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.callDisconnectMethod !==this.props.callDisconnectMethod){
      //to call disconnect method from child component ( page)
      if(nextProps.disconnectSocialAccType=='facebookpage'){
        this.disconnect(this.state.fbIdentityPage,'fbConnectedPage')
      }else if(nextProps.disconnectSocialAccType=='linkedinpage'){
        this.disconnect(this.state.liIdentityPage,'liConnectedPage')
      }else if(nextProps.disconnectSocialAccType=='facebook'){
        this.disconnect(this.state.fbIdentity,'fbConnected')
      }
    }
    if (nextProps.socialAccount !== this.props.socialAccount && nextProps.socialAccount !== null && nextProps.socialAccount.accountsFetched==true ) {
      this.manageSocialAccount(nextProps)
    }
  }
  componentDidMount() {
    if(window.location.href.indexOf("?") !== -1){
       let splitedUrl = window.location.href.split("?");
        let splitedByEndUrl = splitedUrl[1].split("&");
        if(splitedByEndUrl.includes("success=Duplicate")){
            notie.alert('error','You have already connected this account in your personal or company account',5);  
        }
    }
   

    encryptedSessionId=utils.encodeSessionId(Globals.SESSION_ID)
    //after connecting facebook user account if it is successful then to open page popup 
    if(this.props.queryParams.success=='1' && this.props.queryParams.platform=='facebook'){
      this.openPagePopup('facebook')
    }
    if(this.props.queryParams!==undefined && this.props.queryParams.success!==undefined){
      //to redirect to below page after connecting from any social account , bcz after redirecting from social page it had many param which generate error sometimes
      browserHistory.push("/settings/social");
    }
  }
  
  renderCheckedIcon() {
    return (
      <span class="social-check-icon">
        <i class="fa fa-check fa-2"></i>
        &nbsp; Personal
      </span>
    )
  }
  openPagePopup(type) {
    this
      .props
      .socialAccountsActions
      .socialpageInit() //call to set popup flag to false
    this
      .props
      .openPagePopup(type)
  }
renderNotConnectedSocial(socialAccount) {
    return (
      <span>
        {socialAccount}
      </span>
    )
  }
  twitterPage() {
    if (this.state.twConnected == false) {
      notie.alert('error', `Please connect your profile first`, 3)
    } else {
      this.setState({iscompany: 1})
    }
  }

  //@use : to display connected social accounts dynamically
  fetchDetails() {
    
    var accountType = '';
    var identity = '';
    var accountName = '';
    var defaultimg= "http://abs.twimg.com/sticky/default_profile_images/default_profile_normal.png";
   
    
    return (typeof this.state.accountDetails !== "undefined" && this.state.accountDetails !== false
      ? (Object.keys(this.state.accountDetails).length > 0)
        ? this.state.accountDetails.map((data, i) => {
           var userName ='';
           var profileImage = '';
          //add condition bcz to hide fb is connected
          accountType = 
           (data.social_media) == "facebook"
             ? "facebook" :
             (data.social_media) == "linkedin"
              ? "linkedin"
              : (data.social_media) == "twitter"
                ? "twitter"
                : ''
                //below code is comment bcz to hide fb in connected social acc list
           if (accountType == "facebook") {
              identity = this.state.fbIdentity;
              accountName = 'fbConnected';
           } else if (accountType == "linkedin") {
            identity = this.state.liIdentity;
            accountName = 'liConnected';
          } else {
            identity = this.state.twIdentity;
            accountName = 'twConnected';
          }
          Object.keys(data.user_social).length?
          data.user_social.map((userSocial, j) => 
              userSocial.key == 'avatar'
              ? profileImage = userSocial.value ? userSocial.value : defaultimg
                : (userSocial.key == 'name' || userSocial.key == 'page_name') ? userName = userSocial.value:''
          ):''
          return ( Object.keys(data.user_social).length > 0 ?
            
               <tr key={'socialForm'+i}>
                      <td width='10%' className='avtar-col'>
                        <div className="table-avtar-wrapper">
                          <img src={profileImage} alt='bill-contact-img.jpg'  onError = {(e)=>{e.target.src="http://abs.twimg.com/sticky/default_profile_images/default_profile_normal.png"}}/> {/* <img src='/img/bill-contact-img.jpg' alt='bill-contact-img.jpg' /> */}
                          <span className={`table-avtar-social-icon ${accountType}`}><i className={`fa fa--social fa-${accountType}`}/></span>
                        </div>
                      </td>
                      <td width='23%' data-rwd-label="ACCOUNT">{userName}</td>
                      <td width='23%' data-rwd-label="ACCOUNT TYPE">{data.iscompany == 1 ? 'Company' : 'Personal'}</td>
                      <td width='23%' data-rwd-label="CONNECTED BY">{this.props.profile.profile.first_name +' '+this.props.profile.profile.last_name}</td>
                      <td className='actiondiv' width='20%'>
                        <div class="action-button">
                          <ul class="dd-menu context-menu user_3dots">
                            <li class="button-dropdown">
                              <a class="dropdown-toggle user_drop_toggle">
                                <i class="material-icons">more_vert</i>
                              </a>
                              <ul class="dropdown-menu user_dropAction">
                                <li
                                  onClick
                                  ={this
                                  .disconnect
                                  .bind(this, identity, accountName)}>
                                  <span>Disconnect</span>
                                </li>
                              </ul>
                            </li>
                          </ul>
                        </div>
                      </td>
                    </tr>


                  : '')
        })
        : ''
      : "")
  }
  open_notie_connect(social_name)
  {
    notie.alert('error', `Please connect your profile first`, 3)
  }
  render() {
    const {handleSubmit, pristine, submitting} = this.props;

    /**
  * @author : disha
  * @param :
  * @use : api list for connecting accounts
  */
    let fbLink = Globals.API_ROOT_URL + `/auth/facebook?session_id=${encryptedSessionId}&is_company=${this.state.iscompany}&redirect=${Globals.CLIENT_URL}/settings/social`;
    let fbLinkpage = ''
    if (this.state.fbConnected == true) {
      fbLinkpage = Globals.API_ROOT_URL + `/auth/facebook?session_id=${encryptedSessionId}&is_company=${this.state.iscompany}&redirect=${Globals.CLIENT_URL}/settings/social`;
    }

    let twLink = Globals.API_ROOT_URL + `/auth/twitter?session_id=${encryptedSessionId}&is_company=${this.state.iscompany}&redirect=${Globals.CLIENT_URL}/settings/social`;
    let twLinkpage = ''
    if (this.state.twConnected == true) {
      twLinkpage = Globals.API_ROOT_URL + `/auth/twitter?session_id=${encryptedSessionId}&is_company=${this.state.iscompany}&redirect=${Globals.CLIENT_URL}/settings/social`;
    }
    let liLink = Globals.API_ROOT_URL + `/auth/linkedin?session_id=${encryptedSessionId}&is_company=${this.state.iscompany}&redirect=${Globals.CLIENT_URL}/settings/social`;
    var fbOpts = {};
    var lnOpts = {};
    var lnOptsPage = {}
    var twOpts = {};
    var twOptsPage = {};
    twOpts['onClick'] = (this.state.twConnected)
      ? this
        .disconnect
        .bind(this, this.state.twIdentity, 'twConnected')
      : '';
    if (this.state.twConnected == false) {
      twOpts['href'] = twLink;
    }
    if (this.state.twConnected == true) {
      if (this.state.twConnectedPage == false) {
        twOptsPage['href'] = twLinkpage;
      }
    }
    //if user account is connected and user click on company button again then to open page popup to select
    fbOpts['onClick'] = this.state.fbConnected
      ?this.openPagePopup.bind(this,'facebook'):''
    if (this.state.fbConnected == false) {
      fbOpts['href'] = fbLink;

    }
    lnOpts['onClick'] = (this.state.liConnected)
      ? this
        .disconnect
        .bind(this, this.state.liIdentity, 'liConnected')
      : '';
    if (this.state.liConnected == false) {
      lnOpts['href'] = liLink;
    }

    return (

      <div key={1}>
        <form onSubmit={handleSubmit}>
          <div className="widget">
            <header class="heading">
              <h3>SOCIAL ACCOUNTS</h3>
            </header>
            <div className='socialnotes'>
              <p>Click on the social channels below to add either a personal profile or
                company page
                <strong>(Admins only)</strong>
                account.</p>
            </div>
            <div className='social-cols clearfix'>
              <div className='social-col twitter'>
                <div className='social-col-inner'>
                  <div className='social-icon'>
                    <i className="fa fa--social fa-twitter"></i>
                  </div>
                  <div className='social-button-wrapper'>
                    <a {...twOpts} className = {this.state.twConnected ? 'twconnect' : ''}>{this.state.twConnected
                        ? this.renderCheckedIcon()
                        : this.renderNotConnectedSocial("Personal", "twitter")}</a>
                    <a
                     
                      className = {this.state.twConnectedPage ? 'tw-page-connect' : ''}
                      href="javascript:void(0);"
                      {...twOptsPage}
                      onClick={this.state.twConnectedPage
                      ? this
                        .disconnect
                        .bind(this, this.state.twIdentityPage, 'twConnectedPage')
                      : this
                        .twitterPage
                        .bind(this)}>{this.state.twConnectedPage
                        ? <span>
                            <i class="fa fa-check fa-2"></i>&nbsp; Company</span>
                        : this.renderNotConnectedSocial("Company")}</a>

                  </div>
                </div>
              </div>

              <div className='social-col linkedin'>
                <div className='social-col-inner'>
                  <div className='social-icon'>
                    <i className="fa fa--social fa-linkedin"></i>
                  </div>
                  <div className='social-button-wrapper'>

                    <a {...lnOpts} className = {this.state.liConnected ? 'liconnect' : ''}>{this.state.liConnected
                        ? this.renderCheckedIcon()
                        : this.renderNotConnectedSocial("Personal", "linkedin")}</a>
                    <a
                      href="javascript:void(0);"
                      
                      className = {this.state.liConnectedPage ? 'li-page-connect' : ''}
                      onClick={this.state.liConnected
                        ? this.openPagePopup.bind(this, 'linkedin')
                        : this.open_notie_connect
                        .bind(this)}>{(this.state.liConnectedPage && this.state.liConnected) || this.state.liConnectedPage
                        ? <span>
                            <i class="fa fa-check fa-2"></i>&nbsp; Company</span>
                        : 'Company'}</a>

                  </div>
                </div>
              </div>
              <div className='social-col facebook'>
                <div className='social-col-inner'>
                  <div className='social-icon'>
                    <i className="fa fa--social fa-facebook"></i>
                  </div>
                  <div className='social-button-wrapper'>
                  {/* uncommnet below code to add personal button  */}
                     {/*<a {...fbOpts} className = {this.state.fbConnected ? 'fbconnect' : ''}>{this.state.fbConnected
                        ? this.renderCheckedIcon()
                        : this.renderNotConnectedSocial("Personal", "facebook")}</a> */}
                    <a
                    {...fbOpts}
                      // href="javascript:void(0);"
                    
                      className = {this.state.fbConnected ? 'fbconnect' : ''}
                      // className = {this.state.fbConnectedPage  ? 'fb-page-connect' : ''}
                       //onClick={this.state.fbConnected
                       //? (this.state.fbConnectedPage

                         //? this.disconnect.bind(this, this.state.fbIdentityPage, 'fbConnectedPage')
                         //: this.openPagePopup.bind(this, 'facebook'))
                         //: this
                         //.open_notie_connect
                         //.bind(this)}
                        >{(this.state.fbConnectedPage && this.state.fbConnected) || (this.state.fbConnectedPage)
                        ? <span>
                            <i class="fa fa-check fa-2"></i>&nbsp; Company</span>
                        : 'Company'}</a>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </form>
        
        <div className="widget">
          <div className='account-social-wrapper'>
              {/* loader */}
              {this.props.socialAccount.socialLoader ? 

                <div className=" loader-culture-tableExpand">
                <div>
                    <div class="table-anytlics-body">
                        <table class="table responsive-table">
                            <thead>
                                <tr class="reactable-column-header">
                                    <th class="reactable-th-serialno reactable-header-sortable serialno  a-center">
                                        <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                                    </th>
                                    <th class="reactable-th-serialno reactable-header-sortable serialno  a-center">
                                        <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                                    </th>
                                    <th class="reactable-th-serialno reactable-header-sortable serialno  a-center">
                                        <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                                    </th>
                                    <th class="reactable-th-serialno reactable-header-sortable serialno  a-center">
                                        <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                                    </th>

                                </tr>
                            </thead>
                            <tbody class="reactable-data">
                                <tr class="table-body-text">
                                    <td>
                                        <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                                    </td>
                                    <td>
                                        <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                                    </td>
                                    <td>
                                        <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                                    </td>
                                    <td>
                                        <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                                    </td>
                                </tr>
                                <tr class="table-body-text">
                                    <td>
                                        <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                                    </td>
                                    <td>
                                        <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                                    </td>
                                    <td>
                                        <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                                    </td>
                                    <td>
                                        <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                                    </td>

                                </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            // loader ends here
            :
            <div>
            <div className='account-social-table'>
            {/* to hide no account connected while facebook is connected then add below condition */}
              {/* {this.state.twConnected == true || this.state.fbConnected == true || this.state.liConnected == true */}
              {this.state.twConnected == true || this.state.liConnected == true || this.state.fbConnected == true
                ? <table className='table responsive-table'>
                    <thead>
                      <tr>
                        <th>&nbsp;</th>
                        <th>ACCOUNT</th>
                         <th>ACCOUNT TYPE</th>
                        <th>CONNECTED BY</th>
                        <th className='actiondiv a-center'>ACTIONS</th>
                      </tr>
                    </thead>
                    <tbody className='reactable-data'>
                      {this.fetchDetails()}

                    </tbody>
                  </table>
                : <p className="noaccountcoonect">
                  No account connected
                </p>}
            </div>
            <form onSubmit={handleSubmit}>

            </form>

          </div>
        }
        </div>

      </div>
     
      </div>
    )
  }
}
function mapStateToProps(state) {
  return {socialAccount: state.socialAccounts,
          profile: state.profile}
  
}
function mapDispatchToProps(dispatch) {
  return {
    socialAccountsActions: bindActionCreators(socialAccountsActions, dispatch)
  }
}
let connection = connect(mapStateToProps, mapDispatchToProps)
var reduxConnectedComponent = connection(SocialForm)
export default reduxForm({
  form: 'step3', // a unique identifier for this form
})(reduxConnectedComponent)
