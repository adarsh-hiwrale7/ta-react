import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import PreLoader from '../../PreLoader'
import * as settingsActions from "../../../actions/settingsActions"
import * as socialAccountsActions from '../../../actions/socialAccountsActions'
import ReactCSSTransitionGroup from 'react/lib/ReactCSSTransitionGroup';
import notie from "notie/dist/notie.js"
import SocialForm from './SocialForm';
import Header from '../../Header/Header';
import SettingsNav from '../SettingsNav';
import Page from './Page'
import GuestUserRestrictionPopup from '../../GuestUserRestrictionPopup';
var restrictAPI=false;
class SocialSettings extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      show_menu: true,
      openPage: false,
      type:'',
      iscompany:0,
      createdPageDetail:{},
      callDisconnectMethod:false,
      disconnectSocialAccType:''
    }
  }

  componentDidMount() {
    if(restrictAPI!==true){
      this.props.socialAccountsActions.fetchSocialAccounts()
      restrictAPI = true;
      setTimeout(()=>{
        restrictAPI=false
      },5000)
    }
  }
  
  handleFormSubmit(values) {
    this.props.settingsActions.settingsPostStep3(values, "/register/finish");
  }

  openPagePopup(page) {
    //this method is use for social account page open popup
    this.setState({openPage: true, type:page,
    iscompany:1});
    document.body.classList.add('overlay');
  }
  closePage() {
    this.setState({ openPage: false, page:''})
    document.body.classList.remove('overlay');
  }
  pageCreated(e){
    this.setState({
      createdPageDetail:e
    })
  }
  /**
   * @author disha
   * @use this will set flag which is passed to child component and call disconnect method
   */
  disconnectFbOrPagesParent(e){
    //opne page popup and if you got any error then close popup and open it again then you can not disconnect page as there will be newprops and this props  same. so to make them different i set twice. // disha
    this.setState({
      callDisconnectMethod:false
    },()=>
    this.setState({
      callDisconnectMethod:true,
      disconnectSocialAccType:e
    }))
  }
  render() {
    return (
      <div>
         <GuestUserRestrictionPopup/>
          <section className="settings social-settings" >

        <Header title="Social Accounts" nav={SettingsNav} location ={this.props.location}></Header>
        {this.state.openPage ?
          <Page
          disconnectFbOrPages={this.disconnectFbOrPagesParent.bind(this)}
          closePage={this.closePage.bind(this)}
          type={this.state.type}
          iscompany={this.state.iscompany}
          pageCreated={this.pageCreated.bind(this)}
          />
          : ''}
        <div className='main-container'>
          <div id="content">
            <div className="page">
              <div id="scoial-acount-setting">
                <div className="full-container clearfix">
                  <SocialForm
                      disconnectSocialAccType={this.state.disconnectSocialAccType}
                      callDisconnectMethod={this.state.callDisconnectMethod}
                      onSubmit={this.handleFormSubmit.bind(this)}
                      queryParams={this.props.location.query}
                      openPagePopup = {this.openPagePopup.bind(this)}
                      createdPageDetail={this.state.createdPageDetail}
                  > </SocialForm>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      </div>
    );
  }
}
function mapStateToProps (state) {
  return {
    socialAccount: state.socialAccounts
  }
}
function mapDispatchToProps(dispatch) {
  return {
    socialAccountsActions: bindActionCreators(socialAccountsActions, dispatch),
    settingsActions: bindActionCreators(settingsActions, dispatch)
  }
}

let connection = connect(mapStateToProps, mapDispatchToProps);

module.exports = connection(SocialSettings);
