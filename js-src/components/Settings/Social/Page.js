import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import { Field, reduxForm } from 'redux-form';
import { renderField } from '../../Helpers/ReduxForm/RenderField'

import * as socialAccountsActions from "../../../actions/socialAccountsActions"
import * as settingsActions from "../../../actions/settingsActions"

import Globals from "../../../Globals"
import notie from "notie/dist/notie.js"
import PopupWrapper from '../../PopupWrapper'
import SliderPopup from '../../SliderPopup'
import * as utils from '../../../utils/utils'
import PreLoader from '../../PreLoader';

const required = value => value ? undefined : 'Required';
var disableDisconnectButton=false
class Page extends React.Component {
  constructor() {
    super()
    this.state = {
      pages: [],
      pageAccessToken: '',
      pageId: '',
      pageName: '',
      nopages: false,
      pageloader:false
    }
  }
  componentDidMount() {
    this.handleInitialize()
    }
    
     /**
  * @author : disha
  * @param : 
  * @use : to display selected value in dropdown
  */
    handleInitialize(nextProps) {
    var pagename=''
    if(this.props.socialAccounts.accounts.length>0) {
      for(var i=0;i<this.props.socialAccounts.accounts.length;i++){
        if(this.props.socialAccounts.accounts[i].iscompany==1 &&this.props.socialAccounts.accounts[i].social_media==this.props.type){
          var selectedAcc=this.props.socialAccounts.accounts[i]
          Object.keys(selectedAcc.user_social).map((accDetail, i) => {
            if(selectedAcc.user_social[accDetail]['key']=='page_id'){
              pagename=selectedAcc.user_social[accDetail]['value']
            }
          })
        }
      }
    }
    
    if(pagename==''){
      //to disable disconnect page button if there is not page is connected
      disableDisconnectButton=true
    }else{
      disableDisconnectButton=false
    }
    const initData = {
      "pagename": pagename,
      };
    this.props.initialize(initData);

    }
  componentWillMount() {
    this.props.socialAccountsActions.fetchSocialPages(this.props.type)
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.socialAccounts.popupclose == true) {
      //to close page popup
      this
        .props
        .closePage()
        var pageDetail={
          pageCreated:nextProps.socialAccounts.pageCreated,
          AccountName:this.props.type
        }
        this.props.pageCreated(pageDetail)
        this.props.socialAccountsActions.fetchSocialAccounts()
    }
  }
   /**
  * @author : disha
  * @param : 
  * @use : submit selected page
  */
    handlePageSelection(){
      var accesstoken=''
      var data={
        type:this.props.type,
        pagename:this.state.pageName,
        pageid:this.state.pageId,
        accesstoken:this.state.pageAccessToken,
        iscompany:this.props.iscompany,
        picture:this.state.picture
      }
      this.props.socialAccountsActions.sendSelectedPage(data)

    }
  /**
  * @author : disha
  * @param : selected page detail 
  * @use : to store values of selected page
  */
    optionPage(e) {
      var pageid=e.target.value
      
        for(var i=0;i<this.props.socialAccounts.pageList.length;i++){
          if(pageid==this.props.socialAccounts.pageList[i].id){
            this.setState({
              pageAccessToken:this.props.socialAccounts.pageList[i].access_token,
              pageName:this.props.socialAccounts.pageList[i].name,
              pageId:this.props.socialAccounts.pageList[i].id,
              picture:this.props.socialAccounts.pageList[i].picture
            })
          }
        }
    }
    /**
     * @author disha
     * @use to disconnect fb, this will call parent function
     */
    disconnect(e){
      var socialAccountToDisconnect=''
      if(e=='facebook'){
        //if this method is call to disconnect facebook personal account then to pass its name
        socialAccountToDisconnect='facebook'
      }else if(e=='page'){
        //if this method is call to disconnect page then to pass page along with its type name e.g facebookpage
        socialAccountToDisconnect=this.props.type+'page'
      }
      this.props.disconnectFbOrPages(socialAccountToDisconnect)
    }
  render() {
    const { pristine, submitting, reset } = this.props
    if(!this.props.socialAccounts.pageList){
      notie.alert('error', 'No Page Found', 3); 
    }
    return (
      <SliderPopup>
        {this.props.socialAccounts.pageLoader ? <PreLoader /> : <div />}
        <button
          id='close-popup'
          className='btn-default'
          onClick={this.props.closePage.bind(this)}>
          <i className='material-icons'>clear</i>
        </button>
        <div>
          <header class="heading">
            <h3>{this.props.type}</h3>
          </header>
          <PopupWrapper>
            <div class="form-row">
              <Field
                name='pagename'
                className='drop_department'
                onChange={this.optionPage.bind(this)}
                placeholder='Select Page'
                component={renderField}
                type='select'
                validate={[required]}
              >
                <option value=''>Select Page</option>
                {this.props.socialAccounts.pageList
                  ? this.props.socialAccounts.pageList
                    .map(page => (this.props.type !== "facebook"
                      ? <option value={page.id} key={page.id}>
                        {page.name}
                      </option>
                      : <option value={page.id} key={page.id}>
                        {page.name}
                      </option>))
                  : ''}

              </Field>
            </div>
            <div class="clearfix">
              <div className='submitAssetActionbtn pageSelection social-accound-likedin-btn'>
                <button
                  className='btn btn-primary connect-page'
                  type='submit'
                  onClick={this.handlePageSelection.bind(this)}
                  disabled={pristine || submitting}
                >
                  Connect page
              </button>
                <button
                  className='btn btn-primary'
                  type='submit'
                  onClick={this.disconnect.bind(this, 'page')}
                  disabled={disableDisconnectButton}>
                  Disconnect page
              </button>
              </div>
            </div>
            {this.props.type == 'facebook' ?
              <div class="clearfix">
                <div className='submitAssetActionbtn discoonectFacebookWrappper'>
                  <button
                    className='facebook-btn'
                    type='submit'
                    onClick={this.disconnect.bind(this, 'facebook')}>
                    <span class="facebookIconWrapper">
                    <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 58 58"><defs></defs><title>flogo-HexRBG-Wht-58</title><path class="cls-1" d="M53.85,0H3.15A3.15,3.15,0,0,0,0,3.15v50.7A3.15,3.15,0,0,0,3.15,57h27.3V35H23V26.33h7.41V20c0-7.37,4.49-11.38,11.06-11.38A62.15,62.15,0,0,1,48.15,9v7.69H43.61c-3.57,0-4.26,1.69-4.26,4.18v5.5H47.9L46.79,35H39.35V57h14.5A3.15,3.15,0,0,0,57,53.85V3.15A3.15,3.15,0,0,0,53.85,0Z"></path></svg>
                    </span> Disconnect Facebook
                   </button>
                </div>
              </div> : ''}
          </PopupWrapper>
        </div>
        
      </SliderPopup>
    )
  }
}

function mapStateToProps(state) {
  return {socialAccounts: state.socialAccounts}
}

function mapDispatchToProps(dispatch) {
  return {
    settingsActions: bindActionCreators(settingsActions, dispatch),
    socialAccountsActions: bindActionCreators(socialAccountsActions, dispatch)
  }
}

let connection = connect(mapStateToProps, mapDispatchToProps);
var reduxConnectedComponent = connection(Page)
export default reduxForm({
  form: 'page',  // a unique identifier for this form
})(reduxConnectedComponent)

