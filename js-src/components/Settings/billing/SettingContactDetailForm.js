import { Link } from "react-router";
import { browserHistory } from "react-router";
import * as billingActions from '../../../actions/BillingActions';
import { connect } from 'react-redux'; 
import { bindActionCreators } from 'redux';
import { Field, reduxForm } from 'redux-form';
import { renderField } from '../../Helpers/ReduxForm/RenderField'
const required = value => value
  ? undefined
  : 'Required';

const requiredBox = value => value != 0
  ? undefined
  : 'Required';




class SettingContactDetailForm extends React.Component {
			
	constructor(props){
		super(props);
		
		//this.handleView = this.handleView.bind(this);
		
	}

	changeCountry(e){
		this.props.changeCountry(e.target.value);
	}
	changeCountryOnLoad(val){
		this.props.changeCountry(val);
	}
	changeState(e){
		this.props.changeState(e.target.value);
	}
	changeStateOnLoad(val){
		this.props.changeState(val);
	}
	componentWillReceiveProps (nextProps) {
		
		if(typeof nextProps.contactDetail !== "undefined" && 
			nextProps.contactDetail !== this.props.contactDetail){

			this.handleInitialize(nextProps)
		}
		
	}

	handleInitialize (nextProps = null) {
		let countryId = 0;
		if(typeof nextProps.contactDetail.country !== "undefined"){
			this.changeCountryOnLoad(nextProps.contactDetail.country.data.identity);
			countryId = nextProps.contactDetail.country.data.identity;
		}
		if(typeof nextProps.contactDetail.state !== "undefined"){
			this.changeStateOnLoad(nextProps.contactDetail.state.data.identity);
		}
		
		const initData = {
			country: countryId,
	        company: nextProps.contactDetail.company,
	       	address: nextProps.contactDetail.address,
	       	state: (typeof nextProps.contactDetail.state !== "undefined") ? nextProps.contactDetail.state.data.identity : 0,
	       	city: (typeof nextProps.contactDetail.city !== "undefined") ? nextProps.contactDetail.city.data.identity : 0,
	       	postal: nextProps.contactDetail.postal,
	       	vat_no: nextProps.contactDetail.vat_no

	     }

	     this.props.initialize(initData)

	}



	
	
	
	render(){
		//country option
		var countryOption = [];
		countryOption.push(<option key="0" value="0"> Select Country </option>);
		if(this.props.country !== "undefined" && this.props.country.length > 0) {
				
		  		this.props.country.map((country, i) => {
		  	 		countryOption.push(<option key={i+1} value={country.identity}>{country.title}</option>)
		  	 	})	
        }
        //state option
        var stateOption = [];
        stateOption.push(<option key="0" value="0"> Select State </option>);
		if(this.props.state !== "undefined" && this.props.state.length > 0) {
		  		this.props.state.map((state, i) => {
		  	 		stateOption.push(<option key={i+1} value={state.identity}>{state.title}</option>)
		  	 	})	
        }
        //city option
        var cityOption = [];
        cityOption.push(<option key="0" value="0"> Select City </option>);
		if(this.props.city !== "undefined" && this.props.city.length > 0) {
		  		this.props.city.map((city, i) => {
		  	 		cityOption.push(<option key={i+1} value={city.identity}>{city.title}</option>)
		  	 	})	
        }
		const {
	      handleSubmit,
	      pristine
    	} = this.props
		return  (
			
				<form className="billing-contact-form clearfix" onSubmit={handleSubmit}>
				 <div className="form-row">
                    <label for="country">Country</label>
                        <div className='select-wrapper'> 
                                <Field
                                  placeholder='Select Location'
                                  component={renderField}
                                  type='select'
                                  name='country'
                                  onChange={this.changeCountry.bind(this)}
                                  validate={[requiredBox]}

                                >
                                {countryOption}
								 </Field>
								 </div>
								 </div>

                            
                                <Field placeholder="Company Name" label="Company Name" component={renderField}  type="text" name="company" validate={[required]} />
                                <Field placeholder="Street Address" label="Street Address" component={renderField} validate={[required]} type="text" name="address" />

								<div className="form-row">
                                 <label for="state">State</label>
                                    <div className='select-wrapper'>  
                                <Field
                                  placeholder='Select State'
                                  component={renderField}
                                  type='select'
                                  name='state'
                                  onChange={this.changeState.bind(this)}
                                >
                                {stateOption} 

                                </Field>
								</div>
								</div>
								<div className="form-row">
                                 <label for="city">City</label>
                                    <div className='select-wrapper'>  
                                <Field
                                  placeholder='Select City'
                                  component={renderField}
                                  type='select'
                                  name='city'

                                >
                                {cityOption} 

                                </Field>
								</div>
								</div>

                                <Field placeholder="Postal Code" label="Postal Code" component={renderField} type="text" name="postal" />
                                <Field placeholder="VAT ID" label="VAT ID" component={renderField} type="text" name="vat_no" />
                                <div className="form-row"><button class="btn btn-primary" type="submit">Update</button></div>
                              
                </form>
			
		);
	}
}

    function mapStateToProps (state, ownProps) {
	  return {}
	}

	function mapDispatchToProps (dispatch) {
	  return {
	  		 
	  }
	}
	let connection = connect(mapStateToProps, mapDispatchToProps)

	var reduxConnectedComponent = connection(SettingContactDetailForm)

	export default reduxForm({
	  form: 'SettingContactDetailForm' // a unique identifier for this form
	})(reduxConnectedComponent)

   