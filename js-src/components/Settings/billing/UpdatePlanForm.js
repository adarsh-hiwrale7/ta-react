import { Link } from "react-router";
import { browserHistory } from "react-router";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Field, reduxForm } from 'redux-form';
import { renderField } from '../../Helpers/ReduxForm/RenderField';
const required = value => value
  ? undefined
  : 'Required';

class UpdatePlanForm extends React.Component {

  constructor(props) {
    super(props);

    //this.handleView = this.handleView.bind(this);

  }
  closeChangePlanPopup() {
    this.props.closeChangePlanPopupParent();
  }

  componentDidMount() {
    this.handleInitialize()
  }

  handleInitialize(nextProps = null) {

    if(typeof(this.props.billingDetail)!=="undefined") {
      const initData = {
          "planValue": this.props.billingDetail.plan_id,
      };
      this.props.initialize(initData);
    }

  }

  formSubmit(val) {
    this
      .props
      .handleSubmit(val)
  }

  render() {

    return (

      <form onSubmit={this.formSubmit.bind(this)} >
        <div>
          <div className="planChangePopupWrapper">
            <div className="planChangePopup">
              <a href="javascript:void(0);" class="closeChargebeePopup" onClick={this.closeChangePlanPopup.bind(this)}><i class="material-icons close-icon">close</i></a>
              <div id="cb-header-wrap" role="banner" class="cb-header__wrap cb-transition__slide"><div class="cb-header__logo"><img src="https://s3.amazonaws.com/cb-invoice-logos-prod/visibly-test/logo_1532600584.png" alt="logo" class="cb-header__image" /></div></div>
              <div className="planChangeTitle">Change Plan</div>
              <div className="planChangeDescription">
                <div className="planUpdatePara">
                  <p className="planparaTitle">Your current plan is <span>"{this.props.billingDetail.plan}"</span></p>
                  <p className="planparaContent">Do you want to change the plan?</p>
                </div>
                <div className="planChangeDropdownWrpper form-row">
                  <Field
                    component={renderField}
                    type='select'
                    name='planValue'
                    id='planValue'
                    defaultValue={this.props.billingDetail.plan_id}
                  >
                    {(this.props.plan.length > 0) ? 
                          this.props.plan.map((plan, i) => ( 
                                <option key={i} value={plan.identity}>{plan.plan_type}</option>
                          ))  
                      :
                      '' 
                    }
                  </Field>
                </div>
                <div class="planChangeBtnWrapper form-row">
                  <button class="btn btn-primary" type="submit">Upgrade</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </form>

    );
  }
}

function mapStateToProps(state, ownProps) {
  return {}
}

function mapDispatchToProps(dispatch) {
  return {

  }
}
let connection = connect(mapStateToProps, mapDispatchToProps)

var reduxConnectedComponent = connection(UpdatePlanForm)

export default reduxForm({
  form: 'UpdatePlanForm' // a unique identifier for this form
})(reduxConnectedComponent)

