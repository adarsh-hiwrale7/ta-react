import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import PreLoader from '../../PreLoader'
import * as settingsActions from "../../../actions/settingsActions"
import notie from "notie/dist/notie.js"
import * as loginActions from "../../../actions/loginActions"
import SliderPopup from '../../SliderPopup'
import SettingsNav from '../SettingsNav';
import {Link} from 'react-router';
import * as billingActions from '../../../actions/BillingActions';
import {Field, reduxForm} from 'redux-form';
import * as utils from '../../../utils/utils'
import Header from '../../Header/Header';
import BillingNav from './BillingNav';
import PayNowAddCardForm from './PayNowAddCardForm';
import { renderField } from '../../Helpers/ReduxForm/RenderField';

class PayNowPopup extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      addCard:false,
      selectedCardId:null,
      defaultCardId:null
    }
  }
  componentDidMount(){
    this.props.billingActions.getDefaultCards()

  }
  componentWillReceiveProps(newProps){
      if(newProps.billing.defaultCard !== this.props.billing.defaultCard){
          this.handleInitialize(newProps,newProps.billing.defaultCard[0].identity)
      }
      if(newProps.billing.closePayNowPopup !== this.props.billing.closePayNowPopup){
          if(newProps.billing.closePayNowPopup === true){
             this.props.closeAssetDetailPopup();
          }
      }
  }
  openAddCard()
  {
     this.props.billingActions.changePayPopup('card');
  }
  closePaynowPopup() {
    this.props.closeAssetDetailPopup();
    document
    .body
    .classList
    .remove('overlay')
  }
  cardSelect(cardId){
    this.handleInitialize(this.props,cardId);

  }
  handleSubmit(val){
    this.props.billingActions.addCard(val);
  }

  handleInitialize (nextProps,cardId) {
    this.setState({
        'defaultCardId': cardId
    })
    const initData = {
      card_identity: cardId,
      amount: this.props.amount,
    }

    this.props.initialize(initData)
  }

  render() {
    const {
        handleSubmit,
        pristine
      } = this.props
    return (
        <div className='pay_now_popup'>
        <SliderPopup wide>
        <button
          id='close-popup'
          className='btn-default'
          onClick={this.closePaynowPopup.bind(this)}
        >

          <i className='material-icons'>clear</i>
        </button>
        <div id="pay_new_section">
          {(typeof this.props.billing.detail !== "undefined" && this.props.billing.paymentLoading === true)
            ? <PreLoader className = "Page" />
            : null}
          <div className='row'>
          {
            (typeof this.props.billing.payNowPopupScreen === "undefined" || this.props.billing.payNowPopupScreen === "pay")
            ?
            <div className='col-one-of-two'>
            <div className='widget'>
              <header className='heading'>
                <h3>PAY NOW</h3>
              </header>
              <div className='pay-now-section'>
                <div className='pay-now-section-inner'>
                  <div className='pay-now-inner-title'>Existing cards</div>
                  <div className='pay-now-content'>
                    <p>These are all the cards we currently have on record for your organisation.</p>
                  </div>
                  <form onSubmit={handleSubmit}>
                      {
                       this.props.cardDetail.length > 0 ?
                       <div>
                          <Field
                            component={renderField}
                            type='hidden'
                            name='card_identity'
                          />
                          <Field
                            component={renderField}
                            type='hidden'
                            name='amount'
                          />
                           {this.props.cardDetail.map((card, i) => {
                                  return(<label className='existcards_label radio_label' key={i}>
                                    <span className='exist-card-inner'>
                                      <Field
                                        component={renderField}
                                        type='radio'
                                        name='existing_card'
                                        onChange={this.cardSelect.bind(this,card.identity)}
                                        value={card.identity}
                                        checked='checked'
                                        label =  {(card.default === 1) ? <span className='exist-card-default-text'>Default</span> : 'Make default' }
                                      />

                                      <span className="exist-card-holder-name">{card.card_name}</span>
                                      <span className='exist-card-ending-date'>Visa ending in {(card.card_number.toString()).slice(-4)}  - {card.exp_month} / {card.exp_year}</span>
                                     



                                      </span>
                                  </label>)
                                  })

                          } 
                        </div>
                       : 'No card found, Please add card'
                      }


                    <div className='buttons-wrapper clearfix'>
                      <button className='btn btn-primary' type="button" onClick={this.openAddCard.bind(this)}><span className='plus-icon'>+</span> Add card</button>
                      <button className='btn btn-primary' type='submit'>Pay now</button>
                    </div>
                 </form>
                </div>
              </div>
            </div>
          </div>
          :
          <div className='col-one-of-two'>
          <div className='widget'>
            <header className='heading'>
              <h3>Add new card</h3>
            </header>
            <div className='pay-now-section'>
              <PayNowAddCardForm
              onSubmit={this.handleSubmit.bind(this)}
              />
            </div>
          </div>
        </div>
          }

            <div className='col-one-of-two'>
              <div className='widget'>
                <header className='heading'>
                  <h3>PAYMENT INFORMATION</h3>
                </header>
                <div className='pay-now-section'>
                  <div className='pay-now-section-inner'>
                    <div className='pay-now-content'>
                      <p>Your team is on the <strong>{this.props.billingDetail.plan}.</strong><br/> Your plan will renew on <strong>{this.props.billingDetail.bill_date}.</strong><br/> You currently owe <strong>&#163; {this.props.billingDetail.amount_owe}.</strong><br/> You are paying for <strong>{this.props.billingDetail.active_user} users.</strong></p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        </SliderPopup>
      </div>
    )
  }
}
function mapStateToProps (state) {
  return {
    billing: state.billing,
  }
}
function mapDispatchToProps (dispatch) {
  return {
    billingActions: bindActionCreators(billingActions, dispatch),
  }
}
let connection = connect(mapStateToProps, mapDispatchToProps)
var reduxConnectedComponent = connection(PayNowPopup)

export default reduxForm({
  form: 'PayNowPopup' // a unique identifier for this form
})(reduxConnectedComponent)
