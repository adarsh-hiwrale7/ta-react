import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import PreLoader from '../../PreLoader';
import * as settingsActions from "../../../actions/settingsActions";
import notie from "notie/dist/notie.js";
import * as loginActions from "../../../actions/loginActions";
import SettingsNav from '../SettingsNav';
import {Link} from 'react-router';
import Header from '../../Header/Header';
import BillingNav from './BillingNav';
import Globals from '../../../Globals';
import moment from '../../Chunks/ChunkMoment';
import * as utils from '../../../utils/utils'
var restrictAPI=false;
export default class InVoice extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      show_menu: true,
      userAction: null,
      loading: false,
      moment: null,
      mounted: true,
    }
  }

  componentWillMount () {
    var me = this

    moment().then(moment => {
      if (me.state.mounted) {
        me.setState({ moment: moment })
      }
    })

  }
componentDidMount() {
  if(restrictAPI!==true){
    this.setState({loading: true});
    //get the team changes data from api
    fetch(Globals.API_ROOT_URL + `/user/action`,{
      headers: utils.setApiHeaders('get'),
    })
      .then(response => response.json())
      .then(json => {
        if (Array.isArray(json.data) && json.data.length > 0) {
          this.setState({userAction: json.data, loading: false});
        }
          else{
            this.setState({loading: false});
          }
      })
      .catch(err => {
        throw err
      })
    restrictAPI=true;
    setTimeout(()=>{
      restrictAPI=false
    },5000)
  }
    

  }

  render() {
    var date;
    return (

      <section id="campaigns" className="settings">
        <Header
          title="Team changes"
          nav={SettingsNav}
          header_location
          ={this.props.location}
          location={this.props.location}
          />

        <div className='main-container'>
          <div id="content">

            <div className="page campaign-container">

              <div class="full-container">

                <div className="widget">
                  <ul className="responsive_menu">
                    <li>
                      <Link
                        activeClassName={this.props.main_page
                        ? 'active'
                        : ''}
                        to='/settings/billing/'>

                        <span className='text'>Overview</span>
                        <p className="responsive-billing-icon"><i class="material-icons">&#xE313;</i></p>

                      </Link>
                    </li>
                    <li>
                      <Link to='/settings/billing/invoice' activeClassName='active'>
                        <span className='text'>Invoices</span>
                        <p className="responsive-billing-icon"><i class="material-icons">&#xE313;</i></p>

                      </Link>
                    </li>
                    <li>
                      <Link to='/settings/billing/settingcontact' activeClassName='active'>
                        <span className='text'>Contacts</span>
                        <p className="responsive-billing-icon"><i class="material-icons">&#xE313;</i></p>


                      </Link>
                    </li>
                    <li>
                      <Link to='/settings/billing/teamchange' activeClassName='active'>
                        <span className='text'>Team changes</span>
                        <p className="responsive-billing-icon"><i class="material-icons">&#xE313;</i></p>

                      </Link>
                    </li>
                  </ul>
                  <section className='billing-page'>
                    <BillingNav/>
                    <div className='team-content-wrapper'>
                      <div className='team-top-content'>
                        <p>This page lists each change in your workspace that affects your account
                          billing (for example, adding a member, or noting that a member has become
                          inactive). For more information visit our Guide to billing at Visibly.
                        </p>
                      </div>
                    </div>
                    <div className="team-table-wrapper">
                    {this.state.loading==true?
                        <PreLoader location="teamChange-loader"/>
                    :
                      <table className="table">
                        <thead>
                          <tr>
                            <th className="team-date-col">Date</th>
                            <th className="team-change-col">Team Change</th>
                          </tr>
                        </thead>
                        <tbody className='reactable-data'>
                          {
                             (this.state.userAction !== null && this.state.userAction.length > 0) 
                              ? this.state.userAction.map((userAction, i) => {
                                        date = this.state
                                          .moment?
                                          this.state
                                          .moment.tz(userAction.date.date, Globals.SERVER_TZ)
                                          .format("MM/DD/YYYY"):''
                                        return(
                                        <tr key={i}>
                                          <td data-rwd-label="Date">{(userAction.date !== null)
                                              ? date
                                              : ''}</td>
                                          <td data-rwd-label="Team Change">{userAction.message}</td>
                                        </tr>
                                        )})
                              : <tr>
                                  <td data-rwd-label="Date" colSpan='2'>
                                    <span>No action found</span>
                                  </td>
                                </tr>
}
                        </tbody>
                      </table>
                    }
                    </div>

                  </section>

                  <ul className="responsive_menu">
                    <li>
                      <Link to='/settings/billing/paymentmethod' activeClassName='active'>
                        <span className='text'>Payment methods</span>
                        <p className="responsive-billing-icon"><i class="material-icons">&#xE313;</i></p>
                     </Link>
                    </li>
                  </ul>
                </div>
                <div className="billing-footer">Questions?
                  <a href="#" title="Visit our guide">Visit our guide to billings at Visibly.</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}