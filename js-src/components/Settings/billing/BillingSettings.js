import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import PreLoader from '../../PreLoader'
import * as settingsActions from "../../../actions/settingsActions"
import notie from "notie/dist/notie.js"
import * as loginActions from "../../../actions/loginActions"
import SliderPopup from '../../SliderPopup'
import SettingsNav from '../SettingsNav';
import {Link} from 'react-router';
import * as billingActions from '../../../actions/BillingActions';
import {Field, reduxForm} from 'redux-form';
import * as utils from '../../../utils/utils'
import Header from '../../Header/Header';
import BillingNav from './BillingNav';
import PayNowPopup from './PayNowPopup';
import UpdatePlanForm from './UpdatePlanForm';
import CenterPopupWrapper from '../../CenterPopupWrapper'

var restrictAPI=false;
class BillingSettings extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      currentFilter:'',
      show_menu: true,
      show_paynow_popup:false,
      add_new_card:false,
      openChangePlanPopup: false
    }
    // this.open_paynow_popup = this.open_paynow_popup.bind(this);
  }
  closeAssetDetailPopup() {

    this.setState({ show_paynow_popup: false })
    document.body.classList.remove('overlay');

  }


  overview_billing() {
    var blling = document.getElementsByClassName("overview-page");
    blling
      .classList
      .toggle("mystyle");
  }
  handleDeptCreation() {
    this.setState({createDeptPopup: true});

    //  this.props.departmentActions.creatingNewDept();
  }

  openChangePlanPopup(){
    this.setState({openChangePlanPopup: true});
  }

  closeChangePlanPopupParent(){
    this.setState({openChangePlanPopup: false});
  }

  componentDidMount() {

    if(restrictAPI!==true){
        this
        .props
        .billingActions
        .getDetail();
      //this.props.billingActions.getCards();
      this.props.billingActions.getAllPlan();
      restrictAPI=true;
      setTimeout(()=>{
        restrictAPI=false
      },5000)
    }
  }

  componentWillReceiveProps(newProps){
      if(newProps.billing.proceesToPayment !== this.props.billing.proceesToPayment){
        if(this.pr.billing.proceesToPayment === true){
            this.setState({
             show_paynow_popup:true

            });
            document.body.classList.add('overlay');
        }
      }

      if(newProps.billing.planUpdated !== this.props.billing.planUpdated && newProps.billing.planUpdated === true){
         this.closeChangePlanPopupParent();
         this.componentDidMount();
      }

      if(newProps.billing.closePayNowPopup !== this.props.billing.closePayNowPopup){
         this
          .props
          .billingActions
          .getDetail();
        //  this.props.billingActions.getCards();
      }
  }

open_paynow_popup = () =>
{

  //this.props.billingActions.checkDetailsOfBilling();
  // console.log(this.props.billingActions.checkDetailsOfBilling());
  //   console.log(this.props.billingActions.checkDetailsOfBillingSuccess());
  /*this.setState({
      show_paynow_popup:true
  });
  document.body.classList.add('overlay');*/


  if(this.props.billing.proceesToPayment == true)
  {
    this.setState({
      show_paynow_popup : true
    })
  }
  document.body.classList.add('overlay');
}
componentWillMount() {

  this.props.billingActions.checkDetailsOfBilling();
  if (typeof this.props.location !== 'undefined') {
    var newLocation = utils
      .removeTrailingSlash(this.props.location.pathname)
      .split('/')
    var location = newLocation[2] == 'billing' ? 'overview' : newLocation[2]
    this.setState({
      currentFilter: location,
    })
  }
}

handleSubmit(val){
  this.props.billingActions.payNow(val);
}
  open_main_menu() {
    this.setState({
      show_menu: !this.state.show_menu
    })
    var active_class = this.state.show_menu
      ? "in-view has-sib"
      : " has-sib";
    var handle_active_class = this.state.show_menu
      ? 'active inner_main_menu_handle'
      : 'inner_main_menu_handle';
    document
      .getElementById('navdraw')
      .className = active_class;
    document
      .getElementById('inner_main_menu_handle')
      .className = handle_active_class;

  }
  handleSubmitPopUp(val){
      this.props.billingActions.updatePlan(val);
  }
  render() {

    return (

      <div>

        <section id="campaigns" className="settings">

          {(typeof this.props.billing.detail !== "undefined" && this.props.billing.loading === true)
            ? <PreLoader  className = "Page"/>
            : null}

          <Header title="Overview" nav={SettingsNav} location ={this.props.location}/>

          {this.state.show_paynow_popup ?
          <PayNowPopup
          billingDetail={this.props.billing.detail}
          closeAssetDetailPopup={this.closeAssetDetailPopup.bind(this)}
          cardDetail={this.props.billing.paymentCard}
          onSubmit={this.handleSubmit.bind(this)}
          amount={this.props.billing.detail.amount_owe}
          />
          : ''}
          <div className='main-container'>
            <div id="content">

              <div className="page campaign-container">

                <div className="full-container">
                  <div className="widget">

                    <section className='billing-page'>
                      <BillingNav main_page={true}/>
                      <ul className="responsive_menu">
                      <li>
                      <Link
                        activeClassName='active'
                        to='/settings/billing/'>

                        <span className='text'>Overview</span>
                        <p className="responsive-billing-icon"><i class="material-icons">&#xE5C5;</i></p>

                      </Link>
                      </li>
                      </ul>

                      <div className="text-header overview-page">

                        {(this.state.openChangePlanPopup === true) ? 

                   <CenterPopupWrapper>
                             <UpdatePlanForm  
                              onSubmit={this.handleSubmitPopUp.bind(this)} 
                              billingDetail={this.props.billing.detail} 
                              plan={this.props.billing.planData}
                              closeChangePlanPopupParent={this.closeChangePlanPopupParent.bind(this)}
                              />  
                    </CenterPopupWrapper>
                        :""}

                        <div className="inner-text-header">

                          <p>
                            Your team is on the
                            <span className="biiling-bold"> {this.props.billing.detail.plan} plan</span>. Your plan will renew on {this.props.billing.detail.bill_date} for {this.props.billing.detail.currency} {this.props.billing.detail.payable_amount}.<span className="text-billing">
                              <a onClick={this.openChangePlanPopup.bind(this)}>(Change plan)</a>
                            </span>
                          </p>
                          <div className='billing-title'>ACCOUNT SETTINGS</div>
                          <p>
                            You currently owe
                            <span className="biiling-bold"> {this.props.billing.detail.currency} {this.props.billing.detail.amount_owe}. {this.props.billing.detail.amount_owe > 0 ? <a href="javascript:void(0);" title="Pay Now" onClick={this.open_paynow_popup}>Pay Now</a> : '' }</span><br/>
                            Future charges will be billed to your Visa card ending in <span className="biiling-bold">
                              {this.props.billing.detail.card_number}
                            </span>
                            <span className="text-billing">
                              <Link to='/settings/billing/paymentmethod'>(Manage payment method)</Link>
                            </span>.<br/>
                            You are paying for <span className="biiling-bold">
                              {this.props.billing.detail.active_user} users</span>
                              {this.props.billing.detail.active_guest > 0 ? 
                                   <span> and <span className="biiling-bold">{this.props.billing.detail.active_guest} guest</span></span>
                              :''}    
                            .<br/>
                            Billing emails are sent to
                            <span className="biiling-bold">
                              {(typeof this.props.billing.detail.billing_emails !== "undefined")
                                ? this.props.billing.detail.billing_emails.map((emails, i) => (
                                  <span key={i}>
                                     {" "+emails}
                                    {(this.props.billing.detail.billing_emails.length != (parseInt(i) + 1))
                                      ? <span>,</span>
                                      : ''}
                                  </span>
                                ))
                                : ''
                               }
                            </span>
                            <span className="text-billing">
                              <Link to='/settings/billing/settingcontact'>(Manage billing contacts)</Link>
                            </span>
                          </p>

                          <div className='billing-title'>BILLING ACTIONS</div>

                          <p className="text-billing">
                            <Link to='/settings/billing/teamchange'>See a detailed breakdown of billing changes</Link><br/>
                            <Link to="/settings/billing/settingcontact">Edit company name and address included on invoices</Link>
                          </p>
                        </div>

                      </div>

                      <ul className="responsive_menu">
                        <li>
                          <Link to='/settings/billing/invoice' activeClassName='active'>
                            <span className='text'>Invoices</span>
                          <p className="responsive-billing-icon"><i class="material-icons">&#xE5C5;</i></p>

                          </Link>
                        </li>
                        <li>
                          <Link to='/settings/billing/settingcontact' activeClassName='active'>
                            <span className='text'>Contacts</span>
                          <p className="responsive-billing-icon"><i class="material-icons">&#xE5C5;</i></p>

                          </Link>
                        </li>
                        <li>
                          <Link to='/settings/billing/teamchange' activeClassName='active'>
                            <span className='text'>Team changes</span>
                          <p className="responsive-billing-icon"><i class="material-icons">&#xE5C5;</i></p>

                          </Link>
                        </li>
                        <li>
                          <Link to='/settings/billing/paymentmethod' activeClassName='active'>
                            <span className='text'>Payment methods</span>
                          <p className="responsive-billing-icon"><i class="material-icons">&#xE5C5;</i></p>

                          </Link>
                        </li>
                      </ul>
                    </section>

                  </div>
                  <div className="billing-footer">Questions?
                    <a href="https://www.visibly.io/support/billing" target="_blank" title="Visit our guide">Visit our guide to billings at Visibly.</a>
                  </div>

                </div>
              </div>
            </div>
          </div>

        </section>
      </div>

    )
  }

}

function mapStateToProps(state) {
  return {billing: state.billing}
}
function mapDispatchToProps(dispatch) {
  return {
    billingActions: bindActionCreators(billingActions, dispatch)
  }
}
let connection = connect(mapStateToProps, mapDispatchToProps)
var reduxConnectedComponent = connection(BillingSettings)

export default reduxForm({
  form: 'BillingSettings' // a unique identifier for this form
})(reduxConnectedComponent)
