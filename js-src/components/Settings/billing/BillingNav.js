import Collapse, {Panel} from 'rc-collapse'; // collapse
// collapse css
import {IndexLink, Link} from 'react-router';
import * as utils from '../../../utils/utils';

class BillingNav extends React.Component {

  render() {
    return (
      <div>
        <div className='listMenu billng-menu'>
          <nav className='nav-side'>
            <ul className='parent clearfix'>
              <li>
                {/* <Link
              className={this.state.currentFilter == 'overview'? 'active'
                          : ''
                      }
                to='/settings/billing/invoice'
               // activeClassName='active'
              >

                <span className='text'>Overview</span>
              </Link> */}
                <Link
                  activeClassName={this.props.main_page
                  ? 'active'
                  : ''}
                  to='/settings/billing/'>

                  <span className='text'>Overview</span>
                </Link>
              </li>
              <li>
                <Link to='/settings/billing/invoice' activeClassName='active'>
                  <span className='text'>Invoices</span>
                </Link>
              </li>
              <li>
                <Link to='/settings/billing/settingcontact' activeClassName='active'>
                  <span className='text'>Contacts</span>
                </Link>
              </li>
              <li>
                <Link to='/settings/billing/teamchange' activeClassName='active'>
                  <span className='text'>Team changes</span>
                </Link>
              </li>
              <li>
                <Link to='/settings/billing/paymentmethod' activeClassName='active'>
                  <span className='text'>Payment methods</span>
                </Link>
              </li>

            </ul>
          </nav>
        </div>
      </div>
    )
  }
}
module.exports = BillingNav;