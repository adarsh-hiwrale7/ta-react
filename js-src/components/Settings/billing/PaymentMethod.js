import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import PreLoader from '../../PreLoader'
import notie from "notie/dist/notie.js"
import SettingsNav from '../SettingsNav';
import { Link } from 'react-router';
import { Field, reduxForm } from 'redux-form';
import { renderField } from '../../Helpers/ReduxForm/RenderField'
import * as billingActions from '../../../actions/BillingActions';
import PaymentMethodForm from './PaymentMethodForm';
import PaymentMethodUpdateForm from './PaymentMethodUpdateForm';
import BillingNav from './BillingNav';
import Header from '../../Header/Header';
class PaymentMethod extends React.Component {
    constructor(props) {
    super(props);
    this.state = {
        editForm : false,
        show_menu: true,
        cardDetail: null,
        billingDetail:{},
        openChargeBeePopup: false,
        isCardPopupFetched:false
    }
  }
    componentWillReceiveProps(newProps){
      if(newProps.billing.paymentCard !== this.props.billing.paymentCard){
         this.setState({billingDetail:newProps.billing.paymentCard,editForm:false,cardDetail:null});
      }
      if(newProps.billing.addCardHostedPageLink !== ''){
          this.setState({openChargeBeePopup:true, });
      }
  }
  componentDidMount(){
      this.props.billingActions.getCards();
  }
  handleSubmit(value){
    this.props.billingActions.addCard(value);
    
  }
  removeCard(cardId){
      this.props.billingActions.removeCard(cardId);
  }
  updateCard(cardId,index,cardDetail){
    this.setState({editForm:cardId,cardDetail:cardDetail});
  }
  handleUpdateSubmit(val){
       this.props.billingActions.updateCard(val);
  }
  cancelCardUpdateForm(){
      this.setState({editForm:false,cardDetail:null})
  }
  closeChargebeePopup(){
    this.setState({openChargeBeePopup:false})
    this.props.billingActions.removeAddCardHostedPageUrl();
    this.props.billingActions.getCards();
  }
  manageCardClick(){
    this.setState({
      isCardPopupFetched:true
    })
      this.props.billingActions.getAddCardHostedPageUrl();
  }
  /**
   * @author disha 
   * when iframe is loaded in page then to stop loader
   */
  iFrameLoad(){
    this.setState({
      isCardPopupFetched:false
    })
  }
 render() {
  return (
     <section id="campaigns" className="settings">
       {(typeof this.props.billing.paymentCard !== "undefined" && this.props.billing.fetchCardLoading === true ) || this.state.isCardPopupFetched==true ? <PreLoader  className = "Page"/> : null }  

       <Header title="Payment methods" nav={SettingsNav} header_location ={this.props.location} location={this.props.location}/>
        <div className='main-container'>
          <div id="content">

            <div className="page campaign-container">

                  <div class="full-container">
                    <div className="widget">
                    
                       <section className='billing-page'>
                        <BillingNav />

                        <ul className="responsive_menu">
              <li>
               
                <Link
                  activeClassName={this.props.main_page
                  ? 'active'
                  : ''}
                  to='/settings/billing/'>

                  <span className='text'>Overview</span>
                  <p className="responsive-billing-icon"><i class="material-icons">&#xE313;</i></p>
                </Link>
              </li>
              <li>
                <Link to='/settings/billing/invoice' activeClassName='active'>
                  <span className='text'>Invoices</span>
                  <p className="responsive-billing-icon"><i class="material-icons">&#xE313;</i></p>
                </Link>
               
              </li>
              <li>
                <Link to='/settings/billing/settingcontact' activeClassName='active'>
                  <span className='text'>Contacts</span>
                  <p className="responsive-billing-icon"><i class="material-icons">&#xE313;</i></p>
                </Link>
                
              </li>
              <li>
                <Link to='/settings/billing/teamchange' activeClassName='active'>
                  <span className='text'>Team changes</span>
                  <p className="responsive-billing-icon"><i class="material-icons">&#xE313;</i></p>
                </Link>
              </li>
              <li>
                <Link to='/settings/billing/paymentmethod' activeClassName='active'>
                  <span className='text'>Payment methods</span>
                  <p className="responsive-billing-icon"><i class="material-icons">&#xE313;</i></p>
                </Link>
              </li>
          </ul>
                        <div className="billing-payments-cols clearfix">
                            <div className="billing-payment-col payment-left-col">
                               {/*<PaymentMethodForm onSubmit={this.handleSubmit.bind(this)}  /> */}
                               <button class="btn btn-primary" type="button" onClick={this.manageCardClick.bind(this)}>Manage Cards</button>                   
                            </div>
                            { this.state.openChargeBeePopup === true ? 
                              <div>
                                <div class="chargebeepopOverlay" onClick={this.closeChargebeePopup.bind(this)}></div>
                                <div class="chargebeepopUp">
                                  <a href="javascript:void(0);" class="closeChargebeePopup" onClick={this.closeChargebeePopup.bind(this)}><i class="material-icons close-icon">close</i></a>
                                  <iframe src={this.props.billing.addCardHostedPageLink} height="300" width="100" id="test" onLoad={this.iFrameLoad.bind(this)} />
                                </div>
                              </div>
                            : ''}
                            <div className="billing-payment-col payment-right-col">
                              <div className="billing-pament-col-inner">
                              <div className="bill-right-top-content">
                                <div className="billing-title">Existing Cards</div>
                                <div className="billing-content">
                                    <p>These are all the cards we currently have on record for your organisation.</p>
                                  </div>
                                  </div>
                                <div className="existing-card-list">
                                  {(typeof this.props.billing.paymentCard !== "undefined" && Object.keys(this.state.billingDetail).length > 0) ? 
                                  this.state.billingDetail.map((card, i) => (
                                  <div className="existing-card-item" key={i}>
                                    {this.state.editForm !== false && this.state.editForm === card.identity ? 
                                      <div className="bill-card-holder"  id={"form_"+i}>
                                        <PaymentMethodUpdateForm 
                                          onSubmit={this.handleUpdateSubmit.bind(this)} pay_card_index={i} pay_card_name={card.card_name} pay_card_number={(card.card_number.toString()).slice(-4)} pay_card_exp_month={card.exp_month} pay_card_exp_year={card.exp_year} cardIdentity={this.state.editForm} cardData={this.state.cardDetail} cancelCardUpdateForm={this.cancelCardUpdateForm.bind(this)}  /></div>
                                    :
                                    <div>   
                                      <div className="bill-card-holder bill-card-holder-name" id={"name_"+i}>{card.card_name}</div>
                                      <div className="bill-card-detail">Visa ending in {(card.card_number.toString()).slice(-4)}  - {card.exp_month} / {card.exp_year}</div>
                                    </div>
                                    }
                                    
                                    {(card.default === 1) ? <div className="payment-type-text">Default</div> : '' }
                                    
                                  </div>
                                   )) : 'No card found, Please add new card.'}
                                </div>
                              </div>
                            </div>
                        </div>
                      </section>
                
                       
            </div><div className="billing-footer">Questions? <a href="#" title="Visit our guide">Visit our guide to billings at Visibly.</a></div>
          </div>  </div> </div></div>
        
      </section>
 );
  }
}

function mapStateToProps (state) {
  return {
    billing: state.billing,
  }
}
function mapDispatchToProps (dispatch) {
  return {
    billingActions: bindActionCreators(billingActions, dispatch),
  }
}
let connection = connect(mapStateToProps, mapDispatchToProps)
var reduxConnectedComponent = connection(PaymentMethod)

export default reduxForm({
  form: 'PaymentMethod' // a unique identifier for this form
})(reduxConnectedComponent)


  
