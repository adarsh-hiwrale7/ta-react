import { Link } from "react-router";
import { browserHistory } from "react-router";
import * as billingActions from '../../../actions/BillingActions';
import { connect } from 'react-redux'; 
import { bindActionCreators } from 'redux';
import { Field, reduxForm } from 'redux-form';
import { renderField } from '../../Helpers/ReduxForm/RenderField';
const required = value => value
  ? undefined
  : 'Required';




class PaymentMethodForm extends React.Component {
			
	constructor(props){
		super(props);
		const {submitting,reset} = props;
		
		//this.handleView = this.handleView.bind(this);
		
	}
	
	
	render(){
		
		
		const {
	      handleSubmit,
	      pristine
    	} = this.props
		return  (
			
				<div className="billing-pament-col-inner">
                        <div className="bill-left-top-content">
                            <div className="billing-title">Add New Card</div>
                            <div className="billing-content">
                                <p>You can add up to 3 credit cards.</p>
                            </div>
                        </div>
                <form className="bill-payment-form clearfix" onSubmit={handleSubmit}>
                        <div className="form-row  text"><Field placeholder="Name on card" label="Name On Card" component={renderField}  type="text" name="card_name" validate={[required]} /><div></div></div>
                        <div className="form-row  text"><Field placeholder="Card number" label="Card Number" component={renderField}  type="text" name="card_number" validate={[required]} /><div></div></div>
                                  <div className="text">
                                  	<label>Expiry</label>
                                  	<div className='row'>
                                  		<div className='col-one-of-two form-row'><Field placeholder="MM" component={renderField}  type="text" name="exp_month" validate={[required]} /></div>
                                  		<div className='col-one-of-two form-row'><Field placeholder="YYYY" component={renderField}  type="text" name="exp_year" validate={[required]} /></div>
                                  	</div>
                                  	<div></div></div>
                                  <div className="cvv-zip-wrapper">
                                  <div className="clearfix">
                                  <Field placeholder="cvv" label="CVV" component={renderField}  type="text" name="card_cvc" validate={[required]} /><div></div>
                                  <Field placeholder="Zip code" label="Zip Code (US only)" component={renderField}  type="text" name="zip_code" /><div></div>
                                  </div>
                                  </div>
                                  
                                  <div className="form-row"><button class="btn btn-primary" type="submit"><span>+</span> Add Card</button></div> 
                                  <div className="billing-contact-note">Your card will not be charged at this time, but will be set as the 
default for future charges.</div>
                </form>
               	</div>
			
		);
	}
}
	
    function mapStateToProps (state, ownProps) {
	  return {}
	}

	function mapDispatchToProps (dispatch) {
	  return {
	  		 
	  }
	}
	let connection = connect(mapStateToProps, mapDispatchToProps)

	var reduxConnectedComponent = connection(PaymentMethodForm)

	export default reduxForm({
	  form: 'PaymentMethodForm', // a unique identifier for this form
	})(reduxConnectedComponent)

   