import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import PreLoader from '../../PreLoader'
import * as billingActions from "../../../actions/BillingActions"
import SidebarNarrow from '../../Layout/Sidebar/SidebarNarrow'
import notie from "notie/dist/notie.js"
import moment from '../../Chunks/ChunkMoment';
import SettingsNav from '../SettingsNav';
import {Link} from 'react-router';
import Header from '../../Header/Header';
import BillingNav from './BillingNav';
import Globals from '../../../Globals';
import * as utils from '../../../utils/utils';
import { Field, reduxForm } from 'redux-form';
var encryptedSessionId = '';
var restrictAPI=false;
class InVoice extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      show_menu: true,
      moment: null,
    }
  }
  componentWillMount () {
    moment().then(moment => {
      this.setState({ moment: moment })
    })
  }
  componentDidMount(){
    if(restrictAPI!==true){
      this.props.billingActions.fetchInvoice();
      restrictAPI=true;
      setTimeout(() => {
        restrictAPI=false
        },5000);
    }
    encryptedSessionId=utils.encodeSessionId(Globals.SESSION_ID);

  }
  downloadInvoice(invoiceId){
    this.props.billingActions.downloadInvoice(invoiceId);
  }
  render() {
    return (
      <section id="campaigns" className="settings">

        <Header
          title="Invoices"
          nav={SettingsNav}
          header_location={this.props.location}
          location={this.props.location}
          ></Header>
        <div className='main-container'>
          <div id="content">

            <div className="page campaign-container">
              <div class="full-container">
                <div className="widget">

                  <section className='billing-page'>
                    <BillingNav/>
                    <ul className="responsive_menu">
                      <li>
                        <Link
                          activeClassName={this.props.main_page
                          ? 'active'
                          : ''}
                          to='/settings/billing/'>

                          <span className='text'>Overview</span>
                          <p className="responsive-billing-icon"><i class="material-icons">&#xE313;</i></p>
                        </Link>

                      </li>
                      <li>
                        <Link to='/settings/billing/invoice' activeClassName='active'>
                          <span className='text'>Invoices</span>
                          <p className="responsive-billing-icon"><i class="material-icons">&#xE313;</i></p>
                        </Link>

                      </li>
                    </ul>
                    <div className="invoice-page">
                      <div className='invoice-table-wrapper'>
                      {this.props.billing.invoiceLoading==true?
                      <PreLoader location="invoices-loader"/>
                      :
                        <table className="table responsive-table">
                          <thead>
                            <tr>
                              <th>From</th>
                              <th>To</th>
                              <th>Item</th>
                              <th>Plan</th>
                              <th>Charge</th>
                              <th className="view-detail-col actiondiv">Actions</th>
                            </tr>
                          </thead>
                          <tbody className='reactable-data'>
                            {this.props.billing.invoiceList.length > 0 ?
                              this.props.billing.invoiceList.map((invoice, i) => {
                              var paidFor = '';
                              var startDate = this.state.moment?
                                          this.state.moment(invoice.start_date).format("MM/DD/YYYY")
                                        :''
                              var endDate = this.state.moment?
                                          this.state.moment(invoice.end_date).format("MM/DD/YYYY")
                                        :''
                              var attCount = invoice.paymentAmount.data.length - 1;
                              invoice.paymentAmount.data.map((paymentAttributes, i) => {
                                  paidFor = paidFor + paymentAttributes.description;
                                  (i !== attCount) ?  paidFor = paidFor + " + " : ''

                              })
                              var currencySign = invoice.plan.data.currency_sign;

                            return (<tr key={i}>
                              <td data-rwd-label='From'>{startDate}</td>
                              <td data-rwd-label='To'>{endDate}</td>
                              <td data-rwd-label='Item' className = "billing-item-in-invoice">{paidFor}</td>
                              <td data-rwd-label='Plan'>{invoice.plan.data.name}</td>
                              <td data-rwd-label='Charge'>{currencySign}{invoice.amount}</td>
                              <td className="actiondiv">
                                <div className='action-button'>
                                  <ul className='dd-menu context-menu user_3dots'>
                                    <li className='button-dropdown'>
                                      <a className='dropdown-toggle user_drop_toggle'>
                                        <i className='material-icons'>more_vert</i>
                                      </a>
                                      <ul className='dropdown-menu user_dropAction'>
                                        <li>
                                          <a target="_blank" href={Globals.API_ROOT_URL + '/billing/invoice/'+invoice.identity+'?session_id='+encryptedSessionId}>
                                          <span className='btn' onClick={this.downloadInvoice.bind(this,invoice.identity)}>
                                            Download{' '}
                                          </span>
                                          </a>
                                        </li>

                                      </ul>
                                    </li>
                                  </ul>
                                </div>
                              </td>
                            </tr>)
                           }):<tr  className = "no-invoice-found"><td colSpan="6"> <p className = "no-invoice-found"> No invoice found.</p></td> </tr>}
                          </tbody>
                        </table>}
                      </div>
                    </div>
                    <ul className="responsive_menu">
                      <li>
                        <Link to='/settings/billing/settingcontact' activeClassName='active'>
                          <span className='text'>Contacts</span>
                          <p className="responsive-billing-icon"><i class="material-icons">&#xE313;</i></p>
                        </Link>

                      </li>
                      <li>
                        <Link to='/settings/billing/teamchange' activeClassName='active'>
                          <span className='text'>Team changes</span>
                          <p className="responsive-billing-icon"><i class="material-icons">&#xE313;</i></p>
                        </Link>

                      </li>
                      <li>
                        <Link to='/settings/billing/paymentmethod' activeClassName='active'>
                          <span className='text'>Payment methods</span>
                          <p className="responsive-billing-icon"><i class="material-icons">&#xE313;</i></p>
                        </Link>

                      </li>
                    </ul>
                  </section>

                </div>
                <div className="billing-footer">Questions?
                  <a href="#" title="Visit our guide">Visit our guide to billings at Visibly.</a>
                </div>

              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
  open_main_menu() {
    this.setState({
      show_menu: !this.state.show_menu
    });
    var active_class = this.state.show_menu
      ? "in-view has-sib"
      : " has-sib";
    var handle_active_class = this.state.show_menu
      ? 'active inner_main_menu_handle'
      : 'inner_main_menu_handle';
    document
      .getElementById('navdraw')
      .className = active_class;
    document
      .getElementById('inner_main_menu_handle')
      .className = handle_active_class;

  }
}

function mapStateToProps (state) {
  return {
    billing: state.billing,
  }
}
function mapDispatchToProps (dispatch) {
  return {
    billingActions: bindActionCreators(billingActions, dispatch),
  }
}
let connection = connect(mapStateToProps, mapDispatchToProps)
var reduxConnectedComponent = connection(InVoice)

export default reduxForm({
  form: 'InVoice' // a unique identifier for this form
})(reduxConnectedComponent)
