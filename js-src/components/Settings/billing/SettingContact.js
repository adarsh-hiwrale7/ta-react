import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import PreLoader from '../../PreLoader'
import * as settingsActions from "../../../actions/settingsActions"
import notie from "notie/dist/notie.js"
import * as loginActions from "../../../actions/loginActions"
import SettingsNav from '../SettingsNav';
import {Link} from 'react-router';
import {Field, reduxForm} from 'redux-form';
import {renderField} from '../../Helpers/ReduxForm/RenderField'
import * as billingActions from '../../../actions/BillingActions';
import SettingContactForm from './SettingContactForm';
import SettingContactDetailForm from './SettingContactDetailForm';
import Header from '../../Header/Header';
import BillingNav from './BillingNav';
var restrictAPI=false;
class SettingContact extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      show_menu: true
    }
  }
  componentDidMount() {
    
    if(restrictAPI!==true){
      this
      .props
      .billingActions
      .getContactEmail();
      this
        .props
        .billingActions
        .getCountry();
      this
        .props
        .billingActions
        .getContactDetail();
    
      restrictAPI=true;
      setTimeout(()=>{
        restrictAPI=false
      },5000)
    }
  }

  handleSubmit(values) {
    this
      .props
      .billingActions
      .addContactEmail(values);
  }

  changeCountry(val) {
    this
      .props
      .billingActions
      .fetchState(val);
  }
  changeState(val) {
    this
      .props
      .billingActions
      .fetchCity(val);
  }

  handleDetailSubmit(values) {
    this
      .props
      .billingActions
      .updateContactDetail(values);
  }

  removeContact(contactId) {
    this
      .props
      .billingActions
      .removeContactEmail(contactId);
  }

  render() {

    return (

      <section id="campaigns" className="settings">
        {((typeof this.props.billing.billState !== "undefined" && this.props.billing.stateLoading === true) ||( typeof this.props.billing.city !== "undefined" && this.props.billing.cityLoading === true) || (this.props.billing.updateDetailLoading === true))
          ? <PreLoader  className = "Page"/>
          : null}
        {/* {(typeof this.props.billing.city !== "undefined" && this.props.billing.cityLoading === true)
          ?<PreLoader  className = "Page"/>
          : null}
        {(this.props.billing.updateDetailLoading === true)
          ? <PreLoader  className = "Page"/>
          : null} */}

        <Header
          title="Contacts"
          nav={SettingsNav}
          header_location
          ={this.props.location}
          location={this.props.location}
          />
        <div className='main-container'>
          <div id="content">

            <div className="page campaign-container">

              <div class="full-container">
                <div className="widget">

                  <section className='billing-page'>
                    <BillingNav/>
                    <ul className="responsive_menu">
                      <li>
                        <Link
                          activeClassName={this.props.main_page
                          ? 'active'
                          : ''}
                          to='/settings/billing/'>

                          <span className='text'>Overview</span>
                          <p className="responsive-billing-icon"><i class="material-icons">&#xE313;</i></p>

                        </Link>
                      </li>
                      <li>
                        <Link to='/settings/billing/invoice' activeClassName='active'>
                          <span className='text'>Invoices</span>
                          <p className="responsive-billing-icon"><i class="material-icons">&#xE313;</i></p>

                        </Link>
                      </li>
                      <li>
                        <Link to='/settings/billing/settingcontact' activeClassName='active'>
                          <span className='text'>Contacts</span>
                          <p className="responsive-billing-icon"><i class="material-icons">&#xE313;</i></p>

                        </Link>
                      </li>
                    </ul>
                    <div className="billing-contacts-cols clearfix">
                      <div className="billing-contact-col billing-left-col">
                        <div className="billing-contact-col-inner">
                          <header className='heading'>
                            <h3>Company Name &amp; Address</h3>
                          </header>
                          <div className="billing-contact-content">
                            <p>This information will be included on all billing invoices on your account.</p>
                          </div>
                          <SettingContactDetailForm
                            onSubmit={this
                            .handleDetailSubmit
                            .bind(this)}
                            country={this.props.billing.country}
                            changeCountry={this
                            .changeCountry
                            .bind(this)}
                            state={this.props.billing.billState}
                            changeState={this
                            .changeState
                            .bind(this)}
                            city={this.props.billing.city}
                            contactDetail={this.props.billing.contactDetail}/>
                        </div>
                      </div>
                      <div className="billing-contact-col billing-right-col">
                        <div className="billing-contact-col-inner">
                          <div className="billing-right-top-content">
                            <header className='heading'>
                              <h3>Invoice Contacts</h3>
                            </header>
                            <div className="billing-contact-content">
                               {(typeof this.props.billing.email !== "undefined" && this.props.billing.email.length > 0)
                                ? <p>Billing emails will be sent to {this
                                      .props
                                      .billing
                                      .email
                                      .map((emails, i) => (
                                        <a href={"mailto:" + emails.email_id} title={emails.email_id} key={i}>
                                          {emails.email_id}
                                          {(this.props.billing.email.length != (parseInt(i) + 1))
                                            ? <span>,</span>
                                            : ''}
                                        </a>
                                      ))}
                                  </p>
                                : <p>We cannot find any contact to send invoice, please add contact to receive
                                  invoice</p>
}

                              <p>You may add additional billing contacts (for example, your accounting
                                department). All billing-related emails will be sent to this list of contacts.</p>
                            </div>
                          </div>
                          <SettingContactForm
                            onSubmit={this
                            .handleSubmit
                            .bind(this)}/>
                          <div className="billin-contact-table-wrapper">
                          {
                            this.props.billing.contactDetailLoading==true?
                            <PreLoader  location ="contactDetail-loader"/>
                            :
                            <table className='table'>
                              <tbody className='reactable-data'>
                                {(typeof this.props.billing.email !== "undefined" && this.props.billing.email.length > 0)
                                  ? this.props.billing.email.map((emails, i) => (
                                    <tr key={i}>
                                      <td className="billing-image-col"><img src="/img/bill-contact-img.jpg"/></td>
                                      <td>
                                        <p className="billing-email" >{emails.email_id}</p>
                                      </td>
                                      <td>Billing</td>
                                      <td className="remove-btn-wrapper">
                                        <a
                                          className="remove-btn"
                                          href="#"
                                          title="Delete"
                                          onClick={this
                                          .removeContact
                                          .bind(this, emails.identity)}>X</a>
                                      </td>
                                    </tr>
                                  ))
                                  : ''
}

                              </tbody>
                            </table>}
                          </div>
                        </div>
                      </div>
                    </div>
                  </section>
                  <ul className="responsive_menu"> 
                    <li>
                      <Link to='/settings/billing/teamchange' activeClassName='active'>
                        <span className='text'>Team changes</span>
                        <p className="responsive-billing-icon"><i class="material-icons">&#xE313;</i></p>

                      </Link>
                    </li>
                    <li>
                      <Link to='/settings/billing/paymentmethod' activeClassName='active'>
                        <span className='text'>Payment methods</span>
                        <p className="responsive-billing-icon"><i class="material-icons">&#xE313;</i></p>

                      </Link>
                    </li>
                  </ul>

                </div>
                <div className="billing-footer">Questions?
                  <a href="#" title="Visit our guide">Visit our guide to billings at Visibly.</a>
                </div>
              </div>
            </div>
          </div>
        </div>

      </section>
    );
  }
}

function mapStateToProps(state) {
  return {billing: state.billing}
}
function mapDispatchToProps(dispatch) {
  return {
    billingActions: bindActionCreators(billingActions, dispatch)
  }
}
let connection = connect(mapStateToProps, mapDispatchToProps)
var reduxConnectedComponent = connection(SettingContact)

export default reduxForm({
  form: 'SettingContact' // a unique identifier for this form
})(reduxConnectedComponent)
