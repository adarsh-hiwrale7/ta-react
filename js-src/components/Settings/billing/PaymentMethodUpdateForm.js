import { Link } from "react-router";
import { browserHistory } from "react-router";
import * as billingActions from '../../../actions/BillingActions';
import { connect } from 'react-redux'; 
import { bindActionCreators } from 'redux';
import { Field, reduxForm } from 'redux-form';
import { renderField } from '../../Helpers/ReduxForm/RenderField'
const required = value => value
  ? undefined
  : 'Required';

class PaymentMethodUpdateForm extends React.Component {
			
	constructor(props){
		super(props);
		const {submitting,reset} = props;
		
		//this.handleView = this.handleView.bind(this);
		
	}
	
	componentDidMount(){
		this.handleInitialize(this.props)
	}

	/*componentWillReceiveProps (nextProps) {
		console.log(nextProps,'nextprops1');
		this.handleInitialize(nextProps)
		
	}*/

	handleInitialize (nextProps = null) {
		const initData = {
			card_name: nextProps.cardData.card_name,
			card_id: nextProps.cardData.identity
	     }

	     this.props.initialize(initData)
	}

	cancelCardUpdateForm(e){

		this.props.cancelCardUpdateForm();
	}
	
	
	render(){
		const {
	      handleSubmit,
	      pristine
		} = this.props
		var label_payment =this.props.cardData.default != 1 ? "Make default" :<div className="payment-type-text">Default</div>
		return  (
			
				
                <form className="bill-payment-form clearfix" onSubmit={handleSubmit}>
                		<Field component={renderField}  type="hidden" name="card_id"  />
                		<Field placeholder="Name on card" component={renderField}  type="text" name="card_name" validate={[required]}  />
						<Field id="default_card" 
					    component={renderField}
						 type="checkbox" 
						 name="default"
	                 	label = {label_payment} />
                 		{/* {this.props.cardData.default != 1 ?
                		<Field id="default_card" label="Make default" component={renderField} type="checkbox" name="default" /> 
						:<div className="payment-type-text">Default</div>} */}
						<div className="bill-card-holder" id={"name_"+this.props.pay_card_index}>{this.props.pay_card_name}</div>
						<div className="bill-card-detail">Visa ending in {this.props.pay_card_number}  - {this.props.pay_card_exp_month} / {this.props.pay_card_exp_year}</div>
						<div className='buttons-wrapper clearfix'>	
                			<button class="btn btn-primary" type="submit">Save</button>
                			<button class="btn btn-default" type="button" onClick={this.cancelCardUpdateForm.bind(this)}>Cancel</button>
						</div>
                </form>
			
		);
	}
}
	
    function mapStateToProps (state, ownProps) {
	  return {}
	}

	function mapDispatchToProps (dispatch) {
	  return {
	  		 
	  }
	}
	let connection = connect(mapStateToProps, mapDispatchToProps)

	var reduxConnectedComponent = connection(PaymentMethodUpdateForm)

	export default reduxForm({
	  form: 'PaymentMethodUpdateForm', // a unique identifier for this form
	})(reduxConnectedComponent)

   