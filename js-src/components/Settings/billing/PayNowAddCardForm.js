
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import PreLoader from '../../PreLoader'
import SliderPopup from '../../SliderPopup'
import {Link} from 'react-router';
import {Field, reduxForm} from 'redux-form';
import * as utils from '../../../utils/utils';
import { renderField } from '../../Helpers/ReduxForm/RenderField';
import * as billingActions from '../../../actions/BillingActions';
const required = value => (value ? undefined : 'Required')
const number = value => value && isNaN(Number(value)) ? 'Must be a number' : undefined
const minLength = max => value =>
  value && value.length != max ? `Must be ${max} digit` : undefined
const minLength16 = minLength(16)
const minLength2 = minLength(2)
const minLength4 = minLength(4)
const minLength3 = minLength(3)

class PayNowAddCardForm extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      
    }
  }
  componentDidMount(){
  }
  cancelAddCard(){
    this.props.billingActions.changePayPopup('pay');
  }
  
  
  render() {
    const {
        handleSubmit,
        pristine
      } = this.props
    return (
            <div className='pay-now-section-inner'>
                <div className='pay-now-content'>
                  <p>You can add up to 3 credit cards.</p>
                </div>
                <form onSubmit={handleSubmit}>
                    <div className='form-row'>
                      <label></label>
                      <Field
                        component={renderField}
                        type='text'
                        name='card_name'
                        label='Name on card'
                        validate={[required]}
                      />
                    </div>
                    <div className='form-row'>
                      <Field
                        component={renderField}
                        type='text'
                        name='card_number'
                        label='Card number'
                        validate={[required,number,minLength16]}
                      />
                    </div>
                    <div className='form-row'>
                      <Field
                        component={renderField}
                        type='text'
                        name='exp_month'
                        label="MM"
                        validate={[required,number,minLength2]}

                      />
                      <Field
                        component={renderField}
                        type='text'
                        name='exp_year'
                        label="YYYY"
                        validate={[required,number,minLength4]}
                      />
                    </div>
                    <div className='row'>
                      <div className='col-one-of-two'>
                        <div className='form-row'>
                          <Field
                            component={renderField}
                            type='text'
                            name='card_cvc'
                            label='CVV'
                            validate={[required,number,minLength3]}
                          />
                        </div>
                      </div>
                      <div className='col-one-of-two'>
                        <div className='form-row'>
                          <Field
                            component={renderField}
                            type='text'
                            name='zip_code'
                            label='Zip Code (US only)'
                          />
                        </div>
                      </div>
                    </div>
                    <div className='buttons-wrapper clearfix'>
                      <button className='btn btn-primary' type="submit" ><span className='plus-icon'>+</span> Add card</button>
                      <button className='btn btn-default' type="button" onClick={this.cancelAddCard.bind(this)} >Cancel</button>
                    </div>
                </form>
                
                <div className='existing-card-note'>
                  <p>Your card will not be charged at this time, but will be set as the default for future charges.</p>
                </div>
          </div>
    )
  }
}

function mapStateToProps (state) {
  return {
    billing: state.billing,
  }
}
function mapDispatchToProps (dispatch) {
  return {
    billingActions: bindActionCreators(billingActions, dispatch),
  }
}
let connection = connect(mapStateToProps, mapDispatchToProps)
var reduxConnectedComponent = connection(PayNowAddCardForm)

export default reduxForm({
  form: 'PayNowAddCardForm' // a unique identifier for this form
})(reduxConnectedComponent)
