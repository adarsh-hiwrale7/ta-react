import { Link } from "react-router";
import { browserHistory } from "react-router";
import * as billingActions from '../../../actions/BillingActions';
import { connect } from 'react-redux'; 
import { bindActionCreators } from 'redux';
import { Field, reduxForm } from 'redux-form';
import { renderField } from '../../Helpers/ReduxForm/RenderField';
const required = value => value
  ? undefined
  : 'Required';




class SettingContactForm extends React.Component {
			
	constructor(props){
		super(props);
		
		//this.handleView = this.handleView.bind(this);
		
	}
	render(){
		const {
	      handleSubmit,
	      pristine
    	} = this.props
		return  (
			
	     		<form  onSubmit={handleSubmit} >
						<div class="add-new-contact-block clearfix">
                      <div className="form-row text">
                         <Field
						                placeholder='Email'
						                label='Contact'
						                component={renderField}
						                type='text'
						                name='contact_email'
						                validate={[required]}
						              />
                        </div>
                         <div className="form-row contact-settings">
												    <button class="btn btn-primary" type="submit"><span>+</span> Add another contact</button></div>
                        </div>
						
						</form>
			
		);
	}
}

    function mapStateToProps (state, ownProps) {
	  return {}
	}

	function mapDispatchToProps (dispatch) {
	  return {
	  		 
	  }
	}
	let connection = connect(mapStateToProps, mapDispatchToProps)

	var reduxConnectedComponent = connection(SettingContactForm)

	export default reduxForm({
	  form: 'SettingContactForm' // a unique identifier for this form
	})(reduxConnectedComponent)

   