import { Field, reduxForm } from 'redux-form'
import { renderField } from '../../Helpers/ReduxForm/RenderField'
const required = value => (value ? undefined : 'Required')
var isSlackConnected=false
class SlackForm extends React.Component {
  constructor (props) {
    super(props)
    this.state = {}
  }
  componentDidMount() {
    this.refreshChannelList()
    this.handleInitialize()
    //if channelDetail has value means slack is connected 
    if (this.props.channelDetail !== undefined && Object.keys(this.props.channelDetail).length > 0) {
      isSlackConnected = true
    } else {
      isSlackConnected = false
    }
  }
  componentWillReceiveProps(newprops) {
    //to set flag to false if slack is disconnected
    if (newprops.props.intergration !== this.props.props.intergration && newprops.props.intergration.isSlackDisconnect == true) {
      isSlackConnected = false
    }
  }
  /**
   * to connect or disconnect slack
   * @author disha
   */
  ConnectSlack() {
    //check slack is connected or not and if it is connected and this method is calling then slack should disconnect
    if (isSlackConnected == true) {
      //pass user id to disconnect slack
      this.props.disconnectSlack(this.props.channelDetail.userId)
    } else {
      this.props.ConnectSlack()
    }
  }
  handleInitialize() {
    if (this.props.channelDetail !== undefined && Object.keys(this.props.channelDetail).length > 0) {
      const data = {
        slackChannelName: this.props.channelDetail.channelId
      }
      this.props.initialize(data)
    }
  }
  handleSlackNotificationStatus() {
  }
  /**
   * method will call on submit of form (change channel form) and get all the data to pass in parent props
   * @author disha
   */
  ChangeSlackChannel = data => {
    if (document.getElementById(`channel-${data.slackChannelName}`) !== undefined) {
      var ObjToSend = {
        "social_account_identity": this.props.channelDetail.userId,
        "channel_id": data.slackChannelName,
        "channel_name": document.getElementById(`channel-${data.slackChannelName}`).innerHTML
      }
      this.props.handleChangeSlackChannel(ObjToSend)
    }
  }
  /**
   * @author disha
   * to refresh list of slack channels manually
   * @param {*} isRefreshButtonCLicked true /false or undefined if param is not passed
   */
  refreshChannelList(isRefreshButtonCLicked){
    if((isRefreshButtonCLicked !== undefined && isRefreshButtonCLicked==true) || this.props.props.intergration.slackChannelList.length == 0){
      if(Object.keys(this.props.channelDetail).length>0){
         this.props.props.integrationActions.fetchSlackChannelList(this.props.channelDetail.accessToken)
      }
    }
  }
  render () {
      const {handleSubmit, pristine, submitting} = this.props;
      var channelList=this.props.props.intergration.slackChannelList
    return (
      <div>
         <div className='integrationRow integrationConnectRow'>
          <div className="integrationContentColumn">
            <div className="integrationTitle">Slack Integration</div>
            <div className="integtrationDescription">No need to spend all day in your Visibly feeds, now you can get your Visibly notifications in Slack.</div>
            <div className='integrationConnectBtn'>
              <button
                type='submit'
                class='btn btn-primary btn-integration'
                onClick={this.ConnectSlack.bind(this)}
              >
                {isSlackConnected==true?'Disconnect':'Connect'}
              </button>
            </div>
          </div>
          
          </div> {
            // integrationRow ends
          }
          {isSlackConnected==true ?
          
          /* <div className="integrationRow integrationNotificationRow">
            <div className="integrationContentColumn">
            <div className="integrationTitle">Slack notification</div>
            <div className="integtrationDescription">Turn on to receive new notifications in slack.</div>
            <div className='integrationSwitchBtn'>
              <div class="switch">
                <input id="cmn-toggle-1" class="CreditToggle"  type="checkbox"  onChange={this.handleSlackNotificationStatus.bind(this)} />
                <label for="cmn-toggle-1"></label>
              </div>
              </div> 
            </div>
          </div> */
          /* // integrationRow ends */
          
          <div className="integrationRow integrationChannelRow">
            <div className="integrationContentColumn">
        <form onSubmit={handleSubmit(this.ChangeSlackChannel.bind(this))}>
        
          <div className="integrationTitle">Channel Name</div>
          <div className='integtrationDescriptionWrapper'>
          <div className="integtrationDescription">Choose the Slack channel where Visibly notifications should be sent to.</div>
          <div className="integrationDropdown">
            <Field
              type='select'
              component={renderField}
              id='slackChannelName'
              name='slackChannelName'
              validate={required}
              >
              {channelList.length> 0?
                channelList.map((cl,c)=>{
                  if(cl.is_archived==false){
                    return(
                        <option key={c} value={cl.id} id={`channel-${cl.id}`}>{cl.name}</option>
                    )
                  }
                })
              :''}
              </Field>
              <button
                type='submit'
                class='btn btn-primary btn-integration'
                disabled={pristine || submitting}
              >
                Save
              </button>
              <div
                  class='btn btn-primary btn-integration refreshButton'
                  onClick={this.refreshChannelList.bind(this,true)}
                >
                <i class="material-icons">refresh</i>
                 
            </div>
          </div>
          </div>
          </form>
        
            </div>
          </div>:''}
          {
            // integrationRow ends
          }
        
        
      </div>
    )
  }
}
export default reduxForm({
  form: 'SlackForm' // a unique identifier for this form
})(SlackForm)
