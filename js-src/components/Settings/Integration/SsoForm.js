import notie from "notie/dist/notie.js";
import { CopyToClipboard } from 'react-copy-to-clipboard';
import { Field, reduxForm } from 'redux-form';
import CryptoJS from '../../../components/Chunks/ChunkCryptoJs';
import Globals from '../../../Globals';
import ReactDropzone from '../../Chunks/ChunkReactDropzone';
import { renderDropzoneField, renderField } from '../../Helpers/ReduxForm/RenderField';
const required = value => (value ? undefined : 'Required')
const ssoURL = value =>
  value && !value.startsWith("https://") ?
  'Invalid SSO URL' : undefined
class SsoForm extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      isSSOConnected: false,
      reactDropzone:null,
      uploadedFile: null,
      disableGenerateButton:true,
      entityValue:'',
      ssoKey:'',
      askForConfirm :false
    }
  }
  componentDidMount() {
  
    if (Object.keys(this.props.ssoData.ssoDataList).length > 0) {
      this.setState({isSSOConnected: true})
      this.handleInitialize()
    } 
  }
  componentWillMount(){
          ReactDropzone().then(dropzone => {
            this.setState({ reactDropzone: dropzone })
          })
  }
  componentWillReceiveProps(newprops) {
    if (this.props.ssoData.ssoConnectModifed == true) {
       this.handleInitialize(true);
       this.props.resetDisconnectRef();
    }
    //when form is submitted then to change generate button name and reset flag
    if(this.props.ssoData.ssoSaved !== newprops.ssoData.ssoSaved && newprops.ssoData.ssoSaved == true){
      this.props.resetSuccessFlagSSO(false)
      this.setState({
        askForConfirm:true
      })
    }
  }
  handleInitialize(reset = false){
    if(reset){
      const data = {
        "sso_connect_entity_id" : '',
        "sso_connect_url" : '',
        "sso_connect_certificate" : '',
        "sso_connect_name" : '',
        "required_single_sign_on" :undefined,
        "allow_profile_edit" :undefined,
        "sso_secret_key" :'',
        'ApiType': 'POST'

     };
     this.setState({
      ssoKey:'',
      disableGenerateButton:true,
      askForConfirm:false
    })
     this.props.initialize(data)
    }else{
      const {entity_id, login_url, certificate, sso_name, required_single_sign_on, allow_profile_edit,sso_secret_key} = this.props.ssoData.ssoDataList
      var data = {
          "sso_connect_entity_id" : entity_id,
          "sso_connect_url" : login_url,
          "sso_connect_certificate" : certificate,
          "sso_connect_name" : sso_name,
          "required_single_sign_on" : required_single_sign_on,
          "allow_profile_edit" : allow_profile_edit,
          "sso_secret_key" :sso_secret_key,
          "ApiType":sso_secret_key.length>0 ? 'PUT' : 'POST'
      };
      this.setState({
        ssoKey:sso_secret_key,
        disableGenerateButton:false,
        askForConfirm:true
      })
       this.props.initialize(data)
    }
  }
  /**
   * Handle reset sso 
   * @author Yamin
  **/
  resetSSOConnect(){
    if (Object.keys(this.props.ssoData.ssoDataList).length == 0) {
      notie.alert('error', 'Sorry, you are not connect with SSO', 3);
      return;
    }
    this.props.handleResetSSO();
  }
  /**
   * read content of file from url 
   * @author Yamin
  **/
  httpGet(theUrl){
    if (window.XMLHttpRequest)
    {// code for IE7+, Firefox, Chrome, Opera, Safari
        var xmlhttp=new XMLHttpRequest();
    }
    else
    {// code for IE6, IE5
        var xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    var me = this
    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
             var uploadResult = null
             uploadResult = xmlhttp.responseText
             me.renderFieldManually(uploadResult);
        }
    }
    xmlhttp.open("GET", theUrl, false );
    xmlhttp.send();    
  }
  renderFieldManually(uploadedResult=null){
    const data = {
                 "sso_connect_entity_id" : document.getElementById('sso_connect_entity_id').value,
                 "sso_connect_url" : document.getElementById('sso_connect_url').value,
                 "sso_connect_certificate" : document.getElementById('sso_connect_certificate').value,
                 "sso_connect_name" : document.getElementById('sso_connect_name').value,
                 "required_single_sign_on" :document.getElementById('required_single_sign_on').value,
                  "allow_profile_edit" :document.getElementById('allow_profile_edit').value,
                  "sso_secret_key" :document.getElementById('sso_secret_key').value,
                 "sso_connect_certificate_upload" : uploadedResult

    };
    if(uploadedResult !== null){
      Object.assign(data,{'sso_connect_certificate_upload':uploadedResult})
    }
    this.props.initialize(data)
  }
  /**
   * Handle dropzone upload action when user upload file 
   * @author Yamin
  **/
  handleDropzoneSubmit(field) {
      var urlToRead = field[0].preview;
      this.setState({uploadedFile:field[0].name})
      //uploadedFile = field[0].name;

      var res = this.httpGet(urlToRead);
  }
  /**
   * Remove uploaded file 
   * @author Yamin
  **/
  removeUploadedFile(){
    this.setState({uploadedFile:null})
    //uploadedFile = null;
    this.renderFieldManually();
  }
  /**
   * @author disha 
   * to generate sso key
   */
  generateSSoKey(){
    //if key is generated and user try to regeneate it then and then give confirmation alert
    if(this.state.askForConfirm){
    var me = this;
    notie.confirm(
      `Please ensure if you update token you have to update this token in your sso also to authenticate user provisioning.`,
      'Confirm',
      'Cancel',
      function () {
        me.generateKey();
      },
      function () { }
    )
    }else{
      this.generateKey();
    }
  }
  
  generateKey(){
    CryptoJS().then(crypto=>{
      const key = crypto.AES.encrypt(this.state.entityValue,Globals.secretKey).toString()
      this.props.change('sso_secret_key',key)
      this.setState({
        ssoKey:key
      })
    })
  }
  /**
   * @author disha 
   * to save entity id in sate and hanle generate button according to that
   * @param {*} e 
   */
  handleOnChangeEntity(e){
    const value =e.target.value == '' ? true :false
   this.setState({
     disableGenerateButton : value,
     entityValue:e.target.value
   })
  }
  /**
   * copy key
   */
  onCopySSOKey = () => {
    this.setState({copied: true});
    notie.alert('success', 'Copied successfully.', 3)
  };
   /**
   * separate form for sso connect 
   * @author Yamin
  **/
  renderConnectForm(){
      const {
        handleSubmit,
        pristine, submitting
      } = this.props
      return (
                <form role="form" onSubmit={handleSubmit} encType="multipart/form-data">
                        <div className="form-row intigration-fm clearfix">
                            <label>Entity ID</label>
                            <Field
                                placeholder='Entity ID'
                                component={renderField}
                                type='text'
                                name='sso_connect_entity_id'
                                validate= {required}
                                min="1" max="255"
                                onChange={this.handleOnChangeEntity.bind(this)}
                            />
                         </div>
                         <div className="form-row intigration-fm clearfix">
                         <label>SSO URL</label>
                            <Field
                                placeholder='SSO URL'
                                component={renderField}
                                type='text'
                                name='sso_connect_url'
                                validate= {[required, ssoURL]}
                            />
                         </div>
                         <div className="form-row intigration-fm clearfix">
                         <label>SSO Name</label>
                            <Field
                                placeholder='SSO Name'
                                component={renderField}
                                type='text'
                                name='sso_connect_name'
                                className='domain-textbox'
                                validate= {required}
                            />
                         </div>
                          <div className="form-row intigration-fm clearfix">
                            <label>Certificate</label>
                            <Field
                              placeholder='Certificate'
                              component={renderField}
                              type='textarea'
                              name='sso_connect_certificate'
                            />
                          </div>
                          <div className="upload-or-text"><span>OR</span></div>
                         <div className="form-row intigration-fm clearfix ssoIntegrationRow">
                            <Field
                              label='Certificate'
                              name='certificate_upload'
                              cssClass='dropzone-area'
                              handleSubmit={(fileToUpload, e) => {
                                 this.handleDropzoneSubmit(fileToUpload,e)
                              }}
                              title='Upload certificate'
                              Dropzone={this.state.reactDropzone}
                              component={renderDropzoneField}
                            />
                            {this.state.uploadedFile !== null ? 
                            <div class="upload-file-name fileuploaded">
                              <div className="upload-file-icon">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path fill="none" d="M0 0h24v24H0V0z"/><path d="M14 2H6c-1.1 0-1.99.9-1.99 2L4 20c0 1.1.89 2 1.99 2H18c1.1 0 2-.9 2-2V8l-6-6zM6 20V4h7v5h5v11H6z"/></svg>
                              </div>
                              {this.state.uploadedFile}
                              <a className="remove-file-icon" title="remove file" onClick={this.removeUploadedFile.bind(this)}><i class="material-icons">clear</i></a>
                            </div>
                            : ''}
                            <Field
                                component={renderField}
                                type='hidden'
                                name='sso_connect_certificate_upload'
                            />

                         </div>
                          <div className="form-row intigration-fm clearfix">
                            <label>Required Single Sign On</label>
                            <Field id='required_single_sign_on' name='required_single_sign_on' component={renderField} type='select' >
                              <option value='optional'>Optional</option>
                              <option value='required'>Required</option>
                            </Field>
                          </div>
                          <div className="form-row intigration-fm clearfix">
                            <label>Allow Profile Edit</label>
                            <Field id='allow_profile_edit' name='allow_profile_edit' component={renderField} type='select' >
                              <option value='Yes'>Yes</option>
                              <option value='No'>No</option>
                            </Field>
                          </div>
                          <div className="form-row intigration-fm clearfix">
                            <label>SSO Security Key</label>
                            <Field
                              placeholder='SSO Security Key'
                              component={renderField}
                              type='text'
                              id='sso_secret_key'
                              name='sso_secret_key'
                              className='integration-textbox'
                              disabled={true}
                              validate={required}
                            />
                          </div>
                          <Field
                              component={renderField}
                              type='hidden'
                              hidden={true}
                              id='ApiType'
                              name='ApiType'
                            />
                          <div className="form-row btn-int">
                            <button
                              type="button"
                              class="btn btn-primary btn-integration"
                              id="generateSSOKey"
                              onClick={this.generateSSoKey.bind(this)}
                              disabled={this.state.disableGenerateButton}
                            >{this.state.askForConfirm ? 'Generate New' : 'Generate'}</button>
                            <CopyToClipboard
                              onCopy={this.onCopySSOKey}
                              options={{ message: 'Whoa!' }}
                              text={this.state.ssoKey}>
                              <input type="button" class="btn btn-integration" value="Copy Key" disabled={this.state.disableGenerateButton} />
                            </CopyToClipboard>
                          </div>
                         <div className="form-row btn-int">
                         <button type="submit" class="btn btn-primary btn-integration" disabled={pristine || submitting}>Save</button>
                         <input type= "reset" name = " btn-workable" class="btn  btn-integration" value= "Reset" onClick={this.resetSSOConnect.bind(this)}/>
                        </div>
                     </form>
      )
  }
  
  
  
  render () {
    return (
      <div>
          <div className='integrationRow integrationConnectRow'>
                <div className="integrationTitle">SSO Integration</div>
                <div className="integtrationDescription"></div>
                {this.renderConnectForm()}
          </div>
      </div>
    )
  }
}
export default reduxForm({
  form: 'SsoForm' // a unique identifier for this form
})(SsoForm)
