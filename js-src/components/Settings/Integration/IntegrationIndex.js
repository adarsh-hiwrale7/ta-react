import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import PreLoader from '../../PreLoader'
import Header from '../../Header/Header';
import SettingsNav from '../SettingsNav';
import {Link} from 'react-router'
import { browserHistory } from 'react-router'
import IntegrationForm from './IntegrationForm';
import WorkableForm from './WorkableForm';
import * as integrationActions from '../../../actions/integrationActions'
import SlackForm from './SlackForm';
import SsoForm from './SsoForm';
var integration_key_data = '';
var restrictAPI=false;
class IntegrationIndex extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      show_menu: true,
      show_toggle: false,
      show_aerrow: false,
      type_integration: false,
      drive_integration: false,
      workable : false,
      slackDetail:false,
      ssoDetail:false
    }
  }
  componentDidMount() {
    let roleName='';
    if(localStorage.getItem('roles') !== null){
      roleName = JSON.parse(localStorage.getItem('roles'))[0]['role_name'];
    }
    if(restrictAPI != true){
      if(roleName === "super-admin" || roleName === "admin" ){
          this
          .props
          .integrationActions
          .intergrationKeyGenrater(false);

          this
          .props
          .integrationActions
          .workablerdata();

          this
          .props
          .integrationActions
          .getSSOData();
      }

      restrictAPI = true;
      setTimeout(()=>{
        restrictAPI=false;
      },5000)

      //pass true in param to fetch slack data from social account
    }

    //after authorizing and selecting channel from slack , slack will give code so to fetch code from url and pass it to api
    if(Object.keys(this.props.location.query).length>0){
      this.props.integrationActions.ConnenctSlack(this.props.location.query.code)
      browserHistory.push("/settings/integration");
    }
  }
  viewdrive() {
    if (this.state.drive_integration == false) {
      this.setState({drive_integration: true, show_aerrow: true})
    } else {
      this.setState({drive_integration: false, show_aerrow: false})
    }
  }
  viewIntegration() {
    if (this.state.show_toggle == false) {
      this.setState({show_toggle: true, show_aerrow: true})
    } else {
      this.setState({show_toggle: false, show_aerrow: false})
    }
  }
  viewworkable(){
    if (this.state.workable == false) {
      this.setState({workable: true})
    } else {
      this.setState({workable: false, show_aerrow: false})
    }
  }
  viewSlackDetails(){
    if (this.state.slackDetail == false) {
      this.setState({slackDetail: true})
    } else {
      this.setState({slackDetail: false, show_aerrow: false})
    }
  }
  /*call when click on SSO section*/
  viewSsoDetails(){
    if (this.state.ssoDetail == false) {
      this.setState({ssoDetail: true})
    } else {
      this.setState({ssoDetail: false, show_aerrow: false})
    }
  }
  handleSubmit(e) {
    this
      .props
      .integrationActions
      .intergrationKeyGenrater(true);
  }
  handleSubmitWorkable(values){
    const data = {
         key: values.workable_key,
         subdomain:values.workable_subdomain
    }
       this
      .props
      .integrationActions.workableAddKey(data);
  }
  /**
  * To handle submit event of sso connect
  * @author Yamin  
  **/
  handleSubmitSSO(values){
    this.props.integrationActions.ssoConnect(values);
  }
  resetSuccessFlagSSO(value){
    this.props.integrationActions.ssoConnectSuccess(value);
  }
  /**
  * To handle reset event of sso
  * @author Yamin  
  **/
  handleResetSSO(){
    this.props.integrationActions.ssoDisconnect();
  }
  resetDisconnectRef(){
    this.props.integrationActions.resetDisconnect(); 
  }
  /**
   * to connect slack
   * @author disha
   */
  ConnectSlack(){
    this.props.integrationActions.fetchSlackClientInfo()
  }
  /**
   * to disconnect slack
   * @param {*} id user id (social id)
   * @author disha
   */
  disconnectSlack(id){
    this.props.integrationActions.disconnectSlack(id)
  }

  /**
   * to pass data of new changed channel data in api
   * @param {*} newChannelData object, method will call from child form (slackfrom)
   * @author disha
   */
  handleChangeSlackChannel(newChannelData){
    this.props.integrationActions.changeSlackChannel(newChannelData)
  }
  render() {
    let roleName='';
    if(localStorage.getItem('roles') !== null){
      roleName = JSON.parse(localStorage.getItem('roles'))[0]['role_name'];
    }
    const loader = this.props.intergration.ssoLoader  || this.props.intergration.slackLoader || this.props.intergration.loading==true || this.props.intergration.workableLoader
    return (
      <section className="IntegrationIndex">
      {loader ? <PreLoader className="Page" /> : <div />}
        <Header
          title="Integrations"
          nav={SettingsNav}
          location
          ={this.props.location}></Header>
        <div className='main-container'>
          <div id="content">
          <div className="page">
              <div className="content">
            <div className="intergration-main-container">
             
              <div className="intergration-container">
                <div className="int-inner-container">
                  <div className="int-heading">
                    <h3>Integrations</h3>
                  </div>
                  <div className="integration-body">
                  {roleName === "super-admin" || roleName === "admin" ?
                  <div>
                    <div className={`integration-blog ${this.state.show_toggle ? 'active' :''}`}>
                      <div
                        className="int-block clearfix"
                        onClick={this
                        .viewIntegration
                        .bind(this)}>
                        <span className="dropdown-arow">
                        </span>
                        <div className="avtar">
                          <img src="/img/LM_logo.png"/>
                        </div>
                        <div className="avtar-details">
                          <p className="name-text-int">
                            Logic melon
                          </p>
                          <p className="text-integration">
                            Import your latest jobs and enable user-sharing to social
                          </p>
                        </div>
                       </div>
                      {this.state.show_toggle
                        ? <div className="show-intigration-block">
                            <IntegrationForm
                              onSubmit={this
                              .handleSubmit
                              .bind(this)}
                              intergrationData = {this.props.intergration}></IntegrationForm>
                          </div>
                        : ''}
                    </div>
                    <div className={`integration-blog ${this.state.workable ? 'active' :''}`}>
                      <div
                        className="int-block clearfix" onClick = {this.viewworkable.bind(this)}>
                        <span className="dropdown-arow">
                        </span>
                        <div className="avtar workable-avtar">
                          <img src="/img/workable_final.png"/>
                        </div>
                        <div className="avtar-details">
                          <p className="name-text-int">
                             Workable
                          </p>
                          <p className="text-integration">
                            Import your latest jobs and enable user-sharing to social
                          </p>
                        </div>
                       </div>
                      {this.state.workable
                        ? <div className="show-intigration-block">
                            <WorkableForm 
                               onSubmit={this
                              .handleSubmitWorkable
                              .bind(this)}
                              workableData = {this.props.intergration.data}/>
                          </div>
                        : ''}
                    </div> 
                  </div> :''}
                    <div className={`integration-blog ${this.state.slackDetail ? 'active' :''}`}>
                      <div
                        className="int-block clearfix" onClick = {this.viewSlackDetails.bind(this)}>
                        <span className="dropdown-arow">
                        </span>
                        <div className="avtar workable-avtar slack-avatar">
                          <img src="/img/slack.png"/>
                        </div>
                        <div className="avtar-details">
                          <p className="name-text-int">
                             Slack
                          </p>
                          <p className="text-integration">
                            Get all of your Visibly personal notifications sent to your desired Slack channel
                          </p>
                        </div>
                       </div>
                      {this.state.slackDetail
                        ? <div className="show-intigration-block slackIntegrationBlock">
                            <SlackForm 
                               ConnectSlack={this.ConnectSlack.bind(this)}
                               disconnectSlack={this.disconnectSlack.bind(this)}
                              channelDetail={this.props.intergration.slackChannelData}
                              handleChangeSlackChannel={this.handleChangeSlackChannel.bind(this)}
                              props={this.props}/>
                          </div>
                        : ''}
                    </div> 
                     {roleName == "super-admin" || roleName == "admin" ?
                     <div className={`integration-blog ${this.state.ssoDetail ? 'active' :''}`}>
                      <div
                        className="int-block clearfix" onClick = {this.viewSsoDetails.bind(this)}>
                        <span className="dropdown-arow">
                        </span>
                        <div className="avtar workable-avtar slack-avatar">
                          <img src="/img/sso.png"/>
                        </div>
                        <div className="avtar-details">
                          <p className="name-text-int">
                             SSO
                          </p>
                          <p className="text-integration">
                            Configure single sign-on (SSO) and get all user data automatically synced to Visibly.
                          </p>
                        </div>
                       </div>
                      {this.state.ssoDetail
                        ? <div className="show-intigration-block ssoIntegrationBlock">
                            <SsoForm 
                              ssoData={this.props.intergration}
                              onSubmit={this.handleSubmitSSO.bind(this)}
                              resetSuccessFlagSSO={this.resetSuccessFlagSSO.bind(this)}
                              handleResetSSO={this.handleResetSSO.bind(this)}
                              resetDisconnectRef={this.resetDisconnectRef.bind(this)}
                            />
                          </div>
                        : ''}
                    </div> 
                    : ''}
                  {/*<div className="integration-blog">
                      <div className="int-block clearfix">
                        <span className="dropdown-arow">
                          <button className="btn btn-primary drive-btn">Connect</button>
                        </span>
                        <div className="avtar google-drive-avtar">
                          <img src="/img/Google_Drive_Logo.svg.png"/>
                        </div>
                        <div className="avtar-details">
                          <p className="name-text-int">
                            Google Drive
                          </p>
                          <p className="text-integration">
                            Fetch Google Docs file content, and upload new content from Visibly
                          </p>
                        </div> 

                      </div>
                    </div>*/}

                  </div>
                </div>
              </div>
              </div>
              </div>
            </div>
          </div>
        </div>

      </section>
    );
  }
}

function mapStateToProps(state) {
  return {
    intergration: state.intergration,

  }
}

function mapDispatchToProps(dispatch) {
  return {
    integrationActions: bindActionCreators(integrationActions, dispatch)
  }
}

let connection = connect(mapStateToProps, mapDispatchToProps)

module.exports = connection(IntegrationIndex)
