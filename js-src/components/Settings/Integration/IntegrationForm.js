import { Field, reduxForm } from 'redux-form';
import { renderField } from '../../Helpers/ReduxForm/RenderField'
import {CopyToClipboard} from 'react-copy-to-clipboard';
import notie from "notie/dist/notie.js"
var disabled = "disabled";
var intergration_form_key = ''; 
class IntegrationForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            show_menu: true,
            show_toggle: false,
            value :'',
            copied: false
        }
    }
    onCopyIntegration = () => {
        this.setState({copied: true});
        notie.alert('success', 'Copied successfully.', 3)
      };
    onClickIntegration = ({target: {innerHTML}}) => {
        // console.log(`Clicked on "${innerHTML}"!`); // eslint-disable-line
      };
componentDidMount(){
  this.handleInitialize()
}

componentWillReceiveProps(nextprops){
    if(this.props.intergrationData.intdata != nextprops.intergrationData.intdata) {
        intergration_form_key = nextprops.intergrationData.intdata;
    }else{
        intergration_form_key = this.props.intergrationData.intdata;
    }
   this.handleInitialize();
}
handleInitialize() {
    const initData = {
        "integrationKey" :  intergration_form_key,
    };
    this.props.initialize(initData);}
   render() {
        const {
            handleSubmit,
            pristine
          } = this.props
           return (
            <div>
                <form role="form" onSubmit={handleSubmit}>
                    <div className="form-row intigration-fm clearfix">
                        <label>Security key</label>
                        <Field
                            placeholder='Security key'
                            component={renderField}
                            type='text'
                            name='integrationKey'
                            className='integration-textbox'
                            disabled = {disabled}
                        />
                    </div>
                     <div className="form-row btn-int"> 
                     <button type="submit" class="btn btn-primary btn-integration" id="generate">Generate</button>
                     <CopyToClipboard
                        onCopy={this.onCopyIntegration}
                        options={{message: 'Whoa!'}}
                        text={this.props.intergrationData.intdata}>
                        <input type= "button" class="btn btn-integration" onClick={this.onClickIntegration} value="Copy Key"/>
                        {/* <button class="btn btn-primary btn-integration" onClick={this.onClick}>Copy Key</button> */}
                       </CopyToClipboard>
                   
                    </div>
                 </form>
            </div>
        )
    }
}
export default reduxForm({
    form: 'IntegrationForm',  // a unique identifier for this form
})(IntegrationForm)