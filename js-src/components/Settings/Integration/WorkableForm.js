import { Field, reduxForm, reset } from 'redux-form';
import { renderField } from '../../Helpers/ReduxForm/RenderField'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
const required = value => value
  ? undefined
  : 'Required';
class WorkableForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        }
}
componentDidMount(){
    this.handleInitialize()
  }
handleInitialize(){
    const {workable_key, workable_subdomain} = this.props.workableData
    const data = {
        "workable_key" : workable_key,
        "workable_subdomain" : workable_subdomain,

    };
    this.props.initialize(data)
  }
  // to reset the form 
  handleResetWorkable(){
      this.props.initialize({})
    // this.props.dispatch(reset('workableForm')); this is not working
  }
 render() {
      const { handleSubmit, pristine, reset, submitting } = this.props;
               return (
                <div>
                    <form role="form" onSubmit={handleSubmit} name="workableForm" id="workableForm">
                        <div className="form-row intigration-fm clearfix">
                            <label>Security key</label>
                            <Field
                                placeholder='Security key'
                                component={renderField}
                                type='text'
                                name='workable_key'
                                className='workable-textbox'
                                validate= {required}
                                min="1" max="255"
                            />
                         </div>
                         <div className="form-row intigration-fm clearfix">
                         <label>Sub Domain</label>
                            <Field
                                placeholder='Sub Domain'
                                component={renderField}
                                type='text'
                                name='workable_subdomain'
                                className='domain-textbox'
                                validate= {required}
                            />
                      </div>
                         <div className="form-row btn-int">
                         <button type="submit" class="btn btn-primary btn-integration">Save key</button>
                         <input type= "reset" name = " btn-workable" class="btn  btn-integration" value= "Reset"  onClick={this.handleResetWorkable.bind(this)}/>
                        </div>
                     </form>
                </div>
            )
        }
    }
 export default reduxForm({
        form: 'workableForm',  // a unique identifier for this form
})(WorkableForm)
