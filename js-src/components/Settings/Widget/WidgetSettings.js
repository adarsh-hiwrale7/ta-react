import notie from 'notie/dist/notie.js'
import { CopyToClipboard } from 'react-copy-to-clipboard'
import Script from 'react-load-script'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as getSettings from '../../../actions/settings/getSettings'
import * as widgetActions from '../../../actions/settings/widgetSettings'
import Globals from '../../../Globals'
import Header from '../../Header/Header'
import PreLoader from '../../PreLoader'
import SettingsNav from '../SettingsNav'
import WidgetForm from './WidgetForm'
import WidgetNav from './WidgetNav'
var scriptext = '';
var scripttext2 = ''
var restrictAPI=false;
class WidgetSettings extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      departments: [],
      profile: [],
      widgetURL: Globals.WIDGET_URL + '/js/widget.js',
      currentTab: 'Widgets',
      show_menu: true,
      handleScriptLoad: false,
      copied:false,
      widget_limit : 12
    }
    this.handleScriptCreate = this.handleScriptCreate.bind(this)
    this.handleScriptError = this.handleScriptError.bind(this)
    this.handleScriptLoad = this.handleScriptLoad.bind(this)
   
  }
  onCopyWidget = () => {
    notie.alert('success', 'Copied successfully.', 3)
    this.setState({copied: true});
  };

  handleScriptCreate () {
    this.setState({ scriptLoaded: false })
  }

  handleScriptError () {
    this.setState({ scriptError: true })
  }


  handleScriptLoad () {
    let me = this
    setTimeout(function () {
      me.setState({ scriptLoaded: true })
      var elem = document.querySelector('#social-masnory')
      var msnry = new Masonry(elem, {
        // options
        itemSelector: '.social-masnory-grid',
        columnWidth: 200
      })

      // element argument can be a selector string
      //   for an individual element
      var msnry = new Masonry(
        '.grid',
        {
          // options
        }
      )
      // MYLIBRARY.init(["somevalue", 1, "controlId"]); using this we can pass data to server side widget js file
    }, 3000)
  }

  createScript () {
    const script = document.createElement('script')
    script.src = this.state.widgetURL
    script.async = 1
    script.innerHTML = `var $widget_secret = ${localStorage.getItem('widget_secret')}`;
    document.body.appendChild(script)
  }

  componentDidMount () {
    if(restrictAPI!==true){
      this.props.getSettings.GetAllSettings()
      restrictAPI=true;
      setTimeout(()=>{
        restrictAPI=false
      },5000)
    }
    var sociaLimit = localStorage.getItem('social_feed_limit');
    this.setState({
      widget_limit : sociaLimit
    })
    this.createScript()
     /** Add secret key for widget */
     /*const script = document.createElement('script');
     
     document.body.appendChild(script);*/
  }
  /**
   * @author disha
   * @use to get only social account id which is selected
   */
  socialAccountsData(values) {
    let socialData = [];
    for (var prop in values) {
      if (values.hasOwnProperty(prop) && prop.toString().startsWith('chk-') && values[prop] == true) {
        socialData.push( prop.substr(prop.lastIndexOf('-') + 1))
      }
    }
    return socialData;
  }

  /**
   * this will handle submit of widget to save setting widget data
   */
  handlewidgetSubmit (values) {
    //create object to send only required data to the api
    var objToSend={
      connected_social_account_ids_identity:values.connected_social_account_ids_identity,
      connected_social_account_ids:this.socialAccountsData(values),
      socialwall_limit:values.socialwall_limit,
      socialwall_limit_identity:values.socialwall_limit_identity,
      theme:values.theme,
      theme_identity:values.theme_identity
    }
    if(objToSend.connected_social_account_ids.length==0){
      notie.alert('error', 'You must select at least 1 social channel', 5)
      return
    }
    this.setState({
      widget_limit : values.socialwall_limit
    })
    localStorage.setItem('social_feed_limit',values.socialwall_limit);
    this.handleSocialFeedLimit(values.socialwall_limit);
    this.props.widgetActions.widgetInfoSave(objToSend);
  }

  handleSocialFeedLimit(feedLimit){
    const script = document.createElement('script');
    script.innerHTML = `var $social_feed_limit = ${feedLimit}`;
    document.body.appendChild(script)
  }

  componentWillReceiveProps (nextProps) {
    if (
      JSON.stringify(nextProps.companyInfo) !=
      JSON.stringify(this.props.companyInfo)
    ) {
      this.createScript()
    }
  }
  renderBreadcrumbs () {
    // Display additional items depending on which page we are
    return <h1 />
  }
  changeTab (tabText) {
    this.setState({ currentTab: tabText })
  }

  render () {
    let profileloading = this.props.profile.profile.loading
    let companyloading = this.props.settings.loading

    let profile = typeof this.props.profile !== 'undefined'
      ? this.props.profile
      : []
    let companyInfo = typeof this.props.companyInfo !== 'undefined'
      ? this.props.companyInfo
      : []


  var textcopy = '';
  textcopy = document.getElementsByTagName('code');
    if(textcopy[0] !== undefined){
      scriptext = textcopy[0].innerText;
  }
  var textcopy2 = '';
  textcopy2 = document.querySelectorAll('code.scriptcode1')
  if(textcopy2[0] !== undefined){
    scripttext2 = textcopy2[0].innerText;
   }
   function requestFullScreen(elt) {
    if (elt.requestFullscreen) {
      elt.requestFullscreen();
    } else if (elt.msRequestFullscreen) {
      elt.msRequestFullscreen();
    } else if (elt.mozRequestFullScreen) {
      elt.mozRequestFullScreen();
    } else if (elt.webkitRequestFullscreen) {
      elt.webkitRequestFullscreen();
    } else {
        console.error("Fullscreen not available");
    }
}

document.getElementById("widgetFullScreen")!==null?
document.getElementById("widgetFullScreen").addEventListener("click", function () {
    requestFullScreen(
      document.getElementById('social-widget-container')
    );
}):''
  return (
      <section id='settings' className='settings'>
        {companyloading ? <PreLoader className='Page'/>:''}
        <Header title='Widgets' nav={SettingsNav} location={this.props.location} />

        <div className='main-container'>
          <div id='content'>

            <div className='page settings-container'>

              <div className='full-container'>


                <div className='widget'>
                  <div className='inner-widget'>
                    <section className='register-page'>
                      <WidgetNav main_page />
                      {/* <div className='section'>
                {this.state.currentTab=='Widgets'?<WidgetSettings refetchList={this.props.refetchList}  Moment={this.state.Moment} location={this.props.location}></WidgetSettings>:<div></div>}
                {this.state.currentTab=='Map Widgets'? <MapWidgetSettings campaign={this.props.campaign} Moment={this.props.Moment}></MapWidgetSettings>:<div></div>}

              </div> */}
                    </section>
                    <header class='heading'>
                            <h3>Setup social wall</h3>
                    </header>
                    <div className='widget-page'>
                      <div className='row'>
                      <div class="widget-page-block">
                      <div class="col-one-of">


                          {profileloading
                            ? <PreLoader className='Page' />
                            : <div />}
                          <WidgetForm
                            accounts={this.props.accounts}
                            onSubmit={this.handlewidgetSubmit.bind(this)}
                            companyInfo={companyInfo}
                          />


                        </div>
                        </div>
                        <div class="heading-widget">
                        <header class="heading widget"><h3>Embedded Widget</h3>
                        </header>
                        </div>
                        <div class="widget-box">
                        <div class="col-one-of-two-left">


                          <div className="cpy-paste">
                              <label>
                                Copy and Paste this code to your projects index.html.
                              </label>
                              <div className="form-row disabled text scripttext">

                              <code className = "code-scripttext2">

                                &lt;script type="text/javascript"&gt;
                                <br />
                                var $widget_secret = "
                                {localStorage.getItem('widget_secret')}
                                "
                                <br />
                                var $social_feed_limit = "
                                {this.state.widget_limit}
                                "
                                <br />
                                &lt;/script&gt;
                                <br />
                                &lt;script type="text/javascript" src="
                                {' '}
                                {this.state.widgetURL}
                                "&gt;&lt;/script&gt;
                              </code>

                              </div>
                              <div className="widget-copy-btn right">

                              <CopyToClipboard
                                  onCopy={this.onCopyWidget}
                                  options={{message: 'Whoa!'}}
                                  text= {scriptext}>
                                  <input type="button" class="btn btn-integration" value="Copy"/>
                                </CopyToClipboard>
                              </div>
                         </div>
                         </div>

                         <div class="col-one-of-two-right" >
                              <label>
                                Create a div with {' '}
                                <code>#social-widget-container</code>
                                {' '}
                                anywhere in your page
                                </label>
                                {' '}
                                {'\n'}
                                <div className="form-row disabled text scripttext2">
                                  <code className = "scriptcode1">&lt;div id="social-widget-container"&gt;&lt;/div&gt;</code>
                                </div>
                                <div className="widget-copy-btn right">
                                    <CopyToClipboard
                                          onCopy={this.onCopyWidget}
                                          options={{message: 'Whoa!'}}
                                          text= {scripttext2}>
                                          <input type="button" class="btn btn-integration" value="Copy"/>
                                   </CopyToClipboard>
                              </div>
                              </div>
                              <div className="heading-widget">
                                  <header className="heading"><h3>Display On Projector Of Lcd Tv</h3></header>
                              </div>
                      <div className="display">
                              <p>Now that you’ve got your social wall running, why not display it on a video projector or large TV screen at your event venue, point of sale or trade show booth. Setup is quick and easy!</p>
                                <li>
                                To display the social wall on a projector or TV screen, you’ll need a Mac, Windows PC or tablet connected to the projector/TV. The easiest way to set this up is to “mirror” the device’s screen to the projector/TV.
                                </li>
                                <li>
                                Please make sure your device has a stable Internet connection. We recommend using Ethernet cable, since WiFi and 3G often degrade in quality at events.
                                </li>
                                <input type="button" class="btn btn-integration" id='widgetFullScreen' value="Go Fullscreen"/>
                        </div>

                        </div>
                      </div>
                      </div>



                    <div className='butterflyrow'>
                      <header>

                      </header>
                      {/* old socail widget */}
                      <div id='social-widget-container' className = "clearfix">
                        <h4 class='map-widget-align'>
                          Please enter social credentials above
                        </h4>
                      </div>
                      {/* end old socail widget */}
                      {/* new widget desing */}

                      <div id='social-widget-main-container' className='hide'>
                        <div className='widget-container clearfix'>

                          <div id='social-masnory' class='grid clearfixs'>
                            <div className='social-masnory-grid'>
                              <div className='soical-wall-main'>
                                <div className='inner-social-wall'>
                                  <div className='img-container'>
                                    <img src='/img/widget.png' />
                                  </div>
                                  <div className='text-part'>
                                    <div className='text-header'>
                                      <p>
                                        <span>
                                          RT Jamescann:
                                        </span>
                                        10 Visual Content Marketing Statistics for 2017
                                        <span>#awward #celebrate</span>
                                      </p>
                                      <p class='text-link'>
                                        <a href='#'>
                                          http://owl.ly/4jFw30bAZar
                                        </a>
                                      </p>
                                    </div>
                                    <div className='scoial-icon clearfix'>
                                      <div className='visibly-icon' />
                                      <div className='social-icon li'>
                                        <a href='#'>
                                          <i
                                            class='fa fa-linkedin'
                                            aria-hidden='true'
                                          />
                                        </a>
                                        {/* <a class="li"><i class="fa fa-linkedin" aria-hidden="true"></i></a> */}
                                        {/* <a class="tw"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                 <a class="fb"><i class="fa fa-facebook" aria-hidden="true"></i></a> */}
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>

                            <div className='social-masnory-grid'>
                              <div className='soical-wall-main'>
                                <div className='inner-social-wall'>
                                  <div className='img-container'>
                                    <img src='/img/widget-image-2.jpg' />
                                  </div>
                                  <div className='text-part'>
                                    <div className='text-header'>
                                      <p>
                                        <span>
                                          RT Jamescann:
                                        </span>
                                        10 Visual Content Marketing Statistics for 2017
                                        <span>#awward #celebrate</span>
                                      </p>
                                      <p class='text-link'>
                                        <a href='#'>
                                          http://owl.ly/4jFw30bAZar
                                        </a>
                                      </p>
                                    </div>
                                    <div className='scoial-icon clearfix'>
                                      <div className='visibly-icon' />
                                      <div className='social-icon li'>
                                        <a href='#'>
                                          <i
                                            class='fa fa-linkedin'
                                            aria-hidden='true'
                                          />
                                        </a>
                                        {/* <a class="li"><i class="fa fa-linkedin" aria-hidden="true"></i></a> */}
                                        {/* <a class="tw"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                 <a class="fb"><i class="fa fa-facebook" aria-hidden="true"></i></a> */}
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>

                            <div className='social-masnory-grid'>
                              <div className='soical-wall-main'>
                                <div className='inner-social-wall'>
                                  <div className='text-part'>
                                    <div className='text-header'>
                                      <p>
                                        <span>
                                          RT Jamescann:
                                        </span>
                                        10 Visual Content Marketing Statistics for 2017
                                        <span>#awward #celebrate</span>
                                      </p>
                                      <p class='text-link'>
                                        <a href='#'>
                                          http://owl.ly/4jFw30bAZar
                                        </a>
                                      </p>
                                    </div>
                                    <div className='scoial-icon clearfix'>
                                      <div className='visibly-icon' />
                                      <div className='social-icon li'>
                                        <a href='#'>
                                          <i
                                            class='fa fa-linkedin'
                                            aria-hidden='true'
                                          />
                                        </a>
                                        {/* <a class="li"><i class="fa fa-linkedin" aria-hidden="true"></i></a> */}
                                        {/* <a class="tw"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                 <a class="fb"><i class="fa fa-facebook" aria-hidden="true"></i></a> */}
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>

                            <div className='social-masnory-grid'>
                              <div className='soical-wall-main'>
                                <div className='inner-social-wall'>
                                  <div className='img-container'>
                                    <img src='/img/widget-image-4.jpg' />
                                  </div>
                                  <div className='text-part'>
                                    <div className='text-header'>
                                      <p>
                                        <span>
                                          RT Jamescann:
                                        </span>
                                        10 Visual Content Marketing Statistics for 2017
                                        <span>#awward #celebrate</span>
                                      </p>
                                      <p class='text-link'>
                                        <a href='#'>
                                          http://owl.ly/4jFw30bAZar
                                        </a>
                                      </p>
                                    </div>
                                    <div className='scoial-icon clearfix'>
                                      <div className='visibly-icon' />
                                      <div className='social-icon li'>
                                        <a href='#'>
                                          <i
                                            class='fa fa-linkedin'
                                            aria-hidden='true'
                                          />
                                        </a>
                                        {/* <a class="li"><i class="fa fa-linkedin" aria-hidden="true"></i></a> */}
                                        {/* <a class="tw"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                 <a class="fb"><i class="fa fa-facebook" aria-hidden="true"></i></a> */}
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div className='social-masnory-grid'>
                              <div className='soical-wall-main'>
                                <div className='inner-social-wall'>
                                  <div className='img-container'>
                                    <img src='/img/widget.png' />
                                  </div>
                                  <div className='text-part'>
                                    <div className='text-header'>
                                      <p>
                                        <span>
                                          RT Jamescann:
                                        </span>
                                        10 Visual Content Marketing Statistics for 2017
                                        <span>#awward #celebrate</span>
                                      </p>
                                      <p class='text-link'>
                                        <a href='#'>
                                          http://owl.ly/4jFw30bAZar
                                        </a>
                                      </p>
                                    </div>
                                    <div className='scoial-icon clearfix'>
                                      <div className='visibly-icon' />
                                      <div className='social-icon li'>
                                        <a href='#'>
                                          <i
                                            class='fa fa-linkedin'
                                            aria-hidden='true'
                                          />
                                        </a>
                                        {/* <a class="li"><i class="fa fa-linkedin" aria-hidden="true"></i></a> */}
                                        {/* <a class="tw"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                 <a class="fb"><i class="fa fa-facebook" aria-hidden="true"></i></a> */}
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>

                            <div className='social-masnory-grid'>
                              <div className='soical-wall-main'>
                                <div className='inner-social-wall'>
                                  <div className='text-part'>
                                    <div className='text-header'>
                                      <p>
                                        <span>
                                          RT Jamescann:
                                        </span>
                                        10 Visual Content Marketing Statistics for 2017
                                        <span>#awward #celebrate</span>
                                      </p>
                                      <p class='text-link'>
                                        <a href='#'>
                                          http://owl.ly/4jFw30bAZar
                                        </a>
                                      </p>
                                    </div>
                                    <div className='scoial-icon clearfix'>
                                      <div className='visibly-icon' />
                                      <div className='social-icon li'>
                                        <a href='#'>
                                          <i
                                            class='fa fa-linkedin'
                                            aria-hidden='true'
                                          />
                                        </a>
                                        {/* <a class="li"><i class="fa fa-linkedin" aria-hidden="true"></i></a> */}
                                        {/* <a class="tw"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                 <a class="fb"><i class="fa fa-facebook" aria-hidden="true"></i></a> */}
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>

                            <div className='social-masnory-grid'>
                              <div className='soical-wall-main'>
                                <div className='inner-social-wall'>
                                  <div className='img-container'>
                                    <img src='/img/widget-image-3.jpg' />
                                  </div>
                                  <div className='text-part'>
                                    <div className='text-header'>
                                      <p>
                                        <span>
                                          RT Jamescann:
                                        </span>
                                        10 Visual Content Marketing Statistics for 2017
                                        <span>#awward #celebrate</span>
                                      </p>
                                      <p class='text-link'>
                                        <a href='#'>
                                          http://owl.ly/4jFw30bAZar
                                        </a>
                                      </p>
                                    </div>
                                    <div className='scoial-icon clearfix'>
                                      <div className='visibly-icon' />
                                      <div className='social-icon li'>
                                        <a href='#'>
                                          <i
                                            class='fa fa-linkedin'
                                            aria-hidden='true'
                                          />
                                        </a>
                                        {/* <a class="li"><i class="fa fa-linkedin" aria-hidden="true"></i></a> */}
                                        {/* <a class="tw"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                 <a class="fb"><i class="fa fa-facebook" aria-hidden="true"></i></a> */}
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>

                            <div className='social-masnory-grid'>
                              <div className='soical-wall-main'>
                                <div className='inner-social-wall'>
                                  <div className='img-container'>
                                    <img src='/img/widget-image-4.jpg' />
                                  </div>
                                  <div className='text-part'>
                                    <div className='text-header'>
                                      <p>
                                        <span>
                                          RT Jamescann:
                                        </span>
                                        10 Visual Content Marketing Statistics for 2017
                                        <span>#awward #celebrate</span>
                                      </p>
                                      <p class='text-link'>
                                        <a href='#'>
                                          http://owl.ly/4jFw30bAZar
                                        </a>
                                      </p>
                                    </div>
                                    <div className='scoial-icon clearfix'>
                                      <div className='visibly-icon' />
                                      <div className='social-icon li'>
                                        <a href='#'>
                                          <i
                                            class='fa fa-linkedin'
                                            aria-hidden='true'
                                          />
                                        </a>
                                        {/* <a class="li"><i class="fa fa-linkedin" aria-hidden="true"></i></a> */}
                                        {/* <a class="tw"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                 <a class="fb"><i class="fa fa-facebook" aria-hidden="true"></i></a> */}
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>

                            <div className='social-masnory-grid'>
                              <div className='soical-wall-main'>
                                <div className='inner-social-wall'>
                                  <div className='img-container'>
                                    <img src='/img/widget-image-4.jpg' />
                                  </div>
                                  <div className='text-part'>
                                    <div className='text-header'>
                                      <p>
                                        <span>
                                          RT Jamescann:
                                        </span>
                                        10 Visual Content Marketing Statistics for 2017
                                        <span>#awward #celebrate</span>
                                      </p>
                                      <p class='text-link'>
                                        <a href='#'>
                                          http://owl.ly/4jFw30bAZar
                                        </a>
                                      </p>
                                    </div>
                                    <div className='scoial-icon clearfix'>
                                      <div className='visibly-icon' />
                                      <div className='social-icon li'>
                                        <a href='#'>
                                          <i
                                            class='fa fa-linkedin'
                                            aria-hidden='true'
                                          />
                                        </a>
                                        {/* <a class="li"><i class="fa fa-linkedin" aria-hidden="true"></i></a> */}
                                        {/* <a class="tw"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                 <a class="fb"><i class="fa fa-facebook" aria-hidden="true"></i></a> */}
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div className='social-masnory-grid'>
                              <div className='soical-wall-main'>
                                <div className='inner-social-wall'>
                                  <div className='img-container'>
                                    <img src='/img/widget.png' />
                                  </div>
                                  <div className='text-part'>
                                    <div className='text-header'>
                                      <p>
                                        <span>
                                          RT Jamescann:
                                        </span>
                                        10 Visual Content Marketing Statistics for 2017
                                        <span>#awward #celebrate</span>
                                      </p>
                                      <p class='text-link'>
                                        <a href='#'>
                                          http://owl.ly/4jFw30bAZar
                                        </a>
                                      </p>
                                    </div>
                                    <div className='scoial-icon clearfix'>
                                      <div className='visibly-icon' />
                                      <div className='social-icon li'>
                                        <a href='#'>
                                          <i
                                            class='fa fa-linkedin'
                                            aria-hidden='true'
                                          />
                                        </a>
                                        {/* <a class="li"><i class="fa fa-linkedin" aria-hidden="true"></i></a> */}
                                        {/* <a class="tw"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                 <a class="fb"><i class="fa fa-facebook" aria-hidden="true"></i></a> */}
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>

                            <div className='social-masnory-grid'>
                              <div className='soical-wall-main'>
                                <div className='inner-social-wall'>
                                  <div className='text-part'>
                                    <div className='text-header'>
                                      <p>
                                        <span>
                                          RT Jamescann:
                                        </span>
                                        10 Visual Content Marketing Statistics for 2017
                                        <span>#awward #celebrate</span>
                                      </p>
                                      <p class='text-link'>
                                        <a href='#'>
                                          http://owl.ly/4jFw30bAZar
                                        </a>
                                      </p>
                                    </div>
                                    <div className='scoial-icon clearfix'>
                                      <div className='visibly-icon' />
                                      <div className='social-icon li'>
                                        <a href='#'>
                                          <i
                                            class='fa fa-linkedin'
                                            aria-hidden='true'
                                          />
                                        </a>
                                        {/* <a class="li"><i class="fa fa-linkedin" aria-hidden="true"></i></a> */}
                                        {/* <a class="tw"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                 <a class="fb"><i class="fa fa-facebook" aria-hidden="true"></i></a> */}
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>

                            <div className='social-masnory-grid'>
                              <div className='soical-wall-main'>
                                <div className='inner-social-wall'>
                                  <div className='img-container'>
                                    <img src='/img/widget-image-3.jpg' />
                                  </div>
                                  <div className='text-part'>
                                    <div className='text-header'>
                                      <p>
                                        <span>
                                          RT Jamescann:
                                        </span>
                                        10 Visual Content Marketing Statistics for 2017
                                        <span>#awward #celebrate</span>
                                      </p>
                                      <p class='text-link'>
                                        <a href='#'>
                                          http://owl.ly/4jFw30bAZar
                                        </a>
                                      </p>
                                    </div>
                                    <div className='scoial-icon clearfix'>
                                      <div className='visibly-icon' />
                                      <div className='social-icon li'>
                                        <a href='#'>
                                          <i
                                            class='fa fa-linkedin'
                                            aria-hidden='true'
                                          />
                                        </a>
                                        {/* <a class="li"><i class="fa fa-linkedin" aria-hidden="true"></i></a> */}
                                        {/* <a class="tw"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                 <a class="fb"><i class="fa fa-facebook" aria-hidden="true"></i></a> */}
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>

                            <div className='social-masnory-grid'>
                              <div className='soical-wall-main'>
                                <div className='inner-social-wall'>
                                  <div className='img-container'>
                                    <img src='/img/widget-image-4.jpg' />
                                  </div>
                                  <div className='text-part'>
                                    <div className='text-header'>
                                      <p>
                                        <span>
                                          RT Jamescann:
                                        </span>
                                        10 Visual Content Marketing Statistics for 2017
                                        <span>#awward #celebrate</span>
                                      </p>
                                      <p class='text-link'>
                                        <a href='#'>
                                          http://owl.ly/4jFw30bAZar
                                        </a>
                                      </p>
                                    </div>
                                    <div className='scoial-icon clearfix'>
                                      <div className='visibly-icon' />
                                      <div className='social-icon li'>
                                        <a href='#'>
                                          <i
                                            class='fa fa-linkedin'
                                            aria-hidden='true'
                                          />
                                        </a>
                                        {/* <a class="li"><i class="fa fa-linkedin" aria-hidden="true"></i></a> */}
                                        {/* <a class="tw"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                 <a class="fb"><i class="fa fa-facebook" aria-hidden="true"></i></a> */}
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>

                            <div className='social-masnory-grid'>
                              <div className='soical-wall-main'>
                                <div className='inner-social-wall'>
                                  <div className='img-container'>
                                    <img src='/img/widget-image-4.jpg' />
                                  </div>
                                  <div className='text-part'>
                                    <div className='text-header'>
                                      <p>
                                        <span>
                                          RT Jamescann:
                                        </span>
                                        10 Visual Content Marketing Statistics for 2017
                                        <span>#awward #celebrate</span>
                                      </p>
                                      <p class='text-link'>
                                        <a href='#'>
                                          http://owl.ly/4jFw30bAZar
                                        </a>
                                      </p>
                                    </div>
                                    <div className='scoial-icon clearfix'>
                                      <div className='visibly-icon' />
                                      <div className='social-icon li'>
                                        <a href='#'>
                                          <i
                                            class='fa fa-linkedin'
                                            aria-hidden='true'
                                          />
                                        </a>
                                        {/* <a class="li"><i class="fa fa-linkedin" aria-hidden="true"></i></a> */}
                                        {/* <a class="tw"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                 <a class="fb"><i class="fa fa-facebook" aria-hidden="true"></i></a> */}
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div className='social-masnory-grid'>
                              <div className='soical-wall-main'>
                                <div className='inner-social-wall'>
                                  <div className='img-container'>
                                    <img src='/img/widget.png' />
                                  </div>
                                  <div className='text-part'>
                                    <div className='text-header'>
                                      <p>
                                        <span>
                                          RT Jamescann:
                                        </span>
                                        10 Visual Content Marketing Statistics for 2017
                                        <span>#awward #celebrate</span>
                                      </p>
                                      <p class='text-link'>
                                        <a href='#'>
                                          http://owl.ly/4jFw30bAZar
                                        </a>
                                      </p>
                                    </div>
                                    <div className='scoial-icon clearfix'>
                                      <div className='visibly-icon' />
                                      <div className='social-icon li'>
                                        <a href='#'>
                                          <i
                                            class='fa fa-linkedin'
                                            aria-hidden='true'
                                          />
                                        </a>
                                        {/* <a class="li"><i class="fa fa-linkedin" aria-hidden="true"></i></a> */}
                                        {/* <a class="tw"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                 <a class="fb"><i class="fa fa-facebook" aria-hidden="true"></i></a> */}
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>

                            <div className='social-masnory-grid'>
                              <div className='soical-wall-main'>
                                <div className='inner-social-wall'>
                                  <div className='text-part'>
                                    <div className='text-header'>
                                      <p>
                                        <span>
                                          RT Jamescann:
                                        </span>
                                        10 Visual Content Marketing Statistics for 2017
                                        <span>#awward #celebrate</span>
                                      </p>
                                      <p class='text-link'>
                                        <a href='#'>
                                          http://owl.ly/4jFw30bAZar
                                        </a>
                                      </p>
                                    </div>
                                    <div className='scoial-icon clearfix'>
                                      <div className='visibly-icon' />
                                      <div className='social-icon li'>
                                        <a href='#'>
                                          <i
                                            class='fa fa-linkedin'
                                            aria-hidden='true'
                                          />
                                        </a>
                                        {/* <a class="li"><i class="fa fa-linkedin" aria-hidden="true"></i></a> */}
                                        {/* <a class="tw"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                 <a class="fb"><i class="fa fa-facebook" aria-hidden="true"></i></a> */}
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>

                            <div className='social-masnory-grid'>
                              <div className='soical-wall-main'>
                                <div className='inner-social-wall'>
                                  <div className='img-container'>
                                    <img src='/img/widget-image-3.jpg' />
                                  </div>
                                  <div className='text-part'>
                                    <div className='text-header'>
                                      <p>
                                        <span>
                                          RT Jamescann:
                                        </span>
                                        10 Visual Content Marketing Statistics for 2017
                                        <span>#awward #celebrate</span>
                                      </p>
                                      <p class='text-link'>
                                        <a href='#'>
                                          http://owl.ly/4jFw30bAZar
                                        </a>
                                      </p>
                                    </div>
                                    <div className='scoial-icon clearfix'>
                                      <div className='visibly-icon' />
                                      <div className='social-icon li'>
                                        <a href='#'>
                                          <i
                                            class='fa fa-linkedin'
                                            aria-hidden='true'
                                          />
                                        </a>
                                        {/* <a class="li"><i class="fa fa-linkedin" aria-hidden="true"></i></a> */}
                                        {/* <a class="tw"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                 <a class="fb"><i class="fa fa-facebook" aria-hidden="true"></i></a> */}
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>

                            <div className='social-masnory-grid'>
                              <div className='soical-wall-main'>
                                <div className='inner-social-wall'>
                                  <div className='img-container'>
                                    <img src='/img/widget-image-4.jpg' />
                                  </div>
                                  <div className='text-part'>
                                    <div className='text-header'>
                                      <p>
                                        <span>
                                          RT Jamescann:
                                        </span>
                                        10 Visual Content Marketing Statistics for 2017
                                        <span>#awward #celebrate</span>
                                      </p>
                                      <p class='text-link'>
                                        <a href='#'>
                                          http://owl.ly/4jFw30bAZar
                                        </a>
                                      </p>
                                    </div>
                                    <div className='scoial-icon clearfix'>
                                      <div className='visibly-icon' />
                                      <div className='social-icon li'>
                                        <a href='#'>
                                          <i
                                            class='fa fa-linkedin'
                                            aria-hidden='true'
                                          />
                                        </a>
                                        {/* <a class="li"><i class="fa fa-linkedin" aria-hidden="true"></i></a> */}
                                        {/* <a class="tw"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                 <a class="fb"><i class="fa fa-facebook" aria-hidden="true"></i></a> */}
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <Script
                            url='/js/masonry.pkgd.min.js'
                            onCreate={this.handleScriptCreate}
                            onError={this.handleScriptError}
                            onLoad={this.handleScriptLoad}
                          />

                        </div>
                      </div>

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    )
  }
  open_main_menu () {
    this.setState({
      show_menu: !this.state.show_menu
    })
    var active_class = this.state.show_menu ? 'in-view has-sib' : ' has-sib'
    var handle_active_class = this.state.show_menu
      ? 'active inner_main_menu_handle'
      : 'inner_main_menu_handle'
    document.getElementById('navdraw').className = active_class
    document.getElementById(
      'inner_main_menu_handle'
    ).className = handle_active_class
  }
}

function mapStateToProps (state) {
  return {
    onboarding: state.settings.onboarding,
    profile: state.profile,
    companyInfo: state.settings.companyInfo,
    settings: state.settings,
    accounts: state.socialAccounts.accounts
  }
}



function mapDispatchToProps (dispatch) {
  return {
    widgetActions: bindActionCreators(widgetActions, dispatch),
    getSettings: bindActionCreators(getSettings, dispatch)
  }
}

let connection = connect(mapStateToProps, mapDispatchToProps)

module.exports = connection(WidgetSettings)

