import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import PreLoader from '../../PreLoader'
import * as widgetActions from '../../../actions/settings/widgetSettings'
import * as getSettings from '../../../actions/settings/getSettings'
import SidebarNarrow from '../../Layout/Sidebar/SidebarNarrow'
import Collapse, { Panel } from 'rc-collapse' // collapse
import Header from '../../Header/Header';
import SettingsNav from '../SettingsNav'
// collapse css
import { Link } from 'react-router'
import MapWidgetForm from './MapWidgetForm'
import WidgetNav from './WidgetNav'
import Globals from '../../../Globals'
import * as s3functions from '../../../utils/s3'

class MapWidgetSettings extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      googleKey: '',
      widgetURL: Globals.WIDGET_URL + Globals.MAP_SCRIPT_URL,
      currentTab: 'Widgets',
      show_menu: true
    }
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleChangeGoogleMap(event) {
    const target = event.target
    const value = target.value
  }

  handleSubmit(values) {
    this.props.widgetActions.widgetInfoSave(values)
    this.createScript(values.googleKey)
  }

  createScript(key) {
    const script = document.createElement('script')
    script.src = this.state.widgetURL
    // script.src = this.state.widgetURL;
    script.async = 1
    document.body.appendChild(script)
    script.onload = function () {
      const init = document.createElement('script')
      init.innerHTML =
        'MYLIBRARY.init(["' +
        key +
        '", "' +
        localStorage.getItem('map_secret') +
        '"]);'
      document.body.appendChild(init)
    }
    this.setState({ googleKey: key })
  }

  componentWillMount() {
    this.props.widgetActions.getMapWidgetInfo()
  }

  componentDidMount() {
    if (this.props.widgets.widgets.googleMapInfo.length > 0) {
      this.createScript(this.props.widgets.widgets.googleMapInfo[0].values)
    }
  }

  componentWillReceiveProps(nextProps) {
    // console.log(JSON.stringify(nextProps.companyInfo),'(JSON.stringify(nextProps.companyInfo)')
    // console.log(JSON.stringify(this.props.companyInfo),'(JSON.stringify(this.props.companyInfo)')
    if (
      JSON.stringify(nextProps.widgets.widgets.googleMapInfo) !=
      JSON.stringify(this.props.widgets.widgets.googleMapInfo)
    ) {
      this.createScript(nextProps.widgets.widgets.googleMapInfo[0].values)
    }
  }
  changeTab(tabText) {
    this.setState({
      currentTab: tabText
    })
  }
  render() {
    const divStyle = {
      bottom: 0,
      left: 0,
      right: 0,
      top: 0,
      width: '100%',
      height: '500px',
      padding: '0 0 10px 0'
    }
    
    let mapSetting = this.props.widgets.widgets.googleMapInfo.length !== 0
      ? this.props.widgets.widgets.googleMapInfo
      : ''

    return (
      <section id='settings' className='settings'>
        <Header title="Wall" nav={SettingsNav} location ={this.props.location} />
        
      <div className='main-container'>
        <div id='content'>

          <div className='page settings-container'>

            <div className='full-container'>

              <div className='widget'>
                <section className='register-page'>
                <WidgetNav  />
                  {/* <div className='section'>
                      {this.state.currentTab=='Widgets'?<WidgetSettings refetchList={this.props.refetchList}  Moment={this.state.Moment} location={this.props.location}></WidgetSettings>:<div></div>}
                      {this.state.currentTab=='Map Widgets'? <MapWidgetSettings campaign={this.props.campaign} Moment={this.props.Moment}></MapWidgetSettings>:<div></div>}

                    </div> */}
                </section>
                <div className='widget-page'>
                <div className='row'>
                  <div className='col-one-of-two'>
                        <header class='heading'>
                          <h3>Widget Accounts Settings</h3>
                        </header>
                        <MapWidgetForm
                          onSubmit={this.handleSubmit}
                          mapSetting={mapSetting}
                          location={this.props.location}
                        />
                      
                  </div>

                  <div className='col-one-of-two last'>
                        <header class='heading'>
                          <h3>
                            Widget Setup
                              {' '}
                            {/* <a className='btn' href={`#social-widget-container`}>Preview</a> */}
                          </h3>
                        </header>
                        <p className='section'>
                          To find your google map Key:
                            <ul>
                            <li>
                              Go to https://console.developers.google.com/
                              </li>
                            <li>
                              Under Google Maps API, choose Google Maps JavaScript API
                              </li>
                            <li>
                              Click create a project and enter a desired name
                              </li>
                            <li>Now click "Enable"</li>
                            <li>
                              After the API is enabled, go to credentials section & choose create credentials
                              </li>
                            <li>
                              Now choose API Key from the popup which has appeared
                              </li>
                            <li>Copy the key & paste it here</li>
                          </ul>
                        </p>
                        <p className='section'>
                          How to use this widget:
                            <ul>

                            <li>
                              Copy and Paste this code to your projects index.html.
                              </li>
                            <code>
                              &lt;script type="text/javascript" src="
                                {this.state.widgetURL}
                              "&gt;&lt;/script&gt;
                                &lt;script type="text/javascript"&gt;
                                <br />
                              MYLIBRARY.init(["
                                {this.state.googleKey}
                              ", "
                                {localStorage.getItem('map_secret')}
                              "]);
                                <br />
                              &lt;/script&gt;
                              </code>
                            <li>
                              Create a div with
                                <code>#map_element</code>
                              anywhere in your page to display social-wall
                                {'\n'}
                              <div>
                                <code>
                                  &lt;div id="map_element" style="width:500px; height:500px;" &gt;&lt;/div&gt;
                                  </code>
                              </div>
                            </li>
                          </ul>
                        </p>
                      
                  </div>
                </div>
                </div>
                <div className='map-row'>
                    <header>
                      <h3>Preview</h3>
                    </header>
                    <div className='map_element_wrapper'>
                  <div id='map_element' style={divStyle}>
                    <h4 class="map-widget-align">Please enter Google map key</h4>
                  </div>
                  </div>
                </div>
              </div>


            </div>
          </div>
        </div>
        </div>
      </section>
    )
  }
  open_main_menu() {
    this.setState({
      show_menu: !this.state.show_menu
    });
    var active_class = this.state.show_menu ? "in-view has-sib" : " has-sib";
    var handle_active_class = this.state.show_menu ? 'active inner_main_menu_handle' : 'inner_main_menu_handle';
    document.getElementById('navdraw').className = active_class;
    document.getElementById('inner_main_menu_handle').className = handle_active_class;

  }
}

function mapStateToProps(state) {
  return {
    widgets: state.widgets
  }
}

function mapDispatchToProps(dispatch) {
  return {
    widgetActions: bindActionCreators(widgetActions, dispatch),
    getSettings: bindActionCreators(getSettings, dispatch)
  }
}

let connection = connect(mapStateToProps, mapDispatchToProps)

module.exports = connection(MapWidgetSettings)
