
import {Field, reduxForm, propTypes} from 'redux-form'
import {renderField} from '../../Helpers/ReduxForm/RenderField';
import Header from '../../Header/Header';
import SettingsNav from '../SettingsNav'
class MapWidgetForm extends React.Component {
  
  constructor() {
    super();

  }  
  
  // this will be called 1 time when the component loads
  componentDidMount() {
    this.handleInitialize(this.props);
  }

  // this will be called when some props of this component is changed (either from parent or from redux)
  componentWillReceiveProps(nextProps) {
    if (JSON.stringify(nextProps.mapSetting) != JSON.stringify(this.props.mapSetting)) {
      this.handleInitialize(nextProps);
    }
    
  }


  handleInitialize(nextProps=null) {
    const initData = {
      "googleKey": (nextProps.mapSetting.length === 0 && nextProps.mapSetting[0] === undefined) ? "" : nextProps.mapSetting[0].values,
      "googleKey_identity": (nextProps.mapSetting.length === 0 && nextProps.mapSetting[0] === undefined) ? "" : nextProps.mapSetting[0].identity,
    };

    this
      .props
      .initialize(initData);
  }
  render() {
    const {handleSubmit, pristine, submitting} = this.props;
    return (
      <section id='settings' className='map_widget'>
      {/* <Header title="Map" nav={SettingsNav} location ={this.props.location} /> */}
      <form onSubmit={handleSubmit}>
        <div className="section">
          <Field
            placeholder="Google MAP key"
            label="Google MAP key"
            component={renderField}
            type="text"
            name="googleKey"/>
            <div className="form-row">
              <button
                class="btn btn-primary right"
                type="submit"
                disabled={pristine || submitting}>Save</button>
            </div>
        </div>
      </form>
      </section>
    )
  }
}


export default reduxForm({
  form: 'mapWidget', // a unique identifier for this form
})(MapWidgetForm)
