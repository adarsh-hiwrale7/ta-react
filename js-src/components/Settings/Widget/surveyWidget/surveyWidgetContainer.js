
import { connect } from 'react-redux';
import notie from 'notie/dist/notie';
import Header from '../../../Header/Header';
import SettingsNav from '../../SettingsNav';
import WidgetNav from '../WidgetNav';
import CenterPopupWrapper from '../../../CenterPopupWrapper';
import CreateFunnelForm from './CreateFunnelForm';
import { createFunnel, editFunnle, fetchFunnelList, getFunnelData, deleteFunnelAction, generateWidget, editFunnel } from '../../../../actions/settings/surveyWidget';
import PreLoader from '../../../PreLoader';
import reactable from '../../../Chunks/ChunkReactable'
import moment from '../../../Chunks/ChunkMoment'
import FetchuserDetail from '../../../FetchUsersDetail/FetchUsersDetail';
import TableSearchWrapper from '../../../TableSearchWrapper';
import FilterTableColumns from '../../../FilterTableColumns';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import RLDD from 'react-list-drag-and-drop/lib/RLDD';
import Globals from '../../../../Globals'


import Script from 'react-load-script'
var scriptext = '';
var scripttext2 = ''
var preference = null;

let countColums = 0;
const tableColumns = [
  ['avtar', 'USER IMG', ''],
  ['name', 'USER NAME', ''],
  ['funnelname', 'Funnel Name', ''],
  ['Created', 'Created', ''],
  ['Updated', 'Updated', ''],
  ['action', 'action', 'action-td'],
];
class WidgetContainer extends React.Component {
  constructor(props) {
    super(props);
    this.scrollRef =null;
    this.state = {
      openFunnel: false,
      reactable: null,
      moment: null,
      main_state: [],
      filter_state: [],
      widgetCodeSnippet: "demo",
      preference: null,
      feed: false,
      feedback: false,
      widgetSecret: null,
      stageList: [],
      widgetCode: null,
      widgetSupportCode: null,
      question: null,
      feed: 0,
      feedback: 0,

    };
  }

  componentDidMount() {
    reactable().then(reactable => {
      moment().then(moment => {
        this.setState({ moment: moment, reactable: reactable })
      })
    });
    this.props.fetchFunnelList();
  }

  componentDidUpdate(prevProps) {

    if (this.props.widgets.generatedWidget != undefined && prevProps.widgets.generatedWidget != this.props.widgets.generatedWidget) {
      this.setState({
        widgetSecret: JSON.parse(atob(this.props.widgets.generatedWidget)).widget_secret,
        feed: JSON.parse(atob(this.props.widgets.generatedWidget)).feed,
        feedback: JSON.parse(atob(this.props.widgets.generatedWidget)).feedback
      })
      this.scrollToBottom()
    }

    if (this.props.widgets.currFunnelData != undefined && this.props.widgets.currFunnelData.stages != undefined && this.props.widgets.currFunnelData.stages != prevProps.widgets.currFunnelData.stages) {

      this.setState({
        stageList: this.props.widgets.currFunnelData.stages,
      })
    }

    if (this.props.widgets.generatedWidget != undefined && prevProps.widgets.generatedWidget != this.props.widgets.generatedWidget) {
      this.setState({
        widgetSecret: JSON.parse(atob(this.props.widgets.generatedWidget)).widget_secret,
        feed: JSON.parse(atob(this.props.widgets.generatedWidget)).feed,
        feedback: JSON.parse(atob(this.props.widgets.generatedWidget)).feedback
      })
      this.scrollToBottom()
    }

    const { createFunnelLoading, funnelList } = this.props.widgets.funnelWidget;
    if (prevProps.widgets.funnelWidget.createFunnelLoading !== createFunnelLoading &&
      createFunnelLoading === false) {
      this.setState({
        openFunnel: false,
        isEdit: false,
      });
    }
    if (funnelList !== prevProps.widgets.funnelWidget.funnelList) {
      this.setState({ main_state: this.props.widgets.funnelWidget.funnelList })
      this.setState({ filter_state: this.props.widgets.funnelWidget.funnelList })
    }
  }

  createDropdown(tableColumns) {
    var me = this
    return (
      <ul class='dropdown-menu checkboxDropdown'>
        {tableColumns
          ? tableColumns.map(function (column, columnIndex) {
            let columnState = column[0]
            let columnLabel = column[1]
            if (me.state[columnState] == undefined) {
              me.setState({
                [columnState]: ''
              })
            }
            return (
              <li key={columnIndex}>
                <div className='form-row checkbox'>
                  <input
                    id={columnState}
                    name={columnState}
                    className='checknotify'
                    type='checkbox'
                    onChange={me.checkDetails.bind(me, columnState)}
                    checked={me.state[columnState] == ''}
                  />
                  <label for={columnState}>{columnLabel}</label>
                </div>
              </li>
            )
          })
          : ''}
      </ul>
    )
  }

  changeState(stateName, value) {
    var newState = {}
    newState[stateName] = value
    this.setState(newState)
  }

  checkDetails(e) {
    var i = 0
    var checkboxvalue = document.getElementById(e).checked
    if (checkboxvalue == true) {
      this.changeState(e, '')
      countColums++
    } else {
      this.changeState(e, 'hide')
      countColums--
    }
  }

  closePopup = () => {
    this.setState({
      openFunnel: false,
      isEdit: false,
    })
  }

  createFunnel = () => {
    this.setState({ openFunnel: true });
  }

  submitFunnelForm = (values) => {

    const arrayData = {};
    arrayData.name = values.funnelName;
    arrayData.type = values.funnelType;

    arrayData.feed_create = 0;
    if (values.feedName) {
      arrayData.feed_name = values.feedName;
      arrayData.feed_create = 1;
    }
    arrayData.stages = values.stages;
    if (values.identity) {
      arrayData.funnel_id = values.identity;
    }
    if (this.state.isEdit == true) { this.props.editFunnel(arrayData); }
    else { this.props.createFunnel(arrayData); }
  }

  editFunnelPopup = funnelData => {
    const { getFunnelData } = this.props;
    this.setState({
      openFunnel: true,
      isEdit: true,
    });
    getFunnelData(funnelData.identity);
  };

  deleteFunnel = funnelData => {
    const { deleteFunnelAction } = this.props;
    notie.confirm(
      `Are you sure you want to delete this funnel?`,
      'Yes',
      'No',
      function () {
        deleteFunnelAction(funnelData.identity);
      },
      function () {

      });
  };

  renderStages = (funnelData) => {
    const { getFunnelData } = this.props;
    getFunnelData(funnelData.identity);
    this.setState({
      renderStages: true,
    });
  }

  deleteStage = (index, stageData) => {
    this.state.stageList.splice(index, 1);
    var stages = [];
    this.state.stageList.filter(object =>
      stages.push(object.name)
    );

    var objToPass = {
      funnel_id: this.props.widgets.currFunnelData.identity,
      stages: stages
    }

    this.props.editFunnel(objToPass)
    this.setState({ stageList: this.state.stageList })

  }

  getFeedback = (data) => {
    this.setState({
      feedback: data
    })
  }
  getPreference = (data) => {
    this.setState({
      preference: data,
    })
  }

  getFeed = (data) => {
    this.setState({
      feed: data
    })
  }

  copyWidgetCode() {
    notie.alert('success', 'Copied successfully.', 3)
  }

  copyWidgetSupportCode() {
    notie.alert('success', 'Copied successfully.', 3)
  }

  generateWidget(data) {

    var preference1 = this.state.preference != null ? this.state.preference.split('-') : ['']
    var feed = this.state.feed == true ? 1 : 0;
    var feedback = this.state.feedback == true ? 1 : 0
    preference = preference1[0]

    if (preference1[0] == '') {
      notie.alert('error', 'Please select preference', 3);
    }
    else {
      var objToPass = {
        feed: feed,
        feedback: feedback,
        preference: preference,
        stage_id: data.identity
      }
    }
    this.props.generateWidget(objToPass)
  }

  //for handling drag and drop functionality of stages
  handleRLDDChange(reorderedItems) {
    this.setState({ stageList: reorderedItems })
    var stageList = [];

    this.props.widgets.currFunnelData.stages.filter(object => {
      stageList.push(object.name);
    })


    var objToPass = {
      funnel_id: this.props.widgets.currFunnelData.identity,
      stages: stageList
    }
    this.props.editFunnel(objToPass)
  }

  renderStageItems = (stageData, index) => {
    return (
      <div key={`stage-index-${stageData.identity}`} className="stages-card">
        <a className="close-stage" onClick={() => { this.deleteStage(index, stageData) }}>
          <i className='material-icons'>clear</i>
        </a>

        <div className="stage-name">
          <span className=""><i className='material-icons'>drag_indicator</i></span>
          <p>{stageData.name}</p>
        </div>
        <div className="stage-options">
          <div className="radiobtnFeed">
            <label class='customradio'>
              <input
                type='radio'
                name={`feedback-${stageData.identity}`}
                value={`feedback-${stageData.identity}`}
                id={`feedback-${stageData.identity}`}
                onClick={(e) => { this.getFeedback(e.target.checked) }}
              />
              <span class="checkmark"></span>
              Feedback
            </label>
          </div>
          <div className="radiobtnFeed">
            <label class='customradio'>
              <input
                type='radio'
                name={`feed-${stageData.identity}`}
                value={`feed-${stageData.identity}`}
                id={`feed-${stageData.identity}`}
                onClick={(e) => { this.getFeed(e.target.checked) }}
              />
              <span class="checkmark"></span>
              Feed
            </label>
          </div>
          <div className="radiobtnFeed stage-options-inline">
            <label class='customradio'>
              <input
                type='radio'
                name={`type-${stageData.identity}`}
                value={`website-${stageData.identity}`}
                id={`website-${stageData.identity}`}
                checked={this.state.preference == `website-${stageData.identity}` ? true : false}
                onClick={() => { this.getPreference(`website-${stageData.identity}`) }}
              />
              <span class="checkmark"></span>
              Website
            </label>
            <label class='customradio'>
              <input
                type='radio'
                name={`type-${stageData.identity}`}
                value={`email-${stageData.identity}`}
                id={`email-${stageData.identity}`}
                checked={this.state.preference == `email-${stageData.identity}` ? true : false}
                onClick={() => { this.getPreference(`email-${stageData.identity}`) }}
              />
              <span class="checkmark"></span>
              Email
            </label>
            <label class='customradio'>
              <input
                type='radio'
                name={`type-${stageData.identity}`}
                value={`sms-${stageData.identity}`}
                id={`sms-${stageData.identity}`}
                checked={this.state.preference == `sms-${stageData.identity}` ? true : false}
                onClick={() => { this.getPreference(`sms-${stageData.identity}`) }}
              />
              <span class="checkmark"></span>
              SMS
            </label>
          </div>
          <div className="btn-generate-wrapper">
            <button className='btn btn-default btn-generate' onClick={() => { this.generateWidget(stageData) }}>
              Generate
          </button>
          </div>
        </div>
      </div>

    )
  }

  renderStagesList = () => {
    const { currFunnelData, currFunnelLoading, generateWidgetLoading } = this.props.widgets;
    return (

      currFunnelLoading ? <div class="preloaderWrapPage">
        <div class="visibly-loader">
          <img src="/img/visibly-loader.gif" alt="Loader" />
        </div>
      </div>
        : <div className="survey-widget-stage-container">

          {this.state.stageList.length > 0 ?
            // data = this.data(currFunnelData.stages)
            <RLDD
              items={this.state.stageList}
              itemRenderer={this.renderStageItems}
              onChange={this.handleRLDDChange.bind(this)}
            />
            : null
          }
        </div >
    );
  }

  renderCreateFunnel() {
    const { currFunnelData } = this.props.widgets;
    const { isEdit } = this.state;
    return (
      <div className="create-funnel-wrapper">
        <CenterPopupWrapper>
          <header className='heading'>
            <h3>SETUP SURVEY WIDGET</h3>
            <button
              id='close-popup'
              className='btn-default'
              onClick={this.closePopup}
            >
              <i class='material-icons'>clear</i>
            </button>
          </header>

          <CreateFunnelForm
            onSubmit={this.submitFunnelForm}
            currFunnelData={isEdit ? currFunnelData : null}
          />
        </CenterPopupWrapper>
      </div>
    )
  }

  search_text(e) {
    var me = this;
    let Tabledata = this.props.widgets.funnelWidget.funnelList;
    let search = e.target.value.toLowerCase()
    if (e.target.value !== "") {
      var tempobj = Tabledata.filter(function (tableItem) {
        var tdData1 = me.state.moment.unix(tableItem.created_at).format('DD-MM-YYYY');
        var tdData2 = me.state.moment.unix(tableItem.updated_at).format('DD-MM-YYYY');
        if (tdData1.toLowerCase().includes(search) ||
          tableItem.users.name.toLowerCase().includes(search) ||
          tdData2.toLowerCase().includes(search) ||
          tableItem.name.toLowerCase().includes(search))
          return true;
        else
          return false
      })
      this.setState({ main_state: tempobj })
    }
    else {
      this.setState({ main_state: this.state.filter_state })
    }
  }

  renderFunnelList() {
    const me = this;
    return (
      <div>
        <div className="heading">
          <h3>LIVE FUNNELS</h3>
        </div>
        <div className={`${me.state.renderStages?'table-white-container funnel-page-container clearfix':'table-white-container funnel-page-container clearfix full-table-width'}`}>
          <div className='tableControllers clearfix funnel-list-wrapper'>
            <div className='tableControllers clearfix'>
              <TableSearchWrapper>
                <input type="text" placeholder='Search in funnels' onChange={this.search_text.bind(this)} />
              </TableSearchWrapper>
              <div className="filtertable">
                <FilterTableColumns>
                  {this.createDropdown(tableColumns)}
                </FilterTableColumns>
              </div>
            </div>
            {this.state.main_state.length > 0 ?
              <div className='tabel-view-userdata'>
                <div className="funnel-table-wrapper table-user-body ">
                  {this.state.reactable ?
                    <this.state.reactable.Table
                      className='table responsive-table campaign-table'
                      id="funnel-table"
                      itemsPerPage={10}
                      pageButtonLimit={5}>
                      <this.state.reactable.Thead>
                        {
                          tableColumns.map(function (column, columnIndex) {
                            let columnState = column[0];
                            let columnLabel = column[1];
                            let centerClass = column[2];
                            return (
                              <me.state.reactable.Th
                                key={columnIndex}
                                column={columnState}
                                className={`${columnState} ${me.state[columnState]} ${centerClass}`}
                              >
                                {columnLabel}
                              </me.state.reactable.Th>
                            )
                          })
                        }
                      </this.state.reactable.Thead>
                      {this.state.main_state.map((item, index) => {
                        return (
                          <this.state.reactable.Tr
                            className='table-body-text' key={index}>
                            {tableColumns.map(function (column, columnIndex) {

                              let columnState = column[0]
                              let columnLabel = column[1]
                              let centerClass = column[2]
                              let tdData;
                              if (columnState === 'avtar') {
                                tdData = item.users.profile_image
                              } else if (columnState === "name") {
                                tdData = item.users.name;
                              }else if (columnState === "funnelname") {
                                tdData = item.name;
                              } else if (columnState === "Created") {
                                tdData = me.state.moment ? me.state.moment(item.created_at * 1000).format('DD-MM-YYYY') : ''
                              } else if (columnState === 'Updated') {
                                tdData = me.state.moment ? me.state.moment(item.updated_at * 1000).format('DD-MM-YYYY') : ''
                              }
                              return (
                                <me.state.reactable.Td
                                  key={columnIndex}
                                  column={columnState}
                                  className={`${columnState} ${me.state[columnState]} ${centerClass}`}
                                  data-rwd-label={columnLabel}
                                >
                                  {columnState == 'avtar' ?
                                  <div className="post">
                                      <div className="author-thumb">
                                        <img
                                          src={tdData}
                                          alt={"Default image"}
                                          className='list-avatar thumbnail'
                                          data-tip={item.users.email}
                                          data-for={item.users.identity}
                                          onError={(e) => { e.target.src = "https://app.visibly.io/img/default-avatar.png" }}
                                        />
                                        <FetchuserDetail userID={item.users.identity}/>
                                      </div>
                                    </div>
                                    : columnState === 'action' ?
                                      <div className='action-button'>
                                        <ul
                                          className='dd-menu context-menu user_3dots'
                                        >
                                          <li className='button-dropdown'>
                                            <a className='dropdown-toggle user_drop_toggle'>
                                              <i className='material-icons'>more_vert</i>
                                            </a>

                                            <ul className='dropdown-menu user_dropAction'>
                                              <li onClick={me.editFunnelPopup.bind(me, item)}>
                                                <span className='btn'>
                                                  Edit
                                                </span>
                                              </li>
                                              <li onClick={me.deleteFunnel.bind(me, item)}>
                                                <span className='btn'>
                                                  Delete
                                                </span>
                                              </li>

                                            </ul>
                                          </li>
                                        </ul>
                                      </div>
                                      :
                                      <div onClick={() => { me.renderStages(item) }}>
                                        {tdData}
                                      </div>}
                                </me.state.reactable.Td>
                              )
                            }
                            )}
                          </this.state.reactable.Tr>
                        )
                      })}
                    </this.state.reactable.Table>
                    : null}
                </div>
              </div>
              
              :
              <div class="no-data-block">
                No funnel found.
          </div>
            } 
             
            <div ref={(el) => { this.scrollRef = el; }}> </div>
            {this.state.widgetSecret != null && !this.props.widgets.generateWidgetLoading ?
              <div className="row survey-copy-paste-wrapper" >
                <div class="col-one-of-two-left">
                  <div className="cpy-paste">
                    <label>
                      {preference == 'website' ? 'Copy and Paste this code to your projects index.html.'
                        : preference == 'email' ? 'Copy and Paste this code to your email.' : 'Copy and Paste this Link to your SMS'}
                    </label>
                    <div className="form-row text scripttext">

                      {preference == 'website'
                        ?
                        <code id="surveyWidgetCode">
                          &lt;script type="text/javascript"&gt;
                        <br />
                        var $widget_secret = "{this.state.widgetSecret}"
                        <br/>
                        var $feed = "{this.state.feed}"
                        <br/>
                        var $feedback = "{this.state.feedback}"
                        <br /><br />
                        &lt;/script&gt;
                        <br />
                        &lt;script type="text/javascript" src="{' '}{Globals.WIDGET_URL + '/js/SurveyWidget.js'}"
                        &gt;&lt;/script&gt;
                    </code>
                        : preference == 'email'
                          ? <code id="surveyWidgetCode">
                             &lt;!DOCTYPE html&gt;
                              <br/>
                              &lt;html lang="en"&gt;
                              <br/>

                              &lt;head&gt;
                              <br/>
                                  &lt;meta charset="UTF-8"&gt;
                                  <br/>

                                &lt;meta name="viewport" content="width=device-width, initial-scale=1.0"&gt;
                                <br/>

                                  &lt;meta http-equiv="X-UA-Compatible" content="ie=edge"&gt;
                                  <br/>

                                &lt;title&gt;Values&lt;/title&gt;
                                <br/>

                              &lt;/head&gt;
                              <br/>


                              &lt;body style="background-color: #ededed; margin:0;"&gt;
                              <br/>

                              &lt;p&gt;
                              <br/>

                                  &lt;/p&gt;
                                  <br/>
                                  
                                  &lt;table style="width:100%;background:#ededed;margin: 0 auto;"&gt;
                                  <br/>
                                      &lt;tr&gt;
                                      <br/>
                                          &lt;td align="center"&gt;
                                          <br/>
                                  
                                              &lt;table align="center" style="text-align:left; width: 766px; margin: 0 auto; padding: 0;" cellspacing="0"
                                              cellpadding="0"&gt;
                                              <br/>

                                                  &lt;tbody&gt;
                                                  <br/>
                                  
                                                      &lt;tr&gt;
                                                      <br/>

                                                          &lt;td style="background-color: #ffffff; margin-bottom: 18px; padding: 50px 65px 65px 50px;"&gt;
                                                          <br/>

                                                          &lt;table style="width: 100%;" cellspacing="0" cellpadding="0"&gt;
                                                          <br/>

                                                                  &lt;tbody style="display: block; border: 2px solid #ccc;"&gt;
                                                                  <br/>

                                                                      &lt;tr&gt;
                                                                      <br/>

                                                                          &lt;td&gt;
                                                                          <br/>

                                                                              &lt;p style="font-family: Montserrat, Helvetica, sans-serif; font-size: 14px; font-weight: 400; color: #333333; line-height: 28px; margin-bottom: 0px; padding: 10px 10px 10px 14px;"&gt;
                                                                                  On a scale of zero to ten, how likely are you to recommend our business to a friend or colleague?&lt;/p&gt;
                                                                              &lt;!-- &lt;p style="margin-bottom:30px;"&gt;&lt;a style="cursor:pointer" class="button_link"
                                                                                      target="_blank"&gt;&lt;img src="https://visibly.io/wp-content/uploads/2018/07/reset-btn.png"
                                                                                          alt="reset-btn"&gt; &lt;/a&gt;&lt;/p&gt; --&gt;
                                                                                          <br/>
                                    
                                                                          &lt;/td&gt;
                                                                          <br/>

                                                                      &lt;/tr&gt;
                                                                      <br/>

                                                                      &lt;tr style="border: 1px solid #ccc;display: block;"&gt;
                                                                      <br/>
                                                                          &lt;td style="width: 138px;display: inline-block;height:45px;"&gt;&lt;img src="https://visibly.io/wp-content/uploads/2018/07/nps.png"style="height:45px;"&gt;&lt;/td&gt;
                                                                          <br/>

                                                                          &lt;td style="width: 40px;display: inline-block;height: 45px;border-right:1px solid #ccc;text-align: center;line-height: 42px;cursor: pointer;"&gt;
                                                                          <br/>

                                                                          &lt;a style="text-decoration: none; font-family: Montserrat, Helvetica, sans-serif;font-size:12px;font-weight: 600;color: #ff595e" href="{Globals.WIDGET_URL +
                                                                              `/widget.blade.php?op=0&widget_secret=${this.state.widgetSecret}&feed=${this.state.feed}&feedback=${this.state.feedback}`}"&gt; 0&lt;/a&gt; 
                                                                              &lt;/td&gt;
                                                                              <br/>

                                                                          &lt;td style="width: 40px;display: inline-block;height: 45px;border-right:1px solid #ccc;text-align: center;line-height: 42px;cursor: pointer;"&gt;
                                                                          <br/>
                                                                          &lt;a style="text-decoration: none;font-family: Montserrat, Helvetica, sans-serif;font-size:12px;font-weight: 600;color: #ff595e" href="{Globals.WIDGET_URL +
                                                                              `/widget.blade.php?op=1&widget_secret=${this.state.widgetSecret}&feed=${this.state.feed}&feedback=${this.state.feedback}`}"&gt; 1&lt;/a&gt; 
                                                                          &lt;/td&gt;
                                                                          <br/>

                                                                          &lt;td style="width: 40px;display: inline-block;height: 45px;border-right:1px solid #ccc;text-align: center;line-height: 42px;cursor: pointer;"&gt;
                                                                            &lt;a style="text-decoration: none;font-family: Montserrat, Helvetica, sans-serif;font-size:12px;font-weight: 600;color: #ff595e" href="{Globals.WIDGET_URL +
                                                                              `/widget.blade.php?op=2&widget_secret=${this.state.widgetSecret}&feed=${this.state.feed}&feedback=${this.state.feedback}`}"&gt; 2&lt;/a&gt;  
                                                                          &lt;/td&gt;
                                                                          <br/>

                                                                          &lt;td style="width: 40px;display: inline-block;height: 45px;border-right:1px solid #ccc;text-align: center;line-height: 42px;cursor: pointer;"&gt;
                                                                            &lt;a style="text-decoration: none;font-family: Montserrat, Helvetica, sans-serif;font-size:12px;font-weight: 600;color: #ff595e" href="{Globals.WIDGET_URL +
                                                                              `/widget.blade.php?op=3&widget_secret=${this.state.widgetSecret}&feed=${this.state.feed}&feedback=${this.state.feedback}`}"&gt;3&lt;/a&gt; 
                                                                          &lt;/td&gt;
                                                                          <br/>

                                                                          &lt;td style="width: 40px;display: inline-block;height: 45px;border-right:1px solid #ccc;text-align: center;line-height: 42px;cursor: pointer;"&gt;
                                                                            &lt;a style="text-decoration: none;font-family: Montserrat, Helvetica, sans-serif;font-size:12px;font-weight: 600;color: #ff595e" href="{Globals.WIDGET_URL +
                                                                              `/widget.blade.php?op=4&widget_secret=${this.state.widgetSecret}&feed=${this.state.feed}&feedback=${this.state.feedback}`}"&gt;4&lt;/a&gt; 
                                                                          &lt;/td&gt;
                                                                          <br/>

                                                                          &lt;td style="width: 39px;display: inline-block;height: 45px;border-right:1px solid #ccc;text-align: center;line-height: 42px;cursor: pointer;"&gt;
                                                                            &lt;a style="text-decoration: none;font-family: Montserrat, Helvetica, sans-serif;font-size:12px;font-weight: 600;color: #ff595e" href="{Globals.WIDGET_URL +
                                                                              `/widget.blade.php?op=5&widget_secret=${this.state.widgetSecret}&feed=${this.state.feed}&feedback=${this.state.feedback}`}"&gt;5&lt;/a&gt; 
                                                                          &lt;/td&gt;
                                                                          <br/>

                                                                          &lt;td style="width: 39px;display: inline-block;height: 45px;border-right:1px solid #ccc;text-align: center;line-height: 42px;cursor: pointer;"&gt;
                                                                            &lt;a style="text-decoration: none;font-family: Montserrat, Helvetica, sans-serif;font-size:12px;font-weight: 600;color: #ff595e" href="{Globals.WIDGET_URL +
                                                                              `/widget.blade.php?op=6&widget_secret=${this.state.widgetSecret}&feed=${this.state.feed}&feedback=${this.state.feedback}`}"&gt;6&lt;/a&gt;  
                                                                          &lt;/td&gt;
                                                                          <br/>

                                                                          &lt;td style="width: 39px;display: inline-block;;height: 45px;border-right:1px solid #ccc;text-align: center;line-height: 42px;cursor: pointer;"&gt;
                                                                            &lt;a style="text-decoration: none; font-family: Montserrat, Helvetica, sans-serif;font-size:12px;font-weight: 600;color: #ff595e;" href="{Globals.WIDGET_URL +
                                                                              `/widget.blade.php?op=7&widget_secret=${this.state.widgetSecret}&feed=${this.state.feed}&feedback=${this.state.feedback}`}"&gt;7&lt;/a&gt; 
                                                                          &lt;/td&gt;
                                                                          <br/>

                                                                          &lt;td style="width: 39px;display: inline-block;height: 45px;border-right:1px solid #ccc;text-align: center;line-height: 42px;cursor: pointer;"&gt;
                                                                            &lt;a style="text-decoration: none;font-family: Montserrat, Helvetica, sans-serif;font-size:12px;font-weight: 600;color: #ff595e" href="{Globals.WIDGET_URL +
                                                                              `/widget.blade.php?op=8&widget_secret=${this.state.widgetSecret}&feed=${this.state.feed}&feedback=${this.state.feedback}`}"&gt;8&lt;/a&gt; 
                                                                          &lt;/td&gt;
                                                                          <br/>

                                                                          &lt;td style="width: 39px;display: inline-block;height: 45px;border-right:1px solid #ccc;text-align: center;line-height: 42px;cursor: pointer;"&gt;
                                                                            &lt;a style="text-decoration: none;font-family: Montserrat, Helvetica, sans-serif;font-size:12px;font-weight: 600;color: #ff595e" href="{Globals.WIDGET_URL +
                                                                              `/widget.blade.php?op=9&widget_secret=${this.state.widgetSecret}&feed=${this.state.feed}&feedback=${this.state.feedback}`}"&gt;9&lt;/a&gt; 
                                                                          &lt;/td&gt;
                                                                          <br/>

                                                                          &lt;td style="width: 39px;display: inline-block;height: 45px;text-align: center;line-height: 40px;cursor: pointer;"&gt;
                                                                            &lt;a style="text-decoration: none;font-family: Montserrat, Helvetica, sans-serif;font-size:12px;font-weight: 600;color: #ff595e" href="{Globals.WIDGET_URL +
                                                                              `/widget.blade.php?op=10&widget_secret=${this.state.widgetSecret}&feed=${this.state.feed}&feedback=${this.state.feedback}`}"&gt;10&lt;/a&gt; 
                                                                          &lt;/td&gt;
                                                                          <br/>

                                                                      &lt;/tr&gt;
                                                                      <br/>

                                                                    
                                                                  &lt;/tbody&gt;
                                                                  <br/>

                                                                  &lt;tbody&gt;
                                                                  <br/>

                                                                      &lt;tr style="width: 100%;display: inline-table;"&gt;  
                                                                      <br/>
                                                                      &lt;td style="padding-left: 141px;padding-top: 7px;display: inline-block;font-family: Montserrat, Helvetica, sans-serif;font-size:14px;font-weight: 500;color: #333333;"&gt;Not likely&lt;/td&gt;
                                                                      &lt;td style="text-align: right;padding-top: 7px;font-family: Montserrat, Helvetica, sans-serif;font-size:14px;font-weight: 500;color: #333333;"&gt;Very likely&lt;/td&gt;
                                                                      <br/>
                                                                      &lt;/tr&gt;
                                                                  &lt;/tbody&gt;
                                                                  <br/>
                                                              &lt;/table&gt;
                                                              <br/>
                                                          &lt;/td&gt;
                                                          <br/>
                                                      &lt;/tr&gt;
                                                      <br/>
                                                  &lt;/tbody&gt;
                                                  <br/>
                                              &lt;/table&gt;
                                              <br/>
                                          &lt;/td&gt;
                                          <br/>
                                      &lt;/tr&gt;
                                      <br/>
                                  &lt;/table&gt;
                                  <br/>
                                  &lt;/body&gt;
                                  <br/>
                              &lt;/html&gt;
                            </code>
                          : <code id="surveyWidgetCode"> {Globals.WIDGET_URL + `/widget.blade.php?medium=sms&widget_secret=${this.state.widgetSecret}&feed=${this.state.feed}&feedback=${this.state.feedback}`} </code>
                      }
                    </div>
                    <div className="widget-copy-btn right">

                      <CopyToClipboard
                        onCopy={this.copyWidgetCode.bind(this)}
                        options={{ message: 'Whoa!' }}
                        text={scriptext}>
                        <input type="button" class="btn btn-integration" value="Copy" />
                      </CopyToClipboard>
                    </div>
                  </div>
                </div>
                {preference == "website" ?
                  <div class="col-one-of-two-right" >
                    <label>
                      Create a div with {' '}
                      <code >#SurveyWidget</code>
                      {' '}
                      anywhere in your page
                             </label>
                    {' '}
                    {'\n'}
                    <div className="form-row text scripttext2">
                      <code id="surveyWidgetCode2">&lt;div id="SurveyWidget"&gt;&lt;/div&gt;</code>
                    </div>
                    <div className="widget-copy-btn right">
                      <CopyToClipboard
                        options={{ message: 'Whoa!' }}
                        text={scripttext2}>
                        <input type="button" class="btn btn-integration" value="Copy" onClick={this.copyWidgetSupportCode.bind(this)} />
                      </CopyToClipboard>
                    </div>
                  </div> : ''
                }
              </div>
              : this.props.widgets.generateWidgetLoading ? <div class="visibly-loader">
                <img src="/img/visibly-loader.gif" alt="Loader" />
              </div> : null}
          </div>
          <div className="funnel-stages-wrapper">
            {me.state.renderStages ?
              this.renderStagesList()
              : null
            }
          </div>
        </div>
      </div >
    )
  }
  
  /*
  @author: Akshay soni
  @purpose: To scroll down once the widget gets generates
   */
  scrollToBottom = () => {
    this.scrollRef.scrollIntoView({ behavior: "smooth" });
  }

  render() {
    scriptext = document.getElementById('surveyWidgetCode') != null ? document.getElementById('surveyWidgetCode').innerText : '';
    scripttext2 = document.getElementById('surveyWidgetCode2') != null ? document.getElementById('surveyWidgetCode2').innerText : '';

    const { openFunnel, renderStages } = this.state;
    const { createFunnelLoading, currFunnelLoading, deleteFunnelLoading } = this.props.widgets.funnelWidget;
    return (
      <section id='settings' className='settings'>
        {openFunnel && this.renderCreateFunnel()}
        {createFunnelLoading || currFunnelLoading || deleteFunnelLoading ? <PreLoader /> : null}
        <Header title='Wall' nav={SettingsNav} location={this.props.location} />

        <div className='main-container'>
          <div id='content'>

            <div className='page settings-container'>

              <div className='full-container'>


                <div className='widget'>
                  <div className='inner-widget'>
                    <section className='register-page'>
                      <WidgetNav />
                    </section>

                    <ul className='dd-menu context-menu widget_3dots'>

                      <li className='button-dropdown'>
                        <a className='dropdown-toggle'>
                          <i className='material-icons'>more_vert</i>
                        </a>
                        <ul className='dropdown-menu'>
                          <li class='dropdown-parent-li' onClick={this.createFunnel}>
                            <a className="">
                              Create new funnel
                                </a>

                          </li>
                        </ul>
                      </li>
                    </ul>
                    {this.props.widgets.funnelWidget.funnelListLoading ?
                      <PreLoader />
                      :
                      this.props.widgets.funnelWidget.funnelList.lenth === 0 ?
                        <div className="first-widget-funnel">
                          <div className="center">
                            <h4>
                              Create your first NPS funnel.
                              </h4>
                            <div className="funnel-image">

                            </div>
                          </div>
                        </div>
                        :
                        <div>
                          {this.renderFunnelList()}
                        </div>
                    }
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    )
  }
}
const mapStateToProps = (state) => ({
  widgets: state.widgets,
});

const mapDispatchToProps = dispatch => ({
  createFunnel: values => { dispatch(createFunnel(values)) },
  editFunnel: values => { dispatch(editFunnel(values)) },
  generateWidget: values => { dispatch(generateWidget(values)) },
  fetchFunnelList: () => { dispatch(fetchFunnelList()) },
  deleteFunnelAction: id => { dispatch(deleteFunnelAction(id)) },
  getFunnelData: id => { dispatch(getFunnelData(id)) },
})

module.exports = connect(mapStateToProps, mapDispatchToProps)(WidgetContainer);