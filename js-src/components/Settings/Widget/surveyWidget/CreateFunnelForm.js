
import { connect } from 'react-redux'
import { Field, reduxForm, FieldArray } from 'redux-form'
import { renderField } from '../../../Helpers/ReduxForm/RenderField';
const required = value => (value ? undefined : 'Required');
var isEdit = false;

const renderStages = ({ fields }) => (
  <div>
    <label>Funnel Stage</label>
    {fields.map((stage, index) => (
      <div key={index} className="stage-row">
        <div class="state-input">
          <Field
            name={stage}
            type="text"
            placeholder="Type stage"
            component={renderField}
            validate={[required]}
          />
        </div>
        {
          fields.length !== 1
            ? (<span className="stage-remove-btn" onClick={() => fields.remove(index)}>
              <i class="material-icons">remove</i>
              Remove
               </span>)
            : null
        }
      </div>
    ))}
    <div>
      <button type="button" className="btn btn-default" onClick={() => fields.push()}>
        Add Another
      </button>

      <button
        className='btn btn-primary'
        type='submit'
      >
       {isEdit == false ? "Create" : "Update"} 
      </button>
    </div>
  </div>
);

class CreateFunnelForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    }
  }

  componentDidMount() {
    this.handleInitialize();
  }

  componentDidUpdate(prevProps) {
    const { currFunnelData } = this.props;
    if(currFunnelData == null)
    {
      isEdit = false
    }
    else{
      isEdit = true
    }
    if (currFunnelData !== prevProps.currFunnelData) {
      this.handleInitialize();
    }
  }

  handleInitialize() {
    const { currFunnelData } = this.props;
    const initData = {
      stages: [""],
      funnelName: '',
      feedName: '',
      funnelType: 'Customer'
    };
    if (currFunnelData && currFunnelData.identity) {
      initData.feedName = currFunnelData.feed.name;
      initData.funnelName = currFunnelData.name;
      initData.identity = currFunnelData.identity;
      const stages = [];
      currFunnelData.stages.map(stageValue => {
        stages.push(stageValue.name);
      })
      initData.stages = stages;
    }
    this.props.initialize(initData);
  }

  render() {
    return (
      <div className="create-funnel-form">
        <form id="createFunnel" onSubmit={this.props.handleSubmit}>
          <div className="form-row">
            <Field
              name="funnelName"
              id="funnelName"
              type="text"
              label="Funnel Name"
              component={renderField}
              placeholder='Type funnel...'
              validate={[required]}
            />
          </div>
         
          <div className="form-row">
            <Field
              name="funnelType"
              id="funneltype"
              label="Funnel Type"
              type="select"
              component={renderField}
              placeholder='Type funnel...'
              validate={[required]}
            >
              <option value={"Customer"} key={1}>
                Customer
              </option>
              <option value={"Job applicant"} key={2}>
                Job Applicant
              </option>
            </Field>
          </div>

          <Field
            name="identity"
            type="hidden"
            component={renderField}
          />

          <div className="form-row">
            <Field
              name="feedName"
              id="feedName"
              type="text"
              label="Create Associated Feed"
              component={renderField}
              placeholder='Type feed name...'
            />
          </div>
          <div>
            <FieldArray name='stages' component={renderStages} />
          </div>
        </form>
      </div>
    )
  }
}

const mapStateToProps = state => ({
});

const mapDispatchToProps = dispatch => ({

});
let connection = connect(mapStateToProps, mapDispatchToProps)

var reduxConnectedComponent = connection(CreateFunnelForm)

export default reduxForm({
  form: 'CreateFunnelForm', // a unique identifier for this form
})(reduxConnectedComponent)