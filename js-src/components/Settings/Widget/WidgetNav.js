import Collapse, { Panel } from 'rc-collapse'; // collapse
// collapse css
import { IndexLink, Link } from 'react-router';
import * as utils from '../../../utils/utils';

class WidgetNav extends React.Component {
    render(){
      return(
        <div className='listMenu'>
        <nav class='nav-side'>
          <ul class='parent clearfix'>
            <li>
              <Link
                to='/settings/widgets'
                activeClassName={this.props.main_page?'active':''}
              >
                <span className='text' data-toggle='tab'>
                  Wall
                </span>
              </Link>
            </li>
            <li>
              <Link
                to='/settings/widgets/map'
                activeClassName='active'
              >
                <span className='text' data-toggle='tab'>
                  Map
                </span>
              </Link>
            </li>
            <li>
              <Link
                to='/settings/widgets/survey'
                activeClassName='active'
              >
                <span className='text' data-toggle='tab'>
                  Survey
                </span>
              </Link>
            </li>

            {/* <li>
      <a className={(this.state.currentTab == 'tasks') ? ` active` : ``} onClick={ this.changeTab.bind(this, 'tasks')} >
          <span class="text">Tasks</span>
          </a></li> */}
          </ul>
        </nav>
      </div>
               )
  }
}
module.exports = WidgetNav;