import { Field, reduxForm } from 'redux-form'
import { renderField } from '../../Helpers/ReduxForm/RenderField'
import notie from 'notie/dist/notie.js'
import Globals from '../../../Globals'
import reactTooltip from '../../Chunks/ChunkReactTooltip';
//(value !=='' || value !== undefined ? undefined : 'Required')
const required = value => (value ? undefined : 'Required')
class WidgetForm extends React.Component {
  constructor () {
    super()

    this.state = {
      hideImgWrap: false,
      wallCheckbox:[],
      tooltip: null,
    }
  }
  componentWillMount() {
    reactTooltip().then(reactTooltip => {
        this.setState({
            tooltip: reactTooltip 
        })
    })
}
  componentDidMount () {
    this.handleInitialize(this.props.companyInfo)
  }

  componentWillReceiveProps (nextProps) {
    if (JSON.stringify(nextProps.companyInfo) !== JSON.stringify(this.props.companyInfo)) {
      this.handleInitialize(nextProps.companyInfo)
    }
  }
  handleInitialize(companyInfo) {
    var wallCheckboxArray = []
    var accname = ''
    var objToSend = {}
    var initData = {
      connected_social_account_ids_identity: '',
      connected_social_account_ids: '',
      socialwall_limit: '',
      socialwall_limit_identity: '',
      theme: '',
      theme_identity: ''
    }
    for (var i = 0; i < companyInfo.length; i++) {
      for (var k in companyInfo[i]) {
        if (companyInfo[i].hasOwnProperty(k)) {
          if (initData.hasOwnProperty(companyInfo[i]['key'])) {
            initData[companyInfo[i]['key']] = companyInfo[i]['values']
            initData[companyInfo[i]['key'] + '_identity'] =
              companyInfo[i]['identity']
            if (companyInfo[i]['key'] == "connected_social_account_ids") {

              for (var a = 0; a < companyInfo[i]['values'].length; a++) {
                for (var acc = 0; acc < this.props.accounts.length; acc++) {
                  if (this.props.accounts[acc].identity == companyInfo[i]['values'][a]) {

                    //to check social check box when page load 
                    accname = `chk-${this.props.accounts[acc].social_media}-${this.props.accounts[acc].identity}`
                    objToSend = {
                      [accname]: true
                    }
                    
                    Object.assign(initData, objToSend)
                    wallCheckboxArray.indexOf(this.props.accounts[acc].social_media) == -1 ?
                    wallCheckboxArray.push(this.props.accounts[acc].social_media) : ''
                  }
                  
                }
              }
            }
          }
        }
      }
    }
    this.props.initialize(initData)
    this.setState({ wallCheckbox: wallCheckboxArray })
  }

  removeProfilePic () {
    var me = this

    let objToSend = {
      avatar: '',
    }
  }
  socialCheckboxChanged(e, me, type) {
    var flag=0;
    let socialCheckBoxes = document.querySelectorAll('input.social-checkbox') 
    
      for (let ec = 0; ec < me.externalCheckboxes.length; ec++) 
      {
        if (e.target.checked) 
        {
          me.setState({
              wallCheckbox: [...this.state.wallCheckbox, e.target.name.split('-')[1]]  
            });

          if (me.externalCheckboxes[ec] == e.target.name) 
          {
              me
                .props
                .change(me.externalCheckboxes[ec], true)
          }
        }
        else 
        {
          if (me.externalCheckboxes[ec].split('-')[2] ==e.target.name.split('-')[2] || me.externalCheckboxes[ec]==e.target.name) 
          {
              var wallCheckboxArray = this.state.wallCheckbox;
              var index = wallCheckboxArray.indexOf(e.target.name.split('-')[1])
              wallCheckboxArray.splice(index, 1);
              this.setState({wallCheckbox: wallCheckboxArray });
          }
        }
    }
  }
  limitcheck(e){
      this.setState({
        limitcheck : e.target.value,
      })
  }
  themecheck(e){
      this.setState({
        themecheck : e.target.value,
      })
  }
  render () {
    this.allSocialChannels = Globals
      .EXTERNAL_SOCIAL
      .slice()
    var iscompany=0;
    this.externalCheckboxes = []
    var me=this
    const { handleSubmit, pristine, submitting } = this.props
    
    var feedlimit = []
    var hint = {
      position: 'relative',
      top: '-20px'
    }
    for (var i = 1; i <= 10; i++) {
      feedlimit.push(<option key={i} value={5 * i}>{5 * i}</option>)
    }
   var social = typeof this.props.accounts !== 'undefined'
      ? this.props.accounts
      : []
      var defaultimg="https://app.visibly.io/img/default-avatar.png"
    //==
    return (
      <form onSubmit={handleSubmit}>

        <div className='section'>
        <div className='col-one-of-two'>
          <div className='social-checkboxes'>
            <div class='channel_row'>
              <div className='social-channel'>
                <label for='social-checkbox'>Select company page</label>
              </div>
              
              <div class='social-checkbox-wrapper clearfix'>
                {social.map((social, i) => {
                  var pageavatar=''
                  iscompany = social.iscompany

                  if(social.iscompany == 1 ){
                    this
                      .externalCheckboxes
                      .push(`chk-${social.social_media.toLowerCase()}-${social.identity}`)
                    
                      var connectedSocialAccIndex = this.allSocialChannels.indexOf(social.social_media.toLowerCase())
                      if (connectedSocialAccIndex > -1) {
                          this.allSocialChannels.splice(connectedSocialAccIndex, 1)
                      }
                    }
                    //to get avatar of social accounts
                    if(social.social_media.toLowerCase()=='facebook'){
                    Object.keys(social.user_social).map((a, i) => {
                      if(social.user_social[i]['key']=='avatar'){
                        pageavatar=social.user_social[i]['value']
                      }
                    })
                  }if (social.social_media == 'twitter') {
                    if (social.iscompany == 1) {
                      Object.keys(social.user_social).map((a, i) => {
                        if(social.user_social[i]['key']=='avatar'){
                          pageavatar=social.user_social[i]['value']
                        }
                      })
                    }
                  }if (social.social_media == 'linkedin') {
                    if (social.iscompany == 1) {
                      Object.keys(social.user_social).map((a, i) => {
                        if(social.user_social[i]['key']=='avatar'){
                          pageavatar=social.user_social[i]['value']
                        }
                      })
                    }
                  }
                  return (
                    <div 
                      key={i}
                      className={`social-checkbox`}>
                      {iscompany == 1 ?
                          <div>
                              <Field
                                name={`chk-${social.social_media.toLowerCase()}-${social.identity}`}
                                checked={this.state.wallCheckbox.indexOf(social.social_media) > -1 ? true : false}
                                className={`image-checkbox social-checkbox ${social.social_media.toLowerCase()} `}
                                id={`chk-${social.identity}`}
                                onChange={e => me.socialCheckboxChanged(e, me, social.social_media.toLowerCase())}
                                component='input'
                                type='checkbox'
                                ref={'check_' + social.identity}
                                label='Channel'
                              />
                              <label for={`chk-${social.identity}`} >
                                  <img onError={(e) => { e.target.src = "https://app.visibly.io/img/default-avatar.png" }}  src={pageavatar?pageavatar:defaultimg}/>
                                  <i class={`fa fa-${social.social_media.toLowerCase()}`}></i>
                              </label>
                          </div> : ''}

                    </div>
                  )
                })}
                {/* if social account is not connected then to display checkbox as desable mode */}
                {this.allSocialChannels.map((notConnectedSocialAcc, i) => {
                  return (
                    <div>
                      <div key={i}>
                        <div
                          className='social-checkbox'
                          key={i}
                          data-tip={'Please connect your company page in settings'}
                          data-for={'notConnectedSocialChannels'}>
                          
                          {notConnectedSocialAcc == 'facebook'
                            ? <label className="notconnect">
                              <img src="https://app.visibly.io/img/default-avatar.png" />
                              <i className='fa fa-facebook' aria-hidden='true' />
                            </label>
                            : null}
                          {notConnectedSocialAcc == 'twitter'
                            ? <label className="notconnect">
                              <img src="https://app.visibly.io/img/default-avatar.png" />
                              <i className='fa fa-twitter' aria-hidden='true' />
                            </label>
                            : null}
                          {notConnectedSocialAcc == 'linkedin'
                            ? <label className="notconnect">
                              <img src="https://app.visibly.io/img/default-avatar.png" />
                              <i className='fa fa-linkedin' aria-hidden='true' />
                            </label>
                            : null}
                        </div>
                        {this.state.tooltip?
                        <this.state.tooltip type='info' id='notConnectedSocialChannels'
                          effect='solid'
                          className='tooltipsharepopup'
                        />:''}
                      </div>
                    </div>
                  )
                })}
              </div>
            </div>
          </div>
          <div className='form_row'>
            <label for='socialwall_limit'>Feed Limit</label>
            <div className='select-wrapper'>
              <Field
                placeholder='Feed Limit'
                component={renderField}
                type='select'
                name='socialwall_limit'
                onChange= {this.limitcheck.bind(this)}
                validate={[ required ]}
              >
                <option value=''>Select Limit</option>
                {feedlimit}
              </Field>
            </div>
          </div>
          </div>
          
          <div className='col-one-of-two'>
          <div className='form_row'>
            <label for='theme'>Select Theme</label>
            <div className='select-wrapper'>
              <div class='form-row  select'>
                <Field name='theme' 
                  component={renderField} 
                  type='select'
                  onChange= {this.themecheck.bind(this)}
                  validate={[ required ]}
                >
                  <option value='' >Select theme</option>
                  <option value='tiles'>Tiles</option>
                  <option value='featured'>Featured</option>
                </Field>
                </div>
              </div>
            </div>

            <div className="generate-btn right">
            <button
                  id="generate"
                  className='btn btn-primary btn-integration'
                  type='submit'
                  disabled={pristine || submitting }>
                  Generate
                </button>
            </div>
          </div>
        </div>  
      </form>
    )
  }
}

export default reduxForm({
  form: 'step2' // a unique identifier for this form
})(WidgetForm)
