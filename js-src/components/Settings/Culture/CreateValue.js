
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import PopupWrapper from '../../PopupWrapper'
import { Field, reduxForm } from 'redux-form'
import { renderField } from '../../Helpers/ReduxForm/RenderField'
import ReactDropzone from "react-dropzone";
import * as s3functions from "../../../utils/s3";
import * as s3Actions from "../../../actions/s3/s3Actions"    
import Globals from "../../../Globals";
import PreLoader from '../../PreLoader'


const required = value => (value ? undefined : 'Required')
const validValue = value => (value && value.length > 20
  ? 'value should not be greater than 20 characters.'
  : undefined)
class CreateValue extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      file: null,
      image_url : null,

    };
    this.onDrop = this.onDrop.bind(this);
    this.removeImage = this.removeImage.bind(this);

  }

  componentDidMount() {
    s3functions.loadAWSsdk()
    this.handleInitialize(this.props)
  }

  handleInitialize(nextProps = null) {
    if (typeof this.props.currValue !== 'undefined') {
      const initData = {
        identity: this.props.currValue.id,
        valueName: this.props.currValue.title,
        valueDescription: this.props.currValue.description

      }
      this.setState({
        image_url: this.props.currValue.image_url
      })
      this.props.initialize(initData)
    }
  }

  onDrop(files) {
    this.setState({
      file: files
    });
    Globals.AWS_CONFIG.albumName = localStorage.getItem("albumName");
    var s3Config = Globals.AWS_CONFIG;
    var s3Manager = new s3functions.S3Manager(s3Config);
    this.props.s3Actions.uploadingStart();
    var s3MediaURL = s3Manager.s3uploadMedia(
      files[0]
    );
    s3MediaURL
          .then(obj => {
            this.props.updateStateValue(obj.data.Location)
            // var file = obj.data;
            // var originalFileObj = obj.fileObject;
            // var index = obj.index;

          })

          
  }

  removeImage()
  {
    this.setState({
      file: null
    });
  }

  render() {
    console.log('render meth',this.props)
    var title = ''
    var dropzoneStyle = {
      borderColor: '#666',
      borderRadius: '5px',
      borderStyle: 'dashed',
      borderWidth: '1px',
      height: 'auto',
      minHeight: '120px',
      lineHeight: '100px',
      textAlign: 'center',
      padding: '10px',
      width: '100%',
      marginBottom: 20,
    }

    const previewStyle = {
      display: "inline",
      width: 200,
      height: 200,
      marginTop: 20
    };

    let { uploadProgress, uploadDone, uploadStarted } = this.props.aws;
    const { handleSubmit, pristine, submitting } = this.props
    if (typeof this.props.currValue !== 'undefined' && Object.keys(this.props.currValue).length > 0) {
      title = 'Edit Values'
    } else {
      title = 'Add Values'
    }
    return (
      <div>
        <header className='heading'>
          <h3>{title}</h3>
        </header>
        {uploadStarted ? <PreLoader/> :''}
        <PopupWrapper>
          <form onSubmit={handleSubmit} role='form'>
            <div className='section'>
              <Field
                component={renderField}
                type="hidden"
                name="identity"
              />
              <Field
                placeholder='Value Name'
                component={renderField}
                type='text'
                name='valueName'
                validate={[required, validValue]}
                label='Value'
              />
              <Field
                placeholder='Description'
                component={renderField}
                type='textarea'
                name='valueDescription'
                validate={[required]}
                label='Description'
              />
              
              <ReactDropzone
                placeholder='Image'
                label="Media"
                name="files"
                className="value-drag-drop"
                style={dropzoneStyle}
                title={uploadDone ? `Click Next to proceed` : `Drag and drop`}
                onDrop={this.onDrop}
                accept="image/png, image/jpg, image/jpeg"
                maxSize={2048576}
              >
                Drag and drop corresponding image here.
              </ReactDropzone>

              {this.state.file !=null ?
              <div className="createValueImagePreviewWrapper">

              <img 
              alt="Preview"
              key={this.state.file[0].preview }
              src={this.state.file[0].preview} />

            <div className="removeImageBtn" onClick={this.removeImage}><i class="material-icons">clear</i></div>
            </div>
             : null }

              <div className='clearfix'>
                <button
                  className='btn right btn-primary'
                  type='submit'
                  disabled={pristine || submitting || !uploadDone }
                >
                  {/* {(this.props.currDept) ? 'Update' : 'Add'} */}
                  Submit
                </button>
              </div>
            </div>
          </form>
        </PopupWrapper>
      </div>
    )
  }
}

function mapStateToProps(state, ownProps) {
  return {
    aws: state.aws,

  }
}

function mapDispatchToProps(dispatch) {
  return {
    s3Actions: bindActionCreators(s3Actions, dispatch),

  }
}
let connection = connect(mapStateToProps, mapDispatchToProps)

var reduxConnectedComponent = connection(CreateValue)

export default reduxForm({
  form: 'CreateValue' // a unique identifier for this form
})(reduxConnectedComponent)
