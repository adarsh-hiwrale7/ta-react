import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import PreLoader from '../../PreLoader'
import * as settingsActions from "../../../actions/settingsActions"
import * as cultureActions from '../../../actions/cultureActions';
import ReactCSSTransitionGroup from 'react/lib/ReactCSSTransitionGroup';
import notie from "notie/dist/notie.js"

import Header from '../../Header/Header';
import Globals from '../../../Globals';
import SettingsNav from '../SettingsNav';
import CreateValue from './CreateValue';
import ValueList from './ValueList';
import SurveySettingForm from '../Content/SurveySettingForm'
class ValueSettings extends React.Component {
  
 
  constructor(props) {
    super(props);
    this.state = {
      createValuePopup:false,
      updateValuePopup:false,
      value_image:null,
      currValue:{}
    }
  }
  componentDidMount() {
    this.props.cultureActions.fetchValuesList()
    this.props.settingsActions.fetchSurveySetting()
  }

  componentWillReceiveProps(nextProps) {
    if(this.props.culture !== nextProps.culture && nextProps.culture.closeCreateValuePopup ==true){
      this.closePopup('create')
      this.props.cultureActions.resetPopupFlag()
    }
  }
  
  updateStateValue(value)
  {
    this.setState({
      value_image:value
    })
  }

  handleCreateValueSubmit(values){
    var arr=[]
    var objToSend={
      title:values.valueName,
      description:values.valueDescription,
      value_image: this.state.value_image
    }

    arr.push(objToSend);
    if(values.identity !== undefined){
      var idToAdd={
        value_identity:values.identity
      }
      Object.assign(objToSend,idToAdd)
      this.props.cultureActions.updateValues(objToSend)
    }else{
      this.props.cultureActions.saveValues(arr)
    }
  }
  renderCreateValuePopup() {
    return (
      <ReactCSSTransitionGroup transitionName="slide" transitionAppear={true} transitionEnterTimeout={500} transitionLeaveTimeout={500} transitionAppearTimeout={1000}>
        <div className="side-popup-wrap">
          <div id="side-popup-wrapper" className="side-popup">
            <button id="close-popup" className="btn-default" onClick={this.closePopup.bind(this, 'create')}>
              <i className="material-icons">clear</i>
            </button>
            <CreateValue
              onSubmit={this.handleCreateValueSubmit.bind(this)}
              currValue={this.state.currValue}
              updateStateValue={this.updateStateValue.bind(this)}
               />
          </div>
        </div>
      </ReactCSSTransitionGroup>
    )
  }
  closePopup(popup) {
    switch (popup) {
      case "create":
        this.setState({
          createValuePopup: false,
          currValue:{}
        });
        break;
    }
    document.body.classList.remove("overlay");
  }
  handleValueCreation(){
    this.setState({
      createValuePopup:true
    })
    document.body.classList.add("overlay")
  }
  /**
  * HANDLE SUBMIT ENVENT FOR SURVEY SETTING 
  * @author Yamin
  **/
  surveySettingFormSubmit(surveyData){
      this.props.settingsActions.updateSurveySetting(surveyData)
  }
  dropdownPopup(e) {
    e.target.parentNode.parentNode.parentNode.style.zIndex = 11
  }
  valueDeleteClick(id){
this.props.cultureActions.deleteValue(id)
  }
  valueEditClick(valueData){
    this.setState({
      createValuePopup:true,
      currValue:valueData
    })
    document.body.classList.add("overlay")
  }
  render() {
    return (
      <section id="culture-page" className="settings polls-culture-wrapper">
        {this.state.createValuePopup == true ? this.renderCreateValuePopup():''}
        {this.props.settings.loading ? <PreLoader  location="cultureList-loader"/> : ''}
        {/* {this.state.updateDeptPopup
          ? this.renderUpdateDeptPopup()
          : <div></div>} */}
        <Header 
        title={'Values'} 
        nav={SettingsNav} 
        add_new_button={true} 
        location={this.props.location}
        searchInput={this.props.searchInput}
        />

        <div className='main-container'>
        { (this.props.culture.cultureLoader ? <PreLoader  location="cultureList-loader"/> : 
          <div id="content">
            <div className="page campaign-container">

              <div className="full-container">
               {/* survey setting form component to handle */}
               {/* <SurveySettingForm
                        onSubmit={this.surveySettingFormSubmit.bind(this)}
                        surveyData={this.props.settings.surveyData}
                /> */}
                <div className="membersListing">
                  <span id="add_new_button" onClick={this.handleValueCreation.bind(this)} />
                  <ValueList
                  valueEditClick={this.valueEditClick.bind(this)} 
                  valueDeleteClick={this.valueDeleteClick.bind(this)}
                  dropdownPopup={this.dropdownPopup.bind(this)}
                  culture={this.props.culture}
                  />
                </div>
              </div>
            </div>
          </div>
        )}
        </div>


      </section>
    );
  }

}

function mapStateToProps(state) {
  return { 
    profile: state.profile.profile, 
    auth: state.auth, 
    culture:state.culture,
    settings: state.settings
  }
}

function mapDispatchToProps(dispatch) {
  return {
    cultureActions:bindActionCreators(cultureActions,dispatch),
    settingsActions: bindActionCreators(settingsActions, dispatch),
  }
}

let connection = connect(mapStateToProps, mapDispatchToProps);

module.exports = connection(ValueSettings);
