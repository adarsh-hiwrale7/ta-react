import $ from '../../Chunks/ChunkJquery';
import ChunkJqueryDropdown from '../../Chunks/ChunkJqueryDropdown';

import TableSearchWrapper from '../../TableSearchWrapper';
import reactable from '../../Chunks/ChunkReactable'
import * as utils from '../../../utils/utils'
import Globals from '../../../Globals';
var search
export default class ValueList extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      departments: [],
      search: [],
      Selectors: null,
      perPage: 10,
      main_state:[],
      filter_state:[],
      reactable:null
    }
  }

  
 componentWillMount () {
  var cultureValue=this.props.culture.cultureValueList !== undefined && this.props.culture.cultureValueList.length >0 ? this.props.culture.cultureValueList :''
  if(cultureValue!==''){
      this.setState({
        main_state:cultureValue
      })
    }
      reactable().then(reactable=>{
        this.setState({reactable:reactable})
      })

    $().then(jquery => {
      var $ = jquery.$
      this.setState({
        jQuery: $
      })
      ChunkJqueryDropdown().then(jqueryDropdown => {})
    })
  }
  componentWillReceiveProps (nextProps) {
    if (nextProps.culture.cultureValueList !== undefined && nextProps.culture.cultureValueList.length > 0 ) {
      this.setState({
        cultureValueList: nextProps.culture.cultureValueList 
      })
      this.setState({main_state:nextProps.culture.cultureValueList})
      this.setState({filter_state:nextProps.culture.cultureValueList})
    }
  }
  search_text(e)
  {
    if(e.target.value!=="" && this.state.filter_state!==undefined)
    {
      var i=0;
      search=e.target.value.toLowerCase();
      var tempobj=this.state.filter_state.filter(function(temp){
        i=i+1;
        if(temp.title.toLowerCase().includes(search) || i==search || temp.description.toLowerCase().includes(search)){
          return true;
        }
      })
      this.setState({main_state:tempobj})
    }
    else{
      this.setState({main_state:this.state.filter_state})
    }
  }
  render () {
      var cultureData=''
      cultureData=this.props.culture.cultureValueList !== undefined&& this.props.culture.cultureValueList.length >0 ? this.props.culture.cultureValueList :''
    return (
      (
        <div className='table-white-container valueList-wrapper'>
        <div className='tableControllers clearfix'>
        {/* {this.props.location.pathname !== "/search/department"? */}
        <TableSearchWrapper>
        <input type="text" placeholder='Search in Values' onChange={this.search_text.bind(this)}></input>
        </TableSearchWrapper>
      </div>
      <div className={`widget ${this.state.main_state.length ==0 ?'no-value-found-wrapper':'' }`}>
        <div className='table-wrapper'>
        {this.state.reactable && this.state.main_state!==undefined && this.state.main_state.length >0 ?
        <this.state.reactable.Table
          className='table department-list-table responsive-table'
          id='department-table'
          itemsPerPage={10}
          pageButtonLimit={5}
        >
          <this.state.reactable.Thead>
            <this.state.reactable.Th column='id' className='a-center department-id-col'>ID</this.state.reactable.Th>
            <this.state.reactable.Th column='valuename'>Value</this.state.reactable.Th>
            <this.state.reactable.Th className='a-center' column='description'>Description</this.state.reactable.Th>
            <this.state.reactable.Th column='actions' className='actiondiv a-center'>Actions</this.state.reactable.Th>
          </this.state.reactable.Thead>
          {this.state.main_state.map((cd, i) => {

            return (
              <this.state.reactable.Tr key={i}>
                <this.state.reactable.Td className='fd a-center department-id-col' column='id' data-rwd-label='ID'>
                  {i + 1}
                </this.state.reactable.Td>
                <this.state.reactable.Td className='fd a-center' column='valuename' data-rwd-label='Value'>
                  {cd.title}
                </this.state.reactable.Td>
                <this.state.reactable.Td className='fd a-center' column='description' data-rwd-label='Description'>
                  {utils.limitWords(cd.description,Globals.LIMIT_WORD)} 
                </this.state.reactable.Td>
                <this.state.reactable.Td
                  className='fd department-action actiondiv a-center'
                  column='actions'
                  value={cd.title}
                >
          
                  <div className='action-button'>
                    <ul
                      className='dd-menu context-menu user_3dots'
                      onClick={(e) => {this.props.dropdownPopup(e)
                      }}
                    >
                      <li className='button-dropdown'>
                        <a className='dropdown-toggle user_drop_toggle'>
                          <i className='material-icons'>more_vert</i>
                        </a>
                        <ul className='dropdown-menu user_dropAction department-dropdown'>
                          <li>
                            <span
                              onClick={this.props.valueEditClick.bind(this, cd)}
                            >
                             Edit
                            </span>
                          </li>
                          <li>
                            <span
                              onClick={this.props.valueDeleteClick.bind(this, cd.id)}
                            >
                            Delete
                              {' '}
                            </span>
                          </li>
                        </ul>
                      </li>
                    </ul>
                  </div>
                </this.state.reactable.Td>

              </this.state.reactable.Tr>
            )
          })}
        </this.state.reactable.Table>:<div className="noValueFound">No value found</div>}

        </div>
        </div>
        </div>
      )
    )
  }
}