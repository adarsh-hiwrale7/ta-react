import Collapse, { Panel } from 'rc-collapse' // collapse
// collapse css
import * as utils from '../../utils/utils'
import ReactRouter from '../Chunks/ChunkReactRouter';
import { browserHistory } from 'react-router';

class SettingsNav extends React.Component {

  constructor (props) {
    super(props)
    this.state = {
      currentFilter: '',
      currentFilter_billing:'',
      reactRouter:null
    }
  }

  componentWillMount () {
    if (typeof this.props.location !== 'undefined') {
      var newLocation = utils
        .removeTrailingSlash(this.props.location.pathname)
        .split('/')
      var location = newLocation[2] == undefined ? 'widgets' : newLocation[2]
      var location_billing = newLocation[2] == undefined ? 'overview' : newLocation[2]

      this.setState({
        currentFilter: location,
        currentFilter_billing:location_billing
      })
    }
    ReactRouter().then(ReactRouter=>{
      this.setState({
        reactRouter:ReactRouter
      })
    })
    //to redirect back to setting page is logged in user dont have right to share on social media (disha)
    if(this.props.profile.enable_social_posting !== null){
      if(this.props.profile.enable_social_posting == 0 && newLocation[2] !== undefined && newLocation[2] == 'social' ){
        browserHistory.push(`/settings`)
      }
    }
  }
  
  componentWillReceiveProps(newProps) {
    var newLocation = utils
        .removeTrailingSlash(this.props.location.pathname)
        .split('/')
    //to redirect back to setting page is logged in user dont have right to share on social media (disha)
    if(newProps.profile !== this.props.profile && newProps.profile.enable_social_posting !== null){
      if(newProps.profile.enable_social_posting == 0 && newLocation[2] !== undefined && newLocation[2] == 'social'){
        browserHistory.push(`/settings`)
      }
    }
  
}

  setHeader (location) {
    this.setState({
      currentFilter: location
    })
  }
  render () {
    let roleName='';
    if(localStorage.getItem('roles') !== null){
      roleName = JSON.parse(localStorage.getItem('roles'))[0]['role_name'];
    }
    return (
      <nav className='nav-side'>
        <ul className='parent  clearfix'>
         {roleName === "super-admin" || roleName === "admin" ?
          <li>
          {this.state.reactRouter?
            <this.state.reactRouter.IndexLink activeClassName='active' to={`/settings/company`}>
              <span className='text'>Company</span>
            </this.state.reactRouter.IndexLink>
            :''}
          </li>
          :''}
          <li>
          {this.state.reactRouter?
            <this.state.reactRouter.IndexLink activeClassName='active' to={`/settings`}>
              <span className='text'>Account</span>
            </this.state.reactRouter.IndexLink>
            :''}
          </li>
          {/*<li>
            <this.state.reactRouter.Link activeClassName='active' to={`/settings/reset`}>
              <span className='text'>Reset Password</span>
            </this.state.reactRouter.Link>
          </li>*/}  {/*comment by disha */}
          {this.props.profile.enable_social_posting == 1?
           <li>
           {this.state.reactRouter?
            <this.state.reactRouter.Link activeClassName='active' to={`/settings/social`}>
              <span className='text'>Social Accounts</span>
            </this.state.reactRouter.Link>:''}
          </li>:''}
          {roleName === "super-admin" || roleName === "admin" ?
          <li>
            {this.state.reactRouter?
            <this.state.reactRouter.Link activeClassName='active' to={`/settings/culture`}>
              <span className='text'>Values</span>
            </this.state.reactRouter.Link>:''}
          </li>:''}
          <li>
          {this.state.reactRouter?
            <this.state.reactRouter.Link activeClassName='active' to={`/settings/content`}>
              <span className='text'>Content</span>
            </this.state.reactRouter.Link>:''}
          </li>
          {roleName === "super-admin" ?
          <li>
            {this.state.reactRouter?
            <this.state.reactRouter.Link activeClassName='active' to={`/settings/integration`}>
             <span className='text'>Integrations</span>
            </this.state.reactRouter.Link>:''}
          </li>
          :""}
          {roleName === "super-admin" || roleName === "admin" ?
          <li>
            {this.state.reactRouter?
            <this.state.reactRouter.Link
              activeClassName='active'
              to={`/settings/widgets`}
            >
              <span className='text'>Widgets</span>
            </this.state.reactRouter.Link>:''}
          </li>
          :""}
            {/* {roleName === "super-admin" || roleName === "admin" ?
          <li>
            {this.state.reactRouter?
            <this.state.reactRouter.Link activeClassName='active' to={`/settings/departments`}>
              <span className='text'>Departments</span>
            </this.state.reactRouter.Link>:''}
          </li>

          :""} */}
           {roleName === "super-admin" || roleName === "admin" ?
          <li>
            {this.state.reactRouter?
            <this.state.reactRouter.Link activeClassName='active' to={`/settings/users`}>
              <span className='text'>Users</span>
            </this.state.reactRouter.Link>:''}
          </li>
          : ""}
          {roleName === "super-admin" || roleName === "admin" ?
          <li>
            {this.state.reactRouter?
            <this.state.reactRouter.Link activeClassName='active' to={`/settings/Points`}>
              <span className='text'>Gamification</span>
            </this.state.reactRouter.Link>:''}
          </li>
          :""}

          { roleName === "super-admin"?
          <li>
          {this.state.reactRouter?
          <this.state.reactRouter.Link activeClassName='active'
               to={`/settings/billing/`}>
            <span className='text'>Billing</span>
          </this.state.reactRouter.Link>:''}
        </li>
        :"" }
        </ul>
      </nav>
    )
  }
}

module.exports = SettingsNav
