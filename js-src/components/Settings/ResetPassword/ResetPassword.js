import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import PreLoader from '../../PreLoader'
import * as settingsActions from "../../../actions/settingsActions"
import SidebarNarrow from '../../Layout/Sidebar/SidebarNarrow'

import ResetPasswordSettingsForm from './ResetPasswordSettingsForm';
import Globals from '../../../Globals';
import SettingsNav from '../SettingsNav';
import * as loginActions from "../../../actions/loginActions"
import * as utils from '../../../utils/utils'

class ResetPassword extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            departments: []
        }
    }

    componentDidMount() {
        fetch(Globals.API_ROOT_URL + `/department`,{
            headers: utils.setApiHeaders('get'),
        })
            .then(response => response.json())
            .then((json) => {
                if(json.code!==200||json.code==undefined){
                    utils.handleSessionError(json);
                }
                this.setState({
                    departments: json.data
                });

            })
    }

    handleResetPasswordSubmit(values) {
        //this.props.loginActions.savePassword(values, "callbackPath");
        this.props.loginActions.resetPassword(values);
    }
    render() {

        return (
            <section id="campaigns" className="settings" >
            <div id="navdraw" className="has-sib">

            <SidebarNarrow location={this.props.location}/>
              <div className="nd-sib">
                <header className="nd-header sib">
                  <h1>SETTINGS</h1>

                </header>
                   {/*nav header*/}
               <SettingsNav></SettingsNav>
              </div>
            </div>

             <header className="header">
                <a href="#" className="mobile-nav"><i className="material-icons">menu</i></a>

              </header>

            <div id="content">


              <div className="page campaign-container">

                <div className="esc">


                <div className="row">
                    <div className="col-one-of-two">

                        <div class="content">
                          <div className="widget">
                            <header class="heading">
                              <h3>Reset Password</h3>
                            </header>
                            <ResetPasswordSettingsForm onSubmit={ this.handleResetPasswordSubmit.bind(this)} departments={this.state.departments} > </ResetPasswordSettingsForm>
                          </div>
                        </div>
                    </div>
                </div>

                </div>
              </div>
            </div>
        </section>
        );
    }
}

function mapStateToProps(state) {
    return {
        onboarding: state.settings.onboarding,
        profile: state.profile.profile,
        auth: state.auth
    }
}

function mapDispatchToProps(dispatch) {
    return {
        settingsActions: bindActionCreators(settingsActions, dispatch),
        loginActions: bindActionCreators(loginActions, dispatch)
    }
}

let connection = connect(mapStateToProps, mapDispatchToProps);

module.exports = connection(ResetPassword);
