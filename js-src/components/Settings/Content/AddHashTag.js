import { Link } from "react-router";
import { browserHistory } from "react-router";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Field, reduxForm } from 'redux-form';
import MentionTextArea from '../../Helpers/ReduxForm/MentionTextArea';
import * as userActions from '../../../actions/userActions'
import * as tagsActions from '../../../actions/tagsActions'
import * as postsActions from "../../../actions/feed/postsActions";
var selectionstart;
const required = value => (value ? undefined : 'Required');
const validHashTag = value => (value && /^@\[\s\b(\w+)\b]\(([^)]+)\)$|(^#\b\w+\b$)/g.test(value) ?undefined: 'Must be a valid single hashtag');
class AddHashTag extends React.Component {
    constructor(props) {
      super(props)
      this.state = {
        customValidation:true,
        Tag:null
      }
    }
handleMentionFocus(e){
        if(e.target.value == ''){
          this.setState({customValidation:true})
          this.props.change('hashtag' ,'#')
      }
}

componentDidMount(){
   this.handleInitialize();
}

handleInitialize () {
        const initData = {
            hashtag : this.props.editHashTagData.title
        }
        this.props.initialize(initData)
 }
 
 render(){
        const {
            handleSubmit,
            pristine,
            submitting
          } = this.props
        
         let postTags = this.props.postTags;
         return(
      <div> 
         <form role='form' onSubmit={handleSubmit}>
            <div className='section'>
                            <div class="usersbox">
                            <label>Add Hashtag</label>
                            <div class="form-row  select user-tag-editor">
                                <Field
                                    component={MentionTextArea}
                                    tags={postTags}    
                                    type='textarea'
                                    role="textbox"
                                    name='hashtag'
                                    id='hashtag'
                                    userTags={[]}
                                    placeholder='Select tags with # tag'
                                    aria-multiline='true'
                                    validate={[ required, validHashTag ]}
                                    onFocus={this.handleMentionFocus.bind(this)}
                                    />
                            </div>
                            </div>
                           <div className='clearfix'>
                                <button
                                className='btn right btn-primary'
                                type='submit'
                                disabled={pristine || submitting}
                                >
                                Submit
                         </button>
                 </div>
            </div>
          </form>
    </div>
        )
    }
}
function mapStateToProps(state, ownProps) {
    return {
      userTags: state.tags.userTags,
      departmentTags: state.tags.postTags,
      postTags: state.tags.postTags,
    }
  }
  
function mapDispatchToProps(dispatch) {
    return {
      tagsActions: bindActionCreators(tagsActions, dispatch),
      userActions: bindActionCreators(userActions, dispatch),
      postsActions: bindActionCreators(postsActions, dispatch),
    }
  }
let connection = connect(mapStateToProps, mapDispatchToProps)
var reduxConnectedComponent = connection(AddHashTag)
  
export default reduxForm({
    form: 'AddHashTag' // a unique identifier for this form
})(reduxConnectedComponent)

