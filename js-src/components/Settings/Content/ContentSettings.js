import { Field, reduxForm } from 'redux-form'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import Header from '../../Header/Header'
import SettingsNav from '../SettingsNav'
import SurveySettingForm from './SurveySettingForm'
import notie from 'notie/dist/notie.js'
import AddHashTag from './AddHashTag'
import RssForm from '../../Feed/CuratedPages/RssPages/RssForm'
import * as settingsActions from '../../../actions/settingsActions'
import * as feedActions from '../../../actions/feed/feedActions'
import * as tagsActions from '../../../actions/tagsActions';
import PreLoader from '../../PreLoader'
import CenterPopupWrapper from '../../CenterPopupWrapper'
import PopupWrapper from '../../PopupWrapper';
import ToggleHashtagSwitch from './ToggleHashtagSwitch'

var restrictRssAPI = false
var isTopicSelected=false

class ContentSettings extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      creditText: 'Turn on Linkedin author accreditation',
      selectedTopics:[],
      sharelink:0,
      currRssData:null,
      utmData:{},
      edit_rss:false,
      openHashTagPopup:false,
      editFlag:false,
      editHashTagData : [],
      toggleHashtagSwitch:0,
    }
  }
  componentWillMount() {
    this.props.tagsActions.fetchPostTags('all');
   }
  componentDidMount() {
    var roles = JSON.parse(localStorage.getItem('roles'))
    if (restrictRssAPI !== true) {
      if (roles !== undefined && roles[0].role_name !== 'guest') {
        this.props.settingsActions.getrsslink()
        this.props.feedActions.fetchRssFeed()
        
      }
      restrictRssAPI = true
      setTimeout(() => {
        restrictRssAPI = false
      }, 5000)
    }
    if (this.props.profile.profile.identity != null) {
      this.handleInitialize(this.props.profile.profile)
    }
    if (document.getElementsByClassName('mention-input-wrapper__input')) {
      document.getElementsByClassName('mention-input-wrapper__input')[0]?
        document.getElementsByClassName('mention-input-wrapper__input')[0].setAttribute('autocomplete', 'off')
        :''
    }
    roles == 'super-admin' || roles == 'admin' ?
    this.props.settingsActions.fetchHashTagList() :''
    this.props.settingsActions.curationTopicList()
  }
  componentWillReceiveProps(nextProps) {
    if(this.props.settings.utmData!==nextProps.settings.utmData){
      this.setState({
        utmData : nextProps.settings.utmData
      })
    }
    if(this.props.settings.rsslinkDetailLoading!==nextProps.settings.rsslinkDetailLoading && nextProps.settings.rsslinkDetailLoading!==true){
      this.closeRssForm();
      this.props.settingsActions.resetUtmData();
    }
    if(this.props.settings.contactDetailLoading!==nextProps.settings.contactDetailLoading && nextProps.settings.contactDetailLoading!==true){
      this.closeRssForm();
      this.props.settingsActions.resetUtmData();
    }
    document.getElementsByClassName('CreditToggle')[2] !== undefined &&
      document.getElementsByClassName('CreditToggle')[2] !== null
      ? (document.getElementsByClassName('CreditToggle')[
        2
      ].checked = this.props.profile.profile.sharelink)
      : ''
      var sharelink=localStorage.getItem('sharelink') !== undefined ? localStorage.getItem('sharelink') : this.props.profile.profile.sharelink
    if (sharelink == 1) {
      this.setState({
        creditText: 'Turn off Linkedin author accreditation',
        sharelink:1
      })
    } else if (sharelink == 0) {
      this.setState({
        creditText: 'Turn on Linkedin author accreditation',
        sharelink:0
      })
      document.getElementsByClassName('CreditToggle').length>0?document.getElementsByClassName('CreditToggle')[0].checked :''
    }
    if (
      JSON.stringify(nextProps.profile) != JSON.stringify(this.props.profile)
    ) {
      this.handleInitialize(nextProps.profile.profile)
    }
    var me=this
    if(nextProps.settings.curationTopicList !== undefined && this.props.settings.curationTopicList !== nextProps.settings.curationTopicList){
      if(nextProps.settings.curationTopicList.length >0){
        nextProps.settings.curationTopicList.map(function (list, i) {
          if(list.is_selected ==1){
            me.state.selectedTopics.push(list.name)
          }
        })
      }
    }
    if(this.props.settings.hashTagContent !== nextProps.settings.hashTagContent && nextProps.settings.hashTagContent.isHashTagSaved == true ){
      this.closeHashtagPopup();
    }
  }
  handleInitialize(profile) {
    var usertag_name = []
    profile.user_tag !== undefined
      ? profile.user_tag.map(usertag => usertag_name.push(usertag.name))
      : ''
    const initData = {
      user_tags: usertag_name.join(' ')
    }
    this.props.initialize(initData)
  }
  handleCredit(e) {
    var result = document.getElementsByClassName('CreditToggle')[0].checked
    var shareFlag
    if (result == false) {
      shareFlag = 0
      this.setState({
        creditText: 'Turn off Linkedin author accreditation',
        sharelink:0
      })
    } else if (result == true) {
      shareFlag = 1
      this.setState({
        creditText: 'Turn on Linkedin author accreditation',
        sharelink:1

      })
    }
    this.props.settingsActions.shareCredits(shareFlag)
  }
  handleRssSubmit(values) {
    var data = {}
    if(values.utmCampaign.trim()==""&& values.utmMedium.trim()=="" && values.utmSource.trim()==""){
      var rsslink = values
      let rss = rsslink.rss
      let arrayRSS = rss.split(/[\s,]+/)
    
      for (var i = 0; i < arrayRSS.length; i++) {
        // check the URL validation
        var str = arrayRSS[i]
        var re = /^(http[s]?:\/\/){0,1}(www|visi.ly\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/
        if (!re.test(str)) {
          notie.alert('error', 'Invalid URL', 5)
          return false
        }
      }
      data["rss_url"]=arrayRSS;
      data["is_delete"]=0;
    }else{
      if(values.utmCampaign.trim()==""){
        notie.alert('error', 'Please add utm campaign value.', 5)
      }else if(values.utmMedium.trim()==""){
        notie.alert('error', 'Please add utm medium value.', 5)
      }else if(values.utmSource.trim()==""){
        notie.alert('error', 'Please add utm source value.', 5)
      }else{
        var rsslink = values
      let rss = rsslink.rss
      let arrayRSS = rss.split(/[\s,]+/)
    
      for (var i = 0; i < arrayRSS.length; i++) {
        // check the URL validation
        var str = arrayRSS[i]
        var re = /^(http[s]?:\/\/){0,1}(www|visi.ly\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/
        if (!re.test(str)) {
          notie.alert('error', 'Invalid URL', 5)
          return false
        }
      }
      let utm_link = `utm_source=${values.utmSource}&utm_medium=${values.utmMedium}&utm_campaign=${values.utmCampaign}`;
      data["rss_url"]=arrayRSS;
      data["is_delete"]=0;
      data["utm_link"]=utm_link
      }
    }
    if(values.rss_identity){
      if(values.utm_identity){
        data["utm_identity"] = values.utm_identity
        this.props.settingsActions.addRss(data,"editUtm")
      }else{
        data["rss_identity"] = values.rss_identity
        this.props.settingsActions.addRss(data,"addUtm")
      }
    }else{
      if(values.is_admin){
        data["is_admin"] = values.is_admin;
      }
      this.props.settingsActions.addRss(data)
    }
  }

  /**
   * close rss
   * @param {*} rssdata
   */
  close_rss(rssdata) {
    var removearrayRSS = rssdata.url.split(/[\s,]+/)
    var data = {
      rss_url: removearrayRSS,
      is_delete: 1
    }
    this.props.settingsActions.removeRssurl(data)
  }
  selectCurationTopic(topic){
    isTopicSelected=true
    if(this.state.selectedTopics.includes(topic.name)){
      var index=this.state.selectedTopics.indexOf(topic.name)
      this.state.selectedTopics.splice(index,1)
    }else{
      this.state.selectedTopics.push(topic.name)
    }
    this.setState({})
  }
  handleCuratedTopicSubmit(values){
    isTopicSelected=false
    var objToValue = {
      user_tags: this.state.selectedTopics
    }
    var callFrom = 'content'
      this.props.settingsActions.settingsProfilePost(objToValue,'','',null,callFrom)
  }
  handleRssForm(){
    this.setState({
      showAddRssForm : true
    })
  }
  closeRssForm(closebtnClick = null){
    this.setState({
      showAddRssForm : false,
      currRssData:null,
      edit_rss:false
    })
  }
  edit_rss(rssdata){
    this.setState({
      showAddRssForm : true,
      currRssData : rssdata,
      edit_rss:true
    })
  }
  getUtmData(identity){
    this.props.settingsActions.getUtmData(identity);
  }
  openPopupHashTagFlag(){
    this.setState({
       openHashTagPopup : true
    })
  }
  closeHashtagPopup(){
    this.setState({
      openHashTagPopup : false,
      editHashTagData:[],
      editFlag:false
   })
  }
  createTagList(str) {
    var me = this;
    var rx1 = /@\[([^\]]+)]\(([^)]+)\)/g;
    let res = str.match(rx1);
    var userTag = [];

    return new Promise(function (resolve, reject) {
      
      if (res) {
        res
          .forEach(function (tags) {
            /** Get user information */
            var tagsArray = tags.split("@[");
            var nameArray = tagsArray[1].split("]");
            var name = nameArray[0];
            var dept = nameArray[1]
              .substring(0, nameArray[1].length - 1)
              .split(":")[1]
              .split('_');
            var type = "tags";
            if (nameArray[1].substring(0, nameArray[1].length - 1).split(":")[0] !== "(tags") {
              type = dept[1] == 'company' ? 'company' :
                dept[1] == 'dept' ?
                  "department"
                  : "user";

              let tagsUserObject = {
                tagid: dept[0],
                tagName: name.replace(/ /g, ''),
                type: type
              };
              userTag.push(tagsUserObject)
            }

            /** description replace */
            if (type === "tags") {
              str = str.replace(tags, "#" + name.replace(/ /g, ''));
            } else {
              str = str.replace(tags, "@" + name.replace(/ /g, ''));
            }

          })
        me.setState({
          Tag: userTag
        })
      }
      resolve(str);
    })
  }
  handleSubmitAddHashTag(val){
   
   let me = this;
   if (typeof (val.hashtag) !== "undefined" && val.hashtag != "") {
     this
       .createTagList(val.hashtag)
       .then((tags) => {
         var tagsData = [];
         var newTags = tags.split(' ');
         newTags.map((tags,key) =>{
             tagsData.push(tags).toString();  
         })
       let me = this;
       var AllTitleOfTags=[]
       var tagsDataLength=tagsData.length
       var tagListTotalCount = this.props.settings.hashTagContent.isHashTagSaved == true ? this.props.settings.hashTagContent.hashTagList.hashtag:[];
       tagListTotalCount.forEach(element => {
         if(tagsData.indexOf(element.title) >-1){
           tagsDataLength=tagsDataLength -1
          }
        });
        var totalTagCount=tagsDataLength + tagListTotalCount.length
        if(totalTagCount <= 4 ){
          if(this.state.editFlag == true){
              var obj = {
                  title:tagsData.toString(),
                  hashtag_identity:this.state.editHashTagData.hashtag_identity
                }
              me.props.settingsActions.editHashTag(obj);
            }else{
                var obj = {
                      hashtag : tagsData
                  }
              me.props.settingsActions.AddHashTag(obj);
            }
          }else{
                notie.alert('error', 'Sorry! You cannot add more than 4 hashtags.', 5)
          }
       });
     } 
  }
  openHashTagPopup(){
    return(
        <div>
               <CenterPopupWrapper>
                        <div className = "print-page-container">
                          <div className='inner-print-page-container'>
                              <div className='print-details'>
                                <header className='heading clearfix'>
                                    <h3> CONTENT creation</h3>
                                    <button id="close-popup" class="btn-default" onClick = {this.closeHashtagPopup.bind(this)}><i class="material-icons">clear</i></button>
                                </header>
                                <PopupWrapper>
                                     <AddHashTag  
                                          onSubmit = {this.handleSubmitAddHashTag.bind(this)}
                                          editFlag = {this.state.editFlag}
                                          editHashTagData = {this.state.editHashTagData}
                                          hashTagList={this.props.settings.hashTagContent}
                                      />
                                </PopupWrapper>
                              </div>
                          </div>
                        </div>
                    </CenterPopupWrapper>
              </div>
    )
  }
  tableLoader(){
    return(
      <table class="table responsive-table">
      <thead>
          <tr class="reactable-column-header">
              <th class="reactable-th-serialno reactable-header-sortable serialno  a-center"> 
              <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' />
              </th>
              <th class="reactable-th-serialno reactable-header-sortable serialno  a-center"> 
              <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' />
              </th>
              <th class="reactable-th-serialno reactable-header-sortable serialno  a-center"> 
              <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' />
              </th>
          </tr>
      </thead>
      <tbody class="reactable-data">
        <tr class="table-body-text">
          <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
          <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
          <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
          
        </tr>
        <tr class="table-body-text">
          <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
          <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
          <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
         
        </tr>
        <tr class="table-body-text blanck-div-loader-totalpost">
          <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
          <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
          <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
         
        </tr>
       
      </tbody>
  </table>
    )
  }
  editHashTagInList(tags){
    this.setState({
      openHashTagPopup:true,
      editFlag : true,
      editHashTagData : tags
    })
  }
  deleteHashTagInList(tags){
    var obj = {
      hashtag_identity : tags.hashtag_identity
    }
    this.props.settingsActions.deleteHashTag(obj);
  }
  handlechange(val){
     var is_auto = 0;
      if(val == true){
        is_auto = 1;
      }else{
        is_auto = 0;
      }
     var obj = {
       is_auto:is_auto
     }
     this.props.settingsActions.autoHashTag(obj);
  }
  render() {
    let postTags = this.props.postTags
    let roleName = JSON.parse(localStorage.getItem('roles')) !== null
      ? JSON.parse(localStorage.getItem('roles'))[0].role_name
      : ''
      var curationTopicList=this.props.settings.curationTopicList !== undefined ?this.props.settings.curationTopicList:''
      var curationListLoader=this.props.settings.curationListLoader !== undefined ? this.props.settings.curationListLoader:''
      var selectedValueClass=''
      var savedTagCount=this.props.settings.hashTagContent.hashTagList !== undefined && this.props.settings.hashTagContent.hashTagList.hashtag !== undefined ? this.props.settings.hashTagContent.hashTagList.hashtag.length :0
      var me=this;
     
    return (
      <section className='IntegrationIndex'>
        <Header
          title='Content'
          nav={SettingsNav}
          location={this.props.location}
        />
        <div className='main-container'>
          <div id='content'>
            <div className='page'>
              <div className='content'>

                {this.state.openHashTagPopup  == true  ?  this.openHashTagPopup() : ''}
                <div className="clearfix">
                  <div className="content-left-block">
                    
                    {roleName == 'super-admin' || roleName == 'admin'
                      ?
                      <div>
                      <div className='widget'>
                        
                        <header class='heading'>
                          <h3>General</h3>
                        </header>
                        <div>
                          <div className='widget-cover'>
                            <label className='widget-header'>
                              {this.state.creditText}
                            </label>
                            <div class='switch'>
                              <input
                                id='cmn-toggle-1'
                                class='CreditToggle'
                                type='checkbox'
                                checked={this.state.sharelink}
                                onChange={this.handleCredit.bind(this)}
                              />
                              <label for='cmn-toggle-1' />
                            </div>
                          </div>
                        </div>
                      </div>
                      </div>
                      : ''}
                    <div className='widget'>
                      <div className='form-row user-tag-editor culturValue-createPost'>
                        <label>Curated Topics</label>
                  
                        <div className='valueList' id='curatedTopics'>
                          {curationListLoader == true || this.props.settings.rsslinkDetailLoading==true
                            ? <PreLoader className='Page' />
                            :
                            <ul className="cultureValue-List" >
                              {
                              curationTopicList.length > 0 ?
                                curationTopicList.map(function (list, i) {
                                    me.state.selectedTopics.includes(list.name)?
                                   selectedValueClass = "selecetdCulturValue" : selectedValueClass = ''
                                 
                                  
                                    
                                  return (
                                    <li key={i} id={list.identity}
                                      className={`culture-value ${selectedValueClass} clearfix`}>
                                      <a className="valueSelect" onClick={me.selectCurationTopic.bind(me, list)}>
                                        {list.name}
                                        <span className="ContentCount">{list.count}</span>
                                        </a>
                                    </li>
                                  )
                                }
                                ) : <div className='NovalueFound'>
                                  <li className="no-Value-chart"> No value found</li>
                                </div>
                              }
                            </ul>}

                        </div>
                        <button
                            class='btn btn-primary right '
                            type='submit'
                            disabled={!isTopicSelected}
                            onClick={this.handleCuratedTopicSubmit.bind(this)}
                          >
                            Save
                          </button>
                      </div>
                    </div>
                  </div>
                  <div className="content-right-block">
                    <div className='col-three-of-twelve topicintrest rightSidebar'>

                      <div className='rss-chanel-feed'>
                        <div className='trending-tag-container'>
                          {this.state.showAddRssForm==true?
                          <div className='widget new-block-rss'>
                            <RssForm 
                                onSubmit={this.handleRssSubmit.bind(this)} 
                                closeRssForm={this.closeRssForm.bind(this)}
                                currRssData ={this.state.currRssData}
                                utmData={this.state.utmData}
                                editRss = {this.state.edit_rss}
                                getUtmData={this.getUtmData.bind(this)}
                                roleName={roleName}
                            />
                          </div>:''}
                          <div className='widget new-block-rss rss-links-wrapper'>
                            <div className='setting-rss-body'>
                              <div className='heading'>
                                <h3>Connected RSS feed </h3>  
                                {this.state.showAddRssForm!==true?
                                <a className="btn btn-default add-new-button right" onClick={this.handleRssForm.bind(this)}>
                                  <span className='add-new-text'>New</span>
                                </a>:''}
                              </div>
                              <ul className='rss-links-list'>
                                {this.props.settings.rsslink !== undefined
                                  ? this.props.settings.rsslink.map(
                                    (rssLinks, i) => {
                                      return (
                                        <li key={i} className={rssLinks.is_valid == 0?'rss-text-feild invalid-li':'rss-text-feild'}>
                                          <a
                                            target='_blank'
                                            onClick={() => { window.open( `${rssLinks.url}`,'_blank')}}
                                            className={rssLinks.is_valid == 0?'rss-link invalid':'rss-link'}
                                          >
                                            {rssLinks.url}
                                          </a>
                                          <img className="check-img" src={rssLinks.is_valid == 0?'/img/uncheck_circle.svg':'/img/check_circle.svg'}/>
                                          {this.props.profile.profile.identity==rssLinks.user_id || roleName =="super-admin"?
                                          <a
                                            className='remove-btn edit-rss-btn'
                                            onClick={this.edit_rss.bind(
                                              this,
                                              rssLinks
                                            )}
                                          >
                                            <i class='material-icons'>
                                              edit
                                             </i>
                                          </a>:''}
                                          {this.props.profile.profile.identity==rssLinks.user_id || roleName =="super-admin"?
                                          <a
                                            className='remove-btn'
                                            onClick={this.close_rss.bind(
                                              this,
                                              rssLinks
                                            )}
                                          >
                                            <i class='material-icons'>
                                              clear
                                             </i>
                                          </a>:''}
                                         
                                        </li>
                                      )
                                    }
                                  )
                                  : ''}
                              </ul>
                            </div>
                          </div>
                        </div>

                      </div>
                    </div>
                    {roleName == 'super-admin' || roleName == 'admin' ?
                    <div className='widget new-block-rss rss-links-wrapper rss-chanel-feed'>
                         <div className='setting-rss-body'>
                              <div className='heading'>
                                <h3>SAVED HASHTAGS</h3>  
                                {savedTagCount < 4  ?
                                    <a className="btn btn-default add-new-button right">
                                      <span className='add-new-text' onClick = {this.openPopupHashTagFlag.bind(this)}>New</span>
                                    </a>
                                  :''}
                              </div>
                              
                                <ToggleHashtagSwitch 
                                  toggleHashtagSwitch = {this.props.settings.hashTagContent} 
                                  propsData = {this.props.settings}
                                  handlechange = {this.handlechange.bind(this)}
                                  />
                                  
                               <div className = "save-hashtag-body-wraper"> 
                                    {this.props.settings.hashTagContent.loading == true ? 
                                    this.tableLoader() : 
                                    <table className="table responsive-table">
                                    <thead>
                                      <tr>
                                        <th></th> 
                                        <th>HASHTAG</th>
                                        <th className="view-detail-col actiondiv">Actions</th>
                                       
                                      </tr>
                                     </thead>
                                     <tbody className='reactable-data'>
                                      {this.props.settings.hashTagContent.hashTagList !== [] ?
                                        this.props.settings.hashTagContent.hashTagList.hashtag != undefined ? 
                                           this.props.settings.hashTagContent.hashTagList.hashtag.map((tags,key) =>{
                                                  return(
                                                       <tr key = {key}>
                                                      
                                                            <td>{key+1}</td>
                                                            <td>{tags.title}</td>
                                                            <td className="actiondiv">
                                                            <div className='action-button'>
                                                                    <ul className='dd-menu context-menu user_3dots'>
                                                                      <li className='button-dropdown'>
                                                                        <a className='dropdown-toggle user_drop_toggle'>
                                                                          <i className='material-icons'>more_vert</i>
                                                                        </a>
                                                                        <ul className='dropdown-menu user_dropAction'>
                                                                          <li onClick ={this.editHashTagInList.bind(this , tags)}> 
                                                                            <a target="_blank">
                                                                            <span className='btn'>
                                                                                Edit
                                                                            </span>
                                                                            </a>
                                                                          </li>
                                                                          <li onClick = {this.deleteHashTagInList.bind(this , tags)}>
                                                                            <a target="_blank">
                                                                              <span className='btn'>
                                                                                  delete
                                                                              </span>
                                                                            </a>
                                                                          </li>

                                                                        </ul>
                                                                      </li>
                                                                    </ul>
                                                                  </div>
                                                            </td>
                                                    </tr>
                                                      
                                                  )
                                           })
                                         
                                        :''
                                      : "no data available"}
                                     </tbody>
                                    </table>
                                    }
                              </div>
                            </div>
                      </div>
                      :''} 
                   </div>
                </div>

              </div>
            </div>
          </div>
        </div>
      </section>
    )
  }
}

function mapStateToProps(state) {
  return {
    postTags: state.tags.postTags,
    feed: state.feed,
    profile: state.profile,
    settings: state.settings
  }
}
function mapDispatchToProps(dispatch) {
  return {
    tagsActions: bindActionCreators(tagsActions, dispatch),
    feedActions: bindActionCreators(feedActions, dispatch),
    settingsActions: bindActionCreators(settingsActions, dispatch),
  }
}
let connection = connect(mapStateToProps, mapDispatchToProps)
var reduxConnectedComponent = connection(ContentSettings)

export default reduxForm({
  form: 'ContentSettings' // a unique identifier for this form
})(reduxConnectedComponent)
