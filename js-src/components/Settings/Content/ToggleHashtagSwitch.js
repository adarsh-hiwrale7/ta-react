
import { connect } from 'react-redux'
import { reduxForm } from 'redux-form'
class ToggleHashtagSwitch extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      togglecheckbox: 0,
      toggleHashTagValue: false
    }
  }
  componentWillReceiveProps (newProps) {
    if (newProps.propsData.hashTagContent.hashTagList != []) {
      this.setState(
        {
          togglecheckbox: newProps.propsData.hashTagContent.hashTagList.is_auto
        },
        () => {
          var checked = false
          if (this.state.togglecheckbox == 1) {
            checked = true
          } else {
            checked = false
          }
          this.setState({
            toggleHashTag: checked,
            toggleHashTagValue: checked
          })
        }
      )
    }
  }
  handleChangeToggle (e) {
    this.setState(
      {
        toggleHashTag: e.target.checked,
        toggleHashTagValue: e.target.checked
      },
      () => {
        this.props.handlechange(this.state.toggleHashTagValue)
      }
    )
  }
  render () {
    return (
      <div>
        <form role='form'>
          <div class='widget-cover'>
            <label class='widget-header'>Turn on saved hashtags</label>
            <div class='switch'>
              <input
                type='checkbox'
                id='toggleHashTag'
                name='toggleHashTag'
                class='CreditToggle toggleHashTag'
                checked={this.state.toggleHashTagValue}
                onChange={this.handleChangeToggle.bind(this)}
              />
              <label for='toggleHashTag' />
            </div>
          </div>
        </form>
      </div>
    )
  }
}
function mapStateToProps (state, ownProps) {
  return {}
}

function mapDispatchToProps (dispatch) {
  return {}
}
let connection = connect(
  mapStateToProps,
  mapDispatchToProps
)
var reduxConnectedComponent = connection(ToggleHashtagSwitch)

export default reduxForm({
  form: 'ToggleHashtagSwitch' // a unique identifier for this form
})(reduxConnectedComponent)
