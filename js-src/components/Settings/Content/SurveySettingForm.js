import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Field, reduxForm } from 'redux-form';
import {renderField} from '../../Helpers/ReduxForm/RenderField'
import moment from '../../Chunks/ChunkMoment'
const required = value => value
  ? undefined
  : 'Required';
//import LoginForm from './SettingsProfile';
export class SurveySettingForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            moment: null
        }
    }
     componentWillMount() {
        moment().then(moment => {
            this.setState({moment: moment})
        })
    }

    componentDidMount() {
        this.handleInitialize(this.props.surveyData);
    }
    componentWillReceiveProps(nextProps) {
       if(nextProps.surveyData !== this.props.surveyData && typeof nextProps.surveyData !== "undefined" && Object.keys(nextProps.surveyData).length > 0){
          this.handleInitialize(nextProps.surveyData)      
       }
    }
    handleInitialize(data) {
        
        const initData = {
          survey_interval: data.interval,
          survey_email: data.email_survey
        }
        this.props.initialize(initData)
    }
render(){
    const {
        handleSubmit,
        pristine
      } = this.props
     let startDate = ''
     let endDate = ''
     if(this.state.moment != null && typeof this.state.moment.tz !== 'undefined'){
        startDate = this.state.moment.unix(this.props.surveyData.start_date).format('DD-MM-YYYY')
        endDate = this.state.moment.unix(this.props.surveyData.end_date).format('DD-MM-YYYY')
     }
        return (

                     <div className='widget'>
                        <header class='heading'>
                          <h3>Poll settings</h3>
                        </header>
                        <form onSubmit={handleSubmit}>
                        {/* Survey Interval */}
                        <div>
                            <div className='widget-cover'>
                              <label className='widget-header'>
                                Poll Interval
                              </label>
                              <div class='switch'>
                                <Field
                                  component={renderField}
                                  type='select'
                                  name='survey_interval'
                                  validate={[required]}
                                >
                                  <option value='3'>3 Month</option>
                                  <option value='6'>6 Month</option>
                                  <option value='9'>9 Month</option>
                                  <option value='12'>12 Month</option>
                                </Field>
                              </div>
                            </div>
                            Your current poll interval is from : {startDate} to : {endDate}
                        </div>
                        <button
                            class='btn btn-primary right '
                            type='submit'>
                          Save
                        </button>
                       </form> 

                     </div>   
                    
                  
        )
    }
}
function mapStateToProps (state, ownProps) {
    return {}
  }

  function mapDispatchToProps (dispatch) {
    return {
             
    }
  }
let connection = connect(mapStateToProps, mapDispatchToProps)

var reduxConnectedComponent = connection(SurveySettingForm)

export default reduxForm({
    form: 'SurveySettingForm',  // a unique identifier for this form
  })(SurveySettingForm)