import * as utils from '../utils/utils'
import $ from './Chunks/ChunkJquery'

var str_length;
export default class SeeMore extends React.Component {
    constructor (props) {
        super(props)
        this.state = {
            flag:false,
            newpostid:null
        }
        
    }
    componentWillReceiveProps(){
        
        if(this.state.newpostid!==this.props.postid){
            //if newpost id which is set in expandtext method , is different to props.postid
            //remove all style 
            if($(`#${this.props.postid} .seeMoreWrapper`)){
                Array.from(document.querySelectorAll(`#${this.props.postid} .seeMoreWrapper`))
                .forEach(el => el.removeAttribute('style'));
                Array.from(document.querySelectorAll(`#${this.props.postid} .seeMoreLinkWrapper`))
                .forEach(el => el.removeAttribute('style'));
            }
            this.setState({
                flag:false
            })
        }
    }
    /**
     * to check post detail length
     */
    checkPostTextLength(){
        if(this.state.flag==false ){
            //count post details text length
            str_length=utils.convertUnicode(this.props.detail).length;
            if(str_length>600 || this.props.detail.split("\n").length>5){
                //check string length and count number of enter key pressed
                this.trimText(str_length);
            }
        }
    }
    /**
     * to trim text
     */
    trimText(length)   {  
        if(length>600 || this.props.detail.split("\n").length>5){
            let seeMoreMaxHeight = "99px";
            if(this.props.className=='moderation'){
                seeMoreMaxHeight = "40px";
            }else if(this.props.className =='campaign')
            {
                seeMoreMaxHeight = "72px";
            }

            if(document.querySelector(`#${this.props.postid} .seeMoreWrapper`)) {
                document.querySelector(`#${this.props.postid} .seeMoreWrapper`).style.maxHeight=seeMoreMaxHeight;
                document.querySelector(`#${this.props.postid} .seeMoreWrapper`).style.overflow="hidden"
                document.querySelector(`#${this.props.postid} .seeMoreLinkWrapper`).style.display="block"
            }
        }
   }
   /**
    * call when click on see more button to expand text
    * @param {*} e 
    */
   expandText(e){
       this.setState({
           flag:true,
           newpostid:this.props.postid
       })
       
       if(document.querySelector(`#${this.props.postid} .seeMoreWrapper`)){
           document.querySelector(`#${this.props.postid} .seeMoreWrapper`).style.maxHeight="none"
           document.querySelector(`#${this.props.postid} .seeMoreLinkWrapper`).style.display="none"
       }
   }
    render() {
        this.checkPostTextLength()
        return (
            <div className="seeMoreSection" id={this.props.postid}>
                <div className={`seeMoreWrapper`} ref="seeMoreWrapper">
                    {this.props.children}
                </div>
                <div className={`seeMoreLinkWrapper`} ref="seeMoreLinkWrapper">
                    <a class='seeMoreLink' onClick={this.expandText.bind(this)}>
                        see more 
                        <i class="fa fa-angle-double-right"></i>
                    </a>
                </div>
            </div>
        )
    }
}