const React = require('react')
import { connect } from 'react-redux'
import notie from 'notie/dist/notie.js'
// import graphAPI from '../Chunks/ChunkFbReactSDK';

class Facebook extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      likes: 0,
      comments: 0,
      fbflag:false,
      PostImageList:[]
      // graphAPI: null
    }
  }
  componentWillMount () {
    window.fbAsyncInit = function () {
      FB.init({
        appId: '105978646744031',
        // appId:'280656325686422',
        cookie: true, // enable cookies to allow the server to access
        // the session
        xfbml: true, // parse social plugins on this page
        version: 'v2.1' // use version 2.1
      })
      // FB.getLoginStatus(
      //   function (response) {
      //     console.log(response,'=-=-')
      //   //  this.handleClick()
      //     this.statusChangeCallback(response)
      //   }.bind(this)
      // )
    }.bind(this);
    (function (d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0]
      if (d.getElementById(id)) return
      js = d.createElement(s)
      js.id = id
      js.src = 'https://connect.facebook.net/en_US/sdk.js'
      fjs.parentNode.insertBefore(js, fjs)
    })(document, 'script', 'facebook-jssdk')
  }
  testAPI () {
    var me = this
    FB.api('/me', function (response) {
      // console.log('Successful login for: ' + response.name);
      // document.getElementById('status').innerHTML ='Thanks for logging in, ' + response.name + '!';
    })

    FB.api('/1104161959668283_1206806542737157/likes', function (response) {
      if (response && !response.error) {
        // me.props.likes(response.data.length);
      } else {
        // console.log('error fetching likes');
        // console.log(response.error);
      }
    })
    FB.api('/1104161959668283_1206806542737157/comments', function (response) {
      if (response && !response.error) {
        // me.props.comments(response.data.length);
      } else {
        // console.log(response.error);
      }
    })
    FB.api('/1104161959668283_1206806542737157/sharedposts', function (
      response
    ) {
      // console.log(response);
      if (response && !response.error) {
        // me.props.shares(response.data.length);
      } else {
        // console.log('error fetching Shares');
        // console.log(response.error);
      }
    })
  }

  statusChangeCallback (response) {
    if (response.status === 'connected') {
      // this.testAPI()
    } else if (response.status === 'not_authorized') {
      // document.getElementById('status').innerHTML = 'Please log ' +'into this app.';
    } else {
      // document.getElementById('status').innerHTML = 'Please log ' +'into Facebook.';
    }
  }
  shareOnFB () {
    // FB.api('/me/feed', 'post', {
    //   message: 'Hello, world!'
    // });

    // FB.ui({
    //   method: 'share',
    //   href: 'https://developers.facebook.com/docs/', // Link to share
    // }, function(response){
    //   document.getElementById('status').innerHTML ='Item shared!';
    // });
  }

  checkLoginState () {
    FB.getLoginStatus(
      function (response) {
        // this.statusChangeCallback(response);
        if (response.status === 'connected') {
          this.shareOnFB()
        }
      }.bind(this)
    )
  }
  handleShareVideo(){
    var id=''
    let link = this.props.link ? this.props.link : ''
    let pageid=this.props.pageid? this.props.pageid:''
    let userid=this.props.userid? this.props.userid:''
    let access_token=this.props.accessToken? this.props.accessToken:''
    var props=this.props
    if(userid!==''){
      props.sharePostClick?props.sharePostClick(false):'';
      notie.alert('error', 'Sorry, You cannot share video on personal user account', 3)
      return
      // id=userid
    }else{
      id=pageid
    }
    FB.api(`/${id}/videos`,
    "POST",    
    {
      access_token:access_token,
      file_url: link
    },
    function (response) {
      if (!response || response.error) {
        notie.alert('error', response.error.message, 3)
      } else {
        var objToSend={
          id:response.id,
          socialAccId:id
        }
        props.sharePostClick?props.sharePostClick(false):'';
        props.successId(objToSend)
        notie.alert('success', 'Posted on Facebook successfully!', 3)
      }
    })
  }
  sharePostOnPage(pageid,access_token,message){
    var props=this.props
    var objToSend ={
      message:message
    }
    var attached_media=[];
    this.state.PostImageList.map((imageID,index)=>{
      attached_media.push(
        {media_fbid:imageID}
      )
    })
    Object.assign(objToSend,{"attached_media":attached_media})
    FB.api(
      `/${pageid}/feed/?access_token=${access_token}`,
      'POST',
      objToSend,
      function(response) {
        if(response.post_supports_client_mutation_id==true){
          var objToSend={
            id:response.id,
            socialAccId:pageid
          }
          props.successId(objToSend)
          props.sharePostClick?props.sharePostClick(false):'';
          notie.alert("success","post shared successfully",3);
        }else{
          props.sharePostClick?props.sharePostClick(false):'';
          notie.alert("error","We are facing problem while sharing post on facebook, please try again later",3);
        } 
      }
    );
  }

  handleSharePageClick(){
    var assetDetail = this.props.assetData
    var me = this;
    let pageid=this.props.pageid? this.props.pageid:''
    let link = this.props.link ? this.props.link : ''
    let access_token=this.props.accessToken? this.props.accessToken:''
    let message = this.props.message ? this.props.message : ''
    var props=this.props
    var ImageList = [];
    assetDetail !== undefined && assetDetail.data?
        ImageList = assetDetail.data.map((image,index)=>{
            return image.type=="local"? image.media_url:image.asset.data.media_url;
        })
    :''

    var fileUrl=null;
    if(assetDetail !== undefined && assetDetail.data.length==1 ){
      if(assetDetail.data[0].type=="local"){
        fileUrl =   assetDetail.data[0].media_type.includes("application")?  assetDetail.data[0].media_url:null
      }else{
        fileUrl= assetDetail.data[0].asset.data.media_type.includes("application")?assetDetail.data[0].asset.data.media_url:null
      }
    } 
    //when share is from curation or job
    if(fileUrl == null){
      fileUrl=link
    }
    if(fileUrl!==null){
      FB.api(
            `/${pageid}/feed`,
            "POST",
            {
              access_token:access_token,
              message: message,
              // picture: link,
              link: fileUrl
            },
            function (response) {
              if (!response || response.error) {       
                notie.alert('error', response.error.message, 3)
                props.sharePostClick?props.sharePostClick(false):'';
              } else {
                var objToSend={
                  id:response.id,
                  socialAccId:pageid
                }
                props.successId(objToSend)
                props.sharePostClick?props.sharePostClick(false):'';
                notie.alert('success', 'Posted on Facebook successfully!', 3)
              }
            }
        );
    }else{
    ImageList.map((ImageData,index)=>{
      FB.api(
        `/${pageid}/photos?access_token=${access_token}`,
        'POST',
        {"url":ImageData,
        "published":"false"},
        function(response) {
          if(response.id!==null){
            me.state.PostImageList.push(response.id);
            if(me.state.PostImageList.length==ImageList.length){
              me.sharePostOnPage(pageid,access_token,message);
            }
          }
          else{
            props.sharePostClick?props.sharePostClick(false):'';
            notie.alert("error","We are facing problem while sharing post on facebook, please try again later",3);
            return;
          } 
      }
      );
    })
    }
  }
  handleClick() {
    var dataToAdd,
      objToAdd = {}
    if (typeof FB !== 'undefined') {
      this.props.sharePostClick?this.props.sharePostClick(true):'';
      var props = this.props
      if (this.props.pageid && this.props.videourl == 0) {
        this.handleSharePageClick()
      } else if (this.props.videourl == 1) {
        this.handleShareVideo()
      } else {
        let linkToShare = this.props.linkToShare ? this.props.linkToShare : ''
        let userid = this.props.userid ? this.props.userid : ''
        let picture = this.props.picture ? this.props.picture : ''
        let thumbnail_url = this.props.thumbnail_url ? this.props.thumbnail_url : ''
        let message = this.props.message ? this.props.message : ''
        // let access_token=this.props.accessToken? this.props.accessToken:''
        // let shareFrom=this.props.shareFrom? this.props.shareFrom:''

        if (message !== '' && linkToShare !== '') {
          //to share link with message share popup is used bcz feed popup is not support quotes
          objToAdd = {
            method: 'share',
            display: 'popup',
            quote: message,
            href: linkToShare,
          }
        } else {
          // to share only link or image feed is used
          objToAdd = {
            method: 'feed',
            display: 'popup',
          }
          if (linkToShare !== '') {
            dataToAdd = {
              link: linkToShare,
            }
            Object.assign(objToAdd, dataToAdd)
          } else if (picture !== '') {
            dataToAdd = {
              picture: picture,
              // link:`http://ec2-18-130-120-163.eu-west-2.compute.amazonaws.com/image.php?thumbnail_url=${thumbnail_url}`,
            }
            Object.assign(objToAdd, dataToAdd)
          } else {
            //to share only text bcz share or feed both dont allow to share only text
            dataToAdd = {
              method: 'share_open_graph',
              action_type: 'og.shares',
              action_properties: JSON.stringify({
                object: {
                  title: message
                }
              })
            }
            Object.assign(objToAdd, dataToAdd)
          }
        }
        FB.ui(objToAdd, function (response) {
          var me =this
          if (response !== undefined && response.length == 0) {
            // var objToSend={
            //   id:response.id,
            //   socialAccId:userid
            // }
            // props.successId(objToSend)
            props.sharePostClick?props.sharePostClick(false):'';
            notie.alert('success', 'Posted on Facebook successfully!', 3)
          } else if (response !== undefined && response.error_message !== undefined) {
            props.sharePostClick?props.sharePostClick(false):'';
            notie.alert('error', 'Problem in sharing post on Facebook!', 3)
          }else{
            var me =this
            props.sharePostClick?props.sharePostClick(false):'';
          }
        });
      }
    }
  }

  render () {
    return (
      <div>
        {this.props.name!==undefined?
        <span className={`share-on-social ${this.props.isFbHasLink !== undefined && this.props.isFbHasLink == false?'disabled':''} `}>
          <a
            onClick={this.handleClick.bind(this)}
            className={`fb ${this.props.name.toLowerCase()}`}
          >
          {
            this.props.image_url ?
            <img src={this.props.image_url}  onError={(e) => { e.target.src = "https://app.visibly.io/img/default-avatar.png" }}/>
            :''
            }
            {' '}<i className='fa fa-facebook' />
          </a>

        </span>
           :this.handleClick(this)}  
      </div>
    )
  }
}
export default Facebook