const React = require('react');
import Globals from '../../Globals';
import notie from "notie/dist/notie.js"
import * as utils from '../../utils/utils';
class Linkedin extends React.Component {
    constructor(props) {
        super(props);
        linked_data: false
        shareLink:''
    }
    /**
     * @author disha
     * to share post on user account or page
     */
    handleClick() {
        // let newMessage = message.replace(/ /g, '%20') //decodeURI(message) //message.replace(" ", "%20");
        // //console.log(this.props)
        // let thumbnail_url=(this.props.thumbnail_url)?this.props.thumbnail_url:'';
        // let payload;

        // let videoPageUrl = `http://ec2-35-154-191-76.ap-south-1.compute.amazonaws.com/video/video.php?description=${newMessage}&video_url=${link}&thumbnail_url=${thumbnail_url}`
        // let imagePageUrl = `http://ec2-35-154-191-76.ap-south-1.compute.amazonaws.com/video/image.php?description=${newMessage}&thumbnail_url=${link}`
         
        let payload;
        let socialId = ''
        let link = (this.props.link !== '') ? this.props.link : '';
        let socialuserId = (this.props.socialUserId !== '') ? this.props.socialUserId : ''
        let pageid = this.props.pageid ? this.props.pageid : '';
        let thumbnail_url = (this.props.thumbnail_url) ? this.props.thumbnail_url : '';
        let videourl = (this.props.videourl) ? this.props.videourl : '';
        let iscompany = (this.props.iscompany !== null) ? this.props.iscompany : null;
        let message = (this.props.message) ? this.props.message : '';
        if(this.props.shareLink !== '' && this.props.shareLink !== undefined){
            var links='';
            if (this.props.shareLink.linkedin_user_link !== undefined){
                links =  this.props.shareLink.linkedin_user_link;
            }
            else if(this.props.shareLink.linkedin_page_link !== undefined){
                links =  this.props.shareLink.linkedin_page_link;
            }
            else if(this.props.shareLink.twitter_user_link !== undefined){
                links =  this.props.shareLink.twitter_user_link;
            }
            else if(this.props.shareLink.twitter_page_link !== undefined){
                links = this.props.shareLink.twitter_page_link;
            }
            else if(this.props.shareLink.facebook_user_link !== undefined){
                links =  this.props.shareLink.facebook_user_link;
            }
            else if(this.props.shareLink.facebook_page_link !== undefined){
                links =  this.props.shareLink.facebook_page_link;
            }
            message =  message  +"\n" + "Original Author : " + links +" " ;
        }
        if (iscompany == 0) {
            socialId = socialuserId
        } else {
            socialId = pageid
        }
        var medias =[]
        if(this.props.assetData){
            for(var i = 0; i<this.props.assetData.data.length; i++)
            {
                if(this.props.assetData.data[i].type == "asset"){
                    var image={
                        'media_url':this.props.assetData.data[i].asset.data.media_url,
                        'thumbnail_url':this.props.assetData.data[i].asset.data.thumbnail_url, 
                    }
                }else{
                    var image={
                        'media_url':this.props.assetData.data[i].media_url,
                        'thumbnail_url':this.props.assetData.data[i].thumbnail_url, 
                    }  
                }
                medias.push(image) 
            }
        }
       var desc = null;
      if(message.length>0)
        {
          desc = {
            'description_text': message,
          }
        }
        if (link !== '' && typeof link !== 'undefined') { 
            payload = {
                'social_id': socialId,
                'share_on_company': iscompany,
                'media': medias,
                'resource': 'post',
                'local_post_id': this.props.postId
            }

            Object.assign(payload,desc)
          
        }else if(videourl !== '' && typeof videourl !== 'undefined'){
            payload = {
                'social_id': socialId,
                'share_on_company': iscompany,
                'description_text': message,
                'media':medias,
                'resource': 'post',
                'local_post_id': this.props.postId
            }
            Object.assign(payload,desc)

        }else{
             var resourceRef = 'post';
             var localPostId = this.props.postId 
             if(this.props.shareFrom=="jobs"){
                resourceRef = 'job'
             }else if(this.props.shareFrom=="rss"){
                resourceRef = 'rss'
                localPostId = 0
             }
             payload = {
                'social_id': socialId,
                'share_on_company': iscompany,
                'resource': resourceRef,
                'local_post_id': localPostId
            }  
            Object.assign(payload,desc)
       }
        this.props.sharePostClick?this.props.sharePostClick(true):'';
        fetch(Globals.API_ROOT_URL + `/linkedin/share`, {
            method: 'POST',
            headers: utils.setApiHeaders('post'),
            body: JSON.stringify(payload)
        })
            .then(response => response.json())
            .then(json => {
                if (json.code == '200') {
                    notie.alert('success', 'Posted on Linkedin successfully!', 3)
                    this.props.sharePostClick?this.props.sharePostClick(false):'';
                } else {
                    utils.handleSessionError(json)
                    this.props.sharePostClick?this.props.sharePostClick(false):'';
                    notie.alert('warning', json.message, 3)
                }
            })
            .catch(err => { throw err; });
    }

    render() {
        return (
            <div>
                {this.props.name !== undefined ?
                    <span className={`share-on-social ${this.props.isLiHasLink !== undefined && this.props.isLiHasLink == false?'disabled':''} `}>
                        <a onClick={this.handleClick.bind(this)}
                            className={`ln ${this.props.name.toLowerCase()}`} >
                            {
                                this.props.image_url ?
                                    <img src={this.props.image_url}  onError={(e) => { e.target.src = "https://app.visibly.io/img/default-avatar.png" }}/>
                                    : ''
                            }
                            {' '}<i className="fa fa-linkedin"></i>
                        </a>
                    </span>
                    : this.handleClick()}
            </div>
        );

    }
}
export default Linkedin;
