const React = require('react');
import Globals from '../../Globals';
import notie from "notie/dist/notie.js"
import * as utils from '../../utils/utils';

class Twitter extends React.Component {
    constructor(props){
        super(props);
    }

    componentDidMount() {
    }
    success() {
       let link=(this.props.link!=='')?this.props.link:'';
       let pageid=(this.props.pageid!==null)?this.props.pageid:null;
       let postid=(this.props.postId!=='')?this.props.postId:0;
       let iscompany=(this.props.iscompany!==null)?this.props.iscompany:null;
        let message=(this.props.message)?this.props.message:'';
        let size=(this.props.size!==null)?this.props.size:null;
        let media_type=(this.props.media_type!=='')?this.props.media_type:'';
        let url=(this.props.url!=='')?this.props.url:'';
        let resourceType=(this.props.resourceType!=='')?this.props.resourceType:'';
        var trimedText=utils.truncateTwitterText(message)
        var duration=this.props.media_duration !== null ? this.props.media_duration  :0
        var convertSizeInMb=size / (1024 * 1024).toFixed(2)

        let mediaLimit =0;
        let imageError =false;
        if(this.props.videourl==1){
          if(convertSizeInMb > 15 || duration > 131){
            notie.alert('error', 'The video to be uploaded should be under 2 minutes 20 seconds with size limit of less than 15 megabytes.', 5)
            return
          }
        }
        else{
          this.props.assetData ?
            this.props.assetData.data.map((asset,idex)=>{
              var mediaSize =5;
              var convertSizeInMb = asset.media_size / (1024 * 1024).toFixed(2)
              if(asset.media_type==='image/gif'){
                mediaSize = 15;
              }
              if(convertSizeInMb>mediaSize){
                mediaLimit = mediaSize
                imageError=true;
              }
            })
          :''
        }
        if(imageError==true){
          notie.alert('error', `You can not share media size greater than ${mediaLimit} MB`, 3)
          return
        }
        var Images =[];
        if(this.props.videourl==1){
            Images[0]= {
              image:link,
              url:url,
              media_size:size,
              media_type:media_type
            }
        }else{
          this.props.assetData?
          this.props.assetData.data.map((asset,idex)=>{
            var media_url='';
            var media_size='';
            var media_type='';
            if(asset.type == 'local' ){
              media_url=asset.media_url
              media_size=asset.media_size
              media_type=asset.media_type
              
            }else if(asset.type == "asset"){
              media_url=asset.asset.data.media_url
              media_size=asset.asset.data.media_size
              media_type=asset.asset.data.media_type

            }
            var image={
              image:media_url,
              url:media_url,
              media_size:media_size,
              media_type:media_type
            }
            Images = [...Images,image]
          })
         :'' 
        }
        var LinkFromRssJobs='';
        if(resourceType == 'rss' || resourceType == 'jobs'){
          LinkFromRssJobs= message
        }
        if(Images.length>4 ){
          notie.confirm(
            `You can only upload 4 image on Twitter. If you continue, the first four image will be uploaded on Twitter.`,
            'Yes',
            'No',
            function () {
              Images = Images.filter((image,index)=>{
                return index < 4
              })
              var arr = {
                'status': trimedText !== ''?trimedText:undefined,
                "images":Images.length>0?Images:null,
                // 'url':url,
                'is_company':iscompany,
                'pageid':pageid,
                // 'media_size':size,
                // 'media_type':media_type,
                "local_post_id":postid,
                "resource":resourceType
            }
            this.props.sharePostClick?this.props.sharePostClick(true):'';
            fetch(Globals.API_ROOT_URL + `/twitter`, {
            method: 'POST',
            headers: utils.setApiHeaders('post'),
            body: JSON.stringify(arr)
           // mode: 'no-cors'
          })
          .then(response => {
            return response.json();
          })
          .then(json=>{
            if(json.code=='200'){
              this.props.sharePostClick?this.props.sharePostClick(false):'';
              notie.alert('success', 'Posted on Twitter successfully!', 3)
            }else{
              utils.handleSessionError(json);
              this.props.sharePostClick?this.props.sharePostClick(false):'';
              notie.alert('warning', json.message, 3)
            }
          })
          .catch(err => { throw err; });

            },function(){

            })
        }else{
        var arr = {
              'status': trimedText !== ''?trimedText:undefined,
              "images":Images.length>0?Images:[{}],
              // 'url':url,
              'is_company':iscompany,
              'pageid':pageid,
              // 'media_size':size,
              // 'media_type':media_type,
              "local_post_id":postid,
              "resource":resourceType
          }
          this.props.sharePostClick?this.props.sharePostClick(true):'';
          fetch(Globals.API_ROOT_URL + `/twitter`, {
          method: 'POST',
          headers: utils.setApiHeaders('post'),
          body: JSON.stringify(arr)
         // mode: 'no-cors'
        })
        .then(response => {
          return response.json();
        })
        .then(json=>{
          if(json.code=='200'){
            this.props.sharePostClick?this.props.sharePostClick(false):'';
            notie.alert('success', 'Posted on Twitter successfully!', 3)
          }else{
            this.props.sharePostClick?this.props.sharePostClick(false):'';
            utils.handleSessionError(json);
            if(json.message){
              notie.alert('warning', json.message, 3)
            }else{
              notie.alert('warning','Problem in uploading tweet on twitter' , 3)
            }
          }
        })
        .catch(err => { throw err; });
      }
    }
    shareContent() {
      var me = this;
      if(this.props.message !== null && this.props.message !==''&& this.props.message.length > Globals.TWITTER_LIMIT_WORD) {
        notie.confirm(
          `Twitter allows ${Globals.TWITTER_LIMIT_WORD} characters max. Your message may be cut. Do you want to share? `,
          "Yes",
          "No",
          function () {me.success()},
          function () {
          })
      } else {
        me.success();
      }


    }
    onSuccess(data) {
    }
    onError(error) {
    }
    callbackFunction() {
        IN.API.Profile("me").result(function(r) {
        });
    }

    handleClick() {
        this.shareContent();
    }

    render() {
         return (
           <div>
          {this.props.name!==undefined?
            <span className={`share-on-social ${this.props.isTwHasLink !== undefined && this.props.isTwHasLink == false?'disabled':''} `}>
              <a onClick = { this.handleClick.bind(this) }
              className = {`tw ${this.props.name.toLowerCase()}`} > 
              {
                this.props.image_url!==null ?
                <img src={this.props.image_url}  onError={(e) => { e.target.src = "https://app.visibly.io/img/default-avatar.png" }}/>
                :''
              }
              <i className="fa fa-twitter"></i> </a>
          </span>
          :this.handleClick(this)}
          </div>
        );
    }
}
export default Twitter;
