import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { Field, reduxForm } from 'redux-form'

import * as utils from '../../utils/utils'

import { renderField } from '../Helpers/ReduxForm/RenderField'
import { renderTagsField } from '../Helpers/ReduxForm/RenderField'
import MentionTextArea from '../Helpers/ReduxForm/MentionTextArea'
import * as userActions from '../../actions/userActions'
import * as tagsActions from '../../actions/tagsActions'
import ImageLoader from 'react-imageloader'
import PreLoaderMaterial from '../PreLoaderMaterial'
const required = value => (value ? undefined : 'Required')
const validEmail = value =>
  (value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
    ? 'Invalid email address'
    : undefined)

class CreateAssetDetailForm extends React.Component {
  constructor (props) {
    super(props)
  }
  componentWillReceiveProps()
  {
  }
  componentWillMount () {
  }
  openEditorClick (fileData, indexCount) {
    this.props.openEditor(fileData, indexCount)
  }

  renderThumbInForm (me) {
    var file = typeof this.props.isFromEditor !== 'undefined'
      ? this.props.fileUploaded[0]
      : this.props.fileUploaded
    var filePath = ''
    if (typeof file.preview === 'undefined') {
      filePath = utils.blobToUrl(file)
      filePath = filePath.src
    } else {
      filePath = file.preview
    }
    if (file.type.substring(0, 5) == 'image') {
      // any css changes for this code, then it also  reflects on uploaded assetform.
      return (
        <div>
          <div className="preview-wrapper" >

            <ImageLoader
                src={filePath}
                preloader={PreLoaderMaterial}
                className = "thumbnail">
              <div className="image-load-failed">
                  <i class="material-icons no-img-found-icon">&#xE001;</i>
              </div>
            </ImageLoader>

            {/* <img src={filePath} /> */}
            {this.props.DriveFile==undefined && file.type!=="image/gif"?
              <div className='edit-post-wrapper'>
                <span
                  class='edit-post'
                  onClick={me.openEditorClick.bind(
                    me,
                    file,
                    this.props.indexCount
                  )}
                >
                  <i class='material-icons'></i>
                </span>
              </div>
              :''
            }
            
          </div>
        </div>
      )
    } else if (file.type.substring(0, 5) == 'video') {
      return (
        file.isFileSupported==true?
        <div className='preview-wrapper'>
          <div className='video-wrapper' id={`dropPreviewIconInner-${file.preview}`}>
            <div className='video'>
              <i className='material-icons asset-title-icon'>videocam</i>
            </div>
          </div>
        </div>:''
      )
    } else {
      return (
        // this design for doc, video and doc desing are same so video class used in this desing
        (
          <div className='preview-wrapper'>
            <div className='video-wrapper'>
              <div className='video'>
                <i class='material-icons asset-title-icon'></i>
              </div>
            </div>
          </div>
        )
      )
    }
    // if (filePath == '') {
    //   return (
    //     <div class='doc'>
    //       <div class='file-icon file-icon-lg' data-type='' />
    //     </div>
    //   )
    // } else {
    //   return <img src={filePath} width='70' height='70' alt='' />
    // }
  }

  renderForms (me) {
    const { pristine, submitting, tags } = this.props

    var fileInfo = this.props.fileUploaded

    let userTags = this.props.userTags
    let postTags = this.props.postTags
    return (
      <form onSubmit={this.handleSubmit.bind(this)}>

        <div className={`section clearfix`}>
          {this.props.isFileSupported == false
            ? this.nodata(fileInfo)
            :
            <div>
              <div className='left col-20 create-assest-thumb-warpper'>
                <div className={`thumb ${this.props.DriveFile!==undefined ? `DriveAssetLoader` :''}`}>
                  {this.renderThumbInForm(me)}
                </div>
              </div>
              <div className='col-80 right active create-assest-inputs-wrapper'>

                <Field
                  component={renderField}
                  type='hidden'
                  name='fileData'
                  />
                <Field component={renderField} type='hidden' name='catID' />
                <Field component={renderField} type='hidden' name='campID' />
                <Field
                  component={renderField}
                  type='hidden'
                  name='folderID'
                  />
                <Field
                  component={renderField}
                  type='hidden'
                  name='numberOfUploaded'
                  />
                <Field
                  component={renderField}
                  type='hidden'
                  name='filesize'
                  />

                <Field
                  placeholder='Title'
                  label={null}
                  component={renderField}
                  type='text'
                  name='title'
                  validate={[required]}
                  />

                <div className='form-row user-tag-editor'>
                  {/* <label>Tags</label> */}
                  <Field
                    name='description'
                    component={MentionTextArea}
                    placeholder='Add description, #tags or @mention'
                    type='textarea'
                    ref='description'
                      /* component={renderField} */
                    userTags={this.props.userTags}
                      // validatedata={[required]}
                    tags={this.props.postTags}
                    />
                </div>
              </div>
            </div>}

        </div>
      </form>
    )
  }

  renderTick () {
    return (
      <div class='finishedUploadingAsset center'>
        <svg
          className='checkmark active'
          xmlns='http://www.w3.org/2000/svg'
          viewBox='0 0 52 52'
        >
          <circle
            className='checkmark__circle'
            cx='26'
            cy='26'
            r='25'
            fill='none'
          />
          <path
            className='checkmark__check'
            fill='none'
            d='M14.1 27.2l7.1 7.2 16.7-16.8'
          />
        </svg>
        <h4 className='center m-t-one'>
          Your asset was uploaded successfully and sent for moderation!
        </h4>
      </div>
    )
  }

  handleSubmit (values) {
    this.props.handleFormSubmit(values)
  }

  nodata (fileInfo) {
    return (
      <p>
        The file
        <b>{` ${fileInfo.name}` }</b>
         {` you are uploading may not be a valid file. `}
        <a target="_blank" href='https://support.visibly.io/getting-started/quick-guide-to-visibly#3-assets'> See recommended file types </a>
        for uploads.
      </p>
    )
  }

  render () {
    var me = this
    return (
      <div class = "asset-container-form-data">
        {me.props.isFromEditor
          ? <div className='section'>
            {this.renderForms(me)}
          </div>
          : <div className='section'>
            {this.renderForms(me)}
          </div>}
      </div>
    )
  }
}

function mapStateToProps (state) {
  return {
    assets: state.assets,
    userTags: state.tags.userTags,
    postTags: state.tags.postTags,
    departmentTags: state.tags.postTags
  }
}

function mapDispatchToProps (dispatch) {
  return {
    tagsActions: bindActionCreators(tagsActions, dispatch),
    userActions: bindActionCreators(userActions, dispatch)
  }
}
let connection = connect(mapStateToProps, mapDispatchToProps)

var reduxConnectedComponent = connection(CreateAssetDetailForm)

export default reduxForm({
  form: `CreateAssetDetailForm` // a unique identifier for this form
})(reduxConnectedComponent)
