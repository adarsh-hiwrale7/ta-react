import * as utils from '../../utils/utils'
import Asset from './Asset'
import Globals from '../../Globals'
import { IndexLink, Link } from 'react-router'
import ReactDOM from 'react-dom'
import PreLoader from '../PreLoader'
import * as s3functions from '../../utils/s3'
import * as s3Actions from '../../actions/s3/s3Actions'
import * as assetsActions from '../../actions/assets/assetsActions'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
// import PhotoEditorSDK from '../Chunks/ChunkPhotoEditor'

class PhotoEditor extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
        loadedImage : null,
        loading: true,
        mediaName: null,
        mediaUrl: null,
        mediaExtension: null,
        // photoEditor: null,
        renderOneTimeEditor: false
    }
  }
  initialiseImages(editorState){
    if(editorState !== null && this.state.renderOneTimeEditor !== true){
    var me = this;
    this.editorElement  !== undefined  ? 
      this.editorElement.ui.on('export', (result) => {
        var  noPostObj= {
          mimetype: this.props.mediaType
        }
        Object.assign(result,noPostObj)
        this.props.assetsActions.hideEditor(); 
        var imageArray = {};
        var preview = utils.blobToUrl(result);
        imageArray['name'] = this.props.imageName;
        imageArray['preview'] = preview.src; 
        imageArray['fileData'] = result; 
        imageArray['type'] = this.props.mediaType;
        imageArray['size'] = this.props.size;
        this.props.assetsActions.editAssetInBuffer(imageArray,this.props.indexCount);
        this.props.closeEditorPopup(false);
        //jQuery('#close-editor-popup-id').trigger('click');
        //this.saveEditedImage(result,this.props.asset.title,this.props.asset.media_extension).bind(this);
      })
    :'';
    const container = document.querySelector('#asset-editor-wrapper')

    const image = new Image()
    image.crossOrigin = "Anonymous";
    var url = this.props.imageData?this.props.imageData.preview ? this.props.imageData.preview: this.props.imageData:'';
    url = url.replace(/^https:\/\//i, 'http://');
    image.src = url;
    image.onload = () => {
    ReactDOM.render(<PhotoEditor 
                          image={image} 
                          imageName = {this.props.imageName}
                          mediaType = {this.props.mediaType}
                          assetsActions = {this.props.assetsActions}
                          indexCount = {this.props.indexCount}
                          closeEditorPopup = {this.props.closeEditorPopup}
                          photoEditor={this.props.photoEditor}
                      />, container)
       }
      }
  }
componentDidMount() {
    s3functions.loadAWSsdk()
    // PhotoEditorSDK()
    //     .then(photoEditor => {
    //           this.setState({ photoEditor: photoEditor},()=>{
              setTimeout(()=>{
                this.initialiseImages(this.props.photoEditor)
              },3000)  
        //     })
        // })
        // .catch(err => {
        //   console.log(err)
        // })
  }
 closeEditorPopup()
 {
  jQuery('#close-editor-popup').trigger('click');

 }
render () {
  if(this.props.photoEditor != null && typeof this.props.photoEditor.UI!== "undefined"){
    const { ReactComponent } = this.props.photoEditor.UI.DesktopUI;
          var me = this;  
          return (
               <section className='create-asset-page'>
                    <div ref="checkhere" className='checkhere-wrap'>
                    <ReactComponent
                      ref={c => this.editorElement = c} 
                      license='{"owner":"Visibly Ltd","version":"2.1","enterprise_license":false,"available_actions":["magic","filter","transform","sticker","text","adjustments","brush","focus","frames","camera"],"features":["adjustment","filter","focus","overlay","transform","text","sticker","frame","brush","camera","library","export"],"platform":"HTML5","app_identifiers":["app.visibly.io","localhost","staging.visibly.io","preview.visibly.io"],"api_token":"jbv-DGwI-x5k-bteHoT3Zw","domains":["https://api.photoeditorsdk.com"],"issued_at":1520491729,"expires_at":null,"signature":"ZSBbyh7/oFRGMyBT4P1CnM+O1fSiJZNaTlYjM+Dg775NwmohUwRF1nPzyffJaHk1MSCOtrIP4Eri4npPkmmRkMGeduoTzq0Jsc3jdJsVsfsZtIVxNNo1n9asZFRSqNE80ft+bAWLMDriJd9n5a+P1/51rO5DspXdJt6mabCcAevlE6VqxPD7wGbKfZgnP0EJY4OidAn33REJyICkOTjbJrumB5m1lTpvcE4z4qEyKkUeQfDLJOSHQyWzb/lIbmD9H8xkiTwf9+cLXNP32bW4wvyoG8NYOrX2IxvV92X8JmKz8u9F8FSQ0bD7Rwg1G/2X2VwAv+VqXRzun5WYcr9qBk/cniSzTrOdG3PvE3FqdVZhjkQ4YXyu2CYWqi/lqiNNobyXN/ldoj4K6ywkAQk+9RN/Mxbiz3g5eI6U5+q66t8BXha2iq775oWscIq/rhVcVb8INcoZRk6rHIVVX2AU8i9saine6b+7f7Dx6fgY6MAZgT+0K8pPuKiSk/PaucRj/4B6Gu3dYGuKJh6UHP7z3ZuvZtASu5BN4jOmw0Ohq50IbtypGPlQ9dXair+NQjN/Sd4pj9cZQEieV1h/kWpmepvOGntEXCm9M+mOKHZ3Q6Y6NsXsY9oYjfzlcisq+wWyalqhI6S7N2C2xV0QveV2JPUvyF9gm1NmDCvyAejdP3Q="}'
                      assets={{
                        baseUrl: '/assets'
                      }}
                      editor={{
                        image: this.props.image,
                        export:{
                          type: 'blob'
                        },
                        //this is used for custom crop ratio for different social accounts like facebook, twitter, linkedin.
                        controlsOptions: {
                          transform: {
                            categories: [
                              {
                                identifier: 'twitter',
                                defaultName: 'Twitter',
                                ratios: [
                                  {
                                    identifier: 'tw_profile', // A unique identifier for this ratio
                                    defaultName: 'Profile Pic', // The default translation for this ratio
                                    ratio: 1 / 1, // The image ratio (a floating point number)
                                    //dimensions: new PhotoEditorSDK.Math.Vector2(50, 40) // Optional fixed
                                  },
                                  {
                                    identifier: 'tw_cover', // A unique identifier for this ratio
                                    defaultName: 'Cover', // The default translation for this ratio
                                    ratio: 3 / 1, // The image ratio (a floating point number)
                                    //dimensions: new PhotoEditorSDK.Math.Vector2(50, 40) // Optional fixed
                                  },
                                  {
                                    identifier: 'tw_feed', // A unique identifier for this ratio
                                    defaultName: 'Post', // The default translation for this ratio
                                    ratio: 2 / 1, // The image ratio (a floating point number)
                                    //dimensions: new PhotoEditorSDK.Math.Vector2(50, 40) // Optional fixed
                                  }
                                ]
                              },
                              {
                                identifier: 'LinkedIn',
                                defaultName: 'LinkedIn',
                                ratios: [
                                  {
                                    identifier: 'li_profile', // A unique identifier for this ratio
                                    defaultName: 'Profile Pic', // The default translation for this ratio
                                    ratio: 1 / 1, // The image ratio (a floating point number)
                                    //dimensions: new PhotoEditorSDK.Math.Vector2(50, 40) // Optional fixed
                                  },
                                  {
                                    identifier: 'li_cover', // A unique identifier for this ratio
                                    defaultName: 'Cover', // The default translation for this ratio
                                    ratio: 4 / 1, // The image ratio (a floating point number)
                                    //dimensions: new PhotoEditorSDK.Math.Vector2(50, 40) // Optional fixed
                                  },
                                  {
                                    identifier: 'li_feed', // A unique identifier for this ratio
                                    defaultName: 'Post', // The default translation for this ratio
                                    ratio: 13 / 8, // The image ratio (a floating point number)
                                    //dimensions: new PhotoEditorSDK.Math.Vector2(50, 40) // Optional fixed
                                  },
                                  {
                                    identifier: 'li_company_banner', // A unique identifier for this ratio
                                    defaultName: 'Banner', // The default translation for this ratio
                                    ratio: 2 / 1, // The image ratio (a floating point number)
                                    //dimensions: new PhotoEditorSDK.Math.Vector2(50, 40) // Optional fixed
                                  }
                                ]
                              }


                            ],
                            replaceCategories: false
                          }
                        }
                        
                        
                      }}
                      style={{
                        width: 1024,
                        height: 576
                      }}
                    /> 
                    </div>
                    </section>

          )  
      /* }else{
          console.log('create new button called');
          this.renderCreateNewButton()
       }*/
      }else{
        return(<div></div>)
      }
    }
  }

function mapStateToProps(state) {
  return {
     aws: state.aws,
     assets: state.assets
  }
}

function mapDispatchToProps(dispatch) {
  return {
     s3Actions: bindActionCreators(s3Actions, dispatch),
     assetsActions: bindActionCreators(assetsActions, dispatch)
  }
}

let connection = connect(mapStateToProps, mapDispatchToProps);

module.exports = connection(PhotoEditor)
