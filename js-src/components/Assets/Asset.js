import ImageLoader from 'react-imageloader'
import PreLoaderMaterial from '../PreLoaderMaterial'

import * as utils from '../../utils/utils'
import AssetsRowList from './AssetsRowList'
import Script from 'react-load-script';
//import AssetDetail from './AssetDetail'

class Asset extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
       show_editer:false,
    }
  }
  editPostInCratePost(){
    this.props.editOnCratePostasset()
  }
  assetClick (asset) {
    this.props.assetClickFromRowListing(asset)
  }

  editbuttonpopup(){
    var objForEditor = [];
    objForEditor.push({
     name:this.props.title+"."+ this.props.extension,
     preview:this.props.media_url,
     size:this.props.media_size,
     type:this.props.media_type
   })
   objForEditor['files'] = objForEditor;
   this.props.showeditor(objForEditor);
  }

  componentDidMount () {
    utils.pauseVideo(".play-btn-overlay video");
    // this.props.savetoDrive();
 }
//  ScriptLoaded(){
//   console.log("script is been loaded")
//   gapi.savetodrive.go();
// }

renderTitle(type) {
    var title = "";
    if (type == 'image' || type == 'application' || type=='video') {
      title = this.props.name + '.' + this.props.extension;
    }
    else {
      title = this.props.name  ? this.props.name  : type;
    }
    return (

      <span className='title'>{title} </span>

    )
  }
renderDocAsset(faIcon, extension, type,assetStyle) {
    var docClass = "";
    var docIcon = "";
    if (extension.includes("ppt") ||extension.includes("pptx") || extension.includes("presentation") || extension.includes("powerpoint")){
      docClass = "pptx";
      docIcon = "fa fa-file-powerpoint-o fa-3x"
    }
    else if (extension.includes("doc") || extension.includes("docx") || extension.includes("txt") || extension.includes("document") ||extension.includes("mswordtype")){
      docClass = "docx";
      docIcon = "fa fa-file-text-o fa-3x"
    } else if (extension.includes("pdf")) {
      docClass = "pdf";
      docIcon = "fa fa-file-pdf-o fa-3x"
    }
    else if (extension.includes("xls") || extension.includes("xlsx") || extension.includes("spreadsheet") || extension.includes("excel")){
      docClass = "xls";
      docIcon = "fa fa-file-excel-o fa-3x"
    }
    else {
      docIcon = "fa fa-file-o fa-3x"
    }

    return (
        <div>
          <div className='inner_imgwrapper assetInnerWrapper'>
            <a  onClick={this.props.onClick} onDoubleClick={this.props.onDoubleClick} class="type-image">
              <div class={"thumbWrap asset-file-doc  file file-doc thumbnail " + docClass}>
                <i class={docIcon}></i>
              </div>
            </a>
            <div class="heading-edit-post">
                {/* download button  */}
                {/* if any change in this code then reflect in feed selected assets */}
                 <a  href={this.props.media_url} className = "download_Assets" download>
                    <i class="material-icons">&#xE2C4;</i>
                </a>

                {/* <div className="g-savetodrive"
                  data-src={this.props.media_url}
                  data-filename={this.props.name}
                  data-sitename="Visibly">
                </div> */}

              </div>
            {/* <a onClick={this.props.onClick} className='file-details'>
            {!this.props.detailView ? this.renderTitle(type) : <span />}
          </a> */}

         </div>


        <a onClick={this.props.onClick} className='file-details'>
          <i class="material-icons asset-title-icon">insert_drive_file</i>
          {this.renderTitle(type)}
        </a>
      </div>

    )
  }
  imageLoaded(){
    if(document.querySelectorAll('.asset-detail-popup .asset-content-wrapper').length>0){
    let assetWidth = document.querySelectorAll('.asset-detail-popup .asset-content-wrapper')[0].offsetWidth // Get asset width
    document.querySelectorAll('.asset-detail-popup .author-tag-wrapper')[0].style.width = `${assetWidth}px`;
    }
    if(document.querySelectorAll('#analytics .side-popup img').length>0){
      let assetWidth = document.querySelectorAll('#analytics .side-popup img')[1].offsetWidth // Get asset width
      if(assetWidth<320)
      {
        document.querySelectorAll('#analytics .asset-content-wrapper')[0].classList.add('column-wrapper')
      }
      else{
        document.querySelectorAll('#analytics .asset-content-wrapper')[0].classList.remove('column-wrapper')
      }
      }

  }

  // savetoDrive(){
  //   console.log("this.props in assets -----",this.props)
  //   this.props.savetoDrive();
  // }
  divClick()
  {
    // console.log("div clikced")
  } 

  onImageLoadError()
  {
  //  var data = document.getElementsByClassName('type-image');
  //  data.onClick='';
  }

  render() {
    const selected = this.props.selected;
    var index = this.props.media_type.indexOf('/')
    var type = 'application'
    var assets = this.props.assets
    if (index !== -1) {
      type = this.props.media_type.substr(0, index)
    } else {
      type = this.props.media_type
    }
   // var assetCreated = this.props.assets.creation.created;
    var media_url =  this.props.media_url
    var thumbnail_url = this.props.thumbnail_url
    let extension = this.props.extension
    var media_type= this.props.media_type
   // var result = this.checkImageExists(thumbnail_url);
   //  this.props.preview
    var assetStyle = {
      backgroundImage: `url(${typeof thumbnail_url !== 'undefined' ? thumbnail_url : media_url})`
    }
   var img_thumnail = '';
   if(thumbnail_url != 'undefined')  {
     img_thumnail = thumbnail_url ;
   }
   else{
    img_thumnail = media_url;
   }
    let faIcon = utils.getFaIcon(extension)
    switch (type) {
      case 'image': {
        return (
          <div>
        <div id="container">
          <div id={`asset-${this.props.identity}${selected ? '-selected' : ''}`} className='vid file-image asset-file imgwrapper'>
            <div className='inner_imgwrapper assetInnerWrapper'>
             {/* <i class="material-icons img-load-img" >&#xE410;</i>      */}



          <div class="heading-edit-post">
                  {media_type !== 'image/gif' ?
                    <span>
                      <span className="edit-post" onClick = {this.editbuttonpopup.bind(this)}><i class="material-icons">&#xE254;</i></span>
                      <span className="edit-post-asset-details sd" onClick = {this.editPostInCratePost.bind(this)}><i class="material-icons">&#xE254;</i></span>
                    </span>
                  : '' }
                  {/* download button  */}
                  {/* if any change in this code then reflect in feed selected assets */}
                  <a  href={this.props.media_url} className = "download_Assets" download>
                    <i class="material-icons">&#xE2C4;</i>
                  </a>


                  {/* <div class="g-savetodrive"
                    // data-src="http://localhost:3030/img/tejaltest.jpg"
                    data-src={this.props.media_url}
                    data-filename={this.props.name}
                    data-sitename="Visibly">
                  </div>
                  <Script
                    url="https://apis.google.com/js/platform.js"
                    onLoad={this.ScriptLoaded.bind(this)}
                    parsetags= "explicit"
                  /> */}
                </div>

              <a className='type-image' 
                onClick={this.props.onClick}
                onDoubleClick={this.props.onDoubleClick}>
                {/* this edit-post not disply in detail-view */}
                {this.props.detailView ?
                <ImageLoader
                    src={media_url}
                    preloader={PreLoaderMaterial}
                    onError= {this.onImageLoadError}
                    onLoad = {this.imageLoaded} >
                  <div className="image-load-failed">
                   <i class="material-icons no-img-found-icon">&#xE001;</i>
                   </div>
                 </ImageLoader>
                     :
                  <ImageLoader
                    src={this.props.preview ? this.props.preview : img_thumnail}
                    preloader={PreLoaderMaterial}
                    onError= {(e)=>{document.getElementById(`asset-${this.props.identity}`).classList.add('disable')}}
                    className = "thumbnail">
                   <div className="image-load-failed">
                      <i class="material-icons no-img-found-icon">&#xE001;</i>
                   </div>
                 </ImageLoader>

                  // <div className='thumbnail' style={assetStyle}  />

                  }
                  {media_type == 'image/gif' ?
                       <i className="material-icons  asset-video-icon">gif</i>
                       : <span />} 
              </a>
              <div class="heading-edit-post">
                {media_type !== 'image/gif' ?
                <span>
                  <span className="edit-post" onClick = {this.editbuttonpopup.bind(this)}><i class="material-icons">&#xE254;</i></span>
                  <span className="edit-post-asset-details sd" onClick = {this.editPostInCratePost.bind(this)}><i class="material-icons">&#xE254;</i></span>
                </span>:''
                }
                  {/* download button  */}
                  {/* if any change in this code then reflect in feed selected assets */}
                  <a  href={this.props.media_url} className = "download_Assets" download>
                    <i class="material-icons">&#xE2C4;</i>
                  </a>


                  {/* <div class="g-savetodrive"
                    // data-src="http://localhost:3030/img/tejaltest.jpg"
                    data-src={this.props.media_url}
                    data-filename="testing tejal1"
                    data-sitename="Visibly">
                  </div>
                  <Script
                    url="https://apis.google.com/js/platform.js"
                    onLoad={this.ScriptLoaded.bind(this)}
                    parsetags= "explicit"
                  /> */}


                </div>
              {/* <a href="#" className="share"><i className="fa fa-share-alt" aria-hidden="true"></i></a> */}
            </div>

            <a onClick={this.props.onClick} className='file-details'>
               <i class="material-icons asset-title-icon">image</i>
                {!this.props.detailView ? this.renderTitle(type) : <span />}
            </a>

          </div>
          </div>
          </div>
        )
      }

      case 'video':
        var blDetail = !!this.props.detailView
        {
          return (
            <div>
            <div id={`asset-${this.props.identity}`} className={`vid asset-file imgwrapper ${!blDetail ? 'file-video' : ''}`}>
              <div className="inner_imgwrapper assetInnerWrapper videoimg-wrapper">
                <a
                  onClick={this.props.onClick}
                  onDoubleClick={this.props.onDoubleClick}
                  className='play-btn-overlay type-video'
                >
                  <div className="thumbnail">
                      {blDetail?
                        <video
                          id="thumb"
                          poster={img_thumnail}
                          autoPlay
                          controls
                        >
                          <source
                            src={this.props.media_url}
                            type='video/mp4'
                          />
                          <source
                            src={this.props.media_url}
                            type='video/webm'
                          />
                          <source
                            src={this.props.media_url}
                            type='video/ogg'
                          />
                        </video>
                         :
                        <ImageLoader
                            src={img_thumnail}
                            preloader={PreLoaderMaterial}
                            className = "thumbnail">
                          <div className="image-load-failed">
                              <i class="material-icons no-img-found-icon">&#xE001;</i>
                          </div>
                        </ImageLoader> 
                      }
                    {!this.props.detailView ?
                       <i className="material-icons asset-video-icon">play_circle_filled</i>
                       : <span />} 
                  </div>
              </a>
              <div class="heading-edit-post">
                  {/* download button  */}
                  {/* if any change in this code then reflect in feed selected assets */}
                  <a  href={this.props.media_url} className = "download_Assets" download>
                    <i class="material-icons">&#xE2C4;</i>
                  </a>

                  {/* <div class="g-savetodrive"
                    data-src={this.props.media_url}
                    data-filename={this.props.name}
                    data-sitename="Visibly">
                  </div> */}

               </div>

              </div>
              {/* <a className="share"><i className="fa fa-share-alt" aria-hidden="true"></i></a> */}

              <a onClick={this.props.onClick} className='file-details'>
                <i class="material-icons asset-title-icon">videocam</i>
                  {!this.props.detailView ? this.renderTitle(type) : <span />}
              </a>

            </div>
            </div>

          )
        }

      case 'audio':
        return (
          <div>
          <div id={`asset-${this.props.identity}`} className='vid asset-file imgwrapper'>
          <div className='assetInnerWrapper'>
            {this.renderDocAsset('fa-file-audio-o', extension, type)}
            </div>
          </div>
          </div>
        )

      case 'application':
        return (
          <div>
          <div id={`asset-${this.props.identity}`} className='vid asset-file imgwrapper'>
            <div className='assetInnerWrapper'>
            {this.renderDocAsset(faIcon, extension, type, assetStyle)}
            </div>
          </div>
          </div>
        )

      default:
        return (
          <div>
          <div id={`asset-${this.props.identity}`} className='vid asset-file imgwrapper'>
            <div className='assetInnerWrapper'>
            {this.renderDocAsset(faIcon, extension, type, assetStyle)}
            </div>
          </div>
          </div>
        )
    }
  }
}



module.exports = Asset
