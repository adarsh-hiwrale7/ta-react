import Globals from '../../Globals'
import { IndexLink, Link } from 'react-router'
import ReactDOM from 'react-dom'
import * as utils from '../../utils/utils'
const Line = require('rc-progress').Line;
var src = 'http://via.placeholder.com/350x150';
class EditAssetUploadingSteps extends React.Component {
    renderPreview(){
                    var file = this.props.uploadedFiles;    
                    var src = '';
                    if(typeof file.fileData.preview === "undefined"){
                            src = utils.blobToUrl(file.fileData); 
                            src = src.src; 
                    }else{
                            src = file.fileData.preview;
                    }
                    if(file.fileData.type.substring(0, 5)=="image") {
                      return (
                            <img src={src} />
                      )
                    }
                    else if(file.fileData.type.substring(0, 5)=="video") {
                    //any css changes for this code,then reflect is uploaded asset.
                      return (
                        //   for video
                        this.props.sendVideoThumbnailImage.filePreview==file.fileData.preview?
                        <div className='preview-wrapper'>
                        <div className = "video-wrapper" id={`dropPreviewIconInner-${file.fileData.preview}`}>
                        <img src={this.props.sendVideoThumbnailImage.thumbnailImageSrc}/>
                          <div className="video">
                            <i className="material-icons asset-title-icon">videocam</i>
                          </div>
                         </div>
                        </div>
                        :''
                      )
                    }
                    else {
                      return (
                        // for doc,
                        <div className='preview-wrapper'>
                        <div className = "video-wrapper">
                          <div className="video">
                          <i class="material-icons asset-title-icon">&#xE24D;</i>
                          </div>
                         </div>
                        </div>

                      )
                    }
    }
    render() {
    	var strokeColor = "#256eff";
        var trailColor = "#ebeef0"; 
        var uploadFiles = this.props.uploadedFiles;
        if(this.props.uploadingProgress > 0) {
                strokeColor = "#256eff";
                trailColor = "#ebeef0";
        }
        return (
           <div class="edit-asset-preview-wrapper">
        	<ul class="dropzone-preview edit-asset-preview">
                        <li className='item-0' rel='0' >
                         {this.renderPreview()}
                         <div className="uploaded-file-details">
                            <div className="filename" title="">{uploadFiles.title !== null ? uploadFiles.title : ''}</div>
                            <Line percent={this.props.uploadingProgress > 0 ? this.props.uploadingProgress : 0} strokeWidth="4" strokeColor={strokeColor} trailWidth="4" trailColor={trailColor} strokeLinecap={'square'} />
                            </div>
                        </li>
            
        		
            {/*<div>
            <h1>Uploading asset process {Object.keys(this.props.aws.uploadProgress).length > 0 ? this.props.aws.uploadProgress[0].percent : 'its undefined'}</h1>
            
            </div> */}
        	</ul>
        	</div>
        )
    }
}

export default EditAssetUploadingSteps;