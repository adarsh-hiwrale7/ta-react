import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Field, reduxForm } from 'redux-form';
import { renderField } from '../Helpers/ReduxForm/RenderField'

import * as assetsActions from "../../actions/assets/assetsActions"

import Globals from '../../Globals';
import notie from "notie/dist/notie.js"
import PreLoader from '../PreLoader'
import PopupWrapper from '../PopupWrapper'

const required = value => value ? undefined : 'Required';

class CreateCategory extends React.Component {

  constructor(props) {
    super(props);
  }



  renderLoading() {
    return (
      <div className="preloader-wrap">
        <PreLoader/>
      </div>
    )
  }


  render() {

    const {loading} = false;
    const {handleSubmit, pristine, submitting} = this.props;
    const catID = this.props.catID;

    var catName = '';
    if(catID!=null) {
      catName = this.props.catName[0].name;
    }

    var me = this;

    return (
      <section className="create-cat-page" >

          <div id="cat-creation">

            <div>

            	  { (loading ? this.renderLoading() : <div></div> ) }

                  <header className="heading">
                    <h3>Asset Category</h3>
                  </header>
                  <PopupWrapper>
                  <div className="assetForms">

                    <form onSubmit={handleSubmit}>

                      <div className="section clearfix">
                        <Field placeholder="Category name" label="Category Name" component={renderField} type="text" name="catname" validate={[ required ]} />
                        <div className="form-row">
                          <button class="btn btn-primary right" type="submit" disabled={pristine || submitting}>Save</button>
                        </div>
                      </div>
                    </form>

                  </div>
                  </PopupWrapper>

            </div>
          </div>

        </section>
    );
  }

}


function mapStateToProps(state) {
  return {
    assets: state.assets
  }
}

function mapDispatchToProps(dispatch) {
  return {
     assetsActions: bindActionCreators(assetsActions, dispatch)
  }
}

let connection = connect(mapStateToProps, mapDispatchToProps);

export default reduxForm({
  form: 'createCategory', // a unique identifier for this form
})(CreateCategory)
