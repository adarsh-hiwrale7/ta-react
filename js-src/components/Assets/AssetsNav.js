import Collapse, { Panel } from 'rc-collapse'; // collapse
// collapse css
import { IndexLink, Link } from 'react-router';
import * as utils from '../../utils/utils';
import Globals from '../../Globals';
import ReactDOM from 'react-dom'
var current_pagename;
class AssetsNav extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      //  current_pagename:null
    }
  }
  componentDidUpdate() {
    var matches = document.querySelectorAll("#navdraw .nav-side .rc-collapse-item a.active");
    if(matches[0]!=undefined)
    {
      var parent = matches[0].parentNode.parentNode.parentNode.parentNode.parentNode;
      parent.className = 'rc-collapse-item rc-collapse-item-active active';
    }
  }
  renderCatsInSidebar() {
    const cats = this.props.cats
    var me = this;  
    return (
      <ul className='parent clearfix'>
      {
        cats.length==0?
        <div>
            <div class="custom-feed-toggle loader-grey-line loader-line-space loader-line-height loader-sidebar-width loader-line-radius active"></div>
            <div class="custom-feed-toggle loader-grey-line loader-line-space loader-line-height loader-sidebar-width loader-line-radius active"></div>
            <div class="custom-feed-toggle loader-grey-line loader-line-space loader-line-height loader-sidebar-width loader-line-radius active"></div>
            <div class="custom-feed-toggle loader-grey-line loader-line-space loader-line-height loader-sidebar-width loader-line-radius active"></div>
            <div class="custom-feed-toggle loader-grey-line loader-line-space loader-line-height loader-sidebar-width loader-line-radius active"></div>
        </div>
        :
        cats.map(cat => {
          const selected = cat.identity == me.props.currCatID;
          //     selected && me.props.listingType == 'all' ?this.setState({ current_pagename: 'assets-category' }) : '';
          return (
            <li key={cat.identity}>
              <Link
                className={
                  selected && me.props.listingType == 'all' ? ` active` : ``
                }
                to={`/assets/cat/${cat.identity}`}
              >
                <span className='text'>{cat.name}</span>
              </Link>
            </li>
          )
        })}
      </ul>
    )
  }

  renderMyAssetsLinkInSidebar() {
    var me = this;
    //  me.props.listingType == 'user' ?this.setState({ current_pagename: 'assets-category' }) : '';
    me.props.listingType == 'user' ? current_pagename = 'my-assets' : '';

    return (
      <ul className='parent clearfix'>
        <li>
          <Link
            className={me.props.listingType == 'user' ? ` active` : ``}
            to={`/assets/my`}
          >
            <span className='text'>Uploaded assets</span>
          </Link>
        </li>
      </ul>
    )
  }

  renderArchiveAssetsLinkInSidebar() {

    var me = this;
    //me.props.currPage == 'archive'?this.setState({ current_pagename: 'assets-category' }) : '';
    me.props.listingType == 'user' ? current_pagename = 'archive-assets' : '';
    return (
      <ul className='parent clearfix'>
        <li>
          <Link
            className={me.props.currPage == 'archive' ? ` active` : ``}
            to={`/assets/cat/` + this.props.archiveCat}
          >
            <span className='text'>Archive</span>
          </Link>
        </li>
      </ul>
    )
  }



      renderCampaignsFoldersInSidebar() {
        let campaigns = []
        if (typeof this.props.asset_campaigns !== 'undefined') {
          campaigns = this.props.asset_campaigns
        }
        var me = this
        return (
          <ul className='parent  clearfix'>
            {campaigns.map(campaign => {
              const selected = campaign.identity == me.props.currCatID

              return (
                <li key={campaign.identity}>
                  <Link
                    className={
                      selected && me.props.listingType == 'all' ? ` active` : ``
                    }
                    to={{ pathname : `/assets/cat/${campaign.identity}` ,
                          state : {fromCampaign : 'true'}}}
                  >
                    <span className='text'>{campaign.name}</span>
                  </Link>
                </li>
              )
            })}

          </ul>
        )
      }
/**
 * @author disha 
 * asset_customFeed is come from header.js file to display list of asset custom feed folder
 */
      renderCustomFeedAsset(){
        let customfeed = []
        if (typeof this.props.asset_customFeed !== 'undefined') {
          customfeed = this.props.asset_customFeed
        }
        var me = this
        return (
          <ul className='parent  clearfix'>
            {customfeed.map(customasset => {
              const selected = customasset.identity == me.props.currCatID

              return (
                <li key={customasset.identity}>
                  <Link
                    className={
                      selected && me.props.listingType == 'all' ? ` active` : ``
                    }
                    to={`/assets/custom/${customasset.identity}`}

                  >
                    <span className='text'>{customasset.name}</span>
                  </Link>
                </li>
              )
            })}

          </ul>
        )
      }
    render(){
      let roleName =JSON.parse(localStorage.getItem('roles')) !== null? JSON.parse(localStorage.getItem('roles'))[0].role_name:''
        return(
            <nav className='nav-side'>

                          <Collapse
                            accordion={false}
                            onChange={this.onChange}
                            defaultKey={'1'}
                            defaultActiveKey={['1', '2', '3','4']}
                          >
                          {roleName!=='guest'?
                            <Panel header='CATEGORIES' key={'1'}>
                              {this.renderCatsInSidebar()}
                            </Panel>:''}
                            {roleName!=='guest'?
                            <Panel header='CAMPAIGNS' key={'2'}>
                              {this.renderCampaignsFoldersInSidebar()}
                            </Panel>:''}
                            {roleName!=='guest'?
                            <Panel header='MY ASSETS' key={'3'}>
                              {this.renderMyAssetsLinkInSidebar()}
                            </Panel>:''}
                            <Panel header='CUSTOM FEED' key={'4'}>
                              {this.renderCustomFeedAsset()}
                            </Panel>
                            
        </Collapse>
        {roleName!=='guest' && roleName !=='employee'?
        <div className='archive-file-link'>
        {this.renderArchiveAssetsLinkInSidebar()}
        </div>:''}
      </nav>

    )
  }
}
module.exports = AssetsNav;
