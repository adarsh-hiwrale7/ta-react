import * as utils from '../../utils/utils'
import Asset from './Asset'
import Globals from '../../Globals'
import { IndexLink, Link } from 'react-router'
import ReactDOM from 'react-dom'
import PreLoader from '../PreLoader'
import * as s3functions from '../../utils/s3'
import * as s3Actions from '../../actions/s3/s3Actions'
import * as assetsActions from '../../actions/assets/assetsActions'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import notie from 'notie/dist/notie.js'
import PhotoEditorSDK from '../Chunks/ChunkPhotoEditor'


class EditAsset extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
        loadedImage : null,
        loading: true,
        mediaName: null,
        mediaUrl: null,
        mediaExtension: null,
        photoEditor: null

    }
  }

componentWillMount() {
  PhotoEditorSDK().then(editor=>{
    this.setState({photoEditor:editor})
  })
}

  saveEditedImage(imageData,mediaName,mediaExtension){
    //upload media on s3
    var me = this
    Globals.AWS_CONFIG.albumName = localStorage.getItem('albumName')

    var s3Config = Globals.AWS_CONFIG

    var s3Manager = new s3functions.S3Manager(s3Config)
    var filesToUpload = imageData
    mediaName = mediaName+"_copy_"+Math.floor(Date.now() / 1000)+"."+mediaExtension
    filesToUpload.name = mediaName
    this.props.s3Actions.uploadingStart()
    this.props.s3Actions.storeFileDateTemp(imageData,mediaName)
    // upload starts here.
    // it returns a promise whether succeeded or failed
    // after you get the promise, proceed with db insertion
    
    var s3MediaURL = s3Manager.s3uploadMedia(
      filesToUpload,
      0,
      filesToUpload.length
    )
    var me = this;
    s3MediaURL
        .then(obj => {
         
          var file = obj.data
          var originalFileObj = obj.fileObject
          var index = obj.index

          var generatedFileName = file.Location.split('/')
          generatedFileName = generatedFileName[generatedFileName.length - 1]
          generatedFileName = generatedFileName.substring(
            0,
            generatedFileName.length - 41
          )

          var thumbnail = s3Manager.s3uploadThumbnail(
              originalFileObj,
              index,
              filesToUpload.length,
              true
            )
          //console.log(originalFileObj,originalFileObj,filesToUpload.length);

          thumbnail
              .then(function (thumb) {
               
                
               /* var assetData = [];
                assetData['title'] = generatedFileName;
                assetData['fileUrl'] = file.Location;
                assetData['thumbUrl'] = thumb.Location;
                assetData['folderID'] = typeof me.props.currFolderID !== "undefined" ? me.props.currFolderID : null;
                assetData['catID'] = me.props.currCatID;
                assetData['campID'] = null;
                assetData['description'] = me.props.asset.detail;
                //assetData['user_tag'] = me.props.asset.userTag;
                assetData['user_tag'] = [];*/
                var photokey=file.Location.split('amazonaws.com/')[1]
                var thumbPhotokey=thumb.data.Location.split('amazonaws.com/')[1]
                uploadedFile = {
                  fileUrl: `https://s3.${Globals.AWS_BUCKET_REGION}.amazonaws.com/${file.Bucket}/${photokey}`,
                  thumbUrl: `https://s3.${Globals.AWS_BUCKET_REGION}.amazonaws.com/${file.Bucket}/${thumbPhotokey}`,
                  uploadedFile: generatedFileName,
                  description: me.props.asset.detail,
                  userTag: me.props.asset.userTag,
                  tags: me.props.asset.tags
                }
                me.props.s3Actions.uploadedItem(uploadedFile)
                /*me.props.assetsActions.assetsCreate(assetData, {
                  maxItems: 10,
                })*/


                //me.props.s3Actions.uploadedItem(uploadedFile)
              })
              .catch(err => {
                notie.alert(
                  'error',
                  //'There was a problem generating thumbnail',
                  err,
                  5
                )
              })
    })
  }
  
  componentDidMount() {
    /*this.props.assetsActions.assetsCreate([], {
                  maxItems: 10,
                })*/
    s3functions.loadAWSsdk()
    //this.ui = this. editorElement.ui;

   

    this.editorElement.ui.on('export', (result) => {
       
       /*console.log(result,'result in export function',this.props.media_name);
       console.log('media_url',this.props.media_url);
       console.log('media_extension',this.props.extension);*/
       this.saveEditedImage(result,this.props.asset.title,this.props.asset.media_extension).bind(this);
    });
    const container = document.querySelector('#asset-editor-wrapper')
    const image = new Image()
    image.crossOrigin = "anonymous"
    image.src = this.props.asset.media_url;
    //image.src = 'https://s3-us-west-2.amazonaws.com/talent-advocate/pirates_com/1/result_1517396638540.jpg'
    
    image.onload = () => {
      
      ReactDOM.render(<EditAsset 
                          image={image} 
                          asset={this.props.asset} 
                          currCatID={this.props.currCatID}
                          currFolderID={this.props.currFolderID}
                          assetsActions={this.props.assetsActions}
                          assets={this.props.assets}
                          aws={this.props.aws}
                          s3Actions={this.props.s3Actions}
                      />, container)
      
    }
  }

  renderLoading(status) {
    document.querySelector("#edit-asset-loader").classList.add('hide');
    if(status)
    {
      document.querySelector("#edit-asset-loader").classList.remove('hide');
    }
  }
 renderCreateNewButton () {
    if (this.props.assets.creation.created) {
      return (
        <div className='createNewAssetAction'>
          <button
            className='btn btn-primary'
          >
            ADD MORE
          </button>
        </div>
      )
    }
  }
  render () {
      if(this.state.photoEditor!== null){
      const { ReactComponent } = this.state.photoEditor.UI.DesktopUI
      //if(!this.props.assets.creation.created){
          var me = this;  
          return (
                
                   <section className='create-asset-page'>
                   {
                    (me.props.aws.uploadStarted === true && me.props.aws.uploadDone === false) ? 
                    me.renderLoading(true) : 
                    me.renderLoading(false)

                  }

                    <div ref="checkhere" className='checkhere-wrap'>
                    <ReactComponent
                      ref={c => this.editorElement = c} 
                      license='{"owner":"Chris Heron","version":"2.1","enterprise_license":false,"available_actions":["magic","filter","transform","sticker","text","adjustments","brush","focus","frames","camera"],"features":["adjustment","filter","focus","overlay","transform","text","sticker","frame","brush","camera","library","export"],"platform":"HTML5","app_identifiers":["app.visibly.io","localhost","staging.visibly.io"],"api_token":"jbv-DGwI-x5k-bteHoT3Zw","domains":["https://api.photoeditorsdk.com"],"issued_at":1517536669,"expires_at":1518307200,"signature":"J7EJc9APIHMoDBt+PMKfLTcdoSCtYk6gTuff1L8DcXI5BEUUFX0HDJZtWxOmS2ClQISslGaDR0wEkJ6txcccwpOsalNxxuJnYCtGbnalKDicz92/pwzMVsixw+PbVRCpvME+IYMgKi5TwjBrU/eMgUYafD96H0iWSOlAncChzHPxGaNQvlSDMScQY4/A553BrKtHJyJK5KX6c5qYqsPaN2yEUM24QQdeQuIvtk4cSMDcT7lxUW64x/ws0fjX4r33PoOMIFk1itqjYG+6GYEis/EEzvWcKZ232rM8INCwgzka1xAL8BWE/JGGJ7le2Hd/tGiBci1PVmlo5/Me+KbJFIX/BMf8X+GH2mEXIAzWJ1j63GTQGakCzt4ipNykXdkXFGbfsdkHcVcbZx16FsV+nml/5UClHB6cNSqHDxzSXIBHlm6ws6DFrDeT+oM65HxlFs3DnL14QEQ14u7dzHYlehmHIfAk+Qy2tQxZHVMHozub9TMY0EFS4SVebtN+kbiyruXl9CfrcjMnLg9GUUe+5O/uHkpvBuK8acu+xgzEcmuI3r1PbT2faGDvGZt2PdWjyeKLkMgLkSf0mlNj5wp1j56uFytPqHRIaZUvyYzI1KGPbwinM4h8axl8mzJ6Hqb+NSSn5vmg73QQsbtY4R2qBlJrSCmhB4FS76ejNVo5UCU="}'
                      assets={{
                        baseUrl: '/assets'
                      }}
                      editor={{
                        image: this.props.image,
                        export:{
                          type: 'blob'
                        }
                        
                      }}
                      style={{
                        width: 1024,
                        height: 576
                      }}
                    /> 
                    </div>
                    </section>

          )  
      /* }else{
          console.log('create new button called');
          this.renderCreateNewButton()
       }*/
      }else{
        return<div></div>
      }
      
  }
   
}

function mapStateToProps(state) {
  return {
     aws: state.aws,
     assets: state.assets
  }
}

function mapDispatchToProps(dispatch) {
  return {
     s3Actions: bindActionCreators(s3Actions, dispatch),
     assetsActions: bindActionCreators(assetsActions, dispatch)
  }
}

let connection = connect(mapStateToProps, mapDispatchToProps);

module.exports = connection(EditAsset)
