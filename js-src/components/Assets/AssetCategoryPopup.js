// collapse css
import { IndexLink, Link } from 'react-router';
import * as utils from '../../utils/utils';

import ReactDOM from 'react-dom';
import * as folderActions from '../../actions/assets/folderActions'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

class AssetCategoryPopup extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
          layoutDetail: null,
          currentFolder: [],
          breadcrumsArray: [],
          breadcrumsWithName: [],
          selectedFolder:null,
          clickFolderPrevent:false,
          clickTimer:0,
          selectCategory:null,
          selectedFolderParentId:null
        }
    }
    componentWillReceiveProps(nextProps){
      
        if(nextProps.folder.popupFolder !== this.props.folder.popupFolder){
          this.setState({layoutDetail:nextProps.folder.popupFolder
          });
          nextProps.folder.popupFolder.forEach(element => {
            if(element.identity==nextProps.selectedFolder){
              this.setState({selectedFolderParentId:element.parent_identity
              });
            }
          });      
        }
    }
    
    //single click on arrow to open category/campaign  
    singleClickOpenCategory(action){
        clearTimeout(this.state.clickTimer)
        this.setState({
          clickFolderPrevent: true
        })
        if(action == "category"){
            this.setState({
                  layoutDetail:this.props.category,
                  currentFolder:'category',
                  breadcrumsArray:[...this.state.breadcrumsArray, 'category'],
                  breadcrumsWithName:['category'],
                  selectCategory:'category' 
            });   
        }else if(action == "campaign"){
          this.setState({
                  layoutDetail:this.props.campaign,
                  currentFolder:'campaign',
                  breadcrumsArray:[...this.state.breadcrumsArray, 'campaign'],
                  breadcrumsWithName:['campaign'],
                  selectCategory:'campaign'
          });
        }else if(action == "customFeed"){
          this.setState({
                layoutDetail:this.props.customFeedAsset,
                currentFolder:'custom Feed',
                breadcrumsArray:[...this.state.breadcrumsArray, 'customFeed'],
                breadcrumsWithName:['custom Feed'],
                selectCategory:'customFeed' 
          });   
      }
        
    }
    
    //single click on arrow to open folder
    singleClickOpenFolder(identity,name){
        this.setState({
                          currentFolder:name,breadcrumsArray:[...this.state.breadcrumsArray, identity],
                          breadcrumsWithName:[...this.state.breadcrumsWithName, name],
                          selectedFolder:identity            
        });   
        this.props.folderActions.fetchPopupFolders(identity);
    }
    
    backFolder(){
        //get the second last index of array to get parent category id and name to go back
        var parentIdentity = this.state.breadcrumsArray[this.state.breadcrumsArray.length - 2]
        this.state.currentFolder = this.state.breadcrumsWithName[this.state.breadcrumsWithName.length - 2]
        //remove last element from array as we are moving back and set in state
        this.state.breadcrumsArray.splice(-1, 1);
        this.state.breadcrumsWithName.splice(-1, 1);
        this.setState({
            breadcrumsArray:this.state.breadcrumsArray,
            breadcrumsWithName:this.state.breadcrumsWithName,
            selectedFolder:parentIdentity   
        });

        if(this.state.breadcrumsArray.length > 1){
          //if you are in folders then fetch folders
          this.props.folderActions.fetchPopupFolders(parentIdentity);  
        }else if(this.state.breadcrumsArray.length == 1 && this.state.breadcrumsArray[0] === "category"){
          //if first initial page of category arrives and you were in category just show the category
          this.setState({layoutDetail:this.props.category});
        }else if(this.state.breadcrumsArray.length == 1 && this.state.breadcrumsArray[0] === "campaign"){
          //if first initial page of category arrives and you were in campaign just show the category
          this.setState({layoutDetail:this.props.campaign});
        }else{
          //initial state of category and campaign
          this.setState({layoutDetail:null,currentFolder:[]});
        }  
        
    }

    renderPopupValues(detail = null){
        if(detail != null){
            return (
              <ul id="assetMoveSubActions">
                {
                 (detail.length > 0) ?  
                  detail.map((catDetail,i) => {
                    return (<li key={i} className={this.state.selectedFolder === catDetail.identity ? 'active' :'',catDetail.identity == this.props.selectedFolder ? 'disabled':''}>
                    <a className='move-next-text' onClick={this.singleClickOpenFolder.bind(this,catDetail.identity,catDetail.name)} >
                    <i class="material-icons folder-icon">folder</i>{catDetail.name}</a>
                    <a className='move-next-arrow' onClick={this.singleClickOpenFolder.bind(this,catDetail.identity,catDetail.name)}><i className='material-icons'>keyboard_arrow_right</i>
                    </a>
                    </li>)
                  })
                  : 
                   <li className='no-folder-found-li'></li> //no folders
                }
              </ul>
            )
        }else{
            return (
                <ul id="assetMoveActions">
                  <li className={this.state.selectCategory === 'category' ? 'active button-dropdown' : 'button-dropdown'}><a className='move-next-text' onClick={this.singleClickOpenCategory.bind(this,'category')} ><i className='material-icons list-icon'>list</i>Category</a><a className='move-next-arrow' onClick={this.singleClickOpenCategory.bind(this,'category')}><i className='material-icons'>keyboard_arrow_right</i></a></li>
                  <li className={this.state.selectCategory === 'customFeed' ? 'active button-dropdown' : 'button-dropdown'}><a className='move-next-text' onClick={this.singleClickOpenCategory.bind(this,'customFeed')} ><i className='material-icons list-icon'>list</i>Custom Feed</a><a className='move-next-arrow' onClick={this.singleClickOpenCategory.bind(this,'customFeed')}><i className='material-icons'>keyboard_arrow_right</i></a></li>
                  {/* <li className={this.state.selectCategory === 'campaign' ? 'active button-dropdown' : 'button-dropdown'}><a className='move-next-text' onClick={this.singleClickOpenCategory.bind(this,'campaign')} ><i className='material-icons lightbulb_outline-icon'>lightbulb_outline</i>Campaign</a><a className='move-next-arrow' onClick={this.singleClickOpenCategory.bind(this,'campaign')}><i className='material-icons'>keyboard_arrow_right</i></a></li> */}
                </ul>  
            )
        }  
          
    }
   
    unarchiveAction(){
      if(this.props.selectedAsset.length > 0){
        this.props.unArchiveAssetClick(this.state.selectedFolder);
      }else if(this.props.selectedFolder.length > 0){
        this.props.unArchiveFolderClick(this.state.selectedFolder);
      }
      
    }
    moveAction(){
      if(this.props.selectedAsset.length > 0){
        this.props.moveAssetClick(this.state.selectedFolder);
      }else if(this.props.selectedFolder.length > 0){
        this.props.moveFolderClick(this.state.selectedFolder);
      }
       
    }
    render(){
        return(
            <div id="asset-file-action-popup">
                  <div className='asset-file-action-title'>
                      {this.state.currentFolder.length > 0 ?
                        <span>
                              <a onClick={this.backFolder.bind(this)} id="move-back-arrow" className='move-back-arrow'>
                                <i className='material-icons'>arrow_back</i>
                              </a>
                              {this.state.currentFolder}
                        </span>: 
                        <span>
                              <a  id="move-back-arrow" className='move-back-arrow'>
                                <i className='material-icons'>folder_open</i>
                              </a>Assets
                          </span>
                        }
                      </div>
                      {this.renderPopupValues(this.state.layoutDetail)}
                    <div className='asset-file-buttons-wrapper clearfix'>
                        {(this.state.currentFolder.length > 0 && this.state.currentFolder !== "category" && this.state.currentFolder !== "campaign") ? 
                        <a className={`asset-move-here-link ${this.state.selectedFolderParentId == this.state.selectedFolder ? 'disabled':''} ${this.props.selectedAsset.length>0?this.props.selectedAsset[0].category.data.name == this.state.currentFolder ? 'disabled':'':''}`}
                            title="Move here" 
                            onClick={this.props.actionToPerform === "unarchive" ? 
                            this.unarchiveAction.bind(this) 
                            : this.moveAction.bind(this)}>
                            Move here
                        </a>
                        : ''}
                    </div>
            </div>
                
        )
    }
}

function mapStateToProps(state) {
  return {
    folder : state.folder
  }
}

function mapDispatchToProps(dispatch) {
  return {
    folderActions: bindActionCreators(folderActions, dispatch),
  }
}

module.exports = connect(mapStateToProps, mapDispatchToProps)(AssetCategoryPopup)
