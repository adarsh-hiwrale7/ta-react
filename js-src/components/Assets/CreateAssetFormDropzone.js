import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { Field, reduxForm } from 'redux-form';
import * as generalActions from '../../actions/generalActions'
import { renderDropzoneBox } from '../Helpers/ReduxForm/RenderField'
import ReactDropzone from '../Chunks/ChunkReactDropzone';
var FileUploadThumbnail = require('file-upload-thumbnail');
const required = value => value ? undefined : 'Required';
// var count=0
var Current_file=0;
var imgDataTosend={}
class CreateAssetFormDropzone extends React.Component {

  constructor(props) {
    super(props);
    this.state={
      reactDropzone:null
    }
  }
  /**
  * Warning: This lifecycle is currently deprecated, and will be removed in React version 17+
  More details here: https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html
  */
  componentWillMount() {
    ReactDropzone().then(dropzone=>{
      this.setState({
        reactDropzone:dropzone
      })
    })
  }
  componentDidMount() {
    Current_file=0;
  }
/**
 * @author disha
 * to get thumbnail url from file object and display image into div of renderfield
 * @param {*} files
 */
getThumbnailUrl(files, count){
  if(files){
  var props=this.props

    setTimeout(function(){
    new FileUploadThumbnail({
      maxWidth: 500,
      maxHeight: 400,
      file: files[count],
      onSuccess: function(src){
        var img = document.createElement('img');
          img.src = src;
         setTimeout(function(){
          if(document.getElementById(`dropPreviewIconInner-${files[count].preview}`)!==null){
            imgDataTosend={
              thumbnailSrc:src,
              filePreview:files[count].preview,
            }
            Object.assign(files[count], imgDataTosend)
            props.setImageThumbnailElement(imgDataTosend)
            document.getElementById(`dropPreviewIconInner-${files[count].preview}`).appendChild(img)
          }
           },300)
      }
    }).createThumbnail();
     var video = document.createElement('video');
      video.preload = 'metadata';
      video.onloadedmetadata = function() {
        window.URL.revokeObjectURL(video.src);
        var duration = video.duration;
        files[count].duration = duration;
      }
      video.src = URL.createObjectURL(files[count]);
  },400)
 
}
  count = Current_file++;
  if(count<Object.keys(files).length-2)
  {
    this.getThumbnailUrl(files, count)
  }
}

  renderDropzone() {
    const {handleSubmit, pristine, submitting, tags} = this.props;
    var me = this;
    let { uploadProgress, uploadDone, uploadStarted } = this.props.aws;
     if(uploadDone) {
      var dzArea = document.querySelector(".dropzoneUpload");
      if(typeof(dzArea)!=="undefined" && dzArea!=null) {
        dzArea.style.pointerEvents = "none";
      }
   }
  var freeStorage=this.props.freeStorage !==''? this.props.freeStorage:''
  var dropzoneStyle = {
      borderColor: '#666',
      borderRadius: '5px',
      borderStyle: 'dashed',
      borderWidth: '1px',
      height: 'auto',
      minHeight: '120px',
      lineHeight: '100px',
      textAlign: 'center',
      padding: '10px',
      width: '100%',
    }

    var btnWrapStyles = {
      display: "none"
    }
    return (
      <form onSubmit={handleSubmit.bind(this)}>
        <div className="section">
          <div className="step1 active">

            <div className="form-row">
            
              {/* <label>Upload Media</label> */}
              <Field
                label="Media"
                name="files"
                freeStorage={freeStorage}
                title={uploadDone ? `Click Next to proceed` : `Drag and drop`}
                multi={true}
                uploadProgress={uploadProgress}
                style={dropzoneStyle}
                component={renderDropzoneBox}
                validate={[ required ]}
                Dropzone={this.state.reactDropzone}
                FromAsset ={true}
                onChange={(file)=>{this.getThumbnailUrl(file)}}
                uploadedAssetBuffer = {this.props.assets.uploadedAssetBuffer}
                submit={()=> {
                  var submitBtn = document.querySelector("#btn-upload-assets");
                  // a small hack to auto-upload files dropped into dropzone
                  var clickInterval = window.setInterval(function() {

                    //if(pristine) {
                      if(typeof(submitBtn)!=="undefined") {
                        submitBtn.click();
                        clearInterval(clickInterval)
                      }
                   // }
                  }, 100)
                }}
              />
            </div>
            <div style={btnWrapStyles}>
              <button className="btn btn-primary" id="btn-upload-assets" type="submit" >Upload</button>
              <button className="btn btn-primary right" type="submit" disabled={pristine || submitting}>Next</button>
            </div>

          </div>
        </div>
      </form>
    )
  }
render() {
    var assetCreated = this.props.assets;
    return (
        <div className="section">
          {  this.renderDropzone()  }
        </div>
    )
  }

}
function mapStateToProps(state) {
  return {
    aws: state.aws,
    assets: state.assets,
    general:state.general
  }
}

function mapDispatchToProps(dispatch) {
  return {
    generalActions: bindActionCreators(generalActions, dispatch),
  }
}
let connection = connect(mapStateToProps, mapDispatchToProps);

var reduxConnectedComponent = connection(CreateAssetFormDropzone);

export default reduxForm({
  form: `dropzoneForm`,  // a unique identifier for this form
})(reduxConnectedComponent)
