import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { Field, reduxForm } from 'redux-form'

import * as utils from '../../utils/utils'

import { renderField } from '../Helpers/ReduxForm/RenderField'
import { renderTagsField } from '../Helpers/ReduxForm/RenderField'
import MentionTextArea from '../Helpers/ReduxForm/MentionTextArea'
import * as userActions from '../../actions/userActions'
import * as tagsActions from '../../actions/tagsActions'
const required = value => (value ? undefined : 'Required')
const validEmail = value =>
  (value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
    ? 'Invalid email address'
    : undefined)

class CreateAssetForm extends React.Component {
  constructor (props) {
    super(props)
  }
componentWillMount() {
}

  renderThumbInForm () {
    var fileInfo = typeof this.props.isFromEditor !== "undefined" ? this.props.fileUploaded[0] : this.props.fileUploaded;
    if (fileInfo.thumbUrl == '') {
      return (
        <div class='doc'>
          <div class='file-icon file-icon-lg' data-type='' />
        </div>
      )
    } else {
      return <img src={fileInfo.thumbUrl} width='70' height='70' alt='' />
    }
  }

  renderForms () {
    const { pristine, submitting, tags } = this.props

    var fileInfo = this.props.fileUploaded

    var assetCreated = this.props.assets.creation.created
    let userTags = this.props.userTags;
    let postTags = this.props.postTags;
    return (
      <form onSubmit={this.handleSubmit.bind(this)} >

        {
          //assetCreated ? this.renderTick() : null
        }

        <div className={`section clearfix ${assetCreated ? ' created' : ''}`}>
          <div className='left col-20 create-assest-thumb-warpper'>
            <div className='thumb'>
              {this.renderThumbInForm()}
            </div>
          </div>
          <div className='col-80 right active create-assest-inputs-wrapper'>

            <Field
              component={renderField}
              type='hidden'
              name='fileUrl'
            />
            <Field
              component={renderField}
              type='hidden'
              name='thumbUrl'
            />
            <Field
              component={renderField}
              type='hidden'
              name='catID'
            />
            <Field
              component={renderField}
              type='hidden'
              name='campID'
            />
            <Field
              component={renderField}
              type='hidden'
              name='folderID'
            />
            <Field
              component={renderField}
              type='hidden'
              name='numberOfUploaded'
            />
            <Field
              component={renderField}
              type='hidden'
              name='filesize'
            />

            <Field
              placeholder='Title'
              label={null}
              component={renderField}
              type='text'
              name='title'
              validate={[required]}
            />

            <div className='form-row user-tag-editor'>
              {/* <label>Tags</label> */}
              <Field
                name='description'
                component={MentionTextArea}
                type='textarea'
                 ref='description'
                  /* component={renderField} */
                  userTags={this.props.userTags}
                  validatedata={[required]}
                  tags={this.props.postTags}
                  validate={[required]}

              />
            </div>
          </div>
        </div>
      </form>
    )
  }

  renderTick () {
    return (
      <div class='finishedUploadingAsset center'>
        <svg
          className='checkmark active'
          xmlns='http://www.w3.org/2000/svg'
          viewBox='0 0 52 52'
        >
          <circle
            className='checkmark__circle'
            cx='26'
            cy='26'
            r='25'
            fill='none'
          />
          <path
            className='checkmark__check'
            fill='none'
            d='M14.1 27.2l7.1 7.2 16.7-16.8'
          />
        </svg>
        <h4 className='center m-t-one'>
          Your asset was uploaded successfully and sent for moderation!
        </h4>
      </div>
    )
  }

  handleSubmit (values) {
    this.props.handleFormSubmit(values)
  }

  render () {
    var me = this
    return (
     me.props.isFromEditor?
      <div className='section'>
        {this.renderForms()}
      </div>

      :
      <div className='section'>
        {this.renderForms()}
     </div>
    )
  }
}

function mapStateToProps (state) {
  return {
    assets: state.assets,
    userTags: state.tags.userTags,
    postTags: state.tags.postTags,
    departmentTags:state.tags.postTags,
  }
}

function mapDispatchToProps (dispatch) {
  return {
    tagsActions: bindActionCreators(tagsActions, dispatch),
    userActions: bindActionCreators(userActions, dispatch)
  }
}
let connection = connect(mapStateToProps, mapDispatchToProps)

var reduxConnectedComponent = connection(CreateAssetForm)

export default reduxForm(
  {
    // form: ``,  // a unique identifier for this form
  }
)(reduxConnectedComponent)
