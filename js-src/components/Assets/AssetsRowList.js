
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as utils from '../../utils/utils'
import AssetTags from './AssetTags'
import ImageLoader from 'react-imageloader'
import PreLoaderMaterial from '../PreLoaderMaterial'
import Globals from '../../Globals'
class AssetsRowList extends React.Component {
  handleAssetDoubleClick (asset,me) {
    this.props.assetDoubleClick(asset,me)
  }
renderAssetTags (asset) {
    return <AssetTags asset={asset} />
  } 
  showeditor(assets){
    var objForEditor = [];
   objForEditor.push({
     name:assets.title+"."+ assets.media_extension,
     preview:assets.media_url,
     size:assets.size,
     type:assets.media_type
   })
     objForEditor['files'] = objForEditor;
     this.props.showeditor(objForEditor);
  }
  componentWillReceiveProps(){
  //  console.log('assetrow1 called');
   // utils.pauseVideo(".play-btn-overlay video"); 
  } 
componentDidMount () {
  //console.log('assetrow2 called');
  utils.pauseVideo(".play-btn-overlay video");
 }
 handleAssetClick(asset, e) {
   this.props.assetClick(asset,e)
 }
  render () {
    utils.pauseVideo(".play-btn-overlay video");
    const selected = this.props.selected;
    var assets = this.props.assets
    var moment = this.props.moment
    return (
      <div>
          <div className="archive-container asset-file-container clearfix" id="myasset"> 
        <div>
        {assets.map((asset, index) => {
          var authorAvatarURL = asset.profileImage
          var authorAvatar = {
            backgroundImage: `url(${typeof authorAvatarURL !== 'undefined' && authorAvatarURL != '' ? authorAvatarURL : '/img/default-avatar.png'})`
          }

          let postTitle = asset.title, postDate = new Date().getTime() * 1000
          let postThumb = '', media_type = ''

          var assetAuthor = asset.firstname + ' ' + asset.lastname

          var media_type_index = asset.media_type.indexOf('/')

          if (media_type_index !== -1) {
            media_type = asset.media_type.substr(0, media_type_index)
          } else {
            media_type = asset.media_type
          }
          var media_url = asset.media_url
          var thumbnail_url = asset.thumbnail_url
          var media_extension = asset.media_extension
          var faIcon = utils.getFaIcon(media_extension)
          let assetDetail = {
            identity: asset.identity,
            title: postTitle,
            detail: asset.detail,
            approved: asset.approved,
            firstname:asset.firstname,
            lastname:asset.lastname,
            size:asset.media_size,
            media_type,
            media_url,
            thumbnail_url,
           
            media_extension,
            assetAuthor,
            created_date: asset.created_date.date,
            tags: typeof asset.tags !== 'undefined' ? asset.tags : [],
            userTag:asset.userTag
          }
          var imageStyle = {
            backgroundImage: `url(${typeof thumbnail_url !== 'undefined' ? thumbnail_url : media_url})`
          }
          var img_thumnail = '';
              if(thumbnail_url !== 'undefined')  {
                      img_thumnail = thumbnail_url ;
                         }
               else{
                img_thumnail = media_url;
              }
          var docuploadedassets_icon = "";
          switch (media_type) {
            case 'image':
            docuploadedassets_icon = "image";

              postThumb = (
                
                <div id={`asset-${asset.identity}${selected ? '-selected' : ''}`} className='vid file-image asset-file imgwrapper'>
                 <div className='assetInnerWrapper'>
                 {/* <div className="inner_imgwrapper"> */}
                 {/* <i class="material-icons img-load-img" >&#xE410;</i>  */}
                 <a className='type-image' 
                 onClick={this.handleAssetClick.bind(this,assetDetail)} 
                 onDoubleClick= {this.handleAssetDoubleClick.bind(this, assetDetail)}> 
                 {/* onClick={() => this.props.assetClick(assetDetail)} */}
                
                  <ImageLoader
                    src={img_thumnail}
                    preloader={PreLoaderMaterial}
                    className = "thumbnail"
                   >
                   <div className="image-load-failed">
                   <i class="material-icons no-img-found-icon">&#xE001;</i>
                   </div> 
      
                   </ImageLoader>
                 
                 <div class="heading-edit-post">
                   <span className="edit-post" onClick ={this.showeditor.bind(this,assetDetail)}><i class="material-icons">&#xE254;</i></span>
                    <a  href={media_url} className = "download_Assets">
                       <i class="material-icons">&#xE2C4;</i>
                    </a>
                </div>
                 
                  {/* <div className='thumbnail' style={imageStyle} /> */}
                </a>
                </div>
                </div>
              // </div>
              
              )
              break
            case 'video':
            docuploadedassets_icon = "videocam";
              postThumb = (
                <div id={`asset-${asset.identity}${selected ? '-selected' : ''}`} className='vid file-image asset-file imgwrapper file-video'>
                <div className='assetInnerWrapper inner_imgwrapper'>
                {/* <div className="inner_imgwrapper"> */}
                <a className='play-btn-overlay type-video' onClick={this.handleAssetClick.bind(this,assetDetail)} onDoubleClick= {this.handleAssetDoubleClick.bind(this, assetDetail)}> 
                 <div className='thumbnail'>
                    <ImageLoader
                        src={thumbnail_url}
                        preloader={PreLoaderMaterial}
                        className = "thumbnail"
                      >
                      <div className="image-load-failed">
                      <i class="material-icons no-img-found-icon">&#xE001;</i>
                      </div> 
          
                      </ImageLoader>
                      {/* <video width={240} height={285} poster={thumbnail_url}>
                        <source src={media_url} type='video/mp4' />
                        <source src={media_url} type='video/webm' />
                        <source src={media_url} type='video/ogg' />
                      </video> */}
                      <i className="material-icons asset-video-icon">play_circle_filled</i>
                    </div>

                    <div class="heading-edit-post">
                      {/* download button  */}
                      {/* if any change in this code then reflect in feed selected assets */}
                      <a  href={media_url} className = "download_Assets">
                        <i class="material-icons">&#xE2C4;</i>
                      </a>
                                
                    </div>
                </a>
                </div>
                </div>
              //  </div>            
  )
              break
            case 'audio':
            docuploadedassets_icon = "insert_drive_file";
              postThumb = (
                <div id={`asset-${asset.identity}${selected ? '-selected' : ''}`} className='vid file-image asset-file imgwrapper'>
                 <div className='assetInnerWrapper inner_imgwrapper'>
                 {/* <div className="inner_imgwrapper"> */}
                    <a className = {`asset-file-doc  file file-doc thumbnail ${media_extension}`} onClick={this.handleAssetClick.bind(this,assetDetail)} 
                    onDoubleClick= {this.handleAssetDoubleClick.bind(this, assetDetail)}> 
                    <i className='fa fa-file-audio-o fa-3x' />

                       <div class="heading-edit-post">
                        {/* download button  */}
                        {/* if any change in this code then reflect in feed selected assets */}
                        <a  href={media_url} className = "download_Assets">
                          <i class="material-icons">&#xE2C4;</i>
                        </a>
                      </div> 

                    </a>
                  </div>
                  </div>
              //  </div>
               
             
              )
              break
            case 'application':
            docuploadedassets_icon = "insert_drive_file";
            postThumb = (   
               
                <div id={`asset-${asset.identity}${selected ? '-selected' : ''}`} className='vid file-image asset-file imgwrapper'>
                  <div className='assetInnerWrapper inner_imgwrapper'>
                    {/* <div className="inner_imgwrapper">  */}
                    <div className="type-image">
                 <a className={` asset-file-doc  file file-doc thumbnail ${media_extension}`} 
                      onClick={this.handleAssetClick.bind(this,assetDetail)} 
                      onDoubleClick= {this.handleAssetDoubleClick.bind(this, assetDetail)}> 
                    <i className={`fa ${faIcon} fa-3x`} />


                  <div class="heading-edit-post">
                          {/* download button  */}
                          {/* if any change in this code then reflect in feed selected assets */}
                          <a  href={media_url} className = "download_Assets">
                              <i class="material-icons">&#xE2C4;</i>
                          </a>
                  </div>

                  </a>
                </div>
                </div>
               </div>
              //  </div>
              )
              break
            default:
            docuploadedassets_icon = "insert_drive_file";
              postThumb = (
               
                   <div className="image-load-failed">
                   <i class="material-icons no-img-found-icon">&#xE001;</i>
                   </div> 
            //  <div id={`asset-${asset.identity}${selected ? '-selected' : ''}`} className='vid file-image asset-file imgwrapper'>
            //     <a className={`asset-file-doc  file file-doc thumbnail ${media_extension}`}> 
            //     <i className={`fa ${faIcon} fa-3x`} />
            //     </a>
            //    </div>
              )
              break
          }

          return (
            
            <div className="imgwrapper">
               <div className="inner_imgwrapper"> 
                 <a className="file-image type-image">
                  {postThumb}
                 </a>
               </div>
               <div className="file-details">
                 <i class="material-icons asset-title-icon">{docuploadedassets_icon}</i>
                 <span className="title">{postTitle + '.' +media_extension}</span>
               </div>
            </div> 
          )
        })}
        {this.props.loadmore == true?
        <div>
        {
                  Array(3).fill(1).map((el, i) => 
                  <div id="assets-loader">
                <div class="vid file-image asset-file imgwrapper">
                  <div class="inner_imgwrapper assetInnerWrapper">
                    <a class="type-image">
                        <span class="imageloader loaded thumbnail loader-assets-image">
                            <img src="/img/visiblyLoader.gif" />
                        </span>
                    </a>
                </div>
                <a class="file-details">
                    <span class="title">
                        <div className="loader-grey-line loader-line-height loader-line-radius loader-assets-title"> </div>
                    </span>
                </a>
              </div>
              </div>
              )}
        </div>:''}
      </div>
      </div>
      </div>
    )
  }
}

function mapStateToProps (state) {
  return {}
}
function mapDispatchToProps (dispatch) {
  return {}
}
module.exports = connect(mapStateToProps, mapDispatchToProps)(AssetsRowList)
