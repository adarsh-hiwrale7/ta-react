import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Link } from 'react-router'
import PreLoader from '../PreLoader'
import SliderPopup from '../SliderPopup'
import ReactCSSTransitionGroup from 'react/lib/ReactCSSTransitionGroup'
import { browserHistory } from 'react-router'
import $ from '../Chunks/ChunkJquery'
import ChunkJqueryDropdown from '../Chunks/ChunkJqueryDropdown'
import TrendingTagsPanel from '../Feed/TrendingTagsPanel';
import Collapse, { Panel } from 'rc-collapse'
import Globals from '../../Globals'
import * as utils from '../../utils/utils'
import notie from 'notie/dist/notie.js'
import * as s3functions from '../../utils/s3'
import * as assetsActions from '../../actions/assets/assetsActions'
import * as generalActions from '../../actions/generalActions'
import * as searchActions from '../../actions/searchActions'
import * as s3Actions from '../../actions/s3/s3Actions'
import Asset from './Asset'
import moment from '../Chunks/ChunkMoment'
import CreateAsset from './CreateAsset'
import CreateFolder from './CreateFolder'
import CreateCategory from './CreateCategory'
import AssetDetail from './AssetDetail'
import AssetsRowList from './AssetsRowList'
import * as userActions from '../../actions/userActions'
import ReactDOM from 'react-dom'
import TagPopup from '../Feed/TagPopup'
import AssetsNav from './AssetsNav';
import SearchNav from '../Search/SearchNav';
import Script from 'react-load-script';
import Header from '../Header/Header';
import * as tagsActions from "../../actions/tagsActions"
import GuestUserRestrictionPopup from '../GuestUserRestrictionPopup';
var restrictassets=false;
var searchValue ='';
var searchTimeout = null;
var selectedDepartmentData ;
var selectedUserData;
var assetsscroll = true
class Assets extends React.Component {
  constructor(props) {
    super(props)
    document.body.classList.remove('overlay');
    this.state = {
      scrollTimer: null,
      newAssetPopup: false,
      newAssetFolderPopup: false,
      newAssetCategoryPopup: false,
      loading: false,
      currCatID: null, // when user is navigating in particular category ID
      campID: null,
      currFolderID: null, // when user is navigating in particular folderID
      selectedFolderID: null, // when user single clicks on a folder, we need to store it's id here
      currCatName: null,
      assetDetailPopup: false,
      currentAssetDetail: null,
      maxItems: 20,
      currPage: 1,
      listingType: 'all',
      moment: null,
      clickPrevent: false,
      clickTimer: 0,
      overlay_flag: false,
      show_menu: true,
      selectedAssetID: [],
      selectedFolderIDArray: [],
      currentPage: "asset",
      clickAssetPrevent: false,
      clickAssetTimer: 0,
      newTrendingPostPopup:false,
      showEditDetailForm: false,
      openEditer: false,
      noAssetData:false,
      selectedAssetType:null,
      globalSearch:false,
      callUnloadMethod:false,
      selectedMediaSizeTotal:'',
      cancelAssetUpload:false
      
    }
    this.onAssetsScroll = this.onAssetsScroll.bind(this) // bind function once
    this.onFolderClick = this.onFolderClick.bind(this)
    this._handleCloseEvent = this._handleCloseEvent.bind(this)
  }
  renderEditerPopup(e,asset) {
    this.setState({openEditer:true , currentAssetDetail: asset});
  }
  closepopup() {

    //let pop_overlay = document.getElementById("tag-popup-overlay");
    let popup_wrapper = document.getElementById("pop-up-tooltip-wrapper");
    //pop_overlay.className = '';
    popup_wrapper.className += ' hide';
  }
  componentDidMount() {
    window.scrollTo(0, 0);
    let roles =JSON.parse(localStorage.getItem('roles')) !== null? JSON.parse(localStorage.getItem('roles')):''
    if(roles!==undefined){
      if(roles[0].role_name=="guest"&& this.props.auth.category_identity!==null){
        browserHistory.push(`/assets/custom/${this.props.auth.category_identity}`)
      }
    }
    if(this.props.assets.loadingasset == false && this.props.assets.nofiles)
    {
      this.setState({
        noAssetData:true
      })
    }
    //noAssetData

    if(!this.props.location.pathname.includes("search")){
      this.props.assetsActions.fetchFilesStart();

    }
    window.addEventListener('closeAssetDetailPopup', this.closeAssetDetailPopup, false);

   // if user landed on /assets for the 1st time, we need to get cats and store in
    // redux the fetchCats will also fetch uncategorised assets and folders
   var path = utils.removeTrailingSlash(this.props.location.pathname)
    path = path.split('/')
 
    if( path[3] !== undefined){
      this.props.assetsActions.fetchFiles(
        path[3],
        path[5],
        this.state.maxItems,
        1,
        true,
        searchValue
      ) 
    
    }
    if(restrictassets !== true){
      this.props.assetsActions.resetFiles()
    if(roles!==undefined && roles[0].role_name!=="guest"){
      if (this.props.assets.cats.length < 1) {
        this.props.assetsActions.fetchCats()
      }
      if (Object.keys(this.props.assets.archiveCat).length < 1) {
        this.props.assetsActions.fetchArchiveCategory()
      }
      if (this.props.assets.campaigns.length < 1) {
        this.props.assetsActions.fetchCatCampaignFolders()
      }
    }
      
      this.props.assetsActions.fetchCatCustomFeedFolders()
    
    if (typeof path[2] === 'undefined') {
      if(roles!==undefined && roles[0].role_name!=="guest"){
        this.props.assetsActions.fetchCats(true)
        this.props.assetsActions.fetchArchiveCategory();
        this.props.assetsActions.fetchBreadCrums('rpPDp');
      }
    } else if (path[2] === 'cat' || path[2] === 'custom') {

      this.checkLocation(path)
      if (typeof path[5] !== "undefined") {
        this.props.assetsActions.fetchFolders(path[5],searchValue)
        this.props.assetsActions.fetchBreadCrums(path[5]);
      } else {
        this.props.assetsActions.fetchFolders(path[3],searchValue)

        this.props.assetsActions.fetchBreadCrums(path[3]);
      }
      //check if it comes from campaign open asset create
      /*console.log(typeof this.props.location.query.createasset,this.state.currCatName,'did mount check')
      if (typeof this.props.location.query.createasset !== "undefined") {
        var catName = this.props.assets.cats.filter(function (cat) {
          return this.props.assets.cats.identity == path[3]
        })
        console.log(catName,'this.state.currCatName',this.props.assets)

        this.openingAssetCreationFromCampaign(this.state.currCatName);
      }*/
        

      this.setState({ listingType: 'all' })
      //check is it archive

    } else if (path[2] == 'my') {
      this.fetchMyAssets(1, true)
    }
    restrictassets=true;
        setTimeout(()=>{
          restrictassets=false;
        },5000)
      }

    /** call event */
    ReactDOM.findDOMNode(this).addEventListener('click', this._handleCloseEvent);
    this.props.tagsActions.fetchTrendingTags();
    this
      .props
      .tagsActions
      .fetchUserTags(path)
    this
      .props
      .tagsActions
      .fetchPostTags('all')

  }
  _handleCloseEvent(e) {
    if (!document.getElementById('pop-up-tooltip-wrapper').contains(e.target)) {
      if (this.state.overlay_flag) {
        document.getElementById('pop-up-tooltip-wrapper').className = 'hide';
        this.setState({ overlay_flag: false })
      }

    }
  }
  onPostCreationClick(campaignId, campaignTitle) {
    let selectedSocialChannels = this.props.channels.selectedChannels.slice();
    let url = `/feed/external/live?createpost=true&campaigntitle=${campaignTitle}&campaignid=${campaignId}`;
    selectedSocialChannels.map(social => {
      url = url + "&social[]=" + social
    })
    browserHistory.push(url);
  }
  handleTourStart() {
    if(this.props.searchedInput == undefined || (!this.props.location.query.includes("search")))
    browserHistory.push('/feed/external/live/start');
  }
  // componentsWillReceiveProps is called whenever prop changes (route
  // change/redux state change will also trigger prop change as redux state is
  // passed as prop with mapStateToProps)
  componentWillReceiveProps(newProps) {

    /* if(this.props.assets !== newProps.assets){
         console.log('assets not matched');
     }
     if(this.props.assetsActions !== newProps.assetsActions){
         console.log('assets action not matched');
     }
     if(this.props.auth !== newProps.auth){
         console.log('auth not matched');
     }
      if(this.props.children !== newProps.children){
         console.log('children not matched');
     }
      if(this.props.dispatch !== newProps.dispatch){
         console.log('dispatch not matched');
     }
      if(this.props.general !== newProps.general){
         console.log('general not matched');
     }
      if(this.props.history !== newProps.history){
         console.log('history not matched');
     }
      if(this.props.location !== newProps.location){
         console.log('location not matched');
     }
      if(this.props.params !== newProps.params){
         console.log('params not matched');
     }
      if(this.props.profile !== newProps.profile){
         console.log('profile not matched');
     }
      if(this.props.route !== newProps.route){
         console.log('route not matched');
     }
     if(this.props.routeParams !== newProps.routeParams){
         console.log('routeParams not matched');
     }
     if(this.props.routes !== newProps.routes){
         console.log('routes not matched');
     }
     if(this.props.userActions !== newProps.userActions){
         console.log('userActions not matched');
     }
     if(this.props.users !== newProps.users){
         console.log('users not matched');
     }*/
     if(this.props.assets.files !== newProps.assets.files && typeof this.props.location.query.openasset !== "undefined"){
          for (var i = 0; i < newProps.assets.files.length; i++) {
              if (newProps.assets.files[i]['identity'] === this.props.location.query.assetid) {
                  this.assetDoubleClickAction(newProps.assets.files[i])
              }
          }
     }
     if(newProps.assets.isArchiveFolder==true && newProps.assets.isArchiveFolder!==this.props.assets.isArchiveFolder ||
      newProps.assets.isArchiveAsset==true && newProps.assets.isArchiveAsset!==this.props.assets.isArchiveAsset ||
      newProps.assets.isUnarchiveAsset==true && newProps.assets.isUnarchiveAsset!==this.props.assets.isUnarchiveAsset ||
      newProps.assets.isUnarchiveFolder==true && newProps.assets.isUnarchiveFolder!==this.props.assets.isUnarchiveFolder
    ){
      //to remove array after archive /unarchive asset or folder
       this.setState({
         selectedFolderIDArray:[],
         selectedAssetID:[]
     })
     this.props.assetsActions.resetArchiveUnarchiveFlag() //to set flag to default (false)
    }

    var newLocation = utils
      .removeTrailingSlash(newProps.location.pathname)
      .split('/')


    this.checkLocation(newLocation)

    if(newLocation[2]=='custom'){
      this.setState({
        currCatName:newLocation[3]
      })
    }
    let validFolderClicks = [
      'dropdown-toggle',
      'btn',
      'btn-default',
      'title',
      'file-details',
      'fa',
      'material-icons',
      'btn-edit-folder'
    ]

    if (newProps.assets.cats.length > 0) {
      this.setCatsNFoldersName()
    }

    if (newProps.assets.campaigns.length > 0) {
      this.setCatsNFoldersName(newProps)
    }
    //remove selected asset if category has been changed.
    if (this.props.general === newProps.general && this.props.location !== newProps.location) {
      this.setState({ selectedAssetID: [], selectedFolderIDArray: [], selectedFolderID:null })
      //remove all selection from folders
      let allFolderEls = document.querySelectorAll('.asset-dir .file-details')
      for (let a = 0; a < allFolderEls.length; a++) {
        allFolderEls[a].classList.remove('active')
      }
      //remove selection from Assets
      let allFileEls = document.querySelectorAll('.asset-file')

      for (let a = 0; a < allFileEls.length; a++) {
        allFileEls[a].classList.remove('selectedAsset')
      }
    }

    if (newProps.general.ev !== null) {
      let validClassArr = []
      for (let i = 0; i < validFolderClicks.length; i++) {
        if (newProps.general.ev.classList.contains(validFolderClicks[i])) {
          validClassArr.push(validFolderClicks[i])
          break
        }
      }
      if (validClassArr.length == 0) {
        this.setState({ selectedFolderID: null })

        let allFolderEls = document.querySelectorAll('.asset-dir')

        for (let a = 0; a < allFolderEls.length; a++) {
          allFolderEls[a].classList.remove('active')
        }
      }

    }

      if (newProps.location.pathname !== this.props.location.pathname) {
        assetsscroll = false
        window.scrollTo(0, 0);
        //when click on any option from navbar then this will call to fetch data
        if (newLocation[2] === 'cat' || newLocation[2] === 'custom') {

            this.props.assetsActions.resetFolders()

            if (typeof newLocation[5] !== "undefined") {
              this.refs.searchInput.value=""
              this.props.assetsActions.fetchFolders(newLocation[5])
              this.props.assetsActions.fetchBreadCrums(newLocation[5])
            } else {
              this.refs.searchInput.value=""
              this.props.assetsActions.fetchFolders(newLocation[3])
              this.props.assetsActions.fetchBreadCrums(newLocation[3])
            }
            this.props.assetsActions.resetFiles()

            this.setState({ currPage: 1 })
            this.props.assetsActions.fetchFiles(
              newLocation[3],
              newLocation[5],
              this.state.maxItems,
              1,
              true
            )
          this.setState({ listingType: 'all', selectedFolderID: null })
        } else if (newLocation[2] === 'my') {
          this.props.assetsActions.resetFiles()
          this.setState({ currPage: 1 })
          this.fetchMyAssets(1, true)
        }
      }

    if (typeof newLocation[3] !== 'undefined' && typeof this.props.assets.archiveCat.identity !== "undefined" && newLocation[3] === this.props.assets.archiveCat.identity) {
      this.setState({ currentPage: 'archive' })
    } else {
      this.setState({ currentPage: 'asset' })
    }
    //this.hide_add_new_popup()
    if(newProps.assets.customfeedAsset !== this.props.assets.customfeedAsset && newProps.assets.customfeedAsset.length>0 || newProps.location.pathname !== this.props.location.pathname ){
    var feed_id=''
      if(newLocation[2]=='cat'){
      this
          .props
          .tagsActions
          .fetchUserTags(newLocation);
      }
      else {
        Object.keys(newProps.assets.customfeedAsset)?
          newProps.assets.customfeedAsset.map((data,index)=>{
            if(data.identity==newLocation[3]){
              feed_id=data.feed_id;
              this.props.tagsActions.fetchTrendingTags();
              this
                .props
                .tagsActions
                .fetchUserTags(newLocation,feed_id);
            }
            }) : ''
      }
    }
  }

  onAssetsScroll() {
    var me = this
    var location = utils
      .removeTrailingSlash(this.props.location.pathname)
      .split('/')
    var offsetLoader =  document.getElementById("assets-loader")? document.getElementById("assets-loader").offsetTop:null
    if ( offsetLoader !==null && (utils.getDocHeight() < utils.getScrollXY()[1] + window.innerHeight + offsetLoader ) && assetsscroll!==false) {
      // a small debouncer for better performance
      clearTimeout(me.state.scrollTimer)
      let scrollTimer = setTimeout(function () {
        if (me.props.assets.meta.pagination !== undefined && me.props.searchedInput==undefined) {
          var nextpage = me.props.assets.meta.pagination.current_page + 1
          me.setState({ currPage: nextpage })

          if (location[2] === 'my') {
            assetsscroll = false
            me.fetchMyAssets(nextpage, false)
          } else {
            assetsscroll = false
            me.props.assetsActions.fetchFiles(
              me.state.currCatID,
              me.state.currFolderID,
              me.state.maxItems,
              nextpage,
              false,
              searchValue
            )
            me.setState({ listingType: 'all' })
          }
        }
        else if(me.props.assets.meta.pagination == undefined || me.props.assetsMeta !==undefined)
        {
          var gSearch = me.props.searchedInput
          if(me.state.globalSearch==false)
          {
            assetsscroll = false
            var nextpage = me.props.assetsMeta? me.props.assetsMeta.pagination.current_page + 1 :''
            var gSearchData=me.props.assetsData
            me.setState({ globalSearch: true })
            me.setState({ currPage: nextpage })
            me.props.assetsActions.fetchFiles(
              me.state.currCatID,
              me.state.currFolderID,
              me.state.maxItems,
              nextpage,
              false,
              null,
              gSearch,
              gSearchData
            )
          }
          else
          {
            assetsscroll = false
            var nextpage = me.state.currPage + 1
            me.setState({ currPage: nextpage })
            me.props.assetsActions.fetchFiles(
              me.state.currCatID,
              me.state.currFolderID,
              me.state.maxItems,
              nextpage,
              false,
              null,
              gSearch
            )
          }
          me.setState({ listingType: 'all' })
        }
      }, 300)

      me.setState({ scrollTimer: scrollTimer })
    }
  }

  componentWillMount() {
    document.addEventListener('scroll', this.onAssetsScroll)

    $().then(jquery => {
      var $ = jquery.$

      ChunkJqueryDropdown().then(jqueryDropdown => { })
    })

    moment().then(moment => {
      this.setState({ moment: moment })
    })
  }

  componentWillUnmount() {
    document.removeEventListener('scroll', this.onAssetsScroll)
    /** call event */
    ReactDOM.findDOMNode(this).removeEventListener('click', this._handleCloseEvent);
  }

  componentWillUpdate(nextProps, nextState) {
  }


  checkLocation(path) {
    var defaultCat = 0
    var defaultFolder = ''

    var currCatID = path[3]
    var currFolderID = path[5]

    if (typeof currCatID !== 'undefined') {
      defaultCat = currCatID
    } else {
      if (
        Array.isArray(this.props.assets.cats) &&
        this.props.assets.cats.length > 0
      ) {
        defaultCat = this.props.assets.cats[0].identity
      }

    }

    if (typeof currFolderID !== 'undefined') {
      defaultFolder = currFolderID
    }


    this.setState({
      currCatID: defaultCat, // category or campaign
      currFolderID: defaultFolder // category ID or campaign ID
    })
  }

  fetchMyAssets(currPage, resetFiles = false) {
    this.props.assetsActions.fetchUsersFiles(
      currPage,
      resetFiles
    )
    this.setState({ listingType: 'user', selectedFolderID: null })
  }

  /*
  ** At this point, we have cats set in redux store.
  ** So, based on it OR based on catID in url, fetch files/folders
  */

  setCatsNFoldersName(newProps = null) {
    var me = this
    var cats = this.props.assets.cats;
    let campaigns = [];
    if (newProps !== null) {
      campaigns = newProps.assets.campaigns;
    }

    if (this.state.currCatID != null) {
      if (cats.length > 0) {
        var catName = cats.filter(function (cat) {
          return cat.identity == me.state.currCatID
        })

        var campaignName = campaigns.filter(function (campaign) {
          return campaign.identity == me.state.currCatID
        })
        if (catName.length > 0) {
          this.setState({
            currCatName: catName
          })
          if (typeof this.props.location.query.createasset !== "undefined" || typeof this.props.location.query.searchcreateasset !== "undefined") {
            this.openingAssetCreationFromCampaign(catName);
          }
        }
        else if (campaignName.length > 0) {
          if (typeof this.props.location.query.createasset !== "undefined" || typeof this.props.location.query.searchcreateasset !== "undefined") {
            this.openingAssetCreationFromCampaign(campaignName);
          }
          this.setState({
            currCatName: campaignName
          })
        }
        else {
          // check in campaigns now if couldn't match in categories
          // this was changed because of change in API

          // if(campaigns.length > 0) {
          //   var catName = campaigns.filter(function (campaign) {
          //     return campaign.identity == me.state.currCatID
          //   })

          //   if(catName.length > 0) {
          //     this.setState({
          //       currCatName: typeof catName[0].title !== "undefined" ? catName[0].title : null
          //     })
          //   }
          // }
        }

      }




    }

    var folders = this.props.assets.folders

    // if(this.state.currFolderID!=null) {

    if (folders.length > 0) {
      var folderName = folders.filter(function (folder) {
        return folder.identity == me.state.currFolderID
      })

      this.setState({
        currFolderName: folderName.length > 0 ? folderName[0].name : null
      })
    }

    // }
  }
/**
 * @author disha
 * @use to open popup from campaign
 * @param {*} catName
 */
  openingAssetCreationFromCampaign(catName = null) {
    if (typeof this.props.location.query.createasset !== "undefined" || typeof this.props.location.query.searchcreateasset !== "undefined") {
      this.handleAssetsCreation(catName,true);

    }
    else {
      document.body.classList.remove("overlay")
      this.setState({
        newAssetPopup: false
      })
    }
  }
 /**
   * when click on asset function will call
   * @param {*} self  this
   * @param {*} asset id
   */
assetClick(self, asset) {
  //this function is used for assets select  and  deselect
    let allFolderEls = document.querySelectorAll('.archive.asset-dir')
    var selectedElements = document.querySelectorAll(`.selectedAsset`);
    if(this.props.location.pathname == "/assets/my"){
        for(var selectAssetCnt=0; selectAssetCnt<selectedElements.length; selectAssetCnt++){
          // Remove selectedasset class from all file except the current one
          if (selectedElements[selectAssetCnt].id!=='asset-'+asset.identity){ 
            selectedElements[selectAssetCnt].classList.remove('selectedAsset')
          }
        }
    }
    else{
      for(let a = 0; a < allFolderEls.length; a++) {
        allFolderEls[a].classList.remove('active')
      }
    }
    this.setState({ selectedFolderID: null, selectedFolderIDArray: [] })
         /*End: remove file section*/
        if (document.querySelector(`#asset-${asset.identity}`).classList.contains('selectedAsset')) {
           //when clicked on asset and it is already selected then to remove selection
           let selectedAssethtml = document.querySelector(`#asset-${asset.identity}`)
            selectedAssethtml
              .classList
              .remove('selectedAsset');
      //remove this from state
        } else {
      //to select asset
      let selectedAssethtml = document.querySelector(`#asset-${asset.identity}`)
      selectedAssethtml
        .classList
        .add('selectedAsset');
      if(this.props.location.pathname == "/assets/my"){
        this.setState({
          selectedAssetID:[asset]
        })
      }
    }
   if(this.props.location.pathname != "/assets/my"){
    let timer = setTimeout(function () {
      if (!self.state.clickAssetPrevent) {
        //if clickassetPrevent is false means asset click single time
        self.onAssetClickAction(asset)
      }
      self.setState({
        clickAssetPrevent: false
      })
    }, 200)

    self.setState({
      clickAssetTimer: timer
    })
  }
}
onAssetClickAction(asset) {
    let isAdmin = this.props.profile.profile.isAdmin
    if (isAdmin) {
      if (this.state.selectedAssetID.indexOf(asset) != -1) {
        //remove selected asset form array
        var assetStateArray = this.state.selectedAssetID;
        var index = assetStateArray.indexOf(asset);
        assetStateArray.splice(index, 1);
        this.setState({ selectedAssetID: assetStateArray });
      } else {
        //add asset id in array
        this.setState({
          selectedAssetID: [...this.state.selectedAssetID, asset]
        })
      }
    }
  }
/**
 * double click on asset
 * @param {*} self
 * @param {*} asset
 */
  assetDoubleClick(self, asset) {
    this.removeSelectedFolder();
    clearTimeout(self.state.clickAssetTimer)
    self.setState({
      clickAssetPrevent: true,
    })
    self.assetDoubleClickAction(asset);
  }
  assetDoubleClickAction(asset) {
    this.props.s3Actions.uploadReset()
    this.setState({ assetDetailPopup: true, currentAssetDetail: asset })
    document.body.classList.add('overlay')
  }
  viewAssetClick(me) {
    this.setState({ assetDetailPopup: true, currentAssetDetail: this.state.selectedAssetID[0] })
    document.body.classList.add('overlay')
  }

  viewEditHeader(e){
    this.setState({openEditer : true , currentAssetDetail: this.state.selectedAssetID[0] })
  }
  archiveAssetClick(me) {
    if (typeof this.props.location !== 'undefined') {
      var newLocation = utils
        .removeTrailingSlash(this.props.location.pathname)
        .split('/')
    }
    if (this.state.selectedAssetID.length > 0) {
      if (typeof this.props.location !== 'undefined') {
        var newLocation = utils
          .removeTrailingSlash(this.props.location.pathname)
          .split('/')
      }

      this.props.assetsActions.archiveAsset(this.state.selectedAssetID, this.state.currCatID, this.state.currFolderID, this.state.maxItems)
    }


  }
  unArchiveAssetClick(selectedMoveFolder) {

    if (typeof this.props.location !== 'undefined') {
      var newLocation = utils
        .removeTrailingSlash(this.props.location.pathname)
        .split('/')
    }
    if (this.state.selectedAssetID.length > 0) {
      if (typeof this.props.location !== 'undefined') {
        var newLocation = utils
          .removeTrailingSlash(this.props.location.pathname)
          .split('/')
      }

      this.props.assetsActions.unArchiveAsset(this.state.selectedAssetID, this.state.currCatID, this.state.currFolderID, this.state.maxItems, selectedMoveFolder)
    }


  }
  moveAsset(selectedMoveFolder) {
    this.props.assetsActions.moveAsset(this.state.selectedAssetID, this.state.currCatID, this.state.currFolderID, this.state.maxItems, selectedMoveFolder)
  }
  moveFolder(selectedMoveFolder) {
    this.props.assetsActions.moveFolder(this.state.selectedFolderIDArray, this.state.currCatID, this.state.currFolderID, this.state.maxItems, selectedMoveFolder)
  }
  archiveFolderClick(me) {
    if (typeof this.props.location !== 'undefined') {
      var newLocation = utils
        .removeTrailingSlash(this.props.location.pathname)
        .split('/')
    }
    if (this.state.selectedFolderIDArray.length > 0) {
      if (typeof this.props.location !== 'undefined') {
        var newLocation = utils
          .removeTrailingSlash(this.props.location.pathname)
          .split('/')
      }
      this.props.assetsActions.archiveFolder(this.state.selectedFolderIDArray, this.state.currCatID, this.state.currFolderID, this.state.maxItems)
    }


    /*self.setState({ assetDetailPopup: true, currentAssetDetail: asset })
    document.body.classList.add('overlay')*/

  }
  unArchiveFolderClick(selectedMoveFolder) {
    if (typeof this.props.location !== 'undefined') {
      var newLocation = utils
        .removeTrailingSlash(this.props.location.pathname)
        .split('/')
    }

    if (this.state.selectedFolderIDArray.length > 0) {
      if (typeof this.props.location !== 'undefined') {
        var newLocation = utils
          .removeTrailingSlash(this.props.location.pathname)
          .split('/')
      }
      this.props.assetsActions.unArchiveFolder(this.state.selectedFolderIDArray, this.state.currCatID, this.state.currFolderID, this.state.maxItems, selectedMoveFolder)
    }


    /*self.setState({ assetDetailPopup: true, currentAssetDetail: asset })
    document.body.classList.add('overlay')*/

  }
  updateText(id, type, e) {
    this.setState({ overlay_flag: true })
    var isUserFound = false;
    if(type=="department"){
      this.props.departments.list.length>0?
      this.props.departments.list.map((DepartmentDetail,i)=>{
        if(DepartmentDetail.identity==id){
          selectedDepartmentData = DepartmentDetail;
        }
      }):''
    }
    else{
      this.props.usersList.length>0?
      this.props.usersList.map((userDetail,i)=>{
        if(userDetail.identity==id){
          selectedUserData = userDetail;
          isUserFound = true;
        }
      }):''
      if(isUserFound==false){
        selectedUserData ="No user found"
      }
    }
    const domNode = ReactDOM.findDOMNode(e.target);
    let coords = domNode.getBoundingClientRect();
    let coords_top = coords.top + pageYOffset;
    let popup_wrapper = document.getElementById("pop-up-tooltip-wrapper");
    let coords_left = 0,
      coords_area = 0,
      new_class_comb = '';
    let pop_holder = document.getElementById("pop-up-tooltip-holder");
    let popupparent = document.getElementById('department-popup');
    if (popupparent != null) {
      pop_holder.className = 'departmentpopup';
    } else {
      pop_holder.className = '';
    }
    if (screen.width >= 768) {
      coords_left = coords.left + pageXOffset + (coords.width / 2);
    }
    else {
      coords_left = coords.left + pageXOffset;
      coords_area = coords_left + 472;
      if (coords_area < screen.width) {
        new_class_comb = 'popup-left';
      }
      else {
        new_class_comb = 'popup-right';
      }

    }
    if (e.nativeEvent.screenY < 400) {
      new_class_comb += ' bottom';
    }
    else {
      new_class_comb += ' top';
    }
    popup_wrapper.className = new_class_comb;
    this.setState({
      popupTop: coords_top,
      popupLeft: coords_left,
      popupdisplay: 'block',
      tagType: type,
      tagTypeClass: (type === "department" ? 'departmentpopup' : '')
    });
  }
  closeedit() {
    this.setState({ openEditer: false })
    document.body.classList.remove('overlay')
  }
  renderEditerPopup(e, asset) {
    var me = this
    this.setState({openEditer: true, currentAssetDetail: asset })
  }
  closeeditpouprender(e) {
    var me = this
    me.setState({
      assetDetailPopup: false,
      openEditer: false,
      editorPopup:false
    })
  }
  rendereditheader(assets){
   return(
     <AssetDetail
        moment={this.state.moment}
        asset={assets}
        flag="1"
        aws={this.props.aws}
        searchedInput = {this.props.searchedInput}
     />
     )
  }
  renderAssetDetail() {
    document.body.classList.add('overlay')
    return (
      <SliderPopup className="asset-detail-popup">
        <button
          id='close-popup'
          className='btn-default'
          onClick={this.closeAssetDetailPopup.bind(this)}
        >
          <i className='material-icons'>clear</i>
        </button>
        <AssetDetail
          moment={this.state.moment}
          locationInfo = {this.props.location}
          asset={this.state.currentAssetDetail}
          updateText={this.updateText.bind(this)}
          currCatID={this.state.currCatID}
          currFolderID={this.state.currFolderID}
          aws={this.props.aws}
          assetAction={this.props.assetsActions}
          s3Actions={this.props.s3Actions}
          assets={this.props.assets}
          userTags = {this.props.userTags}
          postTags = {this.props.postTags}
          closeAssetDetailPopup = {this.closeAssetDetailPopup.bind(this)}
          openEditDetailFormPopup = {this.openEditDetailFormPopup.bind(this)}
          searchedInput = {this.props.searchedInput}
        />
      </SliderPopup>
    )
  }
  openEditDetailFormPopup(){

    this.setState({
        showEditDetailForm: true
    })
  }
  closeEditDetailFormPopup(){
    document.body.classList.remove('overlay')
    this.setState({
        showEditDetailForm: false,
        openEditer:false,
       })
    if (typeof this.props.location.query.openasset !== "undefined") {
      if(this.props.location.query.source === "dashboard"){
          let url = `/dashboard`
          browserHistory.push(url)
      }else{
          let url
          if(this.props.location.query.searchcreateasset !== undefined)
          url="/search/campaign"
          else
          url = `/campaigns?asset=${this.props.location.query.openasset}&campaignid=${this.props.location.query.campaignid}`
          browserHistory.push(url)
      }

    }
  }
  /**
  * This function will call when then
  **/
  /*closePopupOnSuccess(){
      console.log(this.props.assets.creation.created,'this.props.assets.creation.created')
    if (this.props.assets.creation.created) {
      console.log('comes in')
      this.props.actions.uploadReset();
      this.props.closeAssetCreatePopup();
      this.props.actions.resetCreateAsset(true);
      this.props.closeEditDetailFormPopup();
    }
  }*/
  renderEditDetailFormPopup() {
    document.body.classList.add('overlay');
    return (
      <SliderPopup className="create_asset_popup">
        <button
          id='close-popup'
          className='btn-default'
          onClick={this.closeEditDetailFormPopup.bind(this)}
        >
          <i className='material-icons'>clear</i>
        </button>
         <CreateAsset
          TotalMediaSize={this.fetchTotalMediaSize.bind(this)}
          searchedInput={this.props.searchedInput}
          catID={this.state.currCatID}
          campID={this.state.campID}
          currPage={this.state.currPage}
          maxItems={this.state.maxItems}
          folderID={this.state.currFolderID}
          catName={this.state.currCatName}
          jumpToEditor={true}
          closeAssetCreatePopup = {this.closeAssetCreatePopup.bind(this)}
          closeEditDetailFormPopup={this.closeEditDetailFormPopup.bind(this)}
          location={this.props.location}
          assetProps={this.props.assets}
         />
      </SliderPopup>
    )
  }
handleAssetsCreation(catName,skipReset = false) {
    if(!skipReset){
      this.props.s3Actions.uploadReset();
      this.props.assetsActions.resetCreateAsset();
    }

    if (this.state.newAssetPopup == false && catName !== null) {
      this.setState({
        newAssetPopup: true,
        campID: this.props.location.query.campaignid!==undefined?  this.props.location.query.campaignid: catName[0].campaign_identity
      })

      // asset created flag - change in redux to false
      this.props.assetsActions.creatingNewAsset()

      document.body.classList.add('overlay')
    }

  }

  handleAssetFolderCreation() {
    this.setState({ newAssetFolderPopup: true })
    document.body.classList.add('overlay')
  }

  handleAssetCategoryCreation() {
    this.setState({ newAssetCategoryPopup: true })

    document.body.classList.add('overlay')
  }

  renderMaxItemsSlider() {
    return (
      <ul>
        <li>
          <input
            onChange={this.onMaxItemsChange.bind(this)}
            type='range'
            min='20'
            max='100'
            step='10'
            value={this.state.maxItems}
          />
        </li>
      </ul>
    )
  }

  onFolderClick(me, folderID, e) {
    /*remove file section*/
    //111111
    let allFileEls = document.querySelectorAll('.asset-file')

    for (let a = 0; a < allFileEls.length; a++) {
      allFileEls[a].classList.remove('selectedAsset')
    }
    this.setState({ selectedAssetID: [] })
    /*End:remove file section*/
    let allFolderEls = document.querySelectorAll('.asset-dir .file-details')
    if (document.querySelector(`.file-details.${folderID}`).parentNode.classList.contains('active')) {
      //get selected folder's div's parent active class
            let selectedAssethtml = document.querySelector(`.file-details.${folderID}`).parentNode
            selectedAssethtml
              .classList
              .remove('active');
            //remove this from state
            var folderStateArray = this.state.selectedFolderIDArray;
            var index = folderStateArray.indexOf(folderID);
            folderStateArray.splice(index, 1);

            this.setState({ selectedFolderIDArray: folderStateArray, selectedFolderID: null });

          } else {
            if (this.state.selectedFolderID !== folderID) {
              utils.closest(e.target, 'asset-dir').classList.add('active');
            } else {
              utils.closest(e.target, 'asset-dir').classList.remove('active');
            }
            if (!this.state.selectedFolderIDArray.includes(folderID)) {
              this.setState({
                selectedFolderIDArray: [...this.state.selectedFolderIDArray, folderID]
              })
            }
      let timer = setTimeout(function () {
        if (!me.state.clickPrevent) {
          me.onFolderClickAction(folderID)
        }
        me.setState({
          clickPrevent: false
        })
      }, 200)

      me.setState({
        clickTimer: timer
      })
    }

  }

  onFolderClickAction(folderID) {
    let isAdmin = this.props.profile.profile.isAdmin
    if (isAdmin) {
      if (this.state.selectedFolderID == folderID) {
        this.setState({
          selectedFolderID: null
        })
      } else {
        this.setState({ selectedFolderID: folderID })
      }
    }
  }

  onFolderDoubleClick(me, options, e) {
    let cat = options.cat
    let folderID = options.folderID

    clearTimeout(me.state.clickTimer)
    me.setState({
      clickPrevent: true
    })
    me.onFolderDoubleClickAction(cat, folderID)
  }

  onArchivedFolderDoubleClick(me, options, e) {
    let folderID = options.folderID

    clearTimeout(me.state.clickTimer)
    me.setState({
      clickPrevent: true
    })

    me.onFolderDoubleClickAction(cat, folderID)
  }

  onFolderDoubleClickAction(cat, folderID) {
    browserHistory.push(`/assets/cat/${cat}/folder/${folderID}`)
  }

  onMaxItemsChange(e) {
    this.setState({
      maxItems: e.target.value,
      currPage: 1 // reset page and load the 1st page
    })
    this.props.assetsActions.fetchFiles(
      this.state.currCatID,
      this.state.currFolderID,
      this.state.maxItems,
      1,
      true,
      searchValue
    )
  }

  renderCatsInSidebar() {
    const cats = this.props.assets.cats
    var me = this

    return (
      <ul className='parent clearfix'>
        {cats.map(cat => {
          const selected = cat.identity == me.state.currCatID
          return (
            <li key={cat.identity}>
              <Link
                className={
                  selected && me.state.listingType == 'all' ? ` active` : ``
                }
                to={`/assets/cat/${cat.identity}`}
              >
                <span className='text'>{cat.name}</span>
              </Link>
            </li>
          )
        })}

      </ul>
    )
  }

  renderMyAssetsLinkInSidebar() {
    var me = this

    return (
      <ul className='parent clearfix'>
        <li>
          <Link
            className={me.state.listingType == 'user' ? ` active` : ``}
            to={`/assets/my`}
          >
            <span className='text'>Uploaded assets</span>
          </Link>
        </li>
      </ul>
    )
  }
  //archive category rendering for sidebar
  renderArchiveLinkInSidebar() {

    var me = this

    return (
      <ul className='parent clearfix'>
        {Object.keys(this.props.assets.archiveCat).length > 0 ?
          <li>
            <Link
              className={me.state.listingType == 'Live' ? ` active` : ``}
              to={`/assets/cat/` + this.props.assets.archiveCat.identity}
            >
              <span className='text'>{this.props.assets.archiveCat.name}</span>
            </Link>
          </li>
          : ''}
      </ul>
    )
  }

  renderCampaignsFoldersInSidebar() {
    let campaigns = []
    if (typeof this.props.assets.campaigns !== 'undefined') {
      campaigns = this.props.assets.campaigns
    }
    var me = this
    return (
      <ul className='parent  clearfix'>
        {campaigns.map(campaign => {
          const selected = campaign.identity == me.state.currCatID
          return (
            <li key={campaign.identity}>
              <Link
                className={
                  selected && me.state.listingType == 'Live' ? ` active` : ``
                }
                to={`/assets/cat/${campaign.identity}`}
              >
                <span className='text'>{campaign.name}</span>
              </Link>
            </li>
          )
        })}

      </ul>
    )
  }

  renderFolders() {

    var folders = this.props.assets.folders
    var cat = this.state.currCatID

    var me = this

    var path = utils
      .removeTrailingSlash(this.props.location.pathname)
      .split('/')
    var isAdmin = this.props.profile.profile.isAdmin

    if (folders.length > 0) {
      return (
        <div>
          <div className='archive-container clearfix'>
         {folders.map(function (folder) {
              return (
                <div>
                  {/* loader */}
                {/* <div class="archive asset-dir">
                    <a class="file-details file-details-loader-assets">
                        <span class="side">
                           <div className = "icon-loader loader-grey-line  loader-line-radius"> </div>
                        </span>
                        <p class="folder_name_container">
                            <span class=" foldername_assets">
                            <div className = "loader-grey-line  loader-line-radius loader-line-height"> </div>
                                <div className = "loader-grey-line  loader-line-space loader-line-radius loader-line-height"> </div>
                            </span>
                        </p>
                    </a>
                </div> */}
                  {/* end loader */}
                <div className='archive asset-dir' key={folder.identity}>
                  <a
                    onClick={me.onFolderClick.bind(this, me, folder.identity)}
                    onDoubleClick={me.onFolderDoubleClick.bind(this, me, {
                      cat: cat,
                      folderID: folder.identity
                    })}
                    className={`file-details ${folder.identity}`}
                  >
                    <span className='side'>
                      <i className='material-icons'>folder</i>
                    </span>
                    <p className="folder_name_container">
                    <span className=" foldername_assets"> {folder.name} </span>
                    <span className="asset-count">
                          {folder.no_of_assets} items
                     </span>
                      </p>

                  </a>
                </div>
              </div>
              )
            })}

          </div>
        </div>
      )
    }
 }
 editFolderClick() {
    let me = this
    var props=me.props
    var id=''
    let folderID=null
    let currentfoldername=''
    if( this.state.selectedFolderIDArray.length==1){
      //to pass selected folder id from  array to variable
      folderID=this.state.selectedFolderIDArray.toString()
    }
    if (folderID !== null) {
      notie.input(
        {
          text: String,
          minlength: '1', // default: ''
          placeholder: 'Give this folder a new name' ,// default: ''
        },
        'Edit Folder',
        'Update',
        'Cancel',
        function (val) {
          let objToSend = {
            name: val
          }
          fetch(
            Globals.API_ROOT_URL +
            `/category/${folderID}`,
            {
              method: 'PUT',
              headers: utils.setApiHeaders('put'),
              body: JSON.stringify(objToSend)
            }
          )
          .then(response => response.json())
            .then(json => {
              if (json.code == 200) {
                notie.alert('success', 'Folder name changed to ' + val, 5)

                let editedFolderEl = document.querySelectorAll(
                  '.asset-dir .' + folderID + ' .title'
                )
                if(editedFolderEl!==undefined && editedFolderEl!==null)
                {
                  editedFolderEl.innerHTML = val
                }
                if(props.location.pathname=="/assets" || props.location.pathname=="/assets/"){
                  id=props.assets.cats[0].identity
                }else{
                  id=props.params.catId
                }
                props.assetsActions.fetchFolders(id,searchValue)
              }else{
                if(typeof json.error!=='undefined'){
                  utils.handleSessionError(json)
                notie.alert('error',json.error.name[0],5)
              }
            }
            })
            .catch(err => {
              throw err
            })
        }
      )
    } else {
      notie.alert('warning', 'Please select a folder', 5)
    }
  }

  assetClickFromRowListing(asset) {
    this.setState({ assetDetailPopup: true, currentAssetDetail: asset })
    document.body.classList.add('overlay')
  }
  showEditorAssets(object_editor){
    this.setState({
      openEditer: true,
      imageData:object_editor
  });
  this.props.assetsActions.uploadedAssetInBuffer(object_editor)
  }
  renderUsersFiles() {
    assetsscroll = true
    var files = this.props.assets.files
    var path = utils
      .removeTrailingSlash(this.props.location.pathname)
      .split('/')

    return (
      <div id='feed' className='myassets clearfix'>
        <div className="uploaded-assets">
          <AssetsRowList
            moment={this.state.moment}
            assets={files}
            assetClickFromRowListing={this.assetClickFromRowListing.bind(this)}
            assetClick={asset => this.assetClick(this,asset)}
            assetDoubleClick={asset => this.assetDoubleClick(this, asset)}
            detailView={false}
            showeditor= {this.showEditorAssets.bind(this)}
            AssetLoading ={this.props.assets.assetLoading}
            currpage ={this.state.currPage}
            loadmore ={this.props.assets.loadmore}
          />

        </div>
        {this.state.loading!==true ? 
        <div className="uploaded-treanding-tag render-trendng">
          <div className="trending-xl-container">
            {/* trending tag */}
            <TrendingTagsPanel
              tagsData={this.props.tags.trendingTags}
              currentPath={path}
            />
            {/* trending tag */}
          </div>
        </div>
        :''}
      </div>
    )
  }

  renderLoader(displayFolder)
  {  
    return(
    <div className='container'>
        <div className='archive-container asset-file-container clearfix'>
        <div id="assets-loader">

         {(!this.props.assets.assetLoading && !this.props.assets.loadingFolder) ? null :  this.state.currPage==1 ?<PreLoader location="assets-loader" userAssets={displayFolder} listing ={this.props.tags.trendinglist}/> :''}
        </div>
    </div>
    </div>
    ) 
  }


  renderFiles()
  {
    assetsscroll = true
    var me = this
    var files

    if(this.props.location.pathname=="/search/assets"){

      if(this.props.location.state==null || this.props.location.state.seeAllClick==true){
        files = me.props.assets.files;
      }else{
        if(this.props.location.state.fromSuggestionClick==true){
          files = this.props.assetsData
        }
      }
    }else{
      files = me.props.assets.files
    }
    

    const { nofiles } = this.props.assets
    if (files.length > 0) {
      return (
        <div id="container">
          <div className='archive-container asset-file-container clearfix'>
            {files.map(function (file, index) {
              /* below is an example of how to use arrow functions to bind (not using .bind()) this value */

              return (
                <Asset
                onClick={asset => me.assetClick(me, file)}
                onDoubleClick={asset => me.assetDoubleClick(me, file)}
                editpopup_open={asset => me.renderEditerPopup(me,file)}
                key={index}
                name={file.title}
                extension={file.media_extension}
                identity={file.identity}
                media_type={file.media_type}
                thumbnail_url={file.thumbnail_url}
                media_url={file.media_url}
                approved={file.approved}
                category_id={file.category}
                width='320'
                height='240'
                detailView={false}
                media_size = {file.media_size}
                title={file.title}
                showeditor={me.showEditorAssets.bind(me)}
                // savetoDrive= {me.savetoDrive.bind(me,file)}
              />
           )
            })}
            {this.props.assets.loadmore ==true?
            <div id="assets-loader">
                 {Array(3).fill(1).map((el, i) => 
                  <div>
                <div class="vid file-image asset-file imgwrapper file-video">
                  <div class="inner_imgwrapper assetInnerWrapper">
                    <a class="type-image">
                        <span class="imageloader loaded thumbnail loader-assets-image">
                            <img src="/img/visiblyLoader.gif" />
                        </span>
                    </a>
                </div>
                <a class="file-details">
                    <span class="title">
                        <div className="loader-grey-line loader-line-height loader-line-radius loader-assets-title"> </div>
                    </span>
                </a>
              </div>
              </div>
              )}
              </div>:''
            }
          </div>
        </div>
      )
    } else {
      return <h4 />
    }
  }

  /**
   * @author disha
   * method will call when page is refreshed or canceled or uploading will canceled to release occupied size in db
   * @param {*} event 
   */
  onUnload(event) { 
    // the method that will be used for both add and remove event

    if (this.props.assets.isAssetUploadingStart == true) {
      //api will call when uploading is start and user want to cancel or refresh page
      var objToSend = {
        "file-size": this.state.selectedMediaSizeTotal,
        "action": "decrease"
      }
      this.props.generalActions.fetchS3Size(objToSend)
      this.state.newAssetPopup==true? event.returnValue = 'heyyy':'';
    }
  }
  
  

  closeAssetCreatePopup() {
  
    // delete the uploaded image if user interupt the uploading process
    if(this.props.aws.uploadedItems.length>0 && this.props.aws.uploadDone == false)
    {  
      Globals.AWS_CONFIG.albumName = localStorage.getItem("albumName");
      var s3Config = Globals.AWS_CONFIG;
      var s3Manager = new s3functions.S3Manager(s3Config);
      s3Manager.deleteObjectFromS3(this.props.aws.uploadedItems,s3Config)
    }

    this.setState({ newAssetPopup: false })
    this.props.s3Actions.uploadReset();
    this.props.assetsActions.resetCreateAsset(true);
    document.body.classList.remove('overlay')
    this.onUnload(this)
    if ( typeof this.props.location.query.searchcreateasset !== "undefined" || typeof this.props.location.query.createasset !== "undefined" || typeof this.props.location.query.openasset !== "undefined") {
      if(this.props.location.query.source === "dashboard"){
          let url = `/dashboard`
          browserHistory.push(url)
      }else{
          //if user comes from the campaign redirect him to camapign again
          let url
          if(this.props.location.query.searchcreateasset !== undefined)
          {
            url=`/search/campaign`
            browserHistory.push(url)
          }
          else
          {
            url = `/campaigns?asset=true&campaignid=${this.props.location.query.campaignid}`
            browserHistory.push(url)
          }
      }
    }
  }

  closeAssetDetailPopup(me,notRedirect = false) {
    this.setState({ assetDetailPopup: false, clickAssetPrevent: false })
    document.body.classList.remove('overlay')
    //if user will come from campaign redirect them to campaign again
    if (typeof this.props.location.query.openasset !== "undefined" && notRedirect === false) {
      if(this.props.location.query.source === "dashboard"){
          let url = `/dashboard`
          browserHistory.push(url)
      }else{
          let url = `/campaigns?asset=${this.props.location.query.openasset}&campaignid=${this.props.location.query.campaignid}`
          browserHistory.push(url)
      }
    }
  }

  closeFolderCreatePopup() {
    this.setState({ newAssetFolderPopup: false })
    document.body.classList.remove('overlay')
  }

  closeAssetCategoryCreatePopup() {
    this.setState({ newAssetCategoryPopup: false })
    document.body.classList.remove('overlay')
  }

  handleCreateFolderSubmit(values) {

    var parentId = this.state.currCatID;
    if (typeof this.state.currFolderID !== "undefined" && this.state.currFolderID !== null && this.state.currFolderID !== 0 && this.state.currFolderID !== "") {
      parentId = this.state.currFolderID;
    }
    let payload = {
      name: values.foldername,
      category_id: parentId,
    }
    this.postFolderCatCreationData(payload, 'Folder')
  }

  handleCreateCategorySubmit(values) {
    let payload = {
      name: values.catname
    }
    this.postFolderCatCreationData(payload, 'Category')
  }

  postFolderCatCreationData(data, type) {
    var me = this
    fetch(Globals.API_ROOT_URL + `/category`, {
      method: 'POST',
      headers: utils.setApiHeaders('post'),
      body: JSON.stringify(data)
    })
    .then(response => response.json())
      .then(json => {
        if (typeof json.identity !== 'undefined') {
          notie.alert('success', type + ' added successfully', 3)
          me.setState({
            newAssetFolderPopup: false,
            newAssetCategoryPopup: false
          })
          document.body.classList.remove('overlay')
          if (type == 'Folder') {
            json.no_of_assets = 0;
            me.props.assetsActions.appendNewFolder(json)
          } else if (type == 'Category') {
            me.props.assetsActions.appendNewCat(json)
            browserHistory.push(`/assets/cat/${json.identity}`)
          }
        }else{
          utils.handleSessionError(json)
          notie.alert('error',type +' '+  json.message, 3)
        }
      })
      .catch(err => {

        throw err
      })
  }
  /**
   * to fetch total media size which is came from createasset component while selecting media
   * @param {*} e 
   */
  fetchTotalMediaSize(e){
    this.setState({
      selectedMediaSizeTotal:e
    })
  }
  renderContentCreation() {
    return (
      <SliderPopup wide className='create_asset_popup'>

        <button
          id='close-popup'
          className='btn-default'
          onClick={this.closeAssetCreatePopup.bind(this)}
        >
          <i className='material-icons'>clear</i>
        </button>
        <CreateAsset
          profile={this.props.profile}
          onUnload={this.onUnload.bind(this)}
          TotalMediaSize={this.fetchTotalMediaSize.bind(this)}
          catID={this.state.currCatID}
          campID={this.state.campID}
          currPage={this.state.currPage}
          maxItems={this.state.maxItems}
          folderID={this.state.currFolderID}
          catName={this.state.currCatName}
          closeAssetCreatePopup = {this.closeAssetCreatePopup.bind(this)}
          closeEditDetailFormPopup={this.closeEditDetailFormPopup.bind(this)}
          location={this.props.location}
          assetProps={this.props.assets}
          cancelAssetUpload = {this.state.cancelAssetUpload}
        />
      </SliderPopup>
    )
  }

  renderContentFolderCreation() {
    return (
      <SliderPopup className="create_folder_popup">
        <button
          id='close-popup'
          className='btn-default'
          onClick={this.closeFolderCreatePopup.bind(this)}
        >
          <i className='material-icons'>clear</i>
        </button>
        <CreateFolder
          onSubmit={this.handleCreateFolderSubmit.bind(this)}
          catID={this.state.currCatID}
          folderID={this.state.currFolderID}
          catName={this.state.currCatName}
        />
      </SliderPopup>
    )
  }

  renderCategoryCreation() {
    return (
      <SliderPopup className="create_folder_popup">
        <button
          id='close-popup'
          className='btn-default'
          onClick={this.closeAssetCategoryCreatePopup.bind(this)}
        >
          <i className='material-icons'>clear</i>
        </button>
        <CreateCategory onSubmit={this.handleCreateCategorySubmit.bind(this)} />
      </SliderPopup>
    )
  }

  renderAssets() {
    var path = utils
      .removeTrailingSlash(this.props.location.pathname)
      .split('/')

      let roleName =JSON.parse(localStorage.getItem('roles')) !== null? JSON.parse(localStorage.getItem('roles'))[0].role_name:''
    let notShow = 0;
    if (roleName === "employee" || roleName === "moderator") {
      notShow = 1;
    }
    if(this.props.location.pathname=="/search/assets"){
      if(this.props.assetsData.length==0){
        return (
          <div>
              <div>
                <div className='clearfix parent_assets'>
                <div className='no-data-block'>
                  No asset found.
                  </div>
                  </div>
                {this.no_post_method()}
              </div>
          </div>
        )
      }
      else{
        return (
          <div className='clearfix parent_assets'>
            <div className="render-assets">
              {/* {loading ? this.renderLoading() : <div />} */}
              {this.renderFiles()}
            </div>
              { (!this.props.assets.assetLoading && !this.props.assets.loadingFolder) ==true?
                  <div className="render-trendng">
                  <div className="asset-trending-contaner">
                  {/*start trending tag desing static */}
                  <div className="trending-xl-container">
                  <TrendingTagsPanel
                  tagsData={this.props.tags.trendingTags}
                  currentPath={path}
                  />

                  </div>

                  </div>
                  </div>
                  :''}
          </div>
        )
      }
    }
    else{
        if (
          this.props.assets.files.length == 0 &&
          this.props.assets.folders.length == 0
        ) {
          return (
            <div>
              {/* {this.props.assets.loadingasset ? this.renderLoading() : null} */}
              {this.props.assets.loadingasset == false && this.props.assets.nofiles
                ?
                <div>
                  <div className='clearfix parent_assets'>
                  <div className='no-data-block'>
                    No asset found.
                    </div>
                    </div>
                  {this.no_post_method()}
                </div>
                : this.hide_add_new_popup()}
            </div>
          )
        }
        var loading = this.state.loading
        if (this.state.listingType == 'all') {
          return (
          <div className='clearfix parent_assets'>
              <div className="render-assets">
                {/* {loading ? this.renderLoading() : <div />} */}
                {this.renderFolders()}
                {this.renderFiles()}
              </div>
              { (!this.props.assets.assetLoading && !this.props.assets.loadingFolder) ==true?
              <div className="render-trendng">
                <div className="asset-trending-contaner">
                  {/*start trending tag desing static */}
                  <div className="trending-xl-container">
                    <TrendingTagsPanel
                      tagsData={this.props.tags.trendingTags}
                      currentPath={path}
                    />

                  </div>

                </div>
              </div>:''
              }
            </div>
          )
        } else {
          return (
            <div className='clearfix'>
              {loading && this.state.currPage==1? <PreLoader location="assets-loader" userAssets={true} listing ={this.props.tags.trendinglist}/> : <div />}

              {this.renderUsersFiles()}
            </div>
          )
        }
      }
    }
  renderBreadcrumbs() {
    var path = utils
      .removeTrailingSlash(this.props.location.pathname)
      .split('/')
    return (
      <ul className='clearfix'>

        {this.props.location.pathname !== "/search/assets" ?
          path[2] !== 'my' ?
            this.props.assets.breadCrum.length > 0 ?
              this.props.assets.breadCrum.map((breadCrum, index) => {
                let breadCrumLink = 'javascript:;';
                breadCrum.category !== 'campaign' ?
                  breadCrumLink = (breadCrum.type == 'folder') ? '/assets/cat/' + path[3] + '/folder/' + breadCrum.id : '/assets/cat/' + breadCrum.id
                  : '';
                return (
                  <li key={index}>
                    <Link to={breadCrumLink} title={breadCrum.category}>{breadCrum.category}</Link>
                  </li>
                )
              })
              :
              <li>
                <Link to='/assets/cat/rpPDp' title='General'><strong>General</strong></Link>
              </li>
            :
            <li>
              <Link to='/assets/my' title='My Asset'><strong>My Assets</strong></Link>
            </li>
        :""}
      </ul>
    )

  }
//call a action when link of asset has been click
  assetLinkClick(e) {
    this.props.assetsActions.assetLinkClick()
  }
  removeSelectedFolder(){
    let allFolderEls = document.querySelectorAll('.asset-dir');
    this.setState({ selectedAssetID: [], selectedFolderIDArray: [], selectedFolderID:null });
    for (let a = 0; a < allFolderEls.length; a++) {
      allFolderEls[a].classList.remove('active')
    }
    let allAssetEls = document.querySelectorAll('.asset-file.imgwrapper')
    for (let a = 0; a < allAssetEls.length; a++) {
       allAssetEls[a].classList.remove('selectedAsset')
    }
  }
  onSearchChange(e){
    var me = this;
    var textInput = document.getElementById('searchtext');
    // this condition is to identify that user has stopped typing and then call API for search 
          textInput.onkeyup = function (e) {
            // e.target.value.trim()!==''&&  commented bcz due to this when textbox has no value then asset not fetching again
            if(searchValue.trim()!==e.target.value.trim()){
            clearTimeout(searchTimeout);
            searchTimeout = setTimeout(function () {
             searchValue=textInput.value
                var path = utils
                  .removeTrailingSlash(me.props.location.pathname)
                  .split('/');
                if(path[5]){   
                me.props.assetsActions.fetchFolders(path[5],searchValue);}
                else
                {
                  me.props.assetsActions.fetchFolders(path[3],searchValue);
                }
                me.props.assetsActions.fetchFiles(path[3],path[5],me.state.maxItems,1,true,searchValue)
            }, 500);
            };
            }
            
  }
  render() 
  {
    var me = this
    var selectedcatname = ''
    var selectedcatid = ''
    var foldername = ''
    var folderid = ''
    var assetCreated = this.props.assets.files
    let roleName =JSON.parse(localStorage.getItem('roles')) !== null? JSON.parse(localStorage.getItem('roles'))[0].role_name:''
    let notShow = 0;
    var breadcrumbsLink = '';
    var breadcrumbsTitle = '';
    let NavBar;
    let title;

    if (roleName === "employee" || roleName === "moderator" || roleName==="guest") {
      notShow = 1;
    }
    let user = this.props.users,
      userDetails = user.userDetails,
      isFetching = user.isFetching,
      department = this.props.department

    if (typeof this.props.location !== 'undefined') {
      var newLocation = utils
        .removeTrailingSlash(this.props.location.pathname)
        .split('/')
    }
    if (newLocation[2] !== undefined || newLocation[1] == 'assets' || newLocation[5] !== undefined) {
      for (var i = 0; i <= this.props.assets.cats.length - 1; i++) {
        if (this.props.assets.cats[i].identity !== undefined) {
          selectedcatname = this.props.assets.cats[0].name
          selectedcatid = this.props.assets.cats[0].identity
          if (newLocation[3] == this.props.assets.cats[i].identity) {
            selectedcatname = this.props.assets.cats[i].name
            selectedcatid = this.props.assets.cats[i].identity
          } else if (this.state.currentPage == "archive") {
            selectedcatname = 'Archive'
            selectedcatid = typeof this.props.assets.archiveCat !== "undefined" ? this.props.assets.archiveCat.identity : ''
          }
        }
      }
      for (var i = 0; i <= this.props.assets.folders.length - 1; i++) {
        if (this.props.assets.folders[i].identity !== undefined) {
          if (newLocation[5] == this.props.assets.folders[i].identity) {
            foldername = this.props.assets.folders[i].name
            folderid = this.props.assets.folders[i].identity
          }
        }

        //this.props.assets.cats[i].identity==newLocation[3]
      }
      //  this.props.assets.map(assetcat => {
      //     newLocation[3]==assetcat.identity ? <a href="#" title="Home">{assetcat.name}</a>:(newLocation[2]==undefined?'genral':'')
      //  })

    }
    if(this.props.location.pathname=="/search/assets"){
      NavBar=SearchNav
      title="search"
    }
    else{
      NavBar=AssetsNav
      title="Assets"
    }
      var displayFolder = false
    if(newLocation[2]!==undefined&&newLocation[2]=="my"){
      displayFolder = true
    }
    if(this.props.location.state!==null){
      if(this.props.location.state.fromCampaign=='true'){
        displayFolder = true
      }
    }
  return (
    <div>
    <GuestUserRestrictionPopup />
      <section id='assets' className='assets'>


        {this.state.newAssetPopup ? this.renderContentCreation() : <div />}
        {this.state.newAssetFolderPopup
          ? this.renderContentFolderCreation()
          : <div />}
        {this.state.newAssetCategoryPopup
          ? this.renderCategoryCreation()
          : <div />}
         {this.state.openEditer == true ?  this.renderEditDetailFormPopup() : ''}

        {this.state.assetDetailPopup ? this.renderAssetDetail() : <div />}
        {/* this is user tag popup */}
        {/* <div id="tag-popup-overlay" onClick={this.closepopup.bind()}></div> */}

         {/* this is the trending tag responsive popup */}
        {this.state.newTrendingPostPopup ? this.trendingTagPopup() : ''}
        {this.state.showEditDetailForm ? this.renderEditDetailFormPopup() : this.props.assets.creation.created ? document.body.classList.remove('overlay') : ''}
        {/* this is the trending tag responsive popup */}
        {this.state.newTrendingPostPopup ? this.trendingTagPopup() : ''}
        {/* <div  className="trending-tag-feed-responsive">
             <span id="trending-tag-toggle" className="hide trending-button-collpase" onClick={this.openPopUpTrending.bind(this)}><i class="material-icons add-new-icon">&#xE8E5;</i></span>
        </div>     */}
        <div className="trending-tag-feed-responsive">
          <a id="trending-tag-toggle" className="trending-button-collpase" onClick={this.openPopUpTrending.bind(this)}>><i class="material-icons add-new-icon">&#xE8E5;</i></a>
        </div>
        {/* end trending tag */}

        <div id="pop-up-tooltip-holder">
          <div id="pop-up-tooltip-wrapper" className={this.state.tagTypeClass}>
            <div className="pop-up-tooltip showtotop" style={{ top: this.state.popupTop, left: this.state.popupLeft, display: this.state.popupdisplay }}>
              <div className="abc">
                <span className='popup-arrow'></span>

                {/* <div className='preloader-wrap'>
            <PreLoader />
          </div> */}
          
                <TagPopup
                  userDetails={selectedUserData}
                  isFetching={isFetching}
                  isDept={this.state.tagType}
                  department={selectedDepartmentData}
                />
              </div>
            </div>
          </div>
        </div>
        {!this.props.location.pathname.includes("search")?
          <Header
            title={title}
            nav={NavBar}
            popup_text="Click here to add new asset."
            //add_new_subpopup={this.props.assets.nofiles}
            add_new_subpopup="ASSETS"
            header_location={this.state.currCatName}
            location={this.props.location}
            header_function1={this.handleAssetsCreation.bind(this)}
            header_function2={this.handleAssetFolderCreation.bind(this)}
            header_function3={this.handleAssetCategoryCreation.bind(this)}
            header_notShow={notShow}
            header_cats={this.props.assets.cats}
            header_customFeeds={this.props.assets.customfeedAsset}
            header_currCatID={this.state.currCatID}
            header_listingType={this.state.listingType}
            header_asset_campaigns={this.props.assets.campaigns}
            selectedFolderIDArray={this.state.selectedFolderIDArray}
            editFolderClick={this.editFolderClick.bind(this)}
            selectedAssetID={this.state.selectedAssetID}
            notShow={notShow}
            viewAssetClick={this.viewAssetClick.bind(this)}
            currPage={this.state.currentPage}
            archiveAssetClick={this.archiveAssetClick.bind(this)}
            unArchiveAssetClick={this.unArchiveAssetClick.bind(this)}
            archiveFolderClick={this.archiveFolderClick.bind(this)}
            unArchiveFolderClick={this.unArchiveFolderClick.bind(this)}
            archiveCat={typeof this.props.assets.archiveCat !== "undefined" ? this.props.assets.archiveCat.identity : ''}
            Header_onclick={this.hide_add_new_popup.bind(this)}
            moveAsset={this.moveAsset.bind(this)}
            moveFolder={this.moveFolder.bind(this)}
            assetLinkClick={this.assetLinkClick.bind(this)}
            trending_btn_id='trending-tag-toggle'
            //editpopup = {this.rendereditheader.bind(this)}
            showeditor = {this.showEditorAssets.bind(this)}
            breadCrumLength = {this.props.assets.breadCrum.length}
            breadCrum={this.props.assets.breadCrum}
            path={this.props.location.pathname}
            searchInput={this.props.searchedInput}
          />:''}
       <div className='main-container'>
          <div id='content'>
            <div className='page'>
              <div className='full-container'>
                <div className='asset-container'>
                <div className='searchbox-breacrumb-wrapper clearfix'>
                {
                  this.props.location.pathname !== "/search/assets"?
                  <div className='assetSearchbox'>
                     <input ref="searchInput" id="searchtext" type="text" placeholder="Search in assets..." onChange={this.onSearchChange.bind(this)} />
                    <button className="btn btn-theme searchAssetBtn" value="Search" onClick={this.onSearchChange.bind(this)}><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path fill="none" d="M0 0h24v24H0V0z"/><path d="M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z"/></svg></button>
                  </div>:""
                } 
                  <div className="breadcrunb-wrapper">
                    {this.renderBreadcrumbs()}
                  </div>
                </div>
                {(!this.props.assets.assetLoading && !this.props.assets.loadingFolder) ? null :  me.state.currPage==1 ?<PreLoader location="assets-loader" userAssets={displayFolder} listing ={this.props.tags.trendinglist}/> :''}
                {/* {this.renderLoader(displayFolder)} */}
                  <span onClick={this.removeSelectedFolder.bind(this)}  className='removeSelectedFolder hide'>Deselect</span>
                  <div className='esc-2' id="assets_walkthrough">
                    {this.renderAssets()}
                  </div>

                </div>
              </div>
            </div>
          </div>
        </div>

      </section>

    </div>
    )
  }
  openPopUpTrending(e) {
    this.setState({
      newTrendingPostPopup: true
    });
    document.body.classList.add('overlay_responsive_trending')
  }

  closeNav(popup) {
    this.setState({
      newTrendingPostPopup: false
    });
    document.body.classList.remove('overlay_responsive_trending')
  }
  trendingTagPopup() {
    var path = utils
      .removeTrailingSlash(this.props.location.pathname)
      .split('/')
    return (
      <ReactCSSTransitionGroup
        transitionName='fade'
        transitionAppear
        transitionEnterTimeout={500}
        transitionLeaveTimeout={500}
        transitionAppearTimeout={1000}
      >
        <div className="trending-button-container">
          <div id="mySidenav" class="sidenav">
             <div className="responsive-trending">
               <TrendingTagsPanel
                tagsData={this.props.tags.trendingTags}
                currentPath={path}
              />
            </div>
            <button
              id='close-popup'
              className='btn-default closebtn'
              onClick={this.closeNav.bind(this)}>
              <i className='material-icons'>clear</i>
             </button>
          </div>

        </div>
      </ReactCSSTransitionGroup>
    )
  }
  no_post_method() {
    var add_new_popup = document.getElementById('add-new-popup');
    if(add_new_popup)
    {
      add_new_popup.classList.add('show');
    }
  }
  hide_add_new_popup() {
    var add_new_popup = document.getElementById('add-new-popup');
    if(add_new_popup)
    {
      add_new_popup.classList.remove('show');
    }
  }
}

function mapStateToProps(state) {
  return {
    assets: state.assets,
    profile: state.profile,
    general: state.general,
    users: state.users,
    tags: state.tags,
    aws: state.aws,
    userTags: state.tags.userTags,
    postTags: state.tags.postTags,
    departments:state.departments,
    usersList : state.usersList.userList.data
  }
}
function mapDispatchToProps(dispatch) {
  return {
    generalActions: bindActionCreators(generalActions, dispatch),
    assetsActions: bindActionCreators(assetsActions, dispatch),
    userActions: bindActionCreators(userActions, dispatch),
    tagsActions: bindActionCreators(tagsActions, dispatch),
    s3Actions: bindActionCreators(s3Actions, dispatch),
    searchActions : bindActionCreators (searchActions,dispatch),
  }
}

module.exports = connect(mapStateToProps, mapDispatchToProps)(Assets)
