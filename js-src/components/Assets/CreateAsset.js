import { connect } from 'react-redux'
import { browserHistory } from 'react-router'
import { bindActionCreators } from 'redux'
import CreateAssetForm from './CreateAssetForm'
import CreateAssetFormDropzone from './CreateAssetFormDropzone'
import CreateAssetDetailForm from './CreateAssetDetailForm'
import EditAssetUploadingSteps from './EditAssetUploadingSteps'
import * as generalActions from '../../actions/generalActions'
import PhotoEditor from './PhotoEditor'
import notie from 'notie/dist/notie.js'
import { SubmissionError } from 'redux-form'
import PreLoader from '../PreLoader'
import * as s3functions from '../../utils/s3'
import * as s3Actions from '../../actions/s3/s3Actions'
import PopupWrapper from '../PopupWrapper'
import * as utils from '../../utils/utils'
import { cloneArr } from '../../utils/utils'
import Globals from '../../Globals'
import SliderPopup from '../SliderPopup'
import * as assetsActions from '../../actions/assets/assetsActions'
import Script from 'react-load-script';
import ImageLoader from 'react-imageloader'
import PreLoaderMaterial from '../PreLoaderMaterial'
import PhotoEditorSDK from '../Chunks/ChunkPhotoEditor'
const Line = require('rc-progress').Line;

// var data=[]
var DriveFileObj=''
var countOfForm = 0
var collectedFormData = []
var show_class
var subFiledata=[]
var fieldsType = [];
var feildName = [];
var flag = 0;
var fieldData = {}
var countFile = 0;
var selectedMediaSizeTotal=0  //for storage
var freeStorage=0
var showDriveUpload=false
var DriveAssetsData = null
var urlThumb ='';
var assetTitle;
class CreateAsset extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      tags: [], // Although, folders are stored in redux stazte, we need to convert the
      userTag: [],
      countOfForm: 0,
      openEditor: false,
      imageData: [],
      indexCount: 0,
      directEditor: false,
      accepted: [],
      rejected: [],
      allfiledata:[],
      videoThumbnailImage:[],
      mimetype:[],
			clientId:Globals.google_client_id,
      appId: Globals.google_app_id,
      developerKey:Globals.google_developer_key,
      scope:Globals.google_app_scope,
			pickerApiLoaded : false,
      oauthToken:'',
      renderDriveUI:false,
      DriveData:'',
      isUploadButtonClicked:false,
      isUnloadMethodCalled:false,
      currentAssetTab:"Dropzone",
      photoEditor:null,
      cancelUploadAsset:false,
      uploadingFinished:true   
      // redux-state folders object to something which is compatible with Select <options>.
     
    }
    this.onUnload = this.onUnload.bind(this);
  }

  // list of video types to be added when thumbnail is ready 
  // 'video/mp4','video/mpeg','application/vnd.google-apps.video'
  componentWillMount () {
    if (
      typeof this.props.jumpToEditor !== 'undefined' &&
      this.props.jumpToEditor == true
    ) {
      this.setState({
        directEditor: true
      })
    }
    PhotoEditorSDK().then(photoEditor => {
      this.setState({ photoEditor: photoEditor})
    })  
  }

  componentWillUnmount() {
    
    var me = this
    if(this.state.uploadingFinished == false)
    {
      // notie.confirm(
      //   `Do you want to continue asset uploading?`,
      //   'Yes',
      //   'No',
      //   function () {
      //     me.setState({
      //       cancelUploadAsset: false
      //       })
           
      //   },
      //   function () { 
      //     me.setState({
      //       cancelUploadAsset: true
      //       })
      //   } )
    }
}

  componentDidMount () {
    var mimeType = utils.mimetype();
    this.props.actions.fetchInitialUserFiles();
    this.setState({
      mimetype: mimeType
      })
    this.props.generalActions.fetchS3Size()
    this.catsToOptions()
    s3functions.loadAWSsdk()
    // this.props.actions.uploadReset()
    if (
      typeof this.props.jumpToEditor !== 'undefined' &&
      this.props.jumpToEditor == true
    ) {
      this.openEditor(this.props.assets.uploadedAssetBuffer.files[0], 0)
    }
  }
  componentWillReceiveProps (newProps) {
  if(newProps.assets.driveAssetsURL!==this.props.assets.driveAssetsURL && newProps.assets.driveAssetsURL!==null){
    var data= this.state.storeDriveData
    var campaignId = ''
    if (
      typeof newProps.location !== 'undefined' &&
      typeof newProps.location.query.campaignid !== 'undefined'
    ) {
      campaignId = newProps.location.query.campaignid
    }
    DriveAssetsData = newProps.assets.driveAssetsURL
    data[0]['fileUrl'] = DriveAssetsData
    data[0]['thumbUrl'] = DriveAssetsData
    data[0]['mime_type'] = newProps.assets.driveFileData.mime_type
    data[0]['extension'] = newProps.assets.driveFileData.extension
    var isFirstAsset = newProps.assets.userFiles.length==0?true:false

    this.props.actions.multiAssetCreate(data, {
          maxItems: this.props.maxItems,
          currPage: this.props.currPage,
          campaignId 
        }, null, false,true,isFirstAsset)
      
      this.props.actions.uploadedItem(data)
      DriveFileObj=''
  }
  if(newProps.assets.creation!== this.props.assets.creation){
    showDriveUpload=false
    this.setState({
      DriveData:''
    })
  }
    if (newProps.assets.bufferUpdated !== this.props.assets.bufferUpdated) {
      if (newProps.assets.bufferUpdated == true) {
        this.setState({ openEditor: false, imageData: [], indexCount: 0 })
      }
    }
    if(newProps.general.s3SizeData!=='' && newProps.general.s3SizeData.storage.freeStorage !=='' && newProps.general.isFetchS3Size ==true && this.state.isUploadButtonClicked==true && newProps.general.s3SizeData.access=='allowed' ){
      //if free storage has data and asset is sent to upload
        this.setState({
          isUploadButtonClicked:false
        })
        if(this.state.DriveData!==''){
          this.driveAssetSubmit(this.state.storeFields);
        }
        else{
          this.uploadAssetOnS3(this.state.storeFields,newProps.general.s3SizeData)
        }
    }else if(this.state.isUploadButtonClicked==true && newProps.general.isFetchS3Size ==true && newProps.general.s3SizeData.access=='rejected' ){
      //if button is clicked to upload asset but there is no space to upload asset 
      DriveFileObj = ''
      this.setState({
        isUploadButtonClicked:false,
        DriveData:'',
        renderDriveUI:false
      })
      notie.alert('error', `${newProps.profile.profile.first_name + newProps.profile.profile.last_name }, you have consumed your storage limit. Please ask your admin to upgrade your package to continue.`, 5)
      this.props.actions.resetCreateAsset(true, true)
    }
  }
  /**
   * @author kinjal
   * this method not check the mime type diretly uploaded
   * @param {*} file //file detail came from loop
   * @param {*} totalLength //field length
   * @param {*} fields //fields (all files)
   */
  withOutCheckMimetypeAsset(file,totalLength,fields){
    var noPostObj={
      mimetype:file.type
    }
    Object.assign(file,noPostObj)
    this.props.actions.uploadedAssetInBuffer(fields)
  }

 /**
   * @author Tejal kukadiya
   * this section is for google drive API integrate
   */
  selectFromDrive(){
    gapi.load('auth', {'callback': this.onAuthApiLoad.bind(this)});
		// gapi.load('picker', {'callback': this.onPickerApiLoad.bind(this)});
  }
  
  // when page loaded successfully loaded it initializes the google file picker API 

  handleScriptLoad() {
    // gapi.savetodrive.go('container');
    gapi.load('client:auth2', this.initClient.bind(this));
  }

  initClient() { 
		let data={
			apiKey: this.state.developerKey,
			discoveryDocs: ["https://www.googleapis.com/discovery/v1/apis/drive/v3/rest"],
			clientId:this.state.clientId,
      scope:this.state.scope
		   };
		   let req = gapi.client.init(data);
  }

  // savetoDrive (file){

	// 	  gapi.savetodrive.render('savetodriveButton', {
	// 		src: file.media_url,
	// 		filename: file.title,
	// 		sitename: 'Tejal kukadiya'
	// 	  });
  // }

  onAuthApiLoad(){

    window.gapi.auth.authorize(
      {
      'client_id': this.state.clientId,
      'scope': this.state.scope,
      'immediate': false
      },
      this.handleAuthResult.bind(this));
  }

  handleAuthResult(authResult){
      if (authResult && !authResult.error) {
        this.setState({oauthToken: authResult.access_token});
        gapi.load('picker', {'callback': this.onPickerApiLoad.bind(this)});
          // this.createPicker();
      }
  }


  onPickerApiLoad(){
    this.setState({
      pickerApiLoaded : true
    });
    this.createPicker();
  }
// open picker when clicked on drive button from menu
  createPicker(){
    this.driveConnected();
    if (this.state.pickerApiLoaded && this.state.oauthToken) {
        var view = new google.picker.DocsView(google.picker.ViewId.DOCS);
        view.setMimeTypes(this.state.mimetype);
        var picker = new google.picker.PickerBuilder()
          .enableFeature(google.picker.Feature.NAV_HIDDEN)
          // .enableFeature(google.picker.Feature.MULTISELECT_ENABLED)
          .setAppId(this.state.appId)
          .setOAuthToken(this.state.oauthToken)
          .addView(view.setParent('root').setIncludeFolders(true))
          // .addView(new google.picker.DocsView().setParent('root').setIncludeFolders(true))
          // .addView(view)
          // .addView(new google.picker.DocsUploadView()) this can be used to upload particular aasets to drive form local machine
          .setDeveloperKey(this.state.developerKey)
          .setCallback(this.pickerCallback.bind(this))
          .build();
        picker.setVisible(true);
      }
  }

  pickerCallback (data) {
    if (data.action == google.picker.Action.PICKED) {
      this.props.actions.uploadReset()
      this.props.actions.resetCreateAsset()
      this.setState({
        DriveData: data,
        renderDriveUI :true,
      });
    }
  }
// to check connection status of google drive
  driveConnected(){
    if(localStorage.getItem('isDriveConnected')==null){
      var DriveuserId =this.props.profile.profile.identity
       var obj ={[DriveuserId] : true}
      localStorage.setItem(`isDriveConnected`,JSON.stringify(obj));
    }else{
      var obj1 = JSON.parse( localStorage.getItem('isDriveConnected'))
      var DriveuserId =this.props.profile.profile.identity
      var obj ={[DriveuserId] : true}
       Object.assign(obj1,obj)
      localStorage.setItem(`isDriveConnected`,JSON.stringify(obj1));
    }
  }
// render selected drive asset preview 
  renderDrive(){
    const catID = this.props.catID 
    const campID = this.props.campID
    const folderID = this.props.folderID
    var catName = ''
    if (catID != null && this.props.catName !== null) {
      catName = this.props.catName[0]? this.props.catName[0].name:''
    }
    if(this.state.DriveData!==''  && showDriveUpload!==true){
      var DriveAsset = this.state.DriveData.docs[0];

      var byteLength = DriveAsset.sizeBytes ;
          var buffer = new ArrayBuffer(byteLength);
          var blob = new Blob([buffer]);

      let newFile = new File([buffer], DriveAsset.name, { type: DriveAsset.mimeType })
      var addMimeObj={
        mimetype:DriveAsset.mimeType
      }
      Object.assign(newFile,addMimeObj)
      var flag
      var fileUrl = DriveAsset.url
      var result = DriveAsset.name.split('.');      
      var final = result.pop();             
      var previous = result.join('.');   
      var  fileName = previous!=='' ? previous : DriveAsset.name
      var fileId= DriveAsset.id
      var mimetypecheck = DriveAsset.mimeType.split("/");
      var split1 = fileUrl.split("https://drive.google.com/");
      if( mimetypecheck[0]==='image' ){
        flag=1;
        if(split1[1].charAt(0)==='a'){
          urlThumb = fileUrl.split("file/d")[0]+"uc?id="+fileId
        }else{
          urlThumb = 'https://drive.google.com/uc?id='+fileId
        }
      }
      else if( mimetypecheck[0]==='video'){
        flag=2
        if(split1[1].charAt(0)==='a'){
          urlThumb = fileUrl.split("file/d")[0]+"thumbnail?id="+fileId
        }else{
          urlThumb =  `https://drive.google.com/thumbnail?id=`+fileId
        }
      }
      else{
        flag=3
      }
      newFile.preview = urlThumb
      newFile.isFileSupported =true;
      newFile.newFileHaxCode="89504E47"

      DriveFileObj =  newFile
    var index=0;
    let initialValues = {
      initialValues: {
        title:fileName,
        fileData:DriveAsset,
        catID: catID,
        campID: campID,
        filesize: DriveAsset.sizeBytes,
        folderID: folderID,
        numberOfUploaded: 1
      }
    }

    return (
      <CreateAssetDetailForm
        ref={`createAssetForm${index}`}
        key={index}
        form={`CreateAsset-${index}`}
        onSubmit={this.handleAssetDetailFormSubmit.bind(this)}
        isFileSupported={true}
        tags={this.state.tags}
        catID={catID}
        campID={campID}
        catName={catName}
        filesize={DriveAsset.sizeBytes}
        folderId={folderID}
        {...initialValues}
        fileUploaded={
          newFile
        }
        indexCount={index}
        numberOfUploaded={1}
        openEditor={this.openEditor.bind(this)}
        enableReinitialize
        DriveFile = {DriveAsset}
      />
    )
      }
  }

  AssetDriveFormSubmit(){
    countOfForm = 0
    collectedFormData = []
    this.refs[`createAssetForm${0}`].submit()
  }

  handleDriveSubmit(data){
    this.setState({
      storeDriveData:data
    })
    showDriveUpload = true;
    assetTitle = data[0].title;
    var fileSize = 52428800;
    var driveFile = this.state.DriveData.docs[0];

    var result = driveFile.name.split('.');      
    var final = result.pop();             
    var previous = result.join('.');   
    var  fileName = previous!=='' ? previous : driveFile.name
    
    if(driveFile.sizeBytes>fileSize){
      notie.alert('error', 'File size is bigger than 50MB', 5);
      return;
    }
    else{
      selectedMediaSizeTotal = 0;
      selectedMediaSizeTotal=driveFile.sizeBytes;
        Globals.AWS_CONFIG.albumName = localStorage.getItem('albumName')
        var s3Config = Globals.AWS_CONFIG
        var s3Manager = new s3functions.S3Manager(s3Config)
        var s3BucketURL =   s3Manager.albumName 
        var mimeType,extension;
        if(driveFile.mimeType=="application/msword"){
          mimeType = "application/doc"
          extension = "doc"
         }
         else if(driveFile.mimeType=='application/vnd.ms-powerpoint'){
          mimeType = "application/doc"
          extension = "ppt"
         }
         else if(driveFile.mimeType== "application/vnd.ms-excel"){
            mimeType = "application/doc"
            extension = "xls"
         }
         else{
          mimeType = driveFile.mimeType
          extension = driveFile.mimeType.split("/")[1]
         }
         var arrData = {
           'command':'handle-google-drive-file',
           'file_id': driveFile.id,
           'file_name' : fileName,
           'mime_type':  mimeType,
           'google_access_token' : this.state.oauthToken,
           'extension': extension,
           's3BucketURL' : s3BucketURL
         }
        this.checkStorageSpace(arrData)     
    }
  }

driveAssetSubmit(arrData){
  this.props.actions.uploadDriveAsset(arrData);
}

renderDriveLoading(){
  var strokeColor = "#256eff";
  var trailColor = "#ebeef0";
  var fileType=''
  var src = urlThumb
  return(
  showDriveUpload==true?
          <div id='edit-asset'>
          <label>
              Please wait while we are uploading your media.
          </label>

          <div class="edit-asset-preview-wrapper">
                    <ul class="dropzone-preview edit-asset-preview">
                                  <li className='item-0' rel='0' >

                                {
                                 this.state.DriveData !==''?
                                    this.state.DriveData.docs[0].mimeType.includes("image")?
                                       <ImageLoader
                                          src={src}
                                          preloader={PreLoaderMaterial}
                                          className = "thumbnail">
                                        <div className="image-load-failed">
                                            <i class="material-icons no-img-found-icon">&#xE001;</i>
                                        </div>
                                      </ImageLoader>
                                    : this.state.DriveData.docs[0].mimeType.includes("video")?
                                    <div className='preview-wrapper'>
                                    <div className = "video-wrapper" id={`dropPreviewIconInner}`}>
                                    <img src={src}/>
                                      <div className="video">
                                        <i className="material-icons asset-title-icon">videocam</i>
                                      </div>
                                     </div>
                                    </div>
                                      : 
                                      <div className='preview-wrapper'>
                                      <div className = "video-wrapper">
                                        <div className="video">
                                        <i class="material-icons asset-title-icon">&#xE24D;</i>
                                        </div>
                                       </div>
                                      </div>
                                 :''
                                }
                                  <div className="uploaded-file-details">
                                      <div className="filename" title="">{assetTitle}</div>
                                      <div className = "loader-grey-line loader-line-space loader-line-height loader-line-radius google-drive-loader"> </div>
                                      </div>
                                  </li>
                      
                      
                    </ul>
                    </div>
         </div>:''
  )
}
  /**
   * @author disha
   * to check mime type of file and restrict that file if file type is invalid
   * @param {*} file //file detail came from loop
   * @param {*} totalLength //field length
   * @param {*} fields //fields (all files)
   */
  checkFileMimetype(file,totalLength,fields){
    var me=this
    let fileType=''
    let fileName=''
    let mimetype=''
    let newFileType=''
    let newFileName=''
    let fileinnerdetail={}
    let flag=0
    let invalidType=0
    let newFileHaxCode = ''

    //to read file data use FileReader()
    const filereader = new FileReader()

      const blob = file.slice(0, 4)
      filereader.readAsArrayBuffer(blob)

        filereader.onloadend = function (evt, me) {
          if (evt.target.readyState === FileReader.DONE) {
            var u = new Uint8Array(this.result),
            a = new Array(u.length),
            i = u.length;
            while (i--) // map to hex
                  a[i] = (u[i] < 16 ? '0' : '') + u[i].toString(16);
              u = null; // free memory
            const hex=a.toString().replace (/,/g, "").toUpperCase() //convert into hex code
            fileinnerdetail={
              'preview':file.preview,
              'hexcode':hex,
              'name':file.name
            }
            //push fileobject in array with name and hexcode ,preview
            //countfile is for counting the no of assets of type either video/mp4 or text/CSV while uploading multiple assets
            subFiledata.push(fileinnerdetail)
            subFiledata.length + countFile == totalLength?renderData():'' //call renderdata when last data of field load
          }
        }
      function renderData(){
        var newsubFiledata=subFiledata.filter(function (f) { return f; })
        for(var j=0;j<newsubFiledata.length;j++){
          flag++
          mimetype=Globals.getMimetype(newsubFiledata[j].hexcode)
          for(var f=0;f<fields.files.length;f++){
            if(newsubFiledata[j].preview==fields.files[f].preview){
            var noPostObj={
              mimetype:mimetype,
              newFileHaxCode : newsubFiledata[j].hexcode
            }
            Object.assign(fields.files[f],noPostObj)
            }
          }
          newFileType=mimetype.split('/')
          if(Globals.alllowedFileType.indexOf(newFileType[0]) == -1){
            //file type is in  alllowedFileType array
            var newFieldFile=fields.files.filter(function (f) { return f; })
            for(var i=0;i<newFieldFile.length;i++){
              if(newFieldFile[i].preview==newsubFiledata[j].preview){
                //when fields file preview and new create array preview is same
                invalidType++
               // subFiledata.splice(subFiledata.indexOf(newsubFiledata[j]),1)
                //isFileSupported is a flag and set to false when file is not valid type
                var noPostObj={
                  isFileSupported:false,
                }
                Object.assign(fields.files[i],noPostObj)
              }
            }
          }
          if(file.type==''){
            //if file does not have type
            var noPostObj={
              isFileSupported:false
            }
            Object.assign(fields.files[fields.files.indexOf(file)],noPostObj)
          }
          if (Globals.alllowedFileType=='application' && Globals.allowedApplictionType.indexOf(newFileType[1]) == -1) {
            var noPostObj={
              isFileSupported:false
            }
            Object.assign(fields.files[fields.files.indexOf(file)],noPostObj)
          }
        }
        if(subFiledata.length + countFile ==totalLength && subFiledata.length>0){
          subFiledata=[]
          fileinnerdetail={}
          me.props.actions.uploadedAssetInBuffer(fields)
        }
        countFile = 0;
      }


  }
/**
 * @author disha
 * @use : call when drop files or select files from dropzone
 * @param {*} fields //fields of uploaded files
 */
  handleDropzoneSubmit(fields) {
    this.setState({
      DriveData:'',
      renderDriveUI:false
    })
    selectedMediaSizeTotal = 0
    for (var i = 0; i < fields.files.length; i++) {
      //to count total media size and store in global varibale
      selectedMediaSizeTotal += fields.files[i].size
    }
    //pass total media size to parent so that while canceling upload media size can be sent from there
    this.props.TotalMediaSize(selectedMediaSizeTotal)

    if (selectedMediaSizeTotal < freeStorage && freeStorage !== 0) {
      for (var i = 0; i < fields.files.length; i++) {
        //loop for check mime type of every files

        var file = fields.files[i]

        //isFileSupported is a flag for check wether file is valid or not , default is true
        var noPostObj = {
          isFileSupported: true,
        }
        Object.assign(fields.files[i], noPostObj)
        if (fields.files[i].type == 'video/mp4') {
          countFile = countFile + 1;
          this.withOutCheckMimetypeAsset(file, fields.files.length, fields)
        } else {
          this.checkFileMimetype(file, fields.files.length, fields)
        }
      }
    } else {
      notie.alert('error', `${this.props.profile.profile.first_name + this.props.profile.profile.last_name }, you have consumed your storage limit. Please ask your admin to upgrade your package to continue.`, 5)
      return
    }
  }

  // handleAllFormsSubmit will get all forms references and run the submit function of all redux forms
  handleAllFormsSubmit (e) {
   // reset variable for submit on button click so it will not append history.
    if(freeStorage >selectedMediaSizeTotal )
    {
      countOfForm = 0
      collectedFormData = []
      let { uploadedAssetBuffer } = this.props.assets
      var totalForms = uploadedAssetBuffer.files.length
      for (var i = 0; i < totalForms; i++) {
        // this is why I ❤ reduxform, submit all forms at once
        this.refs[`createAssetForm${i}`].submit()
      }
    }
    else{
      notie.alert('error', `${this.props.profile.profile.first_name + this.props.profile.profile.last_name }, you have consumed your storage limit. Please ask your admin to upgrade your package to continue.`, 5)
      return
    }
    
  }

  openEditor (fileData, indexCount = 0) {
    this.setState({
      openEditor: true,
      imageData: fileData,
      indexCount: indexCount
    })
  }

  closeEditorPopup (closeParent = true) {
    this.setState({ openEditor: false, directEditor: false })
    document.body.classList.remove('overlay')
    if (closeParent) {
      this.props.closeEditDetailFormPopup()
    }
    if (closeParent == false) {
      document.body.classList.add('overlay')
    }
  }

  editorPopup () {
    selectedMediaSizeTotal=this.state.imageData.size
    return (
      <SliderPopup wide className='editorPopup'>
        <button
          id='close-editor-popup-id'
          className='btn-default'
          onClick={this.closeEditorPopup.bind(this)}
        >
          <i className='material-icons'>clear</i>
        </button>
        <div id='asset-editor-wrapper' className={show_class}>
        {this.state.photoEditor?
          <PhotoEditor
            photoEditor={this.state.photoEditor}
            imageData={this.state.imageData}
            imageName={this.state.imageData.name}
            mediaType={this.state.imageData.type}
            indexCount={this.state.indexCount}
            closeEditorPopup={this.closeEditorPopup.bind(this)}
          />:''}
        </div>

      </SliderPopup>
    )
  }

  handleNewAssetCreation () {
    this.props.actions.uploadReset()
    this.props.actions.resetCreateAsset()

    // asset created flag - change in redux to false
    this.props.actions.creatingNewAsset()
  }

  /**
   * @author disha 
   * will call when page is refreshed or trying to close 
   * @param {*} event 
   */
  onUnload(event){
    if (this.state.DriveData!=='' && this.props.assets.driveAssetsUploading==true) {
      //api will call when uploading is start and user want to cancel or refresh page
      var objToSend = {
        "file-size": selectedMediaSizeTotal,
        "action": "decrease"
      }
      this.props.generalActions.fetchS3Size(objToSend)
    }else{
    this.props.onUnload(event)
    }
  }

  /**
   * @author disha
   * when clicked on create after filling desciption to upload it this method will check storage space
   * @param {*} data field file data
   */
  checkStorageSpace(data){
    window.addEventListener("beforeunload", this.onUnload)

    //this will set flag which will be false when api will call 
    if(this.state.DriveData!==''){
      this.props.actions.uploadDriveAssetInit();
    }else{
      this.props.actions.assetUploadingStart()
    }

      this.setState({
        storeFields:data,
        isUploadButtonClicked:true
      })
      var objToSend={
          "file-size":selectedMediaSizeTotal,
          // "file-size":5243336130560,
          "action":"increase"
      }
      this.props.generalActions.fetchS3Size(objToSend)
  }


  uploadAssetOnS3(fields, storageData = null) {
    this.setState({
      uploadingFinished: false
    })
    var isFirstAsset = false;
    if(this.props.assets.userFiles.length==0){
      isFirstAsset = true;
    }
    var count = 0
    //if storage has space then uploading will start
    if (storageData.access !== 'rejected') {
      this.props.actions.uploadingStart()
      var me = this
      Globals.AWS_CONFIG.albumName = localStorage.getItem('albumName')
      var s3Config = Globals.AWS_CONFIG
      var s3Manager = new s3functions.S3Manager(s3Config)
      var objToStore = fields
      // var filesToUpload = fields
      // we are uploading files to s3 now, set the flag in redux
      // this.props.actions.uploadingStart()

      for (var i = 0; i < fields.length; i++) {
        var fileToUpload = fields[i].fileData

        // upload starts here.
        // it returns a promise whether succeeded or failed
        // after you get the promise, proceed with db insertion
        var s3MediaURL = s3Manager.s3uploadMedia(fileToUpload, i, fields.length)
        // On succession of uploading main asset on amazon, we will generate a thumbnail and
        // send next request to amazon
        s3MediaURL
          .then(obj => {
            var file = obj.data
            var originalFileObj = obj.fileObject
            var index = obj.index
            var generatedFileName = file.Location.split('/')
            generatedFileName = generatedFileName[generatedFileName.length - 1]
            generatedFileName = generatedFileName.substring(
              0,
              generatedFileName.length - 41
            )
            if (originalFileObj.type.match('image.*')) {
              var thumbnail = s3Manager.s3uploadThumbnail(
                originalFileObj,
                index,
                1
              )
              thumbnail
                .then(function (thumb) {
                  var photokey=file.Location.split('amazonaws.com/')[1]
                  var thumbPhotokey=thumb.data.Location.split('amazonaws.com/')[1]
                  let uploadedFile = {
                    fileUrl: `https://s3.${Globals.AWS_BUCKET_REGION}.amazonaws.com/${file.Bucket}/${photokey}`,
                    thumbUrl: `https://s3.${Globals.AWS_BUCKET_REGION}.amazonaws.com/${file.Bucket}/${thumbPhotokey}`,
                    uploadedFile: generatedFileName,
                    filesize: originalFileObj.size
                  }
                  count++
                  objToStore[index]['fileUrl'] = `https://s3.${Globals.AWS_BUCKET_REGION}.amazonaws.com/${file.Bucket}/${photokey}`
                  objToStore[index]['thumbUrl'] = `https://s3.${Globals.AWS_BUCKET_REGION}.amazonaws.com/${file.Bucket}/${thumbPhotokey}`
                  objToStore[index]['uploadedFile'] = generatedFileName
                  objToStore[index]['height'] = thumb.thumbnail.height
                  objToStore[index]['width'] = thumb.thumbnail.width

                  // && index == (fields.length-1)
                  if (count == i) {
                    me.props.actions.assetsCreationInitiate()
                    var campaignId = ''
                    if (
                      typeof me.props.location !== 'undefined' &&
                      typeof me.props.location.query.campaignid !== 'undefined'
                    ) {
                      campaignId = me.props.location.query.campaignid
                    }
                    if (me.props.searchedInput !== undefined) {
                      var temp
                      if (me.props.location.searchcreateasset !== undefined) {
                        var searchedInput = me.props.searchedInput
                       
                        if(!me.state.cancelUploadAsset)
                        {
                          me.props.actions.multiAssetCreate(objToStore, {
                            maxItems: me.props.maxItems,
                            currPage: me.props.currPage,
                            campaignId
                          }, searchedInput, true,false,isFirstAsset)
                        }   
                      }
                      else {
                        if(!me.state.cancelUploadAsset)
                        {
                          var searchedInput = me.props.searchedInput
                          me.props.actions.multiAssetCreate(objToStore, {
                            maxItems: me.props.maxItems,
                            currPage: me.props.currPage,
                            campaignId
                          }, searchedInput,null,false,isFirstAsset)
                        }   
                      }
                    }
                    else {
                      if(me.state.cancelUploadAsset==false)
                      {
                        var newLocation = utils
                        var path = utils.removeTrailingSlash(me.props.location.pathname).split('/')
                        let uploadForMyAsset = false
                        if(path[2] !== undefined && path[2] == 'my'){
                          uploadForMyAsset = true
                        }
                        me.props.actions.multiAssetCreate(objToStore, {
                          maxItems: me.props.maxItems,
                          currPage: me.props.currPage,
                          campaignId
                        }, null, false,false,isFirstAsset,uploadForMyAsset)                        
                      }    
                    }
                    me.props.actions.uploadedItem(objToStore)
                  }
                })
                .catch(err => {
                  notie.alert(
                    'error',
                    // 'There was a problem generating thumbnail',
                    err,
                    5
                  )
                })
            } else if (originalFileObj.type.match('video') !== null && originalFileObj.type.match('video').length > 0) {
              var thumbnail = s3Manager.s3uploadThumbnail(
                originalFileObj,
                index,
                1,
                file.Location)
              let uploadedFile = {}
              thumbnail.then(function (thumb) {
                var photokey=file.Location.split('amazonaws.com/')[1]
                var thumbPhotokey=thumb.data.Location.split('amazonaws.com/')[1]
                var newfileUrl=`https://s3.${Globals.AWS_BUCKET_REGION}.amazonaws.com/${file.Bucket}/${photokey}`
                var newthumbUrl=`https://s3.${Globals.AWS_BUCKET_REGION}.amazonaws.com/${file.Bucket}/${thumbPhotokey}`
                uploadedFile = {
                  fileUrl: newfileUrl,
                  thumbUrl: newthumbUrl,
                  uploadedFile: generatedFileName,
                  media_size: originalFileObj.size
                }
                if (thumb.Location == '' || thumb.Location == undefined) {
                  //if video thumbnail from s3 is failed to generate then to re-create thumbnail using plugin and upload on S3
                  var thumbnail = s3Manager.s3uploadThumbnail(
                    originalFileObj,
                    index,
                    1,
                    file.Location, false, true)
                  thumbnail.then(function (thumb) {
                    uploadedFile = {
                      fileUrl: newfileUrl,
                      thumbUrl: newthumbUrl,
                      uploadedFile: generatedFileName,
                      media_size: originalFileObj.size
                    }
                  })
                }
                objToStore[index]['fileUrl'] = newfileUrl
                objToStore[index]['thumbUrl'] = newthumbUrl
                objToStore[index]['uploadedFile'] = generatedFileName
                objToStore[index]['height'] = thumb.thumbnail.height
                objToStore[index]['width'] = thumb.thumbnail.width
                count++
                if (count == i) {
                  me.props.actions.assetsCreationInitiate()
                  var campaignId = ''
                  if (typeof me.props.location.query.campaignid !== 'undefined') {
                    campaignId = me.props.location.query.campaignid
                  }
                  if(me.props.searchedInput!==undefined)
                  {
                    var temp
                    if(me.props.location.searchcreateasset!==undefined)
                    {  
                      if(!me.state.cancelUploadAsset)
                      {
                        var searchedInput = me.props.searchedInput
                        me.props.actions.multiAssetCreate(objToStore, {
                        maxItems: me.props.maxItems,
                        currPage: me.props.currPage,
                        campaignId},searchedInput,true,false,isFirstAsset)
                      }  
                    }
                    else
                    { 
                      if(!me.state.cancelUploadAsset)
                      {
                        var searchedInput = me.props.searchedInput
                        me.props.actions.multiAssetCreate(objToStore, {
                        maxItems: me.props.maxItems,
                        currPage: me.props.currPage,
                        campaignId},searchedInput,null,false,isFirstAsset)
                      }   
                    }

                  }
                  else
                  {  
                  if(!me.state.cancelUploadAsset) 
                  {
                    me.props.actions.multiAssetCreate(objToStore, {
                      maxItems: me.props.maxItems,
                      currPage: me.props.currPage,
                      campaignId
                    },null,false,false,isFirstAsset)
                  }     
                  }
                  me.props.actions.uploadedItem(objToStore)
                }

                // me.props.actions.uploadedItem(uploadedFile)

              })
            }
            else {
              var photokey=file.Location.split('amazonaws.com/')[1]
                var newfileUrl=`https://s3.${Globals.AWS_BUCKET_REGION}.amazonaws.com/${file.Bucket}/${photokey}`
              let uploadedFile = {
                fileUrl: newfileUrl,
                thumbUrl: '',
                uploadedFile: generatedFileName,
                media_size: originalFileObj.size
              }
              objToStore[index]['fileUrl'] = newfileUrl
              objToStore[index]['thumbUrl'] = ''
              objToStore[index]['uploadedFile'] = generatedFileName
              // if(index == (fields.length-1)){
              count++
              if (count == i) {
                if(!me.state.cancelUploadAsset)
                {
                  me.props.actions.assetsCreationInitiate()
                  var campaignId = ''
                  if (typeof me.props.location.query.campaignid !== 'undefined') {
                    campaignId = me.props.location.query.campaignid
                  }
  
                  me.props.actions.multiAssetCreate(objToStore, {
                    maxItems: me.props.maxItems,
                    currPage: me.props.currPage,
                    campaignId
                  },null,null,false,isFirstAsset)
                }  
              }

            }
          }).catch(err => {
            notie.alert('error', 'There was a problem uploading your asset', 5)
          })
      }
    } else {
      notie.alert('error', `${this.props.profile.profile.first_name + this.props.profile.profile.last_name }, you have consumed your storage limit. Please ask your admin to upgrade your package to continue.`, 5)
      this.props.actions.resetCreateAsset()
    }
  }

  createTagList (str) {
    var me = this
    var rx1 = /@\[([^\]]+)]\(([^)]+)\)/g
    let res = str ?str.match(rx1):''
    var userTag = []

    return new Promise(function (resolve, reject) {
      if (res) {
        res.forEach(function (tags) {
          /** Get user information */
          var tagsArray = tags.split('@[')
          var nameArray = tagsArray[1].split(']')
          var name = nameArray[0]
          var dept = nameArray[1]
            .substring(0, nameArray[1].length - 1)
            .split(':')[1]
            .split('_')
          var type = 'tags'
          if (
            nameArray[1].substring(0, nameArray[1].length - 1).split(':')[0] !==
            '(tags'
          ) {
            type = dept[1] == 'company' ? 'company' :
                dept[1] == 'dept' ?
                  "department"
                  : "user";
            let tagsUserObject = {
              tagid: dept[0],
              tagName: name.replace(/ /g, ''),
              type: type
            }
            userTag.push(tagsUserObject)
          }

          /** description replace */
          if (type === 'tags') {
            str = str.replace(tags, '#' + name.replace(/ /g, ''))
          } else {
            str = str.replace(tags, '@' + name.replace(/ /g, ''))
          }
        })
        me.setState({
          userTag: userTag
        })
      }
      resolve(str)
    })
  }
  // handleAssetFormSubmit submits each form values to API
  /* handleAssetFormSubmit (fieldValues) {
    var data=[]
    var count=0
    //add counts and objects in variable so we can compare it and use it in loop
    if(fieldValues.description !== ""){
        countOfForm = countOfForm + 1;
        collectedFormData.push(fieldValues);
    }
    //check how may form has been created
    let { uploadedItems } = this.props.aws
    var totalForms = uploadedItems.length
    //if number of create form and validated form are equal then only call the api
    if(totalForms == countOfForm){
        collectedFormData.map((formData, i) => {
            this.createTagList(formData.description).then((desc) => {
              count++;
              fieldValues.description = desc;
              fieldValues.user_tag = this.state.userTag;
                data.push(formData)
                if(count==totalForms){
                 this.props.actions.assetsCreationInitiate()
                  this.props.actions.multiAssetCreate(
                    data, {
                        maxItems: this.props.maxItems,
                        currPage: this.props.currPage}
                  )
                }
            })
        })
  }
} */
/**
 * to upload asset
 * @param {*} fieldValues
 */
  handleAssetDetailFormSubmit (fieldValues) {
    var data = []
    var count = 0
    var invalidAsset=0
    var validAsset=0
    var totalForms ='';
    let { uploadedAssetBuffer } = this.props.assets
    if(this.state.DriveData!== '' ){
      uploadedAssetBuffer = { files : {
                                0:DriveFileObj
                              }
      }
      totalForms = 1
    }else{
       totalForms = uploadedAssetBuffer.files.length
    }
    if(uploadedAssetBuffer.files.length>0){

      for(var i=0;i<uploadedAssetBuffer.files.length;i++){
        if(uploadedAssetBuffer.files[i].isFileSupported==false){
          invalidAsset++
        }else{
          validAsset++
        }
      }
    }
    // add counts and objects in variable so we can compare it and use it in loop
    if (fieldValues.description !== '' ) {
      countOfForm = countOfForm + 1
      collectedFormData.push(fieldValues)
    }
    // check how many form has been created
    // if number of create form and validated form are equal then only call the api
    if (totalForms == countOfForm ) {
      // this condition is  to identityfy whether the asset is from google drive or not  then set the parameters according to that 
      if(DriveFileObj!=='' && DriveFileObj!==null && this.state.DriveData!==''){
        collectedFormData.map((formData, i) => {
          this.createTagList(formData.description).then(desc => {
            count++
            fieldValues.description = desc
            fieldValues.user_tag = this.state.userTag
            data.push(formData)
          this.handleDriveSubmit(data);
          }) 
        })
      }else{
        collectedFormData.map((formData, i) => {
        //check if file is valid while uploading it or edit asset in photoeditor
        var currFileSize=formData.filesize !== undefined ? formData.filesize : formData.fileData !== undefined ? formData.fileData.size :0
        var objTosend={
          filesize:currFileSize
        }
        Object.assign(formData,objTosend)
        formData.fileData.isFileSupported || formData.fileData.isFileSupported == undefined?
        this.createTagList(formData.description).then(desc => {
          count++
          fieldValues.description = desc
          fieldValues.user_tag = this.state.userTag
          data.push(formData)
          if (count == validAsset) {
            this.props.actions.storeFileDateTemp(data)
            this.props.actions.resetuploadedAssetInBuffer()
            this.checkStorageSpace(data)
            // this.uploadAssetOnS3(data)
          }
          // append data in array and store in redux
        }):''
      })
      } 
    }

  }
 
  // submit form when button is clicked
  handleFormSubmitClick () {
    this.refs[`createAssetFormRef`].submit()
  }

  /*
  ** Cats to Options:
  ** This fuction will get all properties of an object and clone it to a new object with different named
  ** properties.
  ** As API returns [{"identity":"rpPDp","name":"interview"},{"identity":"Opjmp","name":"marketing"}]
  ** we need to append properties to this object
  ** [{"value":"rpPDp","label":"interview"},{"value":"Opjmp","label":"marketing"}]
  ** So, we can use this object in <Select><options></Select> component
  */
  catsToOptions () {
    var tags = this.props.assets.cats.slice()

    for (var t = 0; t < tags.length; t++) {
      for (var prop in tags[t]) {
        if (tags[t].hasOwnProperty('identity')) {
          tags[t]['label'] = tags[t]['name']
          tags[t]['value'] = tags[t]['identity']
        }
      }
    }

    this.setState({
      tags: tags
    })
  }

  renderLoading () {
    return (
      <div className='preloader-wrap'>
        <PreLoader />
      </div>
    )
  }

  renderSubmitFormButton () {
    return(
    typeof this.props.assets.uploadedAssetBuffer.files !== 'undefined' && this.state.currentAssetTab!== "Drive" &&
      !this.props.aws.uploadStarted 
      ? 
      <div className='submitAssetAction'>
        <button
          className='btn btn-primary'
          type='submit'
          onClick={this.handleAllFormsSubmit.bind(this)}
          className = "btn-asset-submit btn btn-primary"
          >
            Create
          </button>
      </div>
      : this.state.renderDriveUI==true && showDriveUpload==false && this.state.currentAssetTab!=="Dropzone"? 
      <div className='submitAssetAction'>
        <button
          className='btn btn-primary'
          type='submit' 
          onClick={this.AssetDriveFormSubmit.bind(this)}
          className = "btn-asset-submit btn btn-primary"
          >
            Create
          </button>
      </div>
      :<div />
    )
  }
  renderLoading () {
    return (
      <div className='preloader-wrap'>
        <PreLoader />
      </div>
    )
  }
  renderCreateNewButton () {
    if (this.props.assets.creation.created) {
      return (
        <div className='createNewAssetAction'>
          <button
            className='btn btn-primary'
            onClick={this.handleNewAssetCreation.bind(this)}
          >
            ADD MORE
          </button>
        </div>
      )
    }
  }

  closePopupOnSuccess () {
    if (this.props.assets.creation.created) {

      //to hide the notie options
      this.setState({
        uploadingFinished:true
      })

      this.props.actions.uploadReset()
      this.props.closeAssetCreatePopup()
      this.props.actions.resetCreateAsset(true,true)
      this.props.closeEditDetailFormPopup()    
    }
  }

  
  /**
   * @author disha
   * get video thumbnail src and push into array
   */
  getVideoThumbnailImage(e){
    this.state.videoThumbnailImage.push(e)
  }
  selectedAssetType(assetType){
    if(assetType=="Drive"){
      this.setState({
        currentAssetTab : "Drive"
      })
      document.getElementsByClassName("DriveAssets").length!==0?document.getElementsByClassName("DriveAssets")[0].classList.remove("hide"):''
      document.getElementsByClassName("DropzoneAssets").length!==0? document.getElementsByClassName("DropzoneAssets")[0].classList.add("hide"):''
    }else if(assetType=="Dropzone"){
      this.setState({
        currentAssetTab : "Dropzone"
      })
      document.getElementsByClassName("DriveAssets").length!==0?document.getElementsByClassName("DriveAssets")[0].classList.add("hide"):''
      document.getElementsByClassName("DropzoneAssets").length!==0?document.getElementsByClassName("DropzoneAssets")[0].classList.remove("hide"):''
    }
  }
  render () {
    var Drivestatus = JSON.parse(localStorage.getItem('isDriveConnected'));
    var Driveidentity =  this.props.profile ? this.props.profile.profile.identity:'';

    const { loading } = this.props.assets.creation
    const catID = this.props.catID
    const campID = this.props.campID
    const folderID = this.props.folderID
    var catName = ''
    if (catID != null && this.props.catName !== null) {
      catName =  this.props.catName[0]?this.props.catName[0].name:'';
    }
    let { uploadedItems, uploadDone, uploadStarted, fileTemp } = this.props.aws
    let { uploadedAssetBuffer, creation, addAssetInDBInit } = this.props.assetProps?this.props.assetProps:this.props.assets
    var uploadedData = uploadedAssetBuffer
    var me = this
    var fileDetail={}
    freeStorage = this.props.general.s3SizeData!=='' ?Object.keys(this.props.general.s3SizeData.storage).length>0 ? this.props.general.s3SizeData.storage.freeStorage * Math.pow(1024,2) : 0:''
    // var freeStorage=12500
    return (
      <section className='create-asset-page'>
        {creation.loading === true || this.props.general.storageLoading ==true ? this.renderLoading() : ''}
        {this.state.openEditor ? this.editorPopup() : ''}
        {this.state.directEditor !== true
          ? <div id='asset-creation'>
            <div>
              <header className='heading'>
                <h3>Asset Creation</h3>
              </header>
              <PopupWrapper>
                <div>
                  <div className="clearfix selectAssetTabWrapper">
                  <div className="selectAssetTabs">
                  <a href="javascript:void(0);" onClick={this.selectedAssetType.bind(this,"Dropzone")} className={`${this.state.currentAssetTab=="Dropzone"? "active btn-primary":''}`}>
                    <label> Upload Assets </label>
                  </a>
                  <a href="javascript:void(0);" onClick={this.selectedAssetType.bind(this,"Drive")} className={ `${this.state.currentAssetTab=="Drive"? "active btn-primary":''}`}>
                    <label> Drive </label>
                  </a>
                  </div>
                  </div>
                  <div
                    className={
                        typeof uploadedAssetBuffer.files !== 'undefined' &&
                          !uploadStarted
                          ? `assetFormDropzone slideUp DropzoneAssets`
                          : `assetFormDropzone DropzoneAssets` && this.state.currentAssetTab== "Dropzone" ? '' : "hide"} 
                    >
                    {typeof uploadedAssetBuffer.files === 'undefined' &&
                        !uploadStarted &&
                        !creation.loading &&
                        !creation.created &&
                        !addAssetInDBInit
                        ? <CreateAssetFormDropzone
                          freeStorage={freeStorage}
                          onSubmit={this.handleDropzoneSubmit.bind(this)}
                          setImageThumbnailElement={this.getVideoThumbnailImage.bind(this)}
                          />
                        :
                          null}
                  </div>
                  {this.state.renderDriveUI!==true ?  
                  <div className={`DriveAssets ${this.state.currentAssetTab== "Drive" ? '' : "hide"}`}>

                        {
                          Drivestatus!==null && Drivestatus[Driveidentity]==true?
                          <div className = "google-drive-container">
                              <div className = "inner-google-container">
                                {/* img part */}
                                  <div className = "main-container-of-img-block">
                                  <div className = "google-drive-img">
                                      <img src = "/img/Google_Drive_Logo.svg.png" />
                                    </div>
                                </div>
                                {/* end img part */}

                                <div className = "text-part-of-google-drive">
                                <div> <p className = "main-title-of-google-drive">Select File From Google Drive</p></div>
                                    <div className = "google-drive-button">
                                        <a className = "btn GDrive-btn"  onClick={this.selectFromDrive.bind(this)}> Choose file </a>
                                    </div> 
                                </div>
                                </div>
                        </div>
                          :
                          <div className = "google-drive-container">
                          <div className = "inner-google-container">
                            {/* img part */}
                            <div className = "main-container-of-img-block">
                              <div className = "google-drive-img">
                                <img src = "/img/Google_Drive_Logo.svg.png" />
                              </div>
                            </div>
                            {/* end img part */}

                            <div className = "text-part-of-google-drive">
                              <div> <p className = "main-title-of-google-drive">Select File From Google Drive</p></div>
                              <div className = "content-title-gDrive">
                                  <p> You need to authenticate with Google Drive.</p>
                                  <p> We only extract images and never modify or delete them.</p>
                                </div>
                                <div className = "google-drive-button">
                                    <a className = "btn GDrive-btn"  onClick={this.selectFromDrive.bind(this)}> Connect Google Drive </a>
                                </div> 
                                <div className = "content-title-gDrive">
                                  <p> A new page will open to connect your account. </p>
                                </div>
                            </div>
                            </div>
                        </div>
                        }
                        </div>:''}
                  
                  <div className='assetForms clearfix'>
                    <div className='section asset-post-details-main'>
                      { this.state.renderDriveUI==true && this.state.currentAssetTab=="Drive"? 
                          <div>
                           { me.renderDrive() }
                           { me.renderDriveLoading()} 
                          </div>
                      :
                      typeof uploadedAssetBuffer.files !== 'undefined' && this.state.currentAssetTab!== "Drive" &&
                      !uploadStarted &&
                      !creation.created
                      ? uploadedAssetBuffer.files.map(
                          (fileUploaded, index) => {
                            if (
                              typeof fileUploaded.fileData !== 'undefined'
                            ) {
                              fileUploaded.fileData.name = fileUploaded.name
                              fileUploaded.fileData.preview =
                                fileUploaded.preview
                            }
                            let initialValues = {
                              initialValues: {
                                title: decodeURI(
                                  fileUploaded.name.replace(/\..+$/, '')
                                ),
                                fileData: typeof fileUploaded.fileData !==
                                  'undefined'
                                  ? fileUploaded.fileData
                                  : fileUploaded,
                                catID: catID,
                                campID: campID,
                                filesize: fileUploaded.size,
                                folderID: folderID,
                                numberOfUploaded: uploadedAssetBuffer.files
                                  .length
                              }
                            }

                            return (
                              <CreateAssetDetailForm
                                ref={`createAssetForm${index}`}
                                key={index}
                                form={`CreateAsset-${index}`}
                                onSubmit={this.handleAssetDetailFormSubmit.bind(
                                  this
                                )}
                                isFileSupported={fileUploaded.isFileSupported}
                                tags={me.state.tags}
                                catID={catID}
                                campID={campID}
                                catName={catName}
                                filesize={fileUploaded.filesize}
                                folderId={folderID}
                                {...initialValues}
                                fileUploaded={
                                  typeof fileUploaded.fileData !==
                                    'undefined'
                                    ? fileUploaded.fileData
                                    : fileUploaded
                                }
                                indexCount={index}
                                numberOfUploaded={
                                  uploadedAssetBuffer.files.length
                                }
                                openEditor={this.openEditor.bind(this)}
                                enableReinitialize
                              />
                            )
                          }
                        )
                      : ''
                      }
                      {(uploadStarted || (uploadDone && !creation.created)) && this.state.currentAssetTab!== "Drive"
                          ? <div id='edit-asset'>
                            <label>
                                Please wait while we are uploading your media.
                              </label>
                            {fileTemp.map((uploadFiles, i) => {
                              var uploadingProgress = 0
                              if (
                                  typeof this.props.aws.uploadProgress !==
                                  'undefined'
                                ) {
                                if (
                                    typeof this.props.aws.uploadProgress[i] !==
                                    'undefined'
                                  ) {
                                  uploadingProgress = this.props.aws
                                      .uploadProgress[i].percent
                                }
                              }
                              this.state.videoThumbnailImage.map((img,d)=>{

                              if(img.filePreview==uploadFiles.fileData.preview){
                                fileDetail={
                                  thumbnailImageSrc:img.thumbnailSrc,
                                  filePreview:img.filePreview
                              }
                            }
                          })
                              return (
                                <EditAssetUploadingSteps
                                  uploadingProgress={uploadingProgress}
                                  uploadedFiles={uploadFiles}
                                  sendVideoThumbnailImage={fileDetail}
                                  key={i}
                                  />
                              )

                            })}
                          </div>
                          : ''}
                      {this.renderSubmitFormButton()}
                      {/* this.renderCreateNewButton() */}
                      {this.closePopupOnSuccess()}

                    </div>

                  </div>
                </div>
              </PopupWrapper>
            </div>
          </div>
          : ''}
          <Script
          url="https://apis.google.com/js/platform.js" 
          onLoad={this.handleScriptLoad.bind(this)}
          parsetags= "explicit"
        />
      </section>
    )
  }
}

function mapStateToProps (state) {
  return {
    assets: state.assets,
    general:state.general,
    aws: state.aws,
    profile:state.profile
  }
}

function mapDispatchToProps (dispatch) {
  return {
    generalActions: bindActionCreators(generalActions, dispatch),
    actions: bindActionCreators(Object.assign({}, assetsActions, s3Actions),dispatch),
  }
}

let connection = connect(mapStateToProps, mapDispatchToProps)

module.exports = connection(CreateAsset)
