import * as utils from '../../utils/utils'
import Asset from './Asset'
import Globals from '../../Globals'
import reactStringReplace from 'react-string-replace'
import { IndexLink, Link } from 'react-router'
import EditAssetUploadingSteps from './EditAssetUploadingSteps'
import PreLoader from '../PreLoader'
import SliderPopup from '../SliderPopup'
import CreateAssetForm from './CreateAssetForm'
import CreateAsset from './CreateAsset'
import PopupWrapper from '../PopupWrapper'

class AssetDetail extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      editorPopup: false,
      userTag: [],
      tags:[],
      imageData: [],
      closeEditorState:false,
    }
  }

  renderLoading () {
    return (
      <div className='preloader-wrap'>
        <PreLoader />
      </div>
    )
  }

  updateText(id, type, e) {
    this.props.updateText(id,type, e);
  }
  
  componentDidMount() {
    let me = this;
    setTimeout(function(){
      //console.log(me.refs.assetDetailImage.offsetWidth)
    },100)

   /* if(this.props.flag == 1){
      this.setState({editorPopup: true})
     }*/
    if(document.querySelector("#asset-detail video"))
    {
      var video = document.querySelector("#asset-detail video");
      var playPromise = video.play();

      if (playPromise !== undefined) {
        playPromise.then(_ => {
          // Automatic playback started!
          // Show playing UI.
          video.pause();
        })
        .catch(error => {
          // Auto-play was prevented
          // Show paused UI.
        });
      }

    }
  }
  closeDetailFormPopup() {
    this.setState({ editorPopup: false })
    document.body.classList.remove('overlay')
    this.props.renderAssetDetail();
  }


 createTagList(str) {
    var me = this;
    var rx1 = /@\[([^\]]+)]\(([^)]+)\)/g;
    let res = str.match(rx1);
    var userTag = [];

    return new Promise (
      function (resolve, reject) {
        if(res) {
      res.forEach(function(tags) {
        /** Get user information */
        var tagsArray = tags.split("@[");
        var nameArray = tagsArray[1].split("]");
        var name = nameArray[0];
        var dept = nameArray[1].substring(0, nameArray[1].length - 1).split(":")[1].split('_');
        var type = "tags";
        if (nameArray[1].substring(0, nameArray[1].length - 1).split(":")[0] !== "(tags") {
            type = dept[1] == 'company' ? 'company' :
                dept[1] == 'dept' ?
                  "department"
                  : "user";
            let tagsUserObject = {
              tagid: dept[0],
              tagName: name.replace(/ /g, ''),
              type: type
            }
            userTag.push(tagsUserObject)
        }

        /** description replace */
        if (type === "tags") {
          str = str.replace(tags, "#" + name.replace(/ /g, ''));
        } else {
          str = str.replace(tags, "@" + name.replace(/ /g, ''));
        }

      })
      me.setState({
        userTag: userTag
      })
    }
      resolve(str);
    })
  }

  //Handle submit form
  handleFormSubmit (fieldValues) {
    this.createTagList(fieldValues.description).then((desc) => {
        fieldValues.description = desc;
        fieldValues.user_tag = this.state.userTag;
        this.props.assetAction.assetsCreationInitiate()
        this.props.assetAction.assetsCreate(fieldValues, {
          maxItems: 10,
        })
    })
  }
  //handle submit event when external submit button clicked
  handleAllFormsSubmit(){
      //submit the form
      this.refs[`createAssetForm0`].submit();
  }
  //render external submit button
  renderSubmitFormButton () {
      return (
        <div className='submitAssetAction'>
          <button

            className='btn btn-primary'
            type='submit'
            onClick={this.handleAllFormsSubmit.bind(this)}
          >
            Create
          </button>
        </div>
      )
  }
  closeEditor(){

      this.renderEditor();
  }
  //render
  renderEditor() {
    return(
          <SliderPopup wide className='editorPopup'>
            <button
              id='close-editor-popup'
              className='btn-default'
              onClick={this
              .closeDetailFormPopup
              .bind(this)}>
              <i className='material-icons'>clear</i>
            </button>
             <button
              id='close-editor-popup-asset-detail'
              className='btn-default'
              onClick={this
              .closeEditor
              .bind(this)}>
              close editor
            </button>
            <div id="asset-editor-wrapper">
                    <CreateAsset
                    closeEditorProp={this.state.closeEditorState}
                    />
                </div>

          </SliderPopup>
    )

  }
  showEditor(){

       this.props.s3Actions.uploadReset();
       this.props.assetAction.resetCreateAsset();
      var objForEditor = []
      objForEditor.push({
        name:this.props.asset.title+"."+this.props.asset.media_extension,
        preview:this.props.asset.media_url,
        size:this.props.asset.media_size,
        type:this.props.asset.media_type
      })
      objForEditor['files'] = objForEditor;
      this.props.assetAction.uploadedAssetInBuffer(objForEditor);
      this.setState({
          editorPopup: true,
          imageData:objForEditor
      });
      this.props.closeAssetDetailPopup(this,true);
      this.props.openEditDetailFormPopup();
  }

  renderAssetTags (asset) {
    let tags = []

    if (typeof asset.tags !== 'undefined') {
      tags = asset.tags.data
    }

    return (

      <div> </div>
    )
  }
  showeditordashboard(e,asset){
    this.props.showeditordashboard(e);
  }
  /**
    * to remove overlay of analytics popup form asset/hash page
    * @author disha
    */
   redirectToHashTag(){
     if(this.props.locationInfo !== undefined){
       var path=utils.removeTrailingSlash(this.props.locationInfo.pathname).split('/')
          if(path[1] !== undefined && (path[1] == "analytics" || path[1]== "assets")){
            document.body.classList.remove('overlay');
        }
     }
  }
  mousedowncapture(e){
    if(document.getElementById('pop-up-tooltip-holder')){
      if (!document.getElementById('pop-up-tooltip-holder').contains(e.target)) {
          document
            .getElementById('pop-up-tooltip-holder')
            .classList.add('hide');
      }
    }
  }
  render() {
    var location = window.location.href;
    var path = utils
      .removeTrailingSlash(location)
      .split('/')
    var gdocCompatibleExt = ['txt', 'pdf']
    var officeCompatibleExt = ['doc', 'docx', 'ppt', 'pptx', 'xls', 'xlsx']
    var fileExt = this.props.asset.media_extension
    var enableGoogleViewer = false
    var enableOfficeViewer = false
    var enableIframe = false

    if (gdocCompatibleExt.indexOf(fileExt) != -1) {
      enableGoogleViewer = true;
      jQuery('#side-popup-wrapper').addClass('asset-doc-popup');

    }

    if (officeCompatibleExt.indexOf(fileExt) != -1) {
      enableOfficeViewer = true;
      jQuery('#side-popup-wrapper').addClass('asset-doc-popup');

    }else{
      if(fileExt.includes("spreadsheet")|| fileExt.includes("document")||fileExt.includes("word")||fileExt.includes("presentation") || fileExt.includes("excel")){
        enableOfficeViewer = true;
        jQuery('#side-popup-wrapper').addClass('asset-doc-popup');
      }
    }

    if (enableOfficeViewer || enableOfficeViewer) {
      enableIframe = true
    }

    var moment = this.props.moment

    const spacing = {
      marginTop: '7px',
      marginLeft: '10px'
    }
     /** replace comment content */

     let replacedDetail = "",
     asset = this.props.asset,
     detail = asset.detail,
     userTag = asset.userTag ? asset.userTag.data :''
     replacedDetail = reactStringReplace((this.props.asset.detail), /@(\w+)/g, (match, i) => (
      userTag[userTag.findIndex(x => x.name == match)] ?

      <a 
      onMouseOver=
      {this.updateText.bind(this, (userTag[userTag.findIndex(x => x.name == match)] ? userTag[userTag.findIndex(x => x.name == match)].identity : ''),(userTag[userTag.findIndex(x => x.name == match)] ? userTag[userTag.findIndex(x => x.name == match)].type : ''))}
      onMouseLeave={this.mousedowncapture.bind(this)}
      >{`@` + match}
      </a>
      :
      <span>
      {`@` + match}
      </span>
     ));
     replacedDetail = reactStringReplace(replacedDetail, /#(\w+)/g, (match, i) => (
       <Link to={`/feed/assets/hash${`#` + match}`} onClick={this.redirectToHashTag.bind(this)}>{`#` + match}</Link>
     ))
     var index = this.props.asset.media_type.indexOf('/')

    var type = 'application'

    if (index !== -1) {
      type = this.props.asset.media_type.substr(0, index)
    } else {
      type = this.props.asset.media_type
    }

    // var mediatype= this.props.asset.media_type;
     var docIconAssset = "";
     if(type == "image"){
      docIconAssset = "image";
     }
     else if(type == "video"){
      docIconAssset = "videocam";
     }
     else{
      docIconAssset = "insert_drive_file";
     }
 {/*<a href={`#` + match}>{`#` + match}</a> //onClick={this.updateText.bind(this, (comment.post.data.tags.data[post.post.data.tags.data.findIndex(x => x.name==match)]?post.post.data.tags.data[post.post.data.tags.data.findIndex(x => x.name==match)].identity:''))}*/}
    return (
      <section className='asset-detail-page'>
      <div id="edit-asset-loader" className='preloader-wrap hide'>
          <PreLoader />
        </div>
        {this.state.editorPopup ? this.renderEditor() : <div />}
        <div id='asset-detail'>
          <header className='heading'>
              <h3>{this.props.asset.title}</h3>
{/*
              <span  onClick = {this.showEditor.bind(this)} className="edit-post-asset-details"><i class="material-icons">&#xE254;</i></span> */}
            </header>
            <PopupWrapper>
            <div className='asset-content-wrapper'>
            {enableGoogleViewer
              ? <div className='google-doc-viewer'>
                <iframe
                  src={`http://docs.google.com/gview?url=${this.props.asset.media_url}&embedded=true`}
                  />
                    </div>
              : ''}
            {enableOfficeViewer
              ? <div className='google-doc-viewer'>
                <iframe
                  src={`https://view.officeapps.live.com/op/embed.aspx?src=${this.props.asset.media_url}`}
                  />
              </div>
              : ''}
              {!enableOfficeViewer && !enableGoogleViewer
              ? <div className="assetDetailImage" ref='assetDetailImage'>
                <Asset
                  key={this.props.asset.identity}
                  name={this.props.asset.title}
                  extension={this.props.asset.media_extension}
                  identity={this.props.asset.identity}
                  media_type={this.props.asset.media_type}
                  media_url={this.props.asset.media_url}
                  thumbnail_url = {this.props.asset.thumbnail_url}
                  approved={this.props.asset.approved}
                  width='640'
                  height='480'
                  detailView
                  />
              </div>
           : ''}
          <div className="author-tag-wrapper">
            <div className='author-data'>
              <div className='author-meta'>
              <span><i class="material-icons asset-title-icon">{docIconAssset}</i></span> Uploaded by <b>{this.props.asset.firstname +' '+ this.props.asset.lastname }</b>
              </div>
              {typeof this.props.asset.created_date.date === 'undefined' &&
                moment !== null
                ? <div
                  className='post-time'
                  title={
                      moment == null
                        ? moment
                            .tz(
                              this.props.asset.created_date,
                              Globals.SERVER_TZ
                            )
                            .format('dddd, Do MMMM  YYYY, h:mm:ss a')
                        : ''
                    }
                  >
                  {moment !== null
                      ? moment
                          .tz(this.props.asset.created_date, Globals.SERVER_TZ)
                          .fromNow()
                      : ''}
                </div>
                : <div
                  className='post-time'
                  title={
                      moment !== null
                        ? moment
                            .tz(
                              this.props.asset.created_date.date,
                              Globals.SERVER_TZ
                            )
                            .format('dddd, Do MMMM YYYY, h:mm:ss a')
                        : ''
                    }
                  >
                  {moment !== null
                      ? moment
                          .tz(
                            this.props.asset.created_date.date,
                            Globals.SERVER_TZ
                          )
                          .fromNow()
                      : ''}
                </div>}

            </div>

            <div className='tags'>
              {replacedDetail}
              {this.renderAssetTags(this.props.asset)}
            </div>

            <div className = "asset-popup-button-download clearfix">
              <a
                href={this.props.asset.media_url}
                style={spacing}
                className='btn btn-primary right'
                download
              >
                Download
              </a>
              {this.props.asset.media_type.match('image.*') ?
               this.props.asset.media_type != "image/gif" ?
               path[3]  ==  "moderation" ?
                ''
                :
               <a
                  id='edit-asset-btn'
                  style={spacing}
                  className='btn btn-theme fb right'
                  onClick= {(path[3] == 'assets' || path[3] == 'campaigns' || path[4] == 'assets') ? this.showEditor.bind(this) : this.showeditordashboard.bind(this,this.props.asset)}
                >
                 Edit
                </a>

             :'' :''}
         </div>
            </div>
            </div>
            </PopupWrapper>


        </div>

      </section>
    )
  }
}

module.exports = AssetDetail
