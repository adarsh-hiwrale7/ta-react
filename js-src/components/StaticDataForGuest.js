import { Link } from 'react-router'
import Header from './Header/Header';
import * as utils from '../utils/utils';
export default class StaticDataForGuest extends React.Component {
componentDidMount(){

}
/**
 * @author disha
 * to display static html data for feed post if user is guest
 */
fetchFeedStaticData(){
    return(
        <div id="feed">
        <div class="clearfix trending-parent ">
        <div class="render-post-feed ">
        <div class="internal">
        <div class="content">
        <div id="post-list-wrapper">
        <div class="feed-post-item clearfix">

    <div id="post-yDazL" class="post-yDazL post">
        <div></div>
        <div class="post-header clearfix ">
            <div class="author-thumb">
                <div class="thumbnail-static"></div>
                <div class="__react_component_tooltip place-top type-dark " data-id="tooltip"></div>
            </div>
            <div class="author-data">
                <div class="author-meta">
                    <b>
                    Jack Sparrow
                    </b>
                    shared
                    <span class="socialChannels"></span>
                </div>
                <div class="post-time">
                    <span class="timeFromNow" title="Monday, January 19th 1970, 12:25:04 am">Friday, September 14th 2018 at 11:24 am</span>
                </div>
                <ul class="dd-menu context-menu">
                    <li class="button-dropdown">
                        <a class="dropdown-toggle" id="dropdown-toggle-yDazL">
                            <i class="material-icons">more_vert</i>
                        </a>
                        <ul class="dropdown-menu hide" id="dropdown-menu-yDazL">
                            <li>
                                <a class="btn-edit-folder">Hide post</a>
                            </li>
                            <li>
                                <a class="btn-edit-folder">Pin on top</a>
                            </li>
                        </ul>
                    </li>
                </ul>

            </div>
        </div>
        <div class="post-detail-pic-wrapper">
            <div id="postdetail" name="postdetail" class="post-title ">
                <div id="detail-yDazL"></div>
                <div class="seeMoreSection" id="yDazL">
                    <div class="seeMoreWrapper">
                        <div class="post-detail-feed-text">

                        </div>

                    </div>
                    <div class="seeMoreLinkWrapper">
                        <a class="seeMoreLink">

                            <i class="fa fa-angle-double-right"></i>
                        </a>
                    </div>
                </div>

            </div>
            <div class="post-pic">
                    <div class="imageloader loaded">
                        <img src="https://s3-us-west-2.amazonaws.com/talent-advocate/VisiblyMedia/13092018214620613.png"/>
                    </div>
                </div>
            <div class="post-meta">
                <div class="like    likeLink " data-postid="yDazL" data-isliked="false">
                    <a class="post-meta-link">
                        <i class="material-icons">thumb_up</i>
                        <span class="likeText">Like</span>
                    </a>
                </div>
                <div class="share">
                    <div class="popup-container"></div>
                </div>
                <div class="commentsLink comments" data-postid="yDazL">
                    <div class="count">
                        <span class="likesCount" data-postid="yDazL">
                            <span class="likesNumber">0</span>
                            <a class="post-meta-link" id="likebox-yDazL" data-like-clicked="false">Like</a>
                        </span>
                        <span class="commentsCount commentsLink" data-postid="yDazL">
                            <span class="commentsNumber">0</span>
                            <a class="post-meta-link" href="#post-yDazL-0">Comment</a>
                        </span>
                    </div>

                </div>
            </div>

        </div>
        <div></div>

    </div>
</div>
        <div class="feed-post-item clearfix">

        <div id="post-yLjgP" class="post-yLjgP post">
            <div></div>
            <div class="post-header clearfix ">
                <div class="author-thumb">
                    <div class="thumbnail-static"></div>
                    <div class="__react_component_tooltip place-top type-dark " data-id="tooltip"></div>
                </div>
                <div class="author-data">
                    <div class="author-meta">

                        <b>Jack Sparrow</b>


                        shared
                        <span class="socialChannels"></span>
                    </div>
                    <div class="post-time">
                        <span class="timeFromNow" title="Monday, January 19th 1970, 12:24:15 am">Thursday, September 13th 2018 at 9:46 pm</span>
                    </div>
                    <ul class="dd-menu context-menu">
                        <li class="button-dropdown">
                            <a class="dropdown-toggle" id="dropdown-toggle-yLjgP">
                                <i class="material-icons">more_vert</i>
                            </a>
                            <ul class="dropdown-menu hide" id="dropdown-menu-yLjgP">
                                <li>
                                    <a class="btn-edit-folder">Hide post</a>
                                </li>
                                <li>
                                    <a class="btn-edit-folder">Pin on top</a>
                                </li>
                            </ul>
                        </li>
                    </ul>

                </div>
            </div>
            <div class="post-detail-pic-wrapper">
                <div id="postdetail" name="postdetail" class="post-title ">
                    <div id="detail-yLjgP"></div>
                    <div class="seeMoreSection" id="yLjgP">
                        <div class="seeMoreWrapper">
                            <div class="post-detail-feed-text">

                                <a>@chris</a>

                            </div>

                        </div>
                        <div class="seeMoreLinkWrapper">
                            <a class="seeMoreLink">

                                <i class="fa fa-angle-double-right"></i>
                            </a>
                        </div>
                    </div>

                </div>
                <div class="post-pic">
                    <div class="imageloader loaded">
                        <img src="https://s3-us-west-2.amazonaws.com/talent-advocate/VisiblyMedia/13092018214620613.png"/>
                    </div>
                </div>
                <div class="post-meta">
                    <div class="like likeLink" data-postid="yLjgP" data-isliked="false">
                        <a class="post-meta-link">
                            <i class="material-icons">thumb_up</i>
                            <span class="likeText">Like</span>
                        </a>
                    </div>
                    <div class="share">
                        <div class="popup-container"></div>
                    </div>
                    <div class="commentsLink comments" data-postid="yLjgP">
                        <div class="count">
                            <span class="likesCount" data-postid="yLjgP">
                                <span class="likesNumber">0</span>
                                <a class="post-meta-link" id="likebox-yLjgP" data-like-clicked="true">Like</a>
                            </span>
                            <span class="commentsCount commentsLink" data-postid="yLjgP">
                                <span class="commentsNumber">0</span>
                                <a class="post-meta-link" href="#post-yLjgP-1">Comment</a>
                            </span>
                        </div>

                    </div>
                </div>

            </div>
            <div></div>
          </div>
          </div>

          <div class="trending-xl-container">
    <div class="trending-tag-container ps">
        <div class="inner-trending-tag">
            <div class="trending-header">
                <h2>TRENDING</h2>
            </div>
            <div class="tranding-body">
                <div class="inner-trending-body">
                    <div class="text-part">
                        <p>
                            <a href="/feed/default/live#Talent">
                            #Talent
                                <span>
                                  15 Mentions
                                </span>
                            </a>
                        </p>
                        <p>
                            <a href="/feed/default/live#Customer">
                            #Customer
                                <span>
                                  1 Mentions
                                </span>
                            </a>
                        </p>
                        <p>
                            <a href="/feed/default/live#chilling">
                            #chilling
                                <span>
                                  1 Mentions
                                </span>
                            </a>
                        </p>
                        <p>
                            <a href="/feed/default/live#lol">
                            #lol
                                <span>
                                  1 Mentions
                                </span>
                            </a>
                        </p>

                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
        </div>
        </div>
        </div>
        </div>
        </div>
    </div>

    )
}
fetchCampaignStaticData(){
    return(
            <div class="CampaignList">
                <div class="table-white-container">
                    <div class="tableControllers clearfix">
                        <div class="tableFilterDropdown">
                            <div class="table-menu dropdown-container">
                                <div class="checkbok-menu-button">
                                    <ul class="dd-menu">
                                        <li class="button-dropdown">
                                            <a class="dropdown-toggle btn btn-theme">Filter columns</a>
                                            <ul class="dropdown-menu checkboxDropdown">
                                                <li>
                                                    <div class="form-row checkbox">
                                                        <input type="checkbox" id="avatar" name="avatar" class="checknotify"
                                                            value="on"/>
                                                        <label for="avatar">Avatar </label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="form-row checkbox">
                                                        <input type="checkbox" id="start_date" name="start_date" class="checknotify"
                                                            value="on"/>
                                                        <label for="start_date">From</label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="form-row checkbox">
                                                        <input type="checkbox" id="end_date" name="end_date" class="checknotify"
                                                            value="on"/>
                                                        <label for="end_date">To</label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="form-row checkbox">
                                                        <input type="checkbox" id="campaign_type" name="campaign_type" class="checknotify"
                                                            value="on"/>
                                                        <label for="campaign_type">Type</label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="form-row checkbox">
                                                        <input type="checkbox" id="title" name="title" class="checknotify" value="on"/>
                                                        <label for="title">Title</label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="form-row checkbox">
                                                        <input type="checkbox" id="targeted_audience" name="targeted_audience"
                                                            class="checknotify" value="on"/>
                                                        <label for="targeted_audience">Audience</label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="form-row checkbox">
                                                        <input type="checkbox" id="post_count" name="post_count" class="checknotify"
                                                            value="on"/>
                                                        <label for="post_count">Posts</label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="form-row checkbox">
                                                        <input type="checkbox" id="asset_count" name="asset_count" class="checknotify"
                                                            value="on"/>
                                                        <label for="asset_count">Assets</label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="form-row checkbox">
                                                        <input type="checkbox" id="channels" name="channels" class="checknotify"
                                                            value="on"/>
                                                        <label for="channels">Channels</label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="form-row checkbox">
                                                        <input type="checkbox" id="actiondiv" name="actiondiv" class="checknotify"
                                                            value="on"/>
                                                        <label for="actiondiv">Actions</label>
                                                    </div>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="tableSearchbox">
                            <input type="text" placeholder="Search in campaign"/>
                            <i class="material-icons">search</i>
                        </div>
                    </div>
                    <div class="widget">
                        <div class="table-wrapper">
                            <table class="table responsive-table campaign-table" id="campaign-table">
                                <thead>
                                    <tr class="reactable-column-header">
                                        <th class="reactable-th-avatar avatar  a-center" tabindex="0"></th>
                                        <th class="reactable-th-start_date start_date  a-center" tabindex="0">
                                            <strong class="name-header">From</strong>
                                        </th>
                                        <th class="reactable-th-end_date end_date  " tabindex="0">
                                            <strong class="name-header">To</strong>
                                        </th>
                                        <th class="reactable-th-campaign_type campaign_type  " tabindex="0">
                                            <strong class="name-header">Type</strong>
                                        </th>
                                        <th class="reactable-th-title title  " tabindex="0">
                                            <strong class="name-header">Title</strong>
                                        </th>
                                        <th class="reactable-th-targeted_audience targeted_audience  " tabindex="0">
                                            <strong class="name-header">Audience</strong>
                                        </th>
                                        <th class="reactable-th-post_count post_count  a-center" tabindex="0">
                                            <strong class="name-header">Posts</strong>
                                        </th>
                                        <th class="reactable-th-asset_count asset_count  a-center" tabindex="0">
                                            <strong class="name-header">Assets</strong>
                                        </th>
                                        <th class="reactable-th-channels channels  a-center" tabindex="0">
                                            <strong class="name-header">Channels</strong>
                                        </th>
                                        <th class="reactable-th-actiondiv actiondiv  a-center" tabindex="0">
                                            <strong class="name-header">Actions</strong>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody class="reactable-data">
                                    <tr class="table-body-text">
                                        <td class="avatar  a-center" data-rwd-label="Avatar " label="">
                                            <div class="campaignTdDiv">
                                                <div>
                                                    <div class="background-user-popup-static" >
                                                        <img src="https://s3-us-west-2.amazonaws.com/talent-advocate/pirates_com/1/07092018094945624.png"
                                                            alt="https://s3-us-west-2.amazonaws.com/talent-advocate/pirates_com/1/07092018094945624.png"
                                                            class="list-avatar" data-tip="jacksparrow@pirates.com" data-for="rpPDp"
                                                            currentitem="false"/>
                                                    </div>
                                                    <div class="__react_component_tooltip place-top type-dark " id="rpPDp"
                                                        data-id="tooltip"></div>
                                                </div>
                                            </div>
                                        </td>
                                        <td class="start_date  a-center" data-rwd-label="From" label="[object Object]">
                                            <div class="campaignTdDiv">05-09-2018</div>
                                        </td>
                                        <td class="end_date  " data-rwd-label="To" label="[object Object]">
                                            <div class="campaignTdDiv">04-10-2018</div>
                                        </td>
                                        <td class="campaign_type  " data-rwd-label="Type" label="[object Object]">
                                            <div class="campaignTdDiv">Internal</div>
                                        </td>
                                        <td class="title  " data-rwd-label="Title" label="[object Object]">
                                            <div class="campaignTdDiv">
                                                <span class="tableOneLine">Techfest</span>
                                            </div>
                                        </td>
                                        <td class="targeted_audience  " data-rwd-label="Audience" label="[object Object]">
                                            <div class="campaignTdDiv">
                                                <span class="tableOneLine">mjnkm</span>
                                            </div>
                                        </td>
                                        <td class="post_count  a-center" data-rwd-label="Posts" label="[object Object]">
                                            <div class="campaignTdDiv">0</div>
                                        </td>
                                        <td class="asset_count  a-center" data-rwd-label="Assets" label="[object Object]">
                                            <div class="campaignTdDiv">0</div>
                                        </td>
                                        <td class="channels  a-center" data-rwd-label="Channels" label="[object Object]">
                                            <div class="campaignTdDiv">
                                                <span class="socialAccountsListing">
                                                    <a class="social iw">
                                                        <i class="material-icons">vpn_lock</i>
                                                    </a>
                                                </span>
                                            </div>
                                        </td>
                                        <td class="actiondiv  a-center" data-rwd-label="Actions" label="[object Object]">
                                            <div class="action-button">
                                                <ul class="dd-menu context-menu user_3dots">
                                                    <li class="button-dropdown">
                                                        <a class="dropdown-toggle user_drop_toggle">
                                                            <i class="material-icons">more_vert</i>
                                                        </a>
                                                        <ul class="dropdown-menu user_dropAction department-dropdown">
                                                            <li>
                                                                <span>Edit</span>
                                                            </li>
                                                            <li>
                                                                <span>
                                                                    1349

                                                                    1350

                                                                </span>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr class="table-body-text">
                                        <td class="avatar  a-center" data-rwd-label="Avatar " label="">
                                            <div class="campaignTdDiv">
                                                <div>
                                                    <div class="background-user-popup-static">
                                                        <img src="https://s3-us-west-2.amazonaws.com/talent-advocate/pirates_com/1/07092018094945624.png"
                                                            alt="https://s3-us-west-2.amazonaws.com/talent-advocate/pirates_com/1/07092018094945624.png"
                                                            class="list-avatar" data-tip="jacksparrow@pirates.com" data-for="rpPDp"
                                                            currentitem="false"/>
                                                    </div>
                                                    <div class="__react_component_tooltip place-top type-dark " id="rpPDp"
                                                        data-id="tooltip"></div>
                                                </div>
                                            </div>
                                        </td>
                                        <td class="start_date  a-center" data-rwd-label="From" label="[object Object]">
                                            <div class="campaignTdDiv">04-09-2018</div>
                                        </td>
                                        <td class="end_date  " data-rwd-label="To" label="[object Object]">
                                            <div class="campaignTdDiv">06-10-2018</div>
                                        </td>
                                        <td class="campaign_type  " data-rwd-label="Type" label="[object Object]">
                                            <div class="campaignTdDiv">Customer</div>
                                        </td>
                                        <td class="title  " data-rwd-label="Title" label="[object Object]">
                                            <div class="campaignTdDiv">
                                                <span class="tableOneLine">Make Money</span>
                                            </div>
                                        </td>
                                        <td class="targeted_audience  " data-rwd-label="Audience" label="[object Object]">
                                            <div class="campaignTdDiv">
                                                <span class="tableOneLine"></span>
                                            </div>
                                        </td>
                                        <td class="post_count  a-center" data-rwd-label="Posts" label="[object Object]">
                                            <div class="campaignTdDiv">5</div>
                                        </td>
                                        <td class="asset_count  a-center" data-rwd-label="Assets" label="[object Object]">
                                            <div class="campaignTdDiv">0</div>
                                        </td>
                                        <td class="channels  a-center" data-rwd-label="Channels" label="[object Object]">
                                            <div class="campaignTdDiv">
                                                <span class="socialAccountsListing">
                                                    <a class="social tw">
                                                        <i class="fa fa-twitter" aria-hidden="true"></i>
                                                    </a>
                                                    <a class="social linkedin">
                                                        <i class="fa fa-linkedin" aria-hidden="true"></i>
                                                    </a>
                                                </span>
                                            </div>
                                        </td>
                                        <td class="actiondiv  a-center" data-rwd-label="Actions" label="[object Object]">
                                            <div class="action-button">
                                                <ul class="dd-menu context-menu user_3dots">
                                                    <li class="button-dropdown">
                                                        <a class="dropdown-toggle user_drop_toggle">
                                                            <i class="material-icons">more_vert</i>
                                                        </a>
                                                        <ul class="dropdown-menu user_dropAction department-dropdown">
                                                            <li>
                                                                <span>Edit</span>
                                                            </li>
                                                            <li>
                                                                <span>
                                                                     1392
                                                                     1393

                                                                </span>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr class="table-body-text">
                                        <td class="avatar  a-center" data-rwd-label="Avatar " label="">
                                            <div class="campaignTdDiv">
                                                <div>
                                                    <div class="background-user-popup-static">
                                                        <img src="https://s3-us-west-2.amazonaws.com/talent-advocate/pirates_com/1/07092018094945624.png"
                                                            alt="https://s3-us-west-2.amazonaws.com/talent-advocate/pirates_com/1/07092018094945624.png"
                                                            class="list-avatar" data-tip="jacksparrow@pirates.com" data-for="rpPDp"
                                                            currentitem="false"/>
                                                    </div>
                                                    <div class="__react_component_tooltip place-top type-dark " id="rpPDp"
                                                        data-id="tooltip"></div>
                                                </div>
                                            </div>
                                        </td>
                                        <td class="start_date  a-center" data-rwd-label="From" label="[object Object]">
                                            <div class="campaignTdDiv">14-07-2018</div>
                                        </td>
                                        <td class="end_date  " data-rwd-label="To" label="[object Object]">
                                            <div class="campaignTdDiv">22-11-2018</div>
                                        </td>
                                        <td class="campaign_type  " data-rwd-label="Type" label="[object Object]">
                                            <div class="campaignTdDiv">Internal</div>
                                        </td>
                                        <td class="title  " data-rwd-label="Title" label="[object Object]">
                                            <div class="campaignTdDiv">
                                                <span class="tableOneLine">Chemistry in the cloud</span>
                                            </div>
                                        </td>
                                        <td class="targeted_audience  " data-rwd-label="Audience" label="[object Object]">
                                            <div class="campaignTdDiv">
                                                <span class="tableOneLine"></span>
                                            </div>
                                        </td>
                                        <td class="post_count  a-center" data-rwd-label="Posts" label="[object Object]">
                                            <div class="campaignTdDiv">5</div>
                                        </td>
                                        <td class="asset_count  a-center" data-rwd-label="Assets" label="[object Object]">
                                            <div class="campaignTdDiv">4</div>
                                        </td>
                                        <td class="channels  a-center" data-rwd-label="Channels" label="[object Object]">
                                            <div class="campaignTdDiv">
                                                <span class="socialAccountsListing">
                                                    <a class="social iw">
                                                        <i class="material-icons">vpn_lock</i>
                                                    </a>
                                                </span>
                                            </div>
                                        </td>
                                        <td class="actiondiv  a-center" data-rwd-label="Actions" label="[object Object]">
                                            <div class="action-button">
                                                <ul class="dd-menu context-menu user_3dots">
                                                    <li class="button-dropdown">
                                                        <a class="dropdown-toggle user_drop_toggle">
                                                            <i class="material-icons">more_vert</i>
                                                        </a>
                                                        <ul class="dropdown-menu user_dropAction department-dropdown">
                                                            <li>
                                                                <span>Edit</span>
                                                            </li>
                                                            <li>
                                                                <span>
                                                                    1433
                                                                    1434

                                                                </span>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                                <tbody class="reactable-pagination">
                                    <tr>
                                        <td colspan="10">
                                            <a class="reactable-page-button reactable-current-page" href="#page-1">1</a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
    )
}
fetchLeaderboardStaticData(){
    return(
<div class="esc-sm">

        <div class="leaderboard-wrap">
            <div id="leaderboard">
                <div class="container">
                    <div data-statsidentity="Opjmp" class="innerdiv-main  current-user-wrapper clearfix">
                        <div class="current  current-user">
                            <div class="index-property  currentuser">
                                <div class="number-id">1</div>
                            </div>
                            <div class="text-part  currentuser-textprt">
                                <div class="inner-text-part-container clearfix">
                                    <div class="flex-item-text-part clearfix leaderboard-name-col">
                                        <div class="img"></div>
                                        <div class="text-header">
                                            <div class="name">
                                               Jack
                                               Sparrow

                                            </div>
                                            <div class="name2">others hr</div>
                                        </div>
                                    </div>
                                    <div class="flex-item-text-part clearfix leaderboard-number-col">
                                        <div class="edit">
                                           1248
                                            <span> POSTS</span>

                                        </div>
                                        <div class="fav">
                                           32905
                                            <span> XP</span>

                                        </div>
                                        <div class="up-aero positive">
                                            <i class="material-icons">arrow_upward</i>

                                            <span class="number">1</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="progressbar">
                                    <div class="start clearfix">
                                        <div class="pg-donet clearfix">
                                            <div class="donet icon-ld">
                                                <i class="material-icons"></i>
                                            </div>
                                            <div class="donet-tx  donet-color-primary">
                                                <p class="tx-ld  donet-color-primary">354</p>
                                                <p class="tx-ld1 donet-color-primary">UPLOADS</p>
                                            </div>
                                        </div>
                                        <div class="pg-donet">
                                            <div class="donet icon-ld">
                                                <i class="material-icons"></i>
                                            </div>
                                            <div class="donet-tx donet-color-theme">
                                                <p class="tx-ld   donet-color-theme">221</p>
                                                <p class="tx-ld1  donet-color-theme">SHARES</p>
                                            </div>
                                        </div>
                                        <div class="pg-donet">
                                            <div class="donet icon-ld">
                                                <i class="material-icons"></i>
                                            </div>
                                            <div class="donet-tx   donet-color-green">
                                                <p class="tx-ld   donet-color-green">270</p>
                                                <p class="tx-ld1  donet-color-green">LIKES</p>
                                            </div>
                                        </div>
                                        <div class="pg-donet">
                                            <div class="donet icon-ld">
                                                <i class="material-icons"></i>
                                            </div>
                                            <div class="donet-tx donet-color-night">
                                                <p class="tx-ld donet-color-night">191</p>
                                                <p class="tx-ld1 donet-color-night">COMMENTS</p>
                                            </div>
                                        </div>
                                        <div class="pg-donet pg-prog">
                                            <p class="donet-color-primary">
                                                <b>
                                                    Approval Rate

                                                    <span class="sp donet-color-theme">
                                                      170%
                                                    </span>
                                                </b>
                                            </p>
                                            <svg class="rc-progress-line progress" viewBox="0 0 100 4" preserveAspectRatio="none">
                                                <path class="rc-progress-line-trail-static" d="M 2,2L 98,2" stroke-linecap="round" stroke="#D9D9D9" stroke-width="1" fill-opacity="0"></path>
                                                <path class="rc-progress-line-path" d="M 2,2"></path>
                                            </svg>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div data-statsidentity="rpPDp" className="innerdiv-main clearfix">
                        <div class="current">
                            <div class="index-property">
                                <div class="number-id">2</div>
                            </div>
                            <div class="text-part">
                                <div class="inner-text-part-container clearfix">
                                    <div class="flex-item-text-part clearfix leaderboard-name-col">
                                        <div class="img"></div>
                                        <div class="text-header">
                                            <div class="name">
                                                Shivangi

                                                Mistry

                                            </div>
                                            <div class="name2">others</div>
                                        </div>
                                    </div>
                                    <div class="flex-item-text-part clearfix leaderboard-number-col">
                                        <div class="edit">
                                           153
                                            <span> POSTS</span>

                                        </div>
                                        <div class="fav">
                                          9715
                                            <span> XP</span>

                                        </div>
                                        <div class="up-aero positive">
                                            <i class="material-icons">arrow_upward</i>

                                            <span class="number">1</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="progressbar">
                                    <div class="start clearfix">
                                        <div class="pg-donet clearfix">
                                            <div class="donet icon-ld">
                                                <i class="material-icons"></i>
                                            </div>
                                            <div class="donet-tx  donet-color-primary">
                                                <p class="tx-ld  donet-color-primary">60</p>
                                                <p class="tx-ld1 donet-color-primary">UPLOADS</p>
                                            </div>
                                        </div>
                                        <div class="pg-donet">
                                            <div class="donet icon-ld">
                                                <i class="material-icons"></i>
                                            </div>
                                            <div class="donet-tx donet-color-theme">
                                                <p class="tx-ld   donet-color-theme">101</p>
                                                <p class="tx-ld1  donet-color-theme">SHARES</p>
                                            </div>
                                        </div>
                                        <div class="pg-donet">
                                            <div class="donet icon-ld">
                                                <i class="material-icons"></i>
                                            </div>
                                            <div class="donet-tx donet-color-green">
                                                <p class="tx-ld   donet-color-green">56</p>
                                                <p class="tx-ld1  donet-color-green">LIKES</p>
                                            </div>
                                        </div>
                                        <div class="pg-donet">
                                            <div class="donet icon-ld">
                                                <i class="material-icons"></i>
                                            </div>
                                            <div class="donet-tx donet-color-night">
                                                <p class="tx-ld donet-color-night">50</p>
                                                <p class="tx-ld1 donet-color-night">COMMENTS</p>
                                            </div>
                                        </div>
                                        <div class="pg-donet pg-prog">
                                            <p class="donet-color-primary">
                                                <b>
                                                    Approval Rate

                                                    <span class="sp donet-color-theme">
                                                        141

                                                        %

                                                    </span>
                                                </b>
                                            </p>
                                            <svg class="rc-progress-line progress" viewBox="0 0 100 4" preserveAspectRatio="none">
                                                <path class="rc-progress-line-trail-static" d="M 2,2
    L 98,2" stroke-linecap="round" stroke="#D9D9D9" stroke-width="1" fill-opacity="0"></path>
                                                <path class="rc-progress-line-path" d="M 2,2
    L 98,2" stroke-linecap="round" stroke="#256eff" stroke-width="4" fill-opacity="0"></path>
                                            </svg>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>


                    <div data-statsidentity="PArdp" class="innerdiv-main clearfix">
                        <div class="current">
                            <div class="index-property">
                                <div class="number-id">3</div>
                            </div>
                            <div class="text-part">
                                <div class="inner-text-part-container clearfix">
                                    <div class="flex-item-text-part clearfix leaderboard-name-col">
                                        <div class="img"></div>
                                        <div class="text-header">
                                            <div class="name">
                                               Vipul testig

                                               patel

                                            </div>
                                            <div class="name2">Technology</div>
                                        </div>
                                    </div>
                                    <div class="flex-item-text-part clearfix leaderboard-number-col">
                                        <div class="edit">
                                            171
                                            <span> POSTS</span>

                                        </div>
                                        <div class="fav">
                                            4181

                                            <span> XP</span>

                                        </div>
                                        <div class="up-aero positive">
                                            <i class="material-icons">arrow_upward</i>

                                            <span class="number">1</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="progressbar">
                                    <div class="start clearfix">
                                        <div class="pg-donet clearfix">
                                            <div class="donet icon-ld">
                                                <i class="material-icons"></i>
                                            </div>
                                            <div class="donet-tx  donet-color-primary">
                                                <p class="tx-ld  donet-color-primary">176</p>
                                                <p class="tx-ld1 donet-color-primary">UPLOADS</p>
                                            </div>
                                        </div>
                                        <div class="pg-donet">
                                            <div class="donet icon-ld">
                                                <i class="material-icons"></i>
                                            </div>
                                            <div class="donet-tx donet-color-theme">
                                                <p class="tx-ld   donet-color-theme">15</p>
                                                <p class="tx-ld1  donet-color-theme">SHARES</p>
                                            </div>
                                        </div>
                                        <div class="pg-donet">
                                            <div class="donet icon-ld">
                                                <i class="material-icons"></i>
                                            </div>
                                            <div class="donet-tx   donet-color-green">
                                                <p class="tx-ld   donet-color-green">22</p>
                                                <p class="tx-ld1  donet-color-green">LIKES</p>
                                            </div>
                                        </div>
                                        <div class="pg-donet">
                                            <div class="donet icon-ld">
                                                <i class="material-icons"></i>
                                            </div>
                                            <div class="donet-tx donet-color-night">
                                                <p class="tx-ld donet-color-night">23</p>
                                                <p class="tx-ld1 donet-color-night">COMMENTS</p>
                                            </div>
                                        </div>
                                        <div class="pg-donet pg-prog">
                                            <p class="donet-color-primary">
                                                <b>
                                                    Approval Rate

                                                    <span class="sp donet-color-theme">
                                                        147

                                                     %

                                                    </span>
                                                </b>
                                            </p>
                                            <svg class="rc-progress-line progress" viewBox="0 0 100 4" preserveAspectRatio="none">
                                                <path class="rc-progress-line-trail-static" d="M 2,2
    L 98,2" stroke-linecap="round" stroke="#D9D9D9" stroke-width="1" fill-opacity="0"></path>
                                                <path class="rc-progress-line-path" d="M 2,2"></path>
                                            </svg>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div data-statsidentity="BymMA" class="innerdiv-main clearfix">
                        <div class="current">
                            <div class="index-property">
                                <div class="number-id">4</div>
                            </div>
                            <div class="text-part">
                                <div class="inner-text-part-container clearfix">
                                    <div class="flex-item-text-part clearfix leaderboard-name-col">
                                        <div class="img"></div>
                                        <div class="text-header">
                                            <div class="name">
                                                Vipul testing sm

                                                patel

                                            </div>
                                            <div class="name2">Technology</div>
                                        </div>
                                    </div>
                                    <div class="flex-item-text-part clearfix leaderboard-number-col">
                                        <div class="edit">
                                            104

                                            <span> POSTS</span>

                                        </div>
                                        <div class="fav">
                                            3217
                                            <span> XP</span>

                                        </div>
                                        <div class="up-aero negative">
                                            <i class="material-icons">arrow_downward</i>

                                            <span class="number">-1</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="progressbar">
                                    <div class="start clearfix">
                                        <div class="pg-donet clearfix">
                                            <div class="donet icon-ld">
                                                <i class="material-icons"></i>
                                            </div>
                                            <div class="donet-tx  donet-color-primary">
                                                <p class="tx-ld  donet-color-primary">203</p>
                                                <p class="tx-ld1 donet-color-primary">UPLOADS</p>
                                            </div>
                                        </div>
                                        <div class="pg-donet">
                                            <div class="donet icon-ld">
                                                <i class="material-icons"></i>
                                            </div>
                                            <div class="donet-tx donet-color-theme">
                                                <p class="tx-ld   donet-color-theme">6</p>
                                                <p class="tx-ld1  donet-color-theme">SHARES</p>
                                            </div>
                                        </div>
                                        <div class="pg-donet">
                                            <div class="donet icon-ld">
                                                <i class="material-icons"></i>
                                            </div>
                                            <div class="donet-tx donet-color-green">
                                                <p class="tx-ld   donet-color-green">19</p>
                                                <p class="tx-ld1  donet-color-green">LIKES</p>
                                            </div>
                                        </div>
                                        <div class="pg-donet">
                                            <div class="donet icon-ld">
                                                <i class="material-icons"></i>
                                            </div>
                                            <div class="donet-tx donet-color-night">
                                                <p class="tx-ld donet-color-night">26</p>
                                                <p class="tx-ld1 donet-color-night">COMMENTS</p>
                                            </div>
                                        </div>
                                        <div class="pg-donet pg-prog">
                                            <p class="donet-color-primary">
                                                <b>
                                                    Approval Rate

                                                    <span class="sp donet-color-theme">
                                                        312

                                                       %

                                                    </span>
                                                </b>
                                            </p>
                                            <svg class="rc-progress-line progress" viewBox="0 0 100 4" preserveAspectRatio="none">
                                                <path class="rc-progress-line-trail-static" d="M 2,2
    L 98,2" stroke-linecap="round" stroke="#D9D9D9" stroke-width="1" fill-opacity="0"></path>
                                                <path class="rc-progress-line-path" d="M 2,2
    L 98,2" stroke-linecap="round" stroke="#256eff" stroke-width="4" fill-opacity="0"></path>
                                            </svg>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div data-statsidentity="MpwEy" class="innerdiv-main clearfix">
                        <div class="current">
                            <div class="index-property">
                                <div class="number-id">5</div>
                            </div>
                            <div class="text-part">
                                <div class="inner-text-part-container clearfix">
                                    <div class="flex-item-text-part clearfix leaderboard-name-col">
                                        <div class="img"></div>
                                        <div class="text-header">
                                            <div class="name">
                                                Shivangi

                                                Employee Account

                                            </div>
                                            <div class="name2">TechnologyNew</div>
                                        </div>
                                    </div>
                                    <div class="flex-item-text-part clearfix leaderboard-number-col">
                                        <div class="edit">
                                           34
                                            <span> POSTS</span>

                                        </div>
                                        <div class="fav">
                                           1165
                                            <span> XP</span>

                                        </div>
                                        <div class="up-aero negative">
                                            <i class="material-icons">arrow_downward</i>

                                            <span class="number">-1</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="progressbar">
                                    <div class="start clearfix">
                                        <div class="pg-donet clearfix">
                                            <div class="donet icon-ld">
                                                <i class="material-icons"></i>
                                            </div>
                                            <div class="donet-tx  donet-color-primary">
                                                <p class="tx-ld  donet-color-primary">17</p>
                                                <p class="tx-ld1 donet-color-primary">UPLOADS</p>
                                            </div>
                                        </div>
                                        <div class="pg-donet">
                                            <div class="donet icon-ld">
                                                <i class="material-icons"></i>
                                            </div>
                                            <div class="donet-tx donet-color-theme">
                                                <p class="tx-ld   donet-color-theme">8</p>
                                                <p class="tx-ld1  donet-color-theme">SHARES</p>
                                            </div>
                                        </div>
                                        <div class="pg-donet">
                                            <div class="donet icon-ld">
                                                <i class="material-icons"></i>
                                            </div>
                                            <div class="donet-tx donet-color-green">
                                                <p class="tx-ld   donet-color-green">9</p>
                                                <p class="tx-ld1  donet-color-green">LIKES</p>
                                            </div>
                                        </div>
                                        <div class="pg-donet">
                                            <div class="donet icon-ld">
                                                <i class="material-icons"></i>
                                            </div>
                                            <div class="donet-tx donet-color-night">
                                                <p class="tx-ld donet-color-night">1</p>
                                                <p class="tx-ld1 donet-color-night">COMMENTS</p>
                                            </div>
                                        </div>
                                        <div class="pg-donet pg-prog">
                                            <p class="donet-color-primary">
                                                <b>
                                                    Approval Rate

                                                    <span class="sp donet-color-theme">
                                                        162

                                                        %

                                                    </span>
                                                </b>
                                            </p>
                                            <svg class="rc-progress-line progress" viewBox="0 0 100 4" preserveAspectRatio="none">
                                                <path class="rc-progress-line-trail-static" d="M 2,2
    L 98,2" stroke-linecap="round" stroke="#D9D9D9" stroke-width="1" fill-opacity="0"></path>
                                                <path class="rc-progress-line-path" d="M 2,2
    L 98,2" stroke-linecap="round" stroke="#256eff" stroke-width="4" fill-opacity="0"></path>
                                            </svg>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div data-statsidentity="myzJy" class="innerdiv-main clearfix">
                        <div class="current">
                            <div class="index-property">
                                <div class="number-id">6</div>
                            </div>
                            <div class="text-part">
                                <div class="inner-text-part-container clearfix">
                                    <div class="flex-item-text-part clearfix leaderboard-name-col">
                                        <div class="img" ></div>
                                        <div class="text-header">
                                            <div class="name">
                                                Jack

                                                sparrow

                                            </div>
                                            <div class="name2">RND</div>
                                        </div>
                                    </div>
                                    <div class="flex-item-text-part clearfix leaderboard-number-col">
                                        <div class="edit">
                                            21

                                            <span> POSTS</span>

                                        </div>
                                        <div class="fav">
                                            764

                                            <span> XP</span>

                                        </div>
                                        <div class="up-aero negative">
                                            <i class="material-icons">arrow_downward</i>

                                            <span class="number">-1</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="progressbar">
                                    <div class="start clearfix">
                                        <div class="pg-donet clearfix">
                                            <div class="donet icon-ld">
                                                <i class="material-icons"></i>
                                            </div>
                                            <div class="donet-tx  donet-color-primary">
                                                <p class="tx-ld  donet-color-primary">44</p>
                                                <p class="tx-ld1 donet-color-primary">UPLOADS</p>
                                            </div>
                                        </div>
                                        <div class="pg-donet">
                                            <div class="donet icon-ld">
                                                <i class="material-icons"></i>
                                            </div>
                                            <div class="donet-tx donet-color-theme">
                                                <p class="tx-ld   donet-color-theme">2</p>
                                                <p class="tx-ld1  donet-color-theme">SHARES</p>
                                            </div>
                                        </div>
                                        <div class="pg-donet">
                                            <div class="donet icon-ld">
                                                <i class="material-icons"></i>
                                            </div>
                                            <div class="donet-tx donet-color-green">
                                                <p class="tx-ld   donet-color-green">2</p>
                                                <p class="tx-ld1  donet-color-green">LIKES</p>
                                            </div>
                                        </div>
                                        <div class="pg-donet">
                                            <div class="donet icon-ld">
                                                <i class="material-icons"></i>
                                            </div>
                                            <div class="donet-tx donet-color-night">
                                                <p class="tx-ld donet-color-night">0</p>
                                                <p class="tx-ld1 donet-color-night">COMMENTS</p>
                                            </div>
                                        </div>
                                        <div class="pg-donet pg-prog">
                                            <p class="donet-color-primary">
                                                <b>
                                                    Approval Rate

                                                    <span class="sp donet-color-theme">
                                                        171

                                                        %

                                                    </span>
                                                </b>
                                            </p>
                                            <svg class="rc-progress-line progress" viewBox="0 0 100 4" preserveAspectRatio="none">
                                                <path class="rc-progress-line-trail-static" d="M 2,2
    L 98,2" stroke-linecap="round" stroke="#D9D9D9" stroke-width="1" fill-opacity="0"></path>
                                                <path class="rc-progress-line-path" d="M 2,2
    L 98,2" stroke-linecap="round" stroke="#256eff" stroke-width="4" fill-opacity="0"></path>
                                            </svg>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div data-statsidentity="PAxwy" class="innerdiv-main clearfix">
                        <div class="current">
                            <div class="index-property">
                                <div class="number-id">7</div>
                            </div>
                            <div class="text-part">
                                <div class="inner-text-part-container clearfix">
                                    <div class="flex-item-text-part clearfix leaderboard-name-col">
                                        <div class="img"></div>
                                        <div class="text-header">
                                            <div class="name">
                                                tejal

                                            kukadiya

                                            </div>
                                            <div class="name2">TechnologyNew</div>
                                        </div>
                                    </div>
                                    <div class="flex-item-text-part clearfix leaderboard-number-col">
                                        <div class="edit">
                                            18

                                            <span> POSTS</span>

                                        </div>
                                        <div class="fav">
                                            362

                                            <span> XP</span>

                                        </div>
                                        <div class="up-aero positive">
                                            <i class="material-icons">arrow_upward</i>

                                            <span class="number">1</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="progressbar">
                                    <div class="start clearfix">
                                        <div class="pg-donet clearfix">
                                            <div class="donet icon-ld">
                                                <i class="material-icons"></i>
                                            </div>
                                            <div class="donet-tx  donet-color-primary">
                                                <p class="tx-ld  donet-color-primary">12</p>
                                                <p class="tx-ld1 donet-color-primary">UPLOADS</p>
                                            </div>
                                        </div>
                                        <div class="pg-donet">
                                            <div class="donet icon-ld">
                                                <i class="material-icons"></i>
                                            </div>
                                            <div class="donet-tx donet-color-theme">
                                                <p class="tx-ld   donet-color-theme">0</p>
                                                <p class="tx-ld1  donet-color-theme">SHARES</p>
                                            </div>
                                        </div>
                                        <div class="pg-donet">
                                            <div class="donet icon-ld">
                                                <i class="material-icons"></i>
                                            </div>
                                            <div class="donet-tx donet-color-green">
                                                <p class="tx-ld   donet-color-green">7</p>
                                                <p class="tx-ld1  donet-color-green">LIKES</p>
                                            </div>
                                        </div>
                                        <div class="pg-donet">
                                            <div class="donet icon-ld">
                                                <i class="material-icons"></i>
                                            </div>
                                            <div class="donet-tx donet-color-night">
                                                <p class="tx-ld donet-color-night">3</p>
                                                <p class="tx-ld1 donet-color-night">COMMENTS</p>
                                            </div>
                                        </div>
                                        <div class="pg-donet pg-prog">
                                            <p class="donet-color-primary">
                                                <b>
                                                    Approval Rate

                                                    <span class="sp donet-color-theme">
                                                        883

                                                        %

                                                    </span>
                                                </b>
                                            </p>
                                            <svg class="rc-progress-line progress" viewBox="0 0 100 4" preserveAspectRatio="none">
                                                <path class="rc-progress-line-trail-static" d="M 2,2
    L 98,2" stroke-linecap="round" stroke="#D9D9D9" stroke-width="1" fill-opacity="0"></path>
                                                <path class="rc-progress-line-path" d="M 2,2
    L 98,2" stroke-linecap="round" stroke="#256eff" stroke-width="4" fill-opacity="0"></path>
                                            </svg>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div data-statsidentity="zyvay" class="innerdiv-main clearfix">
                        <div class="current">
                            <div class="index-property">
                                <div class="number-id">8</div>
                            </div>
                            <div class="text-part">
                                <div class="inner-text-part-container clearfix">
                                    <div class="flex-item-text-part clearfix leaderboard-name-col">
                                        <div class="img"></div>
                                        <div class="text-header">
                                            <div class="name">
                                                akshay

                                                soni

                                            </div>
                                            <div class="name2">TechnologyNew</div>
                                        </div>
                                    </div>
                                    <div class="flex-item-text-part clearfix leaderboard-number-col">
                                        <div class="edit">
                                            8

                                            <span> POSTS</span>

                                        </div>
                                        <div class="fav">
                                            154

                                            <span> XP</span>

                                        </div>
                                        <div class="up-aero positive">
                                            <i class="material-icons">arrow_upward</i>

                                            <span class="number">1</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="progressbar">
                                    <div class="start clearfix">
                                        <div class="pg-donet clearfix">
                                            <div class="donet icon-ld">
                                                <i class="material-icons"></i>
                                            </div>
                                            <div class="donet-tx  donet-color-primary">
                                                <p class="tx-ld  donet-color-primary">0</p>
                                                <p class="tx-ld1 donet-color-primary">UPLOADS</p>
                                            </div>
                                        </div>
                                        <div class="pg-donet">
                                            <div class="donet icon-ld">
                                                <i class="material-icons"></i>
                                            </div>
                                            <div class="donet-tx donet-color-theme">
                                                <p class="tx-ld   donet-color-theme">0</p>
                                                <p class="tx-ld1  donet-color-theme">SHARES</p>
                                            </div>
                                        </div>
                                        <div class="pg-donet">
                                            <div class="donet icon-ld">
                                                <i class="material-icons"></i>
                                            </div>
                                            <div class="donet-tx donet-color-green">
                                                <p class="tx-ld   donet-color-green">2</p>
                                                <p class="tx-ld1  donet-color-green">LIKES</p>
                                            </div>
                                        </div>
                                        <div class="pg-donet">
                                            <div class="donet icon-ld">
                                                <i class="material-icons"></i>
                                            </div>
                                            <div class="donet-tx donet-color-night">
                                                <p class="tx-ld donet-color-night">0</p>
                                                <p class="tx-ld1 donet-color-night">COMMENTS</p>
                                            </div>
                                        </div>
                                        <div class="pg-donet pg-prog">
                                            <p class="donet-color-primary">
                                                <b>
                                                    Approval Rate

                                                    <span class="sp donet-color-theme">
                                                        100

                                                        %

                                                    </span>
                                                </b>
                                            </p>
                                            <svg class="rc-progress-line progress" viewBox="0 0 100 4" preserveAspectRatio="none">
                                                <path class="rc-progress-line-trail-static" d="M 2,2
    L 98,2" stroke-linecap="round" stroke="#D9D9D9" stroke-width="1" fill-opacity="0"></path>
                                                <path class="rc-progress-line-path" d="M 2,2
           L 98,2" stroke-linecap="round" stroke="#256eff" stroke-width="4" fill-opacity="0"></path>
                                            </svg>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div data-statsidentity="kABzy" class="innerdiv-main clearfix">
                        <div class="current">
                            <div class="index-property">
                                <div class="number-id">9</div>
                            </div>
                            <div class="text-part">
                                <div class="inner-text-part-container clearfix">
                                    <div class="flex-item-text-part clearfix leaderboard-name-col">
                                        <div class="img"></div>
                                        <div class="text-header">
                                            <div class="name">
                                                sadikali

                                                dantreliya

                                            </div>
                                            <div class="name2">TechnologyNew</div>
                                        </div>
                                    </div>
                                    <div class="flex-item-text-part clearfix leaderboard-number-col">
                                        <div class="edit">
                                            5

                                            <span> POSTS</span>

                                        </div>
                                        <div class="fav">
                                            140

                                            <span> XP</span>

                                        </div>
                                        <div class="up-aero negative">
                                            <i class="material-icons">arrow_downward</i>

                                            <span class="number">-1</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="progressbar">
                                    <div class="start clearfix">
                                        <div class="pg-donet clearfix">
                                            <div class="donet icon-ld">
                                                <i class="material-icons"></i>
                                            </div>
                                            <div class="donet-tx  donet-color-primary">
                                                <p class="tx-ld  donet-color-primary">3</p>
                                                <p class="tx-ld1 donet-color-primary">UPLOADS</p>
                                            </div>
                                        </div>
                                        <div class="pg-donet">
                                            <div class="donet icon-ld">
                                                <i class="material-icons"></i>
                                            </div>
                                            <div class="donet-tx donet-color-theme">
                                                <p class="tx-ld   donet-color-theme">0</p>
                                                <p class="tx-ld1  donet-color-theme">SHARES</p>
                                            </div>
                                        </div>
                                        <div class="pg-donet">
                                            <div class="donet icon-ld">
                                                <i class="material-icons"></i>
                                            </div>
                                            <div class="donet-tx donet-color-green">
                                                <p class="tx-ld   donet-color-green">0</p>
                                                <p class="tx-ld1  donet-color-green">LIKES</p>
                                            </div>
                                        </div>
                                        <div class="pg-donet">
                                            <div class="donet icon-ld">
                                                <i class="material-icons"></i>
                                            </div>
                                            <div class="donet-tx donet-color-night">
                                                <p class="tx-ld donet-color-night">0</p>
                                                <p class="tx-ld1 donet-color-night">COMMENTS</p>
                                            </div>
                                        </div>
                                        <div class="pg-donet pg-prog">
                                            <p class="donet-color-primary">
                                                <b>
                                                    Approval Rate

                                                    <span class="sp donet-color-theme">
                                                        380

                                                        %

                                                    </span>
                                                </b>
                                            </p>
                                            <svg class="rc-progress-line progress" viewBox="0 0 100 4" preserveAspectRatio="none">
                                                <path class="rc-progress-line-trail-static" d="M 2,2
    L 98,2" stroke-linecap="round" stroke="#D9D9D9" stroke-width="1" fill-opacity="0"></path>
                                                <path class="rc-progress-line-path" d="M 2,2
    L 98,2" stroke-linecap="round" stroke="#256eff" stroke-width="4" fill-opacity="0"></path>
                                            </svg>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div data-statsidentity="RpGQy" class="innerdiv-main clearfix">
                        <div class="current">
                            <div class="index-property">
                                <div class="number-id">10</div>
                            </div>
                            <div class="text-part">
                                <div class="inner-text-part-container clearfix">
                                    <div class="flex-item-text-part clearfix leaderboard-name-col">
                                        <div class="img"></div>
                                        <div class="text-header">
                                            <div class="name">
                                                archa

                                                khandelwal

                                            </div>
                                            <div class="name2">TechnologyNew</div>
                                        </div>
                                    </div>
                                    <div class="flex-item-text-part clearfix leaderboard-number-col">
                                        <div class="edit">
                                            5

                                            <span> POSTS</span>

                                        </div>
                                        <div class="fav">
                                            70

                                            <span> XP</span>

                                        </div>
                                        <div class="up-aero positive">
                                            <i class="material-icons">arrow_upward</i>

                                            <span class="number">1</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="progressbar">
                                    <div class="start clearfix">
                                        <div class="pg-donet clearfix">
                                            <div class="donet icon-ld">
                                                <i class="material-icons"></i>
                                            </div>
                                            <div class="donet-tx donet-color-primary">
                                                <p class="tx-ld  donet-color-primary">0</p>
                                                <p class="tx-ld1 donet-color-primary">UPLOADS</p>
                                            </div>
                                        </div>
                                        <div class="pg-donet">
                                            <div class="donet icon-ld">
                                                <i class="material-icons"></i>
                                            </div>
                                            <div class="donet-tx donet-color-theme">
                                                <p class="tx-ld   donet-color-theme">0</p>
                                                <p class="tx-ld1  donet-color-theme">SHARES</p>
                                            </div>
                                        </div>
                                        <div class="pg-donet">
                                            <div class="donet icon-ld">
                                                <i class="material-icons"></i>
                                            </div>
                                            <div class="donet-tx donet-color-green">
                                                <p class="tx-ld   donet-color-green">0</p>
                                                <p class="tx-ld1  donet-color-green">LIKES</p>
                                            </div>
                                        </div>
                                        <div class="pg-donet">
                                            <div class="donet icon-ld">
                                                <i class="material-icons"></i>
                                            </div>
                                            <div class="donet-tx donet-color-night">
                                                <p class="tx-ld donet-color-night">0</p>
                                                <p class="tx-ld1 donet-color-night">COMMENTS</p>
                                            </div>
                                        </div>
                                        <div class="pg-donet pg-prog">
                                            <p class="donet-color-primary">
                                                <b>
                                                    Approval Rate

                                                    <span class="sp donet-color-theme">
                                                        180

                                                        %

                                                    </span>
                                                </b>
                                            </p>
                                            <svg class="rc-progress-line progress" viewBox="0 0 100 4" preserveAspectRatio="none">
                                                <path class="rc-progress-line-trail-static" d="M 2,2
    L 98,2" stroke-linecap="round" stroke="#D9D9D9" stroke-width="1" fill-opacity="0"></path>
                                                <path class="rc-progress-line-path" d="M 2,2
    L 98,2" stroke-linecap="round" stroke="#256eff" stroke-width="4" fill-opacity="0"></path>
                                            </svg>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    )
}
fetchAnalyticsAssetStaticData(){
    return(
<div class="anylitics-content-main">
    <div class="anytics_container">
        <div>
            <div class="main-container-body-anytlics">
                <div>
                    <div>
                        <div class="chartColsWrapper clearfix">
                            <div class="chartTabContentCol">
                                <div class="chart-container">
                                    <div class="chartdiv-section">
                                        <div class="chart-section">
                                            <div class="chartdiv-wrapper">
                                            <img src='/img/assetchart.png'/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="chartTabbingCol clearfix">
                                <div class="chartTabbingBtn lineChartTab active ">
                                    <div class="chartTabWrapper">
                                        <div class="chartIcon">
                                            <div class="chartBorder"></div>
                                        </div>
                                        <div class="chartTabTitle">Line Chart</div>
                                    </div>
                                </div>
                                <div class="chartTabbingBtn doughnutChartTab  ">
                                    <div class="chartTabWrapper">
                                        <div class="chartIcon"></div>
                                        <div class="chartTabTitle">Doughnut Chart</div>
                                    </div>
                                </div>
                                <div class="chartTabbingBtn barChartTab  ">
                                    <div class="chartTabWrapper">
                                        <div class="chartIcon">
                                            <div class="chartBorder"></div>
                                        </div>
                                        <div class="chartTabTitle">Bar Chart</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="main-container-body-anytlics">
                <div>
                    <div class="tabel-view-anylitcs">
                        <div class="table-menu dropudown-container">
                            <div class="checkbok-menu-button">
                                <ul class="dd-menu">
                                    <li class="button-dropdown">
                                        <a class="dropdown-toggle btn btn-theme">Filter columns</a>
                                        <ul class="dropdown-menu checkboxDropdown">
                                            <li>
                                                <div class="form-row checkbox">
                                                    <input type="checkbox" id="serialno" name="serialno" class="checknotify"
                                                        value="on"/>
                                                    <label for="serialno">#</label>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="form-row checkbox">
                                                    <input type="checkbox" id="detail" name="detail" class="checknotify" value="on"/>
                                                    <label for="detail">Asset detail</label>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="form-row checkbox">
                                                    <input type="checkbox" id="category" name="category" class="checknotify"
                                                        value="on"/>
                                                    <label for="category">Category</label>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="form-row checkbox">
                                                    <input type="checkbox" id="date" name="date" class="checknotify" value="on"/>
                                                    <label for="date">Date</label>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="form-row checkbox">
                                                    <input type="checkbox" id="department" name="department" class="checknotify"
                                                        value="on"/>
                                                    <label for="department">Department</label>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="form-row checkbox">
                                                    <input type="checkbox" id="media_type" name="media_type" class="checknotify"
                                                        value="on"/>
                                                    <label for="media_type">File type</label>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="form-row checkbox">
                                                    <input type="checkbox" id="title" name="title" class="checknotify" value="on"/>
                                                    <label for="title">Title</label>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="form-row checkbox">
                                                    <input type="checkbox" id="uploaded_by" name="uploaded_by" class="checknotify"
                                                        value="on"/>
                                                    <label for="uploaded_by">Uploaded by</label>
                                                </div>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div>

                            <div class="widget">
                                <div class="table-anytlics-body">
                                    <table class="table responsive-table">
                                        <thead>
                                            <tr class="reactable-column-header">
                                                <th class="reactable-th-serialno reactable-header-sortable  serialno  a-center"
                                                    role="button" tabindex="0">
                                                    <strong class="name-header">#</strong>
                                                    <i class="fa fa-sort"></i>
                                                </th>
                                                <th class="reactable-th-detail reactable-header-sortable  detail  " role="button"
                                                    tabindex="0">
                                                    <strong class="name-header">Asset detail</strong>
                                                    <i class="fa fa-sort"></i>
                                                </th>
                                                <th class="reactable-th-category reactable-header-sortable  category  " role="button"
                                                    tabindex="0">
                                                    <strong class="name-header">Category</strong>
                                                    <i class="fa fa-sort"></i>
                                                </th>
                                                <th class="reactable-th-date reactable-header-sortable  date  a-center" role="button"
                                                    tabindex="0">
                                                    <strong class="name-header">Date</strong>
                                                    <i class="fa fa-sort"></i>
                                                </th>
                                                <th class="reactable-th-department reactable-header-sortable  department  "
                                                    role="button" tabindex="0">
                                                    <strong class="name-header">Department</strong>
                                                    <i class="fa fa-sort"></i>
                                                </th>
                                                <th class="reactable-th-media_type reactable-header-sortable  media_type  a-center"
                                                    role="button" tabindex="0">
                                                    <strong class="name-header">File type</strong>
                                                    <i class="fa fa-sort"></i>
                                                </th>
                                                <th class="reactable-th-title reactable-header-sortable  title  " role="button"
                                                    tabindex="0">
                                                    <strong class="name-header">Title</strong>
                                                    <i class="fa fa-sort"></i>
                                                </th>
                                                <th class="reactable-th-uploaded_by reactable-header-sortable  uploaded_by  "
                                                    role="button" tabindex="0">
                                                    <strong class="name-header">Uploaded by</strong>
                                                    <i class="fa fa-sort"></i>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody class="reactable-data">
                                            <tr class="table-body-text">
                                                <td class="serialno  a-center" data-rwd-label="#" label="[object Object],[object Object]">1</td>
                                                <td class="detail  " data-rwd-label="Asset detail" label="[object Object],[object Object]">video</td>
                                                <td class="category  " data-rwd-label="Category" label="[object Object],[object Object]">General</td>
                                                <td class="date  a-center" data-rwd-label="Date" label="[object Object],[object Object]">09-04-2018</td>
                                                <td class="department  " data-rwd-label="Department" label="[object Object],[object Object]">Information Technolgy</td>
                                                <td class="media_type  a-center" data-rwd-label="File type"
                                                    label="[object Object],[object Object]">video/mp4</td>
                                                <td class="title  " data-rwd-label="Title" label="[object Object],[object Object]">small</td>
                                                <td class="uploaded_by  " data-rwd-label="Uploaded by" label="[object Object],[object Object]">Vipul pirate</td>
                                            </tr>
                                            <tr class="table-body-text">
                                                <td class="serialno  a-center" data-rwd-label="#" label="[object Object],[object Object]">2</td>
                                                <td class="detail  " data-rwd-label="Asset detail" label="[object Object],[object Object]">#holi</td>
                                                <td class="category  " data-rwd-label="Category" label="[object Object],[object Object]">General</td>
                                                <td class="date  a-center" data-rwd-label="Date" label="[object Object],[object Object]">09-04-2018</td>
                                                <td class="department  " data-rwd-label="Department" label="[object Object],[object Object]">Information Technolgy</td>
                                                <td class="media_type  a-center" data-rwd-label="File type"
                                                    label="[object Object],[object Object]">image/png</td>
                                                <td class="title  " data-rwd-label="Title" label="[object Object],[object Object]">holi_3</td>
                                                <td class="uploaded_by  " data-rwd-label="Uploaded by" label="[object Object],[object Object]">Vipul pirate</td>
                                            </tr>
                                            <tr class="table-body-text">
                                                <td class="serialno  a-center" data-rwd-label="#" label="[object Object],[object Object]">3</td>
                                                <td class="detail  " data-rwd-label="Asset detail" label="[object Object],[object Object]">#team</td>
                                                <td class="category  " data-rwd-label="Category" label="[object Object],[object Object]">Events</td>
                                                <td class="date  a-center" data-rwd-label="Date" label="[object Object],[object Object]">09-04-2018</td>
                                                <td class="department  " data-rwd-label="Department" label="[object Object],[object Object]">Human Resource11</td>
                                                <td class="media_type  a-center" data-rwd-label="File type"
                                                    label="[object Object],[object Object]">image/png</td>
                                                <td class="title  " data-rwd-label="Title" label="[object Object],[object Object]">holi_3</td>
                                                <td class="uploaded_by  " data-rwd-label="Uploaded by" label="[object Object],[object Object]">Jack Sparrow</td>
                                            </tr>
                                            <tr class="table-body-text">
                                                <td class="serialno  a-center" data-rwd-label="#" label="[object Object],[object Object]">4</td>
                                                <td class="detail  " data-rwd-label="Asset detail" label="[object Object],[object Object]">#awesome</td>
                                                <td class="category  " data-rwd-label="Category" label="[object Object],[object Object]">Archive</td>
                                                <td class="date  a-center" data-rwd-label="Date" label="[object Object],[object Object]">09-04-2018</td>
                                                <td class="department  " data-rwd-label="Department" label="[object Object],[object Object]">Human Resource11</td>
                                                <td class="media_type  a-center" data-rwd-label="File type"
                                                    label="[object Object],[object Object]">image/png</td>
                                                <td class="title  " data-rwd-label="Title" label="[object Object],[object Object]">holi_2</td>
                                                <td class="uploaded_by  " data-rwd-label="Uploaded by" label="[object Object],[object Object]">Jack Sparrow</td>
                                            </tr>
                                            <tr class="table-body-text">
                                                <td class="serialno  a-center" data-rwd-label="#" label="[object Object],[object Object]">5</td>
                                                <td class="detail  " data-rwd-label="Asset detail" label="[object Object],[object Object]">#cool</td>
                                                <td class="category  " data-rwd-label="Category" label="[object Object],[object Object]">Archive</td>
                                                <td class="date  a-center" data-rwd-label="Date" label="[object Object],[object Object]">09-04-2018</td>
                                                <td class="department  " data-rwd-label="Department" label="[object Object],[object Object]">Human Resource11</td>
                                                <td class="media_type  a-center" data-rwd-label="File type"
                                                    label="[object Object],[object Object]">image/png</td>
                                                <td class="title  " data-rwd-label="Title" label="[object Object],[object Object]">holi_1</td>
                                                <td class="uploaded_by  " data-rwd-label="Uploaded by" label="[object Object],[object Object]">Jack Sparrow</td>
                                            </tr>
                                            <tr class="table-body-text">
                                                <td class="serialno  a-center" data-rwd-label="#" label="[object Object],[object Object]">6</td>
                                                <td class="detail  " data-rwd-label="Asset detail" label="[object Object],[object Object]">#cool</td>
                                                <td class="category  " data-rwd-label="Category" label="[object Object],[object Object]">Archive</td>
                                                <td class="date  a-center" data-rwd-label="Date" label="[object Object],[object Object]">12-04-2018</td>
                                                <td class="department  " data-rwd-label="Department" label="[object Object],[object Object]">Human Resource11</td>
                                                <td class="media_type  a-center" data-rwd-label="File type"
                                                    label="[object Object],[object Object]">image/png</td>
                                                <td class="title  " data-rwd-label="Title" label="[object Object],[object Object]">holi_2</td>
                                                <td class="uploaded_by  " data-rwd-label="Uploaded by" label="[object Object],[object Object]">Jack Sparrow</td>
                                            </tr>
                                            <tr class="table-body-text">
                                                <td class="serialno  a-center" data-rwd-label="#" label="[object Object],[object Object]">7</td>
                                                <td class="detail  " data-rwd-label="Asset detail" label="[object Object],[object Object]">#team</td>
                                                <td class="category  " data-rwd-label="Category" label="[object Object],[object Object]">Archive</td>
                                                <td class="date  a-center" data-rwd-label="Date" label="[object Object],[object Object]">12-04-2018</td>
                                                <td class="department  " data-rwd-label="Department" label="[object Object],[object Object]">Human Resource11</td>
                                                <td class="media_type  a-center" data-rwd-label="File type"
                                                    label="[object Object],[object Object]">image/png</td>
                                                <td class="title  " data-rwd-label="Title" label="[object Object],[object Object]">holi_1</td>
                                                <td class="uploaded_by  " data-rwd-label="Uploaded by" label="[object Object],[object Object]">Jack Sparrow</td>
                                            </tr>
                                            <tr class="table-body-text">
                                                <td class="serialno  a-center" data-rwd-label="#" label="[object Object],[object Object]">8</td>
                                                <td class="detail  " data-rwd-label="Asset detail" label="[object Object],[object Object]">#work</td>
                                                <td class="category  " data-rwd-label="Category" label="[object Object],[object Object]">Archive</td>
                                                <td class="date  a-center" data-rwd-label="Date" label="[object Object],[object Object]">12-04-2018</td>
                                                <td class="department  " data-rwd-label="Department" label="[object Object],[object Object]">Human Resource11</td>
                                                <td class="media_type  a-center" data-rwd-label="File type"
                                                    label="[object Object],[object Object]">image/jpeg; charset=UTF-8</td>
                                                <td class="title  " data-rwd-label="Title"
                                                    label="[object Object],[object Object]">Work</td>
                                                <td class="uploaded_by  " data-rwd-label="Uploaded by" label="[object Object],[object Object]">Jack Sparrow</td>
                                            </tr>
                                            <tr class="table-body-text">
                                                <td class="serialno  a-center" data-rwd-label="#" label="[object Object],[object Object]">9</td>
                                                <td class="detail  " data-rwd-label="Asset detail" label="[object Object],[object Object]">#conference</td>
                                                <td class="category  " data-rwd-label="Category" label="[object Object],[object Object]">General</td>
                                                <td class="date  a-center" data-rwd-label="Date" label="[object Object],[object Object]">12-04-2018</td>
                                                <td class="department  " data-rwd-label="Department" label="[object Object],[object Object]">Human Resource11</td>
                                                <td class="media_type  a-center" data-rwd-label="File type"
                                                    label="[object Object],[object Object]">image/jpeg</td>
                                                <td class="title  " data-rwd-label="Title" label="[object Object],[object Object]">Thought Leadership</td>
                                                <td class="uploaded_by  " data-rwd-label="Uploaded by"
                                                    label="[object Object],[object Object]">Jack Sparrow</td>
                                            </tr>
                                            <tr class="table-body-text">
                                                <td class="serialno  a-center" data-rwd-label="#" label="[object Object],[object Object]">10</td>
                                                <td class="detail  " data-rwd-label="Asset detail" label="[object Object],[object Object]">Discussion Q-3</td>
                                                <td class="category  " data-rwd-label="Category" label="[object Object],[object Object]">Archive</td>
                                                <td class="date  a-center" data-rwd-label="Date" label="[object Object],[object Object]">12-04-2018</td>
                                                <td class="department  " data-rwd-label="Department" label="[object Object],[object Object]">Human Resource11</td>
                                                <td class="media_type  a-center" data-rwd-label="File type"
                                                    label="[object Object],[object Object]">image/jpeg; charset=UTF-8</td>
                                                <td class="title  " data-rwd-label="Title"
                                                    label="[object Object],[object Object]">Recruitment and marketing</td>
                                                <td class="uploaded_by  " data-rwd-label="Uploaded by"
                                                    label="[object Object],[object Object]">Jack Sparrow</td>
                                            </tr>
                                        </tbody>
                                        <tbody class="reactable-pagination">
                                            <tr>
                                                <td colspan="8">
                                                    <a class="reactable-page-button reactable-current-page" href="#page-1">1</a>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="pagination-wrapper">
                                    <ul class="pagination">
                                        <li class="disabled">
                                            <a class="" href="#">Prev</a>
                                        </li>
                                        <li class="active">
                                            <a class="undefined" href="#">1</a>
                                        </li>
                                        <li class="">
                                            <a class="" href="#">2</a>
                                        </li>
                                        <li class="">
                                            <a class="" href="#">3</a>
                                        </li>
                                        <li class="">
                                            <a class="" href="#">4</a>
                                        </li>
                                        <li class="">
                                            <a class="" href="#">5</a>
                                        </li>
                                        <li class="">
                                            <a class="" href="#">6</a>
                                        </li>
                                        <li class="">
                                            <a class="" href="#">7</a>
                                        </li>
                                        <li class="">
                                            <a class="" href="#">Next</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    )
}
fetchAnalyticsCampaignStaticData(){
    return(
<div class="anylitics-content-main">
    <div class="anytics_container">
        <div>
            <div class="main-container-body-anytlics">
                <div>
                    <div>
                        <div class="chartColsWrapper clearfix">
                            <div class="chartTabContentCol">
                                <div class="chart-container">
                                    <div class="chartdiv-section">
                                        <div class="chart-section">
                                            <div class="chartdiv-wrapper">
                                            <img src='/img/campaignchart.png'/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="chartTabbingCol clearfix">
                                <div class="chartTabbingBtn lineChartTab active ">
                                    <div class="chartTabWrapper">
                                        <div class="chartIcon">
                                            <div class="chartBorder"></div>
                                        </div>
                                        <div class="chartTabTitle">Line Chart</div>
                                    </div>
                                </div>
                                <div class="chartTabbingBtn doughnutChartTab  ">
                                    <div class="chartTabWrapper">
                                        <div class="chartIcon"></div>
                                        <div class="chartTabTitle">Doughnut Chart</div>
                                    </div>
                                </div>
                                <div class="chartTabbingBtn barChartTab  ">
                                    <div class="chartTabWrapper">
                                        <div class="chartIcon">
                                            <div class="chartBorder"></div>
                                        </div>
                                        <div class="chartTabTitle">Bar Chart</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="main-container-body-anytlics">
                <div>
                    <div class="tabel-view-anylitcs">
                        <div class="table-menu dropudown-container">
                            <div class="checkbok-menu-button">
                                <ul class="dd-menu">
                                    <li class="button-dropdown">
                                        <a class="dropdown-toggle btn btn-theme">Filter columns</a>
                                        <ul class="dropdown-menu checkboxDropdown">
                                            <li>
                                                <div class="form-row checkbox">
                                                    <input type="checkbox" id="serialno" name="serialno" class="checknotify"
                                                        value="on"/>
                                                    <label for="serialno">#</label>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="form-row checkbox">
                                                    <input type="checkbox" id="title" name="title" class="checknotify" value="on"/>
                                                    <label for="title">Title</label>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="form-row checkbox">
                                                    <input type="checkbox" id="campaign_type" name="campaign_type" class="checknotify"
                                                        value="on"/>
                                                    <label for="campaign_type">Type</label>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="form-row checkbox">
                                                    <input type="checkbox" id="start_date" name="start_date" class="checknotify"
                                                        value="on"/>
                                                    <label for="start_date">From Date</label>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="form-row checkbox">
                                                    <input type="checkbox" id="end_date" name="end_date" class="checknotify"
                                                        value="on"/>
                                                    <label for="end_date">To Date</label>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="form-row checkbox">
                                                    <input type="checkbox" id="post_count" name="post_count" class="checknotify"
                                                        value="on"/>
                                                    <label for="post_count">Post Count</label>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="form-row checkbox">
                                                    <input type="checkbox" id="SocialChannel" name="SocialChannel" class="checknotify"
                                                        value="on"/>
                                                    <label for="SocialChannel">Channel</label>
                                                </div>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div>

                            <div class="widget">
                                <div class="table-anytlics-body">
                                    <table class="table responsive-table">
                                        <thead>
                                            <tr class="reactable-column-header">
                                                <th class="reactable-th-serialno reactable-header-sortable  serialno  a-center"
                                                    role="button" tabindex="0">
                                                    <strong class="name-header">#</strong>
                                                    <i class="fa fa-sort"></i>
                                                </th>
                                                <th class="reactable-th-title reactable-header-sortable  title  " role="button"
                                                    tabindex="0">
                                                    <strong class="name-header">Title</strong>
                                                    <i class="fa fa-sort"></i>
                                                </th>
                                                <th class="reactable-th-campaign_type reactable-header-sortable  campaign_type  a-center"
                                                    role="button" tabindex="0">
                                                    <strong class="name-header">Type</strong>
                                                    <i class="fa fa-sort"></i>
                                                </th>
                                                <th class="reactable-th-start_date reactable-header-sortable  start_date  a-center"
                                                    role="button" tabindex="0">
                                                    <strong class="name-header">From Date</strong>
                                                    <i class="fa fa-sort"></i>
                                                </th>
                                                <th class="reactable-th-end_date reactable-header-sortable  end_date  a-center"
                                                    role="button" tabindex="0">
                                                    <strong class="name-header">To Date</strong>
                                                    <i class="fa fa-sort"></i>
                                                </th>
                                                <th class="reactable-th-post_count reactable-header-sortable  post_count  a-center"
                                                    role="button" tabindex="0">
                                                    <strong class="name-header">Post Count</strong>
                                                    <i class="fa fa-sort"></i>
                                                </th>
                                                <th class="reactable-th-socialchannel reactable-header-sortable  SocialChannel  a-center"
                                                    role="button" tabindex="0">
                                                    <strong class="name-header">Channel</strong>
                                                    <i class="fa fa-sort"></i>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody class="reactable-data">
                                            <tr class="table-body-text">
                                                <td class="serialno  a-center" data-rwd-label="#" label="[object Object],[object Object]">1</td>
                                                <td class="title  " data-rwd-label="Title" label="[object Object],[object Object]">Product Launch</td>
                                                <td class="campaign_type  a-center" data-rwd-label="Type"
                                                    label="[object Object],[object Object]">Internal</td>
                                                <td class="start_date  a-center" data-rwd-label="From Date"
                                                    label="[object Object],[object Object]">14-04-2018</td>
                                                <td class="end_date  a-center" data-rwd-label="To Date" label="[object Object],[object Object]">05-07-2018</td>
                                                <td class="post_count  a-center" data-rwd-label="Post Count"
                                                    label="[object Object],[object Object]">12</td>
                                                <td class="SocialChannel  a-center" data-rwd-label="Channel" label="[object Object],[object Object]">
                                                    <strong class="name-header">
                                                        <span class="socialIcons">
                                                            <a class="social iw">
                                                                <i class="material-icons">vpn_lock</i>
                                                            </a>
                                                        </span>
                                                    </strong>
                                                </td>
                                            </tr>
                                            <tr class="table-body-text">
                                                <td class="serialno  a-center" data-rwd-label="#" label="[object Object],[object Object]">2</td>
                                                <td class="title  " data-rwd-label="Title" label="[object Object],[object Object]">Hack-e-thon</td>
                                                <td class="campaign_type  a-center" data-rwd-label="Type"
                                                    label="[object Object],[object Object]">Talent</td>
                                                <td class="start_date  a-center" data-rwd-label="From Date" label="[object Object],[object Object]">18-04-2018</td>
                                                <td class="end_date  a-center" data-rwd-label="To Date" label="[object Object],[object Object]">20-04-2018</td>
                                                <td class="post_count  a-center" data-rwd-label="Post Count"
                                                    label="[object Object],[object Object]">0</td>
                                                <td class="SocialChannel  a-center" data-rwd-label="Channel" label="[object Object],[object Object]">
                                                    <strong class="name-header">
                                                        <span class="socialIcons">
                                                            <a class="social fb">
                                                                <i class="fa fa-facebook" aria-hidden="true"></i>
                                                            </a>
                                                        </span>
                                                    </strong>
                                                </td>
                                            </tr>
                                            <tr class="table-body-text">
                                                <td class="serialno  a-center" data-rwd-label="#" label="[object Object],[object Object]">3</td>
                                                <td class="title  " data-rwd-label="Title" label="[object Object],[object Object]">Product Launch 2</td>
                                                <td class="campaign_type  a-center" data-rwd-label="Type"
                                                    label="[object Object],[object Object]">Customer</td>
                                                <td class="start_date  a-center" data-rwd-label="From Date"
                                                    label="[object Object],[object Object]">18-04-2018</td>
                                                <td class="end_date  a-center" data-rwd-label="To Date" label="[object Object],[object Object]">03-05-2018</td>
                                                <td class="post_count  a-center" data-rwd-label="Post Count"
                                                    label="[object Object],[object Object]">4</td>
                                                <td class="SocialChannel  a-center" data-rwd-label="Channel" label="[object Object],[object Object]">
                                                    <strong class="name-header">
                                                        <span class="socialIcons">
                                                            <a class="social tw">
                                                                <i class="fa fa-twitter" aria-hidden="true"></i>
                                                            </a>
                                                            <a class="social ln li">
                                                                <i class="fa fa-linkedin" aria-hidden="true"></i>
                                                            </a>
                                                        </span>
                                                    </strong>
                                                </td>
                                            </tr>
                                            <tr class="table-body-text">
                                                <td class="serialno  a-center" data-rwd-label="#" label="[object Object],[object Object]">4</td>
                                                <td class="title  " data-rwd-label="Title" label="[object Object],[object Object]">Linkedin campaign</td>
                                                <td class="campaign_type  a-center" data-rwd-label="Type"
                                                    label="[object Object],[object Object]">Customer</td>
                                                <td class="start_date  a-center" data-rwd-label="From Date"
                                                    label="[object Object],[object Object]">11-04-2018</td>
                                                <td class="end_date  a-center" data-rwd-label="To Date" label="[object Object],[object Object]">26-04-2018</td>
                                                <td class="post_count  a-center" data-rwd-label="Post Count"
                                                    label="[object Object],[object Object]">1</td>
                                                <td class="SocialChannel  a-center" data-rwd-label="Channel" label="[object Object],[object Object]">
                                                    <strong class="name-header">
                                                        <span class="socialIcons">
                                                            <a class="social ln li">
                                                                <i class="fa fa-linkedin" aria-hidden="true"></i>
                                                            </a>
                                                        </span>
                                                    </strong>
                                                </td>
                                            </tr>
                                            <tr class="table-body-text">
                                                <td class="serialno  a-center" data-rwd-label="#" label="[object Object],[object Object]">5</td>
                                                <td class="title  " data-rwd-label="Title" label="[object Object],[object Object]">June Project </td>
                                                <td class="campaign_type  a-center" data-rwd-label="Type"
                                                    label="[object Object],[object Object]">Talent</td>
                                                <td class="start_date  a-center" data-rwd-label="From Date" label="[object Object],[object Object]">24-04-2018</td>
                                                <td class="end_date  a-center" data-rwd-label="To Date" label="[object Object],[object Object]">25-05-2018</td>
                                                <td class="post_count  a-center" data-rwd-label="Post Count"
                                                    label="[object Object],[object Object]">7</td>
                                                <td class="SocialChannel  a-center" data-rwd-label="Channel" label="[object Object],[object Object]">
                                                    <strong class="name-header">
                                                        <span class="socialIcons">
                                                            <a class="social tw">
                                                                <i class="fa fa-twitter" aria-hidden="true"></i>
                                                            </a>
                                                        </span>
                                                    </strong>
                                                </td>
                                            </tr>
                                            <tr class="table-body-text">
                                                <td class="serialno  a-center" data-rwd-label="#" label="[object Object],[object Object]">6</td>
                                                <td class="title  " data-rwd-label="Title" label="[object Object],[object Object]">All channels</td>
                                                <td class="campaign_type  a-center" data-rwd-label="Type"
                                                    label="[object Object],[object Object]">Talent</td>
                                                <td class="start_date  a-center" data-rwd-label="From Date" label="[object Object],[object Object]">24-04-2018</td>
                                                <td class="end_date  a-center" data-rwd-label="To Date" label="[object Object],[object Object]">16-08-2018</td>
                                                <td class="post_count  a-center" data-rwd-label="Post Count"
                                                    label="[object Object],[object Object]">15</td>
                                                <td class="SocialChannel  a-center" data-rwd-label="Channel" label="[object Object],[object Object]">
                                                    <strong class="name-header">
                                                        <span class="socialIcons">
                                                            <a class="social fb">
                                                                <i class="fa fa-facebook" aria-hidden="true"></i>
                                                            </a>
                                                            <a class="social tw">
                                                                <i class="fa fa-twitter" aria-hidden="true"></i>
                                                            </a>
                                                            <a class="social ln li">
                                                                <i class="fa fa-linkedin" aria-hidden="true"></i>
                                                            </a>
                                                        </span>
                                                    </strong>
                                                </td>
                                            </tr>
                                            <tr class="table-body-text">
                                                <td class="serialno  a-center" data-rwd-label="#" label="[object Object],[object Object]">7</td>
                                                <td class="title  " data-rwd-label="Title" label="[object Object],[object Object]">Summer Internship</td>
                                                <td class="campaign_type  a-center" data-rwd-label="Type"
                                                    label="[object Object],[object Object]">Customer</td>
                                                <td class="start_date  a-center" data-rwd-label="From Date"
                                                    label="[object Object],[object Object]">30-04-2018</td>
                                                <td class="end_date  a-center" data-rwd-label="To Date" label="[object Object],[object Object]">13-08-2018</td>
                                                <td class="post_count  a-center" data-rwd-label="Post Count"
                                                    label="[object Object],[object Object]">9</td>
                                                <td class="SocialChannel  a-center" data-rwd-label="Channel" label="[object Object],[object Object]">
                                                    <strong class="name-header">
                                                        <span class="socialIcons">
                                                            <a class="social fb">
                                                                <i class="fa fa-facebook" aria-hidden="true"></i>
                                                            </a>
                                                            <a class="social tw">
                                                                <i class="fa fa-twitter" aria-hidden="true"></i>
                                                            </a>
                                                        </span>
                                                    </strong>
                                                </td>
                                            </tr>
                                            <tr class="table-body-text">
                                                <td class="serialno  a-center" data-rwd-label="#" label="[object Object],[object Object]">8</td>
                                                <td class="title  " data-rwd-label="Title" label="[object Object],[object Object]">News Flash Meeting</td>
                                                <td class="campaign_type  a-center" data-rwd-label="Type"
                                                    label="[object Object],[object Object]">Internal</td>
                                                <td class="start_date  a-center" data-rwd-label="From Date"
                                                    label="[object Object],[object Object]">30-04-2018</td>
                                                <td class="end_date  a-center" data-rwd-label="To Date" label="[object Object],[object Object]">04-05-2018</td>
                                                <td class="post_count  a-center" data-rwd-label="Post Count"
                                                    label="[object Object],[object Object]">0</td>
                                                <td class="SocialChannel  a-center" data-rwd-label="Channel" label="[object Object],[object Object]">
                                                    <strong class="name-header">
                                                        <span class="socialIcons">
                                                            <a class="social iw">
                                                                <i class="material-icons">vpn_lock</i>
                                                            </a>
                                                        </span>
                                                    </strong>
                                                </td>
                                            </tr>
                                            <tr class="table-body-text">
                                                <td class="serialno  a-center" data-rwd-label="#" label="[object Object],[object Object]">9</td>
                                                <td class="title  " data-rwd-label="Title" label="[object Object],[object Object]">campaign tag notification</td>
                                                <td class="campaign_type  a-center" data-rwd-label="Type"
                                                    label="[object Object],[object Object]">Internal</td>
                                                <td class="start_date  a-center" data-rwd-label="From Date"
                                                    label="[object Object],[object Object]">02-05-2018</td>
                                                <td class="end_date  a-center" data-rwd-label="To Date" label="[object Object],[object Object]">26-05-2018</td>
                                                <td class="post_count  a-center" data-rwd-label="Post Count"
                                                    label="[object Object],[object Object]">0</td>
                                                <td class="SocialChannel  a-center" data-rwd-label="Channel" label="[object Object],[object Object]">
                                                    <strong class="name-header">
                                                        <span class="socialIcons">
                                                            <a class="social iw">
                                                                <i class="material-icons">vpn_lock</i>
                                                            </a>
                                                        </span>
                                                    </strong>
                                                </td>
                                            </tr>
                                            <tr class="table-body-text">
                                                <td class="serialno  a-center" data-rwd-label="#" label="[object Object],[object Object]">10</td>
                                                <td class="title  " data-rwd-label="Title" label="[object Object],[object Object]">Tesxt</td>
                                                <td class="campaign_type  a-center" data-rwd-label="Type" label="[object Object],[object Object]">Internal</td>
                                                <td class="start_date  a-center" data-rwd-label="From Date"
                                                    label="[object Object],[object Object]">09-06-2018</td>
                                                <td class="end_date  a-center" data-rwd-label="To Date" label="[object Object],[object Object]">10-06-2018</td>
                                                <td class="post_count  a-center" data-rwd-label="Post Count"
                                                    label="[object Object],[object Object]">1</td>
                                                <td class="SocialChannel  a-center" data-rwd-label="Channel" label="[object Object],[object Object]">
                                                    <strong class="name-header">
                                                        <span class="socialIcons">
                                                            <a class="social iw">
                                                                <i class="material-icons">vpn_lock</i>
                                                            </a>
                                                        </span>
                                                    </strong>
                                                </td>
                                            </tr>
                                        </tbody>
                                        <tbody class="reactable-pagination">
                                            <tr>
                                                <td colspan="7">
                                                    <a class="reactable-page-button reactable-current-page" href="#page-1">1</a>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="pagination-wrapper">
                                    <ul class="pagination">
                                        <li class="disabled">
                                            <a class="" href="#">Prev</a>
                                        </li>
                                        <li class="active">
                                            <a class="undefined" href="#">1</a>
                                        </li>
                                        <li class="">
                                            <a class="" href="#">2</a>
                                        </li>
                                        <li class="">
                                            <a class="" href="#">3</a>
                                        </li>
                                        <li class="">
                                            <a class="" href="#">Next</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    )
}
fetchAnalyticsStaticData(){
    return(

    <div class="anylitics-content-main">
        <div class="anytics_container">
            <div>
                <div class="main-container-body-anytlics">
                    <div>
                        <div>
                            <div class="chartColsWrapper clearfix">
                                <div class="chartTabContentCol">
                                <img src='/img/chart.png'/>

                                </div>
                                <div class="chartTabbingCol clearfix">
                                    <div class="chartTabbingBtn lineChartTab active ">
                                        <div class="chartTabWrapper">
                                            <div class="chartIcon">

                                                <div class="chartBorder"></div>
                                            </div>
                                            <div class="chartTabTitle">Line Chart</div>
                                        </div>
                                    </div>
                                    <div class="chartTabbingBtn doughnutChartTab  ">
                                        <div class="chartTabWrapper">
                                            <div class="chartIcon"></div>
                                            <div class="chartTabTitle">Doughnut Chart</div>
                                        </div>
                                    </div>
                                    <div class="chartTabbingBtn barChartTab  ">
                                        <div class="chartTabWrapper">
                                            <div class="chartIcon">
                                                <div class="chartBorder"></div>
                                            </div>
                                            <div class="chartTabTitle">Bar Chart</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="main-container-body-anytlics">
                    <div>
                        <div class="tabel-view-anylitcs">
                            <div class="table-menu dropudown-container">
                                <div class="checkbok-menu-button">
                                    <ul class="dd-menu">
                                        <li class="button-dropdown">
                                            <a class="dropdown-toggle btn btn-theme">Filter columns</a>
                                            <ul class="dropdown-menu checkboxDropdown">
                                                <li>
                                                    <div class="form-row checkbox">
                                                        <input type="checkbox" id="serialno" name="serialno" class="checknotify"
                                                            value="on"/>
                                                        <label for="serialno">#</label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="form-row checkbox">
                                                        <input type="checkbox" id="user" name="user" class="checknotify" value="on"/>
                                                        <label for="user">User Name</label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="form-row checkbox">
                                                        <input type="checkbox" id="post_description" name="post_description"
                                                            class="checknotify" value="on"/>
                                                        <label for="post_description">Description</label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="form-row checkbox">
                                                        <input type="checkbox" id="created_at" name="created_at" class="checknotify"
                                                            value="on"/>
                                                        <label for="created_at">Created At</label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="form-row checkbox">
                                                        <input type="checkbox" id="department" name="department" class="checknotify"
                                                            value="on"/>
                                                        <label for="department">Department</label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="form-row checkbox">
                                                        <input type="checkbox" id="channel" name="channel" class="checknotify"
                                                            value="on"/>
                                                        <label for="channel">Channel</label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="form-row checkbox">
                                                        <input type="checkbox" id="share" name="share" class="checknotify" value="on"/>
                                                        <label for="share">Share</label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="form-row checkbox">
                                                        <input type="checkbox" id="like" name="like" class="checknotify" value="on"/>
                                                        <label for="like">Like</label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="form-row checkbox">
                                                        <input type="checkbox" id="comment" name="comment" class="checknotify"
                                                            value="on"/>
                                                        <label for="comment">Comment</label>
                                                    </div>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div>
                                <div class="widget">
                                    <div class="table-anytlics-body">
                                        <table class="table responsive-table">
                                            <thead>
                                                <tr class="reactable-column-header">
                                                    <th class="reactable-th-serialno reactable-header-sortable  serialno  a-center"
                                                        role="button" tabindex="0">
                                                        <strong class="name-header">#</strong>
                                                        <i class="fa fa-sort"></i>
                                                    </th>
                                                    <th class="reactable-th-user reactable-header-sortable  user  " role="button"
                                                        tabindex="0">
                                                        <strong class="name-header">User Name</strong>
                                                        <i class="fa fa-sort"></i>
                                                    </th>
                                                    <th class="reactable-th-post_description reactable-header-sortable  post_description  "
                                                        role="button" tabindex="0">
                                                        <strong class="name-header">Description</strong>
                                                        <i class="fa fa-sort"></i>
                                                    </th>
                                                    <th class="reactable-th-created_at reactable-header-sortable  created_at  "
                                                        role="button" tabindex="0">
                                                        <strong class="name-header">Created At</strong>
                                                        <i class="fa fa-sort"></i>
                                                    </th>
                                                    <th class="reactable-th-department reactable-header-sortable  department  "
                                                        role="button" tabindex="0">
                                                        <strong class="name-header">Department</strong>
                                                        <i class="fa fa-sort"></i>
                                                    </th>
                                                    <th class="reactable-th-channel reactable-header-sortable  channel  a-center"
                                                        role="button" tabindex="0">
                                                        <strong class="name-header">Channel</strong>
                                                        <i class="fa fa-sort"></i>
                                                    </th>
                                                    <th class="reactable-th-share reactable-header-sortable  share  a-center"
                                                        role="button" tabindex="0">
                                                        <strong class="name-header">Share</strong>
                                                        <i class="fa fa-sort"></i>
                                                    </th>
                                                    <th class="reactable-th-like reactable-header-sortable  like  a-center"
                                                        role="button" tabindex="0">
                                                        <strong class="name-header">Like</strong>
                                                        <i class="fa fa-sort"></i>
                                                    </th>
                                                    <th class="reactable-th-comment reactable-header-sortable  comment  a-center"
                                                        role="button" tabindex="0">
                                                        <strong class="name-header">Comment</strong>
                                                        <i class="fa fa-sort"></i>
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody class="reactable-data">
                                                <tr class="table-body-text">
                                                    <td class="serialno  a-center" data-rwd-label="#" label="[object Object],[object Object]">1</td>
                                                    <td class="user  " data-rwd-label="User Name" label="[object Object],[object Object]">Jack Sparrow</td>
                                                    <td class="post_description  " data-rwd-label="Description"
                                                        label="[object Object],[object Object]">é"§èçà ôîûè yq,in te...</td>
                                                    <td class="created_at  " data-rwd-label="Created At"
                                                        label="[object Object],[object Object]">14-09-2018</td>
                                                    <td class="department  " data-rwd-label="Department" label="[object Object],[object Object]">Human Resource11</td>
                                                    <td class="channel  a-center" data-rwd-label="Channel"
                                                        label="[object Object],[object Object]">
                                                        <strong class="name-header">
                                                            <span class="socialIcons">
                                                                <a class="social fb">
                                                                    <i class="fa fa-facebook" aria-hidden="true"></i>
                                                                </a>
                                                                <a class="social tw">
                                                                    <i class="fa fa-twitter" aria-hidden="true"></i>
                                                                </a>
                                                                <a class="social ln li">
                                                                    <i class="fa fa-linkedin" aria-hidden="true"></i>
                                                                </a>
                                                            </span>
                                                        </strong>
                                                    </td>
                                                    <td class="share  a-center" data-rwd-label="Share" label="[object Object],[object Object]">0</td>
                                                    <td class="like  a-center" data-rwd-label="Like" label="[object Object],[object Object]">0</td>
                                                    <td class="comment  a-center" data-rwd-label="Comment" label="[object Object],[object Object]">0</td>
                                                </tr>
                                                <tr class="table-body-text">
                                                    <td class="serialno  a-center" data-rwd-label="#" label="[object Object],[object Object]">2</td>
                                                    <td class="user  " data-rwd-label="User Name" label="[object Object],[object Object]">Jack Sparrow</td>
                                                    <td class="post_description  " data-rwd-label="Description"
                                                        label="[object Object],[object Object]">&amp;é"§è!çà ûîâà` test ...</td>
                                                    <td class="created_at  " data-rwd-label="Created At"
                                                        label="[object Object],[object Object]">14-09-2018</td>
                                                    <td class="department  " data-rwd-label="Department" label="[object Object],[object Object]">Human Resource11</td>
                                                    <td class="channel  a-center" data-rwd-label="Channel"
                                                        label="[object Object],[object Object]">
                                                        <strong class="name-header">
                                                            <span class="socialIcons">
                                                                <a class="social ln li">
                                                                    <i class="fa fa-linkedin" aria-hidden="true"></i>
                                                                </a>
                                                            </span>
                                                        </strong>
                                                    </td>
                                                    <td class="share  a-center" data-rwd-label="Share" label="[object Object],[object Object]">0</td>
                                                    <td class="like  a-center" data-rwd-label="Like" label="[object Object],[object Object]">0</td>
                                                    <td class="comment  a-center" data-rwd-label="Comment" label="[object Object],[object Object]">0</td>
                                                </tr>
                                                <tr class="table-body-text">
                                                    <td class="serialno  a-center" data-rwd-label="#" label="[object Object],[object Object]">3</td>
                                                    <td class="user  " data-rwd-label="User Name" label="[object Object],[object Object]">Jack Sparrow</td>
                                                    <td class="post_description  " data-rwd-label="Description"
                                                        label="[object Object],[object Object]">&amp;é"§è!çà ûîâà` test ...</td>
                                                    <td class="created_at  " data-rwd-label="Created At"
                                                        label="[object Object],[object Object]">14-09-2018</td>
                                                    <td class="department  " data-rwd-label="Department" label="[object Object],[object Object]">Human Resource11</td>
                                                    <td class="channel  a-center" data-rwd-label="Channel"
                                                        label="[object Object],[object Object]">
                                                        <strong class="name-header">
                                                            <span class="socialIcons">
                                                                <a class="social fb">
                                                                    <i class="fa fa-facebook" aria-hidden="true"></i>
                                                                </a>
                                                                <a class="social tw">
                                                                    <i class="fa fa-twitter" aria-hidden="true"></i>
                                                                </a>
                                                                <a class="social ln li">
                                                                    <i class="fa fa-linkedin" aria-hidden="true"></i>
                                                                </a>
                                                            </span>
                                                        </strong>
                                                    </td>
                                                    <td class="share  a-center" data-rwd-label="Share" label="[object Object],[object Object]">0</td>
                                                    <td class="like  a-center" data-rwd-label="Like" label="[object Object],[object Object]">0</td>
                                                    <td class="comment  a-center" data-rwd-label="Comment" label="[object Object],[object Object]">0</td>
                                                </tr>
                                                <tr class="table-body-text">
                                                    <td class="serialno  a-center" data-rwd-label="#" label="[object Object],[object Object]">4</td>
                                                    <td class="user  " data-rwd-label="User Name" label="[object Object],[object Object]">Jack Sparrow</td>
                                                    <td class="post_description  " data-rwd-label="Description"
                                                        label="[object Object],[object Object]">âe^c^cf è!è!&amp; !ûô`è</td>
                                                    <td class="created_at  " data-rwd-label="Created At"
                                                        label="[object Object],[object Object]">14-09-2018</td>
                                                    <td class="department  " data-rwd-label="Department" label="[object Object],[object Object]">Human Resource11</td>
                                                    <td class="channel  a-center" data-rwd-label="Channel"
                                                        label="[object Object],[object Object]">
                                                        <strong class="name-header">
                                                            <span class="socialIcons">
                                                                <a class="social iw">
                                                                    <i class="material-icons">vpn_lock</i>
                                                                </a>
                                                            </span>
                                                        </strong>
                                                    </td>
                                                    <td class="share  a-center" data-rwd-label="Share" label="[object Object],[object Object]">0</td>
                                                    <td class="like  a-center" data-rwd-label="Like" label="[object Object],[object Object]">0</td>
                                                    <td class="comment  a-center" data-rwd-label="Comment" label="[object Object],[object Object]">0</td>
                                                </tr>
                                                <tr class="table-body-text">
                                                    <td class="serialno  a-center" data-rwd-label="#" label="[object Object],[object Object]">5</td>
                                                    <td class="user  " data-rwd-label="User Name" label="[object Object],[object Object]">Jack Sparrow</td>
                                                    <td class="post_description  " data-rwd-label="Description"
                                                        label="[object Object],[object Object]">&amp;é"§è!çà ûîâà` test ...</td>
                                                    <td class="created_at  " data-rwd-label="Created At"
                                                        label="[object Object],[object Object]">14-09-2018</td>
                                                    <td class="department  " data-rwd-label="Department" label="[object Object],[object Object]">Human Resource11</td>
                                                    <td class="channel  a-center" data-rwd-label="Channel"
                                                        label="[object Object],[object Object]">
                                                        <strong class="name-header">
                                                            <span class="socialIcons">
                                                                <a class="social ln li">
                                                                    <i class="fa fa-linkedin" aria-hidden="true"></i>
                                                                </a>
                                                            </span>
                                                        </strong>
                                                    </td>
                                                    <td class="share  a-center" data-rwd-label="Share" label="[object Object],[object Object]">0</td>
                                                    <td class="like  a-center" data-rwd-label="Like" label="[object Object],[object Object]">0</td>
                                                    <td class="comment  a-center" data-rwd-label="Comment" label="[object Object],[object Object]">0</td>
                                                </tr>
                                                <tr class="table-body-text">
                                                    <td class="serialno  a-center" data-rwd-label="#" label="[object Object],[object Object]">6</td>
                                                    <td class="user  " data-rwd-label="User Name" label="[object Object],[object Object]">Jack Sparrow</td>
                                                    <td class="post_description  " data-rwd-label="Description"
                                                        label="[object Object],[object Object]">Mocha with @chris</td>
                                                    <td class="created_at  " data-rwd-label="Created At"
                                                        label="[object Object],[object Object]">13-09-2018</td>
                                                    <td class="department  " data-rwd-label="Department" label="[object Object],[object Object]">Human Resource11</td>
                                                    <td class="channel  a-center" data-rwd-label="Channel"
                                                        label="[object Object],[object Object]">
                                                        <strong class="name-header">
                                                            <span class="socialIcons">
                                                                <a class="social iw">
                                                                    <i class="material-icons">vpn_lock</i>
                                                                </a>
                                                            </span>
                                                        </strong>
                                                    </td>
                                                    <td class="share  a-center" data-rwd-label="Share" label="[object Object],[object Object]">0</td>
                                                    <td class="like  a-center" data-rwd-label="Like" label="[object Object],[object Object]">0</td>
                                                    <td class="comment  a-center" data-rwd-label="Comment" label="[object Object],[object Object]">0</td>
                                                </tr>
                                                <tr class="table-body-text">
                                                    <td class="serialno  a-center" data-rwd-label="#" label="[object Object],[object Object]">7</td>
                                                    <td class="user  " data-rwd-label="User Name" label="[object Object],[object Object]">Jack Sparrow</td>
                                                    <td class="post_description  " data-rwd-label="Description"
                                                        label="[object Object],[object Object]">asd</td>
                                                    <td class="created_at  " data-rwd-label="Created At" label="[object Object],[object Object]">08-09-2018</td>
                                                    <td class="department  " data-rwd-label="Department" label="[object Object],[object Object]">Human Resource11</td>
                                                    <td class="channel  a-center" data-rwd-label="Channel"
                                                        label="[object Object],[object Object]">
                                                        <strong class="name-header">
                                                            <span class="socialIcons">
                                                                <a class="social iw">
                                                                    <i class="material-icons">vpn_lock</i>
                                                                </a>
                                                            </span>
                                                        </strong>
                                                    </td>
                                                    <td class="share  a-center" data-rwd-label="Share" label="[object Object],[object Object]">0</td>
                                                    <td class="like  a-center" data-rwd-label="Like" label="[object Object],[object Object]">0</td>
                                                    <td class="comment  a-center" data-rwd-label="Comment" label="[object Object],[object Object]">0</td>
                                                </tr>
                                                <tr class="table-body-text">
                                                    <td class="serialno  a-center" data-rwd-label="#" label="[object Object],[object Object]">8</td>
                                                    <td class="user  " data-rwd-label="User Name" label="[object Object],[object Object]">Jack Sparrow</td>
                                                    <td class="post_description  " data-rwd-label="Description"
                                                        label="[object Object],[object Object]">Flowers</td>
                                                    <td class="created_at  " data-rwd-label="Created At" label="[object Object],[object Object]">07-09-2018</td>
                                                    <td class="department  " data-rwd-label="Department" label="[object Object],[object Object]">Human Resource11</td>
                                                    <td class="channel  a-center" data-rwd-label="Channel"
                                                        label="[object Object],[object Object]">
                                                        <strong class="name-header">
                                                            <span class="socialIcons">
                                                                <a class="social tw">
                                                                    <i class="fa fa-twitter" aria-hidden="true"></i>
                                                                </a>
                                                                <a class="social ln li">
                                                                    <i class="fa fa-linkedin" aria-hidden="true"></i>
                                                                </a>
                                                            </span>
                                                        </strong>
                                                    </td>
                                                    <td class="share  a-center" data-rwd-label="Share" label="[object Object],[object Object]">0</td>
                                                    <td class="like  a-center" data-rwd-label="Like" label="[object Object],[object Object]">0</td>
                                                    <td class="comment  a-center" data-rwd-label="Comment" label="[object Object],[object Object]">0</td>
                                                </tr>
                                                <tr class="table-body-text">
                                                    <td class="serialno  a-center" data-rwd-label="#" label="[object Object],[object Object]">9</td>
                                                    <td class="user  " data-rwd-label="User Name" label="[object Object],[object Object]">Jack Sparrow</td>
                                                    <td class="post_description  " data-rwd-label="Description"
                                                        label="[object Object],[object Object]">You</td>
                                                    <td class="created_at  " data-rwd-label="Created At" label="[object Object],[object Object]">07-09-2018</td>
                                                    <td class="department  " data-rwd-label="Department" label="[object Object],[object Object]">Human Resource11</td>
                                                    <td class="channel  a-center" data-rwd-label="Channel"
                                                        label="[object Object],[object Object]">
                                                        <strong class="name-header">
                                                            <span class="socialIcons">
                                                                <a class="social tw">
                                                                    <i class="fa fa-twitter" aria-hidden="true"></i>
                                                                </a>
                                                                <a class="social ln li">
                                                                    <i class="fa fa-linkedin" aria-hidden="true"></i>
                                                                </a>
                                                            </span>
                                                        </strong>
                                                    </td>
                                                    <td class="share  a-center" data-rwd-label="Share" label="[object Object],[object Object]">0</td>
                                                    <td class="like  a-center" data-rwd-label="Like" label="[object Object],[object Object]">0</td>
                                                    <td class="comment  a-center" data-rwd-label="Comment" label="[object Object],[object Object]">0</td>
                                                </tr>
                                                <tr class="table-body-text">
                                                    <td class="serialno  a-center" data-rwd-label="#" label="[object Object],[object Object]">10</td>
                                                    <td class="user  " data-rwd-label="User Name" label="[object Object],[object Object]">Jack Sparrow</td>
                                                    <td class="post_description  " data-rwd-label="Description"
                                                        label="[object Object],[object Object]">Vs team</td>
                                                    <td class="created_at  " data-rwd-label="Created At" label="[object Object],[object Object]">07-09-2018</td>
                                                    <td class="department  " data-rwd-label="Department" label="[object Object],[object Object]">Human Resource11</td>
                                                    <td class="channel  a-center" data-rwd-label="Channel"
                                                        label="[object Object],[object Object]">
                                                        <strong class="name-header">
                                                            <span class="socialIcons">
                                                                <a class="social fb">
                                                                    <i class="fa fa-facebook" aria-hidden="true"></i>
                                                                </a>
                                                                <a class="social tw">
                                                                    <i class="fa fa-twitter" aria-hidden="true"></i>
                                                                </a>
                                                                <a class="social ln li">
                                                                    <i class="fa fa-linkedin" aria-hidden="true"></i>
                                                                </a>
                                                            </span>
                                                        </strong>
                                                    </td>
                                                    <td class="share  a-center" data-rwd-label="Share" label="[object Object],[object Object]">0</td>
                                                    <td class="like  a-center" data-rwd-label="Like" label="[object Object],[object Object]">1</td>
                                                    <td class="comment  a-center" data-rwd-label="Comment" label="[object Object],[object Object]">0</td>
                                                </tr>
                                            </tbody>
                                            <tbody class="reactable-pagination">
                                                <tr>
                                                    <td colspan="9">
                                                        <a class="reactable-page-button reactable-current-page" href="#page-1">1</a>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="pagination-wrapper">
                                        <ul class="pagination">
                                            <li class="disabled">
                                                <a class="" href="#">Prev</a>
                                            </li>
                                            <li class="active">
                                                <a class="undefined" href="#">1</a>
                                            </li>
                                            <li class="">
                                                <a class="" href="#">2</a>
                                            </li>
                                            <li class="">
                                                <a class="" href="#">3</a>
                                            </li>
                                            <li class="">
                                                <a class="" href="#">4</a>
                                            </li>
                                            <li class="">
                                                <a class="" href="#">5</a>
                                            </li>
                                            <li class="">
                                                <a class="" href="#">6</a>
                                            </li>
                                            <li class="">
                                                <a class="" href="#">7</a>
                                            </li>
                                            <li class="">
                                                <a class="" href="#">Next</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    )
}
fetchModerationStaticData(){
    return(
        <div class="esc-sm">
        <div class="content">
            <div id="feed">
                <div>
                    <div class="container-button-checkbox clearfix">
                        <div class="campaign-box">
                            <div class="campaign-header">CAMPAIGN</div>
                            <div class="custom-header">CUSTOM</div>
                        </div>
                        <div class="button-disable-moderation "></div>
                        <div class="selectAllCheckboxForPost">
                            <div class="form-row checkbox">
                                <label>
                                    <input type="checkbox" id="check-123" name="check-moderation-post" class="post-check" value="on"/>
                                    <span> </span>
                                </label>
                            </div>
                            <div class="header-icon clearfix">
                                <div class="add-icon">
                                    <i class="material-icons">add_circle_outline</i>
                                </div>
                                <div class="remove-icon">
                                    <i class="material-icons clear">highlight_off</i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="post post-feed-ui">
                        <div class="select-checkbox">
                            <div class="form-row checkbox">
                                <label>
                                    <input type="checkbox" id="check-123" name="check-moderation-post" class="post-check" value="on"/>
                                    <span> </span>
                                </label>
                            </div>
                        </div>
                        <div class="feed-post-item clearfix">

                            <div id="post-AxrRK" class="post-AxrRK post">
                                <div class="post-header clearfix ">
                                    <div class="author-thumb">
                                        <div class="thumbnail-static"></div>
                                        <div class="__react_component_tooltip place-top type-dark " data-id="tooltip"></div>
                                    </div>
                                    <div class="author-data">
                                        <div class="author-meta">
                                            <b>
                                                Vipul testing sm
                                               patel

                                            </b>

                                            shared

                                            <span class="socialIcons">
                                                <span>via </span>
                                                <span class="socialIconWrapper">
                                                    <a class="li">
                                                        <i class="fa fa-linkedin" aria-hidden="true"></i>
                                                    </a>
                                                </span>
                                            </span>
                                        </div>
                                        <div class="post-time">
                                            <span class="timeFromNow" title="Monday, January 19th 1970, 12:15:18 am">Friday, September 7th 2018 at 4:41 pm</span>
                                        </div>
                                        <ul class="dd-menu context-menu">
                                            <li class="button-dropdown">
                                                <a class="dropdown-toggle" id="dropdown-toggle-AxrRK">
                                                    <i class="material-icons">more_vert</i>
                                                </a>
                                                <ul class="dropdown-menu hide" id="dropdown-menu-AxrRK">
                                                    <li>
                                                        <a class="btn-edit-folder">Hide post</a>
                                                    </li>
                                                    <li>
                                                        <a class="btn-edit-folder">Pin on top</a>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>

                                        <div class="campaign-custom-box">
                                            <div class="Campaigntitle">CAMPAIGN TITLE</div>
                                            <div class="Customtitle">THIS IS A CUSTOM FEE</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="post-detail-pic-wrapper test123">
                                    <div id="postdetail" name="postdetail" class="post-title ">
                                        <div id="detail-AxrRK"></div>
                                        <div class="seeMoreSection" id="AxrRK">
                                            <div class="seeMoreWrapper">
                                                <div class="post-detail-feed-text">
                                                    hi test

                                                </div>

                                            </div>
                                            <div class="seeMoreLinkWrapper">
                                                <a class="seeMoreLink">
                                                    see more

                                                    <i class="fa fa-angle-double-right"></i>
                                                </a>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="post-pic"></div>
                                    <div class="post-meta">
                                        <div class="like    likeLink " data-postid="AxrRK">
                                            <a class="post-meta-link">
                                                <i class="material-icons">thumb_up</i>
                                                <span class="likeText">Like</span>
                                            </a>
                                        </div>
                                        <div class="share">
                                            <div class="popup-container">
                                                <a class="link-share-popup post-meta-link">
                                                    <i class="material-icons">share</i>
                                                    <span class="shareText">Share</span>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="commentsLink comments" data-postid="AxrRK">
                                            <div class="count">
                                                <span class="likesCount" data-postid="AxrRK">
                                                    <span class="likesNumber">0</span>
                                                    <a class="post-meta-link" id="likebox-AxrRK" data-like-clicked="false">Like</a>
                                                </span>
                                                <span class="commentsCount commentsLink" data-postid="AxrRK">
                                                    <span class="commentsNumber">0</span>
                                                    <a class="post-meta-link" href="#post-AxrRK-1">Comment</a>
                                                </span>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="post-meta post-approve-wrapper">
                                        <div class="status pending-approval">
                                            <i class="material-icons">remove_circle_outline</i>
                                            Pending Approval
                                          >
                                        </div>
                                    </div>
                                </div>
                                <div></div>

                            </div>
                        </div>
                        <div class="approval-actions clearfix">
                            <div class="approval-box">
                                <button class="btn btn-fa btn-success approval-btn">
                                    <i class="material-icons">add_circle_outline</i>
                                    <span class="btnText">APPROVE</span>
                                </button>
                            </div>
                            <div class="unapproval-box">
                                <button class="btn btn-fa btn-danger unapproval-btn">
                                    <i class="material-icons">highlight_off</i>
                                    <span class="btnText">UNAPPROVE</span>
                                </button>
                            </div>
                        </div>
                        <div> </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    )


}
fetchRssStaticData(){
    return(
        <div class="main-container-body">
    <div class="row main-container-rss">
        <div class="col-nine-of-twelve hotdiv space-hot">
            <span id="curatedTour"></span>
            <div class="row rss-columns">
            <div class="col-four-of-twelve">
    <div class="feed-job-item-inner">
        <a target="_blank" href="https://gigaom.com/2018/09/14/could-devops-exist-without-cloud-models/" class="visit-source-link">
            <i class="material-icons">launch</i>
            Visit source

        </a>
        <div class="rss-block">
            <div class="banner-hot-header hot-overlay">
                <div class="rss-banner-captions">
                    <p>Could DevOps exist without cloud models?</p>
                </div>
            </div>
            <div class="hot-div-body">
                <div class="date-and-time">
                    Added
                    3 days ago

                </div>
                <div class="text-header">
                    <p>The GigaOm DevOps market landscape report is nearing completion, distilling conversations and briefings
                        into a mere 8,500 word narrative. Yes, it’s big,…</p>
                </div>
            </div>
        </div>
    </div>
    <div class="feed-job-share-block feed-job-share-block-0">
        <div class="feed-job-share-block-title">
            <span>Share</span>
        </div>
        <div>
            <div class="socialIcons clearfix">
                <div class="socialShareItem">
                    <a class="internal-icon" title="Internal channel">
                        <i class="material-icons">vpn_lock</i>
                    </a>
                </div>
                <div class="socialShareItem">
                    <span data-tip="Please connect these social accounts in settings." data-for="notConnectedSocialChannels"
                        currentitem="false">

                        <a class="fb linkNotAvailable">
                            <i class="fa fa-facebook notConnected"></i>
                        </a>
                        <div class="__react_component_tooltip place-top type-warning " id="notConnectedSocialChannels" data-id="tooltip"
                            >Please connect these social accounts in settings.</div>
                    </span>
                </div>
                <div class="socialShareItem">
                    <span data-tip="Please connect these social accounts in settings." data-for="notConnectedSocialChannels"
                        currentitem="false">

                        <a class="tw linkNotAvailable">
                            <i class="fa fa-twitter notConnected"></i>
                        </a>
                        <div class="__react_component_tooltip place-top type-warning " id="notConnectedSocialChannels" data-id="tooltip"
                            >Please connect these social accounts in settings.</div>
                    </span>
                </div>
                <div class="socialShareItem">
                    <a class="linkedin-link rss-share-link li">
                        <i class="fa fa-linkedin" aria-hidden="true"></i>
                    </a>
                </div>
            </div>

        </div>
    </div>
</div>
                <div class="col-four-of-twelve">
                    <div class="feed-job-item-inner">
                        <a target="_blank" href="https://gigaom.com/2018/09/13/voices-in-ai-episode-67-a-conversation-with-amir-khosrowshahi/"
                            class="visit-source-link">
                            <i class="material-icons">launch</i>
                           Visit source

                        </a>
                        <div class="rss-block">
                            <div class="banner-hot-header hot-overlay">
                                <div class="rss-banner-captions">
                                    <p>Voices in AI – Episode 67: A Conversation with Amir Khosrowshahi</p>
                                </div>
                            </div>
                            <div class="hot-div-body">
                                <div class="date-and-time">
                                   Added

                                  5 days ago

                                </div>
                                <div class="text-header">
                                    <p>About this Episode Episode 67 of Voices in AI features host Byron Reese and Amir Khosrowshahi
                                        talk about the explainability, privacy, and…</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="feed-job-share-block feed-job-share-block-1">
                        <div class="feed-job-share-block-title">
                            <span>Share</span>
                        </div>
                        <div>
                            <div class="socialIcons clearfix">
                                <div class="socialShareItem">
                                    <a class="internal-icon" title="Internal channel">
                                        <i class="material-icons">vpn_lock</i>
                                    </a>
                                </div>
                                <div class="socialShareItem">
                                    <span data-tip="Please connect these social accounts in settings." data-for="notConnectedSocialChannels"
                                        currentitem="false">

                                        <a class="fb linkNotAvailable">
                                            <i class="fa fa-facebook notConnected"></i>
                                        </a>
                                        <div class="__react_component_tooltip place-top type-warning " id="notConnectedSocialChannels"
                                            data-id="tooltip" >Please connect these social accounts in settings.</div>
                                    </span>
                                </div>
                                <div class="socialShareItem">
                                    <span data-tip="Please connect these social accounts in settings." data-for="notConnectedSocialChannels"
                                        currentitem="false">

                                        <a class="tw linkNotAvailable">
                                            <i class="fa fa-twitter notConnected"></i>
                                        </a>
                                        <div class="__react_component_tooltip place-top type-warning " id="notConnectedSocialChannels"
                                            data-id="tooltip">Please connect these social accounts in settings.</div>
                                    </span>
                                </div>
                                <div class="socialShareItem">
                                    <a class="linkedin-link rss-share-link li">
                                        <i class="fa fa-linkedin" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-four-of-twelve">
                    <div class="feed-job-item-inner">
                        <a target="_blank" href="https://andrewgelman.com/2018/09/07/bothered-non-monotonicity-heres-one-quick-trick-make-happy/"
                            class="visit-source-link">
                            <i class="material-icons">launch</i>
                            Visit source

                        </a>
                        <div class="rss-block">
                            <div class="banner-hot-header no-img-header">
                                <div class="rss-banner-captions">
                                    <p>Bothered by non-monotonicity? Here’s ONE QUICK TRICK to make you happy.</p>
                                </div>
                            </div>
                            <div class="hot-div-body">
                                <div class="date-and-time">
                                    Added
                                   11 days ago

                                </div>
                                <div class="text-header">
                                    <p>
                                        We’re often modeling non-monotonic functions. For example, performance at just about any task increases with age (babies
                                        can’t do much!) and then eventually decreases (dead people can’t do much either!).
                                        Here’s an example from a few years ago: A function g(x) that increases and then decreases
                                        can be modeled by a quadratic, or some more […] The post Bothered by non-monotonicity?
                                        Here’s ONE QUICK TRICK to make you happy. appeared first on Statistical Modeling,
                                        Causal Inference, and Social Science.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="feed-job-share-block feed-job-share-block-2">
                        <div class="feed-job-share-block-title">
                            <span>Share</span>
                        </div>
                        <div>
                            <div class="socialIcons clearfix">
                                <div class="socialShareItem">
                                    <a class="internal-icon" title="Internal channel">
                                        <i class="material-icons">vpn_lock</i>
                                    </a>
                                </div>
                                <div class="socialShareItem">
                                    <span data-tip="Please connect these social accounts in settings." data-for="notConnectedSocialChannels"
                                        currentitem="false">

                                        <a class="fb linkNotAvailable">
                                            <i class="fa fa-facebook notConnected"></i>
                                        </a>
                                        <div class="__react_component_tooltip place-top type-warning " id="notConnectedSocialChannels"
                                            data-id="tooltip" >Please connect these social accounts in settings.</div>
                                    </span>
                                </div>
                                <div class="socialShareItem">
                                    <span data-tip="Please connect these social accounts in settings." data-for="notConnectedSocialChannels"
                                        currentitem="false">

                                        <a class="tw linkNotAvailable">
                                            <i class="fa fa-twitter notConnected"></i>
                                        </a>
                                        <div class="__react_component_tooltip place-top type-warning " id="notConnectedSocialChannels"
                                            data-id="tooltip" >Please connect these social accounts in settings.</div>
                                    </span>
                                </div>
                                <div class="socialShareItem">
                                    <a class="linkedin-link rss-share-link li">
                                        <i class="fa fa-linkedin" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-four-of-twelve">
                    <div class="feed-job-item-inner">
                        <a target="_blank" href="https://andrewgelman.com/2018/09/06/dynamically-rescaled-hamiltonian-monte-carlo-bayesian-hierarchical-models/"
                            class="visit-source-link">
                            <i class="material-icons">launch</i>
                            Visit source

                        </a>
                        <div class="rss-block">
                            <div class="banner-hot-header no-img-header">
                                <div class="rss-banner-captions">
                                    <p>“Dynamically Rescaled Hamiltonian Monte Carlo for Bayesian Hierarchical Models”</p>
                                </div>
                            </div>
                            <div class="hot-div-body">
                                <div class="date-and-time">
                                    Added
                                   11 days ago

                                </div>
                                <div class="text-header">
                                    <p>
                                        Aki points us to this paper by Tore Selland Kleppe, which begins: Dynamically rescaled Hamiltonian Monte Carlo (DRHMC) is
                                        introduced as a computationally fast and easily implemented method for performing
                                        full Bayesian analysis in hierarchical statistical models. The method relies on introducing
                                        a modified parameterisation so that the re-parameterised target distribution has
                                        close to constant […] The post “Dynamically Rescaled Hamiltonian Monte Carlo for
                                        Bayesian Hierarchical Models” appeared first on Statistical Modeling, Causal Inference,
                                        and Social Science.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="feed-job-share-block feed-job-share-block-3">
                        <div class="feed-job-share-block-title">
                            <span>Share</span>
                        </div>
                        <div>
                            <div class="socialIcons clearfix">
                                <div class="socialShareItem">
                                    <a class="internal-icon" title="Internal channel">
                                        <i class="material-icons">vpn_lock</i>
                                    </a>
                                </div>
                                <div class="socialShareItem">
                                    <span data-tip="Please connect these social accounts in settings." data-for="notConnectedSocialChannels"
                                        currentitem="false">

                                        <a class="fb linkNotAvailable">
                                            <i class="fa fa-facebook notConnected"></i>
                                        </a>
                                        <div class="__react_component_tooltip place-top type-warning " id="notConnectedSocialChannels"
                                            data-id="tooltip">Please connect these social accounts in settings.</div>
                                    </span>
                                </div>
                                <div class="socialShareItem">
                                    <span data-tip="Please connect these social accounts in settings." data-for="notConnectedSocialChannels"
                                        currentitem="false">

                                        <a class="tw linkNotAvailable">
                                            <i class="fa fa-twitter notConnected"></i>
                                        </a>
                                        <div class="__react_component_tooltip place-top type-warning " id="notConnectedSocialChannels"
                                            data-id="tooltip">Please connect these social accounts in settings.</div>
                                    </span>
                                </div>
                                <div class="socialShareItem">
                                    <a class="linkedin-link rss-share-link li">
                                        <i class="fa fa-linkedin" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-four-of-twelve">
                    <div class="feed-job-item-inner">
                        <a target="_blank" href="https://andrewgelman.com/2018/09/06/gaps-1-2-3-just-large/" class="visit-source-link">
                            <i class="material-icons">launch</i>
                           Visit source

                        </a>
                        <div class="rss-block">
                            <div class="banner-hot-header no-img-header">
                                <div class="rss-banner-captions">
                                    <p>The gaps between 1, 2, and 3 are just too large.</p>
                                </div>
                            </div>
                            <div class="hot-div-body">
                                <div class="date-and-time">
                                Added


                                    12 days ago

                                </div>
                                <div class="text-header">
                                    <p>
                                        Someone who wishes to remain anonymous points to a new study of David Yeager et al. on educational mindset interventions
                                        (link from Alex Tabarrok) and asks: On the blog we talk a lot about bad practice
                                        and what not to do. Might this be an example of how *to do* things? Or did they just
                                        […] The post The gaps between 1, 2, and 3 are just too large. appeared first on Statistical
                                        Modeling, Causal Inference, and Social Science.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="feed-job-share-block feed-job-share-block-4">
                        <div class="feed-job-share-block-title">
                            <span>Share</span>
                        </div>
                        <div>
                            <div class="socialIcons clearfix">
                                <div class="socialShareItem">
                                    <a class="internal-icon" title="Internal channel">
                                        <i class="material-icons">vpn_lock</i>
                                    </a>
                                </div>
                                <div class="socialShareItem">
                                    <span data-tip="Please connect these social accounts in settings." data-for="notConnectedSocialChannels"
                                        currentitem="false">

                                        <a class="fb linkNotAvailable">
                                            <i class="fa fa-facebook notConnected"></i>
                                        </a>
                                        <div class="__react_component_tooltip place-top type-warning " id="notConnectedSocialChannels"
                                            data-id="tooltip" >Please connect these social accounts in settings.</div>
                                    </span>
                                </div>
                                <div class="socialShareItem">
                                    <span data-tip="Please connect these social accounts in settings." data-for="notConnectedSocialChannels"
                                        currentitem="false">

                                        <a class="tw linkNotAvailable">
                                            <i class="fa fa-twitter notConnected"></i>
                                        </a>
                                        <div class="__react_component_tooltip place-top type-warning " id="notConnectedSocialChannels"
                                            data-id="tooltip" >Please connect these social accounts in settings.</div>
                                    </span>
                                </div>
                                <div class="socialShareItem">
                                    <a class="linkedin-link rss-share-link li">
                                        <i class="fa fa-linkedin" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-four-of-twelve">
                    <div class="feed-job-item-inner">
                        <a target="_blank" href="https://gigaom.com/2018/09/06/voices-in-ai-episode-66-a-conversation-with-steve-ritter/"
                            class="visit-source-link">
                            <i class="material-icons">launch</i>
                           Visit source

                        </a>
                        <div class="rss-block">
                            <div class="banner-hot-header hot-overlay">
                                <div class="rss-banner-captions">
                                    <p>Voices in AI – Episode 66: A Conversation with Steve Ritter</p>
                                </div>
                            </div>
                            <div class="hot-div-body">
                                <div class="date-and-time">
                                   Added
                                    12 days ago

                                </div>
                                <div class="text-header">
                                    <p>About this Episode Episode 66 of Voices in AI features host Byron Reese and Steve Ritter
                                        talk about the future of AGI,…</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="feed-job-share-block feed-job-share-block-5">
                        <div class="feed-job-share-block-title">
                            <span>Share</span>
                        </div>
                        <div>
                            <div class="socialIcons clearfix">
                                <div class="socialShareItem">
                                    <a class="internal-icon" title="Internal channel">
                                        <i class="material-icons">vpn_lock</i>
                                    </a>
                                </div>
                                <div class="socialShareItem">
                                    <span data-tip="Please connect these social accounts in settings." data-for="notConnectedSocialChannels"
                                        currentitem="false">

                                        <a class="fb linkNotAvailable">
                                            <i class="fa fa-facebook notConnected"></i>
                                        </a>
                                        <div class="__react_component_tooltip place-top type-warning " id="notConnectedSocialChannels"
                                            data-id="tooltip">Please connect these social accounts in settings.</div>
                                    </span>
                                </div>
                                <div class="socialShareItem">
                                    <span data-tip="Please connect these social accounts in settings." data-for="notConnectedSocialChannels"
                                        currentitem="false">

                                        <a class="tw linkNotAvailable">
                                            <i class="fa fa-twitter notConnected"></i>
                                        </a>
                                        <div class="__react_component_tooltip place-top type-warning " id="notConnectedSocialChannels"
                                            data-id="tooltip">Please connect these social accounts in settings.</div>
                                    </span>
                                </div>
                                <div class="socialShareItem">
                                    <a class="linkedin-link rss-share-link li">
                                        <i class="fa fa-linkedin" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-four-of-twelve">
                    <div class="feed-job-item-inner">
                        <a target="_blank" href="https://andrewgelman.com/2018/09/05/journalists-not-running-corrections/" class="visit-source-link">
                            <i class="material-icons">launch</i>
                            Visit source

                        </a>
                        <div class="rss-block">
                            <div class="banner-hot-header no-img-header">
                                <div class="rss-banner-captions">
                                    <p>British journalists not running corrections and talking about putting people in the freezer</p>
                                </div>
                            </div>
                            <div class="hot-div-body">
                                <div class="date-and-time">
                                    Added

                                   13 days ago

                                </div>
                                <div class="text-header">
                                    <p>
                                        I happened to be reading an old issue of Private Eye (a friend subscribes and occasionally gives me some old copies) and
                                        came across this, discussing various misinformation regarding a recent crime that
                                        had been reported by a London tabloid columnist named Rod Liddle (no relation to
                                        the famous statistician, I assume): Here is “what […] The post British journalists
                                        not running corrections and talking about putting people in the freezer appeared
                                        first on Statistical Modeling, Causal Inference, and Social Science.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="feed-job-share-block feed-job-share-block-6">
                        <div class="feed-job-share-block-title">
                            <span>Share</span>
                        </div>
                        <div>
                            <div class="socialIcons clearfix">
                                <div class="socialShareItem">
                                    <a class="internal-icon" title="Internal channel">
                                        <i class="material-icons">vpn_lock</i>
                                    </a>
                                </div>
                                <div class="socialShareItem">
                                    <span data-tip="Please connect these social accounts in settings." data-for="notConnectedSocialChannels"
                                        currentitem="false">

                                        <a class="fb linkNotAvailable">
                                            <i class="fa fa-facebook notConnected"></i>
                                        </a>
                                        <div class="__react_component_tooltip place-top type-warning " id="notConnectedSocialChannels"
                                            data-id="tooltip" >Please connect these social accounts in settings.</div>
                                    </span>
                                </div>
                                <div class="socialShareItem">
                                    <span data-tip="Please connect these social accounts in settings." data-for="notConnectedSocialChannels"
                                        currentitem="false">

                                        <a class="tw linkNotAvailable">
                                            <i class="fa fa-twitter notConnected"></i>
                                        </a>
                                        <div class="__react_component_tooltip place-top type-warning " id="notConnectedSocialChannels"
                                            data-id="tooltip">Please connect these social accounts in settings.</div>
                                    </span>
                                </div>
                                <div class="socialShareItem">
                                    <a class="linkedin-link rss-share-link li">
                                        <i class="fa fa-linkedin" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-four-of-twelve">
                    <div class="feed-job-item-inner">
                        <a target="_blank" href="https://andrewgelman.com/2018/09/05/winner-take-attribution/" class="visit-source-link">
                            <i class="material-icons">launch</i>
                            Visit source

                        </a>
                        <div class="rss-block">
                            <div class="banner-hot-header no-img-header">
                                <div class="rss-banner-captions">
                                    <p>Against Winner-Take-All Attribution</p>
                                </div>
                            </div>
                            <div class="hot-div-body">
                                <div class="date-and-time">
                                   Added

                                13 days ago

                                </div>
                                <div class="text-header">
                                    <p>
                                        This is the anti-Wolfram. I did not design or write the Stan language. I’m a user of Stan. Lots of people designed and wrote
                                        Stan, most notably Bob Carpenter (designed the language and implemented lots of the
                                        algorithms), Matt Hoffman (came up with the Nuts algorithm), and Daniel Lee (put
                                        together lots of the internals […] The post Against Winner-Take-All Attribution appeared
                                        first on Statistical Modeling, Causal Inference, and Social Science.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="feed-job-share-block feed-job-share-block-7">
                        <div class="feed-job-share-block-title">
                            <span>Share</span>
                        </div>
                        <div>
                            <div class="socialIcons clearfix">
                                <div class="socialShareItem">
                                    <a class="internal-icon" title="Internal channel">
                                        <i class="material-icons">vpn_lock</i>
                                    </a>
                                </div>
                                <div class="socialShareItem">
                                    <span data-tip="Please connect these social accounts in settings." data-for="notConnectedSocialChannels"
                                        currentitem="false">

                                        <a class="fb linkNotAvailable">
                                            <i class="fa fa-facebook notConnected"></i>
                                        </a>
                                        <div class="__react_component_tooltip place-top type-warning " id="notConnectedSocialChannels"
                                            data-id="tooltip">Please connect these social accounts in settings.</div>
                                    </span>
                                </div>
                                <div class="socialShareItem">
                                    <span data-tip="Please connect these social accounts in settings." data-for="notConnectedSocialChannels"
                                        currentitem="false">

                                        <a class="tw linkNotAvailable">
                                            <i class="fa fa-twitter notConnected"></i>
                                        </a>
                                        <div class="__react_component_tooltip place-top type-warning " id="notConnectedSocialChannels"
                                            data-id="tooltip" >Please connect these social accounts in settings.</div>
                                    </span>
                                </div>
                                <div class="socialShareItem">
                                    <a class="linkedin-link rss-share-link li">
                                        <i class="fa fa-linkedin" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-four-of-twelve">
                    <div class="feed-job-item-inner">
                        <a target="_blank" href="https://andrewgelman.com/2018/09/04/continuously-increased-number-animals-statistical-significance-reached-support-conclusions-think-not-bad-actually-2/"
                            class="visit-source-link">
                            <i class="material-icons">launch</i>
                           Visit source

                        </a>
                        <div class="rss-block">
                            <div class="banner-hot-header no-img-header" >
                                <div class="rss-banner-captions">
                                    <p>“We continuously increased the number of animals until statistical significance was reached
                                        to support our conclusions” . . . I think this is not so bad, actually!</p>
                                </div>
                            </div>
                            <div class="hot-div-body">
                                <div class="date-and-time">
                                   Added
                                    14 days ago

                                </div>
                                <div class="text-header">
                                    <p>
                                        For some reason, people have recently been asking me what I think of this journal article which I wrote about months ago
                                        . . . so I’ll just repeat my post here: Jordan Anaya pointed me to this post, in
                                        which Casper Albers shared this snippet from a recently-published paper from an article
                                        in Nature […] The post “We continuously increased the number of animals until statistical
                                        significance was reached to support our conclusions” . . . I think this is not so
                                        bad, actually! appeared first on Statistical Modeling, Causal Inference, and Social
                                        Science.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="feed-job-share-block feed-job-share-block-8">
                        <div class="feed-job-share-block-title">
                            <span>Share</span>
                        </div>
                        <div>
                            <div class="socialIcons clearfix">
                                <div class="socialShareItem">
                                    <a class="internal-icon" title="Internal channel">
                                        <i class="material-icons">vpn_lock</i>
                                    </a>
                                </div>
                                <div class="socialShareItem">
                                    <span data-tip="Please connect these social accounts in settings." data-for="notConnectedSocialChannels"
                                        currentitem="false">

                                        <a class="fb linkNotAvailable">
                                            <i class="fa fa-facebook notConnected"></i>
                                        </a>
                                        <div class="__react_component_tooltip place-top type-warning " id="notConnectedSocialChannels"
                                            data-id="tooltip">Please connect these social accounts in settings.</div>
                                    </span>
                                </div>
                                <div class="socialShareItem">
                                    <span data-tip="Please connect these social accounts in settings." data-for="notConnectedSocialChannels"
                                        currentitem="false">

                                        <a class="tw linkNotAvailable">
                                            <i class="fa fa-twitter notConnected"></i>
                                        </a>
                                        <div class="__react_component_tooltip place-top type-warning " id="notConnectedSocialChannels"
                                            data-id="tooltip">Please connect these social accounts in settings.</div>
                                    </span>
                                </div>
                                <div class="socialShareItem">
                                    <a class="linkedin-link rss-share-link li">
                                        <i class="fa fa-linkedin" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-four-of-twelve">
                    <div class="feed-job-item-inner">
                        <a target="_blank" href="https://andrewgelman.com/2018/09/04/robert-heinlein-vs-lawrence-summers/" class="visit-source-link">
                            <i class="material-icons">launch</i>
                           Visit source

                        </a>
                        <div class="rss-block">
                            <div class="banner-hot-header no-img-header">
                                <div class="rss-banner-captions">
                                    <p>Robert Heinlein vs. Lawrence Summers</p>
                                </div>
                            </div>
                            <div class="hot-div-body">
                                <div class="date-and-time">
                                    Added

                                    14 days ago

                                </div>
                                <div class="text-header">
                                    <p>
                                        Thomas Ball writes: In this article about Nabokov and the influence of John Dunne’s theories on him (and others in the period
                                        l’entre deux guerres) you can see intimations of Borges’ story The Garden of Forking
                                        Paths…. The article in question is by Nicholson Baker. Nicholson Baker! It’s great
                                        to see that he’s still writing. […] The post Robert Heinlein vs. Lawrence Summers
                                        appeared first on Statistical Modeling, Causal Inference, and Social Science.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="feed-job-share-block feed-job-share-block-9">
                        <div class="feed-job-share-block-title">
                            <span>Share</span>
                        </div>
                        <div>
                            <div class="socialIcons clearfix">
                                <div class="socialShareItem">
                                    <a class="internal-icon" title="Internal channel">
                                        <i class="material-icons">vpn_lock</i>
                                    </a>
                                </div>
                                <div class="socialShareItem">
                                    <span data-tip="Please connect these social accounts in settings." data-for="notConnectedSocialChannels"
                                        currentitem="false">

                                        <a class="fb linkNotAvailable">
                                            <i class="fa fa-facebook notConnected"></i>
                                        </a>
                                        <div class="__react_component_tooltip place-top type-warning " id="notConnectedSocialChannels"
                                            data-id="tooltip" >Please connect these social accounts in settings.</div>
                                    </span>
                                </div>
                                <div class="socialShareItem">
                                    <span data-tip="Please connect these social accounts in settings." data-for="notConnectedSocialChannels"
                                        currentitem="false">

                                        <a class="tw linkNotAvailable">
                                            <i class="fa fa-twitter notConnected"></i>
                                        </a>
                                        <div class="__react_component_tooltip place-top type-warning " id="notConnectedSocialChannels"
                                            data-id="tooltip">Please connect these social accounts in settings.</div>
                                    </span>
                                </div>
                                <div class="socialShareItem">
                                    <a class="linkedin-link rss-share-link li">
                                        <i class="fa fa-linkedin" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-four-of-twelve">
                    <div class="feed-job-item-inner">
                        <a target="_blank" href="https://gigaom.com/2018/09/04/a-bottoms-up-approach-to-digital-transformation/"
                            class="visit-source-link">
                            <i class="material-icons">launch</i>
                           Visit source

                        </a>
                        <div class="rss-block">
                            <div class="banner-hot-header hot-overlay">
                                <div class="rss-banner-captions">
                                    <p>A Bottoms Up Approach to Digital Transformation</p>
                                </div>
                            </div>
                            <div class="hot-div-body">
                                <div class="date-and-time">
                                    Added
                                  14 days ago

                                </div>
                                <div class="text-header">
                                    <p>Every organization today has strategies in place for managing toward DX. They differ
                                        and are implemented w/ varying intensity, but any business…</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="feed-job-share-block feed-job-share-block-10">
                        <div class="feed-job-share-block-title">
                            <span>Share</span>
                        </div>
                        <div>
                            <div class="socialIcons clearfix">
                                <div class="socialShareItem">
                                    <a class="internal-icon" title="Internal channel">
                                        <i class="material-icons">vpn_lock</i>
                                    </a>
                                </div>
                                <div class="socialShareItem">
                                    <span data-tip="Please connect these social accounts in settings." data-for="notConnectedSocialChannels"
                                        currentitem="false">

                                        <a class="fb linkNotAvailable">
                                            <i class="fa fa-facebook notConnected"></i>
                                        </a>
                                        <div class="__react_component_tooltip place-top type-warning " id="notConnectedSocialChannels"
                                            data-id="tooltip" >Please connect these social accounts in settings.</div>
                                    </span>
                                </div>
                                <div class="socialShareItem">
                                    <span data-tip="Please connect these social accounts in settings." data-for="notConnectedSocialChannels"
                                        currentitem="false">

                                        <a class="tw linkNotAvailable">
                                            <i class="fa fa-twitter notConnected"></i>
                                        </a>
                                        <div class="__react_component_tooltip place-top type-warning " id="notConnectedSocialChannels"
                                            data-id="tooltip">Please connect these social accounts in settings.</div>
                                    </span>
                                </div>
                                <div class="socialShareItem">
                                    <a class="linkedin-link rss-share-link li">
                                        <i class="fa fa-linkedin" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-four-of-twelve">
                    <div class="feed-job-item-inner">
                        <a target="_blank" href="https://gigaom.com/2018/09/04/disruptive-technologies-in-conversation-with-byron-reese-lauren-sallata/"
                            class="visit-source-link">
                            <i class="material-icons">launch</i>
                          Visit source

                        </a>
                        <div class="rss-block">
                            <div class="banner-hot-header hot-overlay">
                                <div class="rss-banner-captions">
                                    <p>Disruptive Technologies: In Conversation with Byron Reese &amp;amp; Lauren Sallata</p>
                                </div>
                            </div>
                            <div class="hot-div-body">
                                <div class="date-and-time">
                                    Added
                                   14 days ago

                                </div>
                                <div class="text-header">
                                    <p>GigaOm&nbsp;CEO, Byron Reese, sits down with Lauren&nbsp;Sallata, Chief Marketing Officer
                                        &amp;amp; VP, Panasonic Corporation of North America, Inc. to discuss IoT devices,…</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="feed-job-share-block feed-job-share-block-11">
                        <div class="feed-job-share-block-title">
                            <span>Share</span>
                        </div>
                        <div>
                            <div class="socialIcons clearfix">
                                <div class="socialShareItem">
                                    <a class="internal-icon" title="Internal channel">
                                        <i class="material-icons">vpn_lock</i>
                                    </a>
                                </div>
                                <div class="socialShareItem">
                                    <span data-tip="Please connect these social accounts in settings." data-for="notConnectedSocialChannels"
                                        currentitem="false">

                                        <a class="fb linkNotAvailable">
                                            <i class="fa fa-facebook notConnected"></i>
                                        </a>
                                        <div class="__react_component_tooltip place-top type-warning " id="notConnectedSocialChannels"
                                            data-id="tooltip">Please connect these social accounts in settings.</div>
                                    </span>
                                </div>
                                <div class="socialShareItem">
                                    <span data-tip="Please connect these social accounts in settings." data-for="notConnectedSocialChannels"
                                        currentitem="false">

                                        <a class="tw linkNotAvailable">
                                            <i class="fa fa-twitter notConnected"></i>
                                        </a>
                                        <div class="__react_component_tooltip place-top type-warning " id="notConnectedSocialChannels"
                                            data-id="tooltip" >Please connect these social accounts in settings.</div>
                                    </span>
                                </div>
                                <div class="socialShareItem">
                                    <a class="linkedin-link rss-share-link li">
                                        <i class="fa fa-linkedin" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-four-of-twelve">
                    <div class="feed-job-item-inner">
                        <a target="_blank" href="https://andrewgelman.com/2018/09/04/stanhel-tutorial-videos/" class="visit-source-link">
                            <i class="material-icons">launch</i>
                            Visit source

                        </a>
                        <div class="rss-block">
                            <div class="banner-hot-header no-img-header">
                                <div class="rss-banner-captions">
                                    <p>StanCon 2018 Helsinki tutorial videos online</p>
                                </div>
                            </div>
                            <div class="hot-div-body">
                                <div class="date-and-time">
                                   Added
                                   14 days ago

                                </div>
                                <div class="text-header">
                                    <p>
                                        StanCon 2018 Helsinki tutorial videos are now online at Stan YouTube channel List of tutorials at StanCon 2018 Helsinki Basics
                                        of Bayesian inference and Stan, parts 1 + 2, Jonah Gabry &amp;amp; Lauren Kennedy
                                        Hierarchical models, parts 1 + 2, Ben Goodrich Stan C++ development: Adding a new
                                        function to Stan, parts 1 + 2, […] The post StanCon 2018 Helsinki tutorial videos
                                        online appeared first on Statistical Modeling, Causal Inference, and Social Science.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="feed-job-share-block feed-job-share-block-12">
                        <div class="feed-job-share-block-title">
                            <span>Share</span>
                        </div>
                        <div>
                            <div class="socialIcons clearfix">
                                <div class="socialShareItem">
                                    <a class="internal-icon" title="Internal channel">
                                        <i class="material-icons">vpn_lock</i>
                                    </a>
                                </div>
                                <div class="socialShareItem">
                                    <span data-tip="Please connect these social accounts in settings." data-for="notConnectedSocialChannels"
                                        currentitem="false">

                                        <a class="fb linkNotAvailable">
                                            <i class="fa fa-facebook notConnected"></i>
                                        </a>
                                        <div class="__react_component_tooltip place-top type-warning " id="notConnectedSocialChannels"
                                            data-id="tooltip" >Please connect these social accounts in settings.</div>
                                    </span>
                                </div>
                                <div class="socialShareItem">
                                    <span data-tip="Please connect these social accounts in settings." data-for="notConnectedSocialChannels"
                                        currentitem="false">

                                        <a class="tw linkNotAvailable">
                                            <i class="fa fa-twitter notConnected"></i>
                                        </a>
                                        <div class="__react_component_tooltip place-top type-warning " id="notConnectedSocialChannels"
                                            data-id="tooltip" >Please connect these social accounts in settings.</div>
                                    </span>
                                </div>
                                <div class="socialShareItem">
                                    <a class="linkedin-link rss-share-link li">
                                        <i class="fa fa-linkedin" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-four-of-twelve">
                    <div class="feed-job-item-inner">
                        <a target="_blank" href="https://andrewgelman.com/2018/09/03/china-ai-parity-west-2020/" class="visit-source-link">
                            <i class="material-icons">launch</i>
                           Visit source

                        </a>
                        <div class="rss-block">
                            <div class="banner-hot-header no-img-header">
                                <div class="rss-banner-captions">
                                    <p>A.I. parity with the West in 2020</p>
                                </div>
                            </div>
                            <div class="hot-div-body">
                                <div class="date-and-time">
                                   Added
                                   15 days ago

                                </div>
                                <div class="text-header">
                                    <p>
                                        Someone just sent me a link to an editorial by Ken Church, in the journal Natural Language Engineering (who knew that journal
                                        was still going? I’d have thought open access would’ve killed it). The abstract of
                                        Church’s column says of China, There is a bold government plan for AI with specific
                                        milestones for parity with […] The post A.I. parity with the West in 2020 appeared
                                        first on Statistical Modeling, Causal Inference, and Social Science.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="feed-job-share-block feed-job-share-block-13">
                        <div class="feed-job-share-block-title">
                            <span>Share</span>
                        </div>
                        <div>
                            <div class="socialIcons clearfix">
                                <div class="socialShareItem">
                                    <a class="internal-icon" title="Internal channel">
                                        <i class="material-icons">vpn_lock</i>
                                    </a>
                                </div>
                                <div class="socialShareItem">
                                    <span data-tip="Please connect these social accounts in settings." data-for="notConnectedSocialChannels"
                                        currentitem="false">

                                        <a class="fb linkNotAvailable">
                                            <i class="fa fa-facebook notConnected"></i>
                                        </a>
                                        <div class="__react_component_tooltip place-top type-warning " id="notConnectedSocialChannels"
                                            data-id="tooltip" >Please connect these social accounts in settings.</div>
                                    </span>
                                </div>
                                <div class="socialShareItem">
                                    <span data-tip="Please connect these social accounts in settings." data-for="notConnectedSocialChannels"
                                        currentitem="false">

                                        <a class="tw linkNotAvailable">
                                            <i class="fa fa-twitter notConnected"></i>
                                        </a>
                                        <div class="__react_component_tooltip place-top type-warning " id="notConnectedSocialChannels"
                                            data-id="tooltip">Please connect these social accounts in settings.</div>
                                    </span>
                                </div>
                                <div class="socialShareItem">
                                    <a class="linkedin-link rss-share-link li">
                                        <i class="fa fa-linkedin" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-four-of-twelve">
                    <div class="feed-job-item-inner">
                        <a target="_blank" href="https://andrewgelman.com/2018/09/03/isaac-newton-alchemy-michael-jordan-golf/"
                            class="visit-source-link">
                            <i class="material-icons">launch</i>
                          Visit source

                        </a>
                        <div class="rss-block">
                            <div class="banner-hot-header no-img-header" >
                                <div class="rss-banner-captions">
                                    <p>Isaac Newton : Alchemy :: Michael Jordan : Golf</p>
                                </div>
                            </div>
                            <div class="hot-div-body">
                                <div class="date-and-time">
                                    Added
                                    15 days ago

                                </div>
                                <div class="text-header">
                                    <p>
                                        Not realizing the domain-specificity of their successes. The post Isaac Newton : Alchemy :: Michael Jordan : Golf appeared
                                        first on Statistical Modeling, Causal Inference, and Social Science.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="feed-job-share-block feed-job-share-block-14">
                        <div class="feed-job-share-block-title">
                            <span>Share</span>
                        </div>
                        <div>
                            <div class="socialIcons clearfix">
                                <div class="socialShareItem">
                                    <a class="internal-icon" title="Internal channel">
                                        <i class="material-icons">vpn_lock</i>
                                    </a>
                                </div>
                                <div class="socialShareItem">
                                    <span data-tip="Please connect these social accounts in settings." data-for="notConnectedSocialChannels"
                                        currentitem="false">

                                        <a class="fb linkNotAvailable">
                                            <i class="fa fa-facebook notConnected"></i>
                                        </a>
                                        <div class="__react_component_tooltip place-top type-warning " id="notConnectedSocialChannels"
                                            data-id="tooltip" >Please connect these social accounts in settings.</div>
                                    </span>
                                </div>
                                <div class="socialShareItem">
                                    <span data-tip="Please connect these social accounts in settings." data-for="notConnectedSocialChannels"
                                        currentitem="false">

                                        <a class="tw linkNotAvailable">
                                            <i class="fa fa-twitter notConnected"></i>
                                        </a>
                                        <div class="__react_component_tooltip place-top type-warning " id="notConnectedSocialChannels"
                                            data-id="tooltip" >Please connect these social accounts in settings.</div>
                                    </span>
                                </div>
                                <div class="socialShareItem">
                                    <a class="linkedin-link rss-share-link li">
                                        <i class="fa fa-linkedin" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-four-of-twelve">
                    <div class="feed-job-item-inner">
                        <a target="_blank" href="https://gigaom.com/2018/08/31/five-steps-to-delivering-devops-at-scale/" class="visit-source-link">
                            <i class="material-icons">launch</i>
                            Visit source

                        </a>
                        <div class="rss-block">
                            <div class="banner-hot-header hot-overlay" >
                                <div class="rss-banner-captions">
                                    <p>Five steps to delivering DevOps at scale</p>
                                </div>
                            </div>
                            <div class="hot-div-body">
                                <div class="date-and-time">
                                   Added
                                   18 days ago

                                </div>
                                <div class="text-header">
                                    <p>So, you’re thinking of taking DevOps out of the relatively safe, yet challenging environment
                                        of the controlled initiative, and into the far…</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="feed-job-share-block feed-job-share-block-15">
                        <div class="feed-job-share-block-title">
                            <span>Share</span>
                        </div>
                        <div>
                            <div class="socialIcons clearfix">
                                <div class="socialShareItem">
                                    <a class="internal-icon" title="Internal channel">
                                        <i class="material-icons">vpn_lock</i>
                                    </a>
                                </div>
                                <div class="socialShareItem">
                                    <span data-tip="Please connect these social accounts in settings." data-for="notConnectedSocialChannels"
                                        currentitem="false">

                                        <a class="fb linkNotAvailable">
                                            <i class="fa fa-facebook notConnected"></i>
                                        </a>
                                        <div class="__react_component_tooltip place-top type-warning " id="notConnectedSocialChannels"
                                            data-id="tooltip" >Please connect these social accounts in settings.</div>
                                    </span>
                                </div>
                                <div class="socialShareItem">
                                    <span data-tip="Please connect these social accounts in settings." data-for="notConnectedSocialChannels"
                                        currentitem="false">

                                        <a class="tw linkNotAvailable">
                                            <i class="fa fa-twitter notConnected"></i>
                                        </a>
                                        <div class="__react_component_tooltip place-top type-warning " id="notConnectedSocialChannels"
                                            data-id="tooltip" >Please connect these social accounts in settings.</div>
                                    </span>
                                </div>
                                <div class="socialShareItem">
                                    <a class="linkedin-link rss-share-link li">
                                        <i class="fa fa-linkedin" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-four-of-twelve">
                    <div class="feed-job-item-inner">
                        <a target="_blank" href="https://gigaom.com/2018/08/31/ai-for-humanity-not-corporations/" class="visit-source-link">
                            <i class="material-icons">launch</i>
                          Visit source

                        </a>
                        <div class="rss-block">
                            <div class="banner-hot-header hot-overlay">
                                <div class="rss-banner-captions">
                                    <p>AI for Humanity, Not Corporations</p>
                                </div>
                            </div>
                            <div class="hot-div-body">
                                <div class="date-and-time">
                                    Added
                                    18 days ago

                                </div>
                                <div class="text-header">
                                    <p>The majority of AI and machine learning is built to bolster the revenue streams of corporations
                                        rather than for the interest of…</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="feed-job-share-block feed-job-share-block-16">
                        <div class="feed-job-share-block-title">
                            <span>Share</span>
                        </div>
                        <div>
                            <div class="socialIcons clearfix">
                                <div class="socialShareItem">
                                    <a class="internal-icon" title="Internal channel">
                                        <i class="material-icons">vpn_lock</i>
                                    </a>
                                </div>
                                <div class="socialShareItem">
                                    <span data-tip="Please connect these social accounts in settings." data-for="notConnectedSocialChannels"
                                        currentitem="false">

                                        <a class="fb linkNotAvailable">
                                            <i class="fa fa-facebook notConnected"></i>
                                        </a>
                                        <div class="__react_component_tooltip place-top type-warning " id="notConnectedSocialChannels"
                                            data-id="tooltip" >Please connect these social accounts in settings.</div>
                                    </span>
                                </div>
                                <div class="socialShareItem">
                                    <span data-tip="Please connect these social accounts in settings." data-for="notConnectedSocialChannels"
                                        currentitem="false">

                                        <a class="tw linkNotAvailable">
                                            <i class="fa fa-twitter notConnected"></i>
                                        </a>
                                        <div class="__react_component_tooltip place-top type-warning " id="notConnectedSocialChannels"
                                            data-id="tooltip" >Please connect these social accounts in settings.</div>
                                    </span>
                                </div>
                                <div class="socialShareItem">
                                    <a class="linkedin-link rss-share-link li">
                                        <i class="fa fa-linkedin" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-four-of-twelve">
                    <div class="feed-job-item-inner">
                        <a target="_blank" href="https://gigaom.com/2018/08/30/voices-in-ai-episode-65-a-conversation-with-luciano-floridi/"
                            class="visit-source-link">
                            <i class="material-icons">launch</i>
                         Visit source

                        </a>
                        <div class="rss-block">
                            <div class="banner-hot-header hot-overlay" >
                                <div class="rss-banner-captions">
                                    <p>Voices in AI – Episode 65: A Conversation with Luciano Floridi</p>
                                </div>
                            </div>
                            <div class="hot-div-body">
                                <div class="date-and-time">
                                   Added
                                    19 days ago

                                </div>
                                <div class="text-header">
                                    <p>About this Episode Episode 65 of Voices in AI features host Byron Reese and Luciano Floridi
                                        discuss ethics, information, AI and government…</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="feed-job-share-block feed-job-share-block-17">
                        <div class="feed-job-share-block-title">
                            <span>Share</span>
                        </div>
                        <div>
                            <div class="socialIcons clearfix">
                                <div class="socialShareItem">
                                    <a class="internal-icon" title="Internal channel">
                                        <i class="material-icons">vpn_lock</i>
                                    </a>
                                </div>
                                <div class="socialShareItem">
                                    <span data-tip="Please connect these social accounts in settings." data-for="notConnectedSocialChannels"
                                        currentitem="false">

                                        <a class="fb linkNotAvailable">
                                            <i class="fa fa-facebook notConnected"></i>
                                        </a>
                                        <div class="__react_component_tooltip place-top type-warning " id="notConnectedSocialChannels"
                                            data-id="tooltip">Please connect these social accounts in settings.</div>
                                    </span>
                                </div>
                                <div class="socialShareItem">
                                    <span data-tip="Please connect these social accounts in settings." data-for="notConnectedSocialChannels"
                                        currentitem="false">

                                        <a class="tw linkNotAvailable">
                                            <i class="fa fa-twitter notConnected"></i>
                                        </a>
                                        <div class="__react_component_tooltip place-top type-warning " id="notConnectedSocialChannels"
                                            data-id="tooltip" >Please connect these social accounts in settings.</div>
                                    </span>
                                </div>
                                <div class="socialShareItem">
                                    <a class="linkedin-link rss-share-link li">
                                        <i class="fa fa-linkedin" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-four-of-twelve">
                    <div class="feed-job-item-inner">
                        <a target="_blank" href="https://gigaom.com/2018/08/23/voices-in-ai-episode-64-a-conversation-with-eli-david/"
                            class="visit-source-link">
                            <i class="material-icons">launch</i>
                           Visit source

                        </a>
                        <div class="rss-block">
                            <div class="banner-hot-header hot-overlay" >
                                <div class="rss-banner-captions">
                                    <p>Voices in AI – Episode 64: A Conversation with Eli David</p>
                                </div>
                            </div>
                            <div class="hot-div-body">
                                <div class="date-and-time">
                                    Added

                                a month ago

                                </div>
                                <div class="text-header">
                                    <p>About this Episode Episode 64 of Voices in AI features host Byron Reese and Dr. Eli David
                                        discuss evolutionary computation, deep learning…</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="feed-job-share-block feed-job-share-block-18">
                        <div class="feed-job-share-block-title">
                            <span>Share</span>
                        </div>
                        <div>
                            <div class="socialIcons clearfix">
                                <div class="socialShareItem">
                                    <a class="internal-icon" title="Internal channel">
                                        <i class="material-icons">vpn_lock</i>
                                    </a>
                                </div>
                                <div class="socialShareItem">
                                    <span data-tip="Please connect these social accounts in settings." data-for="notConnectedSocialChannels"
                                        currentitem="false">

                                        <a class="fb linkNotAvailable">
                                            <i class="fa fa-facebook notConnected"></i>
                                        </a>
                                        <div class="__react_component_tooltip place-top type-warning " id="notConnectedSocialChannels"
                                            data-id="tooltip" >Please connect these social accounts in settings.</div>
                                    </span>
                                </div>
                                <div class="socialShareItem">
                                    <span data-tip="Please connect these social accounts in settings." data-for="notConnectedSocialChannels"
                                        currentitem="false">

                                        <a class="tw linkNotAvailable">
                                            <i class="fa fa-twitter notConnected"></i>
                                        </a>
                                        <div class="__react_component_tooltip place-top type-warning " id="notConnectedSocialChannels"
                                            data-id="tooltip" >Please connect these social accounts in settings.</div>
                                    </span>
                                </div>
                                <div class="socialShareItem">
                                    <a class="linkedin-link rss-share-link li">
                                        <i class="fa fa-linkedin" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-four-of-twelve">
                    <div class="feed-job-item-inner">
                        <a target="_blank" href="https://gigaom.com/2018/08/17/uk-tax-authorities-make-tax-digital-whether-businesses-want-it-or-not/"
                            class="visit-source-link">
                            <i class="material-icons">launch</i>
                            Visit source

                        </a>
                        <div class="rss-block">
                            <div class="banner-hot-header hot-overlay" >
                                <div class="rss-banner-captions">
                                    <p>UK tax authorities Make Tax Digital, whether businesses want it or not</p>
                                </div>
                            </div>
                            <div class="hot-div-body">
                                <div class="date-and-time">
                                   Added
                                   a month ago

                                </div>
                                <div class="text-header">
                                    <p>In a changing world, it is good to know that some certainties remain, such as when companies
                                        log their affairs and give…</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="feed-job-share-block feed-job-share-block-19">
                        <div class="feed-job-share-block-title">
                            <span>Share</span>
                        </div>
                        <div>
                            <div class="socialIcons clearfix">
                                <div class="socialShareItem">
                                    <a class="internal-icon" title="Internal channel">
                                        <i class="material-icons">vpn_lock</i>
                                    </a>
                                </div>
                                <div class="socialShareItem">
                                    <span data-tip="Please connect these social accounts in settings." data-for="notConnectedSocialChannels"
                                        currentitem="false">

                                        <a class="fb linkNotAvailable">
                                            <i class="fa fa-facebook notConnected"></i>
                                        </a>
                                        <div class="__react_component_tooltip place-top type-warning " id="notConnectedSocialChannels"
                                            data-id="tooltip" >Please connect these social accounts in settings.</div>
                                    </span>
                                </div>
                                <div class="socialShareItem">
                                    <span data-tip="Please connect these social accounts in settings." data-for="notConnectedSocialChannels"
                                        currentitem="false">

                                        <a class="tw linkNotAvailable">
                                            <i class="fa fa-twitter notConnected"></i>
                                        </a>
                                        <div class="__react_component_tooltip place-top type-warning " id="notConnectedSocialChannels"
                                            data-id="tooltip" >Please connect these social accounts in settings.</div>
                                    </span>
                                </div>
                                <div class="socialShareItem">
                                    <a class="linkedin-link rss-share-link li">
                                        <i class="fa fa-linkedin" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-three-of-twelve topicintrest rightSidebar">
            <div class="trending-xl-container rss-chanel-feed">
                <div class="trending-tag-container ps">
                    <div class="widget new-block-rss">
                        <section>
                            <form role="form">
                                <div class="form-row Rss-setting">
                                    <label>RSS</label>
                                    <div class="form-row  text">
                                        <input type="text" id="rss" name="rss" value="" placeholder="RSS channel"/>
                                        <div class="form-valid-message"></div>
                                    </div>
                                </div>
                                <div class="form-row rss-setting-button">
                                    <button type="submit" class="btn btn-primary">
                                        <span class="rss-field">+</span>
                                        Add RSS channel

                                    </button>
                                </div>
                            </form>
                        </section>
                    </div>
                    <div class="widget new-block-rss rss-links-wrapper">
                        <div class="setting-rss-body">
                            <div class="rss-links-title">Connected RSS feed</div>
                            <ul class="rss-links-list"></ul>
                        </div>
                    </div>
                    <div class="ps__rail-x">
                        <div class="ps__thumb-x" tabindex="0"></div>
                    </div>
                    <div class="ps__rail-y">
                        <div class="ps__thumb-y" tabindex="0" ></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    )
}
fetchJobsStaticData(){
    return(
        <div class="curated-page">
        <div id="dropdown-asset-container" class="collapsibleDropdown ">
            <div class="dropdown-container collapsible">
                <div class="dropdown-container-inner fixedDropdown">
                    <div class="dropdown-inner-wrapper">
                        <div class="row">
                            <div class="col-one-of-four">
                                <div class="curated-filter-inner">
                                    <div class="form-row">
                                        <label>Search</label>
                                        <input type="text" placeholder="Search Job Title"/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-one-of-four">
                                <div class="curated-filter-inner">
                                    <div class="form-row">
                                        <label>Industry</label>
                                        <div class="select-wrapper">
                                            <select>
                                                <option>Industry1</option>
                                                <option>Industry2</option>
                                                <option>Industry3</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-one-of-four">
                                <div class="curated-filter-inner">
                                    <div class="form-row">
                                        <label>Country</label>
                                        <div class="select-wrapper">
                                            <select>
                                                <option>Country1</option>
                                                <option>Country2</option>
                                                <option>Country3</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-one-of-four">
                                <div class="curated-filter-inner">
                                    <div class="form-row">
                                        <label>Locations</label>
                                        <div class="select-wrapper">
                                            <select>
                                                <option>Locations1</option>
                                                <option>Locations2</option>
                                                <option>Locations3</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-one-of-four">
                                <div class="curated-filter-inner">
                                    <div class="form-row">
                                        <label>Type</label>
                                        <div class="select-wrapper">
                                            <select>
                                                <option>Type1</option>
                                                <option>Type2</option>
                                                <option>Type3</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-one-of-four">
                                <div class="curated-filter-inner">
                                    <div class="form-row">
                                        <label>Salary From</label>
                                        <div class="select-wrapper">
                                            <select>
                                                <option>Salary From1</option>
                                                <option>Salary From2</option>
                                                <option>Salary From3</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-one-of-four">
                                <div class="curated-filter-inner">
                                    <div class="form-row">
                                        <label>Salary To</label>
                                        <div class="select-wrapper">
                                            <select>
                                                <option>Salary To1</option>
                                                <option>Salary To2</option>
                                                <option>Salary To3</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="top-shadow-wrapper">
                        <div class="top-shadow"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="feed-jobs-wrapper">
            <div class="row">
                <div id="jobTour"></div>
                <div class="feed-job-item col-one-of-four">
                    <div class="feed-job-item-inner">
                        <div>
                            <div class="job-date">
                                <p>December 2017</p>
                            </div>
                            <div class="job-title">
                                <p>job one two</p>
                            </div>
                            <div class="job-location">
                                <i class="material-icons">location_on</i>
                                <p>

                                   ahmedabad

                                </p>
                            </div>
                            <div class="job-descripation">
                                <div id="jobFullDesc" class="jobFullDesc"></div>
                                <p>

                                    urget required data

                                </p>
                            </div>
                        </div>
                        <div class="feed-job-share-block feed-job-share-block-0 ">
                            <div class="feed-job-share-block-title">
                                <span>Share</span>
                            </div>
                            <div>
                                <div class="socialIcons clearfix">
                                    <div class="socialShareItem">
                                        <a class="internal-icon" title="Internal channel">
                                            <i class="material-icons">vpn_lock</i>
                                        </a>
                                    </div>
                                    <div class="socialShareItem">
                                        <span data-tip="Please connect these social accounts in settings." data-for="notConnectedSocialChannels"
                                            currentitem="false">

                                            <a class="fb linkNotAvailable">
                                                <i class="fa fa-facebook notConnected"></i>
                                            </a>
                                            <div class="__react_component_tooltip place-top type-dark " id="notConnectedSocialChannels"
                                                data-id="tooltip"></div>
                                        </span>
                                    </div>
                                    <div class="socialShareItem">
                                        <span data-tip="Please connect these social accounts in settings." data-for="notConnectedSocialChannels"
                                            currentitem="false">

                                            <a class="tw linkNotAvailable">
                                                <i class="fa fa-twitter notConnected"></i>
                                            </a>
                                            <div class="__react_component_tooltip place-top type-dark " id="notConnectedSocialChannels"
                                                data-id="tooltip"></div>
                                        </span>
                                    </div>
                                    <div class="socialShareItem">
                                        <span data-tip="Link is not available." data-for="notConnectedSocialChannels" currentitem="false">

                                            <a class="li linkNotAvailable">
                                                <i class="fa fa-linkedin"></i>
                                            </a>
                                            <div class="__react_component_tooltip place-top type-dark " id="notConnectedSocialChannels"
                                                data-id="tooltip"></div>
                                        </span>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="feed-job-item col-one-of-four">
                    <div class="feed-job-item-inner">
                        <div>
                            <div class="job-date">
                                <p>December 2017</p>
                            </div>
                            <div class="job-title">
                                <p>job one</p>
                            </div>
                            <div class="job-location">
                                <i class="material-icons">location_on</i>
                                <p>>ahmedabad

                                </p>
                            </div>
                            <div class="job-descripation">
                                <div id="jobFullDesc" class="jobFullDesc"></div>
                                <p>
                                   urget required data

                                </p>
                            </div>
                        </div>
                        <div class="feed-job-share-block feed-job-share-block-0 ">
                            <div class="feed-job-share-block-title">
                                <span>Share</span>
                            </div>
                            <div>
                                <div class="socialIcons clearfix">
                                    <div class="socialShareItem">
                                        <a class="internal-icon" title="Internal channel">
                                            <i class="material-icons">vpn_lock</i>
                                        </a>
                                    </div>
                                    <div class="socialShareItem">
                                        <span data-tip="Please connect these social accounts in settings." data-for="notConnectedSocialChannels"
                                            currentitem="false">

                                            <a class="fb linkNotAvailable">
                                                <i class="fa fa-facebook notConnected"></i>
                                            </a>
                                            <div class="__react_component_tooltip place-top type-dark " id="notConnectedSocialChannels"
                                                data-id="tooltip"></div>
                                        </span>
                                    </div>
                                    <div class="socialShareItem">
                                        <span data-tip="Please connect these social accounts in settings." data-for="notConnectedSocialChannels"
                                            currentitem="false">

                                            <a class="tw linkNotAvailable">
                                                <i class="fa fa-twitter notConnected"></i>
                                            </a>
                                            <div class="__react_component_tooltip place-top type-dark " id="notConnectedSocialChannels"
                                                data-id="tooltip"></div>
                                        </span>
                                    </div>
                                    <div class="socialShareItem">
                                        <span data-tip="Link is not available." data-for="notConnectedSocialChannels" currentitem="false">

                                            <a class="li linkNotAvailable">
                                                <i class="fa fa-linkedin"></i>
                                            </a>
                                            <div class="__react_component_tooltip place-top type-dark " id="notConnectedSocialChannels"
                                                data-id="tooltip"></div>
                                        </span>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="feed-job-item col-one-of-four">
                    <div class="feed-job-item-inner">
                        <div>
                            <div class="job-date">
                                <p>January 2018</p>
                            </div>
                            <div class="job-title">
                                <p>VisiblyTest01</p>
                            </div>
                            <div class="job-location">
                                <i class="material-icons">location_on</i>
                                <p>
                                   Cambridge

                                </p>
                            </div>
                            <div class="job-descripation">
                                <div id="jobFullDesc" class="jobFullDesc"></div>
                                <p>

                                   LOGIC MELON TEST VACANCY - PLEASE DO NOT APPLY!!! Testing the initial integration to MiHomeCare
                                    Jobs to make sure that the template is compiled properly and that the fields are getting
                                    sent through correctly. THIS IS A LOGIC MELON TEST VACANCY - PLEASE DO NOT APPLY FOR THIS
                                    ROLE AS IT DOES NOT EXIST!! € - Testing Euro Sign £ - Testing Pound Sign


                                </p>
                            </div>
                        </div>
                        <div class="feed-job-share-block feed-job-share-block-0 ">
                            <div class="feed-job-share-block-title">
                                <span>Share</span>
                            </div>
                            <div>
                                <div class="socialIcons clearfix">
                                    <div class="socialShareItem">
                                        <a class="internal-icon" title="Internal channel">
                                            <i class="material-icons">vpn_lock</i>
                                        </a>
                                    </div>
                                    <div class="socialShareItem">
                                        <span data-tip="Please connect these social accounts in settings." data-for="notConnectedSocialChannels"
                                            currentitem="false">

                                            <a class="fb linkNotAvailable">
                                                <i class="fa fa-facebook notConnected"></i>
                                            </a>
                                            <div class="__react_component_tooltip place-top type-dark " id="notConnectedSocialChannels"
                                                data-id="tooltip"></div>
                                        </span>
                                    </div>
                                    <div class="socialShareItem">
                                        <span data-tip="Please connect these social accounts in settings." data-for="notConnectedSocialChannels"
                                            currentitem="false">

                                            <a class="tw linkNotAvailable">
                                                <i class="fa fa-twitter notConnected"></i>
                                            </a>
                                            <div class="__react_component_tooltip place-top type-dark " id="notConnectedSocialChannels"
                                                data-id="tooltip"></div>
                                        </span>
                                    </div>
                                    <div class="socialShareItem">
                                        <span data-tip="Link is not available." data-for="notConnectedSocialChannels" currentitem="false">

                                            <a class="li linkNotAvailable">
                                                <i class="fa fa-linkedin"></i>
                                            </a>
                                            <div class="__react_component_tooltip place-top type-dark " id="notConnectedSocialChannels"
                                                data-id="tooltip"></div>
                                        </span>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="feed-job-item col-one-of-four">
                    <div class="feed-job-item-inner">
                        <div>
                            <div class="job-date">
                                <p>December 2017</p>
                            </div>
                            <div class="job-title">
                                <p>job one</p>
                            </div>
                            <div class="job-location">
                                <i class="material-icons">location_on</i>
                                <p>
                                    ahmedabad

                                </p>
                            </div>
                            <div class="job-descripation">
                                <div id="jobFullDesc" class="jobFullDesc"></div>
                                <p>
                                   urget required data

                                </p>
                            </div>
                        </div>
                        <div class="feed-job-share-block feed-job-share-block-0 ">
                            <div class="feed-job-share-block-title">
                                <span>Share</span>
                            </div>
                            <div>
                                <div class="socialIcons clearfix">
                                    <div class="socialShareItem">
                                        <a class="internal-icon" title="Internal channel">
                                            <i class="material-icons">vpn_lock</i>
                                        </a>
                                    </div>
                                    <div class="socialShareItem">
                                        <span data-tip="Please connect these social accounts in settings." data-for="notConnectedSocialChannels"
                                            currentitem="false">

                                            <a class="fb linkNotAvailable">
                                                <i class="fa fa-facebook notConnected"></i>
                                            </a>
                                            <div class="__react_component_tooltip place-top type-dark " id="notConnectedSocialChannels"
                                                data-id="tooltip"></div>
                                        </span>
                                    </div>
                                    <div class="socialShareItem">
                                        <span data-tip="Please connect these social accounts in settings." data-for="notConnectedSocialChannels"
                                            currentitem="false">

                                            <a class="tw linkNotAvailable">
                                                <i class="fa fa-twitter notConnected"></i>
                                            </a>
                                            <div class="__react_component_tooltip place-top type-dark " id="notConnectedSocialChannels"
                                                data-id="tooltip"></div>
                                        </span>
                                    </div>
                                    <div class="socialShareItem">
                                        <span data-tip="Link is not available." data-for="notConnectedSocialChannels" currentitem="false">

                                            <a class="li linkNotAvailable">
                                                <i class="fa fa-linkedin"></i>
                                            </a>
                                            <div class="__react_component_tooltip place-top type-dark " id="notConnectedSocialChannels"
                                                data-id="tooltip"></div>
                                        </span>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="feed-job-item col-one-of-four">
                    <div class="feed-job-item-inner">
                        <div>
                            <div class="job-date">
                                <p>December 2017</p>
                            </div>
                            <div class="job-title">
                                <p>job one</p>
                            </div>
                            <div class="job-location">
                                <i class="material-icons">location_on</i>
                                <p>
                                   ahmedabad

                                </p>
                            </div>
                            <div class="job-descripation">
                                <div id="jobFullDesc" class="jobFullDesc"></div>
                                <p>
                                    urget required data

                                </p>
                            </div>
                        </div>
                        <div class="feed-job-share-block feed-job-share-block-0 ">
                            <div class="feed-job-share-block-title">
                                <span>Share</span>
                            </div>
                            <div>
                                <div class="socialIcons clearfix">
                                    <div class="socialShareItem">
                                        <a class="internal-icon" title="Internal channel">
                                            <i class="material-icons">vpn_lock</i>
                                        </a>
                                    </div>
                                    <div class="socialShareItem">
                                        <span data-tip="Please connect these social accounts in settings." data-for="notConnectedSocialChannels"
                                            currentitem="false">

                                            <a class="fb linkNotAvailable">
                                                <i class="fa fa-facebook notConnected"></i>
                                            </a>
                                            <div class="__react_component_tooltip place-top type-dark " id="notConnectedSocialChannels"
                                                data-id="tooltip"></div>
                                        </span>
                                    </div>
                                    <div class="socialShareItem">
                                        <span data-tip="Please connect these social accounts in settings." data-for="notConnectedSocialChannels"
                                            currentitem="false">

                                            <a class="tw linkNotAvailable">
                                                <i class="fa fa-twitter notConnected"></i>
                                            </a>
                                            <div class="__react_component_tooltip place-top type-dark " id="notConnectedSocialChannels"
                                                data-id="tooltip"></div>
                                        </span>
                                    </div>
                                    <div class="socialShareItem">
                                        <span data-tip="Link is not available." data-for="notConnectedSocialChannels" currentitem="false">

                                            <a class="li linkNotAvailable">
                                                <i class="fa fa-linkedin"></i>
                                            </a>
                                            <div class="__react_component_tooltip place-top type-dark " id="notConnectedSocialChannels"
                                                data-id="tooltip"></div>
                                        </span>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="feed-job-item col-one-of-four">
                    <div class="feed-job-item-inner">
                        <div>
                            <div class="job-date">
                                <p>January 2018</p>
                            </div>
                            <div class="job-title">
                                <p>VisiblyTest01</p>
                            </div>
                            <div class="job-location">
                                <i class="material-icons">location_on</i>
                                <p>
                                   Cambridge

                                </p>
                            </div>
                            <div class="job-descripation">
                                <div id="jobFullDesc" class="jobFullDesc"></div>
                                <p>
                                    LOGIC MELON TEST VACANCY - PLEASE DO NOT APPLY!!! Testing the initial integration to MiHomeCare
                                    Jobs to make sure that the template is compiled properly and that the fields are getting
                                    sent through correctly. THIS IS A LOGIC MELON TEST VACANCY - PLEASE DO NOT APPLY FOR THIS
                                    ROLE AS IT DOES NOT EXIST!! € - Testing Euro Sign £ - Testing Pound Sign


                                </p>
                            </div>
                        </div>
                        <div class="feed-job-share-block feed-job-share-block-0 ">
                            <div class="feed-job-share-block-title">
                                <span>Share</span>
                            </div>
                            <div>
                                <div class="socialIcons clearfix">
                                    <div class="socialShareItem">
                                        <a class="internal-icon" title="Internal channel">
                                            <i class="material-icons">vpn_lock</i>
                                        </a>
                                    </div>
                                    <div class="socialShareItem">
                                        <span data-tip="Please connect these social accounts in settings." data-for="notConnectedSocialChannels"
                                            currentitem="false">

                                            <a class="fb linkNotAvailable">
                                                <i class="fa fa-facebook notConnected"></i>
                                            </a>
                                            <div class="__react_component_tooltip place-top type-dark " id="notConnectedSocialChannels"
                                                data-id="tooltip"></div>
                                        </span>
                                    </div>
                                    <div class="socialShareItem">
                                        <span data-tip="Please connect these social accounts in settings." data-for="notConnectedSocialChannels"
                                            currentitem="false">

                                            <a class="tw linkNotAvailable">
                                                <i class="fa fa-twitter notConnected"></i>
                                            </a>
                                            <div class="__react_component_tooltip place-top type-dark " id="notConnectedSocialChannels"
                                                data-id="tooltip"></div>
                                        </span>
                                    </div>
                                    <div class="socialShareItem">
                                        <span data-tip="Link is not available." data-for="notConnectedSocialChannels" currentitem="false">

                                            <a class="li linkNotAvailable">
                                                <i class="fa fa-linkedin"></i>
                                            </a>
                                            <div class="__react_component_tooltip place-top type-dark " id="notConnectedSocialChannels"
                                                data-id="tooltip"></div>
                                        </span>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="feed-job-item col-one-of-four">
                    <div class="feed-job-item-inner">
                        <div>
                            <div class="job-date">
                                <p>April 2018</p>
                            </div>
                            <div class="job-title">
                                <p>Test post from Logic Melon</p>
                            </div>
                            <div class="job-location">
                                <i class="material-icons">location_on</i>
                                <p>
                                   United Kingdom

                                </p>
                            </div>
                            <div class="job-descripation">
                                <div id="jobFullDesc" class="jobFullDesc"></div>
                                <p>

                                   You will soon receive an invitation email from your admin with Visibly login credentials.
                                    Once you have received your login and password, you can go straight to the desktop APPor
                                    download themobile APPfrom the app store. You will soon receive an invitation email from
                                    your admin with Visibly login credentials. Once you have received your login and password,
                                    you can go straight to the desktop APPor download themobile APPfrom the app store. You will
                                    soon receive an invitation email from your admin with Visibly login credentials. Once you
                                    have received your login and password, you can go straight to the desktop APPor download
                                    themobile APPfrom the app store.


                                </p>
                            </div>
                        </div>
                        <div class="feed-job-share-block feed-job-share-block-0 ">
                            <div class="feed-job-share-block-title">
                                <span>Share</span>
                            </div>
                            <div>
                                <div class="socialIcons clearfix">
                                    <div class="socialShareItem">
                                        <a class="internal-icon" title="Internal channel">
                                            <i class="material-icons">vpn_lock</i>
                                        </a>
                                    </div>
                                    <div class="socialShareItem">
                                        <span data-tip="Please connect these social accounts in settings." data-for="notConnectedSocialChannels"
                                            currentitem="false">

                                            <a class="fb linkNotAvailable">
                                                <i class="fa fa-facebook notConnected"></i>
                                            </a>
                                            <div class="__react_component_tooltip place-top type-dark " id="notConnectedSocialChannels"
                                                data-id="tooltip"></div>
                                        </span>
                                    </div>
                                    <div class="socialShareItem">
                                        <span data-tip="Please connect these social accounts in settings." data-for="notConnectedSocialChannels"
                                            currentitem="false">

                                            <a class="tw linkNotAvailable">
                                                <i class="fa fa-twitter notConnected"></i>
                                            </a>
                                            <div class="__react_component_tooltip place-top type-dark " id="notConnectedSocialChannels"
                                                data-id="tooltip"></div>
                                        </span>
                                    </div>
                                    <div class="socialShareItem">
                                        <span data-tip="Link is not available." data-for="notConnectedSocialChannels" currentitem="false">

                                            <a class="li linkNotAvailable">
                                                <i class="fa fa-linkedin"></i>
                                            </a>
                                            <div class="__react_component_tooltip place-top type-dark " id="notConnectedSocialChannels"
                                                data-id="tooltip"></div>
                                        </span>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="feed-job-item col-one-of-four">
                    <div class="feed-job-item-inner">
                        <div>
                            <div class="job-date">
                                <p>April 2018</p>
                            </div>
                            <div class="job-title">
                                <p>Test post from Logic Melon</p>
                            </div>
                            <div class="job-location">
                                <i class="material-icons">location_on</i>
                                <p>
                                   United Kingdom

                                </p>
                            </div>
                            <div class="job-descripation">
                                <div id="jobFullDesc" class="jobFullDesc"></div>
                                <p>
                                   You will soon receive an invitation email from your admin with Visibly login credentials.
                                    Once you have received your login and password, you can go straight to the desktop APPor
                                    download themobile APPfrom the app store. You will soon receive an invitation email from
                                    your admin with Visibly login credentials. Once you have received your login and password,
                                    you can go straight to the desktop APPor download themobile APPfrom the app store. You will
                                    soon receive an invitation email from your admin with Visibly login credentials. Once you
                                    have received your login and password, you can go straight to the desktop APPor download
                                    themobile APPfrom the app store.


                                </p>
                            </div>
                        </div>
                        <div class="feed-job-share-block feed-job-share-block-0 ">
                            <div class="feed-job-share-block-title">
                                <span>Share</span>
                            </div>
                            <div>
                                <div class="socialIcons clearfix">
                                    <div class="socialShareItem">
                                        <a class="internal-icon" title="Internal channel">
                                            <i class="material-icons">vpn_lock</i>
                                        </a>
                                    </div>
                                    <div class="socialShareItem">
                                        <span data-tip="Please connect these social accounts in settings." data-for="notConnectedSocialChannels"
                                            currentitem="false">

                                            <a class="fb linkNotAvailable">
                                                <i class="fa fa-facebook notConnected"></i>
                                            </a>
                                            <div class="__react_component_tooltip place-top type-dark " id="notConnectedSocialChannels"
                                                data-id="tooltip"></div>
                                        </span>
                                    </div>
                                    <div class="socialShareItem">
                                        <span data-tip="Please connect these social accounts in settings." data-for="notConnectedSocialChannels"
                                            currentitem="false">

                                            <a class="tw linkNotAvailable">
                                                <i class="fa fa-twitter notConnected"></i>
                                            </a>
                                            <div class="__react_component_tooltip place-top type-dark " id="notConnectedSocialChannels"
                                                data-id="tooltip"></div>
                                        </span>
                                    </div>
                                    <div class="socialShareItem">
                                        <span data-tip="Link is not available." data-for="notConnectedSocialChannels" currentitem="false">

                                            <a class="li linkNotAvailable">
                                                <i class="fa fa-linkedin"></i>
                                            </a>
                                            <div class="__react_component_tooltip place-top type-dark " id="notConnectedSocialChannels"
                                                data-id="tooltip"></div>
                                        </span>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="feed-job-item col-one-of-four">
                    <div class="feed-job-item-inner">
                        <div>
                            <div class="job-date">
                                <p>April 2018</p>
                            </div>
                            <div class="job-title">
                                <p>Test post from Logic Melon</p>
                            </div>
                            <div class="job-location">
                                <i class="material-icons">location_on</i>
                                <p>United Kingdom

                                </p>
                            </div>
                            <div class="job-descripation">
                                <div id="jobFullDesc" class="jobFullDesc"></div>
                                <p>
                                  You will soon receive an invitation email from your admin with Visibly login credentials.
                                    Once you have received your login and password, you can go straight to the desktop APPor
                                    download themobile APPfrom the app store. You will soon receive an invitation email from
                                    your admin with Visibly login credentials. Once you have received your login and password,
                                    you can go straight to the desktop APPor download themobile APPfrom the app store. You will
                                    soon receive an invitation email from your admin with Visibly login credentials. Once you
                                    have received your login and password, you can go straight to the desktop APPor download
                                    themobile APPfrom the app store.


                                </p>
                            </div>
                        </div>
                        <div class="feed-job-share-block feed-job-share-block-0 ">
                            <div class="feed-job-share-block-title">
                                <span>Share</span>
                            </div>
                            <div>
                                <div class="socialIcons clearfix">
                                    <div class="socialShareItem">
                                        <a class="internal-icon" title="Internal channel">
                                            <i class="material-icons">vpn_lock</i>
                                        </a>
                                    </div>
                                    <div class="socialShareItem">
                                        <span data-tip="Please connect these social accounts in settings." data-for="notConnectedSocialChannels"
                                            currentitem="false">

                                            <a class="fb linkNotAvailable">
                                                <i class="fa fa-facebook notConnected"></i>
                                            </a>
                                            <div class="__react_component_tooltip place-top type-dark " id="notConnectedSocialChannels"
                                                data-id="tooltip"></div>
                                        </span>
                                    </div>
                                    <div class="socialShareItem">
                                        <span data-tip="Please connect these social accounts in settings." data-for="notConnectedSocialChannels"
                                            currentitem="false">

                                            <a class="tw linkNotAvailable">
                                                <i class="fa fa-twitter notConnected"></i>
                                            </a>
                                            <div class="__react_component_tooltip place-top type-dark " id="notConnectedSocialChannels"
                                                data-id="tooltip"></div>
                                        </span>
                                    </div>
                                    <div class="socialShareItem">
                                        <span data-tip="Link is not available." data-for="notConnectedSocialChannels" currentitem="false">

                                            <a class="li linkNotAvailable">
                                                <i class="fa fa-linkedin"></i>
                                            </a>
                                            <div class="__react_component_tooltip place-top type-dark " id="notConnectedSocialChannels"
                                                data-id="tooltip"></div>
                                        </span>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="feed-job-item col-one-of-four">
                    <div class="feed-job-item-inner">
                        <div>
                            <div class="job-date">
                                <p>April 2018</p>
                            </div>
                            <div class="job-title">
                                <p>Test post from Logic Melon</p>
                            </div>
                            <div class="job-location">
                                <i class="material-icons">location_on</i>
                                <p>
                                  United Kingdom

                                </p>
                            </div>
                            <div class="job-descripation">
                                <div id="jobFullDesc" class="jobFullDesc"></div>
                                <p>
                                   You will soon receive an invitation email from your admin with Visibly login credentials.
                                    Once you have received your login and password, you can go straight to the desktop APPor
                                    download themobile APPfrom the app store. You will soon receive an invitation email from
                                    your admin with Visibly login credentials. Once you have received your login and password,
                                    you can go straight to the desktop APPor download themobile APPfrom the app store. You will
                                    soon receive an invitation email from your admin with Visibly login credentials. Once you
                                    have received your login and password, you can go straight to the desktop APPor download
                                    themobile APPfrom the app store.


                                </p>
                            </div>
                        </div>
                        <div class="feed-job-share-block feed-job-share-block-0 ">
                            <div class="feed-job-share-block-title">
                                <span>Share</span>
                            </div>
                            <div>
                                <div class="socialIcons clearfix">
                                    <div class="socialShareItem">
                                        <a class="internal-icon" title="Internal channel">
                                            <i class="material-icons">vpn_lock</i>
                                        </a>
                                    </div>
                                    <div class="socialShareItem">
                                        <span data-tip="Please connect these social accounts in settings." data-for="notConnectedSocialChannels"
                                            currentitem="false">

                                            <a class="fb linkNotAvailable">
                                                <i class="fa fa-facebook notConnected"></i>
                                            </a>
                                            <div class="__react_component_tooltip place-top type-dark " id="notConnectedSocialChannels"
                                                data-id="tooltip"></div>
                                        </span>
                                    </div>
                                    <div class="socialShareItem">
                                        <span data-tip="Please connect these social accounts in settings." data-for="notConnectedSocialChannels"
                                            currentitem="false">

                                            <a class="tw linkNotAvailable">
                                                <i class="fa fa-twitter notConnected"></i>
                                            </a>
                                            <div class="__react_component_tooltip place-top type-dark " id="notConnectedSocialChannels"
                                                data-id="tooltip"></div>
                                        </span>
                                    </div>
                                    <div class="socialShareItem">
                                        <span data-tip="Link is not available." data-for="notConnectedSocialChannels" currentitem="false">

                                            <a class="li linkNotAvailable">
                                                <i class="fa fa-linkedin"></i>
                                            </a>
                                            <div class="__react_component_tooltip place-top type-dark " id="notConnectedSocialChannels"
                                                data-id="tooltip"></div>
                                        </span>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="feed-job-item col-one-of-four">
                    <div class="feed-job-item-inner">
                        <div>
                            <div class="job-date">
                                <p>April 2018</p>
                            </div>
                            <div class="job-title">
                                <p>Test job</p>
                            </div>
                            <div class="job-location">
                                <i class="material-icons">location_on</i>
                                <p>
                                   Camasnacroise

                                </p>
                            </div>
                            <div class="job-descripation">
                                <div id="jobFullDesc" class="jobFullDesc"></div>
                                <p>
                                    Just trying to see if the job is now getting posted in Visibly or not. This job should be
                                    posted on Visibly.

                                </p>
                            </div>
                        </div>
                        <div class="feed-job-share-block feed-job-share-block-0 ">
                            <div class="feed-job-share-block-title">
                                <span>Share</span>
                            </div>
                            <div>
                                <div class="socialIcons clearfix">
                                    <div class="socialShareItem">
                                        <a class="internal-icon" title="Internal channel">
                                            <i class="material-icons">vpn_lock</i>
                                        </a>
                                    </div>
                                    <div class="socialShareItem">
                                        <span data-tip="Please connect these social accounts in settings." data-for="notConnectedSocialChannels"
                                            currentitem="false">

                                            <a class="fb linkNotAvailable">
                                                <i class="fa fa-facebook notConnected"></i>
                                            </a>
                                            <div class="__react_component_tooltip place-top type-dark " id="notConnectedSocialChannels"
                                                data-id="tooltip"></div>
                                        </span>
                                    </div>
                                    <div class="socialShareItem">
                                        <span data-tip="Please connect these social accounts in settings." data-for="notConnectedSocialChannels"
                                            currentitem="false">

                                            <a class="tw linkNotAvailable">
                                                <i class="fa fa-twitter notConnected"></i>
                                            </a>
                                            <div class="__react_component_tooltip place-top type-dark " id="notConnectedSocialChannels"
                                                data-id="tooltip"></div>
                                        </span>
                                    </div>
                                    <div class="socialShareItem">
                                        <span data-tip="Link is not available." data-for="notConnectedSocialChannels" currentitem="false">

                                            <a class="li linkNotAvailable">
                                                <i class="fa fa-linkedin"></i>
                                            </a>
                                            <div class="__react_component_tooltip place-top type-dark " id="notConnectedSocialChannels"
                                                data-id="tooltip"></div>
                                        </span>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="feed-job-item col-one-of-four">
                    <div class="feed-job-item-inner">
                        <div>
                            <div class="job-date">
                                <p>July 2018</p>
                            </div>
                            <div class="job-title">
                                <p>Test job</p>
                            </div>
                            <div class="job-location">
                                <i class="material-icons">location_on</i>
                                <p>
                                    Camasnacroise

                                </p>
                            </div>
                            <div class="job-descripation">
                                <div id="jobFullDesc" class="jobFullDesc"></div>
                                <p>
                                   Just trying to see if the job is now getting posted in Visibly or not. This job should be
                                    posted on Visibly.


                                </p>
                            </div>
                        </div>
                        <div class="feed-job-share-block feed-job-share-block-0 ">
                            <div class="feed-job-share-block-title">
                                <span>Share</span>
                            </div>
                            <div>
                                <div class="socialIcons clearfix">
                                    <div class="socialShareItem">
                                        <a class="internal-icon" title="Internal channel">
                                            <i class="material-icons">vpn_lock</i>
                                        </a>
                                    </div>
                                    <div class="socialShareItem">
                                        <span data-tip="Please connect these social accounts in settings." data-for="notConnectedSocialChannels"
                                            currentitem="false">

                                            <a class="fb linkNotAvailable">
                                                <i class="fa fa-facebook notConnected"></i>
                                            </a>
                                            <div class="__react_component_tooltip place-top type-dark " id="notConnectedSocialChannels"
                                                data-id="tooltip"></div>
                                        </span>
                                    </div>
                                    <div class="socialShareItem">
                                        <span data-tip="Please connect these social accounts in settings." data-for="notConnectedSocialChannels"
                                            currentitem="false">

                                            <a class="tw linkNotAvailable">
                                                <i class="fa fa-twitter notConnected"></i>
                                            </a>
                                            <div class="__react_component_tooltip place-top type-dark " id="notConnectedSocialChannels"
                                                data-id="tooltip"></div>
                                        </span>
                                    </div>
                                    <div class="socialShareItem">
                                        <a class="linkedin-link rss-share-link li">
                                            <i class="fa fa-linkedin" aria-hidden="true"></i>
                                        </a>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="feed-job-item col-one-of-four">
                    <div class="feed-job-item-inner">
                        <div>
                            <div class="job-date">
                                <p>December 2017</p>
                            </div>
                            <div class="job-title">
                                <p>job one</p>
                            </div>
                            <div class="job-location">
                                <i class="material-icons">location_on</i>
                                <p>
                                  ahmedabad

                                </p>
                            </div>
                            <div class="job-descripation">
                                <div id="jobFullDesc" class="jobFullDesc"></div>
                                <p>
                                    urget required data

                                </p>
                            </div>
                        </div>
                        <div class="feed-job-share-block feed-job-share-block-0 ">
                            <div class="feed-job-share-block-title">
                                <span>Share</span>
                            </div>
                            <div>
                                <div class="socialIcons clearfix">
                                    <div class="socialShareItem">
                                        <a class="internal-icon" title="Internal channel">
                                            <i class="material-icons">vpn_lock</i>
                                        </a>
                                    </div>
                                    <div class="socialShareItem">
                                        <span data-tip="Please connect these social accounts in settings." data-for="notConnectedSocialChannels"
                                            currentitem="false">

                                            <a class="fb linkNotAvailable">
                                                <i class="fa fa-facebook notConnected"></i>
                                            </a>
                                            <div class="__react_component_tooltip place-top type-dark " id="notConnectedSocialChannels"
                                                data-id="tooltip"></div>
                                        </span>
                                    </div>
                                    <div class="socialShareItem">
                                        <span data-tip="Please connect these social accounts in settings." data-for="notConnectedSocialChannels"
                                            currentitem="false">

                                            <a class="tw linkNotAvailable">
                                                <i class="fa fa-twitter notConnected"></i>
                                            </a>
                                            <div class="__react_component_tooltip place-top type-dark " id="notConnectedSocialChannels"
                                                data-id="tooltip"></div>
                                        </span>
                                    </div>
                                    <div class="socialShareItem">
                                        <span data-tip="Link is not available." data-for="notConnectedSocialChannels" currentitem="false">

                                            <a class="li linkNotAvailable">
                                                <i class="fa fa-linkedin"></i>
                                            </a>
                                            <div class="__react_component_tooltip place-top type-dark " id="notConnectedSocialChannels"
                                                data-id="tooltip"></div>
                                        </span>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    )
}
	render() {
        var newLocation = utils
        .removeTrailingSlash(this.props.props.location.pathname)
        .split('/')
		return (
            <div>
                {newLocation[1]!==undefined && newLocation[1]=='feed'?
                 newLocation[2]=='jobs' ?
                    this.fetchJobsStaticData()
                :newLocation[2]=='rss'?
                    this.fetchRssStaticData():
                    this.fetchFeedStaticData()
                // :newLocation[1]=='campaigns'?
                //     this.fetchCampaignStaticData()
                :newLocation[1]=='leaderboard'?
                    this.fetchLeaderboardStaticData()
                :newLocation[1]=='analytics'?
                    newLocation[2] ==undefined || newLocation[2]=='feed'?
                        this.fetchAnalyticsStaticData()
                    :newLocation[2]=='asset'?
                        this.fetchAnalyticsAssetStaticData():''
                    // :newLocation[2]=='campaign'?
                    //     this.fetchAnalyticsCampaignStaticData():''
                :newLocation[1]=='moderation'?
                    this.fetchModerationStaticData()
                :console.log('')
                }
            </div>
		)
	}
}