import {Component} from 'react';
import {connect} from 'react-redux';
import {browserHistory} from 'react-router';
import Four0Four from "../Four0Four";
let roles='';
export default function (ComposedComponent, allowedRoles) {
  class Authentication extends Component {
  
    componentWillMount() {
      if (!this.props.auth.authenticated) {
        browserHistory.push("/");
      }
    }
   componentWillUpdate(nextProps) {
      if (!nextProps.auth.authenticated) {
        browserHistory.push("/");
      }
    }
    render() {
      // console.log(JSON.parse(localStorage.getItem('roles')),'authentication 11')
      if(JSON.parse(localStorage.getItem('roles')) == 0 || JSON.parse(localStorage.getItem('roles')) == 'undefined' || JSON.parse(localStorage.getItem('roles')) == ' ' || JSON.parse(localStorage.getItem('roles')) == null ){
        // console.log('authentication if part to reload')
        window.location.reload();
      }
      // console.log(JSON.parse(localStorage.getItem('roles')),'authentication')
      if(JSON.parse(localStorage.getItem('roles')) !== null){ 
          roles =JSON.parse(localStorage.getItem('roles')) !== null? JSON.parse(localStorage.getItem('roles'))[0].role_name:''
      }
      if (allowedRoles.includes(roles)) {
        return <ComposedComponent {...this.props}/>
      } else {
        return <Four0Four/>
      }
    }
  }

  function mapStateToProps(state) {
    return {auth: state.auth}
  }

  return connect(mapStateToProps)(Authentication);
}
