import SeeMore from './SeeMore';
class DataRow extends React.Component {
    render() {
        let dataRowClass = `dataRow ${this.props.valign}`;
        return (<div className={dataRowClass} key={`dataRow_${this.props.label}`}>
        <div className='label'>{this.props.label}:</div>
        {
        this.props.campaignDescription=="true"?
        <div className="value">
            <SeeMore
            className='campaign'
            detail = {this.props.value}
            postid={this.props.campaignId}
            > 
                <div>
                    {this.props.value}
                </div> 
            </SeeMore> 
        </div>
        :
        <div className={`value ${this.props.label == 'Title' ? 'tableOneLine' :''}`} >{this.props.value}</div>
        } 
       
      </div>

        )
    }
}
module.exports = DataRow