export default class FilterTableColumns extends React.Component {
	render() {
		return (
       <div className='table-menu dropudown-container'>
             <div className='checkbok-menu-button'>
               <ul class='dd-menu'>
                 <li class='button-dropdown'>
                   <a class='dropdown-toggle btn btn-theme'>Filter columns</a>
                   {this.props.children}
                 </li>
               </ul>
             </div>
       </div>
       
		)
	}
}