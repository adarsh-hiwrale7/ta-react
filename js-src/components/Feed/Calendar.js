import { browserHistory } from 'react-router'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Link } from 'react-router'

import * as assetsActions from '../../actions/assets/assetsActions'
import * as departmentActions from '../../actions/departmentActions'
import * as feedActions from '../../actions/feed/feedActions'
import * as notificationActions from "../../actions/notificationActions"
import * as postsActions from '../../actions/feed/postsActions'
import * as socialAccountsActions from '../../actions/socialAccountsActions'
import * as utils from '../../utils/utils'
import * as userActions from '../../actions/userActions'

import $ from '../Chunks/ChunkJquery'
import Asset from '../Assets/Asset'
import AssetDetail from '../Assets/AssetDetail'
import bigCalendar from '../Chunks/ChunkBigCalendar'
import CalendarButtons from '../Header/CalendarButtons'
import CreatePost from './CreatePost'
import Collapse, { Panel } from 'rc-collapse'
import ChunkJqueryDropdown from '../Chunks/ChunkJqueryDropdown'
import Dnd from './Dnd'
import FeedEvents from './FeedEvents'
import FeedNav from './FeedNav';
import FilterWrapper from '../FilterWrapper';
import Globals from '../../Globals'
import Header from '../Header/Header';
import moment from '../Chunks/ChunkMoment'
import notie from 'notie/dist/notie.js'
import Post from './Post'
import PreLoader from '../PreLoader'
import ReactCSSTransitionGroup from 'react/lib/ReactCSSTransitionGroup'
import 'rc-collapse/assets/index.css'
import ReactDOM from 'react-dom';
import SliderPopup from '../SliderPopup'
import TagPopup from './TagPopup'




// var wizard = require('react-guest-tutorial').Mixin
var _current_page;
var calendarUrl = '';
var feedUrl = '';
var calnder_filter = ['facebook', 'twitter', 'linkedin'];
var rescheduled_post_id;
var rescheduled_post_date;
var new_events = [];
var prevOrNext = null;
var restrictAPI=false;
class Feed extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      currFeed: 'internal',
      currFeedPosts: 'live',
      currPage: 1,
      postCreationCampaignID: null,
      postCreationCampaignTitle: null,
      newPostPopup: false,
      postCreationCampaignChannels: [],
      moment: null,
      currDateTitle: null,
      limit: 10,
      scrollTimer: null,
      jQuery: null,
      tagType: 'user',
      tagTypeClass: '',
      assetDetailPopup: false,
      currentAssetDetail: null,
      overlay_flag: false,
      show_menu: true,
      currentFilter: 'external/live',
      show_calendar_feed_post_state: false,
      calendar_feed_detail: null,
      isAllSocial: true,
      isFacebook: true,
      isTwitter: true,
      isLinkedin: true,
      calendar_loader: false,
      update_post_loader: false,
      dayChosen: new Date(),
      cal_view: 'month'
    }
    this.changeSocialFilter = this.changeSocialFilter.bind(this);
    this._handleCloseEvent = this._handleCloseEvent.bind(this);
    this.move_event = this.move_event.bind(this);
  }


  componentDidMount() {
    let me = this
    var path = utils
      .removeTrailingSlash(this.props.location.pathname)
      .split('/')
    var location = path[4] !== undefined
      ? path[4]
      : path[4]
    this.openingPostCreationFromCampaign()
    $().then(jquery => {
      var $ = jquery.$
      me.setState({ jQuery: $ })
      ChunkJqueryDropdown().then(jqueryDropdown => { })
    })
    ReactDOM
      .findDOMNode(this)
      .addEventListener('click', this._handleCloseEvent);
        this.props.socialAccountsActions.fetchSocialAccounts();
  }
  /**
   * to close popup when click outside 
   * @param {*} e 
   */
  _handleCloseEvent(e) {
    if (document.getElementById('pop-up-tooltip-wrapper')) {
      //hide notification popup on outside click
      if (!document.getElementById('pop-up-tooltip-wrapper').contains(e.target)) {
        if (this.state.overlay_flag) {
          document
            .getElementById('pop-up-tooltip-wrapper')
            .className = 'hide';
          this.setState({ overlay_flag: false })
        }
      }
    }
    if (utils.closest(e.target, 'feed-post-item') == null) {
      //hide calender post popup on outside click
      if (document.querySelectorAll('.bigCalendar-calendarview #close-popup').length != 0) {
        document.querySelectorAll('.bigCalendar-calendarview #close-popup')[0].click()
      }
    }
  }

  closepopup() {
    let popup_wrapper = document.getElementById("pop-up-tooltip-wrapper");
    popup_wrapper.className += ' hide';
  }

  updateText(id, type, e) {
    this.setState({ overlay_flag: true })
    if (type === 'department') {
      this
        .props
        .departmentActions
        .fetchDepartmentById(id);
    } else {
      this
        .props
        .userActions
        .fetchUserDetail(id);
    }
    const domNode = ReactDOM.findDOMNode(e.target);
    let coords = domNode.getBoundingClientRect();
    let coords_top = coords.top + pageYOffset;
    let popup_wrapper = document.getElementById("pop-up-tooltip-wrapper");
    let coords_left = 0,
      coords_area = 0,
      new_class_comb = '';
    let pop_holder = document.getElementById("pop-up-tooltip-holder");
    let popupparent = document.getElementById('department-popup');
    if (popupparent != null) {
      pop_holder.className = 'departmentpopup';
    } else {
      pop_holder.className = '';
    }
    if (screen.width >= 768) {
      coords_left = coords.left + pageXOffset + (coords.width / 2);
    } else {
      coords_left = coords.left + pageXOffset;
      coords_area = coords_left + 472;
      if (coords_area < screen.width) {
        new_class_comb = 'popup-left';
      } else {
        new_class_comb = 'popup-right';
      }

    }
    if (e.nativeEvent.screenY < 400) {
      new_class_comb += ' bottom';
      if (e.target.className == "thumbnail") {
        new_class_comb += ' thumbnai_popup';
      }
    } else {
      new_class_comb += ' top';
    }

    popup_wrapper.className = new_class_comb;
    this.setState({
      popupTop: coords_top,
      popupLeft: coords_left,
      popupdisplay: 'block',
      tagType: type,
      tagTypeClass: (type === "department"
        ? 'departmentpopup'
        : '')
    });
    // let pop_overlay = document.getElementById("tag-popup-overlay");
    // pop_overlay.className = 'show';

  }

  openingPostCreationFromCampaign() {
    if (typeof this.props.location.query.createpost !== 'undefined') {
      this.handlePostCreation()
      this.setState({ 
        postCreationCampaignID: this.props.location.query.campaignid, 
        postCreationCampaignTitle: this.props.location.query.campaigntitle, 
        postCreationCampaignChannels: this.props.location.query['social[]'] 
      })
    } else {
      this.setState({ 
        newPostPopup: false, 
        postCreationCampaignID: null, 
        postCreationCampaignTitle: null, 
        postCreationCampaignChannels: [] 
      })
    }
  }



  componentWillMount() {
    let me = this
    me.openingPostCreationFromCampaign()

    var path = utils
      .removeTrailingSlash(this.props.location.pathname)
      .split('/')

    this
      .props
      .feedActions
      .resetPosts()

    if (typeof path[2] === 'undefined' || path[2] == '' || typeof path[3] === 'undefined' || path[3] == '') {
      browserHistory.push('/feed/external/live')
    } else {
      this.setState({ currFeed: path[2], currFeedPosts: path[3], currPage: 1 })
      this.fetchPosts(path[2], path[4], 1)
    }
    calendarUrl = `/feed/${path[2]}/calendar/${path[4]}`
    feedUrl = `/feed/${path[2]}/${path[4]}`



  }

  componentWillUnmount() {

    ReactDOM
      .findDOMNode(this)
      .removeEventListener('click', this._handleCloseEvent);
  }

  fetchPosts(type, postTypes, currPage) {
    _current_page = currPage;
    moment().then(moment => {
      bigCalendar().then(bigCalendar => {
        if (this.refs.big_calendar) {
          this.setState({
            moment: moment,
            bigCalendar: bigCalendar
          })
        }
        switch (postTypes) {
          case 'scheduled':
            switch (type) {
              case 'internal':
                this.props.feedActions.fetchInternalScheduled(
                  _current_page, this.state.limit, null,
                  true,
                  this.state.moment
                )
                break
              case 'external':
                this.props.feedActions.fetchExternalScheduled(
                  _current_page, this.state.limit, null,
                  true,
                  this.state.moment,
                  calnder_filter
                )
                break
            }
            break
        }
      })
    })

  }
  componentWillReceiveProps(newProps) {
    var path = utils
      .removeTrailingSlash(newProps.location.pathname)
      .split('/')
    var me = this

    me.setState({ e: newProps.e })

    if (newProps.location.pathname !== this.props.location.pathname) {
      me.setState({
        currFeed: path[2], currFeedPosts: path[3], currPage: 1 // user came for the first time, so reset currPage to 1
      })
      this
        .props
        .feedActions
        .resetPosts()
      this.fetchPosts(path[2], path[4], 1)
      this.renderPosts()
      calendarUrl = `/feed/${path[2]}/calendar/${path[4]}`
      feedUrl = `/feed/${path[2]}/${path[4]}`
    }
    if (this.props.disblePopup !== newProps.disblePopup && newProps.disblePopup) {
      this.closePostCreatePopup();
    }
    this.hide_add_new_popup();
    this.initilizeClassToCell();
    if(newProps.posts.calendar_success==true )
    {
      this.fetchPosts(path[2], path[4], 1)
      this.props.postsActions.resetCalendarSuccessFlag()
    }
  }
  scheduledPosts({ posts, postsByType, feed, feedType }) {
    
    let me = this
    return (
      <div id='post-list-wrapper'>
        {posts.map((post, index) => {
          if (me.state.moment != null && typeof me.state.moment.tz !== 'undefined') {
            let post_date = me
              .state
              .moment
              .tz(post.scheduled_at * 1000, Globals.SERVER_TZ)
              .toNow()
            let prev_date = 0
            if (index > 0) {
              // get previous date so avoiding repetition of date header/title
              prev_date = me
                .state
                .moment
                .tz(postsByType.scheduled[index - 1].scheduled_at * 1000, Globals.SERVER_TZ)
                .toNow()
            }
            return (
              <div key={index}>
                {post_date !== prev_date
                  ? <h3 class='title-post-date'>
                    {me
                      .state
                      .moment
                      .tz(post.scheduled_at * 1000, Globals.SERVER_TZ)
                      .format('dddd, Do MMMM  YYYY')}
                  </h3>
                  : null}
                <Post
                  key={index}
                  index={index}
                  feed={feed}
                  feedType={feedType}
                  locationInfo={me.props.location}
                  details={post}
                  tooltipid={`one` + index}
                  handleCloseEvent={this
                    ._handleCloseEvent
                    .bind()}
                  updateText={this
                    .updateText
                    .bind(this)} />

              </div>
            )
          } else {
            return <div />
          }
        })}

      </div>
    )
  }

  livePosts(posts, feedType, feed) {
    let me = this
    return (
      <div id='post-list-wrapper'>
        {posts.map((post, index) => {
          return (<Post
            feed={feed}
            feedType={feedType}
            locationInfo={me.props.location}
            details={post}
            key={index}
            index={index}
            handleCloseEvent={this
              ._handleCloseEvent
              .bind()}
            updateText={this
              .updateText
              .bind(this)} />)
        })}

      </div>
    )
  }

  internalFeedPosts({ posts, feedType, feed }) {
    let me = this
    return (
      <div>
        <div className='content'>
          {/* {me.props.feed.internal.loading
                ? me.renderLoading()
                : null} */}

          {me.props.feed.internal.loading == true && me.props.feed.internal.nopost == false
            ? this.scheduledPosts({ posts: posts.internal.scheduled, postsByType: posts.internal, feedType, feed })
            : ''}

          {/* {me.props.feed.internal.loadmore == true
                ? me.renderLoadingPage()
                : ''} */}
        </div>
      </div>
    )
  }

  externalFeedPosts({ posts, feedType, feed }) {
    let me = this
    return (
      <div className='external-live esc-sm'>
        <div className='content'>
          {/* {me.props.feed.external.loading
            ? me.renderLoading()
            : null} */}
          {me.props.feed.external.loading == true && me.props.feed.external.nopost == false
            ? this.livePosts(posts.external.live, feedType, feed)
            : ''}
          {/* {me.props.feed.external.loadmore == true
            ? me.renderLoadingPage()
            : ''} */}
        </div>
      </div>
    )
  }

  hide_add_new_popup() {
    var add_new_popup = document.getElementById('add-new-popup');
    add_new_popup.className = 'add-new-popup';
  }
  renderPosts() {
    const posts = this.props.feed
    let feedType = this.state.currFeedPosts
    let feed = this.state.currFeed
    let hashAssets = this.props.hashAssets;
    var me = this

    switch (this.state.currFeed) {
      case 'internal':
        return (
          <div className='internal'>
            {me.internalFeedPosts({ posts, feedType, feed })}
          </div>
        )

      case 'external':
        return (
          <div className='external'>
            {me.externalFeedPosts({ posts, feedType, feed })}
          </div>
        )
      default:
        return (
          <div className='external'>
            {me.externalFeedPosts({ posts, feedType, feed })}
          </div>
        )
    }
  }

  handlePostCreation() {
    this.setState({ newPostPopup: true })

    // asset created flag - change in redux to false
    this
      .props
      .postsActions
      .creatingNewPost()

    document
      .body
      .classList
      .add('overlay')
  }

  closePostCreatePopup() {
    this.setState({ newPostPopup: false })

    document
      .body
      .classList
      .remove('overlay')

    if (this.state.postCreationCampaignID !== null) {
      browserHistory.push('/feed/external/live')
    }
  }

  renderPostCreation() {
    return (
      <SliderPopup wide>
        <button
          id='close-popup'
          className='btn-default'
          onClick={this
            .closePostCreatePopup
            .bind(this)}>
          <i className='material-icons'>clear</i>
        </button>
        <CreatePost
          campaignId={this.state.postCreationCampaignID}
          campaignTitle={this.state.postCreationCampaignTitle}
          campaignChannels={this.state.postCreationCampaignChannels}
          location={this.props.location}
          jQuery={this.state.jQuery} />

      </SliderPopup>
    )
  }
  closepostDetailPopup() {
    this.setState({
      show_calendar_feed_post_state: false
    })
  }
  formatTime(time) {
    var myNumber = time;
    return (("0" + myNumber).slice(-2));
  }
  callCalPostDetails(event, e) {
    var week = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']
    var calendar_popup = {
      position: {
        vertical: 'top',
        horizontal: 'left',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0
      },
      grid:
        {
          row_number: 0,
          column_number: 0
        },
      event
    }
    var rbc_month_row = document.querySelectorAll('.rbc-month-row')[0];
    var rbc_day_bg = document.querySelectorAll('.rbc-day-bg')[0];
    var selected_row = utils.closest(e.target, 'rbc-month-row');
    var selected_row_cells;
    if (selected_row) {
      calendar_popup.grid.row_number = utils.index(selected_row)
      selected_row_cells = selected_row.querySelectorAll('.rbc-date-cell');
    }
    var selected_cell = document.querySelectorAll('.selected_cell')[0];


    if (this.state.cal_view == 'month') {
      var calendar_table = {
        border: 2,
        margin_bottom: parseInt(window.getComputedStyle(rbc_month_row)['margin-bottom']),
      }
      var cell = {
        height: rbc_day_bg.offsetHeight + calendar_table.border + calendar_table.margin_bottom,
        width: rbc_day_bg.getBoundingClientRect().width
      }

      var selected_date_var = new Date(event.start);
      var selected_date = {
        date: selected_date_var.getDate(),
        month: selected_date_var.getMonth() + 1
      }

      if (utils.closest(e.target, 'rbc-overlay')) {
        calendar_popup.grid.column_number = parseInt(selected_cell.getAttribute('data-column-number'));
        calendar_popup.grid.row_number = parseInt(selected_cell.getAttribute('data-row-number')) + 1;
      }
      else {
        Array.prototype.forEach.call(selected_row_cells, function (cell_el, cell_index) {
          if (cell_el.textContent == selected_date.date) {
            calendar_popup.grid.column_number = cell_index;
          }
        });
      }
      calendar_popup.position.left = (calendar_popup.grid.column_number + 1) * cell.width;
      calendar_popup.position.top = (calendar_popup.grid.row_number) * cell.height;
      if (calendar_popup.grid.column_number > 3) {
        calendar_popup.position.horizontal = 'right';
        calendar_popup.position.right = cell.width * (7 - calendar_popup.grid.column_number)
      }
      if (calendar_popup.grid.row_number > 3) {
        calendar_popup.position.vertical = 'bottom';
        calendar_popup.position.bottom = cell.height * (5 - calendar_popup.grid.row_number);
      }
    }
    // Week view 
    else {
      var calendar_table = {
        border: 2,
        timeslot_width: document.querySelectorAll('.rbc-time-column')[0].offsetWidth,
        rbc_header: document.querySelectorAll('.rbc-header')[0].offsetHeight
      }
      var cell = {
        height: document.querySelectorAll('.rbc-timeslot-group')[0].offsetHeight,
        width: document.querySelectorAll('.rbc-header')[0].getBoundingClientRect().width
      }

      selected_date = {
        date: 1,
        time: null,
        full_date: new Date(event.start)
      }

      var selected_day = week[selected_date.full_date.getDay()]
      var formated_date = this.formatTime(selected_date.full_date.getDate());
      var formated_month = this.formatTime(selected_date.full_date.getMonth() + 1);

      selected_date.date = `${selected_day} ${formated_date}/${formated_month}`;
      var hours = selected_date.full_date.getHours();
      var minutes = 0;
      var formated_minutes = this.formatTime(minutes);
      var meridiem = 'PM';
      if (hours == 0) {
        hours = 12;
        meridiem = 'AM';
      }
      else if (hours < 12) {
        meridiem = 'AM';
      }
      else if (hours > 12) {
        hours = hours - 12;
      }
      selected_date.time = hours + ':' + formated_minutes + ' ' + meridiem;


      var rbc_timeslot_group = document.querySelectorAll('.rbc-timeslot-group');
      var rbc_header = document.querySelectorAll('.rbc-header');

      Array.prototype.forEach.call(rbc_timeslot_group, function (timeslot_el, timeslot_index) {
        if (timeslot_el.textContent == selected_date.time) {
          calendar_popup.grid.row_number = timeslot_index;
        }
      });

      if (calendar_popup.grid.row_number > 15) {
        calendar_popup.position.vertical = 'bottom';
        calendar_popup.position.bottom = cell.height * (23 - calendar_popup.grid.row_number);
      }
      Array.prototype.forEach.call(rbc_header, function (rbc_header_el, rbc_header_index) {
        if (rbc_header_el.textContent == selected_date.date) {
          calendar_popup.grid.column_number = rbc_header_index + 1;
        }
      })
      if (calendar_popup.grid.column_number > 4) {
        calendar_popup.position.horizontal = 'right';
        calendar_popup.position.right = cell.width * (8 - calendar_popup.grid.column_number)
      }
      calendar_popup.position.left = (calendar_popup.grid.column_number) * cell.width + calendar_table.timeslot_width;
      calendar_popup.position.top = (calendar_popup.grid.row_number) * cell.height + calendar_table.rbc_header;
    }
    this.setState ({
      show_calendar_feed_post_state: true,
      calendar_feed_detail: {
        calendar_popup
      }
    });
  }
  show_calendar_feed_post(data) {
    var data = this.state.calendar_feed_detail.calendar_popup
    var row_number = data.grid.row_number;
    let style =
      {
        left: data.position.left,
        right: data.position.right,
        top: data.position.top,
        bottom: data.position.bottom
      }
    return (
      <div className={`calendar-detail-popup popup_row_${row_number} popup_row_${data.position.vertical} popup_column_${data.position.horizontal}`} style={style}>
        <button
          id='close-popup'
          className='btn-default'
          onClick={this
            .closepostDetailPopup
            .bind(this)}>
          <i className='material-icons'>clear</i>
        </button>
        <Post
          feed={this.state.currFeed}
          feedType={this.state.currFeedPosts}
          locationInfo={this.props.location}
          details={data.event.feed_post}
          customScrollbar={true}
          handleCloseEvent={this
            ._handleCloseEvent
            .bind()}
          updateText={this
            .updateText
            .bind(this)}
        />
      </div>
    )
  }

  changeSocialFilter(paraValue,isAllSocial,isFacebook,isTwitter,isLinkedin) {
    this.props.feedActions.initiateFetchExternalScheduled();
    const value = paraValue;
    this.setState({
      [name]: value,
      calendar_loader: true,
      isAllSocial: false,
          isFacebook,
          isTwitter,
          isLinkedin
    });
    if (isAllSocial) {
      calnder_filter = ['facebook', 'twitter', 'linkedin'];
    }
    else {
      calnder_filter = [];
      if (isFacebook) {
        calnder_filter.push('facebook');
      }
      if (isTwitter) {
        calnder_filter.push('twitter');
      }
      if (isLinkedin) {
        calnder_filter.push('linkedin');
      }

    }
    var path = utils
      .removeTrailingSlash(this.props.location.pathname)
      .split('/')
    this.fetchPosts(path[2], path[4], 1)
  }
  move_event({ event, start, end }) {
    //if calendar view is month assign current time of event to new schedule post when it will be dragged
    if(this.state.cal_view == 'month'){
      
        var oldstartDate = new Date (event.start);
        var oldStartHours = oldstartDate.getHours();
        var oldStartMinutes = oldstartDate.getMinutes();
        var oldStartSeconds = oldstartDate.getSeconds();

        var newstartDate = new Date (start);
        var updateStartDate = newstartDate.setHours(oldStartHours);
        updateStartDate = newstartDate.setMinutes(oldStartMinutes);
        updateStartDate = newstartDate.setSeconds(oldStartSeconds);
        updateStartDate = new Date(newstartDate);
        start = updateStartDate;
        end = updateStartDate;
    }
   
    var path = utils
      .removeTrailingSlash(this.props.location.pathname)
      .split('/')
    var time_message;
    const now = new Date();
    var moment = this.state.moment;

    let tomorrow, newSchedule;

    tomorrow = moment(start).add(0, 'days');
    newSchedule = String(start.getTime() / 1000);
    rescheduled_post_date = start;
    var current_date = moment.tz(now, Globals.SERVER_TZ).format('X');
    let selected_date = moment.tz(start, Globals.SERVER_TZ).format('X');
    let selected_date_second = moment.tz(start, Globals.SERVER_TZ).format('x');
    var current_date_second = moment.tz(now, Globals.SERVER_TZ).format('x');

    if (newSchedule >= current_date) {
      if (selected_date_second > current_date_second) {
        const events_post = this.props.feed.feedEvents;
        rescheduled_post_id = event.feed_post.post.data.identity;
        const idx = events_post.indexOf(event);
        const updatedEvent = { ...event, start, end };
        const nextEvents = [...events_post];
        nextEvents.splice(idx, 1, updatedEvent);
        this.setState({
          events_post: nextEvents,
          event_change: true
        })
        this.props
          .postsActions
          .postUpdateInitiate();
        if (this.props.posts.updation.loading) {
          this.setState({
            update_post_loader: true
          })
        }
        else {
          this.setState({
            update_post_loader: false
          })
        }
        var field_values_array = [];
        if (this.state.currFeed == 'external') {

          event.feed_post.SocialAccount.data.forEach(function (event_post) {
            var field_array_object = {
              social_account_identity: event_post.SocialAccount.data.identity,
              scheduled_at: newSchedule
            }
            field_values_array.push(field_array_object)
          })

        }
        else {
          field_values_array = [{
            social_account_identity: event.feed_post.post.data.identity,
            scheduled_at: newSchedule
          }]
        }

        let fieldValues =
          {

            post_identity: event.feed_post.post.data.identity,
            schedule: field_values_array
          }
        this.props.postsActions.postUpdate(fieldValues)
        if(document.querySelectorAll('.initializeStyle').length>0){
          document.querySelectorAll('.initializeStyle')[0].click();
        }
      }
      else {
        if (this.state.cal_view == 'month') {
          time_message = 'You cannot schedule a post for time that has passed. Please go to week view for more.'
        }
        else {
          time_message = 'You cannot schedule a post for time that has passed.'
        }
        notie.alert(
          'error',
          time_message,
          5
        )
      }
    }
    else {
      notie.alert(
        'error',
        'You cannot schedule a post for date that has passed.',
        5
      )
    }
  }
  callCreateEvent(event) {
  }
  initilizeClassToCell() {
    var month_rows = document.querySelectorAll('.rbc-month-row');
    Array.prototype.forEach.call(month_rows, function (month_el, month_index) {
      var date_cols = month_el.querySelectorAll('.rbc-day-bg');
      Array.prototype.forEach.call(date_cols, function (date_el, date_index) {
        date_el.setAttribute('data-row-number', month_index);
        date_el.setAttribute('data-column-number', date_index);
      });
    });
  }
  render() {
    let hastag_name = '';
    let user = this.props.users;
    let userDetails = user.userDetails;
    let department = this.props.department;
    let isFetching = user.isFetching;
    let feed_asset_class;
    let currentFilter = this.state.currentFilter == '/feed/external/live'
      ? 'external/live'
      : this.state.currentFilter;
    if (this.props.location.pathname == '/feed/assets/hash') {
      feed_asset_class = 'feed-asset-page';
    }
    else {
      feed_asset_class = '';
    }
    hastag_name = this.props.location.hash;
    let popupStyle = {
      backgroundSize: 'cover',
      backgroundRepeat: 'no-repeat'
    }
    var path = utils
      .removeTrailingSlash(this.props.location.pathname)
      .split('/')

    let no_post_class;
    if (this.props.feed.external.nopost || this.props.feed.internal.nopost) {
      no_post_class = "no-post-wrapper";
    }
    else {
      no_post_class = "";
    }

    var BigCalendar = this.state.bigCalendar
    var moment = this.state.moment
    var me = this;

    let { listLoading } = this.props.feed.internal.scheduled;

    let events = typeof this.props.feed.feedEvents !== 'undefined'
      ? this.props.feed.feedEvents
      : []
    events.forEach(function (event_item) {
      if (event_item.feed_post.post.data.identity == rescheduled_post_id) {
        event_item.start = rescheduled_post_date
        event_item.end = rescheduled_post_date

      }
      if (event_item.start.getHours() == 0) {
        if (event_item.start.getMinutes() == 0) {
          var default_time = '00:00:01 GMT+0530 (IST)';
          var date_only = me.state.moment(event_item.start).format('ddd MMM DD YYYY');
          var new_date = new Date(`${date_only} ${default_time}`);
          event_item.start = new_date;
          event_item.end = new_date;
        }

      }
    })
    if (moment !== null) {
      BigCalendar.setLocalizer(BigCalendar.momentLocalizer(moment))
    }
  return (

      <section id='calendar' className='calendar' ref="calendarRef">
        {this.props.posts.updation.loading ?  <PreLoader className='Page' /> : ''}
        <div id="pop-up-tooltip-holder">
          <div id="pop-up-tooltip-wrapper" className={this.state.tagTypeClass}>
            <div
              className="pop-up-tooltip"
              style={{
                top: this.state.popupTop,
                left: this.state.popupLeft,
                display: this.state.popupdisplay
              }}>
              <span className='popup-arrow'></span>
              <TagPopup
                userDetails={userDetails}
                isFetching={isFetching}
                isDept={this.state.tagType}
                department={department} />

            </div>
          </div>
        </div>

        {this.state.newPostPopup
          ? this.renderPostCreation()
          : <div />}
        <Header
          add_new_subpopup="FEED"
          title='FEED'
          nav={FeedNav}
          popup_text="Click here to add new post."
          add_new_button={true}
          currFeed={this.state.currFeed}
          currFeedPosts={this.state.currFeedPosts}
          hash={this.props.location.hash}
          location={this.props.location}
          postSelectedData={this.props.notificationData.postSelectedData}
          hastag_name={hastag_name}
          Header_onclick={this.hide_add_new_popup.bind(this)}
          page = "feedCalendar"
          changeView = {true}
          feedUrl = {feedUrl}
          calendarUrl = {calendarUrl}
          changeSocialFilter = {this.changeSocialFilter}
        />
        {(this.props.feed.external.loading || this.props.feed.internal.loading) ? <PreLoader className='Page' /> : <div />}
              <FilterWrapper className="calendar-dropdown">
                <div className="clearfix">
              <CalendarButtons 
                changeHeaderView={true} 
                page="feedCalendar"
                feedUrl ={feedUrl} 
                calendarUrl = {calendarUrl} 
                currFeedPosts = {this.state.currFeedPosts}
                currFeed = {this.state.currFeed}
                changeSocialFilter ={this.changeSocialFilter}
              />
              </div>
              </FilterWrapper>
        <div className='main-container'>
          <span
            id="add_new_button"
            onClick={this
              .handlePostCreation
              .bind(this)} />
          <div id='content'>
          
            <div className='page feed-container page with-filters feed-calendar-page'>
              <div class='content'>
                <div className='widget'>
                  <div className={`bigCalendar-calendarview ${this.state.cal_view}-view calendar-${this.state.currFeed}`} ref='big_calendar'>
                    {this.state.show_calendar_feed_post_state ? this.show_calendar_feed_post() : ''}
                    {moment
                      ?
                      <Dnd
                        {...this.props}
                        selectable
                        popup={true}
                        onEventDrop={this.move_event}
                        date={this.state.dayChosen}
                        onNavigate={(focusDate, flipUnit) => {
                          const _this = this;
                          const now = new Date();
                          var selected_date = moment(moment.tz(focusDate, Globals.SERVER_TZ).format('YYYY MM DD')).format('X');
                          var current_date = moment(moment.tz(now, Globals.SERVER_TZ).format('YYYY MM DD')).format('X');
                          if (selected_date < current_date) {
                            _this.setState({
                              dayChosen: now
                            });
                          }
                          else {
                            _this.setState({
                              dayChosen: focusDate
                            })
                          }
                        }}
                        events={
                          typeof this.state.moment !== 'undefined' &&
                            this.state.moment != null
                            ? events
                            : []
                        }
                        defaultDate={new Date()}
                        onSelectSlot={event =>
                          this.callCreateEvent(
                            event,
                            me.state.clickListenerInfo
                          )
                        }
                        onSelectEvent={(event, e) =>
                          this.callCalPostDetails(
                            event,
                            e
                          )}
                        components={{
                          event: FeedEvents
                        }}
                        onView={(view) => {
                          this.setState({ cal_view: view })
                        }}
                        views={['month', 'week']}
                      />
                      :
                      ''}
                  </div>
                </div>{/* --end widget-- */}
              </div>{/* --end content-- */}
            </div>{/* --end page-- */}
          </div>
        </div>
      </section>
    )
  }
}

  function mapStateToProps(state) {
  return {
    feed: state.feed,
    users: state.users,
    department: state.departments.department,
    hashAssets: state.assets.hashAssets,
    disblePopup: state.posts.creation.disblePopup,
    notificationData: state.notification_data,
    posts: state.posts,
  }
}

function mapDispatchToProps(dispatch) {
  return {
    feedActions: bindActionCreators(feedActions, dispatch),
    postsActions: bindActionCreators(postsActions, dispatch),
    userActions: bindActionCreators(userActions, dispatch),
    departmentActions: bindActionCreators(departmentActions, dispatch),
    assetsActions: bindActionCreators(assetsActions, dispatch),
    socialAccountsActions: bindActionCreators(socialAccountsActions, dispatch),
    notificationActions: bindActionCreators(notificationActions, dispatch),
  }
}

module.exports = connect(mapStateToProps, mapDispatchToProps)(Feed)
