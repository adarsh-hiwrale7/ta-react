
import { Link } from 'react-router'
import { browserHistory } from 'react-router'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Field, reduxForm, formValueSelector } from 'redux-form'
import { renderField } from '../Helpers/ReduxForm/RenderField'
import ReactDatePicker from '../Helpers/ReduxForm/ReactDatePicker'
import ReactTimePicker from '../Helpers/ReduxForm/ReactTimePicker'
import reactDatepickerChunk from '../Chunks/ChunkDatePicker'
import moment from '../Chunks/ChunkMoment'
import Globals from '../../Globals'
import PreLoader from '../PreLoader';

const required = value => (value ? undefined : 'Required')

class ChangeScheduleForm extends React.Component {
  constructor (props) {
    super(props)
    const { submitting, reset } = props
    this.state = {
      Moment: null,
      ReactDatePicker: null,
      scheduleData: [],
      schedulerCounter: 0
    }
  }
  componentWillReceiveProps (newProps) {
    if(this.props.scheduleDates!==newProps.scheduleDates){
      if (newProps.scheduleDates.length > 0) {
        //set state to store scheduledate 
        this.setState({
          scheduleData: newProps.scheduleDates,
          schedulerCounter: newProps.scheduleDates.length
        })
        this.props.change('allScheduleData', newProps.scheduleDates)
      }
    }
  }
  componentWillMount () {
    moment().then(moment => {
      reactDatepickerChunk()
        .then(reactDatepicker => {
          this.setState({ ReactDatePicker: reactDatepicker, Moment: moment })
        })
        .catch(err => {
          throw err
        })
    })
  }

  /**
 * @author disha
 * when new schedule is added then to store its value in array
 */
  addMoreScheduleRow () {
    var getScheduleData = this.state.scheduleData

    let time = this.state.Moment().format('HH:mm')
    let date = this.state.Moment().format('DD/MM/YYYY')

    let concatenatedDateTime = date + ' ' + time
    let timeStamp = this.state
      .Moment(concatenatedDateTime, 'DD/MM/YYYY HH:mm')
      .format('X')

    getScheduleData.push({
      scheduled_at: timeStamp
    })

    let counterIncrement = this.state.schedulerCounter + 1
    this.setState({
      schedulerCounter: counterIncrement,
      scheduleData: getScheduleData
    })

    this.props.change(`schedulerCounter`, counterIncrement)
    this.props.change('allScheduleData', getScheduleData)
  }

  /**
 * when clicked on save button to save schedule
 * @param {*} values form value
 */
  formSubmit (values) {
    this.props.handleSubmit(values)
  }

  /**
 * @author disha
 * when click on save button then to pass post id in handlesubmit
 * @param {*} e
 */
  clickSubmitButton (e) {
    this.props.change('postid', this.props.postId)
  }

  /**
   * to remove schedule from list
   * @param {*} index
   */
  removeScheduler (index) {
    var getScheduleData = this.state.scheduleData
    getScheduleData.splice(index, 1)
    let totalSchedulers = document.querySelectorAll('.schedule.row')

    this.props.change(`scheduledDate-${index}`, null)
    this.props.change(`scheduledTime-${index}`, null)

    let counterDecrement = this.state.schedulerCounter - 1
    this.setState({
      schedulerCounter: counterDecrement,
      scheduleData: getScheduleData
    })
  }

  render () {
    var feedprops=this.props.props
    const { handleSubmit, pristine, submitting } = this.props
    return (
      <div>
        {
          feedprops.location.pathname =='/campaigns'?
              feedprops.campaigns.scheduleLoading == true || feedprops.campaigns.updation.loading == true?<PreLoader className="Page" />:''
              : feedprops.feed? feedprops.feed.scheduleLoading? feedprops.feed.scheduleLoading == true || feedprops.posts.updation.loading ==true ? <PreLoader className="Page" /> :'':'':''}
        <div className='scheduleTitleWrapper'>
          <div className='label'>Schedule post</div>
        </div>
        <div className='post-details-schedulerInner'>
          <div className='postScheduleTitles clearfix'>
            <div className='scheduleDateColumn'>
              <div className='label'>Date</div>
            </div>
            <div className='scheduleTimeColumn'>
              <div className='label'>Time</div>
            </div>
          </div>
          <div className='postScheduleFields'>
            <form onSubmit={this.formSubmit.bind(this)} ref='form'>
              <div>
                {this.state.scheduleData.map((sch, index) => {
                  return (
                    <div
                      id={`scheduler-${index}`}
                      key={index}
                      className='schedule'
                    >
                      <div className='clearfix'>
                        <div className='scheduleDateColumn'>
                          {typeof sch.identity !== 'undefined'
                            ? <Field
                              ref={`scheduledDate${index}`}
                              name={`scheduledDate-${index}`}
                              dateValue={
                                  this.state.Moment !== null
                                    ? this.state.Moment.unix(sch.scheduled_at)
                                    : ''
                                }
                              value={
                                  this.state.Moment !== null
                                    ? this.state.Moment.unix(sch.scheduled_at)
                                    : ''
                                }
                              component={props => {
                                return (
                                  <ReactDatePicker
                                    {...props}
                                    reactDatePicker={this.state.ReactDatePicker}
                                    moment={this.state.Moment}
                                    dateFormat="DD/MM/YYYY"
                                    />
                                )
                              }}
                              />
                            : <Field
                              ref={`scheduledDate${index}`}
                              name={`scheduledDate-${index}`}
                              value={this.state.Moment !== null? this.state.Moment.unix(sch.scheduled_at): ''}
                              component={props => {
                                return (
                                  <ReactDatePicker
                                    {...props}
                                    reactDatePicker={this.state.ReactDatePicker}
                                    moment={this.state.Moment}
                                    dateFormat="DD/MM/YYYY"
                                    />
                                )
                              }}
                              />}
                        </div>
                        <div className='scheduleTimeColumn'>
                          {typeof sch.identity !== 'undefined'
                            ? <Field
                              ref={`scheduledTime${index}`}
                              name={`scheduledTime-${index}`}
                              timeValue={
                                  this.state.Moment !== null
                                    ? this.state.Moment.unix(sch.scheduled_at)
                                    : ''
                                }
                              component={props => {
                                return (
                                  <ReactTimePicker
                                    {...props}
                                    moment={this.state.Moment}
                                    />
                                )
                              }}
                              />
                            : <Field
                              ref={`scheduledTime${index}`}
                              name={`scheduledTime-${index}`}
                              sch={sch}
                              component={props => {
                                return (
                                  <ReactTimePicker
                                    {...props}
                                    moment={this.state.Moment}
                                    />
                                )
                              }}
                              />}
                        </div>

                        <div className='scheduleActionColumn last'>
                          <div className='addRemoveSchdule clearfix'>
                            <a
                              className='deleteSchedule'
                              title='Remove schedule'
                              onClick={this.removeScheduler.bind(this, index)}
                            >
                              <i className='material-icons'>remove</i>{' '}
                            </a>
                            <a
                              title='Add new schedule'
                              id='add-more-schedule'
                              onClick={this.addMoreScheduleRow.bind(this)}
                              className='addScheduleBtn'
                            >
                              <i className='material-icons'>add</i>
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  )
                })}
              </div>

              <button
                className='btn btn-primary createPostBtn'
                type='submit'
                onClick={this.clickSubmitButton.bind(this)}
              >
                Save
              </button>
            </form>
          </div>
        </div>
      </div>
    )
  }
}

function mapStateToProps (state, ownProps) {
  return {}
}

function mapDispatchToProps (dispatch) {
  return {}
}
let connection = connect(mapStateToProps, mapDispatchToProps)

var reduxConnectedComponent = connection(ChangeScheduleForm)

export default reduxForm({
  form: 'ChangeScheduleForm' // a unique identifier for this form
})(reduxConnectedComponent)
