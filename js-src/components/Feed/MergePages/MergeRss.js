import { connect } from 'react-redux'
import { Field, reduxForm } from 'redux-form'
import moment from '../../Chunks/ChunkMoment'
import PreLoader from '../../PreLoader'
import { Link } from 'react-router';
import MergeJobsRssSharePopup from './MergeJobsRssSharePopup';
import CenterPopupWrapper from '../../CenterPopupWrapper'
import SliderPopup from '../../SliderPopup'
import MergeRssPopup from './MergeRssPopup'
class MergeRss extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            moment: '',
            shareJobRssPopupState: false,
            shareJobRssPopupStateOfPostID: null,
            curation_popup_rss:false,
            curated_data:'',
        }
    }
    componentWillMount() {
        moment().then(moment => {
            this.setState({ moment: moment })
        })
    }

    componentWillReceiveProps(newProps){
        if(this.props.props.posts.creation.disblePopup !== newProps.props.posts.creation.disblePopup && newProps.props.posts.creation.disblePopup){
            this.closeRssPopup();
        }
    }
    closeRssPopup(){
        this.setState({
            curation_popup_rss: false,
            curated_data: ''
          })
        document.body.classList.remove('center-wrapper-body-container')
      }
      curation_popup (e) {
        this.setState({
          curation_popup_rss: true,
          curated_data: e,
        })
      }
       /**
   * to open popup of rss post
   */
  open_curation_popup () {
    document.body.classList.add('center-wrapper-body-container')
  return (
      <div>
       <CenterPopupWrapper>
            <div className = "rss-popup-deatils-page-container">
            <MergeRssPopup
                    alldata={this.state.curated_data}
                    closeRssPopup={this.closeRssPopup.bind(this)}
                    accounts={this.props.accounts}
                />
             </div>
       </CenterPopupWrapper>
       </div>
  )
}
    /**
     * to call different function according to the data of rss
     */
    fetchRSS() {
        var rssdata = ''
            rssdata = this.props.props.feed.RssData
            var firstIndex,lastIndex;
        // rssdata = [{ title: "jhsjhjdfhsjkhfj ksjdhsj", "description":"sjhdjs kjhs jdjksh jd","image": null, "date": 1540989660 }]
        //  { "title": "jhjhj jh jh", "description":"sjhdjs kjhs jdjksh jd", "image": null, "date": 1540988460 }]
        if (this.props.globalcount !== '' && this.props.globalcount !== undefined) {
             firstIndex = this.props.globalcount * 10 - 10;
             lastIndex = this.props.globalcount * 10 - 1;
            rssdata = this.props.props.feed.RssData.slice(firstIndex, lastIndex + 1);

        }
        if (this.props.props.feed.RssData[firstIndex]!==undefined) {
            if (rssdata.length > 0 ) {
                if ((rssdata.length <= 2 && this.props.globalcount == 1) || (this.props.globalcount > 1 && rssdata.length == 1 )) {
                    //if data is 2 or less than 2 and page is 1 then to display only 2 data or page count is 2,3,4.... and data is only one then to display only one data
                    return (
                        <div  className='No-slider-Active clearfix'>
                            {this.fetchRssStaticDataForTwoOrLessData(rssdata)}
                        </div>
                    )
                } else if ((rssdata.length >= 3 && this.props.globalcount== 1) || (rssdata.length >= 3 && this.props.globalcount > 1) || (this.props.globalcount > 1 && rssdata.length <=2)) {
                    return (
                        <div>
                            {this.fetchRssStaticDataForMultiple(rssdata)}
                        </div>
                    )
                }
            }else{
                //code for no rss
            }
        }else if(this.props.props.feed.RssFromFeedLoading==true){
            return (
                <PreLoader feedtype='rss' />
            )
        }
    }
    
    /**
     * to display rss(curated) data in feed page
     * @param {*} rssdata all data of rss
     * @author disha
     */
    fetchRssDynamicData(rssdata){
        var imgstyles = {
            backgroundSize: 'cover',
            backgroundRepeat: 'no-repeat',
            backgroundImage: '/img/banner-dashboard.png'
        }
        return(
            rssdata.map((rs, i) => {
                var image = rs.image ? rs.image : ''
                var striptags = require('striptags')
                let rs_title = rs.title.replace(new RegExp('&amp;', 'g'),'&');
                let rs_descripation = rs.description
                let rs_source = rs.permalink
                var rssImage = document.createElement('div')
                rssImage.innerHTML = rs_descripation
                if (image == '') {
                    if (rssImage.getElementsByTagName('img')[0]) {
                        image = rssImage.getElementsByTagName('img')[0]
                            .src
                    } else {
                        image = '/img/banner-dashboard.png'
                    }
                }
                return (
                    <div className="curated-wrapper clearfix" key={i}>
                        <div className="curated-wrapper-box">
                            <div className="curated-mainbox">
                                <img className="curated-image" onClick={this.curation_popup.bind(this,rs)}  src={image ?image:'/img/banner-dashboard.png'} onError={(e)=>{e.target.src='/img/banner-dashboard.png'}} />
                                <div className="curated-headline-time-button-box">
                                    <div className="curated-headline" onClick={this.curation_popup.bind(this,rs)}>
                                        <p>{rs_title}</p>
                                    </div>
                                    <div className="curated-time-button clearfix">
                                        <div className="curated-time" onClick={this.curation_popup.bind(this,rs)}>
                                            <i className="material-icons">access_time_black_48dp</i><span>{this.state.moment ? this.state.moment.unix(rs.date).local().fromNow() : ''}</span>
                                        </div>
                                        <div className = "curation-share-popup-container">
                                        <div>
                                        <div className="curated-share-button">
                                          
                                            <MergeJobsRssSharePopup
                                                currData={rs}
                                                shareFrom={'rss'}
                                                accounts={this.props.accounts}
                                            />
                                        </div>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                <div className="curated-content" onClick={this.curation_popup.bind(this,rs)}>
                                    <p>{striptags(rs_descripation)}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                )
            })
        )
    }

    /**
     * if data is 2 or less then 2 then slider should not be applied thats why create diff function without silder component
     * @param {*} rssdata rrss data
     * @author disha
     */
    fetchRssStaticDataForTwoOrLessData(rssdata) {
        return (
            <div className={`feed-post-item clearfix ${rssdata.length==1 ? 'one-data-job' :''}`}>
                <div className="curated-icon-content clearfix">
                    <div className="curated-icon">
                        <i className="material-icons">launch</i>
                    </div>
                    <div className="curated-opening-sec">
                        <span>Curated</span>
                        <p>You might like this!</p>
                    </div>
                </div>
                <div className="curated-boxes clearfix">
                    {this.fetchRssDynamicData(rssdata)}
                    {this.props.props.feed.RssData.length > 0 && (this.props.props.feed.RssData.length >= 10 || this.props.globalcount >1) ?
                    //if page count is more than 1 means (2,3,4...) and rss total data length is more than 10 then to display view all
                        <div className="view-curated-main-box job-wrapper">
                            <Link to='/feed/rss'>
                                <div className="job-wrapper-box">
                                    <div class="view-curated">
                                        <div class="view-curated-icon">
                                            <i class="material-icons">launch</i>
                                        </div>
                                        <div class="view-curated-link">
                                            View all curated
                                        </div> 
                                    </div>
                                </div>
                            </Link>
                        </div>
                    :''}
                </div>
            </div>
        )
    }
    /**
     * if rss data is multiple then to add slider and view all page code
     * @param {*} rssdata rss all data
     * @author disha
     */
    fetchRssStaticDataForMultiple(rssdata) {
        const settings = {
            dots: false,
            speed: 500,
            slidesToShow: 2,
            slidesToScroll: 1,
            arrows:true,
            infinite: false,
              responsive: [
                {
                  breakpoint: 768,
                  settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    infinite: false,
                  }
                },
                {
                  breakpoint: 767,
                  settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                  }
                }
              ]
          }
        return (
            <div className="feed-post-item">
                <div className="curated-icon-content clearfix">
                    <div className="curated-icon">
                        <i className="material-icons">launch</i>
                    </div>
                    <div className="curated-opening-sec">
                        <span>Curated</span>
                        <p>You might like this!</p>
                    </div>
                </div>
                <div className="curated-boxes">
                {this.props.Slider?
                    <this.props.Slider.default {...settings}>
                    {this.fetchRssDynamicData(rssdata)}

                    {this.props.props.feed.RssData.length > 0 && (this.props.props.feed.RssData.length >= 10 || this.props.globalcount >1) ?
                    //if page count is more than 1 means (2,3,4...) and rss total data length is more than 10 then to display view all
                        <div className="view-curated-main-box">
                            <Link to='/feed/rss'>
                                <div className="job-wrapper-box">
                                    <div class="view-curated">
                                        <div class="view-curated-icon">
                                            <i class="material-icons">launch</i>
                                        </div>
                                        <div class="view-curated-link">
                                            View all curated
                                        </div> 
                                    </div>
                                </div>
                            </Link>
                        </div>
                    :''}
                    </this.props.Slider.default>:''}
                </div>
            </div>
        )
    }
    render() {
        let roleName = JSON.parse(localStorage.getItem('roles')) !== null ? JSON.parse(localStorage.getItem('roles'))[0].role_name : ''
        return (
            <div>
                <section id='Rss' className='settings'>
                    <div className='main-container'>
                
                        {this.state.curation_popup_rss == true? this.open_curation_popup(): ''}
                        {this.fetchRSS(this)} 
                    </div>
                </section>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        accounts: state.socialAccounts.accounts,
    }
}
function mapDispatchToProps(dispatch) {
    return {
    }
}
let connection = connect(mapStateToProps, mapDispatchToProps)
var reduxConnectedComponent = connection(MergeRss)

export default reduxForm({
    form: 'MergeRss' // a unique identifier for this form
})(reduxConnectedComponent)
