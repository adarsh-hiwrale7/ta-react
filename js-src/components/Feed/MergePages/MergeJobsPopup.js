import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Field, reduxForm } from 'redux-form'
var striptags = require('striptags');
import MergeJobsRssSharePopup from './MergeJobsRssSharePopup'
import PopupWrapper from '../../PopupWrapper'
import moment from '../../Chunks/ChunkMoment'
var jobdata = '';
export default class JobPopup extends React.Component {
    constructor (props) {
      super(props)
      this.state = {
       
      }
    }
    componentWillMount() {
        moment().then(moment => {
            this.setState({ moment: moment })
        })
    }
    componentDidMount(){
        if(document.getElementById('jobFullDesc') !== null && document.getElementById('jobFullDesc') !== undefined){
        document.getElementById('jobFullDesc').innerHTML =  this.props.jobdata.description;
    }
}
    render(){
        jobdata = this.props.jobdata
         return(
      <div> 
        <div className='job-details-container'>
          <div className='job-details '>
            <header className='heading clearfix'>
              <h3>JOB DETAIL</h3>
              <button id="close-popup" class="btn-default" onClick = {this.props.closeJob}><i class="material-icons">clear</i></button>
            </header>
            <PopupWrapper>

                             <div className='read-only-data'>
                                 <div className="job-popup">
                                     <div className="job-popup-heading">
                                         <h2>{jobdata.title}</h2>
                                     </div>
                                     <div className="date-time-box clearfix">
                                         <div className="part-time-job-box clearfix">
                                         {jobdata.type !== null?<div className="part-time-main-box">
                                                 <span>{jobdata.type}</span>
                                             </div>:''}
                                             <p className='job-date'>
                                             {this.state.moment
                                                 ? this.state.moment(this.state.moment.posted_date)
                                                     .format(' Do MMMM YYYY')
                                                 : ''}
                                         </p>
                                         </div>
                                         <div className = "sharepopup-container">
                                            <MergeJobsRssSharePopup
                                                    currData={jobdata}
                                                    shareFrom={'jobs'}
                                                    accounts={this.props.accounts}/>
                                            </div>
                                     </div>

                <div className='form-row'>
                <div className="job-salary-location-industry-mainbox clearfix">
                    <div className="salary-mainbox clearfix">
                    <div className="salary-icon-box">
                           <i className='material-icons'>card_giftcard</i>
                            </div>
                            <div className="salary-content-box">
                            <label>Offered Salary</label>
                            <div class='value'>{jobdata.from_salary} - {jobdata.to_salary}</div>
                            </div>
                    </div>
                    <div className="location-mainbox clearfix">
                    <div className="location-icon-box">
                            <i className='material-icons'>location_on_black_48dp</i>
                            </div>
                            <div className="salary-content-box">
                            <label>Location</label>
                            <div class='value'>{jobdata.location}<span class="india-text">({jobdata.country})</span></div>
                            </div>
                    </div>
                    <div className="industry-mainbox clearfix">
                    <div className="industry-icon-box">
                           <i className='material-icons'>business_black_48dp</i>
                            </div>
                            <div className="salary-content-box">
                            <label>Industry</label>
                            <div class='value'>{jobdata.industry}</div>
                            </div>
                    </div>
                </div>
                </div>
                <div className="job-content-box">
                <div className="job-content">
                <p id='jobFullDesc'>{striptags(jobdata.description)}</p>
                </div>
                </div>
                {/* <div className = "sharepopup-container">
                <MergeJobsRssSharePopup
                        currData={jobdata}
                        shareFrom={'jobs'}
                        accounts={this.props.accounts}/>
                </div> */}
                {/* <div className="job-something-textbox">
                <form role="form">
    <div class="form-row rss_comment_form clearfix user-tag-editor"><label>Say something</label>
        <div class="form-row">
            <div class="mention-input-wrapper mention-input-wrapper--multiLine">
                <div class="mention-input-wrapper__control">
                    <div class="mention-input-wrapper__highlighter">
                       
                    </div><textarea name="twitter_limit_count_9" placeholder="Say something" class="mention-input-wrapper__input"></textarea>
                </div>
            </div>
            <div class="form-valid-message"></div>
            </div>
        </div>
</form>
</div> */}
              </div>
              </div>

            </PopupWrapper>
       
         
         
          </div>
          </div>
      </div>
         )
    }
}