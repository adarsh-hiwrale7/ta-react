import { connect } from 'react-redux';
import { Link } from 'react-router';
import CuratedCommentForm from '../CuratedPages/CuratedCommentForm';
import Globals from '../../../Globals';
import ImageLoader from 'react-imageloader';
import PreLoaderMaterial from '../../PreLoaderMaterial';
import PopupWrapper from '../../PopupWrapper'
import moment from '../../Chunks/ChunkMoment';
import MergeJobsRssSharePopup from './MergeJobsRssSharePopup';
 var striptags = require('striptags');

var readmore = false;
var savedData=false;
var countnum=0;
var lastCursorIndex=0;
var txtarea=0; 
var start=0; 
var end=0;

export default class MergeRssPopup extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      popupVisible: false,
      sharepost: false,
      openCreatePopup: false,
      saveAllData: {},
      savedData: false,
      accname:'',
      comment:{},
      header_height:60,
      cntnum:0,
      msg:""
    }
  }
  
  wordCountForm(e)
  {
    // if(document.getElementsByName('rss-count')[0]){
      if(e>Globals.TWITTER_LIMIT_WORD)
      {
        document.getElementsByClassName("rss-count")[0].style.color="red"
        document.getElementsByClassName("rss-count")[0].style.marginTop="-59px" ;
        document.getElementsByClassName("tweet-msg")[0].style.color="red"
        document.getElementsByClassName("tweet-msg")[0].style.display="inline"
        this.setState({msg:"Twitter Character Limit Exceeded"});
        this.setState({cntnum:e});
      }
      else
      {
        document.getElementsByClassName("tweet-msg")[0].style.display="none"
        document.getElementsByClassName("rss-count")[0].style.color="#7b7b7b"
        document.getElementsByClassName("tweet-msg")[0].style.color="#7b7b7b"
        this.setState({cntnum:e});
      }
   // }
  }
  handleFormtext (data) {
    savedData = false
    this.setState({
      saveAllData: data
    })
    //this.closePostCreatePopup()
  }
  readMore () {
    var rss_Readmore_text = '';
    var readmore_link  = '';
    rss_Readmore_text =  document.getElementById('text-rss');
    readmore_link = document.getElementById('readmorelink');
    
    readmore = !readmore
    if (readmore == true) {
      rss_Readmore_text.classList.add("text-read-more-feed-rss");
      rss_Readmore_text.classList.remove("text_readmore");
      
     // readmore_link.classList.remove("hide");
    } else {
    
      rss_Readmore_text.classList.add("text_readmore");
      rss_Readmore_text.classList.remove("text-read-more-feed-rss");
     // readmore_link.classList.add("hide");
    }
  }
  renderRssForm (e) {
    return( 
      <CuratedCommentForm
      commentFormText={this.handleFormtext.bind(this)}
      countnum={this.wordCountForm.bind(this)}
    />
    )
  }
  componentWillMount()
  {
    countnum=0;lastCursorIndex=0;txtarea=0;start=0;end=0;
    this.setState({cntnum:''});
    moment().then(moment => {
      this.setState({ moment: moment })
  })
  }

  componentDidMount(){ 
    document.getElementById('text-rss').innerHTML = this.props.alldata.description;
    if(document.getElementsByClassName('rss-title')&& document.getElementsByClassName('rss-title')[0]){
      this.setState({
        header_height : document.getElementsByClassName('rss-title')[0].offsetHeight + 15
      })
    }
  }

  //to close popup and call method which is in parent component
  closeMergeRssPopup(){
    this.props.closeRssPopup()
  }
  render () {
    var img = ''
    var rssdata=this.props.alldata
    if (rssdata.image !== null) {
      img = rssdata.image
    }
    var rss_title = rssdata.title.replace(new RegExp('&amp;', 'g'),'&');
    var rss_descripation =  rssdata.description;
    var rss_source = rssdata.permalink;
    var rss_date =this.state.moment
      ? this.state.moment.unix(rssdata.date)
          .format(' Do MMMM  YYYY')
      : ''
    //Check element for apply height to popup container
    if(document.getElementsByClassName('rss-title')[0]){
    let header_height = document.getElementsByClassName('rss-title')[0].offsetHeight + 15;
    let rssPopupScrollbar = {
      maxHeight: `calc(100vh - ${this.state.header_height}px)`
    }
    if(document.querySelectorAll('.rss_text .popupDescSection')[0]){
      document.querySelectorAll('.rss_text .popupDescSection')[0].setAttribute("style","height:"+rssPopupScrollbar.maxHeight);
    }
    }
    return (
      <div className='rss_popup rss-new-design-in-feed' id='rss_popup'>
        <div className='inner-rss-popup clearfix'>
          <div className='rss_text'>
          <header className='heading clearfix'>
            <h3>Curated</h3>
            <button id="close-popup" class="btn-default" onClick={this.closeMergeRssPopup.bind(this)} ><i class="material-icons">clear</i></button>
          </header>
          <PopupWrapper>
          <div className='rss-popup-content-wrapper'>
           <div className = "rss-heading"> 
           <h3> {rss_title}</h3>
           </div>
           <div className = "curated-date clearfix">
              <div className = "date-container">  <span className = "icon-date"> <i class="material-icons">date_range</i> </span> <span className = "date-text"> {rss_date} </span></div>
              <div className = "link-container date-container"> <a target='_blank' href={rss_source}><span className = "icon-date"><i class="material-icons">launch</i></span>  <span className = "date-text"> Source link</span></a> </div>
              <div className = "sharepopup-container">
              <MergeJobsRssSharePopup
                  currData={rssdata}
                  shareFrom={'rss'}
                  accounts={this.props.accounts}
              />
           </div>
           </div>
           <div className = "container-banner-img">
           {img
            ?
           <div className = "banner-img"> 
             <ImageLoader
                src={img}
                preloader={PreLoaderMaterial}
                className='thumbnail'
                >
                <div className='image-load-failed'>
                  <i class='material-icons no-img-found-icon'></i>
                </div>
              </ImageLoader>
           </div>: ''}
          </div>
           <div className='text_readmore' id='text-rss'>
             {striptags(rss_descripation)}
           </div>
          
          {/* <div className='add_comment_box'>
            {this.renderRssForm(this)}
            <div className='rss_share'>
              <div>
                <div className='tweet-msg'>
                  {this.state.msg}
                </div>
                <div className='feed-job-share-block-title'>
                  <span>Share</span>
                </div>
                  {/* <CuratedSharePopup
                    directShare={false}
                    liuserid={this.props.liuserid}
                    fbuserid={this.props.fbuserid}
                    comment={this.state.saveAllData}
                    alldata={rssdata}
                    location={this.props.location}
                    closeRssPopup={this.props.closeRssPopup}
                  /> */}
                  {/*<div className="rss-count">
                    {this.state.cntnum}
                  </div>
                        
              </div>
            </div>
          </div> */}
          </div>
          </PopupWrapper>
          </div>
        </div>
      </div>
    
   )
  }
}
