import { connect } from 'react-redux'
import { Link } from 'react-router'
import { Field, reduxForm } from 'redux-form'
import PreLoader from '../../PreLoader'
import MergeJobsRssSharePopup from './MergeJobsRssSharePopup'
import SliderPopup from '../../SliderPopup'
import MergeJobsPopup from './MergeJobsPopup'
import * as utils from '../../../utils/utils'
import CenterPopupWrapper from '../../CenterPopupWrapper'
var striptags = require('striptags')

class MergeJobs extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
        JobPostsPopup:false,
        SelectedJob:'',
    }
  }
  showJobDetail (alldata, me) {
    this.setState({
      JobPostsPopup: true,
      SelectedJob: alldata
    })
  }
  /**
   * to close popup
   * @param {*} popup
   */
  closePopup (popup) {
    document.body.classList.remove('center-wrapper-body-container');
    this.setState({
      JobPostsPopup: false,
    //   accname: '',
    //   cntnum: 0,
    //   shareLink: '',
    //   comment:{}
    })
  }
  /**
   * to call diff function according to job data length for diff design
   */
  fetchJobs () {
    var jobdata = ''
    jobdata = this.props.props.feed.JobData
    var firstIndex,lastIndex;
    // var jobdata=[{"identity":"vpKjy","location":"ahmedabad","title":"job one two","industry":"science","type":"part time","from_salary":10000,"to_salary":20000,"description":"urget required data","facebook_link":"http:\/\/www.facebook.com\/1384078301816927_1995038707387547","linkedin_link":"https:\/\/www.linkedin.com\/company\/18277490\/comments?topic=6362301377028464640&type=U&scope=18277490&stype=C&a=tt2-","twitter_link":"https:\/\/twitter.com\/recruit_inspire\/status\/956536100520169472","workable_link":null,"posted_date":"2017-12-14 00:00:00","country":"india","job_source":"LM"}]
    // ,{"identity":"vlKjy","location":"ahmedabad","title":"job33 one two","industry":"science","type":"part time","from_salary":10e00,"to_salary":23000,"description":"urget required data","facebook_link":"http:\/\/www.facebook.com\/1384078301816927_1995038707387547","linkedin_link":"https:\/\/www.linkedin.com\/company\/18277490\/comments?topic=6362301377028464640&type=U&scope=18277490&stype=C&a=tt2-","twitter_link":"https:\/\/twitter.com\/recruit_inspire\/status\/956536100520169472","workable_link":null,"posted_date":"2017-12-14 00:00:00","country":"india","job_source":"LM"}]
    let roleName = JSON.parse(localStorage.getItem('roles')) !== null
    ? JSON.parse(localStorage.getItem('roles'))[0].role_name
    : ''
    
    if (this.props.globalcount !== '' && this.props.globalcount !== undefined) {
      firstIndex = this.props.globalcount * 10 - 10
      lastIndex = this.props.globalcount * 10 - 1
      jobdata = this.props.props.feed.JobData.slice(firstIndex, lastIndex + 1)
    }
      if(this.props.props.feed.JobData[firstIndex]!==undefined){
        //to check if job dta is fetched from api
      if (jobdata.length > 0) {
        if ((jobdata.length <= 2 && this.props.globalcount == 1) || (this.props.globalcount > 1 && jobdata.length == 1 ) ) {
          //if data is 2 or less than 2 and page is 1 then to display only 2 data or page count is 2,3,4.... and data is only one then to display only one data
          return (
            <div className='No-slider-Active clearfix'>
              {this.fetchJobStaticDataForTwoOrLessData(jobdata)}
            </div>
          )
        } else if ((jobdata.length >= 3 && this.props.globalcount== 1) || (jobdata.length >= 3 && this.props.globalcount > 1) || (this.props.globalcount > 1 && jobdata.length <=2) ) {
          return (
            <div>
              {this.fetchJobStaticDataForMultiple(jobdata)}
            </div>
          )
        }
      }
    }else if(this.props.props.feed.is_configured==0 && (roleName=="admin" || roleName=="super-admin")){
        // code for sample data display
        return(
          <div>
            {this.fetchSampleDataForJobs()}
          </div>
      )
    }else if(this.props.props.feed.JobsFromFeedLoading==true){
      return <PreLoader feedtype='jobs' />
    }
  }
  /* @author disha
  * chnage the popup design @auther kinjal
  * to open popup of job for detail view
  */
 renderJobspopup () {
  document.body.classList.add('center-wrapper-body-container');
   return (
   <div>
     {/* this componet used for the center popup  */}
    <CenterPopupWrapper>
         <div className = "rss-popup-deatils-page-container">
          <MergeJobsPopup
            jobdata = {this.state.SelectedJob }
            accounts={this.props.accounts}
            closeJob={this.closePopup.bind(this, 'posts')}
            />
          </div>
    </CenterPopupWrapper>
    </div>
    )
 }
  /**
   * sample data will display when job is not available
   */
    fetchSampleDataForJobs() {
        return (
            <div className="feed-post-item" >
                <div className="sample-data-main-box">
                    <div class="job-box clearfix">
                        <div className="job-icon-content clearfix">
                            <div className="job-icon">
                                <i class="material-icons">card_travel_black_48dp</i>
                            </div>
                            <div class="job-opening-sec">
                                <span>Jobs</span>
                                <p>Note: This is sample data</p>
                            </div>
                            <Link to='/settings/integration' className="configure-jobs"  title='Configure'>Configure jobs</Link>
                        </div>
                        <div className="sample-data-job">
                            <div className="job-wrapper clearfix">
                                <div className="job-wrapper-box">
                                <Link to='/settings/integration' title='Configure'>
                                    <div className="job-mainbox">
                                        <div className="part-time-job-box">
                                            <div className="part-time-main-box">
                                                <span>part-time</span>
                                            </div>
                                        </div>
                                        <div className="ui-ux-design-box">
                                            <h3 className="job-heading">UI/UX Designer</h3>
                                            <p class="month-box">$ 10000 / month</p>
                                            <div className="location-box">
                                                <a><i class="material-icons">place</i>London</a>
                                            </div>
                                            <p class="job-content">We are looking for an analytical, results-driven Back-end Developer who will work with team members to troubleshoot and improve current back-end applications and processes </p>
                                            <a class="share-job"
                                            ><i class="material-icons">share</i> <span>Share</span></a>
                                            {/* {this.state.shareJobRssPopupState &&
                                                        this.state.shareJobRssPopupStateOfPostID ==
                                                        jd.identity
                                                        ? this.renderPopup(jd)
                                                        : null} */}
                                        </div>
                                    </div>
                                    </Link>
                                </div>
                            </div>

                        </div>
                        <div className="sample-data-job">
                            <div className="job-wrapper clearfix">
                                <div className="job-wrapper-box">
                                <Link to='/settings/integration' title='Configure'>
                                    <div className="job-mainbox">
                                        <div className="part-time-job-box">
                                            <div className="part-time-main-box">
                                                <span>part-time</span>
                                            </div>
                                        </div>
                                        <div className="ui-ux-design-box">
                                        
                                            <h3 className="job-heading">UI/UX Designer</h3>
                                            <p class="month-box">$ 10000 / month</p>
                                            <div className="location-box">
                                                <a><i class="material-icons">place</i>UK</a>
                                            </div>
                                            <p class="job-content">We are looking for an analytical, results-driven Back-end Developer who will work with team members to troubleshoot and improve current back-end applications and processes </p>
                                            <a class="share-job"
                                            ><i class="material-icons">share</i><span>Share</span></a>
                                            {/* {this.state.shareJobRssPopupState &&
                                                        this.state.shareJobRssPopupStateOfPostID ==
                                                        jd.identity
                                                        ? this.renderPopup(jd)
                                                        : null} */}
                                                        
                                        </div>
                                    </div>
                                    </Link>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>

        )
    }
  /**
   * to display jobs data in feed page
   * @param {*} jobdata all jobs data in array
   */
  fetchJobsDynamicData(jobdata){
      return(
        jobdata.map((jd, i) => {
            var html = jd.description
            var jobTitle = utils.limitLetters(jd.title,30)
            return (
            <div className='slick-slide' key={i}>
              <div className='job-wrapper clearfix'>
                <div className='job-wrapper-box'>
                  <div className='job-mainbox'>
                  <div onClick={this.showJobDetail.bind(this,jd)}>
                    <div className='part-time-job-box'>
                      {jd.type !== null
                        ? <div className='part-time-main-box'>
                          <span>{jd.type}</span>
                        </div>
                        : ''}
                    </div>
                    <div className='ui-ux-design-box'>
                      <h3 className='job-heading single-job-heading' title={jd.title}>
                        {jd.title !== null &&
                          jd.title !== undefined &&
                          jd.title !== ''
                          ? jobTitle
                          : ''}
                      </h3>
                      <p class='month-box'>
                        {jd.from_salary !== 0 && jd.to_salary !== 0
                          ? `$ ${jd.from_salary}- $ ${jd.to_salary}`
                          : ''}
                      </p>
                      <div className='location-box'>
                        {jd.location !== undefined && jd.location !== null
                          ? <a>
                            <i class='material-icons'>place</i>{jd.location}
                          </a>
                          : ''}
                      </div>
                      <p class='job-content'>{striptags(html)}</p>
                    </div>
                    </div>
                    <MergeJobsRssSharePopup
                        currData={jd}
                        shareFrom={'jobs'}
                        accounts={this.props.accounts}/>
                  </div>
                </div>
                </div>
              </div>
            )
          })
      )
  }
  /**
   * if data is 2 or less then 2 then slider should not be applied.
   * @param {*} jobdata all job data list in array
   * @author disha
   */
  fetchJobStaticDataForTwoOrLessData (jobdata) {
    return (
      <div className={`feed-post-item single-data-job ${jobdata.length==1  && this.props.globalcount ==1 ? 'one-data-job' :''}`}>
        <div class='job-box clearfix'>
          <div className='job-icon-content clearfix'>
            <div className='job-icon'>
              <i class='material-icons'>card_travel_black_48dp</i>
            </div>
            <div class='job-opening-sec'>
              <span>Jobs</span>
              <p>New job openings!</p>
            </div>
          </div>
          {this.fetchJobsDynamicData(jobdata)}
          {this.props.props.feed.JobData.length > 0 && (this.props.props.feed.JobData.length >= 10 || this.props.globalcount > 1)
              ? <div className='view-job-main-box job-wrapper'>
                <Link to='/feed/jobs'>
                  <div className='job-wrapper-box'>
                    <div class='view-jobs'>
                      <div class='view-job-icon'>
                        <i class='material-icons'>card_travel_black_48dp</i>
                      </div>
                      <div class='view-job-link'>
                        View all jobs
                      </div>
                    </div>
                  </div>
                </Link>
              </div>
              : ''}
        </div>
      </div>
    )
  }
  /**
   * to add slider in jobs for multiple data
   * @param {*} jobdata all data of jobs in array
   * @author disha
   */
  fetchJobStaticDataForMultiple (jobdata) {
    const settings = {
      dots: false,
      speed: 500,
      slidesToShow: 2,
      slidesToScroll: 1,
      arrows:true,
      infinite: false,
        responsive: [
          {
            breakpoint: 768,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 1,
              infinite: false,
            }
          },
          {
            breakpoint: 767,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1
            }
          }
        ]
    }
    return (
      <div className='feed-post-item'>
        <div class='job-box'>
          <div className='job-icon-content clearfix'>
            <div className='job-icon'>
              <i class='material-icons'>card_travel_black_48dp</i>
            </div>
            <div class='job-opening-sec'>
              <span>Jobs</span>
              <p>New job openings!</p>
            </div>
          </div>
          {this.props.Slider?
          <this.props.Slider.default {...settings}>
           {this.fetchJobsDynamicData(jobdata)}
            {this.props.props.feed.JobData.length > 0 && (this.props.props.feed.JobData.length >= 10 || this.props.globalcount > 1)
            //if page count is more than 1 means (2,3,4...) and job total data length is more than 10 then to display view all
              ? <div className='view-job-main-box job-wrapper'>
                <Link to='/feed/jobs'>
                  <div className='job-wrapper-box'>
                    <div class='view-jobs'>
                      <div class='view-job-icon'>
                        <i class='material-icons'>card_travel_black_48dp</i>
                      </div>
                      <div class='view-job-link'>
                        View all jobs
                      </div>
                    </div>
                  </div>
                </Link>
              </div>
              : ''}
          </this.props.Slider.default>:''}
        </div>
      </div>
    )
  }
  render () {
    let roleName = JSON.parse(localStorage.getItem('roles')) !== null
      ? JSON.parse(localStorage.getItem('roles'))[0].role_name
      : ''
    return (
      <div>
        <section id='jobs' className='settings'>
          <div className='main-container'>
          {this.state.JobPostsPopup ? this.renderJobspopup() : ''}
          {this.fetchJobs(this)}
          </div>
        </section>
      </div>
    )
  }
}

function mapStateToProps (state) {
  return {
    accounts: state.socialAccounts.accounts
  }
}
function mapDispatchToProps (dispatch) {
  return {}
}
let connection = connect(mapStateToProps, mapDispatchToProps)
var reduxConnectedComponent = connection(MergeJobs)

export default reduxForm({
  form: 'MergeJobs' // a unique identifier for this form
})(reduxConnectedComponent)
