import { connect } from 'react-redux'
import { Field, reduxForm } from 'redux-form'
import JobCurationSharePopup from '../JobCurationSharePopup'
import ReactCSSTransitionGroup from 'react/lib/ReactCSSTransitionGroup'
class MergeJobsRssSharePopup extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      shareJobRssPopupState: false,
      shareJobRssPopupStateOfPostID: null
    }
  }
  /**
   * set state to true to open share popup
   * @param {*} postID id of job or link from rss
   */
  onShareLinkClick (postID) {
    var me = this
    if(me.state.shareJobRssPopupState==true){
      me.setState({
        shareJobRssPopupState: false,
        shareJobRssPopupStateOfPostID: null
      })
    }
    me.setState({
      shareJobRssPopupState: true,
      shareJobRssPopupStateOfPostID: postID
    })
  }
  componentDidMount(){
    var me = this
        window.addEventListener('click', function (event) {
          var ShareClickClass = event.target.parentNode
          if(ShareClickClass){
            if(ShareClickClass.className !== undefined && !ShareClickClass.className.includes('nav-side-share-popup') && me.state.shareJobRssPopupState==true && (ShareClickClass.parentNode.className!=='sharePopupWrapper'|| ShareClickClass.className!=="share-job")){
              me.setState({
                shareJobRssPopupState: false,
                shareJobRssPopupStateOfPostID: null
              })
            }
          }
        })
  }
  renderPopup (jobData) {
    return (
      <div className='share-popup-post hide'>

        <ReactCSSTransitionGroup
          transitionName='fade'
          transitionAppear
          transitionEnterTimeout={500}
          transitionLeaveTimeout={500}
          transitionAppearTimeout={1000}
        >
          <JobCurationSharePopup
            AllData={jobData}
            shareFrom={this.props.shareFrom}
            comment={this.props.comment}
            accounts={this.props.accounts}
            onShareLinkClick={this.onShareLinkClick.bind(this)}
            //   onEditPostData = {this.onEditPostData.bind(this)}
            closeJobRssSharePopup={this.closeJobRssSharePopup.bind(this)}
          />
        </ReactCSSTransitionGroup>
      </div>
    )
  }
  /**
   * to close share popup
   */
  closeJobRssSharePopup () {
    this.setState({
      shareJobRssPopupState: false,
      shareJobRssPopupStateOfPostID: null
    })
  }
  render () {
    var id = this.props.shareFrom == 'jobs'
      ? this.props.currData.identity
      : this.props.currData.permalink
      var enableSocialFlag=this.props.profile.profile.enable_social_posting;
    return (
      (this.props.shareFrom == 'jobs' && enableSocialFlag == 1 ) || this.props.shareFrom == 'rss'?
      <div className='sharePopupWrapper'>
        <a onClick={this.onShareLinkClick.bind(this, id)} class='share-job' id={id}>
          <i class='material-icons'>share</i> <span>Share</span>
        </a>
          {
            this.state.shareJobRssPopupState ==true?
            this.renderPopup(this.props.currData) 
            :''
            }
      </div>
      :<div></div>
    )
  }
}
function mapStateToProps (state, ownProps) {
    return {
    profile: state.profile,
  }
}

function mapDispatchToProps (dispatch) {
  return {}
}
let connection = connect(mapStateToProps, mapDispatchToProps)
module.exports = connection(MergeJobsRssSharePopup)