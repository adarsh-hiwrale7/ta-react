import { Link } from "react-router";
import { browserHistory } from "react-router";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Field, reduxForm } from "redux-form";
import { renderField } from "../Helpers/ReduxForm/RenderField";
import MentionTextArea from "../Helpers/ReduxForm/MentionTextArea";
import CenterPopupWrapper from "../CenterPopupWrapper";
import * as postsActions from "../../actions/feed/postsActions";
import PreLoader from "../PreLoader";

var maxChar = 280;
const required = (value) => (value ? undefined : "Required");

class BroadcastPopup extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      charLeft: maxChar,
    };
  }

  /**
   * @author Kushagra
   * @param {e}
   * counts the length of character entered by user in message
   */

  textCount = (e) => {
    var input = e.target.value;
    this.setState({
      charLeft: maxChar - input.length,
    });
  };

  // form on submit and also prevent from refreshing on success
  formSubmit(event, val) {
    event.preventDefault();
    this.props.handleSubmit(val);
  }

  render() {
    return (
      <div>
        {this.props.broadcastLoading ? <PreLoader className="Page" /> : ""}
        <div className="CreateFeedPopupWrapper">
          <CenterPopupWrapper>
            <div className="planChangePopup CreateFeedPopupWrapperForFeed">
              <div className="customfeedMain ">
                <div className="createCustomfeedpopup broadcastpopup-wrapper">
                  <header className="heading clearfix">
                    <h3>Broadcast</h3>
                    <button
                      id="close-popup"
                      className="btn-default"
                      onClick={this.props.handleBroadcast(false)}
                    >
                      <i
                        className="material-icons"
                        //onClick={this.closeCreateFeedPopup.bind(this)}
                      >
                        clear
                      </i>
                    </button>
                  </header>
                  <form onSubmit={this.formSubmit.bind(this)}>
                    <div className="broadcast-detail">
                      <div className="broadcast-message-name">
                        <span>Message</span>
                        <Field
                          component={renderField}
                          type="text"
                          maxLength="280"
                          name="message"
                          onChange={this.textCount.bind(this)}
                          id="title"
                          placeholder="Type your message"
                          validate={[required]}
                          value="message"
                        />
                        <p>Character remaining: {this.state.charLeft}</p>
                      </div>

                      <div
                        className="widget createFeedDetails"
                        id="setting_walkthrough"
                      >
                        <div className="feedRadioWrapper">
                          <div className="radiobtnFeed clearfix">
                            <span className="radiobtnFeed-label">
                              Message Type
                            </span>
                            <label className="customradio">
                              SMS
                              <Field
                                component="input"
                                type="radio"
                                name="messageType"
                                id="feedType1"
                                value="sms"
                              />
                              <span className="checkmark"></span>
                            </label>
                            <label className="customradio">
                              PUSH
                              <Field
                                component="input"
                                type="radio"
                                name="messageType"
                                id="feedType2"
                                value="push"
                              />
                              <span className="checkmark"></span>
                            </label>
                            <label className="customradio">
                              BOTH
                              <Field
                                component="input"
                                type="radio"
                                name="messageType"
                                id="feedType3"
                                value="both"
                              />
                              <span className="checkmark"></span>
                            </label>
                          </div>
                        </div>

                        <div className="clearfix">
                          <button
                            className="btn right btn-primary"
                            type="submit"
                          >
                            Submit
                          </button>
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </CenterPopupWrapper>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state, ownProps) {
  return {};
}

function mapDispatchToProps(dispatch) {
  return {
    postsActions: bindActionCreators(postsActions, dispatch),
  };
}
let connection = connect(mapStateToProps, mapDispatchToProps);

var reduxConnectedComponent = connection(BroadcastPopup);

export default reduxForm({
  form: "BroadcastPopup", // a unique identifier for this form
})(reduxConnectedComponent);
