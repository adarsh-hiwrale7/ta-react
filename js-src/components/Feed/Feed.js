import { bindActionCreators } from 'redux';
import { browserHistory } from 'react-router';
import { connect } from 'react-redux'
import { Link } from 'react-router';

import * as assetsActions from '../../actions/assets/assetsActions'
import * as feedActions from '../../actions/feed/feedActions'
import * as pollActions from '../../actions/feed/PollActions'
import * as headerActions from '../../actions/header/headerActions';
import * as notificationActions from "../../actions/notificationActions"
import * as postsActions from '../../actions/feed/postsActions'
import * as socialAccountsActions from '../../actions/socialAccountsActions'
import * as tagsActions from "../../actions/tagsActions"
import * as searchActions from '../../actions/searchActions';
import * as utils from '../../utils/utils'
import * as userActions from '../../actions/userActions'
import reactDatepickerChunk from '../Chunks/ChunkDatePicker'
import $ from '../Chunks/ChunkJquery'
import reactTooltip from '../Chunks/ChunkReactTooltip';
import Asset from '../Assets/Asset'
import AssetDetail from '../Assets/AssetDetail'
import CollapsibleFilter from '../CollapsibleFilter'
import CreatePost from './CreatePost'
import ChunkJqueryDropdown from '../Chunks/ChunkJqueryDropdown';
import FeedNav from './FeedNav';
import FeedCalendar from './FeedCalendar';
import Globals from '../../Globals'
import Header from '../Header/Header';
import moment from '../Chunks/ChunkMoment'
import notie from 'notie/dist/notie.js'
import Post from './Post'
import PreLoader from '../PreLoader'
import PopupWrapper from '../PopupWrapper'
import ReactCSSTransitionGroup from 'react/lib/ReactCSSTransitionGroup'
import ReactDOM from 'react-dom';
import SliderPopup from '../SliderPopup'
import SearchNav from '../Search/SearchNav';
import TrendingTagsPanel from './TrendingTagsPanel';
import ChangeScheduleForm from './ChangeScheduleForm'
import TagPopup from './TagPopup'
import 'rc-collapse/assets/index.css'
import CreateFeedPopup from './CreateFeedPopup';
import UpdateFeedPopup from './UpdateFeedPopup';
import GuestUserRestrictionPopup from '../GuestUserRestrictionPopup';
import StaticDataForGuest from '../StaticDataForGuest'
import MergeJobs from './MergePages/MergeJobs';
import MergeRss from './MergePages/MergeRss';
import Slider from '../Chunks/ChunkReactSlick'
import PollQuestions from './Polls/PollQuestions'
import * as s3functions from '../../utils/s3'

// var wizard = require('react-guest-tutorial').Mixin
var tour_flag = false;
var calendarUrl = '';
var feedUrl = '';
var editPostData = null;
var postData = null;
var filterFlag = false
var customTextFlag = '';
var propsChanged = true;
var writePostTitle = '';
var writePostType = '';
var feedRestrict = false;
var feedrestrictSocial = false;
var editCreatePostOpenPopup = false;
var scrollPost = false;
var scrollPost2 = false;
var postscroll = false;
var globalJobsCount;
var curationIndex;
var jobIndex;
var tempJobIndex;
var tempCurationIndex;
var tempCurationPage;
var globalCurationCount;
var jobcounts = [];
var jobpageFlag = false;
var curationCounts = [];
var scrollFlag;
var selectedUserData;
var selectedDepartmentData;
var isFilterAppiled = false;
var focusedPoll = false;
var callPostApiFromProfile = false ; // true
class Feed extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      currFeed: 'default',
      currFeedPosts: 'live',
      currPage: 1,
      postCreationCampaignID: null,
      postCreationCampaignTitle: null,
      newPostPopup: false,
      postCreationCampaignChannels: [],
      moment: null,
      currDateTitle: null,
      limit: 10,
      scrollTimer: null,
      jQuery: null,
      tagType: 'user',
      tagTypeClass: '',
      assetDetailPopup: false,
      currentAssetDetail: null,
      overlay_flag: false,
      tour_flag: false,
      show_menu: true,
      tooltip: null,
      newTrendingPostPopup: false,
      currentFilter: 'default/live',
      postFromCampaign: false,
      urlHashTag: null,
      changeScheduleFeed: null,
      changeScheduleFeedType: null,
      changeSchedulePostData: [],
      changeSchedulePopup: false,
      ReactDatePicker: null,
      view: 'list',
      flag_state: false,
      openPostPopup: false,
      createFeed: false,
      userTag: [],
      currentFeed: '',
      openEditFeedPopup: false,
      editFeedId: null,
      Slider: null,
      surveyPopup: false,
      editCurationData : null
    }
    this.onPostsScroll = this
      .onPostsScroll
      .bind(this); // bind function once
    this._handleCloseEvent = this
      ._handleCloseEvent
      .bind(this);
    this.hashChange = this
      .hashChange
      .bind(this);
  }


  hashChange() {
    document.addEventListener('scroll', this.onPostsScroll)

    let me = this
    me.openingPostCreationFromCampaign()

    // moment().then(moment => {
    //   this.setState({moment: moment})
    // })

    var path = utils
      .removeTrailingSlash(this.props.location.pathname)
      .split('/')
    this
      .props
      .feedActions
      .resetPosts()
    if (typeof path[2] === 'undefined' || path[2] == '' || typeof path[3] === 'undefined' || path[3] == '') {
      browserHistory.push('/feed/default/live')
    } else {
      this.setState({ currFeed: path[2], currFeedPosts: path[3], currPage: 1 })

      if (path[2] == "my") {
        this.fetchPosts(path[2], path[3], 1)
      }
      else {
        this.fetchPosts(path[2], path[3], 1)
      }
    }
    calendarUrl = `/feed/${path[2]}/calendar/${path[3]}`
    feedUrl = `/feed/${path[2]}/${path[3]}`
  }

  componentDidMount() {
    // reset the flag if feed is calling after other nav menu ( click on any nav menu except Feed and come back to feed page )
    callPostApiFromProfile = false
    // this part is to open a feedupdate popup from rss page or job page
    window.scrollTo(0, 0);
    if (this.props.location.state !== null && this.props.location.state.fromRssJob == true) {
      if (this.props.location.state.feedId !== null) {
        this.updateFeedOpen(this.props.location.state.feedId);
      }
    }
    isFilterAppiled = false;
    this.props.headerActions.setFilterFlag(true);
    //i put this condition like this bcz if page is changed (feed page then went on setting and then again back to feed page) then post fetch api (from fetchpost method )called twice and due to this the data displayed twice
    //fetchpost method call due to this flag is true and again method will call after fetching feed list data
    if (this.props.feed.feedList && this.props.feed.feedList.length == 0) {
      postscroll = true;
    }

    propsChanged = true;
    if (feedrestrictSocial !== true) {
      window.addEventListener("hashchange", this.hashChange, false);
      let me = this
      document.addEventListener('scroll', this.onPostsScroll)
      var newLocation = utils
        .removeTrailingSlash(this.props.location.pathname)
        .split('/')
      var location = newLocation[4] !== undefined
        ? newLocation[4]
        : newLocation[4]

      this.openingPostCreationFromCampaign()
      $().then(jquery => {
        var $ = jquery.$
        me.setState({ jQuery: $ })
        ChunkJqueryDropdown().then(jqueryDropdown => { })
      })
      ReactDOM
        .findDOMNode(this)
        .addEventListener('click', this._handleCloseEvent);
      this.props.socialAccountsActions.fetchSocialAccounts()
      // commented  to reduce multiple api call for trending tag
      // this.props.tagsActions.fetchTrendingTags(newLocation);
      // this.props.tagsActions.fetchUserTags(newLocation);
      feedrestrictSocial = true;
      setTimeout(() => {
        feedrestrictSocial = false
      }, 5000)
    }
  }
  _handleCloseEvent(e) {
    if (document.getElementById('pop-up-tooltip-holder')) {
      if (!document.getElementById('pop-up-tooltip-holder').contains(e.target)) {
        if (this.state.overlay_flag) {
          document
            .getElementById('pop-up-tooltip-holder')
            .classList.add('hide');
          this.setState({ overlay_flag: false })
        }
      }
    }
  }
  closepopup() {

    let popup_wrapper = document.getElementById("pop-up-tooltip-holder");
    popup_wrapper.classList.add('hide');
  }


  changeScheduleAction(self, feed, feedType, postData) {
    this.props.feedActions.getAllSchedules(postData.post.data.identity)
    this.setState({
      changeSchedulePopup: true,
      changeScheduleFeed: feed,
      changeScheduleFeedType: feedType,
      changeSchedulePostData: postData
    })
    document.body.classList.add('overlay');
  }

  closechangeSchedulePopup() {
    this.setState({
      changeSchedulePopup: false,
    })
    document.body.classList.remove('overlay');
  }

  /**
  * Open survey popup question when user sees first time and it got focus
  * @author Yamin
  **/
  openPollQuestionPopup(open) {
    this.setState({
      surveyPopup: open
    })
    //if popup is close then update seen status in api
    if (!open) {
      this.props.pollActions.updatePollSeenStatus(this.props.polls.pollQuestions);
      document.body.classList.remove('pollPopup')
    } else {
      document.body.classList.add('pollPopup')
    }
  }

  PollQuestionPopup() {
    return (
      <PollQuestions
        pollsData={this.props.polls}
        openPopup='yes'
        closePollQuestionPopup={this.openPollQuestionPopup.bind(this, false)}
        openPollQuestionPopup={this.openPollQuestionPopup.bind(this, true)}
        handleSurveySave={this.handleSurveySave.bind(this)}
        fetchSurvey={this.fetchSurveyPollQuestion.bind(this)}
      />
    )
  }

  fetchSurveyPollQuestion() {
    this.setState({
      surveyPopup: false
    })
    var timeZone = this.state.moment.tz ? this.state.moment.tz.guess() : null;
    this.props.pollActions.fetchSurveyPollQuestion(timeZone);
  }

  /**
  * save user survery result
  * @author Yamin
  **/
  handleSurveySave(data, questionId) {
    this.props.pollActions.saveSurveyResult(data, questionId)
  }


  /**
   * @author disha
   * When click on save button to pass schedule data into Api
   * @param {*} values form values from changeScheduleForm
   */
  handleScheduleSubmit(values) {
    let schedulers = [];
    let output = [];
    var counts = {};
    let currentTime = this.state.Moment().format("HH:mm");
    let currentDate = this.state.Moment().format("DD/MM/YYYY");


    for (let sc = 0; sc < values.allScheduleData.length; sc++) {

      let time = this.state.Moment.unix(values.allScheduleData[sc].scheduled_at).format("HH:mm");
      let date = this.state.Moment.unix(values.allScheduleData[sc].scheduled_at).format("DD/MM/YYYY");

      if (values[`scheduledTime-${sc}`] !== undefined && values[`scheduledTime-${sc}`] !== null) {
        //new schedule is added in list then to get its time , if schedule is removed then it will not come here
        time = this.state.Moment(values[`scheduledTime-${sc}`]).format("HH:mm");
      }
      if (values[`scheduledDate-${sc}`] !== undefined && values[`scheduledDate-${sc}`] !== null) {
        //new schedule is added in list then to get its date , if schedule is removed then it will not come here
        date = this.state.Moment.unix(values[`scheduledDate-${sc}`]).format("DD/MM/YYYY");
      }

      let concatenatedDateTime = date + " " + time;
      let timeStamp = this.state.Moment(concatenatedDateTime, 'DD/MM/YYYY HH:mm').format("X");

      schedulers.forEach(function (x) {
        counts[x] = (counts[x] || 0) + 1;
      });

      if (currentDate == date) {
        //if current date is match with scheduled date and current time is matched with schedule time then to resrtict post
        if (currentTime > time) {
          notie.alert('error', 'Sorry, Your scheduled time is left.', 5)
          return;
        }
      }

      if (!schedulers.includes(parseInt(timeStamp))) {
        //to restrict pervious date
        var scheduler_date = this.state.Moment.unix(parseInt(timeStamp)).format('MM-DD-YYYY');
        var today = this.state.Moment().unix()
        var today_date = this.state.Moment.unix(today).format('MM-DD-YYYY');
        var date1 = new Date(scheduler_date.toString());
        var date2 = new Date(today_date.toString());
        var timeDiff = date2.getTime() - date1.getTime();
        var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
        if (diffDays > 0) {
          notie.alert('error', 'You can not schedule a post in past. Please select a future time.', 5)
          return;
        } else {
          schedulers.push(parseInt(timeStamp))
        }
      } else {
        notie.alert('error', 'You can not select same date for schedule.', 5)
        return;
      }
    }
    var field_values_array = [];
    for (let sc = 0; sc < schedulers.length; sc++) {
      if (values.allScheduleData[sc].SocialAccount !== undefined) {
        values.allScheduleData[sc].SocialAccount.data.forEach(function (event_post) {
          if (event_post.length !== 0) {
            var field_array_object = {
              social_account_identity: event_post.SocialAccount.data.identity,
              scheduled_at: schedulers[sc]
            }
          } else {
            var field_array_object = {
              social_account_identity: 0,
              scheduled_at: schedulers[sc]
            }
          }
          field_values_array.push(field_array_object)
        })
      } else {
        var field_array_object = {
          social_account_identity: 0,
          scheduled_at: schedulers[sc]
        }
        field_values_array.push(field_array_object)
      }
    }
    let objToSend = {
      post_identity: values.postid,
      schedule: field_values_array
    }
    this.props.postsActions.postUpdate(objToSend, false)
  }

  openCreatePostEditCuration(data){
    this.setState({
      newPostPopup: true,
      CurationEditOpenPopup:true,
      editCurationData:data
    })
  }

  //openCreatePopup method ,set the data into the variable and ,if  newPostPopup is true then open the popup of newpost
  openCreatePopup(post) {
    editPostData = post;
    editCreatePostOpenPopup = true
    this.setState({
      newPostPopup: true,
    })
  }
  renderChangeSchedule() {
    return (
      <SliderPopup wide>
        <button
          id='close-popup'
          className='btn-default'
          onClick={this.closechangeSchedulePopup.bind(this)}
        >
          <i className='material-icons'>clear</i>
        </button>
        <header className='heading'>
          <h3>Reschedule post</h3>
        </header>
        <PopupWrapper>
          <div className='schedulePostPopupColumnWrapper clearfix'>
            <div className='PostDetailColumn'>
              <Post
                feed={this.state.changeScheduleFeed}
                feedType={this.state.changeScheduleFeedType}
                details={this.state.changeSchedulePostData}
                updateText={this.updateText.bind(this)}
                key={0}
                Slider={this.state.Slider} 
              />
            </div>
            <div className='postScheduleData'>
              <ChangeScheduleForm
                props={this.props}
                postId={this.state.changeSchedulePostData.post_id}
                scheduleDates={this.props.feed.scheduleDates}
                onSubmit={this.handleScheduleSubmit.bind(this)}
                momentVar={this.state.moment}
              />

            </div>
          </div>
        </PopupWrapper>
      </SliderPopup>
    )
  }

  updateText(id, type, e) {
    this.setState({ overlay_flag: true })
    var isUserFound = false;
    if (type == "department") {
      this.props.departments.list.length > 0 ?
        this.props.departments.list.map((DepartmentDetail, i) => {
          if (DepartmentDetail.identity == id) {
            selectedDepartmentData = DepartmentDetail;
          }
        }) : ''
    }
    else {
      this.props.usersList !== undefined ? 
      this.props.usersList.length > 0 ?
        this.props.usersList.map((userDetail, i) => {
          if (userDetail.identity == id) {
            selectedUserData = userDetail;
            isUserFound = true;
          }
        }) : '':''
      if (isUserFound == false) {
        selectedUserData = "No user found"
      }
    }
    const domNode = ReactDOM.findDOMNode(e.target);
    let coords = domNode.getBoundingClientRect();
    let coords_top = coords.top + pageYOffset;
    let popup_wrapper = document.getElementById("pop-up-tooltip-holder");
    popup_wrapper.classList.remove('hide', 'showtotop');

    let coords_left = 0,
      coords_area = 0

    if (screen.width >= 768) {
      coords_left = coords.left + pageXOffset + (coords.width / 2);
    } else {
      coords_left = coords.left + pageXOffset;
      coords_area = coords_left + 472;
      if (coords_area < screen.width) {
        popup_wrapper.classList.add('popup-left');
      } else {
        popup_wrapper.classList.add('popup-right');
      }

    }
    if (e.nativeEvent.screenY < 400) {
      if (e.target.className == "thumbnail") {
        popup_wrapper.classList.add('thumbnai_popup');
      }
    } else {
      popup_wrapper.classList.add('showtotop')
    }

    this.setState({
      popupTop: coords_top,
      popupLeft: coords_left,
      popupdisplay: 'block',
      tagType: type,
      tagTypeClass: (type === "department"
        ? 'departmentpopup'
        : '')
    });
  }

  /**
   * @author disha
   * @use for open create post popup from campaign
   */
  openingPostCreationFromCampaign() {
    if (typeof this.props.location.query.createpost !== 'undefined' || typeof this.props.location.query.searchcreatepost !== 'undefined') {
      this.setState({
        postFromCampaign: true
      })
      document.body.classList.remove('overlay')
      this.handlePostCreation()
      this.setState({
        postCreationCampaignID: this.props.posts.campaignCreatePostdata.campaign_id,
        postCreationCampaignTitle: this.props.posts.campaignCreatePostdata.campaign_title,
      })
    } else {
      document
        .body
        .classList
        .remove('overlay_responsive_trending')
      this.setState({
        newPostPopup: false,
        postCreationCampaignID: null,
        postCreationCampaignTitle: null,
      })
    }
  }

  onPostsScroll() {
    //this method used for the write post set onto the top side
    // this offset is for  identify the loader at end 
    var loaderOffset = document.getElementById("feed-loader") ? document.getElementById("feed-loader").offsetTop : null;
    /* Start: open poll popup when get focus in scroll */
    let roleName = JSON.parse(localStorage.getItem('roles')) !== null ? JSON.parse(localStorage.getItem('roles'))[0].role_name : ''

    if (roleName !== 'guest' && typeof this.props.polls.pollQuestions.identity !== "undefined" && this.props.polls.pollQuestions.seen === 0 && document.getElementById("survey-poll-question") !== null) {
      var pollOffset = document.getElementById("survey-poll-question") ? document.getElementById("survey-poll-question").offsetTop : null;
      var pollSurveyHeight = document.getElementById("survey-poll-question").offsetHeight;

      if (utils.getScrollXY()[1] + window.innerHeight >= pollOffset + pollSurveyHeight && !focusedPoll && Object.keys(this.props.polls.pollQuestions).length > 0) {
        focusedPoll = true;
        this.openPollQuestionPopup(true);
      }
    }
    /*End*/

    var me = this
    if (loaderOffset !== null && utils.getScrollXY()[1] + window.innerHeight >= loaderOffset + 400 && !this.props.feed.default.live.isPin && !scrollPost2 && !scrollPost && scrollFlag !== true) {
      // a small debouncer for better performance
      clearTimeout(me.state.scrollTimer)
      let scrollTimer = setTimeout(function () {
        if (typeof me.props.feed.meta.pagination !== 'undefined') {
          var nextpage = me.props.feed.meta.pagination.current_page + 1
          if (utils.getDocHeight() == 348) {
            var nextpage = 1;
          }
          me.setState({ currPage: nextpage })
          // me
          //   .props
          //   .feedActions
          //   .userScrollLoader()
          if (me.props.location.hash !== '' && me.props.feed.meta.pagination.total_pages !== 1) {
            me.fetchPosts(me.state.currFeed, me.state.currFeedPosts, nextpage, null, true, me.props.feed.lastFeedType)
          }
          else {
            if (me.props.feed.meta.pagination.total_pages !== 1) {
              if (me.state.selectedFeedStatus !== undefined && me.state.selectedFeedStatus !== '' && me.state.selectedFeedStatus !== null) {
                // me.props.feedActions.filterMyFeedData(me.state.currFeed, me.state.currFeedPosts,me.state.selectedFeedStatus,nextpage,10);
                me.props.feedActions.filterMyFeedData(me.state.currFeed, me.state.currFeedPosts, me.state.selectedFeedStatus, nextpage, 10, false, false, null, null, true);
              }
              else {
                // me.fetchPosts(me.state.currFeed, me.state.currFeedPosts, nextpage)
                me.fetchPosts(me.state.currFeed, me.state.currFeedPosts, nextpage, null, true, me.props.feed)
              }
            }
          }

        }
      }, 300)

      me.setState({ scrollTimer: scrollTimer })
    }
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    
    if (prevState.moment !== this.state.moment && this.state.moment !== null) {
      var timeZone = this.state.moment.tz ? this.state.moment.tz.guess() : null;
      this.props.pollActions.fetchSurveyPollQuestion(timeZone);
    }
  }
  componentWillMount() {
    // if user is just login and page is not refreshed then fetch default feed id  from auth props and call post api
    let defaultFeedId = this.props.auth.userinfo !== undefined ? this.props.auth.userinfo.default_feed:''
    if(defaultFeedId !== ''){
      Globals.defaultFeedId = this.props.auth.userinfo.default_feed
      var newLocation = utils
      .removeTrailingSlash(this.props.location.pathname)
      .split('/')
      const feedData = this.props.auth.userinfo.feed
      callPostApiFromProfile = true // indicate that post api is calling through auth or profile without any dependancy (willmount or willrecvprops)
      this.fetchPosts(newLocation[2], newLocation[3], 1, null, false, feedData)
    }
    let me = this
    me.openingPostCreationFromCampaign()

    moment().then(moment => {
      Slider().then(slider => {
        reactTooltip().then(reactTooltip => {
          this.setState({  moment: moment, Slider: slider , tooltip: reactTooltip })
      })
      })
    })
    window.scrollTo(0, 0);
    var path = utils.removeTrailingSlash(this.props.location.pathname).split('/')
    if (feedRestrict !== true) {
      this.props.feedActions.resetPosts()
      if (typeof path[2] === 'undefined' || path[2] == '' || typeof path[3] === 'undefined' || path[3] == '') {
        if (path[2] == "custom") {
          browserHistory.push('/feed/custom/live')
        }
        else {
          browserHistory.push('/feed/default/live')
        }

      }
    }
    calendarUrl = `/feed/${path[2]}/calendar/${path[3]}`
    feedUrl = `/feed/${path[2]}/${path[3]}`

  }
  componentWillUnmount() {
    this
      .props
      .feedActions
      .resetPosts();
    window.scrollTo(0,0);
    document.removeEventListener('scroll', this.onPostsScroll)
    window.removeEventListener("hashchange", this.hashChange, false);
    ReactDOM
      .findDOMNode(this)
      .removeEventListener('click', this._handleCloseEvent);
      document.body.classList.remove('FeedPage');
  }

  fetchPosts(type, postTypes, currPage, newProps = null, onPostsScroll = false, feedData = null) {
    scrollFlag = true
    var feedId;
    var lastFeedType, jobcount, feedList, is_configured, isJobfinished, curationcount;
    if (feedData !== null) {
      lastFeedType = feedData.lastFeedType;
      jobcount = feedData.jobcount;
      curationcount = feedData.curationcount;
      feedList = feedData.feedList !== undefined ? feedData.feedList : feedData.data;
      is_configured = feedData.is_configured,
      isJobfinished = feedData.isJobfinished
    }
    var path = newProps ? utils.removeTrailingSlash(newProps.location.pathname).split('/') : utils.removeTrailingSlash(this.props.location.pathname).split('/')
    let hash = newProps ? newProps.location.hash : this.props.location.hash
    switch (type) {
      case 'my':
        if (postTypes == "live") {
          if (hash) {
            if (this.state.selectedFeedStatus !== undefined) {
              this.
                props
                .feedActions
                .filterMyFeedData(path[2], path[3], this.state.selectedFeedStatus, currPage, 10, false, false, null, null, false, hash.substr(1));
            } else {
              this
                .props
                .feedActions
                .fetchLiveHash(hash.substr(1), currPage, this.state.limit, onPostsScroll)
            }
          } else {
            if (this.state.selectedFeedStatus !== undefined) {
              this.
                props
                .feedActions
                .filterMyFeedData(path[2], path[3], this.state.selectedFeedStatus, currPage, 10, false, false, null, null, false, hash.substr(1));
            }
            else {
              this
                .props
                .feedActions
                .fetchLive(currPage, this.state.limit, false, false, type, null, onPostsScroll)
            }
          }
        }
        else if(postTypes=="unapproved" || postTypes=="failed" ){
          this
            .props
            .feedActions
            .fetchMyUnapproved(currPage, this.state.limit, type,onPostsScroll)

        }
        else if(postTypes=="pending"){
          this
            .props
            .feedActions
            .fetchMyPending(currPage, this.state.limit, type,onPostsScroll)

        }
        else {
          this
            .props
            .feedActions
            .fetchScheduled(currPage, this.state.limit, false, false, type)
        }
        break;
      case 'default':
        var curation_display, job_display;
        curation_display = feedData.feedList !== undefined ? curation_display : feedData.data.curation_display;
        job_display = feedData.feedList !== undefined ? job_display : feedData.data.job_display;
        feedList !== undefined && feedList.length > 0 ? feedList.map((feedData, index) => {
          if (feedData.identity == Globals.defaultFeedId) {
            job_display = feedData.job_display;
            curation_display = feedData.curation_display;
          }
        }) : ''
        switch (postTypes) {
          case 'live':
            if (hash) {
              var lastPostIdReferer = null
              if (currPage !== 1) {
                lastPostIdReferer = this.props.feed.lastPostId
              }
              this
                .props
                .feedActions
                .fetchDefaultLiveHash(hash.substr(1), currPage, this.state.limit, onPostsScroll, lastPostIdReferer)
            }
            else {
              if (this.props.searchedData) {
                if (this.props.location.state !== null && this.props.location.state.isenterSubmit == true && currPage == 1) {
                  this
                    .props
                    .feedActions
                    .fetchDefaultLive(currPage, this.state.limit, false, false, null, this.props.searchedData)
                }

                if (currPage !== 1) {
                  this
                    .props
                    .feedActions
                    .fetchDefaultLive(currPage, this.state.limit, false, false, null, this.props.searchedData, this.props.feed.lastPostId)
                }

              }
              else {
                var lastPostIdReferer = null
                if (currPage !== 1) {
                  lastPostIdReferer = this.props.feed.lastPostId
                }
                this
                  .props
                  .feedActions
                  .fetchDefaultLive(currPage, this.state.limit, false, false, null, null, lastPostIdReferer, lastFeedType, jobcount, job_display, curation_display, is_configured, isJobfinished, curationcount)
              }
            }
            break
          default:
            if (hash) {
              var lastPostIdReferer = null
              if (currPage !== 1) {
                lastPostIdReferer = this.props.feed.lastPostId
              }
              this
                .props
                .feedActions
                .fetchDefaultLiveHash(hash.substr(1), currPage, this.state.limit, onPostsScroll, lastPostIdReferer)
            } else {
              var lastPostIdReferer = null
              if (currPage !== 1) {
                lastPostIdReferer = this.props.feed.lastPostId
              }
              this
                .props
                .feedActions
                .fetchDefaultLive(currPage, this.state.limit, false, false, null, null, lastPostIdReferer, lastFeedType, jobcount, job_display, curation_display, is_configured, isJobfinished,curationcount)
            }
        }
        break
      case 'custom':
        var curation_display, job_display;
        feedList ? Object.keys(feedList).length > 0 ? feedList.map((feedData, index) => {
          if (feedData.identity == path[4]) {
            job_display = feedData.job_display;
            curation_display = feedData.curation_display;
          }
        }) : '' : ''
        switch (postTypes) {

          case 'live':
            if (hash) {
              var lastPostIdReferer = null
              if (currPage !== 1) {
                lastPostIdReferer = this.props.feed.lastPostId
              }
              this
                .props
                .feedActions
                .fetchCustomLiveHash(hash.substr(1), path[4], currPage, this.state.limit, onPostsScroll, lastPostIdReferer)
            }
            else {
              var lastPostIdReferer = null
              if (currPage !== 1) {
                lastPostIdReferer = this.props.feed.lastPostId
              }
              this
                .props
                .feedActions
                .fetchCustomLive(path[4], currPage, this.state.limit, false, false, lastPostIdReferer, lastFeedType, jobcount, job_display, curation_display, is_configured, isJobfinished,curationcount)
            }
            break
          default:
            var lastPostIdReferer = null
            if (currPage !== 1) {
              lastPostIdReferer = this.props.feed.lastPostId
            }
            this
              .props
              .feedActions
              .fetchCustomLive(path[4], currPage, this.state.limit, false, false, lastPostIdReferer, lastFeedType, jobcount, job_display, curation_display, is_configured, isJobfinished,curationcount)

        }
        break
      case 'assets':
        switch (postTypes) {
          case 'hash':
            this
              .props
              .assetsActions
              .fetchAssetsByHash(hash.substr(1));
            break
        }
      default:
        switch (postTypes) {
          case 'live':
            var lastPostIdReferer = null
            if (currPage !== 1) {
              lastPostIdReferer = this.props.feed.lastPostId
            }
            this
              .props
              .feedActions
              .fetchDefaultLive(currPage, this.state.limit, false, false, null, null, lastPostIdReferer)
            break
        }
    }
  }

  setHeader(location) {
    this.setState({
      currentFilter: location
    })
  }
  /**
   * @author Sadikali
   * @fetch current feed data from feed list
   */

  setCurrentFeed(newProps) {
    var customTextArray = newProps.location.pathname.split("/");
    customTextFlag = customTextArray[2];
    writePostTitle = customTextFlag
    var feedArray = newProps.feed.feedList;
    var currentFeedIdentity = customTextArray[4];
    let roleName = JSON.parse(localStorage.getItem('roles')) !== null ? JSON.parse(localStorage.getItem('roles'))[0].role_name : ''
    if (customTextFlag == 'my') {
      //state will be undefined if filter is not selected
      if (this.state.selectedFeedStatus == undefined) {
        //to set first feed id of guest in my live post 
        if (roleName == 'guest') {
          currentFeedIdentity = feedArray[1].identity
        }
      } else {
        //if feed is creating from my page and feed type is changed from filter then to store its id in variable
        currentFeedIdentity = this.state.selectedFeedStatus
      }
    }
    if (customTextFlag == 'custom' || customTextFlag == 'my') {
      var feedCount;
      for (feedCount = 0; feedCount < feedArray.length; feedCount++) {
        if (feedArray[feedCount].identity == currentFeedIdentity && this.state.currentFeed !== currentFeedIdentity) {
          this.setState({
            currentFeed: feedArray[feedCount]
          })
          writePostTitle = feedArray[feedCount]
          break;
        }
      }
    }
    else {
      writePostTitle = ''
      this.setState({
        currentFeed: ""
      })
    }
    propsChanged = false;
  }

  componentWillReceiveProps(newProps) {
    //reset the flag if location is changed ( custom feed to default feed / default feed to custom / my feed to custom  etc...) so that post api can call as per the below conditions
    if(this.props.location !== newProps.location){
      callPostApiFromProfile = false
    }
    let roleName = JSON.parse(localStorage.getItem('roles')) !== null ? JSON.parse(localStorage.getItem('roles'))[0].role_name : ''
    var isexit = true;
    var newLocation = utils
      .removeTrailingSlash(newProps.location.pathname)
      .split('/')
    moment().then(moment => {
      reactDatepickerChunk().then(reactDatepicker => {
        this.setState({ ReactDatePicker: reactDatepicker, Moment: moment })
      }).catch(err => {
        throw err
      })
    })
    
    //to redirect back to feed/default.live page if location is only /feed (when click on browser's back button and previous page was '/feed' then to redirect back to default feed as api and all was not calling)
    if (newLocation[2] == undefined) {
      browserHistory.push(`/feed/default/live`)
    }

    //fetch default feed id from profile api and save it to as global variable so that api can call according to that feed id
    if (newProps.profile.profile !== this.props.profile.profile && newProps.profile.profile.email !== null && this.props.auth.userinfo == undefined) {

      callPostApiFromProfile = true //indicate that post api is calling through auth or profile without any dependancy (willmount or willrecvprops)
      Globals.defaultFeedId = newProps.profile.profile.default_feed != '' ?newProps.profile.profile.default_feed:Globals.defaultFeedId
      const feedData = newProps.profile.profile.feed !== undefined ? newProps.profile.profile.feed  :null
      this.fetchPosts(newLocation[2], newLocation[3], 1, null, false, feedData)
    }
    if (newProps.posts.editCurationData !== this.props.posts.editCurationData && newProps.posts.editCurationData !== null) {
      this.openCreatePostEditCuration(newProps.posts.editCurationData);
    }

    if (newProps.feed.feedLoading == false && newProps.feed.feedLoading !== this.props.feed.feedLoading) {
      scrollFlag = false
    }
    
    var me = this
    if (newProps.feed.feedList !== undefined && (newProps.location.pathname !== this.props.location.pathname || postscroll == true || newProps.feed.feedList !== this.props.feed.feedList) && Object.keys(newProps.feed.feedList).length !== 0) {

      if(newProps.feed.feedList !== undefined && Object.keys(newProps.feed.feedList).length > 0 ){
          newProps.feed.feedList.map((feedData, index) => {
            if (feedData.identity == newLocation[4]) {
              isexit = false
            }
        })
      }

      if (isexit == true && newProps.feed.feedList !== undefined && newLocation[2] == "custom" && newProps.feed.feedList !== this.props.feed.feedList) {
        if (newProps.feed.feedList[1] !== undefined && roleName == "guest") {
          browserHistory.push(`/feed/custom/live/${newProps.feed.feedList[1].identity}`)
        } else {
          browserHistory.push(`/feed/default/live`)
        }
      }

      scrollPost2 = true;
      postscroll = false;
      window.scrollTo(0,0);
      propsChanged = true;
      this.props.tagsActions.fetchUserTags(newLocation);
      this.props.tagsActions.fetchTrendingTags(newLocation);
      me.setState({
        currFeed: newLocation[2], currFeedPosts: newLocation[3], currPage: 1 // user came for the first time, so reset currPage to 1
      })
      this
        .props
        .feedActions
        .resetPosts()
      if (newLocation[2] == "my") {
        if (this.state.selectedFeedStatus !== undefined && this.state.selectedFeedStatus !== null && this.state.selectedFeedStatus !== '') {
          this.props.feedActions.filterMyFeedData(newLocation[2], newLocation[3], this.state.selectedFeedStatus, 1, 10);
        }
        else {
          // if callPostApiFromProfile is true means post api is called from willmount or willrecv props 
          // to remove dupilicated call of api  ( if post api is called from willmount or willrcv proper then we don't need to call belove api to fetch data)
          callPostApiFromProfile == false ?
          this.fetchPosts(newLocation[2], newLocation[3], 1) :''
        }
      }
      else if (newLocation[2] == "custom") {
        if (newLocation[4] == undefined) {
          callPostApiFromProfile == false ?
          this.fetchPosts(newLocation[2], newLocation[3], 1, newProps, false, newProps.feed):''
        }
        else {
          callPostApiFromProfile == false ?
          this.fetchPosts(newLocation[2], newLocation[3], 1, newProps, false, newProps.feed):''
        }

      }
      else {
        var roles = JSON.parse(localStorage.getItem('roles')) !== null ? JSON.parse(localStorage.getItem('roles'))[0].role_name : ''
        if (roles !== undefined && roles !== "guest") {
          callPostApiFromProfile == false ?
          this.fetchPosts(newLocation[2], newLocation[3], 1, null, false, newProps.feed):''
        }
      }
      this.renderPosts()
      calendarUrl = `/feed/${newLocation[2]}/calendar/${newLocation[3]}`
      feedUrl = `/feed/${newLocation[2]}/${newLocation[3]}`

      //this.handleTourStart()
      this.setState({
        view: 'list'
      });
    }
    else {
      newProps.feed.meta.pagination ? newProps.feed.meta.pagination.current_page == 1 ?
        scrollPost2 = false : '' : ''
    }
    if (newProps.feed.feedList !== undefined && Object.keys(newProps.feed.feedList).length > 0) {
      if (propsChanged || newProps.location.pathname !== this.props.location.pathname || this.props.feed.feedList !== newProps.feed.feedList) {
        this.setCurrentFeed(newProps);
      }
    }
    if (newProps.location.hash !== null && newProps.location.hash !== '' && newProps.location.hash !== this.props.location.hash) {
      scrollPost = true;
      this
        .props
        .feedActions
        .resetPosts()
      //if hash is not null then to call api
      if (newLocation[2] == "my") {
        callPostApiFromProfile == false ?
        this.fetchPosts(newLocation[2], newLocation[3], 1, newProps):''
      }
      else {
        callPostApiFromProfile == false ?
        this.fetchPosts(newLocation[2], newLocation[3], 1, newProps):''
      }
    } else {
      newProps.feed.meta.pagination ? newProps.feed.meta.pagination.current_page == 1 ?
        scrollPost = false : '' : ''
    }
    if (this.props.posts.creation.disblePopup !== newProps.posts.creation.disblePopup && newProps.posts.creation.disblePopup) {
      if(this.state.editCurationData){
        window.scrollTo(0,0);
      }
      this.closePostCreatePopup();
    }
    if (this.props.postData !== undefined) {
      postData = this.props.postData
    }
    this.hide_add_new_popup();
    if (newProps.location !== this.props.location) {
      this.closeUpdateFeedPopup();
    }

    if (newProps.feed.feedCreated !== this.props.feed.feedCreated && newProps.feed.feedCreated === true) {
      this.setState({ createFeed: false })
    }

    //to close popup of change schedule form
    if (newProps.posts.updation.updated !== this.props.posts.updation.updated && newProps.posts.updation.updated !== false) {
      this.closechangeSchedulePopup()
    }

  }
  renderAssetByHash() {
    const selectedFeed = this.state.currFeed
    const selectedFeedPosts = this.state.currFeedPosts
    const hashTag = this.props.location.hash;
    return (
      <ul className='parent  clearfix'>
        <li>
          <Link
            to={`/feed/assets/hash${hashTag}`}
            className={selectedFeed == 'assets'
              ? 'active'
              : ''}>
            <span className='text'>Assets</span>
          </Link>
        </li>

      </ul>
    )
  }

  scheduledPosts({ posts, postsByType, feed, feedType }) {
    let me = this
    return (
      <div id='post-list-wrapper'>
        {
          typeof posts !== "undefined" ?
            posts.map((post, index) => {
              if (me.state.moment != null && typeof me.state.moment.tz !== 'undefined' && typeof post !== 'undefined' && typeof postsByType.scheduled[index] !== 'undefined') {
                let runningDateVar = me.state.moment.unix(post.scheduled_at).format('DD-MM-YYYY')
                let prev_date = 0
                if (index > 0) {
                  // get previous date so avoiding repetition of date header/title
                  prev_date = me
                    .state
                    .moment
                    .unix(postsByType.scheduled[index - 1].scheduled_at)
                    .format('DD-MM-YYYY')
                }
                return (
                  <div className="feedPostItem" key={index}>
                    {runningDateVar !== prev_date || index == 0
                      ? <h3 class='title-post-date'>
                        {me
                          .state
                          .moment
                          .unix(post.scheduled_at)
                          .format('dddd, Do MMMM YYYY')}
                      </h3>
                      : null}

                    <Post
                      Slider={this.state.Slider} 
                      key={index}
                      index={index}
                      feed={feed}
                      feedType={feedType}
                      locationInfo={me.props.location}
                      details={post}
                      tooltipid={`one` + index}
                      handleCloseEvent={this
                        ._handleCloseEvent
                        .bind()}
                      updateText={this
                        .updateText
                        .bind(this)}
                      feedData={this.props.feed}
                      searchInput={this.props.searchedData}
                      changeScheduleAction={(feed, feedType, PostData) => this.changeScheduleAction(this, feed, feedType, PostData)}
                    />


                  </div>
                )
              } else {
                return <div />
              }
            })
            : ''
        }

      </div>
    )
  }
  jobPagination(globalJobsCount, postLength) {
    var firstIndex = globalJobsCount * 10 - 10 + 1;
    var lastIndex = globalJobsCount * 10 - 1;
    var randomjobindex = Math.floor(Math.random() * (lastIndex - firstIndex + 1)) + firstIndex;

    if (jobcounts[globalJobsCount - 1] == undefined) {
      jobcounts[globalJobsCount - 1] = randomjobindex;
      curationCounts[globalJobsCount - 1] = undefined;
    }

    if (jobcounts[globalJobsCount - 1] >= postLength && postLength > firstIndex) {
      randomjobindex = Math.floor(Math.random() * ((postLength - 1) - firstIndex + 1)) + firstIndex;
      jobcounts[globalJobsCount - 1] = randomjobindex
    }

    return randomjobindex = jobcounts[globalJobsCount - 1];
  }

  curationPagination(globalCurationCount, postLength) {
    var firstIndex = globalCurationCount * 10 - 10 + 1;
    var lastIndex = globalCurationCount * 10 - 1;
    var randomCurationindex = Math.floor(Math.random() * (lastIndex - firstIndex + 1)) + firstIndex;

    if (curationCounts[globalCurationCount - 1] == undefined) {
      curationCounts[globalCurationCount - 1] = randomCurationindex;
      jobcounts[globalCurationCount - 1] = undefined;
    }

    if (curationCounts[globalCurationCount - 1] >= postLength && postLength > firstIndex) {
      randomCurationindex = Math.floor(Math.random() * ((postLength - 1) - firstIndex + 1)) + firstIndex;
      curationCounts[globalCurationCount - 1] = randomCurationindex;
    }

    return randomCurationindex = curationCounts[globalCurationCount - 1];
  }

  livePosts(posts, feedType, feed, feedId) {
    var curation_display, job_display;
    this.props.feed.feedList !== undefined && Object.keys(this.props.feed.feedList).length > 0 ? this.props.feed.feedList.map((feedData, index) => {
      if (feedData.identity == feedId) {
        job_display = feedData.job_display;
        curation_display = feedData.curation_display;
      }
    }) : ''
    globalJobsCount = 1;
    globalCurationCount = 1;
    var tempJobCount = 1;
    var tempCurationCount = 1;
    var postLength = posts ? posts.length : ''
    let me = this
    return (
      <div id='post-list-wrapper'>
        {posts ?
          posts.map((post, index) => {
            // random number generator
            var displayJob, displayRss = false;
            var jobPage, curationPage;
            var randomjob1, randomcuration1, mergeCurationCount, mergeJobCount = '';
            // to generate random number for job if page is 1
            if (this.props.location.pathname.includes("search") !== true) {
              if (job_display == 1 && curation_display == 1) {

                //to show jobs 
                if (globalJobsCount == 1) {
                  var firstIndex = 1;
                  var lastIndex = 4;
                  randomjob1 = Math.floor(Math.random() * (lastIndex - firstIndex + 1)) + firstIndex;
                  if (jobcounts[0] == undefined) {
                    jobcounts[0] = randomjob1;
                  }
                  randomjob1 = jobcounts[0];
                  if (randomjob1 >= postLength) {
                    randomjob1 = Math.floor(Math.random() * ((postLength - 1) - firstIndex + 1)) + firstIndex;
                    jobcounts[0] = randomjob1;
                  }
                } else if (this.props.feed.lastFeedType == "job" || jobcounts[globalJobsCount - 1] !== undefined) {
                  // generate random number for job if page is > 1
                  jobPage = this.jobPagination(globalJobsCount, postLength)
                  jobIndex = jobPage
                }


               //to show curation 
                if (globalCurationCount == 1) {
                  var firstIndex1 = 1;
                  var lastIndex1 = 4;
                  randomcuration1 = Math.floor(Math.random() * (lastIndex1 - firstIndex1 + 1)) + firstIndex1;
                  if (curationCounts[0] == undefined) {
                    curationCounts[0] = randomcuration1;
                  }
                  randomcuration1 = curationCounts[0];
                  if (randomcuration1 >= postLength && postLength > 6) {
                    randomcuration1 = Math.floor(Math.random() * ((postLength - 1) - firstIndex + 1)) + firstIndex;
                    curationCounts[0] = randomcuration1;
                  }
                } else if (this.props.feed.lastFeedType == "curation" || curationCounts[globalCurationCount - 1] !== undefined) {
                  // generate random number for curation if page is > 1
                  curationPage = this.curationPagination(globalCurationCount, postLength)
                  curationIndex = curationPage
                }
              }

              else if (job_display == 0 && curation_display == 1)
              {
                // this.displayCuration(postLength, randomcuration1,1,4)
                if (globalCurationCount == 1) {
                  var firstIndex1 = 1;
                  var lastIndex1 = 4;
                  randomcuration1 = Math.floor(Math.random() * (lastIndex1 - firstIndex1 + 1)) + firstIndex1;
                  if (curationCounts[0] == undefined) {
                    curationCounts[0] = randomcuration1;
                  }
                  randomcuration1 = curationCounts[0];
                  if (randomcuration1 >= postLength && postLength > 6) {
                    randomcuration1 = Math.floor(Math.random() * ((postLength - 1) - firstIndex + 1)) + firstIndex;
                    curationCounts[0] = randomcuration1;
                  }
                } else if (this.props.feed.lastFeedType == "curation" || curationCounts[globalCurationCount - 1] !== undefined) {
                  // generate random number for curation if page is > 1
                  curationPage = this.curationPagination(globalCurationCount, postLength)
                  curationIndex = curationPage
                }
              }
              else if (job_display == 1 && curation_display == 0) {
                // to generate random number for curation if page is 1
                if (globalCurationCount == 1) {
                  var firstIndex1 = 6;
                  var lastIndex1 = 8;
                  randomcuration1 = Math.floor(Math.random() * (lastIndex1 - firstIndex1 + 1)) + firstIndex1;
                  if (curationCounts[0] == undefined) {
                    curationCounts[0] = randomcuration1;
                  }
                  randomcuration1 = curationCounts[0];
                  if (randomcuration1 >= postLength && postLength > 6) {
                    randomcuration1 = Math.floor(Math.random() * ((postLength - 1) - firstIndex + 1)) + firstIndex;
                    curationCounts[0] = randomcuration1;
                  }
                } else if (this.props.feed.lastFeedType == "curation" || curationCounts[globalCurationCount - 1] !== undefined) {
                  // generate random number for curation if page is > 1
                  curationPage = this.curationPagination(globalCurationCount, postLength)
                  curationIndex = curationPage
                }

              }
              // to indentify that job has finished and now display curation in all the pages
              if (this.props.feed.isJobfinished == true && jobpageFlag == false) {
                if (tempCurationIndex == undefined && jobIndex !== undefined) {
                  tempJobIndex = jobIndex;
                  var swapData = jobcounts[jobIndex - 1];
                  curationCounts[jobIndex - 1] = swapData;
                  jobcounts[jobIndex - 1] = undefined;
                  tempCurationIndex = curationIndex
                }
                else {
                  if (tempCurationPage == undefined && globalJobsCount == 1) {
                    var swapData = jobcounts[0];
                    curationCounts[0] = swapData;
                    jobcounts[0] = '';
                    tempCurationPage = randomjob1
                    tempCurationIndex = null;
                  }
                }
                jobpageFlag = true;
              }


              if (index == randomjob1 || index == jobPage) {
                mergeJobCount = tempJobCount;
                tempJobCount++;
                if (this.props.feed.lastFeedType == "job" && index == jobPage) {
                  if (curation_display !== 1) {
                    globalJobsCount = globalJobsCount + 1;
                  } else {
                    globalJobsCount = globalJobsCount + 2;
                  }
                  if (globalCurationCount == 1) {
                    globalCurationCount = globalJobsCount + 1;
                  }
                } else {
                  if (jobcounts[globalJobsCount - 1] !== undefined && globalJobsCount !== 1) {
                    globalJobsCount = globalJobsCount + 2;
                  } else {
                    globalJobsCount = globalJobsCount + 1;
                  }
                }
                displayJob = true;
              }
              if (index == randomcuration1 || index == curationPage) {
                mergeCurationCount = tempCurationCount;
                tempCurationCount++;
                if (index == curationPage && this.props.feed.lastFeedType == "curation") {
                  // if condiition is used when job has finished and then increment globalCurationCount by 1 to display curation
                  if ((curationPage >= tempCurationIndex || curationPage == curationCounts[tempJobIndex - 1] || curationPage == curationCounts[tempJobIndex - 2] || tempCurationIndex == null) && this.props.feed.isJobfinished == true) {
                    globalCurationCount = globalCurationCount + 1;
                  }
                  else {
                    if (job_display !== 1) {
                      globalCurationCount = globalCurationCount + 1;
                    }
                    else {
                      globalCurationCount = globalCurationCount + 2;
                    }

                  }
                } else {
                  if (randomcuration1 == tempCurationPage && mergeCurationCount == 1) {
                    globalCurationCount = globalCurationCount + 1;
                  } else if (curationCounts[globalCurationCount - 1] !== undefined) {
                    if (job_display !== 1) {
                      globalCurationCount = globalCurationCount + 1;
                    } else {
                      globalCurationCount = globalCurationCount + 2;
                    }
                  }
                }
                displayRss = true
              }
            }
            let roleName = JSON.parse(localStorage.getItem('roles')) !== null ? JSON.parse(localStorage.getItem('roles'))[0].role_name : '';
            return (
              <div key={index} className="post-item-feed">
                {displayJob == true && index !== 0 ? <MergeJobs props={this.props} Slider={this.state.Slider} globalcount={mergeJobCount}></MergeJobs> : ''}
                {displayRss == true && index !== 0 ? <MergeRss props={this.props} Slider={this.state.Slider} globalcount={mergeCurationCount}></MergeRss> : ''}

                {index == 2 && roleName !== 'guest' && Object.keys(this.props.polls.pollQuestions).length > 0 && feedId == 'rpPDp' ?

                  <PollQuestions
                    pollsData={this.props.polls}
                    openPollQuestionPopup={this.openPollQuestionPopup.bind(this, true)}
                    closePollQuestionPopup={this.openPollQuestionPopup.bind(this, false)}
                    handleSurveySave={this.handleSurveySave.bind(this)}
                    openPopup='no'
                    fetchSurvey={this.fetchSurveyPollQuestion.bind(this)}
                  />
                  : ''}
                <Post
                  feed={feed}
                  feedId={feedId}
                  feedType={feedType}
                  locationInfo={me.props.location}
                  details={post}
                  key={index}
                  Slider={this.state.Slider}
                  index={index}
                  handleCloseEvent={this
                    ._handleCloseEvent
                    .bind()}
                  updateText={this
                    .updateText
                    .bind(this)}
                  feedData={this.props.feed}
                  searchInput={this.props.searchedData}
                  sharePostClick={this.sharePostClick.bind(this)}
                  openCreatePopup={this.openCreatePopup.bind(this, post)}   //this method open the popup
                />
              </div>)
          })
          : ''
        }

      </div>
    )
  }
  sharePostClick(e){
    var me = this;
    if(e==true){
      document
      .body
      .classList
      .add('overlay')
    }else if(e==false){
      document
      .body
      .classList
      .remove('overlay')
    }
    me.setState({
      isPostSharing : e
    })
  }
   
  pollDesign(e) {
    return (
      <div className="quick-survey-post post">
        <div class="post-header clearfix ">
          <div class="author-thumb">
            <div
              className='thumbnail qick-survey-admin-thumbnail'
            />
          </div>
          <div class="author-data">
            <div class="author-meta">
              <b>
                jack sparroww
             </b>
              shared<span class="socialIcons"><span>via </span><span class="socialIconWrapper"><a class="internal-icon analyticinternalicon" title="Internal channel "><i class="material-icons">vpn_lock</i></a></span></span>
            </div>
            <div class="post-time"><span class="timeFromNow" title="Friday, 22nd March 2019 at 3:09 pm">6 minutes ago</span></div>
            <ul class="dd-menu context-menu">
              <li class="button-dropdown">
                <a class="dropdown-toggle" id="dropdown-toggle-ArrJq"><i class="material-icons">more_vert</i></a>
                <ul class="dropdown-menu" id="dropdown-menu-ArrJq">
                  <li><a class="btn-edit-folder">Hide post</a></li>
                  <li><a class="btn-edit-folder">Pin on top</a></li>
                </ul>
              </li>
            </ul>
          </div>
        </div>
        <div className="post-pic-ranking-container" >
          <div className="render-ranking-only-text-container">
            <div className="post-rating-main">
              <div className="post-rating-voting-text-part-wrapper clearfix">
                <div className="post-rating-value-lable">
                  <div className="">
                    <div className="value-container-ranking"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24"><path fill="none" d="M0 0h24v24H0V0z" /><path d="M18 9l-1.41-1.42L10 14.17l-2.59-2.58L6 13l4 4zm1-6h-4.18C14.4 1.84 13.3 1 12 1c-1.3 0-2.4.84-2.82 2H5c-.14 0-.27.01-.4.04-.39.08-.74.28-1.01.55-.18.18-.33.4-.43.64-.1.23-.16.49-.16.77v14c0 .27.06.54.16.78s.25.45.43.64c.27.27.62.47 1.01.55.13.02.26.03.4.03h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zm-7-.25c.41 0 .75.34.75.75s-.34.75-.75.75-.75-.34-.75-.75.34-.75.75-.75zM19 19H5V5h14v14z" /></svg>
                      <span>Quick Pulse</span></div>
                  </div>
                  <div className="valueDescriptionPopup">
                    <div className="valueDescriptionPopupInner">having insightful work  , having insightful work  , having insightful work  , having insightful work , having insightful work , having insightful work , having insightful work , v, having insightful work </div>
                  </div>
                </div>
                <div className="post-rating-desc-part">My work provides me with opportunities to build my capabilities and improve myself.</div>
              </div>
            </div>
          </div>
        </div>
        <div className="quick-suvery-content">
          <div className="inner-quick-suvery-content">
            <div className="clearfix quick-suvery-content-wrapper">
              <div className="quick-survey-illustration">
                <div className="inner-quick-survey-illustration">
                  <img src="/img/survey.png" />
                </div>
              </div>
              <div className="quick-survery-options">
                <ul>
                  <li>Never</li>
                  <li>Rarely</li>
                  <li>Not Very Often</li>
                  <li>Sometimes</li>
                  <li>Often</li>
                  <li>Very often</li>
                  <li>Always</li>
                </ul>
              </div>
            </div>
            <div className="success-msg-quick-suvery-content">
              <div className="success-text">
                <p> Thank you <span className="smile-face-quick-suvery">&#9786;</span>  </p> </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
  myFeedPosts({ posts, feedType, feed }) {
    let me = this
    switch (feedType) {
      case 'live':
        if (this.props.location.pathname == "/search/internal/live") {
          return (
            <div>
              <div className='internal-live esc-sm'>
                <div className='content'>
                  {me.props.feed.mypost.loading && me.props.feed.mypost.loadmore !== true
                    ?
                    <PreLoader feedLoader />
                    :
                    me.props.feed.mypost.loading == false && me.props.feed.mypost.nopost == true
                      ? <div>
                        <div className='no-data-block'>No post found.</div>
                        {this.no_post_method()}
                      </div>
                      :
                      this.livePosts(posts.mypost.live, feedType, feed)

                  }
                  {me.props.feed.mypost.loadmore == true
                    ? <PreLoader feedLoader isBottom={true} />
                    : ''}
                </div>
              </div>

            </div>

          )
        }
        else {
          return (
            <div>
              <div className='internal-live esc-sm'>
                <div className='content'>
                  {me.props.feed.mypost.loading
                    ?
                    <PreLoader feedLoader />
                    :
                    me.props.feed.mypost.loading == false && me.props.feed.mypost.nopost == true
                      ? <div>
                        <div className='no-data-block'>No post found.</div>
                        {this.no_post_method()}
                      </div>
                      : this.livePosts(posts.mypost.live, feedType, feed)}
                  {me.props.feed.mypost.loadmore == true
                    ? <PreLoader feedLoader isBottom={true} />
                    : ''}
                </div>
              </div>

            </div>
          )
        }
      break;

      case 'scheduled':
        return (
          <div className='internal-scheduled esc-sm'>
            <div className='content'>
              {me.props.feed.mypost.loading && me.props.feed.mypost.loadmore !== true
                ?
                <PreLoader feedLoader />
                :
                me.props.feed.mypost.loading == false && me.props.feed.mypost.nopost == true
                  ? <div>
                    <div className='no-data-block'>No post found.</div>
                    {this.no_post_method()}
                  </div>
                  : this.scheduledPosts({ posts: posts.mypost.scheduled, postsByType: posts.mypost, feedType, feed })}
              {me.props.feed.mypost.loadmore == true
                ?
                <PreLoader feedLoader isBottom={true} />
                : ''}
            </div>
          </div>
        )
      break;
    
      case 'unapproved' : case 'failed' :
          return (
            <div className='internal-scheduled esc-sm'>
              <div className='content'>
                {me.props.feed.mypost.loading && me.props.feed.mypost.loadmore !== true
                  ?
                  <PreLoader feedLoader />
                  :
                  me.props.feed.mypost.loading == false && me.props.feed.mypost.nopost == true
                    ? <div>
                      <div className='no-data-block'>No post found.</div>
                      {this.no_post_method()}
                    </div>
                    : this.unapprovedPosts({ posts: posts.mypost.unapproved, postsByType: posts.mypost, feedType, feed })}
                {me.props.feed.mypost.loadmore == true
                  ?
                  <PreLoader feedLoader isBottom={true} />
                  : ''}
              </div>
            </div>
          )
        break;

          case 'pending':
              return (
                <div className='internal-scheduled esc-sm'>
                  <div className='content'>
                    {me.props.feed.mypost.loading && me.props.feed.mypost.loadmore !== true
                      ?
                      <PreLoader feedLoader />
                      :
                      me.props.feed.mypost.loading == false && me.props.feed.mypost.nopost == true
                        ? <div>
                          <div className='no-data-block'>No post found.</div>
                          {this.no_post_method()}
                        </div>
                        : this.pendingPosts({ posts: posts.mypost.pending, postsByType: posts.mypost, feedType, feed })}
                    {me.props.feed.mypost.loadmore == true
                      ?
                      <PreLoader feedLoader isBottom={true} />
                      : ''}
                  </div>
                </div>
              )
            break;
    }
  }
  unapprovedPosts({posts,postsByType,feedType,feed}){
    var me = this;
    return(
      <div id='post-list-wrapper'>
      {
        typeof posts !== "undefined" ?
          posts.map((post, index) => {
                return(
                  <Post
                    Slider={this.state.Slider} 
                    key={index}
                    index={index}
                    feed={feed}
                    feedType={feedType}
                    locationInfo={me.props.location}
                    details={post}
                    showLikesComment={false}
                    tooltip={this.state.tooltip}
                    tooltipid={`one` + index}
                    handleCloseEvent={this
                      ._handleCloseEvent
                      .bind()}
                    updateText={this
                      .updateText
                      .bind(this)}
                    feedData={this.props.feed}
                    searchInput={this.props.searchedData}
                  />
              )
          })
          : ''
      }
    </div>
    )
  }
  pendingPosts({posts,postsByType,feedType,feed}){
    var me = this;
    return(
      <div id='post-list-wrapper'>
      {
        typeof posts !== "undefined" ?
          posts.map((post, index) => {
            return(
              <Post
                Slider={this.state.Slider} 
                key={index}
                index={index}
                feed={feed}
                feedType={feedType}
                locationInfo={me.props.location}
                details={post}
                showLikesComment={false}
                tooltipid={`one` + index}
                handleCloseEvent={this
                  ._handleCloseEvent
                  .bind()}
                updateText={this
                  .updateText
                  .bind(this)}
                feedData={this.props.feed}
                searchInput={this.props.searchedData}
              />
              )
          })
          : ''
      }
    </div>
    )
  }
  defaultFeedPosts({ posts, feedType, feed }) {
    let me = this
    switch (feedType) {
      case 'live':
        if (this.props.location.pathname == "/search/default/live") {
          return (
            <div>
              <div className='internal-live esc-sm'>
                <div className='content'>
                  {me.props.feed.default.loading && me.props.feed.default.loadmore !== true
                    ?
                    <PreLoader feedLoader />
                    :
                    me.props.feed.default.loading == false && me.props.feed.default.nopost == true
                      ? <div>
                        <div className='no-data-block'>No post found.</div>
                        {this.no_post_method()}
                      </div>
                      :
                      this.livePosts(posts, feedType, feed, Globals.defaultFeedId)

                  }
                  {me.props.feed.default.loadmore == true && (me.props.location.state == null || (me.props.location.state !== null && me.props.location.state.fromSuggestionClick !== true))
                    ? <PreLoader feedLoader isBottom={true} />
                    : ''}
                </div>
              </div>

            </div>

          )
        }
        else {
          return (
            <div>
              <div className='internal-live esc-sm'>
                <div className='content'>
                  {me.props.feed.default.loading && me.props.feed.default.loadmore !== true
                    ?
                    <PreLoader  feedLoader/>
                    : 
                     me.props.feed.default.loading == false && me.props.feed.default.nopost == true
                      ? <div>
                        <div className='no-data-block'>No post found.</div>
                        {this.no_post_method()}
                      </div>
                      :
                       this.livePosts(posts.default.live, feedType, feed, Globals.defaultFeedId)}
                  {me.props.feed.default.loadmore == true
                    ?
                    <PreLoader feedLoader isBottom={true} />
                    : ''}

                </div>
              </div>

            </div>
          )
        }


      default:
        return (
          <div className='internal-live esc-sm'>
            <div className='content'>

              {me.props.feed.default.loading && me.props.feed.default.loadmore !== true
                ?
                <PreLoader feedLoader />
                :
                me.props.feed.default.loading == false && me.props.feed.default.nopost == true
                  ? <div>
                    <div className='no-data-block'>No post found.</div>
                    {this.no_post_method()}
                  </div>
                  : this.livePosts(posts.default.live, feedType, feed, Globals.defaultFeedId)}


            </div>
          </div>
        )
    }
  }


  no_post_method() {
    var add_new_popup = document.getElementById('add-new-popup');

    if (tour_flag == false && add_new_popup) {
      add_new_popup.classList.add('show');

    }
  }
  hide_add_new_popup() {

    var add_new_popup = document.getElementById('add-new-popup');
    if (add_new_popup) {
      add_new_popup.classList.remove('show');
    }
  }
  closeAssetDetailPopup() {
    this.setState({ assetDetailPopup: false })
    document
      .body
      .classList
      .remove('overlay')
  }
  assetClick(self, asset) {
    self.setState({ assetDetailPopup: true, currentAssetDetail: asset })

    document
      .body
      .classList
      .add('overlay')
  }
  renderAssetDetail() {
    return (
      <SliderPopup wide>
        <button
          id='close-popup'
          className='btn-default'
          onClick={this
            .closeAssetDetailPopup
            .bind(this)}>
          <i className='material-icons'>clear</i>
        </button>
        <AssetDetail
          moment={this.state.moment}
          asset={this.state.currentAssetDetail}
          updateText={this
            .updateText
            .bind(this)} />

      </SliderPopup>
    )
  }

  customFeedPosts({ posts, feedType, feed, feedId }) {
    let me = this
    switch (feedType) {
      case 'live':
        return (
          <div className='external-live esc-sm'>
            <div className='content'>
              {me.props.feed.custom.loading && me.props.feed.custom.loadmore !== true
                ? <PreLoader feedLoader />
                :
                me.props.feed.custom.loading == false && me.props.feed.custom.nopost == true
                  ? <div>
                    <div className='no-data-block'>No post found.</div>
                    {this.no_post_method()}
                  </div>
                  : this.livePosts(posts.custom.live, feedType, feed, feedId)}
              {me.props.feed.custom.loadmore == true
                ?
                <PreLoader feedLoader isBottom={true} />
                : ''}
            </div>
          </div>
        )

    }
  }
  renderPosts() {

    let feed;
    let posts;
    let feedType = this.state.currFeedPosts
    var feedID = this.state.selectedFeedStatus;
    feed = this.state.currFeed
    let hashAssets = this.props.hashAssets;
    var me = this

    if (this.props.location.pathname == "/search/default/live") {
      if (this.props.NoData == true) {
        posts = []
      }
      else if (this.state.currPage !== undefined) {
        if (this.props.location.state == null || this.props.location.state.seeAllClick == true) {
          posts = this.props.feed.default.live
        } else if (this.props.location.state !== null && this.props.location.state.isenterSubmit == true) {
          posts = this.props.searchData !== undefined && this.state.currPage == 1 ? this.props.searchData.searchData.post.data : this.props.feed.default.live
        }
        else {
          if (this.props.location.state.fromSuggestionClick == true) {
            posts = this.props.postsData
          }
        }
      }
      else {
        posts = this.props.postsData
      }
      if (this.props.location.pathname == "/search/default/live") {
        feed = 'default';
        feedType = 'live';
      }
    }
    else {
      posts = this.props.feed
    }
    switch (feed) {

      case 'my':
        if (this.state.currFeedPosts == "live") {
          var me = this;
          return (
            <div className='internal'>
              {me.myFeedPosts({ posts, feedType, feed })}
            </div>
          )
        }
        else if (this.state.currFeedPosts == "scheduled") {
          return (
            <div className='external'>
              {me.myFeedPosts({ posts, feedType, feed })}
            </div>
          )
        }
        else if (this.state.currFeedPosts == "unapproved" || this.state.currFeedPosts == "failed") {
          return (
            <div className='external'>
              {me.myFeedPosts({ posts, feedType, feed })}
            </div>
          )
        }
        else if (this.state.currFeedPosts == "pending") {
          return (
            <div className='external'>
              {me.myFeedPosts({ posts, feedType, feed })}
            </div>
          )
        }
        break
      case 'default':
        return (
          <div className='internal'>
            {me.defaultFeedPosts({ posts, feedType, feed })}
          </div>
        )
        break
      case 'custom':
        var feedId;
        var feedData = [];
        var path = utils
          .removeTrailingSlash(this.props.location.pathname)
          .split('/')
        if (path[4] == undefined) {
          if (Object.keys(this.props.feed.feedList).length > 0) {
            this.props.feed.feedList.map((value, i) => {
              if (value.identity !== "rpPDp") {
                feedData.push(value);
              }
            })
            browserHistory.push(`/feed/custom/live/${feedData[0].identity}`)
          }
        }
        else {
          feedId = path[4];
        }
        return (
          <div className='external'>
            {me.customFeedPosts({ posts, feedType, feed, feedId })}
          </div>
        )
        break
      case 'assets':
        if (this.props.assets.hashloading !== false) {
          return (
            <div className='page'>
              <div className='full-container'>
                <div className='asset-container'>
                  <div className='esc-2'>
                    <div className='clearfix parent_assets'>
                      <div className="render-assets">
                        <div id="container">
                          <div className='archive-container asset-file-container clearfix'>
                            <div class="vid file-image asset-file imgwrapper">
                              <div class="inner_imgwrapper assetInnerWrapper">
                                <a class="type-image">
                                  <span class="imageloader loaded thumbnail loader-assets-image">
                                    <img src="/img/visiblyLoader.gif" />
                                  </span>
                                </a>
                              </div>
                              <a class="file-details">
                                <span class="title">
                                  <div className="loader-grey-line loader-line-height loader-line-radius loader-assets-title"> </div>
                                </span>
                              </a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          )
        } else {
          if (hashAssets.length > 0) {
            return (
              <div className='full-container'>
                <div className='archive-container clearfix'>
                  {hashAssets
                    .map(function (file, index) {
                      /* below is an example of how to use arrow functions to bind (not using .bind()) this value */
                      return (<Asset
                        onClick={asset => me.assetClick(me, file)}
                        key={index}
                        name={file.title}
                        extension={file.media_extension}
                        identity={file.identity}
                        media_type={file.media_type}
                        thumbnail_url={file.thumbnail_url}
                        media_url={file.media_url}
                        approved={file.approved}
                        category_id={file.category}
                        width='320'
                        height='240'
                        detailView={false} />)
                    })}
                </div>
              </div>
            )
          } else {
            return (
              <div className='no-data-block'>No asset found.</div>
            )
          }
        }
        break
      default:
        return (
          <div className='external'>
            {me.defaultFeedPosts({ posts, feedType, feed })}
          </div>
        )
    }
  }

  handlePostCreation() {
    this.setState({ newPostPopup: true });
    // asset created flag - change in redux to false
    this
      .props
      .postsActions
      .creatingNewPost()
    window.scrollTo(0, 0);

  }
  /**
   * @author disha
   * @use close popup which is open from campaign
   */
  closePostCreatePopup(closebtnClick=null) {
   

    // to delete item from AWS
    if(closebtnClick==true&& this.props.aws.uploadedItems.length>0)
     {
      Globals.AWS_CONFIG.albumName = localStorage.getItem("albumName");
      var s3Config = Globals.AWS_CONFIG;
      s3functions.deleteObjectFromS3(this.props.aws.uploadedItems,s3Config)
     } 
    
    let url
    this.props.postsActions.removeCampaignCreatePostData()

    // need to call to reset saved hashtag props as  createpostform page's handleinit is calling twice and if we disable savedhashtag then old props (which is not updated yet) call handleinit and display saved hashtag
    this.props.feedActions.fetchHashTagFail() 

    this.setState({ newPostPopup: false, openPostPopup: false })
    document.body.classList.remove('overlayForFeedPopup');
    editPostData = null;     
    this.setState({
      CurationEditOpenPopup : false,
      editCurationData: null 
    })   
    if(closebtnClick==true){
      document.getElementById("rss_popup")?document.getElementById("rss_popup").classList.remove("hide"):''
    }
    this.props.postsActions.resetCurationEdit();       //editPostData , if we click on share , Without create we close the popup  the reset the props
    editCreatePostOpenPopup = false;
    document
      .body
      .classList
      .remove('overlay')
    if (this.state.postFromCampaign) {
      if (this.props.location.search.includes("search"))
        url = `/search/campaign`
      else
        url = `/campaigns?campaignid=${this.state.postCreationCampaignID}`
      browserHistory.push(url)
    }
  }

  // this function for close trending tag popup in  responsive
  closeNav(popup) {
    this.setState({
      newTrendingPostPopup: false
    });
    document.body.classList.remove('overlay_responsive_trending')
  }

  trendingTagPopup() {
    var path = utils
      .removeTrailingSlash(this.props.location.pathname)
      .split('/')
    return (
      <ReactCSSTransitionGroup
        transitionName='fade'
        transitionAppear
        transitionEnterTimeout={500}
        transitionLeaveTimeout={500}
        transitionAppearTimeout={1000}
      >
        <div className="trending-button-container">
          <div className="side-popup-wrap">
            <div className="wide">
              <div id="mySidenav" class="sidenav">

                <div className="responsive-trending">

                  <TrendingTagsPanel
                    tagsData={this.props.tags.trendingTags}
                    currentPath={path}
                    listing={this.props.tags.trendinglist}
                  />
                </div>
                <button
                  id='close-popup'
                  className='btn-default closebtn'
                  onClick={this.closeNav.bind(this)}>
                  <i className='material-icons'>clear</i>
                </button>
              </div>
            </div>
          </div>
        </div>
      </ReactCSSTransitionGroup>
    )
  }


  renderPostCreation() {
    //this code for when we edit the popup then set the position of the popup
    var currFeedName = this.state.currentFeed !== '' ? this.state.currentFeed : writePostTitle;
    document.body.classList.add('overlayForFeedPopup');
    if (editCreatePostOpenPopup == true) {

      var bodyRect = document.body.getBoundingClientRect();
      var shareElement = document.getElementById(`post-${editPostData.post_id}`);
      var offsetPoint = shareElement.getBoundingClientRect();
      //this code scroll the popup on special post
      var offset = offsetPoint.top - bodyRect.top + offsetPoint.height - 300;
      var sharePopupEditdata = document.querySelectorAll('.Create-post-wrap-container');
      if (sharePopupEditdata.length > 0) {
        sharePopupEditdata[0].style.top = offset + 'px';
        //this line for find the new cordinater of create post popup
        var newCordinatesCreatepost = sharePopupEditdata[0].getBoundingClientRect();
        sharePopupEditdata[0].style.top = offset - newCordinatesCreatepost.top + 56 + 'px';
      }
    }
    var defaultfeedData = Object.keys(this.props.feed.feedList).length > 0 ? this.props.feed.feedList[0] : ''
    return (
      <div className="createPostMainContainer" id="createMainContainerPopup">
        <SliderPopup className='wide createPostPopup' fadePopup='fadepopup' locationPopup="CreatePost">
          <button
            id='close-popup'
            className='btn-default'
            onClick={this
              .closePostCreatePopup
              .bind(this,true)}>
            <i className='material-icons'>clear</i>
          </button>
          <CreatePost
            campaignId={this.state.postCreationCampaignID}
            campaignTitle={this.state.postCreationCampaignTitle}
            jQuery={this.state.jQuery}
            defaultFeed={defaultfeedData}
            location={this.props.location}
            editPostData={editPostData}   
            editCurationData={this.state.editCurationData}           //for share and edit pass the data in to the CreatePost
            currentFeed={currFeedName} // send Current feed data to create post
            customTextFlag={customTextFlag}
            hashTag = {this.props.feed.hashTagListInCreatePost}
          />
        </SliderPopup>
      </div>
    )
  }
  openPopUpTrending(e) {
    this.setState({
      newTrendingPostPopup: true
    });
    document.body.classList.add('overlay_responsive_trending')
  }
  changeView(view) {
    if (this.props.location.pathname) {
      var newLocation = utils
        .removeTrailingSlash(this.props.location.pathname)
        .split('/')
      if ((view == 'list' && (this.props.location.pathname == "/feed/my/scheduled"))) {
        this.props.feedActions.resetPosts()
        this.fetchPosts(newLocation[2], newLocation[3], 1)
      }
    }
    this.setState({
      view
    })
  }
  /**
   * @author disha
   * set state to open create post popup
   */
  openCreatePostPopup(e) {
    this.props.feedActions.openFeedPopup();
    this.setState({
      openPostPopup: true
    })
  }
  selectedFeedStatus(e) {
    this.setState({
      selectedFeedStatus: e.target.value
    })
  }
  ApplyFeedFilter(e) {
    isFilterAppiled = true;
    propsChanged = true //to recall setCurrentFeed to set state of selected feed type from filter
    var me = this;
    var path = utils
      .removeTrailingSlash(this.props.location.pathname)
      .split('/')
    var hash = this.props.location.hash;
    this
      .props
      .feedActions
      .resetPosts();
    this.props.feedActions.filterMyFeedData(path[2], path[3], this.state.selectedFeedStatus, 1, 10, false, false, null, null, false, hash.substr(1));
  }
  openCreateFeed() {
    this.props.tagsActions.fetchUserTagsAll('user');
    this.setState({ createFeed: true });
  }
  updateFeedOpen(feedId) {


    this.props.feedActions.getUpdateFeedDetail(feedId);
    this.props.tagsActions.fetchUserTagsAll('user', feedId);

    this.setState({
      openEditFeedPopup: true,
      editFeedId: feedId
    })

  }

  closeCreateFeedPopup() {
    document.body.classList.remove('center-wrapper-body-container');
    this.setState({ createFeed: false });
  }
  closeUpdateFeedPopup() {
    document.body.classList.remove('center-wrapper-body-container');
    this.setState({
      openEditFeedPopup: false,
      editFeedId: null
    })
  }
  createTagList(str) {
    var me = this
    var rx1 = /@\[([^\]]+)]\(([^)]+)\)/g
    let res = str.match(rx1)
    var userTag = []

    return new Promise(function (resolve, reject) {
      if (res) {
        res.forEach(function (tags) {
          /** Get user information */
          var tagsArray = tags.split('@[')
          var nameArray = tagsArray[1].split(']')
          var name = nameArray[0]
          var dept = nameArray[1]
            .substring(0, nameArray[1].length - 1)
            .split(':')[1]
            .split('_')
          var type = 'tags'
          if (
            nameArray[1].substring(0, nameArray[1].length - 1).split(':')[0] !==
            '(tags'
          ) {
            type = dept[1] == 'company' ? 'company' :
              dept[1] == 'dept' ?
                "department"
                : "user";
            let tagsUserObject = {
              tagid: dept[0],
              tagName: name.replace(/ /g, ''),
              type: type
            }
            userTag.push(tagsUserObject)
          }

          /** description replace */
          if (type === 'tags') {
            str = str.replace(tags, '#' + name.replace(/ /g, ''))
          } else {
            str = str.replace(tags, '@' + name.replace(/ /g, ''))
          }
        })
        me.setState({
          userTag: userTag
        })
      }
      resolve(str)
    })
  }
  handleSubmitFeed(val) {
    if (typeof (val.users) == "undefined" && typeof (val.guests) == "undefined") {
      let me = this;
      if (val.title.trim() !== '') {
        var ObjToSend = {
          "title": val.title,
          "feedType": val.feedType,
          "users": me.props.companyTag,
          "jobRss": val.JobRss,
          "chat":val.chat
        }
        me.props.feedActions.createCustomFeed(ObjToSend);
      } else {
        notie.alert('error', 'You can not pass empty feed name.', 5)
      }
    }
    else if (typeof (val.users) !== "undefined") {
      this
        .createTagList(val.users)
        .then((desc) => {
          let me = this;
          if (val.title.trim() !== '') {
            var ObjToSend = {
              "title": val.title,
              "feedType": val.feedType,
              "users": me.state.userTag,
              "guests": val.guests,
              "jobRss": val.JobRss,
              "chat":val.chat
              
            }
            me.props.feedActions.createCustomFeed(ObjToSend);
          } else {
            notie.alert('error', 'You can not pass empty feed name.', 5)
          }
        });
    } else {
      let me = this;
      if (val.title.trim() !== '') {
        var ObjToSend = {
          "title": val.title,
          "feedType": val.feedType,
          "guests": val.guests,
          "chat":val.chat

        }
        me.props.feedActions.createCustomFeed(ObjToSend);
      } else {
        notie.alert('error', 'You can not pass empty feed name.', 5)
      }
    }

  }
  createFeed() {
    document.body.classList.add('center-wrapper-body-container');
    return (
      <CreateFeedPopup
        closeCreateFeedPopup={this.closeCreateFeedPopup.bind(this)}
        onSubmit={this.handleSubmitFeed.bind(this)}
        userTags={this.props.allUserTag}
        feed={this.props.feed}
      />
    )
  }

  handleUpdateSubmitFeed(val) {
    let me = this
    if (typeof (val.users) !== "undefined" && val.users != "") {
      this
        .createTagList(val.users)
        .then((desc) => {
          let me = this;
          var ObjToSend = {
            "users": me.state.userTag,
            "guests": val.guests,
            "feedId": val.feedId,
          }
          me.props.feedActions.AddMoreUserCustomFeed(ObjToSend);
        });
    } else {
      let me = this;
      var ObjToSend = {
        "guests": val.guests,
        "feedId": val.feedId,
      }
      me.props.feedActions.AddMoreUserCustomFeed(ObjToSend);
    }

  }
  resetUpdateForm() {
    this.props.feedActions.resetUpdateForm();
  }
  handleUpdateDetailSubmitFeed(val) {
    this.props.feedActions.updateFeedDetail(val);
  }

  updateFeed() {
    document.body.classList.add('center-wrapper-body-container');
    return (
      <UpdateFeedPopup
        feedId={this.state.editFeedId}
        closeUpdateFeedPopup={this.closeUpdateFeedPopup.bind(this)}
        handleUpdateSubmitFeed={this.handleUpdateSubmitFeed.bind(this)}
        handleDetailSubmit={this.handleUpdateDetailSubmitFeed.bind(this)}
        feed={this.props.feed}
        userTags={this.props.allUserTag}
        removeFromFeed={this.removeFromFeed.bind(this)}
        resetUpdateForm={this.resetUpdateForm.bind(this)}
        loginId={this.props.profile.profile.identity ? this.props.profile.profile.identity : ''}
      />
    )
  }

  removeFromFeed(user, feedId) {
    let UserObject;
    if (user.type == "department" || user.type == "company") {
      UserObject = {
        tagid: user.identity,
        tagName: user.name,
        type: user.type
      }
    } else {
      UserObject = {
        tagid: user.identity,
        tagName: user.first_name,
        type: 'user'
      }
    }
    this.props.feedActions.removeUserFromFeed(UserObject, feedId);
  }
  FetchRender() {
    let user = this.props.users;
    let userDetails = user.userDetails;
    let department = this.props.department;
    let isFetching = user.isFetching;
    let feed_asset_class;
    var userAvtar = this.props.profile.profile.avatar ? this.props.profile.profile.avatar : '';
    let roleName = JSON.parse(localStorage.getItem('roles')) !== null ? JSON.parse(localStorage.getItem('roles'))[0].role_name : ''
    let currentFilter = this.state.currentFilter == '/feed/default/live'
      ? 'default/live'
      : this.state.currentFilter;
    if (this.props.location.pathname == '/feed/assets/hash') {
      feed_asset_class = 'feed-asset-page';
    }
    else {
      feed_asset_class = '';
    }
    let no_post_class;
    var path = utils
      .removeTrailingSlash(this.props.location.pathname)
      .split('/')
    var authorAvatar = {
      backgroundImage: `url(${typeof userAvtar !== 'undefined' && userAvtar !== '' ? userAvtar : 'https://app.visibly.io/img/default-avatar.png'})`
    }
    if (path[2] == 'default') {
      if (this.props.feed.feedList !== undefined && Object.keys(this.props.feed.feedList).length > 0) {
        writePostTitle = this.props.feed.feedList[0].name;
        writePostType = this.props.feed.feedList[0].type;
      }
    } else if (path[2] == 'my') {
      //if feed type is changed from filter then to display its name on create post small popup
      writePostTitle = this.state.currentFeed !== '' ? this.state.currentFeed.name : Object.keys(this.props.feed.feedList).length > 0 ? this.props.feed.feedList[0].name : '';
      writePostType = this.state.currentFeed !== '' ? this.state.currentFeed.type : Object.keys(this.props.feed.feedList).length > 0 ? this.props.feed.feedList[0].type : '';
    } else {
      writePostTitle = this.state.currentFeed.name;
      writePostType = this.state.currentFeed.type;
    }
    return (
      <div id='feed'>
        <div className="openPopupOnScroll">
          <a className="goToTopForOpenPopup" id="goToToPPopPopup" onClick={this.handlePostCreation.bind(this)}>
            <span className="gotoTopIcon"> <svg fill="#ffffff" xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 24 24"><path fill="none" d="M0 0h24v24H0V0z" /><path d="M4 12l1.41 1.41L11 7.83V20h2V7.83l5.58 5.59L20 12l-8-8-8 8z" /></svg></span>
            Create Post </a>
        </div>
        <div id='welcome_inputbox' />
        <div id='tour_final_popup' />
        <div id="feed-popup" />

        <div className={`clearfix trending-parent ${feed_asset_class}`}>
          <div className={`render-post-feed ${no_post_class}`}>
            {this.props.location.pathname !== "/feed/assets/hash" ?
              <div class="create_post">
                <div class="esc-sm">
                  <div className="content">
                    {!(this.props.profile.profile.enable_social_posting == 0 &&  writePostType == 'external') || roleName == 'super-admin'?
                      <div id="Create-post-form-wrapper" className="Create-post-popup-main-container-wrapper">
                        <div className="Create-post-popup-main-container" id="write-popup-container">
                          <div class="create_post_heading">
                            <h2>Create {writePostTitle} post</h2>
                          </div>
                          <div class="write_post">
                            <div class="write_post_inner clearfix" onClick={this.handlePostCreation.bind(this)} >
                              <div class="createPostUserThumbnail" style={authorAvatar}></div>
                              <div className="createPostTextareaWrapper" >Share your thoughts...</div>
                            </div>
                          </div>
                        </div>
                      </div>:''}
                  </div>
                </div>
              </div>
              : ''}



            {this.renderPosts()}


            {/* <div className = "curated-loader-container">
                   <div className="curated-loader-inner-container">
                          <div className = "curated-header"> hii </div>
                   </div>
                </div> */}
          </div>
          {/* for showing trending tags */}
          {path[3] !== 'scheduled' ?
            <div>
              <div className="trending-xl-container">
                <TrendingTagsPanel
                  tagsData={this.props.tags.trendingTags}
                  currentPath={path}
                  listing={this.props.tags.trendinglist}
                />
              </div>
            </div>
            : ''
          }

        </div>
        <div id="pop-up-tooltip-holder">
          {selectedUserData == "No user found" && this.state.tagType !== "department" ?
            <div
              className="pop-up-tooltip-not-data-found"
              style={{
                top: this.state.popupTop,
                left: this.state.popupLeft,
                display: this.state.popupdisplay
              }}>
              <span className='popup-arrow'></span>
              <p> No user data found </p>
            </div>


            :
            <div id="pop-up-tooltip-wrapper" className={this.state.tagTypeClass}>
              <div
                className="pop-up-tooltip"
                style={{
                  top: this.state.popupTop,
                  left: this.state.popupLeft,
                  display: this.state.popupdisplay
                }}>
                <span className='popup-arrow'></span>
                <TagPopup
                  userDetails={selectedUserData}
                  isFetching={isFetching}
                  isDept={this.state.tagType}
                  department={selectedDepartmentData} />
              </div>
            </div>

          }
        </div>
      </div>
    )
  }
  render() {
    var flag;
    let hastag_name = '';
    let title;
    let NavBar
    hastag_name = this.props.location.hash;
    let roleName = JSON.parse(localStorage.getItem('roles')) !== null ? JSON.parse(localStorage.getItem('roles'))[0].role_name : ''

    var path = utils
      .removeTrailingSlash(this.props.location.pathname)
      .split('/')
    if (this.props.location.pathname == "/search/default/live") {
      NavBar = SearchNav
      title = "default"
    }
    else {
      NavBar = FeedNav
      title = "Feeds"
    }

    let calendarViewState = this.state.currFeedPosts == 'scheduled' && this.state.view == 'calendar';
    var feedFilter = this.props.feed.feedList;
    return (
      <div>
        {this.state.isPostSharing == true?
        <div className ="feed-post-sharing">
        <div className='preloader-wrap'>
        <PreLoader/>
        </div>
        </div>
      :''}
        <GuestUserRestrictionPopup setOverlay={path[2] !== undefined && (path[2] == 'custom' || path[2] == 'my') ? false : true} />
        <div className="overlay-feed"> </div>
        <section className='feed'>
          <div class="job-popupoverlay"></div>

          {this.state.changeSchedulePopup ? this.renderChangeSchedule() : null}
          {this.state.newTrendingPostPopup ? this.trendingTagPopup() : ''}
          {this.state.openEditFeedPopup ? this.updateFeed() : ''}
          {this.state.createFeed ? this.createFeed() : ''}
          {this.state.surveyPopup && Object.keys(this.props.polls.pollQuestions).length > 0 ? this.PollQuestionPopup() : ''}


          <div className="trending-tag-feed-responsive">
            <a id="trending-tag-toggle" className="trending-button-collpase" onClick={this.openPopUpTrending.bind(this)}><i class="material-icons add-new-icon">&#xE8E5;</i></a>
          </div>
          {!this.props.location.pathname.includes("search") ?
            <Header
              //add_new_subpopup="Feed"
              title={title}
              nav={NavBar}
              feedprops={this.props}
              popup_text="Click here to add new post."
              //  add_new_button={true}
              currFeed={this.state.currFeed}
              open_filter_my_post
              currFeedPosts={this.state.currFeedPosts}
              hash={this.props.location.hash}
              location={this.props.location}
              postSelectedData={this.props.notificationData.postSelectedData}
              hastag_name={hastag_name}
              Header_onclick={this.hide_add_new_popup.bind(this)}
              trending_btn_id='trending-tag-toggle'
              page="feed"
              feedUrl={feedUrl}
              calendarUrl={calendarUrl}
              changeHeaderView={this.changeView.bind(this)}
              pageView={this.state.view}
              searchInput={this.props.searchedData}
              suggestion_selected={this.props.suggestion_selected}
              props_header={this.props.feedActions}
              openCreateFeed={this.openCreateFeed.bind(this)}
              updateFeedAction={this.updateFeedOpen.bind(this)}
              currentView={this.state.view}
              selectedFeedStatus={isFilterAppiled}
            /> : ''}

          <div className='feed-asset-popup'>
            {this.state.assetDetailPopup
              ? this.renderAssetDetail()
              : <div />}
          </div>

          <div className='main-container'>

            {/* <span
             id="add_new_button"
             onClick={this
               .handlePostCreation
               .bind(this)} /> */}

            <div id='content'>

              {
                calendarViewState ?
                  <FeedCalendar
                    location={this.props.location}
                    moment={this.state.moment}
                    feedList={this.props.feed.feedList}
                    feedUrl={feedUrl}
                    filterfeedId={this.state.selectedFeedStatus}
                    calendarUrl={calendarUrl}
                  />
                  :
                  <div>
                    <div className={`page ${this.state.currFeedPosts}-feeds `}>

                      {this.state.newPostPopup
                        ? this.renderPostCreation()
                        : <div />}
                      <div>
                        {
                          path[2] == "my" ?
                            <CollapsibleFilter>
                              <div>
                                <div className='filter-row row'>
                                  <div className='col-one-of-four'>
                                    <div className='curated-filter-inner'>
                                      <div className='form-row'>
                                        <label> Select Feed</label>
                                        <div className='select-wrapper'>
                                          <select onChange={this.selectedFeedStatus.bind(this)} id='FeedStatus' name='FeedStatus'>

                                            <option value=''>Select Feed</option>
                                            {
                                              Object.keys(feedFilter).length !== 0 ?
                                                feedFilter.map((optionData, i) => {
                                                  if (optionData.is_default == 1 && roleName == 'guest') {
                                                  } else {
                                                    return (
                                                      <option value={optionData.identity} key={optionData.identity}>{optionData.name}</option>
                                                    )
                                                  }
                                                })
                                                : ''
                                            }
                                          </select>

                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div className='col-three-of-four'>
                                  <div className='filter-button-wrapper clearfix '>
                                    {
                                      document.getElementById('FeedStatus') ?
                                        document.getElementById('FeedStatus').value !== '' ?
                                          flag = false
                                          : flag = true
                                        : ''
                                    }
                                    <a
                                      disabled={flag == true ? 'disabled' : ''}
                                      onClick={this.ApplyFeedFilter.bind(this)}
                                      className='btn btn-theme'>Apply filter
                                </a>
                                  </div>
                                </div>
                              </div>
                            </CollapsibleFilter>
                            : ''
                        }
                      </div>
                      {roleName == 'guest' && (path[2] !== 'custom' && path[2] !== 'my') ?
                        <StaticDataForGuest props={this.props} />
                        : this.FetchRender()}

                    </div>
                  </div>

              }
            </div>
          </div>
        </section>
      </div>
    )
  }
}
function mapStateToProps(state) {
  return {
    auth:state.auth,
    feed: state.feed,
    assets: state.assets,
    users: state.users,
    departments: state.departments,
    hashAssets: state.assets.hashAssets,
    disblePopup: state.posts.creation.disblePopup,
    notificationData: state.notification_data,
    posts: state.posts,
    profile: state.profile,
    tags: state.tags,
    userTags: state.tags.userTags,
    companyTag: state.tags.companyTags,
    allUserTag: state.tags.allUserTags,
    usersList: state.usersList.userList.data,
    polls: state.polls,
    aws:state.aws
  }
}

function mapDispatchToProps(dispatch) {
  return {
    feedActions: bindActionCreators(feedActions, dispatch),
    postsActions: bindActionCreators(postsActions, dispatch),
    userActions: bindActionCreators(userActions, dispatch),
    assetsActions: bindActionCreators(assetsActions, dispatch),
    headerActions: bindActionCreators(headerActions, dispatch),
    socialAccountsActions: bindActionCreators(socialAccountsActions, dispatch),
    notificationActions: bindActionCreators(notificationActions, dispatch),
    tagsActions: bindActionCreators(tagsActions, dispatch),
    searchActions: bindActionCreators(searchActions, dispatch),
    pollActions: bindActionCreators(pollActions, dispatch)

  }
}

module.exports = connect(mapStateToProps, mapDispatchToProps)(Feed)