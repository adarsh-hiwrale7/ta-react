import Globals from '../../Globals';
import PreLoader from '../PreLoader';
var separatelocation = "";

class TagPopup extends React.Component {
  render() {
    let userDetails = this.props.userDetails? this.props.userDetails: this.props.props? this.props.props.users.userDetailsSidebar:'';
    let department =  this.props.department !== undefined ?this.props.department:'';
    this.allSocialChannels = Globals
      .EXTERNAL_SOCIAL
      .slice();
    if (this.props.isFetching) {
      return <PreLoader />;
    }
    var office = '';
    var region = '';
   var country = '';
   var officecountrycomma = '';
   var countryregion = '';
   var officeregion = '';
   if( userDetails!==undefined && userDetails.length !== 0 )
   {
    if( typeof userDetails.office!=='undefined'){
      if(userDetails.office.data.region!==undefined && userDetails.office.data.country!==undefined && userDetails.office.data.name!==undefined){
          office =  userDetails.office.data.name;
          country =  userDetails.office.data.country;
          region = userDetails.office.data.region;
          separatelocation = ",";
      }
      else if(userDetails.office.data.region!==undefined && userDetails.office.data.country!==undefined){
        country =  userDetails.office.data.country;
        region = userDetails.office.data.region;
        office =  '';
        countryregion = ","
        separatelocation = " ";
      }
      else if(userDetails.office.data.region!==undefined && userDetails.office.data.name!==undefined){
        region = userDetails.office.data.region;
        office =  userDetails.office.data.name;
        country =  '';
        officeregion = ',';
        separatelocation = " ";
      }
      else if(userDetails.office.data.country!==undefined && userDetails.office.data.name!==undefined){
        office =  userDetails.office.data.name;
        country =  userDetails.office.data.country;
        region ='';
        officecountrycomma = ',';
        separatelocation = " ";
      }
    }

    //   if(typeof userDetails.Country !== 'undefined' && typeof userDetails.City !== 'undefined' && typeof userDetails.State !== 'undefined'){
    //       city =  userDetails.City.data.title;
    //       country =  userDetails.Country.data.title;
    //       state = userDetails.State.data.title;
    //       separatelocation = ",";

    //   }
    //   else if(typeof userDetails.Country !== 'undefined' && typeof userDetails.City !== 'undefined'){
    //     city =  userDetails.City.data.title;
    //       country =  userDetails.Country.data.title;
    //       state = '';
    //       countrycity = ',';
    //       separatelocation = " ";
    //   }
    //   else if(typeof userDetails.Country !== 'undefined' && typeof userDetails.State !== 'undefined'){
    //       city =  ''
    //       country =  userDetails.Country.data.title;
    //       state = userDetails.State.data.title;
    //       countrystate = ","
    //       separatelocation = " ";
    //   }
    //   else if(typeof userDetails.City !== 'undefined' && typeof userDetails.State !== 'undefined'){
    //     city =  userDetails.City.data.title;
    //     country =  '';
    //     state = userDetails.State.data.title;
    //     citystatecomma = ',';
    //     separatelocation = " ";
    // }
     else{
      // city = '';
      country =  '';
      // state = '';
      region ='';
      office='';
      separatelocation = " ";
    }
   }
 if (this.props.isDept === "department") {
      return (
        <div id="department-popup">
          <div className="inner-tool-tip">
            <div className="avtar-tool-tip-pop-up">
              <div className="avtar-background-img">
                <div>
                  <p className="department-icon-wrapper"><i class="material-icons">business</i></p>
                  <p className="department-office-wrapper">
                  {department?department.name:''}
                  <span> {department?department.office_name:''} </span> </p>
                </div>
              </div>
            </div>
            <div className="details-tool-tip-pop-up">
              <div>
               <div className="dpt-details border">
                  <p className="count-tool-tip">{department?department.total_user:''}</p>
                  <p className="label-tool-tip">USER </p>
                </div>

                <div className="dpt-details">
                  <p className="count-tool-tip">{department?department.total_post:''}</p>
                  <p className="label-tool-tip">POSTS</p>
                </div>
              </div>
            </div>
          </div>
       </div>
      )
    }

    //let social = "";
    var userimage = "";
    let social = "";
    let socialArray = [];
    let socialaccount = '';
    let facebookavtar = '';
    let linkedinavtar = '';
    let twitteravttar = '';
    if (userDetails.avatar == undefined || userDetails.avatar == '') {
      userimage = {
        backgroundImage: `url(${'https://app.visibly.io/img/default-avatar.png'})`
      }
    }
    else {
      userimage = {
        backgroundImage: `url(${userDetails.avatar})`
      }
    }

    if (userDetails.socialAccounts && userDetails !== null) {
      social = userDetails.socialAccounts.data;
      userDetails.socialAccounts.data.map((social, i) => {
        socialArray.push(social.social_media);
      });
    }
    return (

      <div className="inner-tool-tip">
        <div class="feedlefttringle"></div>
        <div className="avtar-tool-tip-pop-up">

          <div className="avtar-background-img">
            <span style={userimage} className="blur-bg" />
            <div>
               <div className="avtar-detail">
                <h4>{Object.keys(userDetails).length > 0 && userDetails.firstname
                  ? userDetails.firstname + ' ' + userDetails.lastname
                  : <span>no user found </span>}</h4>
                  {typeof this.props.isCallFromSidebaruser!=='undefined' && this.props.isCallFromSidebaruser==true?
                  <div className='userDetailData'>
                      {userDetails.CompanyRole!==undefined &&  userDetails.CompanyRole.data.title!== '' ? userDetails.CompanyRole.data.title : ''}
                    </div>:''}
                  {userDetails.email ?
                  <div className='userDetailData'>{userDetails.email}</div>
                :
                ''}
            </div>
          </div>
        </div>
      </div>
      <div className="details-tool-tip-pop-up">
        <div className='postUserPopupTable'>

          <table>
            <tbody>
              <tr>
                <td>
                  <div>
                    <p className="count-tool-tip">{userDetails.LeaderBoard ? userDetails.LeaderBoard.data.rank : 0}</p>
                    <p className="label-tool-tip">LEVEL</p>
                  </div>
                </td>
                <td>
                  <div>
                    <p className="count-tool-tip">{userDetails.LeaderBoard ? userDetails.LeaderBoard.data.total_post : 0}</p>
                    <p className="label-tool-tip">POSTS</p>
                  </div>
                </td>
                <td>
                  <div>
                    <p className="count-tool-tip">{userDetails.LeaderBoard ? userDetails.LeaderBoard.data.point : 0}</p>
                    <p className="label-tool-tip">XP</p>
                  </div>
                </td>
              </tr>
            </tbody>
          </table>


        </div>
        <div>

          <div class="FeedUserLocationDetailWrapper">
          {
          (userDetails.CompanyRole || userDetails.userDepartment )?
            <div className='FeedUserLocationDetail'><i class="material-icons">business</i><span className='FeedUserLocationDetailText'>
            {typeof this.props.isCallFromSidebaruser!=='undefined' && this.props.isCallFromSidebaruser==true?
                (userDetails.userDepartment) ? userDetails.userDepartment.data.name : ''
              : <div>{(userDetails.CompanyRole) ? <span> {userDetails.CompanyRole.data.title} - </span> : ''}{(userDetails.userDepartment) ? userDetails.userDepartment.data.name : ''}</div>}
            </span></div>
          :
          ''
          }

          { userDetails.length != 0 ?
           typeof userDetails.region !== 'undefined' || typeof userDetails.country !== 'undefined' || typeof userDetails.office !== 'undefined' ?
           <div className='FeedUserLocationDetail'><i class="material-icons">location_on</i><span className='FeedUserLocationDetailText'>{office}{separatelocation}{officecountrycomma} {officeregion}{country}{separatelocation} {countryregion}{region}</span></div>
           :''
          :''
        }

        </div>
          <div className='FeedUserPopupSocialWrapper'>
           {Object.keys(userDetails).length > 0 || userDetails.firstname ?
             <ul className='FeedUserPopupSocialList'>
                {userDetails.socialAccounts !== undefined ?userDetails.socialAccounts.data.length > 0 ?
                     userDetails.socialAccounts.data.map((data , index) => {
                       //this map  for  socialaccount user details
                       data.user_social.data.map((userdetails , index) =>
                        {
                           //for twitter user avatar
                            data.social_media == "twitter" ?
                               userdetails.key == 'avatar'  ?
                                 twitteravttar = userdetails.value

                               :''
                             : ''
                             //for facebook user avatar
                             data.social_media == "facebook" ?
                                userdetails.key == 'avatar' ?
                                facebookavtar = userdetails.value
                            :''
                           :''
                            //for linkdnln user avatar
                           data.social_media == "linkedin"  ?
                             userdetails.key == 'avatar' ?
                               linkedinavtar = userdetails.value
                              :''
                               :''
                        }
                       )

                     }):''

                 :''}
                {this
                  .allSocialChannels

                  .map((notConnectedSocialAcc, i) => {
                    return (

                      <li key={i}>

                       {notConnectedSocialAcc == 'facebook' ? (socialArray.includes("facebook") ?

                           <a className="scoial-img-popup">
                           <img src={facebookavtar} onError = {(e)=>{e.target.src="https://app.visibly.io/img/default-avatar.png"}}></img>
                           <i className='fa fa-facebook fb'></i>
                         </a>


                         :
                           <a className="scoial-img-popup">
                            <img src="https://app.visibly.io/img/default-avatar.png"></img>
                            <i className='fa fa-facebook dis'></i>
                          </a>

                        ) : null}
                        {notConnectedSocialAcc == 'twitter' ? (socialArray.includes("twitter")
                          ?

                         <a className="scoial-img-popup">
                           <img src={twitteravttar}  onError = {(e)=>{e.target.src="https://app.visibly.io/img/default-avatar.png"}}></img>
                         <i className='fa fa-twitter tw'></i>
                         </a>

                          :
                          <a className="scoial-img-popup">
                          <img src="https://app.visibly.io/img/default-avatar.png"></img>
                          <i className='fa fa-twitter dis'></i>
                        </a>

                        )
                          : null}

                        {notConnectedSocialAcc == 'linkedin' ? (socialArray.includes("linkedin")
                          ?


                          <a className="scoial-img-popup ln dis">
                          <img src={linkedinavtar} onError = {(e)=>{e.target.src="https://app.visibly.io/img/default-avatar.png"}}></img>
                          <i className='fa fa-linkedin ln'></i>
                        </a>

                          :
                          <a className="scoial-img-popup">
                          <img src="https://app.visibly.io/img/default-avatar.png"></img>
                          <i className='fa fa-linkedin dis'></i>
                        </a>

                        )
                          : null}
                      </li>
                    )
                  })}
              </ul>
              : ''}
          </div>

        </div>

      </div>
      </div>


    )
  }
}

module.exports = TagPopup;
