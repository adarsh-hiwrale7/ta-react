import { connect } from 'react-redux'
import { Link } from 'react-router'
import { Field, reduxForm } from 'redux-form'
import PreLoader from '../../PreLoader'
import * as utils from '../../../utils/utils'
import CenterPopupWrapper from '../../CenterPopupWrapper';

class PollQuestions extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
        successClass: '',
        blurInitClass: ''
    }
  }
  componentWillReceiveProps(nextProps) {
    if(nextProps.pollsData.surveySaveSuccess !== this.props.pollsData.surveySaveSuccess && nextProps.pollsData.surveySaveSuccess==true){
      this.setState({successClass:'Success-quick-survery'})
      document.body.classList.remove('pollPopup');
      //fade out survey 
      var me = this;
      setTimeout(function(){
        $(".quick-survey-post").fadeOut(1000)
          me.props.fetchSurvey();
      }, 3000);
    }

    if(nextProps.pollsData.saveSurveyLoading !== this.props.pollsData.saveSurveyLoading && nextProps.pollsData.saveSurveyLoading){
        //put any classname for blurr
        //this.setState({blurInitClass:''})
    }

  }
  ComponentDidMount(){
    
    
  }
  //closePollQuestionPopup
  renderPollQuestion(propData){
      return (
          <div className={propData.pollsData.saveSurveyLoading ? "poll-main-container please-wait" : "poll-main-container"}>
          <div className="loader"><img src="/img/visibly-loader.gif" alt="visibly-loader" /></div>
          <div className="post-pic-ranking-container">
                  <div className="render-ranking-only-text-container">
                    <div className="post-rating-main">
                      <div className="post-rating-voting-text-part-wrapper clearfix">
                        <div className="post-rating-value-lable">
                          <div className="" onClick={propData.openPollQuestionPopup}>
                            <div className="value-container-ranking"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24"><path fill="none" d="M0 0h24v24H0V0z" /><path d="M18 9l-1.41-1.42L10 14.17l-2.59-2.58L6 13l4 4zm1-6h-4.18C14.4 1.84 13.3 1 12 1c-1.3 0-2.4.84-2.82 2H5c-.14 0-.27.01-.4.04-.39.08-.74.28-1.01.55-.18.18-.33.4-.43.64-.1.23-.16.49-.16.77v14c0 .27.06.54.16.78s.25.45.43.64c.27.27.62.47 1.01.55.13.02.26.03.4.03h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zm-7-.25c.41 0 .75.34.75.75s-.34.75-.75.75-.75-.34-.75-.75.34-.75.75-.75zM19 19H5V5h14v14z" /></svg>
                              <span>Quick Pulse</span></div>
                          </div>
                          {/*<div className="valueDescriptionPopup">
                            <div className="valueDescriptionPopupInner">having insightful work  , having insightful work  , having insightful work  , having insightful work , having insightful work , having insightful work , having insightful work , v, having insightful work </div>
                          </div>*/}
                        </div>
                        <div className="post-rating-desc-part"> {propData.pollsData.pollQuestions.question}</div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="quick-suvery-content">
                 <div className = {'inner-quick-suvery-content '+ this.state.successClass}> 
                 <div className = "clearfix quick-suvery-content-wrapper">
                  
                  <div className="quick-survery-options ">
                    {Object.keys(propData.pollsData.pollQuestions.options).length > 0 ? 
                    <ul className="cf">
                      {
                        propData.pollsData.pollQuestions.options.map((answer, index) => (
                          <li key={index} onClick={this.props.handleSurveySave.bind(this,answer,propData.pollsData.pollQuestions.identity)}>{answer.option_name}</li>
                        ))
                      }
                    </ul>
                    :''}
                  </div>
                  <div className="quick-survey-illustration">
                    <div className="inner-quick-survey-illustration">
                      <img 
                        src={propData.pollsData.pollQuestions.image_url !== null ? propData.pollsData.pollQuestions.image_url : "/img/survey.png"} 
                        onError={(e)=>{e.target.src="/img/survey.png"}}
                      />
                    </div>
                  </div>
                  </div>
                  <div className = "success-msg-quick-suvery-content"> 
                    <div className = "success-text"> 
                     <p> Thank you <span className = "smile-face-quick-suvery">&#9786;</span>  </p> </div>
                 </div>
               </div>
            </div>
            </div>
      )
  }
  
  render () {
    return (
            <div className="quick-survey-post post" id="survey-poll-question">
             {this.props.openPopup == 'yes' ? 
                 <CenterPopupWrapper><button id="close-popup" className="btn-default close-popup"><i className="material-icons" onClick={this.props.closePollQuestionPopup}>clear</i></button>{this.renderPollQuestion(this.props)}</CenterPopupWrapper>  
              : this.renderPollQuestion(this.props)
             }
           
            
        </div>
    )
  }
}

function mapStateToProps (state) {
  return {
  }
}
function mapDispatchToProps (dispatch) {
  return {}
}
let connection = connect(mapStateToProps, mapDispatchToProps)
var reduxConnectedComponent = connection(PollQuestions)

export default reduxForm({
  form: 'PollQuestions' // a unique identifier for this form
})(reduxConnectedComponent)


