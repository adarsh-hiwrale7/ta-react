;
import { Link } from "react-router";
import { browserHistory } from "react-router";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Field, reduxForm } from 'redux-form';
import { renderField } from '../Helpers/ReduxForm/RenderField';
import Globals from '../../Globals';
import PreLoader from '../PreLoader'
import UpdateFeedDetailForm from './UpdateFeedDetailForm'
import UpdateFeedForm from './UpdateFeedForm'
import CenterPopupWrapper from '../CenterPopupWrapper'

const required = value => value
  ? undefined
  : 'Required';

class UpdateFeedPopup extends React.Component {

  constructor(props) {
    super(props);
    this.state={
      selectedfeedtab:"members",
      finishFeedUpdate:false
    }
  }
  componentWillMount() {
    var me= this
    //OnClick of Outside it will close Popup
    window.addEventListener("click", function(event) {
      if (event.target.className.includes("popup-center-wrapper")) {
        me.closeUpdateFeedPopup();
      }
    });
  }
  componentDidMount() {
    //to active detail tab if update feed is calling for default feed
    if(this.props.feedId == Globals.defaultFeedId){
      this.setState({
        selectedfeedtab:'feedDetail'       
      })
    }
  }
  handleInitialize () {}
  componentWillReceiveProps (nextProps) {
    if (nextProps.feed.finishFeedUpdate == true)
     {
      this.setState({
        finishFeedUpdate:nextProps.feed.finishFeedUpdate  
      })
      this.closeUpdateFeedPopup();
     }
  }
  closeUpdateFeedPopup() {
    this.props.closeUpdateFeedPopup();
  }
  formSubmit(val){
    this.props.handleSubmit(val)
  }
  removeFromFeed(user,feedId){
    this.props.removeFromFeed(user,feedId);
  }

   
  feedUpdateTab(tab){
    this.setState({
      selectedfeedtab:tab
      
    })
   
  }

  handleSubmit(val){
    this.props.handleUpdateSubmitFeed(val);
  }

  handleDetailSubmit(val){
      this.props.handleDetailSubmit(val);
  }

  resetUpdateForm(){
     this.props.resetUpdateForm(); 
  }
  displayUser(identity){
    document.getElementById(identity).classList.toggle("hide");
  }
renderDepartmentDetails(){
  var usersData = Object.keys(this.props.feed.feedDetail.users).length >0? this.props.feed.feedDetail.users :''
  return(
    <div className="department-mainbox">
      <div className="department-name">
        <h3> Departmets </h3>
      </div>
     {usersData !=='' && usersData.department_tag.map((Department, i) => {
        return(
          <div>
            <tr>
            <td> 
              <div class="feedrowname department-feedname">
                {Department.name}
              </div>
            </td>
            <td>
              <div className="feed-user-list department-user-list">
              <a onClick={this.displayUser.bind(this,Department.identity)}>
                UserList<i class="material-icons">arrow_drop_down</i>  
              </a>
              </div>
              </td>
            <div class="feedrowremoveicon department-remove-icon"> 
              <button id="close-popup" class="btn-default" onClick={this.removeFromFeed.bind(this,Department,this.props.feedId)}><i class="material-icons">clear</i></button>
            </div>
            </tr>
            <div id={Department.identity} className="hide DepartmentList">
            {Object.keys(Department.users).length > 0 ? 
              Department.users.map((users,i)=>{
                return(
                  <tr key={i}>
                  <td>
                    <div class="feedrowimage">
                      <img src={users.profile_image} onError={(e)=>{e.target.src="https://app.visibly.io/img/default-avatar.png"}} />
                    </div>
                  </td>
                  <td colSpan = "4"> 
                    <div class="feedrowname">
                      {users.first_name} {users.last_name}
                    </div>
                  </td>
                  <td>
                    <div class="feedrowemail">
                    {users.email}
                    </div>
                  </td>
                  <td>
                    <div class="feedrowmember">{users.role_name}</div>
                  </td>
                  
                </tr>
                )
              })
            :<tr>
              <td >
                  <div>
                     No user found.
                  </div>
              </td>
            </tr>
           }
           </div>
          </div>
        )
      })}
      </div>
  )
}
  renderCompanyDetails(){
    var Company = Object.keys(this.props.feed.feedDetail.users).length >0 ?
      this.props.feed.feedDetail.users.company_tag :[];
    
    if(Company.length!==0){
      return( 
      <div className="feed-popup-company-mainbox">
          <div className="company-heading">
            <h3> All Company </h3>
          </div>
          <div>
            <tr>
            <td> 
              <div class="feedrowname department-feedname">
                {Company.name}
              </div>
            </td>
            <td>
            <div className="feed-user-list department-user-list">
              <a onClick={this.displayUser.bind(this,Company.identity)}>
                UserList<i class="material-icons">arrow_drop_down</i> 
              </a>
              </div>
            </td>
            <div class="feedrowremoveicon department-remove-icon"> 
              <button id="close-popup" class="btn-default" onClick={this.removeFromFeed.bind(this,Company,this.props.feedId)}><i class="material-icons">clear</i></button>
            </div>
            </tr>
            <tr>
            <td colSpan = "5">
            <div id={Company.identity} className="hide CompanyList">
            {Object.keys(Company.users).length > 0 ? 
              Company.users.map((users,i)=>{
                return(
                  <tr key={i}>
                  <td>
                    <div class="feedrowimage">
                      <img src={users.profile_image} onError={(e)=>{e.target.src="https://app.visibly.io/img/default-avatar.png"}} />
                    </div>
                  </td>
                  <td> 
                    <div class="feedrowname">
                      {users.first_name} {users.last_name}
                    </div>
                  </td>
                  <td>
                    <div class="feedrowemail">
                    {users.email}
                    </div>
                  </td>
                  <td>
                    <div class="feedrowmember">{users.role_name}</div>
                  </td>
                  
                </tr>
                )
              })
            :<tr>
              <td >
                  <div>
                     No user found.
                  </div>
              </td>
            </tr>
           }
          </div>
          </td>
          </tr>
          </div>
      </div>
    )}
  }

  render() {
    var feedDetail = Object.keys(this.props.feed.feedDetail).length  > 0 ? this.props.feed.feedDetail : '';
    var usersData =  feedDetail !== '' ? Object.keys(this.props.feed.feedDetail.users).length >0? this.props.feed.feedDetail.users :'':'';
    return (
 <CenterPopupWrapper className="updateFeedPopup">
          <div class="customfeedMain">
          {/* <div className="updatefeedoverlay"></div> */}
              <div class="createCustomfeedpopup">
                  {this.props.feed.custom.createLoading === true ? <PreLoader className="Page"/> : null}
                  <div class="planChangePopup">
                      <header class="heading clearfix">
                      
                   <h3>{feedDetail !== '' ? this.props.feed.feedDetail.name : ''}</h3>
                    {/*this.state.selectedfeedtab=='feedDetail'?
                      <a href="#"><i class="material-icons">delete_outline</i></a>:'' */}
                         <button id="close-popup" class="btn-default" onClick={this.closeUpdateFeedPopup.bind(this)}><i class="material-icons">clear</i></button>
                      </header>
                
                    <div class="widget" id="setting_walkthrough">
                
                     <div class="inner-popup">
                        <section class="register-page">
                          <div class="listMenu">
                            <nav class="nav-side">
                                <ul class="parent">
                                {this.props.feedId !== Globals.defaultFeedId ? 
                                  <li>
                                    <a href="javascript:;" onClick={this.feedUpdateTab.bind(this,"members")} className={this.state.selectedfeedtab=='members'?"active":''}><span class="text" data-toggle="tab">Members</span></a>
                                  </li>:''}
                                  <li>
                                    <a href="javascript:;" onClick={this.feedUpdateTab.bind(this,"feedDetail")} className={this.state.selectedfeedtab=='feedDetail'?"active":''} ><span classNamne="text" data-toggle="tab">Detail</span></a>
                                  </li>
                                   
                                </ul>
                            </nav>
                          </div>
                        </section>
                    </div>
                    {this.state.selectedfeedtab=='members' && this.props.feedId !== Globals.defaultFeedId ?
                    <div className="memberfeed">
                    <div class="feedDetailCover">
                        <div class="feedDetail">
                        <div className="feed-user-detail">
                        <h3>Users</h3>
                        </div>
                          <div class="feedrow clearfix">
                            <table>
                              <thead>
                                    {feedDetail !== '' && usersData !=='' && usersData.users.length > 0 ?
                                        usersData.users.map((users, i) => {
                                          var name = users.first_name + " " +users.last_name
                                                  return(
                                                    <tr key={i}>
                                                    <td>
                                                      <div class="feedrowimage">
                                                        <img src={users.profile_image}  onError={(e)=>{e.target.src="https://app.visibly.io/img/default-avatar.png"}}/>
                                                      </div>
                                                    </td>
                                                    <td> 
                                                      <div title={name} class="feedrowname">
                                                      {name.length>25?
                                                         `${name.substring(0, 25)}...`: name
                                                      }
                                                      </div>
                                                    </td>
                                                    <td>
                                                      <div title={users.email} class="feedrowemail">
                                                      {users.email.length>25?
                                                         `${users.email.substring(0, 25)}...`: users.email
                                                      }
                                                      </div>
                                                    </td>
                                                    <td>
                                                      <div title={users.role_name} class="feedrowmember">
                                                      {users.role_name.length>20?
                                                         `${users.role_name.substring(0, 20)}...`: users.role_name
                                                      }
                                                      </div>
                                                    </td>
                                                    
                                                    <td>
                                                    {users.role_identity !== "rpPDp" && users.identity!==this.props.feed.feedDetail.owner_id && users.identity!==this.props.loginId?
                                                      <div class="feedrowremoveicon">
                                                          <button id="close-popup" class="btn-default" onClick={this.removeFromFeed.bind(this,users,this.props.feedId)}><i class="material-icons">clear</i></button>
                                                      </div>
                                                      :''}
                                                    </td>
                                                    
                                                  </tr>
                                                  )
                                        })    
                                    : ''} 
                                    <tr>
                                      <td colSpan = "5">
                                    {
                                      feedDetail !=='' ?
                                      this.renderCompanyDetails()
                                      :''
                                    }
                                      </td>
                                    </tr>
                                    <tr>
                                      <td colSpan ="5">
                                    {
                                      feedDetail !=='' && usersData !=='' && usersData.department_tag.length > 0 ?
                                      this.renderDepartmentDetails()
                                      :''
                                    }
                                    </td>
                                    </tr>

                               </thead>  
                            </table>
                         </div>       
                         </div>
                    </div>
                    <div class="inviteGuest">
                      <h3>Invite more</h3>
                    </div>
                        <UpdateFeedForm
                          feed={this.props.feed}
                          onSubmit={this.handleSubmit.bind(this)}
                          feedId={this.props.feedId}
                          userTags={this.props.userTags}
                          resetUpdateForm={this.resetUpdateForm.bind(this)}
                        />
                    </div>
                    : 
                        <UpdateFeedDetailForm
                            feedDetail={this.props.feed.feedDetail}
                            feedId={this.props.feedId}
                            onSubmit={this.handleDetailSubmit.bind(this)}
                        />
                    }
                    
                 
                 </div>
             
            
              </div>
          </div>
      </div>
      </CenterPopupWrapper>
    );
  }
}



function mapStateToProps(state, ownProps) {
  return {}
}

function mapDispatchToProps(dispatch) {
  return {

  }
}
let connection = connect(mapStateToProps, mapDispatchToProps)

var reduxConnectedComponent = connection(UpdateFeedPopup)

export default reduxForm({
  form: 'UpdateFeedPopup' // a unique identifier for this form
})(reduxConnectedComponent)

