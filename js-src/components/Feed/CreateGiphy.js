import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'

import {Field, reduxForm} from 'redux-form'

import * as utils from '../../utils/utils'
import * as feedActions from '../../actions/feed/feedActions'
import * as assetsActions from '../../actions/assets/assetsActions'
import * as s3Actions from '../../actions/s3/s3Actions'
import * as s3functions from '../../utils/s3'
import ReactDOM from 'react-dom'
import PreLoader from '../PreLoader'
import ImageLoader from 'react-imageloader'
import PreLoaderMaterial from '../PreLoaderMaterial'
import {Component, PropTypes} from 'react'
const required = value => (value
  ? undefined
  : 'Required')

class CreateGiphy extends React.Component {

  log(e) {
    $('.iUmGPz').wrap('<div class="imgwrapper"><div class="inner_imgwrapper"></div></div>')
    var imgtag = document.getElementsByTagName('img')
    for (var i = 0; i < imgtag.length; i++) {
      imgtag[i]
        .classList
        .remove('selectedimg')
      if (imgtag[i].src == e.fixed_width.url) {
        imgtag[i].className += ' selectedimg'
        $('.selectedimg')
          .parent('div')
          .addClass('pickerwrapper')
      } else {
        imgtag[i]
          .classList
          .remove('selectedimg')
      }
    }
    this
      .props
      .selectgiphy(e.fixed_width.url)
  }
  componentDidMount() {
    this.handleInitialize()
    this
      .props
      .feedActions
      .fetchGiphy()
  }
  handleInitialize() {
    const initData = {
      giphy_search: this.state.giphysearch
    }
  }
  renderLoading() {
    return (
      <div>
              <div className='imgwrapper'>
                  <div className='inner_imgwrapper'>
                      <div className = "loader-grey-line createPostgiphyLoader"> </div>
                  </div>
              </div>
              <div className='imgwrapper'>
                  <div className='inner_imgwrapper'>
                      <div className = "loader-grey-line createPostgiphyLoader"> </div>
                  </div>
              </div>
      </div>
    )
  }
  componentWillMount() {}

  constructor(props) {
    super(props)

    this.state = {
      selectedgiphyvalue: null,
      giphysearch: '',
      name: ''
    }
  }
  selectedgiphy(e) {
    $('.selectedimg').removeClass('selectedimg');
    $('.pickerwrapper').removeClass('pickerwrapper');
    $('#giphySelectBtn').addClass('hide');
    $('#giphySelectBtnDisable').removeClass('hide');
    var mygiphy = JSON.parse(e.target.getAttribute('data-attribute'));
    var imgtag = document.getElementsByClassName('iUmGPz')
    if(mygiphy)
    {
    for (var i = 0; i < imgtag.length; i++) {
      if (imgtag[i].src == mygiphy.images.original.url) {
        imgtag[i].className += ' selectedimg'
        $('.selectedimg')
          .parent('div')
          .addClass('pickerwrapper');
        $('#giphySelectBtn').removeClass('hide');
        $('#giphySelectBtnDisable').addClass('hide');
      }
    }
    this
      .props
      .selectgiphy(mygiphy)
  }else{
    this
      .props
      .selectgiphy(mygiphy)
  }
  }
  handleSearchGiphy(event) {
    this.setState({name: event.target.value})
  }

  enterPressed(event) {
    // var code = event.keyCode || event.which;
    // if (code === 13) { //13 is the enter keycode
      var mysearchvalue = event.target.value
      this
        .props
        .feedActions
        .fetchSearchGiphy(mysearchvalue)
    // }
  }
  hideInvalidGif(e){
    if(e.target.id){
      document.getElementById(e.target.id).parentElement.parentElement.classList.add('hide');
    }
  }
  render() {
    var me = this
    var postallgiphy = []
    var giphydetail = ''
    if (me.props.feed.postGiphy.length !== 0) {
      postallgiphy = me.props.feed.postGiphy
    } else if (me.props.feed.postsearchGiphy.length !== 0) {
      postallgiphy = me.props.feed.postsearchGiphy
    }
    return (
      <div className='section clearfix'>
        {/* <Picker onSelected={this.log.bind(this)} id="test"/> */}
        <div className='RTdSn'>
          <div className='bzojac'>
            <input
              type='text'
              name='giphy_search'
              autoCapitalize='none'
              autoComplete='off'
              autoCorrect='off'
              placeholder='Search for gifs'
              className='hMDWtc search_giphy'
              onKeyUp={me
              .enterPressed
              .bind(this)}
              onChange={me
              .handleSearchGiphy
              .bind(this)}/>

            <div className='ciFBQa'>
              {postallgiphy.length == 0
                ? this.renderLoading()
                : postallgiphy
                .map(function (giphy, i) {
                  return (
                    <div key={i}>
                      <div className='imgwrapper' key={i}>
                      <div
                        className='inner_imgwrapper' onClick={me.selectedgiphy.bind(me)}>
                        <img
                           className='giphy-gif iUmGPz'
                           src={giphy.images.original.url}
                           id={giphy.id}
                           data-attribute={JSON.stringify(giphy)}
                           onError={(e) => me.hideInvalidGif(e)} />
                      </div>
                    </div>
                    
                    </div>
                  )
                })}
            </div>


            
          </div>
        </div>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {aws: state.aws, feed: state.feed}
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(Object.assign({}, assetsActions, s3Actions), dispatch),
    feedActions: bindActionCreators(feedActions, dispatch)
  }
}
let connection = connect(mapStateToProps, mapDispatchToProps)

var reduxConnectedComponent = connection(CreateGiphy)

export default reduxForm({
  form: `CreateGiphy` // a unique identifier for this form
})(reduxConnectedComponent)
