import notie from "notie/dist/notie.js";
import ReactDOM from "react-dom";
import ImageLoader from "react-imageloader";
import Script from "react-load-script";
import { connect } from "react-redux";
import { browserHistory, Link } from 'react-router';
import { bindActionCreators } from "redux";
import { Field, reduxForm } from "redux-form";
import * as assetsActions from "../../actions/assets/assetsActions";
import * as cultureActions from '../../actions/cultureActions';
import * as postsActions from "../../actions/feed/postsActions";
import * as generalActions from "../../actions/generalActions";
import * as s3Actions from "../../actions/s3/s3Actions";
import * as channelActions from "../../actions/socialChannelActions";
import * as tagsActions from "../../actions/tagsActions";
import * as userActions from "../../actions/userActions";
import Globals from "../../Globals";
import * as s3functions from "../../utils/s3";
import * as utils from "../../utils/utils";
import Asset from "../Assets/Asset";
import reactDatepickerChunk from "../Chunks/ChunkDatePicker"; // datepicker chunk
import emojiPicker from "../Chunks/ChunkEmojiPicker";
import moment from "../Chunks/ChunkMoment";
import PhotoEditorSDK from "../Chunks/ChunkPhotoEditor";
import reactTooltip from "../Chunks/ChunkReactTooltip";
import MentionTextArea from "../Helpers/ReduxForm/MentionTextArea";
import ReactDatePicker from "../Helpers/ReduxForm/ReactDatePicker"; // react datepicker field
import ReactTimePicker from "../Helpers/ReduxForm/ReactTimePicker"; // react datepicker field
import { renderField } from "../Helpers/ReduxForm/RenderField";
import PreLoader from "../PreLoader";
import PreLoaderMaterial from "../PreLoaderMaterial";
import SliderPopup from "../SliderPopup";
import CreateGiphy from "./CreateGiphy";
import CreatePostFormDropzone from "./CreatePostFormDropzone";
import CultureValue from "./CultureValue";
import GiphyPost from "./GiphyPost";
import PhotoEditor from "./PhotoEditor";
import ValueAI from "./ValueAI";


const Line = require("rc-progress").Line;

var cntnum = 0;
var count_limits = 280;
var twitterWordLimit = Globals.TWITTER_LIMIT_WORD;
var selectGipy_child = null;
var twitter = false;
var count = 0;

var lastCursorIndex = 0;
var textFocus = false;
var var_selected_asset = "";
var var_selected_asset_detail = "";
var var_asset = {};
var datachange = false;
var assetUploadFlag = false;
var showDriveUpload = false;
var webLinkmain = "";
var selectionstart;
var selectionend;
var mainUtmUrl;
var utmParameter = "?";
var freeStorage = 0;
const required = value => (value ? undefined : "Required");
var textvalue = "";
var emojitest = "";
var media_selected = false;
var selectedAssetDetails;
var giphy_selected = false;
var createPostMainContainer;
var txtarea;
var start;
var end;
var checked_social_twitter = false;
var fileUrl = "";
var fileId = "";
var fileName = "";
var descriptionEditPost = "";
var allDetailsEditPost = "";
var editPostFlag = 0;
var cancleflag = 0;
var assetType = "";
var gifType = null;
var roleName = "";
var selectedMediaSizeTotal = 0; //for storage
var uploadedeDriveFileData = [];
var isDataChanged = [];
var isValidUrl = true;
let totalAssetsCount = 0;
let tempAssetCount = 0;
var gifCount = 0;
var AssetgifCount = 0;
var driveGifCount = 0;
var docCount = 0;
var driveDocCount = 0;
var assetDocCount = 0;
var disableEditPostDataTemp=false
var mediaExtensionList=[]

class CreatePostForm extends React.Component {
  log(gif) {}
  constructor(props) {
    super(props);

    this.state = {
      showValuePopup:false,
      createPostPopup:true,
      startDate: null,
      ReactDatePicker: null,
      Moment: null,
      emojiPicker: null,
      Social: [],
      Users: [],
      assets: [],
      openEditor: false,
      currPage: 1,

      file: null,
      media_wrapper_class: "",
      meta: {
        pagination: {
          total_pages: 1
        }
      },

      type: "Internal", // Internal OR External post type

      selectedAsset: {}, // selected asset main details
      selectedAssetDetails: null, // selected asset full details

      blDirectFileUpload: false, // is media uploaded through Dropzone
      directFileUploadObj: null, // dropzone uploaded object
      uploadedFile: [], // details of the uploaded file
      selectedfile: null,
      uploadedGiphyFile: null,
      selectedAssetSize: null,

      dynamicSchedulers: ["scheduler"],
      removedSchedulers: [],
      schedulerCounter: 1,

      imageSelection: {
        active: false,
        dropzoneScreen: true,
        assetsScreen: false
      },
      showScheduler: false,
      showMediaInterface: false,
      showGiphy: false,
      showEmojiPicker: false,
      showCropInterface: false,
      uploadCrop: null,
      showCroppedResult: true,
      uploadInProgress: false,
      showValues: false,
      loading: false,
      done: false,
      scrollTimer: null,
      showTwitterMsg: false,
      twitter: false,
      cntnum: cntnum,
      limitover: false,
      selectedGiphy: false,
      selectedEmoji: "",
      buttondisabled: false,
      tagsUser: [],
      textemojivalue: null,
      description: "",
      emoji_code: "",
      displaymedia: false,
      wallCheckbox: ["internal"],
      media_select_btn: false,
      giphy_selected_btn: false,
      selectGipy_child_state: null,
      layoutDetail: null,
      DriveAssetsData: null,
      clickTimer: 0,
      currentCategoryId: null,
      currentFolderId: null,
      isFromCategory: true,
      currentAssetTab: "category",
      currentPostAssetTab: "assetLibrary",
      assetUploadState: false,
      create_btnchange: true,
      create_btnchange_desc: false,
      changeDisplayForAssetSection: false,
      mimetype: [],
      clientId: Globals.google_client_id,
      appId: Globals.google_app_id,
      developerKey: Globals.google_developer_key,
      scope: Globals.google_app_scope,
      pickerApiLoaded: false,
      oauthToken: "",
      renderDriveUI: false,
      DriveData: "",

      utm_campaign: "social",
      utm_source: "visibly",
      utm_medium: "visibly_web",
      utmLinkData: [],

      create_btnchange_desc: true,
      changeDisplayForAssetSection: false,
      disableStateButton: false,
      editData: false,
      descriptionEditPost: "",
      freeStorage: "",
      storeFields: "",
      isUploadButtonClicked: false,
      uploaded: "",
      photoEditor: null,
      isValueSelected: false,
      tooltip: null,
      showUTMPopup: false,
      totalImageCount: 0,
      uploadedeDriveFileData: [],
      editPostAssetData:[]
    };

    this._handleCloseEvent = this._handleCloseEvent.bind(this);
    this.assetSelect = this.assetSelect.bind(this);
    this.backPopUp = this.backPopUp.bind(this);


    // this.editDataState = this.editDataState.bind(this)
  }

  // list of video types to be added when thubnail is ready
  // 'video/mp4','video/mpeg','application/vnd.google-apps.video'
  loadMoreAssets() {
    var me = this;

    if (typeof me.state.meta.pagination !== "undefined") {
      var nextpage = parseInt(me.state.meta.pagination.current_page) + 1;
      me.setState({ currPage: nextpage });
      me.fetchAssets(
        this.state.currentCategoryId,
        this.state.currentFolderId,
        10,
        nextpage
      );
    }
  }
  /** Add user tags  */
  addTagsUser(id, display) {
    let tagsUserObject = { tagid: id, tagName: display, type: "user" };
    this.setState({
      tagsUser: [...this.state.tagsUser, tagsUserObject]
    });
  }
  restrictToRemoveSavedHashtag(e){
    if(this.props.campaignId){
      var selectedText = window.getSelection().toString();
      var savedHashtagDetail=this.props.savedHashTagListInPost.hashTagListInCreatePost &&  Object.keys(this.props.savedHashTagListInPost.hashTagListInCreatePost).length >0? this.props.savedHashTagListInPost.hashTagListInCreatePost.detail:''
      let trimmedTag = savedHashtagDetail !==''? savedHashtagDetail.replace(/#/g, ''):''
      if(trimmedTag !==''){
        trimmedTag = trimmedTag.replace(/ +/g, "")
        selectedText = selectedText.replace(/ +/g, "")
        let validSelection = selectedText.includes(trimmedTag)
        if(validSelection && selectedText !==''){
          window.getSelection().removeAllRanges()
        }
      }
      if (e.keyCode == 8 && $('#post_description').is(":focus") &&  $('#post_description').val().length < savedHashtagDetail.length) {
            e.preventDefault();
        }
      }
  }
  /**
   * @author disha
   * @param {*} e 
   * get all textaream data string and  replace tag with tag name and count its length
   */
  trimTagAndPassCount(e){
    delete e.preventDefault;
    delete e.__proto__;
    var str=Object.values(e).join('');
    var rx1 = /@\[([^\]]+)]\(([^)]+)\)/g;
    let res = str.match(rx1);
    if (res) {
        var tagsArray = str.split("@[");
        var nameArray = tagsArray[1].split("]");
        str = str.replace(rx1, nameArray[0]);
    }
    var stringCount=str.length
    this.restrictToRemoveSavedHashtag(e)
    this.focustextbox(this,stringCount);
  }
  focustextbox(e,stringCount=null) {
    lastCursorIndex =
      document.getElementsByName("description")[0].selectionStart + 1;
      txtarea = document.getElementsByName("description")[0];

    
    
    start = txtarea.selectionStart;
    end = txtarea.selectionEnd;
    this.TwitterCount(count,stringCount);
    if (start > 0) {
      this.setState({ create_btnchange_desc: false });
    } else {
      this.setState({ create_btnchange_desc: true });
    }
  }
  TwitterCount(count,stringCount=null) {
    var lengthcount='';
    if(stringCount==null ){
      var descripationcount = document.getElementsByName("description")[0].value;
      lengthcount = descripationcount.length;
    }else{
      lengthcount=stringCount
    }
    this.setState({
      cntnum: Globals.TWITTER_LIMIT_WORD - lengthcount
    });
    var twiterEmojiOverlay = document.getElementsByClassName(
      "twitter-emoji-container"
    )[0];
    twiterEmojiOverlay.classList.remove("twiter-emoji-overlay");
    // if (lengthcount > Globals.TWITTER_LIMIT_WORD && this.state.twitter) { <-- to use state twitter , uncommnet this and commnet below

    if (lengthcount > Globals.TWITTER_LIMIT_WORD && checked_social_twitter) {
      twiterEmojiOverlay.classList.add("twiter-emoji-overlay");
      var count_limits_over = document.getElementById("twiter_limit_count");

      if (
        !count_limits_over.className.includes("twiter_limits_count_display")
      ) {
        count_limits_over.className += " limitover twiter_limits_count_display";
      }
      if (!count_limits_over.className.includes("limitover")) {
        count_limits_over.className += " limitover";
        this.setState({ showTwitterMsg: true });
        this.setState({ limitover: true });
      }
      this.setState({
        cntnum: twitterWordLimit - lengthcount
      });
    } else {
      var count_limits_over = document.getElementById("twiter_limit_count");
      if (checked_social_twitter) {
        twiterEmojiOverlay.classList.add("twiter-emoji-overlay");
      }
      var count_limits_over = document.getElementById("twiter_limit_count");
      count_limits_over.classList.remove("limitover");
      this.setState({ showTwitterMsg: false });
      this.setState({ limitover: false });
    }
  }
  checkLimitPostDesc(value) {
    var textvalue = document.getElementsByName("description")[0].value;
    var startPos = document.getElementsByName("description")[0].selectionStart;
    //  alert(startPos + ", " + endPos);
    var textemoji = "";
    if (txtarea !== null && txtarea.value == null) {
      txtarea.focus();
    } else {
      if (value !== undefined && txtarea !== null) {
        if (value.shortname !== undefined) {
          var emojiUnicode = utils.toUnicode(value.unicode);
          var sel = txtarea.value.substring(start, end);
          var finText =
            txtarea.value.substring(0, start) +
            String(emojiUnicode) +
            sel +
            txtarea.value.substring(end);
          txtarea.value = finText;
          txtarea.focus();
          count++;
          txtarea.selectionEnd = end + 2;
          start = start + 2;
          end = end + 2;
          txtarea.setSelectionRange(start, end);
          textemoji = finText;
          if (start > 0) {
            this.setState({ create_btnchange_desc: false });
          } else {
            this.setState({ create_btnchange_desc: true });
          }
          // this.setState({
          //   description: textemoji,
          // })
          this.props.change("description", textemoji);
          this.props.change("emoji_unicode", textemoji);
          this.TwitterCount(count);
        }
      }
    }
  }
  // checkLimitPostDesc ends

  componentWillUnmount() {
    //in share and Edit into the post ,editPostFlag bydefualt 0, if we share and edit with any media post , then set the value editPostFlag = 1
    //when I Click on cross button without creating the post, so remove the varible value

    editPostFlag = 0;
    cancleflag = 0;
    descriptionEditPost = null;
  }
  componentWillMount() {
    media_selected = false;
    moment().then(moment => {
      reactDatepickerChunk()
        .then(reactDatepicker => {

          emojiPicker().then(emoji => {
            PhotoEditorSDK().then(editor => {
              reactTooltip().then(reactTooltip => {
                this.setState({
                  ReactDatePicker: reactDatepicker,
                  Moment: moment,
                  emojiPicker: emoji,
                  photoEditor: editor,
                  tooltip: reactTooltip
                });
              });
            });
          });
        })
        .catch(err => {
          throw err;
        });
    });
    this.props.cultureActions.fetchValuesList();
  }


  componentDidMount() {

    gifCount = 0;
    AssetgifCount = 0;
    driveGifCount = 0;
    docCount = 0;
    assetDocCount = 0;
    driveDocCount = 0;
    totalAssetsCount = 0;
    tempAssetCount = 0;
    isDataChanged = [];
    mediaExtensionList=[]
    disableEditPostDataTemp=false
    var mimeType = utils.mimetype();
    // var mimeType = ['application/vnd.google-apps.photo','image/jpeg','image/jpg','image/png','image/gif']
    this.setState({
      mimetype: mimeType
    });
    roleName =
      JSON.parse(localStorage.getItem("roles")) !== null
        ? JSON.parse(localStorage.getItem("roles"))[0].role_name
        : "";
    editPostFlag = 0;
    createPostMainContainer = document.getElementsByClassName(
      "post-details-main"
    )[0];
    // this.props.tagsActions.fetchTags('post');
    // this.props.channelActions.fetchChannels({selectedChannels:
    // this.props.post.SocialChannel}); this.props.channelActions.fetchChannels()


    if (this.props.assets.cats.length < 1) {
      this.props.assetsActions.fetchCats();
    }

    if (this.props.assets.campaigns.length < 1 && roleName !== "guest") {
      this.props.assetsActions.fetchCatCampaignFolders();
    }

    if (this.props.assets.customfeedAsset.length < 1) {
      this.props.assetsActions.fetchCatCustomFeedFolders();
    }

    if (
      this.props.campaignId ||
      this.props.location.query.searchcreatepost == "true"
    ) {
      //if create post is from campaign  or came from global search then to remove auto selection of internal checkbox
      this.setState({
        wallCheckbox: [],
        type: ""
      });
      this.props.change("type", "");
    }
    if (roleName == "guest") {
      //to set current asset tab as custom feed
      this.setState({
        currentAssetTab: "custom Feed"
      });
    }
    {
      roleName !== "guest" ? this.props.userActions.fetchUsers() : "";
    }
    //this.fetchAssets({ currPage: 1 })
    this.handleInitialize();
    checked_social_twitter = false;
    start = 0;
    ReactDOM.findDOMNode(this).addEventListener(
      "click",
      this._handleCloseEvent
    );

    var path = utils.removeTrailingSlash(this.props.location.pathname).split('/')
    this.props.tagsActions.fetchUserTags(path)
    this.props.tagsActions.fetchPostTags('all')
    this.focustextbox("");
    // to close popup on outside of click
    var me = this
    window.addEventListener('click', function (event) {
      if(!event.target.closest(".postScheduleSelectedImageWrapper.schedule-popup") && !event.target.closest('.btn.schedule-btn') && !event.target.closest('.react-datepicker__month-container') && !event.target.closest('.rc-time-picker-panel-inner')){
        if(me.state.showScheduler == true){
          me.setState({
            showScheduler: false,
          })
        }
      }
    })
  }

  _handleCloseEvent(e) {
    if (document.getElementsByClassName("emojiPicker").length > 0) {
      if (
        !document.getElementsByClassName("emojiPicker")[0].contains(e.target) &&
        !document.getElementsByClassName("emojiBtn")[0].contains(e.target)
      ) {
        this.closeEmos();
      }
    }
  }

  componentWillReceiveProps(nextProps) {
    var me = this;
    if (
      nextProps.posts.driveAssetsURL !== this.props.posts.driveAssetsURL &&
      nextProps.posts.driveAssetsUploading !== true &&
      nextProps.posts.driveAssetsURL !== null
    ) {
      showDriveUpload = false;
      media_selected = true;
      this.setState({
        renderDriveUI: false,
        DriveAssetsData: nextProps.posts.driveAssetsURL,
        showMediaInterface: false,
        showCropInterface: false,
        showCroppedResult: false,
        blDirectFileUpload: false
      });
      if (
        document
          .getElementsByClassName("disable-overlay")[0]
          .classList.contains("GoogleDrive")
      ) {
        document
          .getElementsByClassName("create-post-image-wrapper")[0]
          .classList.add("hide");
        document
          .getElementsByClassName("disable-overlay")[0]
          .classList.remove("GoogleDrive");
      }
      driveGifCount = 0;
      let uploadedTempData = nextProps.posts.driveAssetsURL;
      for (
        var driveIndex = 0;
        driveIndex < uploadedTempData.length;
        driveIndex++
      ) {
        nextProps.posts.uploadedDriveFile.data.map((FileData, index) => {
          if (FileData.file_id == uploadedTempData[driveIndex].file_id) {
            if (FileData.mime_type.includes("gif")) {
              gifCount++;
            }
            if (FileData.mime_type.includes("application")) {
              docCount++;
            }
            var uploadedFile = {
              fileUrl: uploadedTempData[driveIndex].file_path,
              file_extension: FileData.extension,
              media_size: FileData.size,
              mime_type: FileData.mime_type,
              thumbUrl: uploadedTempData[driveIndex].file_path,
              uploadedFile: FileData.file_name
            };
            this.state.uploadedeDriveFileData.push(uploadedFile);
          }
        });
      }
      // when video functionality is available need to add here
      // if (uploadedeDriveFileData.mime_type.includes('video')) {
      // }
      me.props.change("mediaDriveUpload", this.state.uploadedeDriveFileData);
      me.props.change("mediaGiphyDirectUpload", null);
    }

    if (
      nextProps.posts.utmData !== this.props.posts.utmData &&
      nextProps.posts.utmData !== {} &&
      nextProps.posts.utmLinkLoading !== true &&
      nextProps.posts.utmlinkFailed !== true
    ) {
      this.state.utmLinkData.push(nextProps.posts.utmData);
      var shortenUrl = nextProps.posts.utmData.utm_url;
      var postDesc = document.getElementsByName("description")[0].value;
      if (postDesc.includes(mainUtmUrl)) {
        while (postDesc.includes(mainUtmUrl)) {
          var temp = postDesc;
          var index = temp.indexOf(mainUtmUrl);
          while (index != -1) {
            temp = temp.replace(mainUtmUrl, shortenUrl);
            index = temp.indexOf(mainUtmUrl);
          }
          postDesc = temp;
        }
      } else {
        postDesc = postDesc + " " + shortenUrl;
      }
      this.setState(
        {
          description: postDesc,
          create_btnchange_desc: false
        },
        () => {
          if(postDesc.length>0){
            this.setState({
              create_btnchange_desc: false
            })
          }
          this.props.change("description", postDesc);
        }
      );
      this.setState({
        showUTMPopup: false,
        utm_campaign: "social",
        utm_source: "visibly",
        utm_medium: "visibly_web"
      });
      utmParameter = "?";
    }

    if (
      nextProps.posts.utmLinkLoading !== this.props.posts.utmLinkLoading &&
      nextProps.posts.utmLinkLoading == false &&
      nextProps.posts.utmlinkFailed == true
    ) {
      this.setState({
        showUTMPopup: false,
        utm_campaign: "social",
        utm_source: "visibly",
        utm_medium: "visibly_web"
      });
      utmParameter = "?";
    }

    //when folder click in asset view
    if (nextProps.assets.folders !== this.props.assets.folders) {
      this.setState({
        layoutDetail: nextProps.assets.folders,
        changeDisplayForAssetSection: true
      });
    }
    //get assets
    if (nextProps.assets.files !== this.props.assets.files) {
      this.appendAssetsInState(nextProps.assets.files);
      if (
        Object.keys(nextProps.assets.meta).length > 0 &&
        nextProps.assets.files.length > 0
      ) {
        this.setMeta(nextProps.assets.meta);
      }
    }
    //no asset found
    if (
      nextProps.assets.nofiles !== this.props.assets.nofiles &&
      nextProps.assets.nofiles === true
    ) {
      this.appendAssetsInState([]);
      this.setMeta({});
      this.setState({ changeDisplayForAssetSection: false });
    }

    if (
      (Object.keys(this.props.posts.campaignCreatePostdata).length == 0 &&
        typeof this.props.location.query.createpost !== "undefined") ||
      typeof this.props.location.query.searchcreatepost !== "undefined"
    ) {
      let url = `/campaigns`;
      browserHistory.push(url);
      document.body.classList.remove("overlay");
    }

    if (
      nextProps.general.s3SizeData !== "" &&
      nextProps.general.s3SizeData.storage.freeStorage !== "" &&
      nextProps.general.isFetchS3Size == true &&
      this.state.isUploadButtonClicked == true &&
      nextProps.general.s3SizeData.access == "allowed"
    ) {
      //if free storage has data and asset is sent to upload
      this.setState({
        isUploadButtonClicked: false
      });
      if (
        this.state.DriveData !== "" &&
        this.state.currentPostAssetTab == "GoogleDrive" &&
        showDriveUpload == true
      ) {
        this.driveFileSubmit(this.state.storeFields);
      } else {
        this.uploadToAmazon(
          this.state.storeFields,
          nextProps.general.s3SizeData
        );
      }
    } else if (
      this.state.isUploadButtonClicked == true &&
      nextProps.general.isFetchS3Size == true &&
      nextProps.general.s3SizeData.access == "rejected"
    ) {
      //if button is clicked to upload asset but there is no space to upload asset
      this.setState({
        isUploadButtonClicked: false,
        uploadInProgress: false,
        DriveData: "",
        renderDriveUI: false
      });
      notie.alert(
        "error",
        `${nextProps.profile.profile.first_name +
          nextProps.profile.profile
            .last_name}, you have consumed your storage limit. Please ask your admin to upgrade your package to continue.`,
        5
      );
      me.props.postsActions.uploadingComplete();
      nextProps.s3Actions.uploadReset();
    }
    if(this.props.savedHashTagListInPost.hashTagListInCreatePost !== nextProps.savedHashTagListInPost.hashTagListInCreatePost && Object.keys(nextProps.savedHashTagListInPost.hashTagListInCreatePost).length>0){
      this.setState({ create_btnchange_desc: false });
      this.handleInitialize(nextProps)
    }
  }

  handleInitialize(nextProps = null) {
    txtarea = null;
    var  initData = null;
    var propsToTake=nextProps !== null ? nextProps:this.props
    var savedHashTagList=propsToTake.savedHashTagListInPost.hashTagListInCreatePost
    var hashTagDetail=[]
    if(Object.keys(savedHashTagList).length>0){
      for (let index = 0; index < savedHashTagList.hashtag.length; index++) {
        const element = savedHashTagList.hashtag[index];
        hashTagDetail.push({
          'display': element.display,
          'identity': element.id
        })
      }
    }
    if(this.props.editPostData != null){
       initData = {
         ...initData,
        schedulerCounter: this.state.schedulerCounter,
        description:
        descriptionEditPost !== null && descriptionEditPost !== ""
          ? utils.convertUnicode(
              utils.replaceTags(
                descriptionEditPost,
                this.props.editPostData.post.data.userTag
                  ? this.props.editPostData.post.data.userTag.data
                  : [],
                this.props.editPostData.post.data.tags
                  ? this.props.editPostData.post.data.tags.data
                  : []
              )
            )
          : this.state.description,
      }
    }else{
    // THIS FUNCTIONALITY WILL BE MODIFIED WHEN POST IS EDITED PHASE2
    initData = {
      schedulerCounter: this.state.schedulerCounter,
      type: this.props.editPostData
        ? this.props.editPostData.post.data.type
        : this.state.type,
      campaign: this.props.campaignId,
      cnt: this.state.cntnum,
      description:
        descriptionEditPost !== null && descriptionEditPost !== ""
          ? utils.convertUnicode(
              utils.replaceTags(
                descriptionEditPost,
                this.props.editPostData.post.data.userTag
                  ? this.props.editPostData.post.data.userTag.data
                  : [],
                this.props.editPostData.post.data.tags
                  ? this.props.editPostData.post.data.tags.data
                  : []
              )
            )
          : Object.keys(savedHashTagList).length>0? // to display saved hashtag in desciption box of create post popup
            utils.replaceTags(
              savedHashTagList.detail,
              [],
              hashTagDetail.length>0
              ? hashTagDetail
              : []
          ): this.state.description,
      emoji_unicode: this.state.emoji_code,
      utmLinks: this.state.utmLinkData,
      utmSource: this.state.utm_source,
      utmCampaign: this.state.utm_campaign,
      utmMedium: this.state.utm_medium
    };
  }
  if(this.props.editCurationData){
    var savedtag=Object.keys(savedHashTagList).length>0? // to display saved hashtag in desciption box of create post popup
          utils.replaceTags(
            savedHashTagList.detail,
            [],
            hashTagDetail.length>0
            ? hashTagDetail
            : []
          ):''
      let postDesc = `${savedtag}${this.props.editCurationData.detail}\n${this.props.editCurationData.link_preview ? this.props.editCurationData.link_preview[0].link :''}`
      initData['description']= postDesc;
      this.setState({
        description: postDesc,
        create_btnchange_desc: false
      })
    }
    //currentFeed type will have data if filter is selected or page is on my live as guest user or user is in custom feed
    var internalFeed =
      this.props.currentFeed.type !== undefined
        ? this.props.currentFeed.type == "internal" ||
          this.props.currentFeed.type == "both"
          ? true
          : false
        : this.props.defaultFeed.type == "both" ||
          this.props.defaultFeed.type == "internal"
          ? true
          : false;
    var objToAdd = {
      "chk-internal-0": internalFeed
    };
    if (
      this.props.campaignId ||
      this.props.location.query.searchcreatepost == "true" ||
      this.props.editPostData !== null
    ) {
      //to remove auto selection of internal if post is in edit mode
      // this.state.wallCheckbox = [];
      this.setState({
        wallCheckbox:[]
      })
    } else {
      Object.assign(initData, objToAdd);
    }
    this.props.initialize(initData);
    txtarea ? txtarea.setSelectionRange(start, end) : "";
  }

  fetchAssets(categoryId, folderId, maxItem, currPage) {
    if (folderId == null) {
      folderId = false;
    }
    this.props.assetsActions.fetchFiles(
      categoryId,
      folderId,
      maxItem,
      currPage
    );
    /*fetch(Globals.API_ROOT_URL + `/asset/pending?api_secret=${Globals.API_SECRET}&approval_status=approved&&auth_token=${Globals.AUTH_TOKEN}&limit=${12}&page=${currPage}&access_token=${Globals.AUTH_TOKEN}`)
      .then(response => response.json())
      .then(json => {
        if (Array.isArray(json.data) && json.data.length > 0) {
          this.appendAssetsInState(json.data)
          this.setMeta(json.meta)
        }
      })
      .catch(err => {
        notie.alert('error', err, 5)
        throw err
      })*/
  }

  appendAssetsInState(assets, isNew = true) {
    if (assets.length > 0 && isNew === false) {
      var assets = this.state.assets.concat(assets);
    }
    this.setState({ assets: assets });
  }

  setMeta(meta) {
    if (Object.keys(meta).length > 0) {
      this.setState({ meta: meta });
    } else {
      this.setState({ meta: { pagination: { total_pages: 1 } } });
    }
  }

  addMoreScheduleRow() {
    setTimeout(function() {
      if (createPostMainContainer !== undefined) {
        createPostMainContainer.scrollTo(
          0,
          createPostMainContainer.scrollHeight
        );
      }
    }, 100);
    let newScheduleCounter = this.state.dynamicSchedulers;
    newScheduleCounter.push("scheduler");
    let counterIncrement = this.state.schedulerCounter + 1;
    this.setState({
      schedulerCounter: counterIncrement,
      dynamicSchedulers: newScheduleCounter
    });
    this.props.change(`schedulerCounter`, counterIncrement);
  }
  removeScheduler(index) {
    var dynamicSchedulers = this.state.dynamicSchedulers;
    this.state.dynamicSchedulers.splice(index, 1);
    this.setState({ dynamicSchedulers: dynamicSchedulers });
    let totalSchedulers = document.querySelectorAll(".schedule.row");
    this.props.change(`scheduledDate-${index}`, null);
    this.props.change(`scheduledTime-${index}`, null);
    //elScheduler.parentNode.removeChild(elScheduler)
    let removedSch = this.state.removedSchedulers;
    removedSch.push(index);
    let counterDecrement = this.state.schedulerCounter - 1;

    isDataChanged[index] = false;
    var Highlightschedule = false;
    for (var i = 0; i < isDataChanged.length; i++) {
      if (isDataChanged[i] == true) {
        Highlightschedule = true;
      }
    }
    if (Highlightschedule == false) {
      this.setState({
        isScheduleSelected: false
      });
    }
    if(this.state.dynamicSchedulers.length > 0 && Highlightschedule){
      this.props.change("isScheduled", true);
    }else{
      this.props.change("isScheduled", false);
    }
    this.props.change(`schedulerCounter`, counterDecrement);
    this.setState({
      schedulerCounter: counterDecrement,
      removedSchedulers: removedSch
    });
  }

  /**
   * @use it will check post is schedule or not
   */
  handleSchedulerUI() {
    if (!this.state.showScheduler) {
      setTimeout(function() {
        if (createPostMainContainer !== undefined) {
          createPostMainContainer.scrollTo(
            0,
            createPostMainContainer.scrollHeight
          );
        }
      }, 100);
    }
    this.setState({
      showScheduler: !this.state.showScheduler,
      showMediaInterface: false,
      showEmojiPicker: false,
      displaymedia: false,
      media_wrapper_class: "",
      showGiphy: false //when I click on schedule then emoji popup close
    });
    this.closeValuesPopup();
  }
  handleGiphyUI() {
    if (
      Object.keys(this.state.selectedAsset).length > 0 ||
      Object.keys(this.state.uploadedFile).length > 0 ||
      this.state.uploadedeDriveFileData.length > 0 ||
      (mediaExtensionList.length>0 && mediaExtensionList.indexOf('video') !== -1 || mediaExtensionList.indexOf('image') !== -1)
    ) {
      notie.alert("error", "Please select either photos and video or separately GIF.", 3);
      this.closeSchedule();
      this.closeValuesPopup();
    } else {
      this.setState({
        showGiphy: !this.state.showGiphy,
        displaymedia: false,
        media_wrapper_class: ""
      });
      this.closeSchedule();
      this.closeValuesPopup();
    }
    // document.getElementById('close-popup').classList.add('hide');
  }
  handleValueUI() {
    this.setState({
      showValues: !this.state.showValues,
      displaymedia: false,
      media_wrapper_class: ""
    });
    this.closeGiphyPopup();
    this.closeSchedule();
  }
  handleEmojiUI() {
    // this.props.s3Actions.uploadReset()
    this.setState({
      showEmojiPicker: !this.state.showEmojiPicker,
      showGiphy: false //when I click on emoji  then schedule popup close
    });
    this.closeSchedule();
  }
  handleMediaUI() {
    if (this.state.selectGipy_child_state !== null || (mediaExtensionList.length>0 && mediaExtensionList.indexOf('gif') !== -1)) {
      notie.alert("error", "Please select either photos and video or separately GIF.", 3);
    } else {
      this.props.s3Actions.uploadReset();
      this.setState({
        showMediaInterface: !this.state.showMediaInterface,
        displaymedia: false,
        media_wrapper_class: "media_wrapper_class"
      });
    }
  }

  handleGiphySubmit(e) {}

  handleGifCount(type) {
    if (type == "increment") {
      AssetgifCount++;
    } else if (type == "decrement") {
      AssetgifCount--;
    }
  }

  handleDocCount(type) {
    if (type == "increment") {
      assetDocCount++;
    } else if (type == "decrement") {
      assetDocCount--;
    }
  }

  async handleFilesSubmit(fields) {
    if (gifCount !== 0) {
      if (AssetgifCount !== 0) {
        notie.alert("error", "You can only select one GIF at a time.", 3);
        this.props.assetsActions.resetuploadedAssetInBuffer();
      } else {
        notie.alert(
          "error",
          "Please select either media(s) or GIF.",
          3
        );
        this.props.assetsActions.resetuploadedAssetInBuffer();
      }
    } else if (AssetgifCount > 1 && AssetgifCount == fields.files.length) {
      notie.alert("error", "You can only select one GIF at a time.", 3);
      this.props.assetsActions.resetuploadedAssetInBuffer();
    } else if (
      AssetgifCount > 0 &&
      (fields.files.length > 1 ||
        Object.keys(this.state.selectedAsset).length > 0 ||
        this.state.uploadedFile.length > 0 ||
        this.state.uploadedeDriveFileData.length > 0)
    ) {
      notie.alert("error", "Please select either media(s) or GIF.", 3);
      this.props.assetsActions.resetuploadedAssetInBuffer();
    } else if (docCount !== 0) {
      if (assetDocCount !== 0) {
        notie.alert("error", "You can only select one document at a time.", 3);
        this.props.assetsActions.resetuploadedAssetInBuffer();
      } else {
        notie.alert(
          "error",
          "Please select either video,  image(s) or document.",
          3
        );
        this.props.assetsActions.resetuploadedAssetInBuffer();
      }
    } else if (assetDocCount > 1 && assetDocCount == fields.files.length) {
      notie.alert("error", "You can only select one document at a time.", 3);
      this.props.assetsActions.resetuploadedAssetInBuffer();
    } else if (
      assetDocCount > 0 &&
      (fields.files.length > 1 ||
        Object.keys(this.state.selectedAsset).length > 0 ||
        this.state.uploadedFile.length > 0 ||
        this.state.uploadedeDriveFileData.length > 0)
    ) {
      notie.alert(
        "error",
        "Please select either video,  image(s) or document.",
        3
      );
      this.props.assetsActions.resetuploadedAssetInBuffer();
    } else {
      media_selected = true;
      var me = this;
      var filesToUpload = [];
      // selectedMediaSizeTotal = 0
      // selectedMediaSizeTotal = fields.files[0].size
      if (fields.crop == true) {
        this.cropBeforeUpload(fields.files);
      } else {
        selectedMediaSizeTotal = 0;
        for (var j = 0; j < fields.files.length; j++) {
          //to count total media size and store in global varibale
          selectedMediaSizeTotal += fields.files[j].size;
        }
        // this.props.TotalMediaSize(selectedMediaSizeTotal)

        if (selectedMediaSizeTotal < freeStorage && freeStorage !== 0) {
          for (var i = 0; i < fields.files.length; i++) {
            if (
              fields.files[i].type.match("image") !== null &&
              fields.files[i].type.match("image").length > 0
            ) {
              var resized = await utils.resizeImage(fields.files[i], "", 1500);
              var blob = resized;
              var originalFileData = fields.files[i];
              var temp = originalFileData.name.split(".");
              var fName = temp[0];
              var type = temp[temp.length-1];

              let newFile = new File(
                [blob],
                fName + "_" + Date.now() + "." + type,
                { type: originalFileData.type }
              );
              var addMimeObj = {
                mimetype: originalFileData.mimetype
              };
              Object.assign(newFile, addMimeObj);
              var url = URL.createObjectURL(blob);
              newFile.preview = url;

              if (originalFileData.type == "image/gif") {
                filesToUpload.push(originalFileData);
              } else {
                filesToUpload.push(newFile);
              }
              me.setState({ uploadInProgress: true });
            } else {
              filesToUpload.push(fields.files[i]);
            }
          }
          this.checkStorageSpace(filesToUpload);
        }
      }
    }
  }
  /**
   * @author disha
   * call after creating post
   * @param {*} campaignId
   */
  backForCampaign(campaignId) {
    let url;
    if (campaignId !== undefined) {
      this.props.postsActions.removeCampaignCreatePostData();
      if (this.props.location !== undefined) {
        if (this.props.location.search.includes("search"))
          url = `/search/campaign`;
        else url = `/campaigns?campaignid=${campaignId}`;
      }
      browserHistory.push(url);
    }
    document.body.classList.remove("overlay");
  }

  renderBreadcrumbs() {
    if (this.state.isFromCategory) {
      return (
        <ul className="clearfix">
          <li>
            <a
              href="javascript:;"
              onClick={this.singleClickOpenCategory.bind(
                this,
                this.state.currentAssetTab
              )}
            >
              {this.state.currentAssetTab.charAt(0).toUpperCase() +
                this.state.currentAssetTab.slice(1)}
            </a>
          </li>
        </ul>
      );
    } else {
      return (
        <ul className="clearfix">
          <li>
            <a
              href="javascript:;"
              onClick={this.singleClickOpenCategory.bind(
                this,
                this.state.currentAssetTab
              )}
            >
              {this.state.currentAssetTab.charAt(0).toUpperCase() +
                this.state.currentAssetTab.slice(1)}
            </a>
          </li>

          {this.props.assets.breadCrum.map((breadCrum, index) => {
            return breadCrum.category !== "campaign" &&
              breadCrum.category !== "custom" ? (
              <li key={`breadcrum_${index}`}>
                <a
                  href="javascript:;"
                  onClick={this.singleClickOpenFolder.bind(this, breadCrum.id)}
                >
                  {breadCrum.category}
                </a>
              </li>
            ) : (
              ""
            );
          })}
        </ul>
      );
    }
  }

  cropBeforeUpload(files) {
    this.setState({
      showCropInterface: true,
      showMediaInterface: false,
      showCroppedResult: true
    });
    var me = this;

    let elCroppieArea = document.getElementById("croppie-area");
    elCroppieArea.innerHTML = "";

    me.uploadCrop = new this.props.croppie(elCroppieArea, {
      viewport: {
        width: 520,
        height: 380
      },
      boundary: {
        width: 580,
        height: 460
      },
      showZoomer: true,
      enableResize: true,
      enableOrientation: true
    });
    var reader = new FileReader();

    reader.onload = function(e) {
      me.uploadCrop.bind({ url: e.target.result });
    };

    reader.readAsDataURL(files[0]);
  }
  /**
   * @author disha
   * method will call when page is refreshed or canceled or uploading will canceled to release occupied size in db
   * @param {*} event
   */
  onUnload(event) {
    // the method that will be used for both add and remove event
    if (
      this.props.posts.isUploadingStart == true ||
      this.props.posts.driveAssetsUploading == true
    ) {
      //api will call when uploading is start and user want to cancel or refresh page
      var objToSend = {
        "file-size": selectedMediaSizeTotal,
        action: "decrease"
      };
      this.props.generalActions.fetchS3Size(objToSend);
      event.returnValue = "heyyy";
    }
  }
  /**
     * @author disha
     * when clicked on create after filling desciption to upload it this method will check storage space
     * @param {*} data field file data
     */
  checkStorageSpace(data) {
    window.addEventListener("beforeunload", this.onUnload.bind(this));

    //this will set flag which will be false after uploading asset is done
    if (this.state.DriveData !== "") {
      this.props.postsActions.uploadDriveAssetInit();
    } else {
      this.props.postsActions.assetInPostUploadingStart();
    }

    this.setState({
      storeFields: data,
      isUploadButtonClicked: true
    });
    var objToSend = {
      "file-size": selectedMediaSizeTotal,
      // "file-size":5243336130560,
      action: "increase"
    };
    this.props.generalActions.fetchS3Size(objToSend);
  }

  uploadToAmazon(filesToUpload, storageData = null) {
    AssetgifCount = 0;
    assetDocCount = 0;
    this.giphySelectCancel();
    if (storageData.access !== "rejected") {
      Globals.AWS_CONFIG.albumName = localStorage.getItem("albumName");
      var s3Config = Globals.AWS_CONFIG;
      var s3Manager = new s3functions.S3Manager(s3Config);
      let me = this;
      // we are uploading files to s3 now, set the flag in redux
      this.props.s3Actions.uploadingStart();
      if (this.state.DriveData !== "") {
        this.setState({
          DriveData: ""
        });
        if (showDriveUpload == true) {
          Globals.tempSelection = true;
          Globals.assetSelectCancel = true;
          this.props.postsActions.uploadDriveAsset(null, "discard");
        }
        showDriveUpload = false;
      }
      // the for loop is just to allow multiple uploads in future
      for (var i = 0; i < filesToUpload.length; i++) {
        var fileToUpload = filesToUpload[i];
        // upload starts here. it returns a promise whether succeeded or failed after
        // you get the promise, proceed with db insertion

        var s3MediaURL = s3Manager.s3uploadMedia(
          fileToUpload,
          i,
          filesToUpload.length
        );

        // On succession of uploading main asset on amazon, we will generate a thumbnail
        // and send next request to amazon
        s3MediaURL
          .then(obj => {
            var file = obj.data;
            var originalFileObj = obj.fileObject;
            var index = obj.index;
            var generatedFileName = originalFileObj.name;
            //this is used to split asset_name and attached timestamp
            var temp = generatedFileName.split(".");
            var fName = temp[0].split("_");
            fName.pop();
            var type = temp[1];
            fName = fName.join("_");
            //original name of the asset
            generatedFileName = fName + "." + type;
            if (
              originalFileObj.type.match("image") !== null &&
              originalFileObj.type.match("image").length > 0
            ) {
              if (originalFileObj.type.includes("gif")) {
                gifCount++;
              }
              var thumbnail = s3Manager.s3uploadThumbnail(
                originalFileObj,
                index,
                filesToUpload.length
              );
              thumbnail
                .then(function(thumb) {
                  var photokey = file.Location.split("amazonaws.com/")[1];
                  var thumbPhotokey = thumb.data.Location.split(
                    "amazonaws.com/"
                  )[1];
                  let uploadedFile = {
                    // fileUrl: file.Location,
                    fileUrl: `https://s3.${Globals.AWS_BUCKET_REGION}.amazonaws.com/${file.Bucket}/${photokey}`,
                    // thumbUrl: thumb.data.Location,
                    thumbUrl: `https://s3.${Globals.AWS_BUCKET_REGION}.amazonaws.com/${file.Bucket}/${thumbPhotokey}`,
                    uploadedFile: generatedFileName,
                    media_size: originalFileObj.size,
                    mime_type: originalFileObj.mimetype,
                    file_extension: originalFileObj.type.split("/")[1],
                    height: thumb.thumbnail.height,
                    width: thumb.thumbnail.width
                  };
                  //this will set flag to default to tell uploading is done so if page is refreshed or canceled media size will not be decreased
                  me.props.postsActions.uploadingComplete();

                  me.props.s3Actions.uploadedItem(uploadedFile);
                  me.state.uploadedFile.push(uploadedFile);
                  me.setState({
                    showMediaInterface: false,
                    showCropInterface: false,
                    showCroppedResult: false,
                    blDirectFileUpload: true,
                    directFileUploadObj: originalFileObj,
                    uploadInProgress: i == filesToUpload.length ? false : true
                  });
                  me.props.change("mediaDirectUpload", me.state.uploadedFile);
                })
                .catch(err => {
                  notie.alert("error", err, 5);
                });
            } else if (
              originalFileObj.type.match("video") !== null &&
              originalFileObj.type.match("video").length > 0
            ) {
              var thumbnail = s3Manager.s3uploadThumbnail(
                originalFileObj,
                index,
                filesToUpload.length,
                file.Location
              );
              let uploadedFile = {};
              thumbnail.then(
                function(thumb) {
                  var photokey = file.Location.split("amazonaws.com/")[1];
                  var thumbPhotokey = thumb.data.Location.split(
                    "amazonaws.com/"
                  )[1];
                  uploadedFile = {
                    fileUrl: `https://s3.${Globals.AWS_BUCKET_REGION}.amazonaws.com/${file.Bucket}/${photokey}`,
                    thumbUrl: `https://s3.${Globals.AWS_BUCKET_REGION}.amazonaws.com/${file.Bucket}/${thumbPhotokey}`,
                    uploadedFile: generatedFileName,
                    media_size: originalFileObj.size,
                    mime_type: originalFileObj.mimetype,
                    file_extension: originalFileObj.type.split("/")[1],
                    duration: originalFileObj.duration,
                    height: thumb.thumbnail.height,
                    width: thumb.thumbnail.width
                  };

                  if (thumb.Location == "" || thumb.Location == undefined) {
                    //if video thumbnail from s3 is failed to generate then to re-create thumbnail using plugin and upload on S3
                    thumbnail = s3Manager.s3uploadThumbnail(
                      originalFileObj,
                      index,
                      filesToUpload.length,
                      file.Location,
                      false,
                      true
                    );
                    thumbnail.then(function(thumb) {
                      var photokey = file.Location.split("amazonaws.com/")[1];
                      var thumbPhotokey = thumb.data.Location.split(
                        "amazonaws.com/"
                      )[1];
                      uploadedFile = {
                        fileUrl: `https://s3.${Globals.AWS_BUCKET_REGION}.amazonaws.com/${file.Bucket}/${photokey}`,
                        thumbUrl: `https://s3.${Globals.AWS_BUCKET_REGION}.amazonaws.com/${file.Bucket}/${thumbPhotokey}`,
                        uploadedFile: generatedFileName,
                        media_size: originalFileObj.size,
                        mime_type: originalFileObj.mimetype,
                        file_extension: originalFileObj.type.split("/")[1],
                        height: thumb.thumbnail.height,
                        width: thumb.thumbnail.width
                      };
                    });
                  }
                  me.props.postsActions.uploadingComplete();
                  me.props.s3Actions.uploadedItem(uploadedFile);
                  me.state.uploadedFile.push(uploadedFile);
                  me.setState({
                    showMediaInterface: false,
                    showGiphy: false,
                    showCropInterface: false,
                    showCroppedResult: false,
                    uploadInProgress: i == filesToUpload.length ? false : true,
                    blDirectFileUpload: true,
                    directFileUploadObj: originalFileObj
                    //uploadedGiphyFile: uploadedGiphyFile
                  });
                  me.props.change("mediaDirectUpload", me.state.uploadedFile);
                }
                // me.props.change('mediaGiphyDirectUpload', uploadedGiphyFile)
              );
            } else {
              if (originalFileObj.type.includes("application")) {
                docCount++;
              }
              var photokey = file.Location.split("amazonaws.com/")[1];
              let uploadedFile = {
                fileUrl: `https://s3.${Globals.AWS_BUCKET_REGION}.amazonaws.com/${file.Bucket}/${photokey}`,
                thumbUrl: "",
                uploadedFile: generatedFileName,
                media_size: originalFileObj.size,
                mime_type: originalFileObj.mimetype,
                file_extension: originalFileObj.type.split("/")[1]
              };
              me.props.s3Actions.uploadedItem(uploadedFile);
              me.props.postsActions.uploadingComplete();
              me.state.uploadedFile.push(uploadedFile);
              me.setState({
                showMediaInterface: false,
                showGiphy: false,
                showCropInterface: false,
                showCroppedResult: false,
                uploadInProgress: i == filesToUpload.length ? false : true,
                blDirectFileUpload: true,
                directFileUploadObj: originalFileObj
                //uploadedGiphyFile: uploadedGiphyFile
              });
              me.props.change("mediaDirectUpload", me.state.uploadedFile);
            }
          })
          .catch(err => {
            notie.alert(
              "error",
              "There was a problem uploading your post asset",
              5
            );
          });
      }
    }
  }

  handleCropImage() {
    let me = this;
    me.uploadCrop.result({ type: "blob" }).then(function(blob) {
      let newFile = new File([blob], utils.guid() + Date.now() + ".png", {
        type: "image/png"
      });
      var url = URL.createObjectURL(blob);
      newFile.preview = url;
      me.uploadToAmazon([newFile]);
      me.setState({ uploadInProgress: true });
    });
  }

  handleCancelCrop() {
    this.setState({
      showCropInterface: false,
      showMediaInterface: true,
      showCroppedResult: false
    });
  }

  handlegiphy(val) {
    if (val !== null) {
      selectGipy_child = val;
      this.setState({
        selectedGiphy: true
      });
      let uploadedGiphyFile = {
        fileUrl: selectGipy_child.images.original.url,
        thumbUrl: selectGipy_child.images.original.url,
        filesize: selectGipy_child.images.original.size
      };
      if (
        selectGipy_child.images.original.height &&
        selectGipy_child.images.original.width
      ) {
        var objToAdd = {
          height: selectGipy_child.images.original.height,
          width: selectGipy_child.images.original.width
        };
        Object.assign(uploadedGiphyFile, objToAdd);
      }
      this.props.change("mediaGiphyDirectUpload", uploadedGiphyFile);
    } else {
      this.props.change("mediaGiphyDirectUpload", null);
    }
  }
  renderValues() {
    let showValueUI = this.state.showValues;
    if (showValueUI) {
      return (
        <div className="value-popup-in-feed">
          <div className="postsMediaUI create-post-gipghy-wrapper clearfix">
            <button
              id="close-popup"
              className="btn-default value-close-btn hide"
              onClick={this.closeValuesPopup.bind(this)}
            >
              <i className="material-icons">clear</i>
            </button>
            <div className="popupDescSection">
              <CultureValue
                selectValue={this.handleValue.bind(this)}
                SelectedCulturalValue={this.state.culturValue}
              />
            </div>
          </div>
        </div>
      );
    }
  }
  handleValue(e) {
    this.setState({
      culturValue: e
    });
    if (e == undefined) {
      this.setState({
        isValueSelected: false
      });
      this.props.change("cultureValue", null);
    } else {
      this.setState({
        showValues: false,
        isValueSelected: true
      });
      this.props.change("cultureValue", e);
    }
  }

  closeValuesPopup() {
    this.setState({
      showValues: false
    });
  }
  renderGiphyUI() {
    let showGiphyUI = this.state.showGiphy;
    var me = this;
    const { pristine, submitting, reset } = this.props;
    let { uploadedItems, uploadDone, uploadStarted } = this.props.aws;

    if (showGiphyUI) {
      return (
        <div className="giphy-popup">
          <div className="postsMediaUI create-post-gipghy-wrapper clearfix">
            <button
              id="close-popup"
              className="btn-default giphy-close-btn hide"
              onClick={this.closeGiphyPopup.bind(this)}
            >
              <i className="material-icons">clear</i>
            </button>
            <div className="popupDescSection">
              <CreateGiphy
                onSubmit={this.handleGiphySubmit.bind(this)}
                selectgiphy={this.handlegiphy.bind(this)}
              />

              <div
                className={`action-buttons giphy_button ${uploadStarted
                  ? "disabled"
                  : ""}`}
              >
                <button
                  id="giphySelectBtn"
                  className="btn-theme right hide"
                  onClick={this.giphySelectedBtnClicked.bind(this)}
                >
                  Select
                </button>
                <button
                  id="giphySelectBtnDisable"
                  className="btn-theme right"
                  disabled
                >
                  Select
                </button>
              </div>
            </div>
          </div>
        </div>
      );
    } else {
      return null;
    }
  }
  // choosenemoji(value){
  //   var textvalue = document.getElementsByName('description')[0].value
  //   if (value.shortname !== undefined) {
  //     var emojiUnicode = utils.toUnicode(value.unicode);
  //     var textemoji = String(textvalue) + String(emojiUnicode)
  //     this.setState({
  //       description: textemoji,
  //     }, () => this.handleInitialize())
  //   }

  // }
  closeEmos() {
    this.setState({
      showEmojiPicker: false
    });
  }
  renderEmojiPickerUI() {
    if (this.state.showEmojiPicker) {
      return (
        <div className="emojiPopup">
          <div className="emojiPicker">
            {/* onChange={(e) => this.choosenemoji(e)} */}
            {/* onChange={(e) => this.checkLimitPostDesc(e)} */}
            {this.state.emojiPicker ? (
              <this.state.emojiPicker.default
                onChange={e => this.checkLimitPostDesc(e)}
                search={true}
              />
            ) : (
              ""
            )}
          </div>
        </div>
      );
    }
  }
  postAddAssetTab(currentTab) {
    this.setState({
      currentPostAssetTab: currentTab
    });
  }
  //this functions  are  for google drive
  selectFromDrive() {
    gapi.load("auth", { callback: this.onAuthApiLoad.bind(this) });
    // gapi.load('picker', {'callback': this.onPickerApiLoad.bind(this)});
  }

  // when page loaded successfully loaded it initializes the google file picker API
  handleScriptLoad() {
    gapi.load("client:auth2", this.initClient.bind(this));
  }

  initClient() {
    let data = {
      apiKey: this.state.developerKey,
      discoveryDocs: [
        "https://www.googleapis.com/discovery/v1/apis/drive/v3/rest"
      ],
      clientId: this.state.clientId,
      scope: this.state.scope
    };
    let req = gapi.client.init(data);
  }

  onAuthApiLoad() {
    window.gapi.auth.authorize(
      {
        client_id: this.state.clientId,
        scope: this.state.scope,
        immediate: false
      },
      this.handleAuthResult.bind(this)
    );
  }

  handleAuthResult(authResult) {
    if (authResult && !authResult.error) {
      this.setState({ oauthToken: authResult.access_token });
      gapi.load("picker", { callback: this.onPickerApiLoad.bind(this) });
      //  this.createPicker();
    }
  }

  onPickerApiLoad() {
    this.setState({
      pickerApiLoaded: true
    });
    this.createPicker();
  }
  // open picker when clicked on drive button from menu
  createPicker() {
    this.DriveConnected();
    if (this.state.pickerApiLoaded && this.state.oauthToken) {
      var view = new google.picker.DocsView(google.picker.ViewId.DOCS);
      view.setMimeTypes(this.state.mimetype);
      var picker = new google.picker.PickerBuilder()
        .enableFeature(google.picker.Feature.NAV_HIDDEN)
        .enableFeature(google.picker.Feature.MULTISELECT_ENABLED)
        .setAppId(this.state.appId)
        .setOAuthToken(this.state.oauthToken)
        .addView(view.setParent("root").setIncludeFolders(true))
        // .addView(new google.picker.DocsView().setParent('root').setIncludeFolders(true))
        // .addView(view)
        // .addView(new google.picker.DocsUploadView()) this can be used to upload particular aasets to drive form local machine
        .setDeveloperKey(this.state.developerKey)
        .setCallback(this.pickerCallback.bind(this))
        .build();
      picker.setVisible(true);
    }
  }
  // to check connection status of google drive
  DriveConnected() {
    if (localStorage.getItem("isDriveConnected") == null) {
      var DriveuserId = this.props.profile.profile.identity;
      var obj = { [DriveuserId]: true };
      localStorage.setItem(`isDriveConnected`, JSON.stringify(obj));
    } else {
      var obj1 = JSON.parse(localStorage.getItem("isDriveConnected"));
      var DriveuserId = this.props.profile.profile.identity;
      var obj = { [DriveuserId]: true };
      Object.assign(obj1, obj);
      localStorage.setItem(`isDriveConnected`, JSON.stringify(obj1));
    }
  }
  pickerCallback(data) {
    if (data.action == google.picker.Action.PICKED) {
      this.setState({
        DriveData: data,
        renderDriveUI: true
      });
    }
  }

  removeDriveAsset(data, index) {
    var docData = this.state.DriveData;
    docData.docs = docData.docs.filter((item, index) => {
      return item.url !== data.url;
    });
    this.setState({
      DriveData: docData
    });
  }

  // render selected drive asset preview
  renderDrive() {
    let showDriveData = this.state.renderDriveUI;
    var url,
      urlPortion,
      split1,
      flag = 0;
    var urlThumb = "";
    if (showDriveData && this.state.DriveData.docs !== undefined) {
      var assetsLimit = 9 - totalAssetsCount;
      var i = 0;
      var length = this.state.DriveData.docs.length;
      var data = [];
      if (assetsLimit < length) {
        notie.alert("error", "You can select a maximum of 9 media assets.", 3);
        if (assetsLimit !== 0) {
          for (var j = 0; j < assetsLimit; j++) {
            data.push(this.state.DriveData.docs[j]);
          }
        }
      } else {
        data = this.state.DriveData.docs;
      }
      var strokeColor = "#256eff";
      var trailColor = "#ebeef0";
      var data1 = {};
      data1.files = data;
      return (
        <div>
          {data.length > 0 ? (
            <ul className="dropzone-preview">
              {data.map((doc, i) => {
                fileUrl = doc.url;
                fileId = doc.id;
                var result = doc.name.split(".");
                var final = result.pop();
                var previous = result.join(".");
                fileName = previous !== "" ? previous : doc.name;
                var files;
                // data1.files = data;
                var mimetypecheck = doc.mimeType.split("/");

                mimetypecheck[0] === "image" ? (
                  <div key={`renderDriveImage_${i}`}>
                    {(split1 = fileUrl.split("https://drive.google.com/"))}
                    {(flag = 1)}

                    {split1[1].charAt(0) === "a" ? (
                      <div>
                        {(urlPortion = fileUrl.split("file/d"))}
                        {(urlThumb = urlPortion[0] + "uc?id=" + fileId)}
                        {(url = fileUrl)}
                        {(data1.files[i].url = url)}
                      </div>
                    ) : (
                      <div>
                        {(url = "https://drive.google.com/uc?id=" + fileId)}
                        {(data1.files[i].url = url)}
                        {(urlThumb = url)}
                      </div>
                    )}
                  </div>
                ) : mimetypecheck[0] === "video" ? (
                  <div>
                    {(split1 = fileUrl.split("https://drive.google.com/"))}
                    {(flag = 2)}

                    {split1[1].charAt(0) === "a" ? (
                      <div>
                        {(urlPortion = fileUrl.split("file/d"))}
                        {(url = urlPortion[0] + "thumbnail?id=" + fileId)}
                      </div>
                    ) : (
                      (url = `https://drive.google.com/thumbnail?id=` + fileId)
                    )}
                  </div>
                ) : (
                  //this is for other assets type like doc , pdf , audio
                  <div>{(flag = 3)}</div>
                );
                return (
                  <li className={`item-${i}`} rel={i} key={`driveImageLoader_${i}`}>
                    {flag == 1 ? (
                      // <li className={`item-${i}`} rel={i} key={i}>
                      <div>
                        <div className="preview-wrapper">
                          <ImageLoader
                            src={urlThumb}
                            preloader={PreLoaderMaterial}
                            className="thumbnail"
                          >
                            <div className="image-load-failed">
                              <i class="material-icons no-img-found-icon">
                                &#xE001;
                              </i>
                            </div>
                          </ImageLoader>
                        </div>
                        <div className="file-loader-container">
                          <div className="filename" title={fileName}>
                            {fileName}
                          </div>
                          {!this.props.posts.driveAssetsUploading ? (
                            <button
                              className="btn-default right delete-image-btn"
                              onClick={() => {
                                this.removeDriveAsset(doc, i);
                              }}
                            >
                              <i className="material-icons">clear</i>
                            </button>
                          ) : (
                            ""
                          )}
                          {showDriveUpload == true &&
                          this.props.posts.driveAssetsUploading == true ? (
                            <div className="loader-grey-line loader-line-space loader-line-height loader-line-radius google-drive-loader">
                              {" "}
                            </div>
                          ) : (
                            ""
                          )}
                          {/* <div className='uploadDriveAssetBtn'>
                            <a href="javascript:void(0)" onClick={this.handleDriveSubmit.bind(this, data1)} title="Upload asset" className='btn btn-primary'>Upload</a>
                          </div> */}
                        </div>
                      </div>
                    ) : flag == 2 ? (
                      // <li className={`item-${i}`} rel={i} key={i}>
                      <div>
                        <div className="preview-wrapper">
                          <img src={url} />
                        </div>
                        <div className="file-loader-container">
                          <div className="filename" title={fileName}>
                            {fileName}
                          </div>
                          {!this.props.posts.driveAssetsUploading ? (
                            <button
                              className="btn-default right delete-image-btn"
                              onClick={() => {
                                this.removeDriveAsset(doc, i);
                              }}
                            >
                              <i className="material-icons">clear</i>
                            </button>
                          ) : (
                            ""
                          )}
                          {showDriveUpload == true &&
                          this.props.posts.driveAssetsUploading == true ? (
                            <div className="loader-grey-line loader-line-space loader-line-height loader-line-radius google-drive-loader">
                              {" "}
                            </div>
                          ) : (
                            ""
                          )}
                          {/* <div className='uploadDriveAssetBtn'>
                              <a href="javascript:void(0)" onClick={this.handleDriveSubmit.bind(this, data1)} title="Upload asset" className='btn btn-primary'>Upload</a>
                            </div> */}
                        </div>
                      </div>
                    ) : flag == 3 ? (
                      // <li className={`item-${i}`} rel={i} key={i}>
                      <div>
                        <div className="preview-wrapper">
                          <div class="dropPreviewIcon">
                            <i class="material-icons asset-title-icon">
                              insert_drive_file
                            </i>
                          </div>
                        </div>
                        <div className="file-loader-container">
                          <div className="filename" title={fileName}>
                            {fileName}
                          </div>
                          {!this.props.posts.driveAssetsUploading ? (
                            <button
                              className="btn-default right delete-image-btn"
                              onClick={() => {
                                this.removeDriveAsset(doc, i);
                              }}
                            >
                              <i className="material-icons">clear</i>
                            </button>
                          ) : (
                            ""
                          )}
                          {showDriveUpload == true &&
                          this.props.posts.driveAssetsUploading == true ? (
                            <div className="loader-grey-line loader-line-space loader-line-height loader-line-radius google-drive-loader">
                              {" "}
                            </div>
                          ) : (
                            ""
                          )}

                          {/* <div className='uploadDriveAssetBtn'>
                                <a href="javascript:void(0)" onClick={this.handleDriveSubmit.bind(this, data1)} title="Upload asset" className='btn btn-primary'>Upload</a>
                              </div> */}
                        </div>
                      </div>
                    ) : (
                      ""
                    )}
                  </li>
                );
              })}
              {!this.props.posts.driveAssetsUploading ? (
                <div className="uploadDriveAssetBtn">
                  <a
                    href="javascript:void(0)"
                    onClick={this.handleDriveSubmit.bind(this, data1)}
                    title="Upload asset"
                    className="btn btn-primary"
                  >
                    Upload
                  </a>
                </div>
              ) : (
                ""
              )}
            </ul>
          ) : (
            ""
          )}
        </div>
      );
    }
  }

  handleDriveSubmit(fields) {
    driveGifCount = 0;
    driveDocCount = 0;
    fields.files.map((driveData, index) => {
      if (driveData.mimeType.includes("gif")) {
        driveGifCount++;
      }
      if (driveData.mimeType.includes("application")) {
        driveDocCount++;
      }
    });
    if (gifCount !== 0) {
      if (driveGifCount !== 0) {
        notie.alert("error", "You can only select one GIF at a time.", 3);
        // this.props.assetsActions.resetuploadedAssetInBuffer();
      } else {
        notie.alert(
          "error",
          "Please select either media(s) or GIF.",
          3
        );
        // this.props.assetsActions.resetuploadedAssetInBuffer();
      }
    } else if (driveGifCount > 1 && driveGifCount == fields.files.length) {
      notie.alert("error", "You can only select one GIF at a time.", 3);
      // this.props.assetsActions.resetuploadedAssetInBuffer();
    } else if (
      driveGifCount > 0 &&
      (fields.files.length > 1 ||
        Object.keys(this.state.selectedAsset).length > 0 ||
        this.state.uploadedFile.length > 0 ||
        this.state.uploadedeDriveFileData.length > 0)
    ) {
      notie.alert("error", "Please select either media(s) or GIF.", 3);
      // this.props.assetsActions.resetuploadedAssetInBuffer();
    } else if (docCount !== 0) {
      if (driveDocCount !== 0) {
        notie.alert("error", "You can only select one document at a time.", 3);
      } else {
        notie.alert(
          "error",
          "Please select either video,  image(s) or document.",
          3
        );
      }
    } else if (driveDocCount > 1 && driveDocCount == fields.files.length) {
      notie.alert("error", "You can only select one document at a time.", 3);
    } else if (
      driveDocCount > 0 &&
      (fields.files.length > 1 ||
        Object.keys(this.state.selectedAsset).length > 0 ||
        this.state.uploadedFile.length > 0 ||
        this.state.uploadedeDriveFileData.length > 0)
    ) {
      notie.alert(
        "error",
        "Please select either video,  image(s) or document.",
        3
      );
    } else {
      // if(driveGifCount>1 || gifCount >0){
      //   notie.alert("error"," you can select only one GIF at time.",3);
      // }else if(driveGifCount>0 &&(this.state.uploadedFile.length>1  || this.state.uploadedeDriveFileData.length>1|| Object.keys(this.state.selectedAsset).length>0)|| fields.files.length>1 ){
      //   notie.alert("error","Please select either video,  image(s) or GIF.",3);
      // }else{
      Globals.assetSelectCancel = false;
      showDriveUpload = true;
      var fileSize = 52428800;
      selectedMediaSizeTotal = 0;
      var dataToStore = {};
      dataToStore.data = [];
      for (var j = 0; j < fields.files.length < 0; j++) {
        if (fields.files[j].sizeBytes > fileSize) {
          notie.alert("error", "File size is bigger than 50MB", 5);
          return;
        } else {
          selectedMediaSizeTotal += fields.files[j].sizeBytes;
        }
      }
     
      Globals.AWS_CONFIG.albumName = localStorage.getItem("albumName");
      var s3Config = Globals.AWS_CONFIG;
      var s3Manager = new s3functions.S3Manager(s3Config);
      var s3BucketURL = s3Manager.albumName;

      for (var k = 0; k < fields.files.length; k++) {
        var result = fields.files[k].name.split(".");
        var final = result.pop();
        var previous = result.join(".");
        fileName = previous !== "" ? previous : fields.files[k].name;
        var mimeType, extension;
        if (fields.files[k].mimeType == "application/msword") {
          mimeType = "application/doc";
          extension = "doc";
        } else if (
          fields.files[k].mimeType == "application/vnd.ms-powerpoint"
        ) {
          mimeType = "application/doc";
          extension = "ppt";
        } else if (fields.files[k].mimeType == "application/vnd.ms-excel") {
          mimeType = "application/doc";
          extension = "xls";
        } else {
          mimeType = fields.files[k].mimeType;
          extension = fields.files[k].mimeType.split("/")[1];
        }

        var arrData = {
          command: "handle-google-drive-file",
          file_id: fields.files[k].id,
          file_name: fields.files[k].name,
          mime_type: mimeType,
          google_access_token: this.state.oauthToken,
          extension: extension,
          s3BucketURL: s3BucketURL,
          size: fields.files[k].sizeBytes
        };
        dataToStore.data.push(arrData);
      }
      this.checkStorageSpace(dataToStore);
    }
  }

  driveFileSubmit(arrData) {
    this.props.postsActions.uploadDriveAsset(arrData);
  }

  assetBtnClick(selector) {
    document.getElementById(selector).click();
  }
  /**
   * @author disha
   * get free storage size from child (createPostFormDropzone)
   */
  fetchFreeStorage(e) {
    this.setState({
      freeStorage: e
    });
  }
  renderMediaUI() {
    var Drivestatus = JSON.parse(localStorage.getItem("isDriveConnected"));
    var Driveidentity = this.props.profile.profile.identity;
    let showMediaUI = this.state.showMediaInterface;
    freeStorage =
      this.props.general.s3SizeData !== ""
        ? Object.keys(this.props.general.s3SizeData.storage).length > 0
          ? this.props.general.s3SizeData.storage.freeStorage *
            Math.pow(1024, 2)
          : 0
        : "";
    var me = this;
    const { pristine, submitting } = this.props;
    let { uploadedItems, uploadDone, uploadStarted } = this.props.aws;
    if (showMediaUI) {
      return (
        <div className={`postsMediaUI create-post-image-wrapper`}>
          <div className="clearfix selectAssetTabWrapper">
            <div className="selectAssetTabs">
              <a
                href="javascript:void(0);"
                onClick={this.postAddAssetTab.bind(this, "assetLibrary")}
                className={`asset-lib ${this.state.currentPostAssetTab ==
                "assetLibrary"
                  ? "active btn-primary"
                  : ""}`}
              >
                {/* <i className='material-icons'>filter</i> */}
                <span class="buttonText">Assets Library</span>
              </a>
              <a
                href="javascript:void(0);"
                onClick={this.postAddAssetTab.bind(this, "uploadAsset")}
                className={`uploded-assets ${this.state.currentPostAssetTab ==
                "uploadAsset"
                  ? "active btn-primary"
                  : ""}`}
              >
                {/* <i className='material-icons'>file_upload</i> */}
                <span class="buttonText">Upload Assets</span>
              </a>

              <a
                href="javascript:void(0);"
                id="google-drive-btn"
                onClick={this.postAddAssetTab.bind(this, "GoogleDrive")}
                className={`${this.state.currentPostAssetTab == "GoogleDrive"
                  ? "active btn-primary"
                  : ""}`}
              >
                {/* <i className='material-icons'>file_upload</i> */}
                <span class="buttonText"> Drive </span>
              </a>
            </div>
            <button
              id="close-popup"
              className="btn-default"
              onClick={this.assetSelectCancel.bind(this)}
            >
              <i className="material-icons">clear</i>
            </button>
          </div>

          <div className="createPostTabContainer">
            <div
              className={`createPostDropSection postAddAssetTab ${this.state
                .currentPostAssetTab == "uploadAsset"
                ? "active"
                : ""}`}
            >
              <div className="clearfix postAddAssetTabInner">
                <div className="createPostUploadAssetSection">
                  <div className="popupDescSection">
                    {this.props.general.storageLoading == true &&
                    this.state.currentPostAssetTab !== "assetLibrary"
                      ? this.renderLoading()
                      : ""}

                    <CreatePostFormDropzone
                      ref="childDropzone"
                      isUploadingStart={this.props.posts.isUploadingStart}
                      freeStorage={this.fetchFreeStorage.bind(this)}
                      totalAssets={totalAssetsCount}
                      gifCount={gifCount}
                      handleDocCount={this.handleDocCount.bind(this)}
                      handleGifCount={this.handleGifCount.bind(this)}
                      onSubmit={this.handleFilesSubmit.bind(this)}
                    />
                  </div>
                </div>
              </div>
            </div>
            <div
              className={`createPostAssetSection postAddAssetTab  ${this.state
                .currentPostAssetTab == "assetLibrary"
                ? "active"
                : ""}`}
            >
              <div className="postAddAssetTabInner">
                <div class="listMenu">
                  <nav class="nav-side">
                    <ul class="parent clearfix">
                      {roleName !== "guest" ? (
                        <li>
                          <a
                            href="javascript:void(0);"
                            onClick={this.singleClickOpenCategory.bind(
                              this,
                              "category"
                            )}
                            class={
                              this.state.currentAssetTab === "category"
                                ? "active"
                                : ""
                            }
                          >
                            <span class="text">Category</span>
                          </a>
                        </li>
                      ) : (
                        ""
                      )}
                      {roleName !== "guest" ? (
                        <li>
                          <a
                            href="javascript:void(0);"
                            onClick={this.singleClickOpenCategory.bind(
                              this,
                              "campaign"
                            )}
                            class={
                              this.state.currentAssetTab === "campaign"
                                ? "active"
                                : ""
                            }
                          >
                            <span class="text">Campaign</span>
                          </a>
                        </li>
                      ) : (
                        ""
                      )}
                      <li>
                        <a
                          href="javascript:void(0);"
                          onClick={this.singleClickOpenCategory.bind(
                            this,
                            "custom Feed"
                          )}
                          class={
                            this.state.currentAssetTab === "custom Feed"
                              ? "active"
                              : ""
                          }
                        >
                          <span class="text">Custom Feed</span>
                        </a>
                      </li>
                    </ul>
                  </nav>
                </div>
                <div className="createPostAssetTabContainer">
                  <div className="popupDescSection">
                    <div
                      class={`asset-container assetsList ${uploadStarted
                        ? "disabled"
                        : ""}`}
                    >
                      <div className="breadcrunb-wrapper">
                        {this.renderBreadcrumbs()}
                      </div>
                      {this.props.assets.loadingFolder == true ||
                      this.props.general.storageLoading == true ? (
                        <div className="archive-container clearfix">
                          {Array(6)
                            .fill(1)
                            .map((el, i) => (
                              <div className="archive asset-dir folder">
                                <a className="file-details createpost-assets-loader">
                                  <span className="side">
                                    <div className="icon-loader loader-grey-line  loader-line-radius">
                                      {" "}
                                    </div>
                                  </span>
                                  <p className="foldername_assets">
                                    <div className="loader-line-height loader-grey-line  loader-line-radius title-createpost-assets">
                                      {" "}
                                    </div>
                                  </p>
                                </a>
                              </div>
                            ))}
                        </div>
                      ) : (
                        this.renderFolders(this.state.layoutDetail)
                      )}
                      {this.props.assets.assetLoading == true &&
                      this.state.currPage == 1 ? (
                        <div className="archive-container asset-file-container clearfix">
                          {Array(3)
                            .fill(1)
                            .map((el, i) => (
                              <div class="vid file-image asset-file imgwrapper">
                                <div class="inner_imgwrapper assetInnerWrapper">
                                  <a class="type-image">
                                    <span class="imageloader loaded thumbnail loader-assets-image">
                                      <img src="/img/visiblyLoader.gif" />
                                    </span>
                                  </a>
                                </div>
                                <a class="file-details">
                                  <span class="title">
                                    <div className="loader-grey-line loader-line-height loader-line-radius loader-assets-title">
                                      {" "}
                                    </div>
                                  </span>
                                </a>
                              </div>
                            ))}
                        </div>
                      ) : (
                        this.renderAssets()
                      )}
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div
              className={`createPostAssetDriveSection postAddAssetTab  ${this
                .state.currentPostAssetTab == "GoogleDrive"
                ? "active"
                : ""}`}
            >
              <div className="clearfix postAddAssetTabInner">
                <div className="createPostUploadAssetSection">
                  <div className="popupDescSection">
                    {Drivestatus !== null &&
                    Drivestatus[Driveidentity] == true ? (
                      <div className="google-drive-container">
                        <div className="inner-google-container">
                          {/* img part */}
                          <div className="main-container-of-img-block">
                            <div className="google-drive-img">
                              <img src="/img/Google_Drive_Logo.svg.png" />
                            </div>
                          </div>
                          <div className="text-part-of-google-drive">
                            <div>
                              {" "}
                              <p className="main-title-of-google-drive">
                                Select File From Google Drive
                              </p>
                            </div>
                            <div className="google-drive-button">
                              <a
                                className={`btn GDrive-btn ${showDriveUpload ==
                                true
                                  ? "disabled"
                                  : ""} `}
                                onClick={this.selectFromDrive.bind(this)}
                              >
                                {" "}
                                Choose file{" "}
                              </a>
                            </div>
                          </div>
                        </div>
                      </div>
                    ) : (
                      <div className="google-drive-container">
                        <div className="inner-google-container">
                          {/* img part */}
                          <div className="main-container-of-img-block">
                            <div className="google-drive-img">
                              <img src="/img/Google_Drive_Logo.svg.png" />
                            </div>
                          </div>
                          {/* end img part */}

                          <div className="text-part-of-google-drive">
                            <div>
                              {" "}
                              <p className="main-title-of-google-drive">
                                Select File From Google Drive
                              </p>
                            </div>
                            <div className="content-title-gDrive">
                              <p>
                                {" "}
                                You need to authenticate with Google Drive.
                              </p>
                              <p>
                                {" "}
                                We only extract images and never modify or
                                delete them.
                              </p>
                            </div>
                            <div className="google-drive-button">
                              <a
                                className={`btn GDrive-btn ${showDriveUpload ==
                                true
                                  ? "disabled"
                                  : ""} `}
                                onClick={this.selectFromDrive.bind(this)}
                              >
                                {" "}
                                Connect Google Drive{" "}
                              </a>
                            </div>
                            <div className="content-title-gDrive">
                              <p>
                                {" "}
                                A new page will open to connect your account.{" "}
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    )}

                    {this.renderDrive()}
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div
            className={`action-buttons clearfix ${uploadStarted
              ? "disabled"
              : ""}`}
          >
            <div className="bottom-right-buttons">
              {this.state.currentPostAssetTab == "assetLibrary" ? (
                this.state.media_select_btn ? (
                  <button
                    className="btn selected-assets"
                    onClick={this.assetSelectedBtnClicked.bind(this)}
                  >
                    Select
                  </button>
                ) : (
                  <button disabled className="btn selected-assets">
                    Select
                  </button>
                )
              ) : (
                ""
              )}
            </div>
          </div>
        </div>
      );
    } else {
      return null;
    }
  }
  

  assetSelect(asset) {

    tempAssetCount = totalAssetsCount + Object.keys(var_asset).length + 1;
    // deselect all assets first
    // if(asset.media_type.includes('application')!==true){
    if (tempAssetCount <= 9) {
      let uploadedFile = {
        fileUrl: asset.media_url,
        thumbUrl: asset.thumbnail_url,
        media_size: asset.media_size,
        mime_type: asset.media_type,
        file_extension: asset.media_extension,
        duration: asset.media_duration,
        identity: asset.identity
      };
      var video = document.createElement("video");
      video.preload = "metadata";
      video.onloadedmetadata = function() {
        window.URL.revokeObjectURL(asset.media_url);
        var duration = video.duration;
        asset.duration = duration;
      };
      video.src = asset.media_url;
      let selectedAssethtml = document.querySelector(
        `#asset-${asset.identity}`
      );
      if (selectedAssethtml.classList.contains("selectedAsset")) {
        if (asset.media_extension == "gif") {
          gifCount--;
        }
        if (asset.media_type.includes("application")) {
          docCount--;
        }
        tempAssetCount--;
        delete var_asset[asset.identity];
        selectedAssethtml.classList.remove("selectedAsset");
        Object.keys(var_asset).length > 0
          ? this.setState({
              media_select_btn: true
            })
          : this.setState({
              media_select_btn: false
            });
      } else {
        if (asset.media_extension == "gif") {
          gifCount++;
        }
        if (asset.media_type.includes("application")) {
          docCount++;
        }
        tempAssetCount++;
        selectedAssethtml.classList.add("selectedAsset");
        var_asset[asset.identity] = asset;
        this.setState({
          media_select_btn: true
        });
      }
    } else {
      let selectedAssethtml = document.querySelector(
        `#asset-${asset.identity}`
      );
      if (selectedAssethtml.classList.contains("selectedAsset")) {
        tempAssetCount--;
        delete var_asset[asset.identity];
        selectedAssethtml.classList.remove("selectedAsset");
        Object.keys(var_asset).length > 0
          ? this.setState({
              media_select_btn: true
            })
          : this.setState({
              media_select_btn: false
            });
      } else {
        notie.alert("error", "You can select a maximum of 9 media assets.", 3);
      }
    }
    // }
  }
  giphySelectedBtnClicked() {
    this.assetSelectCancel();
    this.cancelassetSelected();

    //giphy_selected=true;
    this.setState({
      showGiphy: !this.state.showGiphy,
      giphy_selected_btn: false,
      blDirectFileUpload: false,
      directFileUploadObj: null
    });
    document.getElementById("close-popup").classList.remove("hide");
    this.setState({
      selectedAsset: {},
      selectedAssetDetails: null,
      selectedAssetSize: 0
    });

    this.setState({
      selectGipy_child_state: selectGipy_child
    });
  }
  assetSelectedBtnClicked() {
    if (gifCount > 1 || AssetgifCount > 0 || driveGifCount > 0) {
      notie.alert("error", "You can only select one GIF at a time.", 3);
    } else if (
      gifCount !== 0 &&
      (Object.keys(var_asset).length > 1 ||
        Object.keys(this.state.selectedAsset).length > 0 ||
        this.state.uploadedFile.length > 0 ||
        this.state.uploadedeDriveFileData.length > 0)
    ) {
      notie.alert("error", "Please select either photos and video or separately GIF.", 3);
    } else if (docCount > 1 || assetDocCount > 0 || driveDocCount > 0) {
      notie.alert("error", "You can only select one document at a time.", 3);
    } else if (
      docCount !== 0 &&
      (Object.keys(var_asset).length > 1 ||
        Object.keys(this.state.selectedAsset).length > 0 ||
        this.state.uploadedFile.length > 0 ||
        this.state.uploadedeDriveFileData.length > 0)
    ) {
      notie.alert(
        "error",
        "Please select either video,  image(s) or document.",
        3
      );
    } else {
      this.setState({
        showGiphy: false,
        selectedGiphy: false,
        showMediaInterface: !this.state.showMediaInterface,
        media_select_btn: false,
        selectGipy_child_state: null
        // blDirectFileUpload: false
      });
      media_selected = true;
      selectGipy_child = null;
      // Object.assign(this.state.selectedAsset , var_asset);
      var dataPush = this.state.selectedAsset;
      Object.assign(dataPush, var_asset);
      this.setState(
        {
          selectedAsset: dataPush,
          selectedAssetDetails: var_asset,
          selectedAssetSize: var_asset
        },
        () => {
          var_asset = {};
        }
      );
      this.props.change("mediaUploadedasset", dataPush);
      document.getElementById("close-popup").classList.remove("hide");
      this.props.change("mediaGiphyDirectUpload", null);
    }
  }

  cancelassetSelected() {
    //Reset all the value in edit and share functionlity
    cancleflag = 1;
    selectedAssetDetails = "";
    editPostFlag = 0;
    this.props.change("mediaAsset", null);
    this.props.change("mediaAssetSize", null);
    this.props.change("mediaDirectUpload", null);
    this.props.change("mediaDriveUpload", null);
    this.props.change("mediaUploadedasset", null);
  }
  /**
   * to close gif picker popup
   * @author disha
   * @returns nothing
   */
  closeGiphyPopup() {
    this.setState({
      showGiphy: false
    });
  }
  giphySelectCancel() {
    this.setState({
      showGiphy: false,
      selectGipy_child_state: null,
      selectedGiphy: false
    });
    mediaExtensionList=[]
    this.props.change("mediaGiphyDirectUpload", null);
    document.getElementById("close-popup").classList.remove("hide");
  }
  assetRemoveSelected(identity, assetData) {
    let newArray = [];
    let editArray = this.state.editPostAssetData;
    if(assetData.editPostAsset==true){
      if (assetData.media_extension == "gif") {
        gifCount--;
      }
      if (assetData.media_type.includes("application")) {
        docCount--;
      }
      editArray = this.state.editPostAssetData.filter(function(item) {
        return item.media_url !== assetData.media_url;
      });
      this.setState(
        {
          editPostAssetData: editArray
        },
        () => {
          this.props.change("editAssetData", editArray);
        }
      );
      mediaExtensionList=[]
    }
    if (assetData.identity && assetData.editPostAsset!==true) {
      if (assetData.media_extension == "gif") {
        gifCount--;
      }
      if (assetData.media_type.includes("application")) {
        docCount--;
      }
      var tempObj = this.state.selectedAsset;
      delete tempObj[assetData.identity];
      this.setState(
        {
          selectedAsset: tempObj
        },
        () => {
          this.props.change("mediaUploadedasset", this.state.selectedAsset);
        }
      );
    }
      if (this.state.uploadedeDriveFileData.length > 0) {
        let dataFound = this.state.uploadedeDriveFileData.find(item => {
          return item.fileUrl == assetData.media_url;
        });
        if ( dataFound && dataFound.mime_type.includes("gif")) {
          gifCount--;
        }
        if (dataFound && dataFound.mime_type.includes("application")) {
          docCount--;
        }
        newArray = this.state.uploadedeDriveFileData.filter(function(item) {
          return item.fileUrl !== assetData.media_url;
        });
        this.setState(
          {
            uploadedeDriveFileData: newArray
          },
          () => {
            this.props.change("mediaDriveUpload", newArray);
          }
        );
      }
      if (this.state.uploadedFile.length > 0) {
        let dataFound = this.state.uploadedFile.find(item => {
          return item.fileUrl == assetData.media_url;
        });
        if ( dataFound && dataFound.mime_type.includes("gif")) {
          gifCount--;
        }
        if ( dataFound && dataFound.mime_type.includes("application")) {
          docCount--;
        }

        newArray = this.state.uploadedFile.filter(function(item) {
          return item.fileUrl !== assetData.media_url;
        });
        this.setState(
          {
            uploadedFile: newArray
          },
          () => {
            this.props.change("mediaDirectUpload", newArray);
          }
        );
      }
    if (
      Object.keys(this.state.selectedAsset).length == 0 &&
      newArray.length == 0 && editArray.length==0
    ) {
      media_selected = false;
      this.setState({
        media_select_btn: false
      });
      this.cancelassetSelected();
    }

    // media_selected = false;
    // this.setState({
    //   DriveData: '',
    //   DriveAssetsData: null,
    //   selectedAssetDetails: null,
    //   blDirectFileUpload: false,
    //   selectedAsset: null,
    //   showCroppedResult: false,
    //   displaymedia: false,
    //   media_wrapper_class: '',
    //   media_select_btn: false
    // })
    // this.cancelassetSelected()
  }
  assetSelectCancel() {
    AssetgifCount = 0;
    driveGifCount = 0;
    assetDocCount = 0;
    driveDocCount = 0;
    Globals.assetSelectCancel = true;
    tempAssetCount = 0;
    var_asset = {};
    if (this.state.DriveData !== "") {
      this.setState({
        DriveData: ""
        // uploadedeDriveFileData:[]
      });
      if (showDriveUpload == true) {
        Globals.tempSelection = true;
        this.props.postsActions.uploadDriveAsset(null, "discard");
      }
    }
    showDriveUpload = false;
    if (this.state.selectedAsset) {
      this.setState({
        showMediaInterface: false,
        displaymedia: false,
        media_wrapper_class: "",
        media_select_btn: false
      });
      media_selected = false;
    } else {
      this.setState({
        selectedAsset: {},
        editPostAssetData:[],
        freeStorage: 0,
        selectedAssetSize: 0,
        showMediaInterface: false,
        showCroppedResult: false,
        displaymedia: false,
        media_wrapper_class: "",
        media_select_btn: false,
        renderDriveUI: false
        // DriveData:''
      });

      //  this.cancelassetSelected()
    }
    document.getElementById("close-popup").classList.remove("hide");
  }
  renderSelectedGiphy() {
    let selectedGiphyUrl;
    if (this.state.selectedGiphy) {
      return (
        <div className="selected-imgwrapper">
          <i
            className="material-icons btn-remove-photo"
            onClick={this.giphySelectCancel.bind(this)}
          >
            clear
          </i>

          <label>Selected GIPHY</label>
          <div className="archive-container">
            <div className="selected_inner_imgwrapper">
              <div className="imgwrapper">
                <div className="inner_imgwrapper">
                  <div class="selected-giphy">
                    <span className="gif-clear-btn">
                      <i
                        className="material-icons btn-remove-photo"
                        onClick={this.giphySelectCancel.bind(this)}
                      >
                        clear
                      </i>
                    </span>

                    <GiphyPost
                      name={""}
                      extension={this.state.selectGipy_child_state.type}
                      identity={this.state.selectGipy_child_state.id}
                      media_type={`image/${this.state.selectGipy_child_state
                        .type}`}
                      thumbnail_url={
                        this.state.selectGipy_child_state.images.original.url
                      }
                      media_url={
                        this.state.selectGipy_child_state.images.original.url
                      }
                      approved="1"
                      width="320"
                      height="240"
                      detailView={false}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      );
    } else {
      return null;
    }
  }
  singleClickOpenCategory(action) {
    this.props.assetsActions.resetBreadCrums();
    this.props.assetsActions.resetFiles();
    this.setState({
      currentAssetTab: action,
      currPage: 1
    });
    if (action == "category") {
      this.setState({
        layoutDetail: this.props.assets.cats,
        isFromCategory: true,
        changeDisplayForAssetSection: false
      });
    } else if (action == "campaign") {
      this.setState({
        layoutDetail: this.props.assets.campaigns,
        isFromCategory: true,
        changeDisplayForAssetSection: false
      });
    } else if (action == "custom Feed") {
      this.setState({
        layoutDetail: this.props.assets.customfeedAsset,
        isFromCategory: true,
        changeDisplayForAssetSection: false
      });
    }
    this.setMeta({});
  }

  singleClickOpenFolder(identity) {
    this.props.assetsActions.resetFiles();
    this.props.assetsActions.fetchBreadCrums(identity);
    this.props.assetsActions.fetchFolders(identity);
    var categoryId = this.state.currentCategoryId;
    var folderId = identity;
    this.setState({ currPage: 1 });
    if (this.state.isFromCategory) {
      this.setState({
        isFromCategory: false,
        currentCategoryId: identity,
        currentFolderId: null
      });
      categoryId = identity;
      folderId = "";
    } else {
      this.setState({
        currentFolderId: identity
      });
    }
    this.fetchAssets(categoryId, folderId, 10, 1);
  }
  renderSelectedAsset() {
    var selectedAssets = [];
    var editpostdata = this.props.editPostData; //this is the local varible of store the data
    var selectedAsset = this.state.selectedAssetDetails;
    // selectedAssetDetails = this.state.selectedAssetDetails
    if (this.state.uploadedFile.length > 0) {
      let fileObj = this.state.directFileUploadObj;
      for (var data = 0; data < this.state.uploadedFile.length; data++) {
        let fileDetails = this.state.uploadedFile[data];
        var selectedAssetDetails = {
          identity: null,
          media_extension: fileDetails.file_extension
            ? fileDetails.file_extension
            : null,
          media_type: fileDetails.mime_type ? fileDetails.mime_type : null,
          thumbnail_url: fileDetails.thumbUrl,
          media_url: fileDetails.fileUrl,
          approved: true,
          category: null,
          title: fileDetails.uploadedFile
        };
        selectedAssets.push(selectedAssetDetails);
      }
    }
    if (
      this.state.selectedAsset !== null &&
      this.state.selectedAsset !== undefined
    ) {
      let fileObj = this.state.selectedAsset;
      Object.keys(this.state.selectedAsset).map((assetData, index) => {
        var fileObj = this.state.selectedAsset[assetData];
        var selectedAssetDetails = {
          identity: fileObj.identity,
          media_extension: fileObj.media_extension,
          media_type: fileObj.media_type,
          thumbnail_url: fileObj.thumbnail_url,
          media_url: fileObj.media_url,
          approved: true,
          category: fileObj.category,
          title: fileObj.title
        };
        selectedAssets.push(selectedAssetDetails);
      });
    }
    if (
      this.state.DriveAssetsData !== null &&
      this.state.uploadedeDriveFileData.length > 0
    ) {
      for (var i = 0; i < this.state.uploadedeDriveFileData.length; i++) {
        var selectedAssetDetails = {
          identity: null,
          media_extension: this.state.uploadedeDriveFileData[i].file_extension,
          media_type: this.state.uploadedeDriveFileData[i].mime_type,
          thumbnail_url: this.state.uploadedeDriveFileData[i].thumbUrl,
          media_url: this.state.uploadedeDriveFileData[i].fileUrl,
          approved: true,
          category: null,
          title: this.state.uploadedeDriveFileData[i].uploadedFile
        };
        selectedAssets.push(selectedAssetDetails);
      }
    }
    //in editandshare, if I first time click on  editandshare ,of post and I delete the asset ,then I set the value cancleflag = 1
    //so,I first time click on share the cancleflag == 0

    if (editpostdata != "" && cancleflag == 0 && editPostFlag == 1) {
      if (editpostdata.post.data.assetDetail != "") {

        for (var i = 0; i < this.state.editPostAssetData.length; i++) {
          var selectedAssetDetails = {
            identity: this.state.editPostAssetData[i].identity,
            media_extension: this.state.editPostAssetData[i].media_extension,
            media_type: this.state.editPostAssetData[i].media_type,
            thumbnail_url: this.state.editPostAssetData[i].thumbnail_url,
            media_url: this.state.editPostAssetData[i].media_url,
            approved: true,
            category: this.state.editPostAssetData[i].category,
            title: this.state.editPostAssetData[i].title,
            editPostAsset : this.state.editPostAssetData[i].editPostAsset
          };
          if(this.state.editPostAssetData[i].media_extension == 'gif'){
            if(mediaExtensionList.indexOf(this.state.editPostAssetData[i].media_extension) == -1){
              mediaExtensionList.push(this.state.editPostAssetData[i].media_extension)
            }
          }else if(mediaExtensionList.indexOf(this.state.editPostAssetData[i].media_type.split('/')[0]) == -1){
            mediaExtensionList.push(this.state.editPostAssetData[i].media_type.split('/')[0])
          }
          selectedAssets.push(selectedAssetDetails);
        }
      }
    }
    totalAssetsCount = Object.keys(selectedAssets).length;
    return (
      <div>
        {Object.keys(selectedAssets).length > 0 ? (
          <div className="selected-imgwrapper">
            <label>Selected Asset</label>
            <div className="archive-container moreImagesScrollbar">
              {Object.keys(selectedAssets).map((assetData, index) => {
                selectedAssetDetails = selectedAssets[assetData];
                return (
                  <div className="selected_inner_imgwrapper">
                    <div class="selected-asset">
                      <div className="select-asset-cross-wrapper">
                        <i
                          className="material-icons btn-remove-photo"
                          onClick={this.assetRemoveSelected.bind(
                            this,
                            assetData,
                            selectedAssetDetails
                          )}
                        >
                          clear
                        </i>
                      </div>
                      {/* <div className='edit-post-wrapper' onClick={this.editOnCreatPost.bind(this)}>
                <span class='edit-post'>
                  <i class='material-icons'></i>
                </span>
              </div> */}
                      <Asset
                        name={
                          selectedAssetDetails.title
                            ? selectedAssetDetails.title
                            : "image"
                        }
                        extension={selectedAssetDetails.media_extension}
                        identity={selectedAssetDetails.identity}
                        media_type={selectedAssetDetails.media_type}
                        thumbnail_url={selectedAssetDetails.thumbnail_url}
                        media_url={selectedAssetDetails.media_url}
                        media_url1={selectedAssetDetails.media_url}
                        approved={selectedAssetDetails.approved}
                        category_id={selectedAssetDetails.category}
                        preview={selectedAssetDetails.preview}
                        width="320"
                        height="240"
                        detailView={false}
                        selected={true}
                        editOnCratePostasset={this.editOnCreatPost.bind(this)}
                      />
                    </div>
                  </div>
                );
              })}
            {selectedAssetDetails.media_extension !== 'gif' ?
              <div
                className="moreImagesAdd"
                onClick={this.handleMediaUI.bind(this)}
              >
                <span>+</span>
              </div>
              :''}
            </div>
          </div>
        ) : (
          ""
        )}
      </div>
    );
  }
  inShareSelctedAsset() {
    if (assetType == "asset") {
      let uploadedFile = {
        identity: allDetailsEditPost.identity,
        fileUrl: allDetailsEditPost.media_url,
        thumbUrl: allDetailsEditPost.thumbnail_url,
        media_size: allDetailsEditPost.media_size,
        mime_type: allDetailsEditPost.media_type,
        file_extension: allDetailsEditPost.media_extension,
        duration: allDetailsEditPost.media_duration
      };
      this.props.change("mediaAsset", allDetailsEditPost.identity);
      this.props.change("mediaUploadedasset", uploadedFile);
      this.props.change("mediaAssetSize", allDetailsEditPost.media_size);
    } else {
      if (gifType == "gif") {
        let uploadedFile = {
          identity: null,
          fileUrl: allDetailsEditPost.media_url,
          thumbUrl: allDetailsEditPost.thumbnail_url,
          mime_type: allDetailsEditPost.media_type,
          filesize: allDetailsEditPost.media_size,
          file_extension: allDetailsEditPost.media_extension
        };
        this.props.change("mediaGiphyDirectUpload", uploadedFile);
        this.props.change("mediaAsset", null);
        this.props.change("mediaAssetSize", null);
        this.props.change("mediaUploadedasset", null);
      } else {
        let uploadedFile = {
          identity: null,
          fileUrl: allDetailsEditPost.media_url,
          thumbUrl: allDetailsEditPost.thumbnail_url,
          media_size: allDetailsEditPost.media_size,
          mime_type: allDetailsEditPost.media_type,
          file_extension: allDetailsEditPost.media_extension
        };
        this.props.change("mediaAsset", null);
        this.props.change("mediaDirectUpload", uploadedFile);
        this.props.change("mediaAssetSize", null);
        this.props.change("mediaUploadedasset", null);
      }
    }
  }
  renderFolders(detail = null) {
    let files = this.state.assets;
    if (detail == null && roleName !== "guest") {
      //when create post form popup open and user is not guest then as default to show all category data if user is not guest
      detail = this.props.assets.cats;
    } else if (
      detail !== null &&
      (this.state.currentAssetTab == "campaign" ||
        this.state.currentAssetTab == "custom Feed") &&
      roleName !== "guest"
    ) {
      //if tab is changed to campaing or custome feed and user is not a guest then to show its data
      detail = detail;
    } else if (
      detail == null &&
      this.state.currentAssetTab == "custom Feed" &&
      roleName == "guest"
    ) {
      //when create post form popup open and user is guest then as default to show custom feed data
      detail = this.props.assets.customfeedAsset;
    }
    return (
      <div>
        <div className="archive-container clearfix 00000">
          {detail.length > 0 || files.length > 0 ? (
            detail.map((catDetail, i) => {
              return (
                <div
                  className={`archive asset-dir ${!this.state
                    .changeDisplayForAssetSection
                    ? "category"
                    : "folder"}`}
                  key={`renderFolder_${i}`}
                >
                  <a
                    className={`file-details`}
                    onDoubleClick={this.singleClickOpenFolder.bind(
                      this,
                      catDetail.identity
                    )}
                  >
                    <span className="side">
                      <i className="material-icons">
                        {!this.state.changeDisplayForAssetSection
                          ? "list"
                          : "folder"}
                      </i>
                    </span>
                    <p className="foldername_assets">
                      {" "}
                      {catDetail.name}{" "}
                      {this.state.changeDisplayForAssetSection ? (
                        <span className="asset-count">
                          {catDetail.no_of_assets !== null
                            ? catDetail.no_of_assets + " items"
                            : ""}{" "}
                        </span>
                      ) : (
                        ""
                      )}
                    </p>
                  </a>
                </div>
              );
            })
          ) : (
            <div class="no-data-block">No assets found.</div>
          )}
        </div>
      </div>
    );
  }
  renderAssets() {
    let files = this.state.assets;
    let me = this;
    return (
      <div className="assetsList clearfix">
        <div className="archive-container">
          {files.map(function(file, index) {
            /* below is an example of how to use arrow functions to bind (not using .bind()) this value */
            return (
              <Asset
                onClick={asset => me.assetSelect(file)}
                key={index}
                name={file.title}
                extension={file.media_extension}
                identity={file.identity}
                media_type={file.media_type}
                media_size={file.media_size}
                thumbnail_url={file.thumbnail_url}
                media_url={file.media_url}
                approved={file.approved}
                category_id={file.category}
                width="320"
                height="240"
                detailView={false}
              />
            );
          })}
          {this.props.assets.assetLoading == true &&
          this.state.currPage !== 1 ? (
            <div>
              {Array(3)
                .fill(1)
                .map((el, i) => (
                  <div class="vid file-image asset-file imgwrapper">
                    <div class="inner_imgwrapper assetInnerWrapper">
                      <a class="type-image">
                        <span class="imageloader loaded thumbnail loader-assets-image">
                          <img src="/img/visiblyLoader.gif" />
                        </span>
                      </a>
                    </div>
                    <a class="file-details">
                      <span class="title">
                        <div className="loader-grey-line loader-line-height loader-line-radius loader-assets-title">
                          {" "}
                        </div>
                      </span>
                    </a>
                  </div>
                ))}
            </div>
          ) : (
            ""
          )}
        </div>
        <div className="center">
          {this.state.currPage !== me.state.meta.pagination.total_pages ? (
            <a
              className="btn btn-default"
              onClick={this.loadMoreAssets.bind(this)}
            >
              Load More
            </a>
          ) : null}
        </div>
      </div>
    );
  }
  handleChange(value) {
    // this.setState({description: value});
  }

  closeSchedule() {
    this.setState({
      showScheduler: false
    });
  }
  scheduleDataChange(index, e) {
    this.props.change("isScheduled", true);
    isDataChanged[index] = true;
    this.setState({
      isScheduleSelected: true
    });
  }
  renderSchedulerUI() {
    let showScheduler = this.state.showScheduler;
    var me = this;
    const { pristine, submitting } = this.props;
    var date = "Date";
    var time = "Time";
    var startDate = "";
    var endDate = "";
    if (
      this.props.posts.campaignCreatePostdata.campaign_start_date &&
      me.state.Moment
    ) {
      startDate = me.state.Moment
        .unix(this.props.posts.campaignCreatePostdata.campaign_start_date)
        .format("llll")
        .toString();
      endDate = me.state.Moment
        .unix(this.props.posts.campaignCreatePostdata.campaign_end_date)
        .format("llll")
        .toString();
    }
    if (showScheduler) {
      return (
        <div className="post-details-scheduler">
          <div className="hidden-inputs">
            <Field
              name="type"
              component="input"
              type="hidden"
              component={renderField}
              Value={me.state.type}
            />
            <Field
              name="schedulerCounter"
              component="input"
              type="hidden"
              component={renderField}
              value={me.state.schedulerCounter.toString()}
            />

            <Field
              name="mediaAsset"
              component="input"
              type="hidden"
              component={renderField}
              value={me.state.selectedAsset}
            />

            <Field
              name="mediaAssetSize"
              component="input"
              type="hidden"
              component={renderField}
              value={me.state.selectedAssetSize}
            />

            <Field
              name="mediaDirectUpload"
              component="input"
              type="hidden"
              component={renderField}
            />

            <Field
              name="mediaDriveUpload"
              component="input"
              type="hidden"
              component={renderField}
            />

            <Field
              name="mediaUploadedasset"
              component="input"
              type="hidden"
              component={renderField}
            />

            <Field
              name="editAssetData"
              component="input"
              type="hidden"
              component={renderField}
            />

            <Field
              name="mediaGiphyDirectUpload"
              component="input"
              type="hidden"
              component={renderField}
              value={me.state.uploadedGiphyFile}
            />
          </div>
          {/* <div onClick={this.closeSchedule.bind(this)} className='closeBtn schedule-close-btn hide'><i className='material-icons'>clear</i></div> */}
          <div className="post-details-schedulerInner">
            <div className="postScheduleTitles clearfix">
              <div className="scheduleDateColumn">
                <div className="label">Date</div>
              </div>
              <div className="scheduleTimeColumn">
                <div className="label">Time</div>
              </div>
            </div>
            <div className='postScheduleFields'>
              {me
                .state
                .dynamicSchedulers
                .map((sch, index) => {
                  return (
                    <div id={`scheduler-${index}`} key={`scheduler-${index}`} className='schedule'>
                      <div className='clearfix'>
                        <div className='scheduleDateColumn'>

                          <Field
                            ref={`scheduledDate${index}`}
                            name={`scheduledDate-${index}`}
                            component={props => {
                              return (<ReactDatePicker
                                {...props}
                                reactDatePicker={me.state.ReactDatePicker}
                                moment={me.state.Moment}
                                minDate={startDate}
                                maxDate={endDate}
                                dateFormat="DD/MM/YYYY" />)
                            }}
                            onChange={this.scheduleDataChange.bind(this, index)}
                          />

                        </div>
                        <div className='scheduleTimeColumn'>

                          <Field
                            ref={`scheduledTime${index}`}
                            name={`scheduledTime-${index}`}
                            component={props => {
                              return (<ReactTimePicker {...props} moment={me.state.Moment} />)
                            }}
                            onChange={this.scheduleDataChange.bind(this, index)}
                          />

                        </div>

                      <div className="scheduleActionColumn last">
                        <div className="addRemoveSchdule clearfix">
                          <a
                            className="deleteSchedule"
                            title="Remove schedule"
                            onClick={this.removeScheduler.bind(this, index)}
                          >
                            <i className="material-icons">remove</i>{" "}
                          </a>
                          <a
                            title="Add new schedule"
                            id="add-more-schedule"
                            onClick={this.addMoreScheduleRow.bind(this)}
                            className="addScheduleBtn"
                          >
                            <i className="material-icons">add</i>
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                );
              })}
            </div>
          </div>
        </div>
      );
    } else {
      return null;
    }
  }
  socialCheckboxChanged(e, me, type) {
    let totalCount=0;
    let videoCount =0;
    if(e.target.checked!==false){
    if(type=="facebookpage"|| type=="twitterpage" || type=="linkedin" || type=="facebook"|| type=="linkedinpage" || type=="twitter"){ 
      Object.keys(this.state.selectedAsset).map(asset=>{
        totalCount++;
        if(this.state.selectedAsset[asset].media_type.includes("video")){
          videoCount++;
        }
      })

      this.state.uploadedFile.map(asset=>{
        totalCount++;
        if(asset.mime_type.includes("video")){
          videoCount++;
        }
      })
      this.state.uploadedeDriveFileData.map(asset=>{
        totalCount++;
        if(asset.mime_type.includes("video")){
          videoCount++;
        }
      })
    }
  }

    if(videoCount>1){
      notie.alert("error","You can only select one video at a time.",3);
    }else if(videoCount!==0 && totalCount>1){
      notie.alert("error","LinkedIn, Facebook and Twitter only allows one media type at a time. Please select either video, image(s) or GIF.",3);
    }else if((type=="twitter" || type=="twitterpage")&& totalCount >4){
      notie.alert("error","Twitter only allows maximum 4 images at a time.",3);
    }else{
      
    var flag = 0;
    let socialCheckBoxes = document.querySelectorAll("input.social-checkbox");
    checked_social_twitter = false;
    if (e.target.classList.contains("internal")) {
      if (e.target.checked) {
        if (type == "internal") {
          var count_twitter = document.getElementById("twiter_limit_count");
          count_twitter.classList.remove("twiter_limits_count_display");

          var twiterEmojiOverlay = document.getElementsByClassName(
            "twitter-emoji-container"
          )[0];
          twiterEmojiOverlay.classList.remove("twiter-emoji-overlay");
          this.setState({ twitter: false });
        }
        me.uncheckAllExternalChannels(socialCheckBoxes);
        me.props.change(me.internalCheckBoxes[0], true);
        for (let ec = 0; ec < me.externalCheckboxes.length; ec++) {
          me.props.change(me.externalCheckboxes[ec], false);
        }
        me.props.change("type", "Internal");
        me.setState({
          wallCheckbox: ["internal"]
        });
      } else {
        me.setState({
          wallCheckbox: []
        });
        me.props.change(me.internalCheckBoxes[0], false);
        me.props.change("type", "");
      }
    } else {
      // me.uncheckInternalChannels();
      let loop_cnt = 0;
      var count_twiter = document.getElementById("twiter_limit_count");
      var twiterEmojiOverlay = document.getElementsByClassName(
        "twitter-emoji-container"
      )[0];
      for (let ec = 0; ec < me.externalCheckboxes.length; ec++) {
        if (e.target.checked) {
          me.props.change(me.internalCheckBoxes[0], false);
          if (this.state.wallCheckbox.indexOf("internal") > -1) {
            var wallCheckboxArray = this.state.wallCheckbox;
            var index = wallCheckboxArray.indexOf("internal");
            wallCheckboxArray.splice(index, 1);
            this.setState({ wallCheckbox: wallCheckboxArray });
            let internalCheckBox = document.querySelector(
              "input.social-checkbox.internal"
            );
            if (internalCheckBox !== null) {
              internalCheckBox.checked = false;
              internalCheckBox.value = false;
            }
            this.props.change(`chk-${this.internalCheckBoxes[0]}-0`, false);
          }
          if (
            e.target.name.split("-")[1] === "twitter" ||
            e.target.name.split("-")[1] === "twitterpage"
          ) {
            checked_social_twitter = true;

            this.TwitterCount(start);
            if (loop_cnt == 0) {
              if (
                !count_twiter.className.includes("twiter_limits_count_display")
              ) {
                count_twiter.className += " twiter_limits_count_display";
                if (document.querySelector(".twitterMsg") !== null) {
                  document.querySelector(".twitterMsg").style.display = "block";
                }
              }
            }
            this.setState({ twitter: true }, () => this.checkLimitPostDesc());
          }
          me.setState({
            wallCheckbox: [
              ...this.state.wallCheckbox,
              e.target.name.split("-")[1]
            ]
          });

          if (me.externalCheckboxes[ec] == e.target.name) {
            me.props.change(`chk-${this.internalCheckBoxes[0]}-0`, false);
            me.props.change(me.externalCheckboxes[ec], true);
            me.props.change("type", "External");
          }
        } else {
          //to disselect twitter or twitter page
          if (e.target.classList[2] == "twitter") {
            if (
              me.externalCheckboxes[ec].split("-")[2] ==
                e.target.name.split("-")[2] ||
              me.externalCheckboxes[ec] == e.target.name
            ) {
              var wallCheckboxArray = this.state.wallCheckbox;
              var index = wallCheckboxArray.indexOf(
                e.target.name.split("-")[1]
              );
              wallCheckboxArray.splice(index, 1);
              this.setState({ wallCheckbox: wallCheckboxArray });
            }

            //if twitter and twitter page both are unselected then to remove count number
            if (
              this.state.wallCheckbox.indexOf("twitter") == -1 &&
              this.state.wallCheckbox.indexOf("twitterpage") == -1
            ) {
              var twiterEmojiOverlay = document.getElementsByClassName(
                "twitter-emoji-container"
              )[0];
              twiterEmojiOverlay.classList.remove("twiter-emoji-overlay");
              count_twiter.classList.remove("twiter_limits_count_display");
              if (document.querySelector(".twitterMsg") !== null) {
                document.querySelector(".twitterMsg").style.display = "none";
              }
              this.setState({ cntnum: 0 });
              this.setState(
                {
                  twitter: false
                },
                () => this.checkLimitPostDesc()
              );
              checked_social_twitter = false;
            }
          } else {
            //to disselect channels like fb page, linkedin or linkedin page
            if (
              me.externalCheckboxes[ec].split("-")[2] ==
                e.target.name.split("-")[2] ||
              me.externalCheckboxes[ec] == e.target.name
            ) {
              var wallCheckboxArray = this.state.wallCheckbox;
              var index = wallCheckboxArray.indexOf(
                e.target.name.split("-")[1]
              );
              wallCheckboxArray.splice(index, 1);
              this.setState({ wallCheckbox: wallCheckboxArray });
            }
          }
        }
        loop_cnt++;
      }
    }
    for (var i = 0; i < socialCheckBoxes.length; i++) {
      if (socialCheckBoxes[i].checked) {
        this.setState({ create_btnchange: false });
        flag = 1;
      }
    }
    if (flag == 0) {
      this.setState({ create_btnchange: true });
    }
  }
  }

  uncheckAllExternalChannels(checkboxes) {
    for (let sc = 0; sc < checkboxes.length; sc++) {
      checkboxes[sc].checked = false;
    }
    for (let ec = 0; ec < this.externalCheckboxes.length; ec++) {
      this.props.change(this.externalCheckboxes[ec], false);
    }
  }

  uncheckInternalChannels() {
    let internalCheckBox = document.querySelector(
      "input.social-checkbox.internal"
    );
    if (internalCheckBox !== null) {
      internalCheckBox.checked = false;
      internalCheckBox.value = false;
    }
    this.props.change(this.internalCheckBoxes[0], false);
  }

  renderLoading() {
    return (
      <div className="preloader-wrap">
        <PreLoader />
      </div>
    );
  }

  handleUTMLink() {
    this.setState({
      showUTMPopup: !this.state.showUTMPopup
    });

    var postDesc = document.getElementsByName("description")[0]
      ? document.getElementsByName("description")[0].value
      : "";

    selectionstart = document.getElementsByName("description")[0]
      ? document.getElementsByName("description")[0].selectionStart
      : null;
    selectionend = document.getElementsByName("description")[0]
      ? document.getElementsByName("description")[0].selectionEnd
      : null;
    var description = postDesc.substring(selectionstart, selectionend);
    if (description !== "") {
      var links = utils.checkLinkInPost(description);
      if (!links.length > 0) {
        isValidUrl = false;
      } else {
        isValidUrl = true;
      }
    }
    if (isValidUrl == true) {
      if (selectionstart !== selectionend) {
        webLinkmain = postDesc.substring(selectionstart, selectionend);
        if (webLinkmain.includes("?")) {
          utmParameter = "&";
        }
        this.props.change("websiteUrl", webLinkmain);
        this.setState({
          showUTMPopup: !this.state.showUTMPopup
        });
      }
    } else {
      if (selectionstart !== selectionend) {
        notie.alert("error", "Invalid URL", 3);
        this.setState({
          showUTMPopup: false
        });
      }
      this.props.change("websiteUrl", "");
      this.setState({
        utm_campaign: "social",
        utm_source: "visibly",
        utm_medium: "visibly_web"
      });
    }
  }
  handleWebsiteUrl(e) {
    var utmChar = e.target.value.charAt(e.target.value.length - 1);
    if (utmChar == " ") {
      e.preventDefault();
    }
  }
  renderUtmLinkPopup() {
    if (this.state.showUTMPopup == true) {
      return (
        <div className="utmlinkWrapper" id="utmLinkPopup-toggle">
          <div className="popup-container">
            <div className="postScheduleSelectedImageWrapper schedule-popup">
              <div className="post-details-scheduler">
                <div>
                  <label>Website URL:</label>
                  <Field
                    id="websiteUrl"
                    name="websiteUrl"
                    type="text"
                    placeholder="Website URL"
                    component={renderField}
                    validate={[required]}
                    onChange={this.handleWebsiteUrl.bind(this)}
                  />
                  <p>{`${utmParameter}utm_campaign=${this.state
                    .utm_campaign}&utm_source=${this.state
                    .utm_source}&utm_medium=${this.state.utm_medium}`}</p>

                  <div className="utmLinkWrapper">
                    <div className="utmWrapper clearfix">
                      <div className="utmSourceWrapper">
                        <label>UTM Source :</label>

                        <Field
                          id="utmSource"
                          name="utmSource"
                          component={renderField}
                          onChange={this.changeUtmParams.bind(this)}
                          type="text"
                          value={this.state.utm_source}
                          validate={[required]}
                          placeholder="UTM Source"
                        />
                      </div>

                      <div className="utmMediumWrapper">
                        <label>UTM Medium : </label>
                        <Field
                          id="utmMedium"
                          name="utmMedium"
                          component={renderField}
                          onChange={this.changeUtmParams.bind(this)}
                          type="text"
                          value={this.state.utm_medium}
                          validate={[required]}
                          placeholder="UTM Medium"
                        />
                      </div>

                      <div className="utmCampaignWrapper">
                        <label>UTM Campaign : </label>
                        <Field
                          id="utmCampaign"
                          name="utmCampaign"
                          component={renderField}
                          onChange={this.changeUtmParams.bind(this)}
                          type="text"
                          value={this.state.utm_campaign}
                          validate={[required]}
                          placeholder="UTM Campaign"
                        />
                      </div>

                      <button
                        className={`btn btn-theme`}
                        disabled={this.props.posts.utmLinkLoading}
                        onClick={this.generateShortLink.bind(this)}
                      >
                        Generate short UTM link
                      </button>
                      {this.props.posts.utmLinkLoading == true ? (
                        <div class="preloaderWrapPage">
                          <div class="visibly-loader">
                            <img src="/img/visibly-loader.gif" alt="Loader" />
                          </div>
                        </div>
                      ) : (
                        ""
                      )}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      );
    }
  }

  changeUtmParams(e) {
    var utmChar = e.target.value.charAt(e.target.value.length - 1);
    if (utmChar == " ") {
      e.preventDefault();
    } else {
      switch (e.target.name) {
        case "utmSource":
          this.setState({
            utm_source: e.target.value
          });
          break;

        case "utmCampaign":
          this.setState({
            utm_campaign: e.target.value
          });
          break;

        case "utmMedium":
          this.setState({
            utm_medium: e.target.value
          });
          break;
      }
    }
  }
  generateShortLink() {
    mainUtmUrl = document.getElementById("websiteUrl")
      ? document.getElementById("websiteUrl").value
      : "";
    if (mainUtmUrl.includes("?")) {
      utmParameter = "&";
    }
    var url = `${mainUtmUrl}${utmParameter}utm_source=${this.state
      .utm_source}&utm_medium=${this.state.utm_medium}&utm_campaign=${this.state
      .utm_campaign}`;
    var links = utils.checkLinkInPost(url);
    if (!links.length > 0) {
      isValidUrl = false;
    } else {
      isValidUrl = true;
    }
    if (
      mainUtmUrl !== "" &&
      this.state.utm_source !== "" &&
      this.state.utm_medium !== "" &&
      this.state.utm_campaign !== ""
    ) {
      if (isValidUrl == true) {
        this.props.postsActions.generateUtmShortLink(links);
      } else {
        this.setState({
          showUTMPopup: false,
          utm_campaign: "social",
          utm_source: "visibly",
          utm_medium: "visibly_web"
        });
        utmParameter = "?";
        notie.alert("error", "Invalid URL", 3);
      }
    }
  }

  selectAssetDataEdit() {
    var postData = "";
    //var editPostData  = '' ;  //this is the local varible of editPostData
    editPostFlag = 0; //this flag if I edit the post with media then editPostFlag ==1
    allDetailsEditPost = []; //store the all the value of asset
    descriptionEditPost = ""; //for share and edit ,description details
    assetType = "";
    gifType = null;
    postData = this.props.editPostData;
    descriptionEditPost = postData.post.data.detail;
    // //when we edit the asset with media then check the assetdetails in props

    if (postData.post.data.assetDetail.data.length !== 0) {
        media_selected = true;
      postData.post.data.assetDetail.data.map(assetDetails => {
        editPostFlag = 1;
        assetType = assetDetails.type;
        //its give me data 2 type : 1)asset 2)local
        //when we uploaded the asset in computer then type == local
        //when we selected the asset into assets then type == asset
        if (assetDetails.type == "asset") {
          // gifType = assetDetails.asset.data.media_extension;
          let assetDetailData = {
            editPostAsset : true,
            media_extension: assetDetails.asset.data.media_extension,
            media_size: assetDetails.asset.data.media_size,
            identity: assetDetails.asset.data.identity,
            media_type: assetDetails.asset.data.media_type,
            thumbnail_url: assetDetails.asset.data.thumbnail_url,
            media_url: assetDetails.asset.data.media_url
          };
          allDetailsEditPost.push(assetDetailData);
        } else {
          // gifType = assetDetails.media_extension;
          let assetDetailData = {
            editPostAsset : true,
            media_extension: assetDetails.media_extension,
            identity: assetDetails.identity,
            media_size: assetDetails.media_size,
            media_type: assetDetails.media_type,
            thumbnail_url: assetDetails.thumbnail_url,
            media_url: assetDetails.media_url
          };
          allDetailsEditPost.push(assetDetailData);
        }
        if(disableEditPostDataTemp==false){
        this.setState({
          editPostAssetData : allDetailsEditPost
        })
        this.props.change("editAssetData",allDetailsEditPost)}
        disableEditPostDataTemp = true;
      });
    }
  }
  showValuePopup(){
    var post_desc = document.getElementsByName("description")[0].value
      if(this.state.wallCheckbox.length>0)
      {
        this.setState({
          showValuePopup:true,
        })
        this.props.postsActions.getAIValue(post_desc);
      }
      else
      {
        notie.alert('error', 'You must select at least 1 channel', 5)
      }
  }
  backPopUp(){

    this.setState({
      showValuePopup:false,
    })


  }
   

  
  renderPostCreationStep1() {
    var editPostData = null; //for edit and share data
    editPostData = this.props.editPostData;
    const { loading } = this.state.loading;
    const post = this.props.post;
    const { pristine, submitting, tags } = this.props;
    var me = this;
    var userImage = "";
    var campaignChannelName = [];
    var internalDisable = false;
    var internalDisableForCampaign = false;
    let social,
      users,
      options = [];

    //when we first time click on editandshare ,cancleflag == 0 ,
    //if I deleted the media then cancleflag == 1
    if (editPostData !== null && cancleflag == 0) {
      //when edit the post this method is called
      me.selectAssetDataEdit();
    }

    users =
      typeof me.props.users !== "undefined" && me.props.users
        ? me.props.users
        : [];
    options =
      typeof me.props.tags !== "undefined" && me.props.tags
        ? me.props.tags
        : [];
    social = typeof me.props.accounts !== "undefined" ? me.props.accounts : [];
    var start_date = null;
    var end_date = null;
    this.externalCheckboxes = [];
    this.internalCheckBoxes = ["internal"];
    this.allSocialChannels = Globals.EXTERNAL_SOCIAL.slice();
    userImage = this.props.profile.profile.avatar;
    let userTags = this.props.userTags;
    let postTags = this.props.postTags;
    let formRowClass = `form-row`;
    var iscompany = 0;
    var fbavatar,
      fbpageavatar = "";
    var twavatar = "";
    var twpageavatar = "";
    var liavatar = "";
    var lipageavatar = "";
    var defaultimg = "https://app.visibly.io/img/default-avatar.png";
    //==
    var obj = {};
    var currFeedStatus = "";
    var path = utils
      .removeTrailingSlash(this.props.location.pathname)
      .split("/");
    if (path[2] == "default") {
      currFeedStatus = this.props.defaultFeed.type;
    } else if (path[2] == "my") {
      //if feed type is changed from filter then to display its name on create post small popup
      currFeedStatus =
        this.props.currentFeed.type !== undefined &&
        this.props.currentFeed.type !== ""
          ? this.props.currentFeed.type
          : this.props.defaultFeed.type;
    }


    else {
      currFeedStatus = this.props.currentFeed.type;
    }
    social.map((s, i) => {
      if (s.social_media == "facebook") {
        if (s.iscompany == 1) {
          Object.keys(s.user_social).map((a, i) => {
            if (s.user_social[i]["key"] == "avatar") {
              fbpageavatar = s.user_social[i]["value"];
            }
          });
        } else {
          Object.keys(s.user_social).map((a, i) => {
            if (s.user_social[i]["key"] == "avatar") {
              fbavatar = s.user_social[i]["value"];
            }
          });
        }
      }
      if (s.social_media == "twitter") {
        if (s.iscompany == 1) {
          Object.keys(s.user_social).map((a, i) => {
            if (s.user_social[i]["key"] == "avatar") {
              twpageavatar = s.user_social[i]["value"];
            }
          });
        } else {
          Object.keys(s.user_social).map((a, i) => {
            if (s.user_social[i]["key"] == "avatar") {
              twavatar = s.user_social[i]["value"];
            }
          });
        }
      }
      if (s.social_media == "linkedin") {
        if (s.iscompany == 1) {
          Object.keys(s.user_social).map((a, i) => {
            if (s.user_social[i]["key"] == "avatar") {
              lipageavatar = s.user_social[i]["value"];
            }
          });
        } else {
          Object.keys(s.user_social).map((a, i) => {
            if (s.user_social[i]["key"] == "avatar") {
              liavatar = s.user_social[i]["value"];
            }
          });
        }
      }
    });
    var enableSocialFlag=this.props.profile.profile.enable_social_posting;
    var savedHashtagDetail=this.props.savedHashTagListInPost.hashTagListInCreatePost? this.props.savedHashTagListInPost.hashTagListInCreatePost.detail:''
    return (
      <div className="active">
        <div>
          {loading ? this.renderLoading() : <div />}
          {this.state.showValuePopup ?
          <ValueAI 
            aiValue={this.props.posts.aiValue != null ?this.props.posts.aiValue:''}
            aiValueError={this.props.posts.aiValueError}
            backPopUp={this.backPopUp.bind(this)}
            culture ={this.props.culture.cultureValueList !=null ? this.props.culture.cultureValueList: []}
            loader={this.props.posts.aiValueLoading}
            change={this.props.change}
          />:
          <div>
          {currFeedStatus != "internal" ? (
            <div className="social-checkboxes">
              <div className="channel-type-label-wrapper">
                <label for="social-checkbox">Channels</label>
              </div>
              <div className="social-checkbox-wrapper clearfix">
                {/* if post is in edit mode then editpostData will have value ,to restrict selecting internal */}
                {this.props.editPostData !== null
                  ? (internalDisable = true)
                  : (internalDisable = false)}
                {this.props.posts.campaignCreatePostdata.SocialChannel
                  ? this.props.posts.campaignCreatePostdata.SocialChannel.data
                      .length > 0
                    ? this.props.posts.campaignCreatePostdata.SocialChannel.data[0].media.toLowerCase() !==
                      "internal"
                      ? (internalDisableForCampaign = true)
                      : (internalDisableForCampaign = false)
                    : ""
                  : ""}
                  {internalDisableForCampaign ? this.state.wallCheckbox.indexOf("internal") > -1? this.state.wallCheckbox.splice(this.state.wallCheckbox.indexOf("internal")):"":"" }
                {currFeedStatus == "both" ? (
                  <div
                    className={`social-checkbox internal ${internalDisable 
                      ? "disabled"
                      : ""}`}
                    name="internal"
                    data-tip={
                      "This social channel is not available for this campaign."
                    }
                    data-for={`notAvailableSocialChannel_internal`}
                  >
                    <Field
                      name={`chk-internal-0`}
                      checked={
                        this.state.wallCheckbox.indexOf("internal") > -1 
                          ? true
                          : false
                      }
                      className={`image-checkbox social-checkbox internal`}
                      id={`chk-0`}
                      onChange={e =>
                        me.socialCheckboxChanged(e, me, "internal")}
                      component="input"
                      disabled={`${internalDisableForCampaign ? true : ""}`}
                      type="checkbox"
                      ref={"check_0"}
                      label="Channel"
                    />
                    <label for={`chk-0`}>
                      {/* <i class="material-icons">vpn_lock</i> */}
                      <img
                        src={userImage ? userImage : defaultimg}
                        alt="no-img"
                        onError={e => {
                          e.target.src =
                            "https://app.visibly.io/img/default-avatar.png";
                        }}
                      />
                      <span className="internalIconWrapper channelIconWrapper">
                        <i class="material-icons">vpn_lock</i>
                      </span>
                    </label>
                    {internalDisable || internalDisableForCampaign ? (
                      this.state.tooltip ? (
                        <this.state.tooltip
                          type="info"
                          id={`notAvailableSocialChannel_internal`}
                          effect="solid"
                          className="tooltipsharepopup"
                        />
                      ) : (
                        ""
                      )
                    ) : null}
                  </div>
                ) : (
                  ""
                )}
                {enableSocialFlag == 1 ?
                social.map((social, i) => {
                  iscompany = social.iscompany;

                  social.iscompany == 1
                    ? this.externalCheckboxes.push(
                        `chk-${social.social_media.toLowerCase() +
                          "page"}-${social.identity}`
                      )
                    : this.externalCheckboxes.push(
                        `chk-${social.social_media.toLowerCase()}-${social.identity}`
                      );

                  //add fb condition to stop splicing its value from allsocialchannels so that it can display as inactive
                  var connectedSocialAccIndex =
                    social.social_media.toLowerCase() !== "facebook"
                      ? this.allSocialChannels.indexOf(
                          social.social_media.toLowerCase()
                        )
                      : -1;

                  if (connectedSocialAccIndex > -1) {
                    this.allSocialChannels.splice(connectedSocialAccIndex, 1);
                  }
                  let disableSocialChannel = false;
                  var campaignSocialChannel = this.props.posts
                    .campaignCreatePostdata.SocialChannel;
                  if (campaignSocialChannel !== undefined) {
                    if (campaignSocialChannel.data.length > 0) {
                      //loop for store media name of selected campaign channel
                      campaignSocialChannel.data.map(data => {
                        campaignChannelName.push(data.media.toLowerCase());
                      });
                      if (
                        campaignChannelName.indexOf(
                          social.social_media.toLowerCase()
                        ) < 0
                      ) {
                        //to disable social channel which is not selected in campaign
                        disableSocialChannel = true;
                      }
                    }
                  }
                  return (
                    <div
                      key={`socialCheckbox_${i}`}
                      className={`social-checkbox`}
                      name={
                        iscompany == 0
                          ? social.social_media.toLowerCase()
                          : `${social.social_media.toLowerCase()}-page`
                      }
                      data-tip={
                        "This social channel is not available for this campaign."
                      }
                      data-for={`notAvailableSocialChannel${social.social_media.toLowerCase()}`}
                    >
                      {iscompany == 0 &&
                      social.social_media.toLowerCase() !== "facebook" ? (
                        <div>
                          <Field
                            name={`chk-${social.social_media.toLowerCase()}-${social.identity}`}
                            checked={
                              this.state.wallCheckbox.indexOf(
                                social.social_media
                              ) > -1
                                ? true
                                : false
                            }
                            disabled={`${disableSocialChannel ? true : ""}`}
                            className={`image-checkbox social-checkbox ${social.social_media.toLowerCase()} `}
                            id={`chk-${social.identity}`}
                            onChange={e =>
                              me.socialCheckboxChanged(
                                e,
                                me,
                                social.social_media.toLowerCase()
                              )}
                            component="input"
                            type="checkbox"
                            ref={"check_" + social.identity}
                            label="Channel"
                          />
                          <label for={`chk-${social.identity}`}>
                            <img
                              onError={e => {
                                e.target.src =
                                  "https://app.visibly.io/img/default-avatar.png";
                              }}
                              src={
                                social.social_media.toLowerCase() == "facebook"
                                  ? fbavatar ? fbavatar : defaultimg
                                  : social.social_media.toLowerCase() ==
                                    "twitter"
                                    ? twavatar ? twavatar : defaultimg
                                    : social.social_media.toLowerCase() ==
                                      "linkedin"
                                      ? liavatar ? liavatar : defaultimg
                                      : defaultimg
                              }
                              alt="no-img"
                            />
                            <span
                              className={`${social.social_media.toLowerCase()}IconWrapper channelIconWrapper`}
                            >
                              <i
                                class={`fa fa-${social.social_media.toLowerCase()}`}
                              />
                            </span>
                          </label>
                        </div>
                      ) : iscompany == 1 ? (
                        <div>
                          <Field
                            name={`chk-${social.social_media.toLowerCase()}page-${social.identity}`}
                            checked={
                              this.state.wallCheckbox.indexOf(
                                social.social_media + "page"
                              ) > -1
                                ? true
                                : false
                            }
                            className={`image-checkbox social-checkbox ${social.social_media.toLowerCase()} `}
                            id={`chk-${social.identity}`}
                            onChange={e =>
                              me.socialCheckboxChanged(
                                e,
                                me,
                                social.social_media.toLowerCase() + "page"
                              )}
                            disabled={`${disableSocialChannel ? true : ""}`}
                            component="input"
                            type="checkbox"
                            ref={"check_" + social.identity}
                            label="Channel"
                            value={"true"}
                          />
                          <label for={`chk-${social.identity}`}>
                            <img
                              onError={e => {
                                e.target.src =
                                  "https://app.visibly.io/img/default-avatar.png";
                              }}
                              src={
                                social.social_media.toLowerCase() == "facebook"
                                  ? fbpageavatar ? fbpageavatar : defaultimg
                                  : social.social_media.toLowerCase() ==
                                    "twitter"
                                    ? twpageavatar ? twpageavatar : defaultimg
                                    : social.social_media.toLowerCase() ==
                                      "linkedin"
                                      ? lipageavatar ? lipageavatar : defaultimg
                                      : defaultimg
                              }
                            />
                            <span
                              className={`${social.social_media.toLowerCase()}IconWrapper channelIconWrapper`}
                            >
                              <i
                                class={`fa fa-${social.social_media.toLowerCase()}`}
                              />
                            </span>
                          </label>
                        </div>
                      ) : (
                        ""
                      )}
                      {disableSocialChannel ? (
                        this.state.tooltip ? (
                          <this.state.tooltip
                            id={`notAvailableSocialChannel${social.social_media.toLowerCase()}`}
                            type="info"
                            effect="solid"
                            className="tooltipsharepopup"
                          />
                        ) : (
                          ""
                        )
                      ) : null}
                    </div>
                  );
                }):''}
                {enableSocialFlag == 1 ?
                this.allSocialChannels.map((notConnectedSocialAcc, i) => {
                  return (
                    <div key={`socialkey_${i}`}>
                      <div
                        className="social-checkbox"
                        data-tip={
                          "Please connect your social account in settings"
                        }
                        data-for={"notConnectedSocialChannels"}
                      >
                        {/* {notConnectedSocialAcc == 'facebook'
                        ?
                        <label className="notconnect">
                          <img src="https://app.visibly.io/img/default-avatar.png"/>
                          <i className='fa fa-facebook' aria-hidden='true' />
                          </label>
                          : null} */}
                        {notConnectedSocialAcc == "twitter" ? (
                          <label className="notconnect">
                            <Link to="/settings/social">
                            <img src="https://app.visibly.io/img/default-avatar.png" />
                            <span className="twitterIconWrapper channelIconWrapper">
                              <i className="fa fa-twitter" aria-hidden="true" />
                            </span>
                            </Link>
                          </label>
                        ) : null}
                        {notConnectedSocialAcc == "linkedin" ? (
                          <label className="notconnect">
                            <Link to="/settings/social">
                            <img src="https://app.visibly.io/img/default-avatar.png" />
                            <span className="linkedinIconWrapper channelIconWrapper">
                              <i
                                className="fa fa-linkedin"
                                aria-hidden="true"
                              />
                            </span>
                            </Link>
                          </label>
                        ) : null}
                      </div>
                      {this.state.tooltip ? (
                        <this.state.tooltip
                          type="info"
                          id="notConnectedSocialChannels"
                          effect="solid"
                          className="tooltipsharepopup"
                        />
                      ) : (
                        ""
                      )}
                    </div>
                  );
                }):''}
              </div>
            </div>
          ) : (
            <div className="social-checkboxes">
             <div className="channel-type-label-wrapper">
             <label for="social-checkbox">Channels</label>
            </div>
              <div className="social-checkbox-wrapper clearfix">
              <div
                  className={`social-checkbox internal`}
                    name="internal"
                    data-for={`notAvailableSocialChannel_internal`}
                  >
                    <Field
                      name={`chk-internal-0`}
                      checked={
                         true
                      }
                      className={`image-checkbox social-checkbox internal`}
                      id={`chk-0`}
                      onChange={e =>
                        me.socialCheckboxChanged(e, me, "internal")}
                      component="input"
                      disabled={`${internalDisableForCampaign ? true : ""}`}
                      type="checkbox"
                      ref={"check_0"}
                      label="Channel"
                    />
                    <label for={`chk-0`}>
                      {/* <i class="material-icons">vpn_lock</i> */}
                      <img
                        src={userImage ? userImage : defaultimg}
                        alt="no-img"
                        onError={e => {
                          e.target.src =
                            "https://app.visibly.io/img/default-avatar.png";
                        }}
                      />
                      <span className="internalIconWrapper channelIconWrapper">
                        <i class="material-icons">vpn_lock</i>
                      </span>
                    </label>
                  </div>
                </div>
              </div>
          )}
          <div class="socialError">
            <span class="error" />
          </div>
          <div>
            <div className="form-row user-tag-editor">
              {/* <label for="Post Content">CREATE POST</label> */}
              <div className="createPostDescritionWrapper">
                <Field
                  ref="description"
                  name="description"
                  placeholder="What’s new?"
                  onChange={(select) => this.trimTagAndPassCount(select)}
                  component={MentionTextArea}
                  userTags={userTags}
                  tags={postTags}   
                  id='post_description'
                  // onKeyUp={e=>this.restrictToRemoveSavedHashtag(e)}
                  onClick={select => this.focustextbox(select)}
                  onKeyDown={e=>this.restrictToRemoveSavedHashtag(e)}
                  // onKeyDown={select => this.focustextbox(select)}
                />
              <span id="twiter_limit_count"
                  className= {`cnt ${checked_social_twitter == true? this.state.cntnum < 0 ?'twiter_limits_count_display limitover':'twiter_limits_count_display':''}`}
                  >
                  {this.state.cntnum}</span>
              </div>
              <Field
                name="emoji_unicode"
                type="hidden"
                component={MentionTextArea}
                userTags={userTags}
                component={renderField}
                tags={postTags}
                Value={this.state.emoji_code}
              />
              <a
                className={`btn emojiBtn ${this.state.showEmojiPicker
                  ? "btn-primary"
                  : "btn-theme"}`}
                onClick={this.handleEmojiUI.bind(this)}
              >
                <i className="material-icons">insert_emoticon</i>
                {/* <span className='buttonText'>Emos</span> */}
              </a>
              <Field
                name="utmLinks"
                type="hidden"
                component={renderField}
                onChange={select => this.focustextbox(select)}
                Value={this.state.utmData} />

              <a
                className={`btn btn-theme utmlinkBtn`}
                onClick={this.handleUTMLink.bind(this)}
              >
                <i className="material-icons">insert_link</i>
              </a>

              <div className="twitter-emoji-container"> </div>
            </div>
            <div>
              <div />
              {this.state.cntnum < 0 && checked_social_twitter == true ? (
                <div className="twitterMsg">
                  Twitter allows {Globals.TWITTER_LIMIT_WORD} characters max.
                  Your message may be cut
                </div>
              ) : (
                ""
              )}
            </div>
          </div>
          {this.props.posts.campaignCreatePostdata.campaign_id !== null ? (
            <div className="form-row feed-campaign-dropdown">
              <label for="campaign">Campaign</label>
              <div className="select-wrapper">
                <Field
                  placeholder=""
                  component={renderField}
                  type="select"
                  name="campaign"
                >
                  <option value={this.props.campaignId}>
                    {this.props.posts.campaignCreatePostdata.campaign_title}
                  </option>
                </Field>
              </div>
            </div>
          ) : null}
          <div className="clearfix action-buttons">
            <div className="bottom-left-buttons">
              <a
                className="btn btn-theme"
                onClick={this.handleMediaUI.bind(this)}
              >
                <i class="material-icons">filter</i>
                <span className="buttonText">Media</span>
              </a>
              {/* //giphy */}
              <a
                className={` btn  gif-button gif-button-feed  ${this.state
                  .selectGipy_child_state !== null
                  ? "btn-primary"
                  : "btn-theme"}`}
                onClick={this.handleGiphyUI.bind(this)}
              >
                <i className="material-icons">gif</i>
              </a>
              {/* <a
                className={`btn emojiBtn ${this.state.showEmojiPicker?'btn-primary':'btn-theme'}`}
                onClick={this
                  .handleEmojiUI
                  .bind(this)}>
                <i className="material-icons">insert_emoticon</i>
                <span className='buttonText'>Emos</span>
                        </a> */}
              <a
                className={`btn schedule-btn ${this.state.dynamicSchedulers.length > 0 && this.state.isScheduleSelected
                  ? "btn-primary"
                  : "btn-theme"}`}
                onClick={this.handleSchedulerUI.bind(this)}
              >
                <i className="material-icons">schedule</i>
                <span className="buttonText">Schedule</span>
              </a>
              {/* value display  */}
              {/* {roleName !== "guest" ? (
                <a
                  className={`btn value-button-feed ${this.state.isValueSelected
                    ? "btn-primary"
                    : "btn-theme"}`}
                  onClick={this.handleValueUI.bind(this)}
                >
                  <i className="material-icons">favorite_border</i>
                  <span className="buttonText">Values</span>
                </a>
              ) : (
                ""
              )} */}
            </div>
            <div className="bottom-right-buttons clearfix" id="button-right">
              {/* <div className="btn btn-primary" onClick={this.showValuePopup.bind(this)}>Next</div> */}
              {/* {!this.state.showScheduler ? ( */}
                <button
                  className="btn btn-primary createPostBtn"
                  type="button"
                  disabled={
                    submitting ||
                    this.props.posts.creation.buttondisabled ||
                    (this.state.create_btnchange_desc &&
                      (!media_selected && !this.state.selectedGiphy))
                  }
                  onClick={this.props.culture.cultureValueList.length>0 ?this.showValuePopup.bind(this): this.formSubmit.bind(this)}
                >
                  {this.props.post ? "Update" : this.props.culture.cultureValueList.length>0 ?"Next" : "Publish"}
                </button>
              {/* ) : null} */}

              {/* {this.state.showScheduler ? (
                <button
                  type="submit"
                  disabled={
                    submitting ||
                    this.state.dynamicSchedulers.length == 0 ||
                    this.props.posts.creation.buttondisabled
                  }
                  className="btn btn-primary schdulePostBtn"
                  
                >
                  Create
                </button>
              ) : (
                ""
              )} */}
            </div>
          </div>
          <div className="popup-container">
            <div className="postScheduleSelectedImageWrapper schedule-popup">
              {this.renderSchedulerUI()}
            </div>
            <div className="create-post-selected-elements selected-multiple-images clearfix">
              {//selectGipy_child !== null
              this.state.selectGipy_child_state !== null
                ? this.renderSelectedGiphy()
                : null}
              {this.state.selectedAssetDetails !== null ||
              this.state.blDirectFileUpload == true ||
              editPostFlag == 1 ||
              this.state.DriveAssetsData !== null
                ? this.renderSelectedAsset()
                : null}
            </div>
            {this.renderEmojiPickerUI()}
            {this.renderUtmLinkPopup()}
            {this.renderGiphyUI()}
            {this.state.showValues == true ? this.renderValues() : ""}
          </div>
          </div>
          }
        </div>
      </div>

    );
  }

  renderTick() {
    let scrollerElement = document.querySelector(".-reactjs-scrollbar-area");
    if (scrollerElement !== null) {
      scrollerElement.style.marginTop = 0;
    }

    if (this.props.campaignId !== null) {
      window.setTimeout(function() {
        browserHistory.push("/feed/default/live");
      }, 600);
    }

    return (
      <div className="finishedUploadingPost center">
        <svg
          className="checkmark active"
          xmlns="http://www.w3.org/2000/svg"
          viewBox="0 0 52 52"
        >
          <circle
            className="checkmark__circle"
            cx="26"
            cy="26"
            r="25"
            fill="none"
          />
          <path
            className="checkmark__check"
            fill="none"
            d="M14.1 27.2l7.1 7.2 16.7-16.8"
          />
        </svg>
        <h1>this is the demo</h1>
        <h4 className="center m-t-two">{"Post created successfully"}</h4>
      </div>
    );
  }

  formSubmit(values) {
    this.props.handleSubmit(values);
  }

  closeEditor() {
    this.setState({
      openEditor: false
    });
  }
  editOnCreatPost() {
    this.setState({
      openEditor: true
    });
  }
  renderEditorInCreatPost() {
    selectedAssetDetails = this.state.selectedAssetDetails;
    return (
      <SliderPopup wide className="editorPopup">
        <button
          id="close-popup-editor"
          className="btn-default"
          onClick={this.closeEditor.bind(this)}
        >
          <i className="material-icons">clear</i>
        </button>
        <div id="post-editor-wrapper">
          {this.state.photoEditor ? (
            <PhotoEditor
              imageDetails={selectedAssetDetails}
              photoEditor={this.state.photoEditor}
              imageData={this.state.selectedAssetDetails.media_url}
              imageName={this.state.selectedAssetDetails.name}
              mediaType={this.state.selectedAssetDetails.type}
              closeEditorPopup={this.closeEditor.bind(this)}
              updateEditedImage={this.updateEditedImage.bind(
                this,
                this.state.selectedAssetDetails
              )}
            />
          ) : (
            ""
          )}
        </div>
      </SliderPopup>
    );
  }
  changeDropBoxValue() {
    const initData = {
      selectedAssetDetails: this.state.selectedAssetDetails
    };
    this.props.initialize(initData);
  }

  updateEditedImage(e, editedImage) {
    selectedAssetDetails = this.state.selectedAssetDetails;
    var createAsset = {
      name: e.title,
      media_type: editedImage[0].type,
      preview: editedImage[0].preview,
      size: editedImage[0].size
    };
    this.setState({ selectedAssetDetails: createAsset }, () => {
      this.changeDropBoxValue();
    });
  }
  editDataState() {
    this.setState({
      editData: true
    });
  }
  render() {
    assetUploadFlag = this.state.assetUploadState;
    let campaignId = this.props.posts.campaignCreatePostdata.campaign_id;
    var writePostTitle = "";
    let postCreated = this.props.posts.creation.created;
    let gifWrapperClassname = "";
    let createPostTitle = "Post Creation";
    let selectAssetClass = "";

    if (this.state.showMediaInterface) {
      if (this.state.currentPostAssetTab == "assetLibrary") {
        createPostTitle = "Asset selecttion";
        selectAssetClass = "selectAssetPopup";
      } else if (this.state.currentPostAssetTab == "GoogleDrive") {
        createPostTitle = "Upload from Drive";
        selectAssetClass = "GoogleDrive";
      } else {
        createPostTitle = "Asset upload";
        selectAssetClass = "uploadedAssets";
      }
    }
    if (this.state.showGiphy) {
      gifWrapperClassname = "gifWrapper";
      createPostTitle = "GIF selection";
    }
    var path = utils
      .removeTrailingSlash(this.props.location.pathname)
      .split("/");
    if (path[2] == "default") {
      writePostTitle = this.props.defaultFeed.name;
    } else if (path[2] == "my") {
      //if feed type is changed from filter then to display its name on create post popup
      writePostTitle =
        this.props.currentFeed.name !== undefined
          ? this.props.currentFeed.name
          : this.props.defaultFeed.name;
    } else {
      writePostTitle = this.props.currentFeed.name;
    }
    return (
      <div className={`disable-overlay ${selectAssetClass}`}>
        <header className="heading">
          <h3>CREATE {writePostTitle} POST</h3>
        </header>
        {/* {this.state.showValuePopup ?(
        <ValueAI 
        aiValue={this.props.posts.aiValue != null ?this.props.posts.aiValue:''}
        aiValueError={this.props.posts.aiValueError}
        backPopUp={this.backPopUp.bind(this)}
        loader={this.props.posts.aiValueLoading}
        handleSubmit = {this.formSubmit.bind(this)}
       />)
       :!this.state.showValuePopup ?
            */}
           <div className="createPostWrapper">
           <form onSubmit={this.formSubmit.bind(this)}>
             <div className="popupDescSection">
               <div className="clearfix">
                 <div className="section">
                   {campaignId !== "" && postCreated
                     ? this.backForCampaign(campaignId)
                     : !postCreated
                       ? this.renderPostCreationStep1()
                       : this.renderTick()}
                 </div>
               </div>
             </div>
           </form>
 
           {this.renderMediaUI()}
           {this.state.openEditor == true ? this.renderEditorInCreatPost() : ""}
    </div> 
    {/* ):'' */}

  


     
        <Script
          url="https://apis.google.com/js/api.js"
          onLoad={this.handleScriptLoad.bind(this)}
        />
      </div>
    );
  }
}

function mapStateToProps(state, ownProps) {
  return {
    profile: state.profile,
    tags: state.tags.tags,
    channels: state.channels.channels,
    users: state.users.users,
    posts: state.posts,
    general: state.general,
    aws: state.aws,
    accounts: state.socialAccounts.accounts,
    userTags: state.tags.userTags,
    postTags: state.tags.postTags,
    departmentTags: state.tags.postTags,
    assets: state.assets,
    culture: state.culture

  };
}

function mapDispatchToProps(dispatch) {
  return {
    tagsActions: bindActionCreators(tagsActions, dispatch),
    generalActions: bindActionCreators(generalActions, dispatch),
    postsActions: bindActionCreators(postsActions, dispatch),
    channelActions: bindActionCreators(channelActions, dispatch),
    userActions: bindActionCreators(userActions, dispatch),
    s3Actions: bindActionCreators(s3Actions, dispatch),
    assetsActions: bindActionCreators(assetsActions, dispatch),
    cultureActions: bindActionCreators(cultureActions, dispatch)
  };
}
let connection = connect(mapStateToProps, mapDispatchToProps);

var reduxConnectedComponent = connection(CreatePostForm);

export default reduxForm({
  form: "CreatePost" // a unique identifier for this form
})(reduxConnectedComponent);
