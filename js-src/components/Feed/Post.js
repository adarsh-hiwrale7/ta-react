import notie from 'notie/dist/notie.js';
import ReactDOM from 'react-dom';
import ImageLoader from 'react-imageloader';
import { connect } from 'react-redux';
import { browserHistory, Link } from 'react-router';
import reactStringReplace from 'react-string-replace';
import ReactCSSTransitionGroup from 'react/lib/ReactCSSTransitionGroup';
import { bindActionCreators } from 'redux';
import { reset } from 'redux-form';
import * as feedActions from '../../actions/feed/feedActions';
import Globals from '../../Globals';
import Modal from '../../utils/Modal';
import * as utils from '../../utils/utils';
import $ from '../Chunks/ChunkJquery';
import moment from '../Chunks/ChunkMoment';
import PerfectScrollbar from '../Chunks/ChunkPerfectScrollbar';
import reactTooltip from '../Chunks/ChunkReactTooltip';
import FetchuserDetail from '../FetchUsersDetail/FetchUsersDetail';
import PreLoader from '../PreLoader';
import PreLoaderMaterial from '../PreLoaderMaterial';
import SeeMore from '../SeeMore';
import CommentForm from './CommentForm';
import LinkPreview from './LinkPreview';
import SharePopup from './SharePopup';
var  LikepostID  =  '';
var newLikesNumber = '';
let elDataIsLiked = '';
var elLikesNumber = '';
var likeClicked = '';
var isLiked = ''
var flag = ''
var sendmail = false
var hidesuccess = false
var storePlayedVideoId = []
var likePostId = ''
var feed_string="/feed/"
var oldVideoId = null
var rateValue = false
const required = value => (value ? undefined : 'Required')
class Post extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      sharePopupState: false,
      sharePopupStateOfPostID: null,
      showNewCommentBox: false,
      showCommentsList: false,
      showCommentsofPostId: null,
      showLikes: false,
      showLikesofPostId: null,
      newComments: [], // new comments will be appended in this array
      comments: [],
      likes: [],
      media_url: '',
      editComment: false,
      moment: null,
      postIdArr: [],
      mounted: true,
      isOpen: false,
      onClose: false,
      commentbox: false,
      userTag: [],
      titlename: '',
      commentSubmitting: false,
      pintop: false,
      editableTitle: false,
      flitershow:false,
      likePostId:[],
      LikesValue: 'Like',
      trim1:{},
      LikeLoader:false,
      listOfLinks:[],
      isrunning : false,
      isikeapivalue : '',
      likedisble : '',
      storePlayedVideoId:[],
      isPlayButtonClicked:false,
      onVideoPlay : null,
      postRankingId : null,
      perfectScrollbar:null,
      tooltip: null,
      showPostPopup:false,
      commentsLoading:false,
      silderIndex:'',
      Silderflag:false,
      slideIndex: 0,
      updateCount: 0
    }
    this.onCommentsToggle = this.onCommentsToggle.bind(this)
    this._handleCloseEvent = this._handleCloseEvent.bind(this)
    this.onPostsScroll = this.onPostsScroll.bind(this);
  }

  componentWillMount () {
    var me = this
    let scrollbarSelector = '.calendar-detail-popup .post-detail-pic-wrapper';
    moment().then(moment => {
      reactTooltip().then(reactTooltip => {
      PerfectScrollbar().then(scrollbar=>{
        if (me.state.mounted) {
          me.setState({ moment: moment,perfectScrollbar: scrollbar,tooltip: reactTooltip },()=>{
            if(document.querySelectorAll(scrollbarSelector).length>0){
                const ps = new this.state.perfectScrollbar.default(scrollbarSelector, {
                  wheelSpeed: 2,
                  wheelPropagation: true,
                  minScrollbarLength: 20
                });
              }
          })
        }
      })
      })
    })

    $().then(jquery => {
      if (me.state.mounted) {
        me.setState({
          jQuery: jquery.$
        })

        let $ = jquery.$

        $('.commentsLink a[href*=\\#]').on('click', function (event) {
          event.preventDefault()
          $('html, body').stop().animate(
            {
              scrollTop: $($.attr(this, 'href')).offset().top
            },
            800
          )
        })
      }
    })
  }
  handleInitialize () {
    const initData = {
      newtitle: this.state.titlename
    }
  }
  handleCloseEvent (e) {
    this.props.handleCloseEvent(e)
  }
  updateText (id, type, e) {
    this.props.updateText(id, type, e)
  }

  componentWillUnmount () {
    document.removeEventListener('scroll', this.onPostsScroll)
    this.setState({
      mounted: false
    })
    let $ = this.state.jQuery
    if (typeof $ !== 'undefined') {
      $('.commentsLink a').off()
    }
  }
  closeSharePopup(){
    this.setState({
      sharePopupState : false,
      sharePopupStateOfPostID : null
    })
  }
onShareLinkClick (postID){
    var me = this
    me.setState({ sharePopupState: !this.state.sharePopupState, sharePopupStateOfPostID: postID})
}



  // user clicked like/unlike
   /**
   * @author : kinjal
   *  change in like in feed code
   * @param(postID)
   */
  onLikeToggle(postID, e) {
  var postid = postID
  var me = this
  var newLikesNumber = '';
  elDataIsLiked = document.querySelector(`#post-${postID} .likeLink`)
  elLikesNumber = document.querySelector(`.post-${postID} .likesNumber`)
  likeClicked = document.querySelector(`#likebox-${postID}`)
  isLiked = elDataIsLiked.getAttribute('data-isliked')
  likePostId  =  postID

  //is running is start the counter of 5 sec of diferent post
    if(me.state.isrunning == false ) {
      me.setState({
      isrunning : true ,
      isikeapivalue : isLiked                  //this value store the id into the varible so we use the comparing the value of islike

     })
    setTimeout(function () {
        me.islike(postid,flag)
      }, 5000)

    if(isLiked == 'false') {               //if isLiked = 'false' =>  if user like the post
      flag = 1;
      me.addLikeInLikeToggle();            //addLikeInLikeToggle method is set the islike value true and like the post

    } else {
      flag = 0;
      me.removeLikeInLikeToggle();        //removeLikeInLikeToggle method is set the islike value false and remove like  in to  the post
                                          //this method is used for remove the like in post
   }
   }else{

    if (isLiked == 'false') {               //if isLiked = 'false' =>  if user like the post
    flag = 1;
    me.addLikeInLikeToggle();            //addLikeInLikeToggle method is set the islike value true and like the post

  } else {
    flag = 0;
    me.removeLikeInLikeToggle();        //removeLikeInLikeToggle method is set the islike value false and remove like  in to  the post
                                        //this method is used for remove the like in post
  }
  }

  }

   //this method is add like on post
   addLikeInLikeToggle() {
    isLiked=true;
    likeClicked.setAttribute('data-like-clicked', 'true');
    elDataIsLiked.setAttribute('data-isliked', 'true')
    elDataIsLiked
      .classList
      .add('liked')
    newLikesNumber = parseInt(elLikesNumber.innerHTML) + 1;
    if (newLikesNumber > 1) {
      this.setState({LikesValue: 'Likes'});
    } else {
      this.setState({LikesValue: 'Like'});
    }
    elLikesNumber.innerHTML = newLikesNumber

  }

  //this method is remove like on post
  removeLikeInLikeToggle() {

    isLiked = false;
    elDataIsLiked.setAttribute('data-isliked', 'false')
    elDataIsLiked
      .classList
      .remove('liked')
    newLikesNumber = parseInt(elLikesNumber.innerHTML) - 1
    if (newLikesNumber > 1) {
      this.setState({LikesValue: 'Likes'});
    } else {
      this.setState({LikesValue: 'Like'});
    }

    elLikesNumber.innerHTML = newLikesNumber > 0
      ? newLikesNumber
      : 0
  }


//** if counter stop then this method is call  */
 islike(postID , flag) {

  var me = this
  me.setState({
    isrunning : false,

  })
  //if post id same and pervious islike value(isikeapivalue) value and   current isLiked is same then no any action fire ,
  //example - post alredy dislike and after 5 sec calling the API of dislike then check the value and id then action fire

  if(isLiked.toString().localeCompare(me.state.isikeapivalue) == 0) {}
  else
  {
    if (flag == 1) {
      me.likeAPICalling(postID , flag)
    } else {
      me.removeAPICalling(postID , flag)
    }
  }
}


likeAPICalling(postID , flag){
  var me= this
  fetch(Globals.API_ROOT_URL + `/post/likes`, {
    method: 'POST',
    headers: utils.setApiHeaders('post'),
      body: JSON.stringify({post_identity: postID})
    })
    .then(response => response.json())
    .catch(err => {
      notie.alert('error', err, 3); //if any server error this method is remove like in post
      if(isLiked == false){
        this.addLikeInLikeToggle();
       }
      else{
        this.removeLikeInLikeToggle();
      }
  })
    .then(json => {
      if (json !== undefined) {
        if (json.code !== 200) {
          utils.handleSessionError(json)
          notie.alert('warning', json.message, 3);
          if(isLiked == false){
            this.addLikeInLikeToggle();
          }
          else{
            this.removeLikeInLikeToggle();
          }
        }
      }
    })
}

removeAPICalling(postID , flag){
  var me= this
  fetch(Globals.API_ROOT_URL + `/post/likes`, {
    method: 'DELETE',
    headers: utils.setApiHeaders('delete'),
      body: JSON.stringify({post_identity: postID})
    })
    .then(response => response.json())
    .catch(err => {
      notie.alert('error', err, 3);
      if(isLiked == false){
        this.addLikeInLikeToggle();
      }
      else{
        this.removeLikeInLikeToggle();
      }

     //if any server error this method is add like in post
    })
    .then(json => {
      if (json !== undefined) {
        if (json.code !== 200) {
          utils.handleSessionError(json)
          me.setState({likedisble: " "})
          notie.alert('warning', json.message, 3);
          if(isLiked == true){
            this.addLikeInLikeToggle();
          }
          else{
            this.removeLikeInLikeToggle();
          }
         }
        } })
}
onCommentsToggle (postID) {
    var me = this
      me.fetchComments(postID)
    me.setState({
      newComments: [],
      commentbox:true
    })
    if (me.state.showCommentsofPostId == postID && this.state.showCommentsList == true) {
      this.setState({
        showCommentsList: !this.state.showCommentsList,
        showNewCommentBox: !this.state.showNewCommentBox,
        showCommentsofPostId: null
      })
    }
    else {
      this.setState({
        showCommentsList: true,
        showNewCommentBox: true,
        showCommentsofPostId: postID
      })
    }
  }

  toggleModal () {
    var me = this
    document.body.classList.remove('close-sccore')
    me.setState({
      showLikes: !me.state.showLikes
    })
  }

  whoLikedPostToggle (postID, likesCount) {
     var me = this
    var likeClicked = document.querySelector(`#likebox-${postID}`);
    var isLikedAttr = likeClicked.getAttribute('data-like-clicked')
    //TODO: this needs to be do in proper way its path only
   if (likesCount > 0 || isLikedAttr == "true") {


       me.fetchLikes(postID)
      
       if (me.state.showLikesofPostId == postID) {
        me.setState({ showLikes: !me.state.showLikes })
      } else {
        // comments of different post
        me.setState({ showLikes: true, showLikesofPostId: postID })
      }

      me.setState({
        showCommentsList: false,
        showNewCommentBox: false
      })
   }
  }
  createTagList (str) {
    var me = this
    var rx1 = /@\[([^\]]+)]\(([^)]+)\)/g
    let res = str.match(rx1)
    var userTag = []

    return new Promise(function (resolve, reject) {
      if (res) {
        res.forEach(function (tags) {
          /** Get user information */
          var tagsArray = tags.split('@[')
          var nameArray = tagsArray[1].split(']')
          var name = nameArray[0]
          var dept = nameArray[1]
            .substring(0, nameArray[1].length - 1)
            .split(':')[1]
            .split('_')
          var type = 'tags'
          if (
            nameArray[1].substring(0, nameArray[1].length - 1).split(':')[0] !==
            '(tags'
          ) {
              type = dept[1] == 'company' ? 'company' :
                  dept[1] == 'dept' ?
                    "department"
                    : "user";
              let tagsUserObject = {
                tagid: dept[0],
                tagName: name.replace(/ /g, ''),
                type: type
              }
              userTag.push(tagsUserObject)
          }

          /** description replace */
          if (type === 'tags') {
            str = str.replace(tags, '#' + name.replace(/ /g, ''))
          } else {
            str = str.replace(tags, '@' + name.replace(/ /g, ''))
          }
        })
        me.setState(
          {
            userTag: userTag
          })
      }
      resolve(str)
    })
  }

  handleCommentSubmit (postID, values) {
    this.setState({ commentSubmitting: true })
    this.createTagList(utils.convertSpecialCharacterToUnicode(utils.toUnicodetwo(values.comment))).then(desc => {
      var me = this
    //  this.handleChange()
    this.setState({
      listOfLinks:utils.checkLinkInPost(desc)
     })
      let payload = {
        post_identity: postID,
        comment_text: desc,
        user_tag: me.state.userTag,
        link_preview:this.state.listOfLinks
      }
      fetch(
        Globals.API_ROOT_URL +
          `/post/comment?post_identity=${postID}`,
        {
          method: 'POST',
          headers: utils.setApiHeaders('post'),
          body: JSON.stringify(payload)
        }
      )
        .then(response => {
          if (response.status !== 200) {
            notie.alert(
              'error',
              `There was a problem submitting your comment. Error code: ${response.status}`,
              5
            )
          } else {
            notie.alert('success', 'Comment Posted', 3)

            var elCommentsNumber = document.querySelector(
              `.post-${postID} .commentsNumber`
            )
            let newCommentsNumber = parseInt(elCommentsNumber.innerHTML) + 1
            elCommentsNumber.innerHTML = newCommentsNumber
            me.props.dispatch(reset('commentForm'))
            // this.fetchComments(postID);
          }
          return response.json()
        })
        .then(json => {
          var newComment = {}
          var data = json.data
          if (data) {
            if(data[0].post.data.comment_count>1)
            {
              this.setState({value: 'Comments'});
            }
            else {
                this.setState({value: 'Comment'});

            }
            var comments = this.state.comments.slice()
            comments.unshift(data[0])
            me.setState({ comments: comments, commentSubmitting: false })
          }else{
            utils.handleSessionError(json)
          }
        })
        .catch(err => {
          this.setState({ commentSubmitting: false })
          notie.alert('error', 'There was a problem submitting your comment', 3)
          throw err
        })
    })
  }

  /**
 * this method call when the page are scroll
 * @Author:Kinjal
 * @Param: nothing
 * @return: nothing
 **/
onPostsScroll(){
  
  let videoPlay = document.getElementsByClassName('videoPlay')[0];
  if(videoPlay !== undefined){
  var bounding = videoPlay.getBoundingClientRect();
  if (
    bounding.top + bounding.height  >= 0  &&
    bounding.left + bounding.height >= 0  &&
    bounding.right  <= (window.innerWidth +  bounding.height || document.documentElement.clientWidth +  bounding.height) &&
    bounding.bottom <= (window.innerHeight +  bounding.height || document.documentElement.clientHeight +  bounding.height)
  ) {
  } else {
    videoPlay.pause();
    videoPlay.classList.remove('videoPlay');
  }
 }
}
 componentDidMount() {
    document.addEventListener('scroll', this.onPostsScroll)
    this.handleChange()
    this.handleLikeChange()
    utils.pauseVideo(".postVideo video");
    document.addEventListener('click', this._handleCloseEvent);
    if(this.props.notificationcall=='true'){
      this.onCommentsToggle(this.props.location)
    }
  }

	componentWillUnmount() {

    ReactDOM.findDOMNode(this).removeEventListener('click', this._handleCloseEvent);
	}
  _handleCloseEvent(nextProps){
    if(document.getElementsByClassName('link-share-popup').value !== undefined){
      if (utils.closest(event.target, 'link-share-popup') == null) {
        if(this.refs.postcomponent){
          this.setState({
            // sharePopupState: false
          });
        }

      }
    }else if(document.getElementsByClassName('share-job').value !== undefined){
      if (utils.closest(event.target, 'share-job') == null) {
        if(this.refs.postcomponent){
          this.setState({
            // sharePopupState: false
          });
        }

      }
    }
  }
  fetchComments (postID) {
    var me = this
    me.setState({ comments: [],commentsLoading:true })
    fetch(
      Globals.API_ROOT_URL +
        `/post/comment?post_identity=${postID}`,{
          headers: utils.setApiHeaders('get')
        })
      .then(response => response.json())
      .then(json => {
        if (typeof json.data !== 'undefined' && json.data.length > 0) {
          me.setState({ comments: json.data,commentsLoading:false })
        }else{
          utils.handleSessionError(json)
          this.setState({commentsLoading:false})
        }
      })
  }

  fetchLikes (postID) {
    var me = this
    me.setState({
      likes: [],
      LikeLoader:true
    })

    fetch(
      Globals.API_ROOT_URL +
        `/post/likes/${postID}`,{
          headers: utils.setApiHeaders('get')
        })
      .then(response => response.json())
      .then(json => {
        if (typeof json.data !== 'undefined' && json.data.length > 0) {
          me.setState({ likes: json.data, LikeLoader:false })

        }else{
          utils.handleSessionError(json)
        }
      })
  }

  renderSocialChannelsSharedOn (socialAccounts, feed) {
      return (
        <span className='socialIcons'>
        <span>via </span>
          {/* {socialAccounts.length > 0 ? <span>via </span> : null} */}
          <span className='socialIconWrapper'>
          {socialAccounts.indexOf('facebook') !== -1
            ? <a className='fb'>
              <i className='fa fa-facebook' aria-hidden='true' />
            </a>
            : null}
          {socialAccounts.indexOf('twitter') !== -1
            ? <a className='tw'>
              <i className='fa fa-twitter' aria-hidden='true' />
            </a>
            : null}
          {socialAccounts.indexOf('instagram') !== -1
            ? <a className='in'>
              <i className='fa fa-instagram' aria-hidden='true' />
            </a>
            : null}
          {socialAccounts.indexOf('linkedin') !== -1
            ? <a className='li'>
              <i className='fa fa-linkedin' aria-hidden='true' />
            </a>
            : null}
         {this.props.details.post.data.type == "internal" ?
          
           <a class='internal-icon analyticinternalicon' title='Internal channel '>
            <i class='material-icons'>vpn_lock</i>
            </a>
           :''}
           </span>
        </span>
      )
  }

  renderTags () {
    let tags = this.props.details.post.data.tags.data

    return <div />
    // <div className='post-footer'>
    //   <i className="material-icons">local_offer</i>
    //   {tags.map((tag, index) => {
    //     return (
    //       <span key={index}>
    //         {tag.name}
    //       </span>
    //     )
    //   })}

    // </div>
  }

  handleCommentDelete (commentID, postID) {
    var me = this
    notie.confirm(
      `Are you sure you want to delete this comment?`,
      'Yes',
      'No',
      function () {
        me.deleteComment(commentID, postID)
      },
      function () {}
    )
  }

  deleteComment (commentID, postID) {
    var me = this
    fetch(
      Globals.API_ROOT_URL +
        `/post/comment/${commentID}`,
      {
        method: 'DELETE',
        headers: utils.setApiHeaders('get')
      }
    )
      .then(response => {
        if (response.status !== 200) {
          notie.alert(
            'error',
            `There was a problem deleting your comment. Error code: ${response.status}`,
            5
          )
        } else {
          notie.alert('success', 'Comment Removed', 3)

          var elCommentsNumber = document.querySelector(
            `.post-${postID} .commentsNumber`
          )

          let newCommentsNumber = parseInt(elCommentsNumber.innerHTML) - 1

          if(newCommentsNumber>1)
          {
            this.setState({value: 'Comments'});
          }
          else {
              this.setState({value: 'Comment'});

          }

          elCommentsNumber.innerHTML = newCommentsNumber

          // var deleteEl = document.getElementById('comment-' + commentID)

          // deleteEl.remove()
          /** delete comment */
          var index = this.state.comments.findIndex(
            item => item.identity === commentID
          )
          let comments = this.state.comments
          comments.splice(index, 1)
          this.setState({ comments: comments })
        }
        return response.json()
      })
      .then(((json)=>{
        utils.handleSessionError(json)
      }))
      .catch(err => {
        notie.alert('error', 'There was a problem deleting your comment', 3)
        throw err
      })
  }
  componentWillReceiveProps(nextProps) {
    //this method for remove the feed Rank Value
    if(this.props != nextProps){
      if( this.props.feedDetail.ratedPost.length > 0 ) {
        setTimeout(
          rateValue = false
          , 5000);
        }
      }
      
      //for mulitple post display the image
      $().then(jQuery =>{
        let $ = jQuery.$;
      
       if(this.props != nextProps){
          if(this.state.showPostPopup == true){
              $( ".input-rang-multiplePost" ).trigger( "click" );
           }
       }
    })

     

    // commneted this code as comment bo is not opening sometimes when come from ppost notification ----- by tejal
    //  if ((nextProps.details !== this.props.details && !this.props.locationInfo.pathname.includes('moderation') && !this.props.locationInfo.pathname.includes(feed_string))&&!this.props.locationInfo.pathname.includes("search")) {
    //   if(this.props.locationInfo.pathname!=='/campaigns'){
    //       this.onCommentsToggle(this.props.location)
    //     }
    //   }
  }
  renderRatings(ratingArray,postData,ratedPost){
    return(
      <div class="post-rating-main">
                        <div class="post-rating-voting-text-part-wrapper clearfix">
                          <div class="post-rating-value-lable">
                            <div className = ""> <div class = "value-container-ranking"><i class="material-icons">favorite_border</i><span>{postData.value !== undefined ? postData.value.data!== undefined && postData.value.data.length>0 ?postData.value.data[0].title:'':''}</span></div></div>
                            { postData.value !== undefined && postData.value.data!== undefined && postData.value.data.length>0 &&postData.value.data[0].description!=='' ?
                              <div class="valueDescriptionPopup">
                              <div class="valueDescriptionPopupInner">
                                    {postData.value.data[0].description }
                              </div>
                            </div>
                              :''
                            }
                          </div>
                          <div class="post-rating-desc-part">Does your day to day experience of working here feel aligned to this value?</div>
                        </div>
                      <div class="post-rating-main-sub  post-rating-voting-vote-part-wrapper voting-ranking-container clearfix">
                        <div class="post-rating-value voting-ranking-lable"><div class = "value-container-ranking"><i class="material-icons"> how_to_vote</i> <span> Votes {ratedPost.indexOf(postData.identity) == -1 ? postData.votes : postData.votes+1} </span></div></div>
                        <div class="post-rating-desc voting-rating-value">
                          <div className={this.props.feedDetail.ratedPost.indexOf(postData.identity) == -1 ? 'voting-rating-value-inner' : 'voting-rating-value-inner ratingSuccess'}>
                              <ul class="post-rating-points-ul clearfix">
                             
                                {ratingArray.map((index) => {
                                  return(
                                    <div key={`postRating-${index}`}>
                                      <li class={`post-rating-points-li ${rateValue == true && `${this.props.details.post_id}-li-${index}` == this.state.postRankingId ? 'ratingActive' : ''}`}  id = {`${this.props.details.post_id}-li-${index}`} onClick={this.ratePostClick.bind(this,index,postData.identity,postData.value.data[0].identity ,`${this.props.details.post_id}-li-${index}` )}>
                                        {index}
                                      </li> 
                                      </div>
                                  )
                                
                                })}
                              </ul>
                                <div className="ratingSuccessMessage">You have successfully rated the value.</div>
                              </div>
                          </div>
                      </div>  
                  </div>
    );
  }

  renderComments (postID) {
    if (this.props.notificationcall == 'true') {
      postID = this.props.details.post.data.identity
    }

    let comments = this.state.comments
    let newComments = this.state.newComments
    let newcommentid = this.state.newcommentid

    let profile = this.props.profile.profile
    return (
      <div className='post-comments'>
        {this.state.showNewCommentBox || this.props.notificationcall == 'true'
          ? <div className='comment-form'>
            <CommentForm
              form={`commentForm`}
              postID={postID}
              onSubmit={this.handleCommentSubmit.bind(this, postID)}
              commentSubmitting={this.state.commentSubmitting}
              feedId= {this.props.feedId}
              newLocation={this.props.locationInfo.pathname}
              />
          </div>
          : null}
        { this.state.commentSubmitting == true || this.state.commentsLoading==true ? 
          <div className="commentLoader-feed-page">{this.renderCommentsLoader()}</div>:
        <ul className='comments'>
          {newComments.map((comment, index) => {
            let authorAvatarURL = profile.avatar
            var authorAvatar = {
              backgroundImage: `url(${typeof authorAvatarURL !== 'undefined' && authorAvatarURL != '' ? authorAvatarURL : '/img/default-avatar.png'})`
            }
            /** replace comment content */
            var replacedComment = ''
            var usertag_data = comment.userTag.data
            replacedComment = reactStringReplace(
              utils.convertUnicode(comment.comment).replace(/\\/g, ''),
              /@(\w+)/g,
              (match, i) => (
                  usertag_data[usertag_data.findIndex(x => x.name == match)]!==undefined?
                  <a

                  onMouseOver={this.updateText.bind(
                    this,
                    usertag_data[usertag_data.findIndex(x => x.name == match)]
                      ? usertag_data[usertag_data.findIndex(x => x.name == match)]
                          .identity
                      : '',

                    usertag_data[usertag_data.findIndex(x => x.name == match)]
                      ? usertag_data[usertag_data.findIndex(x => x.name == match)].type
                      : ''
                  )}
                  onMouseLeave={this.mouseDownCapture.bind(this)}
                  className='link1 postTextLink'
                >
                  {`@` + match}
                </a>
                  :
                    <span className="postTextLink">
                      {`@` + match}
                    </span>
              )
            )
            replacedComment = reactStringReplace(
              replacedComment,
              /#(\w+)/g,
              (match, i) => <a className='link2 postTextLink' href={`#` + match}>{`#` + match}</a> // onClick={this.updateText.bind(this, (comment.post.data.tags.data[post.post.data.tags.data.findIndex(x => x.name==match)]?post.post.data.tags.data[post.post.data.tags.data.findIndex(x => x.name==match)].identity:''))}
            )

            return (
              <li id={'comment-' + newcommentid} key={index + 1000}>
                <div className='comment'>

                  <div className='comment-thumb author-thumb'>
                    <div
                      className='thumbnail'
                      style={authorAvatar}
                      data-tip={profile.email}
                      data-for={profile.identity}
                    />
                    {this.state.tooltip?<this.state.tooltip id={profile.identity} type='warning' />:''}
                  </div>
                  <div className='comment-text'>
                    <p>
                      <b className='comment-author'>
                        {profile.first_name} {profile.last_name}{' '}
                      </b>
                      {replacedComment}
                    </p>
                    <p className='meta'>
                      Just Now
                    </p>
                  </div>

                  <div className='comment-actions'>
                    <a
                      className='comment-action-delete'
                      onClick={this.handleCommentDelete.bind(
                        this,
                        newcommentid,
                        postID
                      )}
                    >
                      <i className='material-icons'>clear</i>
                    </a>
                  </div>
                </div>
              </li>
            )
          })}
          {this.state.showCommentsList || this.props.notificationcall == 'true'
            ? comments.map((comment, index) => {
              let authorAvatarURL = comment.commentor.data.avatar
              var authorAvatar = {
                backgroundImage: `url(${typeof authorAvatarURL !== 'undefined' && authorAvatarURL != '' ? authorAvatarURL : '/img/default-avatar.png'})`
              }
              var usertag_data =  comment.userTag.data
                /** replace comment content */
              var replacedComment = ''
              replacedComment = reactStringReplace(
                utils.convertUnicode(comment.comment).replace(/\\/g, ''),
                /@(\w+)/g,
                (match, i) =>
                (
                  usertag_data[usertag_data.findIndex(x => x.name == match)]!==undefined?
                  <a
                  key={`commentusertag-${i}`}
                    onMouseOver={this.updateText.bind(
                      this,
                      usertag_data[usertag_data.findIndex(x => x.name == match)]
                        ? usertag_data[usertag_data.findIndex(x => x.name == match)]
                            .identity
                        : '',

                      usertag_data[usertag_data.findIndex(x => x.name == match)]
                        ? usertag_data[usertag_data.findIndex(x => x.name == match)].type
                        : ''
                    )}
                    onMouseLeave={this.mouseDownCapture.bind(this)}
                    className='link3 postTextLink'
                  >
                    {`@` + match}
                  </a>
                  :
                  <span className="postTextLink ">
                    {`@` + match}
                  </span>
                  
                )
              )

              replacedComment = reactStringReplace(
                  replacedComment,
                  /#(\w+)/g,
                  (match, i) => (
                    <a key={match + i} href={`#` + match} className='link4 postTextLink'>{`#` + match}</a>
                  ) // onClick={this.updateText.bind(this, (comment.post.data.tags.data[post.post.data.tags.data.findIndex(x => x.name==match)]?post.post.data.tags.data[post.post.data.tags.data.findIndex(x => x.name==match)].identity:''))}
                )
              return (
                <li id={'comment-' + comment.identity} key={index}>
                  <div className='comment'>

                    <div className='comment-thumb author-thumb'>
                      <div
                        className='thumbnail'
                        style={authorAvatar}
                        data-tip={comment.commentor.data.email}
                        data-for={comment.commentor.data.identity}
                        />
                      {this.state.tooltip?<this.state.tooltip
                        id={comment.commentor.data.identity}
                        type='warning'
                        />:''}

                    </div>
                    <div className='comment-text'>
                      <p>
                        <b className='comment-author'>
                          {comment.commentor.data.firstname}
                          {' '}
                          {comment.commentor.data.lastname}
                          {' '}
                        </b>
                        {replacedComment}

                            {comment.LinkPreview.data.length>0?

                            <LinkPreview
                                commentData={comment}
                            />:''}
                      </p>
                      <p
                        className='meta'
                        title={
                            this.state.moment
                              ? this.state.moment
                                  .tz(comment.added_at.date, Globals.SERVER_TZ)
                                  .format('dddd, Do MMMM  YYYY, h:mm:ss a')
                              : ''
                          }
                        >
                        {this.state.moment
                            ? this.state.moment
                                .tz(comment.added_at.date, Globals.SERVER_TZ)
                                .fromNow()
                            : ''}
                      </p>
                    </div>
                    {comment.commentor.data.email == profile.email
                        ? <div className='comment-actions'>
                          <a
                            className='comment-action-delete'
                            onClick={this.handleCommentDelete.bind(
                                this,
                                comment.identity,
                                postID
                              )}
                            >
                            <i className='material-icons'>clear</i>
                          </a>
                        </div>
                        : null}
                  </div>
                </li>
              )
            })
            : null}

        </ul>}
      </div>
    )
    // }
  }
  renderLikes () {
    let likes = this.state.likes
    return (
    <div>
      {likes.length == 0 ?
       <div className = "no-user-data-likepopup">
         {this.state.LikeLoader? <PreLoader /> : <p> No user data found  </p> }   
       </div>
     :
     this.state.LikeLoader?
      <PreLoader />
      :
      <div>
        <div className='feed-popup-hd'>
          {' '}
          <p>
            {likes.length>1 ?
              'Likes: '
              :
              'Like: '
            }
            <span>{likes.length}</span>
            </p>
        </div>
        <div className='inner-body-popup'>
          {likes.map((like, index) => {
            let authorAvatarURL = like.user.profile_image
            var authorAvatar = {
              backgroundImage: `url(${typeof authorAvatarURL !== 'undefined' && authorAvatarURL != '' ? authorAvatarURL : '/img/default-avatar.png'})`
            }
            return (
              <div className='flex-fd' key={index}>
                <div className='avtar-fd'>
                  <div className='inner-avtar clearfix'>
                    <div
                      className='avtar-pf'
                      style={authorAvatar}
                      data-tip={like.user.email}
                      data-for={like.user.identity}
                    >
                      {' '}
                      {this.state.tooltip?<this.state.tooltip id={like.user.identity} type='warning' />:''}
                    </div>
                    <div className='avtar-detail'>

                      <div>
                        <p>{like.user.first_name} {like.user.last_name}</p>{' '}
                      </div>
                      <div class='avtar-dt'>
                        {' '}{like.user.userDepartment.department_name}
                      </div>
                    </div>
                  </div>

                </div>
                <div className='count-fd'>
                <div class="postUserPopupTable">
                  <table>
                    <tbody>
                      <tr>
                        <td>
                          <div>
                            <p class="count-tool-tip">{like.level}</p>
                            <p class="label-tool-tip">LEVEL</p>
                          </div>
                        </td>
                        <td>
                          <div>
                            <p class="count-tool-tip">{like.posts}</p>
                            <p class="label-tool-tip">POSTS</p>
                          </div>
                        </td>
                        <td>
                          <div>
                            <p class="count-tool-tip">{like.score}</p>
                            <p class="label-tool-tip">XP</p>
                          </div>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                </div>
              </div>
            )
          })}

        </div>
      </div>
      }</div>
    )
  }

  renderLikes1 () {
    let likes = this.state.likes
    return (
      <div className='post-meta'>
        <ul className='user-likes'>

          {likes.map((like, index) => {
            let authorAvatarURL = like.user.profile_image
            var authorAvatar = {
              backgroundImage: `url(${typeof authorAvatarURL !== 'undefined' && authorAvatarURL != '' ? authorAvatarURL : '/img/default-avatar.png'})`
            }

            return (
              <li class='likers' key={index}>
                <div className='like'>

                  <div className='like-thumb author-thumb'>
                    <div
                      className='thumbnail'
                      style={authorAvatar}
                      data-tip={like.user.email}
                      data-for={like.user.identity}
                    />
                    {this.state.tooltip?<this.state.tooltip id={like.user.identity} type='warning' />:''}
                  </div>
                  <div className='like-meta'>
                    <div className='like-author'>
                      {like.user.first_name} {like.user.last_name}
                    </div>
                    <div className='like-date'>
                      {this.state.moment !== null
                        ? this.state.moment
                            .tz(like.created_at * 1000, Globals.SERVER_TZ)
                            .fromNow()
                        : ''}
                    </div>
                  </div>

                </div>
              </li>
            )
          })}

        </ul>
      </div>
    )
  }

  //onEditPostData pass the data into the feed
  onEditPostData(){
    if(this.props.details.post.data.feed && this.props.details.post.data.feed.data[0].type!=="internal"){
      this.props.openCreatePopup(this,this.props.details.post);
    }else{
      notie.alert('warning', 'you can only edit and share on social channels.', 3)
    } 
  }
  renderPopup () {
    return (
      <div className = "share-popup-post">

      <ReactCSSTransitionGroup
        transitionName='fade'
        transitionAppear
        transitionEnterTimeout={500}
        transitionLeaveTimeout={500}
        transitionAppearTimeout={1000}
      >
        <SharePopup
          post={this.props.details.post}
          accounts={this.props.accounts}
          onShareLinkClick={this.onShareLinkClick.bind(this)}
          onEditPostData = {this.onEditPostData.bind(this)}
          closeSharePopup = {this.closeSharePopup.bind(this)}
          sharePostClick={this.props.sharePostClick}
        />
      </ReactCSSTransitionGroup>
      </div>
    )
  }
   /**
   * this function pause all the video
   * @author kinjal
   */
  videoPlay(){
    var VideoTag  =  document.querySelectorAll('video');
    var i;
    for (i = 0; i < VideoTag.length; i++) {
         var allVideoArray =  VideoTag[i].id;
        document.getElementById(allVideoArray).pause();
        VideoTag[i].classList.remove('videoPlay');
     }
  }

  /**
   * when clicked on play button of video thumbnail this method will call and it will play video and store its postid
   * @author disha
   * @param {*} media_type
   * @param {*} media_url
   * @param {*} media_extension
   * @param {*} post_id
   * @param {*} post_thumbnail_url
   * @param {*} e
   */


  playVideo(media_type, media_url, media_extension, post_id, post_thumbnail_url,index, e) {
      this.setState({
        isPlayButtonClicked: true
      })
   if(oldVideoId == media_url || oldVideoId  == null ){}else{
      //this method for pause the video when video is alredy playing and I click on other video
      this.videoPlay();
    }
    let temp =  this.state.storePlayedVideoId;
    if (document.getElementById(`playIconOverlay-${media_url}-${index}`)) {
      document.getElementById(`playIconOverlay-${media_url}-${index}`).classList.remove('overlay')
    }
    if(temp.includes(media_url)!==true){
      temp.push(media_url);
    }
    this.setState({
      storePlayedVideoId  : temp
    },()=>{
    if (this.state.storePlayedVideoId.indexOf(media_url)>-1) {
      setTimeout(function () {
        document.getElementById(`video-${media_url}-${index}`).play();
        document.getElementById(`video-${media_url}-${index}`).classList.add('videoPlay')
      }, 500)
    }}
    )
    oldVideoId  =  media_url
  }
  renderPostMedia (assetDetails, post_id) {
    var PostVideoBackground = {
      backgroundImage: `url(${typeof post_thumbnail_url !== 'undefined' && post_thumbnail_url != '' ? post_thumbnail_url : '/img/default-avatar.png'})`
    }
    var media_type,media_url,thumbnail_url,media_extension;
    return(
      assetDetails.length>0?
      <div className = {`parent-multiple-img-uploaded ${assetDetails.length==3?'parent-three-img-uploaded':''}`}>
        <div class="row"> 
          <div className = {`top-img-container clearfix  ${assetDetails.length==1?'single-image-wrapper':'' } `} >  
              {assetDetails.map((assetData,i)=>{
                if(i<4){
                if (assetData.type == 'asset' && assetData.type !== '') {
                  media_type = assetData.asset.data.media_type
                  media_url = assetData.asset.data.media_url
                  thumbnail_url = assetData.asset.data.thumbnail_url
                  media_extension = assetData.asset.data.media_extension
                } else {
                  media_type = assetData.media_type
                  media_url = assetData.media_url
                  thumbnail_url = assetData.thumbnail_url
                  media_extension = assetData.media_extension
                }
                if (typeof (media_type.split('/')[0] !== 'undefined')) {
                  media_type = media_type.split('/')[0]
                }
                  switch(media_type){
                    case 'image':
                        return(
                            <div key={`renderPostMedia-${i}`} data-slide={i}  className ={`img-item ${i==3 && assetDetails.length>4? 'multiple-img-overlay':''} ${assetDetails.length==3 && i==0?'first-img-item':''}`} onClick={this.handlePostPopup.bind(this,post_id,i)}>
                              <ImageLoader
                                src={media_url}
                                wrapper={React.DOM.div}
                                preloader={PreLoaderMaterial}
                              >
                                  Image load failed!
                              </ImageLoader>
                              {i==3 && assetDetails.length>4?
                              <div className = "addicon-for-img"> 
                                  <span>+{assetDetails.length-i}</span>
                              </div>:''}
                            </div>
                        )
                    case 'video':
                        return (
                          <div key={`renderPostMedia-${i}`} data-slide={i} className={`img-item ${i==3 && assetDetails.length>4? 'multiple-img-overlay ':''} ${assetDetails.length==3 && i==0?'first-img-item':''}`} onClick={this.handlePostPopup.bind(this,post_id,i)}>
                            <div className='postVideo' >
                              <div style= {PostVideoBackground} className = "bgVideoBlur"> </div>
                              {/* { this.state.storePlayedVideoId.indexOf(post_id)== -1  ? */}
                                <div>
                                  <ImageLoader
                                    src={thumbnail_url}
                                    wrapper={React.DOM.div}
                                    preloader={PreLoaderMaterial}
                                  >
                                    Image load failed!
                                  </ImageLoader>
                                  <div id={`playIconOverlay`} class='playIconOverlay overlay' >
                                    <i class="material-icons" >play_circle_outline</i>
                                  </div>
                                </div>
                                {/* :
                                <div>
                                  <video id={`video-${post_id}`}  onPlay={this.playVideo.bind(this, media_type, media_url, media_extension, post_id, thumbnail_url)}  ref={`video_${post_id}`} width={640} height={480} poster={thumbnail_url} controls >
                                    <source src={media_url} type='video/mp4' />
                                    <source src={media_url} type='video/webm' />
                                    <source src={media_url} type='video/ogg' />
                                  </video>
                    
                                </div>} */}
                            </div>
                          </div>
                        )
                      case 'application':
                            return (
                              <div className='file' key={`renderPostMedia-${i}`} onClick={this.handlePostPopup.bind(this,post_id,i)}> 
                                <div
                                  className='file-icon file-icon-sm'
                                  data-type={media_extension}
                                />
                              </div>
                            )
                      default:
                            return (
                              <div className='file' key={`renderPostMedia-${i}`} onClick={this.handlePostPopup.bind(this,post_id,i)}>
                                <div
                                  className='file-icon file-icon-sm'
                                  data-type={media_extension}
                                />
                              </div>
                            )
                     }
                }
              })}
          </div>
        </div>
      </div>
      :''
      )
  }
  onClickBlowUpImage (e) {
  }

  changeSchedule(feed,feedType,postData){
       this.props.changeScheduleAction(feed,feedType,postData);
  }


  Hidepost (e, feed, feedType,scheduleTime) {
    // var customFeedType
    // {
    //   this.props.feedData.feedList.map((value,i)=>{
    //     if(value.identity==this.props.feedId){
    //       customFeedType = value.type
    //     }
    //   })
    // }
    document.body.classList.remove('FeedPage');
    document.body.classList.add('FeedPage');
    let me = this

    // let folderID = this.state.selectedFolderID

    var dropdownToggle = document.querySelector(`#dropdown-toggle-`+e);
    dropdownToggle.classList.remove('active')
    var dropdownMenu = document.querySelector(`#dropdown-menu-`+e);
    dropdownMenu.style.display = 'none'
    var hidePostText = 'Choose option'
    if (e !== null) {
      if (me.props.details.post.data.type == 'external') {
        sendmail = true
        notie.select(
          hidePostText,
          'Cancel',
          [
            {
              title: 'Hide on Visibly only',
              color: '#2fbf71',
              handler: function () {
                document.body.classList.remove('FeedPage');
                if(me.props.searchInput)
                {
                  var searchInput=me.props.searchInput;
                  me.props.feedActions.setHidePost(me.props.feedId,e, sendmail, feed, feedType,searchInput,scheduleTime)
                }
                else
                {
                  me.props.feedActions.setHidePost(me.props.feedId,e, sendmail, feed, feedType,null,scheduleTime)
                }
              }
            },
            {
              title: 'Hide on Visibly and send “remove from social” request to user',
              color: '#ffbf00',
              handler: function () {
                document.body.classList.remove('FeedPage');
                if(me.props.searchInput)
                {
                  var searchInput=me.props.searchInput;
                  me.props.feedActions.setHidePost(me.props.feedId,e, sendmail, feed, feedType,searchInput,scheduleTime)}
                else
                {
                  me.props.feedActions.setHidePost(me.props.feedId,e, sendmail, feed, feedType,null,scheduleTime)
                }
              }
            }
          ],
          'top'
        )
      } else {
        notie.select(
          hidePostText,
          'Cancel',
          [
            {
              title: 'Yes',
              color: '#2fbf71',
              handler: function () {
                document.body.classList.remove('FeedPage');
                if(me.props.searchInput!==undefined)
                {
                  var searchInput=me.props.searchInput;
                  me.props.feedActions.setHidePost(me.props.feedId,e, sendmail, feed, feedType,searchInput,scheduleTime)
                }
                else
                {
                  me.props.feedActions.setHidePost(me.props.feedId,e, sendmail, feed, feedType,null,scheduleTime)
                }
                // me.props.callAllapi()
              }
            }
          ],
          'top'
        )
      }
    } else {
      notie.alert('warning', 'Please select a Post', 3)
    }
  }
  pintop (e, pin, feed, feedType,scheduledAt) {

    var dropdownToggle = document.querySelector(`#dropdown-toggle-`+e);
    dropdownToggle.classList.remove('active')

    var dropdownMenu = document.querySelector(`#dropdown-menu-`+e);
    dropdownMenu.style.display = 'none';
    dropdownMenu.classList.remove('show');

    

    var me = this
    let objToSend = {
      post_id: e,
      schedule_time:scheduledAt
    }
    if(me.props.searchInput)
    {
      me.props.feedActions.pinOnTop(null,objToSend, feed, feedType,me.props.searchInput)
    }
    else
    {
      me.props.feedActions.pinOnTop(me.props.feedId,objToSend, feed, feedType)
    }

  }
  unpinPost (id, feed, feedType,publisherid) {
    this.props.feedActions.readPinedPost(this.props.feedId,id, feed, feedType,publisherid)
  }
  /**
   * call when click on edit button to enter new title for pin
   */
  editpinTitle (e) {
    this.setState(
      {
        editableTitle: true,
        titlename: e
      },
      () => this.handleInitialize()
    )
  }
  /**
   * save new title
   * @param {*} id post id
   */
  setNewtitle (id,scheduledAt) {
    var me = this
    var inputTitle = document.getElementsByClassName('newtitle')[0].value
    if(inputTitle=='')
    {
      inputTitle = 'Admin Announcement';
    }
    if(this.state.titlename!==inputTitle){
            let objToSend = {
              post_id: id,
              pin_message: inputTitle,
              schedule_time:scheduledAt
            }
            this.setState({
              editableTitle:false
            })
            if(me.props.searchInput)
            {
              me.props.feedActions.pinOnTop(null,objToSend,me.props.feed,me.props.feedType,me.props.searchInput)
            }
            else
            {
              me.props.feedActions.pinOnTop(me.props.feedId,objToSend)
            }
      }else{
        this.setState({
          editableTitle:false
        })
      }
  }
  cancelEdit(){
    this.setState({
      editableTitle:false
    })
  }
  _handleChangeEvent (val) {
    return val
  }
  getInitialState() {
    return {value: 'Comment'};
  }

  handleChange(postId) {
    if(this.props.details.post.data.comment_count>1)
    {
      this.setState({value: 'Comments'});
    }
    else
      {
        this.setState({value: 'Comment'});
      }
  }
  handleLikeChange()
  {
    if(this.props.details.post.data.like_count>1)
    {
        this.setState({LikesValue: 'Likes'});
    }
    else
      {
        this.setState({LikesValue: 'Like'});
      }
  }
  read_more_button(replacedText)
  {
     var print_Text;
     if(replacedText[0]=="")
     {
        replacedText.map((repText,i) => {
          if(i%2!=0)
          {
            print_Text+=repText.props.children;
          }
        })
     }
     else
     {
        print_Text=replacedText[0];
     }

     return print_Text;
   }

   mouseDownCapture(e){
    if(document.getElementById('pop-up-tooltip-holder')){
      if (!document.getElementById('pop-up-tooltip-holder').contains(e.target)) {
          document
            .getElementById('pop-up-tooltip-holder')
            .classList.add('hide');
      }
    }
   }
   blankInput(){

    if(document.getElementById('pinPostTitleInput'))
    {
      document.getElementById('pinPostTitleInput').value='';
    }
   }
   editPost(post){
     this.props.openCreatePopup(this,post);
   }
   /**
   Rate post click
   @author Yamin
   @param
   Update by @kinjal for add the class for rank
   **/
  ratePostClick(point,postId,valueId,rateID){
      rateValue = true
      this.setState({
         postRankingId : rateID,
        })
    this.props.feedActions.ratePost(point,postId,valueId);
   }
   /**
    * to redirect to feed page is hashtag is clicked from analytics page
    * @param {*} match name of hashtag
    * @author disha 
    */
   redirectToHashTag(match){
     document.body.classList.remove('overlay');
      browserHistory.push(`/feed/default/live#`+match);
   }

  renderCommentsInPopup(commentData,postData){
    var commenterData = commentData.commentor.data;
    var replacedText='';
    var usertag_data = commentData.userTag.data?commentData.userTag.data:[];
    let profile = this.props.profile.profile;
    if(commentData.comment!==undefined){
      replacedText = reactStringReplace(
        utils.convertUnicode(commentData.comment).replace(/\\/g, ''),
        /@(\w+)/g,
        (match, i) => (
          <span className="postTextLinkWrapper">
          {
            usertag_data[usertag_data.findIndex(x => x.name == match)]!==undefined ?
            <a
            onMouseOver={this.updateText.bind(
              this,
              usertag_data[usertag_data.findIndex(x => x.name == match)]
                ? usertag_data[usertag_data.findIndex(x => x.name == match)]
                    .identity
                : '',

              usertag_data[usertag_data.findIndex(x => x.name == match)]
                ? usertag_data[usertag_data.findIndex(x => x.name == match)].type
                : ''
            )}
            onMouseLeave={this.mouseDownCapture.bind(this)}
            className='link5 postTextLink'
          >
            {`@` + match}
          </a>
          :
          <span className="postTextLink ">
          {`@` + match}
          </span>
          }
          </span>
        )
      )
    }
    replacedText = reactStringReplace(replacedText, /#(\w+)/g, (match, i) => ( 
      <Link onClick={this.redirectToHashTag.bind(this,match)} >{`#` + match}</Link> 
    ))

    replacedText = reactStringReplace(replacedText, /(https?:\/\/\S+)/g, (match, i) => (
      <a  target="_blank" key={match + i} href={match} className = "feed-post-link">{match}</a>
    ));

    replacedText = reactStringReplace(replacedText, /(www?\S+)/g, (match, i) => (
       ((match.indexOf("www.") > -1)) ? 
            <a  target="_blank" key={match + i} href={'https://'+match} className = "feed-post-link">{match}</a> 
        : 
          <span>{match}</span>   
    ));  

    replacedText = reactStringReplace(replacedText, /(visi.ly?\S+)/g, (match, i) => (
      ((match.indexOf("visi.ly/") > -1)) ? 
           <a  target="_blank" key={match + i} href={'https://'+match} className = "feed-post-link">{match}</a>  
    : <span>{match}</span>  
    ));
    return(
      <div className="messagebox-mainbox ">
        <div className="messsageBox clearfix">
          <div className="messageUserThumb">
            <img
              src={commenterData.avatar} 
              onError={(e) => { e.target.src = "https://app.visibly.io/img/default-avatar.png" }}
            />
          </div>
          <div className="messageContentBox">
            <div className="messageDesc">
              <strong> {commenterData.firstname +" "+ commenterData.lastname} </strong>{replacedText}</div>
              <div className="messageTime" title={this.state.moment
                              ? this.state.moment
                                  .tz(commentData.added_at.date, Globals.SERVER_TZ)
                                  .format('dddd, Do MMMM  YYYY, h:mm:ss a')
                              : ''}>
                      {this.state.moment
                        ? this.state.moment
                            .tz(commentData.added_at.date, Globals.SERVER_TZ)
                            .fromNow()
                        : ''}
              </div>
          </div>
          {commenterData.email == profile.email
                        ? 
                        <div className="deleteCommentButton"><a onClick={this.handleCommentDelete.bind(
                            this,
                            commentData.identity,
                            postData.identity)}>
                          <i class="material-icons">clear</i></a>
                        </div>
                        : null}
        </div>
      </div> 
    )
  }
   showPostPopup(postData,dateFromNow,postDate,socialAccounts,replacedText) {
     let assetDetails = postData.assetDetail.data;
     let post_id = postData.identity;
    const settings = {
      dots: false,
      speed: 500,
      slidesToShow:1,
      slidesToScroll: 1,
      arrows:true,
      infinite: false,
      adaptiveHeight:false,
      afterChange: current => {
         utils.pauseVideo(".postVideo video");
        },
      afterChange: () =>{
        utils.pauseVideo(".postVideo video");
        this.setState(state => ({ updateCount: state.updateCount + 1 }))},
      beforeChange: (current, next) =>{ 
        this.setState({ slideIndex: next })},
        responsive: [
          {
            breakpoint: 768,
            settings: {
              slidesToShow:1,
              slidesToScroll: 1,
              infinite: false,
              adaptiveHeight:false,
            }
          },
          {
            breakpoint: 767,
            settings: {
              slidesToShow:1,
              slidesToScroll: 1,
              adaptiveHeight:false,
            }
          }
        ]
    }
    
    var authorAvatarURL = postData.postOwner.data.avatar;
    var authorAvatar = {
      backgroundImage: `url(${typeof authorAvatarURL !== 'undefined' && authorAvatarURL != '' ? authorAvatarURL : '/img/default-avatar.png'})`
    }
    var showLikesComment = typeof this.props.showLikesComment !== 'undefined'
      ? this.props.showLikesComment
      : true
    let feedType = typeof this.props.feedType === 'undefined'
      ? 'live'
      : this.props.feedType
    let feed = typeof this.props.feed === 'undefined'
      ? 'default'
      : this.props.feed
    var location = window.location.href;
    var path = utils
      .removeTrailingSlash(location)
      .split('/')

    var media_extension,media_type,media_url,thumbnail_url;
    var profileImageStyle = {backgroundImage: "url(" + `${this.props.profile.profile.avatar}` + ")"}
    return (
      <div className="Feed-multiple-image-popup-wrapper clearfix">
        <div className="popup-center-wrapper">
          <div className="feed-multiple-image-popup">
            <div className="feed-multiple-image-content-wrapper clearfix">
            <button id="close-popup" class="btn-default" onClick={this.closePostCreatePopup.bind(this)}><i class="material-icons">clear</i></button>
            
            {assetDetails.length>1?
            this.props.Slider?
            <div className="feed-multiple-image">
            {/* this input is use for the multiple post to show the clickable image */}
            <input
              onClick={e => this.slider.slickGoTo(this.state.silderIndex)}
              value={this.state.silderIndex}
              type="text"
              min={0}
              max={3}
              className = "input-rang-multiplePost hide"
           />
              <this.props.Slider.default  
                   ref={slider => (this.slider = slider)}{...settings}>
                  {assetDetails.length>0?
                  assetDetails.map((assetData , i)=>{
                    if (assetData.type == 'asset' && assetData.type !== '') {
                      media_type = assetData.asset.data.media_type
                      media_url = assetData.asset.data.media_url
                      thumbnail_url = assetData.asset.data.thumbnail_url
                      media_extension = assetData.asset.data.media_extension
                    } else {
                      media_type = assetData.media_type
                      media_url = assetData.media_url
                      thumbnail_url = assetData.thumbnail_url
                      media_extension = assetData.media_extension
                    }
                    if (typeof (media_type.split('/')[0] !== 'undefined')) {
                      media_type = media_type.split('/')[0]
                    }
                    var PostVideoBackground = {
                      backgroundImage: `url(${typeof thumbnail_url !== 'undefined' && thumbnail_url != '' ? thumbnail_url : '/img/default-avatar.png'})`
                    }
                    return(
                    <div className='slick-slide' key={i}>
                      {media_type=='image'?
                      <img src={media_url}/>
                        :media_type=='video'?
                        <div>
                        <div className='postVideo' >
                        <div style= {PostVideoBackground} className = "bgVideoBlur"> </div>
                        { this.state.storePlayedVideoId.indexOf(media_url)== -1  ?
                          <div>
                            <ImageLoader
                              src={thumbnail_url}
                              wrapper={React.DOM.div}
                              preloader={PreLoaderMaterial}
                            >
                              Image load failed!
                          </ImageLoader>
                            <div id={`playIconOverlay-${media_url}-${i}`} class='playIconOverlay overlay' >
                              <i class="material-icons" onClick={this.playVideo.bind(this, media_type, media_url, media_extension, post_id, thumbnail_url,i)}>play_circle_outline</i>
                            </div>
                          </div>
                          :
                          <div>
                             <video id={`video-${media_url}-${i}`} ref={`video-${media_url}-${i}`} width={640} height={480} poster={thumbnail_url} controls >
                              <source src={media_url} type='video/mp4' />
                              <source src={media_url} type='video/webm' />
                              <source src={media_url} type='video/ogg' />
                            </video>
              
                          </div>}
                      </div>
                      </div>
                        :''

                      }
                      
                  </div>)
                  })
              :''}
              </this.props.Slider.default>
              </div>
              :'':
              assetDetails.length>0?
                assetDetails.map((assetData , i)=>{
                  if (assetData.type == 'asset' && assetData.type !== '') {
                    media_type = assetData.asset.data.media_type
                    media_url = assetData.asset.data.media_url
                    thumbnail_url = assetData.asset.data.thumbnail_url
                    media_extension = assetData.asset.data.media_extension
                  } else {
                    media_type = assetData.media_type
                    media_url = assetData.media_url
                    thumbnail_url = assetData.thumbnail_url
                    media_extension = assetData.media_extension
                  }
                  if (typeof (media_type.split('/')[0] !== 'undefined')) {
                    media_type = media_type.split('/')[0]
                  }
                  var PostVideoBackground = {
                    backgroundImage: `url(${typeof thumbnail_url !== 'undefined' && thumbnail_url != '' ? thumbnail_url : '/img/default-avatar.png'})`
                  }
                  return(
                  <div className="feed-multiple-image">
                  {media_type=='image'?
                  <img src={media_url}/>
                  :
                  media_type=='video'?
                  <div>
                  <div className='postVideo' >
                  <div style= {PostVideoBackground} className = "bgVideoBlur"> </div>
                  { this.state.storePlayedVideoId.indexOf(media_url)== -1  ?
                    <div>
                      <ImageLoader
                        src={thumbnail_url}
                        wrapper={React.DOM.div}
                        preloader={PreLoaderMaterial}
                      >
                        Image load failed!
                    </ImageLoader>
                      <div id={`playIconOverlay-${media_url}-${i}`} class='playIconOverlay overlay' >
                        <i class="material-icons" onClick={this.playVideo.bind(this, media_type, media_url, media_extension, post_id, thumbnail_url,i)}>play_circle_outline</i>
                      </div>
                    </div>
                    :
                    <div>
                       <video id={`video-${media_url}-${i}`}  ref={`video-${media_url}-${i}`} width={"100%"} height={"auto"} poster={thumbnail_url} controls >
                        <source src={media_url} type='video/mp4' />
                        <source src={media_url} type='video/webm' />
                        <source src={media_url} type='video/ogg' />
                      </video>
        
                    </div>}
                </div>
                </div>:''}
                </div>
                  )
                })
              :''}
              <div className="feed-multiple-image-content">
                <div className="inner-multiple-image-content post">
                  <div className="post-header">
                    <div className="author-thumb">
                      <div
                        className='thumbnail'
                        style={authorAvatar}
                        data-for={this.props.tooltipid}
                      />
                    </div>
                    <div className="author-data">
                      <div className="author-meta">
                        <b>
                          {postData.postOwner.data.firstname}
                          {' '}
                          {postData.postOwner.data.lastname}
                        </b>
                    </div>
                    <div className='post-time'>
                      <span
                        className='timeFromNow'
                        ref={postDate}
                        // title={this.state.moment !== null
                        //   ? post.scheduled_at !== undefined
                        //       ?
                        //       this.state.moment.unix(postDate).format('dddd, Do MMMM YYYY ')+'at'+this.state.moment.unix(postDate).format(' h:mm a')
                        //         : this.state
                        //             .moment(post.post.data.created.date)
                        //             .format('dddd, Do MMMM YYYY, h:mm:ss a')
                        //     : ''
                        // }
                      >{dateFromNow + " "}  
                      </span>
                        {this.renderSocialChannelsSharedOn(socialAccounts,postData.type)}
                    </div>
                    </div>
                  </div>
                  <div className="post-detail-pic-wrapper">
                    <div id="postdetail" name="postdetail" className="post-detail ">
                      {this.props.details.post.data.detail!==null?
                        <SeeMore
                            className='feed'
                            detail = {this.props.details.post.data.detail}
                            postid={postData.identity}>

                          <div className="post-detail-feed-text">
                              {document.innerHTML = replacedText}
                          </div>
                        </SeeMore>:''}
                    </div>
                  </div>
                  {feedType!=="scheduled" &&  feedType !=="unapproved" && feedType !=="pending" && this.props.fromCalendarSchedule!==true?
                  <div className="post-meta">
                    <div
                      className={`like  ${this.state.likedisble}  likeLink ${isLiked ? 'liked' : ''}`}
                      data-postID={postData.identity}
                      data-isliked={postData.is_liked}
                      >
                      <a class="post-meta-link"
                        onClick={this.onLikeToggle.bind(
                            this,
                            postData.identity
                          )}
                        >
                        <i className='material-icons'>thumb_up</i>
                          <span className='likeText'>Like</span>
                        </a>
                    </div>
                    {/* <div className="share">
                      <div className="popup-container postSharePopup">
                      </div>
                    </div> */}
                    <div
                        className='commentsLink comments'
                        data-postID={postData.identity}
                        >
                        {((feedType == 'live' || path[4]=="my") && showLikesComment)
                            ? <div className='count'>
                              <span
                                className='likesCount'
                                data-postID={postData.identity}
                                >
                                <span className='likesNumber'>
                                {(postData.like_count !== null && postData.like_count!==undefined && postData.like_count !== 'NaN')? postData.like_count :'0'}
                                </span>
                                <a className="post-meta-link"
                                  id={"likebox-"+ postData.identity}
                                  data-like-clicked="false"
                                  onClick={this.whoLikedPostToggle.bind(
                                      this,
                                      postData.identity,
                                      postData.like_count
                                    )}
                                  >
                                  {this.state.LikesValue}
                                  </a>

                              </span>
                              <span
                                className='commentsCount commentsLink'
                                data-postID={postData.identity}
                                >
                                <span className='commentsNumber'>
                                {(postData.comment_count !== null && postData.comment_count!==undefined && postData.comment_count !== 'NaN')? postData.comment_count :'0'}
                                </span>
                                <a
                                className="post-meta-link"
                                  // onClick={ this.onCommentsToggle.bind(
                                  //     this,
                                  //     postData.identity
                                  //   )}
                                  >
                                    { this.state.value}
                                  </a>
                              </span>
                            </div>
                            : null}
                      </div>
                  </div>:''}
                </div>
                {feedType!=="scheduled"  &&  feedType !=="unapproved" && feedType !=="pending"  && this.props.fromCalendarSchedule!==true?
                <div>
                <div className="multiple-image-comment-sec">
                  {this.state.commentsLoading==true?
                  this.renderCommentsLoader()
                  :
                  <div className="messagesWrapper">
                    {this.state.comments.length>0?
                      this.state.comments.map((commentData,i)=>{
                        return(
                          this.renderCommentsInPopup(commentData,postData)
                        )
                      })
                      :<div>No comments found.</div>}
                  </div>
                  }
                </div>

                <div className="post-comment-section">
                  <div className="post-comment-sec-wrapper">
                  <div class="write_post-comment">
                      <div class="comment-post-user" style={profileImageStyle}></div>
                  </div>
                    <CommentForm
                      fromPopup ={true}
                      form={`commentForm`}
                      postID={postData.identity}
                      onSubmit={this.handleCommentSubmit.bind(this, postData.identity)}
                      commentSubmitting={this.state.commentSubmitting}
                      feedId= {this.props.feedId}
                      // newLocation={this.props.locationInfo.pathname}
                      />
                  </div>
                </div>
                </div>:''}
                
              </div> 
            </div>
          </div>
        </div>
      </div>
    )
  }
  renderCommentsLoader(){
    return(
      <div>
          <div className = "Job-loader-container">
              <div className="curated-loader-inner-container">
                  <div className = "curated-header clearfix">
                      <div className = "img-avatar"> </div>
                      <div className = "name-header">
                          <div className = "name-curated loader-line-height"> </div>
                          <div className = "date-curated loader-line-height"> </div>
                      </div>
                      </div>
                    <div className = "clearfix curated-block-container">
            
                          <div className = "inner-curated-block">
                              <div className = "curated-decripation">
                                <div className = "clearfix date-and-share-block">
                                      <div className ="cureted-date loader-grey-line loader-line-space loader-line-radius"> </div>
                                      <div className ="cureted-share loader-grey-line loader-line-space loader-line-radius"> </div>
                                </div>
                                  <div className = "cureted-decp-block4 loader-grey-line loader-line-space loader-line-radius loader-line-height"> </div>
                                  <div className = "cureted-decp-block3 loader-grey-line loader-line-space loader-line-radius loader-line-height"> </div>
                                  <div className="cureted-decp-line-padding"></div>
                              </div>
                    </div> 
            </div>
            </div>
          </div>
          {/* <div className = "Job-loader-container">
              <div className="curated-loader-inner-container">
                  <div className = "curated-header clearfix">
                      <div className = "img-avatar"> </div>
                      <div className = "name-header">
                          <div className = "name-curated loader-line-height"> </div>
                          <div className = "date-curated loader-line-height"> </div>
                      </div>
                      </div>
                    <div className = "clearfix curated-block-container">
            
                          <div className = "inner-curated-block">
                              <div className = "curated-decripation">
                                <div className = "clearfix date-and-share-block">
                                      <div className ="cureted-date loader-grey-line loader-line-space loader-line-radius"> </div>
                                      <div className ="cureted-share loader-grey-line loader-line-space loader-line-radius"> </div>
                                </div>
                                  <div className = "cureted-decp-block4 loader-grey-line loader-line-space loader-line-radius loader-line-height"> </div>
                                  <div className = "cureted-decp-block3 loader-grey-line loader-line-space loader-line-radius loader-line-height"> </div>
                                  <div className = "cureted-decp-block1 loader-grey-line loader-line-space loader-line-radius loader-line-height"> </div>
                                  <div className="cureted-decp-line-padding"></div>
                              </div>
                    </div> 
            </div>
            </div>
          </div> */}
      </div>
    )
  }
  handlePostPopup(postID, key){
    this.setState({
      showPostPopup:true,
      silderIndex:key
    })
    this.slider.slickGoTo(key)
    this.fetchComments(postID);
    document.body.classList.add("center-wrapper-body-container");
  }
  closePostCreatePopup(){
    this.setState({
      showPostPopup:false,
       silderIndex:'',
       Silderflag:false
    })
    document.body.classList.remove("center-wrapper-body-container");
  }
  
  render () {
    if(document.getElementById('pinPostTitleInput'))
    {
      document.getElementById('pinPostTitleInput').setAttribute('maxlength',50)
    }
  var location = window.location.href;
  var path = utils
    .removeTrailingSlash(location)
    .split('/')
    let roleName=''
    if(JSON.parse(localStorage.getItem('roles')) !== null){
      roleName = JSON.parse(localStorage.getItem('roles'))[0]['role_name']
    }
    var post = this.props.details
    var loggedInUserId = this.props.profile.profile.identity;
    var postOwnerId = this.props.details.post.data.postOwner.data.identity;


    var showLikesComment = typeof this.props.showLikesComment !== 'undefined'
      ? this.props.showLikesComment
      : true

    let feedType = typeof this.props.feedType === 'undefined'
      ? 'live'
      : this.props.feedType
    let feed = typeof this.props.feed === 'undefined'
      ? 'default'
      : this.props.feed

    var socialAccounts = []
    var authorAvatarURL = post.post!==undefined? post.post.data.postOwner.data.avatar:''
    var authorAvatar = {
      backgroundImage: `url(${typeof authorAvatarURL !== 'undefined' && authorAvatarURL != '' ? authorAvatarURL : '/img/default-avatar.png'})`
    }
    var dateFromNow=';'
    var media_type = ''
    var media_url = ''
    var thumbnail_url = ''
    var media_extension = 'file'
    let postTitle = '',
      postDate = post.scheduled_at == undefined
        ?  post.post!==undefined?post.post.data.created.date:''
        : post.scheduled_at


    if (this.state.moment !== null) {
      var fetchdate = this.state.moment.unix(postDate).format('MM-DD-YYYY');
      var today_date = this.state.moment().format('MM-DD-YYYY')
      var date1 = new Date(today_date.toString());
      var date2 = new Date(fetchdate.toString());
      var timeDiff = date2.getTime() - date1.getTime();
      var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
    } 
    if (diffDays <= 10) {
      dateFromNow = this.state.moment !== null
        ? post.scheduled_at !== undefined
          ? this.state.moment.unix(postDate).fromNow()
          : this.state
            .moment(post.post.data.created.date).fromNow()
        : ''
    } else {
      dateFromNow = this.state.moment !== null
        ? post.scheduled_at !== undefined
          ? this.state.moment.unix(postDate).format('dddd, Do MMMM  YYYY ') + 'at' + this.state.moment.unix(postDate).format(' h:mm a')
          : this.state
            .moment(post.post.data.created.date).format('dddd, Do MMMM  YYYY ') + 'at' + this.state
              .moment(post.post.data.created.date).format(' h:mm a')
        : ''
    }

    var commentCount = 0
     //post.post.data.comments.data.length;
     if( post.post!==undefined){
      if(Object.keys(post.post.data.assetDetail.data).length > 0 &&  post.post.data.assetDetail.data.length !== 0 ){
      post.post.data.assetDetail.data.map(postMedia => {
        if (postMedia.type == 'asset' && postMedia.type !== '') {
          media_type = postMedia.asset.data.media_type
          media_url = postMedia.asset.data.media_url
          thumbnail_url = postMedia.asset.data.thumbnail_url
          media_extension = postMedia.asset.data.media_extension
        } else {
          media_type = postMedia.media_type
          media_url = postMedia.media_url
          thumbnail_url = postMedia.thumbnail_url
          media_extension = postMedia.media_extension
        }
      })
      }
  }
  post.SocialAccount?post.SocialAccount.data.map(acc => {
      if (typeof acc.SocialAccount !== 'undefined') {
        socialAccounts.push(acc.SocialAccount.data.social_media.toLowerCase())
      }
    }):''
    if (typeof (media_type.split('/')[0] !== 'undefined')) {
      media_type = media_type.split('/')[0]
    }
    var data = post.post!==undefined?post.post.data.detail:''
    var isLiked =''
    var usertag_data=''
    var replacedText=''
    var cultureValueName=''
    if(post.post!==undefined){
      isLiked = post.post.data.is_liked,
      usertag_data = post.post.data.userTag.data,
      replacedText = ''
      cultureValueName=post.post.data.value.data.length > 0 ? post.post.data.value.data[0].title :''
    }
      if(post.post!==undefined && post.post.data.detail!==null){
        replacedText = reactStringReplace(
      utils.convertUnicode(post.post.data.detail).replace(/\\\-/g, ''),
      /@(\w+)/g,
      (match, i) => (
        <span className="postTextLinkWrapper">
        {
          usertag_data[usertag_data.findIndex(x => x.name.includes(match) )]!==undefined ?
          <a
          onMouseOver={this.updateText.bind(
            this,
            usertag_data[usertag_data.findIndex(x => x.name.includes(match) )]
              ? usertag_data[usertag_data.findIndex(x =>x.name.includes(match))]
                  .identity
              : '',

            usertag_data[usertag_data.findIndex(x => x.name.includes(match))]
              ? usertag_data[usertag_data.findIndex(x => x.name.includes(match))].type
              : ''
          )}
          onMouseLeave={this.mouseDownCapture.bind(this)}
          className='link5 postTextLink'
        >
          {`@` + match}
        </a>
        :
        <span className="postTextLink ">
        {`@` + match}
        </span>
        }
        </span>
      )
    )
  }
    replacedText = reactStringReplace(replacedText, /#(\w+)/g, (match, i) => {
      return(  (path[3] =='analytics' && path[4] == undefined) || (path[4] !== undefined && path [4]=='feed') || path[3] == 'campaigns' ?
        <Link onClick={this.redirectToHashTag.bind(this,match)} >{`#` + match}</Link> 
        :<a href={`#`+ match}>{`#` + match}</a>
      )
    })
   

    // Match URLs
    replacedText = reactStringReplace(replacedText, /(https?:\/\/\S+)/g, (match, i) => (
        <a  target="_blank" key={match + i} href={match} className = "feed-post-link">{match}</a>
    ));
      replacedText = reactStringReplace(replacedText, /(www?\S+)/g, (match, i) => (
         ((match.indexOf("www.") > -1)) ? 
              <a  target="_blank" key={match + i} href={'https://'+match} className = "feed-post-link">{match}</a> 
          : 
            <span>{match}</span>   
      )); 
      replacedText = reactStringReplace(replacedText, /(visi.ly?\S+)/g, (match, i) => (
        ((match.indexOf("visi.ly/") > -1)) ? 
             <a  target="_blank" key={match + i} href={'https://'+match} className = "feed-post-link">{match}</a>  
      : <span>{match}</span>  
      ));
    
  
    this.read_more_button(replacedText);
    let campaignDetailPopupScrollbar = {
      maxHeight: `calc(100vh - 407px)`
    }
   var t=this.state
   .moment? this.state
          .moment(this.props.cal_Date)
          .format("HH:mm"):'';
          var d=this.state
          .moment?this.state
          .moment(this.props.cal_Date)
          .format("DD/MM/YYYY"):'';
          let concatenatedDateTime = d + " " + t;
      let timeStamp = this.state
      .moment?this
        .state
        .moment(concatenatedDateTime, 'DD/MM/YYYY HH:mm')
        .format("X"):'';
        this.props.cal_view==true && this.props.cal_view!==undefined ? postDate=timeStamp: ""
    var ratingArray = [0,1,2,3,4,5,6,7,8,9,10];  
    var postData = post.post.data;
    var enableSocialFlag=this.props.profile.profile.enable_social_posting;
    return (
      (
        <div>
           <Modal
              show={
                this.state.showLikes &&
                  this.state.showLikesofPostId == post.post.data.identity
              }
              onClose={this.toggleModal.bind(this)}
            > 
           {this.renderLikes(post.post.data.identity)}
            </Modal>

        
        {this.state.showPostPopup==true? this.showPostPopup(postData,dateFromNow,postDate,socialAccounts,replacedText):''}

        <div className='feed-post-item clearfix' key={this.props.index} ref="postcomponent">
          {/* if user edit the text in pin the top,this design is used */}
          {this.state.editableTitle
            ? (<div className='pinned-text-wrapper clearfix'>
              <div className='pin-icon-wrapper edit-textbox-pin-icon'>
                <i className='material-icons'>bookmark</i>
              </div>
              <div className='pin-content-wrapper'>
                <div className='admin-pin-statement'>
                  <input
                    id="pinPostTitleInput"
                    type='text'
                    className='newtitle'
                    onChange={() => {
                      this._handleChangeEvent(this.state.titlename)
                    }}
                    defaultValue={this.state.titlename}
                    />
                </div>
                <div className='pinActionBtns'>
                {roleName=='admin' || roleName=='super-admin'?
                <a
                  href='javascript:void(0);'
                  onClick={this.setNewtitle.bind(
                      this,
                      post.post.data.identity,post.scheduled_at
                    )}
                  className='edit-pin-statement'
                  title='Edit'
                  >
                  <i class='material-icons'></i>
                </a>
                :''}
                <a
                  href='javascript:void(0);'
                  className='remove-pin-statement'
                  title='Remove statement'
                  onClick={this.cancelEdit.bind(this)}
                  >
                  <i className='material-icons'>clear</i>
                </a>
                </div>
              </div>
            </div>)
            : (post.post.data.is_pin == 1 && post.post.data.is_pin_read == 0 && this.props.feed!=="my")
                ? <div className='pinned-text-wrapper clearfix'>
                  <div className='pin-icon-wrapper'>
                    <i className='material-icons'>bookmark</i>
                  </div>
                  <div className='pin-content-wrapper'>
                  <div onClick={roleName=='admin' || roleName=='super-admin' ?this.editpinTitle.bind(
                            this,
                            this.props.feedData.pinMessage.message !== null && this.props.feedData.pinMessage.postId == post.post.data.identity ? this.props.feedData.pinMessage.message : post.post.data.pin_description
                          ):''}>
                    <div
                      className='admin-pin-statement pintitle'
                      id='pintitle'
                      >
                      {this.props.feedData !==undefined?this.props.feedData.pinMessage.message !== null && this.props.feedData.pinMessage.postId == post.post.data.identity
                        ? this.props.feedData.pinMessage.message
                        : post.post.data.pin_description:''}
                    </div>

                    </div>

                    <div className='pinActionBtns'>
                    {roleName=='admin' || roleName=='super-admin'?
                    <a
                      href='javascript:void(0);'
                      className='edit-pin-statement'
                      title='Edit'
                      onClick={this.editpinTitle.bind(
                        this,
                        this.props.feedData.pinMessage.message !== null && this.props.feedData.pinMessage.postId == post.post.data.identity ? this.props.feedData.pinMessage.message : post.post.data.pin_description
                      )}
                      >
                      <i
                        className='material-icons'
                        >
                          mode_edit
                        </i>
                    </a>
                    :''}
                    <a
                      href='javascript:void(0);'
                      className='remove-pin-statement'
                      title='Remove statement'
                      onClick={this.unpinPost.bind(
                          this,
                          post.post.data.identity,
                          feed,
                          feedType,
                          post.publisher_identity
                        )}
                      >
                      <i className='material-icons'>clear</i>
                    </a>
                    </div>
                  </div>
                </div>
                : ''}

          <div
            id={`post-${post.post.data.identity}`}
            className={`post-${post.post.data.identity} post`}
          >
            <div
              className={`post-header clearfix ${!showLikesComment ? 'schedule-post-header' : ''}`}
            >

              <div className='author-thumb'>
              {this.props.cal_view?
                <div
                  className='thumbnail'
                  style={authorAvatar}
                  data-for={this.props.tooltipid}
                />
                :
                <div
                className='thumbnail'
                style={authorAvatar}
                data-for={this.props.tooltipid}
              />
              }
               
            <FetchuserDetail userID={post.post.data.postOwner.data.identity}  />
            
       

              </div>
              <div className='author-data'>
                <div className='author-meta'>
                  <b>

                    {post.post.data.postOwner.data.firstname}
                    {' '}
                    {post.post.data.postOwner.data.lastname}
                  </b>
                  {' '}
                  {'shared' }
                 
                  {showLikesComment
                    ? this.renderSocialChannelsSharedOn(socialAccounts, feed)
                    : this.renderSocialChannelsSharedOn(
                        socialAccounts,
                        post.post.data.type
                      )}
                </div>
                <div className='post-time'>
                  <span
                    className='timeFromNow'
                    ref={postDate}
                    title={this.state.moment !== null
                      ? post.scheduled_at !== undefined
                          ?
                          this.state.moment.unix(postDate).format('dddd, Do MMMM YYYY ')+'at'+this.state.moment.unix(postDate).format(' h:mm a')
                            : this.state
                                .moment(post.post.data.created.date)
                                .format('dddd, Do MMMM YYYY, h:mm:ss a')
                        : ''
                    }
                  >{dateFromNow}
                  </span>
                </div>
                {(roleName !== 'employee' && roleName !=='guest') && typeof this.props.showActionDots === "undefined" && feedType !== "scheduled" &&  path[3] != "campaigns" && path[4] != "my" ?

                <ul class='dd-menu context-menu'>
                  <li class='button-dropdown'>
                    <a class='dropdown-toggle' id={`${'dropdown-toggle-' + post.post.data.identity}`}>
                      <i class='material-icons'>more_vert</i>
                    </a>

                   <ul class='dropdown-menu' id={`${'dropdown-menu-' + post.post.data.identity}`}>
                    {roleName == 'admin' || roleName == 'super-admin' || roleName == 'moderator'?
                      <li
                        onClick={this.Hidepost.bind(
                          this,
                          post.post.data.identity,
                          feed,
                          feedType,
                          post.scheduled_at
                        )}
                      >
                        {/* <a class='btn-edit-folder'>Hide post {feedType}</a> */}
                        <a class='btn-edit-folder'>Hide post</a>
                      </li>
                      :''}
                      {roleName == 'admin' || roleName == 'super-admin'
                        ? <li
                          onClick={this.pintop.bind(
                              this,
                              post.post.data.identity,
                              post.post.data.is_pin,
                              feed,
                              feedType,post.scheduled_at
                            )}
                          >
                          <a class='btn-edit-folder'>
                            {post.post.data.is_pin == 1
                                ? 'Unpin Post'
                                : 'Pin on top'}
                          </a>
                        </li>
                        : ''}
                    </ul>
                  </li>
                </ul> : '' }
                {/* when feed is schedule */}
                {feedType === "scheduled" &&  path[3] != "campaigns" || (this.props.campaignPost==true && this.props.isOwner == true)?
                   <ul class='dd-menu context-menu'>
                    <li class='button-dropdown'>
                      <a class='dropdown-toggle' id={`${'dropdown-toggle-' + post.post.data.identity}`}>
                        <i class='material-icons'>more_vert</i>
                      </a>
                          <ul class='dropdown-menu' id={`${'dropdown-menu-' + post.post.data.identity}`}>
                               <li
                                onClick={this.changeSchedule.bind(
                                  this,
                                  feed,
                                  feedType,
                                  post
                                )}
                              >
                                {/* <a class='btn-edit-folder'>Hide post {feedType}</a> */}
                                <a class='btn-edit-folder'>Change schedule</a>
                              </li>
                          </ul>
                    </li>
                  </ul>
                : ''}
                {path[3]=='moderation'?
                <div className="campaign-custom-box">
                      {post.post.data.associatedCampaign !== undefined &&  post.post.data.associatedCampaign.data !== undefined ? 
                          <div className="Campaigntitle">{post.post.data.associatedCampaign.data.title}</div>:''}
                      {post.post.data.feed !== undefined && post.post.data.feed.data !== undefined && post.post.data.feed.data.length >0 ? <div className="Customtitle">{post.post.data.feed.data[0].name}</div>:''}
                  </div>
                :''}
              </div>

             
            </div>
            <div className='post-detail-pic-wrapper'>
            <div
              id='postdetail'
              name='postdetail'
              ref="post_title"
              className={`post-detail ${!showLikesComment ? 'schedule-post-title' : ''}`}
            >
              <div id={`${'detail-' + post.post.data.identity}`} />
              {this.props.details.post.data.detail!==null?
            <SeeMore
                className='feed'
                detail = {this.props.details.post.data.detail}
                postid={post.post.data.identity}>
              <div className="post-detail-feed-text">
                  {document.innerHTML = replacedText}
              </div>
              </SeeMore>:''}
              {media_type !== 'image' &&
                media_type !== 'video' &&
                media_type !== ''
              ?
              <div className = "linkpreview-parent-class">
               <div>
            
                {media_type == 'text' ?
                <a href = {media_url} target = "_blank">
                  <div className = "csv-preview">
                         <div class ="parent-csv">
                            <i class="fa fa-file-o fa-3x"></i>
                         <span> CSV </span>
                          </div>
                   </div>
                  </a>
                 :
                 media_type == 'application' ?
                  media_extension == 'xls' || media_extension == 'xlsx'  ?
                  <iframe className = "pdf-preview" src={`https://view.officeapps.live.com/op/embed.aspx?src=${media_url}`} frameborder='0'>
                  </iframe>
                   :
                   <iframe className = "pdf-preview post"
                   src={`https://docs.google.com/gview?url=${media_url}&embedded=true`}
                 />
                :''}
               </div>
              </div>
                : ''}

              {post.post.data.LinkPreview.data.length>0?
             //if post has link then this component will called
                  <LinkPreview
                      postdata={post.post.data}
                  />
              :''}
            </div>
           
            {media_type == 'image' || media_type == 'video'?
                <div className = "post-pic-ranking-container">
                  <div className="post-pic">
                    {this.renderPostMedia(postData.assetDetail.data,post.post.data.identity)}
                  </div>
                    {postData.is_user_rate == 0 && feedType == 'live' && path[4]!="my" && roleName != 'guest' && postData.value.data.length > 0 && postOwnerId != loggedInUserId ? 
                      <div className = "renderRatings-container">
                        {this.renderRatings(ratingArray,postData,this.props.feedDetail.ratedPost)}
                      </div>
                    : ''}
                     {path[3]=='moderation'?
                  cultureValueName !=='' ? 
                    <div className="culture-value-name btn btn-primary">
                      <i class="material-icons">favorite_border</i>
                        {cultureValueName}
                      </div>
                    :'':''}    
                </div>
            : 
                <div className = "post-pic-ranking-container">  
                  {postData.is_user_rate == 0 && feedType == 'live' && path[4]!="my" && roleName != 'guest' && postData.value.data.length > 0? 
                  <div className= "render-ranking-only-text-container">
                    {postOwnerId != loggedInUserId ? this.renderRatings(ratingArray,postData,this.props.feedDetail.ratedPost): ""}
                  </div>
                : ''}
                </div>
            }
             
            
            {((feedType == 'live' || path[4]=="my") && showLikesComment && feedType!=="scheduled")
              ? <div className='post-meta'>

                <div
                  className={`like  ${this.state.likedisble}  likeLink ${isLiked ? 'liked' : ''}`}
                  data-postID={post.post.data.identity}
                  data-isliked={post.post.data.is_liked}
                  >
                  <a class="post-meta-link"
                    onClick={this.onLikeToggle.bind(
                        this,
                        post.post.data.identity
                      )}
                    >
                    <i className='material-icons'>thumb_up</i>
                      <span className='likeText'>Like</span>
                    </a>
                </div>
                {enableSocialFlag == 1?
                <div className='share'>
                  <div className='popup-container postSharePopup'>
                    {(this.props.details.post.data.type == 'external')
                        ? <a
                          className='link-share-popup post-meta-link'
                          onClick={this.onShareLinkClick.bind(
                              this,
                              post.post.data.identity
                            )}
                          >
                          <i className='material-icons'>share</i>
                          <span className='shareText'>Share</span>
                          </a>
                        : ''}
                        {this.state.sharePopupState &&
                        this.state.sharePopupStateOfPostID ==
                          post.post.data.identity
                        ? this.renderPopup(post.post.data.identity)
                        : null}
                  </div>
                </div>:''}
                <div
                  className='commentsLink comments'
                  data-postID={post.post.data.identity}
                  >
                  {((feedType == 'live' || path[4]=="my") && showLikesComment)
                      ? <div className='count'>
                        <span
                          className='likesCount'
                          data-postID={post.post.data.identity}
                          >
                          <span className='likesNumber'>
                          {(post.post.data.like_count !== null && post.post.data.like_count!==undefined && post.post.data.like_count !== 'NaN')? post.post.data.like_count :'0'}
                          </span>
                          <a className="post-meta-link"
                            id={"likebox-"+post.post.data.identity}
                            data-like-clicked="false"
                            onClick={this.whoLikedPostToggle.bind(
                                this,
                                post.post.data.identity,
                                post.post.data.like_count
                              )}
                            >
                            {this.state.LikesValue}
                            </a>

                        </span>
                        <span
                          className='commentsCount commentsLink'
                          data-postID={post.post.data.identity}
                          >
                          <span className='commentsNumber'>
                          {(post.post.data.comment_count !== null && post.post.data.comment_count!==undefined && post.post.data.comment_count !== 'NaN')? post.post.data.comment_count :'0'}
                          </span>
                          <a
                          className="post-meta-link"
                            onClick={ this.onCommentsToggle.bind(
                                this,
                                post.post.data.identity
                              )}

                            >
                              { this.state.value}
                            </a>
                        </span>
                      </div>
                      : null}
                  {this.props.notificationcall == 'true'
                      ? ''
                      : // this.onCommentsToggle(post.post.data.identity)
                        ''}
                </div>
              </div>
              : null}
              {  feed=='my' ?



            
            <div className='post-meta post-approve-wrapper'>
              {(feedType=="unapproved" || feedType=="failed") && post.post.data.approved =="Rejected"? 
                  <div className='status unapproved'>
                  <i className='material-icons'>highlight_off</i>
                    <span>Unapproved</span> 
                    <span className="feedback-post-unapprove" data-tip data-for={`feedback-${post.post.data.identity}`}> 
                    <i className='material-icons'>feedback</i></span>
                    {this.props.tooltip !== null ? 
                      <this.props.tooltip place="right" type="light" effect='solid' id={`feedback-${post.post.data.identity}`} delayHide={100} delayUpdate={100}>
                          <div className="tooltip-wrapper">
                              <div className="inner-tooltip-wrapper">
                                  <div className="tooltip-header clearfix">
                                      <div className="analytics-tooltip-title">
                                          <span>Feedback</span>
                                      </div>
                                  </div>
                                  <div className="tooltip-about-wrapper">
                                    {post.unapproveFeedback.length>0?
                                     <ul className="unapproved-feedback-wrapper">
                                       {post.unapproveFeedback.map((feedbackData,index) =>{
                                          return(
                                            <li key={index}>
                                              {post.unapproveFeedback[index]}
                                            </li>
                                          )
                                       })}
                                     </ul>:
                                     <span>
                                       <p>No feedback found.</p>
                                     </span>}
                                  </div>
                              </div>
                              
                            </div>
                        </this.props.tooltip>
                        : ''
                      }
                  </div>
                  : 
                  (feedType=="unapproved" || feedType=="failed") && post.is_published==2? 
                  <div className='status unapproved'>
                  <i className='material-icons'>error_outline</i>
                    Failed
                  </div>:''
              }
              {feedType=="pending"?
                  <div className='status pending-approval'>
                  <i className='material-icons'>remove_circle_outline</i>
                    Pending
                  </div>:''
              }

                {feedType!=="unapproved" && feedType!=="failed"   ? 
                  post.post.data.approved === 'Approved'
                ?
                feedType === 'scheduled'|| !showLikesComment || feedType === 'calendar'
                    ? <div className='status approved'>
                      <i className='material-icons'>add_circle_outline</i>
                        Approved
                      </div>
                    : null
                : '':''}
               </div>  : ''}
            </div>
            {this.renderTags()}
            {this.state.showCommentsofPostId == post.post.data.identity
              ? this.renderComments(post.post.data.identity)
              : null}

            {/* this.state.showLikes &&
              this.state.showLikesofPostId == post.post.data.identity
              ? this.renderLikes(post.post.data.identity)
              :  null */}

          </div>
          </div>
          </div>

      )
      // })}
    )
  }
}

function mapStateToProps (state) {
  return {
    profile: state.profile,
    accounts: state.socialAccounts.accounts,
    general: state.general,
    posts: state.posts,
    feedDetail: state.feed
  }
}

function mapDispatchToProps (dispatch) {
  return {
    dispatch,
    feedActions: bindActionCreators(feedActions, dispatch)
  }
}
let connection = connect(mapStateToProps, mapDispatchToProps)
module.exports = connection(Post)