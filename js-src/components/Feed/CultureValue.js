import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import { Link } from 'react-router';
import {Field, reduxForm} from 'redux-form'
import * as utils from '../../utils/utils'
import * as cultureActions from '../../actions/cultureActions';
import {Component}from 'react'
import PreLoader from '../PreLoader';
var search;
class CultuteValue extends React.Component{

    constructor(props) {
        super(props)
    
        this.state = {
          selectedValue: null,
          valueSearch: '',
          filter_state:[],
          cultureValueList:[]
        }
      }
    componentDidMount(){

        this.handleInitialize();
        this.props.cultureActions.fetchValuesList();
    }

    componentWillReceiveProps(newProps){
        if(this.props.culture!==newProps.culture){
        this.setState({
            cultureValueList:newProps.culture.cultureValueList,
            filter_state:newProps.culture.cultureValueList
        })
    }
    }

    handleSearchValue(e){
        if(e!==null){
            if(e.target.value!=="" && this.state.filter_state!==undefined)
            {
              var i=0;
              search=e.target.value.toLowerCase();
              var tempobj=this.state.filter_state.filter(function(temp){
                i=i+1;
                if(temp.title.toLowerCase().includes(search)){
                  return true;
                }
              })
              this.setState({cultureValueList:tempobj})
            }
            else{
              this.setState({cultureValueList:this.state.filter_state})
            }
          }
    }


    handleInitialize() {
        const initData = {
          value_search: this.state.valueSearch
        }
    }
    valueSelecttest(valueData){
        var me = this;
        if(me.props.SelectedCulturalValue!==undefined && valueData.id==me.props.SelectedCulturalValue.id){
            document.getElementById(valueData.id).classList.toggle("selecetdCulturValue")
            me.props.selectValue(undefined);
        }else{
            me.props.selectValue(valueData);
        } 
    }

    renderLoading(){
        return (
            <div>
                <ul className="cultureValue-List ">
                    <li  className='culture-value culture-value-loader-createPost  clearfix'>
                            <a className="valueSelect loader-grey-line loader-line-height"></a>
                    </li>
                    <li  className='culture-value culture-value-loader-createPost  clearfix'>
                            <a className="valueSelect loader-grey-line loader-line-height"></a>
                    </li>
                    <li  className='culture-value culture-value-loader-createPost  clearfix'>
                            <a className="valueSelect loader-grey-line loader-line-height"></a>
                    </li> 
                </ul>
            </div>
          )
    }
    

    render(){
        var me = this;
        var selectedValueClass='';
        let roleName =JSON.parse(localStorage.getItem('roles')) !== null? JSON.parse(localStorage.getItem('roles'))[0].role_name:''
        return(
            <div className="culturValue-createPost">
                <div className=''>
                <input
                    type='text'
                    name='value_search'
                    autoCapitalize='none'
                    autoComplete='off'
                    autoCorrect='off'
                    placeholder='Search for value'
                    className='search_values'
                    onChange={me
                    .handleSearchValue
                    .bind(this)}/>

                    <div className='valueList'>
              {me.props.culture.cultureLoader== true
                ? this.renderLoading()
                :
                <ul className="cultureValue-List ">
                 {me.state.cultureValueList.length >0 ?
                    me.state.cultureValueList
                    .map(function (value, i) {
                        me.props.SelectedCulturalValue!==undefined?
                        
                            me.props.SelectedCulturalValue.id==value.id?
                            selectedValueClass= "selecetdCulturValue":selectedValueClass=''
                        :selectedValueClass=''
                    return (
                        <li key={i} id={value.id}
                        className={`culture-value ${selectedValueClass} clearfix`}>
                        <a className="valueSelect" onClick={me.valueSelecttest.bind(me,value)}>
                        <i className="material-icons">favorite_border</i>
                        {value.title}</a>
                        </li>
                    )
                    }
                    ):<div className='NovalueFound'>
                    <li className = "no-Value-chart-wrapper"> No value found</li>
                    <li>{roleName=='super-admin' || roleName=='admin'? <Link to="/settings/culture" ><span className="text">Click here to add value</span></Link>:''}</li>
                    </div>
                    }
                    </ul>}
            </div>
                </div>
            </div>
        )
    }
}


function mapStateToProps(state) {
    return {
        culture : state.culture
    }
  }
  
  function mapDispatchToProps(dispatch) {
    return {
        cultureActions: bindActionCreators(cultureActions, dispatch)
    }
  }
  let connection = connect(mapStateToProps, mapDispatchToProps)
  
  var reduxConnectedComponent = connection(CultuteValue)
  
  export default reduxForm({
    form: `CultuteValue` // a unique identifier for this form
  })(reduxConnectedComponent)