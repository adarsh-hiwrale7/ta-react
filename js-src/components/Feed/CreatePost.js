import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import CreatePostForm from './CreatePostForm';
import PreLoader from '../PreLoader'
import * as utils from '../../utils/utils'
import Globals from '../../Globals';
import notie from "notie/dist/notie.js"
import moment from '../Chunks/ChunkMoment';
import croppie from '../Chunks/ChunkCroppie';
import * as feedActions from '../../actions/feed/feedActions'
import * as postsActions from '../../actions/feed/postsActions'
var isTwitterSelected=false
var campaignId= null
class CreatePost extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      Moment: null,
      croppie: null,
      latitude: 0,
      longitude: 0,
      mounted: true,
      userTag: [],
      listOfLinks: []
    }
    this.showPosition = this
      .showPosition
      .bind(this);
    if (navigator.geolocation) {
      navigator
        .geolocation
        .getCurrentPosition(this.showPosition);
    } else {
    }
    this
      .createTagList
      .bind(this);
  }
  showPosition(position) {
    let me = this;
    me.setState({ latitude: position.coords.latitude, longitude: position.coords.longitude })
  }

  componentWillMount() {
    campaignId = null
    if(this.props.campaignId){
      campaignId=this.props.campaignId
    }
    if (this.state.mounted) {
      moment().then((moment) => {
        this.setState({ Moment: moment })
      }).catch(err => {
        throw err;
      });

      croppie().then((croppie) => {
        this.setState({ croppie: croppie })
      }).catch(err => {
        throw err;
      });
    }
  }

  componentDidMount(){
    // this.props.feedActions.fetchLive(1,10,false);
    var campId = null
    // campaignId = null
    if(this.props.campaignId){
      // campaignId=this.props.campaignId
      campId=this.props.campaignId
    }
    this.props.feedActions.fetchHashTagInCreatePost(campId)
  }

  componentWillUnmount() {
    campaignId= null
    this.setState({ mounted: false })

  }
  createTagList(str) {
    var me = this;
    var rx1 = /@\[([^\]]+)]\(([^)]+)\)/g;
    let res = str.match(rx1);
    var userTag = [];
    return new Promise(function (resolve, reject) {
      if (res) {
        res
          .forEach(function (tags) {
            /** Get user information */
            var tagsArray = tags.split("@[");
            var nameArray = tagsArray[1].split("]");
            var name = nameArray[0];
            var dept = nameArray[1]
              .substring(0, nameArray[1].length - 1)
              .split(":")[1]
              .split('_');
            var type = "tags";
            if (nameArray[1].substring(0, nameArray[1].length - 1).split(":")[0] !== "(tags") {
              type = dept[1] == 'company' ? 'company' :
                dept[1] == 'dept' ?
                  "department"
                  : "user";

              let tagsUserObject = {
                tagid: dept[0],
                tagName: name.replace(/ /g, ''),
                type: type
              };
              userTag.push(tagsUserObject)
            }

            /** description replace */
            if (type === "tags") {
              str = str.replace(tags, "#" + name.replace(/ /g, ''));
            } else {
              str = str.replace(tags, "@" + name.replace(/ /g, ''));
            }

          })
        me.setState({
          userTag: userTag
        })
      }
      resolve(str);
    })
  }

  handleSubmit(values) {
    var utm_identities = [];
    if(values.utmLinks !== undefined){
      Object.keys(values.utmLinks).length>0?
        values.utmLinks.map((data,i)=>{
            values.description.includes(data.utm_url)?
              utm_identities.push(data.utm_identity)
            :''
        })
      :''
    }
    this
      .createTagList(values.description)
      .then((desc) => {
        let me = this;
        let postTags = this.postTagsData(values);
        let socialAccounts = this.socialAccountsData(values);
        let schedule = this.schedulersData(values, socialAccounts);
        let media = this.mediaData(values);
        var totalAssetCount =0;
        var videoCount =0;
        media.map(asset=>{
          totalAssetCount++;
          if(asset.mime_type.includes("video")){
            videoCount++;
          }
        })
        let SocialAccList = this.socialAccountsList(values);
        var isLinkedin = SocialAccList.includes("linkedin");
        var isTwitter = SocialAccList.includes("twitter");
        var isFacebook = SocialAccList.includes("facebook");
        if(videoCount>1 && (isLinkedin || isTwitter|| isFacebook)){
          notie.alert("error","You can only select one video at a time.",3);
        }else if(videoCount!==0&&totalAssetCount>1 && (isLinkedin || isTwitter|| isFacebook)){
          notie.alert("error","LinkedIn, Facebook and Twitter only allows one media type at a time. Please select either video, image(s) or GIF.",3);
        }else if(isTwitter==true && totalAssetCount>4){
          notie.alert("error","Twitter only allows maximum 4 images at a time.",3);
        }else{

        if (schedule == undefined) {
          return
        }
        
        if (socialAccounts.length == 0) {
          notie.alert('error', 'You must select at least 1 social channel', 5)
          this.setState({
            restrictHandleSubmitMethodCall: true,
            isWorldDateTimeApiCall: true
          })
          return;
        }
        if(isTwitterSelected==true){
          var imageError = false;
          var videoError = false;
          isTwitterSelected=false
          var type=''
          desc=utils.convertSpecialCharacterToUnicode(utils.toUnicodetwo(utils.truncateTwitterText(desc)))
          media.map((asset=>{
            var convertSizeInMb=asset.media_size / (1024 * 1024).toFixed(2)
            var index = asset.mime_type.indexOf('/')
            if (index !== -1) {
              type = asset.mime_type.substr(0, index)
            } else {
              type = asset.mime_type
            }
            if(type!=='' && type=='image'){
              if(convertSizeInMb > 5){
                imageError = true;
                // notie.alert('error', 'You can not upload image size greater than 5 MB', 5)
              return
              }
            }
            if(asset.media_duration !== undefined && (asset.media_duration > 131 || convertSizeInMb > 15)){
              videoError = true;
              // notie.alert('error', 'Video too long. Please upload video under 2:20 minutes/15MB.', 5)
              return
            }
          }))

          if(imageError==true){
            notie.alert('error', 'You can not upload image size greater than 5 MB', 5)
            return
          }else if(videoError==true){
            notie.alert('error', 'Video too long. Please upload video under 2:20 minutes/15MB.', 5)
            return
          }
          if (typeof (values.mediaGiphyDirectUpload) !== "undefined" && values.mediaGiphyDirectUpload !== null) {
            var convertSizeInMb=values.mediaGiphyDirectUpload.filesize / (1024 * 1024).toFixed(2)
            if(convertSizeInMb > 15){
              notie.alert('error', 'You can not upload GIF more then 15 MB on twitter.', 5)
              return
            }
          }
        }else{
          desc=utils.convertSpecialCharacterToUnicode(utils.toUnicodetwo(desc))
        }
        var isCampaign = false
        var newLocation = utils
          .removeTrailingSlash(this.props.location.pathname)
          .split('/')
        var feedtype = newLocation[2] ? newLocation[2] : ''
        var feedStatus = newLocation[3] ? newLocation[3] : ''
        // if (this.props.location.query.createpost !== undefined || this.props.location.query.searchcreatepost !== undefined) {
        //   //when post will create from campaign 
        //   isCampaign = true
        // }
        if(this.props.campaignId!==null && this.props.campaignId !== undefined){
          isCampaign = true
        }
        this.setState({
          listOfLinks: utils.checkLinkInPost(desc)
        })

        let fieldValues = {}
        let isScheduledPost = false
        if (values.isScheduled == true) {
          isScheduledPost = true
        }
        if (media.length == 0 && desc.trim() == '') {
          //call when media type is not selected and description is also empty
          this.props.postsActions.postCreationErrorInitiate()
          notie.alert('error', 'You can not create empty post.', 5)
          return;
        }
        // if(media.length>1 && values.type !== "Internal"){
        //   this.props.postsActions.postCreationErrorInitiate()
        //   notie.alert('error', 'You can not post multiple media on social.', 5)
        //   return;
        // }
        this.props.postsActions.postCreationInitiate();

        var feedTypeTextArray = this.props.location.pathname.split("/");
        var isFromFilter= false;
        if(feedTypeTextArray.indexOf('default')==2){
          var feed_identity = 'rpPDp'
        }else if(feedTypeTextArray.indexOf('my')==2){
          //if post is creating from my feed and feed type is changed from filter then to pass its id
          var feed_identity = this.props.currentFeed.identity !== undefined?this.props.currentFeed.identity:'rpPDp';
          isFromFilter=this.props.currentFeed.type !== undefined ?true:false;
          // feedtype = this.props.currentFeed.type !== undefined ?this.props.currentFeed.type:feedtype;
        }else{
          var feed_identity = this.props.currentFeed.identity;
          // feedtype = this.props.currentFeed.type;
        }
        fieldValues = {
          feed_identity: feed_identity,
          type: values.type,
          isCampaign: isCampaign,
          latitude: this.state.latitude,
          longitude: this.state.longitude,
          media: media,
          feedtype: feedtype,
          feedStatus: feedStatus,
          tags: postTags,
          schedule: schedule,
          isFromFilter:isFromFilter,
          isScheduledPost: isScheduledPost,
          user_tag: me.state.userTag,
          link_preview: this.state.listOfLinks,
          detail: desc
        }
        if (values.campaign !== null) {
          fieldValues["campaign_identity"] = values.campaign
        }
        if(values.valueAI!==null && values.valueAI!==undefined){
          fieldValues["value_identity"] = values.valueAI;
        }
        if(utm_identities.length>0){
          fieldValues["utm_identities"] = utm_identities;
        }
        var isFirstPost = false;
        if(this.props.feed.mypost.live.length==0){
            isFirstPost = true;
        }
        me
          .props
          .postsActions
          .postCreate(fieldValues,isFirstPost,this.props.profile.profile.role.data[0].role_name)
        }
      });
  }

  schedulersData(values, socialAccounts) {
    let schedulers = [];

    let output = [];
    var counts = {};
    for (let sc = 0; sc < values.schedulerCounter; sc++) {


      let time = 0
      let date = 0
      var currentDate = date
      var currentTime = time

      if (values.hasOwnProperty(`scheduledTime-${sc}`) && values[`scheduledTime-${sc}`] !== null) {
        time = this
          .state
          .Moment(values[`scheduledTime-${sc}`])
          .format("HH:mm");
      }
      if (values.hasOwnProperty(`scheduledDate-${sc}`) && values[`scheduledDate-${sc}`] !== null) {
        date = this
          .state
          .Moment
          .unix(values[`scheduledDate-${sc}`])
          .format("DD/MM/YYYY");
      }
      if(time !==0 && date==0){
        date= this.state.Moment().format("DD/MM/YYYY");
      }
      let concatenatedDateTime = date + " " + time;
      //if date is 0 means post is not scheduled then to pass 'instant' string in param
      let timeStamp = date !== 0 || time !== 0 ? this
        .state
        .Moment(concatenatedDateTime, 'DD/MM/YYYY HH:mm')
        .format("X"):'instant';
      schedulers.forEach(function (x) {
        counts[x] = (counts[x] || 0) + 1;
      });

      if (this.state.Moment().format("DD/MM/YYYY") == date) {
        //if current date is match with scheduled date and current time is matched with schedule time then to resrtict post
        if (this.state.Moment().format("HH:mm") > time) {
          
          notie.alert('error', 'Sorry, Your scheduled time is left.', 5)
          return;
        }
      }
      if (!schedulers.includes(parseInt(timeStamp))) {
        var scheduler_date = this.state.Moment.unix(parseInt(timeStamp)).format('MM-DD-YYYY');
        var today = this.state.Moment().unix()
        var today_date = this.state.Moment.unix(today).format('MM-DD-YYYY');
        var date1 = new Date(scheduler_date.toString());
        var date2 = new Date(today_date.toString());
        var timeDiff = date2.getTime() - date1.getTime();
        var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
        if (diffDays > 0) {
          notie.alert('error', 'You can not schedule a post in past. Please select a future time.', 5)
          return;
        } else {
          schedulers.push(timeStamp !=='instant' ? parseInt(timeStamp):timeStamp)
        }
      } else {
        notie.alert('error', 'You can not select same date for schedule.', 5)
        return;
      }
    }
    for (let a = 0; a < socialAccounts.length; a++) {
      for (let sc = 0; sc < schedulers.length; sc++) {
        output.push({ "social_account_identity": socialAccounts[a].social_account_identity, "scheduled_at": schedulers[sc] })
      }
    }
    return output;
  }

  mediaData(values) {
    let media = [];
      if (values["mediaDirectUpload"] !== undefined && values["mediaDirectUpload"] !== null && values["mediaDirectUpload"].length>0) {
        var uploadedAssetData = values["mediaDirectUpload"];
          for(var i =0;i< uploadedAssetData.length;i++){
            var objToPush ={
              asset_identity: "", 
              type: "local", 
              media_url: uploadedAssetData[i].fileUrl, 
              thumbnail_url: uploadedAssetData[i].thumbUrl, 
              media_size: uploadedAssetData[i].media_size, 
              mime_type: uploadedAssetData[i].mime_type, 
              file_extension: uploadedAssetData[i].file_extension,
              media_duration:uploadedAssetData[i].duration
            }
            if(uploadedAssetData[i].height!==undefined&&uploadedAssetData[i].width!==undefined){
              var objtoAdd ={
                height : uploadedAssetData[i].height,
                width: uploadedAssetData[i].width 
              }
              Object.assign(objToPush,objtoAdd)
            }
            media.push(objToPush);
          }
    }

    if (values["editAssetData"]!==undefined && values["editAssetData"] !== null && values["editAssetData"].length>0) {
      var uploadedEditData = values["editAssetData"];
      for(var i =0;i< uploadedEditData.length;i++){
        var objToPush ={
          asset_identity: "", 
          type: "local", 
          media_url: uploadedEditData[i].media_url, 
          thumbnail_url: uploadedEditData[i].thumbnail_url, 
          media_size: uploadedEditData[i].media_size, 
          mime_type: uploadedEditData[i].media_type, 
          file_extension: uploadedEditData[i].media_extension,
        }

        media.push(objToPush);
      }
    }

    if (values["mediaDriveUpload"]!==undefined && values["mediaDriveUpload"] !== null && values["mediaDriveUpload"].length>0) {
      var uploadedDriveData = values["mediaDriveUpload"];
      for(var i =0;i< uploadedDriveData.length;i++){
        var objToPush ={
          asset_identity: "", 
          type: "local", 
          media_url: uploadedDriveData[i].fileUrl, 
          thumbnail_url: uploadedDriveData[i].thumbUrl, 
          media_size: uploadedDriveData[i].media_size, 
          mime_type: uploadedDriveData[i].mime_type, 
          file_extension: uploadedDriveData[i].file_extension,
        }

        media.push(objToPush);
      }
    }
    if (values["mediaUploadedasset"] !== undefined && values["mediaUploadedasset"] !== null && Object.keys(values["mediaUploadedasset"]).length>0) {
      var uploadedAssetData = values["mediaUploadedasset"];
      Object.keys(uploadedAssetData).map((assetIndex,i)=>{
        var objToPush ={
          asset_identity:assetIndex, 
          type: "asset", 
          media_url: uploadedAssetData[assetIndex].media_url, 
          thumbnail_url: uploadedAssetData[assetIndex].thumbnail_url, 
          media_size: uploadedAssetData[assetIndex].media_size, 
          mime_type: uploadedAssetData[assetIndex].media_type, 
          file_extension: uploadedAssetData[assetIndex].media_extension,
          media_duration:uploadedAssetData[assetIndex].media_duration !== null && uploadedAssetData[assetIndex].media_duration !== undefined ? uploadedAssetData[assetIndex].media_duration :undefined 
        }
        media.push(objToPush);
      })
    }
    if (typeof (values.mediaGiphyDirectUpload) !== "undefined") {
      if (values["mediaGiphyDirectUpload"] !== null) {
        media.push({ asset_identity: "", type: "local", media_url: values["mediaGiphyDirectUpload"].fileUrl, thumbnail_url: values["mediaGiphyDirectUpload"].thumbUrl, media_size: values["mediaGiphyDirectUpload"].filesize, mime_type: 'image/gif', file_extension: 'gif' })
        if(values["mediaGiphyDirectUpload"].height!==undefined&&values["mediaGiphyDirectUpload"].width!==undefined){
          var objtoAdd ={
            height :values["mediaGiphyDirectUpload"].height,
            width:values["mediaGiphyDirectUpload"].width 
          }
          Object.assign(media[0],objtoAdd)
        }
      }
    }

    // if(typeof (values.mediaDriveUpload) !== "undefined"){
    //   if (values["mediaDriveUpload"] !== null) {
    //     media.push({ asset_identity: "", type: "local", media_url: values["mediaDriveUpload"].fileUrl, thumbnail_url: values["mediaDriveUpload"].thumbUrl, media_size: values["mediaDriveUpload"].media_size, mime_type: values["mediaDriveUpload"].mime_type, file_extension: values["mediaDriveUpload"].file_extension })
    //   }
    // }
    // if (typeof(values.mediaAsset) !== "undefined") {
    //   if (values["mediaAsset"] !== null) {
    //     media.push({asset_identity: values["mediaAsset"], type: "asset",media_size:values['mediaAssetSize']})
    //   }
    // }

    return media;
  }

  socialAccountsData(values) {
    let socialData = [];
    for (var prop in values) {
      if (values.hasOwnProperty(prop) && prop.toString().startsWith('chk-') && values[prop] == true) {
        if(prop.split("-")[1]=='twitter' || prop.split("-")[1] =='twitterpage'){
          //to truncate text description if social media is twitter or twitterpage
          isTwitterSelected=true
        }
        socialData.push({
          social_account_identity: prop.substr(prop.lastIndexOf('-') + 1)
        })

      }
    }
    return socialData;
  }
  socialAccountsList(values){
    let socialData = [];
    for (var prop in values) {
      if (values.hasOwnProperty(prop) && prop.toString().startsWith('chk-') && values[prop] == true) {
          if(prop.includes("facebook")){
            socialData.push('facebook')
          }else if(prop.includes('linkedin')){
            socialData.push('linkedin')
          }else if(prop.includes("twitter")){
            socialData.push('twitter')
          }
      }
    }
    return socialData;
  }

  postTagsData(values) {
    let postTags = undefined;
    let tags = [];
    if (typeof (values.tags) !== "undefined") {
      for (var t = 0; t < values.tags.length; t++) {
        tags.push(values.tags[t].label);
      }
    }
    if (tags.length) {
      postTags = {
        post: tags
      }
    }
    return postTags;
  }

  renderLoading() {
    return (
      <div className="preloader-wrap">
        <PreLoader></PreLoader>
      </div>
    )
  }
  render() {
    var loading = this.props.posts.creation.loading;
    var me = this;
    return (
      <section className="">
        <div id="post-creation">

          {(loading
            ? this.renderLoading()
            : <div></div>)}
          <CreatePostForm
            campaignId={campaignId}
            // campaignChannels={this.props.campaignChannels}
            campaignTitle={this.props.campaignTitle}
            jQuery={this.props.jQuery}
            croppie={this.state.croppie}
            location={this.props.location}
            defaultFeed={this.props.defaultFeed}
            currentFeed = {this.props.currentFeed}
            editPostData={this.props.editPostData}
            socialChannelList = {this.socialAccountsData}
            editCurationData = {this.props.editCurationData}
            savedHashTagListInPost={this.props.feed.savedHashTagListInPost}
               //In share & Edit pass the data into the CreatePostFrom
            customTextFlag = {this.props.customTextFlag}
            onSubmit={this
              .handleSubmit
              .bind(this)}></CreatePostForm>


        </div>

      </section>
    );
  }

}

function mapStateToProps(state) {
  return {
    posts: state.posts,
    feed: state.feed,
    profile: state.profile
  }
}

function mapDispatchToProps(dispatch) {
  return {
    postsActions: bindActionCreators(postsActions, dispatch),
    feedActions: bindActionCreators(feedActions, dispatch)
  }
}

let connection = connect(mapStateToProps, mapDispatchToProps);

module.exports = connection(CreatePost);