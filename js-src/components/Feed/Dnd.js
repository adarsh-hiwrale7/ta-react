import bigCalendar from 'react-big-calendar'
import {connect} from 'react-redux'
import { DragDropContext } from 'react-dnd'
import HTML5Backend from 'react-dnd-html5-backend'
import PreLoader from '../PreLoader'
import * as postsActions from '../../actions/feed/postsActions'

import withDragAndDrop from 'react-big-calendar/lib/addons/dragAndDrop'


const DragAndDropCalendar = withDragAndDrop(bigCalendar)
class Dnd extends React.Component {
  resizeEvent = (resizeType, { event, start, end }) => {
    const { events } = this.state

    const nextEvents = events.map(existingEvent => {
      return existingEvent.id == event.id
        ? { ...existingEvent, start, end }
        : existingEvent
    })

    this.setState({
      events: nextEvents,
    })
  }
  
  render() {
    return (
      <div>
      <div className='initializeStyle hide'>click</div>
      <DragAndDropCalendar
        selectable
        events={this.props.events}
        onEventDrop={this.props.onEventDrop}
        components = {this.props.components}
        popup = {this.props.popup}
        resizable
        onSelectEvent = {this.props.onSelectEvent}
        onEventResize={this.resizeEvent}
        views = {this.props.views}
        date = {this.props.date}
        onNavigate = {this.props.onNavigate}
        onView={this.props.onView}
        allDayAccessor={this.props.allDayAccessor}
        onSelectSlot = {this.props.onSelectSlot}
      />
      </div>
    )
  }
}
function mapStateToProps(state) {
  return {posts: state.posts, feed: state.feed}
}

function mapDispatchToProps(dispatch) {
  return {
    postsActions: bindActionCreators(postsActions, dispatch),
    feedActions: bindActionCreators(feedActions, dispatch)
  }
}

let connection = connect(mapStateToProps, mapDispatchToProps);

export default DragDropContext(HTML5Backend)(Dnd)