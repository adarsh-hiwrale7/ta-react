import { bindActionCreators } from 'redux'
import Linkedin from '../Social/Linkedin'
import Twitter from '../Social/Twitter'
import Facebook from '../Social/Facebook'
import { connect } from 'react-redux'
import { Link } from 'react-router'
import Globals from '../../Globals'
import reactTooltip from '../Chunks/ChunkReactTooltip';
import ReactDOM from 'react-dom'
import moment from '../Chunks/ChunkMoment'
import * as utils from '../../utils/utils'
import * as socialAccountsActions from '../../actions/socialAccountsActions'
import * as postsActions from '../../actions/feed/postsActions'

var CommentString = ''
var FinalComment = ''
var userTag=[]
export default class SharePopup extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      popupVisible: false,
      shareLink: '',
      Moment: null,
      latitude: 0,
      longitude: 0,
      tooltip: null,
    }
    this.showPosition = this.showPosition.bind(this)
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(this.showPosition)
    }
  }
  componentWillMount () {
    // add event listener for clicks
    document.addEventListener('click', this.handleClick, false)
    moment()
      .then(moment => {
        reactTooltip().then(reactTooltip => {
        this.setState({ Moment: moment,tooltip: reactTooltip  })
        })
      })
      .catch(err => {
        throw err
      })
  }
  componentWillUnmount () {
    // make sure you remove the listener when the component is destroyed
    document.removeEventListener('click', this.handleClick, false)
  }
  handleClick = e => {
    if (!ReactDOM.findDOMNode(this).contains(e.target)) {
      // the click was outside your component, so handle closing here
      //  ReactDOM.findDOMNode(this).close();
    }
  }

  /**
   * 
   * @param {*} id id of job or link from rss 
   */
  onClose (id) {
    if(id == ''){
      id = this.props.AllData.permalink!==undefined? this.props.AllData.permalink:''
    }
    var sharePopupPost = document.querySelectorAll('.popup-center-wrapper .share-popup-post');
    sharePopupPost.forEach(function(value){
      value.classList.toggle('hide');
    })
    this.props.onShareLinkClick(id)
  }
  
  /**
 * @author disha
 * call api after share is successfully done
 * @param {*} accountName
 * @param {*} id id of success post
 */
  shareOnSocialMedia (accountName, id, socialAccId = null) {
    //if (this.props.shareFrom == 'jobs') {
      var objToSend = {
        socialId: socialAccId.id,
        social_platform: accountName,
        resource: this.props.shareFrom == 'jobs' ? 'job' : 'rss',
        userId: this.props.profile.profile.identity
      }
      if(this.props.AllData.identity !== '' && typeof this.props.AllData.identity !== 'undefined'){
         var ObjToAdd = {
            Localid: this.props.AllData.identity
         }     
         Object.assign(objToSend, {Localid:this.props.AllData.identity})
            
      }else{
        Object.assign(objToSend, {Localid:0})
      }
      var ObjToAdd = {
        social_account_id: socialAccId.socialAccId
      }
      accountName == 'Facebook' ? Object.assign(objToSend, ObjToAdd) : ''
      this.props.socialAccountsActions.shareOnSocialMedia(objToSend)
    //}
  }
//to close share popup 
  closeSharePopupInFeed () {
    this.props.closeJobRssSharePopup()
  }
  createTagList (str) {
    var me = this
    var rx1 = /@\[([^\]]+)]\(([^)]+)\)/g
    let res = str.match(rx1)
    var localuserTag = []

    return new Promise(function (resolve, reject) {
      if (res) {
        res.forEach(function (tags) {
          /** Get user information */
          var tagsArray = tags.split('@[')
          var nameArray = tagsArray[1].split(']')
          var name = nameArray[0]
          var dept = nameArray[1]
            .substring(0, nameArray[1].length - 1)
            .split(':')[1]
            .split('_')
          var type = 'tags'
          if (
            nameArray[1].substring(0, nameArray[1].length - 1).split(':')[0] !==
            '(tags'
          ) {
            type = dept[1] == 'company'
              ? 'company'
              : dept[1] == 'dept' ? 'department' : 'user'

            let tagsUserObject = {
              tagid: dept[0],
              tagName: name.replace(/ /g, ''),
              type: type
            }
            localuserTag.push(tagsUserObject)
          }

          /** description replace */
          if (type === 'tags') {
            str = str.replace(tags, '#' + name.replace(/ /g, ''))
          } else {
            str = str.replace(tags, '@' + name.replace(/ /g, ''))
          }
        })
        userTag=localuserTag
        // me.setState({
        //   userTag: userTag
        // })
      }
      resolve(str)
    })
  }
  shareCuratedInterlly () {
    var postDetails = ''
    let time = this.state.Moment().format('HH:mm')
    let date = this.state.Moment().format('DD/MM/YYYY')
    let concatenatedDateTime = date + ' ' + time
    if (this.props.comment == undefined || this.props.comment == null) {
      CommentString = this.props.AllData.title
    } else {
      for (var i = 0; i < Object.keys(this.props.comment).length - 1; i++) {
        CommentString = CommentString + this.props.comment[i]
      }
    }
    this.createTagList(utils.toUnicodetwo(CommentString)).then(desc => {
      let time = this.state.Moment().format('HH:mm')
      let date = this.state.Moment().format('DD/MM/YYYY')
      let concatenatedDateTime = date + ' ' + time
      let timeStamp = this.state
        .Moment(concatenatedDateTime, 'DD/MM/YYYY HH:mm')
        .format('X')

      let schedule = [
        {
          scheduled_at: timeStamp,
          social_account_identity: 0
        }
      ]
      postDetails = desc

      let fieldValues = {
        detail: postDetails,
        type: 'Internal',
        feed_identity: 'rpPDp',
        isCampaign: false,
        latitude: this.state.latitude,
        longitude: this.state.longitude,
        media: [],
        feedtype: 'default',
        feedStatus: 'live',
        schedule: schedule,
        isScheduledPost: false,
        user_tag: userTag,
        link_preview: [{ link: this.props.AllData.permalink }]
      }
      this.props.postsActions.postCreate(fieldValues)

      CommentString = ''
      postDetails = ''
    })
  }
  showPosition (position) {
    let me = this
    me.setState({
      latitude: position.coords.latitude,
      longitude: position.coords.longitude
    })
  }
  editCurationData(){
    var postDetails = ''
    let time = this.state.Moment().format('HH:mm')
    let date = this.state.Moment().format('DD/MM/YYYY')
    let concatenatedDateTime = date + ' ' + time
    if (this.props.comment == undefined || this.props.comment == null) {
      CommentString = this.props.AllData.title
    } else {
      for (var i = 0; i < Object.keys(this.props.comment).length - 1; i++) {
        CommentString = CommentString + this.props.comment[i]
      }
    }
    this.createTagList(utils.toUnicodetwo(CommentString)).then(desc => {
      let time = this.state.Moment().format('HH:mm')
      let date = this.state.Moment().format('DD/MM/YYYY')
      let concatenatedDateTime = date + ' ' + time
      let timeStamp = this.state
        .Moment(concatenatedDateTime, 'DD/MM/YYYY HH:mm')
        .format('X')

      let schedule = [
        {
          scheduled_at: timeStamp,
          social_account_identity: 0
        }
      ]
      postDetails = desc

      let fieldValues = {
        detail: postDetails,
        // type: 'Internal',
        // feed_identity: 'rpPDp',
        // isCampaign: false,
        // latitude: this.state.latitude,
        // longitude: this.state.longitude,
        // media: [],
        // feedtype: 'default',
        // feedStatus: 'live',
        // schedule: schedule,
        // isScheduledPost: false,
        // user_tag: userTag,
        link_preview: [{ link: this.props.AllData.permalink }]
      }
      document.getElementById("rss_popup")?document.getElementById("rss_popup").classList.add("hide"):''
      this
      .props
      .postsActions
      .creatingNewPost();
      this.props.postsActions.editCurationInit(fieldValues);
    })
  }
  render () {
    var isFbHasLink=false
    var isInternalHasLink=false
    var isTwHasLink=false
    var isLiHasLink=false
    var allData = this.props.AllData
    var jobrssid = allData.identity !== undefined? allData.identity: ''
    let social = typeof this.props.accounts !== 'undefined'? this.props.accounts: []
    var profileAvatar = this.props.profile.profile.avatar !== undefined? this.props.profile.profile.avatar: ''
    let shareLink, TWshareLink, FBshareLink, LIshareLink = ''
    var FeedComment = ''
    this.allSocialChannels = Globals.EXTERNAL_SOCIAL.slice()
    var twitterMessage = ''
    var linkedinMessage = ''
    var liniscompany, fbiscompany, twiscomapny = ''
    var pageid = {}
    var fbpageid,
      fbtoken,
      liavatar,
      fbavatar,
      twavatar,
      fbusername,
      fbuserid,
      fbpageavatar,
      twpageavatar,
      lipageavatar,
      fbpagename,
      twusername,
      twuserName,
      twotherusername,
      twotheruser,
      lipageName,
      lipagename,
      liusername,
      liuserName,
      LinkedinSocialPageId,
      LinkedinSocialUserId = ''
    this.usersSocialChannels = []
    this.userSocialPageDetail = []

    //if comment is undefined means it is not passed and share job/rss direct without adding comment
    if (this.props.comment !== undefined) {
      //if comment is added thn to fetch its string from object
      for (var i = 0; i < Object.keys(this.props.comment).length - 1; i++) {
        FeedComment = FeedComment + this.props.comment[i]
      }
      this.createTagList(utils.toUnicodetwo(FeedComment)).then(desc => {
        FinalComment = desc
      })
    }
    
    //to pass link to share from jobs/rss
    if (allData.permalink !== undefined && this.props.shareFrom=='rss') {
      //if trying to share rss then to pass its link
      shareLink = allData.permalink
      isFbHasLink=true
        isTwHasLink=true
        isLiHasLink=true
    } else if(this.props.shareFrom=='jobs') {
      //if trying to share jobs 
      if (allData.workable_link !== '' && allData.workable_link !== null && allData.workable_link !== undefined) {
        //check workable link is available
        shareLink = allData.workable_link
        isFbHasLink=true
        isTwHasLink=true
        isLiHasLink=true
      }
      if (allData.facebook_link !== null) {
        FBshareLink = allData.facebook_link,
        isFbHasLink=true
      }
      if (allData.twitter_link !== null) {
        TWshareLink = allData.twitter_link
        isTwHasLink=true
      }
      if (allData.linkedin_link !== null ) {
        LIshareLink = allData.linkedin_link
        isLiHasLink=true
      }
    }
    social.map(s => {
      pageid = s.identity
      if (s.social_media == 'linkedin') {
        var liuserName = s.user_social.filter(function (v) {
          return v['key'] == 'name'
        })
        liuserName.map(username => {
          liusername = username.value
        })
        var useravatar = s.user_social.filter(function (v) {
          return v['key'] == 'avatar'
        })

        if (s.iscompany == 1) {
          liniscompany = s.iscompany
          LinkedinSocialPageId = s.identity
          var lipageName = s.user_social.filter(function (v) {
            return v['key'] == 'page_name'
          })
          lipageName.map(pagename => {
            lipagename = pagename.value
          })
          var useravatar = s.user_social.filter(function (v) {
            return v['key'] == 'avatar'
          })
          useravatar.map(userpic => {
            lipageavatar = userpic.value
              ? userpic.value
              : 'https://app.visibly.io/img/default-avatar.png'
          })
        } else {
          LinkedinSocialUserId = s.identity
          useravatar.map(userpic => {
            liavatar = userpic.value
              ? userpic.value
              : 'https://app.visibly.io/img/default-avatar.png'
          })
        }
      }
      if (s.social_media == 'twitter') {
        var useravatar = s.user_social.filter(function (v) {
          return v['key'] == 'avatar'
        })

        if (s.iscompany == 1) {
          var twuserName = s.user_social.filter(function (v) {
            return v['key'] == 'name'
          })
          twiscomapny = s.iscompany
          var useravatar = s.user_social.filter(function (v) {
            return v['key'] == 'avatar'
          })

          useravatar.map(userpic => {
            twpageavatar = userpic.value
          })
          twuserName.map(username => {
            twotherusername = username.value
          })
        } else {
          var twuserName = s.user_social.filter(function (v) {
            return v['key'] == 'name'
          })
          twuserName.map(username => {
            twusername = username.value
          })
          useravatar.map(userpic => {
            twavatar = userpic.value
              ? userpic.value
              : 'https://app.visibly.io/img/default-avatar.png'
          })
        }
      }
      if (s.social_media == 'facebook') {
        var fbuserName = s.user_social.filter(function (v) {
          return v['key'] == 'name'
        })
        fbuserName.map(username => {
          fbusername = username.value
        })
        var fbaccesstoken = s.user_social.filter(function (v) {
          return v['key'] == 'api_token'
        })
        if (s.iscompany == 1) {
          fbiscompany = s.iscompany
          var fbPageidvalue = s.user_social.filter(function (v) {
            return v['key'] == 'page_id'
          })
          var fbpageName = s.user_social.filter(function (v) {
            return v['key'] == 'page_name'
          })

          var useravatar = s.user_social.filter(function (v) {
            return v['key'] == 'avatar'
          })
          useravatar.map(userpic => {
            fbpageavatar = userpic.value
              ? userpic.value
              : 'https://app.visibly.io/img/default-avatar.png'
          })
          fbpageName.map(pagename => {
            fbpagename = pagename.value
          })
          fbPageidvalue.map(pageid => {
            fbpageid = pageid.value
          })
          fbaccesstoken.map(token => {
            fbtoken = token.value
          })
        } else {
          var fbuseridvalue = s.user_social.filter(function (v) {
            return v['key'] == 'user_id'
          })
          var useravatar = s.user_social.filter(function (v) {
            return v['key'] == 'avatar'
          })
          fbuseridvalue.map(pageid => {
            fbuserid = pageid.value
          })
          useravatar.map(userpic => {
            fbavatar = userpic.value
              ? userpic.value
              : 'https://app.visibly.io/img/default-avatar.png'
          })
          fbaccesstoken.map(token => {
            fbtoken = token.value
          })
        }
      }
      var connectedSocialAccIndex = this.allSocialChannels.indexOf(
        s.social_media.toLowerCase()
      )
      if (connectedSocialAccIndex > -1) {
        this.usersSocialChannels.push(s.social_media.toLowerCase())
        this.allSocialChannels.splice(connectedSocialAccIndex, 1)
      }
    })
    if (FinalComment !== '') {
      //if comment is inserted then to pass comment with link
      twitterMessage = shareLink !== '' && shareLink !== undefined? FinalComment + ' ' + shareLink: FinalComment + ' ' + TWshareLink
      linkedinMessage = shareLink !== '' && shareLink !== undefined? FinalComment + ' ' + shareLink: FinalComment + ' ' + LIshareLink
    } else {
      twitterMessage = shareLink !== '' && shareLink !== undefined? shareLink: TWshareLink
      linkedinMessage = shareLink !== '' && shareLink !== undefined? shareLink: LIshareLink
    }
    var enableSocialFlag=this.props.profile.profile.enable_social_posting;
    return (
      <div className='popup-container'>
        <div
          className='popup sharePopup'
          id={`share-popup-${jobrssid !== '' ? jobrssid : this.props.AllData.permalink !== undefined ? this.props.AllData.permalink : ''}`}
        >

          <nav className='nav-side'>
            <div className='parent  clearfix  nav-side-share-popup'>
              {/* this button is used outside close button  */}
              {/* <button  className = "closeSharePopupPost" name = "close-share-popup " onClick ={this.closeSharePopupInFeed.bind(this)}>  close </button> */}
              <ul className='channels'>
                {this.props.shareFrom=="rss" ?
                <li className = "channel" onClick = {this.editCurationData.bind(this)}>
                  <span className = "edit-post-icon">
                  <i class="material-icons"> create </i>
                  </span>
                </li>:''}
                {this.props.shareFrom == 'rss'
                  ? this.usersSocialChannels.length>0?
                     <li className='channel'>
                      <span className='share-on-social'>
                        <a
                          onClick={this.shareCuratedInterlly.bind(this)}
                          className='internal-icon rss-job-internal'
                          title='Internal channel'
                          >

                          {profileAvatar !== null && profileAvatar !== ''
                              ? <img
                                src={profileAvatar}
                                onError={e => {
                                  e.target.src =
                                      'https://app.visibly.io/img/default-avatar.png'
                                }}
                                />
                              : ''}
                          <i class='material-icons'>vpn_lock</i>
                          {' '}
                        </a>
                      </span>
                    </li>:''
                  : ''}
              {enableSocialFlag == 1 ?
                this.usersSocialChannels.indexOf('facebook') > -1
                  ? <li className={`channel ${isFbHasLink==false ?'jobrssCheckBox':''}`}>
                  <span
                    data-tip={isFbHasLink ==false ?'Link is not available.':''}
                    data-for={`linknotAvailableforfacebook`}>
                    <Facebook
                      className=''
                      name='share-fb'
                      text='Facebook'
                      message={FinalComment}
                      isFbHasLink={isFbHasLink}
                      videourl={0}
                      linkToShare={
                          shareLink !== '' && shareLink !== undefined
                            ? shareLink
                            : FBshareLink
                        }
                      accessToken={fbtoken}
                      image_url='https://app.visibly.io/img/default-avatar.png'
                      userid={fbuserid}
                      shareFrom={this.props.shareFrom}
                      successId={this.shareOnSocialMedia.bind(
                          this,
                          'Facebook',
                          fbuserid
                        )}
                      />
                      {isFbHasLink==false
                      ? this.state.tooltip?
                      <this.state.tooltip
                        type='info'
                        id={`linknotAvailableforfacebook`}
                        effect = 'solid'
                        className = 'tooltipsharepopup'
                         />:''
                      : null}
                      </span>
                  </li>
                  : '':''}
                {enableSocialFlag == 1 ?
                this.usersSocialChannels.indexOf('twitter') > -1
                  ? <li className={`channel ${isTwHasLink==false ?'jobrssCheckBox':''}`}>
                    <span
                      data-tip={`${isTwHasLink==false ?'Link is not available.':twusername}`}
                      data-for={'notConnectedSocialChannels'}
                      >
                      <Twitter
                        className=''
                        name='share-tw'
                        text='Twitter'
                        isTwHasLink={isTwHasLink}
                        message={twitterMessage}
                        iscompany={0}
                        image_url={twavatar}
                        postId={jobrssid}
                        resourceType={this.props.shareFrom}
                        />
                      {this.state.tooltip?<this.state.tooltip
                        type='info'
                        id='notConnectedSocialChannels'
                        effect='solid'
                        className='tooltipsharepopup'
                        />:''}
                    </span>
                  </li>
                  : '':''}
              {enableSocialFlag == 1 ?
                this.usersSocialChannels.indexOf('linkedin') > -1
                  ? <li className={`channel ${isLiHasLink==false ?'jobrssCheckBox':''}`}>
                    <span
                      data-tip={`${isLiHasLink==false ?'Link is not available.':liusername}`}
                      data-for={'notConnectedSocialChannels'}
                      >
                      <Linkedin
                        className=''
                        name='share-li'
                        text='Linkedin'
                        isLiHasLink={isLiHasLink}
                        message={linkedinMessage}
                        iscompany={0}
                        socialUserId={LinkedinSocialUserId}
                        image_url={liavatar}
                        shareFrom={this.props.shareFrom}
                        postId={this.props.AllData.identity}
                        />

                    </span>
                    {this.state.tooltip?
                    <this.state.tooltip
                      type='info'
                      id='notConnectedSocialChannels'
                      effect='solid'
                      className='tooltipsharepopup'
                      />:''}
                  </li>
                  : '':''}
                {enableSocialFlag == 1 ?fbiscompany == 1 &&
                  this.usersSocialChannels.indexOf('facebook') > -1
                  ? <li className={`channel ${isFbHasLink==false ?'jobrssCheckBox':''}`}>
                    <span
                      data-tip={`${isFbHasLink==false ?'Link is not available.':fbpagename}`}
                      data-for={'notConnectedSocialChannels'}
                      >

                      <Facebook
                        className=''
                        name='share-fb'
                        text='Facebook'
                        isFbHasLink={isFbHasLink}
                        link={
                            shareLink !== '' && shareLink !== undefined
                              ? shareLink
                              : FBshareLink
                          }
                        videourl={0}
                        pageid={fbpageid}
                        accessToken={fbtoken}
                        image_url={fbpageavatar}
                        shareFrom={this.props.shareFrom}
                        successId={this.shareOnSocialMedia.bind(
                            this,
                            'Facebook',
                            fbpageid
                          )}
                        />
                        {this.state.tooltip?
                      <this.state.tooltip
                        type='info'
                        id='notConnectedSocialChannels'
                        effect='solid'
                        className='tooltipsharepopup'
                        />:''}
                    </span>
                  </li>
                  : '':''}
                {enableSocialFlag == 1 ?
                twiscomapny == 1 &&
                  this.usersSocialChannels.indexOf('twitter') > -1
                  ? <li className={`channel ${isTwHasLink==false ?'jobrssCheckBox':''}`}>
                    <span
                      data-tip={`${isTwHasLink==false ?'Link is not available.':twotherusername}`}
                      data-for={'notConnectedSocialChannels'}
                      >
                      <Twitter
                        className=''
                        name='share-tw'
                        text='Twitter'
                        isTwHasLink={isTwHasLink}
                        message={twitterMessage}
                        iscompany={twiscomapny}
                        identity={pageid}
                        image_url={twpageavatar}
                        postId={jobrssid}
                        resourceType={this.props.shareFrom}
                        />
                      {this.state.tooltip?
                      <this.state.tooltip
                        type='info'
                        id='notConnectedSocialChannels'
                        effect='solid'
                        className='tooltipsharepopup'
                        />
                        :''}
                    </span>
                  </li>
                  : '':''}
                {enableSocialFlag == 1 ?
                  liniscompany == 1 &&
                  this.usersSocialChannels.indexOf('linkedin') > -1
                  ? <li className={`channel ${isLiHasLink==false ?'jobrssCheckBox':''}`}>
                    <span
                      data-tip={`${isLiHasLink==false ?'Link is not available.':lipagename}`}
                      data-for={'notConnectedSocialChannels'}
                      >
                      <Linkedin
                        className=''
                        name='share-li'
                        text='Linkedin'
                        isLiHasLink={isLiHasLink}
                        iscompany={liniscompany}
                        pageid={LinkedinSocialPageId}
                        message={linkedinMessage}
                        image_url={lipageavatar}
                        postId={this.props.AllData.identity}
                        />
                      {this.state.tooltip ?
                      <this.state.tooltip
                        type='info'
                        id='notConnectedSocialChannels'
                        effect='solid'
                        className='tooltipsharepopup'
                        />:''}
                    </span>

                  </li>
                  : '':''}
                {enableSocialFlag == 1  && this.usersSocialChannels.indexOf('facebook') < 0 &&
                  this.usersSocialChannels.indexOf('linkedin') < 0 &&
                  this.usersSocialChannels.indexOf('twitter') < 0
                  ? <li className='channel noAccounts'>
                    <p>
                      <span>
                          No social accounts connected, please update in{' '}
                      </span>
                    </p>
                    <div>
                      <Link to='/settings/social' className='btn'>
                          Settings
                        </Link>
                    </div>
                  </li>
                  : ''}
              </ul>

            </div>
          </nav>
          <div className='pointer' />
          <div
            // className='shareoverlay'
            onClick={this.onClose.bind(this, jobrssid)}
          />
        </div>

      </div>
    )
  }
}

function mapStateToProps (state) {
  return {
    profile: state.profile,
    notificationData: state.notification_data,
    posts: state.posts
  }
}
function mapDispatchToProps (dispatch) {
  return {
    socialAccountsActions: bindActionCreators(socialAccountsActions, dispatch),
    postsActions: bindActionCreators(postsActions, dispatch)
  }
}
let connection = connect(mapStateToProps, mapDispatchToProps)
module.exports = connection(SharePopup)
