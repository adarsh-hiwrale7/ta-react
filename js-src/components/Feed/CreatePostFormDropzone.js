import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { Field, reduxForm } from 'redux-form';
import Globals from '../../Globals';
import * as utils from '../../utils/utils'
import notie from 'notie/dist/notie.js'
import { renderDropzoneBox } from '../Helpers/ReduxForm/RenderField'
import * as assetsActions from '../../actions/assets/assetsActions'
import * as generalActions from '../../actions/generalActions'
import * as s3Actions from '../../actions/s3/s3Actions'
import * as s3functions from '../../utils/s3'
import PhotoEditor from './PhotoEditor'
import SliderPopup from '../SliderPopup'
import { FETCH_LIST } from '../../actions/actionTypes';
import ReactDropzone from '../Chunks/ChunkReactDropzone';
import PhotoEditorSDK from '../Chunks/ChunkPhotoEditor'
const required = value => value ? undefined : 'Required';
var FileUploadThumbnail = require('file-upload-thumbnail');
var freeStorage=0
var Current_file=0;
class CreatePostFormDropzone extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      file: [],
      openEditor: false,
      mimeTypeisChecking:false,
      reactDropzone:null,
      photoEditor: null,
    }
  }
/**
* Warning: This lifecycle is currently deprecated, and will be removed in React version 17+
More details here: https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html
*/
componentWillMount() {
  PhotoEditorSDK().then(photoEditor => {
    ReactDropzone().then(dropzone=>{
      this.setState({
        reactDropzone:dropzone,
        photoEditor: photoEditor
      })
    })
  })
}
  componentDidMount() {
    Current_file=0;
    this.props.generalActions.fetchS3Size()
    s3functions.loadAWSsdk()
    this.props.actions.uploadReset()
  }

  closeEditor(){
    this.setState({ openEditor: false,editIndex : 0})
    document.body.classList.remove('overlay')
  }
  changeDropBoxValue(){
    this.checkFileMimetype(this.state.file)
     const initData = {
        files: this.state.file,
       }
      this.props.initialize(initData)
  }

  updateEditedImage(editedImage){
    let editedFile =[];
    this.state.file.map(item =>{
      if(item.name==editedImage[0].name){
        let newFile = new File(
          [editedImage[0]],
          item.name,
          { type: editedImage[0].type }

        );
        newFile.preview = editedImage[0].preview;
        editedFile.push(newFile)
      }else{
        editedFile.push(item);
      }
    })
      this.setState({file:editedFile}, () => {
            this.changeDropBoxValue();
        });
   }
 renderEditor(){
    return (
      <SliderPopup wide className="editorPopup">
        <button
          id='close-popup-uploaded-editor'
          className='btn-default'
          onClick={this.closeEditor.bind(this)}
        >
          <i className='material-icons'>clear</i>
        </button>
        <div id="post-editor-wrapper">
        {this.state.photoEditor?
           <PhotoEditor
                      indexCount={0}
                      photoEditor={this.state.photoEditor}
                       assetsActions={this.props.assetsActions}
                       imageData = {this.state.file[this.state.editIndex]}
                       imageName = {this.state.file[this.state.editIndex].name}
                       mediaType = {this.state.file[this.state.editIndex].type}
                       closeEditorPopup = {this.closeEditor.bind(this)}
                       updateEditedImage = {this.updateEditedImage.bind(this)}
                      />:''}
           </div>
      </SliderPopup>
    )

  }
/**
 * @author disha
 * to get thumbnail url from file object and display image into div of renderfield
 * @param {*} files
 */
getThumbnailUrl(files,count){
  var props = this.props;
  
  if(files && !files.length > 0){
    return ;
    // no files no need to generate thumbnail
  }
  
  var totalFiles =  Object.keys(files).length-1;
  var totalAssetsLimit = totalFiles <  9-this.props.totalAssets?totalFiles:9-this.props.totalAssets;
  if(totalAssetsLimit <  totalFiles){
    notie.alert('error','You can select a maximum of 9 media assets.',3);
  }

  
  setTimeout(function(){
    new FileUploadThumbnail({
      maxWidth: 500,
      maxHeight: 400,
      file: files[count],
      onSuccess: function(src){
        var img = document.createElement('img');
          img.src = src;
         setTimeout(function(){
          if(document.getElementById(`dropPreviewIconInner-${files[count].preview}`)){
             var imgDataTosend={
              thumbnailSrc:src,
              filePreview:files[count].preview,
            }
            Object.assign(files[count], imgDataTosend)
            document.getElementById(`dropPreviewIconInner-${files[count].preview}`).appendChild(img)
          }
           },300)
      }
    }).createThumbnail();
     var video = document.createElement('video');
      video.preload = 'metadata';
      video.onloadedmetadata = function() {
        window.URL.revokeObjectURL(video.src);
        var duration = video.duration;
        files[count].duration = duration;
      }
      video.src = URL.createObjectURL(files[count]);
  },400)
    count = Current_file++;
    if(count<Object.keys(files).length-2)
    {
      this.getThumbnailUrl(files, count)
    }
}
  openEditor(index){
      this.setState({
        openEditor: true,
        editIndex : index
      })
} 

readFileAsync(file,fields,k){
  let me  = this;

  let fileType=''
    let fileName=''
    let mimetype=''
    let newFileType=''
    let newFileName=''
    let fileinnerdetail={}
    let flag=0
    let invalidType=0
    let hex=''
  // const filereader = new FileReader()
  // const blob = file.slice(0, 4)
  // filereader.readAsArrayBuffer(blob)
  return new Promise((resolve, reject) => {
    let reader = new FileReader();

    const blob = file.slice(0, 4)
    reader.readAsArrayBuffer(blob);

    reader.onloadend = function (evt) {
    if (evt.target.readyState === FileReader.DONE) {
      var u = new Uint8Array(this.result),
      a = new Array(u.length),
      i = u.length;
      while (i--){ // map to hex
            a[i] = (u[i] < 16 ? '0' : '') + u[i].toString(16);
      }
        u = null; // free memory
        hex = a.toString().replace (/,/g, "").toUpperCase(); //convert into hex code

      //call function when last data of field load
        (function () {
          // body of the function
            mimetype = Globals.getMimetype(hex)
            newFileType = mimetype.split('/')
            var addMimeObj={
              mimetype:mimetype
            }
            Object.assign(fields[k],addMimeObj)
            if (Globals.alllowedFileType.indexOf(newFileType[0]) == -1) {
              fields.splice(fields[k], 1)
              me.setState({ file: null })
              me.setState({ mimeTypeisChecking: false })
              notie.alert('error',`This file type is not allowed`,5)
              resolve(fields);
            } else {
              if (file.type !== '') {
                if (Globals.alllowedFileType=='application' && Globals.allowedApplictionType.indexOf(newFileType[1]) == -1) {
                  fields.splice(fields[k], 1)
                  me.setState({ file: null })
                  me.setState({ mimeTypeisChecking: false })
                  notie.alert('error',`This file type is not allowed.`,5)
                  resolve(fields);
                }else{
                me.setState({ file: fields })
                me.setState({ mimeTypeisChecking: false })
                resolve(fields);
                }
              } else {
                notie.alert('error', `File type is required.`, 5)
                fields.splice(fields[k], 1)
                me.setState({ file: null })
                me.setState({ mimeTypeisChecking: false })
                resolve(fields);
              }
            }
        }());
    }
  }
    reader.onerror = reject;
    
  })
}
  /**
   * check file mime type
   * @author disha
   * @param {*} fields selected file
   */
  async checkFileMimetype(fields){
    // var file=fields[0]
    var me=this
    me.setState({
      mimeTypeisChecking:true
    })

    for(var k=0;k<fields.length;k++){
      var file = fields[k];
      let contentBuffer = await this.readFileAsync(file,fields,k);
        }
          this.props.actions.uploadedAssetInBuffer(fields)
  }
    renderDropzone() {
    let limitAssets = 9-this.props.totalAssets;
    //convert mb size into bytes
    freeStorage = Object.keys(this.props.general.s3SizeData).length>0 ?Object.keys(this.props.general.s3SizeData.storage).length>0 ? this.props.general.s3SizeData.storage.freeStorage * Math.pow(1024,2) : 0:''
    const {handleSubmit, pristine, submitting, tags} = this.props;
    var me = this;
    let { uploadProgress, uploadDone, uploadStarted } = this.props.aws;

    if(uploadDone) {
      var dzArea = document.querySelector(".dropzoneUpload");
      if(typeof(dzArea)!=="undefined" && dzArea!=null) {
        dzArea.style.pointerEvents = "none";
      }

    }

    var dropzoneStyle = {
      borderColor: '#666',
      borderRadius: '5px',
      borderStyle: 'dashed',
      borderWidth: '1px',
      height: 'auto',
      minHeight: '120px',
      lineHeight: '100px',
      textAlign: 'center',
      padding: '10px',
      width: '100%',
    }
    return (
      <form>
        <div className="section">
          <div className="step1 active">
             <div className="form-row">
              <Field
                label="Media"
                name="files"
                fromCreatePost = {true}
                isUploadingStart={this.props.isUploadingStart}
                freeStorage={freeStorage}
                title={uploadDone ? `Please wait...` : `Drag and drop`}
                multiple={true}
                limit={limitAssets}
                uploadProgress={uploadProgress}
                uploadDone ={uploadDone}
                style={dropzoneStyle}
                component={renderDropzoneBox}
                validate={[ required ]}
                Dropzone={this.state.reactDropzone}
                onChange={(file)=>{this.getThumbnailUrl(file,0)}}
                handleGifCount={()=>{this.props.handleGifCount("decrement")}}
                handleDocCount={()=>{this.props.handleDocCount("decrement")}}
                submit={(file)=> {
                  limitAssets= file.length < limitAssets ? file.length : limitAssets
                  var uploadedFile =[];
                  for(var i =0;i<limitAssets;i++){
                    if(file[i].type.includes("gif")){
                      this.props.handleGifCount("increment");
                    }
                    if(file[i].type.includes("application")){
                      this.props.handleDocCount("increment");
                    }

                    if(file[i].type == 'video/mp4'){
                      var addMimeObj={
                        mimetype:'video/mp4'
                      }
                      Object.assign(file[i],addMimeObj)
                      uploadedFile.push(file[i]);
                      this.state.file.push(file[i]);
                    }else{
                      uploadedFile.push(file[i]);
                      this.state.file.push(file[i]);
                    }
                  }
                  this.checkFileMimetype(uploadedFile);
                }}
                   />
            </div>
         <div>
              {
                this.state.file!==null?
                this.state.file.length>0 ?
                 this.state.file[0].type.match("image") !==null && this.state.file[0].type.match("image").length > 0
                ?
                <button className="btn btn-primary feed-btn hide"  type="button"
                  onClick={handleSubmit(values =>
                    this.props.onSubmit({
                      ...values,
                      crop: true
                    }))} >
                  Crop
                  </button>:'':''
                :
                null
              }
              <div className='dropzoneBtn'>
              <button className="btn btn-primary feed-btn" id="btn-upload-post" type="button" onClick={handleSubmit(values =>
                this.props.onSubmit({
                  ...values,
                  crop: false
                }))} >
              Upload</button>
              {/* edit button is static */}
            {
                this.state.file!==null?
                this.state.file.length>0 ?
                this.state.file.map((fileData,index) => {
                  return(
                    fileData.type.match("image") !==null && fileData.type.match("image").length > 0 && fileData.type!=="image/gif"?
                    <button id={`editAssetBtn${index}`} className="btn btn-primary feed-btn" type="button" onClick={this.openEditor.bind(this,index)}>Edit</button>:''
                  )}
                ):'': null
            }
            </div>
            </div>



          </div>
        </div>
      </form>
    )
  }

  render() {
    var assetCreated = this.props.assets;
    return (
        <div className="section">

          {this.state.openEditor === true ? this.renderEditor() : ''}

          {//to prevent file to display if it is not valid and this state will call when mime type propcess is complete
             this.state.mimeTypeisChecking==false? this.renderDropzone():''  }
        </div>
    )
  }

}


function mapStateToProps(state) {
  return {
    aws: state.aws,
    general: state.general,
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(
      Object.assign({}, assetsActions, s3Actions),
      dispatch
    ),
    assetsActions: bindActionCreators(assetsActions, dispatch),
    generalActions: bindActionCreators(generalActions, dispatch),
  }
}
let connection = connect(mapStateToProps, mapDispatchToProps);

var reduxConnectedComponent = connection(CreatePostFormDropzone);

export default reduxForm({
  form: `postFormDropzone`,  // a unique identifier for this form
})(reduxConnectedComponent)
