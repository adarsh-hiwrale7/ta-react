import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import * as CalendarActions from '../../actions/feed/CalendarActions'
import * as postsActions from '../../actions/feed/postsActions'
import * as utils from '../../utils/utils'

import CalendarButtons from '../Header/CalendarButtons';
import FeedEvents from './FeedEvents'
import FilterWrapper from '../FilterWrapper';
import localizer from 'react-big-calendar/lib/localizers/globalize'
import Post from '../Feed/Post'

import BigCalendar from 'react-big-calendar'
import Dnd from '../calendarViews/calendarDragAndDrop'
import Globals from '../../Globals';
import notie from 'notie/dist/notie.js'
import CollapsibleFilter from '../CollapsibleFilter'
import Slider from '../Chunks/ChunkReactSlick'
var firstDateTimestampParent, lastDateTimestampParent;
var rescheduled_post_id;
var rescheduled_post_date;
var _current_page;
var calnder_filter = ['facebook', 'twitter', 'linkedin'];

class FeedCalendar extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      currFeed: 'default',
      cal_view: 'month',
      limit: 10,
      currFeedPosts:'scheduled',
      Slider: null,
    }
    this._handleCloseEvent = this._handleCloseEvent.bind(this);
    this.move_event = this.move_event.bind(this);
    this.fetchTimestamps = this.fetchTimestamps.bind(this);
  }
  fetchTimestamps(firstDateTimestamp, lastDateTimestamp) {
    firstDateTimestampParent = firstDateTimestamp
    lastDateTimestampParent = lastDateTimestamp
    var path = utils
      .removeTrailingSlash(this.props.location.pathname)
      .split('/');
    this.fetchPosts(path[2], path[3], 1, firstDateTimestamp, lastDateTimestamp)

  }
  componentWillMount(){
    Slider().then(slider => {
      this.setState({Slider: slider })
    })
  }
  componentDidMount() {
    if(this.props.filterfeedId!==null&&this.props.filterfeedId!==''&&this.props.filterfeedId!==undefined){
    this.setState({
      selectedFeedStatus:this.props.filterfeedId
    })
    document.getElementById('FeedStatus').value= this.props.filterfeedId
    }
  }
  closepostDetailPopup() {
    this.setState({
      show_calendar_feed_post_state: false
    })
  }
  callCalPostDetails(event, e) {
    var week = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']
    var calendar_popup = {
      position: {
        vertical: 'top',
        horizontal: 'left',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0
      },
      grid:
        {
          row_number: 0,
          column_number: 0
        },
      event
    }
    var rbc_month_row = document.querySelectorAll('.rbc-month-row')[0];
    var rbc_day_bg = document.querySelectorAll('.rbc-day-bg')[0];
    var selected_row = utils.closest(e.target, 'rbc-month-row');
    var selected_row_cells;
    if (selected_row) {
      calendar_popup.grid.row_number = utils.index(selected_row)
      selected_row_cells = selected_row.querySelectorAll('.rbc-date-cell');
    }
    var selected_cell = document.querySelectorAll('.selected_cell')[0];


    if (this.state.cal_view == 'month') {
      var calendar_table = {
        border: 2,
        margin_bottom: parseInt(window.getComputedStyle(rbc_month_row)['margin-bottom']),
      }
      var cell = {
        height: rbc_day_bg.offsetHeight + calendar_table.border + calendar_table.margin_bottom,
        width: rbc_day_bg.getBoundingClientRect().width
      }
      var selected_date_var = new Date(event.start);
      var selected_date = {
        date: selected_date_var.getDate(),
        month: selected_date_var.getMonth() + 1
      }

      if (utils.closest(e.target, 'rbc-overlay')) {
        calendar_popup.grid.column_number = parseInt(selected_cell.getAttribute('data-column-number'));
        calendar_popup.grid.row_number = parseInt(selected_cell.getAttribute('data-row-number')) + 1;
      }
      else {
        Array.prototype.forEach.call(selected_row_cells, function (cell_el, cell_index) {
          if (cell_el.textContent == selected_date.date) {
            calendar_popup.grid.column_number = cell_index;
          }
        });
      }
      calendar_popup.position.left = (calendar_popup.grid.column_number + 1) * cell.width;
      calendar_popup.position.top = (calendar_popup.grid.row_number) * cell.height;
      if (calendar_popup.grid.column_number > 3) {
        calendar_popup.position.horizontal = 'right';
        calendar_popup.position.right = cell.width * (7 - calendar_popup.grid.column_number)
      }
      if (calendar_popup.grid.row_number > 3) {
        calendar_popup.position.vertical = 'bottom';
        calendar_popup.position.bottom = cell.height * (5 - calendar_popup.grid.row_number);
      }
    }
    // Week view 
    else {
      var calendar_table = {
        border: 2,
        timeslot_width: document.querySelectorAll('.rbc-time-column')[0].offsetWidth,
        rbc_header: document.querySelectorAll('.rbc-header')[0].offsetHeight
      }
      var cell = {
        height: document.querySelectorAll('.rbc-timeslot-group')[0].offsetHeight,
        width: document.querySelectorAll('.rbc-header')[0].getBoundingClientRect().width
      }

      selected_date = {
        date: 1,
        time: null,
        full_date: new Date(event.start)
      }

      var selected_day = week[selected_date.full_date.getDay()]
      var formated_date = this.formatTime(selected_date.full_date.getDate());
      var formated_month = this.formatTime(selected_date.full_date.getMonth() + 1);

      selected_date.date = `${selected_day} ${formated_date}/${formated_month}`;
      var hours = selected_date.full_date.getHours();
      var minutes = 0;
      var formated_minutes = this.formatTime(minutes);
      var meridiem = 'PM';
      if (hours == 0) {
        hours = 12;
        meridiem = 'AM';
      }
      else if (hours < 12) {
        meridiem = 'AM';
      }
      else if (hours > 12) {
        hours = hours - 12;
      }
      selected_date.time = hours + ':' + formated_minutes + ' ' + meridiem;


      var rbc_timeslot_group = document.querySelectorAll('.rbc-timeslot-group');
      var rbc_header = document.querySelectorAll('#weekHeader .rbc-header');

      Array.prototype.forEach.call(rbc_timeslot_group, function (timeslot_el, timeslot_index) {
        if (timeslot_el.textContent == selected_date.time) {
          calendar_popup.grid.row_number = timeslot_index;
        }
      });

      if (calendar_popup.grid.row_number > 15) {
        calendar_popup.position.vertical = 'bottom';
        calendar_popup.position.bottom = cell.height * (23 - calendar_popup.grid.row_number);
      }
      Array.prototype.forEach.call(rbc_header, function (rbc_header_el, rbc_header_index) {
        if (rbc_header_el.textContent == selected_date.date) {
          calendar_popup.grid.column_number = rbc_header_index + 1;
        }
      })
      if (calendar_popup.grid.column_number > 4) {
        calendar_popup.position.horizontal = 'right';
        calendar_popup.position.right = cell.width * (8 - calendar_popup.grid.column_number)
      }
      calendar_popup.position.left = (calendar_popup.grid.column_number) * cell.width + calendar_table.timeslot_width;
      calendar_popup.position.top = (calendar_popup.grid.row_number) * cell.height + calendar_table.rbc_header;
    }
    this.setState({
      show_calendar_feed_post_state: true,
      calendar_feed_detail: {
        calendar_popup
      }
    });
  }
  componentWillReceiveProps() {
    this.initilizeClassToCell();
    var path = utils
      .removeTrailingSlash(this.props.location.pathname)
      .split('/')
    if (typeof path[2] === 'undefined' || path[2] == '' || typeof path[3] === 'undefined' || path[3] == '') {
        browserHistory.push('/feed/default/live')
      }
    else {
        this.setState({ currFeed: path[2], currFeedPosts: path[3], currPage: 1 })
      //  this.fetchPosts(path[2], path[4], 1)
      }
    }
  closepostDetailPopup() {
    this.setState({
      show_calendar_feed_post_state: false
    })
  }
  
  changeSocialFilter(paraValue,isAllSocial,isFacebook,isTwitter,isLinkedin) { 
    const value = paraValue;
    this.setState({
      [name]: value,
      calendar_loader: true,
      isAllSocial: false,
          isFacebook,
          isTwitter,
          isLinkedin
    });
    if (isAllSocial) {
      calnder_filter = ['facebook', 'twitter', 'linkedin'];
    }
    else {
      calnder_filter = [];
      if (isFacebook) {
        calnder_filter.push('facebook');
      }
      if (isTwitter) {
        calnder_filter.push('twitter');
      }
      if (isLinkedin) {
        calnder_filter.push('linkedin');
      }
  
    }
    var path = utils
      .removeTrailingSlash(this.props.location.pathname)
      .split('/')
    this.fetchPosts(path[2], path[4], 1,firstDateTimestampParent, lastDateTimestampParent)
  }
  fetchPosts(type, postTypes, currPage, firstDateTimestamp, lastDateTimestamp) {
    _current_page = currPage;
    if(type=='my'){
      if(this.state.selectedFeedStatus!==null && this.state.selectedFeedStatus!=='' && this.state.selectedFeedStatus!==undefined){
        this.props.CalendarActions.fetchCalendarScheduled(_current_page, this.state.limit, null, this.props.moment, firstDateTimestampParent, lastDateTimestampParent,this.state.selectedFeedStatus)
      }else{
        this.props.CalendarActions.fetchCalendarScheduled(
          _current_page, this.state.limit, null, this.props.moment, firstDateTimestamp, lastDateTimestamp
        )
      }
    }
  }
  formatTime(time) {
    var myNumber = time;
    return (("0" + myNumber).slice(-2));
  }
  _handleCloseEvent(e) {
    if (document.getElementById('pop-up-tooltip-wrapper')) {
      //hide notification popup on outside click
      if (!document.getElementById('pop-up-tooltip-wrapper').contains(e.target)) {
        if (this.state.overlay_flag) {
          document
            .getElementById('pop-up-tooltip-wrapper')
            .className = 'hide';
          this.setState({ overlay_flag: false })
        }
      }
    }
    if (utils.closest(e.target, 'feed-post-item') == null) {
      //hide calender post popup on outside click
      if (document.querySelectorAll('.bigCalendar-calendarview #close-popup').length != 0) {
        document.querySelectorAll('.bigCalendar-calendarview #close-popup')[0].click()
      }
    }
  }
  initilizeClassToCell() {
    var month_rows = document.querySelectorAll('.rbc-month-row');
    Array.prototype.forEach.call(month_rows, function (month_el, month_index) {
      var date_cols = month_el.querySelectorAll('.rbc-day-bg');
      Array.prototype.forEach.call(date_cols, function (date_el, date_index) {
        date_el.setAttribute('data-row-number', month_index);
        date_el.setAttribute('data-column-number', date_index);
      });
    });
  }
  move_event({ event, start, end }) {
    //if calendar view is month assign current time of event to new schedule post when it will be dragged
    if(this.state.cal_view == 'month'){
        var oldstartDate = new Date (event.start);
        var oldStartHours = oldstartDate.getHours();
        var oldStartMinutes = oldstartDate.getMinutes();
        var oldStartSeconds = oldstartDate.getSeconds();

        var newstartDate = new Date (start);
        var updateStartDate = newstartDate.setHours(oldStartHours);
        updateStartDate = newstartDate.setMinutes(oldStartMinutes);
        updateStartDate = newstartDate.setSeconds(oldStartSeconds);
        updateStartDate = new Date(newstartDate);
        start = updateStartDate;
        end = updateStartDate;
    }
    var path = utils
      .removeTrailingSlash(this.props.location.pathname)
      .split('/')
    var time_message;
    const now = new Date();
    var moment = this.props.moment;

    let tomorrow, newSchedule;
    tomorrow = moment(start).add(0, 'days');
    newSchedule = String(start.getTime() / 1000);
    rescheduled_post_date = start;
    var current_date = moment.tz(now, Globals.SERVER_TZ).format('X');
    let selected_date = moment.tz(start, Globals.SERVER_TZ).format('X');
    let selected_date_second = moment.tz(start, Globals.SERVER_TZ).format('x');
    var current_date_second = moment.tz(now, Globals.SERVER_TZ).format('x');
  // to reduce API call if the new schedule is same as old schedule
    if (newSchedule >= current_date && newSchedule !== event.feed_post.scheduled_at.toString()) {
      if (selected_date_second > current_date_second) {
        const events_post = this.props.feedCalendar.feedPosts;
        rescheduled_post_id = event.feed_post.publisher_identity;
        const idx = events_post.indexOf(event);
        const updatedEvent = { ...event, start, end };
        const nextEvents = [...events_post];
        nextEvents.splice(idx, 1, updatedEvent);
        this.setState({
          events_post: nextEvents,
          event_change: true
        })



        this.props
          .postsActions
          .postUpdateInitiate();
        if (this.props.posts.updation.loading) {
          this.setState({
            update_post_loader: true
          })
        }
        else {
          this.setState({
            update_post_loader: false
          })
        }

        let fieldValues =
          {
            publisher_identity: event.feed_post.publisher_identity,
            scheduled_at: newSchedule
          }


        this.props.postsActions.postUpdate(fieldValues,true,this.props.moment,firstDateTimestampParent,lastDateTimestampParent)
        // this.fetchTimestamps(firstDateTimestampParent,lastDateTimestampParent)
        if(document.querySelectorAll('.initializeStyle').length>0){
          document.querySelectorAll('.initializeStyle')[0].click();
        }
      }
      else {
        if (this.state.cal_view == 'month') {
          time_message = 'You cannot schedule a post for time that has passed. Please go to week view for more.'
        }
        else {
          time_message = 'You cannot schedule a post for time that has passed.'
        }
        notie.alert(
          'error',
          time_message,
          5
        )
      }
    }
    else if(newSchedule < current_date){
      notie.alert(
        'error',
        'You cannot schedule a post for date that has passed.',
        5
      )
    }
  }
  setView(cal_view){
    this.setState({
      cal_view
    })
  }
  show_calendar_feed_post(data) {
    var data = this.state.calendar_feed_detail.calendar_popup
    var row_number = data.grid.row_number;
    let style =
      {
        left: data.position.left,
        right: data.position.right,
        top: data.position.top,
        bottom: data.position.bottom
      }
    return (
      <div className={`calendar-detail-popup popup_row_${row_number} popup_row_${data.position.vertical} popup_column_${data.position.horizontal}`} style={style}>
        <button
          id='close-popup'
          className='btn-default'
          onClick={this
            .closepostDetailPopup
            .bind(this)}>
          <i className='material-icons'>clear</i>
        </button>
        <Post
          Slider={this.state.Slider} 
          cal_view={true}
          feed={this.state.currFeed}
          feedType={this.state.currFeedPosts}
          locationInfo={this.props.location}
          details={data.event.feed_post}
          customScrollbar={true}
          cal_Date={data.event.end}
          handleCloseEvent={this
            ._handleCloseEvent
            .bind()}
        />
      </div>
    )
  }

  selectedFeedStatus(e){
    this.setState({
      selectedFeedStatus:e.target.value
    })
  }

  ApplyFeedFilter(e){
    // propsChanged=true //to recall setCurrentFeed to set state of selected feed type from filter
    this.props.CalendarActions.fetchCalendarScheduled(1, this.state.limit, null, this.props.moment, firstDateTimestampParent, lastDateTimestampParent,this.state.selectedFeedStatus,
    )
    // this.props.filterMyFeedData(path[2],path[3],this.state.selectedFeedStatus,1,10,false,false,null,null,false);
  }

  render() {

    var flag;
    var feedFilter = this.props.feedList;
    var path = utils
    .removeTrailingSlash(this.props.location.pathname)
    .split('/');
    let me = this;
    let events = typeof this.props.feedCalendar.feedPosts !== 'undefined'
      ? this.props.feedCalendar.feedPosts
      : []
    events.forEach(function (event_item) {
      if (event_item.feed_post.publisher_identity == rescheduled_post_id) {
        event_item.start = rescheduled_post_date
        event_item.end = rescheduled_post_date
      }
      if (event_item.start.getHours() == 0) {
        if (event_item.start.getMinutes() == 0) {
          var default_time = '00:00:01 GMT+0530 (IST)';
          var date_only = me.props.moment(event_item.start).format('ddd MMM DD YYYY');
          var new_date = new Date(`${date_only} ${default_time}`);
          event_item.start = new_date;
          event_item.end = new_date;
        }

      }
      
    })
    if (this.props.moment) {
          BigCalendar.setLocalizer(BigCalendar.momentLocalizer(this.props.moment))
        }
    return (
      <div className='page feed-container page with-filters feed-calendar-page'>
        <FilterWrapper className="calendar-dropdown">
            <div className="clearfix">
              <CalendarButtons
                changeHeaderView={true}
                pageView={this.state.view}
                feedUrl={this.props.feedUrl}
                calendarUrl={this.props.calendarUrl}
                currFeedPosts={this.state.currFeedPosts}
                currFeed={this.state.currFeed}
                monthWeekview = {true}
                changeSocialFilter ={this.changeSocialFilter.bind(this)}
              />
            </div>
          </FilterWrapper>
        <div class='content'>
           <div>
                {
                  path[2] == "my" ?
                    <CollapsibleFilter>
                      <div>
                        <div className='filter-row row'>
                          <div className='col-one-of-four'>
                            <div className='curated-filter-inner'>
                              <div className='form-row'>
                                <label> Select Feed</label>
                                <div className='select-wrapper'>
                                  <select onChange={this.selectedFeedStatus.bind(this)} id='FeedStatus' name='FeedStatus'>

                                    <option value=''>Select Channel</option>
                                    {
                                      Object.keys(feedFilter).length !== 0 ?
                                        feedFilter.map((optionData, i) => {
                                          return (
                                            <option value={optionData.identity}>{optionData.name}</option>
                                          )
                                        })
                                        : ''
                                    }
                                  </select>

                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className='col-three-of-four'>
                          <div className='filter-button-wrapper clearfix '>
                            {
                              document.getElementById('FeedStatus') ?
                                document.getElementById('FeedStatus').value !== '' ?
                                  flag = false
                                  : flag = true
                                : ''
                            }
                            <a
                              disabled={flag == true ? 'disabled' : ''}
                              onClick={this.ApplyFeedFilter.bind(this)}
                              className='btn btn-theme'>Apply filter
                        </a>
                          </div>
                        </div>
                      </div>
                    </CollapsibleFilter>
                    : ''
                }
              </div>
          <div className='widget'>
            <div className={`bigCalendar-calendarview ${this.state.cal_view}-view calendar-${this.state.currFeed}`} ref='big_calendar'>
              {this.state.show_calendar_feed_post_state ? this.show_calendar_feed_post() : ''}
              {this.props.moment ?
                <Dnd
                  location={this.props.location}
                  move_event={this.move_event}
                  callCalPostDetails={this.callCalPostDetails.bind(this)}
                  moment={this.props.moment}
                  fetchTimestamps={this.fetchTimestamps}
                  fetchPosts={this.fetchPosts.bind(this)}
                  setView = {this.setView.bind(this)}
                  components={{
                    event: FeedEvents
                  }}
                  events={
                    typeof this.props.moment !== 'undefined' &&
                      this.props.moment != null
                      ? events
                      : []
                  }
                />
                :
                ''}
            </div>
          </div>{/* --end widget-- */}
        </div>{/* --end page-- */}
      </div>
    )
  }
}


function mapStateToProps(state) {
  return {
    // feed: state.feed,
    // users: state.users,
    // department: state.departments.department,
    // hashAssets: state.assets.hashAssets,
    // disblePopup: state.posts.creation.disblePopup,
    // notificationData: state.notification_data,
     posts: state.posts,
    feedCalendar: state.feedCalendar
  }
}

function mapDispatchToProps(dispatch) {
  return {
    //feedActions: bindActionCreators(feedActions, dispatch),
     postsActions: bindActionCreators(postsActions, dispatch),
    // userActions: bindActionCreators(userActions, dispatch),
    // departmentActions: bindActionCreators(departmentActions, dispatch),
    // assetsActions: bindActionCreators(assetsActions, dispatch),
    // socialAccountsActions: bindActionCreators(socialAccountsActions, dispatch),
    // notificationActions: bindActionCreators(notificationActions, dispatch),
    CalendarActions: bindActionCreators(CalendarActions, dispatch)
  }
}

module.exports = connect(mapStateToProps, mapDispatchToProps)(FeedCalendar)