import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Field, reduxForm } from 'redux-form'
import Facebook from '../../Social/Facebook'
import Globals from '../../../Globals';
import Linkedin from '../../Social/Linkedin'
import moment from '../../Chunks/ChunkMoment';
import reactTooltip from '../../Chunks/ChunkReactTooltip';
import Twitter from '../../Social/Twitter'
import * as utils from '../../../utils/utils'
import * as feedActions from '../../../actions/feed/feedActions';
import * as postsActions from '../../../actions/feed/postsActions'
import * as socialAccountsActions from '../../../actions/socialAccountsActions'
var CommentString = '';
var externalDesc = '';
var PostDataOnChannel = false
var facebookaccount = false
var twitteraccount = false
var linkedinaccount = false
var restrictShareApiCall=false
var a = ''
var resourceName=''
export class CuratedSharePopup extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      accname: '',
      shareLink: '',
      sharefrompopup: false,
      latitude: 0,
      longitude: 0,
      Moment: null,
      tooltip: null,
    }
    this.showPosition = this
      .showPosition
      .bind(this);
    if (navigator.geolocation) {
      navigator
        .geolocation
        .getCurrentPosition(this.showPosition);
    }
  }
/**
 * @author disha
 * call api after share is successfully done
 * @param {*} accountName
 * @param {*} id id of success post
 */
  shareOnSocialMedia(accountName,socialAllId){
    var objToSend={
      socialId:socialAllId.id,
      Localid:this.props.alldata.identity,
      social_platform:accountName,
      resource:'job',
      userId: this.props.profile.profile.identity
    }
    var ObjToAdd={
      'social_account_id':socialAllId.socialAccId
    }
    accountName=='Facebook'?
      Object.assign(objToSend,ObjToAdd):''
    if(restrictShareApiCall==false){
      //if share is from RSS then to restrict call of share social api
      this.props.socialAccountsActions.shareOnSocialMedia(objToSend)
    }
    externalDesc = '';
    CommentString = '';
  }

  componentWillMount() {

      moment().then((moment) => {
        reactTooltip().then(reactTooltip => {
          this.setState({Moment: moment,tooltip: reactTooltip })
        })
      }).catch(err => {
        throw err;
      });

  }

  componentWillReceiveProps () {
    this.setState({
      sharefrompopup: this.props.directShare
    })
  }
  /**
   * @author disha
   * when click on any social channel this function will call whether channel is in popup or not
   * @param {*} acc account name
   * @param {*} e
   */
  selectacc (alldata, acc, e) {
    CommentString=''
      if(this.props.comment == undefined ||  this.props.comment == null ){
        CommentString = this.props.alldata.title
      }
      else{
        for(var i=0;i<Object.keys(this.props.comment).length-1;i++){
          CommentString = CommentString + this.props.comment[i];
        }
      }
  this
  .createTagList(utils.toUnicodetwo(CommentString))
  .then((desc) => {
    externalDesc = desc;
    PostDataOnChannel = true
    var newLocation = utils
      .removeTrailingSlash(this.props.location.pathname)
      .split('/')
    if (newLocation[2] == 'rss') {
        //if page is rss
      if (this.props.directShare == true) {
          //if page is rss and data is shared directly
        this.setState({
          accname: acc,
          sharefrompopup: false,
          shareLink: alldata.permalink
        })
      } else {
        this.setState({
          accname: acc,
          sharefrompopup: true,
          shareLink: alldata.permalink
        })
      }
    } else {
        //if page is job
        if (this.props.directShare == true) {
          //share Post direct without comment and opening popup
          this.setState({
            sharefrompopup: false,
          })
        } else {
            this.setState({
              sharefrompopup: true,
            })
        }
        if (acc == 'fb') {
          // if account name is fb then it will set state and its link (workable/logicMalon)
          this.setState({
            accname: acc,
            shareLink: alldata.facebook_link!==null?alldata.facebook_link:alldata.workable_link
          })
        } else if (acc == 'tw') {
          this.setState({
            accname: acc,
            shareLink: alldata.twitter_link!==null?alldata.twitter_link:alldata.workable_link
          })
        } else if (acc == 'li') {
          this.setState({
            accname: acc,
            shareLink: alldata.linkedin_link!==null?alldata.linkedin_link:alldata.workable_link
          })
        }
    }
  })
  }

  showPosition(position) {
    let me = this;
    me.setState({latitude: position.coords.latitude, longitude: position.coords.longitude})

  }

  createTagList(str) {
    var me = this;
    var rx1 = /@\[([^\]]+)]\(([^)]+)\)/g;
    let res = str.match(rx1);
    var userTag = [];

    return new Promise(function (resolve, reject) {
      if (res) {
        res
          .forEach(function (tags) {
            /** Get user information */
            var tagsArray = tags.split("@[");
            var nameArray = tagsArray[1].split("]");
            var name = nameArray[0];
            var dept = nameArray[1]
              .substring(0, nameArray[1].length - 1)
              .split(":")[1]
              .split('_');
            var type = "tags";
            if (nameArray[1].substring(0, nameArray[1].length - 1).split(":")[0] !== "(tags") {
              type = dept[1]=='company'?'company':
              dept[1]=='dept'?
                 "department"
                : "user";

              let tagsUserObject = {
                tagid: dept[0],
                tagName: name.replace(/ /g, ''),
                type: type
              };
              userTag.push(tagsUserObject)
            }

            /** description replace */
            if (type === "tags") {
              str = str.replace(tags, "#" + name.replace(/ /g, ''));
            } else {
              str = str.replace(tags, "@" + name.replace(/ /g, ''));
            }

          })
        me.setState({
          userTag: userTag
        })
      }
      resolve(str);
    })
  }

  shareCuratedInterlly(){
    var postDetails, linkShare;
    let time = this
        .state
        .Moment()
        .format("HH:mm");
    let date = this
        .state
        .Moment()
        .format("DD/MM/YYYY");
    let concatenatedDateTime = date + " " + time;
        let timeStamp = this
          .state
          .Moment(concatenatedDateTime, 'DD/MM/YYYY HH:mm')
          .format("X");
          if(this.props.comment == undefined ||  this.props.comment == null){
            CommentString = this.props.alldata.title
          }
          else{
            for(var i=0;i<Object.keys(this.props.comment).length-1;i++){
              CommentString = CommentString + this.props.comment[i];
            }
          }
        this
        .createTagList(utils.toUnicodetwo(CommentString))
        .then((desc) => {
                let time = this
                .state
                .Moment()
                .format("HH:mm");
            let date = this
                .state
                .Moment()
                .format("DD/MM/YYYY");
            let concatenatedDateTime = date + " " + time;
                let timeStamp = this
                  .state
                  .Moment(concatenatedDateTime, 'DD/MM/YYYY HH:mm')
                  .format("X");

            let schedule = [{
            scheduled_at    : timeStamp,
            social_account_identity:0

            }]
            postDetails = desc;
          if(this.props.location.pathname == "/feed/rss"){
            linkShare = this.props.alldata.permalink
          }
          else if(this.props.location.pathname=="/feed/jobs"){
               if(this.props.alldata.workable_link!==null){
                  linkShare = this.props.alldata.workable_link
                }else{
                  if(this.props.alldata.facebook_link!==null){
                    linkShare = this.props.alldata.facebook_link
                  }
                  else if(this.props.alldata.twitter_link!==null){
                    linkShare = this.props.alldata.twitter_link
                  }
                  else if(this.props.alldata.linkedin_link!==null){
                    linkShare = this.props.alldata.linkedin_link
                  }
                }
          }

            let fieldValues = {
              detail:postDetails,
              type: 'Internal',
              feed_identity: 'rpPDp',
              isCampaign:false,
              latitude: this.state.latitude,
              longitude: this.state.longitude,
              media:[],
              feedtype:"default",
              feedStatus:"live",
              schedule: schedule,
              isScheduledPost:false,
              user_tag:this.state.userTag,
              link_preview:[
                {link: linkShare}]

            }
            this
            .props
            .postsActions
            .postCreate(fieldValues)

            CommentString = '';
            postDetails='';
          });


  }
  render () {
    var isFacebookHasLink=false
    var isTwitterHasLink=false
    var isLinkedinHasLink=false
    var fbuserid=this.props.fbuserid
    var liuserid=this.props.liuserid
    if(this.props.location.pathname=="/feed/rss"){
      resourceName='rss'
      restrictShareApiCall=true
      if(this.props.alldata.permalink !==null){
        // if permalink is not null
        isFacebookHasLink=true
        isTwitterHasLink=true
        isLinkedinHasLink=true
      }
    }else{
      resourceName='job'
      if(this.props.alldata.job_source=="workable"){
        if(this.props.alldata.workable_link!==null){
          isFacebookHasLink=true
          isTwitterHasLink=true
          isLinkedinHasLink=true
        }
      }else if(this.props.alldata.job_source=="LM"){
        if(this.props.alldata.facebook_link!==null){
          // if facebook link is null then to stop sharing job on social media
          isFacebookHasLink=true
        }
        if(this.props.alldata.twitter_link !==null ){
          isTwitterHasLink=true
        }
        if(this.props.alldata.linkedin_link !==null){
          isLinkedinHasLink=true
        }
      }
    }
    var selectedJob_Rss_id=''
    selectedJob_Rss_id=this.props.alldata.identity!==undefined?this.props.alldata.identity:''

    if (this.props.socialAccount.accounts.length !== 0) {
      this.props.socialAccount.accounts.map((acc, i) => {
        // to check which account channels are connnected
        if (acc.iscompany == 0 && acc.social_media == 'facebook') {
          // if account is company account and social media is facebook
          facebookaccount = true
        }
        if (acc.iscompany == 0 && acc.social_media == 'linkedin') {
          linkedinaccount = true
        }
        if (acc.iscompany == 0 && acc.social_media == 'twitter') {
          twitteraccount = true
        }
      })
    }
    return (
      <div>
        <div className='socialIcons clearfix'>
        {this.props.location.pathname != "/feed/jobs" ?
        <div className='socialShareItem'><a class="internal-icon" onClick={this.shareCuratedInterlly.bind(this)} title="Internal channel"><i class="material-icons">vpn_lock</i></a></div>
        :''}
       <div className='socialShareItem'>
            {facebookaccount
              ? // if facebook account is connected
              isFacebookHasLink==true
              ?
                <a
                  className='facebook-link rss-share-link fb'
                  onClick={this.selectacc.bind(this, this.props.alldata, 'fb')}
                >
                  <i class='fa fa-facebook' aria-hidden='true' />
                </a>
              : // if facebook account is not connected then to give tip
              <span
                  data-tip={'Link is not available.'}
                  data-for={'notConnectedSocialChannels'}
                >
                  {' '}
                  <a className='fb linkNotAvailable'>
                    <i className='fa fa-facebook ' />
                  </a>
                  {this.state.tooltip?
                  <this.state.tooltip
                    type='warning'
                    id='notConnectedSocialChannels'
                  />:''}
                </span>
                :
                <span
                  data-tip={'Please connect these social accounts in settings.'}
                  data-for={'notConnectedSocialChannels'}
                >
                  {' '}
                  <a className='fb linkNotAvailable'>
                    <i className='fa fa-facebook notConnected' />
                  </a>
                  {this.state.tooltip?
                  <this.state.tooltip
                    type='warning'
                    id='notConnectedSocialChannels'
                  />:''}
                </span>
              }
          </div>
          <div className='socialShareItem'>
            {twitteraccount
              ?
              isTwitterHasLink==true
              ?
               <a
                className='twitter-link rss-share-link tw'
                onClick={this.selectacc.bind(this, this.props.alldata, 'tw')}
                >
                <i class='fa fa-twitter' aria-hidden='true' />
              </a>
              :
              <span
              data-tip={'Link is not available.'}
              data-for={'notConnectedSocialChannels'}
              >
              {' '}
              <a className='tw linkNotAvailable'>
                <i className='fa fa-twitter' />
              </a>
              {this.state.tooltip ?
              <this.state.tooltip
                type='warning'
                id='notConnectedSocialChannels'
                />:''}
            </span>:
              <span
                data-tip={'Please connect these social accounts in settings.'}
                data-for={'notConnectedSocialChannels'}
                >
                {' '}
                <a className='tw linkNotAvailable'>
                  <i className='fa fa-twitter notConnected' />
                </a>
                {this.state.tooltip?
                <this.state.tooltip
                  type='warning'
                  id='notConnectedSocialChannels'
                  />:''}
              </span>}
          </div>
          <div className='socialShareItem'>
            {linkedinaccount
              ?
              isLinkedinHasLink==true
              ?
              <a
                className='linkedin-link rss-share-link li'
                onClick={this.selectacc.bind(this, this.props.alldata, 'li')}
                >
                <i class='fa fa-linkedin' aria-hidden='true' />
              </a>
              : <span
                data-tip={'Link is not available.'}
                data-for={'notConnectedSocialChannels'}
                >
                {' '}
                <a className='li linkNotAvailable'>
                  <i className='fa fa-linkedin' />
                </a>
                {this.state.tooltip?
                <this.state.tooltip
                  type='warning'
                  id='notConnectedSocialChannels'
                  />:''}
              </span>
              :
              <span
                data-tip={'Please connect these social accounts in settings.'}
                data-for={'notConnectedSocialChannels'}
                >
                {' '}
                <a className='li linkNotAvailable'>
                  <i className='fa fa-linkedin notConnected' />
                </a>
                {this.state.tooltip?
                <this.state.tooltip
                  type='warning'
                  id='notConnectedSocialChannels'
                  />:''}
              </span>}
          </div>
        </div>
        {this.props.directShare
          ? // if data is share direct without opening popup
            this.state.accname == 'fb'
              ?
               <Facebook linkToShare={this.state.shareLink} successId={this.shareOnSocialMedia.bind(this,'Facebook')} userid={fbuserid} shareFrom={resourceName}/>
              : this.state.accname == 'tw'
                  ? <Twitter message={this.state.shareLink} iscompany={0} postId={selectedJob_Rss_id} resourceType={resourceName}/>
                  : this.state.accname == 'li'
                      ? <Linkedin message={this.state.shareLink} socialUserId={liuserid} iscompany={0}/>
                      : ''
          : ''}

        {this.state.sharefrompopup
        //if data is share through popup
          ? this.state.accname == 'fb'
              ? // if popup is open and account name is facebook
                Object.keys(this.props.comment).length > 0
                  ? // if comment is entered in field
                    <Facebook
                      linkToShare={this.state.shareLink}
                      message={externalDesc}
                      userid={fbuserid}
                      successId={this.shareOnSocialMedia.bind(this,'Facebook')}
                      shareFrom={resourceName}
                    />
                  : <Facebook linkToShare={this.state.shareLink} successId={this.shareOnSocialMedia.bind(this,'Facebook')} userid={fbuserid} shareFrom={resourceName}/>
              : this.state.accname == 'tw'
                  ? Object.keys(this.props.comment).length > 0
                      ? <Twitter
                        message={externalDesc + ' ' + this.state.shareLink}
                        iscompany={0}
                        postId={selectedJob_Rss_id}
                        resourceType={resourceName}
                        />
                      : <Twitter message={this.state.shareLink} iscompany={0} postId={selectedJob_Rss_id} resourceType={resourceName}/>
                  : this.state.accname == 'li'
                      ? Object.keys(this.props.comment).length > 0
                          ? <Linkedin
                            message={externalDesc}
                            link={this.state.shareLink}
                            socialUserId={liuserid}
                            iscompany={0}
                            />
                          : <Linkedin message={this.state.shareLink} iscompany={0} socialUserId={liuserid}/>
                      : ''
          : ''}
      </div>
    )
  }
}
function mapStateToProps (state) {
  return {
    feed: state.feed,
    profile: state.profile,
    socialAccount: state.socialAccounts
  }
}
function mapDispatchToProps (dispatch) {
  return {
    feedActions: bindActionCreators(feedActions, dispatch),
    postsActions: bindActionCreators(postsActions, dispatch),
    socialAccountsActions: bindActionCreators(socialAccountsActions, dispatch)
  }
}
let connection = connect(mapStateToProps, mapDispatchToProps)
var reduxConnectedComponent = connection(CuratedSharePopup)

export default reduxForm({
  form: 'CuratedSharePopup' // a unique identifier for this form
})(reduxConnectedComponent)
