import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Field, reduxForm } from 'redux-form'
import { Link } from 'react-router'
import PopupWrapper from '../../../PopupWrapper'
var jobdata = '';
export default class JobPopup extends React.Component {
    constructor (props) {
      super(props)
      this.state = {
       
      }
    }
    componentDidMount(){
        document.getElementById('jobFullDesc').innerHTML =  this.props.jobdata.description;
    }
    render(){
        jobdata = this.props.jobdata
         return(
      <div> 
        <div className='job-details-container'>
          <div className='job-details '>
            <header className='job-title'>
              <h2>{jobdata.title}</h2>
              <p className='job-date'>
                {this.props.moment
                  ? this.props.moment(this.props.moment.posted_date)
                      .format('MMMM YYYY')
                  : ''}
              </p>
            </header>
            <PopupWrapper>

              <div className='read-only-data'>

                <div className='form-row'>
                  <label> <i className='material-icons'>location_on</i></label>
                  <div class='value'>{ jobdata.location}</div>
                  <label><i className='material-icons'>business</i></label>
                  <div class='value'>{ jobdata.industry}</div>
                </div>
                <div className='form-row'>
                  <label> <i className='material-icons'>work</i></label>
                  <div class='value'>{ jobdata.type}</div>
                  <label>
                    <i className='material-icons'>monetization_on</i>
                  </label>
                  <div class='value'>
                    { jobdata.from_salary}
                    -
                    {jobdata.to_salary}
                  </div>
                </div>
                <div className='description'>
               
                  <div class='value job-description' id='text-job'>
                    <div id='jobFullDesc' className='jobFullDesc' />
                  </div>
                </div>
              </div>
            {this.props.jobform()} 
            {this.props.jobshare()} 

            </PopupWrapper>
       
         
          </div>
          </div>
      </div>
         )
    }
}