import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Field, reduxForm } from 'redux-form'
import { Link } from 'react-router'
import * as feedActions from '../../../../actions/feed/feedActions'
import * as postsActions from '../../../../actions/feed/postsActions'
import * as socialAccountsActions from '../../../../actions/socialAccountsActions'
import CuratedSharePopup from '../CuratedSharePopup'
import CuratedCommentForm from '../CuratedCommentForm'
import CollapsibleFilter from '../../../CollapsibleFilter'
import FeedNav from '../../FeedNav'
import Globals from '../../../../Globals'
import Header from '../../../Header/Header'
import moment from '../../../Chunks/ChunkMoment'
import PreLoader from '../../../PreLoader'
import SliderPopup from '../../../SliderPopup'
import JobPopup from './JobPopup'
var striptags = require('striptags');
import GuestUserRestrictionPopup from '../../../GuestUserRestrictionPopup';
import StaticDataForGuest from '../../../StaticDataForGuest';
import JobCurationSharePopup from '../../JobCurationSharePopup';
import ReactCSSTransitionGroup from 'react/lib/ReactCSSTransitionGroup'
import MergeJobsRssSharePopup from '../../MergePages/MergeJobsRssSharePopup';
import CenterPopupWrapper from '../../../CenterPopupWrapper';
import MergeJobsPopup from '../../MergePages/MergeJobsPopup';
import * as utils from '../../../../utils/utils';

var jobdetail = '';
var restrictJobAPI = false;
var fbuserid,
  liuserid = ''
class Jobs extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      currentFilter: '',
      show_menu: true,
      JobPostsPopup: false,
      moment: '',
      SelectedJob: {},
      accname: '',
      comment: {},
      cntnum: 0,
      msg: '',
      shareLink: '',
      sharefrompopup: false,
      shareJobPopupState: false,
      shareJobPopupStateOfPostID: null
    }
  }
  componentWillReceiveProps(nextProps) {
    if (this.props.posts.creation.created === true) {
      this.closePopup();
    }
  }
  /**
   * to initailize facebook instace
   */
  facebookInit() {
    window.fbAsyncInit = function () {
      FB.init({
        appId: '105978646744031',
        // appId:'280656325686422',
        cookie: true, // enable cookies to allow the server to access
        // the session
        xfbml: true, // parse social plugins on this page
        version: 'v2.1' // use version 2.1
      })
    }
      ; (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0]
        if (d.getElementById(id)) return
        js = d.createElement(s)
        js.id = id
        js.src = 'https://connect.facebook.net/en_US/sdk.js'
        fjs.parentNode.insertBefore(js, fjs)
      })(document, 'script', 'facebook-jssdk')
  }

  componentWillMount() {
    moment().then(moment => {
      this.setState({ moment: moment })
    })
    this.setState({ cntnum: '' })
  }
  renderLoading() {
    return (
        <div class="feed-jobs-wrapper">
        <div class="row">
          <div class="feed-job-item col-one-of-four">
            <div class="job-box">
              <div class="feed-job-item-inner feed-job-loader-border">
                <div><div class="job-date">
                  <p class="loader-grey-line  loader-line-radius loader-line-height loader-date"></p></div>
                  <div class="job-title loader-grey-line  loader-line-radius loader-line-height job-loader-text">
                    <p>
                    </p>
                  </div>
                  <div class="job-location loader-grey-line  loader-line-radius loader-line-height job-loader-text">
                    <p></p></div>
                  <div class="job-descripation">
                    <div id="jobFullDesc" class="jobFullDesc">
                    </div>
                    <p class="job-location loader-grey-line loader-line-height job-loader-description"> </p>
                  </div>
                </div>
                <div class="feed-job-share-block feed-job-share-block-0 ">
                  <div class="sharePopupWrapper">
                    <a class="share-job" id="rpPDp">

                      <span class="job-location loader-grey-line  loader-line-radius loader-line-height job-share-loader"></span></a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="feed-job-item col-one-of-four">
            <div class="job-box">
              <div class="feed-job-item-inner feed-job-loader-border">
                <div><div class="job-date">
                  <p class="loader-grey-line  loader-line-radius loader-line-height loader-date"></p></div>
                  <div class="job-title loader-grey-line  loader-line-radius loader-line-height job-loader-text">
                    <p>
                    </p>
                  </div>
                  <div class="job-location loader-grey-line  loader-line-radius loader-line-height job-loader-text">
                    <p></p></div>
                  <div class="job-descripation">
                    <div id="jobFullDesc" class="jobFullDesc">
                    </div>
                    <p class="job-location loader-grey-line loader-line-height job-loader-description"> </p>
                  </div>
                </div>
                <div class="feed-job-share-block feed-job-share-block-0 ">
                  <div class="sharePopupWrapper">
                    <a class="share-job" id="rpPDp">

                      <span class="job-location loader-grey-line  loader-line-radius loader-line-height job-share-loader"></span></a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="feed-job-item col-one-of-four">
            <div class="job-box">
              <div class="feed-job-item-inner feed-job-loader-border">
                <div><div class="job-date">
                  <p class="loader-grey-line  loader-line-radius loader-line-height loader-date"></p></div>
                  <div class="job-title loader-grey-line  loader-line-radius loader-line-height job-loader-text">
                    <p>
                    </p>
                  </div>
                  <div class="job-location loader-grey-line  loader-line-radius loader-line-height job-loader-text">
                    <p></p></div>
                  <div class="job-descripation">
                    <div id="jobFullDesc" class="jobFullDesc">
                    </div>
                    <p class="job-location loader-grey-line loader-line-height job-loader-description"> </p>
                  </div>
                </div>
                <div class="feed-job-share-block feed-job-share-block-0 ">
                  <div class="sharePopupWrapper">
                    <a class="share-job" id="rpPDp">

                      <span class="job-location loader-grey-line  loader-line-radius loader-line-height job-share-loader"></span></a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
  componentDidMount () {
    if(restrictJobAPI!=true){
        this.props.feedActions.fetchJobList()
      this.props.socialAccountsActions.fetchSocialAccounts()
      restrictJobAPI = true;
      setTimeout(() => {
        restrictJobAPI = false
      }, 5000)
    }
    this.facebookInit()
  }
  /**
   * when click on job post to open popup
   * @author disha
   * @argument alldata
   * @argument me this
   */
  showJobDetail(alldata, me) {
    this.setState({
      JobPostsPopup: true,
      SelectedJob: alldata
    })
    // document.body.classList.add('overlay')
  }

  /**
   * to close popup
   * @param {*} popup
   */
  closePopup(popup) {
    this.setState({
      JobPostsPopup: false,
      accname: '',
      cntnum: 0,
      shareLink: '',
      comment: {}
    })
    document.body.classList.remove('overlay')
  }
  /**
   * @author disha
   * when popup is open and to share job on social media
   */
  jobShare() {
    return (
      <div>
        <div className='tweet-msg'>
          {this.state.msg}
        </div>
        <MergeJobsRssSharePopup
          currData={this.state.SelectedJob}
          shareFrom={'jobs'}
          accounts={this.props.accounts}
          comment={this.state.comment}
        />
        <div className='job-count'>
          {this.state.cntnum}
        </div>
      </div>
    )
  }
  /**
   * to get value from curatedcomment form field
   * @param {*} data text from curatedcommentform
   */
  handleFormtext(data) {
    this.setState({
      comment: data
    })
  }
  /**
   * to count word of comment box for twitter
   * @param {*} e
   */
  wordCountForm(e) {
    if (e > Globals.TWITTER_LIMIT_WORD) {
      document.getElementsByClassName('job-count')[0].style.color = 'red'
      document.getElementsByClassName('job-count')[0].style.marginTop = '-59px'
      document.getElementsByClassName('tweet-msg')[0].style.color = 'red'
      document.getElementsByClassName('tweet-msg')[0].style.display = 'inline'
      this.setState({ msg: 'Twitter Character Limit Exceeded' })
      this.setState({ cntnum: e })
    } else {
      document.getElementsByClassName('tweet-msg')[0].style.display = 'none'
      document.getElementsByClassName('job-count')[0].style.color = '#7b7b7b'
      document.getElementsByClassName('tweet-msg')[0].style.color = '#7b7b7b'
      this.setState({ cntnum: e })
    }
    // }
  }
  /**
   * call curated commnet form component for comment field
   */
  jobForm() {
    return (
      <CuratedCommentForm
        commentFormText={this.handleFormtext.bind(this)}
        countnum={this.wordCountForm.bind(this)}
      />
    )
  }
  /**
   * @author disha
   * chnage the popup design @auther kinjal
   * to open popup of job for detail view
   */
  renderJobspopup() {
    return (
      <div>
        {/* this componet used for the center popup  */}
        <CenterPopupWrapper>
          <div className="rss-popup-deatils-page-container">
            <MergeJobsPopup
              jobdata={this.state.SelectedJob}
              accounts={this.props.accounts}
              closeJob={this.closePopup.bind(this, 'posts')}
            />
          </div>
        </CenterPopupWrapper>
      </div>
    )
  }
  FetchRender() {
    var allJob = this.props.feed

    this.props.socialAccount.accounts.length > 0 ?
      this.props.socialAccount.accounts.map(s => {
        if (s.social_media == 'facebook') {
          if (s.iscompany == 0) {
            var fbuseridvalue = s.user_social.filter(function (v) {
              return v['key'] == 'user_id'
            })
            fbuseridvalue.map(pageid => {
              fbuserid = pageid.value
            })
          }
        } else if (s.social_media == 'linkedin') {
          if (s.iscompany == 0) {
            liuserid = s.identity
          }
        }
      }) : ''
    return (

      <div className='feed-jobs-wrapper'>
        <div className='row'>
          <div id="jobTour"></div>
          {allJob.loading == true
            ? this.renderLoading()
            : allJob.JobData.length > 0
              //to display job post
              ? allJob.JobData.map((jobdetail, i) => {
                var html = jobdetail.description;
                var jobTitle = utils.limitLetters(jobdetail.title,30)
                return (

                  <div
                    className='feed-job-item col-one-of-four'
                    key={i}
                  >
                  <div className="job-box">
                    <div className='feed-job-item-inner'>
                      <div
                        onClick={this.showJobDetail.bind(
                          this,
                          jobdetail
                        )}
                      >
                        <div className='job-date'>
                          <p>
                            {this.state.moment
                              ? this.state
                                .moment(jobdetail.posted_date)
                                .format('MMMM YYYY')
                              : ''}
                          </p>
                        </div>
                        <div className='job-title'>
                          <p title={jobdetail.title}>{jobTitle}</p>
                        </div>
                        <div className='job-location'>
                          <i className='material-icons'>
                            location_on
                                      </i>
                            <p> {jobdetail.location}
                            </p>


                          </div>
                          <div className='job-descripation'>
                            <div id='jobFullDesc' className='jobFullDesc' />
                            <p> {striptags(html)} </p>

                          </div>
                        </div>
                        <div className='feed-job-share-block feed-job-share-block-0 '>
                          <MergeJobsRssSharePopup
                            currData={jobdetail}
                            shareFrom={'jobs'}
                            accounts={this.props.accounts}
                            comment={this.state.comment}
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                )
              })
              : <div className='no-data-block'>No jobs found.</div>}
        </div>
      </div>
    )
  }
  render() {
    return (
      <div>
        <section id='jobs' className='settings'>

          {this.state.JobPostsPopup ? this.renderJobspopup() : ''}
          <Header
            title='Jobs'
            nav={FeedNav}
            popup_text='Click here to add new post.'
            // add_new_button={true}
            currFeed
            currFeedPosts
            hash={this.props.location.hash}
            location={this.props.location}
            open_filter_fn
          />
          <div className='main-container'>
            <div id='content'>
              <div className='page campaign-container with-filters'>
                {/* Curated page */}
                {/* Curated filters */}


                <div className='curated-page'>
                  {/* <CollapsibleFilter>
                    <div className='row'>
                      <div className='col-one-of-four'>
                        <div className='curated-filter-inner'>
                          <div className='form-row'>
                            <label>Search</label>
                            <input type='text' placeholder='Search Job Title' />
                          </div>
                        </div>
                      </div>
                      <div className='col-one-of-four'>
                        <div className='curated-filter-inner'>
                          <div className='form-row'>
                            <label>Industry</label>
                            <div className='select-wrapper'>
                              <select>
                                <option>Industry1</option>
                                <option>Industry2</option>
                                <option>Industry3</option>
                              </select>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className='col-one-of-four'>
                        <div className='curated-filter-inner'>
                          <div className='form-row'>
                            <label>Country</label>
                            <div className='select-wrapper'>
                              <select>
                                <option>Country1</option>
                                <option>Country2</option>
                                <option>Country3</option>
                              </select>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className='col-one-of-four'>
                        <div className='curated-filter-inner'>
                          <div className='form-row'>
                            <label>Locations</label>
                            <div className='select-wrapper'>
                              <select>
                                <option>Locations1</option>
                                <option>Locations2</option>
                                <option>Locations3</option>
                              </select>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className='col-one-of-four'>
                        <div className='curated-filter-inner'>
                          <div className='form-row'>
                            <label>Type</label>
                            <div className='select-wrapper'>
                              <select>
                                <option>Type1</option>
                                <option>Type2</option>
                                <option>Type3</option>
                              </select>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className='col-one-of-four'>
                        <div className='curated-filter-inner'>
                          <div className='form-row'>
                            <label>Salary From</label>
                            <div className='select-wrapper'>
                              <select>
                                <option>Salary From1</option>
                                <option>Salary From2</option>
                                <option>Salary From3</option>
                              </select>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className='col-one-of-four'>
                        <div className='curated-filter-inner'>
                          <div className='form-row'>
                            <label>Salary To</label>
                            <div className='select-wrapper'>
                              <select>
                                <option>Salary To1</option>
                                <option>Salary To2</option>
                                <option>Salary To3</option>
                              </select>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </CollapsibleFilter> */}
                  {/* Curated filters ends */}
                     {this.FetchRender()}
                  {/* Curated page ends */}
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    accounts: state.socialAccounts.accounts,
    feed: state.feed,
    posts: state.posts,
    profile: state.profile,
    socialAccount: state.socialAccounts
  }
}
function mapDispatchToProps(dispatch) {
  return {
    feedActions: bindActionCreators(feedActions, dispatch),
    postsActions: bindActionCreators(postsActions, dispatch),
    socialAccountsActions: bindActionCreators(socialAccountsActions, dispatch)
  }
}
let connection = connect(mapStateToProps, mapDispatchToProps)
var reduxConnectedComponent = connection(Jobs)

export default reduxForm({
  form: 'Jobs' // a unique identifier for this form
})(reduxConnectedComponent)
