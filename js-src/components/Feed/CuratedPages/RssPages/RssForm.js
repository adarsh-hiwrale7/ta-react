
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Field, reduxForm } from 'redux-form'
import {renderField} from '../../../Helpers/ReduxForm/RenderField'
import * as settingsActions from '../../../../actions/settingsActions'
const required = value => value
  ? undefined
  : 'Required';
const validUrl = value =>
  (value && !/(((ftp|https?):\/\/|(www|visi.ly))[\-\w@:%_\+.~#?,&\/\/=]+)/g.test(value) || (value.match(/https:\/\/|http:\/\//g) || []).length >1
    ? 'Invalid URL'
    : undefined)
  class RssForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {  
          utm_campaign: "",
          utm_source: "",
          utm_medium: "",
          showAddRssForm:false,
          rssAddOption:null,
          url:"",
        }
    }

componentDidMount(){
  if(this.props.roleName!=="super-admin" && this.props.roleName !== "admin" ||  this.props.currRssData){
    this.setState({
      showAddRssForm:true
    })
  }
  this.handleInitialize(this.props);
  if(this.props.currRssData&& this.props.currRssData.utm_id){
    this.props.getUtmData(this.props.currRssData.utm_id);
  }
}
componentWillReceiveProps(newProps){
  if( newProps.editRss!==false && newProps.editRss !== this.props.editRss){
    this.setState({
      showAddRssForm:true
    })
    if(newProps.currRssData !== this.props.currRssData  && newProps.currRssData !== null && newProps.currRssData.utm_id !== null ){
      this.props.getUtmData(newProps.currRssData.utm_id);
    }else{
      this.handleInitialize(newProps);
    }
  }
  if(this.props.utmData!==newProps.utmData){
    this.handleInitialize(newProps);
  }
}
handleInitialize(propsData=null){
  if(propsData.currRssData){
    const initData ={}
    initData["rss"] = propsData.currRssData.url
    initData["rss_identity"] = propsData.currRssData.identity
    initData["utm_identity"] = propsData.currRssData.utm_id

    if(propsData.utmData.url){
      let utm_data = propsData.utmData

      var testsource = utm_data.url.match("utm_source=(.*)&utm_medium");
      var testmedium = utm_data.url.match("utm_medium=(.*)&utm_campaign");
      var testcampaign = utm_data.url.match("utm_campaign=(.*)");
      initData["utmCampaign"]=testcampaign[1];
      initData["utmMedium"]=testmedium[1];
      initData["utmSource"]=testsource[1];
    }else{
      initData["utmCampaign"]=this.state.utm_campaign
      initData["utmMedium"]=this.state.utm_medium
      initData["utmSource"]=this.state.utm_source
    }
    this.props.initialize(initData);
  }else{
   const initData ={}
   initData["utmCampaign"]=this.state.utm_campaign
   initData["utmMedium"]=this.state.utm_medium
   initData["utmSource"]=this.state.utm_source
   this.props.initialize(initData);
  }
}
handleRssSelect(value){
  this.setState({
    showAddRssForm:true,
    rssAddOption : value
  })
  if(value=="all"){
    this.props.change("is_admin",1);
  }
}
closeRssForm(){
  if(this.props.editRss!==true && this.state.showAddRssForm==true && (this.props.roleName=="super-admin" || this.props.roleName=="admin")){
    this.setState({
      showAddRssForm:false,
      rssAddOption : null
    })
  }else{
    this.props.closeRssForm();
  }
}
render(){
  const {handleSubmit,pristine,submitting} = this.props
        return (
                  <section className="add-rss-form">
                      <form role="form" onSubmit={handleSubmit}>
                        <div className="form-row heading">
                           <h3> RSS </h3>
                            <a
                              className='close-rss-btn'
                              onClick={this.closeRssForm.bind(this)}>
                              <i class='material-icons'>
                                clear
                                </i>
                            </a>
                        </div>
                        
                        {this.state.showAddRssForm==true?
                          <div>
                              <div>
                                <Field
                                  type='hidden'
                                  name="rss_identity"
                                  component={renderField}
                                />
                                <Field
                                  type='hidden'
                                  name="utm_identity"
                                  component={renderField}
                                />
                                <Field
                                  type='hidden'
                                  name="is_admin"
                                  component={renderField}
                                />
                                <Field
                                  placeholder='RSS Channel'
                                  component={renderField}
                                  type='text'
                                  name='rss'
                                  className='rss-textbox'
                                  value={this.state.url}
                                  disabled ={this.props.currRssData?true:false}
                                  validate= {[required,validUrl]}
                                />
                              <div className="utmLinkWrapper">
                                <div className="utmWrapper clearfix">
                                  <div className="utmSourceWrapper">
                                    <label>UTM Source :</label>

                                    <Field
                                      id="utmSource"
                                      name="utmSource"
                                      component={renderField}
                                      type="text"
                                      value={this.state.utm_source}
                                      // validate={[required]}
                                      placeholder="UTM Source"
                                    />
                                  </div>

                                  <div className="utmMediumWrapper">
                                    <label>UTM Medium : </label>
                                    <Field
                                      id="utmMedium"
                                      name="utmMedium"
                                      component={renderField}
                                      type="text"
                                      value={this.state.utm_medium}
                                      // validate={[required]}
                                      placeholder="UTM Medium"
                                    />
                                  </div>

                                  <div className="utmCampaignWrapper">
                                    <label>UTM Campaign : </label>
                                    <Field
                                      id="utmCampaign"
                                      name="utmCampaign"
                                      component={renderField}
                                      type="text"
                                      value={this.state.utm_campaign}
                                      // validate={[required]}
                                      placeholder="UTM Campaign"
                                    />
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div className="form-row rss-setting-button">
                                  <button type="submit" class="btn btn-primary" disabled={pristine || submitting}>
                                    <span class="rss-field">+</span>
                                    {this.props.editRss==true?
                                    'update RSS'
                                    :this.state.rssAddOption =="all" ? 'Add RSS channel for all' :'Add RSS channel for myself'}
                                   </button>
                            </div>
                            <div className="rss-note"> <span>Note:</span> <p>RSS will be Available in 15 minute</p></div>
                      </div>:
                      <div className="add-RSS-option-btn">
                        <a className="btn btn-default add-new-button " onClick={this.handleRssSelect.bind(this,"all")}>
                                  <span className='add-new-text'>RSS for all</span>
                        </a>
                        <a className="btn btn-default add-new-button " onClick={this.handleRssSelect.bind(this,"me")}>
                                  <span className='add-new-text'>RSS for me</span>
                        </a>
                      </div>
                    }

                     </form>
                  </section>
        )
    }
}
function mapStateToProps (state, ownProps) {
    return {
      settings: state.settings
    }
  }

  function mapDispatchToProps (dispatch) {
    return {
      settingsActions: bindActionCreators(settingsActions, dispatch), 
    }
  }
let connection = connect(mapStateToProps, mapDispatchToProps)

var reduxConnectedComponent = connection(RssForm)

export default reduxForm({
    form: 'RssForm',  // a unique identifier for this form
  })(RssForm)