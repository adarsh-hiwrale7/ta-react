import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Field, reduxForm } from 'redux-form'
import * as feedActions from '../../../../actions/feed/feedActions'
import * as postsActions from '../../../../actions/feed/postsActions'
import * as settingsActions from '../../../../actions/settingsActions'
import * as socialAccountsActions from '../../../../actions/socialAccountsActions'
import Header from '../../../Header/Header'
import FeedNav from '../../FeedNav'
import moment from '../../../Chunks/ChunkMoment'
import PreLoader from '../../../PreLoader'
import ReactCSSTransitionGroup from 'react/lib/ReactCSSTransitionGroup'
import Rss_Popup from './Rss_Popup'
import SliderPopup from '../../../SliderPopup'
import RssForm from './RssForm'
import Globals from '../../../../Globals'
import ScrollToFetch from 'react-scroll-to-fetch';
import GuestUserRestrictionPopup from '../../../GuestUserRestrictionPopup';
import StaticDataForGuest from '../../../StaticDataForGuest';
import MergeJobsRssSharePopup from '../../MergePages/MergeJobsRssSharePopup';
import CenterPopupWrapper from '../../../CenterPopupWrapper';
import MergeRssPopup from '../../MergePages/MergeRssPopup';
var filter_active = ''
var savedData = false
var striptags = require('striptags')
var count = 1
var facebookaccount = false
var twitteraccount = false
var linkedinaccount = false
var restrictRssAPI = false
var isDataAdded = true

var fbuserid,
  liuserid = ''
class Rss extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      currentFilter: '',
      moment: '',
      show_menu: true,
      rssData:{},
      finished:false,
      show_filters: true,
      clickedDivId: null,
      fbpopup: false,
      twpopup: false,
      linkpopup: false,
      openpopup: false,
      permalink: '',
      saveAllData: {},
      savedData: false,
      accname: '',
      test: '',
      curation_popup_rss: false,
      curated_data: '',
      show_menu: true,
      shareRssPopupState: false,
      shareRssPopupStateOfPostID: null,
    }
    this.fetchRSS = this.fetchRSS.bind(this)
  }
  
  fetchRSS ()
  { 
    if ((window.innerHeight + window.scrollY) == document.documentElement.scrollHeight && !this.props.feed.loading) {
       this.props.feedActions.fetchRssFeed(count)
    }
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.posts.creation.created === true) {
      this.closeRssPopup();
    }
  }

 
  /**
   * initalise facebook
   */
  facebookInit() {
    window.fbAsyncInit = function () {
      FB.init({
        appId: '105978646744031',
        // appId:'280656325686422',
        cookie: true, // enable cookies to allow the server to access
        // the session
        xfbml: true, // parse social plugins on this page
        version: 'v2.1' // use version 2.1
      })
    }
      ; (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0]
        if (d.getElementById(id)) return
        js = d.createElement(s)
        js.id = id
        js.src = 'https://connect.facebook.net/en_US/sdk.js'
        fjs.parentNode.insertBefore(js, fjs)
      })(document, 'script', 'facebook-jssdk')
  }
  componentDidMount() {
    // get rss link
    window.scrollTo(0,0);
    this.props.feedActions.fetchRssFeed(count)
    if(restrictRssAPI!==true){
      this.props.socialAccountsActions.fetchSocialAccounts()
      restrictRssAPI = true;
      setTimeout(() => {
        restrictRssAPI = false
      }, 5000)
    }
    this.facebookInit();
  }

  componentWillMount() {
    window.addEventListener('scroll', this.fetchRSS);
    moment().then(moment => {
      this.setState({ moment: moment })
    })
  }

  componentWillUnmount()
  {
    window.removeEventListener('scroll', this.fetchRSS);
  }

  componentDidUpdate(prevProps)
  {  
    var same = this.props.feed.RssData.data == prevProps.feed.RssData.data
    if(same == false)
    {
      count = count +1;
      this.setState({
        rssData : {...this.state.rssData, ...this.props.feed.RssData.data}
      })
    }  
  }


  /**
   * when select on social channel to share
   * @param {*} data
   * @param {*} acc
   */
  selectacc(data, acc) {
    savedData = true
    this.setState({
      accname: acc,
      saveAllData: data.permalink
    })
  }

  closeRssPopup() {
    this.closePage();

  }
   /**
   * fetch data for rssfeed
   * @author {*} Priyanka
   * @param {*} data
   */
  // fetch=page=>{
  //   // return new Promise((resolve)=>{
  //     this.props.feedActions.fetchRssFeed(page)
  //     // setTimeout(()=>{this.fakeFetch(page);resolve()},5000)
  //   // })
  // }
  // /**
  //  * set data into rssfeed state
  //  * @author {*} Priyanka
  //  * @param {*} data
  //  */
  // fakeFetch=(page)=>{
  //   const prevTmp=this.state.rssData;
  //   let rssDetail=this.props.feed.RssData.data?this.props.feed.RssData.data:[];
  //   this.setState({rssData:{...prevTmp,...rssDetail}});
  //   //if last page reached then set flag to doesn't allow scroll
  //   if(this.props.feed.RssData.last_page == page){
  //     this.setState({finished:true})
  //   }
  // }
  /**
   * to open popup of rss post
   */
  open_curation_popup() {
    return (
      <div>
        <CenterPopupWrapper>
          <div className="rss-popup-deatils-page-container">
            <MergeRssPopup
              alldata={this.state.curated_data}
              closeRssPopup={this.closeRssPopup.bind(this)}
              accounts={this.props.socialAccount.accounts}
            />
          </div>
        </CenterPopupWrapper>
      </div>
    )
  }
  curation_popup(e) {
    savedData = false
    this.setState({
      curation_popup_rss: true,
      curated_data: e,
      saveAllData: {}
    })
    // document.body.classList.add('overlay')
  }
  closePage() {
    this.setState({
      curation_popup_rss: false,
      curated_data: ''
    })
    document.body.classList.remove('overlay')
  }

  

  FetchRender() {
    if (this.props.socialAccount.accounts.length !== 0) {
      this.props.socialAccount.accounts.map((acc, i) => {
        //to check whether account is connected or not
        if (acc.iscompany == 0 && acc.social_media == 'facebook') {
          facebookaccount = true
          var fbuseridvalue = acc.user_social.filter(function (v) {
            return v['key'] == 'user_id'
          })
          fbuseridvalue.map(pageid => {
            fbuserid = pageid.value
          })
        }
        if (acc.iscompany == 0 && acc.social_media == 'linkedin') {
          linkedinaccount = true
          liuserid = acc.identity
        }
        if (acc.iscompany == 0 && acc.social_media == 'twitter') {
          twitteraccount = true
        }
      })
    }
    var imgstyles = {
      backgroundSize: 'cover',
      backgroundRepeat: 'no-repeat'
    }
    return(
    <div className='main-container-body'>
                <div className='row main-container-rss'>
                  <div className='col-nine-of-twelve hotdiv space-hot'>
                    <span id="curatedTour" />
                   {/* <ScrollToFetch
                      fetch={ !this.props.feed.loading ? this.fetch : ''} 
                      finished={this.state.finished}
                      initialLoad={true}
                      currentPage={count}
                      loader={this.renderLoadingPage()}
                    >   */}
                {this.props.feed.loading && count ==1 ? this.renderLoadingPage() :
                    this.state.rssData
                      ? 
                      <div>
                      <div className='row rss-columns'>
                        {Object.entries(this.state.rssData).map((rd, i) => {
                          var image = rd[1].image ? rd[1].image : ''
                          // let rd_title = rd[1].title.replace(new RegExp('&amp;', 'g'),'&');
                          // let rd_descripation = rd[1].description
                          // let rd_source = rd[1].permalink
                          // var rssImage = document.createElement('div')
                          // rssImage.innerHTML = rd_descripation
                          // if (image == '') {
                          //   if (rssImage.getElementsByTagName('img')[0]) {
                          //     image = rssImage.getElementsByTagName('img')[0]
                          //         .src
                          //   }
                          // }
              
                          return (
                            
                            <div className='col-four-of-twelve curatedParent' key={i} >
                             <div className="curated-box">
                              <div className='feed-job-item-inner'>
                                <a
                                  target='_blank'
                                  href={rd[1].permalink}
                                  className='visit-source-link'
                                  >
                                  <i class='material-icons'>launch</i>
                                  {' '}
                                    Visit source
                                  </a>
                                <div
                                  className='rss-block'
                                  onClick={this.curation_popup.bind(this, rd[1])}
                                  >
                                  <div
                                    className={`banner-hot-header ${image ? 'hot-overlay' : 'no-img-header'}`}
                                    >
                                    <img src={image?image:'/img/banner-dashboard.png'} onError={(e)=>{e.target.src="/img/banner-dashboard.png"}}/>
                                    <div className='rss-banner-captions'>
                                      <p>{rd[1].title.replace(new RegExp('&amp;', 'g'),'&')}</p>
                                    </div>
                                  </div>

                               <div className='hot-div-body'>
                                 <div className='date-and-time'>
                                   Added
                                         {' '}
                                   {this.state.moment
                                    ? this.state.moment
                                      .unix(rd[1].date)
                                      .local()
                                      .fromNow()
                                    : ''}
                                </div>
                                <div className='text-header'>
                                  <p>{striptags(rd[1].description)}</p>
                                </div>
                              </div>
                            </div>

                          </div>
                          <div
                            className={`feed-job-share-block feed-job-share-block-${i}`}
                          >
                            <MergeJobsRssSharePopup
                              currData={rd[1]}
                              shareFrom={'rss'}
                              accounts={this.props.socialAccount.accounts}
                            />
                          </div>

                        </div>
                      </div>
                    )

                  })}

                </div>
                <div>{ this.props.feed.loading ? this.renderLoadingPage(): ''}</div>
                </div>
                
                : <div className='no-data-block'>No curated content found.</div>
              }
        {/* </ScrollToFetch> */}

          </div>

        </div>
      </div>
    )
  }
  render () {
    return (
      <div>
      <section id='Rss' className='settings'>
        {this.state.curation_popup_rss == true
          ? this.open_curation_popup()
          : ''}
        <Header
          title='Curated'
          nav={FeedNav}
          popup_text='Click here to add new post.'
          // add_new_button
          currFeed
          currFeedPosts
          hash={this.props.location.hash}
          location={this.props.location}
          />
          <div className='main-container'>
            <div id='content'>
            <div className='page campaign-container with-filters'>
              {this.FetchRender()
              }
            </div>
          </div>
          </div>
        </section>
      </div>
    )
  }
  
  renderLoadingPage() {
    return (
      <div className="rss-loader-wrapper">
        <div class="col-four-of-twelve curatedParent">
          <div class="curated-box">
            <div class="feed-job-item-inner feed-rss-loader">

              <div class="rss-block">
                <div class="no-img-header loader-grey-line loader-line-height rss-image">
                  <div class="rss-banner-captions">

                  </div>
                </div>
                <div class="hot-div-body">
                  <div class="date-and-time loader-grey-line  loader-line-radius loader-line-height job-share-loader rss-hour-loader">
                  </div>
                  <div class="text-header"><p class="loader-grey-line loader-line-radius loader-line-height rss-description-loader">
                  </p>
                  <p class="loader-grey-line loader-line-radius loader-line-height rss-description">
                  </p>
                  <p class="loader-grey-line loader-line-radius loader-line-height rss-description">
                  </p>
                  </div>
                </div>
              </div>
            </div>
            <div class="feed-job-share-block feed-job-share-block-0">
              <div class="sharePopupWrapper  loader-grey-line  loader-line-radius loader-line-height job-share-loader">
              </div>
            </div>
          </div>
        </div>
        <div class="col-four-of-twelve curatedParent">
          <div class="curated-box">
            <div class="feed-job-item-inner feed-rss-loader">

              <div class="rss-block">
                <div class="no-img-header loader-grey-line loader-line-height rss-image">
                  <div class="rss-banner-captions">

                  </div>
                </div>
                <div class="hot-div-body">
                  <div class="date-and-time loader-grey-line  loader-line-radius loader-line-height job-share-loader rss-hour-loader">
                  </div>
                  <div class="text-header"><p class="loader-grey-line loader-line-radius loader-line-height rss-description-loader">
                  </p>
                  <p class="loader-grey-line loader-line-radius loader-line-height rss-description">
                  </p>
                  <p class="loader-grey-line loader-line-radius loader-line-height rss-description">
                  </p>
                  </div>
                </div>
              </div>
            </div>
            <div class="feed-job-share-block feed-job-share-block-0">
              <div class="sharePopupWrapper  loader-grey-line  loader-line-radius loader-line-height job-share-loader">
              </div>
            </div>
          </div>
        </div>
        <div class="col-four-of-twelve curatedParent">
          <div class="curated-box">
            <div class="feed-job-item-inner feed-rss-loader">

              <div class="rss-block">
                <div class="no-img-header loader-grey-line loader-line-height rss-image">
                  <div class="rss-banner-captions">

                  </div>
                </div>
                <div class="hot-div-body">
                  <div class="date-and-time loader-grey-line  loader-line-radius loader-line-height job-share-loader rss-hour-loader">
                  </div>
                  <div class="text-header"><p class="loader-grey-line loader-line-radius loader-line-height rss-description-loader">
                  </p>
                  <p class="loader-grey-line loader-line-radius loader-line-height rss-description">
                  </p>
                  <p class="loader-grey-line loader-line-radius loader-line-height rss-description">
                  </p>
                  </div>
                </div>
              </div>
            </div>
            <div class="feed-job-share-block feed-job-share-block-0">
              <div class="sharePopupWrapper  loader-grey-line  loader-line-radius loader-line-height job-share-loader">
              </div>
            </div>
          </div>
        </div>
      </div>

      // <div className='preloader-wrap-page'>
      //   <PreLoader className="Page" />
      // </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    feed: state.feed,
    socialAccount: state.socialAccounts,
    posts: state.posts,
    profile: state.profile,
    settings: state.settings
  }
}
function mapDispatchToProps(dispatch) {
  return {
    feedActions: bindActionCreators(feedActions, dispatch),
    postsActions: bindActionCreators(postsActions, dispatch),
    socialAccountsActions: bindActionCreators(socialAccountsActions, dispatch),
    settingsActions: bindActionCreators(settingsActions, dispatch)
  }
}
let connection = connect(mapStateToProps, mapDispatchToProps)
var reduxConnectedComponent = connection(Rss)

export default reduxForm({
  form: 'Rss' // a unique identifier for this form
})(reduxConnectedComponent)
