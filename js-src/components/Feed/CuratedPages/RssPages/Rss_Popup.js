import { connect } from 'react-redux';
import { Link } from 'react-router';
import CuratedSharePopup from '../CuratedSharePopup'
import CuratedCommentForm from '../CuratedCommentForm';
import Facebook from '../../../Social/Facebook';
import Globals from '../../../../Globals';
import ImageLoader from 'react-imageloader';
import Linkedin from '../../../Social/Linkedin';
import PreLoaderMaterial from '../../../PreLoaderMaterial';
import PopupWrapper from '../../../PopupWrapper'
import JobCurationSharePopup from '../../JobCurationSharePopup';
import ReactCSSTransitionGroup from 'react/lib/ReactCSSTransitionGroup'
import MergeJobsRssSharePopup from '../../MergePages/MergeJobsRssSharePopup';

var readmore = false;
var savedData=false;
var countnum=0;
var lastCursorIndex=0;
var txtarea=0; 
var start=0; 
var end=0;

export default class Rss_Popup extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      popupVisible: false,
      sharepost: false,
      openCreatePopup: false,
      saveAllData: {},
      savedData: false,
      accname:'',
      comment:{},
      header_height:60,
      cntnum:0,
      msg:"",
      shareRssPopupState : false,
      shareRssPopupStateOfPostID : null
    }
  }
  wordCountForm(e)
  {
    // if(document.getElementsByName('rss-count')[0]){
      if(e>Globals.TWITTER_LIMIT_WORD)
      {
        document.getElementsByClassName("rss-count")[0].style.color="red"
        document.getElementsByClassName("rss-count")[0].style.marginTop="-59px" ;
        document.getElementsByClassName("tweet-msg")[0].style.color="red"
        document.getElementsByClassName("tweet-msg")[0].style.display="inline"
        this.setState({msg:"Twitter Character Limit Exceeded"});
        this.setState({cntnum:e});
      }
      else
      {
        document.getElementsByClassName("tweet-msg")[0].style.display="none"
        document.getElementsByClassName("rss-count")[0].style.color="#7b7b7b"
        document.getElementsByClassName("tweet-msg")[0].style.color="#7b7b7b"
        this.setState({cntnum:e});
      }
   // }
  }
  handleFormtext (data) {
    savedData = false
    this.setState({
      saveAllData: data
    })
    //this.closePostCreatePopup()
  }
  readMore () {
    var rss_Readmore_text = '';
    var readmore_link  = '';
    rss_Readmore_text =  document.getElementById('text-rss');
    readmore_link = document.getElementById('readmorelink');
    
    readmore = !readmore
    if (readmore == true) {
      rss_Readmore_text.classList.add("text-read-more-feed-rss");
      rss_Readmore_text.classList.remove("text_readmore");
      
     // readmore_link.classList.remove("hide");
    } else {
    
      rss_Readmore_text.classList.add("text_readmore");
      rss_Readmore_text.classList.remove("text-read-more-feed-rss");
     // readmore_link.classList.add("hide");
    }
  }
  renderRssForm (e) {
    return( 
      <CuratedCommentForm
      commentFormText={this.handleFormtext.bind(this)}
      countnum={this.wordCountForm.bind(this)}
    />
    )
  }
  componentWillMount()
  {
    countnum=0;lastCursorIndex=0;txtarea=0;start=0;end=0;
    this.setState({cntnum:''});
  }
  componentDidMount(){ 
    document.getElementById('rssFullDesc').innerHTML = this.props.alldata.description;
    this.setState({
      header_height : document.getElementsByClassName('rss-title')[0].offsetHeight + 15
    })
  }
  render () {
    var img = ''
    if (this.props.alldata.image !== null) {
      img = this.props.alldata.image
    }
   // var striptags = require('striptags');
    var rd_title = this.props.alldata.title.replace(new RegExp('&amp;', 'g'),'&');
    var rd_descripation =  this.props.alldata.description;
    var rd_source = this.props.alldata.permalink;

    //Check element for apply height to popup container
    if(document.getElementsByClassName('rss-title')[0]){
    let header_height = document.getElementsByClassName('rss-title')[0].offsetHeight + 15;
    let rssPopupScrollbar = {
      maxHeight: `calc(100vh - ${this.state.header_height}px)`
    }
    if(document.querySelectorAll('.rss_text .popupDescSection')[0]){
      document.querySelectorAll('.rss_text .popupDescSection')[0].setAttribute("style","height:"+rssPopupScrollbar.maxHeight);
    }
    }
    return (
      <div className='rss_popup' id='rss_popup'>
        <div className='inner-rss-popup clearfix'>
          <div className='rss_text'>
          <div className='rss-title'>
            <div className='visit-source-link-wrapper'><a target='_blank' href={rd_source} className='visit-source-link'><i class="material-icons">launch</i> Visit source</a></div>
            <h2>{rd_title}</h2>
          </div>
          <PopupWrapper>
          <div className='rss-popup-content-wrapper'>
          <div className='clearfix'>
          {img
            ? <div className='banner_img'>
              <ImageLoader
                src={img}
                preloader={PreLoaderMaterial}
                className='thumbnail'
                >
                <div className='image-load-failed'>
                  <i class='material-icons no-img-found-icon'></i>
                </div>
              </ImageLoader>
            </div>
            : ''}
            <div className='text_readmore' id='text-rss'>
              <div id='rssFullDesc' className='rssFullDesc' />
           </div>
          
          </div>
          <div className='add_comment_box'>
            {this.renderRssForm(this)}
            <div className='rss_share'>
              <div>
                <div className='tweet-msg'>
                  {this.state.msg}
                </div>
                <MergeJobsRssSharePopup
                  currData={this.props.alldata}
                  shareFrom={'rss'}
                  comment={this.state.saveAllData}
                  accounts={this.props.accounts}
                />
                  {/* <CuratedSharePopup
                    directShare={false}
                    liuserid={this.props.liuserid}
                    fbuserid={this.props.fbuserid}
                    comment={this.state.saveAllData}
                    alldata={this.props.alldata}
                    location={this.props.location}
                    closeRssPopup={this.props.closeRssPopup}
                  /> */}
                  <div className="rss-count">
                    {this.state.cntnum}
                  </div>
                        
              </div>
            </div>
          </div>
          </div>
          </PopupWrapper>
          </div>
        </div>
      </div>
    
   )
  }
}
