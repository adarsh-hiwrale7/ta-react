import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Field, reduxForm } from 'redux-form';
import { renderField } from '../../Helpers/ReduxForm/RenderField'
import { reset } from 'redux-form'
import MentionTextArea from '../../Helpers/ReduxForm/MentionTextArea'
import * as tagsActions from '../../../actions/tagsActions'
var lastCursorIndex,end ,start,txtarea;
var CommentString ='';
const required = value => value
    ? undefined
    : 'Required';
class CuratedCommentForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
        }
    }
    componentWillMount(){
        this
        .props
        .tagsActions
        .fetchUserTags()
    }

    commentText(e){
        this.wordCount(e)
        this.props.commentFormText(e)
    }
    /**
     * count words
     * @param {*} e 
     */
    wordCount(e){
        lastCursorIndex=document.getElementsByName('twitter_limit_count_9')[0].selectionStart+1;
        txtarea = document.getElementsByName('twitter_limit_count_9')[0];
        start = txtarea.selectionStart;
        end = txtarea.selectionEnd;
        this.props.countnum(start)
    }
    render(){
        let userTags = this.props.userTags;
        return(
        <div> 
              <form role="form"  >
                    <div className="form-row rss_comment_form clearfix user-tag-editor">
                        <label>Say something</label>
                        <Field
                            placeholder="Say something"
                            component={MentionTextArea}
                            type='textarea'
                            name='twitter_limit_count_9'
                            className='cmt-textbox'
                            onChange={select=>this.commentText(select)}
                            userTags={userTags}
                        />
                       </div>
                    </form>
                </div>
        ) 
    }
}
const afterSubmit = (result, dispatch) => {
    dispatch(reset('CuratedCommentForm'))
  }
  function mapStateToProps (state) {
    return {
        userTags: state.tags.userTags
    }
  }
  
  function mapDispatchToProps (dispatch) {
    return {
        tagsActions: bindActionCreators(tagsActions, dispatch)
    }
  }
  let connection = connect(mapStateToProps, mapDispatchToProps)
  
  var reduxConnectedComponent = connection(CuratedCommentForm)
  
  export default reduxForm({
    form: 'CuratedCommentForm',
    onSubmitSuccess: afterSubmit
  })(reduxConnectedComponent)