
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as cultureActions from '../../actions/cultureActions';
import PreLoader from "../PreLoader";
import CreatePostForm from "../Feed/CreatePostForm"
import Slider from '../Chunks/ChunkReactSlick'


class ValueAI extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            cultureValueList: [],
            valueTitle: '',
            valueDes: '',
            valueId: null,
            valueLogo:null,
            Slider:null

        }
    }
    componentDidMount() {
        // if (this.props.culture.cultureValueList.length == 0) {
        //     this.props.cultureActions.fetchValuesList();
        // }
        this.setState({
            cultureValueList: this.props.culture.cultureValueList,
        })
    }

    componentWillMount(){

        Slider().then(slider=>{
            this.setState({
              Slider:slider
            })
          })
    }

    componentWillReceiveProps(newProps) {
        if (this.props.aiValue !== newProps.aiValue) {

            if ((Object.keys(newProps.culture.cultureValueList).length > 0)) {
                newProps.culture.cultureValueList.map((value, key) => {
                    if (value.title.toLowerCase() === newProps.aiValue.toLowerCase()) {
                        this.setState({
                            valueDes: value.description,
                            valueId: value.id,
                            valueTitle: value.title,
                            valueLogo:value.value_image

                        })
                        return;

                    }
                })
            }
        }
    }

    addValue = (value) => {
        this.setState({
            valueTitle: value.title,
            valueDes: value.description,
            valueId: value.id,
            valueLogo:value.value_image
        })
    }
    backPopUp = () => {
        this.props.backPopUp();
    }

    valueSubmit(){
        this.props.change('valueAI',this.state.valueId)
        CreatePostForm.onSubmit
    }
    render() {
        const settings = {
            dots: false,
            speed: 500,
            slidesToShow: 3,
            slidesToScroll: 1,
            arrows: true,
            infinite: false,
            slickGoTo: 2,
            responsive: [
              {
                breakpoint: 768,
                settings: {
                  slidesToShow: 3,
                  slidesToScroll: 1,
                  infinite: false,
                }
              },
              {
                breakpoint: 767,
                settings: {
                  slidesToShow: 2,
                  slidesToScroll: 1
                }
              }
            ]
          }
        if (this.props.loader || this.props.culture.cultureLoader) {
            return (
                
                <div className="popupDescSection loader">
                    <div className="loader-grey-line selectValueMainBlock" /> 
                    <div className="cf aiValueSuggestionBlockWrapper">
                        <div className="aiValueSuggestionBlock">
                        <div className="loader-grey-line selectValueBlock" /> 
                        </div>
                        <div className="aiValueSuggestionBlock">
                        <div className="loader-grey-line selectValueBlock" /> 
                        </div>
                        <div className="aiValueSuggestionBlock">
                        <div className="loader-grey-line selectValueBlock" /> 
                        </div>
                    </div>
                    </div>
            )
        }
         else {
            return (
            <div>
                <div className="aiValueSection">
                    <div className="popupDescSection">
                    <div className="aiValueTitle">Values</div>

                        {this.state.valueDes ?
                        <div className="aiValueBlock">
                            <div className="aiValueIconTitle">{this.state.valueTitle}</div>
                            <div className="aiValueBlockInner cf">
                            <div className="aiValueIconCol">
                                <div class="aiValueIconWrapper"><img src={this.state.valueLogo?this.state.valueLogo:"/img/ai-value-icon.png"} alt="ai-value-icon" /></div>
                            </div>
                            <div className="aiValueContentCol">
                                {/* <h2>{this.state.valueTitle}</h2> */}
                                <p>{this.state.valueDes ? this.state.valueDes : 'Please select a desired value'}</p>
                            </div>
                            </div>
                        </div>
                        :
                        <div>
                        {/* <div className="aiValueBlock no-value-selected">VisAi needs your help!</div>
                        <div className="aiValueBlock no-value-selected">VisAI thinks you will make a better choice for values on this post.</div> */}

                        <div> 
                        <div className="aiValueBlock">
                            <div className="aiValueIconTitle">VisAi needs your help!</div>
                            <div className="aiValueBlockInner cf">
                            <div className="aiValueIconCol">
                                <div class="aiValueIconWrapper"><img src="/img/ai-no-value-icon.svg" alt="ai-value-icon" /></div>
                            </div>
                            <div className="aiValueContentCol">
                                <p>VisAI thinks you will make a better choice for values on this post.</p>
                            </div>
                            </div>
                        </div>
                        </div>

                        </div>
                        }
                        <div className="aiValueSuggestionBlockSection">
                            <div className="aiValueTitle">Suggested values</div>

                        {/* slider start */}
            <div className="aiValueSuggestionBlockWrapper cf">
                {(Object.keys(this.props.culture.cultureValueList).length > 0) && this.state.Slider?
                    <this.state.Slider.default {...settings} ref={slider => (this.slider = slider)}>
                    {
                        this.props.culture.cultureValueList.map((value, key)=>{
                        return(
                            <div className={this.state.valueTitle == value.title?"aiValueSuggestionBlock active":"aiValueSuggestionBlock"} key={key}>
                            <div className="aiValueSuggestionBlockInner cf" onClick={this.addValue.bind(this, value)}>
                                <div className="aiValueSuggestionBlockImage"><img src={value.value_image?value.value_image:"/img/ai-value-icon.png"} alt="ai-value-icon" /></div>
                                <div className="aiValueSuggestionBlockTitle">{value.title}</div>
                            </div>
                        </div>
                        )
                    })
              } 
          </this.state.Slider.default>:''}
          </div>
        {/* slider end */}

                            <div class="clearfix action-buttons">
                                <div class="bottom-right-buttons clearfix" id="button-right">
                                    <div class="btn btn-primary" onClick={this.backPopUp.bind(this)}>Back</div>
                                    <button
                                        className="btn btn-primary createPostBtn"
                                        type="submit"
                                        onClick={this.valueSubmit.bind(this)}
                                    >
                                      {this.state.valueId == null ? 'Skip' : 'Publish'}  
                                    </button>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            )
        }
    }
}


const mapStateToProps = (state) => ({
    culture: state.culture
})

const mapDispatchToProps = (dispatch) => ({
    cultureActions: bindActionCreators(cultureActions, dispatch)


})

export default connect(mapStateToProps, mapDispatchToProps)(ValueAI)