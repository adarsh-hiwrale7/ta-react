// collapse css
import { IndexLink, Link } from 'react-router';
import * as utils from '../../utils/utils';
import PerfectScrollbar from '../Chunks/ChunkPerfectScrollbar'
class TrendingTagsPanel extends React.Component {
  constructor(props) {
    super(props)
    this.state={
      perfectScrollbar:null
    }
  }
  clickTrendingTags(tagsName){

  }
  componentWillMount () {
    var me = this
    let scrollbarSelector = '.trending-tag-container';
    
      PerfectScrollbar().then(scrollbar=>{
          me.setState({ perfectScrollbar: scrollbar},()=>{
            if(document.querySelectorAll(scrollbarSelector).length>0){
              const ps = new this.state.perfectScrollbar.default(scrollbarSelector, {
                wheelSpeed: 2,
                wheelPropagation: true,
                minScrollbarLength: 20
              });
            }
          })
        })
  }
  render() {
    var hashPath = ''
    if(this.props.currentPath[2] == 'my'){
      hashPath = this.props.currentPath[2]+'/' + this.props.currentPath[3]
    }
    else if((this.props.currentPath[1] == "search") && (this.props.currentPath[2]=="assets")){
      hashPath = this.props.currentPath[2]+'/hash'
    }
    else if (this.props.currentPath[2] == 'custom'){
      hashPath = this.props.currentPath[2]+"/"+ this.props.currentPath[3]+"/"+this.props.currentPath[4]
    }
    else{
      hashPath = this.props.currentPath[2] + '/' + this.props.currentPath[3]
    }
    return (
      <div>
        {/* loader */}
        {this.props.listing && this.props.listing.loading==true && this.props.listing.noData ==false?
           <div>
           <div className="trending-tag-container">
                     <div className="inner-trending-tag">
                           <div className="trending-header">
                             <div className="loader-grey-line loader-line-height loader-line-radius"> </div>
                           </div>
                           <div className="tranding-body"> 
                             <div className="inner-trending-body">
                                 <div className="text-part">
                                      <p className= "loader-grey-line loader-line-height loader-line-space loader-line-radius"> </p> 
                                      <p className= "loader-grey-line loader-line-height loader-line-space loader-line-radius"> </p>
                                      <p className= "loader-grey-line loader-line-height loader-line-space loader-line-radius"> </p>
                                      <p className= "loader-grey-line loader-line-height loader-line-space loader-line-radius"> </p>
                                      <p className= "loader-grey-line loader-line-height loader-line-space loader-line-radius"> </p>
                                      <p className= "loader-grey-line loader-line-height loader-line-space loader-line-radius"> </p>
                                 </div>
                             </div>
                           </div>
                     </div>
                 </div>
           </div>
          // loader end
        :
           <div className="trending-tag-container">
                  <div className="inner-trending-tag">
                        <div className="trending-header">
                            <h2>TRENDING</h2>
                             
                        </div>
                        <div className="tranding-body"> 
                          <div className="inner-trending-body">
                              <div className="text-part">
                                 {
                                     typeof this.props.tagsData !== "undefined" && this.props.tagsData !== false ? 
                                      (Object.keys(this.props.tagsData).length > 0) ? 
                                         this.props.tagsData.map((trendingTags, i) => (
                                            <p key={i}>
                                              <Link
                                                to={ this.props.currentPath[1] == "assets" ? '/feed/assets/hash#'+trendingTags.tag_name : 
                                                '/feed/'+hashPath+'#'+trendingTags.tag_name} 
                                              >
                                                  {'#'+trendingTags.tag_name} 
                                                  <span>{trendingTags.total} {trendingTags.total>1 ? "Mentions" : "Mention" }</span>
                                             </Link> 
                                           </p>
                                       
                                          ))
                                         
                                      : <p className="no-trending-tag">Nothing trending right now </p> 
                                  : <p className="no-trending-tag">Nothing trending right now </p> 
                                 }   
                              </div>
                          </div>
                        </div>
                  </div>
              </div>
        }
        </div>
           
 
      
    )
  }
}


module.exports = TrendingTagsPanel;