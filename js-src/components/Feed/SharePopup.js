import { bindActionCreators } from 'redux'
import Linkedin from '../Social/Linkedin'
import Twitter from '../Social/Twitter'
import Facebook from '../Social/Facebook'
import { connect } from 'react-redux'
import { Link } from 'react-router'
import Globals from '../../Globals'
import reactTooltip from '../Chunks/ChunkReactTooltip';
import ReactDOM from 'react-dom'
import PropTypes from 'prop-types'
import * as utils from '../../utils/utils'
import * as socialAccountsActions from '../../actions/socialAccountsActions'
import * as postsActions from '../../actions/feed/postsActions'
var enableSocialFlag = '';
export default class SharePopup extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      popupVisible: false,
      shareLink : '',
      tooltip: null,
    }
  }
  componentWillMount () {
    // add event listener for clicks
    document.addEventListener('click', this.handleClick, false)
    reactTooltip().then(reactTooltip => {
      this.setState({
          tooltip: reactTooltip 
      })
  })
  }
  componentWillUnmount () {
    // make sure you remove the listener when the component is destroyed
    document.removeEventListener('click', this.handleClick, false)
    this.setState({
      tooltip: null 
  })
  }
  handleClick = e => {
    if (!ReactDOM.findDOMNode(this).contains(e.target)) {
      // the click was outside your component, so handle closing here
      // console.log(ReactDOM.findDOMNode(this),"ane close thavnu 6")
      //  ReactDOM.findDOMNode(this).close();
    }
  }
  componentDidMount () {
    if(this.props.profile.profile.sharelink==1){
      fetch(Globals.API_ROOT_URL + `/post/credits?post_identity=${this.props.post.data.identity}`, {
      method: 'GET',
      headers: utils.setApiHeaders('get',{
          'Access-Control-Allow-Origin':'*',
              Accept: 'application/json',
          'Content-Type': 'application/json'
          }),
      })
      .then(response =>{
          return response.json() })
      .then( json =>{
          if(json.code == 200){
            this.setState({
                shareLink :json.data
            })
          }
          else{
            utils.handleSessionError(json)
            this.setState({
                shareLink : ''
            })
          }
      })
    }

  }

  editPostData(){

    //creatingNewPost method false the value of create in reducer
    this
      .props
      .postsActions
      .creatingNewPost()
    //pass the data in post
    this.props.onEditPostData();
  }
  onClose (id) {
    var d = document.getElementById('shar-popup-' + id)
    d.className += ' close-shareoverlay1'
    this.props.onShareLinkClick(id)
  }
/**
 * @author disha
 * call api after share is successfully done
 * @param {*} accountName
 * @param {*} id id of success post
 */
  shareOnSocialMedia(accountName,id,socialAccId=null){
    var objToSend={
      socialId:socialAccId.id,
      Localid:this.props.post.data.identity,
      social_platform:accountName,
      resource:'post',
      userId: this.props.profile.profile.identity
    }
    var ObjToAdd={
      'social_account_id':socialAccId.socialAccId
    }
    accountName=='Facebook'?
      Object.assign(objToSend,ObjToAdd):''
    this.props.socialAccountsActions.shareOnSocialMedia(objToSend)
  }
  closeSharePopupInFeed(){
    this.props.closeSharePopup()
  }
  render () {
    var media_type = 'application'
    var media_url, thumbnail_url = ''
    var linkToShare=''
    var videourl=0
    var media_size = 0
    var media_extension = 'file'
    var media_duration =0
    var postId=this.props.post.data.identity
    var message = typeof this.props.post.data.detail !== 'undefined'
      ? utils.convertSpecialCharacterToUnicode(utils.toUnicodetwo(this.props.post.data.detail))
      : ''
      var messageToShareOnTwitter=typeof this.props.post.data.detail !== 'undefined'
      ? this.props.post.data.detail
      : ''
    linkToShare=typeof this.props.post.data.LinkPreview !=='undefined'?
      this.props.post.data.LinkPreview.data.length>0?
      this.props.post.data.LinkPreview.data[0].url:'':''
    let assetData = this.props.post.data.assetDetail.data[0]
    var url = ''
    var linkedinUrl = ''
    let social = typeof this.props.accounts !== 'undefined'
      ? this.props.accounts
      : []

    this.allSocialChannels = Globals.EXTERNAL_SOCIAL.slice()
    var liniscompany, fbiscompany, twiscomapny = ''
    var pageid = {}
    var fbpageid,
      fbtoken,
      liavatar,
      fbavatar,
      twavatar,
      fbusername,
      fbuserid,
      fbpageavatar,
      twpageavatar,
      lipageavatar,
      fbpagename,
      twusername,
      twuserName,
      twotherusername,
      twotheruser,
      lipageName,
      lipagename,
      liusername,
      liuserName ,
      LinkedinSocialPageId,
      LinkedinSocialUserId= ''
    this.usersSocialChannels = []
    this.userSocialPageDetail = []
    social.map(s => {

      pageid = s.identity
      if (s.social_media == 'linkedin') {
        var liuserName = s.user_social.filter(function (v) {
          return v['key'] == 'name'
        })
        liuserName.map(username => {
          liusername = username.value
        })
        var useravatar = s.user_social.filter(function (v) {
          return v['key'] == 'avatar'
        })

        if (s.iscompany == 1) {
          liniscompany = s.iscompany
          LinkedinSocialPageId=s.identity
          var lipageName = s.user_social.filter(function (v) {
            return v['key'] == 'page_name'
          })
          lipageName.map(pagename => {
            lipagename = pagename.value
          })
          var useravatar = s.user_social.filter(function (v) {
            return v['key'] == 'avatar'
          })
          useravatar.map(userpic => {
            lipageavatar = userpic.value?userpic.value:"https://app.visibly.io/img/default-avatar.png"
          })
        } else {
          LinkedinSocialUserId=s.identity
          useravatar.map(userpic => {
            liavatar = userpic.value ? userpic.value : "https://app.visibly.io/img/default-avatar.png"
          })
        }
      }
      if (s.social_media == 'twitter') {
        var useravatar = s.user_social.filter(function (v) {
          return v['key'] == 'avatar'
        })

        if (s.iscompany == 1) {
          var twuserName = s.user_social.filter(function (v) {
            return v['key'] == 'name'
          })
          twiscomapny = s.iscompany
          var useravatar = s.user_social.filter(function (v) {
            return v['key'] == 'avatar'
          })

          useravatar.map(userpic => {
            twpageavatar = userpic.value
          })
          twuserName.map(username => {
            twotherusername = username.value
          })
        } else {
          var twuserName = s.user_social.filter(function (v) {
            return v['key'] == 'name'
          })
          twuserName.map(username => {
            twusername = username.value
          })
          useravatar.map(userpic => {
            twavatar = userpic.value ? userpic.value : "https://app.visibly.io/img/default-avatar.png"
          })
        }
      }
      if (s.social_media == 'facebook') {
        var fbuserName = s.user_social.filter(function (v) {
          return v['key'] == 'name'
        })
        fbuserName.map(username => {
          fbusername = username.value
        })
        var fbaccesstoken = s.user_social.filter(function (v) {
          return v['key'] == 'api_token'
        })
        if (s.iscompany == 1) {
          fbiscompany = s.iscompany
          var fbPageidvalue = s.user_social.filter(function (v) {
            return v['key'] == 'page_id'
          })
          var fbpageName = s.user_social.filter(function (v) {
            return v['key'] == 'page_name'
          })

          var useravatar = s.user_social.filter(function (v) {
            return v['key'] == 'avatar'
          })
          useravatar.map(userpic => {
            fbpageavatar = userpic.value?userpic.value:"https://app.visibly.io/img/default-avatar.png"
          })
          fbpageName.map(pagename => {
            fbpagename = pagename.value
          })
          fbPageidvalue.map(pageid => {
            fbpageid = pageid.value
          })
          fbaccesstoken.map(token => {
            fbtoken = token.value
          })
        } else {
          var fbuseridvalue = s.user_social.filter(function (v) {
            return v['key'] == 'user_id'
          })
          var useravatar = s.user_social.filter(function (v) {
            return v['key'] == 'avatar'
          })
          fbuseridvalue.map(pageid => {
            fbuserid = pageid.value
          })
          useravatar.map(userpic => {
            fbavatar = userpic.value ? userpic.value : "https://app.visibly.io/img/default-avatar.png"
          })
          fbaccesstoken.map(token => {
            fbtoken = token.value
          })
        }
      }
      var connectedSocialAccIndex = this.allSocialChannels.indexOf(
        s.social_media.toLowerCase()
      )
      if (connectedSocialAccIndex > -1) {
        let socialName = s.iscompany == 1 ? s.social_media.toLowerCase() + '-company' : s.social_media.toLowerCase()
        this.usersSocialChannels.push(socialName)
        // this.allSocialChannels.splice(connectedSocialAccIndex, 1) 
        // commneted this code becuase the social page is not comming in list  by Tejal
      }
    })
    if (typeof assetData !== 'undefined') {
      if (assetData.type == 'asset') {
        media_type = assetData.asset.data.media_type
        media_url = assetData.asset.data.media_url
        media_extension = assetData.asset.data.media_extension
        media_size=assetData.asset.data.media_size
        thumbnail_url = assetData.asset.data.thumbnail_url
        media_duration=assetData.asset.data.media_duration !== null ? assetData.asset.data.media_duration: 0
      } else {
        media_duration=assetData.media_duration !== null ? assetData.media_duration : 0
        media_type = assetData.media_type
        media_url = assetData.media_url
        media_extension = assetData.media_extension
        thumbnail_url = assetData.thumbnail_url
        media_size=assetData.media_size
      }
      var newmedia_type = ''
      var media_type_index = media_type.indexOf('/')
      if (media_type_index !== -1) {
        newmedia_type = media_type.substr(0, media_type_index)
      } else {
        newmedia_type = media_type.media_type
      }
      url = media_type !== 'image' && media_type !== 'video' ? media_url : ''
      newmedia_type == 'video' ? (videourl = 1) : ''
      linkedinUrl = media_type !== 'video' ? media_url : ''
      if (media_type == 'video') {
        linkedinUrl =
          'http://app.talentadvocate.com/api/linkedin_share?v_url=' + media_url
      } else {
        linkedinUrl = media_url
      }
    }
    return (
      <div>
        <div
          className='popup sharePopup'
          id={`shar-popup-${this.props.post.data.identity}`}
        >

          <nav className='nav-side'>
            <div className='parent  clearfix  nav-side-share-popup'>
              {/* this button is used outside close button  */}
             <button  className = "closeSharePopupPost hide" name = "close-share-popup " onClick ={this.closeSharePopupInFeed.bind(this)}>  close </button>
              <ul className='channels'>
              <li className = "channel" onClick = {this.editPostData.bind(this)}>
                <span className = "edit-post-icon">
                 <i class="material-icons"> create </i>
                </span>
               </li>
               {this.usersSocialChannels.indexOf('facebook') > -1
               ? <li className='channel'>
                      <Facebook
                        className=''
                        name='share-fb'
                        text='Facebook'
                        picture={media_url}
                        thumbnail_url={thumbnail_url}
                        linkToShare={linkToShare}
                        message={message}
                        accessToken={fbtoken}
                        image_url='https://app.visibly.io/img/default-avatar.png'
                        videourl={videourl}
                        userid={fbuserid}
                        shareFrom={'post'}
                        sharePostClick={this.props.sharePostClick}
                        successId={this.shareOnSocialMedia.bind(this,'Facebook',fbuserid)}
                        />
                    </li>:''}
                {this.usersSocialChannels.indexOf('twitter') > -1
                  ? <li className='channel'>
                    <span
                      data-tip={twusername}
                      data-for={'notConnectedSocialChannels'}
                      >
                      <Twitter
                        className=''
                        name='share-tw'
                        text='Twitter'
                        link={media_url}
                        message={messageToShareOnTwitter}
                        url={url}
                        assetData ={this.props.post.data.assetDetail}
                        media_duration={media_duration}
                        media_type={media_type}
                        size={media_size}
                        iscompany={0}
                        image_url={twavatar}
                        videourl={videourl}
                        sharePostClick={this.props.sharePostClick}
                        postId={postId}
                        resourceType='post'
                        />
                      {this.state.tooltip?
                      <this.state.tooltip
                        type='warning'

                        id='notConnectedSocialChannels'
                        effect = 'solid'
                        className = 'tooltipsharepopup'
                        />:''}
                    </span>
                  </li>
                  : ''}
                {this.usersSocialChannels.indexOf('linkedin') > -1
                  ? <li className='channel'>
                    <span
                      data-tip={liusername}
                      data-for={'notConnectedSocialChannels'}
                      >
                      <Linkedin
                        className=''
                        name='share-li'
                        text='Linkedin'
                        link={linkedinUrl}
                        message={message}
                        assetData ={this.props.post.data.assetDetail}
                        iscompany={0}
                        shareLink={this.state.shareLink}
                        videourl={videourl}
                        socialUserId={LinkedinSocialUserId}
                        thumbnail_url={thumbnail_url}
                        image_url={liavatar}
                        shareFrom={'post'}
                        postId={postId}
                        sharePostClick={this.props.sharePostClick}
                        />

                    </span>
                    {this.state.tooltip?
                      <this.state.tooltip
                        type='warning'
                        id='notConnectedSocialChannels'
                        effect = 'solid'
                        className = 'tooltipsharepopup'
                        />:''}
                  </li>
                  : ''}
                {fbiscompany == 1 &&
                  this.usersSocialChannels.indexOf('facebook-company') > -1
                  ? <li className='channel'>
                    <span
                      data-tip={fbpagename}
                      data-for={'notConnectedSocialChannels'}
                      >

                      <Facebook
                        className=''
                        name='share-fb'
                        text='Facebook'
                        assetData={this.props.post.data.assetDetail}
                        link={media_url}
                        message={message}
                        pageid={fbpageid}
                        accessToken={fbtoken}
                        videourl={videourl}
                        image_url={fbpageavatar}
                        sharePostClick={this.props.sharePostClick}
                        shareFrom='post'
                        successId={this.shareOnSocialMedia.bind(this,'Facebook',fbpageid)}
                        />
                      {this.state.tooltip?
                      <this.state.tooltip
                        type='warning'
                        id='notConnectedSocialChannels'
                        effect = 'solid'
                        className = 'tooltipsharepopup'
                        />:''}
                    </span>
                  </li>
                  : ''}
                {twiscomapny == 1 &&
                  this.usersSocialChannels.indexOf('twitter-company') > -1
                  ? <li className='channel'>
                    <span
                      data-tip={twotherusername}
                      data-for={'notConnectedSocialChannels'}
                      >
                      <Twitter
                        className=''
                        name='share-tw'
                        text='Twitter'
                        link={media_url}
                        message={messageToShareOnTwitter}
                        url={url}
                        size={media_size}
                        media_type={media_type}
                        iscompany={twiscomapny}
                        assetData ={this.props.post.data.assetDetail}
                        identity={pageid}
                        videourl={videourl}
                        image_url={twpageavatar}
                        postId={postId}
                        resourceType='post'
                        media_duration={media_duration}
                        sharePostClick={this.props.sharePostClick}
                        />
                      {this.state.tooltip?
                      <this.state.tooltip
                        type='warning'
                        id='notConnectedSocialChannels'
                        effect = 'solid'
                        className = 'tooltipsharepopup'
                        />:''}

                    </span>
                  </li>
                  : ''}
                {liniscompany == 1 &&
                  this.usersSocialChannels.indexOf('linkedin-company') > -1
                  ? <li className='channel'>
                    <span
                      data-tip={lipagename}
                      data-for={'notConnectedSocialChannels'}
                      >
                      <Linkedin
                        className=''
                        name='share-li'
                        text='Linkedin'
                        iscompany={liniscompany}
                        pageid={LinkedinSocialPageId}
                        message={message }
                        postId={postId}
                        shareLink={this.state.shareLink}
                        assetData ={this.props.post.data.assetDetail}
                        videourl={videourl}
                        link={linkedinUrl}
                        thumbnail_url={thumbnail_url}
                        image_url={lipageavatar}
                        shareFrom={'post'}
                        sharePostClick={this.props.sharePostClick}
                        />
                      {this.state.tooltip?
                      <this.state.tooltip
                        type='warning'
                        id='notConnectedSocialChannels'
                        effect = 'solid'
                        className = 'tooltipsharepopup'
                        />:''}
                    </span>

                  </li>
                  : ''}
                {enableSocialFlag == 1 && this.usersSocialChannels.indexOf('facebook') < 0 &&
                  this.usersSocialChannels.indexOf('linkedin') < 0 &&
                  this.usersSocialChannels.indexOf('twitter') < 0 && 
                  this.usersSocialChannels.indexOf('facebook-company') < 0 && 
                  this.usersSocialChannels.indexOf('twitter-company') < 0 && 
                  this.usersSocialChannels.indexOf('linkedin-company') < 0

                  ? <li className='channel noAccounts'>
                    <p>
                      <span>
                      No social accounts connected,<br />please update in <Link to='/settings/social' className='noAccountsSettingLink'><svg class="nav-icon" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path fill="none" d="M0 0h24v24H0V0z"></path><path class="nav-icon-path" d="M19.43 12.98c.04-.32.07-.64.07-.98 0-.34-.03-.66-.07-.98l2.11-1.65c.19-.15.24-.42.12-.64l-2-3.46c-.09-.16-.26-.25-.44-.25-.06 0-.12.01-.17.03l-2.49 1c-.52-.4-1.08-.73-1.69-.98l-.38-2.65C14.46 2.18 14.25 2 14 2h-4c-.25 0-.46.18-.49.42l-.38 2.65c-.61.25-1.17.59-1.69.98l-2.49-1c-.06-.02-.12-.03-.18-.03-.17 0-.34.09-.43.25l-2 3.46c-.13.22-.07.49.12.64l2.11 1.65c-.04.32-.07.65-.07.98 0 .33.03.66.07.98l-2.11 1.65c-.19.15-.24.42-.12.64l2 3.46c.09.16.26.25.44.25.06 0 .12-.01.17-.03l2.49-1c.52.4 1.08.73 1.69.98l.38 2.65c.03.24.24.42.49.42h4c.25 0 .46-.18.49-.42l.38-2.65c.61-.25 1.17-.59 1.69-.98l2.49 1c.06.02.12.03.18.03.17 0 .34-.09.43-.25l2-3.46c.12-.22.07-.49-.12-.64l-2.11-1.65zm-1.98-1.71c.04.31.05.52.05.73 0 .21-.02.43-.05.73l-.14 1.13.89.7 1.08.84-.7 1.21-1.27-.51-1.04-.42-.9.68c-.43.32-.84.56-1.25.73l-1.06.43-.16 1.13-.2 1.35h-1.4l-.19-1.35-.16-1.13-1.06-.43c-.43-.18-.83-.41-1.23-.71l-.91-.7-1.06.43-1.27.51-.7-1.21 1.08-.84.89-.7-.14-1.13c-.03-.31-.05-.54-.05-.74s.02-.43.05-.73l.14-1.13-.89-.7-1.08-.84.7-1.21 1.27.51 1.04.42.9-.68c.43-.32.84-.56 1.25-.73l1.06-.43.16-1.13.2-1.35h1.39l.19 1.35.16 1.13 1.06.43c.43.18.83.41 1.23.71l.91.7 1.06-.43 1.27-.51.7 1.21-1.07.85-.89.7.14 1.13zM12 8c-2.21 0-4 1.79-4 4s1.79 4 4 4 4-1.79 4-4-1.79-4-4-4zm0 6c-1.1 0-2-.9-2-2s.9-2 2-2 2 .9 2 2-.9 2-2 2z"></path></svg> Settings</Link>
                      </span>
                    </p>
                  </li>
                  : ''}
              </ul>

            </div>
          </nav>
          <div className='pointer' />
          <div
            className='shareoverlay'
            onClick={this.onClose.bind(this, this.props.post.data.identity)}
          />
        </div>

      </div>
    )
  }
}


function mapStateToProps (state) {
  return {
    profile: state.profile,
    notificationData: state.notification_data,
    posts: state.posts,
  }
}
function mapDispatchToProps (dispatch) {
  return {
    socialAccountsActions: bindActionCreators(socialAccountsActions, dispatch),
    postsActions: bindActionCreators(postsActions, dispatch),

  }
}
let connection = connect(mapStateToProps, mapDispatchToProps)
module.exports = connection(SharePopup)
