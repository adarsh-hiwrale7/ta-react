import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {browserHistory} from 'react-router'
import { Link } from 'react-router'
import PreLoader from '../PreLoader'
import SidebarNarrow from '../Layout/Sidebar/SidebarNarrow'
import $ from '../Chunks/ChunkJquery'
import * as utils from '../../utils/utils'
import ChunkJqueryDropdown from '../Chunks/ChunkJqueryDropdown'
import Post from './Post'
import * as notificationActions from '../../actions/notificationActions'
import Header from '../Header/Header';
import FeedNav from './FeedNav';
import * as feedActions from '../../actions/feed/feedActions'
import Globals from '../../Globals'
import * as userActions from '../../actions/userActions'
import ReactDOM from 'react-dom';
import TagPopup from './TagPopup'
import moment from '../Chunks/ChunkMoment'
 



var calendarUrl='';
var feedUrl='';
var selectedDepartmentData;
var selectedUserData;
class PostNotification extends React.Component {
  constructor (props) {
    super(props)
    this.state = {}
    this._handleCloseEvent = this
      ._handleCloseEvent
      .bind(this);
      this.hashChange = this
        .hashChange
        .bind(this);
  }
  hashChange() {
    document.addEventListener('scroll', this.onPostsScroll)
    // let me = this
    // me.openingPostCreationFromCampaign()

    moment().then(moment => {
      this.setState({moment: moment})
    })

    var path = utils
      .removeTrailingSlash(this.props.location.pathname)
      .split('/')

    this
      .props
      .feedActions
      .resetPosts()


    if (typeof path[1] === 'undefined' || path[1] == '' || typeof path[2] === 'undefined' || path[2] == '') {
      browserHistory.push('feed/default/live')
    } else {
      this.setState({currFeed: path[1], currFeedPosts: path[2], currPage: 1})
      this
        .props
        .feedActions
        .fetchDefaultLiveHash(this.props.location.hash.substr(1), 1, 10)

    }
    calendarUrl=`/feed/${path[2]}/calendar/${path[3]}`
    feedUrl=`/feed/${path[2]}/${path[3]}`
  }

  componentDidMount () {
    let roleName =JSON.parse(localStorage.getItem('roles')) !== null? JSON.parse(localStorage.getItem('roles'))[0].role_name:''
    
    // this.openingPostCreationFromCampaign()
    // $().then(jquery => {
    //   var $ = jquery.$
    //   me.setState({jQuery: $})
    //   ChunkJqueryDropdown().then(jqueryDropdown => {})
    // })

    var location = utils
      .removeTrailingSlash(this.props.location.pathname)
      .split('/')
    this.props.notificationActions.fetchSelectedPost(location[2])
    ReactDOM
      .findDOMNode(this)
      .addEventListener('click', this._handleCloseEvent);
      window.addEventListener("hashchange", this.hashChange, false);

  }
  componentWillMount () {
    $().then(jquery => {
      var $ = jquery.$

      ChunkJqueryDropdown().then(jqueryDropdown => {})
    })
  }
  closepopup() {

    // let pop_overlay = document.getElementById("tag-popup-overlay");
    let popup_wrapper = document.getElementById("pop-up-tooltip-wrapper");
    // pop_overlay.className = '';
    popup_wrapper.className += ' hide';
  }
  updateText(id, type, e) {
    this.setState({overlay_flag: true})
    var isUserFound = false;
    if(type=="department"){
      this.props.departments.list.length>0?
      this.props.departments.list.map((DepartmentDetail,i)=>{
        if(DepartmentDetail.identity==id){
          selectedDepartmentData = DepartmentDetail;
        }
      }):''
    }
    else{
      this.props.usersList.length>0?
      this.props.usersList.map((userDetail,i)=>{
        if(userDetail.identity==id){
          selectedUserData = userDetail;
          isUserFound = true;
        }
      }):''
      if(isUserFound==false){
        selectedUserData ="No user found"
      }
    }
    const domNode = ReactDOM.findDOMNode(e.target);
    let coords = domNode.getBoundingClientRect();
    let coords_top = coords.top + pageYOffset;
    let popup_wrapper = document.getElementById("pop-up-tooltip-wrapper");
    let coords_left = 0,
      coords_area = 0,
      new_class_comb = '';
    let pop_holder = document.getElementById("pop-up-tooltip-holder");
    let popupparent = document.getElementById('department-popup');
    if (popupparent != null && pop_holder!=null) {
      pop_holder.className = 'departmentpopup';
    } else {
    }
    if (screen.width >= 768) {
      coords_left = coords.left + pageXOffset + (coords.width / 2);
    } else {
      coords_left = coords.left + pageXOffset;
      coords_area = coords_left + 472;
      if (coords_area < screen.width) {
        new_class_comb = 'popup-left';
      } else {
        new_class_comb = 'popup-right';
      }

    }
    if (e.nativeEvent.screenY < 400) {
      new_class_comb += ' bottom';
      if (e.target.className == "thumbnail") {
        new_class_comb += ' thumbnai_popup';
      }
    } else {
      new_class_comb += ' top';
    }

    popup_wrapper.className = new_class_comb;
    this.setState({
      popupTop: coords_top,
      popupLeft: coords_left,
      popupdisplay: 'block',
      tagType: type,
      tagTypeClass: (type === "department"
        ? 'departmentpopup'
        : '')
    });
    // let pop_overlay = document.getElementById("tag-popup-overlay");
    // pop_overlay.className = 'show';

  }
  renderLoading() {
    return (
        <div className="feed-loader-wrapper">
            <div className = "curated-loader-container Job-loader-container">
              <div className="curated-loader-inner-container">
                  <div className = "curated-header clearfix">
                      <div className = "img-avatar"> </div>
                      <div className = "name-header">
                          <div className = "name-curated loader-line-height"> </div>
                          <div className = "date-curated loader-line-height"> </div>
                      </div>
                      </div>
                    <div className = "clearfix curated-block-container">
            
                          <div className = "inner-curated-block">
                              <div className = "curated-decripation">
                                <div className = "clearfix date-and-share-block">
                                      <div className ="cureted-date loader-grey-line loader-line-space loader-line-radius"> </div>
                                      <div className ="cureted-share loader-grey-line loader-line-space loader-line-radius"> </div>
                                </div>
                                  <div className = "cureted-decp-block4 loader-grey-line loader-line-space loader-line-radius loader-line-height"> </div>
                                  <div className = "cureted-decp-block3 loader-grey-line loader-line-space loader-line-radius loader-line-height"> </div>
                                  <div className = "cureted-decp-block1 loader-grey-line loader-line-space loader-line-radius loader-line-height"> </div>
                                  <div className="cureted-decp-line-padding"></div>
                              </div>
                              <div className = "curated-img-part loader-grey-line"></div>                                      
                    </div> 
            </div>
            </div>
          </div>
          </div>
    )
  }
_handleCloseEvent(e) {
    if (!document.getElementById('pop-up-tooltip-wrapper').contains(e.target)) {
      if (this.state.overlay_flag) {
        document.getElementById('pop-up-tooltip-wrapper').className = 'hide';
        this.setState({ overlay_flag: false })
      }
    }
  }
  render () {
    let user = this.props.users;
    let userDetails = user.userDetails;
    let department = this.props.department;
    let isFetching = user.isFetching;

    var newLocation = utils
    .removeTrailingSlash(this.props.location.pathname)
    .split('/')
    var posts = this.props.notificationData.postSelectedData
    return (
      <section className='postnoti'>
        <div id='navdraw' className='has-sib'>
          <Header
            title="Post"
            location={this.props.location}
            nav={FeedNav}
          />
        </div>
        {/* {this.fetchpost(newLocation[2])} */}
        <div id='content'>
          <div className='page'>
            <div id='feed'>

               {this.props.notificationData.notification_error==true?
               <div className="no-longer-authorised-wrapper-box">
               <div className="no-longer-authorised-wrapper">
               <div className="no-longer-authorised-icon">
               <i class="material-icons">not_interested</i>
               </div>
              <div className='no-longer-authorised-block'>
                {this.props.notificationData.postSelectedData}
              </div>
              </div>
              </div>
            :
            <div className='internal'>

                <div className='esc-sm'>
                  <div className='content'>
                    <div id='post-list-wrapper'>
                    {this.props.notificationData.loading_post==true?this.renderLoading():
                       posts.map((post, index) => {
                         if(post.post.data.is_hide==1){
                            return(
                              <div>
                                <div className='no-data-block'>
                                  This post is no longer available.
                                </div>
                              </div>
                            )
                         }else{
                            return (
                              <Post
                                key={index}
                                index={index}
                                feed={post.post.data.type}
                                notificationcall='true'
                                locationInfo={this.props.location}
                                details={post}
                                location={newLocation[2]}
                                updateText={this.updateText.bind(this)}
                                handleCloseEvent = {this._handleCloseEvent.bind(this)}
                              />
                            )
                         }
                      })}
                    </div>
                  </div>
                </div>
              </div>
               }
              <div id="pop-up-tooltip-holder">
                <div id="pop-up-tooltip-wrapper" className={this.state.tagTypeClass}>
                  <div
                    className="pop-up-tooltip"
                    style={{
                    top: this.state.popupTop,
                    left: this.state.popupLeft,
                    display: this.state.popupdisplay
                  }}>
                    <span className='popup-arrow'></span>
                    <TagPopup
                      userDetails={selectedUserData}
                      isFetching={isFetching}
                      isDept={this.state.tagType}
                      department={selectedDepartmentData}/>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    )
  }
}
function mapStateToProps (state) {
  return {
    notificationData: state.notification_data,
    users: state.users,
    tags: state.tags,
    departments:state.departments,
    usersList : state.usersList.userList.data
  }
}

function mapDispatchToProps (dispatch) {
  return {
    notificationActions: bindActionCreators(notificationActions, dispatch),
    userActions: bindActionCreators(userActions, dispatch),
    feedActions: bindActionCreators(feedActions, dispatch),
  }
}

module.exports = connect(mapStateToProps, mapDispatchToProps)(PostNotification)
