import Collapse, { Panel } from 'rc-collapse'; // collapse
// collapse css
import { browserHistory, Link } from 'react-router';
import * as utils from '../../utils/utils';
var search;
var obj2={}
class FeedNav extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      customfeedData:[],
      filter_feed:[],
      searchFlag:"hide",

    }
  }
componentWillMount(){
  this.setState({customfeedData:this.props.customFeed})
}
componentWillReceiveProps(nextProps){
     if(this.props.location.pathname != nextProps.location.pathname){
         window.scrollTop = 0;
     }
    this.setState({
      customfeedData:nextProps.customFeed,
      filter_feed:nextProps.customFeed
    })
        if(document.getElementById("searchFeedText") !==null){
         if( document.getElementById("searchFeedText").value !== null  && document.getElementById("searchFeedText").value!==''){
           this.search_feed(null,document.getElementById("searchFeedText").value)
         }
        }
  }

  onSearchClick(){
    if(this.state.searchFlag=="hide"){
      this.setState({
        searchFlag:''
      })
    }
    else{
      this.setState({
        searchFlag:"hide"
      })
    }
  }

  render() {
    let assets_feed_class = '';
    let curated_feed_class = '';
    var job_curated = '';
    var my_post='';
    var custom_feed='active';
    if(this.props.currFeed=='curated')
    {
      curated_feed_class='active';
    }
    else if(this.props.currFeed=='feed/curated/job'){
      job_curated = 'active'
    }
    else if(this.props.currFeed=='my')
    {
        my_post='active'
    }
    let roleName =JSON.parse(localStorage.getItem('roles')) !== null? JSON.parse(localStorage.getItem('roles'))[0].role_name:''
    return (
      <nav className='nav-side'>
        <Collapse
          accordion={false}
          onChange={this.onChange}
          defaultKey={'1'}
          defaultActiveKey={['1', '2', '3', '4','5','6']}
        >
          {/* render All feed */}
          {
          <div class="custom-sidebar-feed-wrapper">
            <div className="custom-feed-title"><div className="custom-feed-toggle active"><i class="arrow"></i>Feeds</div> 
            {roleName=='super-admin' || roleName =='admin'?
            !this.props.location.pathname.includes('rss') && !this.props.location.pathname.includes('jobs') ? 
                  <div className="add-circle-btn"><i class="material-icons" onClick={this.props.createFeedOpen}>add_circle_outline</i></div>
             : '':''}
            </div>
            <div class="custom-feed-list">
            {this.props.customFeed!==undefined && Object.keys(this.props.customFeed).length!==0 ?
              this.renderFeed(): 
                <div>
                  <div class="custom-feed-toggle loader-grey-line loader-line-space loader-line-height loader-sidebar-width loader-line-radius active"></div>
                    <div class="custom-feed-toggle loader-grey-line loader-line-space loader-line-height loader-sidebar-width loader-line-radius active"></div>
                    <div class="custom-feed-toggle loader-grey-line loader-line-space loader-line-height loader-sidebar-width loader-line-radius active"></div>
                    <div class="custom-feed-toggle loader-grey-line loader-line-space loader-line-height loader-sidebar-width loader-line-radius active"></div>
                    <div class="custom-feed-toggle loader-grey-line loader-line-space loader-line-height loader-sidebar-width loader-line-radius active"></div>
                </div>
              }
            </div>
            </div>
          }
          
         {/* for rss */}
         <div className="curated-link">
               {/* {this.rendercuratedByHash()} */}
         </div>
         {/* for jobs */}
          <div className="curated-link">
             {/* {this.rendercuratedJobByHash()} */}
          </div>

          {/* for asset in feed */}
          {(this.props.hastag_name) ?
            <Panel className={assets_feed_class} header='ASSETS' key={'4'}>
              {this.renderAssetByHash()}
          </Panel>:''}

          {/* my post */}
          <Panel className={my_post} header='My Posts' key={'5'}>
            {this.renderMyPostFeedByHash()}
          </Panel>
        </Collapse>
      </nav>
    )
  }

search_feed(e,data){
  if(e!==null){
    if(e.target.value!=="" && this.state.filter_feed!==undefined)
    {
      var i=0;
      search=e.target.value.toLowerCase();
      var tempobj=this.state.filter_feed.filter(function(temp){
        i=i+1;
        if(temp.name.toLowerCase().includes(search)){
          return true;
        }
      })
      this.setState({customfeedData:tempobj})
    }
    else{
      this.setState({customfeedData:this.state.filter_feed})
    }
  }
  else if(data!==undefined&& data!==null && data!==''){
    if(data!=="" && this.state.filter_feed!==undefined)
    {
      var i=0;
      search=data.toLowerCase();
      var tempobj=this.state.filter_feed.filter(function(temp){
        i=i+1;
        if(temp.name.toLowerCase().includes(search)){
          return true;
        }
      })
      this.setState({customfeedData:tempobj})
    }
    else{
      this.setState({customfeedData:this.state.filter_feed})
    }
  }
  }
  closeFeed(feedId){
    this.props.closeFeed(feedId);
  }
  updateFeed(feedId){
    if(this.props.location.pathname=="/feed/rss" || this.props.location.pathname=="/feed/jobs" || this.props.location.pathname.includes('post')){
      if(feedId=="rpPDp"){
        browserHistory.push({
          pathname : `/feed/default/live`,
          state:{
            fromRssJob : true,
            feedId:feedId
          }
        })
      }else{
        browserHistory.push({
          pathname : `/feed/custom/live/${feedId}`,
          state:{
            fromRssJob : true,
            feedId:feedId
          }
      })
    }
    }else{
        this.props.updateFeedAction(feedId);
    }
  }
  renderFeed(){
    let selectedFeed = this.props.currFeed
    const hashTag = this.props.hash;
    let selectedFeedPosts;
    var FeedTypeIcon;
    var path = utils
    .removeTrailingSlash(this.props.location.pathname)
    .split('/');

    // set selection flag to add active class in feed selection 
    //if page is calling from notification detail then to select feed 
    if(path[1]!== undefined && path[1] == 'post'){
      const notiPostId = this.props.notificationData.length >0? this.props.notificationData[0].post.data.feed_identity: ''
      const defaultFeedId =  this.props.profile.email !== null? this.props.profile.default_feed :''
      selectedFeedPosts = notiPostId !== '' ? notiPostId : selectedFeedPosts;
      selectedFeed = defaultFeedId == notiPostId ? 'default' : this.props.currFeed
    }
    if(path[4]!==undefined){
       selectedFeedPosts = path[4];
    }
    var loginUserId=this.props.profile !== undefined ? this.props.profile.identity !== null ? this.props.profile.identity:'':''
    let roleName =JSON.parse(localStorage.getItem('roles')) !== null? JSON.parse(localStorage.getItem('roles'))[0].role_name:''
    return (

    <div className="customSearch">
        <input
                  placeholder='Search..'
                  id='searchFeedText'
                  type='text'
                  onChange={this.search_feed.bind(this)}
                  name='searchFeedText'
                   />
                    {/* <div className ={`searchInput  ${this.state.searchFlag}`}>  </div> */}
                    <button  className="search-icon" onClick= {this.onSearchClick.bind(this)}> <i class="material-icons">search</i></button>
    
      <ul className='parent clearfix'>
      {this.state.customfeedData.map((FeedData,i) => {
            if(FeedData.privacy=="close"){
              FeedTypeIcon="lock_outline";
            }else{
              FeedTypeIcon="sort";
            }
            
            if(FeedData.identity=="rpPDp"){
              return(
                <li key={i}>
                <Link
                  to={hashTag ? `/feed/default/live${hashTag}`: `/feed/default/live`}
                  className={
                    selectedFeed == 'default' || path[2] =='rss' || path[2] == 'jobs'
                      ? 'active'
                      : ''
                  }
                >

                  <span className = 'custom-feed'>
                  <span className = "icon-feed"><i class="material-icons">{FeedTypeIcon}</i></span>

                  <span className="mainbox defaultFeed">{utils.limitLetters(FeedData.name)}</span>
                  </span>


                </Link>
                {/* { selectedFeed == 'default' ? */}
                    <div className="create-close-icon defaultupdate">
                    {roleName == 'super-admin' || roleName =='admin' ?<span className  = "Edit-Feed"> <i class="material-icons" onClick={this.updateFeed.bind(this,FeedData.identity)}>create</i></span>:''}
                  </div>
                {/* : ''} */}
              </li>
              )
            }
            else{
              return(
                <li key={i}>
                  <Link
                    to={ hashTag ? `/feed/custom/live/${FeedData.identity}${hashTag}` : `/feed/custom/live/${FeedData.identity}`}
                    className={
                      selectedFeed == 'custom' && selectedFeedPosts ==  FeedData.identity
                        ? 'active'
                        : ''
                    }
                  >
  
                    <span className = 'custom-feed'>
                    <span className = "icon-feed"><i class="material-icons">{FeedTypeIcon}</i></span>
  
                    <span className="mainbox"> {utils.limitLetters(FeedData.name)}</span>
                    </span>
  
  
                  </Link>
                  {/* { FeedData.identity == selectedFeedPosts ? */}
                      <div className="create-close-icon">
                      {roleName == 'super-admin' || roleName =='admin' ?<span className  = "Edit-Feed"> <i class="material-icons" onClick={this.updateFeed.bind(this,FeedData.identity)}>create</i></span>:''}
                      {loginUserId !== FeedData.owner_id ? <span className = "close-Feed"><i class="material-icons" onClick={this.closeFeed.bind(this,FeedData.identity)} >clear</i></span>:''}
                    </div>
                  {/* : ''} */}
                </li>
              )
            }
          })
      }

      </ul>
      </div>
    )
  }

  rendercuratedJobByHash(){
    var me = this;
    if(me.props.hash == null || me.props.hash == '' ||me.props.hash == undefined){
      return (
        <ul className='parent nav-feed  clearfix'>
          <li>
            <Link
              to='/feed/jobs'
              className={me.props.location.pathname == '/feed/jobs' ? ` active` : ``}
            >
              <span className='text'>Jobs</span>
            </Link>
          </li>
        </ul>
      )
    }
  }

  rendercuratedByHash() {
    var me = this;
    if(me.props.hash == null || me.props.hash == '' ||me.props.hash == undefined){
      return (
        <ul className='parent nav-feed  clearfix'>

        <li>
          <Link
            to='/feed/rss'
            className={me.props.location.pathname == '/feed/rss' ? ` active` : ``}
          >
            <span className='text'>Curated</span>
          </Link>
        </li>
        </ul>
      )
    }
  }
  renderMyPostFeedByHash()
  {
    const selectedFeed = this.props.currFeed
    const selectedFeedPosts = this.props.currFeedPosts
    const hashTag = this.props.hash;
    let roleName =JSON.parse(localStorage.getItem('roles')) !== null? JSON.parse(localStorage.getItem('roles'))[0].role_name:''
    var me=this;
    return (
      <ul className='parent  clearfix'>
        <li>
            <Link
              to={(hashTag ? `/feed/my/live${hashTag}` : '/feed/my/live')}
              className={me.props.location.pathname == '/feed/my/live' ? ` active` : ``}
            >
            <span className='text'>Live</span>
            </Link>
            </li>
            {(!hashTag) ?
          <li>
            <Link
              to='/feed/my/scheduled'
              className={me.props.location.pathname == '/feed/my/scheduled' ? ` active` : ``
              }
            >
              <span className='text'> Schedule</span>
            </Link>
          </li>
          : ''}
           {(!hashTag) && roleName!=='admin' && roleName!=='super-admin' && roleName!=="moderator"?
          <li>
            <Link
              to='/feed/my/unapproved'
              className={me.props.location.pathname == '/feed/my/unapproved' ? ` active` : ``
              }
            >
              <span className='text'> Unapproved / Failed </span>
            </Link>
          </li>
          : ''}

        {(!hashTag) && (roleName=='admin' || roleName=='super-admin' || roleName =="moderator")?
          <li>
            <Link
              to='/feed/my/failed'
              className={me.props.location.pathname == '/feed/my/failed' ? ` active` : ``
              }
            >
              <span className='text'> Failed </span>
            </Link>
          </li>
          : ''}
          
           {(!hashTag) && roleName!=='admin' && roleName!=='super-admin' ?
          <li>
            <Link
              to='/feed/my/pending'
              className={me.props.location.pathname == '/feed/my/pending' ? ` active` : ``
              }
            >
              <span className='text'> Pending approval</span>
            </Link>
          </li>
          : ''}

      </ul>
    )

  }
  renderAssetByHash() {
    const selectedFeed = this.props.currFeed
    const selectedFeedPosts = this.props.currFeedPosts
    const hashTag = this.props.hash;
    return (
      <ul className='parent  clearfix'>
        <li>
          <Link
            to={`/feed/assets/hash${hashTag}`}
            className={
              selectedFeed == 'assets'
                ? 'active'
                : ''
            }
          >
            <span className='text'>Assets</span>
          </Link>
        </li>

      </ul>
    )
  }
}


module.exports = FeedNav;