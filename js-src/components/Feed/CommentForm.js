

import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { Field, reduxForm } from 'redux-form';

import * as utils from '../../utils/utils'

import { renderField } from '../Helpers/ReduxForm/RenderField'
import MentionTextArea from '../Helpers/ReduxForm/MentionTextArea'
import * as tagsActions from '../../actions/tagsActions'


const required = value => value ? undefined : 'Required';


class CommentForm extends React.Component {

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    var path = utils
    .removeTrailingSlash(this.props.newLocation)
    .split('/')
    this
    .props
    .tagsActions
    .fetchUserTags(path,this.props.feedId)
  this
    .props
    .tagsActions
    .fetchPostTags('all')
    this.handleInitialize()
  }

  handleInitialize() {
    const initData = {
        "comment": this.props.comment,
    };
    this.props.initialize(initData);
  }


  handleKeyDown (e, cb) {
    if (e.key === 'Enter' && e.shiftKey === false) {
      e.preventDefault();
      cb();
    }
  }

  renderForm() {
    const {pristine, submitting, handleSubmit, commentSubmitting} = this.props;

    let userTags = this.props.userTags;
    let postTags = this.props.postTags;

    return (
      // <form onSubmit={handleSubmit} onKeyDown={(e) => { this.handleKeyDown(e, handleSubmit); }}>
<form onSubmit={handleSubmit}>
        <div className= "clearfix user-tag-editor">
            <Field placeholder="Type comment..." rows={"1"} cssClass="comment-response" component={MentionTextArea} type="textarea" name="comment" validate={[ required ]}
            tags={postTags}
            userTags={userTags}
            />

            <button class="btn btn-primary right feed-comment" type="submit" disabled={commentSubmitting || pristine}>Submit</button>

        </div>
      </form>
    )
  }
  submitComment(values){
    var me = this;
    me.props.handleSubmit(values);
  }
  renderCommentFormPopup(){
    const {pristine, submitting, handleSubmit, commentSubmitting} = this.props;

    let userTags = this.props.userTags;
    let postTags = this.props.postTags;

    return (
      // <form onSubmit={handleSubmit} onKeyDown={(e) => { this.handleKeyDown(e, handleSubmit); }}>
      <form onSubmit={handleSubmit}>
        <div className="user-tag-editor">
            <Field 
              placeholder="write a comment...." rows={"1"} 
              cssClass="comment-response" 
              component={MentionTextArea} 
              type="textarea" 
              name="comment" 
              validate={[ required ]}
              tags={postTags}
              userTags={userTags}
            />
            <div class="post-tag" onClick={this.submitComment.bind(this)}>
                <span>Post</span>
            </div>
            {/* <button class="btn btn-primary right feed-comment" type="submit" disabled={commentSubmitting || pristine}>Submit</button> */}

        </div>
      </form>
    )
  }

  render() {
    return (
        <div className="comment-form-main">
          {this.props.fromPopup==true?
          this.renderCommentFormPopup()
          :
          this.renderForm() }
        </div>
    )
  }

}


function mapStateToProps(state) {
  return {
    userTags: state.tags.userTags,
    postTags: state.tags.postTags,
  }
}

function mapDispatchToProps(dispatch) {
  return {
    tagsActions: bindActionCreators(tagsActions, dispatch),
  }
}
let connection = connect(mapStateToProps, mapDispatchToProps);

var reduxConnectedComponent = connection(CommentForm);

export default reduxForm({
  //form: 'commentForm'
})(reduxConnectedComponent)
