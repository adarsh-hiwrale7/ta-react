;
import { Link } from "react-router";
import { browserHistory } from "react-router";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Field, reduxForm } from 'redux-form';
import { renderField } from '../Helpers/ReduxForm/RenderField';
import MentionTextArea from '../Helpers/ReduxForm/MentionTextArea';
import PreLoader from '../PreLoader';
import CenterPopupWrapper from '../CenterPopupWrapper';
const required = value => (value ? undefined : 'Required')

class CreateFeedPopup extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
          customValidation:true,
          guestValueList :[],
          emailValidation : true
    }
    //this.handleView = this.handleView.bind(this);

  }
  componentWillMount() {
    var me= this
    //OnClick of Outside it will close Popup
    window.addEventListener("click", function(event) {
      if (event.target.className.includes("popup-center-wrapper")) {
        me.closeCreateFeedPopup();
      }
    });
  }

   componentDidMount() {
      this.handleInitialize();
      var myInput = document.getElementById("guestsdata");
      if(myInput.addEventListener ) {
          myInput.addEventListener('keydown',this.handleGuestAdd.bind(this),false);
      } else if(myInput.attachEvent ) {
          myInput.attachEvent('onkeydown',this.handleGuestAdd.bind(this)); /* damn IE hack */
      }
      }

      handleGuestAdd(event){
        var me = this;
        if(me.state.emailValidation==false  && event.target.value!=='' && (event.keyCode==32 || event.keyCode==13 || event.keyCode==9)){
            var guestList = me.state.guestValueList;
            guestList.push(event.target.value);
            if(event.keyCode==32){
              event.preventDefault();
            }
            me.setState({
              guestValueList : guestList
            },()=>{
              event.target.value="";
              me.props.change("guests",me.state.guestValueList)
            })
          }
      }

   handleInitialize () {
      const initData = {
        feedType: 'both',
        JobRss:'both',
        chat:'no'
      }
      this.props.initialize(initData)
    }

  closeCreateFeedPopup() {
    this.props.closeCreateFeedPopup();
  }
  formSubmit(val){
    this.props.handleSubmit(val)
  }
   handleMentionFocus(e){
      if(e.target.value == ''){
       this.setState({customValidation:true})
       this.props.change('users' ,'@')
      }
  }

  checkUsertags(e){
    var guestData = document.getElementById("guestsdata").value;
    var me = this;
  if(document.querySelector(".viewper").style.display=="inline"){
    e.preventDefault();
  }else{
    if(guestData!==''){
      var guestList = me.state.guestValueList;
      guestList.push(guestData);
      me.setState({
        guestValueList : guestList
      },()=>{
        document.getElementById("guestsdata").value="";
        me.props.change("guests",me.state.guestValueList)
      })
    }
  }
}
  validateUserTag(e)
          {
              var temp='',count=0,count2=0,flag=0;
              const len=Object.keys(e).length-1;

              for(var i=0;i<Object.keys(e).length-1;i++){
                //for loop is for checking the whole string every time it is updated
                temp=e[i];
                if(e[i]=="@"){count+=1}  //count of at the rate
                if((e[i]==")"&&e[i+1]==" "&& e[i+2]=="@" && flag!==2)){ //count of )" "@
                  count2+=1;flag=1;
                }
                if((e[i]==")"&&e[i+1]==" "&& e[i+2]!=="@" && e[i+2]!==undefined) || (e[i]==")"&& e[i+1]!=="@" && e[i+1]!==undefined && e[i+1]!==" ")){
                  flag=2;
                }
              }
              var first=e[0];
              var last=temp;

              temp=e[0]
                if(temp !== undefined){
                  if(first=="@" && last==")" && e[i]==null){
                    this.setState({customValidation:false})
                    document.querySelector(".viewper").style.display="none";
                  }
                  else if(first=="@" && last==")" && e[i]==" "){
                    if(flag==1){
                      if(count==count2+1){
                        this.setState({customValidation:false})
                        document.querySelector(".viewper").style.display="none";
                      }
                      else{
                        this.setState({customValidation:true})
                        document.querySelector(".viewper").style.display="inline";
                      }
                    }
                    else{
                      this.setState({customValidation:false})
                      document.querySelector(".viewper").style.display="none";
                    }
                  }
                  else{
                    this.setState({customValidation:true})
                    document.querySelector(".viewper").style.display="inline";
                  }
                  if(flag==2 && (e[0]!=='@'|| e[0]!==' ')){
                    this.setState({customValidation:true})
                    document.querySelector(".viewper").style.display="inline";
                  }
                }
                else
                {
                  if(document.getElementById('title').value === ""){
                    this.setState({customValidation:true})
                  }else{
                     this.setState({customValidation:false})
                  }
                  document.querySelector(".viewper").style.display="none";
                }
  }

  validateMultipleEmail(e){
              var re = /^[A-Z0-9\._%-]+@[A-Z0-9\.-]+\.[A-Z]{2,4}(?: ?[,] ?[A-Z0-9\._%-]+@[A-Z0-9\.-]+\.[A-Z]{2,4})*$/i;
              if(e.target.value == ""){
                  if(document.getElementById('title').value !== ""){
                    this.setState({
                      customValidation:false,
                      emailValidation : false
                    })
                  }else{
                    if(this.state.customValidation === false){
                      this.setState({customValidation:true,
                        emailValidation : true})
                    }
                  }
                  document.querySelector(".guest-email-validation").style.display="none";
              }else{
                if(re.test(e.target.value)){
                  this.setState({customValidation:false,
                    emailValidation : false})
                  document.querySelector(".guest-email-validation").style.display="none";
                }else{
                  this.setState({customValidation:true,emailValidation : true})
                  document.querySelector(".guest-email-validation").style.display="inline";
                }
              }
          }
    removeGuest(index){
      this.state.guestValueList.splice(index,1);
      this.setState({
        test:""
      })
      }
  /**
  * Disable button when you type feed title
  * @Author: Yamin
  * @Param: e
  * @return: nothing
  **/
  disableHandleForTitle(e){

      var regex = /^[a-zA-Z ]*$/;
        if(regex.test(e.target.value)==false){
          e.preventDefault(); 
        }else{
          if(e.target.value == ""){
            this.setState({customValidation:true});
        }else{
            this.setState({customValidation:false});
        }
        }  
  }

  render() {
    let userTags = this.props.userTags
    var index=0;
    return (
      <div>

        {this.props.feed.custom.createLoading ? <PreLoader className='Page' /> : ''}
        <div className="CreateFeedPopupWrapper">
           <CenterPopupWrapper>
              <div className="planChangePopup CreateFeedPopupWrapperForFeed">
                  <div className="customfeedMain ">
                    <div className="createCustomfeedpopup">
                        <header className="heading clearfix">
                          <h3>Create Custom Feed</h3>
                          <button id="close-popup" className="btn-default"><i className="material-icons" onClick={this.closeCreateFeedPopup.bind(this)}>clear</i></button>
                        </header>
                        <div className="widget createFeedDetails" id="setting_walkthrough">
                          <div className="feedtext">
                              <span>Feeds are where your members communicate.<br/></span>
                          </div>
                          <form onSubmit={this.formSubmit.bind(this)}>
                            <div className="feedRadioWrapper">
                              <div className="radiobtnFeed clearfix">
                                <span className="radiobtnFeed-label">Type</span>
                                <label className="customradio">
                                    INTERNAL
                                    <Field
                                      component='input'
                                      type='radio'
                                      name='feedType'
                                      id='feedType1'
                                      value='internal'
                                      />
                                    <span className="checkmark"></span>
                                </label>
                                <label className="customradio">
                                    EXTERNAL
                                    <Field
                                      component='input'
                                      type='radio'
                                      name='feedType'
                                      id='feedType2'
                                      value='external'
                                      />
                                    <span className="checkmark"></span>
                                </label>
                                <label className="customradio">
                                    BOTH
                                    <Field
                                      component='input'
                                      type='radio'
                                      name='feedType'
                                      id='feedType3'
                                      value='both'
                                      />
                                    <span className="checkmark"></span>
                                </label>
                              </div>
                              <div className="radiobtnFeed clearfix">
                                <span className="radiobtnFeed-label">Chat</span>
                                <label className="customradio">
                                    YES
                                    <Field
                                      component='input'
                                      type='radio'
                                      name='chat'
                                      id='yes'
                                      value='yes'
                                      />
                                    <span className="checkmark"></span>
                                </label>
                                <label className="customradio">
                                    NO
                                    <Field
                                      component='input'
                                      type='radio'
                                      name='chat'
                                      id='no'
                                      value='yes'
                                      />
                                    <span className="checkmark"></span>
                                </label>
                              </div>
                              <div className='radiobtnFeed clearfix'>
                                <span className="radiobtnFeed-label">Include</span>
                                <label className="customradio">
                                    JOBS
                                    <Field
                                      component='input'
                                      type='radio'
                                      name='JobRss'
                                      id='Jobs'
                                      value='jobs'
                                      />
                                    <span className="checkmark"></span>
                                </label>
                                <label className="customradio">
                                    CURATION
                                    <Field
                                      component='input'
                                      type='radio'
                                      name='JobRss'
                                      id='Curation'
                                      value='curation'
                                      />
                                    <span className="checkmark"></span>
                                </label>
                                <label className="customradio">
                                    BOTH
                                    <Field
                                      component='input'
                                      type='radio'
                                      name='JobRss'
                                      id='both'
                                      value='both'
                                      />
                                    <span className="checkmark"></span>
                                </label>
                                <label className="customradio">
                                    NONE
                                    <Field
                                      component='input'
                                      type='radio'
                                      name='JobRss'
                                      id='none'
                                      value='none'
                                      />
                                    <span className="checkmark"></span>
                                </label>
                              </div>
                              </div>
                              <div className="feedname">
                                <span>Name</span>
                                <Field
                                    component={renderField}
                                    type='text'
                                    name='title'
                                    id='title'
                                    placeholder='Enter feed name'
                                    onChange={this.disableHandleForTitle.bind(this)}
                                    validate={[required]}
                                    />
                              </div>
                              <Field
                                component={renderField}
                                type='hidden'
                                name='guests'
                                />
                              <div id = "custom-feed-popup-user-tag">
                                <div className="usersbox form-row ">
                                    <div className='user-tag-editor'>
                                      <span className = "users-tag-mention-area-label">Users</span>
                                      <div className='createPostDescritionWrapper'>
                                          <Field
                                            component={MentionTextArea}
                                            userTags={userTags}
                                            type='text'
                                            name='users'
                                            id='users'
                                            placeholder='Add user with @name'
                                            onFocus={this.handleMentionFocus.bind(this)}
                                            onChange={this.validateUserTag.bind(this)}
                                            />
                                      </div>
                                    </div>
                                    <div className="viewper">* Please add only user tags</div>
                                </div>
                              </div>
                              <div className="sendbtn clearfix">
                                <button type="submit" disabled={this.state.customValidation} onClick={this.checkUsertags.bind(this)}><span>Create</span></button>
                              </div>
                          </form>
                          <div className="guestEmail">
                              <label>Guests</label>
                              <div className="form-row multiple-email-container" >
                                <input 
                                    // type="text" 
                                    type='textarea'
                                    name='guestsdata'
                                    id='guestsdata'
                                    placeholder='Enter guest emails'
                                    class="multiple_emails-input text-left"
                                    onBlur={this.validateMultipleEmail.bind(this)}
                                    />
                                <ul className="multiple_emails-ul" key={index}>
                                    {
                                    this.state.guestValueList.length !== 0?
                                    this.state.guestValueList.map((guestEmail,i)  => {
                                    return(
                                    <li className ="multiple_emails-email">
                                      <span class="email_name" data-email={guestEmail}>{guestEmail}</span>
                                      <span
                                          tabindex="8000"
                                          id='close-popup'
                                          className='btn-default multiple_emails-close'
                                          onClick={this.removeGuest.bind(this,i)}
                                          >
                                      <i className='material-icons'>clear</i>
                                      </span>
                                    </li>
                                    )
                                    })
                                    :''
                                    }
                                </ul>
                              </div>
                              <div className="guest-email-validation">* Please add valid email address</div>
                          </div>
                        </div>
                    </div>
                  </div>
              </div>
            
            </CenterPopupWrapper>
        </div>
        </div>
    );
  }
}


function mapStateToProps(state, ownProps) {
  return {}
}

function mapDispatchToProps(dispatch) {
  return {

  }
}
let connection = connect(mapStateToProps, mapDispatchToProps)

var reduxConnectedComponent = connection(CreateFeedPopup)

export default reduxForm({
  form: 'CreateFeedPopup' // a unique identifier for this form
})(reduxConnectedComponent)

