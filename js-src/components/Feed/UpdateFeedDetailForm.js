;
import { Link } from "react-router";
import { browserHistory } from "react-router";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Field, reduxForm } from 'redux-form';
import { renderField } from '../Helpers/ReduxForm/RenderField';
import MentionTextArea from '../Helpers/ReduxForm/MentionTextArea';
import PreLoader from '../PreLoader'

const required = value => value
  ? undefined
  : 'Required';

class UpdateFeedDetailForm extends React.Component {
  componentDidMount() {

    this.handleInitialize(this.props);
  }

  handleInitialize(nextProps) {
    var JobRssData = '';
    if (nextProps.feedDetail.curation_display == 0 && nextProps.feedDetail.job_display !== 0) {
      JobRssData = 'jobs'
    } else if (nextProps.feedDetail.curation_display !== 0 && nextProps.feedDetail.job_display == 0) {
      JobRssData = "curation"
    }
    else if (nextProps.feedDetail.curation_display !== 0 && nextProps.feedDetail.job_display !== 0) {
      JobRssData = "both"
    }
    else if (nextProps.feedDetail.curation_display == 0 && nextProps.feedDetail.job_display == 0) {
      JobRssData = "none"
    }
    const initData = {
      feedId: nextProps.feedId,
      feed_name: nextProps.feedDetail.name,
      feedType: nextProps.feedDetail.type,
      JobRss: JobRssData
    }
    this.props.initialize(initData)
  }
  onFeedNameChange(e) {
    var regex = /^[a-zA-Z ]*$/;
    if (regex.test(e.target.value) == false) {
      e.preventDefault();
    }
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.feedDetail !== this.props.feedDetail && nextProps.feedDetail !== "") {
      this.handleInitialize(nextProps);
    }
  }

  render() {
    const {
      handleSubmit,
      pristine
    } = this.props
    return (
      <div className="DetailFeed editFeedForm">
        <form onSubmit={handleSubmit}>
          <div className="feedRadioWrapper">
            <div className="radiobtnFeed clearfix">
              <span className="radiobtnFeed-label">Type</span>
              <label className="customradio">INTERNAL
                                                      <Field
                  component='input'
                  type='radio'
                  name='feedType'
                  id='feedType1'
                  value='internal'
                />
                <span className="checkmark"></span>
              </label>
              <label className="customradio">EXTERNAL
                                                      <Field
                  component='input'
                  type='radio'
                  name='feedType'
                  id='feedType2'
                  value='external'
                />
                <span className="checkmark"></span>
              </label>
              <label className="customradio">BOTH
                                                      <Field
                  component='input'
                  type='radio'
                  name='feedType'
                  id='feedType3'
                  value='both'
                />
                <span className="checkmark"></span>
              </label>
            </div>
            <div className="radiobtnFeed clearfix">
              <span className="radiobtnFeed-label">Chat</span>
              <label className="customradio">
                YES
                <Field
                  component='input'
                  type='radio'
                  name='chat'
                  id='yes'
                  value='yes'
                />
                <span className="checkmark"></span>
              </label>
              <label className="customradio">
                NO
              <Field
                  component='input'
                  type='radio'
                  name='chat'
                  id='no'
                  value='no'
                />
                <span className="checkmark"></span>
              </label>
            </div>
            <div className='radiobtnFeed clearfix'>
              <span className="radiobtnFeed-label">Include</span>
              <label className="customradio">JOBS
                                                    <Field
                  component='input'
                  type='radio'
                  name='JobRss'
                  id='Jobs'
                  value='jobs'
                />
                <span className="checkmark"></span>
              </label>
              <label className="customradio">CURATION
                                                    <Field
                  component='input'
                  type='radio'
                  name='JobRss'
                  id='Curation'
                  value='curation'
                />
                <span className="checkmark"></span>
              </label>
              <label className="customradio">BOTH
                                                    <Field
                  component='input'
                  type='radio'
                  name='JobRss'
                  id='both'
                  value='both'
                />
                <span className="checkmark"></span>
              </label>
              <label className="customradio">NONE
                                                    <Field
                  component='input'
                  type='radio'
                  name='JobRss'
                  id='none'
                  value='none'
                />
                <span className="checkmark"></span>
              </label>
            </div>
          </div>
          <div className="settingfeedname">
            <span className="settingfeednameLabel">Name</span>
            <Field
              component='input'
              type='hidden'
              name='feedId'

            />
            <Field
              component='input'
              type='text'
              name='feed_name'
              cols='80'
              rows='1'
              onChange={this.onFeedNameChange.bind(this)}
              validate={[required]}
            />
          </div>

          <div className="update-btn">
            <div className="sendbtn clearfix">
              <button type="submit"><span>Update</span></button>
            </div>
          </div>
        </form>
      </div>
    )
  }
}


function mapStateToProps(state, ownProps) {
  return {}
}

function mapDispatchToProps(dispatch) {
  return {

  }
}
let connection = connect(mapStateToProps, mapDispatchToProps)

var reduxConnectedComponent = connection(UpdateFeedDetailForm)

export default reduxForm({
  form: 'UpdateFeedDetailForm' // a unique identifier for this form
})(reduxConnectedComponent)                    