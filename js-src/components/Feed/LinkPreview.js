export default class LinkPreview extends React.Component {
    constructor (props) {
        super(props)
        this.state = {
        }   
    }
    render() {
        var post=''
        if(this.props.postdata){
            //true when linkpreview call from post
            post=this.props.postdata
        }else if(this.props.commentData){
           post=this.props.commentData
       }
        return(
        <div>
        {post.LinkPreview.data.map((linkdata,i)=>{
                return(
                 <div className = "linkpreview-parent-class"> 
                     <div className="link_preview_parent_class" id={linkdata.identity} key={i}>
                          <div className = "inner-link-preview "> 
                                    <div className = "logo-preview"> 
                                    {linkdata.cover!==''?
                                         linkdata.cover.startsWith('http')?
                                            <img src={linkdata.cover} className="link_cover" alt = "image not found"/>
                                        :<i class="material-icons">image</i>:<i class="material-icons">image</i>}
                                    </div>
                                    <div className = "details-preview"> 
                                         <div className = "title-preview"> {linkdata.title} </div>
                                         <div className = "description-preview"> 
                                             <p> 
                                             {linkdata.description}
                                             </p>
                                         </div>
                                         <div className = "link-preview"> <a className="url_name" target="_blank" href={linkdata.url}>{linkdata.url}</a> </div>
                                    </div>
                          </div> 
                     </div>
                </div>
               )
            })}
        </div> 
        )
        
    }
}