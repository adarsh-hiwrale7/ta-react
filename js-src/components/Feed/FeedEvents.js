
import SliderPopup from '../SliderPopup'
import Post from './Post'
import * as utils from '../../utils/utils'

class FeedEvents extends React.Component {
  constructor (props) {
    super(props)
  }
  
  render () {
    let social_channels = this.props.event.feed_post.SocialAccount || [];
    let post_type = this.props.event.feed_post.post&&this.props.event.feed_post.post.data.type ;
    let social_channel_array = [];
    if(post_type=='external')
    {
    social_channels && social_channels.data.forEach(function(element) {
      if(element.length != 0)
      {
        social_channel_array.push(element.SocialAccount.data.social_media.toLowerCase());
      }
    });
    }
    var postTitle=this.props.event.title?utils.convertUnicode(this.props.event.title):''
    return (
      <div>
        <div
          className='calendar_event_content clearfix'
          data-toggle='popover'
          data-placement='top'
          data-popover-content={'#custom_event_' + this.props.event.id}
          tabIndex='0'
          id='btnCampaignDetail'
          data-isbtndetail="true"
        >
          <div className="calendar-event-title  campaign-calender-icon " title={postTitle}>
          {postTitle}
          <span className = "template-icon">
          {
            this.props.event.type=="print"?
            <i class='material-icons'>text_fields</i>:
              this.props.event.type=="email"?
              <i class='material-icons'>mail_outline</i>:
                this.props.event.type=="sms"?
                <i class='material-icons'>mail_outline</i>:''

          }
          </span>
          </div>
          {
          post_type=='external'
          ?
          <div className="calendar-socials">
          {social_channel_array.map(function(element, index){
                    return <span key={index} className={`calendar-social-icon ${element}`}><i key={index} class={`fa fa-${element}`} aria-hidden="true"></i></span>;
                  })}
          
            
          
          </div>
          :''
        }
        </div>

        <div className='hidden' id={'custom_event_' + this.props.event.id}>
          <div className='popover-heading'>
            {this.props.event.driver}
          </div>

          <div className='popover-body'>
            {postTitle}<br />
          </div>
        </div>
      </div>
    )
  }
}
export default FeedEvents
