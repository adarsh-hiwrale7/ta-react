import ImageLoader from 'react-imageloader'
import PreLoaderMaterial from '../PreLoaderMaterial'

import * as utils from '../../utils/utils'

class GiphyPost extends React.Component {

  renderTitle(type) {
   
    // return <span className='title'>{type == 'image' ? this.props.name + '.' + this.props.extension : this.props.name}</span>
  
  }

  renderDocAsset(faIcon, extension) {
    return (
      <div className='file-doc-wrapper'>
        <div className={`file file-doc ${extension}`}>
          <i className={`fa ${faIcon} fa-3x`} />
        </div>
        {!this.props.detailView ? this.renderTitle() : <span />}
        <a onClick={this.props.onClick} className='type-doc file-details'>

        </a>
      </div>
    )
  }

  render() {
    const selected = this.props.selected;
    var index = this.props.media_type.indexOf('/')
    var type = 'application'
    if (index !== -1) {
      type = this.props.media_type.substr(0, index)
    } else {
      type = this.props.media_type
    }

    // var assetCreated = this.props.assets.creation.created;

    var media_url = this.props.media_url
    var thumbnail_url = typeof this.props.thumbnail_url !== 'undefined' ? this.props.thumbnail_url : media_url //this.props.thumbnail_url
    let extension = this.props.extension
    var assetStyle = {
      backgroundImage: `url(${thumbnail_url})`
    }

    let faIcon = utils.getFaIcon(extension)

    switch (type) {

      case 'image': {
        return (
          <div id={`asset-${this.props.identity}${selected ? '-selected' : ''}`} className='vid file-image'>
            <div className='inner_imgwrapper'>
              <a className='type-image' onClick={this.props.onClick}>

                {this.props.detailView
                  ? <ImageLoader
                    src={media_url}
                    wrapper={React.DOM.div}
                    preloader={PreLoaderMaterial}
                  >
                    Image load failed!
                      </ImageLoader>
                  : 
                  
                  <ImageLoader
                    src={thumbnail_url}
                    preloader={PreLoaderMaterial}
                    className = "thumbnail">
                   <div className="image-load-failed">
                      <i class="material-icons no-img-found-icon">&#xE001;</i>
                   </div> 
                 </ImageLoader> 

                  }
              </a>
            </div>
            {!this.props.detailView ? this.renderTitle(type) : <span />}
            <a onClick={this.props.onClick} className='file-details'>

            </a>
          </div>
        )
      }

      default:
        return (
          <div id={`asset-${this.props.identity}`} className='archive asset-file'>
            {this.renderDocAsset(faIcon, extension)}
          </div>
        )
    }
  }
}

module.exports = GiphyPost
