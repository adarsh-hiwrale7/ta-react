import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Field, reduxForm } from 'redux-form'
import { IndexLink, Link } from 'react-router'
import { renderField } from '../Helpers/ReduxForm/RenderField'
import * as analyticsActions from '../../actions/analytics/analyticsActions'
import * as notificationActions from '../../actions/notificationActions'
import * as utils from '../../utils/utils'
import AnalyticsNav from './AnalyticsNav'
import AnalyticsPopup from './AnalyticsPopup'
import AnalyticsFilters from './AnalyticsFilters'
import AnalyticsPopupParent from './AnalyticsPopupParent'
import AnalyticsTable from './AnalyticsTable'
import CollapsibleFilter from '../CollapsibleFilter'
import Header from '../Header/Header'
import * as generalActions from '../../actions/generalActions'
import moment from '../Chunks/ChunkMoment'
import notie from 'notie/dist/notie.js'
import PreLoader from '../PreLoader'
import ReactDatePicker from '../Helpers/ReduxForm/ReactDatePicker' // react datepicker field
import reactDatepickerChunk from '../Chunks/ChunkDatePicker' // datepicker chunk
import SliderPopup from '../SliderPopup'
import GuestUserRestrictionPopup from '../GuestUserRestrictionPopup'
import StaticDataForGuest from '../StaticDataForGuest';
const required = value => (value ? undefined : 'Required')
var filter_active = '';
var startDate = '';
var endDate = ''; 
var dataType = '';
var popupData
var flagToRestrictApiCall=true
var restrictFeedAPI = false;
var restrictAssetsAPI = false;
var restrictCampaignAPI = false;
var isFilterDataChanged=false
var allsavedFilterData={}
var filterRemoved=false
var isSavedFilterButtonClicked=false
var selectedEndDate=''
var selectedStartDate=''
class Analytics extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      show_menu: true,
      isTourActive: true,
      ReactDatePicker: null,
      tourStep: 1,
      show_filters: true,
      filter_menu_show: true,
      inputValue: true,
      serian_no_col: '',
      post_details: '',
      like_analytics: '',
      share_analytics: '',
      comment_analytics: '',
      Moment: null,
      side_popup: false,
      walltype: '',
      type: '',
      tagname:'',
      tagid:'',
      component_action: '',
      like_state: '',
      share_state: '',
      comment_state: '',
      serialno_state: '',
      postdetail_state: '',
      user_state: '',
      department_state: '',
      channel_state: '',
      show_popup_class: '',
      assetType: '',
      selected_socialmedia: '',
      selected_campStatus: '',
      pageno: null,
      exportbuttonisClicked:false,
      savedFilterData:[],
      savedTagType:'',
      savedTagId:'',
      isFilterSaved:false,
      isFilterDataChanged:false,
      currPageType:'feed',
      test:'',
      isDateChanged:false,
      saveClicked: false,
      isFilterShow: false,
      selectedPopupData : null,
    }
    this.selectedDate = this.selectedDate.bind(this)
  }
  renderLoading () {
    return (
      <div className='preloader-wrap'>
        <PreLoader className='Page' />
      </div>
    )
  }
  handleInitialize(){
    var initdata={
    }
    this.props.initialize(initData)
  }
  componentDidMount () {
    //when page is loaded then  to null this variable value to default or null
    flagToRestrictApiCall=true
    startDate=''
    endDate=''
    selectedStartDate=''
    selectedEndDate=''
    isSavedFilterButtonClicked=false
    this.props.generalActions.fetchClientIp()
  }
componentWillReceiveProps(nextProps) {
  var passTagTypeToObj=''
  var passTagIdToObj=''
  //call api to get saved filter data
  if(flagToRestrictApiCall==true && nextProps.profile.profile.identity!==null || nextProps.analytics.isFilterSaved==true && this.props.analytics.isFilterSaved!==nextProps.analytics.isFilterSaved ){
    //if condtion will true when profile has identity Or call when user save filter and to fetch saved data
    startDate=''
    endDate=''
    flagToRestrictApiCall=false
    var location=''
    if(typeof this.props.location !== 'undefined') {
          var newLocation = utils.removeTrailingSlash(this.props.location.pathname).split('/')
          location = newLocation[2] == undefined ? 'feed' : newLocation[2]
    }
    this.setState({
      currPageType:location //set currant page name
    })
    if(nextProps.profile.profile.role!==undefined && nextProps.profile.profile.role.data[0].role_name!=="guest"){
      this.props.analyticsActions.fetchSavedAnalyticsFilterData(nextProps.profile.profile.identity,location)
    }
  }
  
  if(nextProps.analytics.savedFilterAnalyticsData.length>0 && nextProps.analytics.savedFilterAnalyticsData!==this.props.analytics.savedFilterAnalyticsData){
    //when filter is saved then to store its value in state
        this.setState({isFilterSaved:true})
        var savedData=nextProps.analytics.savedFilterAnalyticsData
        var savedFilterData=savedData[0].filter_string?JSON.parse(savedData[0].filter_string):savedData
        var pagetype={
            pageType:savedData[0].type
          }

          //convert data into json format
        this.setState({
          savedFilterData:savedData[0].filter_string?JSON.parse(savedData[0].filter_string):savedData
        })

        //pass saved date in variable which is global
        startDate=savedFilterData.start_date?savedFilterData.start_date:''
        endDate=savedFilterData.end_date?savedFilterData.end_date:''

        if(typeof savedFilterData.user!=='undefined'){
          //if tag type is user then to pass its type into state
            passTagTypeToObj='user'
            passTagIdToObj=savedFilterData.user

            this.setState({
                tagid:savedFilterData.user,
                tagtype:'user'
            })
          }else if(typeof savedFilterData.department!=='undefined'){

            passTagTypeToObj='department'
            passTagIdToObj=savedFilterData.department

            this.setState({
                tagtype:'department',
                tagid:savedFilterData.department
            })

          }else if(typeof savedFilterData.company!=='undefined'){

            passTagTypeToObj='company'
            passTagIdToObj=savedFilterData.company

            this.setState({
                tagtype:'company',
                tagid:savedFilterData.company
            })
          }
          this.setState({
            selected_socialmedia:savedFilterData.social_media,
            selected_campStatus:savedFilterData.campaign_status,
            walltype:savedFilterData.type
          })
          //create object allsavedFilterData which is global , to get saved filter value from table and chart api , i have to create this object to pass saved value to fetch
          allsavedFilterData = {
            startDate: savedFilterData.start_date,
            endDate: savedFilterData.end_date,
            type: savedFilterData.type,
            tagtype:passTagTypeToObj.toLowerCase(),
            tagid:passTagIdToObj,
            socialmedia:savedFilterData.social_media,
            campStatus:savedFilterData.campaign_status,
            pageno: this.state.pageno ? this.state.pageno : '',
          }
        this.setState({
          savedFilterData:nextProps.analytics.savedFilterAnalyticsData
        },()=>this.fetchFeedData())    
  }


  if(nextProps.analytics.isFilterUnsave==true & nextProps.analytics.isFilterUnsave!==this.props.analytics.isFilterUnsave ){
    //when filter is clear then to clear all state and variable
      filterRemoved=true
      allsavedFilterData={}
      endDate=''
      startDate=''
    this.setState({
      isFilterDataChanged:false,
      isFilterSaved:false,
      selected_socialmedia:'',
      selected_campStatus:'',
      walltype:'',
      tagtype:'',
      type:'',
      tagid:'',
      socialmedia:'',
      campStatus: '',
      pageno:''
    },()=>this.fetchFeedData())
    //to reset flag isFilterUnsave
    this.props.analyticsActions.removeUnsaveFlag()
  }else{
    filterRemoved=false
  }
}
  open_filter_popup () {
    this.setState({
      show_filters: !this.state.show_filters
    })
    if (this.state.show_filters) {
      filter_active = 'active'
    } else {
      filter_active = ''
    }
  }
  closePopup () {
    this.setState({
      side_popup: false
    })
    document.body.classList.remove('overlay')
  }
  popupanylitcs (data) {
    popupData = data;
     document.body.classList.add('overlay');
    this.setState({
      side_popup: true,
      selectedPopupData  : data
    })
  }
  handlesubmit () {}
  componentWillMount () {
    moment().then(moment => {
      reactDatepickerChunk()
        .then(reactDatepicker => {
              this.setState({ ReactDatePicker: reactDatepicker, Moment: moment})
        })
        .catch(err => {
          throw err
        })
    })
  }

  /**
   * @author disha
   * call when date is select
   * @param {*} e
   */
  selectedDate (e) {
    if (e.input.name == 'scheduledDate-0' && e.input.value !== '' && startDate !== e.input.value) {
      //when schedule date is changed and if value is not changed then its default value will be null , to restrict this condition to call multiple
      isFilterDataChanged=true
      startDate = e.input.value

      if(endDate == '') {
          endDate = Math.floor(Date.now() / 1000)
        }
      this.setState({
      isDateChanged:true
      })
      var scheduler_date = this.state.Moment?this.state.Moment.unix(parseInt(startDate)).format('MM-DD-YYYY'):''
      var lastdate = this.state.Moment?this.state.Moment.unix(parseInt(endDate)).format('MM-DD-YYYY'):''
      var date1 = new Date(scheduler_date.toString())
      var date2 = new Date(lastdate.toString())
      var timeDiff = date2.getTime() - date1.getTime()
      var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24))
      if (diffDays < 0) {
        notie.alert('error', 'Start Date must be greater then End Date.', 5)
        return
      }
    }
    if(e.input.name == 'scheduledDate-1' && e.input.value !== '' && endDate !== e.input.value) {
      isFilterDataChanged=true

      endDate = e.input.value

      if(startDate == '') {
          startDate = Math.floor(Date.now() / 1000)
      }
      this.setState({
        isDateChanged:true
      })
      var scheduler_date = this.state.Moment?this.state.Moment.unix(parseInt(startDate)).format('MM-DD-YYYY'):''
      var lastdate = this.state.Moment?this.state.Moment.unix(parseInt(endDate)).format('MM-DD-YYYY'):''
      var date1 = new Date(scheduler_date.toString())
      var date2 = new Date(lastdate.toString())
      var timeDiff = date2.getTime() - date1.getTime()
      var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24))
      if (diffDays < 0) {
        notie.alert('error', 'End Date must be greater then Start Date.', 5)
        return
      }
    }
  }

/**
   * @author disha
   * call when media type is changed
   * @param {*} chartType
   */
  selectedMedia (e) {
    if(allsavedFilterData.social_media!==e.target.value || typeof allsavedFilterData.social_media=='undefined'){
      //flag to know wheather data is changed or not
        isFilterDataChanged=true
    }
    this.setState({
        selected_socialmedia: e.target.value
      }
    )
  }
  /**
   * @author disha
   * call when campaign staus is selected
   * @param {*} chartType
   */
  selectedCamp_status (e) {
    if(allsavedFilterData.campStatus!==e.target.value || allsavedFilterData.campStatus==''){
      isFilterDataChanged=true
    }
    this.setState(
      {
        selected_campStatus: e.target.value
      }
    )
  }
 /**
   * @author disha
   * call when feed/asset/campaign type is selected
   * @param {*} chartType
   */
  selectedWalltype (e) {
    // call when select walltype form filter and set state
    if(allsavedFilterData.type!==e.target.value || allsavedFilterData.type=='' ){
      isFilterDataChanged=true
    }
    this.setState(
      {
        walltype: e.target.value
      }
    )
  }

   /**
   * @author disha
   * to get data of selected tag from child
   * @param {*} e
   */
  getTagData(e){

    if(this.state.tagtype!==e.type || this.state.tagtype==''){
      isFilterDataChanged=true
  }
    this.setState(
      {
        type: e.type,
        tagname: e.tagname,
        tagid: e.tagid
      })
  }

  /**
   * @author disha
   * api call and pass all data into param through object
   */
  fetchFeedData () {
    var timezone=Object.keys(this.props.generalData.clientIpDetail).length>0?this.props.generalData.clientIpDetail.timezone:''
    var tags = ''
    var tagtype=''
    if(this.props.profile.profile.role !==undefined && this.props.profile.profile.role.data[0].role_name!=="guest"){
      if(this.state.tagtype !==undefined){
        //tagtype state will set when there is saved data
        tagtype=this.state.tagtype
      }else{
        //type state will set when there is no data and select tag
        tagtype=this.state.type
      }
      if (tagtype == 'Department') {
        tags = this.state.tagid.split('_')[0]
      } else {
        tags = this.state.tagid
      }
      if (this.props.location.pathname == '/analytics/feed' ||this.props.location.pathname == '/analytics') {
        //when path is feed then to call feed api and pass its value
        var allfeeddata = {
          startDate: startDate,
          endDate: endDate,
          type: this.state.walltype ? this.state.walltype : '',
          tagtype: tagtype?tagtype.toLowerCase():'',
          tagid: tags,
          pageno: this.state.pageno ? this.state.pageno : '',
          analyticsType:'post',
          timezone
        }
        if(this.state.exportbuttonisClicked==true){
            //if export button is clicked then to export data of feed
            this.props.analyticsActions.exportAnalytics(allfeeddata)
            this.setState({
              exportbuttonisClicked:false
            })
          }else{
            isFilterDataChanged=false
            this.setState({
              isDateChanged:false
            })
            if(restrictFeedAPI !==true){
              this.props.analyticsActions.fetchFeedChartAnalyticsPage(allfeeddata)
              this.props.analyticsActions.fetchFeedAnalytics(allfeeddata)
              restrictFeedAPI=true;
              setTimeout(() => {
                restrictFeedAPI=false
              }, 5000);
            }
        }
      } else if (this.props.location.pathname == '/analytics/asset') {
        var allassetdata = {
          startDate: startDate,
          endDate: endDate,
          tagtype: tagtype?tagtype.toLowerCase():'',
          tagid: tags,
          type: this.state.walltype ? this.state.walltype : '',
          pageno: this.state.pageno ? this.state.pageno : '',
          analyticsType:'asset',
          timezone
        }
        if(this.state.exportbuttonisClicked==true){
          this.props.analyticsActions.exportAnalytics(allassetdata)
          this.setState({
            exportbuttonisClicked:false
          })
        }else{
          isFilterDataChanged=false
          this.setState({
            isDateChanged:false
          })
          if(restrictAssetsAPI !==true){
            this.props.analyticsActions.fetchAssetChartAnalyticsPage(allassetdata)
            this.props.analyticsActions.fetchAssetAnalyticsPage(allassetdata)
            restrictAssetsAPI=true;
            setTimeout(() => {
              restrictAssetsAPI=false;
            }, 5000);
          }
        
        }
      } else if (this.props.route.path == 'campaign') {
  
        var allcampdata = {
          startDate: startDate,
          endDate: endDate,
          type: this.state.walltype ? this.state.walltype : '',
          tagtype: tagtype?tagtype.toLowerCase():'',
          tagid: tags,
          socialmedia: this.state.selected_socialmedia
            ? this.state.selected_socialmedia
            : '',
          campStatus: this.state.selected_campStatus
            ? this.state.selected_campStatus
            : '',
          pageno: this.state.pageno ? this.state.pageno : '',
          analyticsType:'campaign',
          timezone
        }
        if(this.state.exportbuttonisClicked==true){
          this.props.analyticsActions.exportAnalytics(allcampdata)
          this.setState({
            exportbuttonisClicked:false
          })
        }else{
          isFilterDataChanged=false
          this.setState({
            isDateChanged:false
          })

          if(restrictCampaignAPI !==true){
            this.props.analyticsActions.fetchCampaignChartAnalyticsPage(allcampdata)
            this.props.analyticsActions.fetchCampaignAnalyticsPage(allcampdata)
            restrictCampaignAPI=true;
            setTimeout(() => {
              restrictCampaignAPI=false
            }, 5000);
          }
         }
      }
    }
  }


  parentClick (clickFlag, e) {
    e.stopPropagation()
    if (clickFlag) {
      this.setState({
        show_popup_class: 'active'
      })
    } else if (this.state.show_popup_class !== '') {
      this.setState({
        show_popup_class: ''
      })
    }
  }
  /**
   * @author disha
   * @param {*} e this
   * call when export button is click
   */
  exportAnalytics(e){
    this.setState({
      exportbuttonisClicked:true
    },
    () => this.fetchFeedData())
  }
  /**
   * to call api to get next page of data
   * @param {*} pageno number of page
   */
  selectedPageno (pageno) {
    this.setState(
      {
        pageno: pageno
      },
      () => this.fetchFeedData()
    )
  }
 /**
  * @author disha
  * when click on save button then this function will call and call save api
  * @param {*} e
  */
  saveAnalyticsFilter(e){
    isSavedFilterButtonClicked=false  //flag used to set save button disable mode after saving data
    isFilterDataChanged=false
    var location=''
    var pagetype='feed'
    var tags = ''
    this.setState({
      isDateChanged:false,
      saveClicked:true
    })
    if (typeof this.props.location !== 'undefined') {
      var newLocation = utils
        .removeTrailingSlash(this.props.location.pathname)
        .split('/')
        location = newLocation[2] == undefined ? 'feed' : newLocation[2]
    }

    if (this.state.type == 'Department') {
      tags = this.state.tagid.split('_')[0]
    } else {
      tags = this.state.tagid
    }
    if(location=='feed' || location==''){
      pagetype='feed'
    }else if(location=='asset'){
      pagetype='asset'
    }else if(location=='campaign'){
      pagetype='campaign'
    }
    var tagtype= this.state.type ? this.state.type.toLowerCase() : this.state.tagtype
    var allFilterData={
      type:pagetype,
      string:{
        start_date: startDate,
        end_date: endDate,
        type: this.state.walltype ? this.state.walltype : '',
        [tagtype]:tags,
        social_media: this.state.selected_socialmedia
            ? this.state.selected_socialmedia
            : '',
        campaign_status: this.state.selected_campStatus
            ? this.state.selected_campStatus
            : '',
      }
    }
    this.props.analyticsActions.saveAnalyticsFilterData(allFilterData)
  }
  /**
   * @author disha
   * call when apply button is clicked , come from child component
   * @param {*} e
   */
  applySelectedFilter(e){

    isSavedFilterButtonClicked=true   //to active save button this flag is needed
    selectedStartDate=startDate
    selectedEndDate=endDate
    this.fetchFeedData()
  }

  /**
   * @author disha
   * when click on clear filter button from child component
   * @param {*} e
   */
  parentUnsaveFilter(deleteApicall=null){
    if(deleteApicall !==null && deleteApicall==false){
      //to remove selected value of textbox or dropdown 
      this.props.analyticsActions.clearUnsavedFilterData()
    }else{
      this.props.analyticsActions.unsaveFilterAnalytics(this.props.profile.profile.identity,this.state.currPageType)
    }
  }

  /**
   * @author disha
   * to remove selected date value to null when filter data is removed
   */
  removeDate(){
    startDate=''
    endDate=''
  }
  
  FetchRender(){
    var path = ''
    let analyticsData
    analyticsData = this.props.analytics
    if (
      this.props.route.path == 'feed' ||
      this.props.route.path == 'analytics'
    ) {
      dataType = 'post'
    } else if (this.props.route.path == 'asset') {
      dataType = 'asset'
    } else if (this.props.route.path == 'campaign') {
      dataType = 'campaign'
    }
    this.props.route.path == 'feed' || this.props.route.path == 'analytics'
      ? (path = 'feed')
      : (path = this.props.route.path)
    return(
      <div className='anylitics-content-main'>
      <div className='anytics_container'>
        <div>
            <div className='main-container-body-anytlics'>

              <div>
                {
                  Object.keys(analyticsData.feedAnalytics).length>0 ||
                  Object.keys(analyticsData.assetAnalytics).length>0 ||
                  Object.keys(analyticsData.campaignAnalytics).length>0
                  ?
                  <AnalyticsTable
                      analyticsData={analyticsData}
                      popupanylitcs={this.popupanylitcs.bind(this)}
                      route={path}
                      dataType={dataType}
                      pageNo={this.selectedPageno.bind(this)}
                      location={this.props.location}
                  />
                  :""}
              </div>
            </div>
          </div>
      </div>
    </div>
    )
  }
  render () { 
    let roleName =JSON.parse(localStorage.getItem('roles')) !== null? JSON.parse(localStorage.getItem('roles'))[0].role_name:''
    return (
      <div>
      <GuestUserRestrictionPopup setOverlay={true}/>
     <section id='analytics' onClick={this.parentClick.bind(this, false)}>

         {this.state.side_popup ?
        <AnalyticsPopupParent
          selectedPopupData = {this.state.selectedPopupData}
          location = {this.props.location}
          dataType = {dataType}
          closePopup = {this.closePopup.bind(this)}
          props = {this.props}

         /> : ''}
        <Header
          title='Analytics'
          externalpost='External Post'
          internalpost='Internal Post'
          asset='Assets'
          id='analytics'
          open_filter_fn
          nav={AnalyticsNav}
          location={this.props.location}
          exportAnalytics={this.exportAnalytics.bind(this)}
        />
        <form>
          <div className='main-container'>

            <div id='content'>
              <div className='page with-filters analyticPage'>
                <CollapsibleFilter>
                {this.state.savedFilterData.length>0?
                  <AnalyticsFilters
                    selectedWalltype={this.selectedWalltype.bind(this)}
                    parentClick={this.parentClick.bind(this)}
                    analyticsProps={this.props}
                    selectedDate={this.selectedDate}
                    show_popup_class={this.state.show_popup_class}
                    route={this.props.route.path}
                    tagdata={this.getTagData.bind(this)}
                    selectedMedia={this.selectedMedia.bind(this)}
                    selectedCamp_status={this.selectedCamp_status.bind(this)}
                    saveAnalyticsFilter={this.saveAnalyticsFilter.bind(this)}
                    renderSavedFilterTickmarkChild={this.state.savedFilterData}
                    location={this.props.location}
                    parentTagid = {this.state.tagid}
                    applySelectedFilter={this.applySelectedFilter.bind(this)}
                    isFilterDataChanged={isFilterDataChanged}
                    isSavedFilterButtonClicked={isSavedFilterButtonClicked}
                    unsaveFilter={this.parentUnsaveFilter.bind(this)}
                    filterRemoved={filterRemoved}
                    isDateChanged={this.state.isDateChanged}
                    removeDate={this.removeDate.bind(this)}
                    saveClicked={this.state.saveClicked}
                  />:''}
                </CollapsibleFilter>
                <div className='full-container clearfix'>
                {roleName == 'guest'?
                        <StaticDataForGuest props={this.props} />
                        : this.FetchRender()}
                </div>
              </div>
            </div>
          </div>
        </form>
      </section>
     </div>
   )
}
}
function mapStateToProps (state) {
  return {
    generalData:state.general,
    profile: state.profile,
    analytics: state.analytics,
    notificationData: state.notification_data,
    tags: state.tags
  }
}
function mapDispatchToProps (dispatch) {
  return {
    generalActions : bindActionCreators(generalActions,dispatch),
    analyticsActions: bindActionCreators(analyticsActions, dispatch),
    notificationActions: bindActionCreators(notificationActions, dispatch),
  }
}
let connection = connect(mapStateToProps, mapDispatchToProps)
var reduxConnectedComponent = connection(Analytics)

export default reduxForm({
  form: 'Analytics' // a unique identifier for this form
})(reduxConnectedComponent)
