import { IndexLink, Link } from 'react-router'
class SocialAnalyticsIndex extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
    }
  }
  render() {
    return (
      <div className="chart-container-in-analytics-wrapper">
        <div className="inner-chart-container-in-analytics-wrapper">
          <div className="row-container-of-the-chart-analytics">
             <div className = "Backbutton"> <a><i class="material-icons">keyboard_arrow_left</i> Back </a></div> 
           {/* chart part1 */}
            <div className="adoption-chart">
              <div className="inner-adoption-chart">
                {/* header-part */}
                <div className="header-analytics-wrapper clearfix">
                  <div className="title"> Engagement by channel <span> <i class="material-icons" data-tip data-for={'adoption'}>help</i> </span> </div>
                 
                  <div className="filter-weekey-container clearfix">
                  <div className = "filter-container">
                    <a className="week-filter"> Week </a>
                    <a className="month-filter">  Month  </a>
                    <a className="year-filter">Year </a>
                    <span>  <i class="material-icons">filter_list</i>  </span>
                  </div>
                </div>
              </div>
                {/* End header part */}
                {/* body part */}
                <div className  = "body-engagement-channel expand-body-chart-wrapper"> 
                    <div className = "inner-body-engagement-channel">
                         <div className = "clearfix social-graph-wrapper">
                            <div className = "social-graph-block social-graph-tw"> 
                              {/* grpah size: width :188px; height : 190 */}
                              <img src = "img/analytics-tw.png" alt = "no-image"  />
                            </div> 
                            <div className = "social-graph-block social-graph-linkdn">
                            <img src= "img/analytics-linkdn.png"  alt = "no-image" />
                              
                             </div> 
                            <div className = "social-graph-block social-graph-internal">
                               <img src = "img/analytics-internal.png"  alt = "no-image" />
                             </div> 
                            <div className = "social-graph-block social-graph-fb"> 
                            <img src = "img/analytics-fb.png" alt = "no-image" />
                            </div> 
                          </div> 
                    </div>
                </div>
             {/* End body part */}
              </div>
            </div>
            {/* chart1 part End */}
             {/* chart part2 */}
             <div className="total-post-chart">
              <div className="inner-adoption-chart">
                {/* header-part */}
                <div className="header-analytics-wrapper clearfix">
                  <div className="title"> Engagement over time <span> <i class="material-icons" data-tip data-for={'adoption'}>help</i> </span> </div>
                  <div className="filter-weekey-container clearfix">
                  <div className = "filter-container">
                    <a className="week-filter"> Week </a>
                    <a className="month-filter">  Month  </a>
                    <a className="year-filter">Year </a>
                    <span>  <i class="material-icons">filter_list</i>  </span>
                  </div>
                </div>
              </div>
                {/* End header part */}
                 {/* body part */}
                 <div className  = "body-engagement-channel expand-body-chart-wrapper expand-overtime-container"> 
                    <div className = "inner-body-engagement-channel">
                         <div className = "overtime-social-chart"> 
                              <img src = "img/analytics-social.png" alt = "analytics" />
                         </div>
                    </div>
                </div>
             {/* End body part */}
              </div>
            </div>
            {/* chart2 part End */}
          </div>
        </div>
      </div>
    )
  }
}

module.exports = SocialAnalyticsIndex