import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import PreLoader from '../../PreLoader'
import * as utils from '../../../utils/utils';
import CultureLoader from './CultureLoader';
import AnalyticsDashboardFilter from '../AnalyticsDashboard/AnalyticsDashboardFilter';
class VotedValuesWidget extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            opacity: {},
            expandLegend: false
        }
       
    }
    componentDidMount(){
        
    }
    expandLegend(action){
        this.setState({expandLegend: action})
    }

    CustomizedLabel = (props) => {
        const { payload } = props;
        return (
            <div className={props.expandLegend == true?'showMore legendUlWrapper':'legendUlWrapper'}>
            {props.expandLegend == true?
                <div className="legendHeader clearfix">
                    <div className="legendHeaderTitle">Voted values</div>
                    <div className="showLessBtnWrapper"> <a onClick={this.expandLegend.bind(this, false)}> <i class="material-icons">clear</i></a></div>
                </div>
                                         :''   
                                        }
            <ul>
            
                {payload.map((entry, index) => (
                        props.expandLegend == false ? 
                                index <= 4 ? 
                                    <li key={`item-${index}`}>
                                        <div  onMouseEnter={this.handleMouseEnter.bind(this,entry)} onMouseLeave={this.handleMouseLeave.bind(this,entry)} key={`item-${index}`} data-color={entry.color}>
                                            <span  className = "rounder-dot" style={{background:entry.color}}></span>
                                            <span title={entry.value} className ="title-active-user">{entry.value}</span>
                                            <span className="text-no"> {entry.payload.count}</span>
                                        </div>
                                        
                                    </li>
                               :''
                        : 
                                        <li key={`item-${index}`} onMouseEnter={this.handleMouseEnter.bind(this,entry)} onMouseLeave={this.handleMouseLeave.bind(this,entry)}>
                                            <span  className = "rounder-dot" style={{background:entry.color}}></span>
                                            <span className ="title-active-user">{entry.value}</span>
                                            <span className="text-no"> {entry.payload.count}</span>
                                        </li>
                                    
                ))}

            </ul>
            {payload.length > 5 ?
                <div className="readMoreLi" >
                    {/* <a onClick={this.expandLegend.bind(this, true)}>+{payload.length - 5} more</a> */}
                    <a title={'+'+payload.length - 5+' more'} onClick={this.expandLegend.bind(this, true)}><span>...</span></a>
                </div>
                :''   
            }
    </div>
        );
    }
    customizeTooltip = props => {
        const { payload, newTooltipdata, label } = props
        if (payload.length > 0) {
            return(
                <div className='custom-tooltip'>
                    <p className='label' style={{color:payload[0].payload.fill}}>
                        {payload[0].name}: {payload[0].value}
                    </p>
                </div>
            )
                
        }
    }
    //to handle mouse over and leave event on chart
    handleMouseEnter(o) {
        const { opacity } = this.state;
        this.setState({
            opacity: { key: o.value, val: 0.5 },
        });
    }
  
   handleMouseLeave(o) {
    const { opacity } = this.state;
    this.setState({
            opacity: [],
    });
   
   }
    
    renderCharts(culture, ReactTooltip, VotedValuesGraphDimention){
        return(

                                    <div className="participation">
                                        <div className="participation-header clearfix">
                                            <div className="participation-wrapper">
                                                <h4>All voted values <i class="material-icons" data-tip  data-for='allVote'>help</i></h4>
                                                {ReactTooltip !== null ? 
                                                    <ReactTooltip place="right" type="light" effect='solid' id='allVote' delayHide={100} delayUpdate={100}>
                                                        <div className="tooltip-wrapper">
                                                            <div className="inner-tooltip-wrapper">
                                                                <div className="tooltip-header clearfix">
                                                                    <div className="analytics-tooltip-title">
                                                                        <span>All voted values</span>
                                                                    </div>

                                                                </div>
                                                                <div className="tooltip-about-wrapper">
                                                                    <span>ABOUT</span>
                                                                    <p>Total number of voted posts by value.</p>
                                                                </div>
                                                            </div>
                                                        </div>           
                                                    </ReactTooltip>
                                                :''  
                                                }
                                            </div>
                                            <div className="expand-year-main-wrapper">
                                                <div className="expand-year-wrapper">
                                                    <div className="yearly-filtered-data">
                                                        <a className={`week-filter ${this.props.period=='week'?'active':''}`} onClick={this.props.votedTimeSpan.bind(this,'week')}> Week </a>
                                                         <a className={`month-filter ${this.props.period == undefined || this.props.period=='month'?'active':''}`} onClick={this.props.votedTimeSpan.bind(this,'month')}>  Month  </a>
                                                         <a className={`year-filter ${this.props.period=='year'?'active':''}`} onClick={this.props.votedTimeSpan.bind(this,'year')}>Year </a>
                                                         <span className={`filterIcon ${this.props.parentState['filterData-voted']!== undefined && Object.keys(this.props.parentState['filterData-voted']).length>2? 'active':'' }`} onClick={this.props.openFilterPopup.bind(this)}>
                                                    <i class='material-icons'>filter_list</i>{' '}
                                                </span>
                                            </div>
                                                {this.props.isFilterClicked==true ? <AnalyticsDashboardFilter includeLocation={true} callFrom='voted' period={this.props.period} fetchFilterData={this.props.fetchFilterData}/>:''}
                                                </div>
                                            </div>

                                        </div>
                                        <div className="voted-value-piechart clearfix piechart-wrapper">
                                        {/* pie-chart */}
                                        {typeof culture.votedValueData.chart !== 'undefined' && culture.votedValueData.chart.length>0 ? 
                                        <div>
                                                <this.props.PieChart width={VotedValuesGraphDimention.width} height={VotedValuesGraphDimention.height} dataKey="title">
                                                    <this.props.Pie data={culture.votedValueData.chart} cx={VotedValuesGraphDimention.cx}
                                                        cy={VotedValuesGraphDimention.cy}
                                                        activeShape={<text x={0} y={0} textAnchor="middle" fill='#82ca9d'>120 values</text>}
                                                        innerRadius={VotedValuesGraphDimention.innerRadius}
                                                        outerRadius={VotedValuesGraphDimention.outerRadius} fill="#82ca9d" dataKey="count" nameKey='title'>
                                                        {
                                                            culture.votedValueData.chart.map((entry, index) => <this.props.Cell key={index} fill={this.props.COLORS[index % this.props.COLORS.length]} opacity={Object.keys(this.state.opacity).length > 0 ? (this.state.opacity.key == entry.title) ? 1 : 0.4 : 1} />)
                                                        }
                                                    </this.props.Pie>
                                                    <this.props.Tooltip content={this.customizeTooltip}/>
                                                   <this.props.Legend align="left" verticalAlign="middle" iconType="circle" iconSize={10} layout="vertical" wrapperStyle={{ width:0 }} expandLegend={this.state.expandLegend} content={this.CustomizedLabel}/>
                                                </this.props.PieChart>
                                                <div class="donutchart-center-content"><span>{typeof culture.votedValueData.Values !== "undefined" ? Math.round(culture.votedValueData.Values) : 0}</span><p>Values</p></div>
                                                </div>
                                        : <div className = "no-Value-chart"> No value found</div>}
                                        
                                        </div>
                                        
                                    </div>
        )
    }

    render() {
        var culture = this.props.cultureData;
        var windowDimention=utils.windowSize();
        var VotedValuesGraphDimention = utils.getChartDimension(windowDimention.w,'cultureChart');
        return (                    
                 <div className="vote-wrapper">
                        { culture.votedValueLoading == true ? 
                                <CultureLoader/>     
                            :
                                this.renderCharts(culture, this.props.tooltip, VotedValuesGraphDimention)   
                        } 
                 </div>
            );
        //content={this.CustomizedLabe}
    }
}
function mapStateToProps(state) {
    return {
    }
}
function mapDispatchToProps(dispatch) {
    return {
    }
}
let connection = connect(mapStateToProps, mapDispatchToProps);
module.exports = connection(VotedValuesWidget);                               