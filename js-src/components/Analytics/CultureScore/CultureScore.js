import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import PreLoader from '../../PreLoader'
import * as utils from '../../../utils/utils';

class CultureScore extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            currentDate: null
        }
    }
    componentDidMount() {
        var currentDate = utils.currentDate();
        this.setState({ currentDate: currentDate.toLocaleDateString("en-us", { month: "short" }) + ' ' + utils.getOrdinalNum(currentDate.getDate()) });
    }
    loaderBox() {
        return (
            <div className="culture-index-loader">
                <div className="culture-score">
                    <div className="culture-score-header clearfix">
                        <div className="culture-score-wrapper loader-grey-line loader-line-radius loader-line-height">

                        </div>
                        <div className="expand-wrapper loader-grey-line loader-line-radius loader-line-height">

                        </div>

                    </div>
                    <div className="culture-score-number-value-wrapper clearfix">

                        <div className="culture-score-number">
                            <div className="culture-index-number loader-grey-line loader-line-radius loader-line-height">
                            </div>
                        </div>
                        <div className="culture-score-value">
                            <div className="cultur-score-value-wrapper">
                                <div className="culture-value-icon-content clearfix">
                                    <div className="culture-value-icon loader-grey-line loader-line-radius loader-line-height">

                                    </div>
                                    <div className="culture-score-value-content">
                                        <div className="promoted-value loader-grey-line loader-line-radius loader-line-height"></div>
                                        <div className="promoted-value-wrapper loader-grey-line loader-line-radius loader-line-height"></div>
                                    </div>
                                </div>
                                <div className="culture-value-icon-content clearfix">
                                    <div className="culture-value-icon loader-grey-line loader-line-radius loader-line-height">

                                    </div>
                                    <div className="culture-score-value-content">
                                        <div className="promoted-value loader-grey-line loader-line-radius loader-line-height"></div>

                                        <div className="promoted-value-wrapper loader-grey-line loader-line-radius loader-line-height"></div>
                                    </div>
                                </div>
                            </div>
                            <div className="culture-score-percentage">
                                <div className="culture-score-percentage-wrapper loader-grey-line loader-line-radius loader-line-height"></div>
                                <div className="culture-score-percentage-wrapper loader-grey-line loader-line-radius loader-line-height"></div>
                            </div>
                        </div>



                    </div>
                    <div className="culture-score-graph-wrapper clearfix">
                        <div className="culture-score-date-time-graph">
                            <div className="culture-score-graph-img-loader loader-grey-line loader-line-radius loader-line-height"></div>
                            <div className="culture-score-date loader-grey-line loader-line-radius loader-line-height"></div>
                        </div>
                        <div className="culture-score-detractors csClm">
                            <div className="culture-score-detractors-count cscnt">
                                <svg class="detractors-loader loader-grey-line loader-line-radius loader-line-height" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"></svg><label className="detractors-count loader-grey-line loader-line-radius loader-line-height">
                                </label>
                            </div>
                            <div className="detractors-content loader-grey-line loader-line-radius loader-line-height"></div>


                        </div>
                        <div className="culture-score-passives csClm">
                            <div className="culture-score-passives-count cscnt">
                                <svg class="detractors-loader loader-grey-line loader-line-radius loader-line-height" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                </svg><label class="passives-count loader-grey-line loader-line-radius loader-line-height"></label>

                            </div>
                            <div className="passives-content loader-grey-line loader-line-radius loader-line-height"></div>


                        </div>
                        <div className="culture-score-promotors csClm">
                            <div className="culture-score-promotors-count cscnt">

                                <i class="material-icons culture-count-loader loader-grey-line loader-line-radius loader-line-height"></i>
                                <label class="promotors-count loader-grey-line loader-line-radius loader-line-height"></label>
                            </div>
                            <div className="promotors-content loader-grey-line loader-line-radius loader-line-height"></div>


                        </div>
                    </div>
                </div>
            </div>
        )
    }

    scoreData(culture, ReactTooltip) {
        return (

            <div className="culture-score">



                <div className="culture-score-header clearfix">
                    <div className="culture-score-wrapper">
                        <h4>Net Value Score<span className="svg-icon help-element"><svg xmlns="http://www.w3.org/2000/svg" data-tip data-for='culturescore' width="24" height="24" viewBox="0 0 24 24"><path d="M0 0h24v24H0z" fill="none" /><path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm1 17h-2v-2h2v2zm2.07-7.75l-.9.92C13.45 12.9 13 13.5 13 15h-2v-.5c0-1.1.45-2.1 1.17-2.83l1.24-1.26c.37-.36.59-.86.59-1.41 0-1.1-.9-2-2-2s-2 .9-2 2H8c0-2.21 1.79-4 4-4s4 1.79 4 4c0 .88-.36 1.68-.93 2.25z" /></svg></span></h4>
                        {ReactTooltip !== null ?
                            <ReactTooltip place="right" type="light" effect='solid' delayHide={100} delayUpdate={100} id='culturescore'>
                                <div className="tooltip-wrapper">
                                    <div className="inner-tooltip-wrapper">
                                        <div className="tooltip-header clearfix">
                                            <div className="analytics-tooltip-title">
                                                <span>Net Value Score</span>
                                            </div>
                                            {/* <div className="analytics-tooltip-read-more">
                                                                <a href="#">Read more<i class="material-icons">keyboard_arrow_right</i>
                                                                </a>
                                                                </div> */}

                                        </div>
                                        <div className="tooltip-about-wrapper">
                                            <span>ABOUT</span>

                                            <p>The Culture Index is a concept that builds off the NPS system, allowing employers to measure and get a snapshot of employees' willingness to be ambassadors for the company by advocating employment there.</p>
                                        </div>
                                    </div>
                                </div>
                            </ReactTooltip>
                            : ''
                        }
                    </div>
                    {/*<div className="expand-wrapper">
                                            <a href="#"> Expand<i class="material-icons">keyboard_arrow_right</i></a>
                                        </div>*/}

                </div>

                <div className="culture-score-number-value-wrapper clearfix">
                    <div className={culture.cultureIndex.index > -100 ? 'culture-score-number' : 'culture-score-number small-score-number'}>
                        <span className="large-number">{typeof (culture.cultureIndex.index) !== 'undefined' ? Math.round(culture.cultureIndex.index) : 0}</span>
                    </div>
                    <div className="culture-score-value">
                        <div className="cultur-score-value-wrapper">
                            <div className="culture-value-icon-content clearfix">
                                <div className="culture-value-icon">
                                    <i class="material-icons">favorite_border</i>
                                </div>
                                <div className="culture-score-value-content">
                                    <p>Most promoted value</p>
                                    <span>{typeof (culture.cultureIndex.most_promoted_value) !== 'undefined' ? culture.cultureIndex.most_promoted_value : ''}</span>
                                </div>
                            </div>
                        </div>
                        <div className="culture-score-percentage">
                            <span>{typeof (culture.cultureIndex.most_promoted_value_percentage) !== 'undefined' ? Math.round(culture.cultureIndex.most_promoted_value_percentage) : 0}%</span>
                        </div>



                    </div>
                    <div className="culture-score-value">
                        <div className="cultur-score-value-wrapper">
                            <div className="culture-value-icon-content clearfix">
                                <div className="culture-value-icon">
                                    <i class="material-icons">favorite_border</i>
                                </div>
                                <div className="culture-score-value-content">
                                    <p>Recently promoted value</p>
                                    <span>{typeof (culture.cultureIndex.recently_promoted_value) !== 'undefined' ? culture.cultureIndex.recently_promoted_value : ''}</span>
                                </div>
                            </div>
                        </div>
                        <div className="culture-score-percentage">
                            <span>{typeof (culture.cultureIndex.recently_promoted_value_percentage) !== 'undefined' ? Math.round(culture.cultureIndex.recently_promoted_value_percentage) : 0}%</span>
                        </div>
                    </div>


                </div>
                <div className="culture-score-graph-wrapper clearfix">
                    <div className="culture-score-date-time-graph">
                        <div className="culture-score-graph-image">
                            {utils.rankDiff(culture.cultureIndex.diff_index, true)}

                            <span className="number">{Math.round(culture.cultureIndex.diff_index)}</span>
                        </div>
                        <span className="culture-score-date">Change since {this.state.currentDate}</span>
                    </div>
                    <div className="csClmWpr">
                        <div className="culture-score-detractors csClm">
                            <div className="culture-score-detractors-count cscnt">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><circle cx="15.5" cy="9.5" r="1.5" /><circle cx="8.5" cy="9.5" r="1.5" /><path d="M11.99 2C6.47 2 2 6.48 2 12s4.47 10 9.99 10C17.52 22 22 17.52 22 12S17.52 2 11.99 2zM12 20c-4.42 0-8-3.58-8-8s3.58-8 8-8 8 3.58 8 8-3.58 8-8 8zm0-6c-2.33 0-4.32 1.45-5.12 3.5h1.67c.69-1.19 1.97-2 3.45-2s2.75.81 3.45 2h1.67c-.8-2.05-2.79-3.5-5.12-3.5z" /></svg><span class="detractors-count cscnt">{typeof (culture.cultureIndex.detractors_percentage) !== 'undefined' ? Math.round(culture.cultureIndex.detractors_percentage) : 0}%</span>
                            </div>
                            <span className="detractors-content">Detractors<span className="cultureNumber">({typeof (culture.cultureIndex.detractors) !== 'undefined' ? culture.cultureIndex.detractors : 0}) </span> </span>


                        </div>
                        <div className="culture-score-passives csClm">
                            <div className="culture-score-passives-count cscnt">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><circle cx="15.5" cy="9.5" r="1.5"></circle><circle cx="8.5" cy="9.5" r="1.5"></circle><path d="M11.99 2C6.47 2 2 6.48 2 12s4.47 10 9.99 10C17.52 22 22 17.52 22 12S17.52 2 11.99 2zM12 20c-4.42 0-8-3.58-8-8s3.58-8 8-8 8 3.58 8 8-3.58 8-8 8zm0-6c-2.33 0-4.32 "></path></svg><span class="passives-count cscnt">{typeof (culture.cultureIndex.passives_percentage) !== 'undefined' ? Math.round(culture.cultureIndex.passives_percentage) : 0}%</span>
                            </div>
                            <span className="passives-content">Passives<span className="cultureNumber">({typeof (culture.cultureIndex.passives) !== 'undefined' ? culture.cultureIndex.passives : 0})</span> </span>


                        </div>
                        <div className="culture-score-promotors csClm">
                            <div className="culture-score-promotors-count cscnt">
                                <i class="material-icons">sentiment_satisfied_alt</i><span class="promotors-count cscnt">{typeof (culture.cultureIndex.promotors_percentage) !== 'undefined' ? Math.round(culture.cultureIndex.promotors_percentage) : 0}%</span>
                            </div>
                            <span className="promotors-content">Promoters <span className="cultureNumber"> ({typeof (culture.cultureIndex.promotors) !== 'undefined' ? culture.cultureIndex.promotors : 0})</span></span>


                        </div>
                    </div>
                </div>
            </div>


        );
    }

    render() {
        var culture = this.props.cultureData;

        return (
            <div className="culture-score-main-wrapper">
                {culture.indexLoading == true ?
                    this.loaderBox()
                    :
                    this.scoreData(culture, this.props.tooltip)
                }
            </div>
        )

    }
}
function mapStateToProps(state) {
    return {
    }
}
function mapDispatchToProps(dispatch) {
    return {
    }
}
let connection = connect(mapStateToProps, mapDispatchToProps);
module.exports = connection(CultureScore);

