import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import PreLoader from '../../PreLoader'
import * as utils from '../../../utils/utils';
import AnalyticsDashboardFilter from '../AnalyticsDashboard/AnalyticsDashboardFilter';
import CultureLoader from './CultureLoader';

var screenWidth = document.body.clientWidth;
class ParticipationWidget extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            opacity: {}
        }
       
    }
   
    //to personalise the legends of graph
    CustomizedLabel = (props) => {
         const { payload } = props;
        return (
            <div className="legendUlWrapper">
            <ul>
                {payload.map((entry, index) => (
                    <li key={`item-${index}`} data-color={entry.color} onMouseEnter={this.handleMouseEnter.bind(this,entry)} onMouseLeave={this.handleMouseLeave.bind(this,entry)}
                    >
                        <span  className = "rounder-dot" style={{background:entry.color}}></span>
                        <span className ="title-active-user">{entry.value}</span>
                        <span className="text-no"> {entry.payload.count}</span>
                    </li>
                ))}
            </ul>
            </div>
        );
    }
    //to personalise tooltip
    customizeTooltip = props => {
        const { payload, newTooltipdata, label } = props
        if (payload.length > 0) {
            return(
                <div className='custom-tooltip'>
                    <p className='label' style={{color:payload[0].payload.fill}}>
                        {payload[0].name}: {payload[0].value}
                    </p>
                </div>
            )
                
        }
    }
    //to handle mouse over and leave event on chart
    handleMouseEnter(o) {
        const { opacity } = this.state;
        this.setState({
            opacity: { key: o.value, val: 0.5 },
        });
    }
  
   handleMouseLeave(o) {
    const { opacity } = this.state;
    this.setState({
            opacity: [],
    });
   
   }
    renderCharts(culture, ReactTooltip, ParticipationGraphDimention){
        return(
                       <div className="participation"> 
                             <div className="participation-header clearfix">
                                    <div className="participation-wrapper">
                                         <h4>Participation <span className = "svg-icon help-element"  data-tip data-for='Participation'> <svg xmlns="http://www.w3.org/2000/svg" data-tip data-for='Participation' width="24" height="24" viewBox="0 0 24 24"><path d="M0 0h24v24H0z" fill="none"/><path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm1 17h-2v-2h2v2zm2.07-7.75l-.9.92C13.45 12.9 13 13.5 13 15h-2v-.5c0-1.1.45-2.1 1.17-2.83l1.24-1.26c.37-.36.59-.86.59-1.41 0-1.1-.9-2-2-2s-2 .9-2 2H8c0-2.21 1.79-4 4-4s4 1.79 4 4c0 .88-.36 1.68-.93 2.25z"/></svg></span>  </h4>
                                             {ReactTooltip !== null ? 
                                                    <ReactTooltip place="right" type="light" effect='solid' delayHide={100} delayUpdate={100} id='Participation'>
                                                        <div className="tooltip-wrapper">
                                                            <div className="inner-tooltip-wrapper">
                                                                <div className="tooltip-header clearfix">
                                                                    <div className="analytics-tooltip-title">
                                                                        <span>Participation</span>
                                                                    </div>

                                                                </div>
                                                                <div className="tooltip-about-wrapper">
                                                                    <span>ABOUT</span>
                                                                    <p>Total number and percentage of employee users who participated in the voting of a value post within Visibly. The overall scores are judged against industry standard benchmarks, 0-39 - Bad, 40-69 - Moderate, 70-100 Good.</p>
                                                                </div>
                                                            </div>
                                                        </div>           
                                                    </ReactTooltip>
                                                :''  
                                                }
                                            </div>
                                            {/* {   (typeof this.props.noFilters == "undefined" || this.props.noFilters == false) ?
                                                    <div class="expand-wrapper"><a href="#">Expand <i class="material-icons">keyboard_arrow_right</i></a></div>
                                                :''
                                            } */}
                                            <div className="expand-year-main-wrapper participation-filter-view">
                                                <div className="expand-year-wrapper">
                                                    <div className="yearly-filtered-data">
                                                         <a className={`week-filter ${this.props.period=='week'?'active':''}`} onClick={this.props.participationTimeSpan.bind(this,'week')}> Week </a>
                                                         <a className={`month-filter ${this.props.period == undefined || this.props.period=='month'?'active':''}`} onClick={this.props.participationTimeSpan.bind(this,'month')}>  Month  </a>
                                                         <a className={`year-filter ${this.props.period=='year'?'active':''}`} onClick={this.props.participationTimeSpan.bind(this,'year')}>Year </a>
                                                          {   (typeof this.props.noFilters == "undefined" || this.props.noFilters == false) ?
                                                                <span className={`filterIcon ${this.props.parentState['filterData-participation']!== undefined && Object.keys(this.props.parentState['filterData-participation']).length>2? 'active':'' }`} onClick={this.props.openFilterPopup.bind(this)}>  <i class="material-icons">filter_list</i>  </span>
                                                              : '' 
                                                          }
                                                    </div>
                                                    {this.props.isFilterClicked==true ? <AnalyticsDashboardFilter includeLocation={true} callFrom='participation' period={this.props.period} fetchFilterData={this.props.fetchFilterData}/>:''}
                                                   
                                                </div>
                                            </div>
                                            
                                        </div>
                                        <div className="participation-piechart-wrapper cf piechart-wrapper">
                                        {typeof culture.participationData !== 'undefined' && culture.participationData.chart.length>0? 
                                        <div>
                                                    <this.props.PieChart width={ParticipationGraphDimention.width} height={ParticipationGraphDimention.height} dataKey="title">
                                                    <this.props.Pie data={culture.participationData.chart} cx={ParticipationGraphDimention.cx}
                                                        cy={ParticipationGraphDimention.cy}
                                                        activeShape={<text x={0} y={0} textAnchor="middle" fill='#82ca9d'>120 values</text>}
                                                        innerRadius={ParticipationGraphDimention.innerRadius}
                                                        outerRadius={ParticipationGraphDimention.outerRadius} fill="#82ca9d" dataKey="count" nameKey='title'>
                                                        {
                                                            culture.participationData.chart.map((entry, index) => <this.props.Cell key={index} fill={this.props.COLORS[index % this.props.COLORS.length]} opacity={Object.keys(this.state.opacity).length > 0 ? (this.state.opacity.key == entry.title) ? 1 : 0.4 : 1} />)
                                                        }
                                                    </this.props.Pie>
                                                    <this.props.Tooltip content={this.customizeTooltip} />
                                                    <this.props.Legend align="left" verticalAlign="middle" iconType="circle" iconSize={10} layout="vertical"  wrapperStyle={{ width:0 }} content={this.CustomizedLabel}/>
                                                </this.props.PieChart>    
                                        <div class="donutchart-center-content"><span>{typeof culture.participationData.Participation !== "undefined" ? Math.round(culture.participationData.Participation) : 0}%</span><p>Participation</p><span class="participation-low">{culture.participationData.Participation_rate?culture.participationData.Participation_rate:""}</span></div>
                                        </div>
                                        : <div className = "no-Value-chart">No Value Found</div>}
                                        </div>
                                 </div>   
                             
        )    

    }
   
    render() {
        var windowDimention=utils.windowSize()
        var ParticipationGraphDimention = utils.getChartDimension(windowDimention.w,'cultureChart');
        var culture = this.props.cultureData;
        return (
               <div>
                   <div className="participation-main-wrapper">
                            { culture.participationLoading == true ? 
                            <CultureLoader/>  
                            :
                            this.renderCharts(culture,this.props.tooltip, ParticipationGraphDimention) 
                            } 
                    </div>
                </div>        
                 );
    }
}
function mapStateToProps(state) {
    return {
    }
}
function mapDispatchToProps(dispatch) {
    return {
    }
}
let connection = connect(mapStateToProps, mapDispatchToProps);
module.exports = connection(ParticipationWidget);                               