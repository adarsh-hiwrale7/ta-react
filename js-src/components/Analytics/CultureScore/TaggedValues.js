import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import PreLoader from '../../PreLoader'
import * as utils from '../../../utils/utils';
import CultureLoader from './CultureLoader';
import AnalyticsDashboardFilter from '../AnalyticsDashboard/AnalyticsDashboardFilter';
class TaggedValues extends React.Component {
    constructor(props) {
        super(props);
         this.state = {
            opacity: {},
             expandLegend: false
        }
        
       
    }
    componentDidMount(){
    }
    expandLegend(action){
        this.setState({expandLegend: action})
    }
  
    CustomizedLabel = (props) => {
         const { payload } = props;
        return (
            <div className={props.expandLegend == true?'showMore legendUlWrapper':'legendUlWrapper'}>
            {props.expandLegend == true?
                <div className="legendHeader clearfix">
                    <div className="legendHeaderTitle">Tagged values</div>
                    <div className="showLessBtnWrapper"> <a onClick={this.expandLegend.bind(this, false)}> <i class="material-icons">clear</i></a></div>
                </div>
                                         :''   
                                        }
            <ul>
                {payload.map((entry, index) => (
                        props.expandLegend == false ? 
                            index <= 4 ? 
                                <li key={index} onMouseEnter={this.handleMouseEnter.bind(this,entry)} onMouseLeave={this.handleMouseLeave.bind(this,entry)}>
                                    <div key={`item-${index}`} data-color={entry.color}>
                                        <span  className = "rounder-dot" style={{background:entry.color}}></span>
                                        <span className ="title-active-user">{entry.value}</span>
                                        <span className="text-no"> {entry.payload.count}</span>
                                    </div>
                                </li>
                            :''
                        : 
                            <div key={index} onMouseEnter={this.handleMouseEnter.bind(this,entry)} onMouseLeave={this.handleMouseLeave.bind(this,entry)}>
                                        <li key={`item-${index}`} data-color={entry.color}>
                                            <span  className = "rounder-dot" style={{background:entry.color}}></span>
                                            <span className ="title-active-user">{entry.value}</span>
                                            <span className="text-no"> {entry.payload.count}</span>
                                        </li>
                            </div>    

           
                ))}
            </ul>
            {payload.length > 5 ?
                <div className="readMoreLi" >
                    {/* <a onClick={this.expandLegend.bind(this, true)}>+{payload.length - 5} more</a> */}
                    <a title={'+'+payload.length - 5+' more'} onClick={this.expandLegend.bind(this, true)}><span>...</span></a>
                </div>
                :''   
            }
            </div>
        );
    }
    customizeTooltip = props => {
        const { payload, newTooltipdata, label } = props
        if (payload.length > 0) {
            return(
                <div className='custom-tooltip'>
                    <p className='label' style={{color:payload[0].payload.fill}}>
                        {payload[0].name}: {payload[0].value}
                    </p>
                </div>
            )
                
        }
    }
    handleMouseEnter(o) {
        const { opacity } = this.state;
        this.setState({
            opacity: { key: o.value, val: 0.5 },
        });
    }
  
   handleMouseLeave(o) {
    const { opacity } = this.state;
    this.setState({
            opacity: [],
    });
   
   }
    renderCharts(culture, ReactTooltip, TaggedValuesGraphDimention){
        return(
                            <div className="taggedMain2">
                                 {/* <div className="tagged-voted-value-header">
                                        <div className="expand-wrapper">
                                            <a href="#"> Expand<i class="material-icons">keyboard_arrow_right</i></a>
                                        </div>
                                    </div>*/}
                                    <div className="tagged-value-wrapper">               
                                     <div className="participation">
                                        <div className="participation-header clearfix">
                                            <div className="participation-wrapper">
                                                <h4>Tagged values <i class="material-icons" data-tip data-for='tagvalue'>help</i></h4>
                                                {ReactTooltip !== null ? 
                                                <ReactTooltip place="right" type="light" effect='solid' delayHide={100} delayUpdate={100} id='tagvalue'>
                                                    <div className="tooltip-wrapper">
                                                        <div className="inner-tooltip-wrapper">
                                                            <div className="tooltip-header clearfix">
                                                                <div className="analytics-tooltip-title">
                                                                    <span>Tagged values</span>
                                                                </div>

                                                            </div>
                                                            <div className="tooltip-about-wrapper">
                                                                <span>ABOUT</span>
                                                                <p>Total number of tagged posts by value. </p>
                                                            </div>
                                                        </div>
                                                    </div>           
                                                </ReactTooltip>
                                            :''  
                                            }
                                            </div>
                                            <div className="expand-year-main-wrapper">
                                                <div className="expand-year-wrapper">
                                                    <div className="yearly-filtered-data">
                                                        <a className={`week-filter ${this.props.period=='week'?'active':''}`} onClick={this.props.taggedTimeSpan.bind(this,'week')}> Week </a>
                                                         <a className={`month-filter ${this.props.period == undefined || this.props.period=='month'?'active':''}`} onClick={this.props.taggedTimeSpan.bind(this,'month')}>  Month  </a>
                                                         <a className={`year-filter ${this.props.period=='year'?'active':''}`} onClick={this.props.taggedTimeSpan.bind(this,'year')}>Year </a>
                                                         <span className={`filterIcon ${this.props.parentState['filterData-tagged']!== undefined && Object.keys(this.props.parentState['filterData-tagged']).length>2? 'active':'' }`} onClick={this.props.openFilterPopup.bind(this)}>
                                <i class='material-icons'>filter_list</i>{' '}
                            </span>
                        </div>
                        {this.props.isFilterClicked==true ? <AnalyticsDashboardFilter includeLocation={true} callFrom='tagged' period={this.props.period} fetchFilterData={this.props.fetchFilterData}/>:''}
                                                </div>
                                            </div>

                                        </div>
                                       <div class="tagged-value-pie-chart clearfix piechart-wrapper">
                                       {/* pie-chart */}
                                       {typeof culture.taggedValueData.chart !== 'undefined' && culture.taggedValueData.chart.length >0 ? 
                                        <div>
                                                <this.props.PieChart width={TaggedValuesGraphDimention.width} height={TaggedValuesGraphDimention.height} dataKey="title">
                                                    <this.props.Pie data={culture.taggedValueData.chart} cx={TaggedValuesGraphDimention.cx}
                                                        cy={TaggedValuesGraphDimention.cy}
                                                        activeShape={<text x={0} y={0} textAnchor="middle" fill='#82ca9d'>120 values</text>}
                                                        innerRadius={TaggedValuesGraphDimention.innerRadius}
                                                        outerRadius={TaggedValuesGraphDimention.outerRadius} fill="#82ca9d" dataKey="count" nameKey='title'>
                                                        {
                                                            culture.taggedValueData.chart.map((entry, index) =>
                                                                    <this.props.Cell key={index} fill={this.props.COLORS[index % this.props.COLORS.length]} opacity={Object.keys(this.state.opacity).length > 0 ? (this.state.opacity.key == entry.title) ? 1 : 0.4 : 1}  />
                                                            )
                                                        }
                                                    </this.props.Pie>
                                                    <this.props.Tooltip content={this.customizeTooltip} />
                                                    <this.props.Legend align="left" verticalAlign="middle" iconType="circle" iconSize={10} layout="vertical" wrapperStyle={{ width:0 }}  content={this.CustomizedLabel} expandLegend={this.state.expandLegend}  />
                                                </this.props.PieChart>
                                            <div class="donutchart-center-content"><span>{typeof culture.taggedValueData.Values !== "undefined" ? Math.round(culture.taggedValueData.Values) : 0}</span><p>Values</p></div>
                                         </div>
                                        : <div className = "no-Value-chart">No Value Found</div>}
                                       </div>
                                    </div>
                                 </div>
                        </div>         
        )
    }
render() {
        var culture = this.props.cultureData;
        var windowDimention=utils.windowSize();
        var TaggedValuesGraphDimention = utils.getChartDimension(windowDimention.w,'cultureChart');

        return (                    
                    <div className="taggedMain1">
                    
                         { culture.taggedValueLoading == true ? 
                         <div className="tagged-value-wrapper">
                                 <CultureLoader/> 
                                 </div>  
                            :
                                this.renderCharts(culture, this.props.tooltip, TaggedValuesGraphDimention)   
                        } 
                    </div>
                    

                 );
        //content={this.CustomizedLabe}
    }
}
function mapStateToProps(state) {
    return {
    }
}
function mapDispatchToProps(dispatch) {
    return {
    }
}
let connection = connect(mapStateToProps, mapDispatchToProps);
module.exports = connection(TaggedValues);                               