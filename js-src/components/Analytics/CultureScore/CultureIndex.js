import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import PreLoader from '../../PreLoader'
import Header from '../../Header/Header';
import Globals from '../../../Globals';
import { Link } from 'react-router';
import * as utils from '../../../utils/utils';
import CultureScore from './CultureScore';
import ParticipationWidget from './ParticipationWidget';
import TaggedValues from './TaggedValues';
import VotedValuesWidget from './VotedValuesWidget';
import * as analyticsActions from '../../../actions/analytics/analyticsActions';
import * as cultureAnalyticsActions from '../../../actions/analytics/cultureAnalyticsActions';
import recharts from '../../Chunks/ChunkRecharts';
import reactTooltip from '../../Chunks/ChunkReactTooltip';
import AnalyticsNav from '../AnalyticsNav'
import GuestUserRestrictionPopup from '../../GuestUserRestrictionPopup';

const COLORS = ['#4bcf99', '#ff595e', '#4b74e0', '#ffbf00', '#b87d7a', '#FF8042','#495867','#7FB069','#B8D8D8','#A93F55','#577399','#F9ADA0',"#ACF39D",'#19323C'];
var screenWidth = document.body.clientWidth;
var currFrom = ''
class CultureIndex extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            Recharts: null,
            tooltip: null,
            refreshChart: false,
            period: 'month',
            isFilterClicked: false,
            filterData: {}
        }
    }
    componentWillMount() {
        recharts()
            .then(recharts => {
                reactTooltip().then(reactTooltip => {
                    this.setState({ Recharts: recharts, tooltip: reactTooltip })
                })
            })
            .catch(err => {
                throw err
            })
    }
    componentDidMount() {
        //remove overlay is pending
        let roleName =JSON.parse(localStorage.getItem('roles')) !== null? JSON.parse(localStorage.getItem('roles'))[0].role_name:''
        if(roleName!=='guest'){
        var e = this;

        window.addEventListener("resize", function () {
            var width = document.body.clientWidth;
            var height = document.body.clientHeight;
            if (screenWidth < 1500 && width >= 1500) {
                screenWidth = width
                e.setState({ refreshChart: true });
            } else if (screenWidth > 1500 && width <= 1500) {
                screenWidth = width
                e.setState({ refreshChart: true });
            } else if (screenWidth > 767 && width <= 767) {
                screenWidth = width
                e.setState({ refreshChart: true });
            } else if (screenWidth < 767 && width >= 767) {
                screenWidth = width
                e.setState({ refreshChart: true });
            } else {
                if (e.state.refreshChart == true) {
                    e.setState({ refreshChart: false });
                }
                screenWidth = width
            }

        }, true);

        document.body.classList.add('cultureOverlay');
        var timezone = utils.clientTimezone();
        var ObjToSend = {
            'timezone': timezone,
            timespan: 'month'
        }
        this.props.cultureAnalyticsActions.getIndexData(timezone);
        this.props.cultureAnalyticsActions.getVotedValues(ObjToSend);
        this.props.cultureAnalyticsActions.getParticipationData(ObjToSend);
        this.props.cultureAnalyticsActions.getTaggedValues(ObjToSend);


        var legendText = document.getElementsByClassName('recharts-legend-item')
        if (legendText !== undefined) {
            for (var i = 0; i < legendText.length; i++) {
                var appendData = document.createElement('span')
                appendData.classList.add('rechart-legend-count')
                appendData.appendChild(document.createTextNode('120'))
                legendText[i].appendChild(appendData)
            }
        }
        var rechartWrapper = document.getElementsByClassName('recharts-wrapper')

        var me = this
        window.addEventListener('click', function (event) {
            if (event.target.innerText !== 'filter_list') {
                if (!event.target.closest('.filter-tag-child') && !event.target.closest('.filter-tag-parent')) {
                    if(currFrom!==''){
                        me.setState({
                            isFilterClicked: {...me.state.isFilterClicked, [currFrom]: false }
                        })
                    }
                }
            }
        })
        this.props.analyticsActions.resetAllFilterValues()
    }
    }
    openFilterPopup(from, status) {
        currFrom = from
        this.props.analyticsActions.resetSelectedFilterValues(from.replace(/Filter/g, ""))
        var me=this
        Object.keys(me.state.isFilterClicked).forEach(function(key) {
            if (me.state.isFilterClicked[key]!== undefined && me.state.isFilterClicked[key] ==true) {
                if(key !== from ){
                    me.state.isFilterClicked[key] = false;
                }
            }
          })
        me.setState({
            isFilterClicked: {...me.state.isFilterClicked, [from]: !me.state.isFilterClicked[from] }
        })
    }
    selectedTimeSpan(newprops = null, from, time) {
        var timezone = utils.clientTimezone();
        var ObjToSend = {
            timezone: timezone,
            timespan: time
        }
        if (this.state[`filterData-${from}`] !== undefined) {
            Object.assign(this.state[`filterData-${from}`], ObjToSend)
        }
        var dataToPass = this.state[`filterData-${from}`] !== undefined && Object.keys(this.state[`filterData-${from}`]).length > 0 ? this.state[`filterData-${from}`] : ObjToSend
        this.setState({
            period: { ...this.state.period, [from]: time }
        })
        switch (from) {

            case 'participation':
                this.props.cultureAnalyticsActions.getParticipationData(dataToPass)
                break;
            case 'tagged':
                this.props.cultureAnalyticsActions.getTaggedValues(dataToPass)
                break;
            case 'voted':
                this.props.cultureAnalyticsActions.getVotedValues(dataToPass)
                break;
            default:
                break;
        }
    }
    fetchFilterData(data, from) {
        this.setState({
            [`filterData-${from}`]: data
        })
    }
    render() {
        var culture = this.props.culture;
        return (
            <section id="culture-dashboard">
                <GuestUserRestrictionPopup setOverlay={true}/>
                <Header
                    title={'Analytics'}
                    location={this.props.location}
                />
                <div className='main-container'>
                    <div id="content">
                        <div className="culture-dashboard-tab-responsive culture-score-tab clearfix">
                            <AnalyticsNav location={this.props.location} />
                        </div>
                        <div className="culture-participation-mainbox dashboard-analytics-level1-container">
                            <div className="culture-participation-wrapper clearfix">
                                <CultureScore cultureData={culture} tooltip={this.state.tooltip} />
                                {this.state.Recharts != null ?
                                    <ParticipationWidget
                                        parentState={this.state}
                                        fetchFilterData={this.fetchFilterData.bind(this)}
                                        isFilterClicked={this.state.isFilterClicked['participationFilter']}
                                        openFilterPopup={this.openFilterPopup.bind(this, 'participationFilter', true)}
                                        PieChart={this.state.Recharts.PieChart}
                                        Pie={this.state.Recharts.Pie}
                                        cultureData={culture}
                                        Cell={this.state.Recharts.Cell}
                                        COLORS={COLORS}
                                        Tooltip={this.state.Recharts.Tooltip}
                                        Legend={this.state.Recharts.Legend}
                                        tooltip={this.state.tooltip}
                                        refreshChart={this.state.refreshChart}
                                        period={this.state.period['participation']}
                                        participationTimeSpan={this.selectedTimeSpan.bind(this, null, 'participation')} />
                                    : ''
                                }
                            </div>
                            <div className="tagged-voted-value">
                                <div className="tagged-voted-value-main-box clearfix">
                                    {this.state.Recharts != null ?
                                        <TaggedValues
                                            parentState={this.state}
                                            fetchFilterData={this.fetchFilterData.bind(this)}
                                            isFilterClicked={this.state.isFilterClicked['taggedFilter']}
                                            openFilterPopup={this.openFilterPopup.bind(this, 'taggedFilter', true)}
                                            PieChart={this.state.Recharts.PieChart}
                                            Pie={this.state.Recharts.Pie}
                                            cultureData={culture}
                                            Cell={this.state.Recharts.Cell}
                                            COLORS={COLORS} Tooltip={this.state.Recharts.Tooltip}
                                            Legend={this.state.Recharts.Legend}
                                            tooltip={this.state.tooltip}
                                            refreshChart={this.state.refreshChart}
                                            period={this.state.period['tagged']}
                                            taggedTimeSpan={this.selectedTimeSpan.bind(this, null, 'tagged')} />
                                        : ''
                                    }
                                    {this.state.Recharts != null ?
                                        <VotedValuesWidget
                                            parentState={this.state}
                                            fetchFilterData={this.fetchFilterData.bind(this)}
                                            isFilterClicked={this.state.isFilterClicked['votedFilter']}
                                            openFilterPopup={this.openFilterPopup.bind(this, 'votedFilter', true)}
                                            PieChart={this.state.Recharts.PieChart}
                                            Pie={this.state.Recharts.Pie}
                                            cultureData={culture}
                                            Cell={this.state.Recharts.Cell}
                                            COLORS={COLORS}
                                            Tooltip={this.state.Recharts.Tooltip}
                                            Legend={this.state.Recharts.Legend}
                                            tooltip={this.state.tooltip}
                                            refreshChart={this.state.refreshChart}
                                            period={this.state.period['voted']}
                                            votedTimeSpan={this.selectedTimeSpan.bind(this, null, 'voted')} />
                                        : ''
                                    }

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

function mapStateToProps(state) {
    return {
        culture: state.cultureAnalytics,
        analytics: state.analytics,
    }
}
function mapDispatchToProps(dispatch) {
    return {
        cultureAnalyticsActions: bindActionCreators(cultureAnalyticsActions, dispatch),
        analyticsActions: bindActionCreators(analyticsActions, dispatch),
    }
}
let connection = connect(mapStateToProps, mapDispatchToProps);
module.exports = connection(CultureIndex);