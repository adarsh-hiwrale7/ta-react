import AddSegmentPopup from './AddSegmentPopup'
import * as countryActions from '../../../actions/countryActions'
import * as companyInfo from '../../../actions/settings/companyInfo'
import * as userActions from '../../../actions/userActions'
import * as cultureAnalyticsActions from '../../../actions/analytics/cultureAnalyticsActions';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import PreLoader from '../../PreLoader'
import HeatmapChart from '../../Heatmap/HeatMapDemo';
import moment from '../../Chunks/ChunkMoment';
let resultLoop = {}
const optionMainConst = [
    { value: 'region', label: 'Region' },
    { value: 'country', label: 'Country'},
    { value: 'office', label: 'Office' },
    { value: 'department', label: 'Department' },
    { value: 'business', label: 'Business unit' },
    { value: 'company_role', label: 'Company role' },
    { value: 'level', label: 'Level' },
    { value: 'gender', label: 'Gender' },
    { value: 'age', label: 'Age' },
    { value: 'start_date', label: 'Start date' },
    { value: 'visibly_role', label: 'Visibly role' } 
  ];
class Heatmap extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            openSegmentPopup: false,
            selectedOption: null,
            Moment: null
        }
    }

    componentWillMount () {
        var me = this
        moment().then(moment => {
                this.setState({
                  Moment: moment,
                })
        })

    }

    componentWillReceiveProps(nextProps) {
        if(nextProps.culture.saveHeatMap !==  this.props.culture.saveHeatMap && nextProps.culture.saveHeatMap){
            this.setState({openSegmentPopup: false})
            this.props.cultureAnalyticsActions.resetSaveFlag()
            document.body.classList.remove('bdsgmntpp');
        }
    }
    
    openSegmentPopup(){
        let segmentState = this.state.openSegmentPopup ? false : true;
        document.body.classList.remove('bdsgmntpp');
        if(!this.state.openSegmentPopup){
            document.body.classList.add('bdsgmntpp');
            this.props.cultureAnalyticsActions.fetchAllSegments()
        }
        this.setState({openSegmentPopup: segmentState})
    }
    showPerticularSegment(segmentId){
        this.props.cultureAnalyticsActions.fetchPerticularSegment(segmentId)

    }
    componentDidMount(){
        this.props.cultureAnalyticsActions.heatMapChartData()
    }
  

    selectedLoopCheck(selectedArray){
        selectedArray.forEach(function(checkData) {
            switch(checkData.selected){
                case 'business':
                    if(checkData.value.length > 0){
                        let parentId = [];
                        let parentType = '';
                        checkData.value.map((checkVal)=>{
                            parentId.push(checkVal.value)
                            parentType='business'
                        })
                        resultLoop = {parentId: parentId, parentType: parentType}
                        return
                    }
                break;
                case 'department':
                    if(checkData.value.length > 0){
                        let parentId = [];
                        let parentType = '';
                        checkData.value.map((checkVal)=>{
                            parentId.push(checkVal.value)
                            parentType='department'
                        })
                        resultLoop = {parentId: parentId, parentType: parentType}
                        return
                    }
                break;
                case 'office':
                    if(checkData.value.length > 0){
                        let parentId = [];
                        let parentType = '';
                        checkData.value.map((checkVal)=>{
                            parentId.push(checkVal.value)
                            parentType='office'
                        })
                        resultLoop = {parentId: parentId, parentType: parentType}
                        return
                    }
                break;
                case 'country':
                    if(checkData.value.length > 0){
                        let parentId = [];
                        let parentType = '';
                        checkData.value.map((checkVal)=>{
                            parentId.push(checkVal.value)
                            parentType='country'
                        })
                        resultLoop = {parentId: parentId, parentType: parentType}
                        return
                    }
                break;
                case 'region':
                    if(checkData.value.length > 0){
                        let parentId = [];
                        let parentType = '';
                        checkData.value.map((checkVal)=>{
                            parentId.push(checkVal.value)
                            parentType='region'
                        })
                        resultLoop = {parentId: parentId, parentType: parentType}
                        return
                    }
                break;
                default:
                       resultLoop = {}
            }
            
        });
    }



    callFetchFieldData(type,selectedArray){
        resultLoop = {}
        this.selectedLoopCheck(selectedArray);

        let parentId = null
        let parentType = null
        if(typeof resultLoop.parentId !== 'undefined' && resultLoop.parentId.length > 0){
            parentId = resultLoop.parentId
            parentType = resultLoop.parentType
        }
        switch(type){
            case 'region':
                 this.props.companyInfoAction.fetchRegion()
            break; 
            case 'country':
                 this.props.countryActions.fetchAllCountry(parentId !== null ? parentId : 0,true)
            break;     
            case 'office':
                 this.props.companyInfoAction.showMasterData('office',parentId,true,parentType);
            break;
            case 'department':
                 this.props.companyInfoAction.showMasterData('department',parentId,true,parentType);
            break;     
            case 'business':
                 this.props.companyInfoAction.showMasterData('business',parentId,true,parentType);
            break;     
            case 'company_role':
                 this.props.companyInfoAction.showMasterData('role',parentId,true,parentType); 
            break;         
            case 'level':
                 this.props.cultureAnalyticsActions.getLevelData();
            break;     
            case 'gender':
                 this.props.cultureAnalyticsActions.getGenderData();     
            break;
            case 'visibly_role':
                 this.props.userActions.fetchRoles()
                 //this.props.cultureAnalyticsActions.getVisiblyRoleData();
            break;
        }
    }

    removeSegmentation(segmentationId){
        this.props.cultureAnalyticsActions.removeSegmentation(segmentationId);
    }
    
    submitForm(valueArray){
        let allSegmentArray = []
        allSegmentArray[0]=[];allSegmentArray[1]=[];allSegmentArray[2]=[];allSegmentArray[3]=[];allSegmentArray[4]=[];allSegmentArray[5]=[];allSegmentArray[6]=[];allSegmentArray[7]=[];allSegmentArray[8]=[];allSegmentArray[9]=[];allSegmentArray[10]=[]
        
        
        for(let i=0;i<valueArray.noOfSegment;i++){
            if(typeof valueArray['keyName'+i] !== 'undefined' && valueArray['keyName'+i] !== '' && valueArray['valueName'+i]){
                 
                 let tempArray = []
                /*if(valueArray['valueName'+i] instanceof Array){
                    let multiArray = []
                    tempArray[valueArray['keyName'+i]] = []
                    valueArray['valueName'+i].map((valArr,index)=>{
                        multiArray.push({key: valueArray['keyName'+i], value: valArr, operation: valueArray['operateName'+i], selected: valueArray['saveAsSegment'+i]})  
                    })
                    tempArray = multiArray
                }else{*/
                    tempArray = [{key: valueArray['keyName'+i], value: valueArray['valueName'+i], operation: valueArray['operateName'+i], selected: valueArray['saveAsSegment'+i]}]
                //}

                switch(valueArray['keyName'+i]){
                        case 'region':
                            allSegmentArray[0] = tempArray
                        break;
                        case 'country':
                            allSegmentArray[1] = tempArray
                        break;
                        case 'office':
                            allSegmentArray[2]= tempArray
                        break;
                        case 'department':
                            allSegmentArray[3] = tempArray
                        break;
                        case 'business':
                            allSegmentArray[4] = tempArray
                        break;
                        case 'company_role':
                            allSegmentArray[5]= tempArray
                        break;
                        case 'level':
                            allSegmentArray[6] = tempArray
                        break;
                        case 'gender':
                            allSegmentArray[7]= tempArray
                        break;
                        case 'age':
                            allSegmentArray[8] = tempArray
                        break;
                        case 'start_date':
                            allSegmentArray[9] = tempArray
                        break;
                        case 'visibly_role':
                            allSegmentArray[10] = tempArray
                        break;
                    }
            }
        }
        let apiRequest = {};
        apiRequest['segment'] = {}
        if(valueArray['edit_identity'] !== undefined){
            apiRequest['segment']['identity'] = valueArray['edit_identity']    
        }
        let j = 1
        allSegmentArray.map((allSegmentData,index)=>{
           // console.log(allSegmentData,'allSegmentData')
            
            if(allSegmentData.length > 0){
                        let newValue = []
                        if(allSegmentData[0].value instanceof Array){
                            newValue = allSegmentData[0].value 
                        }else{
                            let newValueArray = []
                            newValueArray.push(allSegmentData[0].value)   
                            newValue = newValueArray 
                        }
                        if(allSegmentData[0].operation == 'IB'){
                            newValue = []
                            if(allSegmentData[0].key == 'start_date'){
                                newValue.push(allSegmentData[0].value[0].from, allSegmentData[0].value[0].to)
                            }else{
                                newValue.push(allSegmentData[0].value.from, allSegmentData[0].value.to)
                            }
                        }
                        apiRequest['segment'][allSegmentData[0].key] = [{value: newValue, selected: allSegmentData[0].selected !== undefined ? allSegmentData[0].selected : false, operation: allSegmentData[0].operation, sequence: j}]
                        j++
            }
            
        })
        apiRequest.statusSelected = valueArray.selectedStatus
        // apiRequest['segment'].statusSelected = valueArray.selectedStatus
        this.props.cultureAnalyticsActions.saveSegmentation(apiRequest);
    }

    
    render(){
     

        const colorArray = [{"code":[255, 89, 94]},{"code":[47, 191, 113]},{"code":[229, 228, 228]}];
        return(
            <div className="hmAsPopup">
                 {this.props.culture.segmentLoader || this.props.users.rolesLoader || this.props.allcountry.loadCountry || this.props.masterData.companyMasterDataLoader || this.props.settings.companySettingLoader ? <PreLoader className='Page'/> : ''}
                {this.state.openSegmentPopup ? 
                    <AddSegmentPopup 
                        closeAddSegmentPopup={this.openSegmentPopup.bind(this)}
                        callFetchFieldData={this.callFetchFieldData.bind(this)}
                        countryData={this.props.allcountry.regioncountry}
                        officeData={this.props.masterData.office}
                        departmentData={this.props.masterData.department}
                        businessData={this.props.masterData.business}
                        companyRoleData={this.props.masterData.role}
                        levelData={this.props.culture.levelData}
                        genderData={this.props.culture.genderData}
                        visiblyRoleData={this.props.users.roles}
                        regionData={this.props.settings.companyHierarchyData.regions}
                        onSubmit={this.submitForm.bind(this)}
                        segmentData={this.props.culture.segmentData}
                        showPerticularSegment={this.showPerticularSegment.bind(this)}
                        perticularSegment={this.props.culture.perticularSegment}
                        removeSegmentation={this.removeSegmentation.bind(this)}
                        optionMainConst={optionMainConst}
                    /> 
                : ''}
                <div className="heatmapSection"> 
                <div className="bklkwrpr">
                    <a className="expand-link" onClick={this.props.closeHeatmapExpand.bind(this)}><i class="material-icons">clear</i></a>
                    </div>
                <div className="heatmapSectionInner"> 
                    <div className="heatmapSectionInnerWrapper">
                    
                    <div className="segment-btn-driver-wrapper clearfix">
                    <button className="btn btn-primary" onClick={this.openSegmentPopup.bind(this)}>
                    <span className="sgmtBtnIcn"><i class="material-icons">menu</i></span>
                    <span className="btnSgtSlctTxt">Selected</span>
                    <span className="btnSgtCnt">{this.props.culture.heatmapData.length > 0 ? this.props.culture.heatmapData[0].count[0] : ''} segments</span>
                    </button>
                        <div className="segment-driver-wrapper">
                            <div className="segment-driver-wrapper-inner clearfix">
                                <div className="segment-driver"><div className="segment-driver-inner">Productivity</div></div>
                                <div className="segment-driver"><div className="segment-driver-inner">Capability</div></div>
                                <div className="segment-driver"><div className="segment-driver-inner">Innovation</div></div>
                                <div className="segment-driver"><div className="segment-driver-inner">Customer Experience</div></div>
                                <div className="segment-driver"><div className="segment-driver-inner">Advocacy</div></div>
                            </div>
                        </div>
                    </div>
                    <HeatmapChart
                    allData={this.props.culture.heatmapData}
                    colorArray ={colorArray}
                    moment={this.state.Moment}
                    heatmapLoading ={this.props.culture.heatmapLoader }
                    />
                </div>
                </div>
                </div>
            </div>
          )
    }

    
}

function mapStateToProps(state) {
  return { 
    allcountry:state.allcountry,
    masterData: state.settings.companyMasterData,
    settings:state.settings,
    culture: state.cultureAnalytics,
    users: state.users

  }
}

function mapDispatchToProps(dispatch) {
  return {
    companyInfoAction:bindActionCreators(companyInfo,dispatch),
    countryActions:bindActionCreators(countryActions,dispatch),
    cultureAnalyticsActions: bindActionCreators(cultureAnalyticsActions, dispatch),
    userActions: bindActionCreators(userActions, dispatch)

  }
}

let connection = connect(mapStateToProps, mapDispatchToProps);

module.exports = connection(Heatmap);