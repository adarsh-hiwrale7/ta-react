import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import PreLoader from '../../PreLoader'
import * as utils from '../../../utils/utils';
import recharts from '../../Chunks/ChunkRecharts';
import reactTooltip from '../../Chunks/ChunkReactTooltip';
const COLORS = ['#4bcf99', '#ff595e', '#4b74e0', '#ffbf00', '#b87d7a', '#FF8042','#495867','#7FB069','#B8D8D8','#A93F55','#577399','#F9ADA0',"#ACF39D",'#19323C'];
class EngagementScore extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            opacity:{},
            currentDate:null,
            Recharts: null,
            tooltip: null,
            period:"month"
        }
    }
    componentWillMount() {
        recharts()
            .then(recharts => {
                reactTooltip().then(reactTooltip => {
                    this.setState({ Recharts: recharts, tooltip: reactTooltip })
                })
            })
            .catch(err => {
                throw err
            })
    }
    componentDidMount(){
        var currentDate = utils.currentDate();
        this.setState({currentDate: currentDate.toLocaleDateString("en-us", { month: "short" })+' '+utils.getOrdinalNum(currentDate.getDate())}); 
    }
       //to personalise the legends of graph
       CustomizedLabel = (props) => {
        const { payload } = props;
       return (
           <div className="legendUlWrapper">
           <ul>
               {payload.map((entry, index) => (
                   <li key={`item-${index}`} data-color={entry.color} onMouseEnter={this.handleMouseEnter.bind(this,entry)} onMouseLeave={this.handleMouseLeave.bind(this,entry)}
                   >
                       <span  className = "rounder-dot" style={{background:entry.color}}></span>
                       <span className ="title-active-user">{entry.value}</span>
                       <span className="text-no"> {entry.payload.count}</span>
                   </li>
               ))}
           </ul>
           </div>

           
       );
   }
   customizeTooltip = (props) => {
    const { payload, newTooltipdata, label } = props
    if (payload.length > 0) {
        return(
            <div className='custom-tooltip'>
                <p className='label' style={{color:payload[0].payload.fill}}>
                    {payload[0].name}: {payload[0].value}
                </p>
            </div>
        )
            
    }
}
    //to handle mouse over and leave event on chart
    handleMouseEnter(o) {
        const { opacity } = this.state;
        this.setState({
            opacity: { key: o.value, val: 0.5 },
        });
    }
  
   handleMouseLeave(o) {
    const { opacity } = this.state;
    this.setState({
            opacity: [],
    });
   
   }
   selectedTimeSpan(time){
    this.setState({
        period: time
    })
    this.props.fetchParticipationData(time)   
}
advocacyLoaderBox(){
    return(
        <div className="culture-index-loader">
        <div className="culture-score">
                                        <div className="culture-score-header clearfix">
                                            <div className="culture-score-wrapper loader-grey-line loader-line-radius loader-line-height">

                                            </div>
                                            <div className="expand-wrapper loader-grey-line loader-line-radius loader-line-height">

                                            </div>

                                        </div>
                                        <div className="culture-score-number-value-wrapper clearfix">

                                            <div className="culture-score-number">
                                                <div className="culture-index-number loader-grey-line loader-line-radius loader-line-height">
                                                </div>
                                            </div>
                                            <div className="culture-score-date-time-graph loader-grey-line"></div>
                                          
                                  
                                            
                                        </div>
                                        <div className="culture-score-graph-wrapper clearfix">
                                            
                                            <div className="culture-score-detractors csClm">
                                                <div className="culture-score-detractors-count cscnt">
                                                    <svg class="detractors-loader loader-grey-line loader-line-radius loader-line-height" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"></svg><label className="detractors-count loader-grey-line loader-line-radius loader-line-height">
                                                    </label>
                                                </div>
                                                <div className="detractors-content loader-grey-line loader-line-radius loader-line-height"></div>


                                            </div>
                                            <div className="culture-score-detractors csClm">
                                                <div className="culture-score-detractors-count">
                                                    <svg class="detractors-loader loader-grey-line loader-line-radius loader-line-height" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"></svg><label className="detractors-count loader-grey-line loader-line-radius loader-line-height">
                                                    </label>
                                                </div>
                                                <div className="detractors-content loader-grey-line loader-line-radius loader-line-height"></div>


                                            </div>
                                            <div className="culture-score-detractors csClm">
                                                <div className="culture-score-detractors-count">
                                                    <svg class="detractors-loader loader-grey-line loader-line-radius loader-line-height" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"></svg><label className="detractors-count loader-grey-line loader-line-radius loader-line-height">
                                                    </label>
                                                </div>
                                                <div className="detractors-content loader-grey-line loader-line-radius loader-line-height"></div>


                                            </div>
                                            <div className="culture-score-passives csClm">
                                                <div className="culture-score-passives-count">
                                                    <svg class="detractors-loader loader-grey-line loader-line-radius loader-line-height" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                                    </svg><label class="passives-count loader-grey-line loader-line-radius loader-line-height"></label>

                                                </div>
                                                <div className="passives-content loader-grey-line loader-line-radius loader-line-height"></div>


                                            </div>
                                            <div className="culture-score-promotors csClm">
                                                <div className="culture-score-promotors-count">

                                                    <i class="material-icons culture-count-loader loader-grey-line loader-line-radius loader-line-height"></i>
                                                    <label class="promotors-count loader-grey-line loader-line-radius loader-line-height"></label>
                                                </div>
                                                <div className="promotors-content loader-grey-line loader-line-radius loader-line-height"></div>


                                            </div>
                                        </div>
                                    </div>
                                   </div> 
    )
}
participationLoaderBox(){
    return(
        <div className="culture-index-loader">
        <div className="culture-score">
                                        <div className="culture-score-header clearfix">
                                            <div className="culture-score-wrapper loader-grey-line loader-line-radius loader-line-height">

                                            </div>
                                            <div className="expand-wrapper loader-grey-line loader-line-radius loader-line-height">

                                            </div>

                                        </div>
                                        <div className="title-active-user-wrapper clearfix">

                                            <div className="culture-score-number">
                                                <div className="title-active-user loader-grey-line loader-line-radius loader-line-height" />
                                                <div className="title-active-user loader-grey-line loader-line-radius loader-line-height" />
                                                
                                            </div>
                                            <div className="culture-score-date-time-graph loader-grey-line"></div>
                                          
                                  
                                            
                                        </div>
                                        <div className="linear-gauge loader-grey-line" />
                                    </div>
                                   </div> 
    )
}
    render(){
        var cultureData = this.props.culture;
        return(
            <div className="culture-participation-wrapper clearfix ">
                            <div class="culture-score-main-wrapper engagementscore-wrapper">
                            
                                

                                {
                                    cultureData.engagementDataLoading==true?
                                    this.advocacyLoaderBox()
                                :
                                <div class="culture-score">
                                    <div class="culture-score-number-value-wrapper clearfix">
                                            <div class="">
                                            <div class="header-analytics-wrapper clearfix">
                                    
                                     <div className="title"> Advocacy score<span className="svg-icon help-element"><svg xmlns="http://www.w3.org/2000/svg" data-tip  data-for='advocacyscore' width="24" height="24" viewBox="0 0 24 24"><path d="M0 0h24v24H0z" fill="none"/><path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm1 17h-2v-2h2v2zm2.07-7.75l-.9.92C13.45 12.9 13 13.5 13 15h-2v-.5c0-1.1.45-2.1 1.17-2.83l1.24-1.26c.37-.36.59-.86.59-1.41 0-1.1-.9-2-2-2s-2 .9-2 2H8c0-2.21 1.79-4 4-4s4 1.79 4 4c0 .88-.36 1.68-.93 2.25z"/></svg></span>
                                            {this.state.tooltip !== null ? 
                                                    <this.state.tooltip place="right" type="light" effect='solid' id='advocacyscore' delayHide={100} delayUpdate={100}>
                                                        <div className="tooltip-wrapper">
                                                            <div className="inner-tooltip-wrapper">
                                                                <div className="tooltip-header clearfix">
                                                                    <div className="analytics-tooltip-title">
                                                                        <span>Advocacy Score</span>
                                                                    </div>

                                                                </div>
                                                                <div className="tooltip-about-wrapper">
                                                                    <span>ABOUT</span>
                                                                    <p>An aggregated score of all 3P pulse responses resulting in an overall organisational Advocacy rating.</p>
                                                                </div>
                                                            </div>
                                                        </div>           
                                                    </this.state.tooltip>
                                                :''  
                                                }
                                                </div>
                                                <a className="expand-link" onClick={this.props.openHeatmapExpand.bind(this)}> Expand <span><i class="material-icons">navigate_next</i></span> </a>
                                                </div> 
                                                <div className="csnmbrvlwrpr clearfix">
                                                <div class= "culture-score-number">
                                                    
                                                        <span className="large-number">{typeof cultureData.engagementData.normalized_score!=="undefined"? cultureData.engagementData.normalized_score:0 } / 10</span>
                                                        
                                                    
                                                </div> 
                                                {/* <div class = "benchmark-rank culture-score-date"> 
                                                    { typeof cultureData.engagementData.index!=="undefined"?cultureData.engagementData.index-1.4:0}  {cultureData.engagementData.index-1.4>0?"above":"below"}  Benchmark  <span class = "benchmark-no"> 1.4 </span>
                                                </div> */}
                                                    <div class="culture-score-date-time-graph">
                                                    {typeof cultureData.engagementData.chart_data!=="undefined" && cultureData.engagementData.chart_data.length>0?
                                                        <div class="culture-score-graph-image">
                                                                { this.state.Recharts!==null?
                                                                <div>
                                                                        <this.state.Recharts.LineChart width={300} height={100} data={cultureData.engagementData.chart_data}>
                                                                                < this.state.Recharts.Line type='linear' dataKey='overall_score' stroke='#4bcf99' strokeWidth={2} dot={false}/>
                                                                        </this.state.Recharts.LineChart>
                                                                </div>:''}
                                                                <span class="number">{typeof cultureData.engagementData.diffrence!=="undefined"?cultureData.engagementData.diffrence:0}
                                                                %
                                                                </span>
                                                        </div>:''}
                                                        <div className="culture-score-date">
                                                            <div>Change since</div> 
                                                            <div>{this.state.currentDate}</div>
                                                        </div>
                                                    </div>
                                                </div>
                                                    
                                            </div>
                                        
                                            <div>
                                                <div className="culture-segment-wrapper">
                                                            <div className="title">Segmentations<span className="svg-icon help-element"><svg xmlns="http://www.w3.org/2000/svg" data-tip  data-for='segmentations' width="24" height="24" viewBox="0 0 24 24"><path d="M0 0h24v24H0z" fill="none"/><path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm1 17h-2v-2h2v2zm2.07-7.75l-.9.92C13.45 12.9 13 13.5 13 15h-2v-.5c0-1.1.45-2.1 1.17-2.83l1.24-1.26c.37-.36.59-.86.59-1.41 0-1.1-.9-2-2-2s-2 .9-2 2H8c0-2.21 1.79-4 4-4s4 1.79 4 4c0 .88-.36 1.68-.93 2.25z"/></svg></span>
                                                            {this.state.tooltip !== null ? 
                                                                    <this.state.tooltip place="right" type="light" effect='solid' id='segmentations' delayHide={100} delayUpdate={100}>
                                                                        <div className="tooltip-wrapper">
                                                                            <div className="inner-tooltip-wrapper">
                                                                                <div className="tooltip-header clearfix">
                                                                                    <div className="analytics-tooltip-title">
                                                                                        <span>Segmentations </span>
                                                                                    </div>

                                                                                </div>
                                                                                <div className="tooltip-about-wrapper">
                                                                                    <span>ABOUT</span>
                                                                                    <p>An overall organisational Identification model for potential Advocates and Detractors.</p>
                                                                                </div>
                                                                            </div>
                                                                        </div>           
                                                                    </this.state.tooltip>
                                                                :''  
                                                                }
                                                                </div>
                                                                <div className="csClmWpr clearfix">
                                                                <div class="culture-score-detractors csClm">
                                                                    <div class="culture-score-detractors-count">
                                                                    <span className="csicnwrpr"><i class="material-icons">mood_bad</i></span>
                                                                    <span class="detractors-count cscnt">
                                                                            {typeof cultureData.engagementData.detractors_percentage!=="undefined"?Math.round(cultureData.engagementData.detractors_percentage):0}
                                                                            %
                                                                        </span></div><span class="detractors-content">
                                                                        Detractors<span className="cultureNumber">({typeof cultureData.engagementData.detractors!=="undefined"?cultureData.engagementData.detractors:0})</span>
                                                                    </span>
                                                                </div>
                                                                <div class="cspd csClm">
                                                                    <div class="culture-score-potential-detractors-count">
                                                                    <span className="csicnwrpr"><i class="material-icons">sentiment_very_dissatisfied</i></span>
                                                                    <span class="detractors-count cscnt">
                                                                        {typeof cultureData.engagementData.potential_detractors_percentage!=="undefined"?Math.round(cultureData.engagementData.potential_detractors_percentage):0}
                                                                            %
                                                                        </span></div><span class="detractors-content">
                                                                        Potential Detractors<span className="cultureNumber">( {typeof cultureData.engagementData.potential_detractors!=="undefined"?cultureData.engagementData.potential_detractors:0} )</span>
                                                                    </span>
                                                                </div>
                                                                <div class="culture-score-passives csClm">
                                                                    <div class="culture-score-passives-count">
                                                                    
                                                                    <span className="csicnwrpr"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                                            viewBox="0 0 24 24">
                                                                            <circle cx="15.5" cy="9.5" r="1.5"></circle>
                                                                            <circle cx="8.5" cy="9.5" r="1.5"></circle>
                                                                            <path d="M11.99 2C6.47 2 2 6.48 2 12s4.47 10 9.99 10C17.52 22 22 17.52 22 12S17.52 2 11.99 2zM12 20c-4.42 0-8-3.58-8-8s3.58-8 8-8 8 3.58 8 8-3.58 8-8 8zm0-6c-2.33 0-4.32"></path>
                                                                        </svg>
                                                                        </span>
                                                                        <span class="passives-count cscnt">
                                                                        {typeof cultureData.engagementData.passives_percentage!=="undefined"?Math.round(cultureData.engagementData.passives_percentage):0}
                                                                            %
                                                                        </span></div><span class="passives-content">
                                                                        Passives<span className="cultureNumber">( {typeof cultureData.engagementData.passives!=="undefined"?cultureData.engagementData.passives:0} )</span>
                                                                    </span>
                                                                </div>
                                                                <div class="cspotential csClm">
                                                                    <div class="culture-score-promotors-count">
                                                                    <span className="csicnwrpr"><i class="material-icons">sentiment_satisfied_alt</i></span>
                                                                    <span class="promotors-count cscnt">
                                                                        {typeof cultureData.engagementData.potential_advocates_percentage!=="undefined"?Math.round(cultureData.engagementData.potential_advocates_percentage):0}
                                                                            %
                                                                        </span></div><span class="promotors-content">
                                                                        Potential Promoters<span className="cultureNumber">( {typeof cultureData.engagementData.potential_advocates!=="undefined"?cultureData.engagementData.potential_advocates:0} )</span>
                                                                    </span>
                                                                </div>
                                                                <div class="culture-score-promotors csClm">
                                                                        <div class="culture-score-promotors-count">
                                                                        <span className="csicnwrpr"><i class="material-icons">sentiment_very_satisfied</i></span>
                                                                        <span
                                                                            class="promotors-count cscnt">
                                                                            {typeof cultureData.engagementData.advocates_percentage!=="undefined"?Math.round(cultureData.engagementData.advocates_percentage):0}
                                                                            %
                                                                        </span>
                                                                        </div>
                                                                        <span class="promotors-content">
                                                                        Promoters<span className="cultureNumber">( {typeof cultureData.engagementData.advocates!=="undefined"?cultureData.engagementData.advocates:0} )</span>
                                                                        </span>
                                                                </div>
                                                                </div>
                                                    </div>
                                            </div>
                                        </div>
                                        </div> 
                                    }
                                                                       
                            </div>
                            <div className="participation-main-wrapper participation-linear-wrapper">
                            {cultureData.cultureParticipationLoading==true?
                                this.participationLoaderBox()
                            :
                                    <div className="culture-score-graph-wrapper clearfix">
                                                    <div className="header-analytics-wrapper clearfix">
                                                        <div className="title"> Participation<span className="svg-icon help-element"><svg xmlns="http://www.w3.org/2000/svg" data-tip  data-for='participationCulture' width="24" height="24" viewBox="0 0 24 24"><path d="M0 0h24v24H0z" fill="none"/><path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm1 17h-2v-2h2v2zm2.07-7.75l-.9.92C13.45 12.9 13 13.5 13 15h-2v-.5c0-1.1.45-2.1 1.17-2.83l1.24-1.26c.37-.36.59-.86.59-1.41 0-1.1-.9-2-2-2s-2 .9-2 2H8c0-2.21 1.79-4 4-4s4 1.79 4 4c0 .88-.36 1.68-.93 2.25z"/></svg></span>
                                                            {this.state.tooltip !== null ? 
                                                                    <this.state.tooltip place="right" type="light" effect='solid' id='participationCulture' delayHide={100} delayUpdate={100}>
                                                                        <div className="tooltip-wrapper">
                                                                            <div className="inner-tooltip-wrapper">
                                                                                <div className="tooltip-header clearfix">
                                                                                    <div className="analytics-tooltip-title">
                                                                                        <span>Participation</span>
                                                                                    </div>

                                                                                </div>
                                                                                <div className="tooltip-about-wrapper">
                                                                                    <span>ABOUT</span>
                                                                                    <p>Total number and percentage of employee users who participated in the 3P pulse questions.</p>
                                                                                </div>
                                                                            </div>
                                                                        </div>           
                                                                    </this.state.tooltip>
                                                                :''  
                                                                }
                                                                </div>


                                                                <div class="expand-year-main-wrapper">
                                                                    <div class="expand-year-wrapper">
                                                                    <div className="yearly-filtered-data">
                                                                        <a className={`week-filter ${this.state.period=='week'?'active':''}`} onClick={this.selectedTimeSpan.bind(this,'week')}>  Week  </a>
                                                                        <a className={`month-filter ${this.state.period=='month'?'active':''}`} onClick={this.selectedTimeSpan.bind(this,'month')}>  Month  </a>
                                                                        <a className={`year-filter ${this.state.period=='year'?'active':''}`} onClick={this.selectedTimeSpan.bind(this,'year')}>Year </a>
                                                                        {/* <span className={`filterIcon`}><i class="material-icons">filter_list</i>  </span> */}
                                                                    </div>
                                                                    
                                                                    </div>
                                                                </div>
                                                        </div>
                                                        <div className="participation-piechart-wrapper cf piechart-wrapper">
                                                            {typeof cultureData.cultureParticipation.chart_data !== 'undefined' && cultureData.cultureParticipation.chart_data.length>0? 
                                                            <div class="culture-score-date-time-graph">
                                                                <div class="culture-score-graph-image">
                                                                {this.state.Recharts!==null?
                                                                <div>

                                                                        <this.state.Recharts.LineChart width={300} height={100} data={cultureData.cultureParticipation.chart_data}>
                                                                            < this.state.Recharts.Line type='linear' dataKey='percentage' stroke='#4bcf99' strokeWidth={2} dot={false}/>
                                                                        </this.state.Recharts.LineChart>
                                                                </div>:''}
                                                                <span class="number">
                                                                    {typeof cultureData.cultureParticipation.diffrence!=="undefined"?cultureData.cultureParticipation.diffrence:0}
                                                                    %
                                                                </span>
                                                                </div>
                                                                <div className="culture-score-date">
                                                                    <div>Change since</div> 
                                                                    <div>{this.state.currentDate}</div>
                                                                </div>
                                                            </div>
                                                            : <div className = "no-Value-chart">No Data Found</div>}
                                                                {typeof cultureData.cultureParticipation!=='undefined'?
                                                                    <div className="legendUlWrapper">
                                                                        <ul>
                                                                            {typeof cultureData.cultureParticipation.questions!=='undefined'?
                                                                                <li key={1}>
                                                                                    <span  className = "rounder-dot" ></span>
                                                                                    <span className ="title-active-user">Questions</span>
                                                                                    <span className="text-no">{cultureData.cultureParticipation.questions}</span>
                                                                                </li>:''}
                                                                            {typeof cultureData.cultureParticipation.answers!=='undefined'?
                                                                                <li key={2}>
                                                                                    <span  className = "rounder-dot" ></span>
                                                                                    <span className ="title-active-user">Responses</span>
                                                                                    <span className="text-no">{cultureData.cultureParticipation.answers}</span>
                                                                                </li>:''}
                                                                        </ul>
                                                                        </div>:''}
                                                        </div>
                                                   

                                                {/* below portion is for linear gauge chart */}
                                                <div className="linear-gauge-wrapper">
                                                    <div className="linear-gauge-title">
                                                    % of Response Received V/S No of Survey Sent
                                                    </div>
                                            <div className="linear-gauge clearfix">
                                                <div className="lgaWrapper">
                                                  <span className="linear-gauge-arrow" style={{"left":`${cultureData.cultureParticipation.percentage}%`}}></span>
                                                  </div>
                                                  <div className="linear-gauge-part">
                                                                    <ul>
                                                                        <li><span className="lgsclnmbr">0</span></li>
                                                                        <li></li>
                                                                        <li></li>
                                                                        <li></li>
                                                                        <li><span className="lgsclnmbr">25</span></li>
                                                                    </ul>
                                                  </div>
                                                  <div className="linear-gauge-part">
                                                  <ul>
                                                                        <li></li>
                                                                        <li></li>
                                                                        <li></li>
                                                                        <li></li>
                                                                        <li><span className="lgsclnmbr">50</span></li>
                                                                    </ul>
                                                  </div>
                                                  <div className="linear-gauge-part">
                                                  <ul>
                                                                        <li></li>
                                                                        <li></li>
                                                                        <li></li>
                                                                        <li></li>
                                                                        <li><span className="lgsclnmbr">75</span></li>
                                                                    </ul>
                                                    </div>
                                                  <div className="linear-gauge-part">
                                                  <ul>
                                                                        <li></li>
                                                                        <li></li>
                                                                        <li></li>
                                                                        <li></li>
                                                                        <li><span className="lgsclnmbr">100</span></li>
                                                                    </ul>
                                                  </div>
                                            </div>
                                            </div>
                                        </div>}
                                    </div>

                            </div>    
        )
    }
}

function mapStateToProps(state){
    return{

    }
}

function mapDispatchToProps(dispatch){
    return{

    }
}

var connection = connect(mapStateToProps,mapDispatchToProps);
module.exports=connection(EngagementScore);

