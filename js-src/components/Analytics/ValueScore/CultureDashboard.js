import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as utils from '../../../utils/utils';
import EngageMentScroe from './EngagementScore';
import recharts from '../../Chunks/ChunkRecharts';
import reactTooltip from '../../Chunks/ChunkReactTooltip';
import moment from '../../Chunks/ChunkMoment';
var screenWidth = '';
const COLORS = ['#ff5e62', '#2fbf71', '#256eff','#ffbf00', '#cc6699', '#ff6a00', '#ffd700', '#b0e0e6', '#7A9E9F', '#254441', '#F9ADA0', '#577399', '#A93F55', '#ff7373','#ff9966', '#ccff99', '#99ffcc', '#33ccff', '#33cccc', '#00cc99', '#ccffcc', '#ffcccc', '#ffcc99', '#b3d9ff' ,'#e6e6fa', '#808080']
class CultureDashboard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            Recharts: null,
            tooltip: null,
            moment: null,
            hideChart: false,
            hideData: false,
            period: 'month',
            // peopleDriveList: false,
            // prideDriverList: false,
            // purposeDriverList: false,
            ProductivityDriverList: false,
            CapabilityDriverList: false,
            InnovationDriverList: false,
            CustomerExperienceDriverList: false,
            AdvocacyDriverList: false,
        }
    }

    componentWillMount() {
        moment().then(moment => {
            recharts()
                .then(recharts => {
                    reactTooltip().then(reactTooltip => {
                        this.setState({ Recharts: recharts, tooltip: reactTooltip, moment: moment })
                    })
                })
        })
            .catch(err => {
                throw err
            })
    }

    componentDidMount() {
        var e = this;
        window.addEventListener("resize", function () {
            var width = document.body.clientWidth;
            var height = document.body.clientHeight;
            if (screenWidth < 1500 && width >= 1500) {
                screenWidth = width
                e.setState({ refreshChart: true });
            } else if (screenWidth > 1500 && width <= 1500) {
                screenWidth = width
                e.setState({ refreshChart: true });
            } else if (screenWidth > 767 && width <= 767) {
                screenWidth = width
                e.setState({ refreshChart: true });
            } else if (screenWidth < 767 && width >= 767) {
                screenWidth = width
                e.setState({ refreshChart: true });
            } else {
                if (e.state.refreshChart == true) {
                    e.setState({ refreshChart: false });
                }
                screenWidth = width
            }

        }, true);

        this.setState({
            hideData: {
                ...this.state.hideData,
                ["Optimisation"]: true,
                ["Clarity"]: true,
                ["Coordination"]: true,
                ["Decisiveness"]: true,
                ["Enablement"]: true,
                ["Empowerment"]: true,
                ["Performance"]: true,
                ["Processes"]: true,
                ["Prioritisation"]: true,
                ["Inclusiveness"]: true,
                ["Safety"]: true,
                ["Encouragement"]: true,
                ["Team-orientated"]: true,
                ["Customer-centric"]: true,
                ["Responsive"]: true,
                ["Collaborative"]: true,
                ["Belief"]: true,
                ["Fulfillment"]: true,
                ["Integrity"]: true,
                ["Alignment"]: true,
            }
        })
    }
    renderLoading() {
        return (
            <div className="culture-index-loader">
                <div className="culture-score">
                    <div className="culture-score-header clearfix">
                        <div className="culture-score-wrapper loader-grey-line loader-line-radius loader-line-height">

                        </div>
                        <div className="expand-wrapper loader-grey-line loader-line-radius loader-line-height">

                        </div>

                    </div>
                    <div className="culture-score-number-value-wrapper clearfix">

                        <div className="culture-score-number">
                            <div className="culture-index-number loader-grey-line loader-line-radius loader-line-height">
                            </div>
                        </div>
                        <div className="culture-score-value">
                            <div className="cultur-score-value-wrapper">
                                <div className="culture-value-icon-content clearfix">
                                    <div className="culture-value-icon loader-grey-line loader-line-radius loader-line-height">

                                    </div>
                                    <div className="culture-score-value-content">
                                        <div className="promoted-value loader-grey-line loader-line-radius loader-line-height"></div>
                                        <div className="promoted-value-wrapper loader-grey-line loader-line-radius loader-line-height"></div>
                                    </div>
                                </div>
                                <div className="culture-value-icon-content clearfix">
                                    <div className="culture-value-icon loader-grey-line loader-line-radius loader-line-height">

                                    </div>
                                    <div className="culture-score-value-content">
                                        <div className="promoted-value loader-grey-line loader-line-radius loader-line-height"></div>

                                        <div className="promoted-value-wrapper loader-grey-line loader-line-radius loader-line-height"></div>
                                    </div>
                                </div>
                            </div>
                            <div className="culture-score-percentage">
                                <div className="culture-score-percentage-wrapper loader-grey-line loader-line-radius loader-line-height"></div>
                                <div className="culture-score-percentage-wrapper loader-grey-line loader-line-radius loader-line-height"></div>
                            </div>
                        </div>



                    </div>
                    <div className="culture-score-graph-wrapper clearfix">
                        <div className="culture-score-date-time-graph">
                            <div className="culture-score-graph-img-loader loader-grey-line loader-line-radius loader-line-height"></div>
                            <div className="culture-score-date loader-grey-line loader-line-radius loader-line-height"></div>
                        </div>
                        <div className="culture-score-detractors csClm">
                            <div className="culture-score-detractors-count cscnt">
                                <svg class="detractors-loader loader-grey-line loader-line-radius loader-line-height" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"></svg><label className="detractors-count loader-grey-line loader-line-radius loader-line-height">
                                </label>
                            </div>
                            <div className="detractors-content loader-grey-line loader-line-radius loader-line-height"></div>


                        </div>
                        <div className="culture-score-passives csClm">
                            <div className="culture-score-passives-count cscnt">
                                <svg class="detractors-loader loader-grey-line loader-line-radius loader-line-height" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                </svg><label class="passives-count loader-grey-line loader-line-radius loader-line-height"></label>

                            </div>
                            <div className="passives-content loader-grey-line loader-line-radius loader-line-height"></div>


                        </div>
                        <div className="culture-score-promotors csClm">
                            <div className="culture-score-promotors-count cscnt">

                                <i class="material-icons culture-count-loader loader-grey-line loader-line-radius loader-line-height"></i>
                                <label class="promotors-count loader-grey-line loader-line-radius loader-line-height"></label>
                            </div>
                            <div className="promotors-content loader-grey-line loader-line-radius loader-line-height"></div>


                        </div>
                        <div className="culture-score-promotors csClm">
                            <div className="culture-score-promotors-count cscnt">

                                <i class="material-icons culture-count-loader loader-grey-line loader-line-radius loader-line-height"></i>
                                <label class="promotors-count loader-grey-line loader-line-radius loader-line-height"></label>
                            </div>
                            <div className="promotors-content loader-grey-line loader-line-radius loader-line-height"></div>


                        </div>
                        <div className="culture-score-promotors csClm">
                            <div className="culture-score-promotors-count cscnt">

                                <i class="material-icons culture-count-loader loader-grey-line loader-line-radius loader-line-height"></i>
                                <label class="promotors-count loader-grey-line loader-line-radius loader-line-height"></label>
                            </div>
                            <div className="promotors-content loader-grey-line loader-line-radius loader-line-height"></div>


                        </div>
                    </div>
                </div>
            </div>
        )
    }

    convertTimeStampToDate = (date, props) => {
        var period = this.state.period
        var dateFormat = period !== undefined ? period : 'month'
        var newFormat = ''
        if (dateFormat == 'month' || dateFormat == 'week') {
            newFormat = 'DD MMM'
        } else if (dateFormat == 'year') {
            newFormat = 'MMM YY'
        }
        return (
            this.state.moment ? this.state.moment.unix(date).format(newFormat) : ''
        )
    }

    customizeTooltip = (date) => {
        return (
            this.state.moment ? this.state.moment.unix(date).format('DD-MM-YYYY') : ''
        )
    }

    hideSelectedLegend(e) {
        if (this.state.hideData[e] == true) {
            this.setState({
                hideData: { ...this.state.hideData, [e]: false }
            });
        } else {
            this.setState({
                hideData: {
                    ...this.state.hideData,
                    [e]: true
                }
            });
        }
    }
    handleDriverClick(event) {
        this.setState({
            hideChart: { ...this.state.hideChart, [event]: !this.state.hideChart[event] }
        })
    }

    UpdatedLegend = (props) => {
        const { payload, countData } = props;
        if (payload.length > 0) {
            var LegendKeys = Object.keys(payload)
            return (
                <ul>
                    {payload.length > 0 ? payload.map((entry, index) => (

                        this.state.hideData[entry.dataKey] == undefined ?
                            <li className="clearfix" key={index} >
                                <div className="culture-legend-li-inner" >
                                    <span className="rounder-dot" style={{ background: entry.color }} ></span>
                                    <span className={`title-active-user ${this.state.hideChart[entry.dataKey] !== undefined && this.state.hideChart[entry.dataKey] == true ? 'strikeLegend' : ''}`} onClick={this.handleDriverClick.bind(this, entry.dataKey)}> {entry.dataKey.charAt(0).toUpperCase() + entry.dataKey.slice(1)}  </span> <span className="text-no">{countData[entry.dataKey]}</span>
                                    <span className="clarrow" onClick={this.clickMap.bind(this, entry.dataKey)} id={`driverList-${entry.dataKey}`}> </span>
                                </div>
                                {/* { entry.dataKey=="people"?
                           this.state.peopleDriveList==true?
                                <ul>
                                    <li onClick={this.hideSelectedLegend.bind(this,"inclusion")}>
                                        <span className="rounder-dot" style={{ background: COLORS[3] }}></span>
                                        <span className={`title-active-user ${this.state.hideData["inclusion"] !== undefined && this.state.hideData["inclusion"] == true ? 'strikeLegend' : ''}`} > Inclusion  </span> <span className="text-no">{countData["inclusion"]}</span>
                                    </li>
                                    <li onClick={this.hideSelectedLegend.bind(this,"fairness")}>
                                        <span className="rounder-dot" style={{ background:  COLORS[5] }}></span>
                                        <span className={`title-active-user ${this.state.hideData["fairness"] !== undefined && this.state.hideData["fairness"] == true ? 'strikeLegend' : ''}`}> Fairness  </span> <span className="text-no">{countData["fairness"]}</span></li>
                                    <li onClick={this.hideSelectedLegend.bind(this,"belonging")}>                                        
                                        <span className="rounder-dot" style={{ background:  COLORS[4] }}></span>
                                        <span className={`title-active-user ${this.state.hideData["belonging"] !== undefined && this.state.hideData["belonging"] == true ? 'strikeLegend' : ''}`}> Belonging  </span> <span className="text-no">{countData["belonging"]}</span></li>
                                </ul>:'' */}
                                {entry.dataKey == "Productivity" ?
                                    this.state.ProductivityDriverList == true ?
                                        <ul>
                                            <li onClick={this.hideSelectedLegend.bind(this, "Optimisation")}>
                                                <span className="rounder-dot" style={{ background: COLORS[6] }}></span>
                                                <span className={`title-active-user ${this.state.hideData["Optimisation"] !== undefined && this.state.hideData["Optimisation"] == true ? 'strikeLegend' : ''}`} > Optimisation  </span> <span className="text-no">{countData["Optimisation"]}</span>
                                            </li>
                                            <li onClick={this.hideSelectedLegend.bind(this, "Clarity")}>
                                                <span className="rounder-dot" style={{ background: COLORS[7] }}></span>
                                                <span className={`title-active-user ${this.state.hideData["Clarity"] !== undefined && this.state.hideData["Clarity"] == true ? 'strikeLegend' : ''}`}> Clarity  </span> <span className="text-no">{countData["Clarity"]}</span></li>
                                            <li onClick={this.hideSelectedLegend.bind(this, "Coordination")}>
                                                <span className="rounder-dot" style={{ background: COLORS[8] }}></span>
                                                <span className={`title-active-user ${this.state.hideData["Coordination"] !== undefined && this.state.hideData["Coordination"] == true ? 'strikeLegend' : ''}`}> Coordination  </span> <span className="text-no">{countData["Coordination"]}</span></li>
                                            <li onClick={this.hideSelectedLegend.bind(this, "Decisiveness")}>
                                                <span className="rounder-dot" style={{ background: COLORS[9] }}></span>
                                                <span className={`title-active-user ${this.state.hideData["Decisiveness"] !== undefined && this.state.hideData["Decisiveness"] == true ? 'strikeLegend' : ''}`}> Decisiveness  </span> <span className="text-no">{countData["Decisiveness"]}</span></li>
                                        </ul> : ''
                                    //    : entry.dataKey=="purpose"?
                                    //    this.state.purposeDriverList==true?
                                    //    <ul>
                                    //         <li onClick={this.hideSelectedLegend.bind(this,"achievement")}>
                                    //             <span className="rounder-dot" style={{ background: COLORS[6] }}></span>
                                    //             <span className={`title-active-user ${this.state.hideData["achievement"] !== undefined && this.state.hideData["achievement"] == true ? 'strikeLegend' : ''}`}> Achievement  </span> <span className="text-no">{countData["achievement"]}</span>
                                    //         </li>
                                    //         <li onClick={this.hideSelectedLegend.bind(this,"insight")}>                                        
                                    //             <span className="rounder-dot" style={{ background:  COLORS[7] }}></span>
                                    //             <span className={`title-active-user ${this.state.hideData["insight"] !== undefined && this.state.hideData["insight"] == true ? 'strikeLegend' : ''}`}> Insight  </span> <span className="text-no">{countData["insight"]}</span></li>
                                    //         <li onClick={this.hideSelectedLegend.bind(this,"autonomy")}>                                        
                                    //             <span className="rounder-dot" style={{ background:  COLORS[8] }}></span>
                                    //             <span className={`title-active-user ${this.state.hideData["autonomy"] !== undefined && this.state.hideData["autonomy"] == true ? 'strikeLegend' : ''}`}> Autonomy  </span> <span className="text-no">{countData["autonomy"]}</span></li>
                                    //     </ul>:''
                                    : entry.dataKey == "Capability" ?
                                        this.state.CapabilityDriverList == true ?
                                            <ul>
                                                <li onClick={this.hideSelectedLegend.bind(this, "Enablement")}>
                                                    <span className="rounder-dot" style={{ background: COLORS[10] }}></span>
                                                    <span className={`title-active-user ${this.state.hideData["Enablement"] !== undefined && this.state.hideData["Enablement"] == true ? 'strikeLegend' : ''}`}> Enablement  </span> <span className="text-no">{countData["Enablement"]}</span>
                                                </li>
                                                <li onClick={this.hideSelectedLegend.bind(this, "Empowerment")}>
                                                    <span className="rounder-dot" style={{ background: COLORS[11] }}></span>
                                                    <span className={`title-active-user ${this.state.hideData["Empowerment"] !== undefined && this.state.hideData["Empowerment"] == true ? 'strikeLegend' : ''}`}> Empowerment  </span> <span className="text-no">{countData["Empowerment"]}</span></li>
                                                <li onClick={this.hideSelectedLegend.bind(this, "Performance")}>
                                                    <span className="rounder-dot" style={{ background: COLORS[12] }}></span>
                                                    <span className={`title-active-user ${this.state.hideData["Performance"] !== undefined && this.state.hideData["Performance"] == true ? 'strikeLegend' : ''}`}> Performance  </span> <span className="text-no">{countData["Performance"]}</span></li>
                                                <li onClick={this.hideSelectedLegend.bind(this, "Processes")}>
                                                    <span className="rounder-dot" style={{ background: COLORS[13] }}></span>
                                                    <span className={`title-active-user ${this.state.hideData["Processes"] !== undefined && this.state.hideData["Processes"] == true ? 'strikeLegend' : ''}`}> Processes  </span> <span className="text-no">{countData["Processes"]}</span></li>
                                            </ul> : ''
                                        // :entry.dataKey=="pride"?
                                        // this.state.prideDriverList==true?
                                        // <ul>
                                        //     <li onClick={this.hideSelectedLegend.bind(this,"appreciation")}>
                                        //         <span className="rounder-dot" style={{ background: COLORS[9] }}></span>
                                        //         <span className={`title-active-user ${this.state.hideData["appreciation"] !== undefined && this.state.hideData["appreciation"] == true ? 'strikeLegend' : ''}`}> Appreciation  </span> <span className="text-no">{countData["appreciation"]}</span>
                                        //     </li>
                                        //     <li onClick={this.hideSelectedLegend.bind(this,"development")}>                                        
                                        //         <span className="rounder-dot" style={{ background:  COLORS[10] }}></span>
                                        //         <span className={`title-active-user ${this.state.hideData["development"] !== undefined && this.state.hideData["development"] == true ? 'strikeLegend' : ''}`}> Development  </span> <span className="text-no">{countData["development"]}</span></li>
                                        //     <li onClick={this.hideSelectedLegend.bind(this,"affection")}>                                       
                                        //          <span className="rounder-dot" style={{ background:  COLORS[11] }}></span>
                                        //         <span className={`title-active-user ${this.state.hideData["affection"] !== undefined && this.state.hideData["affection"] == true ? 'strikeLegend' : ''}`}> Affection  </span> <span className="text-no">{countData["affection"]}</span></li>
                                        // </ul>:''
                                        // :   ''}
                                        : entry.dataKey == "Innovation" ?
                                            this.state.InnovationDriverList == true ?
                                                <ul>
                                                    <li onClick={this.hideSelectedLegend.bind(this, "Prioritisation")}>
                                                        <span className="rounder-dot" style={{ background: COLORS[14] }}></span>
                                                        <span className={`title-active-user ${this.state.hideData["Prioritisation"] !== undefined && this.state.hideData["Prioritisation"] == true ? 'strikeLegend' : ''}`}> Prioritisation  </span> <span className="text-no">{countData["Prioritisation"]}</span>
                                                    </li>
                                                    <li onClick={this.hideSelectedLegend.bind(this, "Inclusiveness")}>
                                                        <span className="rounder-dot" style={{ background: COLORS[15] }}></span>
                                                        <span className={`title-active-user ${this.state.hideData["Inclusiveness"] !== undefined && this.state.hideData["Inclusiveness"] == true ? 'strikeLegend' : ''}`}> Inclusiveness  </span> <span className="text-no">{countData["Inclusiveness"]}</span></li>
                                                    <li onClick={this.hideSelectedLegend.bind(this, "Safety")}>
                                                        <span className="rounder-dot" style={{ background: COLORS[16] }}></span>
                                                        <span className={`title-active-user ${this.state.hideData["Safety"] !== undefined && this.state.hideData["Safety"] == true ? 'strikeLegend' : ''}`}> Safety  </span> <span className="text-no">{countData["Safety"]}</span></li>
                                                    <li onClick={this.hideSelectedLegend.bind(this, "Encouragement")}>
                                                        <span className="rounder-dot" style={{ background: COLORS[17] }}></span>
                                                        <span className={`title-active-user ${this.state.hideData["Encouragement"] !== undefined && this.state.hideData["Encouragement"] == true ? 'strikeLegend' : ''}`}> Encouragement  </span> <span className="text-no">{countData["Encouragement"]}</span></li>
                                                </ul> : ''
                                            : entry.dataKey == "Customer-Experience" ?
                                                this.state.CustomerExperienceDriverList == true ?
                                                    <ul>
                                                        <li onClick={this.hideSelectedLegend.bind(this, "Team-orientated")}>
                                                            <span className="rounder-dot" style={{ background: COLORS[18] }}></span>
                                                            <span className={`title-active-user ${this.state.hideData["Team-orientated"] !== undefined && this.state.hideData["Team-orientated"] == true ? 'strikeLegend' : ''}`}> Team orientated  </span> <span className="text-no">{countData["Team-orientated"]}</span>
                                                        </li>
                                                        <li onClick={this.hideSelectedLegend.bind(this, "Customer-centric")}>
                                                            <span className="rounder-dot" style={{ background: COLORS[19] }}></span>
                                                            <span className={`title-active-user ${this.state.hideData["Customer-centric"] !== undefined && this.state.hideData["Customer-centric"] == true ? 'strikeLegend' : ''}`}> Customer-centric  </span> <span className="text-no">{countData["Customer-centric"]}</span></li>
                                                        <li onClick={this.hideSelectedLegend.bind(this, "Responsive")}>
                                                            <span className="rounder-dot" style={{ background: COLORS[20] }}></span>
                                                            <span className={`title-active-user ${this.state.hideData["Responsive"] !== undefined && this.state.hideData["Responsive"] == true ? 'strikeLegend' : ''}`}> Responsive  </span> <span className="text-no">{countData["Responsive"]}</span></li>
                                                        <li onClick={this.hideSelectedLegend.bind(this, "Collaborative")}>
                                                            <span className="rounder-dot" style={{ background: COLORS[21] }}></span>
                                                            <span className={`title-active-user ${this.state.hideData["Collaborative"] !== undefined && this.state.hideData["Collaborative"] == true ? 'strikeLegend' : ''}`}> Collaborative  </span> <span className="text-no">{countData["Collaborative"]}</span></li>
                                                    </ul> : ''
                                                : entry.dataKey == "Advocacy" ?
                                                    this.state.AdvocacyDriverList == true ?
                                                        <ul>
                                                            <li onClick={this.hideSelectedLegend.bind(this, "Belief")}>
                                                                <span className="rounder-dot" style={{ background: COLORS[22] }}></span>
                                                                <span className={`title-active-user ${this.state.hideData["Belief"] !== undefined && this.state.hideData["Belief"] == true ? 'strikeLegend' : ''}`}> Belief  </span> <span className="text-no">{countData["Belief"]}</span>
                                                            </li>
                                                            <li onClick={this.hideSelectedLegend.bind(this, "Fulfillment")}>
                                                                <span className="rounder-dot" style={{ background: COLORS[23] }}></span>
                                                                <span className={`title-active-user ${this.state.hideData["Fulfillment"] !== undefined && this.state.hideData["Fulfillment"] == true ? 'strikeLegend' : ''}`}> Fulfillment  </span> <span className="text-no">{countData["Fulfillment"]}</span></li>
                                                            <li onClick={this.hideSelectedLegend.bind(this, "Integrity")}>
                                                                <span className="rounder-dot" style={{ background: COLORS[24] }}></span>
                                                                <span className={`title-active-user ${this.state.hideData["Integrity"] !== undefined && this.state.hideData["Integrity"] == true ? 'strikeLegend' : ''}`}> Integrity  </span> <span className="text-no">{countData["Integrity"]}</span></li>
                                                            <li onClick={this.hideSelectedLegend.bind(this, "Alignment")}>
                                                                <span className="rounder-dot" style={{ background: COLORS[25] }}></span>
                                                                <span className={`title-active-user ${this.state.hideData["Alignment"] !== undefined && this.state.hideData["Alignment"] == true ? 'strikeLegend' : ''}`}> Alignment  </span> <span className="text-no">{countData["Alignment"]}</span></li>
                                                        </ul> : ''
                                                    : ''}

                            </li>
                            : ''
                    )) : ''}
                </ul>
            );
        }
    }
    clickMap(targetLine) {
        switch (targetLine) {
            // case "pride":
            //     if(this.state.prideDriverList==true){
            //         document.getElementById(`driverList-${targetLine}`).classList.remove('active');
            //         this.setState({
            //             prideDriverList:!this.state.prideDriverList,
            //             hideData:{...this.state.hideData,
            //                     ["affection"]:true,
            //                     ["appreciation"]:true,
            //                     ["development"]:true}
            //         }) 
            //     }else{
            //         document.getElementById(`driverList-${targetLine}`).classList.add('active');
            //     this.setState({
            //         prideDriverList:!this.state.prideDriverList,
            //         hideData:{...this.state.hideData,
            //                 ["affection"]:!this.state.hideData["affection"],
            //                 ["appreciation"]:!this.state.hideData["appreciation"],
            //                 ["development"]:!this.state.hideData["development"]}
            //     })}
            //     break;
            case "Productivity":
                if (this.state.ProductivityDriverList == true) {
                    document.getElementById(`driverList-${targetLine}`).classList.remove('active');
                    this.setState({
                        ProductivityDriverList: !this.state.ProductivityDriverList,
                        hideData: {
                            ...this.state.hideData,
                            ["Optimisation"]: true,
                            ["Clarity"]: true,
                            ["Coordination"]: true,
                            ["Decisiveness"]: true
                        }
                    })
                } else {
                    document.getElementById(`driverList-${targetLine}`).classList.add('active');
                    this.setState({
                        ProductivityDriverList: !this.state.ProductivityDriverList,
                        hideData: {
                            ...this.state.hideData,
                            ["Optimisation"]: !this.state.hideData["Optimisation"],
                            ["Clarity"]: !this.state.hideData["Clarity"],
                            ["Coordination"]: !this.state.hideData["Coordination"],
                            ["Decisiveness"]: !this.state.hideData["Decisiveness"]
                        }
                    })
                }
                break;
            case "Capability":
                if (this.state.CapabilityDriverList == true) {
                    document.getElementById(`driverList-${targetLine}`).classList.remove('active');
                    this.setState({
                        CapabilityDriverList: !this.state.CapabilityDriverList,
                        hideData: {
                            ...this.state.hideData,
                            ["Enablement"]: true,
                            ["Empowerment"]: true,
                            ["Performance"]: true,
                            ["Processes"]: true
                        }
                    })
                } else {
                    document.getElementById(`driverList-${targetLine}`).classList.add('active');
                    this.setState({
                        CapabilityDriverList: !this.state.CapabilityDriverList,
                        hideData: {
                            ...this.state.hideData,
                            ["Enablement"]: !this.state.hideData["Enablement"],
                            ["Empowerment"]: !this.state.hideData["Empowerment"],
                            ["Performance"]: !this.state.hideData["Performance"],
                            ["Processes"]: !this.state.hideData["Processes"]
                        }
                    })
                }
                break;
            case "Innovation":
                if (this.state.InnovationDriverList == true) {
                    document.getElementById(`driverList-${targetLine}`).classList.remove('active');
                    this.setState({
                        InnovationDriverList: !this.state.InnovationDriverList,
                        hideData: {
                            ...this.state.hideData,
                            ["Prioritisation"]: true,
                            ["Inclusiveness"]: true,
                            ["Safety"]: true,
                            ["Encouragement"]: true
                        }
                    })
                } else {
                    document.getElementById(`driverList-${targetLine}`).classList.add('active');
                    this.setState({
                        InnovationDriverList: !this.state.InnovationDriverList,
                        hideData: {
                            ...this.state.hideData,
                            ["Prioritisation"]: !this.state.hideData["Prioritisation"],
                            ["Inclusiveness"]: !this.state.hideData["Inclusiveness"],
                            ["Safety"]: !this.state.hideData["Safety"],
                            ["Encouragement"]: !this.state.hideData["Encouragement"]
                        }
                    })
                }
                break;
            case "Customer-Experience":
                if (this.state.purposeDriverList == true) {
                    document.getElementById(`driverList-${targetLine}`).classList.remove('active');
                    this.setState({
                        CustomerExperienceDriverList: !this.state.CustomerExperienceDriverList,
                        hideData: {
                            ...this.state.hideData,
                            ["Team-orientated"]: true,
                            ["Customer-centric"]: true,
                            ["Responsive"]: true,
                            ["Collaborative"]: true
                        }
                    })
                } else {
                    document.getElementById(`driverList-${targetLine}`).classList.add('active');
                    this.setState({
                        CustomerExperienceDriverList: !this.state.CustomerExperienceDriverList,
                        hideData: {
                            ...this.state.hideData,
                            ["Team-orientated"]: !this.state.hideData["Team-orientated"],
                            ["Customer-centric"]: !this.state.hideData["Customer-centric"],
                            ["Responsive"]: !this.state.hideData["Responsive"],
                            ["Collaborative"]: !this.state.hideData["Collaborative"]
                        }
                    })
                }
                break;
            case "Advocacy":
                if (this.state.AdvocacyDriverList == true) {
                    document.getElementById(`driverList-${targetLine}`).classList.remove('active');
                    this.setState({
                        AdvocacyDriverList: !this.state.AdvocacyDriverList,
                        hideData: {
                            ...this.state.hideData,
                            ["Belief"]: true,
                            ["Fulfillment"]: true,
                            ["Integrity"]: true,
                            ["Alignment"]: true
                        }
                    })
                } else {
                    document.getElementById(`driverList-${targetLine}`).classList.add('active');
                    this.setState({
                        AdvocacyDriverList: !this.state.AdvocacyDriverList,
                        hideData: {
                            ...this.state.hideData,
                            ["Belief"]: !this.state.hideData["Belief"],
                            ["Fulfillment"]: !this.state.hideData["Fulfillment"],
                            ["Integrity"]: !this.state.hideData["Integrity"],
                            ["Alignment"]: !this.state.hideData["Alignment "]

                        }
                    })
                }
                break;
        }
    }
    selectedTimeSpan(time, method) {
        var currProps = this.props
        var ObjToSend = {
            'timezone': this.state.timezone,
            timespan: time
        }
        this.setState({
            period: time
        })
        if(method == 'driver')
        { this.props.fetchAllDriversData(ObjToSend) }
        else
        {this.props.fetchSurveyComparisonData(ObjToSend) }
        
    }

    renderChartLoading() {
        return (
            <div className="inner-adoption-chart">
                <div class="header-analytics-wrapper clearfix header-loader-wrapper-analytics">
                    <div className="title  loader-grey-line  loader-line-radius loader-line-height"></div>

                </div>
                <div className="bodyPartTotalPostExapnd" id='expandTotalPost'>
                    <div class="recharts-legend-wrapper">
                        <ul>
                            <li class="loader-grey-line loader-line-radius loader-line-height"> </li>
                            <li class="loader-grey-line loader-line-radius loader-line-height"></li>
                            <li class="loader-grey-line loader-line-radius loader-line-height"></li>
                            <li class="loader-grey-line loader-line-radius loader-line-height"></li>
                        </ul>
                    </div>
                    <div className="loader-grey-line  chart-container-loader"> </div>
                </div>
            </div>

        )
    }
    render() {
        var cultureData = this.props.culture;
        var windowDimention = utils.windowSize()
        var GraphDimention = utils.getChartDimension(windowDimention.w, 'driversView');
        var days = cultureData.allDriversData.chart ? cultureData.allDriversData.chart.length : ''
        var intervalDay = 0
        if (days > 31 && days <= 365) {
            intervalDay = 60
        } else if (days <= 31 && days > 7) {
            intervalDay = 3
        } else {
            intervalDay = 0
        }
        var strokeWidth = 4;
        var isDriverListOpen = this.state.ProductivityDriverList || this.state.CapabilityDriverList || this.state.InnovationDriverList || this.state.CustomerExperienceDriverList || this.state.AdvocacyDriverList ? true : false;
        return (
            <div className="culture-participation-mainbox dashboard-analytics-level1-container new-culture-section">
                <div className="culture-participation-wrapper clearfix engagementdata-wrapper">
                    {/* {cultureData.engagementDataLoading==true || cultureData.participationLoading==true?
                                <div>
                                    {this.renderLoading()}
                                </div>
                            : */}
                    <EngageMentScroe
                        culture={cultureData}
                        openHeatmapExpand={this.props.openHeatmapExpand}
                        fetchParticipationData={this.props.fetchParticipationData.bind(this)}
                    />
                    {/* } */}

                </div>
                
                {/* manager survey vs employee survey comparision chart */}
                <div class="tagged-voted-value culture-second-row-graphs driver-chart-wrapper">
                    <div class="tagged-voted-value-main-box clearfix">
                        <div class="vote-wrapper ">
                            <div class="participation">
                                <div class="participation-header clearfix">
                                    <div class="participation-wrapper">
                                        <div className="title"> Culture Gap<span className="svg-icon help-element"><svg xmlns="http://www.w3.org/2000/svg" data-tip data-for='drivers' width="24" height="24" viewBox="0 0 24 24"><path d="M0 0h24v24H0z" fill="none" /><path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm1 17h-2v-2h2v2zm2.07-7.75l-.9.92C13.45 12.9 13 13.5 13 15h-2v-.5c0-1.1.45-2.1 1.17-2.83l1.24-1.26c.37-.36.59-.86.59-1.41 0-1.1-.9-2-2-2s-2 .9-2 2H8c0-2.21 1.79-4 4-4s4 1.79 4 4c0 .88-.36 1.68-.93 2.25z" /></svg></span>
                                            {this.state.tooltip !== null ?
                                                <this.state.tooltip place="right" type="light" effect='solid' id='drivers' delayHide={100} delayUpdate={100}>
                                                    <div className="tooltip-wrapper">
                                                        <div className="inner-tooltip-wrapper">
                                                            <div className="tooltip-header clearfix">
                                                                <div className="analytics-tooltip-title">
                                                                    <span>Culture Gap</span>
                                                                </div>

                                                            </div>
                                                            <div className="tooltip-about-wrapper">
                                                                <span>ABOUT</span>
                                                                <p>Manager survey vs employee survey comparison chart.</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </this.state.tooltip>
                                                : ''
                                            }
                                        </div>
                                    </div>
                                    <div class="expand-year-main-wrapper">
                                        <div class="expand-year-wrapper">
                                            <div className="yearly-filtered-data">
                                                <a className={`month-filter ${this.state.period == 'month' ? 'active' : ''}`} onClick={this.selectedTimeSpan.bind(this, 'month', 'survey')}>  Month  </a>
                                                <a className={`year-filter ${this.state.period == 'year' ? 'active' : ''}`} onClick={this.selectedTimeSpan.bind(this, 'year', 'survey')}>Year </a>
                                                {/* <span className={`filterIcon`}><i class="material-icons">filter_list</i>  </span> */}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="voted-value-piechart clearfix piechart-wrapper">
                                    <div class={`chart-workplace-wrapper ${isDriverListOpen == true ? 'driver-selected' : ''}`} >
                                        <div className="Expand-design-wrapper">
                                            <div className="chart-container-in-analytics-wrapper ">
                                                <div className="inner-chart-container-in-analytics-wrapper">

                                                    <div className="bodyPartTotalPostExapnd culture-analytics-exapnd" id="driversData">
                                                        {
                                                            cultureData.allDriversDataLoading == true ?
                                                                this.renderChartLoading() :
                                                                this.state.Recharts !== null && cultureData.allDriversData.chart !== undefined ?
                                                                    <div>
                                                                        <this.state.Recharts.BarChart width={GraphDimention.width} height={GraphDimention.height} data={cultureData.surveyComparison} margin={{ top: 5, right: 30, left: 20, bottom: 5 }} >
                                                                            <this.state.Recharts.XAxis dataKey="name" />
                                                                            <this.state.Recharts.YAxis />
                                                                            <this.state.Recharts.Tooltip />
                                                                            <this.state.Recharts.Bar dataKey="manager_score" fill={COLORS[0]} />
                                                                            <this.state.Recharts.Bar dataKey="user_score" fill={COLORS[1]} />
                                                                        </this.state.Recharts.BarChart>
                                                                    </div>
                                                                    : <div>
                                                                        No Value Found
                                                            </div>
                                                        }
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div></div>
                        </div>


                {/* lasr */}
                <div class="tagged-voted-value culture-second-row-graphs driver-chart-wrapper">
                    <div class="tagged-voted-value-main-box clearfix">
                        <div class="vote-wrapper ">
                            <div class="participation">
                                <div class="participation-header clearfix">
                                    <div class="participation-wrapper">
                                        <div className="title"> Drivers & Sub Drivers Over Time Over Time<span className="svg-icon help-element"><svg xmlns="http://www.w3.org/2000/svg" data-tip data-for='drivers' width="24" height="24" viewBox="0 0 24 24"><path d="M0 0h24v24H0z" fill="none" /><path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm1 17h-2v-2h2v2zm2.07-7.75l-.9.92C13.45 12.9 13 13.5 13 15h-2v-.5c0-1.1.45-2.1 1.17-2.83l1.24-1.26c.37-.36.59-.86.59-1.41 0-1.1-.9-2-2-2s-2 .9-2 2H8c0-2.21 1.79-4 4-4s4 1.79 4 4c0 .88-.36 1.68-.93 2.25z" /></svg></span>
                                            {this.state.tooltip !== null ?
                                                <this.state.tooltip place="right" type="light" effect='solid' id='drivers' delayHide={100} delayUpdate={100}>
                                                    <div className="tooltip-wrapper">
                                                        <div className="inner-tooltip-wrapper">
                                                            <div className="tooltip-header clearfix">
                                                                <div className="analytics-tooltip-title">
                                                                    <span>Drivers & Sub Drivers Over Time</span>
                                                                </div>

                                                            </div>
                                                            <div className="tooltip-about-wrapper">
                                                                <span>ABOUT</span>
                                                                <p>A detailed culture analysis of Advocacy at the drivers and sub drivers over time.</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </this.state.tooltip>
                                                : ''
                                            }
                                        </div>
                                    </div>
                                    <div class="expand-year-main-wrapper">
                                        <div class="expand-year-wrapper">
                                            <div className="yearly-filtered-data">
                                                <a className={`month-filter ${this.state.period == 'month' ? 'active' : ''}`} onClick={this.selectedTimeSpan.bind(this, 'month', 'driver')}>  Month  </a>
                                                <a className={`year-filter ${this.state.period == 'year' ? 'active' : ''}`} onClick={this.selectedTimeSpan.bind(this, 'year', 'driver')}>Year </a>
                                                {/* <span className={`filterIcon`}><i class="material-icons">filter_list</i>  </span> */}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="voted-value-piechart clearfix piechart-wrapper">
                                    <div class={`chart-workplace-wrapper ${isDriverListOpen == true ? 'driver-selected' : ''}`} >
                                        <div className="Expand-design-wrapper">
                                            <div className="chart-container-in-analytics-wrapper ">
                                                <div className="inner-chart-container-in-analytics-wrapper">

                                                    <div className="bodyPartTotalPostExapnd culture-analytics-exapnd" id="driversData">

                                                        {
                                                            cultureData.allDriversDataLoading == true ?
                                                                this.renderChartLoading() :
                                                                this.state.Recharts !== null && cultureData.allDriversData.chart !== undefined ?
                                                                    <div>
                                                                        <this.state.Recharts.LineChart width={GraphDimention.width} height={GraphDimention.height} data={cultureData.allDriversData.chart} margin={{ top: 5, right: 30, left: 20, bottom: 5 }} >
                                                                            <this.state.Recharts.XAxis dataKey="date" tickFormatter={this.convertTimeStampToDate.bind(this)} tickSize={0} minTickGap={12} tickMargin={10} />
                                                                            <this.state.Recharts.YAxis />
                                                                            <this.state.Recharts.CartesianGrid strokeDasharray="0 0" vertical={false} stroke={"#f1f2f3"} />
                                                                            <this.state.Recharts.Tooltip labelFormatter={this.customizeTooltip.bind(this)} />
                                                                            <this.state.Recharts.Legend iconType="circle" verticalAlign="top" content={this.UpdatedLegend} countData={cultureData.allDriversData} />
                                                                            <this.state.Recharts.Line type="monotone" dataKey="Productivity" stroke={COLORS[0]} strokeWidth={strokeWidth} dot={false} hide={this.state.hideChart['Productivity']} />
                                                                            <this.state.Recharts.Line type="monotone" dataKey="Capability" stroke={COLORS[1]} strokeWidth={strokeWidth} dot={false} hide={this.state.hideChart['Capability']} />
                                                                            <this.state.Recharts.Line type="monotone" dataKey="Innovation" stroke={COLORS[2]} strokeWidth={strokeWidth} dot={false} hide={this.state.hideChart['Innovation']} />
                                                                            <this.state.Recharts.Line type="monotone" dataKey="Customer-Experience" stroke={COLORS[3]} strokeWidth={strokeWidth} dot={false} hide={this.state.hideChart['Customer Experience']} />
                                                                            <this.state.Recharts.Line type="monotone" dataKey="Advocacy" stroke={COLORS[5]} strokeWidth={strokeWidth} dot={false} hide={this.state.hideChart['Advocacy']} />

                                                                            <this.state.Recharts.Line type="monotone" dataKey="Optimisation" stroke={COLORS[6]} strokeWidth={strokeWidth} dot={false} hide={this.state.hideData['Optimisation']} />
                                                                            <this.state.Recharts.Line type="monotone" dataKey="Clarity" stroke={COLORS[7]} strokeWidth={strokeWidth} dot={false} hide={this.state.hideData['Clarity']} />
                                                                            <this.state.Recharts.Line type="monotone" dataKey="Coordination" stroke={COLORS[8]} strokeWidth={strokeWidth} dot={false} hide={this.state.hideData['Coordination']} />
                                                                            <this.state.Recharts.Line type="monotone" dataKey="Decisiveness" stroke={COLORS[9]} strokeWidth={strokeWidth} dot={false} hide={this.state.hideData['Decisiveness']} />
                                                                            
                                                                            <this.state.Recharts.Line type="monotone" dataKey="Enablement" stroke={COLORS[10]} strokeWidth={strokeWidth} dot={false} hide={this.state.hideData['Enablement']} />        
                                                                            <this.state.Recharts.Line type="monotone" dataKey="Empowerment" stroke={COLORS[11]} strokeWidth={strokeWidth} dot={false} hide={this.state.hideData['Empowerment']} />
                                                                            <this.state.Recharts.Line type="monotone" dataKey="Performance" stroke={COLORS[12]} strokeWidth={strokeWidth} dot={false} hide={this.state.hideData['Performance']} />
                                                                            <this.state.Recharts.Line type="monotone" dataKey="Processes" stroke={COLORS[13]} strokeWidth={strokeWidth} dot={false} hide={this.state.hideData['Processes']} />
                                                                            
                                                                            <this.state.Recharts.Line type="monotone" dataKey="Prioritisation" stroke={COLORS[14]} strokeWidth={strokeWidth} dot={false} hide={this.state.hideData['Prioritisation']} />        
                                                                            <this.state.Recharts.Line type="monotone" dataKey="Inclusiveness" stroke={COLORS[15]} strokeWidth={strokeWidth} dot={false} hide={this.state.hideData['Inclusiveness']} />
                                                                            <this.state.Recharts.Line type="monotone" dataKey="Safety" stroke={COLORS[16]} strokeWidth={strokeWidth} dot={false} hide={this.state.hideData['Safety']} />
                                                                            <this.state.Recharts.Line type="monotone" dataKey="Encouragement" stroke={COLORS[17]} strokeWidth={strokeWidth} dot={false} hide={this.state.hideData['Encouragement']} />
                                                                           
                                                                            <this.state.Recharts.Line type="monotone" dataKey="Team-orientated" stroke={COLORS[18]} strokeWidth={strokeWidth} dot={false} hide={this.state.hideData['Team-orientated']} />        
                                                                            <this.state.Recharts.Line type="monotone" dataKey="Customer-centric" stroke={COLORS[19]} strokeWidth={strokeWidth} dot={false} hide={this.state.hideData['Customer-centric']} />
                                                                            <this.state.Recharts.Line type="monotone" dataKey="Responsive" stroke={COLORS[20]} strokeWidth={strokeWidth} dot={false} hide={this.state.hideData['Responsive']} />
                                                                            <this.state.Recharts.Line type="monotone" dataKey="Collaborative" stroke={COLORS[21]} strokeWidth={strokeWidth} dot={false} hide={this.state.hideData['Collaborative']} />
                                                                           
                                                                            <this.state.Recharts.Line type="monotone" dataKey="Belief" stroke={COLORS[22]} strokeWidth={strokeWidth} dot={false} hide={this.state.hideData['Belief']} />        
                                                                            <this.state.Recharts.Line type="monotone" dataKey="Fulfillment" stroke={COLORS[23]} strokeWidth={strokeWidth} dot={false} hide={this.state.hideData['Fulfillment']} />
                                                                            <this.state.Recharts.Line type="monotone" dataKey="Integrity" stroke={COLORS[24]} strokeWidth={strokeWidth} dot={false} hide={this.state.hideData['Integrity']} />
                                                                            <this.state.Recharts.Line type="monotone" dataKey="Alignment" stroke={COLORS[25]} strokeWidth={strokeWidth} dot={false} hide={this.state.hideData['Alignment']} />
                                                                           
                                                                        </this.state.Recharts.LineChart>
                                                                    </div>
                                                                    : <div>
                                                                        No Value Found
                                                            </div>
                                                        }
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div></div>
                        </div>
            </div>

        )
    }




}

function mapStateToProps(state) {
    return {

    }
}

function mapDispatchToProps(dispatch) {
    return {

    }
}

var connection = connect(mapStateToProps, mapDispatchToProps);
module.exports = connection(CultureDashboard);