import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import PreLoader from '../../PreLoader'
import Header from '../../Header/Header';
import Globals from '../../../Globals';
import { Link } from 'react-router';
import AnalyticsNav from '../AnalyticsNav';
import CultureDashboard from './CultureDashboard'
import Heatmap from './Heatmap'
import * as cultureAnalyticsActions from '../../../actions/analytics/cultureAnalyticsActions';
import * as utils from '../../../utils/utils';
import GuestUserRestrictionPopup from '../../GuestUserRestrictionPopup';

class ValueScoreIndex extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            openHeatmap: false
        }
    }
    componentDidMount(){
        let roleName =JSON.parse(localStorage.getItem('roles')) !== null? JSON.parse(localStorage.getItem('roles'))[0].role_name:''
        if(roleName!=='guest'){
        var timezone = utils.clientTimezone();
        var ObjToSend = {
            'timezone':timezone,
            'timespan':"month"
        }
        this.props.cultureAnalyticsActions.getCultureParticipationData(ObjToSend);
        this.props.cultureAnalyticsActions.getEngagementData(ObjToSend);
        this.props.cultureAnalyticsActions.getAllDriversData(ObjToSend);
        this.props.cultureAnalyticsActions.getsurveyComparisonData(ObjToSend);

        }   
    }
    fetchAllDriversData(values){
        this.props.cultureAnalyticsActions.getAllDriversData(values);
    }

    fetchSurveyComparisonData(values){
        this.props.cultureAnalyticsActions.getsurveyComparisonData(values);
    }


    openHeatmapExpand(){
        this.setState({
            openHeatmap: true   
        })
    }
    closeHeatmapExpand(){
        this.setState({
            openHeatmap: false   
        })   
    }
    renderExpandHeatmap(){
        return(
            <div></div>
        )
    }
    fetchParticipationData(time){
        var timezone = utils.clientTimezone();
        var ObjToSend = {
            'timezone': timezone,
             timespan:time
        }
        this.props.cultureAnalyticsActions.getCultureParticipationData(ObjToSend);
    }

    render(){
      return(
            <div className="Value-Scroll-page">
            <GuestUserRestrictionPopup setOverlay={true}/>
             <section id="analytics-Main-container" className="analytics-wrapper Value-Scroll-Container culture-new-tab">
                <Header
                  title={'Analytics'}
                  location={this.props.location}
                />
                <div className='main-container'>
                    <div id="content">
                        <div className="culture-dashboard-tab-responsive culture-score-tab clearfix">
                            <AnalyticsNav location={this.props.location} />
                        </div>
                        {!this.state.openHeatmap ? 
                            <CultureDashboard
                                openHeatmapExpand={this.openHeatmapExpand.bind(this)}
                                culture ={this.props.culture}
                                fetchAllDriversData={this.fetchAllDriversData.bind(this)}
                                fetchSurveyComparisonData={this.fetchSurveyComparisonData.bind(this)}
                                fetchParticipationData={this.fetchParticipationData.bind(this)}
                            /> 
                        : 
                            <Heatmap
                                closeHeatmapExpand={this.closeHeatmapExpand.bind(this)}
                            />
                        }
                    </div>
                </div>    
            </section>  
           </div>  
            
      )
        
    }
}
function mapStateToProps(state){
    return{
        culture: state.cultureAnalytics,
    }
}
function mapDispatchToProps(dispatch){
    return{
        cultureAnalyticsActions : bindActionCreators(cultureAnalyticsActions,dispatch)
    }
}
var connection = connect(mapStateToProps,mapDispatchToProps);
module.exports = connection(ValueScoreIndex);