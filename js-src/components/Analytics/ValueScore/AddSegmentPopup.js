import CenterPopupWrapper from '../../CenterPopupWrapper';
import ChunkSelect from '../../Chunks/ChunkReactSelect';
import reactDatepickerChunk from '../../Chunks/ChunkDatePicker'
import moment from '../../Chunks/ChunkMoment'
import { Field, reduxForm } from 'redux-form'
import { renderField } from '../../Helpers/ReduxForm/RenderField'
import { connect } from 'react-redux'
import ReactDatePicker from '../../Helpers/ReduxForm/ReactDatePicker'
import * as utils from '../../../utils/utils';
const required = value => (value ? undefined : 'Required')
let optionMain=[];
const optionOperation = [
  { value: 'gt', label: 'Is more than' },
  { value: 'lt', label: 'Is less than' },
  { value: 'IB', label: 'Is inbetween' }
];

const optionOperationDefault = [
    { value: 'gt', label: 'Is more than' }
]

let optionAction = []
let lastIndex = null
var indexArray={}
let handleFormVal = []
handleFormVal[0] = []
handleFormVal[0]['segment'] = ''
handleFormVal[0]['operation'] = 'equal'
handleFormVal[0]['value'] = ''
handleFormVal[0]['showInHeatmap'] = 0
let checkAPICall = false
let selectedMainOptionArray=[]

class AddSegmentPopup extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          selectedMainOption0: null,
          selectedSubOption0: null,
          selectedOperationOption0:null, 
          dynamicField: ['field'],
          numberOfField:1,
          showOperation0: 'equal',
          currentIndex:null,
          ReactDatePicker: null,
          Moment: null,
          selectedArray:[],
          removedIndex:[],
          twoBox0:false,
          editId:null,
          lastIndex: 0,
          currSegmentId:null,
          saveSegmentation: 'All',
          reactSelect :null
        }
    }

  componentWillMount() {
    var me = this
    moment().then(moment => {
      ChunkSelect().then(reactselect => {
        reactDatepickerChunk()
          .then(reactDatepicker => {
            this.setState({
              reactSelect: reactselect,
              ReactDatePicker: reactDatepicker,
              Moment: moment,
            })
          })
      })
        .catch(err => {
          throw err
        })
    })
  }

    componentWillReceiveProps(nextProps) {
       let optionArrayDrop = [];
      
       if(this.props.regionData !== nextProps.regionData && nextProps.regionData.length > 0){
        var storeIndex = indexArray['region']!==undefined?indexArray['region']:this.state.currentIndex;
          nextProps.regionData.map((regionDataVar,index)=>{
              optionArrayDrop.push({value:regionDataVar.identity, label:regionDataVar.name})
          },optionAction[storeIndex] = optionArrayDrop)
          checkAPICall = true
       }
       if(this.props.countryData !== nextProps.countryData && nextProps.countryData.length > 0){
        var storeIndex = indexArray['country']!==undefined?indexArray['country']:this.state.currentIndex;
          nextProps.countryData.map((countryDataVar,index)=>{
              optionArrayDrop.push({value:countryDataVar.identity, label:countryDataVar.title})
          },optionAction[storeIndex] = optionArrayDrop)
          checkAPICall = true
       }
       if(this.props.officeData !== nextProps.officeData && nextProps.officeData.length > 0){
        var storeIndex = indexArray['office']!==undefined?indexArray['office']:this.state.currentIndex;
          nextProps.officeData.map((officeDataVar,index)=>{
              optionArrayDrop.push({value:officeDataVar.identity[0], label:officeDataVar.name})
          },optionAction[storeIndex] = optionArrayDrop)
          checkAPICall = true
       }
       if(this.props.departmentData !== nextProps.departmentData && nextProps.departmentData.length > 0){
        var storeIndex = indexArray['department']!==undefined?indexArray['department']:this.state.currentIndex;
          nextProps.departmentData.map((departmentDataVar,index)=>{
              optionArrayDrop.push({value:departmentDataVar.identity[0], label:departmentDataVar.name})
          },optionAction[storeIndex] = optionArrayDrop)
          checkAPICall = true
       }
       if(this.props.businessData !== nextProps.businessData && nextProps.businessData.length > 0){
        var storeIndex = indexArray['business']!==undefined?indexArray['business']:this.state.currentIndex;
          nextProps.businessData.map((businessDataVar,index)=>{
              optionArrayDrop.push({value:businessDataVar.identity[0], label:businessDataVar.name})
          },optionAction[storeIndex] = optionArrayDrop)
          checkAPICall = true
       }
       if(this.props.companyRoleData !== nextProps.companyRoleData && nextProps.companyRoleData.length > 0){
        var storeIndex = indexArray['company_role']!==undefined?indexArray['company_role']:this.state.currentIndex;
          nextProps.companyRoleData.map((roleDataVar,index)=>{
              optionArrayDrop.push({value:roleDataVar.identity[0], label:roleDataVar.name})
          },optionAction[storeIndex] = optionArrayDrop)
          checkAPICall = true
       }
       if(this.props.levelData !== nextProps.levelData && Object.keys(nextProps.levelData).length > 0){
        var storeIndex = indexArray['level']!==undefined?indexArray['level']:this.state.currentIndex;
          optionAction[storeIndex] = nextProps.levelData.data
          checkAPICall = true
       }
       if(this.props.genderData !== nextProps.genderData && Object.keys(nextProps.genderData).length > 0){
        var storeIndex = indexArray['gender']!==undefined?indexArray['gender']:this.state.currentIndex;
          optionAction[storeIndex] = nextProps.genderData.data
          checkAPICall = true
       }
       if(this.props.visiblyRoleData !== nextProps.visiblyRoleData && Object.keys(nextProps.visiblyRoleData).length > 0){
        var storeIndex = indexArray['visibly_role']!==undefined?indexArray['visibly_role']:this.state.currentIndex;
          nextProps.visiblyRoleData.map((visiblyRoleVar,index)=>{
              optionArrayDrop.push({value:visiblyRoleVar.role_id, label:visiblyRoleVar.role_name})
          },optionAction[storeIndex] = optionArrayDrop)
          checkAPICall = true
       }
       /*render predefined selected segment*/
       //console.log(nextProps.perticularSegment.segment,this.props.perticularSegment,checkAPICall)
       if(nextProps.perticularSegment !== this.props.perticularSegment && Object.keys(nextProps.perticularSegment).length > 0 && !checkAPICall){
          checkAPICall = true
          let preDynamicField = ['field']
          let preNoOfField = 0
          nextProps.perticularSegment.segment[0].filter.map((preSelectedSegment,index)=>{
            //console.log(preSelectedSegment,'preSelectedSegment',Object.keys(preSelectedSegment)[0])
            let key = Object.keys(preSelectedSegment)[0]
            handleFormVal[index] = []
            /*handleFormVal[index]['segment'] = key
            handleFormVal[index]['operation'] = preSelectedSegment[key]['operation']
            handleFormVal[index]['value'] = preSelectedSegment[key]['value']
            handleFormVal[index]['showInHeatmap'] = preSelectedSegment[key]['selected']*/
            let MultiArray = []
            if(preSelectedSegment[key]['operation'] == 'IB'){
              this.handleOperationChangeBox(index,{value:preSelectedSegment[key]['operation'], label: 'Is inbetween'});
            }else if(preSelectedSegment[key]['operation'] == 'lt'){
              this.handleOperationChangeBox(index,{value:preSelectedSegment[key]['operation'], label: 'Is less than'});
            }else if(preSelectedSegment[key]['operation'] == 'gt'){
              this.handleOperationChangeBox(index,{value:preSelectedSegment[key]['operation'], label: 'Is more than'});
            }
            //select checkbox auto
            if(preSelectedSegment[key]['selected'] == true){
              this.saveSegmentCheckboxClick(index,'direct');  
            }

             // to set the key of particular segment to place data
             indexArray[key]=index;
            

            if(preSelectedSegment[key]['operation'] !== 'IB'){
              for(let j=0; j<preSelectedSegment[key]['value'].length; j++){
                let labelHold = typeof preSelectedSegment[key]['name'] !== 'undefined' ? utils.capitalizeFirstLetter(preSelectedSegment[key]['name'][j]) : preSelectedSegment[key]['value'][j]
                MultiArray.push({value:preSelectedSegment[key]['value'][j], label: labelHold})  
              }  
              this.handleChange(index,{value:key, label: utils.capitalizeFirstLetter(key)},this.handleSubChange.bind(this,index,MultiArray),true)
              
            }else{
               let fromToVar = {from: preSelectedSegment[key]['value'][0], to: preSelectedSegment[key]['value'][1]}
               MultiArray.push({value:fromToVar, label: fromToVar})  
               this.handleChange(index,{value:key, label: utils.capitalizeFirstLetter(key)},false,true)
               handleFormVal[index]['value']['from'] = preSelectedSegment[key]['value'][0]
               handleFormVal[index]['value']['to'] = preSelectedSegment[key]['value'][1]
            }            
            this.setState({editId: nextProps.perticularSegment.segment[0].id,selectedStatus : nextProps.perticularSegment.segment[0].status},()=>{this.handleInitialize()})
            
            
            
            
            //this.handleSubChange(index,MultiArray)
            /*preSelectedSegment[key]['values'].map((preSelectedValue,j){
                MultiArray.push()
            })*/
            //this.handleSubChange(index,selectedOption)
           /* this.setState({['selectedMainOption'+index]:{value:key, label: utils.capitalizeFirstLetter(key)}})
            this.setState({['selectedMainOption'+index]:{value:key, label: preSelectedSegment[key]['values']}})*/
            if(index > 0){
                preDynamicField.push('field')
            }
            preNoOfField++
          })

          this.setState({dynamicField:preDynamicField,numberOfField:preNoOfField,lastIndex:preNoOfField-1})
       }
       
    }

   

    

    componentDidMount (){
        handleFormVal = []
        handleFormVal[0] = []
        indexArray={}
        handleFormVal[0]['segment'] = ''
        handleFormVal[0]['operation'] = 'equal'
        handleFormVal[0]['value'] = '' 
        handleFormVal[0]['showInHeatmap'] = 0
        const {optionMainConst} =  this.props;
        optionMain = [...optionMainConst.map(option=>{return {...option}})]
        selectedMainOptionArray = []
        this.setState({
          selectedStatus : 'All'
        })
        // added for initializing check box for field
        
        this.handleInitialize()
    }

    handleInitialize () {
        let initData = {}
        initData['noOfSegment'] = this.state.numberOfField
        if(this.state.editId !== null){
          initData['edit_identity'] = this.state.editId
        }
        initData['selectedStatus']=this.state.selectedStatus;
        if(this.state.selectedStatus=="None"){
          document.getElementById("matchStatus").value= "None";
        }else{
          document.getElementById("matchStatus").value = "All";
        }
       
        let totalField = handleFormVal.length;
        handleFormVal.map((handleFormVar,index)=>{
          initData['keyName'+index] = handleFormVar.segment
          initData['operateName'+index] = handleFormVar.operation
          initData['valueName'+index] = handleFormVar.value
          initData['saveAsSegment'+index] = this.state.numberOfField == (index+1) ? true : handleFormVar.showInHeatmap
          if(handleFormVar.segment == 'age'){
              if(handleFormVar.operation == 'IB'){
                  initData['ageFrom'+index] = handleFormVar.value.from
                  initData['ageTo'+index] = handleFormVar.value.to
              }else{
                  initData['age'+index] = handleFormVar.value  
              }
          }
          if(handleFormVar.segment == 'start_date'){
              if(handleFormVar.operation == 'IB'){
                  initData['start_date_from'+index] = handleFormVar.value.from
                  initData['start_date_to'+index] = handleFormVar.value.to
                  initData['valueName'+index] = [{'from':Date.now(),'to':Date.now()}]
                  if(typeof handleFormVar.value.from !== 'undefined'){
                    initData['valueName'+index][0]['from'] = handleFormVar.value.from
                  }
                  if(typeof handleFormVar.value.to !== 'undefined'){
                    initData['valueName'+index][0]['to'] = handleFormVar.value.to
                  }
              }else{
                  initData['start_date'+index] = handleFormVar.value
                  initData['valueName'+index] = handleFormVar.value !== '' && handleFormVar.value.length !== 0 ? handleFormVar.value : Date.now()
              }
              
          }
        })
        this.props.initialize(initData)
    }


    handleChange(index,selectedOption,callback,calledFromProps = false){
      let obj = this.state.selectedArray.filter((item,i)=>{
        return i !== index;
      })
      optionAction[index] = []
      //if no callback function passed
      if(selectedOption.value == 'age' || selectedOption.value == 'start_date'){

        this.setState({['selectedMainOption'+index]:selectedOption, ['showOperation'+index]: selectedOption.value, currentIndex: index,selectedArray : obj}, 
                      () => {
                              this.props.callFetchFieldData(selectedOption.value,this.state.selectedArray)
                              typeof callback === "function" ? callback() : ''
                            }
                    );  
        if(calledFromProps == false){
          handleFormVal[index]['value'] = []
          handleFormVal[index]['operation'] = 'gt'
        }
      }else{

        this.setState({['selectedMainOption'+index]:selectedOption, ['showOperation'+index]: 'equal', currentIndex: index, ['selectedSubOption'+index]:null,selectedArray : obj}, 
                      () => {
                              this.props.callFetchFieldData(selectedOption.value,this.state.selectedArray);
                              typeof callback === "function" ? callback() : ''
                            }
                    );
       if(calledFromProps == false){ 
          handleFormVal[index]['value'] = []
       }
       handleFormVal[index]['operation'] = 'equal'
      }

      handleFormVal[index]['segment'] = selectedOption.value
      selectedMainOptionArray[index] = selectedOption.value
      this.removeAddOptionMainBox();
      this.checkParents(selectedOption.value);
      this.handleInitialize()
      
    }
    checkParents(segmentName){
      switch(segmentName){
        case "country":
          let checkArrayCountry = ['region']
          handleFormVal.map((selectedArray,index)=>{
            checkArrayCountry.map((checkArrayInit,j)=>{
                if(selectedArray.segment !== checkArrayInit){

                    optionMain = optionMain.map((renderOption,j)=>{

                      if(renderOption.value == checkArrayInit){
                           return {...renderOption, isDisabled:true}
                      }else{
                        return {...renderOption}
                      }
                    })
                }
            })
          })
        break;
        case "office":
          let checkArrayOffice = ['region','country']
          handleFormVal.map((selectedArray,index)=>{
            checkArrayOffice.map((checkArrayInit,j)=>{
                if(selectedArray.segment !== checkArrayInit){

                  optionMain = optionMain.map((renderOption,j)=>{

                    if(renderOption.value == checkArrayInit){
                         return {...renderOption, isDisabled:true}
                    }else{
                      return {...renderOption}
                    }
                  })
              }
            })
          })
        break;
        case "department":
          let checkArrayDepartment = ['region','country','office']
          handleFormVal.map((selectedArray,index)=>{
            checkArrayDepartment.map((checkArrayInit,j)=>{
                if(selectedArray.segment !== checkArrayInit){

                  optionMain = optionMain.map((renderOption,j)=>{

                    if(renderOption.value == checkArrayInit){
                         return {...renderOption, isDisabled:true}
                    }else{
                      return {...renderOption}
                    }
                  })
              }
            })
          })
        break;
        case "business":
          let checkArrayBusiness = ['region','country','office','department']
          handleFormVal.map((selectedArray,index)=>{
            checkArrayBusiness.map((checkArrayInit,j)=>{
                if(selectedArray.segment !== checkArrayInit){

                  optionMain = optionMain.map((renderOption,j)=>{

                    if(renderOption.value == checkArrayInit){
                         return {...renderOption, isDisabled:true}
                    }else{
                      return {...renderOption}
                    }
                  })
              }
            })
          })
        case "company_role":
          let checkArrayRole = ['region','country','office','department','business']
          handleFormVal.map((selectedArray,index)=>{
            checkArrayRole.map((checkArrayInit,j)=>{
                if(selectedArray.segment !== checkArrayInit){

                  optionMain = optionMain.map((renderOption,j)=>{

                    if(renderOption.value == checkArrayInit){
                         return {...renderOption, isDisabled:true}
                    }else{
                      return {...renderOption}
                    }
                  })
              }
            })
          })
        break;
      }
      
    }
    removeAddOptionMainBox(){
      let newMainArray = [];
      var optionMain1 = optionMain.filter(function (el) {
        return selectedMainOptionArray.indexOf(el.value) === -1
      });
      optionMain = optionMain1;
     
    }
    handleSubChange(index,selectedOption){

      let allSelectedArray = this.state.selectedArray;
      let newArray= [];
      let inArray = false
      const checkArray = ['region','country','office','department','business','company_role']
      if(checkArray.indexOf(this.state['selectedMainOption'+index].value) !== -1){
          allSelectedArray.map((allData,i)=>{

            if(allData.selected == this.state['selectedMainOption'+index].value){
               allSelectedArray[i]['value'] = selectedOption
               inArray = true
            }
          })
          if(!inArray){
               allSelectedArray.push({selected: this.state['selectedMainOption'+index].value, value: selectedOption})
          }
          this.setState({selectedArray: allSelectedArray, ['selectedSubOption'+index]: selectedOption})
      }else{
          this.setState({['selectedSubOption'+index]: selectedOption})
      }
      let newSelectedValue = []
      selectedOption.map((selectOptionLoop)=>{
          newSelectedValue.push(selectOptionLoop.value)
      })
      handleFormVal[index]['value'] = newSelectedValue
      this.handleInitialize();
    }
    addMoreSegment(lastIndex){
      let newFieldCounter = this.state.dynamicField
      newFieldCounter.push('field')
      let numberOfField = this.state.numberOfField + 1
      let currentIndex = this.state.lastIndex + 1
      handleFormVal[currentIndex] = [];
      handleFormVal[currentIndex]['segment'] = ''
      handleFormVal[currentIndex]['operation'] = 'equal'
      handleFormVal[currentIndex]['value'] = ''
      handleFormVal[currentIndex]['showInHeatmap'] = 0
      this.setState({ numberOfField: numberOfField, dynamicField: newFieldCounter, lastIndex: currentIndex},()=>{this.handleInitialize()})
      //this.handleInitialize()
    }

   handleSubmitButton(){
      this.setState({removedIndex: []})
   }  

   handleOperationChangeBox(index,selectedOption){
      handleFormVal[index]['operation'] = selectedOption.value
      if(selectedOption.value == 'IB'){
        this.setState({['twoBox'+index]: true})
        handleFormVal[index]['value'] = []
      }else{
        handleFormVal[index]['value'] = ''
        this.setState({['twoBox'+index]: false})
      }
      this.setState({['selectedOperationOption'+index]: selectedOption})
      this.handleInitialize()
      this.forceUpdate()
   }
   handleSubChangeText(index,me){
       handleFormVal[index]['value'] = me.target.value
       this.handleInitialize()
       this.forceUpdate()
   }

   handleSubChangeFromToText(index,type,me){
       handleFormVal[index]['value'][type] = typeof me.target.value !== 'undefined' ? me.target.value : me
       this.handleInitialize()
       this.forceUpdate()
   }

   removeElement(index){
    //let numberOfField = this.state.numberOfField - 1
    let removeIndexVar = this.state.removedIndex
    removeIndexVar.push(index)
    let numberOfField = this.state.numberOfField - 1
    let currentIndex = this.state.lastIndex - 1

    this.setState({ removedIndex: removeIndexVar, numberOfField: numberOfField, lastIndex: currentIndex})
    //document.getElementById("aging"+index).disabled = true;
    // document.getElementById("newSegmentMainBox"+index).remove();
    this.props.change("keyName"+index,'')
    this.props.change("operateName"+index,'')
    this.props.change("valueName"+index,'')
  
    var tempValueArr = [];
    handleFormVal.map((value,i)=>{
      if(i!==index){
        tempValueArr.push(value);
      }
    }
    )
    for(var shiftIndex =index;shiftIndex<=lastIndex;shiftIndex++){
      if(shiftIndex!==lastIndex){
        this.setState({
          ['selectedMainOption'+shiftIndex]:this.state['selectedMainOption'+(shiftIndex+1)],
          ['selectedSubOption'+shiftIndex]:this.state['selectedSubOption'+(shiftIndex+1)],
          ['selectedOperationOption'+shiftIndex]:this.state['selectedOperationOption'+(shiftIndex+1)],
          ['showOperation'+shiftIndex]:this.state['showOperation'+(shiftIndex+1)],
          ['twoBox'+shiftIndex]:this.state['twoBox'+(shiftIndex+1)]
        })
      }else{
        this.setState({
          ['selectedMainOption'+shiftIndex]:undefined,
          ['selectedSubOption'+shiftIndex]:undefined,
          ['showOperation'+shiftIndex]:undefined,
          ['twoBox'+shiftIndex]:undefined
        })
      }
    }

    let newFieldCounter = this.state.dynamicField
      newFieldCounter.pop('field');
      handleFormVal = tempValueArr;
      lastIndex = lastIndex - 1
      this.setState({
        dynamicField : newFieldCounter,
        numberOfField : this.state.numberOfField-1 
      },()=>{
        this.handleInitialize()
      })
   }  
   updateDateValueFromTo(type = null,value,index){
      handleFormVal[index]['value'][type] = value  
      this.handleInitialize()
   }
   updateDateValue(value,index){
      handleFormVal[index]['value'] = value  
      this.handleInitialize()
   }
   saveSegmentCheckboxClick(index,me){
    if(me == 'direct'){
        handleFormVal[index]['showInHeatmap'] = 1
    }else{
      if(me.target.value == 0){
        handleFormVal[index]['showInHeatmap'] = 1
      }else{
        handleFormVal[index]['showInHeatmap'] = 0
      }  
    }
    
    this.handleInitialize();
    
   }

   showSegmentDetail(segmentId){
     this.setState({
       currSegmentId: segmentId
     })
      checkAPICall = false
      handleFormVal.map((allVal,index)=>{
            this.setState({['selectedMainOption'+index]:null,
            ['selectedSubOption'+index]:null,
            ['selectedOperationOption'+index]:null,
            ['showOperation'+index]:'equal'
            ,['twoBox'+index]:null
          })
      })

      this.setState({dynamicField:['field'],selectedArray:[],removedIndex:[],currentIndex:null,numberOfField:1})
      handleFormVal = []
      handleFormVal[0] = []
      handleFormVal[0]['segment'] = ''
      handleFormVal[0]['operation'] = 'equal'
      handleFormVal[0]['value'] = '' 
      handleFormVal[0]['showInHeatmap'] = 0
      const {optionMainConst} =  this.props;
      optionMain = [...optionMainConst.map(option=>{return {...option}})]
      selectedMainOptionArray = [];
      this.props.showPerticularSegment(segmentId)
   }

   refreshSegment(){
    indexArray={};
     selectedMainOptionArray = [];
     optionAction=[];
     var optionMain1 = this.props.optionMainConst.filter(function (el) {
      return selectedMainOptionArray.indexOf(el.value) === -1
    });
    optionMain = optionMain1;
     
     handleFormVal.map((allVal,index)=>{
          this.setState({
            ['selectedMainOption'+index]:null,
            ['selectedSubOption'+index]:null,
            ['selectedOperationOption'+index]:null,
            ['showOperation'+index]:'equal',
            ['twoBox'+index]:null,
            ['keyName'+index]:null,
            ['operateName'+index]:null,
            ['valueName'+index]:null,
            ['saveAsSegment'+index]:null
          },()=>{
            handleFormVal = []
            handleFormVal[0] = []
            handleFormVal[0]['segment'] = ''
            handleFormVal[0]['operation'] = 'equal'
            handleFormVal[0]['value'] = '' 
            handleFormVal[0]['showInHeatmap'] = 0
          })
          this.props.change('keyName'+index ,'');
          this.props.change('operateName'+index ,'');
          this.props.change('saveAsSegment'+index ,'');
          this.props.change('valueName'+index ,'');
          this.props.change('noOfSegment' ,1);
    })
     this.setState({dynamicField:['field'],selectedArray:[],removedIndex:[],currentIndex:null,numberOfField:1,lastIndex:0})
     lastIndex=0
   }


    
   renderNewSegmentsFormNew(fetchedProps){
        const { handleSubmit, pristine, submitting } = fetchedProps
        // isDisabled is set to restrict submit button
        var isDisabled = false;
        for( var index = 0; index<this.state.dynamicField.length; index++){
          if(isDisabled!==true){
            isDisabled=true;
            if(this.state['showOperation'+index]=="start_date" && this.state['selectedOperationOption'+index]){
              isDisabled = false;
            }else{
              if(this.state['selectedMainOption'+index] && this.state['selectedSubOption'+index] && this.state['selectedSubOption'+index].length>0){
                isDisabled = false;
              }
            }
          }
        }
        return (  
                <div className="MoreSegmentsWrapper">
                  <div className="addSegementSection">
                      <div className="selectAllSegmentCheckboxWrapper">
                        {/* <div className='form-row checkbox'>
                          <input
                            className='checknotify'
                            type='checkbox'
                            id="selectAllNewSegments"
                          />

                          <label for="selectAllNewSegments"></label>
                          <span className="removeAllSegments"><i class="material-icons">remove_circle_outline</i></span>
                        </div> */}
                          <div className="refreshsegmentButton clearfix" >
                              <a title="Refresh" className="refreshSegmentBtn" onClick={this.refreshSegment.bind(this)}><i class="material-icons">refresh</i> Reset</a>
                          </div> 
                        <div>

                        </div>
                      </div>
                        <div className="addSegementSectionInner">
                          <div>
                            <form name='addSegmentForm' onSubmit={handleSubmit}>
                              <Field
                                  name={'noOfSegment'}
                                  component={renderField}
                                  type='hidden'
                              />
                              <Field
                                  name={'edit_identity'}
                                  component={renderField}
                                  type='hidden'
                              />
                              <Field
                                  name={'selectedStatus'}
                                  component={renderField}
                                  type='hidden'
                              />
                              
                              <table className="table responsive-table">
                              {this.state.dynamicField.map((dynamicField,index)=>{
                                lastIndex = index
                              let optionState = this.state['selectedMainOption'+index] 
                              let valueState = this.state['selectedSubOption'+index] 
                              let operationState = this.state['selectedOperationOption'+index]
                              
                              return(

                                  
                                    <tr id={'newSegmentMainBox'+index} key={index} className={this.state['showOperation'+index] !== 'equal' &&  typeof this.state['showOperation'+index] !== 'undefined' ? "newSegmentColumnWrapper cleafix numeric-segment" : "newSegmentColumnWrapper cleafix"} >
                                    <td className="newSegmentColumn" width='5%'>
                                      <div className="campaignSelectSegmentCheckbox">
                                        <div className='form-row checkbox'>
                                          {/*<input
                                            className='checknotify'
                                            type='checkbox'
                                          />*/}
                                          <Field
                                            name={'saveAsSegment'+index}
                                            component={renderField}
                                            type='checkbox'
                                            onChange={this.saveSegmentCheckboxClick.bind(this,index)}
                                            disabled={this.state.dynamicField.length == (index+1) ? true : false}
                                          />
                                          
                                        </div>
                                      </div>
                                      </td>
                                      <td className="newSegmentColumn firstSegmentDropdown" width='30%'>
                                        <div className="newSegmentColumnInner">
                                          <div className="newSegmentText">If</div>
                                            <div class="select-wrapper multi-select-wrapper">
                                              {this.state.reactSelect !== null ?
                                                <this.state.reactSelect.default
                                                value={optionState}
                                                onChange={this.handleChange.bind(this,index)}
                                                options={optionMain}
                                              />
                                              :''} 
                                              <Field
                                                name={'keyName'+index}
                                                component={renderField}
                                                type='hidden'
                                              />
                                            </div>
                                        </div>
                                      </td>
                                      <td className="newSegmentColumn" width='30%'>
                                        <div className="newSegmentText isEqualToText">is equal to</div>
                                        <div className="numericDropdown">
                                          <div class="select-wrapper multi-select-wrapper">
                                            {this.state.reactSelect !== null ?
                                            <this.state.reactSelect.default
                                              value={operationState}
                                              options={optionOperation}
                                              onChange={this.handleOperationChangeBox.bind(this,index)}
                                              defaultValue={optionOperationDefault}
                                            />:''}
                                            <Field
                                                name={'operateName'+index}
                                                component={renderField}
                                                type='hidden'
                                            />
                                          </div>
                                        </div>
                                      </td>
                                      {this.state['showOperation'+index] !== 'equal' &&  typeof this.state['showOperation'+index] !== 'undefined'? 
                                        <td className="newSegmentColumn valueColumn" width='30%'>
                                            <div>
                                              {this.state['showOperation'+index] == 'age' ?
                                                <div>
                                                  {this.state['twoBox'+index] ?
                                                    <div className="newSegmentColumnInner two-fields">
                                                      <Field
                                                        name={'ageFrom'+index}
                                                        placeholder='From'
                                                        component={renderField}
                                                        type='text'
                                                        onChange={this.handleSubChangeFromToText.bind(this,index,'from')}
                                                      />
                                                      <Field
                                                        name={'ageTo'+index}
                                                        placeholder='To'
                                                        component={renderField}
                                                        type='text'
                                                        onChange={this.handleSubChangeFromToText.bind(this,index,'to')}
                                                      />
                                                      <Field
                                                          name={'valueName'+index}
                                                          component={renderField}
                                                          type='hidden'
                                                      />
                                                    </div>  
                                                  :
                                                    <div>
                                                      <Field
                                                        name={'age'+index}
                                                        placeholder='Age'
                                                        component={renderField}
                                                        type='text'
                                                        onChange={this.handleSubChangeText.bind(this,index)}
                                                      />
                                                      <Field
                                                          name={'valueName'+index}
                                                          component={renderField}
                                                          type='hidden'
                                                      /> 
                                                    </div> 
                                                  }
                                                </div> 
                                              :
                                                <div>
                                                  {this.state['twoBox'+index] ?
                                                      <div className="newSegmentColumnInner two-fields">
                                                        <Field
                                                          name={'start_date_from'+index}
                                                          placeholder='From'
                                                          component={props => {
                                                            return (
                                                              <ReactDatePicker
                                                                {...props}
                                                                reactDatePicker={this.state.ReactDatePicker}
                                                                moment={this.state.Moment}
                                                                dateFormat="DD/MM/YYYY"
                                                                indexVal={index}
                                                                updateDateValue={this.updateDateValueFromTo.bind(this,'from')}
                                                              />
                                                            )
                                                          }}
                                                        />
                                                        <Field
                                                          name={'start_date_to'+index}
                                                          placeholder='To'
                                                          component={props => {
                                                            return (
                                                              <ReactDatePicker
                                                                {...props}
                                                                reactDatePicker={this.state.ReactDatePicker}
                                                                moment={this.state.Moment}
                                                                dateFormat="DD/MM/YYYY"
                                                                indexVal={index}
                                                                updateDateValue={this.updateDateValueFromTo.bind(this,'to')}
                                                              />
                                                            )
                                                          }}
                                                        />
                                                     </div> 
                                                  :
                                                      <Field
                                                        name={'start_date'+index}
                                                        placeholder='Start date'
                                                        component={props => {
                                                          return (
                                                            <ReactDatePicker
                                                              {...props}
                                                              reactDatePicker={this.state.ReactDatePicker}
                                                              moment={this.state.Moment}
                                                              dateFormat="DD/MM/YYYY"
                                                              indexVal={index}
                                                              updateDateValue={this.updateDateValue.bind(this)}
                                                            />
                                                          )
                                                        }}
                                                      />
                                                  }
                                                  <Field
                                                    name={'valueName'+index}
                                                    className='hid'
                                                    component={renderField}
                                                    type='hidden'
                                                  />
                                                </div>
                                              }
                                            </div>
                                        </td>
                                      :  
                                        <td className="newSegmentColumn valueColumn" width='30%'>
                                          <div className="newSegmentColumnInner">
                                            <div class="select-wrapper multi-select-wrapper">
                                              {this.state.reactSelect !== null ?
                                                <this.state.reactSelect.default
                                                value={valueState}
                                                options={optionAction[index]}
                                                isMulti={true}
                                                onChange={this.handleSubChange.bind(this,index)}
                                              />:''} 
                                              <Field
                                                name={'valueName'+index}
                                                component={renderField}
                                                type='hidden'
                                              />
                                            </div>
                                          </div>
                                        </td>
                                      }
                                      
                                        <td className="action-col" width='5%'>
                                        {
                                          
                                        // this.state.dynamicField.length !== (index+1)
                                        this.state.dynamicField.length !==1
                                         ? 
                                          <div className="addRemoveSegmentData clearfix" onClick={this.removeElement.bind(this,index)}>
                                            <a title="Remove" className="removeSegmentBtn"><i className="material-icons">clear</i></a>
                                          </div> 
                                          :'' 
                                        }   
                                          </td>
                                        
                                    </tr>  
                                         
                              )})}
                              </table>
                              <div className="segmentSaveButtonWrapper">
                                <button className="btn save-btn theme-btn" type="submit" disabled ={isDisabled} onClick={this.handleSubmitButton.bind(this)}>Save</button>
                                {/* onClick={this.props.closeCreateSegmentPopup.bind(this)} */}
                                <button  className="btn cancel-btn default-btn" onClick={this.props.closeAddSegmentPopup.bind(this)}>Cancel</button>
                              </div> 
                            </form>              
                          </div>      
                        </div>
                  </div>    
                </div>            

        )
   }
   selectedMatchStatus(e){
    this.props.change('selectedStatus',e.target.value)
      this.setState({
        selectedStatus : e.target.value
      })
   }

    render(){
        return(
            <CenterPopupWrapper>
                <header class="heading">
                      <h3>Segments</h3>
                      <button id="close-popup" className="btn-default" onClick={this.props.closeAddSegmentPopup.bind(this)}><i className="material-icons" >clear</i></button>
                 </header>
                <div>
                <div className="createSegmentSection">
            <div className="SegmentsSection">
            <div className="clearfix segment-list-wrapper">
              {typeof this.props.segmentData !== 'undefined' && typeof this.props.segmentData.segment !== 'undefined' && this.props.segmentData.segment.length > 0 ? 
                  this.props.segmentData.segment.map((segment,index)=>{
                    var selectedStatus = ''
                      let key = Object.keys(segment.name)[0]
                      Object.keys(segment.filters).length>0?
                      Object.keys(segment.filters).map((filterData,index)=>{
                        if(key==filterData){
                          var data = segment.filters[key];
                          if(segment.selectedStatus=="None"){
                            selectedStatus = '!='
                          }
                          if(data.operation == 'lt'){
                            if(segment.selectedStatus=="None"){
                            selectedStatus = '!<'
                            }else{
                              selectedStatus = '<'
                            }
                         }else if(data.operation == 'gt'){
                          if(segment.selectedStatus=="None"){
                            selectedStatus = '!>'
                            }else{
                              selectedStatus = '>'
                            }
                         }else if(data.operation == 'IB'){
                           selectedStatus ='!='
                         }
                        }
                      })
                      :''
                      let name = ''
                      let keyName = ''
                      segment['name'][key].map((segmentName,index)=>{
                        keyName = key
                        if(key == 'start_date'){
                          segmentName = this.state.Moment !== null ? this.state.Moment.unix(segmentName).format('DD-MM-YYYY') : segmentName
                          keyName = 'Start Date'
                        }
                        if(name != ''){
                            name = name + ' - ' + segmentName
                        }else{
                            name = segmentName
                        }
                        
                      })
                     // name = keyName + ': ' + name //active class
                      return(<div key={index} className={`heatMap segment-list ${this.state.currSegmentId==segment.id?'active':''}`}>
                        <span className="segmentData" onClick={this.showSegmentDetail.bind(this,segment.id)}>{keyName} {selectedStatus} {" : "} <span className="segmentName">{name}</span></span>
                        <span className="segmentDataRemove" onClick={this.props.removeSegmentation.bind(this,segment.id)}><i class="material-icons">clear</i></span>
                        </div>)
                  })
              :''}
            </div>
            <div className="newSegmentsTitle">New segments</div>
                <div className="defaultSegmentField">
                    <div className="newSegmentText">User match</div>
                    <div className="defaultSegmentDropdown">
                        <div class="select-wrapper">
                            <select id ="matchStatus" onChange={this.selectedMatchStatus.bind(this)} >
                            <option value="All">All</option>
                            <option value="None">None</option>
                            </select>
                        </div>
                    </div>
                    <div className="newSegmentText">of the following conditions:</div>
                </div>
                  
                {this.renderNewSegmentsFormNew(this.props)}
                <div className="addMoreSegmentsWrapper">
                    <button className="addSegmentButton" onClick={this.addMoreSegment.bind(this,lastIndex)}><span className="addMoreButtonIcon"><i className="material-icons">add</i></span> Add segment</button>
                    
                </div>
            </div>
           
             </div>
                {/*this.createNewSegment()*/}
                </div>
            </CenterPopupWrapper>    
          )
    }

    
}

function mapStateToProps (state, ownProps) {
  return {
  }
}

function mapDispatchToProps (dispatch) {
  return {
  }
}
let connection = connect(mapStateToProps, mapDispatchToProps)

var reduxConnectedComponent = connection(AddSegmentPopup)

export default reduxForm({
  form: 'addSegmentForm' // a unique identifier for this form
})(reduxConnectedComponent)