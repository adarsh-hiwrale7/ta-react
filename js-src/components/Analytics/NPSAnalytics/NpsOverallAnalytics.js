import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import recharts from '../../Chunks/ChunkRecharts';
import * as utils from '../../../utils/utils';
import * as NPSAnalyticsActions from '../../../actions/analytics/NpsAnalyticsActions';
import reactTooltip from '../../Chunks/ChunkReactTooltip';
import NPSTopFunnel from './NPSTopFunnel'
const COLORS = ['#4bcf99', '#ff595e', '#4b74e0', '#ffbf00', '#b87d7a', '#FF8042','#495867','#7FB069','#B8D8D8','#A93F55','#577399','#F9ADA0',"#ACF39D",'#19323C'];
class NpsOverallAnalytics extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
            currentDate :null,
            Recharts: null,
            expand: false,
            tooltip: null,
            refreshChart: false,
            period: 'month',
            isFilterClicked: false,
            filterData: {},
            funnelId:null,
            funnelType:null,
        }
    }
    
    componentWillMount() {
        reactTooltip().then(reactTooltip => {
            recharts().then(recharts => {
                this.setState({ tooltip: reactTooltip, Recharts: recharts })
            })
        })
    }

    componentDidMount(){
       var  currentDate = utils.currentDate();
       this.setState({
        currentDate : currentDate.toLocaleDateString("en-us", { month: "short" })+' '+utils.getOrdinalNum(currentDate.getDate())
       })
       var timezone = utils.clientTimezone();
       var ObjToSend = {
        'timezone': timezone,
        'timespan': "month"
    }

       this.props.NPSAnalyticsActions.getOverAllNPS(ObjToSend);
       this.props.NPSAnalyticsActions.getFunnelListAnalytics(this.state.period);
       this.props.NPSAnalyticsActions.getNPSTopFunnel();  
    }

    fetchFunnelListData(values) {
        this.setState({
            period:values
        })
        this.props.NPSAnalyticsActions.getFunnelListAnalytics(values);
    }

    selectedTimeSpan(newprops = null, time) {
        var timezone = utils.clientTimezone();
        var ObjToSend = {
            timezone: timezone,
            timespan: time
        }
        this.props.fetchFunnelListData(dataToPass)
    }
    render() {
        return (
                <div className="culture-participation-wrapper nps-analytics-main-wrapper clearfix">
                    <div className="culture-score-main-wrapper">
                        <div className="culture-score">
                            <div className="culture-score-header clearfix">
                                <div className="culture-score-wrapper">
                                    <h4>NPS<span className="svg-icon help-element"><svg xmlns="http://www.w3.org/2000/svg" data-tip data-for='npstitle' width="24" height="24" viewBox="0 0 24 24"><path d="M0 0h24v24H0z" fill="none" /><path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm1 17h-2v-2h2v2zm2.07-7.75l-.9.92C13.45 12.9 13 13.5 13 15h-2v-.5c0-1.1.45-2.1 1.17-2.83l1.24-1.26c.37-.36.59-.86.59-1.41 0-1.1-.9-2-2-2s-2 .9-2 2H8c0-2.21 1.79-4 4-4s4 1.79 4 4c0 .88-.36 1.68-.93 2.25z" /></svg></span></h4>
                                   {this.props.tooltip!=null ?
                                    <this.props.tooltip place="right" type="light" effect='solid' delayHide={100} delayUpdate={100} id='npstitle'>
                                                    <div className="tooltip-wrapper">
                                                        <div className="inner-tooltip-wrapper">
                                                            <div className="tooltip-header clearfix">
                                                                <div className="analytics-tooltip-title">
                                                                    <span>NPS</span>
                                                                </div>

                                                            </div>
                                                            <div className="tooltip-about-wrapper">
                                                                <span>ABOUT</span>
                                                                <p>An aggregated score of all NPS responses from customer, job seekers and candidates, resulting in an overall organisational rating. </p>
                                                            </div>
                                                        </div>
                                                    </div>           
                                    </this.props.tooltip> :''}
                                </div>
                            </div>
                            <div className="culture-score-number-value-wrapper clearfix">
                                <div className="culture-score-number">
                                     <span className="large-number">{ this.props.data!=undefined ? Math.floor(this.props.data.normalized_score): 0} / 10</span>
                                </div>
                                <div className="culture-score-date-time-graph">
                                    {/* chart here */} 
                                                    {typeof this.props.data!=="undefined"?
                                                     <div>
                                                        <div class="culture-score-graph-image">
                                                                <div>
                                                                { this.props.data.difference!=undefined ? utils.rankDiff(this.props.data.difference.toFixed(0), true) : utils.rankDiff(0, true)}
                                                                </div>
                                                                <span class="number">{typeof this.props.data.difference!=="undefined"?this.props.data.difference:0}
                                                                %
                                                                </span>
                                                        </div>
                                                        <div className="culture-score-date">
                                                        <div>Change since</div> 
                                                        <div>{this.state.currentDate}</div>
                                                    </div> 
                                                    </div>
                                                        :''}                         
                                </div>
                            </div>
                            <div className="culture-score-graph-wrapper clearfix">
                                <div className="segmentation-wrapper">
                                    <h4>Segmentations<span className="svg-icon help-element"><svg xmlns="http://www.w3.org/2000/svg" data-tip data-for='npssegment' width="24" height="24" viewBox="0 0 24 24"><path d="M0 0h24v24H0z" fill="none" /><path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm1 17h-2v-2h2v2zm2.07-7.75l-.9.92C13.45 12.9 13 13.5 13 15h-2v-.5c0-1.1.45-2.1 1.17-2.83l1.24-1.26c.37-.36.59-.86.59-1.41 0-1.1-.9-2-2-2s-2 .9-2 2H8c0-2.21 1.79-4 4-4s4 1.79 4 4c0 .88-.36 1.68-.93 2.25z" /></svg></span></h4>
                                   {this.props.tooltip!= null ? <this.props.tooltip place="right" type="light" effect='solid' delayHide={100} delayUpdate={100} id='npssegment'>
                                                    <div className="tooltip-wrapper">
                                                        <div className="inner-tooltip-wrapper">
                                                            <div className="tooltip-header clearfix">
                                                                <div className="analytics-tooltip-title">
                                                                    <span>Segmentations</span>
                                                                </div>

                                                            </div>
                                                            <div className="tooltip-about-wrapper">
                                                                <span>ABOUT</span>
                                                                <p> A segmentation of responders to NPS, broken down into groups of Advocates and Detractors.</p>
                                                            </div>
                                                        </div>
                                                    </div>           
                                    </this.props.tooltip> : ''}
                                </div>
                                <div className="csClmWpr">
                                    <div className="culture-score-detractors csClm">
                                        <div className="culture-score-detractors-count cscnt">
        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><circle cx="15.5" cy="9.5" r="1.5" /><circle cx="8.5" cy="9.5" r="1.5" /><path d="M11.99 2C6.47 2 2 6.48 2 12s4.47 10 9.99 10C17.52 22 22 17.52 22 12S17.52 2 11.99 2zM12 20c-4.42 0-8-3.58-8-8s3.58-8 8-8 8 3.58 8 8-3.58 8-8 8zm0-6c-2.33 0-4.32 1.45-5.12 3.5h1.67c.69-1.19 1.97-2 3.45-2s2.75.81 3.45 2h1.67c-.8-2.05-2.79-3.5-5.12-3.5z" /></svg><span class="detractors-count cscnt">{this.props.data!=undefined ? Math.floor(this.props.data.detractors_percentage): 0}%</span>
                                        </div>
                                        <span className="detractors-content">Detractors<span className="cultureNumber">{`(${this.props.data!=undefined ? this.props.data.detractors: 0})`}</span> </span>


                                    </div>
                                    <div className="culture-score-detractors csClm">
                                        <div className="culture-score-detractors-count cscnt potential-detractors">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><circle cx="15.5" cy="9.5" r="1.5" /><circle cx="8.5" cy="9.5" r="1.5" /><path d="M11.99 2C6.47 2 2 6.48 2 12s4.47 10 9.99 10C17.52 22 22 17.52 22 12S17.52 2 11.99 2zM12 20c-4.42 0-8-3.58-8-8s3.58-8 8-8 8 3.58 8 8-3.58 8-8 8zm0-6c-2.33 0-4.32 1.45-5.12 3.5h1.67c.69-1.19 1.97-2 3.45-2s2.75.81 3.45 2h1.67c-.8-2.05-2.79-3.5-5.12-3.5z" /></svg><span class="detractors-count cscnt">{this.props.data!=undefined ? Math.floor(this.props.data.potential_detractors_percentage): 0}%</span>
                                        </div>
                                        <span className="detractors-content">Potential Detractors<span className="cultureNumber">{`(${this.props.data!=undefined ? this.props.data.potential_detractors: 0})`}</span> </span>


                                    </div>
                                    <div className="culture-score-passives csClm">
                                        <div className="culture-score-passives-count cscnt">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><circle cx="15.5" cy="9.5" r="1.5"></circle><circle cx="8.5" cy="9.5" r="1.5"></circle><path d="M11.99 2C6.47 2 2 6.48 2 12s4.47 10 9.99 10C17.52 22 22 17.52 22 12S17.52 2 11.99 2zM12 20c-4.42 0-8-3.58-8-8s3.58-8 8-8 8 3.58 8 8-3.58 8-8 8zm0-6c-2.33 0-4.32 "></path></svg><span class="passives-count cscnt"> {this.props.data!=undefined ? Math.floor(this.props.data.passives_percentage): 0}%</span>
                                        </div>
                                        <span className="passives-content">Passives<span className="cultureNumber">{`(${this.props.data!=undefined ? this.props.data.passives: 0})`}</span> </span>


                                    </div>
                                    <div className="culture-score-promotors csClm">
                                        <div className="culture-score-promotors-count cscnt potential-promoters">
                                            <i class="material-icons">sentiment_satisfied_alt</i><span class="promotors-count cscnt">{this.props.data!=undefined ? Math.floor(this.props.data.potential_advocates_percentage): 0}%</span>
                                        </div>
                                        <span className="promotors-content">Potential Promoters <span className="cultureNumber"> {`(${this.props.data!=undefined ? this.props.data.potential_advocates: 0})`}</span></span>


                                    </div>
                                    <div className="culture-score-promotors csClm">
                                        <div className="culture-score-promotors-count cscnt">
                                            <i class="material-icons">sentiment_satisfied_alt</i><span class="promotors-count cscnt">{this.props.data!=undefined ? Math.floor(this.props.data.advocates_percentage): 0}%</span>
                                        </div>
                                        <span className="promotors-content">Promoters <span className="cultureNumber"> {`(${this.props.data!=undefined ? this.props.data.advocates: 0})`}</span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="participation-main-wrapper">
                        <div className="participation">
                        {this.state.Recharts != null ? 

                           <NPSTopFunnel 
                           data={this.props.NPS.funnelListData}
                           loading={this.props.NPS.funnelListDataLoading}
                           COLORS={COLORS}
                           period={this.state.period}
                           tooltip={this.state.tooltip}
                           refreshChart={this.state.refreshChart}
                           fetchFunnelListData={this.fetchFunnelListData.bind(this)}
                           taggedTimeSpan={this.selectedTimeSpan.bind(this, null)}
                            />
                           : ""

                           }
                        </div>
                    </div>
                </div>
        );
    }
}
function mapStateToProps(state) {
    return {
        NPS: state.NPSAnalytics,
    }
}
function mapDispatchToProps(dispatch) {
    return {
        NPSAnalyticsActions: bindActionCreators(NPSAnalyticsActions, dispatch)
    }
}
let connection = connect(mapStateToProps, mapDispatchToProps);
module.exports = connection(NpsOverallAnalytics);                               