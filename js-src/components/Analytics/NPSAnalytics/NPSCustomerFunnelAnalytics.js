import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import PreLoader from '../../PreLoader'
import * as utils from '../../../utils/utils';
import CultureLoader from '../CultureScore/CultureLoader'; 
import AnalyticsDashboardFilter from '../AnalyticsDashboard/AnalyticsDashboardFilter';
class NPSCustomerFunnelListAnalytics extends React.Component {
    constructor(props) {
        super(props);
         this.state = {
            opacity: {},
             expandLegend: false
        }
        
       
    }
    componentDidMount(){
    }
    expandLegend(action){
        this.setState({expandLegend: action})
    }
  
    CustomizedLabel = (props) => {
         const {payload}  = props;

        return (
            <div>
            <div className={props.expandLegend == true?'showMore legendUlWrapper':'legendUlWrapper'}>
             {props.expandLegend == true?
                <div className="legendHeader clearfix">
                    <div className="legendHeaderTitle">Customer NPS by Funnel</div>
                    <div className="showLessBtnWrapper"> <a onClick={this.expandLegend.bind(this, false)}> <i class="material-icons">clear</i></a></div>
                </div>
                                         :''   
                                        }
            <ul>
                {payload.map((entry, index) => (
                        props.expandLegend == false ? 
                            index <= 4 ? 
                                <li key={index} onMouseEnter={this.handleMouseEnter.bind(this,entry)} onMouseLeave={this.handleMouseLeave.bind(this,entry)}>
                                    <div key={`item-${index}`} data-color={entry.color}>
                                        <span  className = "rounder-dot" style={{background:entry.color}}></span>
                                        <span className ="title-active-user">{entry.value}</span>
                                        <span className="text-no"> {entry.payload.count}</span>
                                    </div>
                                </li>
                            :''
                        : 
                            <div key={index} onMouseEnter={this.handleMouseEnter.bind(this,entry)} onMouseLeave={this.handleMouseLeave.bind(this,entry)}>
                                        <li key={`item-${index}`} data-color={entry.color}>
                                            <span  className = "rounder-dot" style={{background:entry.color}}></span>
                                            <span className ="title-active-user">{entry.value}</span>
                                            <span className="text-no"> {entry.payload.count}</span>
                                        </li>
                            </div>    

           
                ))}
            </ul>
            {payload.length > 5 ?
                <div className="readMoreLi" >
                    {/* <a onClick={this.expandLegend.bind(this, true)}>+{payload.length - 5} more</a> */}
                    <a title={'+'+payload.length - 5+' more'} onClick={this.expandLegend.bind(this, true)}><span>...</span></a>
                </div>
                :''   
            }
            </div>
            </div>
        );
    }
    customizeTooltip = props => {
        const { payload, newTooltipdata, label } = props
        if (payload.length > 0) {
            return(
                <div className='custom-tooltip'>
                    <p className='label' style={{color:payload[0].payload.fill}}>
                        {payload[0].name}: {payload[0].value}
                    </p>
                </div>
            )
                
        }
    }
    handleMouseEnter(o) {
        const { opacity } = this.state;
        this.setState({
            opacity: { key: o.value, val: 0.5 },
        });
    }
  
   handleMouseLeave(o) {
    const { opacity } = this.state;
    this.setState({
            opacity: [],
    });
   
   }
    renderCharts(nps, avg, ReactTooltip, TaggedValuesGraphDimention){
        return(
                            <div className="taggedMain2">
                                 {/* <div className="tagged-voted-value-header">
                                        <div className="expand-wrapper">
                                            <a href="#"> Expand<i class="material-icons">keyboard_arrow_right</i></a>
                                        </div>
                                    </div>*/}
                                    <div className="tagged-value-wrapper">               
                                     <div className="participation">
                                        <div className="participation-header clearfix">
                                            <div className="participation-wrapper">
                                                <h4>Customer NPS by Funnel <i class="material-icons" data-tip data-for='npscustomer'>help</i></h4>
                                                {ReactTooltip !== null ? 
                                                <ReactTooltip place="right" type="light" effect='solid' delayHide={100} delayUpdate={100} id='npscustomer'>
                                                    <div className="tooltip-wrapper">
                                                        <div className="inner-tooltip-wrapper">
                                                            <div className="tooltip-header clearfix">
                                                                <div className="analytics-tooltip-title">
                                                                    <span>Customer NPS by Funnel</span>
                                                                </div>

                                                            </div>
                                                            <div className="tooltip-about-wrapper">
                                                                <span>ABOUT</span>
                                                                <p>A mean NPS score broken down by the customer journey toward the purchase of a good or service. </p>
                                                            </div>
                                                        </div>
                                                    </div>           
                                                </ReactTooltip>
                                            :''  
                                            }
                                            </div>
                                            {nps.length>0 ?<a  className="expand-link" onClick={this.props.onExpand()}>Expand <span><i class="material-icons">navigate_next</i></span></a>:''}
                                          
                                            <div className="expand-year-main-wrapper">
                                                <div className="expand-year-wrapper">
                                                    <div className="yearly-filtered-data">
                                                        <a className={`week-filter ${this.props.period=='week'?'active':''}`} onClick={this.props.fetchFunnelListData.bind(this,'week')}> Week </a>
                                                         <a className={`month-filter ${this.props.period == undefined || this.props.period=='month'?'active':''}`} onClick={this.props.fetchFunnelListData.bind(this,'month')}>  Month  </a>
                                                         <a className={`year-filter ${this.props.period=='year'?'active':''}`} onClick={this.props.fetchFunnelListData.bind(this,'year')}>Year </a>
                                                         
                        </div>
                        {this.props.isFilterClicked==true ? <AnalyticsDashboardFilter includeLocation={true} callFrom='tagged' period={this.props.period} fetchFilterData={this.props.fetchFilterData}/>:''}
                                                </div>
                                            </div>
                                          

                                        </div>
                                       <div class="tagged-value-pie-chart clearfix piechart-wrapper">
                                       {/* pie-chart */}
                                       {typeof nps !== 'undefined' && nps.length >0 ? 
                                        <div>
                                                <this.props.PieChart width={TaggedValuesGraphDimention.width} height={TaggedValuesGraphDimention.height} dataKey="title">
                                                    <this.props.Pie data={nps} cx={TaggedValuesGraphDimention.cx}
                                                        cy={TaggedValuesGraphDimention.cy}
                                                        activeShape={<text x={0} y={0} textAnchor="middle" fill='#82ca9d'>120 values</text>}
                                                        innerRadius={TaggedValuesGraphDimention.innerRadius}
                                                        outerRadius={TaggedValuesGraphDimention.outerRadius} fill="#82ca9d" dataKey="count" nameKey='title'>
                                                        {
                                                            nps.map((entry, index) =>
                                                                    <this.props.Cell key={index} fill={this.props.COLORS[index % this.props.COLORS.length]} opacity={Object.keys(this.state.opacity).length > 0 ? (this.state.opacity.key == entry.title) ? 1 : 0.4 : 1}  />
                                                            )
                                                        }
                                                    </this.props.Pie>
                                                    <this.props.Tooltip content={this.customizeTooltip} />
                                                    <this.props.Legend align="left" verticalAlign="middle" iconType="circle" iconSize={10} layout="vertical" wrapperStyle={{ width:0 }}  content={this.CustomizedLabel} expandLegend={this.state.expandLegend}  />
                                                </this.props.PieChart>
                                            <div class="donutchart-center-content"><span>{typeof avg !== "undefined" ? Math.round(avg) : 0}</span><p>Score</p></div>
                                         </div>
                                        : <div className = "no-Value-chart">No Funnel Found</div>}
                                       </div>
                                    </div>
                                 </div>
                        </div>         
        )
    }
render() {
       var customerArray=[]

       if(this.props.data.customer!= undefined)
       {
        this.props.data.customer.filter(element =>
            {  
                var obj ={
                    title: element.funnel_name,
                    count: parseInt(element.funnel_nps) 
                }
                customerArray.push(obj);
            })
       }

        var windowDimention=utils.windowSize();
        var TaggedValuesGraphDimention = utils.getChartDimension(windowDimention.w,'cultureChart');

        return (                    
                    <div className="taggedMain1">
                    
                         { 
                         this.props.loading == true ? 
                         <div className="tagged-value-wrapper">
                                 <CultureLoader/> 
                         </div>  

                            :
                            this.renderCharts(customerArray, this.props.data.customer_avg, this.props.tooltip, TaggedValuesGraphDimention)
                        } 
                    </div>
                    

                 );
        //content={this.CustomizedLabe}
    }
}
function mapStateToProps(state) {
    return {
    }
}
function mapDispatchToProps(dispatch) {
    return {
    }
}
let connection = connect(mapStateToProps, mapDispatchToProps);
module.exports = connection(NPSCustomerFunnelListAnalytics);                               