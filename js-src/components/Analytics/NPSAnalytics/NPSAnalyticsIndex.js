import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import PreLoader from '../../PreLoader'
import Header from '../../Header/Header';
import Globals from '../../../Globals';
import { Link } from 'react-router';
import AnalyticsNav from '../AnalyticsNav';
import NpsOverallAnalytics from './NpsOverallAnalytics';
import NPSCustomerFunnelListAnalytics from './NPSCustomerFunnelAnalytics';
import NPSJobFunnelListAnalytics from './NPSJobFunnelListAnalytics';
import reactTooltip from '../../Chunks/ChunkReactTooltip';
import NPSCommentList from './NpsComment';
import * as NPSAnalyticsActions from '../../../actions/analytics/NpsAnalyticsActions';
import * as utils from '../../../utils/utils';
import GuestUserRestrictionPopup from '../../GuestUserRestrictionPopup';
import recharts from '../../Chunks/ChunkRecharts';
const COLORS = ['#4bcf99', '#ff595e', '#4b74e0', '#ffbf00', '#b87d7a', '#FF8042','#495867','#7FB069','#B8D8D8','#A93F55','#577399','#F9ADA0',"#ACF39D",'#19323C'];
class NPSAnalyticsIndex extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            expand: false,
            tooltip: null,
            refreshChart: false,
            period: 'month',
            isFilterClicked: false,
            filterData: {},
            funnelId:null,
            funnelType:null,
            Recharts: null,
        }
    }

    componentWillMount() {
        reactTooltip().then(reactTooltip => {
            recharts().then(recharts => {
                this.setState({ tooltip: reactTooltip, Recharts: recharts })
            })
        })
    }

    componentDidMount() {

        let roleName = JSON.parse(localStorage.getItem('roles')) !== null ? JSON.parse(localStorage.getItem('roles'))[0].role_name : ''
        if (roleName !== 'guest') {
            var timezone = utils.clientTimezone();
            var ObjToSend = {
                'timezone': timezone,
                'timespan': "month"
            }
            this.props.NPSAnalyticsActions.getOverAllNPS(ObjToSend);
            this.props.NPSAnalyticsActions.getFunnelListAnalytics(this.state.period);
        }

    }
    

    fetchfunnelDetail(values) {
        this.props.NPSAnalyticsActions.getFunnelDetailAnalytics(values);
    }

    fetchFunnelCommentList(values) {
        this.props.NPSAnalyticsActions.getNPSCommentList(values);
    }

    fetchFunnelListData(values) {
        this.setState({
            period:values
        })
        this.props.NPSAnalyticsActions.getFunnelListAnalytics(values);
    }

    onExpand(type) {        
        if(!this.state.expand == false)
        {    
            if(type == 'customer')
            {
                this.setState({
                    expand: !this.state.expand,
                    funnelId:this.props.NPS.funnelListData.customer[0].funnel_identity,
                    funnelType:type,
                })  
            }
            else{
                this.setState({
                    expand: !this.state.expand,
                    funnelId:this.props.NPS.funnelListData.job[0].funnel_identity,
                    funnelType:type,
                })
            } 
        }
        else{
            this.setState({
                expand: !this.state.expand,
                funnelId:null,
                funnelType:null,
            })
        } 
    }

    closeExpand() {
        this.setState({
            expand: false
        })
    }

    selectedTimeSpan(newprops = null, time) {
        var timezone = utils.clientTimezone();
        var ObjToSend = {
            timezone: timezone,
            timespan: time
        }
        this.props.fetchFunnelListData(dataToPass)
    }

    render() {
        return (
            <div id="NPS-dashboard" className="Value-Scroll-page">
                <GuestUserRestrictionPopup setOverlay={true} />
                <section id="analytics-Main-container" className="analytics-wrapper Value-Scroll-Container culture-new-tab">
                    <Header
                        title={'Analytics'}
                        location={this.props.location}
                    />
                    <div className='main-container'>
                        <div id="content">
                            <div className="culture-dashboard-tab-responsive culture-score-tab clearfix">
                                <AnalyticsNav location={this.props.location} />
                            </div>
                            {!this.state.expand ?
                             <div className="culture-participation-mainbox dashboard-analytics-level1-container">

                                    <NpsOverallAnalytics
                                        data={this.props.NPS.overAllNPSData}
                                        tooltip={this.state.tooltip}

                                    />

                                    <div class="tagged-voted-value nps-participation-wrapper">
                                    {this.state.Recharts !== null?
                                    <div class="tagged-voted-value-main-box clearfix">
                                    <NPSCustomerFunnelListAnalytics
                                        data={this.props.NPS.funnelListData}
                                        loading={this.props.NPS.funnelListDataLoading}
                                        PieChart={this.state.Recharts.PieChart}
                                        Pie={this.state.Recharts.Pie}
                                        Cell={this.state.Recharts.PieChart.Cell}
                                        COLORS={COLORS}
                                        Tooltip={this.state.Recharts.Tooltip}
                                        period={this.state.period}
                                        Legend={this.state.Recharts.Legend}
                                        tooltip={this.state.tooltip}
                                        onExpand={()=>this.onExpand.bind(this,'customer')}
                                        refreshChart={this.state.refreshChart}
                                        fetchFunnelListData={this.fetchFunnelListData.bind(this)}
                                        taggedTimeSpan={this.selectedTimeSpan.bind(this, null)} />

                                    <NPSJobFunnelListAnalytics
                                       data={this.props.NPS.funnelListData}
                                       loading={this.props.NPS.funnelListDataLoading}
                                       PieChart={this.state.Recharts.PieChart}
                                       Pie={this.state.Recharts.Pie}
                                       Cell={this.state.Recharts.PieChart.Cell}
                                       COLORS={COLORS}
                                       Tooltip={this.state.Recharts.Tooltip}
                                       period={this.state.period}
                                       onExpand={()=>this.onExpand.bind(this,'customer')}
                                       Legend={this.state.Recharts.Legend}
                                       tooltip={this.state.tooltip}
                                       refreshChart={this.state.refreshChart}
                                       fetchFunnelListData={this.fetchFunnelListData.bind(this)}
                                       taggedTimeSpan={this.selectedTimeSpan.bind(this, null)} />   
                                        </div>
                                        : ""}
                                        
                                        </div> 
                                </div>
                                :
                                <div>
                                    <NPSCommentList
                                        funnelId={this.props.funnelId}
                                        funnelType={this.state.funnelType}
                                        Expand={this.closeExpand.bind(this)}
                                        funnelId={this.props.NPS.funnelListData.customer[0].funnel_identity}
                                        funnelType={'customer'}
                                        COLORS={COLORS}
                                        tooltip={this.state.tooltip}
                                        taggedTimeSpan={this.selectedTimeSpan.bind(this, null)}
                                    />
                                </div>
                            }

                        </div>
                    </div>
                </section>
            </div>
        )
    }
}
function mapStateToProps(state) {
    return {
        NPS: state.NPSAnalytics,
    }
}
function mapDispatchToProps(dispatch) {
    return {
        NPSAnalyticsActions: bindActionCreators(NPSAnalyticsActions, dispatch)
    }
}
var connection = connect(mapStateToProps, mapDispatchToProps);
module.exports = connection(NPSAnalyticsIndex);