import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import NPSFunnelDetailAnalytics from './NPSFunnelDetailAnalytics';
import NPSfilter from './NPSfilter';

import * as NPSAnalyticsActions from '../../../actions/analytics/NpsAnalyticsActions';


class NpsComment extends React.Component {
    constructor(props) {
        super(props);
        this.state =
        {
            period: 'month',
            showFilter: false,
            funnelStage: []
        }
    }

    componentWillReceiveProps(nextProps) {
        { nextProps.NPS.funnelDetailData != null ? this.getFunnelStages(nextProps.NPS.funnelDetailData.stages) : '' }
    }

    componentWillMount() {
        var Obj = {
            funnelId: this.props.funnelId,
            timezone: 'month'
        }
        this.props.NPSAnalyticsActions.getNPSCommentList(Obj);
    }

    selectedFilterType(funnelid) {
        var Obj = {
            funnelId: funnelid,
            timezone: this.state.period
        }
        this.props.NPSAnalyticsActions.getNPSCommentList(Obj);
        this.props.NPSAnalyticsActions.getFunnelDetailAnalytics(Obj);
    }

    openFilterPopup() {
        this.setState({
            showFilter: !this.state.showFilter
        })
    }

    onTimeFilterChange(value) {
        var Obj = {
            funnelId: this.props.funnelId,
            timezone: value
        }
        this.setState({ period: value, showFilter: false })
        this.props.NPSAnalyticsActions.getNPSCommentList(Obj);
        this.props.NPSAnalyticsActions.getFunnelDetailAnalytics(Obj);

    }

    /*
    @author: Akshay soni
    @purpose: to the sort the object array,
    to show data for funnel chart in ascending order
     */
    compare(a, b) {
        const countA = a.stage_nps;
        const countB = b.stage_nps;
        let comparison = 0;
        if (countA > countB) {
            comparison = 1;
        } else if (countA < countB) {
            comparison = -1;
        }
        return comparison;
    }

    //to funnel stages for funnel chart
    getFunnelStages(stageList) {
        var sortedStages = stageList.sort(this.compare)
        this.setState({
            funnelStage: sortedStages
        })

    }

    render() {
        return (
            <div className="NpsCommentWrapper">
                <div className="InnerNpsCommentWrapper">
                    {/* funnel title section start */}
                    <div className="NpsFunnelCommentTitle clearfix">
                        <div className="funnelTitle">
                            <div> <a onClick={this.props.Expand}><i class="material-icons">keyboard_arrow_left</i> Back </a></div>
                            <h4>Filter by Funnel <span class="filterIcon npsFilter" onClick={this.openFilterPopup.bind(this)}>
                                {/* <i class="material-icons">filter_list</i> */}
                                {this.state.showFilter == true ? <NPSfilter data={this.props.funnelType == 'customer' ? this.props.NPS.funnelListData.customer : this.props.NPS.funnelListData.job} selectedFilterType={this.selectedFilterType.bind(this)}></NPSfilter> : ''}
                            </span></h4>
                        </div>
                        <div className="weekYearWrapper">
                            <div class="yearly-filtered-data">
                                <a class={`week-filter" ${this.state.period == "week" ? 'active' : ''}`} onClick={this.onTimeFilterChange.bind(this, 'week')}> Week </a>
                                <a class={`month-filter" ${this.state.period == "month" ? 'active' : ''}`} onClick={this.onTimeFilterChange.bind(this, 'month')}>  Month </a>
                                <a class={`year-filter" ${this.state.period == "year" ? 'active' : ''}`} onClick={this.onTimeFilterChange.bind(this, 'year')}>Year </a>
                                <span class="filterIcon " onClick={this.openFilterPopup.bind(this)}>
                                    <i class="material-icons">filter_list</i>
                                </span></div>
                        </div>
                    </div>
                    {/* funnel title section start */}
                    {/* funnel chart section start */}
                    <div className="funnelChartWrapper">
                        <div className="innerFunnelChartWrapper">
                            <div className="FunnelChartDiv">
                                <div className="NpsCommentTitle">
                                    <h4>NPS by Funnel Stage <i class="material-icons" data-tip data-for='npsstage'>help</i></h4>
                                </div>
                                <div className="dashboard-analytics-level1-container">
                                    <NPSFunnelDetailAnalytics
                                        expand={this.props.expand}
                                        funnelId={this.props.funnelId}
                                        funnelType={this.props.funnelType}
                                        COLORS={this.props.COLORS}
                                        tooltip={this.props.tooltip}
                                        taggedTimeSpan={this.props.taggedTimeSpan}
                                    />
                                </div>
                                <div className="nps-funnel-stage-chart-wrapper">
                                    <div className="inner-funnel-stage-wrapper">
                                        <div className="funnel-stage-block-wrapper">
                                            {this.state.funnelStage.length > 0 ?

                                                <div className="funnel-stage-block">
                                                    <div className="inner-funnel-stage-block"></div>

                                                    <div className="funnel-stage-block-content">
                                                        <span>{this.state.funnelStage[0].stage_nps}</span>
                                                        <p>{this.state.funnelStage[0].stage_name}</p>
                                                    </div>
                                                </div> : ''
                                            }
                                            {this.state.funnelStage.length > 1 ?
                                                <div className="funnel-stage-block">
                                                    <div className="inner-funnel-stage-block"></div>
                                                    <div className="funnel-stage-block-content">
                                                        <span>{this.state.funnelStage[1].stage_nps}</span>
                                                        <p>{this.state.funnelStage[1].stage_name}</p>
                                                    </div>
                                                </div> : ''
                                            }

                                            {this.state.funnelStage.length > 2 ?
                                                <div className="funnel-stage-block">
                                                    <div className="inner-funnel-stage-block"></div>
                                                    <div className="funnel-stage-block-content">
                                                        <span>{this.state.funnelStage[2].stage_nps}</span>
                                                        <p>{this.state.funnelStage[2].stage_name}</p>
                                                    </div>
                                                </div> : ''
                                            }

                                            {this.state.funnelStage.length > 3 ?
                                                <div className="funnel-stage-block">
                                                    <div className="inner-funnel-stage-block"></div>
                                                    <div className="funnel-stage-block-content">
                                                        <span>{this.state.funnelStage[3].stage_nps}</span>
                                                        <p>{this.state.funnelStage[3].stage_name}</p>
                                                    </div>
                                                </div> : ''
                                            }
                                            {this.state.funnelStage.length > 4 ?

                                                <div className={`funnel-stage-block application-block-`}>
                                                    <div className="inner-funnel-stage-block"></div>
                                                    <div className="onboardingWrapper"></div>
                                                    <div className="funnel-stage-block-content">
                                                        <span>{this.state.funnelStage[4].stage_nps}</span>
                                                        <p>{this.state.funnelStage[4].stage_name}</p>
                                                    </div>
                                                </div>

                                                : ''}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* funnel chart section end */}

                    {/* funnel comment list section start */}
                    <div className="NpsCommentSection clearfix">
                        {this.props.NPS.funnelCommentListLoading == false && this.props.NPS.funnelCommentList != null ?
                            <div className="innerCommentSection clearfix">
                                <div className="NpsCommentTitle">
                                    <h4>NPS Comments<i class="material-icons" data-tip="true" data-for="npscomment" currentitem="false">help</i></h4>
                                    {this.props.tooltip != null ?
                                        <this.props.tooltip place="right" type="light" effect='solid' delayHide={100} delayUpdate={100} id='npscomment'>
                                            <div className="tooltip-wrapper">
                                                <div className="inner-tooltip-wrapper">
                                                    <div className="tooltip-header clearfix">
                                                        <div className="analytics-tooltip-title">
                                                            <span>NPS Comments</span>
                                                        </div>

                                                    </div>
                                                    <div className="tooltip-about-wrapper">
                                                        <span>ABOUT</span>
                                                        <p> A list of scores and comments from NPS responders. </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </this.props.tooltip> : ''}
                                </div>

                                {this.props.NPS.funnelCommentList.map((item, index) =>
                                    <div className={`NpsCommentBlockWrapper clearfix ${index}`} key={`commentlist-${index}`}>
                                        <div className="NpsCommentBlock clearfix">
                                            <div className="NpsCommentLeftBlock clearfix">
                                                <div className={`SmilesIconText NpsBlock 
                                                 ${item.category == 'advocates' ? 'NpsGreenBlock' :
                                                        item.category == 'potential_advocates' ? 'NpsBlueBlock' :
                                                            item.category == 'detractors' ? '' :
                                                                item.category == 'potential_detractors' ? 'NpsOrangeBlock' :
                                                                    item.category == 'passives' ? 'NpsGrayBlock' :
                                                                        ''}
                                                 `}
                                                >
                                                    <div className="SmilesIcon">
                                                        {item.category == "detractors" ?
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><circle cx="15.5" cy="9.5" r="1.5"></circle><circle cx="8.5" cy="9.5" r="1.5"></circle><path d="M11.99 2C6.47 2 2 6.48 2 12s4.47 10 9.99 10C17.52 22 22 17.52 22 12S17.52 2 11.99 2zM12 20c-4.42 0-8-3.58-8-8s3.58-8 8-8 8 3.58 8 8-3.58 8-8 8zm0-6c-2.33 0-4.32 1.45-5.12 3.5h1.67c.69-1.19 1.97-2 3.45-2s2.75.81 3.45 2h1.67c-.8-2.05-2.79-3.5-5.12-3.5z"></path>
                                                            </svg>
                                                            : item.category == 'advocates' ?
                                                                <i class="material-icons">sentiment_satisfied_alt</i>
                                                                : item.category == 'potential_advocates' ?
                                                                    <i class="material-icons">sentiment_satisfied_alt</i>
                                                                    : item.category == 'passives' ?
                                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><circle cx="15.5" cy="9.5" r="1.5"></circle><circle cx="8.5" cy="9.5" r="1.5"></circle><path d="M11.99 2C6.47 2 2 6.48 2 12s4.47 10 9.99 10C17.52 22 22 17.52 22 12S17.52 2 11.99 2zM12 20c-4.42 0-8-3.58-8-8s3.58-8 8-8 8 3.58 8 8-3.58 8-8 8zm0-6c-2.33 0-4.32 "></path></svg>
                                                                        : item.category == 'potential_detractors' ?
                                                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><circle cx="15.5" cy="9.5" r="1.5"></circle><circle cx="8.5" cy="9.5" r="1.5"></circle><path d="M11.99 2C6.47 2 2 6.48 2 12s4.47 10 9.99 10C17.52 22 22 17.52 22 12S17.52 2 11.99 2zM12 20c-4.42 0-8-3.58-8-8s3.58-8 8-8 8 3.58 8 8-3.58 8-8 8zm0-6c-2.33 0-4.32 1.45-5.12 3.5h1.67c.69-1.19 1.97-2 3.45-2s2.75.81 3.45 2h1.67c-.8-2.05-2.79-3.5-5.12-3.5z"></path></svg> : ''
                                                        }

                                                    </div>
                                                    <div className="SmilesText">
                                                        {item.category == 'advocates' ? 'Advocates' :
                                                         item.category == 'potential_advocates' ? 'Potential Advocates' :
                                                         item.category == 'detractors' ? 'Detractors' :
                                                         item.category == 'potential_detractors' ? 'Potential Detractors' :
                                                         item.category == 'passives' ? 'Passives' :''}
                                                    </div>
                                                </div>
                                                <div className="valuesWrapper NpsBlock">
                                                    <span className="large-value">{item.score}</span> <span className="divide-value">/</span> <span className="small-value">10</span>
                                                </div>
                                                <div className="SocialInformationwrapper NpsBlock">
                                                    <div className="socailInformation">
                                                        <h5>{item.medium == "" ? 'medium not found' : item.medium}</h5>
                                                        <span className="mailWrapper">{item.feed_type}</span>
                                                        {/* <span>02/03/2020</span> */}
                                                    </div>
                                                </div>
                                                <div className="guestUser NpsBlock">
                                                    <span>Guest User <i _ngcontent-jbn-c19="" class="material-icons icon-image-preview">{item.feed_join == 1 ? 'done' : 'close'}</i> </span>
                                                </div>
                                            </div>
                                            <div className="NpsCommentRightBlock clearfix">
                                                <div className="NpsCommentDescription NpsBlockDescription">
                                                    <p>{item.comment}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                )
                                }
                            </div>
                            : ''}
                    </div>
                    {/* funnel comment list section end */}

                </div>
            </div>
        );
    }
}
function mapStateToProps(state) {
    return {
        NPS: state.NPSAnalytics,

    }
}
function mapDispatchToProps(dispatch) {
    return {
        NPSAnalyticsActions: bindActionCreators(NPSAnalyticsActions, dispatch)

    }
}
let connection = connect(mapStateToProps, mapDispatchToProps);
module.exports = connection(NpsComment);                               