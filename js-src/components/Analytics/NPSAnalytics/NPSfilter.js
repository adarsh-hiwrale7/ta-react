import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

class NPSFilter extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            currentList:[]
        }
    }

    componentDidMount()
    {
        this.setState({
            currentList: this.props.data
        })
    }

    renderInnerFilterLoader() {
        return (
            <div>
                <a className="loader-line-radius loader-line-height loader-grey-line loader-tag-child-value-analytics"> </a>
                <a className="loader-line-radius loader-line-height loader-grey-line loader-tag-child-value-analytics"> </a>
                <a className="loader-line-radius loader-line-height loader-grey-line loader-tag-child-value-analytics"> </a>
                <a className="loader-line-radius loader-line-height loader-grey-line loader-tag-child-value-analytics"> </a>
                <a className="loader-line-radius loader-line-height loader-grey-line loader-tag-child-value-analytics"> </a>
                <a className="loader-line-radius loader-line-height loader-grey-line loader-tag-child-value-analytics"> </a>
                <a className="loader-line-radius loader-line-height loader-grey-line loader-tag-child-value-analytics"> </a>
                <a className="loader-line-radius loader-line-height loader-grey-line loader-tag-child-value-analytics"> </a>
            </div>

        )
    }

    handleSearchValue(e) {
        if (e !== null) {
            if (e.target.value !== "" && this.props.data.length > 0) {
                var i = 0;
                search = e.target.value.toLowerCase();
               


                var tempobj = this.props.data.filter(function (temp) {
                    i = i + 1;
                    var name = temp.funnel_name
                    if (name.toLowerCase().includes(search)) {
                        return true;
                    }
                })
                this.setState({
                    currentList: tempobj
                })
            }
            else {
                this.setState({
                    currentList: this.props.data
                })
            }
        }
    }

    render() {
        var filterList = this.state.currentList
        return (

            <section id="analytics-Main-container" className="analytics-wrapper">
                <div className="filter-analytics">
                    <div className="inner-filter-analytics" >
                        <div className={`main-filter`} >
                            <div className="main-filter-container clearfix" id='main-filter'>
                                <div className="filter-tag-parent">
                                    <div className="tag-container">

                                        <div className="filter-tag-child">
                                            <div className="inner-filter-tag-child">
                                                <div className="search-header">
                                                    <div className="search-box-container">
                                                        <input
                                                            type='text'
                                                            name='filter_search'
                                                            autoCapitalize='none'
                                                            autoComplete='off'
                                                            autoCorrect='off'
                                                            placeholder='Search'
                                                            className='search_values'
                                                            onChange={this.handleSearchValue.bind(this)} />
                                                        <a className="search-icon"><span> <i class="material-icons">search</i></span></a>
                                                    </div>
                                                </div>

                                                <div className="child-tag-container">
                                                    <div className="tag-container">
                                                        {filterList.map((list, i) => {
                                                            return (
                                                                <a key={i} >
                                                                    <span onClick={this.props.selectedFilterType.bind(this, list.funnel_identity)}> {list.funnel_name}  </span>
                                                                </a>
                                                            )
                                                        })}
                                                    </div>
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        );
    }

}

function mapStateToProps(state) {
    return {

    }
}

function mapDispatchToProps(dispatch) {
    return {

    }
}

let connection = connect(mapStateToProps, mapDispatchToProps);

module.exports = connection(NPSFilter);
