import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import PreLoader from "../../PreLoader";
import recharts from "../../Chunks/ChunkRecharts";
import * as utils from "../../../utils/utils";
import CultureLoader from "../CultureScore/CultureLoader";
import AnalyticsDashboardFilter from "../AnalyticsDashboard/AnalyticsDashboardFilter";
import * as NPSAnalyticsActions from "../../../actions/analytics/NpsAnalyticsActions";
/**
 * @author Meet
 * NPS Top Funnel Chart
 */
class NPSTopFunnel extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      opacity: {},
      expandLegend: false,
      Recharts: null,
    };
  }
  componentWillMount() {
    recharts().then((recharts) => {
      this.setState({ Recharts: recharts });
    });
  }

  componentDidMount() {
    this.props.NPSAnalyticsActions.getNPSTopFunnel();
  }

  CustomizedLabel = (props) => {
    const { payload } = props;
    return (
      <div
        className={
          props.expandLegend == true
            ? "showMore legendUlWrapper"
            : "legendUlWrapper"
        }
      >
        {props.expandLegend == true ? (
          <div className="legendHeader clearfix">
            <div className="legendHeaderTitle">Tagged values</div>
            <div className="showLessBtnWrapper">
              {" "}
              <a onClick={this.expandLegend.bind(this, false)}>
                {" "}
                <i class="material-icons">clear</i>
              </a>
            </div>
          </div>
        ) : (
          ""
        )}
        <ul>
          {payload.map((entry, index) =>
            props.expandLegend == false ? (
              index <= 4 ? (
                <li
                  key={index}
                  onMouseEnter={this.handleMouseEnter.bind(this, entry)}
                  onMouseLeave={this.handleMouseLeave.bind(this, entry)}
                >
                  <div key={`item-${index}`} data-color={entry.color}>
                    <span
                      className="rounder-dot"
                      style={{ background: entry.color }}
                    ></span>
                    <span className="title-active-user">{entry.value}</span>
                    <span className="text-no"> {entry.payload.count}</span>
                  </div>
                </li>
              ) : (
                ""
              )
            ) : (
              <div
                key={index}
                onMouseEnter={this.handleMouseEnter.bind(this, entry)}
                onMouseLeave={this.handleMouseLeave.bind(this, entry)}
              >
                <li key={`item-${index}`} data-color={entry.color}>
                  <span
                    className="rounder-dot"
                    style={{ background: entry.color }}
                  ></span>
                  <span className="title-active-user">{entry.value}</span>
                  <span className="text-no"> {entry.payload.count}</span>
                </li>
              </div>
            )
          )}
        </ul>
        {payload.length > 5 ? (
          <div className="readMoreLi">
            <a
              title={"+" + payload.length - 5 + " more"}
              onClick={this.expandLegend.bind(this, true)}
            >
              <span>...</span>
            </a>
          </div>
        ) : (
          ""
        )}
      </div>
    );
  };
//tool tip with customization of colour
  customizeTooltip = (props) => {
    const { payload, newTooltipdata, label } = props;
    if (payload.length > 0) {
      return (
        <div className="custom-tooltip">
          <p className="label" style={{ color: payload[0].payload.fill }}>
            {payload[0].name}: {payload[0].value}
          </p>
        </div>
      );
    }
  };

  //will handle mouse Hover
  handleMouseEnter(o) {
    const { opacity } = this.state;
    this.setState({
      opacity: { key: o.value, val: 0.5 },
    });
  }
//Will handle mouse leave
  handleMouseLeave(o) {
    const { opacity } = this.state;
    this.setState({
      opacity: [],
    });
  }
 
  //will render the Top funnel Chart
  renderCharts(nps, nps_avg, ReactTooltip, TaggedValuesGraphDimention) {
    return (
      <div className="taggedMain2 clearfix">
            <div className="participation-header clearfix">
              <div className="participation-wrapper">
                <h4>
                  NPS Top Funnel{" "}
                  <i class="material-icons" data-tip data-for="top-funnel-tooltip">
                    help
                  </i>
                </h4>
                {ReactTooltip !== null ? (
                  <ReactTooltip
                    place="right"
                    type="light"
                    effect="solid"
                    delayHide={100}
                    delayUpdate={100}
                    id="top-funnel-tooltip"
                  >
                    <div className="tooltip-wrapper">
                      <div className="inner-tooltip-wrapper">
                        <div className="tooltip-header clearfix">
                          <div className="analytics-tooltip-title">
                            <span>NPS Top Funnel</span>
                          </div>
                        </div>
                        <div className="tooltip-about-wrapper">
                          <span>ABOUT</span>
                          <p>
                           A breakdown of top performing NPS funnel.{" "}
                          </p>
                        </div>
                      </div>
                    </div>
                  </ReactTooltip>
                ) : (
                  ""
                )}
              </div>
              <div className="expand-year-main-wrapper">
                <div className="expand-year-wrapper">
                  <div className="yearly-filtered-data">
                    <a
                      className={`week-filter ${
                        this.props.period == "week" ? "active" : ""
                      }`}
                      onClick={this.props.fetchFunnelListData.bind(
                        this,
                        "week"
                      )}
                    >
                      {" "}
                      Week{" "}
                    </a>
                    <a
                      className={`month-filter ${
                        this.props.period == undefined ||
                        this.props.period == "month"
                          ? "active"
                          : ""
                      }`}
                      onClick={this.props.fetchFunnelListData.bind(
                        this,
                        "month"
                      )}
                    >
                      {" "}
                      Month{" "}
                    </a>
                    <a
                      className={`year-filter ${
                        this.props.period == "year" ? "active" : ""
                      }`}
                      onClick={this.props.fetchFunnelListData.bind(
                        this,
                        "year"
                      )}
                    >
                      Year{" "}
                    </a>
                  </div>

                  {this.props.isFilterClicked == true ? (
                    <AnalyticsDashboardFilter
                      includeLocation={true}
                      callFrom="tagged"
                      period={this.props.period}
                      fetchFilterData={this.props.fetchFilterData}
                    />
                  ) : (
                    ""
                  )}
                </div>
              </div>
            </div>
            <div class="tagged-value-pie-chart clearfix piechart-wrapper">
              {/* pie-chart */}
              {typeof nps !== "undefined" &&
              nps.length > 0 &&
              this.state.Recharts !== null ? (
                <div>
                  <this.state.Recharts.PieChart
                    width={TaggedValuesGraphDimention.width}
                    height={TaggedValuesGraphDimention.height}
                    dataKey="title"
                  >
                    <this.state.Recharts.Pie
                      data={nps}
                      cx={TaggedValuesGraphDimention.cx}
                      cy={TaggedValuesGraphDimention.cy}
                      activeShape={
                        <text x={0} y={0} textAnchor="middle" fill="#82ca9d">
                          120 values
                        </text>
                      }
                      innerRadius={TaggedValuesGraphDimention.innerRadius}
                      outerRadius={TaggedValuesGraphDimention.outerRadius}
                      fill="#82ca9d"
                      dataKey="count"
                      nameKey="title"
                    >
                      {nps.map((entry, index) => {
                        <this.state.Recharts.Cell
                          key={index}
                          fill={
                            this.props.COLORS[index % this.props.COLORS.length]
                          }
                          opacity={
                            Object.keys(this.state.opacity).length > 0
                              ? this.state.opacity.key == entry.title
                                ? 1
                                : 0.4
                              : 1
                          }
                        />;
                      })}
                    </this.state.Recharts.Pie>
                    <this.state.Recharts.Tooltip
                      content={this.customizeTooltip}
                    />
                    <this.state.Recharts.Legend
                      align="left"
                      verticalAlign="middle"
                      iconType="circle"
                      iconSize={10}
                      layout="vertical"
                      wrapperStyle={{ width: 0 }}
                      content={this.CustomizedLabel}
                      expandLegend={this.state.expandLegend}
                    />
                  </this.state.Recharts.PieChart>
                  <div class="donutchart-center-content">
                    <span>
                      {typeof nps_avg !== "undefined" ? Math.round(nps_avg) : 0}
                    </span>
                    <p>Score</p>
                  </div>
                </div>
              ) : (
                <div className="no-Value-chart">No Data Found</div>
              )}
            </div>
      </div>
    );
  }

  render() {
    //render charts by stages
    var stageList = [];
    if (
     Object.keys(this.props.NPS.topFunnel).length > 0 &&
      this.props.NPS.topFunnel.stages != undefined
    ) {
      this.props.NPS.topFunnel.stages.filter((element) => {
        var obj = {
          title: element.stage_name,
          count: parseInt(element.stage_nps),
        };
        stageList.push(obj);
      });
    }

    var windowDimention = utils.windowSize();
    var TaggedValuesGraphDimention = utils.getChartDimension(
      windowDimention.w,
      "cultureChart"
    );
    return (
      <div className="taggedMain1">
        {this.props.loading == true ? (
          <div className="tagged-value-wrapper">
            <CultureLoader />
          </div>
        ) : (
          this.renderCharts(
            stageList,
            this.props.NPS.topFunnel.funnel_nps,
            this.props.tooltip,
            TaggedValuesGraphDimention
          )
        )}
      </div>
    );
  }
}
function mapStateToProps(state) {
  return {
    NPS: state.NPSAnalytics,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    NPSAnalyticsActions: bindActionCreators(NPSAnalyticsActions, dispatch),
  };
}
let connection = connect(mapStateToProps, mapDispatchToProps);
module.exports = connection(NPSTopFunnel);
