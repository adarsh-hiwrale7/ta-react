
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import AnalyticsPopup from './AnalyticsPopup'
import SliderPopup from '../SliderPopup'
import * as analyticsActions from '../../actions/analytics/analyticsActions'

export default class AnalyticsPopupParent extends React.Component {
   componentDidMount(){
     if(this.props.dataType=='campaign'){
      this
      .props.props
      .analyticsActions
      .campaignAnalyticsPopup(this.props.selectedPopupData.identity);
     }
      // this
      // .props.props
      // .analyticsActions
      // .campaignAnalyticsPopup(this.props.selectedPopupData.identity);
   }
    closePopup(e){
        this.props.closePopup(e)
    }
    render(){
        const {selectedPopupData ,location,dataType} = this.props
        return(
            <SliderPopup className='analyticPopup'>
            <button id='close-popup' className='btn-default'>
              <a onClick={this.closePopup.bind(this)}>
                <i className='material-icons'>clear</i>
              </a>
            </button>

            <AnalyticsPopup
              popupData={selectedPopupData}
              location = {location}
              dataType = {dataType}
              props = {this.props.props}
            />

          </SliderPopup>

        )
    }
}
function mapStateToProps (state) {
    return {
        analytics: state.analytics,
    }
  }
  function mapDispatchToProps (dispatch) {
    return {
      analyticsActions: bindActionCreators(analyticsActions, dispatch),
    }
  }
  let connection = connect(mapStateToProps, mapDispatchToProps)
  var reduxConnectedComponent = connection(AnalyticsPopupParent)
