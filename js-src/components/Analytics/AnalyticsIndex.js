import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import PreLoader from '../PreLoader'
import Header from '../Header/Header';
import Globals from '../../Globals';
import * as analyticsActions from '../../actions/analytics/analyticsActions'
import AnalyticsDashboardIndex from './AnalyticsDashboard/AnalyticsDashboardIndex'
import * as utils from '../../utils/utils'

class AnalyticsIndex extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
    }
  }

  
  render() {
    return (
      <section id="analytics-Main-container" className="analytics-wrapper">
        <Header
          title={'Analytics'}
          location={this.props.location}
        />
        <div className='main-container'>
          <div id="content">
            <div className="page campaign-container">
              <AnalyticsDashboardIndex analyticsProps={this.props.analytics} location={this.props.location}/>
            </div>
          </div>
        </div>
      </section>
    );
  }

}

function mapStateToProps(state) {
  return {
    analytics: state.analytics,
  }
}

function mapDispatchToProps(dispatch) {
  return {
    analyticsActions: bindActionCreators(analyticsActions, dispatch),
  }
}

let connection = connect(mapStateToProps, mapDispatchToProps);

module.exports = connection(AnalyticsIndex);
