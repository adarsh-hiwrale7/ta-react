import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import moment from '../../Chunks/ChunkMoment';
import * as analyticsActions from '../../../actions/analytics/analyticsActions';
import AnalyticsAdoptionIndex from './Adoption/AnalyticsAdoptionIndex';
import * as utils from '../../../utils/utils'
import recharts from '../../Chunks/ChunkRecharts';
import reactTooltip from '../../Chunks/ChunkReactTooltip';
import AnalyticsNav from '../AnalyticsNav'
import Header from '../../Header/Header';
import * as companyInfo from '../../../actions/settings/companyInfo';
import * as countryActions from '../../../actions/countryActions';

import AnalyticsPopupParent from '../AnalyticsPopupParent'
import GuestUserRestrictionPopup from '../../GuestUserRestrictionPopup';
import GuestUserAnalytics from './Adoption/GuestUserAnalytics';
var currFrom = ''
class AnalyticsDashboardIndex extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            Recharts: null,
            moment: null,
            tooltip: null,
            refreshChart: false,
            side_popup: false,
            selectedPopupData: null,
            period: 'month',
            isFilterClicked: false
        }
    }
    componentWillMount() {
        window.scrollTo(0,0);
        moment().then(moment => {
            recharts().then(recharts => {
                reactTooltip().then(reactTooltip => {
                    this.setState({ moment: moment, Recharts: recharts, tooltip: reactTooltip })
                })
            })
        })
    }
    componentDidMount() {
        let roleName =JSON.parse(localStorage.getItem('roles')) !== null? JSON.parse(localStorage.getItem('roles'))[0].role_name:''
        if(roleName!=='guest'){
            var e = this;
        window.addEventListener("resize", function () {
            e.setState({ refreshChart: !e.state.refreshChart });
        }, true);
        var timezone = utils.clientTimezone();
        var ObjToSend = {
            'timezone': timezone,
            timespan: 'month'
        }
        this.props.analyticsActions.fetchAdoptionData(ObjToSend)
        this.props.analyticsActions.fetchGuestUserChart(ObjToSend)
        this.props.analyticsActions.fetchGuestUserActivity(ObjToSend)
        this.props.companyInfoAction.fetchRegion();
        this.props.countryAction.fetchAllCountry();


        var me = this
        window.addEventListener('click', function (event) {
            if (event.target.innerText !== 'filter_list') {
                if (!event.target.closest('.filter-tag-child') && !event.target.closest('.filter-tag-parent')) {
                    if(currFrom!==''){
                        me.setState({
                            isFilterClicked: {...me.state.isFilterClicked, [currFrom]: false }
                        })
                    }
                }
            }
        })
        this.props.analyticsActions.resetAllFilterValues()
        }
    }
    customizeTooltip = (date) => {
        return (
            this.state.moment ? this.state.moment.unix(date).format('DD-MM-YYYY') : ''
        )
    }
    convertTimeStampToDate = (date, props) => {
        var period = props ? props.period : 'month'
        var dateFormat = period !== undefined ? period : 'month'
        var newFormat = ''
        if (dateFormat == 'month' || dateFormat == 'week') {
            newFormat = 'DD MMM'
        } else if (dateFormat == 'year') {
            newFormat = 'MMM YY'
        }
        return (
            this.state.moment ? this.state.moment.unix(date).format(newFormat) : ''
        )
    }
    closePopup() {
        this.setState({
            side_popup: false
        })
        document.body.classList.remove('overlay')
    }
    popupanylitcs(data) {
        document.body.classList.add('overlay');
        this.setState({
            side_popup: true,
            selectedPopupData: data
        })
    }
    selectedTimeSpan(from, time) {
        var timezone = utils.clientTimezone();
        var ObjToSend = {
            'timezone': timezone,
            timespan: time
        }
        if (this.state[`filterData-${from}`] !== undefined) {
            Object.assign(this.state[`filterData-${from}`], ObjToSend)
        }
        var dataToPass = this.state[`filterData-${from}`] !== undefined && Object.keys(this.state[`filterData-${from}`]).length > 0 ? this.state[`filterData-${from}`] : ObjToSend
        this.setState({
            period: { ...this.state.period, [from]: time }
        })
        switch (from) {
            case 'adoption':
                this.props.analyticsActions.fetchAdoptionData(dataToPass)
                break;
            case 'guestDonut':
                this.props.analyticsActions.fetchGuestUserChart(dataToPass)
                break;
            default:
                break;
        }
    }
    fetchFilterData(data, from) {
        this.setState({
            [`filterData-${from}`]: data
        })
    }
    openFilterPopup(from, status) {
        currFrom = from
        this.props.analyticsActions.resetSelectedFilterValues(from.replace(/Filter/g, ""))
        var me=this
        Object.keys(me.state.isFilterClicked).forEach(function(key) {
            if (me.state.isFilterClicked[key]!== undefined && me.state.isFilterClicked[key] ==true) {
                if(key !== from){
                    me.state.isFilterClicked[key] = false;
                }
            }
          })
        me.setState({
            isFilterClicked: {...me.state.isFilterClicked, [from]: !me.state.isFilterClicked[from] }
        })
    }
    render() {
        return (
            <section id="analytics-Main-container" className="analytics-wrapper">
                <GuestUserRestrictionPopup setOverlay={true}/>
                {this.state.side_popup ?
                    <AnalyticsPopupParent
                        selectedPopupData={this.state.selectedPopupData}
                        location='/analytics/feed'
                        dataType='post'
                        props={this.props}
                        closePopup={this.closePopup.bind(this)}
                    />
                    : ''}
                <Header
                    title={'Analytics'}
                    location={this.props.location}
                />
                <div className='main-container'>
                    <div id="content">
                        <div>
                                <div>
                                    <div className="culture-dashboard-tab-responsive clearfix culture-score-tab">
                                        <AnalyticsNav location={this.props.location} />
                                    </div>

                                        <div className="chart-container-in-analytics-wrapper dashboard-analytics-level1-container">
                                            <div className="inner-chart-container-in-analytics-wrapper">
                                                <div className="row-container-of-the-chart-analytics">
                                                    <AnalyticsAdoptionIndex
                                                        parentState={this.state}
                                                        fetchFilterData={this.fetchFilterData.bind(this)}
                                                        isFilterClicked={this.state.isFilterClicked['adoptionFilter']}
                                                        openFilterPopup={this.openFilterPopup.bind(this, 'adoptionFilter', true)}
                                                        period={this.state.period['adoption']}
                                                        tooltip={this.state.tooltip}
                                                        customizeTooltip={this.customizeTooltip.bind(this)}
                                                        convertTimeStampToDate={this.convertTimeStampToDate.bind(this)}
                                                        analyticsProps={this.props.analytics.adoptionAnalytics}
                                                        Recharts={this.state.Recharts}
                                                        refreshChart={this.state.refreshChart}
                                                        adoptionTimeSpan={this.selectedTimeSpan.bind(this, 'adoption')} />

                                                        <GuestUserAnalytics
                                                         tooltip={this.state.tooltip}
                                                         analyticsProps={this.props.analytics}
                                                         Recharts={this.state.Recharts}
                                                         guestPeriod={this.state.period['guestDonut']}
                                                         guestTimeSpan={this.selectedTimeSpan.bind(this, 'guestDonut')}/>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                        </div>
                    </div>
                </div>
            </section>

        );
    }

}

function mapStateToProps(state) {
    return {
        analytics: state.analytics,

    }
}

function mapDispatchToProps(dispatch) {
    return {
        analyticsActions: bindActionCreators(analyticsActions, dispatch),
        companyInfoAction : bindActionCreators(companyInfo,dispatch),
        countryAction : bindActionCreators(countryActions,dispatch)


    }
}

let connection = connect(mapStateToProps, mapDispatchToProps);

module.exports = connection(AnalyticsDashboardIndex);
