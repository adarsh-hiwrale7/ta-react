import { connect } from 'react-redux'
import PreLoader from '../../../PreLoader'
import reactTagCloud from '../../../Chunks/ChunkTagCloud'
import AnalyticsDashboardFilter from '../AnalyticsDashboardFilter'
const COLORS = ['#4bcf99', '#ff595e', '#4b74e0', '#ffbf00', '#b87d7a', '#FF8042','#495867','#7FB069','#B8D8D8','#A93F55','#577399','#F9ADA0',"#ACF39D",'#19323C'];
class TagCloudIndex extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      tagcloud:null
    }
  }
  /**
  * Warning: This lifecycle is currently deprecated, and will be removed in React version 17+
  More details here: https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html
  */
  componentWillMount() {
    reactTagCloud().then(tagcloud => {
        this.setState({ tagcloud: tagcloud })
    })
  }
  fontSizeMapper(word) {
    return (Math.log(word) / Math.log(2)) + 15
  }
  renderLoader() {
    return (
      <div className='inner-TagCloud-index-container-analytics'>
          <div className="header-analytics-wrapper clearfix header-loader-wrapper-analytics ">
            <div className="title loader-grey-line  loader-line-radius loader-line-height"></div>

          </div>
          <div class="body-TagCloud-analytics">
            <div class="Tag-anaylicts-body-wrapper">
             <div class = "TagCloud-index-container-analytics-loader">
              <div class="app-outer">
                <div class="app-inner">
                  <div class="tag-cloud loader-grey-line">
                  </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          </div>
    )
  }
  
  // shouldComponentUpdate(nextProps, nextState) {   
  //     return nextProps.analyticsProps.tagCloudAnalytics != this.props.analyticsProps.tagCloudAnalytics;
  // }

  render() {
    var tagCloudData =
      Object.keys(this.props.analyticsProps.tagCloudAnalytics).length > 0
        ? this.props.analyticsProps.tagCloudAnalytics.tagCloudData !== undefined ?
          Object.keys(this.props.analyticsProps.tagCloudAnalytics.tagCloudData).length > 0
            ? this.props.analyticsProps.tagCloudAnalytics.tagCloudData
            : '' : ''
        : ''
    var tagCloudLoader = Object.keys(this.props.analyticsProps.tagCloudAnalytics).length > 0 ? this.props.analyticsProps.tagCloudAnalytics.tagCloudLoader : ''
    return (
      <div className="TagCloud-index-container-analytics">
      {tagCloudLoader == false ?
        <div className="inner-TagCloud-index-container-analytics">
            <div className="header-analytics-wrapper clearfix">
              <div className="title"> Tagcloud 
              <span className="svg-icon help-element"><svg xmlns="http://www.w3.org/2000/svg" data-tip  data-for='tagcloud' width="24" height="24" viewBox="0 0 24 24"><path d="M0 0h24v24H0z" fill="none"/><path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm1 17h-2v-2h2v2zm2.07-7.75l-.9.92C13.45 12.9 13 13.5 13 15h-2v-.5c0-1.1.45-2.1 1.17-2.83l1.24-1.26c.37-.36.59-.86.59-1.41 0-1.1-.9-2-2-2s-2 .9-2 2H8c0-2.21 1.79-4 4-4s4 1.79 4 4c0 .88-.36 1.68-.93 2.25z"/></svg></span>
              </div>
              {this.props.tooltip !== null ?
                <this.props.tooltip place="right" type="light" effect='solid' id='tagcloud' delayHide={100} delayUpdate={100}>
                  <div className="tooltip-wrapper">
                    <div className="inner-tooltip-wrapper">
                      <div className="tooltip-header clearfix">
                        <div className="analytics-tooltip-title">
                          <span>Tagcloud</span>
                        </div>

                      </div>
                      <div className="tooltip-about-wrapper">
                        <span>ABOUT</span>
                        <p>Breakdown of the most used tags within the Visibly platform.</p>
                      </div>
                    </div>
                  </div>
                </this.props.tooltip>
                : ''
              }
              <div className="filter-weekey-container clearfix">
              <div className = "filter-container">
              <a className={`week-filter ${this.props.period=='week'?'active':''}`} onClick={this.props.tagCloudTimeSpan.bind(this,'week')}> Week </a>
                <a className={`month-filter ${this.props.period== undefined || this.props.period=='month'?'active':''}`} onClick={this.props.tagCloudTimeSpan.bind(this,'month')}>  Month  </a>
                <a className={`year-filter ${this.props.period=='year'?'active':''}`} onClick={this.props.tagCloudTimeSpan.bind(this,'year')}>Year </a>
                <span className={`filterIcon ${this.props.parentState['filterData-tagcloud']!== undefined && Object.keys(this.props.parentState['filterData-tagcloud']).length>2? 'active':'' }`} onClick={this.props.openFilterPopup.bind(this)}> <i class="material-icons">filter_list</i>  </span>
              </div>
              {this.props.isFilterClicked==true ? <AnalyticsDashboardFilter fetchFilterData={this.props.fetchFilterData} includeLocation={true} callFrom='tagcloud' period={this.props.period}/>:''}
              </div>
            </div>
              <div className="body-TagCloud-analytics">
                <div className="Tag-anaylicts-body-wrapper">
                  <div className='app-outer'>
                    <div className='app-inner'>
                      {tagCloudData && this.state.tagcloud ?
                        <this.state.tagcloud.default
                          className='tag-cloud'
                          style={{
                            fontFamily: 'sans-serif',
                            fontSize: 30,
                            padding: 5
                          }}
                        >
                          {tagCloudData.map((tc, i) => {
                            return (
                              <div
                                key={i}
                                style={{
                                  fontFamily: 'Montserrat',
                                  fontSize: this.fontSizeMapper(tc.count),
                                  fontWeight: '500',
                                  color: COLORS[Math.floor(Math.random() * COLORS.length)],
                                }}
                              >
                                {tc.tag}
                              </div>
                            )
                          })}
                        </this.state.tagcloud.default> 
                        
                        : <div className = "no-Value-chart">No tag found</div>}
                    </div>
                  </div>
                </div>
              </div> 
            
        </div> : this.renderLoader()}
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {}
}

function mapDispatchToProps(dispatch) {
  return {}
}

let connection = connect(
  mapStateToProps,
  mapDispatchToProps
)

module.exports = connection(TagCloudIndex)