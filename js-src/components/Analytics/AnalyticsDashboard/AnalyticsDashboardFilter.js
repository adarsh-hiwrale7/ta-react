import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import PreLoader from '../../PreLoader'
import Globals from '../../../Globals';
import { Link } from 'react-router';

import moment from '../../Chunks/ChunkMoment';
import * as analyticsActions from '../../../actions/analytics/analyticsActions';
import * as utils from '../../../utils/utils'
import * as userActions from '../../../actions/userActions'
import * as countryActions from '../../../actions/countryActions';
import * as companyInfo from '../../../actions/settings/companyInfo';
import * as cultureAnalyticsActions from '../../../actions/analytics/cultureAnalyticsActions';
var color = ['#ff595e', '#256eff', '#ffbf00', '#2fbf71', '#e27c3f','#256eff'];
var currFilterValue = []
var search;
var Gender = ['Female', 'Male', 'Other']
var Age = ['15 - 25', '26 - 35', '36 - 45', '46 - 55', '56 - 65', '66 - 75', '76 - 85', '86 - 95']
class AnalyticsDashboardFilter extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            moment: null,
            selectedType: '',
            selectedFilterDetail: {},
            'User role': [],
            Department: [],
            Region: [],
            Country: [],
            Gender: [],
            Age: [],
            'Office location': [],
            'User role-search': null,
            'Department-search': null,
            'Gender-search': null,
            'Age-search': null,
            'Office location-search': null,
            'Country-search': null,
            'Region-search': null,
            currFilterValue: [],
            callFrom: this.props.callFrom
        }
    }
    componentWillMount() {
        moment().then(moment => {
            this.setState({ moment: moment })
        })
    }
renderInnerFilterLoader(){
    return(
        <div>
                                 <a className = "loader-line-radius loader-line-height loader-grey-line loader-tag-child-value-analytics"> </a>
                                <a className = "loader-line-radius loader-line-height loader-grey-line loader-tag-child-value-analytics"> </a>
                                <a className = "loader-line-radius loader-line-height loader-grey-line loader-tag-child-value-analytics"> </a>
                                <a className = "loader-line-radius loader-line-height loader-grey-line loader-tag-child-value-analytics"> </a>
                                <a className = "loader-line-radius loader-line-height loader-grey-line loader-tag-child-value-analytics"> </a>
                                <a className = "loader-line-radius loader-line-height loader-grey-line loader-tag-child-value-analytics"> </a>
                                <a className = "loader-line-radius loader-line-height loader-grey-line loader-tag-child-value-analytics"> </a>
                                <a className = "loader-line-radius loader-line-height loader-grey-line loader-tag-child-value-analytics"> </a> 
                                </div>
                            
    )
}
    selectedFilterType(name, color) {
        var obj = {
            'filterName': name,
            'color': color,
            'callFrom': this.state.callFrom
        }
        this.setState({
            selectedFilterDetail: obj,
        })
        this.props.analyticsActions.selectedFilterDetail(obj)
        switch (name) {
            case 'User role':
                if (this.props.roles.length == 0) {
                    this.props.userActions.fetchRoles();
                }
                break;
            case 'Office location':
                if (this.props.officeLocationData.officeLocations!== undefined && Object.keys(this.props.officeLocationData.officeLocations).length == 0){
                    this.props.companyActions.getOfficeLocation();
                }
                break;

            default:
                break;
        }
    }
    clearSelectedFilter(name){
        this.props.analyticsActions.clearSelectedFilterData({callFrom:this.state.callFrom,filtername:name})
        this.setFilterValuesInObj()
    }
    selectedFilterValue(selectedValue, selectedFrom) {
        if (this.state[selectedFrom].includes(selectedValue)) {
            this.state[selectedFrom].splice(this.state[selectedFrom].indexOf(selectedValue), 1)
            this.forceUpdate();
        } else {
            this.state[selectedFrom].push(selectedValue)
            this.props.analyticsActions.selectedFilterValues({ selectedFrom, value: selectedValue, callFrom: this.state.callFrom })
        }
        this.setFilterValuesInObj()
    }
    setFilterValuesInObj() {
        var timezone = utils.clientTimezone();
        var ObjToSend = {
            'timezone': timezone,
            'timespan': this.props.period == undefined ? 'month' : this.props.period,
        }
        if (this.props.analytics[`${this.state.callFrom}Filter`]["Department"].length > 0) {
            var ObjToAdd = {
                'department_identity': this.props.analytics[`${this.state.callFrom}Filter`]["Department"],
            }
            Object.assign(ObjToSend, ObjToAdd)
        }
        if (this.props.analytics[`${this.state.callFrom}Filter`]["Country"].length > 0) {
            var ObjToAdd = {
                'country': this.props.analytics[`${this.state.callFrom}Filter`]["Country"],
            }
            Object.assign(ObjToSend, ObjToAdd)
        }
        if (this.props.analytics[`${this.state.callFrom}Filter`]["Region"].length > 0) {
            var ObjToAdd = {
                'region': this.props.analytics[`${this.state.callFrom}Filter`]["Region"],
            }
            Object.assign(ObjToSend, ObjToAdd)
        }
        if (this.props.analytics[`${this.state.callFrom}Filter`]['User role'].length > 0) {
            var ObjToAdd = {
                'role_identity': this.props.analytics[`${this.state.callFrom}Filter`]['User role'],
            }
            Object.assign(ObjToSend, ObjToAdd)
        }
        if (this.props.analytics[`${this.state.callFrom}Filter`]['Age'].length > 0) {
            var ObjToAdd = {
                'age': this.props.analytics[`${this.state.callFrom}Filter`]['Age'],
            }
            Object.assign(ObjToSend, ObjToAdd)
        }
        if (this.props.analytics[`${this.state.callFrom}Filter`]['Gender'].length > 0) {
            var arr = []
            if (this.props.analytics[`${this.state.callFrom}Filter`]['Gender'].includes('Female')) {
                arr.push('f')
            }
            if (this.props.analytics[`${this.state.callFrom}Filter`]['Gender'].includes('Male')) {
                arr.push('m')
            }
            if (this.props.analytics[`${this.state.callFrom}Filter`]['Gender'].includes('Other')) {
                arr.push('o')
            }
            var ObjToAdd = {
                'gender': arr
            }
            Object.assign(ObjToSend, ObjToAdd)
        }
        if (this.props.analytics[`${this.state.callFrom}Filter`]['Office location'].length > 0) {
            var ObjToAdd = {
                'location_identity': this.props.analytics[`${this.state.callFrom}Filter`]['Office location'],
            }
            Object.assign(ObjToSend, ObjToAdd)
        }
        this.props.fetchFilterData(ObjToSend,this.state.callFrom)

        this.callFilterApi(ObjToSend)
    }
    callFilterApi(ObjToSend) {
        switch (this.state.callFrom) {
            case 'adoption':
                this.props.analyticsActions.fetchAdoptionData(ObjToSend)
                break;
            case 'totalpost':
                this.props.analyticsActions.fetchTotalPostsChartData(ObjToSend)
                break;
            case 'assets':
                this.props.analyticsActions.fetchAssetAnalyticsData(ObjToSend)
                break;
            case 'quality':
                this.props.analyticsActions.fetchQualityAnalyticsData(ObjToSend)
                break;
            case 'tagcloud':
                this.props.analyticsActions.fetchTagCloudData(ObjToSend)
                break;
            case 'totalSocial':
                this.props.analyticsActions.fetchInternalEngagementData(ObjToSend)
                this.props.analyticsActions.fetchTotalSocialEngagementData(ObjToSend)
                break;
            case 'byChannel':
                this.props.analyticsActions.fetchEngagementByChannel(ObjToSend)
                break;
            case 'overTime':
                this.props.analyticsActions.fetchEngagementOverTime(ObjToSend)
                break;
            case 'guestDonut':
                this.props.analyticsActions.fetchGuestUserChart(ObjToSend)
                break;
            case 'locationChart':
                this.props.analyticsActions.fetchLocationBaseAdoption(ObjToSend)
                break;
            case 'network':
                this.props.analyticsActions.getPostByNetwork(ObjToSend);
                break;
            case 'participation':
                this.props.cultureAnalyticsActions.getParticipationData(ObjToSend);
                break;
            case 'tagged':
                this.props.cultureAnalyticsActions.getTaggedValues(ObjToSend);
                break;
            case 'voted':
                this.props.cultureAnalyticsActions.getVotedValues(ObjToSend);
                break;
            default:
                break;
        }
    }
    renderInnerFilter(e) {
        var callFrom = this.state.callFrom
        var innerData = []
        var filtername = this.props.analytics[`${callFrom}Filter`].selectedFilterDetail.filterName
        var selectedFilterProps = this.props.analytics[`${callFrom}Filter`] && this.props.analytics[`${callFrom}Filter`][filtername] ? this.props.analytics[`${callFrom}Filter`][filtername] : ''
        var updateAtagStyle = {}
        var color = this.props.analytics[`${callFrom}Filter`].selectedFilterDetail.color
        var aTagStyle = {
            border: `1px solid ${color}`, color: color
        }
        switch (filtername) {
            case 'Department':

                var departmentList = this.state[`${filtername}-search`] !== null ? this.state[`${filtername}-search`] : this.props.departments.list
                if (this.props.departments !== undefined && this.props.departments.list !== undefined && this.props.departments.list.length > 0) {
                    currFilterValue = this.props.departments.list
                    if(departmentList.length>0){
                    departmentList.map((department, i) => {
                        if (selectedFilterProps !== '' && selectedFilterProps.includes(department.identity)) {
                            updateAtagStyle = {
                                background: color,
                                border: `1px solid ${color}`
                            }
                        }
                        innerData.push(<a className={`tag-title tag-border ${selectedFilterProps !== '' ? selectedFilterProps.includes(department.identity) ? 'selected' : '' : ''}`}
                            
                            id={department.identity}
                            onClick={this.selectedFilterValue.bind(this, department.identity, filtername)}
                            style={selectedFilterProps !== '' ? selectedFilterProps.includes(department.identity) ? updateAtagStyle : aTagStyle : aTagStyle}>
                            <span>{department.name} 
                                {department.office_name &&  department.office_name.trim()!=='' ?
                                    <span className="filter-office-location">
                                         {department.office_name}
                                    </span>:''
                                }
                            </span>
                        </a>)
                    })
                        }else{
                            innerData.push(<div className = "no-Value-chart">No department found</div>)
                        }
                    }
                
                
                break;
            case 'User role':
                var roleList = this.state[`${filtername}-search`] !== null ? this.state[`${filtername}-search`] : this.props.roles
                if (roleList !== undefined && roleList.length > 0) {
                    currFilterValue = this.props.roles
                    roleList.map((role, i) => {
                        if (selectedFilterProps !== '' && selectedFilterProps.includes(role.role_id)) {
                            updateAtagStyle = {
                                background: color,
                                border: `1px solid ${color}`
                            }
                        }
                        innerData.push(<a className={`tag-title tag-border ${selectedFilterProps.includes(role.role_id) ? 'selected' : ''}`}
                            id={role.role_id}
                            style={selectedFilterProps !== '' ? selectedFilterProps.includes(role.role_id) ? updateAtagStyle : aTagStyle : aTagStyle}
                            onClick={this.selectedFilterValue.bind(this, role.role_id, filtername)} >
                            <span>{role.role_display_name}</span>
                        </a>)
                    })
                }else{
                    innerData.push(<div className = "no-Value-chart">No role found</div>)
                }
                break;
            case 'Gender':
                currFilterValue = Gender
                var genderList = this.state[`${filtername}-search`] !== null ? this.state[`${filtername}-search`] : Gender
                if(genderList!== undefined && genderList.length>0){
                genderList.map((gender, i) => {
                    if (selectedFilterProps !== '' && selectedFilterProps.includes(gender)) {
                        updateAtagStyle = {
                            background: color,
                            border: `1px solid ${color}`
                        }
                    }
                    innerData.push(<a className={`tag-title tag-border ${selectedFilterProps.includes(gender) ? 'selected' : ''}`}
                        style={selectedFilterProps !== '' ? selectedFilterProps.includes(gender) ? updateAtagStyle : aTagStyle : aTagStyle}
                        onClick={this.selectedFilterValue.bind(this, gender, filtername)} >
                        <span>{gender}</span>
                    </a>)
                })
            }else{
                innerData.push(<div className = "no-Value-chart">No gender found</div>)
            }
                break;
            case 'Age':
                currFilterValue = Age
                var ageList = this.state[`${filtername}-search`] !== null ? this.state[`${filtername}-search`] : Age
                if(ageList!== undefined && ageList.length>0){
                ageList.map((age, i) => {
                    if (selectedFilterProps !== '' && selectedFilterProps.includes(age)) {
                        updateAtagStyle = {
                            background: color,
                            border: `1px solid ${color}`
                        }
                    }
                    innerData.push(<a className={`tag-title tag-border ${selectedFilterProps.includes(age) ? 'selected' : ''}`}
                        style={selectedFilterProps !== '' ? selectedFilterProps.includes(age) ? updateAtagStyle : aTagStyle : aTagStyle}
                        onClick={this.selectedFilterValue.bind(this, age, filtername)} >
                        <span>{age}</span>
                    </a>)
                })
            }else{
                innerData.push(<div className = "no-Value-chart">No age found</div>)
            }

                break;
            case 'Office location':
                var locationList = this.state[`${filtername}-search`] !== null ? this.state[`${filtername}-search`] : this.props.officeLocationData.officeLocations.data
                if (this.props.officeLocationData !== undefined && this.props.officeLocationData.officeLocations!== undefined && this.props.officeLocationData.officeLocations.data.length > 0) {
                    currFilterValue = this.props.officeLocationData.officeLocations.data
                    locationList.map((location, i) => {
                        if (selectedFilterProps !== '' && selectedFilterProps.includes(location.identity)) {
                            updateAtagStyle = {
                                background: color,
                                border: `1px solid ${color}`
                            }
                        }
                        innerData.push(<a className={`tag-title tag-border ${selectedFilterProps !== '' ? selectedFilterProps.includes(location.identity) ? 'selected' : '' : ''}`}
                            id={location.identity}
                            onClick={this.selectedFilterValue.bind(this, location.identity, filtername)}
                            style={selectedFilterProps !== '' ? selectedFilterProps.includes(location.identity) ? updateAtagStyle : aTagStyle : aTagStyle}>
                            <span>{location.name}</span>
                        </a>)
                    })
                }else{
                    innerData.push(<div className = "no-Value-chart">No location found</div>)
                }
                break;
                case 'Country':
                    var countryList = this.state[`${filtername}-search`] !== null ? this.state[`${filtername}-search`] : this.props.country.allcountry
                    if (this.props.country !== undefined && this.props.country.allcountry !== undefined && this.props.country.allcountry.length > 0) {
                        currFilterValue = this.props.country.allcountry
                        if(countryList.length>0){
                        countryList.map((country, i) => {
                            if (selectedFilterProps !== '' && selectedFilterProps.includes(country.identity)) {
                                updateAtagStyle = {
                                    background: color,
                                    border: `1px solid ${color}`
                                }
                            }
                            innerData.push(<a className={`tag-title tag-border ${selectedFilterProps !== '' ? selectedFilterProps.includes(country.identity) ? 'selected' : '' : ''}`}
                                
                                id={country.identity}
                                key={country.identity}
                                onClick={this.selectedFilterValue.bind(this, country.identity, filtername)}
                                style={selectedFilterProps !== '' ? selectedFilterProps.includes(country.identity) ? updateAtagStyle : aTagStyle : aTagStyle}>
                                <span>{country.title} 
                                    {country.initial &&  country.initial.trim()!=='' ?
                                        <span className="filter-office-location">
                                             {country.initial}
                                        </span>:''
                                    }
                                </span>
                            </a>)
                        })
                            }else{
                                innerData.push(<div className = "no-Value-chart">No department found</div>)
                            }
                        }
                break;
                case 'Region':
                    var regionList = this.state[`${filtername}-search`] !== null ? this.state[`${filtername}-search`] : this.props.officeLocationData.companyHierarchyData.regions
                    if (this.props.officeLocationData !== undefined && this.props.officeLocationData.companyHierarchyData.regions !== undefined && this.props.officeLocationData.companyHierarchyData.regions.length > 0) {
                        currFilterValue = this.props.officeLocationData.companyHierarchyData.regions
                        if(regionList.length>0){
                        regionList.map((region, i) => {
                            if (selectedFilterProps !== '' && selectedFilterProps.includes(region.identity)) {
                                updateAtagStyle = {
                                    background: color,
                                    border: `1px solid ${color}`
                                }
                            }
                            innerData.push(<a className={`tag-title tag-border ${selectedFilterProps !== '' ? selectedFilterProps.includes(region.identity) ? 'selected' : '' : ''}`}
                                
                                id={region.identity}
                                key={region.identity}
                                onClick={this.selectedFilterValue.bind(this, region.identity, filtername)}
                                style={selectedFilterProps !== '' ? selectedFilterProps.includes(region.identity) ? updateAtagStyle : aTagStyle : aTagStyle}>
                                <span>{region.name} 
                                    {/* {department.office_name &&  department.office_name.trim()!=='' ?
                                        <span className="filter-office-location">
                                             {department.office_name}
                                        </span>:''
                                    } */}
                                </span>
                            </a>)
                        })
                            }else{
                                innerData.push(<div className = "no-Value-chart">No region found</div>)
                            }
                        }
                break;

            default:
                break;
        }
        return innerData;

    }
    backFromInnerFilter() {
        this.setState({
            [`${this.state.selectedFilterDetail.filterName}-search`]: null,
        })
        this.props.analyticsActions.resetSelectedFilterValues(this.state.callFrom)

    }
    handleSearchValue(e) {
        if (e !== null) {
            if (e.target.value !== "" && currFilterValue.length > 0) {
                var i = 0;
                search = e.target.value.toLowerCase();
                var tempobj = currFilterValue.filter(function (temp) {
                    i = i + 1;
                    var name = temp.name !== undefined ? temp.name : temp.role_display_name !== undefined ? temp.role_display_name : temp.office_location !== undefined ? temp.office_location :temp
                    if (name.toLowerCase().includes(search)) {
                        return true;
                    }
                })
                this.setState({
                    [`${this.props.analytics[`${this.state.callFrom}Filter`].selectedFilterDetail.filterName}-search`]: tempobj
                })
            }
            else {
                this.setState({
                    [`${this.props.analytics[`${this.state.callFrom}Filter`].selectedFilterDetail.filterName}-search`]: currFilterValue
                })
            }
        }
    }
    render() {
        var filterList = []
        var callFrom = this.props.callFrom
        var includeLocation = this.props.includeLocation
        if (includeLocation !== undefined && includeLocation == false) {
            filterList = ['Department', 'User role', 'Gender', 'Age','Country','Region']
        } else {
            filterList = ['Department', 'User role', 'Gender', 'Age', 'Office location','Country','Region']
        }
        return (
            
            <section id="analytics-Main-container" className="analytics-wrapper">
                <div className="filter-analytics">
                    <div className="inner-filter-analytics" >
                        <div className={`main-filter ${this.props.analytics[`${callFrom}Filter`] !== undefined && Object.keys(this.props.analytics[`${callFrom}Filter`].selectedFilterDetail).length > 0 ? 'slider-child-filter' : ''}`} >
                            <div className="main-filter-container clearfix" id='main-filter'>
                                <div className="filter-tag-parent">
                                    <div className="tag-container">
                                        {filterList.map((list, i) => {
                                            return (
                                               
                                                <a key={i} className="tag-title" style={{ background: color[i] }} >
                                                    <span onClick={this.selectedFilterType.bind(this, list, color[i])}> {list} {`${this.props.analytics[`${callFrom}Filter`] !== undefined && this.props.analytics[`${callFrom}Filter`][list] !== undefined ? this.props.analytics[`${callFrom}Filter`][list].length>0? `(${this.props.analytics[`${callFrom}Filter`][list].length})`:'':''}`}  </span> 
                                                    <span className = "close-icon-filter">  <i className='material-icons' onClick={this.clearSelectedFilter.bind(this,list)}>clear</i></span>
                                                </a>
                                               
                                               
                                            )
                                        })}
                                    </div>
                                </div>
                                {this.props.analytics[`${callFrom}Filter`] !== undefined && Object.keys(this.props.analytics[`${callFrom}Filter`].selectedFilterDetail).length > 0 ?
                                    <div className="filter-tag-child">
                                        <div className="inner-filter-tag-child">
                                            <div className="search-header">
                                                <div className="search-box-container">
                                                    <input
                                                        type='text'
                                                        name='filter_search'
                                                        autoCapitalize='none'
                                                        autoComplete='off'
                                                        autoCorrect='off'
                                                        placeholder='Search'
                                                        className='search_values'
                                                        onChange={this.handleSearchValue.bind(this)} />
                                                    <a className="search-icon"><span> <i class="material-icons">search</i></span></a>
                                                    <a className="back-icon" onClick={this.backFromInnerFilter.bind(this)}><span> <i class="material-icons">arrow_back</i></span></a>
                                                </div>

                                            </div>

                                            <div className="child-tag-container">
                                                <div className="tag-container">
                                                {this.props.analytics[`${callFrom}Filter`].selectedFilterDetail.filterName =='User role' && this.props.rolesLoader ==true ?
                                                    this.renderInnerFilterLoader()
                                                : this.props.analytics[`${callFrom}Filter`].selectedFilterDetail.filterName == 'Office location' && this.props.officeLocationData.companyLocationLoader ==true ?
                                                    this.renderInnerFilterLoader():this.renderInnerFilter(this)}
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                    : ''}
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        );
    }

}

function mapStateToProps(state) {
    return {
        analytics: state.analytics,
        departments: state.departments,
        roles: state.users.roles,
        rolesLoader: state.users.rolesLoader,
        companyData:state.companyData,
        culture: state.cultureAnalytics,
        officeLocationData: state.settings,
        country:state.allcountry,


    }
}

function mapDispatchToProps(dispatch) {
    return {
        userActions: bindActionCreators(userActions, dispatch),
        analyticsActions: bindActionCreators(analyticsActions, dispatch),
        companyActions: bindActionCreators(companyInfo, dispatch),
        cultureAnalyticsActions: bindActionCreators(cultureAnalyticsActions, dispatch),
        countryAction : bindActionCreators(countryActions,dispatch)

    }
}

let connection = connect(mapStateToProps, mapDispatchToProps);

module.exports = connection(AnalyticsDashboardFilter);
