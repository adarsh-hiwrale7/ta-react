import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as utils from '../../../../utils/utils';
import Header from '../../../Header/Header';
import moment from '../../../Chunks/ChunkMoment';
import * as analyticsActions from '../../../../actions/analytics/analyticsActions';
import * as cultureAnalyticsActions from '../../../../actions/analytics/cultureAnalyticsActions';
import recharts from '../../../Chunks/ChunkRecharts';
import reactTooltip from '../../../Chunks/ChunkReactTooltip';
import AnalyticsNav from '../../AnalyticsNav';
import GuestUserRestrictionPopup from '../../../GuestUserRestrictionPopup';
import TotalSocialEngagement from '../SocialEngagement/TotalSocialEngagement';
import EngagementByChannel from '../SocialEngagement/EngagementByChannel';
import EngagementOverTime from '../SocialEngagement/EngagementOverTime';

var currFrom = ''

class EngagmentIndex extends React.Component {
  constructor(props){
    super(props);
    this.state={
      period: 'month',
      isFilterClicked: false,
      Recharts: null,
      moment: null,
      tooltip: null,
      refreshChart: false,
    };
  }

  componentWillMount() {
    window.scrollTo(0,0);
    moment().then(moment => {
        recharts().then(recharts => {
            reactTooltip().then(reactTooltip => {
                this.setState({ moment: moment, Recharts: recharts, tooltip: reactTooltip })
            })
        })
    })
}

  componentDidMount(){
    let roleName =JSON.parse(localStorage.getItem('roles')) !== null? JSON.parse(localStorage.getItem('roles'))[0].role_name:''
        if(roleName!=='guest'){
          var e = this;
          window.addEventListener("resize", function () {
              e.setState({ refreshChart: !e.state.refreshChart });
          }, true);
          var timezone = utils.clientTimezone();
          var ObjToSend = {
              'timezone': timezone,
              timespan: 'month'
          }
          this.props.analyticsActions.fetchInternalEngagementData(ObjToSend);
          this.props.analyticsActions.fetchTotalSocialEngagementData(ObjToSend);
          var me = this
              window.addEventListener('click', function (event) {
                  if (event.target.innerText !== 'filter_list') {
                      if (!event.target.closest('.filter-tag-child') && !event.target.closest('.filter-tag-parent')) {
                          if(currFrom!==''){
                              me.setState({
                                  isFilterClicked: {...me.state.isFilterClicked, [currFrom]: false }
                              })
                          }
                      }
                  }
              })
              this.props.analyticsActions.resetAllFilterValues()
      }
  }


  customizeTooltip = (date) => {
      return (
          this.state.moment ? this.state.moment.unix(date).format('DD-MM-YYYY') : ''
      )
  }

  fetchFilterData(data, from) {
    this.setState({
        [`filterData-${from}`]: data
    })
  }
  openFilterPopup(from, status) {
      currFrom = from
      this.props.analyticsActions.resetSelectedFilterValues(from.replace(/Filter/g, ""))
      var me=this
      Object.keys(me.state.isFilterClicked).forEach(function(key) {
          if (me.state.isFilterClicked[key]!== undefined && me.state.isFilterClicked[key] ==true) {
              if(key !== from){
                  me.state.isFilterClicked[key] = false;
              }
          }
        })
      me.setState({
          isFilterClicked: {...me.state.isFilterClicked, [from]: !me.state.isFilterClicked[from] }
      })
  }

  convertTimeStampToDate = (date, props) => {
    var period = props ? props.period : 'month'
    var dateFormat = period !== undefined ? period : 'month'
    var newFormat = ''
    if (dateFormat == 'month' || dateFormat == 'week') {
        newFormat = 'DD MMM'
    } else if (dateFormat == 'year') {
        newFormat = 'MMM YY'
    }
    return (
        this.state.moment ? this.state.moment.unix(date).format(newFormat) : ''
    )
}

  selectedTimeSpan(from, time) {
      var timezone = utils.clientTimezone();
      var ObjToSend = {
          'timezone': timezone,
          timespan: time
      }
      if (this.state[`filterData-${from}`] !== undefined) {
          Object.assign(this.state[`filterData-${from}`], ObjToSend)
      }
      var dataToPass = this.state[`filterData-${from}`] !== undefined && Object.keys(this.state[`filterData-${from}`]).length > 0 ? this.state[`filterData-${from}`] : ObjToSend
      this.setState({
          period: { ...this.state.period, [from]: time }
      })
      switch (from) {
          case 'totalSocial':
              this.props.analyticsActions.fetchInternalEngagementData(dataToPass)
              this.props.analyticsActions.fetchTotalSocialEngagementData(dataToPass)
              break;
          case 'byChannel':
              this.props.analyticsActions.fetchEngagementByChannel(dataToPass)
              break;
          case 'overTime':
              this.props.analyticsActions.fetchEngagementOverTime(dataToPass)
              break;
          default:
              break;
      }
  }
  render(){
    return(
      <section id="analytics-Main-container">
      <GuestUserRestrictionPopup setOverlay={true}/>
      <Header
          title={'Analytics'}
          location={this.props.location}
      />
      <div className='main-container'>
          <div id="content">
            <div className="culture-dashboard-tab-responsive culture-score-tab clearfix">
                <AnalyticsNav location={this.props.location} />
            </div>
            <div className = "Total-Social-Engagement-Wrapper">
              <div className="Total-Social-engagement">
                  <TotalSocialEngagement
                      parentState={this.state}
                      fetchFilterData={this.fetchFilterData.bind(this)}
                      isFilterClicked={this.state.isFilterClicked['totalSocialFilter']}
                      openFilterPopup={this.openFilterPopup.bind(this, 'totalSocialFilter', true)}
                      period={this.state.period['totalSocial']}
                      totalSocialTimeSpan={this.selectedTimeSpan.bind(this, 'totalSocial')}
                      tooltip={this.state.tooltip}
                      analyticsProps={this.props.analytics} />
              </div>
            </div>
            <div className="Expand-design-wrapper">
                    <div className="chart-container-in-analytics-wrapper ">
                        <div className="inner-chart-container-in-analytics-wrapper">
                            <div className="row-container-of-the-chart-analytics .row-container-of-the-chart-analytics-loader-totalpost">
                                <EngagementByChannel
                                    parentState={this.state}
                                    fetchFilterData={this.fetchFilterData.bind(this)}
                                    isFilterClicked={this.state.isFilterClicked['byChannelFilter']}
                                    openFilterPopup={this.openFilterPopup.bind(this, 'byChannelFilter', true)}
                                    channelPeriod={this.state.period['byChannel']}
                                    engagementChannelTimespan={this.selectedTimeSpan.bind(this, 'byChannel')}
                                    analyticsProps={this.props.analytics}
                                    analyticsActions={this.props.analyticsActions}
                                    tooltip={this.state.tooltip}
                                    refreshChart={this.state.refreshChart}
                                    Recharts={this.state.Recharts}
                                    customizeTooltip={this.customizeTooltip.bind(this)}
                                    convertTimeStampToDate={this.convertTimeStampToDate.bind(this)} />
                                <EngagementOverTime
                                    parentState={this.state}
                                    fetchFilterData={this.fetchFilterData.bind(this)}
                                    openFilterPopup={this.openFilterPopup.bind(this, 'overTimeFilter', true)}
                                    isFilterClicked={this.state.isFilterClicked['overTimeFilter']}
                                    overTimePeriod={this.state.period['overTime']}
                                    engagementOverTimespan={this.selectedTimeSpan.bind(this, 'overTime')}
                                    analyticsProps={this.props.analytics}
                                    tooltip={this.state.tooltip}
                                    analyticsActions={this.props.analyticsActions}
                                    Recharts={this.state.Recharts}
                                    customizeTooltip={this.customizeTooltip.bind(this)}
                                    convertTimeStampToDate={this.convertTimeStampToDate.bind(this)} />
                            </div>
                        </div>
                    </div>
                </div>
          </div>
      </div>
      </section>
    )
  }
}
function mapStateToProps(state) {
  return {
      culture: state.cultureAnalytics,
      analytics: state.analytics,
  }
}
function mapDispatchToProps(dispatch) {
  return {
      cultureAnalyticsActions: bindActionCreators(cultureAnalyticsActions, dispatch),
      analyticsActions: bindActionCreators(analyticsActions, dispatch),
  }
}
let connection = connect(mapStateToProps, mapDispatchToProps);
module.exports = connection(EngagmentIndex);