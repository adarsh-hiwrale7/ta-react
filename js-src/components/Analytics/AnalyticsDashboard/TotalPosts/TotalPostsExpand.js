import { connect } from 'react-redux'
import PreLoader from '../../../PreLoader'
import Globals from '../../../../Globals'
import * as utils from '../../../../utils/utils'
import AnalyticsTable from '../../AnalyticsTable'
import AnalyticsDashboardFilter from '../AnalyticsDashboardFilter'
const COLORS = ['#3b5998', '#1da1f2', '#007bb5', '#ff595e']
class TotalPostsExpand extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
          pageno: null,
          hideChart:false,
          period:'month',
          timezone: null,
        }
    }
    componentDidMount(){
        
        var timezone = utils.clientTimezone();
        this.setState({timezone: timezone},() => this.fetchFeedData('month'))
        var ObjToSend = {
            'timezone': timezone,
            timespan:'month'
        }
        this.props.analyticsActions.getPostByNetwork(ObjToSend);

    }

    customizeTooltipTotalPost = props => {
        const { payload } = props
        var chartDate=0
        if (payload.length > 0) {
            chartDate=payload[0].payload.date
        }
        return(
            <div> 
                <div className='chartDate'>
                    <div className='label'>
                       {this.props.customizeTooltip(chartDate)}
                    </div> 
                </div>
                <div className="internal-external-post-wrapper">
            {payload.length > 0
            ? payload.map((entry, i) => {
                return (
                    this.state.hideChart[entry.dataKey] !== undefined && this.state.hideChart[entry.dataKey] == true && (entry.dataKey == 'Active users' || entry.dataKey == 'Non-active users') ? '' :
                        <div className='custom-tooltip' key={i}  >
                                <div className='label' style={{color:entry.color}}>{`${entry.dataKey} :${entry.value}`}</div>
                        </div>
                )
            })
            : ''}
            </div>
            </div>
        )
    }

    selectedTimeSpan(time){
                var currProps=this.props
                var ObjToSend = {
                    'timezone': this.state.timezone,
                    timespan:time
                }
                this.setState({
                    period: time
                })

                this.props.analyticsActions.getPostByNetwork(ObjToSend)   
                this.fetchFeedData(time)
    }

    rearrangeAnalytics(type,chartData,actionType=null) {
        let chart_data = [];
        if (chartData !== '') {
          if (type == 'month') {
            chart_data = chartData.slice(chartData.length - 30)
            this.props.analyticsActions[actionType]({chart_data})
          } else if (type == 'week') {
            chart_data = chartData.slice(chartData.length - 7)
            this.props.analyticsActions[actionType]({chart_data})
          } else {
            chart_data = chartData.slice()
            this.props.analyticsActions[actionType]({chart_data})
          }
        }
      }
    popupanylitcs(data){
        this.props.popupanylitcs(data);
    }
   
    UpdatedLegend = (props) => {
        const { payload, countData } = props;
        if(payload.length>0){
            var LegendKeys = Object.keys(payload)
            return (
              <ul>
                {payload.length > 0 ? payload.map((entry, index) => (
                      <li className="clearfix" key={index} onClick={this.hideSelectedLegend.bind(this, entry.dataKey)} >
                       <span className="rounder-dot" style={{ background: entry.color }}></span>
                        <span className={`title-active-user ${this.state.hideChart[entry.dataKey] !== undefined && this.state.hideChart[entry.dataKey] == true ? 'strikeLegend' : ''}`}> {entry.dataKey}  </span> <span className="text-no">{countData[entry.dataKey]}</span>  
                      </li>
                )) : ''}
              </ul>
            ); 
        }
    }
    fetchFeedData(timespan){
        
        var allfeeddata = { 
            'timezone': this.state.timezone,
             pageno: this.state.pageno ? this.state.pageno : '',
             timespan: timespan
        }
        this.props.analyticsActions.fetchFeedAnalytics(allfeeddata)
    }
    totalPostExpandClick(expandSection){
        this.props.analyticsActions.resetAllFilterValues()
      this.props.totalPostExpandClick(expandSection)
    }
    selectedPageno (pageno) {
      this.setState(
        {
          pageno: pageno
        },
        () => this.fetchFeedData(this.state.period)
      )
    }
    hideSelectedLegend(e) {
        if(this.state.hideChart[e]==true){
            this.setState({
              hideChart:{...this.state.hideChart,[e]:false}
            });
          }else{
            this.setState({
              hideChart:{...this.state.hideChart,[e]:true}
          });
          }
    }
    renderLoader(){
        return(
         
            <div className  ="inner-adoption-chart">
               <div class="header-analytics-wrapper clearfix header-loader-wrapper-analytics">
                    <div className="title  loader-grey-line  loader-line-radius loader-line-height"></div>
                   
              </div>
                <div className = "bodyPartTotalPostExapnd" id='expandTotalPost'>
                       <div class="recharts-legend-wrapper">
                        <ul>
                          <li class="loader-grey-line loader-line-radius loader-line-height"> </li>
                          <li class="loader-grey-line loader-line-radius loader-line-height"></li>
                          <li class="loader-grey-line loader-line-radius loader-line-height"></li>
                           <li class="loader-grey-line loader-line-radius loader-line-height"></li>
                        </ul>
                      </div>
                    <div className = "loader-grey-line  chart-container-loader"> </div>  
                </div>
             </div>
          

        )
    }

    renderChart(thisProps, intervalDay, GraphDimention){
        return(  
               <div className  ="inner-adoption-chart">
                    <div className = "header-part">
                        {/* header-part */}
                        <div className="header-analytics-wrapper clearfix">
                          <div className="title"> Posts by network <span className="svg-icon help-element"><svg xmlns="http://www.w3.org/2000/svg" data-tip  data-for='bynetwork' width="24" height="24" viewBox="0 0 24 24"><path d="M0 0h24v24H0z" fill="none"/><path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm1 17h-2v-2h2v2zm2.07-7.75l-.9.92C13.45 12.9 13 13.5 13 15h-2v-.5c0-1.1.45-2.1 1.17-2.83l1.24-1.26c.37-.36.59-.86.59-1.41 0-1.1-.9-2-2-2s-2 .9-2 2H8c0-2.21 1.79-4 4-4s4 1.79 4 4c0 .88-.36 1.68-.93 2.25z"/></svg></span>
                        {this.props.tooltip !== null ? 
                                                    <this.props.tooltip place="right" type="light" effect='solid' id='bynetwork' delayHide={100} delayUpdate={100}>
                                                        <div className="tooltip-wrapper">
                                                            <div className="inner-tooltip-wrapper">
                                                                <div className="tooltip-header clearfix">
                                                                    <div className="analytics-tooltip-title">
                                                                        <span>Posts by network</span>
                                                                    </div>

                                                                </div>
                                                                <div className="tooltip-about-wrapper">
                                                                    <span>ABOUT</span>
                                                                    <p>Review total post breakdown for each network.</p>
                                                                </div>
                                                            </div>
                                                        </div>           
                                                    </this.props.tooltip>
                                                :''  
                                                }
                                                </div>
                         
                          <div className="filter-weekey-container clearfix">
                          <div className = "filter-container">
                             <a className={`week-filter ${this.state.period=='week'?'active':''}`} onClick={this.selectedTimeSpan.bind(this,'week')}> Week </a>
                             <a className={`month-filter ${this.state.period=='month'?'active':''}`} onClick={this.selectedTimeSpan.bind(this,'month')}>  Month  </a>
                             <a className={`year-filter ${this.state.period=='year'?'active':''}`} onClick={this.selectedTimeSpan.bind(this,'year')}>Year </a>
                            <span className={`filterIcon ${this.props.parentState['filterData-network']!== undefined && Object.keys(this.props.parentState['filterData-adoption']).length>2? 'active':'' }`} onClick={this.props.openFilterPopup.bind(this)}>  <i class="material-icons">filter_list</i>  </span>
                          </div>
                          {this.props.isFilterClicked==true ? <AnalyticsDashboardFilter fetchFilterData={this.props.fetchFilterData} includeLocation={true} callFrom='network' period={this.props.period}/>:''}
                        </div>
                    </div>
                        {/* End header part */}
                    </div>
                    <div className = "bodyPartTotalPostExapnd" id='expandTotalPost'> 
                        {thisProps.Recharts !== null && typeof thisProps.analytics.postByNetworkData.chart_data !== "undefined" ? 
                        <this.props.Recharts.LineChart width={GraphDimention.width} height={GraphDimention.height} data={thisProps.analytics.postByNetworkData.chart_data} margin={{top: 5, right: 30, left: 20, bottom: 5}}>
                           <this.props.Recharts.XAxis dataKey="date" tickFormatter={thisProps.convertTimeStampToDate} tickSize={0} minTickGap={12} tickMargin={10} />
                           <this.props.Recharts.YAxis/>
                           <this.props.Recharts.CartesianGrid strokeDasharray="0 0" vertical={false} stroke={"#f1f2f3"}/>
                           <this.props.Recharts.Tooltip labelFormatter={this.props.customizeTooltip} />
                           <this.props.Recharts.Legend iconType="circle" verticalAlign="top" content={this.UpdatedLegend} countData={thisProps.analytics.postByNetworkData}/>
                           <this.props.Recharts.Line type="linear" dataKey="Facebook" stroke={COLORS[0]} strokeWidth={3} dot={false} hide={this.state.hideChart['Facebook']} />
                            <this.props.Recharts.Line type="linear" dataKey="Twitter" stroke={COLORS[1]} strokeWidth={3} dot={false} hide={this.state.hideChart['Twitter']}/>
                           <this.props.Recharts.Line type="linear" dataKey="Linkedin" stroke={COLORS[2]} strokeWidth={3} dot={false} hide={this.state.hideChart['Linkedin']}/>
                           <this.props.Recharts.Line type="linear" dataKey="Internal" stroke={COLORS[3]} strokeWidth={3} dot={false} hide={this.state.hideChart['Internal']}/>
                         </this.props.Recharts.LineChart>
                          :<div></div>
                        }
                    </div>
                 </div>
        )
    }
 render() {
         var windowDimention = utils.windowSize()
         var GraphDimention = utils.getChartDimension(windowDimention.w, 'totalPostExpand');
         var days = this.props.analytics.postByNetworkData.chart_data ? this.props.analytics.postByNetworkData.chart_data.length : ''

         var intervalDay = 0
            if (days > 31 && days <= 365) {
              intervalDay = 60
            } else if (days <= 31 && days > 7) {
              intervalDay = 3
            } else {
              intervalDay = 0
            }
        return (
         <div className = "culture-analytics-exapnd">
            <div className = "innerTotalPostExapnd">
            <div className = {`row-container-of-the-chart-analytics ${this.props.analytics.postByNetworkLoader  ? 'row-container-of-the-chart-analytics-loader-totalpost' :''}`}>  
            <div className = "Backbutton"> 
                 <a onClick={this.totalPostExpandClick.bind(this,false)}>
                     <i class="material-icons">keyboard_arrow_left</i> Back </a>
                </div>
                 {/*loader*/}
                 {
                    this.props.analytics.postByNetworkLoader ? 
                        this.renderLoader()
                    :   this.renderChart(this.props,intervalDay, GraphDimention)
                 }  
                
                 </div> 
                 <AnalyticsTable
                        analyticsData={this.props.analytics}
                        popupanylitcs={this.popupanylitcs.bind(this)}
                        route='feed'
                        dataType='post'
                        location='/analytics/feed'
                        pageNo={this.selectedPageno.bind(this)}
                    />
         </div>
        </div>
        )
    }
}

function mapStateToProps(state) {
    return {}
}

function mapDispatchToProps(dispatch) {
    return {}
}

let connection = connect(
    mapStateToProps,
    mapDispatchToProps
)

module.exports = connection(TotalPostsExpand)
