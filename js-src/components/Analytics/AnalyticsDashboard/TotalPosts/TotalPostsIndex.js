import { connect } from 'react-redux'
import PreLoader from '../../../PreLoader'
import Globals from '../../../../Globals'
import * as utils from '../../../../utils/utils'
import AnalyticsDashboardFilter from '../AnalyticsDashboardFilter'
var isDivFound=false
var GraphDimention={}
class TotalPostsIndex extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            hideChart:false,
        }
    }
    renderLoader(){
        return(
            <div class="total-post-chart adoption-loader-wrapper-analytics">
                  <div class="inner-total-post-chart">
                     {/* header start */}
                     <div class="header-analytics-wrapper clearfix header-loader-wrapper-analytics">
                        <div className="title  loader-grey-line  loader-line-radius loader-line-height"></div>
                        <div className="header-year-expand-wrapper loader-grey-line  loader-line-radius loader-line-height">
                        </div>
                     </div>
                     {/* header end */}
                     <div class="data-part-analytics clearfix">
                        <div class="big-no-adoption">
                           <div class="big-no-adoption-wrapper loader-grey-line  loader-line-radius loader-line-height">
                           </div>
                        </div>
                        <div class="data-adoption">
                           <ul>
                              <li class="clearfix active-user loader-grey-line  loader-line-radius loader-line-height">
                              </li>
                              <li class="clearfix no-active-user loader-grey-line  loader-line-radius loader-line-height">
                              </li>
                           </ul>
                        </div>
                     </div>
                     <div class="chart-container">
                     <div id='rightSideChart'>
                        <div class="recentasset-wrapper">
                           <div class="recharts-wrapper">
                              <svg class="recharts-surface loader-grey-line  loader-line-radius loader-line-height" width="493" height="352" viewBox="0 0 509 287" version="1.1">
                              </svg>
                           </div>
                        </div>
                        </div>
                     </div>
                  </div>
                  </div>
        )
    }
    UpdatedLegend = props => {
        const { payload, customizeLegendValue } = props
        var LegendKeys = Object.keys(customizeLegendValue[0])
        return (
            <ul>
                {LegendKeys.length > 0
                    ? LegendKeys.map((entry, index) => (
                        <li
                            className={`clearfix`}
                            key={index}
                            onClick={this.hideSelectedLegend.bind(this, entry)}
                        >
                            <span className = {`title-active-user ${ this.state.hideChart[payload[index].dataKey] !== undefined && this.state.hideChart[payload[index].dataKey]==true ? 'strikeLegend':''}`}> {entry} </span>{' '}
                            <span className = "rounder-dot" style={{background:payload[index].color}}></span>
                            <span className='text-no'>
                                {customizeLegendValue[0][entry]}
                            </span>
                        </li>
                    ))
                    : ''}
            </ul>
        )
    }
    hideSelectedLegend(e) {
        if(this.state.hideChart[e]==true){
            this.setState({
              hideChart:{...this.state.hideChart,[e]:false}
            });
          }else{
            this.setState({
              hideChart:{...this.state.hideChart,[e]:true}
          });
          }
    }
    customizeTooltipForPost = props => {
        const { payload, newTooltipdata, label } = props
        var FBData,LIData,TWData,chartDate=0
        if (payload.length > 0) {
            chartDate=payload[0].payload.date
            FBData=payload[0].payload.Facebook
            LIData=payload[0].payload.Linkedin
            TWData=payload[0].payload.Twitter
        }
        return(
            <div> 
                <div className='chartDate'>
                    <div className='label'>
                       {this.props.customizeTooltip(chartDate)}
                    </div> 
                </div>
                <div className="internal-external-post-wrapper">
            {payload.length > 0
            ? payload.map((entry, i) => {
                return (
                    this.state.hideChart[entry.dataKey] !== undefined && this.state.hideChart[entry.dataKey] == true && (entry.dataKey == 'External posts' || entry.dataKey == 'Internal posts') ? '' :
                        <div className={`custom-tooltip ${entry.dataKey=='Internal posts'?'internalPostLabel':''} ${payload.length == 1? 'internalPostWrapper':''}`} key={i}  >
                                <div className='label' style={{color:entry.color}}>
                                    <span className="labelText">{`${entry.dataKey =='Internal posts' ?'Internal':entry.dataKey=='External posts'?'External':''}`} </span> : <span class="social-icon-number">{`${entry.value}`}</span>
                                </div>
                        </div>
                )
            })
            : ''}
           {this.state.hideChart['External posts'] !== undefined && this.state.hideChart['External posts'] == true  ? '' :
            <div class="chart-social-data">
                <div className="chart-social-fb-wrapper chart-social-data-wrapper">
                    <span class="social fb">
                        Facebook
                    </span>
                    :<span className="social-icon-number">{FBData}</span>
                </div>
                <div className="chart-social-twitter-wrapper chart-social-data-wrapper">
                    <span class="social tw">
                        Twitter
                    </span>
                    :<span className="social-icon-number">{TWData}</span>
                </div>
                <div className="chart-social-linkedin-wrapper chart-social-data-wrapper">
                    <span class="social ln li">
                        Linkedin:
                    </span>
                    :<span className="social-icon-number">{LIData}</span>
                </div>
        </div> }
            </div>
            </div>
        )
    }

    totalPostExpandClick(expandTotalPost){
        this.props.totalPostExpandClick(expandTotalPost);
    }

    render() {
        var windowDimention = utils.windowSize()
        GraphDimention =utils.getChartDimension(windowDimention.w,'totalPostChart')
        var totalPostData =
            Object.keys(this.props.analyticsProps.totalPostAnalytics).length > 0
                ? this.props.analyticsProps.totalPostAnalytics.totalPostData !==
                    undefined
                    ? Object.keys(
                        this.props.analyticsProps.totalPostAnalytics.totalPostData
                    ).length > 0
                        ? this.props.analyticsProps.totalPostAnalytics.totalPostData
                        : ''
                    : ''
                : ''
                var totalPostLoader=Object.keys(this.props.analyticsProps.totalPostAnalytics).length > 0 ? this.props.analyticsProps.totalPostAnalytics.totalPostLoader:''
        var days = totalPostData ? totalPostData.chart_data.length : 0
        var intervalDay = 0
        if (days > 31 && days <= 365) {
        intervalDay = 60
        } else if (days <= 31 && days >7) {
        intervalDay = 7
        } else {
        intervalDay = 0
        }
        var arrayLegendValue = []
        if (totalPostData !== '') {
            var objToAdd = {
                'Internal posts': totalPostData['Internal posts'],
                    'External posts': totalPostData['External posts']
                }
                arrayLegendValue.push(objToAdd)
            }
        return (
            totalPostLoader==false && this.props.Recharts?
            <div className='total-post-chart'>
           
                <div className='inner-total-post-chart'>
                    {/* header-part */}
                    <div className='header-analytics-wrapper clearfix'>
                        <div className='title'>Total posts
                        <span className="svg-icon help-element"><svg xmlns="http://www.w3.org/2000/svg" data-tip  data-for='totalpost' width="24" height="24" viewBox="0 0 24 24"><path d="M0 0h24v24H0z" fill="none"/><path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm1 17h-2v-2h2v2zm2.07-7.75l-.9.92C13.45 12.9 13 13.5 13 15h-2v-.5c0-1.1.45-2.1 1.17-2.83l1.24-1.26c.37-.36.59-.86.59-1.41 0-1.1-.9-2-2-2s-2 .9-2 2H8c0-2.21 1.79-4 4-4s4 1.79 4 4c0 .88-.36 1.68-.93 2.25z"/></svg></span>
                        {this.props.tooltip !== null ? 
                                                    <this.props.tooltip place="right" type="light" effect='solid' id='totalpost' delayHide={100} delayUpdate={100}>
                                                        <div className="tooltip-wrapper">
                                                            <div className="inner-tooltip-wrapper">
                                                                <div className="tooltip-header clearfix">
                                                                    <div className="analytics-tooltip-title">
                                                                        <span>Total posts</span>
                                                                    </div>

                                                                </div>
                                                                <div className="tooltip-about-wrapper">
                                                                    <span>ABOUT</span>
                                                                    <p>Review total internal and external post count, drill down to total count per channel.</p>
                                                                </div>
                                                            </div>
                                                        </div>           
                                                    </this.props.tooltip>
                                                :''  
                                                }
                        </div>
                         <a className='expand-link' onClick={this.totalPostExpandClick.bind(this, true)}>Expand
                            <span>
                                <i class='material-icons'>navigate_next</i>
                            </span>{' '}
                        </a>
                        <div className='filter-weekey-container clearfix'>
                        <div className = "filter-container">
                            <a className={`week-filter ${this.props.period=='week'?'active':''}`} onClick={this.props.totalPostTimeSpan.bind(this,'week')}> Week </a>
                            <a className={`month-filter ${this.props.period == undefined || this.props.period=='month'?'active':''}`} onClick={this.props.totalPostTimeSpan.bind(this,'month')}>  Month  </a>
                            <a className={`year-filter ${this.props.period=='year'?'active':''}`} onClick={this.props.totalPostTimeSpan.bind(this,'year')}>Year </a>
                            <span className={`filterIcon ${this.props.parentState['filterData-totalpost']!== undefined && Object.keys(this.props.parentState['filterData-totalpost']).length>2? 'active':'' }`} onClick={this.props.openFilterPopup.bind(this)}>
                                <i class='material-icons'>filter_list</i>{' '}
                            </span>
                        </div>
                        {this.props.isFilterClicked==true ? <AnalyticsDashboardFilter includeLocation={true} callFrom='totalpost' period={this.props.period} fetchFilterData={this.props.fetchFilterData}/>:''}
                     </div>
                    </div>
                    {/* end header part */}
                    {/* active data part */}
                    <div className='data-part-analytics clearfix'>
                    <div className = "big-no-total-user">  <span className = "big-no-adoption">  {' '}
                            {totalPostData !== '' ? totalPostData.Posts : 0}{' '} </span> Posts </div>
                    </div>
                    {/* active data part */}
                    {/* Chart-start */}
                    <div className='chart-container'>
                        <div id='rightSideChart'>
                            {totalPostData !== '' && GraphDimention !== undefined&&Object.keys(GraphDimention).length>0? 
                                <this.props.Recharts.ComposedChart
                                    width={GraphDimention.width}
                                    height={GraphDimention.height}
                                    data={totalPostData.chart_data}
                                    margin={{ top: 0, right: 10, left: 0, bottom: 0 }}
                                    >
                                    <this.props.Recharts.CartesianGrid vertical={false} stroke='#f1f2f3' />
                                    <this.props.Recharts.XAxis
                                        dataKey='date'
                                        tickFormatter={(date)=>this.props.convertTimeStampToDate(date,this.props)}
                                        tickSize={0}
                                        minTickGap={12}
                                        tickMargin={10}
                                        
                                    />
                                    <this.props.Recharts.YAxis
                                        tickSize={0}
                                        tickMargin={10} />
                                    <this.props.Recharts.Tooltip
                                        content={this.customizeTooltipForPost}
                                        newTooltipdata={arrayLegendValue}
                                    />
                                    <this.props.Recharts.Legend
                                        height={0}
                                        verticalAlign='top'
                                        iconType='circle'
                                        iconSize={10}
                                        layout='vertical'
                                        content={this.UpdatedLegend}
                                        customizeLegendValue={arrayLegendValue}
                                    />
                                    <this.props.Recharts.Bar
                                        dataKey='Internal posts'
                                        barSize={40}
                                        fill='#ff7a7e'
                                        hide={this.state.hideChart['Internal posts']}
                                    />
                                    <this.props.Recharts.Bar
                                        dataKey='External posts'
                                        fill='#6fbe94'
                                        barSize={40}
                                        hide={this.state.hideChart['External posts']}
                                    />
                                </this.props.Recharts.ComposedChart>:''}
                        </div>
                    </div>
                    {/* chart-End */}
                </div>
            </div>:this.renderLoader()
            
        )
           
    }
}

function mapStateToProps(state) {
    return {}
}

function mapDispatchToProps(dispatch) {
    return {}
}

let connection = connect(
    mapStateToProps,
    mapDispatchToProps
)

module.exports = connection(TotalPostsIndex)
