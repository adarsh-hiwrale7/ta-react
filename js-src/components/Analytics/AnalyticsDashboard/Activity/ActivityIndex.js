import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as utils from '../../../../utils/utils';
import Header from '../../../Header/Header';
import moment from '../../../Chunks/ChunkMoment';
import * as analyticsActions from '../../../../actions/analytics/analyticsActions';
import * as cultureAnalyticsActions from '../../../../actions/analytics/cultureAnalyticsActions';
import recharts from '../../../Chunks/ChunkRecharts';
import reactTooltip from '../../../Chunks/ChunkReactTooltip';
import AnalyticsNav from '../../AnalyticsNav';
import TotalPostsIndex from '../TotalPosts/TotalPostsIndex'
import QualityIndex from '../Quality/QualityIndex';
import AssetsIndex from '../Assets/AssetsIndex';
import TagCloudIndex from '../TagCloud/TagCloudIndex';
import GuestUserRestrictionPopup from '../../../GuestUserRestrictionPopup';
import TotalPostsExpand from '../TotalPosts/TotalPostsExpand';
import AnalyticsPopupParent from '../../AnalyticsPopupParent';
const COLORS = ['#4bcf99', '#ff595e', '#4b74e0', '#ffbf00', '#b87d7a', '#FF8042','#495867','#7FB069','#B8D8D8','#A93F55','#577399','#F9ADA0',"#ACF39D",'#19323C'];
var screenWidth = document.body.clientWidth;
var currFrom = ''
class ActivityIndex extends React.Component {
  constructor(props){
    super(props);
    this.state={
      Recharts: null,
      moment: null,
      period: 'month',
      tooltip: null,
      refreshChart: false,
      isFilterClicked: false,
      expandTotalPost: false,
      side_popup: false,
      selectedPopupData: null,
    }
  }
  componentWillMount() {
    window.scrollTo(0,0);
    moment().then(moment => {
        recharts().then(recharts => {
            reactTooltip().then(reactTooltip => {
                this.setState({ moment: moment, Recharts: recharts, tooltip: reactTooltip })
            })
        })
    })
}
  componentDidMount(){
    let roleName =JSON.parse(localStorage.getItem('roles')) !== null? JSON.parse(localStorage.getItem('roles'))[0].role_name:''
    if(roleName!=='guest'){
    var e = this;
        window.addEventListener("resize", function () {
            e.setState({ refreshChart: !e.state.refreshChart });
        }, true);
      var timezone = utils.clientTimezone();
      var ObjToSend = {
        'timezone': timezone,
        timespan: 'month'
      }
      this.props.analyticsActions.fetchQualityAnalyticsData(ObjToSend)
      this.props.analyticsActions.fetchAssetAnalyticsData(ObjToSend)
      this.props.analyticsActions.fetchTagCloudData(ObjToSend)
      this.props.analyticsActions.fetchLocationBaseAdoption(ObjToSend);
      this.props.analyticsActions.fetchTotalPostsChartData(ObjToSend)
      var me =this;
      window.addEventListener('click', function (event) {
        if (event.target.innerText !== 'filter_list') {
            if (!event.target.closest('.filter-tag-child') && !event.target.closest('.filter-tag-parent')) {
                if(currFrom!==''){
                    me.setState({
                        isFilterClicked: {...me.state.isFilterClicked, [currFrom]: false }
                    })
                }
            }
        }
    })
    this.props.analyticsActions.resetAllFilterValues()
    }
  }

  CustomizedContent = (props) => {
    const { root, depth, x, y, width, height, index, payload, rank } = props;
    var colors = COLORS
    return (
      <g>
        <rect
          x={x}
          y={y}
          width={width}
          height={height}
          style={{
            fill: colors[Math.floor(index / root.children.length * 6)],
            stroke: '#fff',
          }}
        />
        <text
          x={x + width / 2}
          y={y + height / 2 + 7}
          textAnchor="middle"
          fill="#fff"
          fontSize={14}
        >
          {utils.capitalizeFirstLetter(props['office-location'])} : {props.value}
        </text>
      </g>
    );
  }

  totalPostExpandClick(expandSection) {
    var timezone = utils.clientTimezone();
    var ObjToSend = {
        'timezone': timezone,
        timespan: 'month'
    }
    this.props.analyticsActions.fetchTotalPostsChartData(ObjToSend)
    this.props.analyticsActions.fetchLocationBaseAdoption(ObjToSend)
    this.state[`filterData-adoption`] !== undefined ? Object.assign(this.state[`filterData-adoption`], ObjToSend):''
    this.state[`filterData-totalpost`] !== undefined ? Object.assign(this.state[`filterData-totalpost`], ObjToSend) :''
    this.setState({ expandTotalPost: expandSection,period:'month' })
}

  renderLocationBaseLoader() {
    return (
      <div className="inner-adoption-chart">
        {/* loader add*/}
        {/* header-part */}
        <div className="header-analytics-wrapper clearfix header-loader-wrapper-analytics ">
          <div className="title loader-grey-line  loader-line-radius loader-line-height"></div>
          <div className="header-year-expand-wrapper loader-grey-line  loader-line-radius loader-line-height">
          </div>
        </div>
        {/* end header */}

        <div className="body-engagement-channel expand-body-chart-wrapper">
          <div className="inner-body-engagement-channel" id='leftSideChart'>
            <div className="TotalNetworkchartContainer TotalNetworkchartContainerLoader loader-grey-line">
            </div>
          </div>
        </div>
        {/* loader End */}
      </div>
    )
  }
  customizeTooltipForAdoption = props => {
    const {  payload } = props;
    var colors = COLORS
    if (payload.length > 0) {
      return (
        <div className='custom-tooltip'>
          <p className='label' style={{ color: colors[Math.floor(payload[0].payload.index / payload[0].payload.root.children.length * 6)] }} >
            {utils.capitalizeFirstLetter(payload[0].name)}: {payload[0].value}
          </p>
        </div>
      )
    }
}
customizeTooltip = (date) => {
  return (
      this.state.moment ? this.state.moment.unix(date).format('DD-MM-YYYY') : ''
  )
}
popupanylitcs(data) {
  document.body.classList.add('overlay');
  this.setState({
      side_popup: true,
      selectedPopupData: data
  })
}
fetchFilterData(data, from) {
  this.setState({
      [`filterData-${from}`]: data
  })
}
openFilterPopup(from, status) {
  currFrom = from
  this.props.analyticsActions.resetSelectedFilterValues(from.replace(/Filter/g, ""))
  var me=this
  Object.keys(me.state.isFilterClicked).forEach(function(key) {
      if (me.state.isFilterClicked[key]!== undefined && me.state.isFilterClicked[key] ==true) {
          if(key !== from){
              me.state.isFilterClicked[key] = false;
          }
      }
    })
  me.setState({
      isFilterClicked: {...me.state.isFilterClicked, [from]: !me.state.isFilterClicked[from] }
  })
}
convertTimeStampToDate = (date, props) => {
  var period = props ? props.period : 'month'
  var dateFormat = period !== undefined ? period : 'month'
  var newFormat = ''
  if (dateFormat == 'month' || dateFormat == 'week') {
      newFormat = 'DD MMM'
  } else if (dateFormat == 'year') {
      newFormat = 'MMM YY'
  }
  return (
      this.state.moment ? this.state.moment.unix(date).format(newFormat) : ''
  )
}
selectedTimeSpan(from, time) {
  var timezone = utils.clientTimezone();
  var ObjToSend = {
      'timezone': timezone,
      timespan: time
  }
  if (this.state[`filterData-${from}`] !== undefined) {
      Object.assign(this.state[`filterData-${from}`], ObjToSend)
  }
  var dataToPass = this.state[`filterData-${from}`] !== undefined && Object.keys(this.state[`filterData-${from}`]).length > 0 ? this.state[`filterData-${from}`] : ObjToSend
  this.setState({
      period: { ...this.state.period, [from]: time }
  })
  switch (from) {
      case 'totalpost':
          this.props.analyticsActions.fetchTotalPostsChartData(dataToPass)
          break;
      case 'assets':
          this.props.analyticsActions.fetchAssetAnalyticsData(dataToPass)
          break;
      case 'quality':
          this.props.analyticsActions.fetchQualityAnalyticsData(dataToPass)
          break;
      case 'tagcloud':
          this.props.analyticsActions.fetchTagCloudData(dataToPass)
          break;
      case 'locationChart':
          this.props.analyticsActions.fetchLocationBaseAdoption(dataToPass)
          break;
      default:
          break;
  }
}
    closePopup() {
      this.setState({
          side_popup: false
      })
      document.body.classList.remove('overlay')
    }
    render() {
      var culture = this.props.culture;
      var windowDimention = utils.windowSize();
      var GraphDimention = utils.getChartDimension(windowDimention.w, 'LocationGraph');
      var locationBaseAdoption = this.props.analytics.locationBaseAdoption !== undefined && Object.keys(this.props.analytics.locationBaseAdoption).length > 0 ? this.props.analytics.locationBaseAdoption.locationBaseData !== undefined && Object.keys(this.props.analytics.locationBaseAdoption.locationBaseData).length > 0 ? this.props.analytics.locationBaseAdoption.locationBaseData : '' : ''
      var locationBaseLoader = this.props.analytics.locationBaseAdoption !== undefined && Object.keys(this.props.analytics.locationBaseAdoption).length > 0 ? this.props.analytics.locationBaseAdoption.locationBaseLoader !== undefined ? this.props.analytics.locationBaseAdoption.locationBaseLoader : '' : ''
      return (
            <section id="analytics-Main-container" className="analytics-wrapper">
              <GuestUserRestrictionPopup setOverlay={true}/>
              {this.state.side_popup ?
                    <AnalyticsPopupParent
                        selectedPopupData={this.state.selectedPopupData}
                        location='/analytics/feed'
                        dataType='post'
                        props={this.props}
                        closePopup={this.closePopup.bind(this)}
                    />
                    : ''}
              <Header
                  title={'Analytics'}
                  location={this.props.location}
              />
              <div className='main-container'>
                  <div id="content">
                  {(!this.state.expandTotalPost) ?
                  <div>
                      <div className="culture-dashboard-tab-responsive culture-score-tab clearfix">
                          <AnalyticsNav location={this.props.location} />
                      </div>
                      <div className="chart-container-in-analytics-wrapper dashboard-analytics-level1-container">
                        <div className="inner-chart-container-in-analytics-wrapper">
                            <div className="row-container-of-the-chart-analytics">
                            <div className="adoption-chart totalPostNetworkchartContainer">
                                {locationBaseLoader == true ? this.renderLocationBaseLoader() :
                                  <div className="inner-adoption-chart">
                                    <div className="header-analytics-wrapper">
                                      <div className="clearfix">
                                        <div className="title"> Posts by location
                                        <span className="svg-icon help-element"><svg xmlns="http://www.w3.org/2000/svg" data-tip data-for='locationchart' width="24" height="24" viewBox="0 0 24 24"><path d="M0 0h24v24H0z" fill="none"/><path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm1 17h-2v-2h2v2zm2.07-7.75l-.9.92C13.45 12.9 13 13.5 13 15h-2v-.5c0-1.1.45-2.1 1.17-2.83l1.24-1.26c.37-.36.59-.86.59-1.41 0-1.1-.9-2-2-2s-2 .9-2 2H8c0-2.21 1.79-4 4-4s4 1.79 4 4c0 .88-.36 1.68-.93 2.25z"/></svg></span>
                                        </div>
                                        {this.state.tooltip !== null ?
                                        <this.state.tooltip place="right" type="light" effect='solid' id='locationchart' delayHide={100} delayUpdate={100}>
                                            <div className="tooltip-wrapper">
                                                <div className="inner-tooltip-wrapper">
                                                    <div className="tooltip-header clearfix">
                                                        <div className="analytics-tooltip-title">
                                                            <span>Posts by location</span>
                                                        </div>
                                                        {/* <div className="analytics-tooltip-read-more">
                                                          <a href="#">Read more<i class="material-icons">keyboard_arrow_right</i>
                                                          </a>
                                                          </div> */}

                                                    </div>
                                                    <div className="tooltip-about-wrapper">
                                                        <span>ABOUT</span>
                                                        <p>Review activity by location.</p>
                                                    </div>
                                                </div>

                                              </div>
                                          </this.state.tooltip>
                                          : ''
                                        }
                                        <div className="filter-weekey-container clearfix">
                                          <div className="filter-container">
                                            <a className={`week-filter ${this.state.period['locationChart'] == 'week' ? 'active' : ''}`} onClick={this.selectedTimeSpan.bind(this, 'locationChart','week')}> Week </a>
                                            <a className={`month-filter ${this.state.period['locationChart'] == undefined || this.state.period['locationChart'] == 'month' ? 'active' : ''}`} onClick={this.selectedTimeSpan.bind(this, 'locationChart','month')}>  Month  </a>
                                            <a className={`year-filter ${this.state.period['locationChart'] == 'year' ? 'active' : ''}`} onClick={this.selectedTimeSpan.bind(this, 'locationChart','year')}>Year </a>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    {/* End header part */}
                                    {/* body part */}
                                    <div className="body-engagement-channel expand-body-chart-wrapper">
                                      <div className="inner-body-engagement-channel" id='leftSideChart'>
                                        <div className={`TotalNetworkchartContainer ${locationBaseAdoption && this.props.Recharts && locationBaseAdoption.chart.length>0 ?'':'TotalNetworkchartContainerNoValueData'}`}>
                                          {locationBaseAdoption && this.state.Recharts ?
                                          locationBaseAdoption.chart.length>0?
                                            <this.state.Recharts.Treemap
                                              width={GraphDimention.width}
                                              height={GraphDimention.height}
                                              data={locationBaseAdoption.chart}
                                              dataKey="count"
                                              ratio={4 / 3}
                                              stroke="#fff"
                                              nameKey='office-location'
                                              content={this.CustomizedContent}
                                            >
                                            <this.state.Recharts.Tooltip content={this.customizeTooltipForAdoption}/>
                                            </this.state.Recharts.Treemap>
                                            : <div className = "no-Value-chart">No data available</div>
                                            : <div className = "no-Value-chart">No data available</div>}
                                        </div>
                                      </div>
                                    </div>
                                    {/* End body part */}

                                  </div>}
                              </div>
                                  <TotalPostsIndex
                                    parentState={this.state}
                                    fetchFilterData={this.fetchFilterData.bind(this)}
                                    isFilterClicked={this.state.isFilterClicked['totalpostFilter']}
                                    openFilterPopup={this.openFilterPopup.bind(this, 'totalpostFilter', true)}
                                    period={this.state.period['totalpost']}
                                    totalPostTimeSpan={this.selectedTimeSpan.bind(this, 'totalpost')}
                                    tooltip={this.state.tooltip}
                                    customizeTooltip={this.customizeTooltip.bind(this)}
                                    convertTimeStampToDate={this.convertTimeStampToDate.bind(this)}
                                    analyticsProps={this.props.analytics}
                                    Recharts={this.state.Recharts}
                                    refreshChart={this.state.refreshChart}
                                    totalPostExpandClick={this.totalPostExpandClick.bind(this)}
                                    />
                                </div>
                                </div>
                                </div>
                                <div className = "Total-Social-Engagement-Wrapper">
                                  <div className="last-block-engagement-analytics">
                                                <div className="inner-last-block-engagment clearfix">
                                                    <AssetsIndex
                                                        parentState={this.state}
                                                        fetchFilterData={this.fetchFilterData.bind(this)}
                                                        isFilterClicked={this.state.isFilterClicked['assetsFilter']}
                                                        openFilterPopup={this.openFilterPopup.bind(this, 'assetsFilter', true)}
                                                        period={this.state.period['assets']}
                                                        assetsTimeSpan={this.selectedTimeSpan.bind(this, 'assets')}
                                                        analyticsProps={this.props.analytics}
                                                        tooltip={this.state.tooltip} />

                                                    <QualityIndex
                                                        parentState={this.state}
                                                        fetchFilterData={this.fetchFilterData.bind(this)}
                                                        isFilterClicked={this.state.isFilterClicked['qualityFilter']}
                                                        openFilterPopup={this.openFilterPopup.bind(this, 'qualityFilter', true)}
                                                        period={this.state.period['quality']}
                                                        qualityTimeSpan={this.selectedTimeSpan.bind(this, 'quality')}
                                                        analyticsProps={this.props.analytics}
                                                        tooltip={this.state.tooltip} />
                                                    <TagCloudIndex
                                                        parentState={this.state}
                                                        fetchFilterData={this.fetchFilterData.bind(this)}
                                                        isFilterClicked={this.state.isFilterClicked['tagcloudFilter']}
                                                        openFilterPopup={this.openFilterPopup.bind(this, 'tagcloudFilter', true)}
                                                        period={this.state.period['tagcloud']}
                                                        tagCloudTimeSpan={this.selectedTimeSpan.bind(this, 'tagcloud')}
                                                        analyticsProps={this.props.analytics}
                                                        tooltip={this.state.tooltip} />
                                                </div>
                                            </div>
                                          </div>
                            </div>
                      :
                      <div className="main-wrppaer-analytics">
                        <TotalPostsExpand
                          parentState={this.state}
                          tooltip={this.state.tooltip}
                          fetchFilterData={this.fetchFilterData.bind(this)}
                          isFilterClicked={this.state.isFilterClicked['networkFilter']}
                          openFilterPopup={this.openFilterPopup.bind(this, 'networkFilter', true)}
                          Recharts={this.state.Recharts}
                          convertTimeStampToDate={this.convertTimeStampToDate.bind(this)}
                          analyticsActions={this.props.analyticsActions}
                          analytics={this.props.analytics}
                          popupanylitcs={this.popupanylitcs.bind(this)}
                          totalPostExpandClick={this.totalPostExpandClick.bind(this)}
                          customizeTooltip={this.customizeTooltip.bind(this)}
                        />
                      </div>
                      }
                  </div>
              </div>
              </section>
      )
    }
}
function mapStateToProps(state) {
  return {
      culture: state.cultureAnalytics,
      analytics: state.analytics,
  }
}
function mapDispatchToProps(dispatch) {
  return {
      cultureAnalyticsActions: bindActionCreators(cultureAnalyticsActions, dispatch),
      analyticsActions: bindActionCreators(analyticsActions, dispatch),
  }
}
let connection = connect(mapStateToProps, mapDispatchToProps);
module.exports = connection(ActivityIndex);