import { connect } from 'react-redux'
import PreLoader from '../../../PreLoader'
import * as utils from '../../../../utils/utils'
import AnalyticsDashboardFilter from '../AnalyticsDashboardFilter'
class QualityIndex extends React.Component {
  constructor (props) {
    super(props)
    this.state = {}
  }
  renderLoader () {
    return (
      <div class='inner-qulity-index-container-analytics'>
        <div className='header-analytics-wrapper clearfix header-loader-wrapper-analytics '>
          <div className='title loader-grey-line  loader-line-radius loader-line-height' />
        </div>
        <div class='body-quality-analytics'>
          <div class=' quality-analytics-body-wrapper  anyalitcs-tabuler-data anaytlics-tabuler-loader'>
            <table>
              <tbody>
                <tr>
                  <td class='rank-td-container'>
                    <div class='rank-title loader-grey-line  loader-line-radius loader-line-height'>
                      <span class='rank-no loader-grey-line  loader-line-radius loader-line-height' />
                    </div>
                  </td>
                  <td class='chart-container'>
                    <div className='small-chart-img loader-grey-line  loader-line-radius loader-line-height'>
                      <div class='small-chart' />
                    </div>
                  </td>
                </tr>
                <tr>
                  <td class='rank-td-container'>
                    <div class='rank-title loader-grey-line  loader-line-radius loader-line-height'>
                      <span class='rank-no loader-grey-line  loader-line-radius loader-line-height' />
                    </div>
                  </td>
                  <td class='chart-container'>
                    <div className='small-chart-img loader-grey-line  loader-line-radius loader-line-height'>
                      <div class='small-chart' />
                    </div>
                  </td>
                </tr>
                <tr>
                  <td class='rank-td-container'>
                    <div class='rank-title loader-grey-line  loader-line-radius loader-line-height'>
                      <span class='rank-no loader-grey-line  loader-line-radius loader-line-height'>
                        {' '}
                      </span>
                    </div>
                  </td>
                  <td class='chart-container'>
                    <div className='small-chart-img loader-grey-line  loader-line-radius loader-line-height'>
                      <div class='small-chart' />
                    </div>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    )
  }
  render () {
    var Allcount = []
    var countDiff = []
    var qualityData =
      Object.keys(this.props.analyticsProps.qualityAnalytics).length > 0
        ? this.props.analyticsProps.qualityAnalytics.qualityData !== undefined
          ? Object.keys(this.props.analyticsProps.qualityAnalytics.qualityData)
            .length > 0
            ? this.props.analyticsProps.qualityAnalytics.qualityData
            : ''
          : ''
        : ''
    var qualityLoader =
      Object.keys(this.props.analyticsProps.qualityAnalytics).length > 0
        ? this.props.analyticsProps.qualityAnalytics.qualityLoader
        : ''
    var qualityType = ['Posts approved', 'Assets approved', 'Total approvals']
    if (qualityData !== '') {
      Allcount = [qualityData.posts, qualityData.assets, qualityData.total]
      countDiff = [
        qualityData.diff_posts,
        qualityData.diff_assets,
        qualityData.diff_total
      ]
    }
    return (
      <div className='qulity-index-container-analytics'>
      {qualityLoader == false ? 
        <div className='inner-qulity-index-container-analytics'>
            <div className='header-analytics-wrapper clearfix'>
              <div className='title'>
                {' '}
                Quality{' '}
                <span>
                  {' '}
                  <i class='material-icons' data-tip data-for='quality'>
                    {' '}
                    help
                  </i>{' '}
                </span>{' '}
              </div>
              {this.props.tooltip !== null ? (
                <this.props.tooltip
                  place='right'
                  type='light'
                  effect='solid'
                  id='quality'
                  delayHide={100} delayUpdate={100}
                >
                  <div className='tooltip-wrapper'>
                    <div className='inner-tooltip-wrapper'>
                      <div className='tooltip-header clearfix'>
                        <div className='analytics-tooltip-title'>
                          <span>Quality</span>
                        </div>
                      </div>
                      <div className='tooltip-about-wrapper'>
                        <span>ABOUT</span>
                        <p>
                        Total percentage of external posts and assets approved by a Moderator.
                        </p>
                      </div>
                    </div>
                  </div>
                </this.props.tooltip>
              ) : (
                ''
              )}
              <div className='filter-weekey-container clearfix'>
              <div className = "filter-container">
              <a className={`week-filter ${this.props.period=='week'?'active':''}`} onClick={this.props.qualityTimeSpan.bind(this,'week')}> Week </a>
                <a className={`month-filter ${this.props.period == undefined || this.props.period=='month'?'active':''}`} onClick={this.props.qualityTimeSpan.bind(this,'month')}>  Month  </a>
                <a className={`year-filter ${this.props.period=='year'?'active':''}`} onClick={this.props.qualityTimeSpan.bind(this,'year')}>Year </a>
                <span className={`filterIcon ${this.props.parentState['filterData-quality']!== undefined && Object.keys(this.props.parentState['filterData-quality']).length>2? 'active':'' }`} onClick={this.props.openFilterPopup.bind(this)}>
                  {' '}
                  <i class='material-icons'>filter_list</i>{' '}
                </span>
              </div>
              {this.props.isFilterClicked==true ? <AnalyticsDashboardFilter fetchFilterData={this.props.fetchFilterData} includeLocation={true} callFrom='quality' period={this.props.period}/>:''}
              </div>
              </div>
              <div className='body-quality-analytics'>
                <div className=' quality-analytics-body-wrapper  anyalitcs-tabuler-data'>
                  <table>
                    <tbody>
                      {qualityData !== ''
                        ? qualityType.map((qt, i) => {
                          return (
                            <tr key={i}>
                              <td class='rank-td-container'>
                                <div className='rank-title'>
                                  {' '}
                                  {qt}{' '}
                                  <span className='rank-no'>
                                    {Math.round(Allcount[i])}%{' '}
                                  </span>{' '}
                                </div>
                              </td>
                              <td className='chart-container'>
                                <div className='rank-data-container'>
                                  <div className='small-chart'>
                                    {utils.rankDiff(countDiff[i], true)}
                                  </div>
                                  <div className='coun-diff'><div className="rank-icon">{utils.rankDiff(countDiff[i], false)}</div><div className="rank-count-no"> {Math.round(Math.abs(countDiff[i]))}</div> </div>
                                </div>
                              </td>
                            </tr>
                          )
                        })
                        : ''}
                    </tbody>
                  </table>
                </div>
              </div>
              </div>
        : 
            this.renderLoader()}
      </div>
    )
  }
}
function mapStateToProps (state) {
  return {}
}
function mapDispatchToProps (dispatch) {
  return {}
}
let connection = connect(
  mapStateToProps,
  mapDispatchToProps
)

module.exports = connection(QualityIndex)
