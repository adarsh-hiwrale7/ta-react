import { connect } from 'react-redux'
import PreLoader from '../../../PreLoader'
import * as utils from '../../../../utils/utils'
import AnalyticsDashboardFilter from '../AnalyticsDashboardFilter'
class AseetsIndex extends React.Component {
  constructor (props) {
    super(props)
    this.state = {}
  }
  renderLoader () {
    return (
      <div class='assets-index-container-analytics'>
        <div class='inner-assets-index-container-analytics'>
          <div className='header-analytics-wrapper clearfix header-loader-wrapper-analytics '>
            <div className='title loader-grey-line  loader-line-radius loader-line-height' />
          </div>
          <div class='body-asset-analytics'>
            <div class='asset-anaylicts-body-wrapper anyalitcs-tabuler-data anaytlics-tabuler-loader'>
              <table>
                <tbody>
                  <tr>
                    <td class='rank-title-td'>
                      <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                    </td>
                    <td class='rank-no-td'>
                      <div class='rank-no loader-grey-line  loader-line-radius loader-line-height' />
                    </td>
                    <td class='chart-td'>
                      <div className='small-chart-img loader-grey-line  loader-line-radius loader-line-height'>
                        <div class='small-chart' />
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td class='rank-title-td'>
                      <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                    </td>
                    <td class='rank-no-td'>
                      <div class='rank-no loader-grey-line  loader-line-radius loader-line-height' />
                    </td>
                    <td class='chart-td'>
                      <div className='small-chart-img loader-grey-line  loader-line-radius loader-line-height'>
                        <div class='small-chart' />
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td class='rank-title-td'>
                      <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                    </td>
                    <td class='rank-no-td'>
                      <div class='rank-no loader-grey-line  loader-line-radius loader-line-height' />
                    </td>
                    <td class='chart-td'>
                      <div className='small-chart-img loader-grey-line  loader-line-radius loader-line-height'>
                        <div class='small-chart' />
                      </div>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    )
  }
  render () {
    var assetData =
      Object.keys(this.props.analyticsProps.assetsAnalytics).length > 0
        ? this.props.analyticsProps.assetsAnalytics.assetData !== undefined
          ? Object.keys(this.props.analyticsProps.assetsAnalytics.assetData)
            .length > 0
            ? this.props.analyticsProps.assetsAnalytics.assetData
            : ''
          : ''
        : ''
    var aseetLoder =
      Object.keys(this.props.analyticsProps.assetsAnalytics).length > 0
        ? this.props.analyticsProps.assetsAnalytics.assetLoader
        : ''
    var assetType = ['Images', 'Videos', 'Files']
    var Allcount = []
    var countDiff = []
    if (assetData !== '') {
      Allcount = [assetData.images, assetData.videos, assetData.docs]
      countDiff = [
        assetData.diff_images,
        assetData.diff_videos,
        assetData.diff_docs
      ]
    }
    return aseetLoder == false ? (
      <div className='assets-index-container-analytics'>
        <div className='inner-assets-index-container-analytics'>
          {/* header */}
          <div className='header-analytics-wrapper clearfix'>
            <div className='title'>Assets<span className="svg-icon help-element"><svg xmlns="http://www.w3.org/2000/svg" data-tip data-for='assets' width="24" height="24" viewBox="0 0 24 24"><path d="M0 0h24v24H0z" fill="none"/><path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm1 17h-2v-2h2v2zm2.07-7.75l-.9.92C13.45 12.9 13 13.5 13 15h-2v-.5c0-1.1.45-2.1 1.17-2.83l1.24-1.26c.37-.36.59-.86.59-1.41 0-1.1-.9-2-2-2s-2 .9-2 2H8c0-2.21 1.79-4 4-4s4 1.79 4 4c0 .88-.36 1.68-.93 2.25z"/></svg></span>
            </div>
            {this.props.tooltip !== null ? (
              <this.props.tooltip
                place='right'
                type='light'
                effect='solid'
                id='assets'
                delayHide={100} delayUpdate={100}
              >
                <div className='tooltip-wrapper'>
                  <div className='inner-tooltip-wrapper'>
                    <div className='tooltip-header clearfix'>
                      <div className='analytics-tooltip-title'>
                        <span>Assets</span>
                      </div>
                    </div>
                    <div className='tooltip-about-wrapper'>
                      <span>ABOUT</span>
                      <p>
                      Total number of assets uploaded to Visibly via the mobile or desktop applications.
                      </p>
                    </div>
                  </div>
                </div>
              </this.props.tooltip>
            ) : (
              ''
            )}
           <div className='filter-weekey-container clearfix'>
           <div className = "filter-container">
              <a className={`week-filter ${this.props.period=='week'?'active':''}`} onClick={this.props.assetsTimeSpan.bind(this,'week')}> Week </a>
                <a className={`month-filter ${this.props.period == undefined || this.props.period=='month'?'active':''}`} onClick={this.props.assetsTimeSpan.bind(this,'month')}>  Month  </a>
                <a className={`year-filter ${this.props.period=='year'?'active':''}`} onClick={this.props.assetsTimeSpan.bind(this,'year')}>Year </a>
              <span className={`filterIcon ${this.props.parentState['filterData-assets']!== undefined && Object.keys(this.props.parentState['filterData-assets']).length>2? 'active':'' }`} onClick={this.props.openFilterPopup.bind(this)}>
                {' '}
                <i class='material-icons'>filter_list</i>{' '}
              </span>
            </div> 
            {this.props.isFilterClicked==true ? <AnalyticsDashboardFilter fetchFilterData={this.props.fetchFilterData} includeLocation={true} callFrom='assets' period={this.props.period}/>:''}
            </div>
          </div>
          {/* end header */}
          {/* bodypart */}
          <div className='body-asset-analytics'>
            <div className='asset-analytics-body-wrapper anyalitcs-tabuler-data'>
              <table>
                <tbody>
                  {assetType.length > 0
                    ? assetType.map((at, i) => {
                      return (
                        <tr key={i}>
                          <td className='rank-title-td'>
                            {' '}
                            <div className='rank-title'> {at} </div>{' '}
                          </td>
                          <td className='rank-no-td'>
                            {' '}
                            <div className='rank-no'>
                              {' '}
                              {Allcount[i]}{' '}
                            </div>{' '}
                          </td>
                          <td className='chart-td'>
                            {' '}
                            <div className='rank-data-container'>
                              <div className='small-chart'>
                                {utils.rankDiff(countDiff[i], true)}
                              </div>
                                <div className='coun-diff'>
                                  <div className="rank-icon">
                                    {utils.rankDiff(countDiff[i], false)}{' '}</div>
                                    <div className="rank-count-no"> 
                                    {Math.abs(countDiff[i])}
                                    </div>
                                  </div>
                            </div>
                          </td>
                        </tr>
                      )
                    })
                    : ''}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    ) : (
      this.renderLoader()
    )
  }
}
function mapStateToProps (state) {
  return {}
}
function mapDispatchToProps (dispatch) {
  return {}
}
let connection = connect(
  mapStateToProps,
  mapDispatchToProps
)

module.exports = connection(AseetsIndex)
