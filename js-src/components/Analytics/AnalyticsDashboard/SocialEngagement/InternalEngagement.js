import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import PreLoader from '../../../PreLoader'
import * as utils from '../../../../utils/utils'
class InternalEngagement extends React.Component {
  constructor (props) {
    super(props)
    this.state = {}
  }

  render () {
    var Allcount = []
    var countDiff = []
    var internalData =
      Object.keys(this.props.analyticsProps.internalEngaementAnalytics).length >
      0 ? this.props.analyticsProps.internalEngaementAnalytics.internalData !== undefined
        ? Object.keys(
          this.props.analyticsProps.internalEngaementAnalytics.internalData
        ).length > 0
          ? this.props.analyticsProps.internalEngaementAnalytics.internalData
          : '':''
        : ''
    var internalType = ['Mentions', 'Comments', 'Likes']
    if (internalData !== '') {
      Allcount = [
        internalData.mentions,
        internalData.comments,
        internalData.likes
      ]
      countDiff = [
        internalData.diff_mentions,
        internalData.diff_comments,
        internalData.diff_likes
      ]
    }
    return (
        <div className='inner-social-part2'>
        {/* headepar */}
        <div className='header-anayltics'>
          <div className='header-analytics-wrapper clearfix'>
            <div className='int-engagement title'>Internal engagement<span className="svg-icon help-element"><svg xmlns="http://www.w3.org/2000/svg" data-tip  width="24" height="24" viewBox="0 0 24 24"><path d="M0 0h24v24H0z" fill="none"/><path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm1 17h-2v-2h2v2zm2.07-7.75l-.9.92C13.45 12.9 13 13.5 13 15h-2v-.5c0-1.1.45-2.1 1.17-2.83l1.24-1.26c.37-.36.59-.86.59-1.41 0-1.1-.9-2-2-2s-2 .9-2 2H8c0-2.21 1.79-4 4-4s4 1.79 4 4c0 .88-.36 1.68-.93 2.25z"/></svg></span></div>
            {this.props.tooltip !== null ? 
                <this.props.tooltip place="right" type="light" effect='solid' delayHide={100} delayUpdate={100}>
                    <div className="tooltip-wrapper">
                        <div className="inner-tooltip-wrapper">
                            <div className="tooltip-header clearfix">
                                <div className="analytics-tooltip-title">
                                    <span>Internal engagement</span>
                                </div>

                            </div>
                            <div className="tooltip-about-wrapper">
                                <span>ABOUT</span>
                                <p>Top level view of total engagement across all feeds within Visibly.</p>
                            </div>
                        </div>
                    </div>           
                </this.props.tooltip>
            :''  
            }
            {/* <a className = "expand-link"> Expand <span><i class="material-icons">navigate_next</i></span> </a> */}
          </div>
        </div>
        {/* end header part */}
        <div className='anyalitcs-tabuler-data'>
          <table>
            <tbody>
            {internalType.length > 0
              ? internalType.map((it, i) => {
                return (
                  <tr key={i}>
                    <td className='rank-title-td'>
                      {' '}
                      <div className='rank-title'> {it} </div>{' '}
                    </td>
                    <td className='rank-no-td'>
                      {' '}
                      <div className='rank-no'> {Allcount[i]} </div>{' '}
                    </td>
                    <td className='chart-td'>
                      {' '}
                      <div className = "rank-data-container">
                      <div className='small-chart'>
                      {' '}
                      {utils.rankDiff(countDiff[i],true)}
                    </div>
                     {utils.rankDiff(countDiff[i],false) == '' ? <div className='coun-diff'>{countDiff[i]}</div>:<div className='coun-diff'>{utils.rankDiff(countDiff[i],false)} {Math.abs(countDiff[i])} </div>}
                     </div>
                    </td>
                  </tr>
                )
              })
              : ''}
              </tbody>
          </table>
        </div>
      </div>
    )
  }
}
module.exports = InternalEngagement
