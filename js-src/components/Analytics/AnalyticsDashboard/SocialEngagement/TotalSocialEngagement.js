import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import PreLoader from '../../../PreLoader'
import * as utils from '../../../../utils/utils'
import AnalyticsDashboardFilter from '../AnalyticsDashboardFilter'
import InternalEngagement from './InternalEngagement'
class TotalSocialEngagement extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
  }
  renderLoader() {
    return (
      <div class="inner-Total-Social-engagement clearfix">
        <div class="inner-social-part1">
          <div class="header-analytics ">
            <div className="header-analytics-wrapper clearfix header-loader-wrapper-analytics ">
              <div className="title loader-grey-line  loader-line-radius loader-line-height"></div>
              <div className="header-year-expand-wrapper loader-grey-line  loader-line-radius loader-line-height">
              </div>
            </div>
          </div>
          <div class="body-container-social-engagement Total-Social-engagement-body clearfix">
            <div class="block-part1">
              <div class="anyalitcs-tabuler-data anaytlics-tabuler-loader">
                <table>
                  <tbody>
                    <tr>
                      <td class="rank-title-td">
                        <div class="rank-title loader-grey-line  loader-line-radius loader-line-height">  </div>
                      </td>
                      <td class="rank-no-td">
                        <div class="rank-no loader-grey-line  loader-line-radius loader-line-height"> </div>
                      </td>
                      <td class="chart-td">
                        <div className="small-chart-img loader-grey-line  loader-line-radius loader-line-height">
                          <div class="small-chart">
                          </div>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td class="rank-title-td">
                        <div class="rank-title loader-grey-line  loader-line-radius loader-line-height">  </div>
                      </td>
                      <td class="rank-no-td">
                        <div class="rank-no loader-grey-line  loader-line-radius loader-line-height"> </div>
                      </td>
                      <td class="chart-td">
                        <div className="small-chart-img loader-grey-line  loader-line-radius loader-line-height">
                          <div class="small-chart">
                          </div>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td class="rank-title-td">
                        <div class="rank-title loader-grey-line  loader-line-radius loader-line-height">  </div>
                      </td>
                      <td class="rank-no-td">
                        <div class="rank-no loader-grey-line  loader-line-radius loader-line-height"> </div>
                      </td>
                      <td class="chart-td">
                        <div className="small-chart-img loader-grey-line  loader-line-radius loader-line-height">
                          <div class="small-chart">
                          </div>
                        </div>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <div class="block-part2">
              <div class="anyalitcs-tabuler-data anaytlics-tabuler-loader">
                <table>
                  <tbody>
                    <tr>
                      <td class="rank-title-td">
                        <div class="rank-title loader-grey-line  loader-line-radius loader-line-height">  </div>
                      </td>
                      <td class="rank-no-td">
                        <div class="rank-no loader-grey-line  loader-line-radius loader-line-height"> </div>
                      </td>
                      <td class="chart-td">
                        <div className="small-chart-img loader-grey-line  loader-line-radius loader-line-height">
                          <div class="small-chart">
                          </div>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td class="rank-title-td">
                        <div class="rank-title loader-grey-line  loader-line-radius loader-line-height">  </div>
                      </td>
                      <td class="rank-no-td">
                        <div class="rank-no loader-grey-line  loader-line-radius loader-line-height"> </div>
                      </td>
                      <td class="chart-td">
                        <div className="small-chart-img loader-grey-line  loader-line-radius loader-line-height">
                          <div class="small-chart">
                          </div>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td class="rank-title-td">
                        <div class="rank-title loader-grey-line  loader-line-radius loader-line-height">  </div>
                      </td>
                      <td class="rank-no-td">
                        <div class="rank-no loader-grey-line  loader-line-radius loader-line-height"> </div>
                      </td>
                      <td class="chart-td">
                        <div className="small-chart-img loader-grey-line  loader-line-radius loader-line-height">
                          <div class="small-chart">
                          </div>
                        </div>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <div class="inner-social-part2">
          <div class="header-analytics">
            <div className="header-analytics-wrapper clearfix header-loader-wrapper-analytics ">
              <div className="title loader-grey-line  loader-line-radius loader-line-height"></div>

            </div>
          </div>
          <div class="anyalitcs-tabuler-data anaytlics-tabuler-loader">
            <table>
              <tbody>
                <tr>
                  <td class="rank-title-td">
                    <div class="rank-title loader-grey-line  loader-line-radius loader-line-height">  </div>
                  </td>
                  <td class="rank-no-td">
                    <div class="rank-no loader-grey-line  loader-line-radius loader-line-height">  </div>
                  </td>
                  <td class="chart-td">
                    <div className="small-chart-img loader-grey-line  loader-line-radius loader-line-height">
                      <div class="small-chart">
                      </div>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td class="rank-title-td">
                    <div class="rank-title loader-grey-line  loader-line-radius loader-line-height">  </div>
                  </td>
                  <td class="rank-no-td">
                    <div class="rank-no loader-grey-line  loader-line-radius loader-line-height"> </div>
                  </td>
                  <td class="chart-td">
                    <div className="small-chart-img loader-grey-line  loader-line-radius loader-line-height">
                      <div class="small-chart">
                      </div>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td class="rank-title-td">
                    <div class="rank-title loader-grey-line  loader-line-radius loader-line-height"></div>
                  </td>
                  <td class="rank-no-td">
                    <div class="rank-no loader-grey-line  loader-line-radius loader-line-height"> </div>
                  </td>
                  <td class="chart-td">
                    <div className="small-chart-img loader-grey-line  loader-line-radius loader-line-height">
                      <div class="small-chart">
                      </div>
                    </div>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    )
  }
  render() {
    var AllSocialcount = []
    var AllSocialcountTwo = []

    var countSocialDiff = []
    var countSocialDiffTwo = []
    var totalSocialEngagementData =
      Object.keys(this.props.analyticsProps.totalSocialAnalytics).length >
        0 ? this.props.analyticsProps.totalSocialAnalytics.totalSocialEngagementData !== undefined
          ? Object.keys(
            this.props.analyticsProps.totalSocialAnalytics.totalSocialEngagementData
          ).length > 0
            ? this.props.analyticsProps.totalSocialAnalytics.totalSocialEngagementData
            : '' : ''
        : ''
    var socialLoader = Object.keys(this.props.analyticsProps.totalSocialAnalytics).length > 0 ? this.props.analyticsProps.totalSocialAnalytics.totalSocialEngagementLoader : ''
    var socialType = ['Shares', 'Retweets', 'Views']
    var socialTypeTwo = ['Reach', 'Comments', 'Likes']
    if (totalSocialEngagementData !== '') {
      AllSocialcount = [
        totalSocialEngagementData.shares,
        totalSocialEngagementData.retweets,
        totalSocialEngagementData.views,
      ]
      countSocialDiff = [
        totalSocialEngagementData.diff_shares,
        totalSocialEngagementData.diff_retweets,
        totalSocialEngagementData.diff_views,
      ]
      AllSocialcountTwo = [
        totalSocialEngagementData.reach,
        totalSocialEngagementData.comments,
        totalSocialEngagementData.likes,
      ]
      countSocialDiffTwo = [
        totalSocialEngagementData.diff_reach,
        totalSocialEngagementData.diff_comments,
        totalSocialEngagementData.diff_likes,
      ]
    }
    var internalLoader = Object.keys(this.props.analyticsProps.internalEngaementAnalytics).length > 0 ? this.props.analyticsProps.internalEngaementAnalytics.internalLoader : ''

    return (
      internalLoader==false && socialLoader == false ?
      <div className='inner-Total-Social-engagement clearfix'>
        {/* heading part  */}
          <div className='stickey-header'>
          <div className='header-anayltics header-for-comman-filter'>
            <div className='header-analytics-wrapper clearfix'>
               <div className='filter-weekey-container clearfix'>
               <div className = "filter-container">
                <a className={`week-filter ${this.props.period=='week'?'active':''}`} onClick={this.props.totalSocialTimeSpan.bind(this,'week')}> Week </a>
                  <a className={`month-filter ${this.props.period == undefined || this.props.period=='month'?'active':''}`} onClick={this.props.totalSocialTimeSpan.bind(this,'month')}>  Month  </a>
                  <a className={`year-filter ${this.props.period=='year'?'active':''}`} onClick={this.props.totalSocialTimeSpan.bind(this,'year')}>Year </a>
                <span className={`filterIcon ${this.props.parentState['filterData-totalSocial']!== undefined && Object.keys(this.props.parentState['filterData-totalSocial']).length>2? 'active':'' }`} onClick={this.props.openFilterPopup.bind(this)}>
                  {' '}
                  <i class='material-icons'>filter_list</i>{' '}
                </span>
              </div>
              {this.props.isFilterClicked==true ? <AnalyticsDashboardFilter fetchFilterData={this.props.fetchFilterData} includeLocation={true} callFrom='totalSocial' period={this.props.period}/>:''}
              </div>
            </div>
          </div>
        </div>
        <div className='inner-social-part1'>
          {/* headerpart */}
          <div className='header-anayltics'>
            <div className='header-analytics-wrapper clearfix'>
              <div className='title'>
                Total social engagement<span className="svg-icon help-element"><svg xmlns="http://www.w3.org/2000/svg" data-tip data-for='totalsocialenagage' width="24" height="24" viewBox="0 0 24 24"><path d="M0 0h24v24H0z" fill="none"/><path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm1 17h-2v-2h2v2zm2.07-7.75l-.9.92C13.45 12.9 13 13.5 13 15h-2v-.5c0-1.1.45-2.1 1.17-2.83l1.24-1.26c.37-.36.59-.86.59-1.41 0-1.1-.9-2-2-2s-2 .9-2 2H8c0-2.21 1.79-4 4-4s4 1.79 4 4c0 .88-.36 1.68-.93 2.25z"/></svg></span>
              </div>
              {this.props.tooltip !== null ?
                <this.props.tooltip place="right" type="light" effect='solid' id='totalsocialenagage' delayHide={100} delayUpdate={100}>
                    <div className="tooltip-wrapper">
                        <div className="inner-tooltip-wrapper">
                            <div className="tooltip-header clearfix">
                                <div className="analytics-tooltip-title">
                                    <span>Total social engagement</span>
                                </div>
                                {/* <div className="analytics-tooltip-read-more">
                                  <a href="#">Read more<i class="material-icons">keyboard_arrow_right</i>
                                  </a>
                                  </div> */}

                            </div>
                            <div className="tooltip-about-wrapper">
                                <span>ABOUT</span>
                                <p>Top level view of total engagement across all social media channels.</p>
                            </div>
                        </div>

                      </div>
                  </this.props.tooltip>
                  : ''
                }
              </div>
            </div>
            {/* end headr part */}
            {/* body part */}
            <div className='body-container-social-engagement Total-Social-engagement-body clearfix'>
              <div className='block-part1'>
                <div className='anyalitcs-tabuler-data'>
                  <table>
                    <tbody>
                      {socialType.length > 0
                        ? socialType.map((st, i) => {
                          return (
                            <tr key={i}>
                              <td className='rank-title-td'>
                                {' '}
                                <div className='rank-title'> {st} </div>{' '}
                              </td>
                              <td className='rank-no-td'>
                                {' '}
                                <div className='rank-no'> {AllSocialcount[i]} </div>{' '}
                              </td>
                              <td className='chart-td'>
                                {' '}
                                <div className="rank-data-container">
                                  <div className='small-chart'>
                                    {' '}
                                    {utils.rankDiff(countSocialDiff[i], true)}
                                  </div>
                                    <div className='coun-diff'><div className="rank-icon">{utils.rankDiff(countSocialDiff[i], false)}</div><div className="rank-count-no"> {Math.abs(countSocialDiff[i])}</div> </div>
                                </div>
                              </td>
                            </tr>
                          )
                        })
                        : ''}
                    </tbody>
                  </table>

                </div>
              </div>

              <div className='block-part2'>
                <div className='anyalitcs-tabuler-data'>
                  <table>
                    <tbody>
                      {socialTypeTwo.length > 0
                        ? socialTypeTwo.map((st, i) => {
                          return (
                            <tr key={i}>
                              <td className='rank-title-td'>
                                {' '}
                                <div className='rank-title'> {st} </div>{' '}
                              </td>
                              <td className='rank-no-td'>
                                {' '}
                                <div className='rank-no'> {AllSocialcountTwo[i]} </div>{' '}
                              </td>
                              <td className='chart-td'>
                                {' '}
                                <div className="rank-data-container">
                                  <div className='small-chart'>
                                    {' '}
                                    {utils.rankDiff(countSocialDiffTwo[i], true)}
                                  </div>
                                  <div className='coun-diff'><div className="rank-icon">{utils.rankDiff(countSocialDiffTwo[i], false)}</div><div className="rank-count-no"> {Math.abs(countSocialDiffTwo[i])}</div> </div>
                                </div>
                              </td>
                            </tr>
                          )
                        })
                        : ''}
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            {/* end body part */}
          </div>

          <InternalEngagement analyticsProps={this.props.analyticsProps} tooltip={this.props.tooltip} />
          {/* body part */}
        </div> : this.renderLoader()
    )
  }
}
module.exports = TotalSocialEngagement
