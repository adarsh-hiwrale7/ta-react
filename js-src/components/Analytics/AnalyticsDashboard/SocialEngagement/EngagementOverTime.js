import { IndexLink, Link } from 'react-router'
import AnalyticsDashboardFilter from '../AnalyticsDashboardFilter'
import * as utils from '../../../../utils/utils';
class EngagementOverTime extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            opacity: 1,
            hideChart: false
        }
    }
    componentDidMount() {
        var timezone = utils.clientTimezone();
        var ObjToSend = {
            'timezone': timezone,
            timespan: 'month'
        }
        this.props.analyticsActions.fetchEngagementOverTime(ObjToSend)
    }

    UpdatedLegend = (props) => {
        const { payload,countData } = props;
        if (payload.length > 0) {
            // var LegendKeys = Object.keys(customizeLegendValue[0])
            return (
                <ul>
                    {payload.map((entry, index) => (
                        <li className="clearfix" key={index} onClick={this.hideSelectedLegend.bind(this, entry.dataKey)}>
                            <span className="rounder-dot" style={{ background: entry.color }}></span>
                            <span className={`title-active-user ${this.state.hideChart[entry.dataKey] !== undefined && this.state.hideChart[entry.dataKey] == true ? 'strikeLegend' : ''}`}> {entry.dataKey}   </span><span className="text-no">{countData[entry.dataKey]} </span> </li>
                    ))}
                </ul>
            );
        }
    }
    hideSelectedLegend(e) {
        if (this.state.hideChart[e] == true) {
            this.setState({
                hideChart: { ...this.state.hideChart, [e]: false }
            });
        } else {
            this.setState({
                hideChart: { ...this.state.hideChart, [e]: true }
            });
        }
    }
    renderLoader() {
        return (
            <div className="inner-adoption-chart">

                {/* header-part */}
                <div className="header-analytics-wrapper clearfix header-loader-wrapper-analytics ">
                    <div className="title loader-grey-line  loader-line-radius loader-line-height"></div>
                    <div className="header-year-expand-wrapper loader-grey-line  loader-line-radius loader-line-height">
                    </div>
                </div>
                {/* end header */}

                {/* second chart container */}
                <div className="body-engagement-channel expand-body-chart-wrapper">
                    <div className="inner-body-engagement-channel" id='bottomRightSideChart'>
                        <div className="overtime-social-chart">
                            <div className="legend-wrapper-loader-for-overTime-socialpart">
                                <ul>
                                    <li className="loader-grey-line loader-line-radius loader-line-height">  </li>
                                    <li className="loader-grey-line loader-line-radius loader-line-height"></li>
                                </ul>
                            </div>
                            <div>
                                <div className="loader-grey-line overTimeChartLoaderContainer"> </div>
                            </div>
                        </div>

                    </div>
                </div>
                {/* second chart container  */}
            </div>
        )
    }
    render() {
        var engagementOverTimeData = Object.keys(this.props.analyticsProps.engagementOverTime).length > 0 ? this.props.analyticsProps.engagementOverTime.engagementOverTimeData : ''
        var engagementOverTimeLoader = Object.keys(this.props.analyticsProps.engagementOverTime).length > 0 ? this.props.analyticsProps.engagementOverTime.engagementOverTimeLoader : ''
        var windowDimention = utils.windowSize()
        var GraphDimention = utils.getChartDimension(windowDimention.w, 'engagementOverTime');
        var intervalDay = 0
        var days = engagementOverTimeData ? engagementOverTimeData.chart_data.length : ''
        var intervalDay = 0
        if (days > 31 && days <= 365) {
            intervalDay = 60
        } else if (days <= 31 && days > 7) {
            intervalDay = 7
        } else {
            intervalDay = 0
        }
        return (

            <div className="adoption-chart engagementOverTimeContainer">
                {engagementOverTimeLoader == true ? this.renderLoader() :
                    <div className="inner-adoption-chart">
                        {/* header-part */}
                        <div className="header-analytics-wrapper clearfix">
                            <div className="title"> Engagement over time 
                            <span className="svg-icon help-element"><svg xmlns="http://www.w3.org/2000/svg" data-tip data-for='overtime' width="24" height="24" viewBox="0 0 24 24"><path d="M0 0h24v24H0z" fill="none"/><path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm1 17h-2v-2h2v2zm2.07-7.75l-.9.92C13.45 12.9 13 13.5 13 15h-2v-.5c0-1.1.45-2.1 1.17-2.83l1.24-1.26c.37-.36.59-.86.59-1.41 0-1.1-.9-2-2-2s-2 .9-2 2H8c0-2.21 1.79-4 4-4s4 1.79 4 4c0 .88-.36 1.68-.93 2.25z"/></svg></span>
                            </div>
                            {this.props.tooltip !== null ? 
                                <this.props.tooltip place="right" type="light" effect='solid' id='overtime' delayHide={100} delayUpdate={100}>
                                    <div className="tooltip-wrapper">
                                        <div className="inner-tooltip-wrapper">
                                            <div className="tooltip-header clearfix">
                                                <div className="analytics-tooltip-title">
                                                    <span>Engagement over time </span>
                                                </div>
                                                {/* <div className="analytics-tooltip-read-more">
                                                <a href="#">Read more<i class="material-icons">keyboard_arrow_right</i>
                                                </a>
                                                </div> */}

                                            </div>
                                            <div className="tooltip-about-wrapper">
                                                <span>ABOUT</span>
                                                <p>Review engagement per social channel over time.</p>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </this.props.tooltip>
                                : ''
                                }
                            <div className="filter-weekey-container clearfix">
                                <div className="filter-container">
                                    <a className={`week-filter ${this.props.overTimePeriod == 'week' ? 'active' : ''}`} onClick={this.props.engagementOverTimespan.bind(this, 'week')}> Week </a>
                                    <a className={`month-filter ${this.props.overTimePeriod == undefined || this.props.overTimePeriod == 'month' ? 'active' : ''}`} onClick={this.props.engagementOverTimespan.bind(this, 'month')}>  Month  </a>
                                    <a className={`year-filter ${this.props.overTimePeriod == 'year' ? 'active' : ''}`} onClick={this.props.engagementOverTimespan.bind(this, 'year')}>Year </a>
                                    <span className={`filterIcon ${this.props.parentState['filterData-overTime']!== undefined && Object.keys(this.props.parentState['filterData-overTime']).length>2? 'active':'' }`} onClick={this.props.openFilterPopup.bind(this)}>  <i class="material-icons">filter_list</i>  </span>
                                </div>
                                {this.props.isFilterClicked == true ? <AnalyticsDashboardFilter fetchFilterData={this.props.fetchFilterData} includeLocation={true} callFrom='overTime' period={this.props.overTimePeriod} /> : ''}
                            </div>
                        </div>
                        {/* End header part */}
                        {/* body part */}
                        <div className="body-engagement-channel expand-body-chart-wrapper expand-overtime-container">
                            <div className="inner-body-engagement-channel" id='bottomRightSideChart'>
                                <div className="overtime-social-chart">
                                    {/* <img src="img/analytics-social.png" alt="analytics" /> */}
                                    {engagementOverTimeData !== '' && engagementOverTimeData!== undefined && GraphDimention !== undefined && this.props.Recharts ?
                                        <this.props.Recharts.LineChart
                                            width={GraphDimention.width}
                                            height={GraphDimention.height}
                                            data={engagementOverTimeData.chart_data}
                                            margin={{ top: 0, right: 25, left: 0, bottom: 0 }}>
                                            <this.props.Recharts.CartesianGrid stroke="#f1f2f3" vertical={false} />
                                            <this.props.Recharts.XAxis dataKey="date" tickSize={0} minTickGap={12} tickMargin={10} tickFormatter={(date) => this.props.convertTimeStampToDate(date, this.props)} />
                                            <this.props.Recharts.YAxis tickSize={0} tickMargin={10} />
                                            <this.props.Recharts.Tooltip labelFormatter={this.props.customizeTooltip} />
                                            <this.props.Recharts.Legend verticalAlign="top" iconType='circle' iconSize={10} layout="vertical" content={this.UpdatedLegend} countData={engagementOverTimeData}/>
                                            <this.props.Recharts.Line type="linear" dataKey="Facebook" stroke="#4267B2" hide={this.state.hideChart['Facebook']} dot={false} strokeWidth={2} />
                                            <this.props.Recharts.Line type="linear" dataKey="Internal" stroke="#ff595e" hide={this.state.hideChart['Internal']} dot={false} strokeWidth={2} />
                                            <this.props.Recharts.Line type="linear" dataKey="Linkedin" stroke="#0077b5" hide={this.state.hideChart['Linkedin']} dot={false} strokeWidth={2} />
                                            <this.props.Recharts.Line type="linear" dataKey="Twitter" stroke="#1da1f2" hide={this.state.hideChart['Twitter']} dot={false} strokeWidth={2} />
                                        </this.props.Recharts.LineChart>
                                        : ''}
                                </div>
                            </div>
                        </div>
                        {/* End body part */}
                    </div>}
            </div>
        )
    }
}

module.exports = EngagementOverTime