import { IndexLink, Link } from 'react-router'
import * as utils from '../../../../utils/utils';
import AnalyticsDashboardFilter from '../AnalyticsDashboardFilter'
var obj = {
  InternalColors: ['#ff595e', '#ffd5d7', '#ffacae'],
  FacebookColors: ['#4267b2', '#80a2cf', '#a0c0dd'],
  LinkedinColors: ['#007bb5', '#bfdeec', '#096ba3'],
  TwitterColors: ['#00b6f1', '#0997d0', '#91cce5']
}
class EngagementByChannel extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      opacity_internal: {},
      opacity_facebook: {},
      opacity_linkedin: {},
      opacity_twitter: {},
    }
  }
  //to handle mouse over and leave event on chart
  handleMouseEnter(data, callFrom) {
    this.setState({
      [`opacity_${callFrom}`]: { key: `${data.value}-${callFrom}`, val: 0.5 },
    });
  }

  handleMouseLeave(data, index) {
    this.setState({
      opacity_internal: {},
      opacity_facebook: {},
      opacity_linkedin: {},
      opacity_twitter: {},
    });
  }
  componentDidMount() {
    var timezone = utils.clientTimezone();
    var ObjToSend = {
      'timezone': timezone,
      timespan: 'month'
    }
    this.props.analyticsActions.fetchEngagementByChannel(ObjToSend)
  }

  //to personalise tooltip
  customizeTooltip = props => {
    const { payload, label } = props
    if (payload.length > 0) {
      return (
        <div className='custom-tooltip'>
          <p className='label' style={{ color: payload[0].payload.fill }}>
            {payload[0].name}: {payload[0].value}
          </p>
        </div>
      )

    }
  }
  CustomizedLabel = (props) => {
    const { payload,LegendFrom } = props;
   return (
       <div className="legendUlWrapper">
       <ul>
           {payload.map((entry, index) => (
               <li key={`item-${index}`} data-color={entry.color} onMouseEnter={this.handleMouseEnter.bind(this,entry,LegendFrom)} onMouseLeave={this.handleMouseLeave.bind(this,entry,LegendFrom)}
               >
                   <span  className = "rounder-dot" style={{background:entry.color}}></span>
                   <span className ="title-active-user">{entry.value}</span>
                   <span className="text-no"> {entry.payload.count}</span>
               </li>
           ))}
       </ul>
       </div>
   );
}
  renderLoader(){
    return(
      <div className="adoption-chart engagementByChannel">
        <div className="inner-adoption-chart">

          {/* header-part */}
          <div className="header-analytics-wrapper clearfix header-loader-wrapper-analytics ">
            <div className="title loader-grey-line  loader-line-radius loader-line-height"></div>
            <div className="header-year-expand-wrapper loader-grey-line  loader-line-radius loader-line-height">
            </div>
          </div>
          {/* end header */}
          {/* chart design  */}
          <div className="body-engagement-channel expand-body-chart-wrapper">
            <div className="inner-body-engagement-channel">
              <div className=" clearfix social-graph-wrapper">
                <div className="social-graph-block" >
                  <div className="rounder-graph"><div className="chart-svg-loader loader-grey-line"> </div> </div>
                  <div className="legend">
                    <ul>
                      <li className="loader-grey-line loader-line-radius loader-line-height legend-loader"> </li>
                      <li className="loader-grey-line loader-line-radius loader-line-height legend-loader" > </li>
                    </ul>
                  </div>
                </div>
                <div className="social-graph-block" >
                  <div className="rounder-graph"><div className="chart-svg-loader loader-grey-line"> </div> </div>
                  <div className="legend">
                    <ul>
                      <li className="loader-grey-line loader-line-radius loader-line-height legend-loader"> </li>
                      <li className="loader-grey-line loader-line-radius loader-line-height legend-loader" > </li>
                    </ul>
                  </div>
                </div>
                <div className="social-graph-block" >
                  <div className="rounder-graph"><div className="chart-svg-loader loader-grey-line"> </div> </div>
                  <div className="legend">
                    <ul>
                      <li className="loader-grey-line loader-line-radius loader-line-height legend-loader"> </li>
                      <li className="loader-grey-line loader-line-radius loader-line-height legend-loader" > </li>
                    </ul>
                  </div>
                </div>
                <div className="social-graph-block" >
                  <div className="rounder-graph"><div className="chart-svg-loader loader-grey-line"> </div> </div>
                  <div className="legend">
                    <ul>
                      <li className="loader-grey-line loader-line-radius loader-line-height legend-loader"> </li>
                      <li className="loader-grey-line loader-line-radius loader-line-height legend-loader" > </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>

          {/* end chart design */}

        </div>
      </div>
    )
  }

  render() {
    var totalCount = 0
    var windowDimention = utils.windowSize()
    var GraphDimention = utils.getChartDimension(windowDimention.w, 'EngagementByChannel');
    var EngagementByChannelData = Object.keys(this.props.analyticsProps.engagementByChannel).length > 0 ? this.props.analyticsProps.engagementByChannel.engagementByChannelData : ''
    var engagementByChannelLoader = Object.keys(this.props.analyticsProps.engagementByChannel).length > 0 ? this.props.analyticsProps.engagementByChannel.engagementByChannelLoader : ''
    // var data = { 'Linkedin': [{ name: "Likes", value: 10 }, { name: "Comments", value: 20 }, { name: "Shares", value: 30 }], 'Twitter': [{ name: "Likes", value: 40 }, { name: "shares", value: 50 }, { name: " Comments", value: 60 },{ name: "Link Clicks", value: 60 }], 'Facebook': [{ name: "shares", value: 80 },{ name: "Likes", value: 70 },{ name: "Comments	", value: 90 },{ name: "Link Clicks", value: 60 }], 'Internal': [{ name: "Likes", value: 100 }, { name: "shares", value: 110 }, { name: "Total", value: 120 }, { name: "Mentions", value: 20 }] }

    return (
      engagementByChannelLoader == true ? this.renderLoader() :
        <div className="adoption-chart engagementByChannel">
          <div className="inner-adoption-chart">
            {/* header-part */}
            <div className="header-analytics-wrapper clearfix">
              <div className="title"> Engagement by channel
              <span className="svg-icon help-element"><svg xmlns="http://www.w3.org/2000/svg" data-tip data-for='bychannel' width="24" height="24" viewBox="0 0 24 24"><path d="M0 0h24v24H0z" fill="none"/><path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm1 17h-2v-2h2v2zm2.07-7.75l-.9.92C13.45 12.9 13 13.5 13 15h-2v-.5c0-1.1.45-2.1 1.17-2.83l1.24-1.26c.37-.36.59-.86.59-1.41 0-1.1-.9-2-2-2s-2 .9-2 2H8c0-2.21 1.79-4 4-4s4 1.79 4 4c0 .88-.36 1.68-.93 2.25z"/></svg></span>
               </div>
                {this.props.tooltip !== null ? 
                  <this.props.tooltip place="right" type="light" effect='solid' id='bychannel' delayHide={100} delayUpdate={100}>
                      <div className="tooltip-wrapper">
                          <div className="inner-tooltip-wrapper">
                              <div className="tooltip-header clearfix">
                                  <div className="analytics-tooltip-title">
                                      <span>Engagement by channel</span>
                                  </div>
                                  {/* <div className="analytics-tooltip-read-more">
                                    <a href="#">Read more<i class="material-icons">keyboard_arrow_right</i>
                                    </a>
                                    </div> */}

                              </div>
                              <div className="tooltip-about-wrapper">
                                  <span>ABOUT</span>
                                  <p>Review user engagement per social channel.</p>
                              </div>
                          </div>
                          
                        </div>
                    </this.props.tooltip>
                    : ''
                  }
              <div className="filter-weekey-container clearfix">
                <div className="filter-container">
                  <a className={`week-filter ${this.props.channelPeriod == 'week' ? 'active' : ''}`} onClick={this.props.engagementChannelTimespan.bind(this, 'week')}> Week </a>
                  <a className={`month-filter ${this.props.channelPeriod == undefined || this.props.channelPeriod == 'month' ? 'active' : ''}`} onClick={this.props.engagementChannelTimespan.bind(this, 'month')}>  Month  </a>
                  <a className={`year-filter ${this.props.channelPeriod == 'year' ? 'active' : ''}`} onClick={this.props.engagementChannelTimespan.bind(this, 'year')}>Year </a>
                  <span className={`filterIcon ${this.props.parentState['filterData-byChannel']!== undefined && Object.keys(this.props.parentState['filterData-byChannel']).length>2? 'active':'' }`} onClick={this.props.openFilterPopup.bind(this)}>  <i class="material-icons">filter_list</i>  </span>
                </div>
                {this.props.isFilterClicked == true ? <AnalyticsDashboardFilter fetchFilterData={this.props.fetchFilterData} includeLocation={true} callFrom='byChannel' period={this.props.channelPeriod} /> : ''}
              </div>
            </div>
            {/* End header part */}
            {/* body part */}
            <div className="body-engagement-channel expand-body-chart-wrapper">
              <div className="inner-body-engagement-channel">
                {this.props.Recharts && EngagementByChannelData !== '' ?
                  <div className="clearfix social-graph-wrapper">
                     <div className={`social-graph-block social-graph-tw ${EngagementByChannelData.Twitter.length > 0 ?  '' : 'noValueDataRecharts'}`}>
                      <div id='engagementChartWrapper' className={`engagementChartWrapperContainer donut-engagement-bychannel-${1}`}>
                        {EngagementByChannelData.Twitter.length > 0 ?
                        <div>
                          <this.props.Recharts.PieChart
                            width={GraphDimention.width}
                            height={GraphDimention.height} >
                            <this.props.Recharts.Pie
                              data={EngagementByChannelData.Twitter}
                              cx={GraphDimention.cx}
                              cy={GraphDimention.cy}
                              labelLine={false}
                              outerRadius={GraphDimention.outerRadius}
                              innerRadius={GraphDimention.innerRadius}
                              fill="#8884d8"
                              dataKey="count" nameKey='title'

                            >
                              {
                                EngagementByChannelData.Twitter.map((entry, index) => <this.props.Recharts.Cell fill={obj['TwitterColors'][index % obj['TwitterColors'].length]} key={index} opacity={Object.keys(this.state.opacity_twitter).length > 0 ? (this.state.opacity_twitter.key == `${entry.title}-twitter`) ? 1 : 0.4 : 1} />)
                              }
                            </this.props.Recharts.Pie>
                            <this.props.Recharts.Tooltip content={this.customizeTooltip} />
                            <this.props.Recharts.Legend content={this.CustomizedLabel} iconType="circle" iconSize={10} LegendFrom='twitter'/>
                          </this.props.Recharts.PieChart>

                              <div className="donutChartforEngagement">
                          <div className={`background-socialIcon twitter`}>
                            <i class={`fa fa-twitter`}></i>
                          </div>

                          <div className={`value twitter`}>
                            {EngagementByChannelData.Twitter_total_count}
                          </div>

                        </div>
                        </div>
                        : <div className="no-Value-chart"> No data available for Twitter</div>}
                              </div>

                    </div>


                    <div className={`social-graph-block social-graph-tw ${EngagementByChannelData.Linkedin.length > 0 ? '' : 'noValueDataRecharts'}`}>

                      <div id='engagementChartWrapper' className={`engagementChartWrapperContainer donut-engagement-bychannel-${2}`}>
                        {EngagementByChannelData.Linkedin.length > 0 ?
                          <div>
                            <this.props.Recharts.PieChart
                              width={GraphDimention.width}
                              height={GraphDimention.height} >
                              <this.props.Recharts.Pie
                                data={EngagementByChannelData.Linkedin}
                                cx={GraphDimention.cx}
                                cy={GraphDimention.cy}
                                labelLine={false}
                                outerRadius={GraphDimention.outerRadius}
                                innerRadius={GraphDimention.innerRadius}
                                fill="#8884d8"
                                dataKey="count" nameKey='title'

                              >
                              {/* //(this.state.opacity.key == `${entry.title}-linkedin`) ? 1 : 0.4 */}
                                {
                                  EngagementByChannelData.Linkedin.map((entry, index) => <this.props.Recharts.Cell fill={obj['LinkedinColors'][index % obj['LinkedinColors'].length]} key={index} opacity={Object.keys(this.state.opacity_linkedin).length > 0 ? this.state.opacity_linkedin.key == `${entry.title}-linkedin`?1 : 0.4 : 1} />)
                                }
                              </this.props.Recharts.Pie>
                              <this.props.Recharts.Tooltip content={this.customizeTooltip} />
                              <this.props.Recharts.Legend content={this.CustomizedLabel} iconType="circle" iconSize={10}  LegendFrom='linkedin' />
                            </this.props.Recharts.PieChart>
                            {/* line-n0 :1315 */}
                            <div className="donutChartforEngagement">
                              <div className={`background-socialIcon linkedin`}>
                                <i class={`fa fa-linkedin`}></i>
                              </div>

                              <div className={`value linkedin`}>
                                {EngagementByChannelData.Linkedin_total_count}
                              </div>

                            </div>
                          </div>
                          : <div className="no-Value-chart">   No data available for Linkedin</div>}
                      </div>

                    </div>

                    <div className={`social-graph-block social-graph-tw ${EngagementByChannelData.Internal.length > 0 ?  '' :'noValueDataRecharts'}`}>
                      <div id='engagementChartWrapper' className={`engagementChartWrapperContainer donut-engagement-bychannel-${3}`}>
                        {EngagementByChannelData.Internal.length > 0 ?
                          <div>
                            <this.props.Recharts.PieChart
                              width={GraphDimention.width}
                              height={GraphDimention.height} >
                              <this.props.Recharts.Pie
                                data={EngagementByChannelData.Internal}
                                cx={GraphDimention.cx}
                                cy={GraphDimention.cy}
                                labelLine={false}
                                outerRadius={GraphDimention.outerRadius}
                                innerRadius={GraphDimention.innerRadius}
                                fill="#8884d8"
                                dataKey="count" nameKey='title'

                              >
                                {
                                  EngagementByChannelData.Internal.map((entry, index) => <this.props.Recharts.Cell fill={obj['InternalColors'][index % obj['InternalColors'].length]} key={index} opacity={Object.keys(this.state.opacity_internal).length > 0 ? this.state.opacity_internal.key == `${entry.title}-internal`?1 : 0.4 : 1} />)
                                }
                              </this.props.Recharts.Pie>
                              <this.props.Recharts.Tooltip content={this.customizeTooltip} />
                              <this.props.Recharts.Legend content={this.CustomizedLabel} iconType="circle" iconSize={10}  LegendFrom='internal'/>
                            </this.props.Recharts.PieChart>
                            {/* line-n0 :1315 */}
                            <div className="donutChartforEngagement">
                              <div className={`background-socialIcon internal`}>
                                <i class="material-icons">vpn_lock</i>
                              </div>

                              <div className={`value internal`}>
                                {EngagementByChannelData.Internal_total_count}
                              </div>

                            </div>
                          </div>
                          : <div className="no-Value-chart">  No data available for Internal</div>}
                      </div>
                    </div>


                    <div className={`social-graph-block social-graph-tw ${EngagementByChannelData.Facebook.length > 0 ? '' : 'noValueDataRecharts'}`}>
                      <div id='engagementChartWrapper' className={`engagementChartWrapperContainer donut-engagement-bychannel-${4}`}>
                        {EngagementByChannelData.Facebook.length > 0 ?
                          <div>
                            <this.props.Recharts.PieChart
                              width={GraphDimention.width}
                              height={GraphDimention.height} >
                              <this.props.Recharts.Pie
                                data={EngagementByChannelData.Facebook}
                                cx={GraphDimention.cx}
                                cy={GraphDimention.cy}
                                labelLine={false}
                                outerRadius={GraphDimention.outerRadius}
                                innerRadius={GraphDimention.innerRadius}
                                fill="#8884d8"
                                dataKey="count" nameKey='title'

                              >
                                {
                                  EngagementByChannelData.Facebook.map((entry, index) => <this.props.Recharts.Cell fill={obj['FacebookColors'][index % obj['FacebookColors'].length]} key={index} opacity={Object.keys(this.state.opacity_facebook).length > 0 ? (this.state.opacity_facebook.key == `${entry.title}-facebook`) ? 1 : 0.4 : 1} />)
                                }
                              </this.props.Recharts.Pie>
                              <this.props.Recharts.Tooltip content={this.customizeTooltip} />
                              <this.props.Recharts.Legend content={this.CustomizedLabel} iconType="circle" iconSize={10}   LegendFrom='facebook'/>
                            </this.props.Recharts.PieChart>
                            {/* line-n0 :1315 */}
                            <div className="donutChartforEngagement">
                              <div className={`background-socialIcon facebook`}>
                                <i class={`fa fa-facebook`}></i>
                              </div>

                              <div className={`value facebook`}>
                                {EngagementByChannelData.Facebook_total_count}
                              </div>

                            </div>
                          </div>
                          : <div className="no-Value-chart"> No data available for facebook</div>}
                      </div>
                    </div>
                    
                  </div>
                  : ''}
                 </div>
                 
                </div>
                </div>
                </div>
            
            )
          }
        }
        
module.exports = EngagementByChannel