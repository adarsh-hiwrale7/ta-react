import * as utils from '../../../../utils/utils'
import GuestUserAnalytics from './GuestUserAnalytics'
const data = [
  {
    'office-location': 'axis00', count: 102
  },
  {
    'office-location': 'controls', count: 318
  },
  {
    'office-location': 'data', count: 24
  },
  {
    'office-location': 'events', count: 73
  },
  {
    'office-location': 'legend', count: 129
  },
  { 'office-location': 'operator', count: 401 },
];

const data1 = [{ name: 'Group A', value: 400 }, { name: 'Group B', value: 300 },
{ name: 'Group C', value: 300 }, { name: 'Group D', value: 200 }, { name: 'test', value: 401 },
{ name: 'test2', value: 41 },
{ name: 'test3', value: 211 },
{ name: 'test4', value: 21 }];
const COLORS = ['#4bcf99', '#ff595e', '#4b74e0', '#ffbf00', '#b87d7a', '#FF8042','#495867','#7FB069','#B8D8D8','#A93F55','#577399','#F9ADA0',"#ACF39D",'#19323C'];
const RADIAN = Math.PI / 180;

class AdoptionExpand extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
    }
  }
  componentDidMount() {
    var timezone = utils.clientTimezone();
    var ObjToSend = {
      'timezone': timezone,
      timespan: 'month'
    }
    this.props.analyticsActions.fetchLocationBaseAdoption(ObjToSend)
    this.props.analyticsActions.fetchGuestUserChart(ObjToSend)
    this.props.analyticsActions.fetchGuestUserActivity(ObjToSend)
  }
  CustomizedContent = (props) => {
    const { root, depth, x, y, width, height, index, payload, rank } = props;
    var colors = COLORS
    return (
      <g>
        <rect
          x={x}
          y={y}
          width={width}
          height={height}
          style={{
            fill: colors[Math.floor(index / root.children.length * 6)],
            stroke: '#fff',
          }}
        />
        <text
          x={x + width / 2}
          y={y + height / 2 + 7}
          textAnchor="middle"
          fill="#fff"
          fontSize={14}
        >
          {utils.capitalizeFirstLetter(props['office-location'])} : {props.value}
        </text>
      </g>
    );
  }

  renderLocationBaseLoader() {
    return (
      <div className="inner-adoption-chart">
        {/* loader add*/}
        {/* header-part */}
        <div className="header-analytics-wrapper clearfix header-loader-wrapper-analytics ">
          <div className="title loader-grey-line  loader-line-radius loader-line-height"></div>
          <div className="header-year-expand-wrapper loader-grey-line  loader-line-radius loader-line-height">
          </div>
        </div>
        {/* end header */}

        <div className="body-engagement-channel expand-body-chart-wrapper">
          <div className="inner-body-engagement-channel" id='leftSideChart'>
            <div className="TotalNetworkchartContainer TotalNetworkchartContainerLoader loader-grey-line">
            </div>
          </div>
        </div>
        {/* loader End */}
      </div>
    )
  }
  customizeTooltipForAdoption = props => {
    const {  payload } = props;
    var colors = COLORS
    if (payload.length > 0) {
      return (
        <div className='custom-tooltip'>
          <p className='label' style={{ color: colors[Math.floor(payload[0].payload.index / payload[0].payload.root.children.length * 6)] }} >
            {utils.capitalizeFirstLetter(payload[0].name)}: {payload[0].value}
          </p>
        </div>
      )
    }
}
  render() {
    var windowDimention = utils.windowSize()
    var GraphDimention = utils.getChartDimension(windowDimention.w, 'LocationGraph');
    var locationBaseAdoption = this.props.analyticsProps.locationBaseAdoption !== undefined && Object.keys(this.props.analyticsProps.locationBaseAdoption).length > 0 ? this.props.analyticsProps.locationBaseAdoption.locationBaseData !== undefined && Object.keys(this.props.analyticsProps.locationBaseAdoption.locationBaseData).length > 0 ? this.props.analyticsProps.locationBaseAdoption.locationBaseData : '' : ''
    var locationBaseLoader = this.props.analyticsProps.locationBaseAdoption !== undefined && Object.keys(this.props.analyticsProps.locationBaseAdoption).length > 0 ? this.props.analyticsProps.locationBaseAdoption.locationBaseLoader !== undefined ? this.props.analyticsProps.locationBaseAdoption.locationBaseLoader : '' : ''
    return (

      <div className="chart-container-in-analytics-wrapper">
        <div className="inner-chart-container-in-analytics-wrapper">
          <div className="row-container-of-the-chart-analytics">
            <div className="Backbutton"> <a onClick={this.props.closeAdoptionExpand}><i class="material-icons">keyboard_arrow_left</i> Back </a></div>
            {/* chart part1 */}
            <div className="adoption-chart totalPostNetworkchartContainer">
              {locationBaseLoader == true ? this.renderLocationBaseLoader() :
                <div className="inner-adoption-chart">
                  <div className="header-analytics-wrapper">
                    <div className="clearfix">
                      <div className="title"> Posts by location
                      <span className="svg-icon help-element"><svg xmlns="http://www.w3.org/2000/svg" data-tip data-for='locationchart' width="24" height="24" viewBox="0 0 24 24"><path d="M0 0h24v24H0z" fill="none"/><path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm1 17h-2v-2h2v2zm2.07-7.75l-.9.92C13.45 12.9 13 13.5 13 15h-2v-.5c0-1.1.45-2.1 1.17-2.83l1.24-1.26c.37-.36.59-.86.59-1.41 0-1.1-.9-2-2-2s-2 .9-2 2H8c0-2.21 1.79-4 4-4s4 1.79 4 4c0 .88-.36 1.68-.93 2.25z"/></svg></span>
                      </div>
                      {this.props.tooltip !== null ?
                      <this.props.tooltip place="right" type="light" effect='solid' id='locationchart' delayHide={100} delayUpdate={100}>
                          <div className="tooltip-wrapper">
                              <div className="inner-tooltip-wrapper">
                                  <div className="tooltip-header clearfix">
                                      <div className="analytics-tooltip-title">
                                          <span>Posts by location</span>
                                      </div>
                                      {/* <div className="analytics-tooltip-read-more">
                                        <a href="#">Read more<i class="material-icons">keyboard_arrow_right</i>
                                        </a>
                                        </div> */}

                                  </div>
                                  <div className="tooltip-about-wrapper">
                                      <span>ABOUT</span>
                                      <p>Review activity by location.</p>
                                  </div>
                              </div>

                            </div>
                        </this.props.tooltip>
                        : ''
                      }
                      <div className="filter-weekey-container clearfix">
                        <div className="filter-container">
                          <a className={`week-filter ${this.props.locationPeriod == 'week' ? 'active' : ''}`} onClick={this.props.locationTimeSpan.bind(this, 'week')}> Week </a>
                          <a className={`month-filter ${this.props.locationPeriod == undefined || this.props.locationPeriod == 'month' ? 'active' : ''}`} onClick={this.props.locationTimeSpan.bind(this, 'month')}>  Month  </a>
                          <a className={`year-filter ${this.props.locationPeriod == 'year' ? 'active' : ''}`} onClick={this.props.locationTimeSpan.bind(this, 'year')}>Year </a>
                        </div>
                      </div>
                    </div>
                  </div>
                  {/* End header part */}
                  {/* body part */}
                  <div className="body-engagement-channel expand-body-chart-wrapper">
                    <div className="inner-body-engagement-channel" id='leftSideChart'>
                      <div className={`TotalNetworkchartContainer ${locationBaseAdoption && this.props.Recharts && locationBaseAdoption.chart.length>0 ?'':'TotalNetworkchartContainerNoValueData'}`}>
                        {locationBaseAdoption && this.props.Recharts ?
                        locationBaseAdoption.chart.length>0?
                          <this.props.Recharts.Treemap
                            width={GraphDimention.width}
                            height={GraphDimention.height}
                            data={locationBaseAdoption.chart}
                            dataKey="count"
                            ratio={4 / 3}
                            stroke="#fff"
                            nameKey='office-location'
                            content={this.CustomizedContent}
                          >
                          <this.props.Recharts.Tooltip content={this.customizeTooltipForAdoption}/>
                          </this.props.Recharts.Treemap>
                          : <div className = "no-Value-chart">No data available</div>
                          : <div className = "no-Value-chart">No data available</div>}
                      </div>
                    </div>
                  </div>
                  {/* End body part */}

                </div>}
            </div>
            <GuestUserAnalytics
              tooltip={this.props.tooltip}
              analyticsProps={this.props.analyticsProps}
              Recharts={this.props.Recharts}
              guestPeriod={this.props.guestPeriod}
              guestTimeSpan={this.props.guestTimeSpan} />
          </div>
        </div>
      </div>

    )
  }
}

module.exports = AdoptionExpand