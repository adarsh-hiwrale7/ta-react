import { connect } from 'react-redux'
import PreLoader from '../../../PreLoader'
import Globals from '../../../../Globals'
import * as utils from '../../../../utils/utils'
import AnalyticsDashboardFilter from '../AnalyticsDashboardFilter'

var isDivFound=false
var GraphDimention={}
class AnalyticsAdoptionIndex extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      hideChart: false,
      GraphDimention:{}
    }
  }
  UpdatedLegend = (props) => {
    const { payload, customizeLegendValue } = props;
    if(customizeLegendValue.length>0){
    var LegendKeys = Object.keys(customizeLegendValue[0])
    return (
      <ul>
        {LegendKeys.length > 0 ? LegendKeys.map((entry, index) => (
          <li className="clearfix" key={index} onClick={this.hideSelectedLegend.bind(this, payload[index].dataKey)}>
            <span className="rounder-dot" style={{ background: payload[index].color }}></span>
            <span className={`title-active-user ${this.state.hideChart[payload[index].dataKey] !== undefined && this.state.hideChart[payload[index].dataKey] == true ? 'strikeLegend' : ''}`}> {payload[index].dataKey}  </span> <span className="text-no">{customizeLegendValue[0][payload[index].dataKey]}</span>  </li>
        )) : ''}
      </ul>
    );
        }
  }
  hideSelectedLegend(e) {
    if (this.state.hideChart[e] == true) {
      this.setState({
        hideChart: { ...this.state.hideChart, [e]: false }
      });
    } else {
      this.setState({
        hideChart: { ...this.state.hideChart, [e]: true }
      });
    }
  }
  renderLoder() {
    return (
      <div className="adoption-chart adoption-loader-wrapper-analytics">
        <div className="inner-adoption-chart">
          {/* header-part */}
          <div className="header-analytics-wrapper clearfix header-loader-wrapper-analytics ">
            <div className="title loader-grey-line  loader-line-radius loader-line-height"></div>
            <div className="header-year-expand-wrapper loader-grey-line  loader-line-radius loader-line-height">
            </div>
          </div>
          {/* end header */}
          <div class="data-part-analytics clearfix">
            <div class="big-no-adoption">
              <div class="big-no-adoption-wrapper loader-grey-line  loader-line-radius loader-line-height">
              </div>
            </div>
            <div class="data-adoption">
              <ul>
                <li class="clearfix active-user loader-grey-line  loader-line-radius loader-line-height">
                </li>
                <li class="clearfix no-active-user loader-grey-line  loader-line-radius loader-line-height">
                </li>
              </ul>
            </div>
          </div>
          <div class="chart-container">
            <div class="recentasset-wrapper" id='leftSideChart'>
              <div class="recharts-wrapper">
                <svg class="recharts-surface loader-grey-line  loader-line-radius loader-line-height" width="509" height="287" viewBox="0 0 509 287" version="1.1">
                </svg>
              </div>
            </div>
          </div>
          {/* end data part analytics */}
        </div>
      </div>
    )
  }
  customizeTooltipForAdoption = props => {
    const { payload } = props
    var chartDate=0
    if (payload.length > 0) {
        chartDate=payload[0].payload.date
    }
    return(
        <div>
            <div className='chartDate'>
                <div className='label'>
                   {this.props.customizeTooltip(chartDate)}
                </div>
            </div>
            <div className="internal-external-post-wrapper">
        {payload.length > 0
        ? payload.map((entry, i) => {
            return (
                this.state.hideChart[entry.dataKey] !== undefined && this.state.hideChart[entry.dataKey] == true && (entry.dataKey == 'Active users' || entry.dataKey == 'Non-active users') ? '' :
                    <div className={`custom-tooltip ${entry.dataKey=='Non-active users'?'internalPostLabel':''} ${payload.length == 1? 'internalPostWrapper':''}`} key={i}  >
                            <div className='label' style={{color:entry.color}}>{`${entry.dataKey} :${entry.value}`}</div>
                    </div>
            )
        })
        : ''}
        </div>
        </div>
    )
}
  render() {
    var windowDimention = utils.windowSize()
    GraphDimention =utils.getChartDimension(windowDimention.w,'adoptionChart')
    var adoptionData = this.props.analyticsProps !== undefined  && Object.keys(this.props.analyticsProps).length > 0 ? this.props.analyticsProps.adoptionData !== undefined && Object.keys(this.props.analyticsProps.adoptionData).length > 0 ? this.props.analyticsProps.adoptionData : '' : ''
    var adoptionLoder = this.props.analyticsProps !== undefined && Object.keys(this.props.analyticsProps).length > 0? this.props.analyticsProps.adoptionLoader : ''
    var days = adoptionData ? adoptionData.chart_data.length : 0
    var intervalDay = 0
    if (days > 31 && days <= 365) {
      intervalDay = 60
    } else if (days <= 31 && days >7) {
      intervalDay = 7
    } else {
      intervalDay = 0
    }
    var arrayLegendValue = []
    if (adoptionData !== '') {
      var objToAdd = {
        'Active users': adoptionData['Active users'],
        'Non-active users': adoptionData['Non-active users']
      }
      arrayLegendValue.push(objToAdd)
    }
    return (
      adoptionLoder ==false && this.props.Recharts ?
        <div className="adoption-chart">

          <div className="inner-adoption-chart">
            {/* header-part */}
            <div className="header-analytics-wrapper clearfix">
              <div className="title">Adoption<span className="svg-icon help-element"><svg xmlns="http://www.w3.org/2000/svg" data-tip data-for='adoption' width="24" height="24" viewBox="0 0 24 24"><path d="M0 0h24v24H0z" fill="none"/><path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm1 17h-2v-2h2v2zm2.07-7.75l-.9.92C13.45 12.9 13 13.5 13 15h-2v-.5c0-1.1.45-2.1 1.17-2.83l1.24-1.26c.37-.36.59-.86.59-1.41 0-1.1-.9-2-2-2s-2 .9-2 2H8c0-2.21 1.79-4 4-4s4 1.79 4 4c0 .88-.36 1.68-.93 2.25z"/></svg></span></div>
              {this.props.tooltip !== null ?

                <this.props.tooltip place="right" type="light" effect='solid' id='adoption' delayHide={100} delayUpdate={100}>
                  <div className="tooltip-wrapper">
                    <div className="inner-tooltip-wrapper">
                      <div className="tooltip-header clearfix">
                        <div className="analytics-tooltip-title">
                          <span>Adoption</span>
                        </div>

                      </div>
                      <div className="tooltip-about-wrapper">
                        <span>ABOUT</span>
                        <p>Review the platform adoption numbers by active and non active users over time. An active user is defined as someone who logs into the platform.</p>
                      </div>
                    </div>
                  </div>
                </this.props.tooltip>
                : ''
              }

            <div className="filter-weekey-container clearfix">
              <div className = "filter-container">
                <a className={`week-filter ${this.props.period=='week'?'active':''}`} onClick={this.props.adoptionTimeSpan.bind(this,'week')}> Week </a>
                <a className={`month-filter ${this.props.period == undefined || this.props.period=='month'?'active':''}`} onClick={this.props.adoptionTimeSpan.bind(this,'month')}>  Month  </a>
                <a className={`year-filter ${this.props.period=='year'?'active':''}`} onClick={this.props.adoptionTimeSpan.bind(this,'year')}>Year </a>
                <span className={`filterIcon ${this.props.parentState['filterData-adoption']!== undefined && Object.keys(this.props.parentState['filterData-adoption']).length>2? 'active':'' }`} onClick={this.props.openFilterPopup.bind(this)}>  <i class="material-icons">filter_list</i>  </span>
              </div>
             {this.props.isFilterClicked==true ? <AnalyticsDashboardFilter fetchFilterData={this.props.fetchFilterData} includeLocation={false} callFrom='adoption' period={this.props.period}/>:''}
             </div>
            </div>
            {/* end header part */}
            {/* active data part */}
            <div className="data-part-analytics clearfix">
              <div className="big-no-total-user">  <span className="big-no-adoption"> {adoptionData !== '' ? adoptionData.Users : 0} </span> Users  </div>
            </div>
            {/* active data part */}
            {/* Chart-start */}
            <div className="chart-container" >
              <div className='recentasset-wrapper' id='leftSideChart'>
                {adoptionData !== '' && GraphDimention !== undefined && Object.keys(GraphDimention).length>0?
                  <this.props.Recharts.AreaChart
                    width={GraphDimention.width}
                    height={GraphDimention.height}
                    data={adoptionData.chart_data}
                    margin={{ top: 0, right: 20, left: 0, bottom: 0 }}
                    >
                    <this.props.Recharts.CartesianGrid stroke="#f1f2f3" vertical={false} />
                    <this.props.Recharts.XAxis dataKey="date" tickFormatter={(date)=>this.props.convertTimeStampToDate(date,this.props)}  tickSize={0} minTickGap={12} tickMargin={10}/>
                    <this.props.Recharts.YAxis tickSize={0} tickMargin={10} />
                    <this.props.Recharts.Tooltip content={this.customizeTooltipForAdoption}/>
                    <this.props.Recharts.Legend verticalAlign="top" iconType='circle' iconSize={10} layout="vertical" content={this.UpdatedLegend} customizeLegendValue={arrayLegendValue} />
                    <defs>
                      <linearGradient id="splitColor" x1="10" y1="10" x2="20" y2="21">
                        <stop offset={0} stopColor="#6fbe94 " />
                        <stop offset={0} stopColor="#6fbe94" />
                      </linearGradient>
                      <linearGradient id="splitColortwo" x1="10" y1="10" x2="20" y2="21">
                        <stop offset={0} stopColor="#ff7a7e" />
                        <stop offset={0} stopColor="#ff7a7e" />
                      </linearGradient>
                    </defs>

                    <this.props.Recharts.Area type="monotone" stackId={'Non-active users'} dataKey="Non-active users" stroke="#ff7a7e" fill="url(#splitColortwo)" hide={this.state.hideChart['Non-active users']} />
                    <this.props.Recharts.Area type="monotone" stackId={'Active users'} dataKey="Active users" stroke="#6fbe94" fill="url(#splitColor)" hide={this.state.hideChart['Active users']} />
                  </this.props.Recharts.AreaChart> : ''}
              </div>
            </div>
            {/* chart-End */}
          </div>
        </div>
        : this.renderLoder()

    )
  }
}

function mapStateToProps(state) {
  return {}
}

function mapDispatchToProps(dispatch) {
  return {}
}

let connection = connect(
  mapStateToProps,
  mapDispatchToProps
)

module.exports = connection(AnalyticsAdoptionIndex)
