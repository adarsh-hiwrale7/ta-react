import * as utils from '../../../../utils/utils'
const data = [
  {
    'title': 'axis00', count: 102
  },
  {
    'title': 'controls', count: 318
  },
  {
    'title': 'data', count: 24
  },
  {
    'title': 'events', count: 73
  },
  {
    'title': 'legend', count: 129
  },
  { 'title': 'operator', count: 401 },
];
const COLORS = ['#4bcf99', '#ff595e', '#4b74e0', '#ffbf00', '#b87d7a', '#FF8042','#495867','#7FB069','#B8D8D8','#A93F55','#577399','#F9ADA0',"#ACF39D",'#19323C'];

class GuestUserAnalytics extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      opacity: {},
      expandLegend: false
    }
  }
  handleMouseEnter(o) {
    const { opacity } = this.state;
    this.setState({
      opacity: { key: o.value, val: 0.5 },
    });
  }

  handleMouseLeave(o) {
    const { opacity } = this.state;
    this.setState({
      opacity: [],
    });

  }
  CustomizedLegend = (props) => {
    const { payload } = props;
    return (
      <div className={props.expandLegend == true ? 'showMore legendUlWrapper' : 'legendUlWrapper'}>
        {props.expandLegend == true ?
          <div className="legendHeader clearfix">
            <div className="legendHeaderTitle">Tagged values</div>
            <div className="showLessBtnWrapper"> <a onClick={this.expandLegend.bind(this, false)}> <i class="material-icons">clear</i></a></div>
          </div>
          : ''
        }
        <ul>
          {payload.map((entry, index) => (
            props.expandLegend == false ?
              index <= 4 ?
                <li key={index} onMouseEnter={this.handleMouseEnter.bind(this, entry)} onMouseLeave={this.handleMouseLeave.bind(this, entry)}>
                  <div key={`item-${index}`} data-color={entry.color}>
                    <span className="rounder-dot" style={{ background: entry.color }}></span>
                    <span className="title-active-user">{utils.capitalizeFirstLetter(entry.value)}</span>
                    <span className="text-no"> {entry.payload.count}</span>
                  </div>
                </li>
                : ''
              :
              <div key={index} onMouseEnter={this.handleMouseEnter.bind(this, entry)} onMouseLeave={this.handleMouseLeave.bind(this, entry)}>
                <li key={`item-${index}`} data-color={entry.color}>
                  <span className="rounder-dot" style={{ background: entry.color }}></span>
                  <span className="title-active-user">{utils.capitalizeFirstLetter(entry.value)}</span>
                  <span className="text-no"> {entry.payload.count}</span>
                </li>
              </div>


          ))}
        </ul>
        {payload.length > 5 ?
          <div className="readMoreLi" >
            <a title={'+' + payload.length - 5 + ' more'} onClick={this.expandLegend.bind(this, true)}><span>...</span></a>
          </div>
          : ''
        }
      </div>
    );
  }
  expandLegend(action) {
    this.setState({ expandLegend: action })
  }
  renderGuestLoader() {
    return (
      <div className="inner-adoption-chart">
        {/* loader  */}
        <div className="header-analytics-wrapper clearfix header-loader-wrapper-analytics ">
          <div className="title loader-grey-line  loader-line-radius loader-line-height"></div>
          <div className="header-year-expand-wrapper loader-grey-line  loader-line-radius loader-line-height">
          </div>
        </div>
        <div className="loader-chart-adoption-body-part" id='guestDonutDiv'>
          <div className="chart-adoption clearfix">
            <div className="legend">
              <ul>
                <li className="loader-grey-line loader-line-radius loader-line-height legend-loader"> </li>
                <li className="loader-grey-line loader-line-radius loader-line-height legend-loader" > </li>
              </ul>
            </div>
            <div className="chart-loader">
              <div className="chart-svg-loader loader-grey-line"> </div>
            </div>

          </div>
          <div className="userDetailsContainer loader-grey-line userDetailsContainerLoader">
            <div className="inneruserDetailsContainer clearfix">
              <div className="loader-grey-line"> </div>
            </div>
          </div>
        </div>
        {/* end loader */}
      </div>
    )
  }
  customizeTooltip = props => {
    const { payload, label } = props
    if (payload.length > 0) {
      return (
        <div className='custom-tooltip'>
          <p className='label' style={{ color: payload[0].payload.fill }}>
            {utils.capitalizeFirstLetter(payload[0].name)}: {payload[0].value}
          </p>
        </div>
      )

    }
  }
  render() {
    var windowDimention = utils.windowSize()
    var guestGraphDimention = utils.getChartDimension(windowDimention.w, 'guestDonutGraph');
    var guestUserChartLoader = this.props.analyticsProps.guestUserChart !== undefined && Object.keys(this.props.analyticsProps.guestUserChart).length > 0 ? this.props.analyticsProps.guestUserChart.guestUserChartLoader !== undefined ? this.props.analyticsProps.guestUserChart.guestUserChartLoader : '' : ''
    var guestUserChartData = this.props.analyticsProps.guestUserChart !== undefined && Object.keys(this.props.analyticsProps.guestUserChart).length > 0 ? this.props.analyticsProps.guestUserChart.guestUserChartData !== undefined && Object.keys(this.props.analyticsProps.guestUserChart.guestUserChartData).length > 0 ? this.props.analyticsProps.guestUserChart.guestUserChartData : '' : ''
    var guestUserActivityData = this.props.analyticsProps.guestUserActivity !== undefined && Object.keys(this.props.analyticsProps.guestUserActivity).length > 0 ? this.props.analyticsProps.guestUserActivity.guestUserActivityData !== undefined && Object.keys(this.props.analyticsProps.guestUserActivity.guestUserActivityData).length > 0 ? this.props.analyticsProps.guestUserActivity.guestUserActivityData : '' : ''
    var guestUserActivityLoader = this.props.analyticsProps.guestUserActivity !== undefined && Object.keys(this.props.analyticsProps.guestUserActivity).length > 0 ? this.props.analyticsProps.guestUserActivity.guestUserActivityLoader !== undefined ? this.props.analyticsProps.guestUserActivity.guestUserActivityLoader : '' : ''
    return (

      <div className="total-post-chart totalPostNetworkchartContainer">
        {guestUserChartLoader == true && guestUserActivityLoader == true ? this.renderGuestLoader() :
          <div className="inner-adoption-chart total-guest-data">
            {/* header-part */}

            <div className="header-analytics-wrapper clearfix">
              <div className="title"> Total Guest
              <span className="svg-icon help-element"><svg xmlns="http://www.w3.org/2000/svg" data-tip data-for='guestuser' width="24" height="24" viewBox="0 0 24 24"><path d="M0 0h24v24H0z" fill="none"/><path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm1 17h-2v-2h2v2zm2.07-7.75l-.9.92C13.45 12.9 13 13.5 13 15h-2v-.5c0-1.1.45-2.1 1.17-2.83l1.24-1.26c.37-.36.59-.86.59-1.41 0-1.1-.9-2-2-2s-2 .9-2 2H8c0-2.21 1.79-4 4-4s4 1.79 4 4c0 .88-.36 1.68-.93 2.25z"/></svg></span>
               </div>
               {this.props.tooltip !== null ?
                <this.props.tooltip place="right" type="light" effect='solid' id='guestuser' delayHide={100} delayUpdate={100}>
                    <div className="tooltip-wrapper">
                        <div className="inner-tooltip-wrapper">
                            <div className="tooltip-header clearfix">
                                <div className="analytics-tooltip-title">
                                    <span>Total Guest</span>
                                </div>
                                {/* <div className="analytics-tooltip-read-more">
                                  <a href="#">Read more<i class="material-icons">keyboard_arrow_right</i>
                                  </a>
                                  </div> */}

                            </div>
                            <div className="tooltip-about-wrapper">
                                <span>ABOUT</span>
                                <p>Review the platform adoption numbers for guest users over time.</p>
                            </div>
                        </div>

                      </div>
                  </this.props.tooltip>
                  : ''
                }
            </div>
            {/* End header part */}
            {/* body part */}
            <div className="data-part-analytics clearfix">
              <div className="big-no-total-user">  <span className="big-no-adoption"> {guestUserChartData !== '' ? guestUserChartData.total_guest:0} </span> Guest Users </div>
            </div>
            <div className="body-engagement-channel expand-body-chart-wrapper expand-overtime-container">
              <div className="inner-body-engagement-channel" id='rightSideChart'>
                <div className={`overtime-social-chart ${this.props.Recharts && guestUserChartData.chart && guestUserChartData.chart.length>0 ? '' : 'overtime-social-chart-no-data-value'}`}>
                  {this.props.Recharts && guestUserChartData.chart && guestUserChartData.chart.length>0 ?
                    <this.props.Recharts.PieChart width={guestGraphDimention.width} height={guestGraphDimention.height} onMouseEnter={this.onPieEnter}>
                      <this.props.Recharts.Pie
                        data={guestUserChartData.chart}
                        cx={guestGraphDimention.cx}
                        cy={guestGraphDimention.cy}
                        labelLine={false}
                        outerRadius={guestGraphDimention.outerRadius}
                        fill="#8884d8"
                        dataKey="count" nameKey='title'
                      >
                        {
                          guestUserChartData.chart.map((entry, index) => <this.props.Recharts.Cell fill={COLORS[index % COLORS.length]} key={index} opacity={Object.keys(this.state.opacity).length > 0 ? (this.state.opacity.key == entry.title) ? 1 : 0.4 : 1} />)
                        }
                      </this.props.Recharts.Pie>
                      <this.props.Recharts.Tooltip  content={this.customizeTooltip}/>
                      <this.props.Recharts.Legend iconType="circle" iconSize={10} align='center' content={this.CustomizedLegend} expandLegend={this.state.expandLegend} />
                    </this.props.Recharts.PieChart>
                    : <div className = "no-Value-chart">No data available</div>}
                </div>
                <div className="userDetailsContainer">
                  <div className="inneruserDetailsContainer clearfix">
                    <div className="user-details-container">
                      <div className="inner-user-details-container">
                        <div className="img-women">
                          <img src="/img/totalPostwomen.png" />
                        </div>
                        <div className="value-data">{Math.round(guestUserActivityData.Female)}% Female </div>
                      </div>
                    </div>
                    <div className="user-details-container">
                      <div className="inner-user-details-container">
                        <div className="img-women img-man">
                          <img src="/img/TotalPostMAn.png" />
                        </div>
                        <div className="value-data"> {Math.round(guestUserActivityData.Male)}% Male </div>
                      </div>
                    </div>
                    <div className="user-details-container average-active-user-container">
                      <div className="inner-user-details-container">
                        <div className="img-women">
                          <p className="average-active-user">  Average active user age </p>
                          <p className="average-active-user-value"> <a> {guestUserActivityData.Age} </a></p>
                        </div>

                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {/* End body part */}
          </div>}
      </div>

    )
  }
}

module.exports = GuestUserAnalytics