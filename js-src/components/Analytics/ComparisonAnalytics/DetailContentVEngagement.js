import { Component } from 'react'
import reactable from '../../Chunks/ChunkReactable'
import * as utils from '../../../utils/utils'
import Globals from '../../../Globals'
import moment from '../../Chunks/ChunkMoment'
import SliderPopup from '../../SliderPopup'
import PopupWrapper from '../../PopupWrapper'
import ImageLoader from 'react-imageloader'
import PreLoaderMaterial from '../../PreLoaderMaterial'
import AnalyticsPopup from '../AnalyticsPopup'
import PerfectScrollbar from '../../Chunks/ChunkPerfectScrollbar'
export default class DetailContentVEngagement extends Component {
  constructor (props) {
    super(props)

    this.state = {
      perPage: 10,
      reactable: null,
      moment: null,
      openTemplatePreviewPopup: false,
      previewLink: '',
      side_popup: false,
      selectedPopupData: null,
      perfectScrollbar:null
    }
  }
  componentWillMount () {
    let scrollbarSelector2 = '.analyticPopup';
    reactable().then(reactable => {
      moment().then(moment => {
        PerfectScrollbar().then(scrollbar=>{
          this.setState({ moment: moment, reactable: reactable ,perfectScrollbar:scrollbar },()=>{
            if(document.querySelectorAll(scrollbarSelector2).length>0){
              const ps = new this.state.perfectScrollbar.default(scrollbarSelector2, {
              wheelSpeed: 2,
              wheelPropagation: true,
              minScrollbarLength: 20,
              suppressScrollX:true
              });
            }
          })
        })
      })
    })
  }
  openTemplatePreviewPopupIsClicked (data) {
    this.setState({
      openTemplatePreviewPopup: true,
      previewLink: data
    })
    this.props.fetchPostData(data.type,data.identity)
  }
  openTemplatePreviewPopup () {
    var html_Preview_campaigns = ''
    var templatePreview = ''
    document.body.classList.add('overlay')
    var media_url =
      'https://visibly-staging.s3.eu-west-2.amazonaws.com/pirates_com/content_print/1567686479066.pdf'
    if (this.state.previewLink.type == 'print') {
      if (this.state.previewLink.pdf_s3_url) {
        media_url = this.state.previewLink.pdf_s3_url
      }
    } else if(this.state.previewLink.type == 'email') {
      if(this.state.previewLink.preview !== null ){
        html_Preview_campaigns =  this.state.previewLink.preview 
        templatePreview = decodeURIComponent(escape(atob(html_Preview_campaigns)))
      }
    }
    let analyticsPopupCount=this.props.contentCounts.printEmailAnalyticsCountData !== undefined ?this.props.contentCounts.printEmailAnalyticsCountData:''
    return (
        <SliderPopup className='analyticPopup'>
          {this.state.previewLink.type !== 'post'? 
          <header class="heading">
            <h3>Performance of the magazine campaign</h3>
            {/* <h3>{this.state.previewLink.title}</h3> */}
          </header> :''}
            <button id='close-popup' className='btn-default'>
              <a onClick={this.closePopup.bind(this)}>
                <i className='material-icons'>clear</i>
              </a>
            </button>
          <PopupWrapper>
                  <div className='linkpreview-parent-class'>
                    {this.state.previewLink.type == 'print' ? (
                      <div>
                        <div className='comparison-right'>
                        {media_url ==''?
                          <div className='no-data-block'>No preview found.</div>
                          :
                          <iframe
                            className='pdf-preview detailContentVengage'
                            src={`https://docs.google.com/gview?url=${media_url}&embedded=true`}
                            // src={media_url}
                          />}
                        </div>
                      <div className='PostPerformanceColumn comparison-left'>
                      <div className='totalEngagementWrapper'>
                        <div className='totalEngagement'>
                          <strong> {analyticsPopupCount !== ''? analyticsPopupCount.magazine_count:0} </strong>
                          no. of magazine</div>
                      </div>
                      <div className='analyticPostDataWrapper'>
                      {this.state.reactable?
                        <this.state.reactable.Table>
                          <this.state.reactable.Tr>
                            <this.state.reactable.Td column='channel'>
                              <span className='tdInner'>
                                  Shipped Date
                              </span>
                            </this.state.reactable.Td>
                            <this.state.reactable.Td column='likes' className='likes'>
                              <span className='tdInner'>
                              {analyticsPopupCount !=='' ?  
                                this.state.moment !== null ? this.state.moment.unix(parseInt(analyticsPopupCount.shipped_date)).format('DD-MM-YYYY')
                                 : '':''}
                            </span>
                            </this.state.reactable.Td>
          
                          </this.state.reactable.Tr>
                          <this.state.reactable.Tr>
                            <this.state.reactable.Td column='channel'>
                              <span className='tdInner'>
                                Shipping address
                              </span>
                            </this.state.reactable.Td>
                            <this.state.reactable.Td column='likes' className='likes'>
                              <span className='tdInner'>
                                {analyticsPopupCount !=='' ? analyticsPopupCount.shipping_address :'-'}
                              </span>
                            </this.state.reactable.Td>
                          </this.state.reactable.Tr>
                        </this.state.reactable.Table>:''}
                      </div>
                    </div>
                    </div>
                    ) : this.state.previewLink.type =='email'?(
                      <div id='emailWrapper'>
                        <div className="comparison-right">
                        {templatePreview ==''?
                          <div className='no-data-block'>No preview found.</div>
                          :
                          <div
                            dangerouslySetInnerHTML={{ __html: templatePreview }}
                            className='inner-pdf-wrapper'
                          />
                          }
                        </div>
                        <div className="comparison-left">
                        <div className='PostPerformanceColumn'>
                      <div className='totalEngagementWrapper'>
                        <div className='totalEngagement'>
                          <strong> {analyticsPopupCount !== ''? analyticsPopupCount.email_count:0} </strong>
                          no. of email</div>
                      </div>
                      <div className='analyticPostDataWrapper'>
                      {this.state.reactable?
                        <this.state.reactable.Table>
                          <this.state.reactable.Tr>
                          <this.state.reactable.Td column='likes' className='likes'>
                              <strong className='analytic-label tdInner'>
                                Click
                              </strong>
                            </this.state.reactable.Td>
                            <this.state.reactable.Td column='channel'>
                              <span className='tdInner'>
                                {analyticsPopupCount !=='' ? analyticsPopupCount.total_clicks :'-'}
                              </span>
                            </this.state.reactable.Td>
                            
          
                          </this.state.reactable.Tr>
                          <this.state.reactable.Tr>
                          <this.state.reactable.Td column='likes' className='likes'>
                              <strong className='tdInner analytic-label'>
                                Open
                              </strong>
                            </this.state.reactable.Td>
                            <this.state.reactable.Td column='channel'>
                              <span className='tdInner'>
                                {analyticsPopupCount !=='' ? analyticsPopupCount.total_opens :'-'}
                              </span>
                            </this.state.reactable.Td>
                            
                          </this.state.reactable.Tr>
                          <this.state.reactable.Tr>
                          <this.state.reactable.Td column='likes' className='likes'>
                              <strong className='tdInner analytic-label'>
                              Not Open
                              </strong>
                            </this.state.reactable.Td>
                            <this.state.reactable.Td column='channel'>
                              <span className='tdInner'>
                                {analyticsPopupCount !=='' ? analyticsPopupCount.total_non_opens :'-'}
                              </span>
                            </this.state.reactable.Td>
                          </this.state.reactable.Tr>
                        </this.state.reactable.Table>:''}
                      </div>
                    </div>
                    </div>
                      </div>
                    ):this.props.postData.postSelectedData.length>0?
                       <AnalyticsPopup
                          popupData={this.props.postData.postSelectedData[0]}
                          location = {location}
                          dataType = 'post'
                          props = {this.props.props}
                        />:''}
                  </div>
                  </PopupWrapper>
                  </SliderPopup>
                  
    )
  }
  closePopup() {
    this.setState({
        side_popup: false,
        openTemplatePreviewPopup: false,
      previewLink: {}
    })
    document.body.classList.remove('overlay')
}
  renderPostPreview(contentdata){
    if(contentdata.thumbnail_url == null){
      return (
        <div className='archive-container post-preview-block'>
          <div className='vid asset-file imgwrapper'>
          <div className='inner_imgwrapper assetInnerWrapper'>
              <a  class="type-image">
                <div class={"thumbWrap asset-file-doc  file file-doc thumbnail docx" }>
                  <i class='fa fa-file-text-o fa-3x'></i>
                </div>
              </a>
          </div>
        </div>
        </div>
      )
    }else{
      return(
        <div className='full-container'>
          <div className='archive-container clearfix'>
            <div className='vid file-image asset-file'>
                <div className='assetInnerWrapper inner_imgwrapper'>
                    <a className='type-image'>
                      <ImageLoader
                        src={contentdata.thumbnail_url}
                        preloader={PreLoaderMaterial}
                        className = "thumbnail"
                      >
                      <div className="image-load-failed">
                      <i class="material-icons no-img-found-icon">&#xE001;</i>
                      </div> 
          
                      </ImageLoader>
                    </a>
                </div>
            </div>
          </div>
        </div>
      )
    }

  }
  render () {
    let contentGraphDetail = this.props.contentGraphDetail

    return (
      <div className='widget'>
        {this.state.openTemplatePreviewPopup == true
          ? this.openTemplatePreviewPopup()
          : ''}
        <div className='table-wrapper'>
          {this.state.reactable &&
          contentGraphDetail.contentGraphDetailData !== undefined &&
          contentGraphDetail.contentGraphDetailData.length > 0 ? (
            <this.state.reactable.Table
                className='table contentEngage-list-table responsive-table'
                id='contentEngage-table'
                itemsPerPage={10}
                pageButtonLimit={5}
              >
                <this.state.reactable.Thead>
                <this.state.reactable.Th
                    column='id'
                    className='a-center department-id-col'
                  >
                  Id
                  </this.state.reactable.Th>
                <this.state.reactable.Th
                    column='title'
                    className='a-center department-id-col'
                  >
                  Title
                  </this.state.reactable.Th>
                <this.state.reactable.Th column='type'>
                  Type
                  </this.state.reactable.Th>
                <this.state.reactable.Th className='a-center' column='preview'>
                  Preview
                  </this.state.reactable.Th>
                <this.state.reactable.Th column='date' className='a-center'>
                  Date
                  </this.state.reactable.Th>
              </this.state.reactable.Thead>
                {contentGraphDetail.contentGraphDetailData.map(
                  (contentdata, i) => {
                    let templatePreviewDefault = ''
                    if (contentdata.preview) {
                      templatePreviewDefault = decodeURIComponent(
                        escape(atob(contentdata.preview))
                      )
                    } else {
                      let templateFirstScreen = ''
                      if (
                        contentdata.type == 'print' ||
                      contentdata.type == 'email'
                      ) {
                        templateFirstScreen = `<div class="first-pdf-page">
             <div class="pdf-wrapper">
                 <div class="container">
                     <div class="pdf-background">
                         <img src="https://magazine.visibly.io/images/pdf.png" alt="pdf-background" />
                     </div>
                 </div>
             </div>
             <div class="pdf-content">
                 <div class="container">
                     <div class="pdf-content-heading">
                         <h2>Admin message</h2>
                     </div>
                     <div class="content-paragraph">
                         <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been
                             the
                             industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of
                             type
                             and scrambled it to make a type specimen book. It has survived not only five<br /><br /><br />

                             centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It
                             was
                             popularised in the 1960s with the release of Letraset sheets containing Lorem Curabitur
                             pellentesque ut risus auctor varius. Integer imperdiet viverra neque, vel pharetra lacus
                             sagittis at. Utvariusvel tortor quis sagit-tis. Donec risus nisi, hendrerit sed mollis quis,
                             semper ut ligula. Cras pulvinar molestie neque, nec dapibus diam tempus vitae. Fusce
                             consectetur, ex nec venenatis consequat, diam quam semper diam, id porta lacus ex et nulla. Cras
                             commodo, augue nec commodo cursus, mi justo mattis ante, vitae varius orci massa a eros. Sed
                             hendrerit euismod leo. Curabitur pellentesque ut risus auctor varius. Integer imperdiet viverra
                             neque, vel pharetra lacus sagittis at. Utvariusvel tortor quis sagit-tis. Donec risus nisi,
                             hendrerit sed mollis quis, semper ut ligula. Cras pulvinar molestie neque, nec dapibus diam
                             tempus vitae. Fusce consectetur, ex nec venenatis consequat, diam quam semper diam, id porta
                             lacus ex et nulla. Cras commodo, augue nec commodo cursus, mi justo mattis ante, vitae varius
                             orci massa a eros. Sed hendrerit euismod leo.</p>
                     </div>
                 </div>
             </div>
             </div> `
                      }

                      let tempObj = btoa(
                        unescape(encodeURIComponent(templateFirstScreen))
                      )
                      templatePreviewDefault = decodeURIComponent(
                        escape(atob(tempObj))
                      )
                    }
                    return (
                      <this.state.reactable.Tr
                        key={i}
                        onClick={this.openTemplatePreviewPopupIsClicked.bind(
                          this,
                          contentdata
                        )}
                      >
                        <this.state.reactable.Td
                          className='fd a-center contentdata-id-col'
                          column='id'
                          data-rwd-label='ID'
                        >
                          {i + 1}
                        </this.state.reactable.Td>
                        <this.state.reactable.Td
                          className='fd'
                          column='title'
                          data-rwd-label='Title'
                        >
                          {utils.limitWords(
                            contentdata.title,
                            Globals.LIMIT_WORD
                          )}
                        </this.state.reactable.Td>
                        <this.state.reactable.Td
                          className='fd'
                          column='type'
                          data-rwd-label='Type'
                        >
                          {contentdata.type}
                        </this.state.reactable.Td>
                        <this.state.reactable.Td
                          className='a-center asset-detail '
                          column='preview'
                          data-rwd-label='Preview'
                          data={contentdata.type == 'post' ? this.renderPostPreview(contentdata):
                            <div className=' archive-container clearfix table-email-print-preview'>
                              <div className='vid file-image asset-file imgwrapper'>
                                <div className='inner_imgwrapper assetInnerWrapper'>
                                  <div
                                    dangerouslySetInnerHTML={{
                                      __html: templatePreviewDefault
                                    }}
                                    className='inner-pdf-wrapper'
                                  />
                                </div>
                              </div>
                            </div>
                          }
                        />
                        <this.state.reactable.Td
                          className='a-center'
                          column='date'
                          data-rwd-label='Date'
                        >
                          {this.state.moment !== null
                            ? this.state.moment
                              .unix(parseInt(contentdata.created_at))
                              .format('DD-MM-YYYY')
                            : ''}
                        </this.state.reactable.Td>
                      </this.state.reactable.Tr>
                    )
                  }
                )}
              </this.state.reactable.Table>
            ) : (
              ''
            )}
        </div>
      </div>
    )
  }
}
