import { Component } from 'react'
import { connect } from 'react-redux'
import Header from '../../Header/Header'
import GuestUserRestrictionPopup from '../../GuestUserRestrictionPopup'
import * as utils from '../../../utils/utils'
import recharts from '../../Chunks/ChunkRecharts'
import reactTooltip from '../../Chunks/ChunkReactTooltip'
import moment from '../../Chunks/ChunkMoment'
import * as analyticsActions from '../../../actions/analytics/analyticsActions'
import SliderPopup from '../../SliderPopup'
import { bindActionCreators } from 'redux'
import DetailContentVEngagement from './DetailContentVEngagement'
import * as notificationAction from '../../../actions/notificationActions'
import AnalyticsNav from '../AnalyticsNav';
var screenWidth = ''
const COLORS = ['#ff5e62', '#2fbf71', '#256eff','#ffbf00', '#cc6699', '#ff6a00', '#ffd700', '#b0e0e6', '#7A9E9F', '#254441', '#F9ADA0', '#577399', '#A93F55', '#ff7373','#ff9966', '#ccff99', '#99ffcc', '#33ccff', '#33cccc', '#00cc99', '#ccffcc', '#ffcccc', '#ffcc99', '#b3d9ff' ,'#e6e6fa', '#808080']

export default class ComparisonIndex extends Component {
  constructor(props) {
    super(props)
    this.state = {
      Recharts: null,
      tooltip: null,
      moment: null,
      hideChart: { Productivity: true, Capability: true, Innovation: true, CustomerExperience: true, Advocacy: true },
      period: 'month',
      ProductivityDriverList: false,
      CapabilityDriverList: false,
      InnovationDriverList: false,
      CustomerExperienceDriverList: false,
      AdvocacyDriverList: false,
      idsToShowDetail: [],
      openContentEngageDetailTable: false
    }
  }
  componentWillMount() {
    moment()
      .then(moment => {
        recharts().then(recharts => {
          reactTooltip().then(reactTooltip => {
            this.setState({
              Recharts: recharts,
              tooltip: reactTooltip,
              moment: moment
            })
          })
        })
      })
      .catch(err => {
        throw err
      })
  }
  componentDidMount() {
    var e = this
    window.addEventListener(
      'resize',
      function () {
        var width = document.body.clientWidth
        var height = document.body.clientHeight
        if (screenWidth < 1500 && width >= 1500) {
          screenWidth = width
          e.setState({ refreshChart: true })
        } else if (screenWidth > 1500 && width <= 1500) {
          screenWidth = width
          e.setState({ refreshChart: true })
        } else if (screenWidth > 767 && width <= 767) {
          screenWidth = width
          e.setState({ refreshChart: true })
        } else if (screenWidth < 767 && width >= 767) {
          screenWidth = width
          e.setState({ refreshChart: true })
        } else {
          if (e.state.refreshChart == true) {
            e.setState({ refreshChart: false })
          }
          screenWidth = width
        }
      },
      true
    )
    this.props.analyticsActions.getContentVEngageData(this.state.period)
    this.setState({
      hideData: {
        ...this.state.hideData,
        Optimisation: true,
        Clarity: true,
        Coordination: true,
        Decisiveness: true,
        Enablement: true,
        Empowerment: true,
        Performance: true,
        Processes: true,
        Prioritisation: true,
        Inclusiveness: true,
        Safety: true,
        Encouragement: true,
        Team_orientated: true,
        Customer_centric: true,
        Responsive: true,
        Collaborative: true,
        Belief: true,
        Fulfillment: true,
        Integrity: true,
        Alignment: true
      }
    })
  }
  renderLoading() {
    return (
      <div className='culture-index-loader'>
        <div className='culture-score'>
          <div className='culture-score-header clearfix'>
            <div className='culture-score-wrapper loader-grey-line loader-line-radius loader-line-height' />
            <div className='expand-wrapper loader-grey-line loader-line-radius loader-line-height' />
          </div>
          <div className='culture-score-number-value-wrapper clearfix'>
            <div className='culture-score-number'>
              <div className='culture-index-number loader-grey-line loader-line-radius loader-line-height' />
            </div>
            <div className='culture-score-value'>
              <div className='cultur-score-value-wrapper'>
                <div className='culture-value-icon-content clearfix'>
                  <div className='culture-value-icon loader-grey-line loader-line-radius loader-line-height' />
                  <div className='culture-score-value-content'>
                    <div className='promoted-value loader-grey-line loader-line-radius loader-line-height' />
                    <div className='promoted-value-wrapper loader-grey-line loader-line-radius loader-line-height' />
                  </div>
                </div>
                <div className='culture-value-icon-content clearfix'>
                  <div className='culture-value-icon loader-grey-line loader-line-radius loader-line-height' />
                  <div className='culture-score-value-content'>
                    <div className='promoted-value loader-grey-line loader-line-radius loader-line-height' />

                    <div className='promoted-value-wrapper loader-grey-line loader-line-radius loader-line-height' />
                  </div>
                </div>
              </div>
              <div className='culture-score-percentage'>
                <div className='culture-score-percentage-wrapper loader-grey-line loader-line-radius loader-line-height' />
                <div className='culture-score-percentage-wrapper loader-grey-line loader-line-radius loader-line-height' />
              </div>
            </div>
          </div>
          <div className='culture-score-graph-wrapper clearfix'>
            <div className='culture-score-date-time-graph'>
              <div className='culture-score-graph-img-loader loader-grey-line loader-line-radius loader-line-height' />
              <div className='culture-score-date loader-grey-line loader-line-radius loader-line-height' />
            </div>
            <div className='culture-score-detractors csClm'>
              <div className='culture-score-detractors-count cscnt'>
                <svg
                  class='detractors-loader loader-grey-line loader-line-radius loader-line-height'
                  xmlns='http://www.w3.org/2000/svg'
                  width='24'
                  height='24'
                  viewBox='0 0 24 24'
                />
                <label className='detractors-count loader-grey-line loader-line-radius loader-line-height' />
              </div>
              <div className='detractors-content loader-grey-line loader-line-radius loader-line-height' />
            </div>
            <div className='culture-score-passives csClm'>
              <div className='culture-score-passives-count cscnt'>
                <svg
                  class='detractors-loader loader-grey-line loader-line-radius loader-line-height'
                  xmlns='http://www.w3.org/2000/svg'
                  width='24'
                  height='24'
                  viewBox='0 0 24 24'
                />
                <label class='passives-count loader-grey-line loader-line-radius loader-line-height' />
              </div>
              <div className='passives-content loader-grey-line loader-line-radius loader-line-height' />
            </div>
            <div className='culture-score-promotors csClm'>
              <div className='culture-score-promotors-count cscnt'>
                <i class='material-icons culture-count-loader loader-grey-line loader-line-radius loader-line-height' />
                <label class='promotors-count loader-grey-line loader-line-radius loader-line-height' />
              </div>
              <div className='promotors-content loader-grey-line loader-line-radius loader-line-height' />
            </div>
            <div className='culture-score-promotors csClm'>
              <div className='culture-score-promotors-count cscnt'>
                <i class='material-icons culture-count-loader loader-grey-line loader-line-radius loader-line-height' />
                <label class='promotors-count loader-grey-line loader-line-radius loader-line-height' />
              </div>
              <div className='promotors-content loader-grey-line loader-line-radius loader-line-height' />
            </div>
            <div className='culture-score-promotors csClm'>
              <div className='culture-score-promotors-count cscnt'>
                <i class='material-icons culture-count-loader loader-grey-line loader-line-radius loader-line-height' />
                <label class='promotors-count loader-grey-line loader-line-radius loader-line-height' />
              </div>
              <div className='promotors-content loader-grey-line loader-line-radius loader-line-height' />
            </div>
          </div>
        </div>
      </div>
    )
  }

  customizeTooltip = (date, forTooltip = false) => {
    var period = this.state.period
    var dateFormat = period !== undefined ? period : 'month'
    var newFormat = ''
    if (dateFormat == 'month' || dateFormat == 'week') {
      newFormat = 'DD MMM'
    } else if (dateFormat == 'year') {
      newFormat = 'MMM YY'
    }
    if (forTooltip == true) {
      return this.state.moment
        ? this.state.moment.unix(date).format('DD-MM-YYYY')
        : ''
    }
    return this.state.moment
      ? this.state.moment.unix(date).format(newFormat)
      : ''
  }

  async hideSelectedLegend(e, callback) {
    if (e == 'advocacy') {
      var factorFlag = this.state.hideData[e] == undefined ? false : this.state.hideData[e]
      if (factorFlag == true && (this.state.ProductivityDriverList == true || this.state.CapabilityDriverList == true || this.state.InnovationDriverList == true || this.state.CustomerExperienceDriverList == true || this.state.AdvocacyDriverList == true)) {
        await this.setState({
          ProductivityDriverList: false,
          CapabilityDriverList: false,
          InnovationDriverList: false,
          CustomerExperienceDriverList: false,
          AdvocacyDriverList: false,
          hideData: {
            ...this.state.hideData,
            Optimisation: false,
            Clarity: true,
            Coordination: true,
            Decisiveness: true,
            Enablement: true,
            Empowerment: true,
            Performance: true,
            Processes: true,
            Prioritisation: true,
            Inclusiveness: true,
            Safety: true,
            Encouragement: true,
            Team_orientated: true,
            Customer_centric: true,
            Responsive: true,
            Collaborative: true,
            Belief: true,
            Fulfillment: true,
            Integrity: true,
            Alignment: true
          }
        })
      }
      await this.setState({
        hideData: { ...this.state.hideData, 'factor': factorFlag },
        hideChart: { ...this.state.hideChart, Productivity: factorFlag, Capability: factorFlag, Innovation: factorFlag, CustomerExperience: factorFlag, Advocacy: factorFlag },

      })
    }
    if (e == 'factor') {
      var advocacyFlag = this.state.hideData[e] == undefined ? false : this.state.hideData[e]
      await this.setState({
        hideData: { ...this.state.hideData, 'advocacy': advocacyFlag },
        hideChart: { ...this.state.hideChart, Productivity: !advocacyFlag, Capability: !advocacyFlag, Innovation: !advocacyFlag, CustomerExperience: !advocacyFlag, Advocacy: !advocacyFlag }
      })
    }
    if (this.state.hideData[e] == true) {
      this.setState({
        hideData: { ...this.state.hideData, [e]: false }
      })
    } else {
      this.setState({
        hideData: { ...this.state.hideData, [e]: true }
      })
    }
    if (typeof callback !== 'undefined') {
      callback(this, e);
    }
  }
  handleDriverClick(event) {
    this.setState({
      hideChart: {
        ...this.state.hideChart,
        [event]: !this.state.hideChart[event]
      }
    })
  }

  UpdatedLegend = props => {
    const { payload, countData } = props
    if (payload.length > 0) {
      var LegendKeys = Object.keys(payload)
      var contentDataList = ['email', 'print', 'sms', 'post']
      return (
        <div>
          <ul className="top-legend-wrapper">
            {payload.length > 0
              ? payload.map((entry, index) =>
                contentDataList.indexOf(entry.payload.name) > -1 ? (
                  <li
                    className='clearfix'
                    key={index}
                    onClick={this.hideSelectedLegend.bind(
                      this,
                      entry.payload.name
                    )}
                  >
                    <span
                      className='rounder-dot'
                      style={{ background: entry.color }}
                    />
                    <span
                      className={`title-active-user ${
                        this.state.hideData[entry.payload.name] !== undefined &&
                          this.state.hideData[entry.payload.name] == true
                          ? 'strikeLegend'
                          : ''
                        }`}
                    >
                      {' '}
                      {entry.payload.name}{' '}
                    </span>
                  </li>
                ) : ''
              )
              : ''}
          </ul>
          <ul className="bottom-legend-wrapper">
            {payload.length > 0
                ? payload.map((entry, index) =>
                  contentDataList.indexOf(entry.payload.name) == -1 && this.state.hideData[entry.payload.name] == undefined ? (
                    <li className='clearfix' key={index} title="asd">
                      <div className='culture-legend-li-inner'>
                        <span
                          className='rounder-dot'
                          style={{ background: entry.color }}
                        />
                        <span
                          className={`title-active-usenbr ${
                            this.state.hideChart[entry.payload.name] !==
                              undefined &&
                              this.state.hideChart[entry.payload.name] == true
                              ? 'strikeLegend'
                              : ''
                            }`}
                          onClick={this.handleDriverClick.bind(
                            this,
                            entry.payload.name
                          )}
                        >
                          {entry.payload.name.charAt(0).toUpperCase() +
                            entry.payload.name.slice(1)}{' '}
                        </span>
                        {/* <span className="text-no">{countData[entry.payload.name] !== undefined ? countData[entry.payload.name] : countData.engagement_score[entry.payload.name]}</span> */}
                        <span
                          className='clarrow'
                          onClick={this.clickMap.bind(this, entry.payload.name)}
                          id={`driverList-${entry.payload.name}`}
                        >
                          {' '}
                        </span>
                      </div>
                      {entry.payload.name == 'Productivity' ? (
                        this.state.ProductivityDriverList == true ? (
                          <ul>
                            <li
                              onClick={this.hideSelectedLegend.bind(
                                this,
                                'Optimisation'
                              )}
                            >
                              <span
                                className='rounder-dot'
                                style={{ background: COLORS[6] }}
                              />
                              <span
                                className={`title-active-user ${
                                  this.state.hideData['Optimisation'] !==
                                    undefined &&
                                    this.state.hideData['Optimisation'] == true
                                    ? 'strikeLegend'
                                    : ''
                                  }`}
                              >
                                {' '}
                                Optimisation{' '}
                              </span>{' '}
                              <span className='text-no'>
                                {countData['Optimisation']}
                              </span>
                            </li>
                            <li
                              onClick={this.hideSelectedLegend.bind(
                                this,
                                'Clarity'
                              )}
                            >
                              <span
                                className='rounder-dot'
                                style={{ background: COLORS[7] }}
                              />
                              <span
                                className={`title-active-user ${
                                  this.state.hideData['Clarity'] !== undefined &&
                                    this.state.hideData['Clarity'] == true
                                    ? 'strikeLegend'
                                    : ''
                                  }`}
                              >
                                {' '}
                                Clarity{' '}
                              </span>{' '}
                              <span className='text-no'>
                                {countData['Clarity']}
                              </span>
                            </li>
                            <li
                              onClick={this.hideSelectedLegend.bind(
                                this,
                                'Coordination'
                              )}
                            >
                              <span
                                className='rounder-dot'
                                style={{ background: COLORS[8] }}
                              />
                              <span
                                className={`title-active-user ${
                                  this.state.hideData['Coordination'] !==
                                    undefined &&
                                    this.state.hideData['Coordination'] == true
                                    ? 'strikeLegend'
                                    : ''
                                  }`}
                              >
                                {' '}
                                Coordination{' '}
                              </span>{' '}
                              <span className='text-no'>
                                {countData['Coordination']}
                              </span>
                            </li>
                            <li
                              onClick={this.hideSelectedLegend.bind(
                                this,
                                'Decisiveness'
                              )}
                            >
                              <span
                                className='rounder-dot'
                                style={{ background: COLORS[9] }}
                              />
                              <span
                                className={`title-active-user ${
                                  this.state.hideData['Decisiveness'] !==
                                    undefined &&
                                    this.state.hideData['Decisiveness'] == true
                                    ? 'strikeLegend'
                                    : ''
                                  }`}
                              >
                                {' '}
                                Decisiveness{' '}
                              </span>{' '}
                              <span className='text-no'>
                                {countData['Decisiveness']}
                              </span>
                            </li>
                          </ul>
                        ) : (
                            ''
                          )
                      ) : entry.payload.name == 'Capability' ? (
                        this.state.CapabilityDriverList == true ? (
                          <ul>
                            <li
                              onClick={this.hideSelectedLegend.bind(
                                this,
                                'Enablement'
                              )}
                            >
                              <span
                                className='rounder-dot'
                                style={{ background: COLORS[10] }}
                              />
                              <span
                                className={`title-active-user ${
                                  this.state.hideData['Enablement'] !==
                                    undefined &&
                                    this.state.hideData['Enablement'] == true
                                    ? 'strikeLegend'
                                    : ''
                                  }`}
                              >
                                {' '}
                                Enablement{' '}
                              </span>{' '}
                              <span className='text-no'>
                                {countData['Enablement']}
                              </span>
                            </li>
                            <li
                              onClick={this.hideSelectedLegend.bind(
                                this,
                                'Empowerment'
                              )}
                            >
                              <span
                                className='rounder-dot'
                                style={{ background: COLORS[11] }}
                              />
                              <span
                                className={`title-active-user ${
                                  this.state.hideData['Empowerment'] !== undefined &&
                                    this.state.hideData['Empowerment'] == true
                                    ? 'strikeLegend'
                                    : ''
                                  }`}
                              >
                                {' '}
                                Empowerment{' '}
                              </span>{' '}
                              <span className='text-no'>
                                {countData['Empowerment']}
                              </span>
                            </li>
                            <li
                              onClick={this.hideSelectedLegend.bind(
                                this,
                                'Performance'
                              )}
                            >
                              <span
                                className='rounder-dot'
                                style={{ background: COLORS[12] }}
                              />
                              <span
                                className={`title-active-user ${
                                  this.state.hideData['Performance'] !== undefined &&
                                    this.state.hideData['Performance'] == true
                                    ? 'strikeLegend'
                                    : ''
                                  }`}
                              >
                                {' '}
                                Performance{' '}
                              </span>{' '}
                              <span className='text-no'>
                                {countData['Performance']}
                              </span>
                            </li>
                            <li
                              onClick={this.hideSelectedLegend.bind(
                                this,
                                'Processes'
                              )}
                            >
                              <span
                                className='rounder-dot'
                                style={{ background: COLORS[13] }}
                              />
                              <span
                                className={`title-active-user ${
                                  this.state.hideData['Processes'] !== undefined &&
                                    this.state.hideData['Processes'] == true
                                    ? 'strikeLegend'
                                    : ''
                                  }`}
                              >
                                {' '}
                                Processes{' '}
                              </span>{' '}
                              <span className='text-no'>
                                {countData['Processes']}
                              </span>
                            </li>
                          </ul>
                        ) : (
                            ''
                          )
                      )
                          : entry.payload.name == 'Innovation' ? (
                            this.state.InnovationDriverList == true ? (
                              <ul>
                                <li
                                  onClick={this.hideSelectedLegend.bind(
                                    this,
                                    'Prioritisation'
                                  )}
                                >
                                  <span
                                    className='rounder-dot'
                                    style={{ background: COLORS[14] }}
                                  />
                                  <span
                                    className={`title-active-user ${
                                      this.state.hideData['Prioritisation'] !==
                                        undefined &&
                                        this.state.hideData['Prioritisation'] == true
                                        ? 'strikeLegend'
                                        : ''
                                      }`}
                                  >
                                    {' '}
                                    Prioritisation{' '}
                                  </span>{' '}
                                  <span className='text-no'>
                                    {countData['Prioritisation']}
                                  </span>
                                </li>
                                <li
                                  onClick={this.hideSelectedLegend.bind(
                                    this,
                                    'Inclusiveness'
                                  )}
                                >
                                  <span
                                    className='rounder-dot'
                                    style={{ background: COLORS[15] }}
                                  />
                                  <span
                                    className={`title-active-user ${
                                      this.state.hideData['Inclusiveness'] !==
                                        undefined &&
                                        this.state.hideData['Inclusiveness'] == true
                                        ? 'strikeLegend'
                                        : ''
                                      }`}
                                  >
                                    {' '}
                                    Inclusiveness{' '}
                                  </span>{' '}
                                  <span className='text-no'>
                                    {countData['Inclusiveness']}
                                  </span>
                                </li>
                                <li
                                  onClick={this.hideSelectedLegend.bind(
                                    this,
                                    'Safety'
                                  )}
                                >
                                  <span
                                    className='rounder-dot'
                                    style={{ background: COLORS[16] }}
                                  />
                                  <span
                                    className={`title-active-user ${
                                      this.state.hideData['Safety'] !==
                                        undefined &&
                                        this.state.hideData['Safety'] == true
                                        ? 'strikeLegend'
                                        : ''
                                      }`}
                                  >
                                    {' '}
                                    Safety{' '}
                                  </span>{' '}
                                  <span className='text-no'>
                                    {countData['Safety']}
                                  </span>
                                </li>
                                <li
                                  onClick={this.hideSelectedLegend.bind(
                                    this,
                                    'Encouragement'
                                  )}
                                >
                                  <span
                                    className='rounder-dot'
                                    style={{ background: COLORS[17] }}
                                  />
                                  <span
                                    className={`title-active-user ${
                                      this.state.hideData['Encouragement'] !==
                                        undefined &&
                                        this.state.hideData['Encouragement'] == true
                                        ? 'strikeLegend'
                                        : ''
                                      }`}
                                  >
                                    {' '}
                                    Encouragement{' '}
                                  </span>{' '}
                                  <span className='text-no'>
                                    {countData['Encouragement']}
                                  </span>
                                </li>
                              </ul>
                            ) : (
                              '')
                            ) : entry.payload.name == 'Customer Experience' ? (
                              this.state.CustomerExperienceDriverList == true ? (
                                <ul>
                                  <li
                                    onClick={this.hideSelectedLegend.bind(
                                      this,
                                      'Team orientated'
                                    )}
                                  >
                                    <span
                                      className='rounder-dot'
                                      style={{ background: COLORS[18] }}
                                    />
                                    <span
                                      className={`title-active-user ${
                                        this.state.hideData['Team orientated'] !==
                                          undefined &&
                                          this.state.hideData['Team orientated'] == true
                                          ? 'strikeLegend'
                                          : ''
                                        }`}
                                    >
                                      {' '}
                                      Team orientated{' '}
                                    </span>{' '}
                                    <span className='text-no'>
                                      {countData['Team orientated']}
                                    </span>
                                  </li>
                                  <li
                                    onClick={this.hideSelectedLegend.bind(
                                      this,
                                      'Customer-centric'
                                    )}
                                  >
                                    <span
                                      className='rounder-dot'
                                      style={{ background: COLORS[19] }}
                                    />
                                    <span
                                      className={`title-active-user ${
                                        this.state.hideData['Customer-centric'] !== undefined &&
                                          this.state.hideData['Customer-centric'] == true
                                          ? 'strikeLegend'
                                          : ''
                                        }`}
                                    >
                                      {' '}
                                      Customer-centric{' '}
                                    </span>{' '}
                                    <span className='text-no'>
                                      {countData['Customer-centric']}
                                    </span>
                                  </li>
                                  <li
                                    onClick={this.hideSelectedLegend.bind(
                                      this,
                                      'Responsive'
                                    )}
                                  >
                                    <span
                                      className='rounder-dot'
                                      style={{ background: COLORS[20] }}
                                    />
                                    <span
                                      className={`title-active-user ${
                                        this.state.hideData['Responsive'] !== undefined &&
                                          this.state.hideData['Responsive'] == true
                                          ? 'strikeLegend'
                                          : ''
                                        }`}
                                    >
                                      {' '}
                                      Responsive{' '}
                                    </span>{' '}
                                    <span className='text-no'>
                                      {countData['Responsive']}
                                    </span>
                                  </li>
                                  <li
                                    onClick={this.hideSelectedLegend.bind(
                                      this,
                                      'Collaborative'
                                    )}
                                  >
                                    <span
                                      className='rounder-dot'
                                      style={{ background: COLORS[21] }}
                                    />
                                    <span
                                      className={`title-active-user ${
                                        this.state.hideData['Collaborative'] !== undefined &&
                                          this.state.hideData['Collaborative'] == true
                                          ? 'strikeLegend'
                                          : ''
                                        }`}
                                    >
                                      {' '}
                                      Collaborative{' '}
                                    </span>{' '}
                                    <span className='text-no'>
                                      {countData['Collaborative']}
                                    </span>
                                  </li>
                                </ul>
                              ) : (
                                  ''
                                )
                            ) : entry.payload.name == 'Advocacy' ? (
                              this.state.AdvocacyDriverList == true ? (
                                <ul>
                                  <li
                                    onClick={this.hideSelectedLegend.bind(
                                      this,
                                      'Belief'
                                    )}
                                  >
                                    <span
                                      className='rounder-dot'
                                      style={{ background: COLORS[22] }}
                                    />
                                    <span
                                      className={`title-active-user ${
                                        this.state.hideData['Belief'] !==
                                          undefined &&
                                          this.state.hideData['Belief'] == true
                                          ? 'strikeLegend'
                                          : ''
                                        }`}
                                    >
                                      {' '}
                                      Belief{' '}
                                    </span>{' '}
                                    <span className='text-no'>
                                      {countData['Belief']}
                                    </span>
                                  </li>
                                  <li
                                    onClick={this.hideSelectedLegend.bind(
                                      this,
                                      'Fulfillment'
                                    )}
                                  >
                                    <span
                                      className='rounder-dot'
                                      style={{ background: COLORS[23] }}
                                    />
                                    <span
                                      className={`title-active-user ${
                                        this.state.hideData['Fulfillment'] !== undefined &&
                                          this.state.hideData['Fulfillment'] == true
                                          ? 'strikeLegend'
                                          : ''
                                        }`}
                                    >
                                      {' '}
                                      Fulfillment{' '}
                                    </span>{' '}
                                    <span className='text-no'>
                                      {countData['Fulfillment']}
                                    </span>
                                  </li>
                                  <li
                                    onClick={this.hideSelectedLegend.bind(
                                      this,
                                      'Integrity'
                                    )}
                                  >
                                    <span
                                      className='rounder-dot'
                                      style={{ background: COLORS[24] }}
                                    />
                                    <span
                                      className={`title-active-user ${
                                        this.state.hideData['Integrity'] !== undefined &&
                                          this.state.hideData['Integrity'] == true
                                          ? 'strikeLegend'
                                          : ''
                                        }`}
                                    >
                                      {' '}
                                      Integrity{' '}
                                    </span>{' '}
                                    <span className='text-no'>
                                      {countData['Integrity']}
                                    </span>
                                  </li>
                                  <li
                                    onClick={this.hideSelectedLegend.bind(
                                      this,
                                      'Alignment'
                                    )}
                                  >
                                    <span
                                      className='rounder-dot'
                                      style={{ background: COLORS[25] }}
                                    />
                                    <span
                                      className={`title-active-user ${
                                        this.state.hideData['Alignment'] !== undefined &&
                                          this.state.hideData['Alignment'] == true
                                          ? 'strikeLegend'
                                          : ''
                                        }`}
                                    >
                                      {' '}
                                      Alignment{' '}
                                    </span>{' '}
                                    <span className='text-no'>
                                      {countData['Alignment']}
                                    </span>
                                  </li>
                                </ul>
                              ) : (
                                  ''
                                )
                            ) 
                           : (
                              ''
                            )}
                    </li>) : ''
                ) : ''}
          </ul>
        </div>
      )
    }
  }
  clickMap(targetLine) {
    var me = this
    switch (targetLine) {
      case 'Productivity':
        if (this.state.ProductivityDriverList == true) {
          document
            .getElementById(`driverList-${targetLine}`)
            .classList.remove('active')
          this.setState({
            ProductivityDriverList: !this.state.ProductivityDriverList,
            hideData: {
              ...this.state.hideData,
              Optimisation: true,
              Clarity: true,
              Coordination: true,
              Decisiveness: true
            }
          })
        } else {
          document
            .getElementById(`driverList-${targetLine}`)
            .classList.add('active')
          this.setState({
            ProductivityDriverList: !this.state.ProductivityDriverList,
            hideData: {
              ...this.state.hideData,
              Optimisation: !this.state.hideData['Optimisation'],
              Clarity: !this.state.hideData['Clarity'],
              Coordination: !this.state.hideData['Coordination'],
              Decisiveness: !this.state.hideData['Decisiveness']
            }
          })
        }
        break
      case 'Capability':
        if (this.state.CapabilityDriverList == true) {
          document
            .getElementById(`driverList-${targetLine}`)
            .classList.remove('active')
          this.setState({
            CapabilityDriverList: !this.state.CapabilityDriverList,
            hideData: {
              ...this.state.hideData,
              Enablement: true,
              Empowerment: true,
              Performance: true,
              Processes: true
            }
          })
        } else {
          document
            .getElementById(`driverList-${targetLine}`)
            .classList.add('active')
          this.setState({
            CapabilityDriverList: !this.state.CapabilityDriverList,
            hideData: {
              ...this.state.hideData,
              Enablement: !this.state.hideData['Enablement'],
              Empowerment: !this.state.hideData['Empowerment'],
              Performance: !this.state.hideData['Performance'],
              Processes: !this.state.hideData['Processes']
            }
          })
        }
        break
      case 'Innovation':
        if (this.state.InnovationDriverList == true) {
          document
            .getElementById(`driverList-${targetLine}`)
            .classList.remove('active')
          this.setState({
            InnovationDriverList: !this.state.InnovationDriverList,
            hideData: {
              ...this.state.hideData,
              Prioritisation: true,
              Inclusiveness: true,
              Safety: true,
              Encouragement: true
            }
          })
        } else {
          document
            .getElementById(`driverList-${targetLine}`)
            .classList.add('active')
          this.setState({
            InnovationDriverList: !this.state.InnovationDriverList,
            hideData: {
              ...this.state.hideData,
              Prioritisation: !this.state.hideData['Prioritisation'],
              Inclusiveness: !this.state.hideData['Inclusiveness'],
              Safety: !this.state.hideData['Safety'],
              Encouragement: !this.state.hideData['Encouragement']
            }
          })
        }
        break
      case 'Customer Experience':
        if (this.state.CustomerExperienceDriverList == true) {
          document
            .getElementById(`driverList-${targetLine}`)
            .classList.remove('active')
          this.setState({
            CustomerExperienceDriverList: !this.state.CustomerExperienceDriverList,
            hideData: {
              ...this.state.hideData,
              Team_orientated: true,
              Customer_centric: true,
              Responsive: true,
              Collaborative: true
            }
          })
        } else {
          document
            .getElementById(`driverList-${targetLine}`)
            .classList.add('active')
          this.setState({
            CustomerExperienceDriverList: !this.state.CustomerExperienceDriverList,
            hideData: {
              ...this.state.hideData,
              Team_orientated: !this.state.hideData['Team_orientated'],
              Customer_centric: !this.state.hideData['Customer_centric'],
              Responsive: !this.state.hideData['Responsive'],
              Collaborative: !this.state.hideData['Collaborative']
            }
          })
        }
        break
      case 'Advocacy':
        if (this.state.AdvocacyDriverList == true) {
          document
            .getElementById(`driverList-${targetLine}`)
            .classList.remove('active')
          this.setState({
            AdvocacyDriverList: !this.state.AdvocacyDriverList,
            hideData: {
              ...this.state.hideData,
              Belief: true,
              Fulfillment: true,
              Integrity: true,
              Alignment: true
            }
          })
        } else {
          document
            .getElementById(`driverList-${targetLine}`)
            .classList.add('active')
          this.setState({
            AdvocacyDriverList: !this.state.AdvocacyDriverList,
            hideData: {
              ...this.state.hideData,
              Belief: !this.state.hideData['Belief'],
              Fulfillment: !this.state.hideData['Fulfillment'],
              Integrity: !this.state.hideData['Integrity'],
              Alignment: !this.state.hideData['Alignment']
            }
          })
        }
        break
      case 'factor':
        if (this.state.ProductivityDriverList == true) {
          document
            .getElementById(`driverList-${targetLine}`)
            .classList.remove('active')
          this.setState({
            ProductivityDriverList: !this.state.ProductivityDriverList,
            hideData: {
              ...this.state.hideData,
              Optimisation: true,
              Clarity: true,
              Coordination: true,
              Decisiveness: true,
            },
            hideChart: {
              ...this.state.hideChart,
              Productivity: true,
              Capability: true,
              Innovation: true,
              CustomerExperience: true,
              Advocacy: true
            }
          })
        } else {
          document
            .getElementById(`driverList-${targetLine}`)
            .classList.add('active')
          this.setState({
            CapabilityDriverList: !this.state.CapabilityDriverList,
            hideData: {
              ...this.state.hideData,
              Enablement: !this.state.hideData['Enablement'],
              Empowerment: !this.state.hideData['Empowerment'],
              Performance: !this.state.hideData['Performance'],
              Processes: !this.state.hideData['Processes'],

            },
            hideChart: {
              ...this.state.hideChart,
              Productivity: !this.state.hideChart['Productivity'],
              Capability: !this.state.hideChart['Capability'],
              Innovation: !this.state.hideChart['Innovation'],
              CustomerExperience: !this.state.hideChart['CustomerExperience'],
              Advocacy: !this.state.hideChart['Advocacy'],
            }
          })
        }
        break
    }
  }
  selectedTimeSpan(time) {
    this.setState({
      period: time
    })
    this.props.analyticsActions.getContentVEngageData(time)
  }
  renderChartLoading() {
    return (
      <div className='bodyPartTotalPostExapnd culture-analytics-exapnd' >
        <div className='inner-adoption-chart'>
          <div className='bodyPartTotalPostExapnd' id='driversData'>
            <div class='recharts-legend-wrapper'>
              <ul>
                <li class='loader-grey-line loader-line-radius loader-line-height'>
                  {' '}
                </li>
                <li class='loader-grey-line loader-line-radius loader-line-height' />
                <li class='loader-grey-line loader-line-radius loader-line-height' />
                <li class='loader-grey-line loader-line-radius loader-line-height' />
              </ul>
            </div>
            <div className='loader-grey-line  chart-container-loader' />
          </div>
        </div>
      </div>
    )
  }
  customizeTooltipForComparison = props => {
    const { payload } = props
    var chartDate = 0
    if (payload.length > 0) {
      chartDate = payload[0].payload.date
    }
    return (
      <div>
        <div className='chartDate'>
          <div className='label'>{this.customizeTooltip(chartDate, true)}</div>
        </div>
        <div className='internal-external-post-wrapper'>
          {payload.length > 0
            ? payload.map((entry, i) => {
              var type = ''
              Object.keys(entry.payload).map(k =>
                /(_identity)/g.test(k)
                  ? (type = k.replace(/(_identity)/g, ''))
                  : ''
              )
              return (
                entry.name !== 'date' ?
                  <div key={i}>
                    <div className={`custom-tooltip`}>
                      <div className='label' style={{ color: entry.color }}>
                        {/* {entry.name =='date'?type  !==''? 'Type':'': entry.name == 'count' && type =='post' ? 'Likes': entry.name == 'count' && ( type =='email' || type == 'print') ?'Recipients':entry.name} {entry.name =='date' ? type !=='' ?':'+type:'': ':'+entry.value} */}
                        {entry.name == 'count' && type == 'post' ? 'Likes' : entry.name == 'count' && (type == 'email' || type == 'print') ? 'Recipients' : entry.name} :{entry.value}
                      </div>
                    </div>
                  </div> : ''
              )
            })
            : ''}
        </div>
      </div>
    )
  }
  clickDot(e) {
    var contentVengData =
      this.props.analytics.contentVengageDetail.contentVengageData !== undefined
        ? this.props.analytics.contentVengageDetail.contentVengageData
        : ''
    contentVengData = JSON.parse(JSON.stringify(contentVengData))
    delete contentVengData.engagement_score
    var finalData = []
    if (Object.keys(contentVengData).length > 0) {
      for (var key in contentVengData) {
        for (let index = 0; index < contentVengData[key].length; index++) {
          const element = contentVengData[key][index]
          if (element.date == e.date && element.hour == e.hour) {
            var type = ''
            Object.keys(element).map(k =>
              /(_identity)/g.test(k)
                ? (type = k.replace(/(_identity)/g, ''))
                : ''
            )
            let idToPass =
              element[`${type}_identity`] !== undefined
                ? element[`${type}_identity`]
                : ''
            var isTypeExists = finalData.findIndex(p => p.type == type)
            if (isTypeExists > -1) {
              finalData[isTypeExists].id.push(idToPass)
            } else {
              var ObjToAdd = {
                id: [idToPass],
                type: type
              }
              finalData.push(ObjToAdd)
            }
          }
        }
      }
    }
    this.setState({
      idsToShowDetail: finalData,
      openContentEngageDetailTable: true
    })
    this.props.analyticsActions.fetchContentGraphDetail(finalData)
  }
  closeContentEngageDetailPopup() {
    document.body.classList.remove('overlay')
    this.setState({
      openContentEngageDetailTable: false,
      idsToShowDetail: []
    })
  }
  renderScatterChart(contentVengageDetail, GraphDimention) {

    console.log('contentVengageDetail', contentVengageDetail)
    var strokeWidth = 4
    return (
      <this.state.Recharts.ScatterChart
        width={GraphDimention.width}
        height={GraphDimention.height}
        margin={{
          top: 10,
          right: 50,
          left: 20,
          bottom: 20
        }}
      >
        <this.state.Recharts.XAxis
          dataKey='date'
          type='number'
          domain={[
            contentVengageDetail
              .contentVengageData
              .intervalDates[0],
            contentVengageDetail
              .contentVengageData.intervalDates[
            contentVengageDetail
              .contentVengageData.intervalDates
              .length - 1
            ]
          ]}
          allowDuplicatedCategory={false}
          allowDataOverflow={false}
          tickFormatter={this.customizeTooltip.bind(
            this
          )}
          ticks={
            contentVengageDetail
              .contentVengageData.intervalDates
          }
          label={{ value: 'Date', position: 'bottom' }}
        />
        <this.state.Recharts.Tooltip
          content={
            this.customizeTooltipForComparison
          }
          filterNull={false}
        />
        <this.state.Recharts.CartesianGrid strokeDasharray='3 3' />
        <this.state.Recharts.YAxis
          dataKey='count'
          name='Count'
          label={{ value: 'Drivers and sub-drivers', angle: -90, position: 'left' }}
        />
        <this.state.Recharts.YAxis
          dataKey='hour'
          name='Time'
          orientation='right'
          yAxisId='right'
          label={{ value: 'Time (24 Hours)', angle: 270, position: 'insideRight' }}
          domain={[0, 24]}
          ticks={[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24]}
        />
        <this.state.Recharts.ZAxis
          zAxisId='rightz'
          type='number'
          dataKey='count'
          range={[40, 2500]}
        />
        <this.state.Recharts.ZAxis
          zAxisId='engagementZAxix'
          type='number'
          dataKey='count'
          range={[10, 10]}
        />
        <this.state.Recharts.Legend
          verticalAlign='top'
          iconType='circle'
          iconSize={10}
          layout='vertical'
          content={this.UpdatedLegend}
          countData={
            contentVengageDetail.contentVengageData
          }
        />
        <this.state.Recharts.Scatter
          data={
            contentVengageDetail
              .contentVengageData.email
          }
          name='email'
          fill={COLORS[13]}
          yAxisId='right'
          zAxisId='rightz'
          onClick={this.clickDot.bind(this)}
          hide={this.state.hideData['email']}
          opacity={0.5}
        />
        <this.state.Recharts.Scatter
          data={
            contentVengageDetail
              .contentVengageData.post
          }
          name='post'
          fill={COLORS[14]}
          yAxisId='right'
          zAxisId='rightz'
          onClick={this.clickDot.bind(this)}
          hide={this.state.hideData['post']}
          opacity={0.5}
        />
        <this.state.Recharts.Scatter
          data={
            contentVengageDetail
              .contentVengageData.print
          }
          name='print'
          fill={COLORS[15]}
          yAxisId='right'
          zAxisId='rightz'
          onClick={this.clickDot.bind(this)}
          hide={this.state.hideData['print']}
          opacity={0.5}
        />
        <this.state.Recharts.Scatter
          data={
            contentVengageDetail
              .contentVengageData.sms
          }
          name='sms'
          fill={COLORS[16]}
          yAxisId='right'
          zAxisId='rightz'
          onClick={this.clickDot.bind(this)}
          hide={this.state.hideData['sms']}
          opacity={0.5}
        />
        <this.state.Recharts.Scatter
          data={
            contentVengageDetail
              .contentVengageData.engagement_score
              .Productivity_average
          }
          name='Productivity'
          line={{ stroke: COLORS[0] }}
          lineJointType='monotoneX'
          lineType='joint'
          fill={COLORS[0]}
          strokeWidth={strokeWidth}
          zAxisId='engagementZAxix'
          // onClick={this.clickDot.bind(this)}
          hide={this.state.hideData['Productivity']}
        />
        <this.state.Recharts.Scatter
          data={
            contentVengageDetail
              .contentVengageData.engagement_score
              .Optimisation
          }
          name='Optimisation'
          line={{ stroke: COLORS[6] }}
          lineJointType='monotoneX'
          lineType='joint'
          fill={COLORS[6]}
          strokeWidth={strokeWidth}
          zAxisId='engagementZAxix'
          // onClick={this.clickDot.bind(this)}
          hide={this.state.hideData['Optimisation']}
        />
        <this.state.Recharts.Scatter
          data={
            contentVengageDetail
              .contentVengageData.engagement_score
              .Clarity
          }
          name='Clarity'
          line={{ stroke: COLORS[7] }}
          lineJointType='monotoneX'
          lineType='joint'
          fill={COLORS[7]}
          strokeWidth={strokeWidth}
          zAxisId='engagementZAxix'
          // onClick={this.clickDot.bind(this)}
          hide={this.state.hideData['Clarity']}
        />

        <this.state.Recharts.Scatter
          data={
            contentVengageDetail
              .contentVengageData.engagement_score
              .Coordination
          }
          name='Coordination'
          line={{ stroke: COLORS[8] }}
          lineJointType='monotoneX'
          lineType='joint'
          fill={COLORS[8]}
          strokeWidth={strokeWidth}
          zAxisId='engagementZAxix'
          // onClick={this.clickDot.bind(this)}
          hide={this.state.hideData['Coordination']}
        />

        <this.state.Recharts.Scatter
          data={
            contentVengageDetail
              .contentVengageData.engagement_score
              .Decisiveness
          }
          name='Decisiveness'
          line={{ stroke: COLORS[9] }}
          lineJointType='monotoneX'
          lineType='joint'
          fill={COLORS[9]}
          strokeWidth={strokeWidth}
          zAxisId='engagementZAxix'
          // onClick={this.clickDot.bind(this)}
          hide={this.state.hideData['Decisiveness']}
        />  


        <this.state.Recharts.Scatter
          data={
            contentVengageDetail
              .contentVengageData.engagement_score
              .Capability_average
          }
          name='Capability'
          line={{ stroke: COLORS[1] }}
          lineJointType='monotoneX'
          lineType='joint'
          fill={COLORS[1]}
          strokeWidth={strokeWidth}
          zAxisId='engagementZAxix'
          // onClick={this.clickDot.bind(this)}
          hide={this.state.hideData['Capability']}
        />

<this.state.Recharts.Scatter
          data={
            contentVengageDetail
              .contentVengageData.engagement_score
              .Enablement
          }
          name='Enablement'
          line={{ stroke: COLORS[10] }}
          lineJointType='monotoneX'
          lineType='joint'
          fill={COLORS[10]}
          strokeWidth={strokeWidth}
          zAxisId='engagementZAxix'
          // onClick={this.clickDot.bind(this)}
          hide={this.state.hideData['Enablement']}
        />

<this.state.Recharts.Scatter
          data={
            contentVengageDetail
              .contentVengageData.engagement_score
              .Empowerment
          }
          name='Empowerment'
          line={{ stroke: COLORS[11] }}
          lineJointType='monotoneX'
          lineType='joint'
          fill={COLORS[11]}
          strokeWidth={strokeWidth}
          zAxisId='engagementZAxix'
          // onClick={this.clickDot.bind(this)}
          hide={this.state.hideData['Empowerment']}
        />

<this.state.Recharts.Scatter
          data={
            contentVengageDetail
              .contentVengageData.engagement_score
              .Performance
          }
          name='Performance'
          line={{ stroke: COLORS[12] }}
          lineJointType='monotoneX'
          lineType='joint'
          fill={COLORS[12]}
          strokeWidth={strokeWidth}
          zAxisId='engagementZAxix'
          // onClick={this.clickDot.bind(this)}
          hide={this.state.hideData['Performance']}
        />

<this.state.Recharts.Scatter
          data={
            contentVengageDetail
              .contentVengageData.engagement_score
              .Processes
          }
          name='Processes'
          line={{ stroke: COLORS[13] }}
          lineJointType='monotoneX'
          lineType='joint'
          fill={COLORS[13]}
          strokeWidth={strokeWidth}
          zAxisId='engagementZAxix'
          // onClick={this.clickDot.bind(this)}
          hide={this.state.hideData['Processes']}
        />



        <this.state.Recharts.Scatter
          data={
            contentVengageDetail
              .contentVengageData.engagement_score
              .Innovation_average
          }
          name='Innovation'
          line={{ stroke: COLORS[2] }}
          lineJointType='monotoneX'
          lineType='joint'
          fill={COLORS[2]}
          strokeWidth={strokeWidth}
          zAxisId='engagementZAxix'
          // onClick={this.clickDot.bind(this)}
          hide={this.state.hideData['Innovation']}
        />

<this.state.Recharts.Scatter
          data={
            contentVengageDetail
              .contentVengageData.engagement_score
              .Prioritisation
          }
          name='Prioritisation'
          line={{ stroke: COLORS[14] }}
          lineJointType='monotoneX'
          lineType='joint'
          fill={COLORS[14]}
          strokeWidth={strokeWidth}
          zAxisId='engagementZAxix'
          // onClick={this.clickDot.bind(this)}
          hide={this.state.hideData['Prioritisation']}
        />
        <this.state.Recharts.Scatter
          data={
            contentVengageDetail
              .contentVengageData.engagement_score
              .Inclusiveness
          }
          name='Inclusiveness'
          line={{ stroke: COLORS[15] }}
          lineJointType='monotoneX'
          lineType='joint'
          fill={COLORS[15]}
          strokeWidth={strokeWidth}
          zAxisId='engagementZAxix'
          // onClick={this.clickDot.bind(this)}
          hide={this.state.hideData['Inclusiveness']}
        />
        <this.state.Recharts.Scatter
          data={
            contentVengageDetail
              .contentVengageData.engagement_score
              .Safety
          }
          name='Safety'
          line={{ stroke: COLORS[16] }}
          lineJointType='monotoneX'
          lineType='joint'
          fill={COLORS[16]}
          strokeWidth={strokeWidth}
          zAxisId='engagementZAxix'
          // onClick={this.clickDot.bind(this)}
          hide={this.state.hideData['Safety']}
        />
        <this.state.Recharts.Scatter
          data={
            contentVengageDetail
              .contentVengageData.engagement_score
              .Encouragement
          }
          name='Encouragement'
          line={{ stroke: COLORS[17] }}
          lineJointType='monotoneX'
          lineType='joint'
          fill={COLORS[17]}
          strokeWidth={strokeWidth}
          zAxisId='engagementZAxix'
          // onClick={this.clickDot.bind(this)}
          hide={this.state.hideData['Encouragement']}
        /> 
        <this.state.Recharts.Scatter
          data={
            contentVengageDetail
              .contentVengageData.engagement_score
              .Customer_Experience_average
          }
          name='Customer Experience'
          line={{ stroke: COLORS[3] }}
          lineJointType='monotoneX'
          lineType='joint'
          fill={COLORS[3]}
          strokeWidth={strokeWidth}
          zAxisId='engagementZAxix'
          // onClick={this.clickDot.bind(this)}
          hide={this.state.hideData['Customer Experience']}
        />

<this.state.Recharts.Scatter
          data={
            contentVengageDetail
              .contentVengageData.engagement_score
              .Team_orientated
          }
          name='Team_orientated'
          line={{ stroke: COLORS[18] }}
          lineJointType='monotoneX'
          lineType='joint'
          fill={COLORS[18]}
          strokeWidth={strokeWidth}
          zAxisId='engagementZAxix'
          // onClick={this.clickDot.bind(this)}
          hide={this.state.hideData['Team_orientated']}
        />
        <this.state.Recharts.Scatter
          data={
            contentVengageDetail
              .contentVengageData.engagement_score
              .Customer_centric
          }
          name='Customer_centric'
          line={{ stroke: COLORS[19] }}
          lineJointType='monotoneX'
          lineType='joint'
          fill={COLORS[19]}
          strokeWidth={strokeWidth}
          zAxisId='engagementZAxix'
          // onClick={this.clickDot.bind(this)}
          hide={this.state.hideData['Customer_centric']}
        />
        <this.state.Recharts.Scatter
          data={
            contentVengageDetail
              .contentVengageData.engagement_score
              .Responsive
          }
          name='Responsive'
          line={{ stroke: COLORS[20] }}
          lineJointType='monotoneX'
          lineType='joint'
          fill={COLORS[20]}
          strokeWidth={strokeWidth}
          zAxisId='engagementZAxix'
          // onClick={this.clickDot.bind(this)}
          hide={this.state.hideData['Responsive']}
        />
        <this.state.Recharts.Scatter
          data={
            contentVengageDetail
              .contentVengageData.engagement_score
              .Collaborative
          }
          name='Collaborative'
          line={{ stroke: COLORS[21] }}
          lineJointType='monotoneX'
          lineType='joint'
          fill={COLORS[21]}
          strokeWidth={strokeWidth}
          zAxisId='engagementZAxix'
          // onClick={this.clickDot.bind(this)}
          hide={this.state.hideData['Collaborative']}
        />


        <this.state.Recharts.Scatter
          data={
            contentVengageDetail
              .contentVengageData.engagement_score
              .Advocacy_average
          }
          name='Advocacy'
          line={{ stroke: COLORS[5] }}
          lineJointType='monotoneX'
          lineType='joint'
          fill={COLORS[5]}
          strokeWidth={strokeWidth}
          zAxisId='engagementZAxix'
          // onClick={this.clickDot.bind(this)}
          hide={this.state.hideData['Advocacy']}
        />
        <this.state.Recharts.Scatter
          data={
            contentVengageDetail
              .contentVengageData.engagement_score
              .Belief
          }
          name='Belief'
          line={{ stroke: COLORS[22] }}
          lineJointType='monotoneX'
          lineType='joint'
          fill={COLORS[22]}
          strokeWidth={strokeWidth}
          zAxisId='engagementZAxix'
          // onClick={this.clickDot.bind(this)}
          hide={this.state.hideData['Belief']}
        /><this.state.Recharts.Scatter
        data={
          contentVengageDetail
            .contentVengageData.engagement_score
            .Fulfillment
        }
        name='Fulfillment'
        line={{ stroke: COLORS[23] }}
        lineJointType='monotoneX'
        lineType='joint'
        fill={COLORS[23]}
        strokeWidth={strokeWidth}
        zAxisId='engagementZAxix'
        // onClick={this.clickDot.bind(this)}
        hide={this.state.hideData['Fulfillment']}
      /><this.state.Recharts.Scatter
      data={
        contentVengageDetail
          .contentVengageData.engagement_score
          .Integrity
      }
      name='Integrity'
      line={{ stroke: COLORS[24] }}
      lineJointType='monotoneX'
      lineType='joint'
      fill={COLORS[24]}
      strokeWidth={strokeWidth}
      zAxisId='engagementZAxix'
      // onClick={this.clickDot.bind(this)}
      hide={this.state.hideData['Integrity']}
    /><this.state.Recharts.Scatter
    data={
      contentVengageDetail
        .contentVengageData.engagement_score
        .Alignment
    }
    name='Alignment'
    line={{ stroke: COLORS[25] }}
    lineJointType='monotoneX'
    lineType='joint'
    fill={COLORS[25]}
    strokeWidth={strokeWidth}
    zAxisId='engagementZAxix'
    // onClick={this.clickDot.bind(this)}
    hide={this.state.hideData['Alignment']}
  />
      </this.state.Recharts.ScatterChart>
    )
  }
  renderTableLoader() {
    return (
      <div className=' loader-culture-tableExpand'>
        <div class='widget'>
          <div class='table-anytlics-body'>
            <table class='table responsive-table'>
              <thead>
                <tr class='reactable-column-header'>
                  <th class='reactable-th-serialno reactable-header-sortable serialno  a-center'>
                    <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                  </th>
                  <th class='reactable-th-serialno reactable-header-sortable serialno  a-center'>
                    <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                  </th>
                  <th class='reactable-th-serialno reactable-header-sortable serialno  a-center'>
                    <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                  </th>
                  <th class='reactable-th-serialno reactable-header-sortable serialno  a-center'>
                    <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                  </th>
                  <th class='reactable-th-serialno reactable-header-sortable serialno  a-center'>
                    <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                  </th>
                  <th class='reactable-th-serialno reactable-header-sortable serialno  a-center'>
                    <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                  </th>
                  <th class='reactable-th-serialno reactable-header-sortable serialno  a-center'>
                    <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                  </th>
                  <th class='reactable-th-serialno reactable-header-sortable serialno  a-center'>
                    <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                  </th>
                  <th class='reactable-th-serialno reactable-header-sortable serialno  a-center'>
                    <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                  </th>
                </tr>
              </thead>
              <tbody class='reactable-data'>
                <tr class='table-body-text'>
                  <td>
                    {' '}
                    <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                  </td>
                  <td>
                    {' '}
                    <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                  </td>
                  <td>
                    {' '}
                    <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                  </td>
                  <td>
                    {' '}
                    <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                  </td>
                  <td>
                    {' '}
                    <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                  </td>
                  <td>
                    {' '}
                    <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                  </td>
                  <td>
                    {' '}
                    <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                  </td>
                  <td>
                    {' '}
                    <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                  </td>
                  <td>
                    {' '}
                    <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                  </td>
                </tr>
                <tr class='table-body-text'>
                  <td>
                    {' '}
                    <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                  </td>
                  <td>
                    {' '}
                    <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                  </td>
                  <td>
                    {' '}
                    <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                  </td>
                  <td>
                    {' '}
                    <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                  </td>
                  <td>
                    {' '}
                    <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                  </td>
                  <td>
                    {' '}
                    <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                  </td>
                  <td>
                    {' '}
                    <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                  </td>
                  <td>
                    {' '}
                    <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                  </td>
                  <td>
                    {' '}
                    <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                  </td>
                </tr>
                <tr class='table-body-text blanck-div-loader-totalpost'>
                  <td>
                    {' '}
                    <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                  </td>
                  <td>
                    {' '}
                    <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                  </td>
                  <td>
                    {' '}
                    <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                  </td>
                  <td>
                    {' '}
                    <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                  </td>
                  <td>
                    {' '}
                    <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                  </td>
                  <td>
                    {' '}
                    <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                  </td>
                  <td>
                    {' '}
                    <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                  </td>
                  <td>
                    {' '}
                    <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                  </td>
                  <td>
                    {' '}
                    <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                  </td>
                </tr>
                <tr class='blanck-div-loader-totalpost table-body-text'>
                  <td /> <td />
                  <td />
                  <td />
                  <td />
                  <td />
                  <td />
                  <td />
                  <td />{' '}
                </tr>
                <tr class='blanck-div-loader-totalpost table-body-text'>
                  <td /> <td />
                  <td />
                  <td />
                  <td />
                  <td />
                  <td />
                  <td />
                  <td />{' '}
                </tr>
                <tr class='blanck-div-loader-totalpost table-body-text'>
                  <td /> <td />
                  <td />
                  <td />
                  <td />
                  <td />
                  <td />
                  <td />
                  <td />{' '}
                </tr>
                <tr class='blanck-div-loader-totalpost table-body-text'>
                  <td /> <td />
                  <td />
                  <td />
                  <td />
                  <td />
                  <td />
                  <td />
                  <td />{' '}
                </tr>
                <tr class='blanck-div-loader-totalpost table-body-text'>
                  <td /> <td />
                  <td />
                  <td />
                  <td />
                  <td />
                  <td />
                  <td />
                  <td />{' '}
                </tr>
                <tr class='blanck-div-loader-totalpost table-body-text'>
                  <td /> <td />
                  <td />
                  <td />
                  <td />
                  <td />
                  <td />
                  <td />
                  <td />{' '}
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    )
  }
  fetchPostData(type, id) {
    if (type == 'post') {
      this.props.notificationAction.fetchSelectedPost(id)
    } else {
      this.props.analyticsActions.fetchSelectedEmailPrint(type, id)
    }
  }
  render() {
    var contentVengageDetail = this.props.analytics.contentVengageDetail
    var windowDimention = utils.windowSize()
    var GraphDimention = utils.getChartDimension(
      windowDimention.w,
      'contentScatter'
    )
    var isDriverListOpen = !!(
      this.state.ProductivityDriverList ||
      this.state.CustomerExperienceDriverList ||
      this.state.CapabilityDriverList ||
      this.state.InnovationDriverList ||
      this.state.AdvocacyDriverList
    )
    return (
      <section
        id='analytics-Main-container'
        className={`analytics-wrapper culture-new-tab ${contentVengageDetail.loading == false ? 'Value-Scroll-Container' : ''}`}
      >
        <GuestUserRestrictionPopup setOverlay />
        <Header title={'Analytics'} location={this.props.location} />
        <div className='main-container comparison-section'>
          <div id='content'>
            <div className="culture-dashboard-tab-responsive culture-score-tab clearfix">
              <AnalyticsNav location={this.props.location} />
            </div>
            <div class='culture-participation-mainbox dashboard-analytics-level1-container new-culture-section'>
              <div class='tagged-voted-value-main-box clearfix driver-chart-wrapper '>
                <div class='vote-wrapper '>
                  <div class='participation'>
                    <div class='participation-header clearfix'>
                      <div class='participation-wrapper'>
                        <div className='title'>
                          {' '}
                          Content v Drivers and sub Drivers{' '}
                          <span className='svg-icon help-element'>
                            <svg
                              xmlns='http://www.w3.org/2000/svg'
                              data-tip
                              data-for='drivers'
                              width='24'
                              height='24'
                              viewBox='0 0 24 24'
                            >
                              <path d='M0 0h24v24H0z' fill='none' />
                              <path d='M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm1 17h-2v-2h2v2zm2.07-7.75l-.9.92C13.45 12.9 13 13.5 13 15h-2v-.5c0-1.1.45-2.1 1.17-2.83l1.24-1.26c.37-.36.59-.86.59-1.41 0-1.1-.9-2-2-2s-2 .9-2 2H8c0-2.21 1.79-4 4-4s4 1.79 4 4c0 .88-.36 1.68-.93 2.25z' />
                            </svg>
                          </span>
                          {this.state.tooltip !== null ? (
                            <this.state.tooltip
                              place='right'
                              type='light'
                              effect='solid'
                              id='drivers'
                              delayHide={100}
                              delayUpdate={100}
                            >
                              <div className='tooltip-wrapper'>
                                <div className='inner-tooltip-wrapper'>
                                  <div className='tooltip-header clearfix'>
                                    <div className='analytics-tooltip-title'>
                                      <span>Content v Drivers and sub Drivers </span>
                                    </div>
                                  </div>
                                  <div className='tooltip-about-wrapper'>
                                    <span>ABOUT</span>
                                    <p>
                                      Content v Drivers and sub Drivers shows how overall
                                      advocacy across the organisation is
                                      impacted by content type, channel and
                                      frequency. Understand what content is
                                      having the greatest impact on your
                                        advocacy scores and why.{' '}
                                    </p>
                                  </div>
                                </div>
                              </div>
                            </this.state.tooltip>
                          ) : (
                              ''
                            )}
                        </div>
                      </div>
                      <div class='expand-year-main-wrapper'>
                        <div class='expand-year-wrapper'>
                          <div className='yearly-filtered-data'>
                            <a
                              className={`month-filter ${
                                this.state.period == undefined ||
                                  this.state.period == 'month'
                                  ? 'active'
                                  : ''
                                }`}
                              onClick={this.selectedTimeSpan.bind(
                                this,
                                'month'
                              )}
                            >
                              {' '}
                              Month{' '}
                            </a>
                            <a
                              className={`year-filter ${
                                this.state.period == 'year' ? 'active' : ''
                                }`}
                              onClick={this.selectedTimeSpan.bind(
                                this,
                                'year'
                              )}
                            >
                              Year{' '}
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class='voted-value-piechart clearfix'>
                      <div class={`chart-workplace-wrapper ${isDriverListOpen == true ? 'driver-selected' : ''}`}>
                        <div className='Expand-design-wrapper' id='driversData'>
                          <div className='chart-container-in-analytics-wrapper '>
                            <div className='inner-chart-container-in-analytics-wrapper'>
                              {contentVengageDetail.loading == true ?
                                this.renderChartLoading()
                                : this.state.Recharts !== null ?
                                  <div className='bodyPartTotalPostExapnd culture-analytics-exapnd'>
                                    {this.renderScatterChart(contentVengageDetail, GraphDimention)}
                                  </div>
                                  : <div>No Value Found</div>}
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className='culture-analytics-exapnd comparisonTable-wrapper'>
            <div className='main-wrppaer-analytics'>
              {this.props.analytics.contentGraphDetail.loading == true ?
                this.renderTableLoader() :
                this.state.openContentEngageDetailTable == true ? (
                  <DetailContentVEngagement
                    contentGraphDetail={this.props.analytics.contentGraphDetail}
                    fetchPostData={this.fetchPostData.bind(this)}
                    postData={this.props.postData}
                    contentCounts={this.props.analytics.printEmailAnalyticsCount}
                  />
                ) : (
                    ''
                  )}
            </div>
          </div>
        </div>
      </section>
    )
  }
}

function mapStateToProps(state) {
  return {
    analytics: state.analytics,
    postData: state.notification_data
  }
}

function mapDispatchToProps(dispatch) {
  return {
    analyticsActions: bindActionCreators(analyticsActions, dispatch),
    notificationAction: bindActionCreators(notificationAction, dispatch)
  }
}

var connection = connect(
  mapStateToProps,
  mapDispatchToProps
)
module.exports = connection(ComparisonIndex)
