import { bindActionCreators } from 'redux'
import { Field, reduxForm } from 'redux-form'
import { renderField } from '../Helpers/ReduxForm/RenderField'
import {connect} from 'react-redux'
import moment from '../Chunks/ChunkMoment'
import * as utils from '../../utils/utils'
import * as tagsActions from '../../actions/tagsActions'
import ReactDatePicker from '../Helpers/ReduxForm/ReactDatePicker' // react datepicker field
import reactDatepickerChunk from '../Chunks/ChunkDatePicker' // datepicker chunk
import * as generalActions from '../../actions/generalActions';
var savedFilterData={}
var addNewTextInField=false
var savedTagid=''
var isTagDataChanged=false
var tagtype=''
var deleteApicall=false
class AnalyticsFilters extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            filterState:false,
            Moment: null,
            noItemsClass: '',
            tagname: '',
            tagid: '',
            type:'',
            showSuggestionList:'',
            isSaveButtonClick:false,
            enableUnsaveButton:false,
            isDateInputBoxSelected:false
        }
        this.fitlerSelectedDate = this.fitlerSelectedDate.bind(this);
        this.filterfetchTagList = this.filterfetchTagList.bind(this);
        this.hidefilterTagList = this.hidefilterTagList.bind(this);
    }
    handleInitialize () {
      var location=''
      if (typeof this.props.location !== 'undefined') {
        var newLocation = utils
          .removeTrailingSlash(this.props.location.pathname)
          .split('/')
          location = newLocation[2] == undefined ? 'feed' : newLocation[2]
      }

    if(Object.keys(savedFilterData).length>0  && this.props.filterRemoved==false){
      //to initialise saved data 
        const initData = {
          'scheduledDate-0': savedFilterData.start_date,
          'scheduledDate-1':savedFilterData.end_date,
          'campaignStatus':savedFilterData.campaign_status,
          'socialMedia':savedFilterData.social_media,
          'walltype':savedFilterData.type
        }
      
        if(typeof savedFilterData.user!=='undefined'){
          tagtype=savedFilterData.user
        }else if(typeof savedFilterData.department!=='undefined'){
          tagtype=savedFilterData.department
        }else if(typeof savedFilterData.company!=='undefined'){
          tagtype=savedFilterData.company
        }
        if(tagtype){
          //to set saved tag value in tag area
          if( document.querySelectorAll(`#userDepartdropdown li#${tagtype}`).length>0 ){
            savedTagid=tagtype
              var options = document.querySelectorAll(`#userDepartdropdown li#${tagtype}`)[0].innerText;
              if(document.getElementById('searchUserDepartment')){
                document.getElementById('searchUserDepartment').value = options
              }
            }
        }
        this.props.initialize(initData)    
        if(document.getElementById('scheduledDate-0')){
          //to set saved value in date area
            document.getElementById('scheduledDate-0').setAttribute("value", savedFilterData.start_date);
            document.getElementById('scheduledDate-1').setAttribute("value", savedFilterData.end_date);
        }
      }else{
        //call when filter data is removed and to blank all selected data from its field 
          document.getElementById('searchUserDepartment').value = ''
          this.props.removeDate()
          var initData={}
          this.props.initialize(initData)
      }
    }
    
  componentDidUpdate(prevProps, prevState) {
    if(this.props.filterRemoved==true){
      //call when filter is cleared 
      this.handleInitialize()
    }
  }
  componentWillReceiveProps(newProps) {
    if(newProps.parentTagid!==''){
      //when tag is selected then to display selected value in tag search area
      if(document.querySelectorAll(`#userDepartdropdown li#${newProps.parentTagid}`).length>0){
            var options = document.querySelectorAll(`#userDepartdropdown li#${newProps.parentTagid}`)[0].innerText;
            if(document.getElementById('searchUserDepartment')){
                document.getElementById('searchUserDepartment').value = options
                document.getElementById('searchUserDepartment').setAttribute("value", options);
            }
        }
    }
  }
  componentWillMount () {
    moment().then(moment => {
      reactDatepickerChunk()
        .then(reactDatepicker => {
          this.setState({ ReactDatePicker: reactDatepicker, Moment: moment })
        })
        .catch(err => {
          throw err
        })
    })
  }

  fitlerSelectedDate (props){
    //call when date is changed
   this.props.selectedDate(props)
  }

  hidefilterTagList(){
    this.setState({
      showSuggestionList:''
    })
  }
 
   /**
    * to fetch tag list from when click on tag search area
    */
  filterfetchTagList(){
    var input = ''
    var output = jQuery('#userDepartdropdown li')
    var showedLi = 0

    this.setState({
      showSuggestionList:'active'
    })
    
    if(document.getElementById('searchUserDepartment')){
        input = document.getElementById('searchUserDepartment').value.toLowerCase()
        addNewTextInField=true
        this.setState({
          tagname: input
        })
    }
    for (var i = 0; i < output.length; i++) {
      if (output[i].innerText.toLowerCase().indexOf(input) != -1) {
          output[i].style.display = 'block'
          showedLi++
      } else {
          output[i].style.display = 'none'
      }
      if (showedLi == 0) {
          this.setState({
            noItemsClass: 'no-items'
          })
      } else {
          this.setState({
            noItemsClass: ''
          })
      }
    }
  }
  /**
   * when select tag from displayed list
   * @param {*} alltag 
   * @param {*} e 
   */
  filterSelectTag(alltag,e){
    // call when tag will be selected from filter department/user
    var tagdata={
      type: alltag.type,
      tagname: alltag.display,
      tagid: alltag.id
    }
    this.props.tagdata(tagdata)
    this.setState(
      {
        type: alltag.type,
        tagname: alltag.display,
        tagid: alltag.id
      })
      if(tagtype!==alltag.id || tagtype==''){
        //to check wheather tag is changed , to disable save button if it is changed
        isTagDataChanged=true
      }
      var options = document.querySelectorAll(`#userDepartdropdown li#${alltag.id}`)[0].innerText;
      document.getElementById('searchUserDepartment').value = options
      document.getElementById('searchUserDepartment').setAttribute("value", options);
      var output = jQuery('#userDepartdropdown li')
      for (var i = 0; i < output.length; i++) {
        output[i].style.display = 'none'
      }
  }

  filterParentClick (clickFlag,e)
  {
    this.props.parentClick(clickFlag,e)
  }
  
  /**
   * @author disha
   * call when save button is clicked and call parent componet function
   * @param {*} e 
   */
  SaveAnalyticsFilter(e){
    isTagDataChanged=false
    this.setState({
      isSaveButtonClick:true,
      enableUnsaveButton:true
    })
    this.props.saveAnalyticsFilter(e)
  }                  
  selectPostAssetType(optionList){
    var value;
    return(

      <Field 
        type='select'
        component={renderField}
        id='walltype'
        name='walltype'
        onChange={this.props.selectedWalltype}>
        {
          optionList.map((element,index)=>{
            if(element!=='Select Channel' && element !== 'Select Asset Types' && element !== 'Select Campaign Types')
            {
              value=element;
            }
            else
            {
              value='';
            }
            if(this.props.route=='feed'|| this.props.route=='analytics' || this.props.route=='asset')
           {
             value=value.toLowerCase()
           }
           return(
               <option key={index} value={value}>{element}</option>
           )
          })
        }
    </Field>
  )
  }
  componentDidMount(){
    addNewTextInField=false
    if(document.getElementById('searchUserDepartment')){
      document.getElementById('searchUserDepartment').setAttribute("value", '');
      document.getElementById('searchUserDepartment').setAttribute('autocomplete', "off");
    }
    this.handleInitialize()
    this.props.generalActions.fetchClientIp()
    this.props.tagsActions.fetchUserTags()
  } 
  /**
   * call when apply button is clicked
   */
  applySelectedFilter(e){
    addNewTextInField=false
    isTagDataChanged=false    
    this.props.applySelectedFilter(e)
  } 
  /**
   * call when click on clear filter to unsave filter
   * @param {*} e 
   */
  unsaveFilter(e){
    this.setState({
      //flag to disable clear button after selecting
      enableUnsaveButton:false  
    })
    this.props.unsaveFilter(deleteApicall)
  }          
	render() {
    let optionList = [];
    if(this.props.route=='feed'|| this.props.route=='analytics')
    {
      optionList = ['Select Channel','Internal','Facebook','Twitter','Linkedin']
    }
    else if(this.props.route=='asset')
    {
      optionList = ['Select Asset Types','Image','Video','Document']
    }else if(this.props.route=='campaign')
    {
      optionList = ['Select Campaign Types','Internal','Talent','Customer']
    }
    var isDataSaved=false
    var displayTick=false
     if(this.props.isFilterDataChanged==false && this.props.isDateChanged==false && this.props.isSavedFilterButtonClicked==true){
       //to enable save button if data is not changed and apply button is clicked
      isDataSaved=false
    }else{
      isDataSaved=true
    }

    if(this.state.enableUnsaveButton==true || this.props.renderSavedFilterTickmarkChild.length>0 && this.props.renderSavedFilterTickmarkChild[0]!=='no data'){
      //to enable clear filter button if save button is clicked or data is saved
      deleteApicall=true
    }else{
      deleteApicall=false
    }
    if(this.props.renderSavedFilterTickmarkChild.length>0 && this.props.renderSavedFilterTickmarkChild[0]!=='no data'){
      //to render tick mark when data is saved
      displayTick=true
      var pagetype={
        pageType:this.props.renderSavedFilterTickmarkChild[0].type
      }
      savedFilterData=this.props.renderSavedFilterTickmarkChild[0].filter_string?JSON.parse(this.props.renderSavedFilterTickmarkChild[0].filter_string):this.props.renderSavedFilterTickmarkChild
      this.props.renderSavedFilterTickmarkChild[0].filter_string?Object.assign(savedFilterData,pagetype):''
    }else{
      displayTick=false
      savedFilterData={}
    }
    return (
       
      <div>
      <div className='filter-row row'>
        <div className='col-one-of-four'>
          <div className='curated-filter-inner'>
            <div className='form-row'>
              {/* reactdatepicker has onchange method in its component and to pass its data pass props in param */}
              <Field
                ref={`scheduledDate-0`}
                name={`scheduledDate-0`}
                id={`scheduledDate-0`}
                component={props => {
                  return (
                    <ReactDatePicker
                      {...props}
                      callFrom='analytics'
                      placeholderText="Click to select a date"
                      label={`Date (From)`}
                      reactDatePicker={this.state.ReactDatePicker}
                      moment={this.state.Moment}
                      onChange={this.fitlerSelectedDate(props)}
                    />
                  )
                }}
              />
            </div>
          </div>
        </div>
        <div className='col-one-of-four'>
          <div className='curated-filter-inner'>
            <div className='form-row'>
              <Field
                ref={`scheduledDate-1`}
                name={`scheduledDate-1`}
                id={'scheduledDate-1'}
                component={props => {
                  return (
                    <ReactDatePicker
                      {...props}
                      callFrom='analytics'
                      label={`Date (To)`}
                      placeholderText="Click to select a date"
                      reactDatePicker={this.state.ReactDatePicker}
                      moment={this.state.Moment}
                      value={this.state.date}
                      onChange={this.fitlerSelectedDate(props)}
                    />
                  )
                }}
              />
            </div>
          </div>
        </div>
        <div className='col-one-of-four'>
          <div className='curated-filter-inner'>
            <div className='form-row'>
              <label>{this.props.route=='asset'?'Asset Type':this.props.route=='feed'|| this.props.route=='analytics'?'Channel':this.props.route=='campaign'?'Campaign Type':''}</label>
              <div className='select-wrapper'>
              {this.selectPostAssetType(optionList)}
              </div>
            </div>
          </div>
        </div>
        <div className='col-one-of-four'>
          <div className='curated-filter-inner'>
            <div className='form-row'>
              <label>User/Department</label>
              {/* //onClick={this.filterParentClick.bind(this,true)} */}
              <div className='select-wrapper' >
                <input 
                  placeholder='Search..'
                  id='searchUserDepartment'
                  type='text'
                 // component={renderField}
                  name='searchUserDepartment'
                  onFocus={this.filterfetchTagList}
                  onChange={this.filterfetchTagList}
                  onBlur={this.hidefilterTagList}
                   />
                <div id='userDepartdropdown' className={`search-suggestion-list-section ${this.state.showSuggestionList} ${this.props.noItemsClass}`}>
                <div className='search-suggestion-list-wrapper'>
                  <div className='no-user-text'>No user/department found.</div>
                  <ul className='search-suggestion-list'>
                    {this.props.tags.userTags.map((alltag, i) => {
                      return (
                        <li
                          id={alltag.id}
                          key={alltag.id}
                          onClick={this.filterSelectTag.bind(this,alltag)}
                        >
                          <span className='userName'>{alltag.display}</span>
                          {
                            alltag.email?
                          <span className='userEmail'>({alltag.email})</span>
                          :''
                        }
                        </li>
                      )
                    })}
                  </ul>
                </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className='filter-row row'>
        
        {this.props.route=='campaign'?
        <div className='col-one-of-four'>
          <div className='curated-filter-inner'>
            <div className='form-row'>
              <label>Campaign Status</label>
              <div className='select-wrapper'>
              <Field id='campaignStatus' name='campaignStatus' component={renderField} type='select' onChange={this.props.selectedCamp_status}>
                {/* <select onChange={this.props.selectedCamp_status} id='campaignStatus' name='campaignStatus'> */}
                <option value=''>Select Campaign Status</option>
                  <option value='all'>All</option>
                  <option value='expired'>Expired</option>
                  <option value='live'>Live</option>
                  <option value='terminated'>Terminate</option>
                {/* </select> */}
                </Field>
              </div>
            </div>
          </div>
        </div>:''}
        {this.props.route=='campaign'?
        <div className='col-one-of-four'>
          <div className='curated-filter-inner'>
            <div className='form-row'>
              <label>Channel</label>
              <div className='select-wrapper'>
              <Field onChange={this.props.selectedMedia} name='socialMedia' id='socialMedia' component={renderField} type='select'>
                 {/* <select onChange={this.props.selectedMedia} name='socialMedia'> */}
                <option value=''>Select Channel</option>
                  <option value='internal' >Internal</option>
                  <option value='facebook'>Facebook</option>
                  <option value='linkedin'>Linkedin</option>
                  <option value='twitter'>Twitter</option>
                {/* </select> */}
                </Field>
              </div>
            </div>
          </div>
        </div>:''}
      
      </div>
      <div className='filter-button-wrapper clearfix'>
              <a
                className='btn btn-theme' disabled={isDataSaved?'disabled':''} onClick={this.SaveAnalyticsFilter.bind(this)}>
                {displayTick?<span>
                  <i class="material-icons">done</i>&nbsp; Save filter</span>:'Save Filter'}
              </a>
              <a
                onClick={this.unsaveFilter.bind(this)}
                className='btn btn-theme'>Clear Filter
              </a>
              <a
                className='btn btn-theme' onClick={this.applySelectedFilter.bind(this)}>Apply filter
              </a>
             </div>
    </div>
		)
	}
}

function mapStateToProps (state) {
  return {
    tags: state.tags,
    general:state.general
  }
}
function mapDispatchToProps (dispatch) {
  return {
    generalActions:bindActionCreators(generalActions, dispatch),
    tagsActions: bindActionCreators(tagsActions, dispatch)    
  }
}
let connection = connect(mapStateToProps, mapDispatchToProps)
var reduxConnectedComponent = connection(AnalyticsFilters)

export default reduxForm({
  form: 'AnalyticsFilters' // a unique identifier for this form
})(reduxConnectedComponent)
