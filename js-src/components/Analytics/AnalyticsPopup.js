import reactable from '../Chunks/ChunkReactable'
import AssetDetail from '../Assets/AssetDetail';
import CampaignDetail from '../Campaigns/CampaignDetail'
import Globals from '../../Globals'
import ImageLoader from 'react-imageloader'
import moment from '../Chunks/ChunkMoment';
import PreLoader from '../PreLoader'
import PreLoaderMaterial from '../PreLoaderMaterial'
import PopupWrapper from '../PopupWrapper'
import Post from '../Feed/Post'


class AnalyticsPopup extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      moment: null,
      reactable:null
    }
  }
  renderLoading () {
    return (
      <div className='preloader-wrap'>
        <PreLoader className='Page' />
      </div>
    )
  }
  componentWillMount() {

    moment().then(moment => {
      reactable().then(reactable=>{
        this.setState({moment: moment,reactable:reactable})
      })
    })
  }

  renderAssetDetail() {
    var asset = this.props.popupData;
    if (asset !== undefined) {

      var assetDetail = {
        identity: 1,
        title: '',
        media_extension: '',
        media_type: 'application',
        media_url: '',
        thumbnail_url: '',
        approved: false,
        created_date: '',
        assetAuthor: '',
        tags: [],
        detail: '',
        userTag: [],
        firstname: '',
        lastname: ''
      }

      assetDetail = {
        identity: asset.identity,
        title: asset.title,
        media_type: asset.media_type,
        media_url: asset.media_url,
        thumbnail_url: asset.thumbnail_url,
        media_extension: asset.media_extension,
        approved: asset.approved,
        created_date: asset.created_date,
        assetAuthor: asset.assetAuthor,
        tags: asset.tags,
        detail: asset.detail,
        userTag: asset.userTag,
        firstname: asset.firstname,
        lastname: asset.lastname
      }
      return (

        <div className='analyticPopupColumnWrapper clearfix'>
          <div className='PostDetailColumn'>
            <AssetDetail moment={this.state.moment} asset={assetDetail} locationInfo={this.props.location}/>

          </div>
          <div className='PostPerformanceColumn'>
            <div className='totalEngagementWrapper'>
              <div className='totalEngagement'>
                <strong> {asset.total_count} </strong>
                times used in post</div>
            </div>
            <div className='analyticPostDataWrapper'>
            {this.state.reactable?
              <this.state.reactable.Table>
                <this.state.reactable.Tr>
                  <this.state.reactable.Td column='channel' className='channel'>
                    <span className='tdInner'>
                      <span class="social fb">
                        <i class="fa fa-facebook" aria-hidden="true"></i>
                      </span>
                    </span>
                  </this.state.reactable.Td>
                  <this.state.reactable.Td column='likes' className='likes'>
                    <span className='tdInner'>
                      <strong>{asset.facebook_count}</strong>Times</span>
                  </this.state.reactable.Td>

                </this.state.reactable.Tr>
                <this.state.reactable.Tr>
                  <this.state.reactable.Td column='channel' className='channel'>
                    <span className='tdInner'>
                      <span class="social tw">
                        <i class="fa fa-twitter" aria-hidden="true"></i>
                      </span>
                    </span>
                  </this.state.reactable.Td>
                  <this.state.reactable.Td column='likes' className='likes'>
                    <span className='tdInner'>
                      <strong>{asset.twitter_count}</strong>Times</span>
                  </this.state.reactable.Td>
                </this.state.reactable.Tr>
                <this.state.reactable.Tr>
                  <this.state.reactable.Td column='channel' className='channel'>
                    <span className='tdInner'>
                      <span class="social li">
                        <i class="fa fa-linkedin" aria-hidden="true"></i>
                      </span>
                    </span>
                  </this.state.reactable.Td>
                  <this.state.reactable.Td column='likes' className='likes'>
                    <span className='tdInner'>
                      <strong>{asset.linkedin_count}</strong>Times</span>
                  </this.state.reactable.Td>
                </this.state.reactable.Tr>
                <this.state.reactable.Tr>
                  <this.state.reactable.Td column='channel' className='channel'>
                    <span className='tdInner'>
                      <span class="social iw">
                      <svg
                                    class="nav__image image"
                                    xmlns="http://www.w3.org/2000/svg"
                                    viewBox="0 0 69 95.95">
                                    <defs></defs>
                                    <g id="Layer_2" data-name="Layer 2">
                                      <g id="Layer_1-2" data-name="Layer 1">
                                        <path
                                          class="svg-cls-2"
                                          d="M17.21,22V49.66H40.52V72.71H67.85V22ZM57.73,62.58H50.64V39.52H27.33v-7.4h30.4Z"></path>
                                        <path
                                          class="svg-cls-3"
                                          d="M50.64,90.25V62.57H27.33V39.52H0V90.25ZM10.12,49.66h7.09V72.71H40.52v7.4H10.12Z"></path>
                                      </g>
                                    </g>
                                  </svg>
                      </span>
                    </span>
                  </this.state.reactable.Td>
                  <this.state.reactable.Td column='likes' className='likes'>
                    <span className='tdInner'>
                      <strong>{asset.internal_count}</strong>Times</span>
                  </this.state.reactable.Td>
                </this.state.reactable.Tr>
              </this.state.reactable.Table>:''}
            </div>
          </div>
        </div>

      )
    }
    // else {   return (     <div>       <h1>There is no media</h1>     </div>   );
    // }

  }
  renderPostDetail() {
    var postdetail = this.props.popupData;
    var like = 0
      var share = 0
        var comment = 0
          var username = ''
            var channel = [],
              avatar = '';
            let facebookdatasum = 0;
            let twitterdatasum = 0;
            let linkedindatasum = 0;
            let internaldatasum = 0;
               if(postdetail.SocialAccount.data.length>0 && postdetail.SocialAccount.data[0].length != 0) {
              postdetail
                .SocialAccount
                .data
                .map(td => {
                  channel.push(td.SocialAccount.data.social_media)
                })
            } else {
              channel = ['internal']
            }
            var postSocialData=postdetail.post?postdetail.post.data?postdetail.post.data.social_count?postdetail.post.data.social_count.data:'':'':''
            //for total count of facebook ,linkedin,internal,twitter like ,comment ,share
            if (channel.indexOf('facebook') > -1) {
              facebookdatasum = postSocialData?postSocialData.facebook.likes + postSocialData.facebook.comments + postSocialData.facebook.shares:0
            }
            if (channel.indexOf('twitter') > -1) {
              twitterdatasum = postSocialData?postSocialData.twitter.shares + postSocialData.twitter.likes + postSocialData.twitter.comments:0
            }
            if (channel.indexOf('linkedin') > -1) {
              linkedindatasum = postSocialData?postSocialData.linkedin.likes + postSocialData.linkedin.comments + postSocialData.linkedin.shares:0
            }
            internaldatasum = postdetail.post.data.like_count + postdetail.post.data.comment_count + postdetail.post.data.share_count
            let totalsum = internaldatasum + facebookdatasum + twitterdatasum + linkedindatasum
            // if (this.props.popupData.postSelectedData.length > 0) {   if
            // (postdetail[0].post.data !== undefined) {     like =
            // postdetail[0].post.data.like_count     share =
            // postdetail[0].post.data.share_count     comment =
            // postdetail[0].post.data.comment_count     username =
            // postdetail[0].post.data.postOwner.data.firstname + ' ' +
            // postdetail[0].post.data.postOwner.data.lastname     avatar =
            // postdetail[0].post.data.postOwner.data.avatar   } }
            return (
              <div className='analyticPopupColumnWrapper clearfix'>
                <div className='PostDetailColumn'>
                  <div id='post-list-wrapper'>
                    <Post details={postdetail} locationInfo={this.props.location}/> {/* likes comments shares */}
                  </div>
                </div>
                <div className='PostPerformanceColumn'>
                  <div className='totalEngagementWrapper'>
                      {
                       postdetail.post.data.type =='external' ? 
                    
                      <div className='totalEngagement'>
                        <strong>0</strong>
                        people reached</div>
                        :''
                      }
                    <div className='totalEngagement'>
                      <strong>{totalsum}</strong>
                       Likes, Comments &amp; Shares</div>
                  </div>
                  <div className='analyticPostDataWrapper'>
                  {this.state.reactable?
                    <this.state.reactable.Table>
                      {channel.indexOf('internal') == -1
                        ? <this.state.reactable.Tr>
                            <this.state.reactable.Td column='channel' className='channel'>
                              <span className='tdInner'>
                                <span class="social iw">
                                  <svg
                                    class="nav__image image"
                                    xmlns="http://www.w3.org/2000/svg"
                                    viewBox="0 0 69 95.95">
                                    <defs></defs>
                                    <g id="Layer_2" data-name="Layer 2">
                                      <g id="Layer_1-2" data-name="Layer 1">
                                        <path
                                          class="svg-cls-2"
                                          d="M17.21,22V49.66H40.52V72.71H67.85V22ZM57.73,62.58H50.64V39.52H27.33v-7.4h30.4Z"></path>
                                        <path
                                          class="svg-cls-3"
                                          d="M50.64,90.25V62.57H27.33V39.52H0V90.25ZM10.12,49.66h7.09V72.71H40.52v7.4H10.12Z"></path>
                                      </g>
                                    </g>
                                  </svg>
                                </span>
                              </span>
                            </this.state.reactable.Td>
                            <this.state.reactable.Td column='likes' className='likes'>
                              <span className='tdInner'>
                                <strong>{postdetail.post.data.like_count}</strong>Likes</span>
                            </this.state.reactable.Td>
                            <this.state.reactable.Td column='comments' className='comments'>
                              <span className='tdInner'>
                                <strong>{postdetail.post.data.comment_count}</strong>Comments</span>
                            </this.state.reactable.Td>
                            <this.state.reactable.Td column='shar1es' className='shares'>
                              <span className='tdInner'>
                                <strong>{postdetail.post.data.share_count}</strong>Shares</span>
                            </this.state.reactable.Td>
                          </this.state.reactable.Tr>
                        : ''
}
                      {channel.indexOf('facebook') > -1
                        ? <this.state.reactable.Tr>
                            <this.state.reactable.Td column='channel' className='channel'>
                              <span className='tdInner'>
                                <span class="social fb">
                                  <i class="fa fa-facebook" aria-hidden="true"></i>
                                </span>
                              </span>
                            </this.state.reactable.Td>
                            <this.state.reactable.Td column='likes' className='likes'>
                              <span className='tdInner'>
                                <strong>{postSocialData?postSocialData.facebook.likes:0}</strong>Likes</span>
                            </this.state.reactable.Td>
                            <this.state.reactable.Td column='comments' className='comments'>
                              <span className='tdInner'>
                                <strong>{postSocialData?postSocialData.facebook.comments:0}</strong>Comments</span>
                            </this.state.reactable.Td>
                            <this.state.reactable.Td column='shares' className='shares'>
                              <span className='tdInner'>
                                <strong>{postSocialData?postSocialData.facebook.shares:0}</strong>Shares</span>
                            </this.state.reactable.Td>
                          </this.state.reactable.Tr>
                        : ''}
                      {channel.indexOf('twitter') > -1
                        ? <this.state.reactable.Tr>
                            <this.state.reactable.Td column='channel' className='channel'>
                              <span className='tdInner'>
                                <span class="social tw">
                                  <i class="fa fa-twitter" aria-hidden="true"></i>
                                </span>
                              </span>
                            </this.state.reactable.Td>
                            <this.state.reactable.Td column='likes' className='likes'>
                              <span className='tdInner'>
                                <strong>{postSocialData?postSocialData.twitter.shares:0}</strong>Retweets</span>
                            </this.state.reactable.Td>
                            <this.state.reactable.Td column='comments' className='comments'>
                              <span className='tdInner'>
                                <strong>{postSocialData?postSocialData.twitter.likes:0}</strong>Favorites</span>
                            </this.state.reactable.Td>
                            <this.state.reactable.Td column='shares' className='shares'>
                              <span className='tdInner'>
                                <strong>{postSocialData?postSocialData.twitter.comments:0}</strong>Replies</span>
                            </this.state.reactable.Td>
                          </this.state.reactable.Tr>
                        : ''}
                      {channel.indexOf('linkedin') > -1
                        ? <this.state.reactable.Tr>
                            <this.state.reactable.Td column='channel' className='channel'>
                              <span className='tdInner'>
                                <span class="social li">
                                  <i class="fa fa-linkedin" aria-hidden="true"></i>
                                </span>
                              </span>
                            </this.state.reactable.Td>
                            <this.state.reactable.Td column='likes' className='likes'>
                              <span className='tdInner'>
                                <strong>{postSocialData?postSocialData.linkedin.likes:0}</strong>Likes</span>
                            </this.state.reactable.Td>
                            <this.state.reactable.Td column='comments' className='comments'>
                              <span className='tdInner'>
                                <strong>{postSocialData?postSocialData.linkedin.comments:0}</strong>Comments</span>
                            </this.state.reactable.Td>
                            <this.state.reactable.Td column='shares' className='shares'>
                              <span className='tdInner'>
                                <strong>{postSocialData?postSocialData.linkedin.shares:0}</strong>Shares</span>
                            </this.state.reactable.Td>
                          </this.state.reactable.Tr>
                        : ''}
                      {channel.indexOf('internal') > -1
                        ? <this.state.reactable.Tr>
                            <this.state.reactable.Td column='channel' className='channel'>
                              <span className='tdInner'>
                                <span class="social iw">
                                  <svg
                                    class="nav__image image"
                                    xmlns="http://www.w3.org/2000/svg"
                                    viewBox="0 0 69 95.95">
                                    <defs></defs>
                                    <g id="Layer_2" data-name="Layer 2">
                                      <g id="Layer_1-2" data-name="Layer 1">
                                        <path
                                          class="svg-cls-2"
                                          d="M17.21,22V49.66H40.52V72.71H67.85V22ZM57.73,62.58H50.64V39.52H27.33v-7.4h30.4Z"></path>
                                        <path
                                          class="svg-cls-3"
                                          d="M50.64,90.25V62.57H27.33V39.52H0V90.25ZM10.12,49.66h7.09V72.71H40.52v7.4H10.12Z"></path>
                                      </g>
                                    </g>
                                  </svg>
                                </span>
                              </span>
                            </this.state.reactable.Td>
                            <this.state.reactable.Td column='likes' className='likes'>
                              <span className='tdInner'>
                                <strong>{postdetail.post.data.like_count}</strong>Likes</span>
                            </this.state.reactable.Td>
                            <this.state.reactable.Td column='comments' className='comments'>
                              <span className='tdInner'>
                                <strong>{postdetail.post.data.comment_count}</strong>Comments</span>
                            </this.state.reactable.Td>
                            <this.state.reactable.Td column='shares' className='shares'>
                              <span className='tdInner'>
                                <strong>{postdetail.post.data.share_count}</strong>Shares</span>
                            </this.state.reactable.Td>
                          </this.state.reactable.Tr>
                        : ''
}
                    </this.state.reactable.Table>:''}
                  </div>
                </div>
              </div>
            )
          }
          renderCampaignDetail() {
            let campaignDetail = this.props.popupData;
            let campaignAnalyticsPopupDetails = this.props.props.analytics.campaingPopupData
        return(
            <div className='analyticPopupColumnWrapper clearfix'>
             {campaignAnalyticsPopupDetails == null  ? this.renderLoading() :
                <div>
                <div className='PostDetailColumn'>
                <CampaignDetail
                campaign={campaignDetail}
                Moment={this.state.moment}
                //channels={campaignDetail.channels}
                //editdata={campaignDetail.editdata}
              />

              </div>
          <div className='PostPerformanceColumn'>
            <div className='totalEngagementWrapper'>
              <div className='totalEngagement'>
                <strong>{campaignAnalyticsPopupDetails.post_count}</strong>
                posts</div>
              <div className='totalEngagement'>
                <strong>{campaignAnalyticsPopupDetails.asset_count}</strong>
                assets</div>
              <div className='totalEngagement'>
                <strong>{campaignAnalyticsPopupDetails.reach_count}</strong>
                people reached</div>
              <div className='totalEngagement'>
                <strong>{campaignAnalyticsPopupDetails.totalCount}</strong>
                likes, comments &amp; shares</div>
            </div>
            <div className='analyticPostDataWrapper'>
            {this.state.reactable?
              <this.state.reactable.Table>
                <this.state.reactable.Tr>
                  <this.state.reactable.Td column='channel' className='channel'>
                    <span className='tdInner'>
                      <span class="social fb">
                        <i class="fa fa-facebook" aria-hidden="true"></i>
                      </span>
                    </span>
                  </this.state.reactable.Td>
                  <this.state.reactable.Td column='likes' className='likes'>
                    <span className='tdInner'>
                      <strong>{campaignAnalyticsPopupDetails.facebook.likes}</strong>Likes</span>
                  </this.state.reactable.Td>
                  <this.state.reactable.Td column='comments' className='comments'>
                    <span className='tdInner'>
                      <strong>{campaignAnalyticsPopupDetails.facebook.comments}</strong>Comments</span>
                  </this.state.reactable.Td>
                  <this.state.reactable.Td column='shares' className='shares'>
                    <span className='tdInner'>
                      <strong>{campaignAnalyticsPopupDetails.facebook.shares}</strong>Shares</span>
                  </this.state.reactable.Td>
                </this.state.reactable.Tr>
                <this.state.reactable.Tr>
                  <this.state.reactable.Td column='channel' className='channel'>
                    <span className='tdInner'>
                      <span class="social tw">
                        <i class="fa fa-twitter" aria-hidden="true"></i>
                      </span>
                    </span>
                  </this.state.reactable.Td>
                  <this.state.reactable.Td column='likes' className='likes'>
                    <span className='tdInner'>
                      <strong>{campaignAnalyticsPopupDetails.twitter.shares}</strong>Retweets</span>
                  </this.state.reactable.Td>
                  <this.state.reactable.Td column='comments' className='comments'>
                    <span className='tdInner'>
                      <strong>{campaignAnalyticsPopupDetails.twitter.likes}</strong>Favorites</span>
                  </this.state.reactable.Td>
                  <this.state.reactable.Td column='shares' className='shares'>
                    <span className='tdInner'>
                      <strong>{campaignAnalyticsPopupDetails.twitter.comments}</strong>Replies</span>
                  </this.state.reactable.Td>
                </this.state.reactable.Tr>
                <this.state.reactable.Tr>
                  <this.state.reactable.Td column='channel' className='channel'>
                    <span className='tdInner'>
                      <span class="social li">
                        <i class="fa fa-linkedin" aria-hidden="true"></i>
                      </span>
                    </span>
                  </this.state.reactable.Td>
                  <this.state.reactable.Td column='likes' className='likes'>
                    <span className='tdInner'>
                      <strong>{campaignAnalyticsPopupDetails.linkedin.likes}</strong>Likes</span>
                  </this.state.reactable.Td>
                  <this.state.reactable.Td column='comments' className='comments'>
                    <span className='tdInner'>
                      <strong>{campaignAnalyticsPopupDetails.linkedin.comments}</strong>Comments</span>
                  </this.state.reactable.Td>
                  <this.state.reactable.Td column='shares' className='shares'>
                    <span className='tdInner'>
                      <strong>{campaignAnalyticsPopupDetails.linkedin.shares}</strong>Shares</span>
                  </this.state.reactable.Td>
                </this.state.reactable.Tr>
                <this.state.reactable.Tr>
                  <this.state.reactable.Td column='channel' className='channel'>
                    <span className='tdInner'>
                      <span class="social iw">
                      <svg
                              class="nav__image image"
                              xmlns="http://www.w3.org/2000/svg"
                              viewBox="0 0 69 95.95">
                              <defs></defs>
                              <g id="Layer_2" data-name="Layer 2">
                                <g id="Layer_1-2" data-name="Layer 1">
                                  <path
                                    class="svg-cls-2"
                                    d="M17.21,22V49.66H40.52V72.71H67.85V22ZM57.73,62.58H50.64V39.52H27.33v-7.4h30.4Z"></path>
                                  <path
                                    class="svg-cls-3"
                                    d="M50.64,90.25V62.57H27.33V39.52H0V90.25ZM10.12,49.66h7.09V72.71H40.52v7.4H10.12Z"></path>
                                </g>
                              </g>
                            </svg>
                      </span>
                    </span>
                  </this.state.reactable.Td>
                  <this.state.reactable.Td column='likes' className='likes'>
                    <span className='tdInner'>
                      <strong>{campaignAnalyticsPopupDetails.internal.likes}</strong>Likes</span>
                  </this.state.reactable.Td>
                  <this.state.reactable.Td column='comments' className='comments'>
                    <span className='tdInner'>
                      <strong>{campaignAnalyticsPopupDetails.internal.comments}</strong>Comments</span>
                  </this.state.reactable.Td>
                  <this.state.reactable.Td column='shares' className='shares'>
                    <span className='tdInner'>
                      <strong>{campaignAnalyticsPopupDetails.internal.shares}</strong>Shares</span>
                  </this.state.reactable.Td>
                </this.state.reactable.Tr>
              </this.state.reactable.Table>:''}
            </div>
          </div>
          </div>
              }
  </div>

            )
          }
          renderPopup() {

            return (
              <section className='analyticDetailPopup'>
                <header className='heading'>
                  <h3>{`${this.props.dataType} Performance`}</h3>
                </header>
                <PopupWrapper>
                  {this.props.dataType == 'post'
                    ? this.renderPostDetail() //Post
                    : ''
}
                  {this.props.dataType == 'asset'
                    ? this.renderAssetDetail() //asset
                    : ''
}
                  {this.props.dataType == 'campaign'
                    ? this.renderCampaignDetail()
                    : ''
}
                </PopupWrapper>
              </section>

            //   <div className='inner-container'>   <div className='post-container'>
            // <div>       <ImageLoader         src={avatar?avatar             :
            // '/img/default-avatar.png'         }         wrapper={React.DOM.div}
            // preloader={PreLoaderMaterial}       >         Image load failed!
            // </ImageLoader>       {/* <img src="/img/default-avatar.png"/> */}     </div>
            //    <div className='post-text'>       <p>         I am {username}       </p>
            //   </div>   </div>   <div className='details-container'>     <div
            // className='detailpost clearfix'>       <div className='like-post text-blog'>
            //        <div className='clearfix like-icon'>           <i
            // class='material-icons'></i>           <p className='num'>             {like}
            //           </p>           <p className='text'>             Likes
            // </p>         </div>       </div>       <div className='share-post text-blog'>
            //         <div className='clearfix like-icon'>           <i
            // class='material-icons'></i>           <p className='num'>
            // {share}           </p>           <p className='text'>Share</p>         </div>
            //       </div>     </div>     <div className='comment-post text-blog'>
            // <div className='clearfix like-icon'>         <i class='material-icons'></i>
            //        <p className='num'>           {comment}         </p>         <p
            // className='text'>           Comment         </p>       </div>     </div>
            // </div> </div>
            )
          }
          render() {
            return (
              <section>
                <div className='anytics-container-popup'>
                  {this.props.popupData.loading == true
                    ? <PreLoader className="Page"/>
                    : this.renderPopup()}
                </div>
              </section>
            )
          }
        }
  module.exports = AnalyticsPopup
