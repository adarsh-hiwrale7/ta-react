import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import PreLoader from '../PreLoader'
import Header from '../Header/Header';
import Globals from '../../Globals';
import * as analyticsActions from '../../actions/analytics/analyticsActions'
import AnalyticsAdoptionIndex from './Adoption/AnalyticsAdoptionIndex'
import QualityIndex from './Quality/QualityIndex';
import AssetsIndex from './Assets/AssetsIndex';

import TagCloudIndex from './TagCloud/TagCloudIndex';
import * as utils from '../../utils/utils'
class TotalSocialEngagement extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
    }
  }

  render() {
      return(

<div className = "inner-Total-Social-engagement clearfix">
 {/*heading part  */}
 <div className = "stickey-header">
 <div className = "header-anayltics header-for-comman-filter"> 
   <div className = "header-analytics-wrapper clearfix"> 
       <div className = "filter-weekey-container clearfix"> 
       <div className = "filter-container">
            <a className = "week-filter"> Week </a>
            <a className = "month-filter">  Month  </a>
            <a className = "year-filter">Year </a>
            <span> <i class="material-icons">filter_list</i>  </span>
        </div>
   </div>
  </div>
 </div>

 <div className = "header-anayltics header-for-comman-expand"> 
  <div className = "header-analytics-wrapper clearfix"> 
      <a className = "expand-link"> Expand <span><i class="material-icons">navigate_next</i></span> </a>
    </div>
 </div>
 </div>
    <div className = "inner-social-part1"> 
    {/* headerpart */}
    <div className = "header-anayltics"> 
    <div className = "header-analytics-wrapper clearfix"> 
        
      <div className = "title"> Total Social engagement <span className="svg-icon help-element"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M0 0h24v24H0z" fill="none"/><path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm1 17h-2v-2h2v2zm2.07-7.75l-.9.92C13.45 12.9 13 13.5 13 15h-2v-.5c0-1.1.45-2.1 1.17-2.83l1.24-1.26c.37-.36.59-.86.59-1.41 0-1.1-.9-2-2-2s-2 .9-2 2H8c0-2.21 1.79-4 4-4s4 1.79 4 4c0 .88-.36 1.68-.93 2.25z"/></svg></span>
      </div>
    
      {/* <div className = "filter-weekey-container clearfix"> 
           <a className = "week-filter"> Week </a>
           <a className = "month-filter">  Month  </a>
           <a className = "year-filter">Year </a>
           <span> <i class="material-icons">filter_list</i>  </span>
       </div>
        */}
     </div>
    </div>
    {/* end headr part */}
    {/* body part */}
    <div className = "body-container-social-engagement Total-Social-engagement-body clearfix">
        <div className = "block-part1"> 
        <div className = "anyalitcs-tabuler-data"> 
             <table>
                  <tr> 
                    <td className = "rank-title-td"> <div className="rank-title"> Shares </div> </td>
                    <td className = "rank-no-td">  <div className="rank-no"> 334 </div> </td>
                    <td className = "chart-td"> <div className="small-chart">  <img src = "/img/down.png" /></div><div className = "coun-diff"> 05 </div></td>
                  
                  </tr>
                  <tr> 
                    <td className = "rank-title-td"> <div className="rank-title">Retweets</div> </td>
                    <td className = "rank-no-td">  <div className="rank-no"> 322 </div> </td>
                    <td className = "chart-td"> <div className="small-chart">  <img src = "/img/down.png" /></div><div className = "coun-diff"> 05 </div></td>
                
                  </tr>
                  <tr> 
                    <td className = "rank-title-td"> <div className="rank-title"> Views </div> </td>
                    <td className = "rank-no-td">  <div className="rank-no"> 333 </div> </td>
                    <td className = "chart-td">  <div className = "rank-data-container"> <div className="small-chart">  <img src = "/img/down.png" /></div><div className = "coun-diff"> 05 </div></div></td>
                  
                  </tr>
                  
                </table>
                </div>
              
              </div>
        
        <div className = "block-part2">
        <div className = "anyalitcs-tabuler-data"> 
             <table>
                  <tr> 
                    <td className = "rank-title-td"> <div className="rank-title"> Reach </div> </td>
                    <td className = "rank-no-td">  <div className="rank-no"> 344 </div> </td>
                    <td className = "chart-td"> <div className="small-chart">  <img src = "/img/down.png" /></div><div className = "coun-diff"> 05 </div> </td>
                
                  </tr>
                  <tr> 
                    <td className = "rank-title-td"> <div className="rank-title"> Comments </div> </td>
                    <td className = "rank-no-td">  <div className="rank-no"> 333 </div> </td>
                    <td className = "chart-td"> <div className="small-chart">  <img src = "/img/down.png" /></div> <div className = "coun-diff"> 05 </div></td>
                
                  </tr>
                  <tr> 
                    <td  className = "rank-title-td"> <div className="rank-title"> Likes </div> </td>
                    <td className = "rank-no-td">  <div className="rank-no"> 344</div> </td>
                    <td className = "chart-td">  <div className="small-chart">  <img src = "/img/down.png" /></div> <div className = "coun-diff"> 05 </div></td>
        
                  </tr>
                  
                </table>
                </div>
              
         </div>
    </div>
    {/* end body part */}
    </div>
    <div className = "inner-social-part2"> 
    {/* headepar */}
      <div className = "header-anayltics"> 
      <div className = "header-analytics-wrapper clearfix">
        <div className = "int-engagement"> Internal engagement<span className="svg-icon help-element"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M0 0h24v24H0z" fill="none"/><path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm1 17h-2v-2h2v2zm2.07-7.75l-.9.92C13.45 12.9 13 13.5 13 15h-2v-.5c0-1.1.45-2.1 1.17-2.83l1.24-1.26c.37-.36.59-.86.59-1.41 0-1.1-.9-2-2-2s-2 .9-2 2H8c0-2.21 1.79-4 4-4s4 1.79 4 4c0 .88-.36 1.68-.93 2.25z"/></svg></span>
        </div>
        <a className = "expand-link"> Expand <span><i class="material-icons">navigate_next</i></span> </a>
        
      </div>
      </div>
    {/* end header part */}
    <div className = "anyalitcs-tabuler-data"> 
             <table>
                  <tr> 
                    <td  className = "rank-title-td"> <div className="rank-title"> Mentions </div> </td>
                    <td className = "rank-no-td">  <div className="rank-no"> 333 </div> </td>
                    <td className = "chart-td"> <div className="small-chart">  <img src = "/img/down.png" /></div><div className = "coun-diff"> 05 </div></td>
                  
                  </tr>
                  
                  <tr> 
                    <td className = "rank-title-td"> <div className="rank-title"> Comments </div> </td>
                    <td className = "rank-no-td">  <div className="rank-no"> 333 </div> </td>
                    <td className = "chart-td"> <div className="small-chart">  <img src = "/img/down.png" /></div><div className = "coun-diff"> 05 </div></td>
                   
                  </tr>
                  
                  <tr> 
                    <td className = "rank-title-td"> <div className="rank-title">Likes </div> </td>
                    <td className = "rank-no-td">  <div className="rank-no"> 3 </div> </td>
                    <td className = "chart-td"> <div className="small-chart">  <img src = "/img/down.png" /></div><div className = "coun-diff"> 05 </div></td>
                  </tr>
                  
                </table>
                </div>
              
    </div>
 
 {/* body part */}
 
</div>

      )}
}
module.exports = TotalSocialEngagement;
