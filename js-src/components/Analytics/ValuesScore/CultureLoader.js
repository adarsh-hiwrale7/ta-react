import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

class CultureLoader extends React.Component {
    constructor(props) {
        super(props);
    }
    componentDidMount(){
       
    }
    render() {
       
        return (
           
             <div className="culture-index-loader">
                                        <div class="participation">
                                            <div class="participation-piechart-wrapper cf piechart-wrapper">
                                                <div class="recharts-wrapper recharts-loader-wrapper">
                                                    <svg class="recharts-surface recharts-surface-loader loader-grey-line loader-line-radius loader-line-height" width="200" height="200" viewBox="0 0 200 200" version="1.1">
                                                    </svg>
                                                    <div class="recharts-legend-wrapper rechart-loader-wrapper">
                                                        <ul>
                                                            <li class="loader-grey-line loader-line-radius loader-line-height">
                                                            </li>
                                                            <li class="loader-grey-line loader-line-radius loader-line-height">

                                          </li></ul></div>
                                 </div>
                         </div>
                 </div>
                                    
           
        </div>
        );
    }
}
function mapStateToProps(state) {
    return {
    }
}
function mapDispatchToProps(dispatch) {
    return {
    }
}
let connection = connect(mapStateToProps, mapDispatchToProps);
module.exports = connection(CultureLoader);                               