import { IndexLink, Link } from 'react-router'
class AnalyticsNav extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
    }
  }
  render() {
    return (
      this.props.location.pathname !== undefined && this.props.location.pathname.includes("analytics") ?
        <div className="culture-dashboard-tabbing-sec clearfix">
          <Link
            to='/analytics/adoption'
            title='Adoption'
            className={this.props.location.pathname == '/analytics/adoption' ? 'active' : ''}
          >
           Adoption
          </Link>
          <Link
            to='/analytics/activity'
            title='Activity'
            className={this.props.location.pathname == '/analytics/activity' ? 'active' : ''}
          >
           Activity
          </Link>
          <Link
            to='/analytics/engagement'
            title='Reach'
            className={this.props.location.pathname == '/analytics/engagement' ? 'active' : ''}
          >
           Reach
          </Link>
          <Link
            to='/analytics/values'
            title='NVS'
            className={this.props.location.pathname == '/analytics/values' ? 'active' : ''}
          >
           NVS
          </Link>
          <Link
            to='/analytics/NPS'
            title='NPS'
            className={this.props.location.pathname == '/analytics/NPS' ? 'active' : ''}
          >
           NPS
          </Link>
          <Link
            to='/analytics/culture'
            title='culture'
            class={this.props.location.pathname == '/analytics/culture' ? 'active' : ''}
          >
            Culture
           </Link>
           <Link
            to='/analytics/resonance'
            title='resonance'
            class={this.props.location.pathname == '/analytics/resonance' ? 'active' : ''}
            >
              Resonance
           </Link>
         
        </div>

        : ''
    )
  }
}

module.exports = AnalyticsNav
