import reactable from '../Chunks/ChunkReactable'
import * as utils from '../../utils/utils'
import Globals from '../../Globals'
import moment from '../Chunks/ChunkMoment'

import FilterTableColumns from '../FilterTableColumns';
import Pagination from '../Chunks/ChunkReactPagination';
var lis = []
var count = 0
var pagination=1
var location=''
var countColums = 0
var tableColumns ;
class AnalyticsTable extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      moment: null,
      activePage:1,
      reactable:null,
      Pagination:null
    }
  }
  componentDidMount () {
    countColums = tableColumns.length
    if(typeof this.props !== 'undefined')
    if(location!==this.props.location.pathname)
    {
      location=this.props.location.pathname
      pagination=1
    }
  }
  componentWillMount () {
    moment().then(moment => {
      Pagination().then(pagination=>{
      reactable().then(reactable=>{
        this.setState({ moment: moment,reactable:reactable,Pagination:pagination })
      })
      })
    })
  }
  changeState (stateName, value) {
    var newState = {}
    newState[stateName] = value
    this.setState(newState)
  }
  checkDetails (e) {
    var i = 0
    var checkboxvalue = document.getElementById(e).checked
    if (checkboxvalue == true) {
      this.changeState(e, '')
      countColums++
    } else {
      this.changeState(e, 'hide')
      countColums--
    }
  }
  createDropdown (tableColumns) {
    var me = this
    return (
      <ul class='dropdown-menu checkboxDropdown'>
        {tableColumns
          ? tableColumns.map(function (column, columnIndex) {
            let columnState = column[0]
            let columnLabel = column[1]
            if (me.state[columnState] == undefined) {
              me.setState({
                [columnState]: ''
              })
            }
            return (
              <li key={columnIndex}>
                <div className='form-row checkbox'>
                  <input
                    id={columnState}
                    name={columnState}
                    className='checknotify'
                    type='checkbox'
                    onChange={me.checkDetails.bind(me, columnState)}
                    checked={me.state[columnState] == ''}
                    />
                  <label for={columnState}>{columnLabel}</label>
                </div>
              </li>
            )
          })
          : ''}
      </ul>
    )
  }

  tablePopupanylitcs (data) {
    this.props.popupanylitcs(data)
  }
  fetchOtherPageData (pageno) {
    // props comes from analyticsContainer to send next page number
    this.props.pageNo(pageno)
  }
  /**
   * @author disha
   * to add page in table
   * @param {*} totalPage
   */
  handlePageChange(pageNumber)
  {
    this.setState({
      activePage: pageNumber
    })
    pagination=pageNumber
    this.fetchOtherPageData(pageNumber)
  }
  renderLoader(){
    return(
    <div  className = " loader-culture-tableExpand"> 
         <div class="widget">
    <div class="table-anytlics-body">
        <table class="table responsive-table">
            <thead>
                <tr class="reactable-column-header">
                    <th class="reactable-th-serialno reactable-header-sortable serialno  a-center"> 
                    <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                     </th>
                     <th class="reactable-th-serialno reactable-header-sortable serialno  a-center"> 
                    <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                     </th>
                     <th class="reactable-th-serialno reactable-header-sortable serialno  a-center"> 
                    <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                     </th>
                     <th class="reactable-th-serialno reactable-header-sortable serialno  a-center"> 
                    <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                     </th>
                     <th class="reactable-th-serialno reactable-header-sortable serialno  a-center"> 
                    <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                     </th>
                     <th class="reactable-th-serialno reactable-header-sortable serialno  a-center"> 
                    <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                     </th>
                     <th class="reactable-th-serialno reactable-header-sortable serialno  a-center"> 
                    <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                     </th>
                     <th class="reactable-th-serialno reactable-header-sortable serialno  a-center"> 
                    <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                     </th>
                     <th class="reactable-th-serialno reactable-header-sortable serialno  a-center"> 
                    <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                     </th>
                </tr>
            </thead>
            <tbody class="reactable-data">
              <tr class="table-body-text">
                <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                
               </tr>
               <tr class="table-body-text">
                <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                
               </tr>
               <tr class="table-body-text blanck-div-loader-totalpost">
                <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                
               </tr>
               <tr class="blanck-div-loader-totalpost table-body-text"><td></td> <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td> </tr>
               <tr class="blanck-div-loader-totalpost table-body-text"><td></td> <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td> </tr>
               <tr class="blanck-div-loader-totalpost table-body-text"><td></td> <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td> </tr>
               <tr class="blanck-div-loader-totalpost table-body-text"><td></td> <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td> </tr>
               <tr class="blanck-div-loader-totalpost table-body-text"><td></td> <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td> </tr>
               <tr class="blanck-div-loader-totalpost table-body-text"><td></td> <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td> </tr>

            </tbody>
        </table>
    </div>
</div>
         </div>
         )
      {/* end lloader */}
  }
  renderFeedTable(analyticsData, tableColumns){
    var channel = []
    var AnalyticsTabledata = []
    var AnalyticsPageDetail = {}
    var totalPage = 0
    var routename = this.props.route
      if (Object.keys(this.props.analyticsData.feedAnalytics).length>0) {
        if(analyticsData.feedAnalytics.data.length>0){
        AnalyticsTabledata = analyticsData.feedAnalytics.data
        AnalyticsPageDetail = analyticsData.feedAnalytics.meta.pagination
        }
      
    }
    var me = this
    if (AnalyticsPageDetail) {
      totalPage = Math.ceil(AnalyticsPageDetail.total / 10)
    }
    return(
     <div className='widget'>
        {countColums == 0
          ? <div className='no-data-block'> Select a Columns</div>
          : <div className='table-anytlics-body'>
          {this.state.reactable?
            <this.state.reactable.Table
              className='table responsive-table'
              itemsPerPage={10}
              pageButtonLimit={5}
              sortable
              >
              <this.state.reactable.Thead>
                {tableColumns.map(function (column, columnIndex) {
                  let columnState = column[0]
                  let columnLabel = column[1]
                  let centerClass = column[2]
                  return (
                    <me.state.reactable.Th
                      key={columnIndex}
                      column={columnState}
                      className={`${columnState} ${me.state[columnState]} ${centerClass}`}
                      >
                      <strong className='name-header'>{columnLabel}</strong>
                      <i class='fa fa-sort' />
                    </me.state.reactable.Th>
                  )
                })}
              </this.state.reactable.Thead>
              {AnalyticsTabledata
                  ? AnalyticsTabledata.map((feeddata, i) => {
                    channel = [];
                    return (
                        // onClick={this.tablePopupanylitcs.bind(this,feeddata)}
                        (
                          <this.state.reactable.Tr
                          onClick={this.tablePopupanylitcs.bind(this,feeddata)}
                           className='table-body-text' key={i}>
                            {tableColumns.map(function (column, columnIndex) {
                              let columnState = column[0]
                              let columnLabel = column[1]
                              let centerClass = column[2]
                              let tdData;

                              if (columnState == 'serialno') {

                                var pageNoData =  pagination - 1
                                pageNoData  = pageNoData  * 10
                                pageNoData  = pageNoData  + i + 1;
                                tdData  = pageNoData ;
                               }
                              else if (columnState == 'user') {
                                tdData = feeddata.post.data.postOwner.data.firstname + ' ' + feeddata.post.data.postOwner.data.lastname
                              }
                              else if (columnState == 'post_description') {
                                if(feeddata.post.data.detail !==null && feeddata.post.data.detail.length>20)
                                {
                                tdData = feeddata.post.data.detail.substring(0, 20)+'...';
                                }
                                else{
                                  tdData = feeddata.post.data.detail
                                }
                              }
                              else if (columnState == 'created_at') {
                                tdData=me.state.moment
                                        ? me.state
                                            .moment(feeddata.post.data.created.date)
                                            .format('DD-MM-YYYY')
                                        : ''
                              }

                              else if (columnState == 'department') {
                                tdData = feeddata.post.data.postOwner.data.Department.data.name
                              }
                              else if (columnState == 'channel') {
                                tdData = feeddata.SocialAccount.data
                              }
                              else if (columnState == 'share') {
                                tdData = feeddata.post.data.share_count
                              }
                              else if (columnState == 'like') {
                                tdData = feeddata.post.data.like_count
                              }
                              else if (columnState == 'comment') {
                                tdData = feeddata.post.data.comment_count
                              } else {
                                tdData = feeddata[columnState]
                              }
                              var tdDataDesc = ''
                              columnState == 'post_description' ||
                                columnState == 'description' ||
                                columnState == 'title'
                                ? tdData!==null ?(tdDataDesc = utils.limitWords(
                                    tdData,
                                    Globals.LIMIT_WORD
                                  )):''
                                : ''
                              columnState == 'channel'?
                              feeddata.post.data.type == 'external' ?
                              tdData.map(td => {
                                channel.push(
                                    td.SocialAccount.data.social_media
                                  )})
                                : channel = ['internal']
                                : ''
                              return (
                                <me.state.reactable.Td
                                  key={columnIndex}
                                  column={columnState}
                                  className={`${columnState} ${me.state[columnState]} ${centerClass}`}
                                  data-rwd-label={columnLabel}
                                >
                                  {columnState == 'channel'
                                    ?
                                    <strong className='name-header'>
                                      <span className='socialIcons'>
                                        {channel.indexOf('facebook') > -1
                                            ? <a className='social fb'>
                                              <i
                                                className='fa fa-facebook'
                                                aria-hidden='true'
                                                />
                                            </a>
                                            : null}
                                        {channel.indexOf('twitter') > -1
                                            ? <a className='social tw'>
                                              <i
                                                className='fa fa-twitter'
                                                aria-hidden='true'
                                                />
                                            </a>
                                            : null}
                                        {channel.indexOf('linkedin') > -1
                                            ? <a className='social ln li'>
                                              <i
                                                className='fa fa-linkedin'
                                                aria-hidden='true'
                                                />
                                            </a>
                                            : null}
                                        {channel.indexOf('internal') > -1
                                            ? <a className='social iw internal-icon-analytics '>
                                              <i class='material-icons'>
                                                  vpn_lock
                                                </i>
                                            </a>
                                            : null}
                                      </span>
                                    </strong>
                                     : columnState ==
                                                'post_description' ||
                                                columnState == 'description' ||
                                                columnState == 'title'
                                                ? tdDataDesc
                                                : tdData}
                                </me.state.reactable.Td>
                              )
                            })}
                          </this.state.reactable.Tr>
                        )
                      )
                  })
                  : ''}
            </this.state.reactable.Table>:''}
          </div>}
          <div className='pagination-wrapper'>
          {this.state.Pagination?
            <this.state.Pagination.default
              hideFirstLastPages
              prevPageText='Prev'
              nextPageText='Next'
              pageRangeDisplayed={7}
              activePage={pagination}
              itemsCountPerPage={10}
              totalItemsCount={AnalyticsPageDetail.total}
              onChange={::this.handlePageChange}
            />:''}
            </div>
      </div>
      )
  }
  analyticFeedTable (analyticsData, tableColumns) {
    
    return (
      <div className = "tableExpandViewAnalytics">
      {/* loader */}
      {
        analyticsData.analyticsPostTableLoader && Object.keys(analyticsData.feedAnalytics).length == 0 ?
          this.renderLoader()
        :  
          this.renderFeedTable(analyticsData, tableColumns)
      }
       
      </div>
    )

    /* table-anytlics-body end */
  }
  // analyticCampaignTable (analyticsData, tableColumns) {
  //   var channel = []
  //   var AnalyticsTabledata = []
  //   var AnalyticsPageDetail = {}
  //   var totalPage = 0;
  //   var routename = this.props.route

  //     if (Object.keys(this.props.analyticsData.campaignAnalytics).length>0) {
  //       if(analyticsData.campaignAnalytics.data.length>0){
  //       AnalyticsTabledata = analyticsData.campaignAnalytics.data
  //       AnalyticsPageDetail = analyticsData.campaignAnalytics.meta.pagination
  //       }
  //     }
  //   var me = this
  //   if (AnalyticsPageDetail) {
  //     totalPage = Math.ceil(AnalyticsPageDetail.total / 10)
  //   }
  //   return (
  //     <div className='widget'>
  //       {countColums == 0
  //         ? <div className='no-data-block'> Select a Columns</div>
  //         : <div className='table-anytlics-body'>
  //         {this.state.reactable?
  //           <this.state.reactable.Table
  //             className='table responsive-table'
  //             itemsPerPage={10}
  //             pageButtonLimit={5}
  //             sortable
  //             >
  //             <this.state.reactable.Thead>
  //               {tableColumns.map(function (column, columnIndex) {
  //                 let columnState = column[0]
  //                 let columnLabel = column[1]
  //                 let centerClass = column[2]
  //                 return (
  //                   <this.state.reactable.Th
  //                     key={columnIndex}
  //                     column={columnState}
  //                     className={`${columnState} ${me.state[columnState]} ${centerClass}`}
  //                     >
  //                     <strong className='name-header'>{columnLabel}</strong>
  //                     <i class='fa fa-sort' />
  //                   </this.state.reactable.Th>
  //                 )
  //               })}
  //             </this.state.reactable.Thead>
  //             {AnalyticsTabledata
  //                 ? AnalyticsTabledata.map((feeddata, i) => {
  //                   channel = [];

  //                   return (

  //                       (
  //                         <this.state.reactable.Tr
  //                          className='table-body-text' key={i} onClick={this.tablePopupanylitcs.bind(this,feeddata)}>

  //                           {tableColumns.map(function (column, columnIndex) {
  //                             let columnState = column[0]
  //                             let columnLabel = column[1]
  //                             let centerClass = column[2]
  //                             let tdData;

  //                             if (columnState == 'serialno') {
  //                               var pageNoData =  pagination - 1
  //                               pageNoData  = pageNoData  * 10
  //                               pageNoData  = pageNoData  + i + 1;
  //                               tdData  = pageNoData ;
  //                             }

  //                             else if (columnState == 'SocialChannel') {
  //                               tdData = feeddata.SocialChannel.data
  //                             }

  //                             else {
  //                               tdData = feeddata[columnState]
  //                             }
  //                             columnState == 'SocialChannel'?
  //                             tdData.length>0?
  //                             tdData.map(td => {
  //                               channel.push(
  //                                   td.media
  //                                 )})
  //                               :''
  //                               : ''
  //                             var date = ''
  //                             var todate = ''
  //                             // if column state is to_date then its data will store in variable
  //                             columnState == 'start_date'
  //                               ? (todate = me.state.moment
  //                                   ? me.state
  //                                       .moment.unix(tdData)
  //                                       .format('DD-MM-YYYY')

  //                                   : '')
  //                               : ''
  //                             columnState == 'end_date'
  //                               ? (date = tdData
  //                                   ? me.state.moment
  //                                       ? me.state
  //                                           .moment.unix(tdData)
  //                                           .format('DD-MM-YYYY')
  //                                       : ''
  //                                   : '')
  //                               : ''
  //                             return (
  //                               <this.state.reactable.Td
  //                                 key={columnIndex}
  //                                 column={columnState}
  //                                 className={`${columnState} ${me.state[columnState]} ${centerClass}`}
  //                                 data-rwd-label={columnLabel}
  //                               >
  //                                 {columnState == 'SocialChannel'
  //                                   ? <strong className='name-header'>
  //                                     <span className='socialIcons'>
  //                                       {channel.indexOf('Facebook') > -1
  //                                           ? <a className='social fb'>
  //                                             <i
  //                                               className='fa fa-facebook'
  //                                               aria-hidden='true'
  //                                               />
  //                                           </a>
  //                                           : null}
  //                                       {channel.indexOf('Twitter') > -1
  //                                           ? <a className='social tw'>
  //                                             <i
  //                                               className='fa fa-twitter'
  //                                               aria-hidden='true'
  //                                               />
  //                                           </a>
  //                                           : null}
  //                                       {channel.indexOf('Linkedin') > -1
  //                                           ? <a className='social ln li'>
  //                                             <i
  //                                               className='fa fa-linkedin'
  //                                               aria-hidden='true'
  //                                               />
  //                                           </a>
  //                                           : null}
  //                                       {channel.indexOf('Internal') > -1
  //                                           ? <a className='social iw'>
  //                                             <i class='material-icons'>
  //                                                 vpn_lock
  //                                               </i>
  //                                           </a>
  //                                           : null}
  //                                     </span>
  //                                   </strong>
  //                                   : columnState == 'start_date'
  //                                       ? // if columnstate is todate then to display todate data
  //                                         typeof todate !== 'undefined'
  //                                           ? todate
  //                                           : ''
  //                                       : columnState == 'end_date'
  //                                           ? typeof date !== 'undefined'
  //                                               ? date
  //                                               : ''
  //                                           : tdData}
  //                               </this.state.reactable.Td>
  //                             )
  //                           })}
  //                         </this.state.reactable.Tr>
  //                       )
  //                     )
  //                 })
  //                 : ''}
  //           </this.state.reactable.Table>:''}
  //         </div>}
  //         <div className='pagination-wrapper'>
  //           <Pagination
  //             hideFirstLastPages
  //             prevPageText='Prev'
  //             nextPageText='Next'
  //             pageRangeDisplayed={7}
  //             activePage={pagination}
  //             itemsCountPerPage={10}
  //             totalItemsCount={AnalyticsPageDetail.total}
  //             onChange={::this.handlePageChange}
  //           />
  //           </div>
  //     </div>
  //   )

  //   /* table-anytlics-body end */
  // }
  // analyticAssetTable (analyticsData, tableColumns) {
  //   var channel = []
  //   var AnalyticsTabledata = []
  //   var AnalyticsPageDetail = {}
  //   var totalPage = 0;
  //   var routename = this.props.route
  //   if (Object.keys(this.props.analyticsData.assetAnalytics).length>0) {
  //     if(analyticsData.assetAnalytics.data.length>0){
  //     AnalyticsTabledata = analyticsData.assetAnalytics.data
  //     AnalyticsPageDetail = analyticsData.assetAnalytics.meta.pagination
  //     }
  //   }
  //   var me = this
  //   if (AnalyticsPageDetail) {
  //     totalPage = Math.ceil(AnalyticsPageDetail.total / 10)
  //   }
  //   return (
  //     <div className='widget'>
  //       {countColums == 0
  //         ? <div className='no-data-block'> Select a Columns</div>
  //         : <div className='table-anytlics-body'>
  //           {this.state.reactable?
  //           <this.state.reactable.Table
  //             className='table responsive-table'
  //             itemsPerPage={10}
  //             pageButtonLimit={5}
  //             sortable
  //             >
  //             <this.state.reactable.Thead>
  //               {tableColumns.map(function (column, columnIndex) {
  //                 let columnState = column[0]
  //                 let columnLabel = column[1]
  //                 let centerClass = column[2]
  //                 return (
  //                   <me.state.reactable.Th
  //                     key={columnIndex}
  //                     column={columnState}
  //                     className={`${columnState} ${me.state[columnState]} ${centerClass}`}
  //                     >
  //                     <strong className='name-header'>{columnLabel}</strong>
  //                     <i class='fa fa-sort' />
  //                   </me.state.reactable.Th>
  //                 )
  //               })}
  //             </this.state.reactable.Thead>
  //             {AnalyticsTabledata.length>0
  //                 ? AnalyticsTabledata.map((assetData, i) => {
  //                   channel = [];

  //                   return (
  //                       (
  //                         <this.state.reactable.Tr
  //                         onClick={this.tablePopupanylitcs.bind(this,assetData)}
  //                          className='table-body-text' key={i}>
  //                           {tableColumns.map(function (column, columnIndex) {
  //                             let columnState = column[0]
  //                             let columnLabel = column[1]
  //                             let centerClass = column[2]
  //                             let tdData;
  //                             if (columnState == 'serialno') {
  //                               var pageNoData =  pagination - 1
  //                               pageNoData  = pageNoData  * 10
  //                               pageNoData  = pageNoData  + i + 1;
  //                               tdData  = pageNoData ;
  //                             }
  //                             else if (columnState == 'category') {
  //                               tdData = assetData.category.data.name
  //                             }
  //                             else if (columnState == 'date') {
  //                               tdData = assetData.created_date
  //                             }else if(columnState=='media_type'){
  //                               tdData=assetData[columnState].split(';')[0]
  //                             }else {
  //                               tdData = assetData[columnState]
  //                             }
  //                             var tdDataDesc = ''

  //                             var date = ''

  //                             // if column state is to_date then its data will store in variable
  //                              columnState == 'date'
  //                                   ? (date = me.state.moment
  //                                       ? me.state
  //                                           .moment(tdData.date)
  //                                           .format('DD-MM-YYYY')
  //                                       : '')
  //                                   : ''
  //                             return (
  //                               <me.state.reactable.Td
  //                                 key={columnIndex}
  //                                 column={columnState}
  //                                 className={`${columnState} ${me.state[columnState]} ${centerClass}`}
  //                                 data-rwd-label={columnLabel}
  //                               >
  //                                 {  columnState == 'date'
  //                                           ? typeof date !== 'undefined'
  //                                               ? date
  //                                               : ''
  //                                           : tdData}
  //                               </me.state.reactable.Td>
  //                             )
  //                           })}
  //                         </this.state.reactable.Tr>
  //                       )
  //                     )
  //                 })
  //                 : ''}
  //           </this.state.reactable.Table>:''}
  //         </div>}
  //         <div className='pagination-wrapper'>
  //         <Pagination
  //             hideFirstLastPages
  //             prevPageText='Prev'
  //             nextPageText='Next'
  //             pageRangeDisplayed={7}
  //             activePage={pagination}
  //             itemsCountPerPage={10}
  //             totalItemsCount={AnalyticsPageDetail.total}
  //             onChange={::this.handlePageChange}
  //           />
  //           </div>
  //     </div>
  //   )

  //   /* table-anytlics-body end */
  // }

  render () {
    if (this.props.location == '/analytics/feed') {
      tableColumns = [
        ['serialno', 'ID', 'a-center'],
        ['user', 'User Name', ''],
        ['post_description', 'Description', ''],
        ['created_at', 'Created At', ''],
        ['department', 'Department', ''],
        ['channel', 'Channel', 'a-center'],
        ['share', 'Share', 'a-center'],
        ['like', 'Like', 'a-center'],
        ['comment', 'Comment', 'a-center']
      ]
    }
    let searchScrollbar = {
      maxHeight: '155px'
    }
    return (
      <div className='tabel-view-anylitcs'>
      { this.props.analyticsData.analyticsPostTableLoader == false ?
      <FilterTableColumns>
      {this.createDropdown(tableColumns)}
      </FilterTableColumns>
      :''}

        { Object.keys(this.props.analyticsData.feedAnalytics).length>0 ||
           Object.keys(this.props.analyticsData.assetAnalytics).length>0 ||
           Object.keys(this.props.analyticsData.campaignAnalytics).length>0 || this.props.route=='feed'
           ? <div>
            {
              this.props.route=='feed'?
              this.analyticFeedTable(this.props.analyticsData, tableColumns)
              :''
            }
            
          </div>
          : <div className='no-data-block'>
              No {this.props.dataType} Found.
            </div>}

      </div>
    )
  }
}

module.exports = AnalyticsTable

