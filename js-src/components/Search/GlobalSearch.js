import {bindActionCreators} from 'redux';
import {browserHistory} from 'react-router';
import {connect} from 'react-redux'
import Collapse, {Panel} from 'rc-collapse'
import { renderField } from '../Helpers/ReduxForm/RenderField'

import * as departmentActions from "../../actions/departmentActions"
import * as searchActions from '../../actions/searchActions'
import * as userActions from '../../actions/userActions'
import * as feedActions from '../../actions/feed/feedActions';
import * as assetActions from '../../actions/assets/assetsActions';
import * as campaignsActions from '../../actions/campaigns/campaignsActions';

import Assets from '../Assets/Index'
import Campaigns from '../Campaigns/Index'
import DepartmentSettings from '../Settings/Department/DepartmentSettings';
import Feed from '../Feed/Feed';
import { reduxForm } from 'redux-form';
import Header from '../Header/Header';
import moment from '../Chunks/ChunkMoment'
import PreLoader from '../PreLoader'
import SearchNav  from './SearchNav'
import 'rc-collapse/assets/index.css'
import UsersSettings from '../Settings/User/UsersSettings';

let posts;
let assetsData;
let assetsMeta;
let userData;
let campaignData;
let departmentData;
let post;
class GlobalSearch extends React.Component{
    constructor(props){
        super(props);
        this.state={
            currFeedPosts: 'live',
            moment:null,
            updateCampaignPopup: false,
            detailCampaignPopup: false,
            updateUserPopup: false,
            updateDeptPopup:false,
            currentCampaign: [],
            loading :false,
            searchData:{},
            flag_auto_suggest:false,
        }
    }
    componentDidMount()
    {
        if(this.props.location.state !== null && this.props.location.state.seeAllClick !== undefined && this.props.location.state.seeAllClick==true){
        switch(this.props.location.pathname){
            case  "/search/default/live"  :
                this.props.searchActions.searchstart();
                this.props.feedActions.fetchDefaultLive(1,10,false,false,null,this.props.searchInput);
                this.props.searchActions.resetseeAllClick();
                this.props.searchActions.resetSuggestionFlag();
                break;
            case "/search/assets":
                this.props.assetActions.fetchFiles('','',20,1,false,null,this.props.searchInput);
                window.scrollTo(0,0);
                this.props.searchActions.resetseeAllClick();
                this.props.searchActions.resetSuggestionFlag();
                break;
            case "/search/campaign":
                this.props.campaignsActions.fetchCampaigns("all");
                this.props.searchActions.resetSuggestionFlag();
                this.props.searchActions.resetseeAllClick();
                break;
            case "/search/user": 
                this.props.userActions.fetchUsers(this.props.searchInput);
                this.props.searchActions.resetseeAllClick();
                this.props.searchActions.resetSuggestionFlag();
                break;
            case  "/search/department":
                this.props.departmentActions.fetchDepartments(this.props.searchInput);
                this.props.searchActions.resetseeAllClick();
                this.props.searchActions.resetSuggestionFlag();
                break;
        }
    }
    }
    componentWillMount(){
        var me = this
        moment().then(moment => {
        me.setState({
            Moment: moment
        })
    })
}
    componentWillReceiveProps(newProps){
    if(this.props.location.pathname!==newProps.location.pathname && newProps.location.state==null || (newProps.location.state!==null && newProps.location.state.seeAllClick==true && newProps.searchData.flag_see_all==true)){
            switch(newProps.location.pathname){
                case  "/search/default/live"  :
                    this.props.searchActions.searchstart();
                    this.props.feedActions.fetchDefaultLive(1,10,false,false,null,newProps.searchInput);
                    this.props.searchActions.resetSuggestionFlag();
                    this.props.searchActions.resetseeAllClick();

                    break;
                case "/search/campaign":
                    this.props.campaignsActions.fetchCampaigns("all");
                    this.props.searchActions.resetSuggestionFlag();
                    this.props.searchActions.resetseeAllClick();
                    break;
                case "/search/assets":
                    this.props.assetActions.fetchFiles('','',20,1,false,null,newProps.searchInput);
                    window.scrollTo(0,0);
                    this.props.searchActions.resetSuggestionFlag();
                    this.props.searchActions.resetseeAllClick();
                    break;
                case "/search/user": 
                    this.props.userActions.fetchUsers(newProps.searchInput);
                    this.props.searchActions.resetSuggestionFlag();
                    this.props.searchActions.resetseeAllClick();
                    break;
                case  "/search/department":
                    this.props.departmentActions.fetchDepartments(newProps.searchInput);
                    this.props.searchActions.resetSuggestionFlag();
                    this.props.searchActions.resetseeAllClick();
                    break;
            }
        }
    }

    fetchPostDetails(posts){
        var NoData = false
        if( posts.length == 0 )
        {
            NoData = true
        }
        if(this.props.searchData.suggestion!==null && this.props.searchData.suggestion!==undefined  && this.props.searchData.flag_suggested==true){
               if(posts.length!==0){
            return(   
                <Feed
                    location={this.props.location}
                    postsData={posts}
                    NoData={NoData}
                    searchedData={this.props.searchInput}
                    suggestion_selected={this.props.searchData.suggestion}
                />
            );
            }
        }
        else{
            return(   
                <Feed
                    location={this.props.location}
                    postsData={posts}
                    NoData={NoData}
                    searchedData={this.props.searchInput}
                    // searchData ={this.props.searchData}
                />
            );
            }
    }
      
    
    renderAssets(assetsData){
        if(this.props.searchData.suggestion!==null && this.props.searchData.suggestion!==undefined && this.props.searchData.flag_suggested==true){
            if(assetsData.length!==0){
            return (
                <Assets 
                location={this.props.location}
                assetsMeta={assetsMeta}
                assetsData={assetsData}
                searchedInput={this.props.searchInput}
                suggestion_selected={this.props.searchData.suggestion}
                />
            );
            }
        }
        else{
            return (
                <Assets 
                location={this.props.location}
                assetsMeta={assetsMeta}
                assetsData={assetsData}
                searchedInput={this.props.searchInput}
                />
            );
        }
    }

    fetchCampaignDetails(campaignData){
        if(this.props.searchData.suggestion!==null && this.props.searchData.suggestion!==undefined &&  this.props.searchData.flag_suggested==true)
            return (
                <Campaigns
                location={this.props.location}
                campaignData={campaignData}
                searchedInput={this.props.searchInput}
                suggestion_selected={this.props.searchData.suggestion}
                />
            )
        else
            return (
                <Campaigns
                location={this.props.location}
                campaignData={campaignData}
                searchedInput={this.props.searchInput}
                />
            )
    }

    fetchUser(userData){
        if(this.props.searchData.suggestion!==null && this.props.searchData.suggestion!==undefined && this.props.searchData.flag_suggested==true){
            if(userData.length!==0){
            return (
                <UsersSettings   
                location={this.props.location} 
                userData={userData}
                searchInput={this.props.searchInput}
                suggestion_selected={this.props.searchData.suggestion}
                />
            );
            }
        }
        else{
            return (
                <UsersSettings   
                location={this.props.location} 
                userData={userData}
                searchInput={this.props.searchInput}
                />
            );
        }
    }

    fetchDepartment(departmentData){
        if(this.props.searchData.suggestion!==null && this.props.searchData.suggestion!==undefined && this.props.searchData.flag_suggested==true){
            if(departmentData.length!==0){
            return (
                <DepartmentSettings
                    location={this.props.location}
                    departmentData={departmentData}
                    searchInput={this.props.searchInput}
                    suggestion_selected={this.props.searchData.suggestion}
                />
            );
            }
        }
        else{
            return (
                <DepartmentSettings
                location={this.props.location}
                departmentData={departmentData}
                searchInput={this.props.searchInput}
                />
            ); 
            }  
    }   

    render(){
        var searchDetails= this.props.searchData.searchData
        var x;
        var me=this
        var suggestion_selected =  ( me.props.searchData.suggestion !== null ) ? true : false
        return(
        <div>
            <Header
            add_new_subpopup="FEED"
            title="SEARCH"    
            nav={SearchNav}
            popup_text="Click here to add new post."
            add_new_button={true}
            searchInput={this.props.searchInput}
            location={this.props.location}
            suggestion_selected={true}
            />
                {
                    (Object.keys(searchDetails).length > 0)?
                Object.keys(searchDetails).map(function(key, index){
                        if(key=="asset")
                        {
                            if(me.props.searchData.suggestion!==null && me.props.searchData.suggestion!==undefined && me.props.searchData.flag_suggested==true  && me.props.location.state!==null)
                            {
                                if(me.props.searchData.suggestion.key=="asset"){
                                var filter = me.props.searchData.searchData.asset.data.filter(function(asset){
                                    return asset.asset_id == me.props.searchData.suggestion.id
                                })
                                assetsData = filter
                                }
                                else{
                                    assetsData=[]
                                }
                            }
                            else 
                            {
                                if( me.props.searchData.flag_suggested==false || (me.props.location.state!==null && me.props.location.state.seeAllClick==true)){
                                assetsData= searchDetails[key].data
                                assetsMeta=searchDetails[key].meta
                                }
                            }
                        }
                        else if(key=="post"){
                            if(me.props.searchData.suggestion!==null && me.props.searchData.suggestion!==undefined &&  me.props.searchData.flag_suggested==true && me.props.location.state!==null)
                            {
                                if( me.props.searchData.suggestion.key=="post" ){
                                var filter = me.props.searchData.searchData.post.data.filter(function(temp){
                                    return temp.post_id == me.props.searchData.suggestion.id
                                })
                                post = filter 
                                }
                                else{
                                    post=[]
                                }
                            }
                            else
                            {
                                if( me.props.searchData.flag_suggested==false){
                                post = searchDetails[key].data
                                }
                            }
                        }
                        else if(key=="campaign"){
                            if(me.props.searchData.suggestion!==null && me.props.searchData.suggestion!==undefined && me.props.searchData.flag_suggested==true  && me.props.location.state!==null)
                            {
                                if(me.props.searchData.suggestion.key=="campaign"){
                                    var filter = me.props.searchData.searchData.campaign.data.filter(function(temp){
                                        return temp.identity == me.props.searchData.suggestion.id
                                    })
                                    campaignData = filter
                             }
                            }
                             else{
                                 if( me.props.searchData.flag_suggested==false){ 
                                    campaignData = searchDetails[key].data
                                 }
                             }
                            }
                        else if(key=="user"){
                            if(me.props.searchData.suggestion!==null && me.props.searchData.suggestion!==undefined  && me.props.searchData.flag_suggested==true  && me.props.location.state!==null)
                            {
                               if(me.props.searchData.suggestion.key=="user"){
                                var filter = me.props.searchData.searchData.user.data.filter(function(temp){
                                    return temp.identity == me.props.searchData.suggestion.id
                                })
                                userData = filter
                            }
                            }
                            else{
                                if( me.props.searchData.flag_suggested==false){ 
                                userData= searchDetails[key].data
                                }
                            }
                        }
                        else if(key=="department"){
                            if(me.props.searchData.suggestion!==null && me.props.searchData.suggestion!==undefined   && me.props.searchData.flag_suggested==true && me.props.location.state!==null)
                            {
                                if(me.props.searchData.suggestion.key=="department"){
                                var filter = me.props.searchData.searchData.department.data.filter(function(temp){
                                    if(temp.identity == me.props.searchData.suggestion.id)
                                    return temp.identity == me.props.searchData.suggestion.id
                                })
                                departmentData = filter
                            }
                            }
                            else{
                                if( me.props.searchData.flag_suggested==false){
                                 departmentData = searchDetails[key].data
                                }
                            }
                        }
                        

                }): ''
            }
            {(this.props.searchData.loading==true &&this.props.searchData.auto_suggest!==true)?
            <div>
            {
                this.props.location.pathname == '/search/default/live' ? 
                    <section className="feed">
                    <div id="content">
                    <div className="page live-feeds ">
                    <div id="feed">
                    <div className="clearfix trending-parent">
                    <div className="render-post-feed">
                    <div className='internal'>
                    <div className='internal-live esc-sm'>
                    <div className='content'>
                        <PreLoader feedLoader/>
                    </div>
                    </div>
                    </div>
                    </div>
                    </div>
                    </div>
                    </div>
                    </div>
                    </section>
                    :this.props.location.pathname == '/search/assets'? 
                            <PreLoader location="assets-loader" userAssets={true}/>
                        : this.props.location.pathname == '/search/campaign'?
                                <div id='content'>
                                    <PreLoader location="campaign-loader" /> 
                                </div>
                            : this.props.location.pathname == '/search/user'?
                                    <div id="content">
                                    <div className="page campaign-container">
                                    <div className="full-container">
                                    <div className="membersListing">
                                    <PreLoader location="userList-Loader"/>
                                    </div>
                                    </div>
                                    </div>
                                    </div>
                                : this.props.location.pathname == '/search/department'?
                                        <PreLoader location="department-loader"/>
                                    :<PreLoader className="Page"/>
            }
            </div>
            :''}
            {(this.props.searchData.auto_suggest!==true || this.props.searchData.auto_suggest==undefined)?
            (Object.keys(searchDetails).length > 0)?
                this.props.location.pathname == '/search/assets' && assetsData!==undefined? 
                this.renderAssets(assetsData)
                    : this.props.location.pathname == '/search/default/live' && post!==undefined? 
                            this.fetchPostDetails(post) 
                                : this.props.location.pathname == '/search/campaign' && campaignData!==undefined ?
                                this.fetchCampaignDetails(campaignData) 
                                    :  this.props.location.pathname == '/search/user' && userData!==undefined?
                                        this.fetchUser(userData)
                                        :  this.props.location.pathname == '/search/department' && departmentData!==undefined?
                                            this.fetchDepartment(departmentData)
                :   
                <div className='main-container'>
                    <div id='content'>
                    <div className='page'>
                    <div className='full-container'>   
                    <div className="asset-container">
                    <div className="esc-2"> 
                    <div>
                        <div className='no-data-block'>No Data found.</div>
                    </div>  
                    </div>
                    </div>
                    </div>
                    </div>    
                    </div>
                    </div>
                    :'':''}
             </div>

        );
    }
}


function mapStateToProps(state) {
    return {
        searchData: state.searchData,
        searchInput:state.searchData.searchInput,
        suggestion:state.suggestion
    }
}
function mapDispatchToProps(dispatch) {
    return {
        // dispatch,
      searchActions : bindActionCreators (searchActions,dispatch),
      departmentActions: bindActionCreators(departmentActions,dispatch),
      userActions : bindActionCreators (userActions,dispatch),
      feedActions : bindActionCreators (feedActions,dispatch),
      assetActions : bindActionCreators (assetActions,dispatch),
      campaignsActions : bindActionCreators(campaignsActions,dispatch)
    }
}

//module.exports = connect(mapStateToProps, mapDispatchToProps)(GlobalSearch)
/*let connection = connect(mapStateToProps, mapDispatchToProps);
export default GlobalSearch;*/
let connection = connect(mapStateToProps, mapDispatchToProps);
var reduxFormConnection = connection(GlobalSearch);
export default reduxForm({
  form: 'GlobalSearch' // a unique identifier for this form
})(reduxFormConnection)
