import * as searchActions from '../../actions/searchActions' 
import Collapse, { Panel } from 'rc-collapse'; // collapse
// collapse css
import { IndexLink, Link } from 'react-router';
import * as utils from '../../utils/utils'; 

class SearchNav extends React.Component{
  render() {
    let external_feed_class = '';
    let internal_feed_class = '';
    let assets_feed_class = '';
    let campaign_feed_class = '';
    let user_feed_class = '';
    let department_feed_class = '';
    if(this.props.currFeed=='external')
    {
      external_feed_class = 'active';
    }
    else if(this.props.currFeed=='internal')
    {
      internal_feed_class = 'active';
    }
    else if(this.props.location.pathname == '/search/assets')
    {
      assets_feed_class = 'active';
    }
    else if(this.props.location.pathname =='/search/campaign')
    {
      campaign_feed_class = 'active';
    }
    else if(this.props.location.pathname=='/search/user')
    {
      user_feed_class = 'active';
    }
    else if(this.props.location.pathname=='/search/department')
    {
      department_feed_class = 'active';
    }


    var roleName = localStorage.getItem('roles') !== null? JSON.parse(localStorage.getItem('roles'))[0]['role_name']:''
    return (
      <nav className='nav-side'>
        
        <Collapse
          accordion={false}
          onChange={this.onChange}
          defaultKey={'1'}
          defaultActiveKey={['1', '2', '3', '4','5','6']}
        >
          <Panel className={external_feed_class} header='FEED' key={'1'}>
           
            {this.renderFeedTypeExternalInSidebar()}
          </Panel>
          
          <Panel className={assets_feed_class} header='ASSETS' key={'3'}>
              {this.renderAssets()}
          </Panel>
          <Panel className={campaign_feed_class} header='CAMPAIGN' key={'4'}>
              {this.renderCampaign()}
          </Panel>
          {
            roleName!=='employee' ? 
            <Panel className={user_feed_class} header='USER' key={'5'}>
              {this.renderUser()}
            </Panel>:''
          }
         {
           roleName!=='employee' ? 
           <Panel className={department_feed_class} header='DEPARTMENT' key={'6'}>
              {this.renderDeaprtment()}
          </Panel> :''
         }
        </Collapse>
      </nav>
    )
  }
  renderFeedTypeExternalInSidebar() {
    
    let selectedFeed = this.props.location.pathname
    const hashTag = this.props.hash;
    var actionid = utils
   .removeTrailingSlash(this.props.pathname)
      .split('/')
    let newactionid = actionid[4]
    //this.props.notificationActions.fetchSelectedPost(newactionid)
    var postDetail = this.props.postSelectedData
    


    return (
      <ul className='parent  clearfix'>
        <li>
          <Link
            to={('/search/default/live')}
            className={
              selectedFeed == '/search/default/live'
                ? 'active'
                : ''
            }
          >
            <span className='text'>Live</span>
          </Link>
        </li>
      </ul>
    )
  }
  renderFeedTypeInternalInSidebar() {
    const selectedFeed = this.props.location.pathname
    const hashTag = this.props.hash;

    return (
      <ul className='parent  clearfix'>
        <li>
          <Link
            to={('/search/internal/live')}
            className={
              selectedFeed == '/search/internal/live'
                ? 'active'
                : ''
            }
          >
            <span className='text'>Live</span>
          </Link>
        </li>
      </ul>
    )
  }



  renderAssets() {
    const selectedFeed = this.props.location.pathname
    return (
      <ul className='parent  clearfix'>
        <li>
          <Link
            to={`/search/assets`}
            className={
              selectedFeed == '/search/assets'
                ? 'active'
                : ''
            }
          >
            <span className='text'>Assets</span>
          </Link>
        </li>

      </ul>
    )
  }

  renderCampaign(){
    const selectedFeed = this.props.location.pathname
    return (
      <ul className='parent  clearfix'>
        <li>
          <Link
            to={`/search/campaign`}
            className={
              selectedFeed == '/search/campaign'
                ? 'active'
                : ''
            }
          >
            <span className='text'>Campaign</span>
          </Link>
        </li>

      </ul>
    )
  }

  renderUser(){
    const selectedFeed = this.props.location.pathname
    return (
      <ul className='parent  clearfix'>
        <li>
          <Link
            to={`/search/user`}
            className={
              selectedFeed == '/search/user'
                ? 'active'
                : ''
            }
          >
            <span className='text'>User</span>
          </Link>
        </li>

      </ul>
    )
  }

  renderDeaprtment(){
    const selectedFeed = this.props.location.pathname
    return (
      <ul className='parent  clearfix'>
        <li>
          <Link
            to={`/search/department`}
            className={
              selectedFeed == '/search/department'
                ? 'active'
                : ''
            }
          >
            <span className='text'>Department</span>
          </Link>
        </li>

      </ul>
    )
  }

}


module.exports = SearchNav;