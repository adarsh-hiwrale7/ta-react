import {browserHistory} from 'react-router';

export default class Four0Four extends React.Component {

  constructor(props) {
    super(props);
    
  }

  goBack(){
    browserHistory.goBack();
  }
  
  componentDidMount() {

  }

  render() {

    return (
      <section className="four0four basic-layout">
          <div>
                <div className = "page-404">
                    <div className="girls-banner-image"> <img src = "/img/girls-banner-image.png"  alt = "img not found"/> </div>
                    <div className = "content-page-404"> 
                        <div className = "Error-404-msg">Not, what you’re looking for?</div>
                        <div className = "page-not-found-image"> <img src = "/img/404page.png" alt = "404 page"/></div>
                        <div className = "go-to-feed-button"> <a href = "/feed/default/live" > Go to Feed </a></div>
                     </div>
                </div>
          </div>
      </section>
    
    );
  }
}