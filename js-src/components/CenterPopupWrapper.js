//this componet used for the center popup 

class CenterPopupWrapper extends React.Component {
    render() {
        return (
              <div className = {`popup-center-wrapper ${this.props.className}`}> 
                 <div className = "inner-popup-center-wrapper"> 
                    <div className = "main-popup-wrapper">
                       {this.props.children}
                    </div>
                 </div>
              </div>
                    
            
        )
    }
}
module.exports = CenterPopupWrapper