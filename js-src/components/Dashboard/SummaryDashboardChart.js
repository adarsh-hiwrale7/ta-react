import { connect } from 'react-redux'
import * as utils from '../../utils/utils';
class SummaryDashboardChart extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            opacity: 1,
            hideChart:false
        }
    }
    UpdatedLegend = (props) => {
        const { payload, customizeLegendValue } = props;
        var LegendKeys = Object.keys(customizeLegendValue[0])
        return (
            <ul>
                {LegendKeys.length > 0 ? LegendKeys.map((entry, index) => (
                    <li className="clearfix" key={index} onClick={this.hideSelectedLegend.bind(this, payload[index].dataKey)}>
                        <span className="rounder-dot" style={{ background: payload[index].color }}></span>
                        <span className="title-active-user"> {payload[index].dataKey}  </span> <span className="text-no">{customizeLegendValue[0][payload[index].dataKey]}</span>  </li>
                )) : ''}
            </ul>
        );
    }
    customizeTooltip = (date) => {
        return (
            this.props.moment ? this.props.moment.unix(date).format('DD-MM-YYYY') : ''
        )
    }
    convertTimeStampToDate = (date) => {
        var newFormat = ''
        if (this.props.period == 'month' || this.props.period == 'week') {
            newFormat = 'MMM, D'
        } else if (this.props.period == 'year') {
            newFormat = 'MMM, YY'
        }
        return (
            this.props.moment ? this.props.moment.unix(date).format(newFormat) : ''
        )
    }
    hideSelectedLegend(dataKey) {
        var opacityCount=1
        var hideSelectedChart=false
        if(this.state.opacity[dataKey]==0){
            opacityCount=1
            hideSelectedChart=false
        }else{
            opacityCount=0
            hideSelectedChart=true
        }
        this.setState({
            opacity: { ...this.state.opacity, [dataKey]: opacityCount },
            hideChart:{...this.state.hideChart,[dataKey]:hideSelectedChart}
        });
        
        
    }
    UpdatedLegend = (props) => {
        const { payload } = props;
        return (
          <ul>
             {payload.length>0?payload.map((entry, index) => (
                 
                  <li className="clearfix" key={index} onClick={this.hideSelectedLegend.bind(this,entry.dataKey)}> 
                  <span className = "rounder-dot" style={{background:entry.color}}></span>
                  <span className ={`title-active-user ${this.state.hideChart[entry.dataKey]!== undefined && this.state.hideChart[entry.dataKey]==true ?'strikeLegend' :''} `}> {entry.dataKey}  </span> </li>
              )):''}
          </ul>
        );
      }
      customizeTooltipForAdoption = props => {
        const { payload } = props
        var chartDate=0
        if (payload.length > 0) {
            chartDate=payload[0].payload.date
        }
        return(
            <div> 
                <div className='chartDate'>
                    <div className='label'>
                       {this.customizeTooltip(chartDate)}
                    </div> 
                </div>
                <div className="internal-external-post-wrapper">
                    {payload.length > 0
                    ? payload.map((entry, i) => {
                        return (
                            this.state.hideChart[entry.dataKey] !== undefined && this.state.hideChart[entry.dataKey] == true ? '' :
                                <div className={`custom-tooltip`} key={i}  >
                                        <div className='label' style={{color:entry.color}}>{`${entry.dataKey} :${entry.value}`}</div>
                                </div>
                        )
                    })
            : ''}
            </div>
            </div>
        )
    }

    render(){
        return(
            <div>  
                {this.renderContent(this.props.dashboardProps, this.props.period, this.props.LineChart, this.props.XAxis, this.props.YAxis, this.props.CartesianGrid, this.props.Legend, this.props.Tooltip, this.props.Line, this.props.moment, this.props.refreshChart)
            }
            </div>
        )   
    }
    renderContent(dashboard, period, LineChart, XAxis, YAxis, CartesianGrid, Legend, Tooltip, Line, moment, refreshChart) {
        var windowDimention = utils.windowSize()
        var GraphDimention = utils.getChartDimension(windowDimention.w,'fromDashboard');
        var summaryData = Object.keys(dashboard.dashboardAnalytics).length > 0 ? dashboard.dashboardAnalytics.chart_data !== undefined && dashboard.dashboardAnalytics.chart_data.length > 0 ? dashboard.dashboardAnalytics.chart_data : '' : ''
        var days = period !== '' ? period : ''
        var intervalDay = 0
        if (days == 'year') {
            intervalDay = 60
        } else if (days == 'month') {
            intervalDay = 7
        } else {
            intervalDay = 0
        }
        return (
            summaryData !== '' && GraphDimention !==undefined ?
                <LineChart
                    width={GraphDimention.width}
                    height={GraphDimention.height}
                    data={summaryData}
                    margin={{ top: 5, right: 30, left: 0, bottom: 0 }}>
                    <CartesianGrid stroke="#f1f2f3" vertical={false} />
                    <XAxis dataKey="date" tickSize={0} minTickGap={12} interval={intervalDay} tickMargin={10} tickFormatter={this.convertTimeStampToDate} />
                    <YAxis tickSize={0} tickMargin={10} />
                    <Tooltip content={this.customizeTooltipForAdoption} />
                    <Legend verticalAlign="top" width={1} iconType='circle' iconSize={10} content={this.UpdatedLegend}/>
                    <Line type="linear" dataKey="All Assets" stroke="#256eff"  hide={this.state.hideChart['All Assets']} dot={false} strokeWidth={2}/>
                    <Line type="linear" dataKey="All Posts" stroke="#ff595e"  hide={this.state.hideChart['All Posts']} dot={false} strokeWidth={2}/>
                    <Line type="linear" dataKey="My Assets" stroke="#e27c3f"  hide={this.state.hideChart['My Assets']} dot={false} strokeWidth={2}/>
                    <Line type="linear" dataKey="My Posts" stroke="#ffbf00"  hide={this.state.hideChart['My Posts']} dot={false} strokeWidth={2}/>
                </LineChart>
                : <div></div>
        )
    }
}

function mapStateToProps(state) {
    return {}
}

function mapDispatchToProps(dispatch) {
    return {}
}

let connection = connect(
    mapStateToProps,
    mapDispatchToProps
)

module.exports = connection(SummaryDashboardChart)
