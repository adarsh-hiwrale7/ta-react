import * as utils from '../../utils/utils';
import { Link } from 'react-router'
import ImageLoader from 'react-imageloader'
import PreLoaderMaterial from '../PreLoaderMaterial'
export default class MyRecentAsset extends React.Component {
constructor(props) {
    super(props)
    this.state = {
      checking:false,
    }
  }
  handleAssetPopup(assetDetail) {
    this.props.handleAssetPopup(assetDetail);
  }
  componentWillReceiveProps(){
    // utils.pauseVideo(".play-btn-overlay video");
  }
  loader(){
      return(
        <div>
            <div class="Approved-div-asset-block Approved-div-asset-block-loader">
                        <div class="approvediv">
                          <div className = "image-block-assets">
                              <div className ="loader-grey-line"> </div>
                           </div>
                          <div className = "assets-descripation">
                          <div className ="loader-grey-line loader-line-height">  </div>
                          <div className ="loader-grey-line loader-line-height">  </div>
                          </div>
            </div>
          </div>
          <div class="Approved-div-asset-block Approved-div-asset-block-loader">
                        <div class="approvediv">
                          <div className = "image-block-assets">
                              <div className ="loader-grey-line"> </div>
                           </div>
                          <div className = "assets-descripation">
                          <div className ="loader-grey-line loader-line-height">  </div>
                          <div className ="loader-grey-line loader-line-height">  </div>
                          </div>
            </div>
          </div>
        </div>  
      )
  }
  render(){
    return(
         <div className='recentasset-wrapper clearfix'>
            {
              this.props.contentLoader == true ? 
                this.loader()
              :  
                this.renderAsset(this.props.myRecentAssets)
            }
         </div>
    )
  }
 
  renderAsset(assetData) {
    const imageWidth = {
      //  width: '30%'
    };

    
    let myRecentAssets = assetData;
    var recentAsset = null;
    var temp = null;
   
    if (Object.keys(myRecentAssets).length !== 0) {
      recentAsset = myRecentAssets.map(function (myRecentAsset, index) {
        var thumbnail_url = myRecentAsset.thumbnail_url;
        let statusClass = "approve-tick";
        let iconName = "ic_add_circle_outline";
        if (myRecentAsset.approved === 2) {
          statusClass = "approve-tick undecide";
          iconName = "panorama_fish_eye";
        } else if (myRecentAsset.approved === 3) {
          statusClass = "approve-tick unapprove";
          iconName = "ic_highlight_off";
        } else if (myRecentAsset.approved === 0) {
          statusClass = "approve-tick pending";
          iconName = "ic_remove_circle_outline";
        }
        let assetImage = {
          backgroundImage: 'url(' + myRecentAsset.thumbnail_url + ')'
        }
        var media_type_index = myRecentAsset.media_type.indexOf('/')
        var media_type = '',
          postThumb = '',
          media_url = myRecentAsset.media_url;
        if (media_type_index !== -1) {
          media_type = myRecentAsset.media_type.substr(0, media_type_index)
        } else {
          media_type = myRecentAsset.media_type
        }
  
        var media_extension = myRecentAsset.media_extension
        var faIcon = utils.getFaIcon(media_extension);
        switch (media_type) {
          case 'image':
            postThumb = (
              <div className='assetimage unapprove' style={assetImage}>
              </div>
            )
            break
          case 'video':
            postThumb = (
              <div
                className='assetimage file-video type-video' style={assetImage}>
                <div className='play-btn-overlay' style={imageWidth}>
                  <i className="material-icons">play_circle_filled</i>
                </div>
              </div>
            )
            break
          case 'audio':
            postThumb = (
              <div className={`thumbWrap file file-doc ${media_extension}`} style={imageWidth}>
                <i className='fa fa-file-audio-o fa-3x' />
              </div>
            )
            break
          case 'application':
            postThumb = (
              <div className={`file file-doc ${media_extension}`} style={imageWidth}>
                <i className={`fa ${faIcon} fa-3x`} />
              </div>
            )
            break
          default:
            postThumb = (
              <div className={`thumbWrap file file-doc ${media_extension}`} style={imageWidth}>
                  <i className={`fa ${faIcon} fa-3x`} />
              </div>
            )
            break
        }

        return (
        <div className ="Approved-div-asset-block" key={index} onClick={this.handleAssetPopup.bind(this, myRecentAsset)}>
        <div className='approvediv'>
          {postThumb}
         
          <div className="asset-detail"><i>{utils.convertUnicode(myRecentAsset.title)}</i></div>
          <div className={statusClass} >
            <a className={iconName} href="javascript:void(0);"><i className="material-icons">{iconName}</i></a>
          </div>
        </div>
        </div>
        
      );

      }, this)
    }
    
    else {
      return (
        <div className='clearfix'>
          <div className="content_nodata text-no-data text-bg-data">
          <div className="myRecentAssetInner">
          <p>No asset created yet <i class="material-icons">sentiment_very_dissatisfied</i></p>
          <Link to="/assets"><p className="create-assets">Add new asset now.</p></Link>
          </div>
          </div>
        </div>
      );
    }
   /* if (Object.keys(myRecentAssets).length == 2) {

      temp = 
      <div className='clearfix'>
        <div className="content_nodata second_content_nodata">
          <p>Add new asset...!!</p>

        </div>
      </div>;

    
      

    }*/

    return (<div>
      {recentAsset} {temp}</div>);
  }
}