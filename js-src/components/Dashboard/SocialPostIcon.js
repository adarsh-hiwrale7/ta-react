export default class SocialPostIcon extends React.Component {
    render() {
        let social = this.props.socialAccounts;
        return (
            <div className='author-data'>
                <div className='socialIcons clearfix'>
                    {social.indexOf('facebook') !== -1
                        ? <div className='socialShareItem'><a className='fb'>
                            <i className='fa fa-facebook' aria-hidden='true' />
                        </a></div>
                        : null}
                        {social.indexOf('twitter') !== -1
                        ? <div className='socialShareItem'><a className='tw'>
                            <i className='fa fa-twitter' aria-hidden='true' />
                        </a></div>
                        : null}
                        {social.indexOf('instagram') !== -1
                        ? <div className='socialShareItem'><a className='in'>
                            <i className='fa fa-instagram' aria-hidden='true' />
                        </a></div>
                        : null}
                        {social.indexOf('linkedin') !== -1
                        ? <div className='socialShareItem'><a className='li'>
                            <i className='fa fa-linkedin' aria-hidden='true' />
                        </a></div>
                        : null}
                </div>
            </div>
        );
    }
}