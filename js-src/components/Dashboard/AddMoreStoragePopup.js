;
import { Link } from "react-router";
import { browserHistory } from "react-router";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Field, reduxForm } from 'redux-form';
import { renderField } from '../Helpers/ReduxForm/RenderField';
import MentionTextArea from '../Helpers/ReduxForm/MentionTextArea';
import PreLoader from '../PreLoader';
import * as generalAction from '../../actions/generalActions'
import * as dashboardAction from '../../actions/dashboard/dashboardActions'

const required = value => (value ? undefined : 'Required')

class AddMoreStoragePopup extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
          customValidation:true,
    }
    //this.handleView = this.handleView.bind(this);

  }
  closeAddMoreStorage(){
    this.props.closeAddMoreStorage();
  }
  componentDidMount() {
    this.props.dashboardAction.getStoragePrice();
    
  }
  addMoreStorage(storage){
    this.props.dashboardAction.addMoreStorage(storage);
  } 
  componentWillReceiveProps(newProps){
      if(newProps.dashboard.fetchStorageSuccess !== this.props.dashboard.fetchStorageSuccess && newProps.dashboard.fetchStorageSuccess == true){
          this.props.generalAction.fetchS3Size();
          this.props.closeAddMoreStorage();
      }
  }

  render() {

    let dashBoard = this.props.dashboard
    return (
      <div>
          <div className="planChangePopupWrapper createFeedPopup">
            <div className="planChangePopup">
                <div className="customfeedMain">
                     {dashBoard.storagePopoupLoading ? <PreLoader className='Page' /> : ''}
                    <div className="createCustomfeedpopup">
                            <header className="heading clearfix">
                                    <h3>Add more storage</h3>
                                    <button id="close-popup" className="btn-default"><i className="material-icons"  onClick={this.closeAddMoreStorage.bind(this)}>clear</i></button>
                            </header>

                            <div className="storageMainDiv">
                                <div className="storageMainSubDiv">
                                              <div className="storageTitle">Storage Bundles</div>
                                              <div className="storageAmountMain">
                                                    <div className="storageAmountParent">
                                                        <div className="storageAmountSub main-title-storage-amount-sub">


                                                            <div className="storageAmountIcon storage-text-amount-icon">
                                                                {Object.keys(dashBoard.storagePrice).length > 0 ? dashBoard.storagePrice.planTracker.data.tracker_storage_price_show: ''}
                                                            </div>
                                                            <div className="storageAmountText">
                                                                Per month
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="storageAmountParent">
                                                        <div className="storageAmountSub" onClick={this.addMoreStorage.bind(this,2)}>
                                                            <div className="storageAmountIcon">
                                                                <i className="material-icons">storage</i>
                                                            </div>
                                                            <div className="storageAmountText">
                                                                2 GB
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="storageAmountParent">
                                                        <div className="storageAmountSub" onClick={this.addMoreStorage.bind(this,5)}>
                                                            <div className="storageAmountIcon">
                                                              <i className="material-icons">storage</i>
                                                            </div>
                                                            <div className="storageAmountText">
                                                              5 GB
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="storageAmountParent">
                                                        <div className="storageAmountSub" onClick={this.addMoreStorage.bind(this,10)}>
                                                            <div className="storageAmountIcon">
                                                              <i className="material-icons">storage</i>
                                                            </div>
                                                            <div className="storageAmountText">
                                                              10 GB
                                                            </div>
                                                        </div>
                                                    </div>
                                             </div> 
                            </div>
                           </div> 
                    </div>
        </div>
        </div>
        </div>
        </div>
    );
  }
}

function mapStateToProps(state, ownProps) {
  return {
    dashboard: state.dashboard
  }
}

function mapDispatchToProps(dispatch) {
  return {
    dashboardAction: bindActionCreators(dashboardAction, dispatch),
    generalAction: bindActionCreators(generalAction, dispatch),
  }
}
let connection = connect(mapStateToProps, mapDispatchToProps)

var reduxConnectedComponent = connection(AddMoreStoragePopup)

export default reduxForm({
  form: 'AddMoreStoragePopup' // a unique identifier for this form
})(reduxConnectedComponent)

