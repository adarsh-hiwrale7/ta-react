import {Link} from 'react-router';
import ImageLoader from 'react-imageloader'
import PreLoaderMaterial from '../PreLoaderMaterial'
import * as utils from '../../utils/utils';
import Globals from '../../Globals';
import reactTooltip from '../Chunks/ChunkReactTooltip';
import SocialPostIcon from './SocialPostIcon';

export default class MyRecentPost extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
        tooltip: null,
    }
}
  addDefaultSrc(ev){
    ev.target.src = 'img/banner-dashboard.png';
  }
  componentWillMount() {
    reactTooltip().then(reactTooltip => {
        this.setState({tooltip: reactTooltip })
    })
  }
    render() {
        let myRecentPosts = this.props.myRecentPosts,
        recentPost = null,
        share = "",
        me = this;
        if(Object.keys(myRecentPosts).length !== 0 ) {
            recentPost = myRecentPosts.map(function(myRecentPost, index){
              /** Post image */
              var thumbnail_url = '',
              media_type = '',
              postImage = {};
              if(myRecentPost.assetDetail.data.length > 0) {
                myRecentPost.assetDetail.data.map(postMedia => {
                  if (postMedia.type === 'asset') {
                    //if select the post on assets
                    media_type = postMedia.asset!== undefined? postMedia.asset.data.media_type:''
                    thumbnail_url =  postMedia.asset!== undefined? postMedia.asset.data.thumbnail_url:''
                  } else {
                    //if direct uploaded post in feed
                    thumbnail_url = postMedia.thumbnail_url;
                    media_type = postMedia.media_type
                  }
                });
                
                if (media_type && typeof (media_type.split('/')[0] !== 'undefined')) {
                  media_type = media_type.split('/')[0]
                }
              
                if(media_type === 'image') {
                  postImage={
                    backgroundImage: 'url('+ thumbnail_url + ')',
                  }
                }
              }

              let imgurl=myRecentPost.postOwner.data.avatar;
              let imgstyles={
                      backgroundImage: 'url('+ imgurl + ')',
                      backgroundSize: 'cover',
              }
              
              /** get social array */
              let socialAccounts = [];
              if (myRecentPost.socialAccount !== undefined) {
                  let social = myRecentPost.socialAccount.data;
                  social.map(acc => {
                      if (typeof acc.SocialAccount !== 'undefined') {
                          socialAccounts.push(acc.SocialAccount.data.social_media.toLowerCase())
                      }
                  });
              }
              let detail = myRecentPost.detail?utils.limitWords(myRecentPost.detail, Globals.LIMIT_WORD):'';
              /** check type for extrenal and internal */
            if(myRecentPost.types === "external") {
              share = <SocialPostIcon socialAccounts = {socialAccounts}/>;
          } else {
               share =  <div className='author-data'>
                              <div className='socialIcons clearfix'>
                                <div className='socialShareItem'><a><i className="material-icons">vpn_lock</i></a></div>
                              </div>
                          </div>;
          }
                return (
                    <div className="post-item" key={index}>
                        <div className="hotinnerdiv">
                      <div className="colHotdiv">
                      <div className='headerdiv'>
                        <div className='headercolordiv'>
                        <div className='post-header-block'>
                            <div className="inner-header1"> 
                            { (media_type === 'image') ?

                                <ImageLoader
                                src={thumbnail_url}
                                preloader={PreLoaderMaterial}
                                className="inner-header-img">
                                <div className="image-load-failed">
                                <img src="img/banner-dashboard.png" className="inner-header-img" />
                                </div> 
                                </ImageLoader> 
                                :
                                media_type === 'video'?
                                <div className='thumbnail'>
                                    <ImageLoader
                                        src={thumbnail_url}
                                        preloader={PreLoaderMaterial}
                                        className="inner-header-img">
                                        <div className="image-load-failed">
                                            <img src="img/banner-dashboard.png" className="inner-header-img" />
                                        </div> 
                                    </ImageLoader> 
                                    <div className='postVideoIconWrapper'><i className="material-icons">play_circle_filled</i></div>
                                    </div>
                            :   <img src="img/banner-dashboard.png" className="inner-header-img"/>
                            
                            }
                              </div> 
                              <div className='userimage' style={imgstyles}
                               data-tip={myRecentPost.postOwner.data.email}
                                data-for={myRecentPost.postOwner.data.identity} > 
                                {this.state.tooltip ?
                                <this.state.tooltip
                                id={myRecentPost.postOwner.data.identity}
                                type='warning'
                                effect = 'solid'
                                className = 'tooltipsharepopup'
                            />:''}</div>
                            </div>
                          <div className='content'>
                          <div className='post-title'>{myRecentPost.postOwner.data.firstname + ' ' + myRecentPost.postOwner.data.lastname}</div>
                          <div class="post-description">
                            <p>
                              { utils.convertUnicode(detail)}
                            </p>
                            </div>
                            <div className='channel-title'><span>CHANNELS</span></div>
                          </div>
                          <div className='socialLogo'>
                            {share}
                           </div>
                        </div>
                        <div className='footerdiv '>
                          <div className='socialLogo'>
                            <div className='post-meta'>
                              <a>
                               <i className="material-icons">thumb_up</i>
                                <span>{myRecentPost.like_count}</span>
                              </a>
                            </div>
                            { myRecentPost.types =='external' ? 
                            <div className='post-meta'>
                              <a>
                                <i className="material-icons">share</i>
                                <span>{myRecentPost.share_count}</span>
                              </a>
                            </div>
                            :''}
                            <div className='post-meta'>
                              <a>
                                <i className="material-icons">chat_bubble</i>
                                <span>{myRecentPost.comment_count}</span>
                              </a>
                            </div>
                          </div>

                        </div>
                        
                      </div>
                    </div>
                    </div>
                </div>    
                );

            });
        }
        else{
          // let imgurl=myRecentPost.post.data.postOwner.data.avatar;
          //     let imgstyles={
          //             backgroundImage: 'url('+ imgurl + ')',
          //             backgroundSize: 'cover',
          //             backgroundRepeat:'no-repeat'

             // }
             let imgurl="img/default-avatar.png";
             let imgstyles={
              backgroundImage: 'url('+ imgurl + ')',
              backgroundSize: 'cover',
      }
          return(
              <div className="post-item single-item">
                        <div className="hotinnerdiv">
                      
                      <div className='headerdiv'>
                        <div className='headercolordiv'>
                          <div className='post-header-block'>
                          <div className="inner-header1"> 
                          <img src="img/banner-dashboard.png" className="inner-header-img"/> 
                              </div> 
                          <div className='userimage' style={imgstyles} >
                          </div>
                          </div>
                          <div className='content'>
                          <div class="post-description">
                          <p>
                          No Post.<i class="material-icons">sentiment_very_dissatisfied</i></p> 
                          <p>Be the new star.. </p>
                          <Link to="/feed" ><span className="text">Create a new post now</span></Link>
                          </div>
                          </div>
                          <div className='socialLogo'>
                            <div className='author-data'></div>
                           </div>
                        </div>
                        <div className='footerdiv '>
                          <div className='socialLogo'>
                            {/* <div className='post-meta'>
                              <a>
                               <i className="material-icons">thumb_up</i>
                                <span>0</span>
                              </a>
                            </div>

                            <div className='post-meta'>
                              <a>
                                <i className="material-icons">share</i>
                                <span>0</span>
                              </a>
                            </div>
                            <div className='post-meta'>
                              <a>
                                <i className="material-icons">chat_bubble</i>
                                <span>0</span>
                              </a>
                            </div> */}
                          </div>

                        </div>
                        
                      </div>
                    </div>
                </div>
          );
        }
        return (<div>{recentPost}</div>);
    }
}