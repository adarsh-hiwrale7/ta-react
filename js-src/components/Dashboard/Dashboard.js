import { bindActionCreators } from 'redux'
import { browserHistory } from 'react-router'
import { connect } from 'react-redux'
import { Field, reduxForm } from 'redux-form'
import { Link } from 'react-router'
import * as assetsActions from '../../actions/assets/assetsActions'
import * as s3functions from '../../utils/s3'
import * as s3Actions from '../../actions/s3/s3Actions'
import * as generalActions from '../../actions/generalActions'
import PreLoader from '../PreLoader'
import AssetDetail from '../Assets/AssetDetail'
import Globals from '../../Globals'
import InternalPost from './InternalPost'
import ValueLists from './ValueLists'
import moment from '../Chunks/ChunkMoment'
import MyRecentAsset from './MyRecentAsset'
import MyRecentPost from './MyRecentPost'
import CultureDashboardAnalytics from './CultureDashboardAnalytics'
import PhotoEditor from '../Feed/PhotoEditor'
import { Component } from 'react';
import PostMap from './PostMap';
import SliderPopup from '../SliderPopup';
import GuestUserRestrictionPopup  from '../GuestUserRestrictionPopup'
import recharts from '../Chunks/ChunkRecharts';
import SummaryDashboardChart from './SummaryDashboardChart'

import * as authActions from '../../actions/authActions'
import * as dashboardActions from '../../actions/dashboard/dashboardActions'
import * as cultureActions from '../../actions/cultureActions'
import PhotoEditorSDK from '../Chunks/ChunkPhotoEditor'
var screenWidth = document.body.clientWidth;
var dashboardrestrict = false;
var dashboardMapPosts=[]
class Dashboard extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      moment: null,
      assetDetailPopup: false,
      currAsset: null,
      overlay_flag: false,
      contentStorage: 0,
      storagePercentage: 0,
      s3Manager: null,
      openEditor: false,
      assets: [],
      storageType:'MB',
      Recharts:null,
      refreshChart: false ,
      photoEditor: null,   }

    this.chartInstance = null;
    this.period = 'month';
    this.handleAssetPopup = this.handleAssetPopup.bind(this)
    this.getStorageSize = this.getStorageSize.bind(this)
  }
  handleAssetPopup(assetDetail) {
    this.setState({
      assetDetailPopup: true,
      currAsset: assetDetail
    })

    document.body.classList.add('overlay')

  }
  updateText(id, type, e) {
    this.setState({ overlay_flag: true });
    if (type === 'department') {
      this.props.departmentActions.fetchDepartmentById(id);
    } else {
      this.props.userActions.fetchUserDetail(id);
    }
    const domNode = ReactDOM.findDOMNode(e.target);
    let coords = domNode.getBoundingClientRect();
    let coords_top = coords.top + pageYOffset;
    let popup_wrapper = document.getElementById("pop-up-tooltip-wrapper");
    let coords_left = 0,
      coords_area = 0,
      new_class_comb = '';
    let pop_holder = document.getElementById("pop-up-tooltip-holder");
    let popupparent = document.getElementById('department-popup');
    if (popupparent != null) {
      pop_holder.className = 'departmentpopup';
    } else {
      pop_holder.className = '';
    }
    if (screen.width >= 768) {
      coords_left = coords.left + pageXOffset + (coords.width / 2);
    }
    else {
      coords_left = coords.left + pageXOffset;
      coords_area = coords_left + 472;
      if (coords_area < screen.width) {
        new_class_comb = 'popup-left';
      }
      else {
        new_class_comb = 'popup-right';
      }

    }
    if (e.nativeEvent.screenY < 400) {
      new_class_comb += ' bottom';
    }
    else {
      new_class_comb += ' top';
    }
    popup_wrapper.className = new_class_comb;
    this.setState({
      popupTop: coords_top,
      popupLeft: coords_left,
      popupdisplay: 'block',
      tagType: type,
      tagTypeClass: (type === "department" ? 'departmentpopup' : '')
    });
    let pop_overlay = document.getElementById("tag-popup-overlay");
    pop_overlay.className = 'show';

  }

  closeAssetDetailPopup() {
    this.setState({
      assetDetailPopup: false
    })
    document.body.classList.remove('overlay')
  }
  renderAssetDetail(asset) {
    if(asset !== undefined){
      var url = 'cat/'+asset.parent;
      if(asset.folder !== ""){
        url = url+'/folder/'+asset.folder;
      }
      browserHistory.push('/assets/'+url+'?openasset=true&source=dashboard&assetid='+asset.identity)
    }
  }
  renderEditor(asset) {
    return (
      <SliderPopup className='wide editorPopup'>
        <button
          id='close-popup'
          className='btn-default'
          onClick={this.closeEditorPopup.bind(this)}
        >
          <i className='material-icons'>clear</i>
        </button>
        <div id="post-editor-wrapper">
        {this.state.photoEditor?
          <PhotoEditor
            photoEditor={this.state.photoEditor}
            imageData={this.state.currAsset.media_url}
            imageName={this.state.currAsset.title}
            mediaType={this.state.currAsset.media_type}
            closeEditorPopup={this.closeEditorPopup.bind(this)}
            updateEditedImage={this.updateEditedImage.bind(this, this.state.currAsset)}
          />:''}
        </div>
      </SliderPopup>
    );
  }
  changeDropBoxValue() {
    const initData = {
      assets: this.state.currAsset
    }
    this.props.initialize(initData)
  }
  updateEditedImage(e, editedImage) {
    var assetdetail = {
      name: editedImage[0].name,
      preview: editedImage[0].preview,
      size: editedImage[0].size,
      media_type: editedImage[0].type,
      identity: e.identity,
      title: e.title,
      thumbnail_url: e.thumbnail_url,
      media_extension: e.media_extension,
      approved: e.approved,
      created_date: e.created_date,
      assetAuthor: e.assetAuthor,
      tags: e.tags,
      detail: e.detail,
      userTag: e.userTag,
      firstname: e.firstname,
      lastname: e.lastname,
      roleName: null
    }
    this.setState({ currAsset: assetdetail }, () => {
      this.changeDropBoxValue.bind(this);
    });
  }
  closeEditorPopup() {
    this.setState({ openEditor: false })
  }
  showeditor(assetDetail) {
    this.setState({
      openEditor: true,
      currAsset: assetDetail
    })
  }
  componentDidMount() {
    
    document.title =  'Dashboard / Visibly';
    this.props.generalActions.fetchS3Size()
    var me = this
    if(dashboardrestrict!==true){
      this.props.dashboardActions.fetchPostsAssetsAnalytics()
      this.props.dashboardActions.fetchMyRecentAssets('month')
      this.props.cultureActions.fetchValuesList()
      this.init('month')
      dashboardrestrict=true;
      setTimeout(()=>{
        dashboardrestrict=false;
      },5000)
    }
    var e = this;

    window.addEventListener("resize", function(){
      
          var width = document.body.clientWidth;
          var height = document.body.clientHeight;  
          e.setState({refreshChart:!e.state.refreshChart});      
    }, true);
    if(JSON.parse(localStorage.getItem('roles')) !== null){
          
        this.setState({
          'roleName': JSON.parse(localStorage.getItem('roles'))[0]['role_name']
        })
    }
  }
  
  init(period) {
    this.period = period;
    this.rearrangeAnalytics(period)
    //this.props.dashboardActions.fetchChannel(period)
    //this.props.dashboardActions.fetchModerated(period)
    this.props.dashboardActions.fetchTopCompanyExternalPost(period)
    this.props.dashboardActions.fetchTopExternalPost(period)
    this.props.dashboardActions.fetchTopCompanyInternalPost(period)
    this.props.dashboardActions.fetchTopInternalPost(period)
    //this.props.dashboardActions.fetchMyRecentPosts(period)
    this.props.dashboardActions.fetchMapPosts(period)
  }
  componentWillReceiveProps(newprops){
    if(Object.keys(newprops.dashboard.dashboardMapPosts ).length>0 && this.props.dashboard.dashboardMapPosts!==newprops.dashboard.dashboardMapPosts ){
      dashboardMapPosts=newprops.dashboard.dashboardMapPosts
    }
    if(this.props.dashboard.dashboardAnalyticsInitial !== newprops.dashboard.dashboardAnalyticsInitial && Object.keys(newprops.dashboard.dashboardAnalyticsInitial).length > 0){
       this.rearrangeAnalytics(this.period, newprops.dashboard.dashboardAnalyticsInitial); 
    }
  }

  componentWillMount() {
    window.scrollTo(0,0);
    let me = this
    moment().then(moment => {
      recharts().then(recharts=>{
        PhotoEditorSDK().then(photoEditor => {
          me.setState({ moment: moment,Recharts:recharts ,photoEditor: photoEditor})
        })
      })
    })
    // var s3 = s3functions.loadAWSsdk();
    // s3
    //   .then(function () {
    //     var s3Config = Globals.AWS_CONFIG;
    //     var s3Manager = new s3functions.S3Manager(s3Config);
    //     me.setState({ s3Manager: s3Manager })
    //   })
    // this.getStorageSize()
  }
  componentWillUnmount() {
    window.scrollTo(0,0);
  }

  rearrangeAnalytics(type, data = null) {
    let chartData = data !== null ? data.chart_data : Object.keys(this.props.dashboard.dashboardAnalyticsInitial).length>0?this.props.dashboard.dashboardAnalyticsInitial.chart_data:'';
    let chart_data = [];
    if (chartData !== '') {
      if (type == 'month') {
        chart_data = chartData.slice(chartData.length - 30)
        this.props.dashboardActions.modifyPostsAssetsAnalytics({chart_data})
      } else if (type == 'week') {
        chart_data = chartData.slice(chartData.length - 7)
        this.props.dashboardActions.modifyPostsAssetsAnalytics({chart_data})
      } else {
        chart_data = chartData.slice()
        this.props.dashboardActions.modifyPostsAssetsAnalytics({chart_data})
      }
    }
  }
  getStorageSize() {
    var me = this
    Globals.AWS_CONFIG.albumName = localStorage.getItem('albumName')
    if (me.state.s3Manager) {
      me.state.s3Manager.getFolderSize(Globals.AWS_BUCKET_NAME, Globals.AWS_CONFIG.albumName, function (err, size) {
        var sizeInMB = (size / (1024 * 1024)).toFixed(2);
        // me.setState({ contentStorage: sizeInMB, storagePercentage: ((sizeInMB * 100) / Globals.contentStorage) })
      })
    }
  }
  renderChartLoader(){
    return(
        <div class="culture-analytics-exapnd">
        <div class="innerTotalPostExapnd">
            <div class="row-container-of-the-chart-analytics row-container-of-the-chart-analytics-loader-totalpost">
                <div class="inner-adoption-chart">
                    <div class="header-analytics-wrapper clearfix header-loader-wrapper-analytics">
                        <div class="title  loader-grey-line  loader-line-radius loader-line-height"></div>
                    </div>
                    <div class="bodyPartTotalPostExapnd" id="expandTotalPost">
                        <div class="recharts-legend-wrapper">
                            <ul>
                                <li class="loader-grey-line loader-line-radius loader-line-height"> </li>
                                <li class="loader-grey-line loader-line-radius loader-line-height"></li>
                                <li class="loader-grey-line loader-line-radius loader-line-height"></li>
                               
                            </ul>
                        </div>
                        <div class="loader-grey-line  chart-container-loader"> </div>
                    </div>
                </div>
            </div>
         </div>
    </div>
    )
}
  
  render() {
    let dashboardChannels = this.props.dashboard.dashboardChannels,
      dashboardInternalPost = this.props.dashboard.dashboardInternalPost[0],
      dashboardCompanyInternalPost = this.props.dashboard.dashboardCompanyInternalPost[0],
      dashboardMyRecentAssets = this.props.dashboard.dashboardMyRecentAssets,
      myRecentAssetsLoader = this.props.dashboard.recentAssetLoader,
      dashboardMyRecentPosts = this.props.dashboard.dashboardMyRecentPosts,
      dashboardCompanyExternalPost = this.props.dashboard
        .dashboardCompanyExternalPost[0],
      dashboardExternalPost = this.props.dashboard.dashboardExternalPost[0],
      currAsset
      var dashboardProps=this.props.dashboard
      
     
    return (
      <div>
      <GuestUserRestrictionPopup setOverlay={true}/>
      <section id='dashboard' className='home-page'>

        {this.state.assetDetailPopup
          ? this.renderAssetDetail(this.state.currAsset)
          : ''}
        {this.state.openEditor ? this.renderEditor(this.state.currAsset) : ""}
         <div className='main-container'>
          <div id='content'>
            <div id='firstdiv'>
              <div className='clearfix full-container'>
                <div className='welcome-div'>
                <div className= "current-date">{this.state.moment !== null ? this.state.moment().format('DD MMM, YYYY') : ''}</div>
                  <div className='welcome-div-inner'>
                    <h3>Welcome to your dashboard, <span className='username'>{this.props.profile.profile.first_name}</span></h3>
                    {/* <div className='username'>{this.props.profile.profile.first_name}</div> */}
                  </div>
                </div>
               </div>
            </div>
            { this.state.roleName == 'admin' || this.state.roleName == 'super-admin'? 
              <div className = "culture-dashboard-design-wrapper"> 
                  <CultureDashboardAnalytics />
              </div>
              :''}
          {/*value list component for dashboard*/}    
               <ValueLists culture={this.props.culture}/>
          {/*finish*/}
         <div className='moderation-section'>
              <div className='full-container'>
                <div className="title">MODERATION TO DO (Admin)</div>
                <div className='moderation-item-wrapper'>
                  <div className='moderation-item clearfix'>
                    <div className='mod-label'><i class="material-icons">filter</i><span>Posts</span></div>
                    <div className='mod-value'>1001</div>
                  </div>
                  <div className='moderation-item clearfix'>
                    <div className='mod-label'><i class="material-icons">folder_open</i><span>Assets</span></div>
                    <div className='mod-value'>10</div>
                  </div>
                </div>
              </div>
            </div>
            {this.props.dashboard.chartLoader == true ?
              this.renderChartLoader():
            <div className='chartdiv'>
              <div className='chartinnerdiv full-container'>
              <div className="header-analytics-wrapper clearfix">
              <div className="title"> SUMMARY </div>
             
               <div className="filter-weekey-container clearfix">
                <div className = "filter-container">
                  <a className={`week-filter ${this.period == "week" ? 'active' : ''}`} onClick={this.init.bind(this, 'week')}> Week </a>
                  <a className={`month-filter ${this.period == "month" ? 'active' : ''}`} onClick={this.init.bind(this, 'month')}> Month </a>
                  <a className={`year-filter ${this.period == "year" ? 'active' : ''}`} onClick={this.init.bind(this, 'year')}>Year </a>
                </div>
              </div>
            </div>
                <div id='dashboardLineChart' className='dashboardlinechart'>
                  <div className='chart-wrapper dashboard-analytics-level1-container'>
                    {this.state.Recharts ? 
                      <SummaryDashboardChart 
                        dashboardProps={this.props.dashboard} 
                        period={this.period}
                        LineChart={this.state.Recharts.LineChart}
                        XAxis={this.state.Recharts.XAxis}
                        YAxis={this.state.Recharts.YAxis}
                        CartesianGrid={this.state.Recharts.CartesianGrid}
                        Legend={this.state.Recharts.Legend}
                        Tooltip={this.state.Recharts.Tooltip}
                        Line={this.state.Recharts.Line}
                        moment={this.state.moment}
                        refreshChart={this.state.refreshChart} 
                        />:''}
                  </div>
                </div>
              </div>
            </div>}
            <div className='hotdiv'>
              <div className='hotinnerdiv full-container'>
                <div className='row innerpost-wrapper clearfix'>
                <div className = "company-post-wrapper">
                 <div className = "title"> TOP COMPANY POSTS  </div>
                 <div className= "clearfix">
                 <InternalPost
                    internalPost={dashboardCompanyInternalPost}
                    profile={this.props.profile.profile}
                    title='Top Internal Company Post'
                    internalColor='internalpost'
                    contentLoader={dashboardProps.companyInternalPostLoader}
                  />
                 
                  <InternalPost
                    internalPost={dashboardCompanyExternalPost}
                    profile={this.props.profile.profile}
                    title='Top External Company Post'
                    type='external'
                    contentLoader={dashboardProps.companyExternalPostLoader}
                  />
                 
                  </div>
                  </div> 
                  <div className = "my-top-post-wrapper">
                  <div className = "title"> MY TOP POSTS  </div>
                 <div className= "clearfix">
                 <InternalPost
                    internalPost={dashboardInternalPost}
                    profile={this.props.profile.profile}
                    title='My Top Internal Post'
                    internalColor='internaltoppost'
                    contentLoader={dashboardProps.internalPostLoader}

                  />
                 <InternalPost
                    internalPost={dashboardExternalPost}
                    profile={this.props.profile.profile}
                    title='My Top External Post'
                    type='external'
                    contentLoader={dashboardProps.externalPostLoader}
                  />
                  </div>
                  </div>
                </div>
              </div>
              {/* ======================== */}

            </div>


            {/* ============= */}

            <div className='contributdiv'>
              <div className='inner-contribut full-container'>
                <div className='title'>MY RECENT ASSETS</div>
                <div className='row recent-assets-posts-wrapper'>
                  <div className='col-one-of-two'>
                      <MyRecentAsset myRecentAssets={dashboardMyRecentAssets} contentLoader={myRecentAssetsLoader} handleAssetPopup={this.handleAssetPopup} />
                      {/* <div className='dashboard-bottom-title'>My Recent Assets</div> */}
                  </div>

                  {/* <div className='col-one-of-two last '>
                    <div className='row'>
                      <MyRecentPost myRecentPosts={dashboardMyRecentPosts} profile={this.props.profile} />
                    </div>
                    <div className='dashboard-bottom-title'>My Recent Posts</div>
                  </div> */}
                </div>
              </div>
            </div>
            {/* ============== */}
            {/* map */}
            <div className='mapdiv'>
                <div className='map-innerdiv'>
                  <div className='full-container'>
                  <div className="header-analytics-wrapper clearfix">
                    <div className="title"> ACTIVITY </div>
                  </div>
                <PostMap postMap={dashboardMapPosts} />
                  </div>
                </div>
              </div>
            {/* map */}

          </div>
        </div>
      </section>
     </div>

    )
  }
}

function mapStateToProps(state) {

  return {
    profile: state.profile,
    dashboard: state.dashboard,
    assets: state.assets,
    general: state.general,
    aws: state.aws,
    culture: state.culture
  }
}
function mapDispatchToProps(dispatch) {
  return {
    authActions: bindActionCreators(authActions, dispatch),
    generalActions: bindActionCreators(generalActions, dispatch),
    dashboardActions: bindActionCreators(dashboardActions, dispatch),
    assetsActions: bindActionCreators(assetsActions, dispatch),
    s3Actions: bindActionCreators(s3Actions, dispatch),
    cultureActions: bindActionCreators(cultureActions, dispatch),
  }
}
// calling connect on our component automatically adds dispatch prop to our component. We can use this dispatch prop to call our action creators like this.props.dispatch(actionCreator()). When we use bindActionCreators, we can call our action creators like this.props.actions.actionCreator(). This helps to keep our components simple
module.exports = connect(mapStateToProps, mapDispatchToProps)(Dashboard)
// function loadJS(src) {
//   var ref = window.document.getElementsByTagName("script")[0];
//   var script = window.document.createElement("script");
//   script.src = src;
//   script.async = false;
//   ref.parentNode.insertBefore(script, ref);
// }