import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import CultureScore from '../Analytics/CultureScore/CultureScore';
import ParticipationWidget from '../Analytics/CultureScore/ParticipationWidget';
import * as utils from '../../utils/utils';
import * as cultureAnalyticsActions from '../../actions/analytics/cultureAnalyticsActions';
import recharts from '../Chunks/ChunkRecharts';
import reactTooltip from '../Chunks/ChunkReactTooltip';
import CultureLoader from "../Analytics/CultureScore/CultureLoader";
var screenWidth='';
const COLORS = ['#4bcf99', '#ff595e', '#4b74e0', '#ffbf00', '#b87d7a', '#FF8042','#495867','#7FB069','#B8D8D8','#A93F55','#577399','#F9ADA0',"#ACF39D",'#19323C'];
class CultureDashboardAnalytics extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
             tooltip: null,
             refreshChart: false,
             Recharts: null,
             period:'month'
        }
       
    }
    componentDidMount(){
        var timezone = utils.clientTimezone();
        this.props.cultureAnalyticsActions.getIndexData(timezone);
        var ObjToSend = {
            'timezone': timezone,
            timespan:'month'
        }
        this.props.cultureAnalyticsActions.getParticipationData(ObjToSend);

         var e = this;

        window.addEventListener("resize", function () {
            var width = document.body.clientWidth;
            var height = document.body.clientHeight;
            if (screenWidth < 1500 && width >= 1500) {
                screenWidth = width
                e.setState({ refreshChart: true });
            } else if (screenWidth > 1500 && width <= 1500) {
                screenWidth = width
                e.setState({ refreshChart: true });
            } else if (screenWidth > 767 && width <= 767) {
                screenWidth = width
                e.setState({ refreshChart: true });
            } else if (screenWidth < 767 && width >= 767) {
                screenWidth = width
                e.setState({ refreshChart: true });
            } else {
                if (e.state.refreshChart == true) {
                    e.setState({ refreshChart: false });
                }
                screenWidth = width
            }

        }, true);

    }
    componentWillMount() {
        recharts()
            .then(recharts => {
                 reactTooltip().then(reactTooltip => {
                    this.setState({ Recharts: recharts, tooltip: reactTooltip})
                 })   
            })
            .catch(err => {
                throw err
            })
    }
    selectedTimeSpan(newprops=null,from,time){
        var timezone = utils.clientTimezone();
        switch (from) {
           
            case 'participation':
                this.setState({
                    period: { ...this.state.period, [from]: time }
                })
                var ObjToSend = {
                    timezone: timezone,
                    timespan:time
                }
                this.props.cultureAnalyticsActions.getParticipationData(ObjToSend)
                break;
            default:
                break;
        }
    }
    render(){
        var culture = this.props.culture;
        return(
            <div class="culture-participation-mainbox dashboard-analytics-level1-container">
            <div class="culture-participation-wrapper clearfix">
                <CultureScore cultureData={culture} tooltip={this.state.tooltip} />
                {
                    this.state.Recharts != null ?
                        <ParticipationWidget 
                            PieChart={this.state.Recharts.PieChart} 
                            Pie={this.state.Recharts.Pie} 
                            cultureData={culture} 
                            Cell={this.state.Recharts.Cell} 
                            COLORS={COLORS} 
                            Tooltip={this.state.Recharts.Tooltip} 
                            Legend={this.state.Recharts.Legend} 
                            tooltip={this.state.tooltip} 
                            refreshChart={this.state.refreshChart} 
                            period={this.state.period['participation']} 
                            participationTimeSpan={this.selectedTimeSpan.bind(this,null,'participation')} 
                            noFilters={true}
                        />
                        :
                        <div className="participation-main-wrapper">
                            <CultureLoader/>  
                        </div>
                }
            </div>
         </div>
        
        )
    }
}
function mapStateToProps(state) {
    return {
        culture: state.cultureAnalytics,
    }
}
function mapDispatchToProps(dispatch) {
    return {
        cultureAnalyticsActions: bindActionCreators(cultureAnalyticsActions, dispatch),
    }
}
let connection = connect(mapStateToProps, mapDispatchToProps);
module.exports = connection(CultureDashboardAnalytics);