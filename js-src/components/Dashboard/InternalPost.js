import { Link } from 'react-router';

import * as utils from '../../utils/utils';
import ImageLoader from 'react-imageloader'
import PreLoaderMaterial from '../PreLoaderMaterial'

import Globals from '../../Globals';
import reactTooltip from '../Chunks/ChunkReactTooltip';
import SocialPostIcon from "./SocialPostIcon";

export default class InternalPost extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            tooltip: null,
        }
    }
    componentWillMount() {
        reactTooltip().then(reactTooltip => {
            this.setState({
                tooltip: reactTooltip 
            })
        })
    }
    addDefaultSrc(ev) {
        ev.target.src = 'img/banner-dashboard.png';
        ev.target.className = 'show-loader inner-header-img';
    }
    loader(){
        return(
            <div class="col-one-of-two post-item hot-div-loader">
                    <div class="headerdiv">
                        <div class="headercolordiv">
                            <div class="post-header-block">
                                <div class="inner-header1">
                                    <div class="thumbnail">
                                        <span class="imageloader failed inner-header-img">
                                            <div class="image-load-failed loader-grey-line loader-line-height image-in-top-company-post"></div>
                                        </span>
                                    </div>
                                </div>
                                <div class="userimage loader-grey-line"> </div>
                            </div>
                            <div class="content">
                                <div class="post-title loader-grey-line loader-line-height loader-line-radius"> </div>
                                <div class="post-description">
                                    <p class="post-title loader-grey-line loader-line-height loader-line-radius"> </p>
                                </div>
                                <div className="Blank-div-for-footer"> </div>
                            </div>
                            <div className="footer-loader loader-grey-line loader-line-height"> </div>
                        </div>

                    </div>
            </div>
        )
    }
    render(){
        return(
            <div>
                {
                    this.props.contentLoader == true ?
                        this.loader()
                    :    
                        this.renderContent(this.props.internalPost, this.props.type, this.props.profile)
                }
            </div>
        )
    }
    renderContent(postData, type, profile) {
        let post = postData;
        var me = this;
        if (post) {
            /** get social array */
            let socialAccounts = [];
            let share = "";
            /** Post image */
            var thumbnail_url = '',
                media_type = '',
                postImage = {};
            if (post.assetDetail.data.length > 0) {
                post.assetDetail.data.map(postMedia => {
                    if (postMedia.type === 'asset') {
                        media_type = postMedia.asset.data.media_type
                        thumbnail_url = postMedia.asset.data.thumbnail_url
                    } else {
                        thumbnail_url = postMedia.thumbnail_url;
                        media_type = postMedia.media_type
                    }
                });
                if (media_type && typeof (media_type.split('/')[0] !== 'undefined')) {
                    media_type = media_type.split('/')[0]
                }
                if (media_type === 'image') {
                    postImage = {
                        backgroundImage: 'url(' + thumbnail_url + ')',
                        backgroundSize: 'cover',
                        backgroundRepeat: 'no-repeat'
                    }
                }
            }

            let imgurl;
            if(post.postOwner.data.avatar!=='undefined'){
                imgurl = post.postOwner.data.avatar;
            }
            else{
                imgurl = "img/default-avatar.png"
            }

            let imgstyles = {
                backgroundImage: 'url(' + imgurl + ')',
                backgroundSize: 'cover',
                backgroundRepeat: 'no-repeat'

            }
            if (typeof post.socialAccount !== 'undefined') {
                let social = post.socialAccount.data;
                social.map(acc => {
                    if (typeof acc.SocialAccount !== 'undefined') {
                        socialAccounts.push(acc.SocialAccount.data.social_media.toLowerCase())
                    }
                });
            }

            /** check type for extrenal and internal */
            if (type === "external") {
                share = <SocialPostIcon socialAccounts={socialAccounts} />;
            } else {
                share = <div className='socialLogo'>
                    <div className='author-data star'>
                        <div className='socialIcons clearfix'>
                            <div className='socialShareItem'>
                            <a className='internal-icon' title='Internal channel'>
                                <i className="material-icons">vpn_lock</i>
                            </a>
                            </div>
                        </div>
                    </div>
                </div>;
            }
            let detail = post.detail?utils.limitWords(post.detail, Globals.LIMIT_WORD):'';
            return (
                <div className='col-one-of-two post-item'>
                    <div className='headerdiv'>
                        <div className='headercolordiv'>
                            <div className='post-header-block'>
                                <div className="inner-header1">
                                    {(media_type === 'image') ?
                                        <div>
                                                <ImageLoader
                                                        src={thumbnail_url}
                                                        preloader={PreLoaderMaterial}
                                                        className="inner-header-img" >
                                                       <div className="image-load-failed">
                                                       <img src="img/banner-dashboard.png" className="inner-header-img" />
                                                       </div> 
                                                    </ImageLoader> 
                                          
                                        </div>
                                        : media_type === 'video'?
                                        <div className='thumbnail'>
                                            <ImageLoader
                                                src={thumbnail_url}
                                                preloader={PreLoaderMaterial}
                                                className="inner-header-img" >
                                                <div className="image-load-failed">
                                                     <img src="img/banner-dashboard.png" className="inner-header-img" />
                                                </div> 
                                            </ImageLoader>
                                    <div className='postVideoIconWrapper'><i className="material-icons">play_circle_filled</i></div>
                                    </div>
                                        :<img src="img/banner-dashboard.png" className="inner-header-img" />
                                    }
                                </div>

                                <div className='userimage' style={imgstyles}
                                    data-tip={post.postOwner.data.email}
                                    data-for={post.postOwner.data.identity} > </div>
                            </div>
                            {this.state.tooltip?
                            <this.state.tooltip
                                id={post.postOwner.data.identity}
                                type='warning'
                                effect = 'solid'
                                className = 'tooltipsharepopup'

                            />:''}
                            {/* <img src={post.postOwner.data.avatar} />*/}

                            <div className='content'>

                                <div className='post-title'>{post.postOwner.data.firstname + ' ' + post.postOwner.data.lastname}</div>

                                <div class="post-description">
                                    <p>
                                        {utils.convertUnicode(detail)}
                                    </p>
                                </div>
                                <div className='channel-title'><span>CHANNELS</span></div>
                            </div>

                            {share}
                        </div>
                        <div className='footerdiv'>
                            <div className='socialLogo'>
                                <div className='post-meta'>
                                    <a>
                                        <i className="material-icons">thumb_up</i>
                                        <span>{post.like_count}</span>
                                    </a>
                                </div>
                                {post.types == 'external' ?

                                    <div className='post-meta'>
                                        <a>
                                            <i className="material-icons">share</i>
                                            <span>{post.share_count}</span>
                                        </a>
                                    </div>
                                    : ''}
                                <div className='post-meta'>
                                    <a>
                                        <i className="material-icons">chat_bubble</i>
                                        <span>{post.comment_count}</span>
                                    </a>
                                </div>
                            </div>

                        </div>
                        {/* <div className='dashboard-bottom-title'>{this.props.title}</div> */}
                    </div>
                </div>

            );
        } else {
            let imgurl = profile.avatar;
            let imgstyles = {
                backgroundImage: 'url(' + imgurl + ')',
                backgroundSize: 'cover',
                backgroundRepeat: 'no-repeat'

            }
            return (
                <div className='col-one-of-two post-item'>
                    <div className='headerdiv'>
                        <div className='headercolordiv'>
                            <div className='post-header-block'>
                                <div className="inner-header1">
                                    <img src="img/banner-dashboard.png" className="inner-header-img" />
                                </div>

                                <div className='userimage' style={imgstyles}
                                >
                                    {/*<img src="/img/" />*/}
                                </div>
                            </div>
                            <div className='content'>
                                <div className='post-title'></div>
                                <div class="post-description">
                                    <p>
                                     No post created yet <i class="material-icons">sentiment_very_dissatisfied</i></p>
                                    <Link to="/feed" ><span className="text">Add new post now.</span></Link>
                                </div>
                            </div>
                            <div class="author-data"><div class="author-meta socialIcons"></div></div>
                        </div>
                        <div className='footerdiv'>
                            <div className='socialLogo'>
                                {/* <div className='post-meta'>
                                <a>
                                <i className="material-icons">thumb_up</i>
                                <span>0</span>
                                </a>
                            </div>

                            <div className='post-meta'>
                                <a>
                                <i className="material-icons">share</i>
                                <span>0</span>
                                </a>
                            </div>
                            <div className='post-meta'>
                                <a>
                                <i className="material-icons">chat_bubble</i>
                                <span>0</span>
                                </a>
                            </div> */}
                            </div>

                        </div>
                        {/* <div className='dashboard-bottom-title'>{this.props.title}</div> */}
                    </div>
                </div>

            );
        }
    }
}
