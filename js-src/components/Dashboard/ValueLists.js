import { fetchTotalSocialEngagementData } from '../../actions/analytics/analyticsActions';
var selectedValueId=null
export default class ValueLists extends React.Component {
constructor(props) {
    super(props)
    this.state = {
        expandView:false
    }
  }
  componentDidMount(){
      var me = this
    window.addEventListener('click', function (event) {
        if(!(event.target.classList.contains("value-list-title")|| event.target.classList.contains('value-list-descripation') ||event.target.classList.contains("value-list-container")|| event.target.classList.contains("valueAccordian"))){
            selectedValueId= null;
            me.setState({});
        }
    })
  }

  ValueListToggleEvent(id,e){
    selectedValueId=selectedValueId == `description-${id}` ? null : `description-${id}`;
    this.setState({});
  }
  loader(){
      return(
          <div className="value-containe-in-dashboard">
                 <div className="full-container">
                <div className="inner-value-containe-in-dashboard">
                    <div className="clearfix">
                        <div className="overview-value-details-description">
                            <div className="value-title">
                                 <div className = "loader-line-height loader-line-radius loader-line-space loader-grey-line"> </div>
                                 <div className = "loader-line-height loader-line-radius loader-line-space loader-grey-line"> </div>
                                 <div className = "loader-line-height loader-line-radius loader-line-space loader-grey-line"> </div>
                            </div>
                        </div>
                        <div className="value-details-description">
                            <div className="value-list" id='value-list'>
                                <div className="list-value1 list-value-loader">
                                    <div className="value-list-container">
                                        <div className="value-list-title loader-line-height loader-line-radius  loader-grey-line"></div>
                                        <div className="value-list-descripation loader-line-height loader-line-radius  loader-grey-line"></div>
                                    </div>
                            
                                </div>
                                <div className="list-value1 list-value-loader">
                                    <div className="value-list-container">
                                        <div className="value-list-title loader-line-height loader-line-radius loader-grey-line"></div>
                                        <div className="value-list-descripation loader-line-height loader-line-radius  loader-grey-line"></div>
                                    </div>
                          
                                </div>
                              
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      )
  }
  openSeeMore(){
      this.setState({
           expandView : !this.state.expandView,
      })
  }
  valuesListRender(culture){
      var displaySeeAll=fetchTotalSocialEngagementData
    if(document.getElementById('value-list')) {
        if(document.getElementById('value-list').clientHeight >= 191){
            displaySeeAll=true
        }else{
            displaySeeAll=false
        }
    }
      return(
        culture !== undefined && culture.cultureValueList.length>0?
          <div className = "value-containe-in-dashboard"> 
              <div className = "full-container"> 
                  <div className = "inner-value-containe-in-dashboard"> 
                     <div className = "clearfix"> 
                        <div className = "overview-value-details-description">  
                          <div className="value-title"> 
                            <span className = "bold-title"> Values </span> are the guiding principles that dictate behaviours within a company, they work as a compass for what’s right and wrong. Visibly uses game mechanics and AI to proactively encourage employees to tag their content with company core values.
                            <span className= "value-icon"> <i class="material-icons">favorite_border</i> </span>
                          </div>
                         </div>
                  
                        <div className = {`value-details-description  ${this.state.expandView == true ? 'expand-value-list' : ''}`}>  
                                <div className = "value-list cf" id='value-list'>
                                    <div class="value-column"> 
                                      {culture.cultureValueList.map((values, index)=>{
                                          if(index % 2 == 0 ){
                                          return(<div className = "list-value1" key={`valuelist`+index}>
                                            <div class="value-item-inner">
                                                <div className = "value-list-container" onClick={this.ValueListToggleEvent.bind(this,values.id)}> 
                                                     <div className = {`value-list-title ${selectedValueId == `description-${values.id}` ? 'active':''}`} id={values.id}> {values.title} 
                                                        <a className="valueAccordian ValueListArrow"><i class={`material-icons collapsible-icon down-arrow valueAccordian`} > keyboard_arrow_down</i></a>
                                                      </div>
                                                     <div className = {`value-list-description ${selectedValueId == `description-${values.id}` ? '':'hide'}`} id={`description-${values.id}`}> {values.description.substring(0,200)+"..."} </div>
                                                </div>
                                                <div className = "icon-container"> 
                                                   <div className = "icon-of-value"> 
                                                     <img src={values.value_image}/>
                                                   </div>
                                                </div>
                                            </div>
                                        </div>
                                        )}
                                      })}
                                      </div>
                                      <div class="value-column"> 
                                      {culture.cultureValueList.map((values, index)=>{
                                          if(index % 2 !== 0){
                                          return(<div className = "list-value1" key={index}>
                                            <div class="value-item-inner">
                                                <div className = "value-list-container" onClick={this.ValueListToggleEvent.bind(this,values.id)}> 
                                                     <div className = {`value-list-title ${selectedValueId == `description-${values.id}` ? 'active':''}`} id={values.id}> {values.title} 
                                                     <a className="valueAccordian ValueListArrow"><i class={`material-icons collapsible-icon down-arrow valueAccordian`} > keyboard_arrow_down</i></a>
                                                     </div>
                                                     <div className = {`value-list-description ${selectedValueId == `description-${values.id}` ? '':'hide'}`} id={`description-${values.id}`}> {values.description.substring(0,200)+"..."} </div>
                                                </div>
                                                <div className = "icon-container"> 
                                                   <div className = "icon-of-value"> 
                                                   <img src={values.value_image}/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        )}
                                      })}
                                      </div>
                                          </div>
                                          {/* {displaySeeAll==true ? <div className = "see-all-text-value" onClick = {this.openSeeMore.bind(this)}><a> {this.state.expandView == true ? 'See Less' : 'See All'} </a> </div>:''} */}

                        </div>
                </div>
            </div>
            </div>
            </div>
            :<div></div>
      )
  }
 
  render() {
    return(
        <div>{
            this.props.culture.cultureLoader == true ?
                  this.loader()
            : 
                  this.valuesListRender(this.props.culture)
          }
        </div>
    )
  }
}