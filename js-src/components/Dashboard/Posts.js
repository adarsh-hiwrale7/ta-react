import { Link } from "react-router";
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import Post from '../Post';

import * as postsActions from "../../actions/postsActions"

const styles = {
  container: {
    textAlign: 'center',
    paddingTop: 20,
  },
};

class Posts extends React.Component {

  constructor(props) {
    super(props);
    this.loadUnmoderatedPosts = this.loadUnmoderatedPosts.bind(this);
    this.loadModeratedPosts = this.loadModeratedPosts.bind(this);
  }


  componentDidMount(){
    this.loadModeratedPosts();
    this.loadUnmoderatedPosts();

  }

  
  renderPosts(posts) {
    return posts.map((post, index) => {

      return (
        <Post key={ index }
            fullName={ post.fullName } 
            date={ post.date } 
            title={ post.title }
            imgSrc={ post.imgSrc }
            comment={ post.comment }
            foo= { post.foo.one }
            footwo= { post.foo.two }
          >
          </Post>
      )
    })
  }

  loadUnmoderatedPosts() {
    this.props.postsActions.loadUnmoderatedPosts();
  }

  loadModeratedPosts() {
    this.props.postsActions.loadModeratedPosts();
  }

  render() {
    const { unmoderatedposts, moderatedposts } = this.props.posts;
    const slider = {
      styles: {
        paddingLeft: 30,
        paddingRight: 30
      }
    }

    return (
      <div  style={ slider.styles }>
            
            <div className='m-t-two'>
              { this.renderPosts(unmoderatedposts.posts) }
            </div>
            
            <div className='m-t-two'>
              { this.renderPosts(moderatedposts.posts) }
            </div>

      </div>
    );
  }
}


function mapStateToProps(state) {
  // If this functioned is defined, the component will subscribe to redux store updates.
  // Any time the store updates, mapStateToProps will be called

  // return only part of state which is relevant to this component.
  // this will expose that part of state to the props
  return {
    posts: state.posts
  }
}

function mapDispatchToProps(dispatch) {
  // defines actions we want on props of component
  // bindActionCreators is a redux function will remove the necessity of writing dispatch method and directly write actions on our props.  Refer the comment above connect() method
  return {
    postsActions: bindActionCreators(postsActions, dispatch)
  }
}

// calling connect on our component automatically adds dispatch prop to our component. We can use this dispatch prop to call our action creators like this.props.dispatch(actionCreator()). When we use bindActionCreators, we can call our action creators like this.props.actions.actionCreator(). This helps to keep our components simple
module.exports = connect(mapStateToProps, mapDispatchToProps) (Posts);