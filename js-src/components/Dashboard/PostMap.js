import Globals from '../../Globals';
import * as utils from '../../utils/utils';
var screenWidth = document.body.clientWidth;
var zoomValue= 2; 
export default class PostMap extends React.Component {
    constructor() {
        super();
        this.mapLibsReady = 0;
   }
    componentDidMount() {
        if(screenWidth > 1500){
            zoomValue = 3
        }
        else if(screenWidth<1500){
            zoomValue = 2
        }
        window.initMap = this.initMap;
        loadJS('https://maps.googleapis.com/maps/api/js?key=' + Globals.GOOGLE_MAP_KEY + '&callback=initMap');
        loadJS(Globals.WIDGET_URL + '/js/oms.min.js');
    }
       mapHTML(data) {
           
            var postData = '<div class="mainContainer"><div class="innercontainer clearfix">';

            if (data.thumbnail_url.length > 0) {
                if(data.media_type[0]!==undefined && data.media_type[0].includes("video")){
                    postData += `<div id="banner"><img src=${data.thumbnail_url[0]} onerror="this.src='https://app.visibly.io/img/default-avatar.png';" ></img><div id="playIconOverlay-pwKDJ" class="playIconOverlay overlay"><i class="material-icons">play_circle_outline</i></div></div>`; 
                }else{
                    postData += `<div id="banner"><img src=${data.thumbnail_url[0]} onerror="this.src='https://app.visibly.io/img/default-avatar.png';" ></img></div>`;
                } 
            }
            else {
                postData += '<div id="banner"></div>'
            }
            postData += '<div id="text-header">';
            if(data.detail ) {
                postData += '<div id="text">' + utils.convertUnicode(data.detail.split(" ").splice(0,Globals.LIMIT_WORD).join(" ")) + '...</div>';
            } 
            postData += '<div id="footer">';
            postData += '<div id="footer_textheader" class="map-channels"><div class="mapChannelTitle"><span>CHANNELS</span></div>'
            if(data.user_social.length > 0) {
                var proImage=''
                var Imagesrc=''
                var defaultImage='https://app.visibly.io/img/default-avatar.png'
                postData += '<ul class="channels">';
                for(let social = 0; social < data.user_social.length; social++) {   
                    //to fetch avatar from use_social
                    data.user_social[social].user_social.map((innerSocial,i)=>{
                        if(innerSocial.social_key=='avatar'){
                            proImage=innerSocial.social_value
                        }
                    })
                    Imagesrc=typeof proImage!=='undefined'?proImage:defaultImage
                    postData += `<li class="channel"><span data-tip="" data-for="" ><a class="tw share-tw">
                        <img src="${Imagesrc}" class="mapChannelMainImage" onerror="this.src='https://app.visibly.io/img/default-avatar.png';">
                        <img src="img/${data.user_social[social].social_accounts.social_name.toLowerCase()}.png" width="35px" class="mapSocialIcon"  align="left"/>
                        <span class="mapSocialIconWhiteRound">
                        </a> 
                        </span>
                        </li>` 
                }
                postData += '</ul>';
            }
            postData += '</div>';
            postData += '</div>';
            postData += '</div>';
            postData += '</div></div>';
            return postData;
        }
    initMap()  {
            if (++this.mapLibsReady < 2) 
                return;
            if (typeof google === 'object' && typeof google.maps === 'object') {
            var mapElement = document.getElementById('map');
            var map = new google
                .maps
                .Map(mapElement, {
                    zoom:zoomValue,
                    minZoom :zoomValue, 
                    scrollwheel: false
                });
            google.maps.event.addListener(map, 'zoom_changed', function () {
            if (map.getZoom() < zoomValue) map.setZoom(zoomValue);
            });
            var iw = new google
                .maps
                .InfoWindow();
                function iwClose() {
                    iw.close();
                }
                google
                    .maps
                    .event
                    .addListener(map, 'click', iwClose);           
            var oms = new OverlappingMarkerSpiderfier(map, {
                markersWontMove: true,
                markersWontHide: true
            });
        
            var me = this;

            
            var bounds = new google.maps.LatLngBounds();
            for (var i = 0, len = window.mapData.length; i < len; i++) {
                (function () { // make a closure over the marker and marker data
                    var markerData = window.mapData[i]; // e.g. { lat: 50.123, lng: 0.123, text: 'XYZ' }
                    var marker = new google
                        .maps
                        .Marker({
                            position: markerData,
                            //optimized: ! isIE  // makes SVG icons work in IE
                        });
                    bounds.extend( new google.maps.LatLng(markerData.lat, markerData.lng) );
                   
                    google
                        .maps
                        .event
                        .addListener(marker, 'click', iwClose);
                    oms.addMarker(marker, function (e) {
                        iw.setContent(me.mapHTML(markerData.data));
                        iw.open(map, marker);
                    });

                    /** For marker image */
                    var randMarker= typeof markerData.data.user_social !=='undefined'?
                         typeof markerData.data.user_social[Math.floor(Math.random() * markerData.data.user_social.length)] !=='undefined'?
                                       markerData.data.user_social[Math.floor(Math.random() * markerData.data.user_social.length)]
                                        :'':'';
                    if (typeof randMarker == 'undefined' || !randMarker ) {
                        randMarker = "map.png";
                    } else {
                        //randmark1 = randMarker.slice(1);
                        var randmark1 =randMarker.social_accounts? randMarker.social_accounts.social_name.toLowerCase():'';
                        randMarker = randmark1 + "-Pointer.png";
                    }

                    google.maps.event.addListener(marker, 'spider_format', function(status) {
                        marker.setIcon({
                            url: 'img/' + randMarker,
                            scaledSize: new google.maps.Size(50, 50)  // makes SVG icons work in IE
                        });
                        });
                   
                })();

            }
            map.setCenter(bounds.getCenter());

            //map.fitBounds(bounds);
            

            window.map = map;
            window.oms = oms;
            }
        }

        
    render() {
        let postMap = this.props.postMap;
        let postArr = Array();
                    for (let i = 0; i < postMap.length; i++) {
                        if(postMap[i].user_social.length>0) {
                            postArr.push({lng: postMap[i].longitude, lat: postMap[i].latitude, data: postMap[i]});
                        }
                        
                    }
                    window.mapData = postArr;
                    this.initMap();
        return (
            <div className='livemap-wrapper'>
            <div className="livemap" ref="map" id="map">Map</div>
            </div>
        );
    }
}

function loadJS(src) {
    var allscripts=[]
    var srcList=[]
    allscripts=window.document.getElementsByTagName("script")
    for (var key in allscripts) {
        if (allscripts.hasOwnProperty(key)) {
            //store all src in array
            if(allscripts[key].src){
                srcList.push(allscripts[key].src)
            }
        }
    }
    if(srcList.indexOf(src)<=-1){
        //if map api is not loaded 
        var ref = window.document.getElementsByTagName("script")[0];
        var script = window.document.createElement("script");
        script.src = src;
        script.async = false;
        ref.parentNode.insertBefore(script, ref);
    }
    
}