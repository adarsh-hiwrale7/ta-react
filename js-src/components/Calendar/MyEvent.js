
import * as utils from '../../utils/utils'
var campaignIdArray = [];
class MyEvent extends React.Component {
  constructor (props) {
    super(props)
  }
  componentDidMount(){
    var campaignId = 'campaign-'+this.props.event.campaign.identity;
    if(campaignIdArray.indexOf(campaignId)==-1){
      campaignIdArray.push(campaignId)
    }
    var calendarEvent = document.querySelectorAll('.calendar-event-wrapper');
    var calendarEventCount = 0;
    for (calendarEventCount=0; calendarEventCount<calendarEvent.length; calendarEventCount++){
      var campaignIdArrayCount=0;
      for(campaignIdArrayCount=0; campaignIdArrayCount<campaignIdArray.length; campaignIdArrayCount++){
        
        if(campaignIdArray[campaignIdArrayCount]==calendarEvent[calendarEventCount].classList[1])
        {
          calendarEvent[calendarEventCount].parentElement.parentElement.classList.add(`row-${campaignIdArrayCount}`)
          const id = campaignIdArray[campaignIdArrayCount].replace(/(campaign-)/g, '')
          if(this.props.event.campaign.identity ==  id && this.props.event.callFrom == 'event'){
            calendarEvent[calendarEventCount].parentElement.parentElement.classList.add('campaign-event')
          }
        }
      }
    }
  }
  
  componentWillReceiveProps(nextProps){
    if(nextProps!==this.props){
        var campaignId = 'campaign-'+nextProps.event.campaign.identity;
        if(campaignIdArray.indexOf(campaignId)==-1){
          campaignIdArray.push(campaignId)
        }
        var calendarEvent = document.querySelectorAll('.calendar-event-wrapper');
        var calendarEventCount = 0;
        for (calendarEventCount=0; calendarEventCount<calendarEvent.length; calendarEventCount++){
          var campaignIdArrayCount=0;
          for(campaignIdArrayCount=0; campaignIdArrayCount<campaignIdArray.length; campaignIdArrayCount++){
            if(campaignIdArray[campaignIdArrayCount]==calendarEvent[calendarEventCount].classList[1])
            {
              calendarEvent[calendarEventCount].parentElement.parentElement.classList.add(`row-${campaignIdArrayCount}`)
              const id = campaignIdArray[campaignIdArrayCount].replace(/(campaign-)/g, '')
              if(nextProps.event.campaign.identity ==  id && nextProps.event.callFrom == 'event'){
                calendarEvent[calendarEventCount].parentElement.parentElement.classList.add('campaign-event')
              }
            }
          }
        }
    }
  }
  render () {
    return (
      <div className={`calendar-event-wrapper campaign-${this.props.event.campaign.identity}`}>
        <div
          className='calendar_event_content clearfix'
          data-toggle='popover'
          data-color = {`${this.props.event.callFrom !== undefined && this.props.event.callFrom == 'event' ? 'eventView':''}`}
          data-placement='top'
          data-popover-content={'#custom_event_' + this.props.event.id}
          tabIndex='0'
          id='btnCampaignDetail'
          data-isbtndetail="true"
        >
          <div className="calendar-event-title"><span class="info-title">{this.props.event.title?utils.convertUnicode(this.props.event.title):''}</span>
          {this.props.event.callFrom !== undefined && this.props.event.callFrom == 'event'? <span class="eventIcon"><svg 
                                xmlns="http://www.w3.org/2000/svg" 
                                height="13" 
                                viewBox="0 0 24 24" 
                                width="13">
                                    <path d="M0 0h24v24H0V0z" fill="none"/>
                                    <path class="event-svg" d="M18 9l-1.41-1.42L10 14.17l-2.59-2.58L6 13l4 4zm1-6h-4.18C14.4 1.84 13.3 1 12 1c-1.3 0-2.4.84-2.82 2H5c-.14 0-.27.01-.4.04-.39.08-.74.28-1.01.55-.18.18-.33.4-.43.64-.1.23-.16.49-.16.77v14c0 .27.06.54.16.78s.25.45.43.64c.27.27.62.47 1.01.55.13.02.26.03.4.03h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zm-7-.25c.41 0 .75.34.75.75s-.34.75-.75.75-.75-.34-.75-.75.34-.75.75-.75zM19 19H5V5h14v14z"/></svg></span> : <span class="infoIcon"> <svg xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24"><path d="M0 0h24v24H0V0z" fill="none"/><path class="info-svg" d="M11 7h2v2h-2zm0 4h2v6h-2zm1-9C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm0 18c-4.41 0-8-3.59-8-8s3.59-8 8-8 8 3.59 8 8-3.59 8-8 8z"/></svg></span>}
          </div>
          {/* {this.props.event.callFrom !== undefined && this.props.event.callFrom == 'event' ?
          ''
          :
          <div className="campaign-count-wrapper">
            <span className='campaign-count'>
              <i class="material-icons">filter</i>
                {this.props.event.campaign.post_count}
            </span>
            <span className='campaign-count'>
              <i class="material-icons">folder_open</i>
                {this.props.event.campaign.asset_count}
            </span>
            <span className='campaign-count'>
              <i class="material-icons">mail_outline</i>
                {this.props.event.campaign.email_count}
            </span>
            <span className='campaign-count'>
              <svg xmlns="http://www.w3.org/2000/svg" height="13" viewBox="0 0 24 24" width="13"><path d="M0 0h24v24H0V0z" fill="none"/><path d="M20 2H4c-1.1 0-2 .9-2 2v18l4-4h14c1.1 0 2-.9 2-2V4c0-1.1-.9-2-2-2zm0 14H6l-2 2V4h16v12zM7 9h2v2H7zm4 0h2v2h-2zm4 0h2v2h-2z"/></svg>
                {this.props.event.campaign.sms_count}
            </span>
            <span className='campaign-count'>
              <i class="material-icons">text_fields</i>
                {this.props.event.campaign.print_count}
            </span>
            <span className='campaign-count'>
              <svg xmlns="http://www.w3.org/2000/svg" height="13" viewBox="0 0 24 24" width="13"><path d="M0 0h24v24H0V0z" fill="none"/><path d="M18 9l-1.41-1.42L10 14.17l-2.59-2.58L6 13l4 4zm1-6h-4.18C14.4 1.84 13.3 1 12 1c-1.3 0-2.4.84-2.82 2H5c-.14 0-.27.01-.4.04-.39.08-.74.28-1.01.55-.18.18-.33.4-.43.64-.1.23-.16.49-.16.77v14c0 .27.06.54.16.78s.25.45.43.64c.27.27.62.47 1.01.55.13.02.26.03.4.03h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zm-7-.25c.41 0 .75.34.75.75s-.34.75-.75.75-.75-.34-.75-.75.34-.75.75-.75zM19 19H5V5h14v14z"/></svg>
                {this.props.event.campaign.task_count}
            </span>
          </div>} */}
        </div>

        <div className='hidden' id={'custom_event_' + this.props.event.id}>
          <div className='popover-heading'>
            {this.props.event.driver}
          </div>

          <div className='popover-body'>
            {this.props.event.title?utils.convertUnicode(this.props.event.title):''}<br />
          </div>
        </div>
      </div>
      
    )
  }
}
export default MyEvent
