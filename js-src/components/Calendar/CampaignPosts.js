import { bindActionCreators } from 'redux';
import { browserHistory } from 'react-router';
import { connect } from 'react-redux';
import { cloneArr } from '../../utils/utils';

import * as s3functions from '../../utils/s3';
import * as utils from '../../utils/utils';
import * as campaignsActions from '../../actions/campaigns/campaignsActions';
import ScheduledCalender from '../Campaigns/ScheduledCalender';

import Collapse, { Panel } from 'rc-collapse';
import PopupWrapper from '../PopupWrapper';
import Globals from '../../Globals';
import moment from '../Chunks/ChunkMoment';
import notie from 'notie/dist/notie.js';
import PreLoader from '../PreLoader';
import Post from '../Feed/Post';

class CampaignPosts extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      folders: [], // Although, folders are stored in redux state, we need to convert the
      loading: false,
      posts: [],
      moment: null,
      calendarPosts:[]
    }
  }

  componentDidMount () {}
  componentWillMount() {
    var me = this
    var campaignId=me.props.campaign.identity;
    moment().then(moment => {
      me.setState({ moment: moment },()=>{
        const callFromPlanner = true
        me.props.campaignsActions.fetchPlannerPosts(campaignId,moment,null,null);
        me.props.campaignsActions.fetchPlannerPrints(campaignId,moment);
        me.props.campaignsActions.fetchTasks(campaignId,null,this.props.roleName,this.props.profile.identity,null,null,callFromPlanner,moment);  
        // me.props.campaignsActions.fetchPlannerProjects(campaignId,moment);
        // me.props.campaignsActions.fetchCampaignPosts(campaignId,"calendar",moment);
      })
    })
  }
  
  renderLoading () {
    return (
      <div className='preloader-wrap'>
        <PreLoader />
      </div>
    )
  }
  fetchPostsData(campID,currView){
    this.props.campaignsActions.fetchCampaignPosts(campID,currView,this.state.moment); 
  }

  render () {
        
    const { loading } = this.props.campaigns.creation;
    const campaign = this.props.campaign;
    var me = this;
    var tags = campaign.Tags.data ? campaign.Tags.data : [];
    let posts = this.state.posts ? this.state.posts : [];
    let campaignDetailPopupScrollbar = {
      maxHeight: `calc(100vh - 57px)`
    }
    return (
      <div className=''>
        <div id='campaign-posts'>
            {this.props.campaigns.postLoading ? this.renderLoading() : 
            <div className="scheduledPost">
              <ScheduledCalender
              handleChangeStatus={this.props.handleChangeStatus}
              campaign={this.props.campaign}
              profile={this.props.profile}
              location ={this.props.location}
              moment={this.state.moment}
              fetchPostsData ={this.fetchPostsData.bind(this)}
              {...this.props}
              roleName={this.props.roleName}
              handleEditPlanner={this.props.handleEditPlanner}
              campaignAllUserList={this.props.campaignAllUserList}
              />
            </div> 
            }
          </div>
      </div>
    )
  }
}

function mapStateToProps (state) {
  return {
    campaigns: state.campaigns
  }
}

function mapDispatchToProps (dispatch) {
  return {
    campaignsActions: bindActionCreators(campaignsActions, dispatch)
  }
}

let connection = connect(mapStateToProps, mapDispatchToProps)

module.exports = connection(CampaignPosts)
