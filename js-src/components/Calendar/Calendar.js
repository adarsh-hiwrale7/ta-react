import CampaignNav from '../Campaigns/CampaignNav';

import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import moment from '../Chunks/ChunkMoment'
import bigCalendar from '../Chunks/ChunkBigCalendar'
import SidebarNarrow from '../Layout/Sidebar/SidebarNarrow'
import { Link } from 'react-router'
import Globals from '../../Globals'
import Header from '../Header/Header';
import * as utils from '../../utils/utils'
import * as campaignsActions from '../../actions/campaigns/campaignsActions'
import PreLoader from '../PreLoader'
import SliderPopup from '../SliderPopup'
import CreateCampaign from '../Campaigns/CreateCampaign'
import CampaignViewTabs from '../Campaigns/CampaignViewTabs'
import UpdateCampaign from '../Campaigns/UpdateCampaign'

import CampaignPosts from './CampaignPosts'
import MyEvent from './MyEvent'

// collapse
import Collapse, { Panel } from 'rc-collapse'

class Calendar extends React.Component {
  constructor (props, context) {
    super(props, context)

    this.context = context
    this.state = {
      bigCalendar: null,
      moment: null,
      events: [],
      filter: 'all',
      currentCampaign: null,
      currentFilter: 'all',
      newCampaignPopup: false,
      updateCampaignPopup: false,
      detailCampaignPopup: false,
      clickListenerInfo: null
    }

    this.getClickListenerInfo = this.getClickListenerInfo.bind(this)
  }
  componentWillMount () {
    var newLocation = utils
      .removeTrailingSlash(this.props.location.pathname)
      .split('/')
    var location = newLocation[3] == undefined ? 'live' : newLocation[3]
    this.setHeader(location)

    document.addEventListener('click', this.getClickListenerInfo)

    moment().then(moment => {
      bigCalendar().then(bigCalendar => {
        this.setState({
          moment: moment,
          bigCalendar: bigCalendar
        })
        this.props.campaignsActions.fetchCampaigns(
          location,
          true,
          this.state.moment
        )
      })
    })
  }

  componentWillUnmount () {
    document.removeEventListener('click', this.getClickListenerInfo)
  }

  handleCampaignCreation () {
    this.setState({
      newCampaignPopup: true
    })

    this.props.campaignsActions.creatingNewCampaign()
    document.body.classList.add('overlay')
  }
  setHeader (location) {
    this.setState({
      currentFilter: location,
      
    })
  }

  closePopup (popup) {
    switch (popup) {
      case 'new':
        this.setState({
          newCampaignPopup: false
        })
        break
      case 'update':
        this.setState({
          updateCampaignPopup: false
        })
        break
      case 'posts':
        this.setState({
          CampaignPostsPopup: false
        })
        break
      case 'detail':
        this.setState({
          detailCampaignPopup: false
        })
        break
    }

    document.body.classList.remove('overlay')
  }
  componentWillReceiveProps (newProps) {
    var newLocation = utils
      .removeTrailingSlash(newProps.location.pathname)
      .split('/')
    if (newProps.location.pathname !== this.props.location.pathname) {
      var location = newLocation[3] == undefined ? 'live' : newLocation[3]
      this.setHeader(location)
      this.props.campaignsActions.fetchCampaigns(
        location,
        true,
        this.state.moment
      )
    }
  }

  getClickListenerInfo (e) {
    var me = this
    if (e.button == 0) {
      me.setState({
        clickListenerInfo: e
      })
    }
  }

  callCampaignDetails (event, e) {
    let roleName =JSON.parse(localStorage.getItem('roles')) !== null? JSON.parse(localStorage.getItem('roles'))[0].role_name:''
		let notShow = 0;
		if(roleName === "employee" || roleName === "moderator") {
      return;
    }
    if (e.target.dataset.btncampaignpost == 'btnCampaignPost') {
      this.setState({
        currentCampaign: event.campaign,
        CampaignPostsPopup: true
      })
    } else if (e.target.dataset.isbtndetail == 'true') {
      // reset all flags which were set while creating campaigns
      this.props.campaignsActions.viewingCampaign();
      this.setState({
        currentCampaign: event.campaign,
        detailCampaignPopup: true
      })
    }
    document.body.classList.add('overlay')
  }
  callCreateEvent (event)
  {
  }
  callCampaignPosts (event) {
    this.setState({
      currentCampaign: event.campaign,
      CampaignPostsPopup: true
    })
    document.body.classList.add('overlay')
  }
  campaignUpdateClick (campaign) {
    this.setState({
      updateCampaignPopup: true,
      currentCampaign: campaign
    })
    this.props.campaignsActions.creatingNewCampaign()

    document.body.classList.add('overlay')
  }
  renderBreadcrumbs () {
    // Display additional items depending on which page we are
    return <h1>Calendar</h1>
  }

  renderCampaignPostsPopup () {
    return (
      <SliderPopup wide={true}>
        <button
          id='close-popup'
          className='btn-default'
          onClick={this.closePopup.bind(this, 'posts')}
        >
          <i className="material-icons">clear</i>
        </button>
        <CampaignPosts
          campaign={this.state.currentCampaign}
          Moment={this.state.moment}
          location={this.props.location} 
        />

      </SliderPopup>
    )
  }
  renderCampaignUpdate () {
    return (
      <SliderPopup wide={true}>
            <button
              id='close-popup'
              className='btn-default'
              onClick={this.closePopup.bind(this, 'update')}
            >
              <i className="material-icons">clear</i>
            </button>
            <UpdateCampaign
              campaign={this.state.currentCampaign}
              Moment={this.state.moment}
              location={this.props.location}
            />
       </SliderPopup>
    )
  }
  renderCampaignDetails () {
    return (
      <SliderPopup wide={true}>
            <button
              id='close-popup'
              className='btn-default'
              onClick={this.closePopup.bind(this, 'detail')}
            >
              <i className="material-icons">clear</i>
            </button>
            <CampaignViewTabs
              campaign={this.state.currentCampaign}
              Moment={this.state.moment}
              location={this.props.location}
              onEditClick={this.campaignUpdateClick.bind(this)}
              onClose={this.closePopup.bind(this, 'detail')}
            />
      </SliderPopup>
    )
  }

  renderLoading () {
    return (
      <div className='preloader-wrap'>
        <PreLoader />
      </div>
    )
  }

  renderCampaignCreation () {
    return (
      <SliderPopup wide={true}>
            <button
              id='close-popup'
              className='btn-default'
              onClick={this.closePopup.bind(this, 'new')}
            >
              <i className="material-icons">clear</i>
            </button>
            <CreateCampaign
              location={this.props.location}
              campaign={this.state.currentCampaign}
              Moment={this.state.moment}
            />
      </SliderPopup>
    )
  }

  render() {
    var BigCalendar = this.state.bigCalendar
    var moment = this.state.moment
    var me = this
    let { listLoading } = this.props.campaigns
    let events = typeof this.props.campaigns.events !== 'undefined'
      ? this.props.campaigns.events
      : []

    if (moment !== null) {
      BigCalendar.setLocalizer(BigCalendar.momentLocalizer(moment))
    }

    let roleName =JSON.parse(localStorage.getItem('roles')) !== null? JSON.parse(localStorage.getItem('roles'))[0].role_name:''
		let notShow = 0;
		if(roleName === "employee" || roleName === "moderator") {
			notShow = 1;
    }
    return (
      <section id='calendar' className='calendar'>

        {this.state.CampaignPostsPopup
          ? this.renderCampaignPostsPopup()
          : <div />}
        {this.state.newCampaignPopup ? this.renderCampaignCreation() : <div />}
        {this.state.updateCampaignPopup ? this.renderCampaignUpdate() : <div />}
        {this.state.detailCampaignPopup
          ? this.renderCampaignDetails()
          : <div />}

<Header 
          add_new_subpopup ="CAMPAIGNS"
          title="CAMPAIGNS" 
          nav={CampaignNav} 
          popup_text="Click here to add new Campaign." 
          add_new_button={true}
        />

        <div id='content'>

          <div className='page campaign-container'>

            {listLoading ? this.renderLoading() : <div />}
            {(this.props.pathname!==undefined)?(!this.props.location.pathname.includes("search"))?
            <div className='esc'>

              <div class='content'>
                <div className='widget'>

                  <div className='section calendar-nav-btns'>
                    <Link
                      to={'/campaigns/calendar/' + this.state.currentFilter}
                      activeClassName='btn-active'
                      className='btn btn-primary'
                    >
                      <i class="material-icons" title='Calendar View'>today</i>

                    </Link>
                    <Link
                      to={'/campaigns/' + this.state.currentFilter}
                      activeClassName='btn-active'
                      className='btn btn-primary'
                    >
                      <i
                        className='material-icons'
                        aria-hidden='true'
                        title='List View'
                      >view_headline</i>
                    </Link>

                    <div class='row'>
                      <div class='col'>
                        <div className='bigCalendar-calendarview'>
                          {!moment
                            ? ''
                            : <BigCalendar
                              {...this.props}
                              selectable
                              events={
                                  typeof this.state.moment !== 'undefined' &&
                                    this.state.moment != null
                                    ? events
                                    : []
                                }
                              defaultDate={new Date()}
                              onSelectSlot={event =>
                                this.callCreateEvent(
                                  event,
                                  me.state.clickListenerInfo
                                )
                              }
                              onSelectEvent={event =>
                                  this.callCampaignDetails(
                                    event,
                                    me.state.clickListenerInfo
                                  )}
                              components={{
                                event: MyEvent
                              }}
                              views={['month']}
                              />}
                        </div>
                      </div>
                    </div>{/* --end row-- */}

                  </div>{/* --end section-- */}
                </div>{/* --end widget-- */}
              </div>{/* --end content-- */}
            </div>
            :"":""}{/* --end esc-- */}
          </div>{/* --end page-- */}
        </div>
      </section>
    )
  }
}

function mapStateToProps (state) {
  return {
    campaigns: state.campaigns
  }
}

function mapDispatchToProps (dispatch) {
  return {
    campaignsActions: bindActionCreators(campaignsActions, dispatch)
  }
}

let connection = connect(mapStateToProps, mapDispatchToProps)

module.exports = connection(Calendar)
