class FilterWrapper extends React.Component {
    render() {
        return (
            <div id="dropdown-container" className={this.props.className}>
              <div className="dropdown-container clearfix">
              <div className="dropdown-container-inner">
                <div className='dropdown-inner-wrapper'>
                    {this.props.children}
              </div>
              <div className='top-shadow-wrapper'>
              <div class="top-shadow"></div>
              </div>
              </div>
           </div>
         </div> 
        )
    }
}
module.exports = FilterWrapper