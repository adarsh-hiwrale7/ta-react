class HeaderPageTitle extends React.Component {
	
	render() {
		
		let pageTitle = this.props.pageTitle;
		let subTitle = this.props.subTitle;
		let dateCreated = this.props.dateCreated;
		let dateUpdated = this.props.dateUpdated;

		return (
			<div className="content-header">
		        <div className="content-header-left">
				    <h1 className="title heading--title">{ pageTitle }</h1>
				    <h2 className="subtitle heading--subtitle">{ subTitle }</h2>
				  </div>
				  <div className="content-header-right">
				    <p>{ dateCreated }</p>
				    <p>{ dateUpdated }</p>
				</div>
		    </div>
		);
	}
}

export default HeaderPageTitle