import { Link } from 'react-router'
export default class GuestUserRestrictionPopup extends React.Component {
componentDidMount(){

}
	render() {
        let roleName =JSON.parse(localStorage.getItem('roles')) !== null? JSON.parse(localStorage.getItem('roles'))[0].role_name:''
        if(this.props.setOverlay !== undefined && this.props.setOverlay==true && roleName == 'guest' ){
            document.body.classList.add('guest-user-overlay');
        }else{
            if(document.body.classList.length>0){
                document.body.classList.remove('guest-user-overlay');
            }
        }
		return (
            this.props.setOverlay !== undefined && this.props.setOverlay==true && roleName == 'guest' ?
                <div>
                   <div className='overlay-page'>
                    <div className="guestUserOverlayText">
                        <h1> Unavailable</h1>
                        <p>Please start your own Visibly account to use these features, its free!</p>
                        <a
                            target='_blank'
                            href='https://visibly.io/request-a-demo-by-mail/'
                            className='btn btn-primary'
                        >Signup
                        </a>
                    </div>
                    </div>
                </div> : <div></div>
		)
	}
}