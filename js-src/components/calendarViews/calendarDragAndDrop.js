import { DragDropContext } from 'react-dnd'

import * as utils from '../../utils/utils'

import BigCalendar from 'react-big-calendar';
import events from '../Feed/events';
import HTML5Backend from 'react-dnd-html5-backend';
import moment from '../Chunks/ChunkMoment';

import ReactDOM from 'react-dom';
import withDragAndDrop from 'react-big-calendar/lib/addons/dragAndDrop';
import Globals from '../../Globals';

var firstDate, lastDate, firstDateTimestamp, lastDateTimestamp
const DragAndDropCalendar = withDragAndDrop(BigCalendar)
var currentView = 'month';
class Dnd extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      events: events,
      currentView: 'month',
      moment: null,
    }
    this.move_event = this.move_event.bind(this);
  }
  componentWillMount() {
    var me = this
    moment().then(moment => {
      me.setState({ moment: moment})
    })
  }
  // Get first date and last date of calendar
  getDateRange() {
    let _this = this;
    setTimeout(function () {
      let dateCells,
        firstRage,
        LastRange,
        showedFirstRange,
        showedLastRage;
      if (currentView == 'month') {
        firstRage = 3,
          LastRange = 11,
          showedFirstRange = 0,
          showedLastRage = 2,
          dateCells = document.getElementsByClassName('rbc-date-cell')
      }
      else {
        firstRage = 9,
          LastRange = 18,
          showedFirstRange = 0,
          showedLastRage = 9;
          if(document.getElementById('weekHeader')){
            document.getElementById('weekHeader').innerHTML = document.querySelectorAll('.rbc-time-header .rbc-row')[0].innerHTML;
          }
        dateCells = document.querySelectorAll('#weekHeader .rbc-header')
      }
      if (dateCells && dateCells.length > 0 && _this.state.moment !== null) {

        firstDate = dateCells[0].innerText.slice(firstRage, LastRange),
        lastDate = dateCells[dateCells.length - 1].innerText.slice(firstRage, LastRange)
        var firstdateNext = _this.state.moment(firstDate).add(1, 'days');
        var lastdateNext = _this.state.moment(lastDate).add(1, 'days');
        firstDate = new Date(firstdateNext._d);
        lastDate = new Date(lastdateNext._d);
         firstDateTimestamp = _this.state.moment.tz(firstDate, Globals.SERVER_TZ).format('X');
         lastDateTimestamp = _this.state.moment.tz(lastDate, Globals.SERVER_TZ).format('X');
        for (var i = 0; i < dateCells.length; i++) {
          var dateText = dateCells[i].getElementsByTagName('a')[0].innerText;
          dateCells[i].getElementsByTagName('a')[0].innerHTML = dateText.slice(showedFirstRange, showedLastRage)
          dateCells[i].classList.add('loaded')
        }
        
        if(_this.props.fetchTimestamps){
          _this.props.fetchTimestamps(firstDateTimestamp,lastDateTimestamp)
        }
      }
    }, 20)
  }
  componentWillReceiveProps(newProps){
    this.setState({
     events : newProps.events
   })
  }
  componentDidMount() {
    currentView="month"
    this.setState({
      events:this.props.events
    })
    this.getDateRange(this)
  }
  
  componentWillUnmount() {
    ReactDOM
      .findDOMNode(this)
      .removeEventListener('click', this._handleCloseEvent);
  }
  move_event({ event, start, end }) {
    this.props.move_event({ event, start, end });
  }
  resizeEvent = (resizeType, { event, start, end }) => {
    const { events } = this.state

    const nextEvents = events.map(existingEvent => {
      return existingEvent.id == event.id
        ? { ...existingEvent, start, end }
        : existingEvent
    })

    this.setState({
      events: nextEvents,
    })
    this.getDateRange(this)
    alert(`${event.title} was resized to ${start}-${end}`)
  }
  ViewChanged(view) {
    currentView = view
    this.setState({
      currentView: view
    })
    this.getDateRange(this)
    this.props.setView(view)
  }
  navigate() {
    this.getDateRange(this)
  }
  setDates() {
    this.getDateRange(this)
  }
  render() {
    //  this.getDateRange(this)
    let formats = {
      dateFormat: 'DD MM/DD/YY',
      dayFormat: 'ddd DD/MM MM/DD/YY'
    }
    return (
      <div ref='big_calendar'>
        {
          this.state.currentView == 'week' ?
            <div id="weekHeader" />
            : ''
        }
        <div className='initializeStyle hide'>click</div>
        <DragAndDropCalendar
          formats={formats}
          popup
          selectable
          events={this.state.events}
          onEventDrop={this.move_event}
          resizable
          onEventResize={this.resizeEvent.bind(this)}
          onNavigate={this.navigate.bind(this)}
          onView={this.ViewChanged.bind(this)}
          components = {this.props.components}
          defaultView="month"
          //   defaultDate={new Date(2015, 3, 12)}
          onSelectEvent={(event, e) =>
            this.props.callCalPostDetails(
              event,
              e
            )}
        />
        <div className='setDates hide' onClick={this.setDates.bind(this)}>setDate</div>
      </div>
    )
  }
}

export default DragDropContext(HTML5Backend)(Dnd)
