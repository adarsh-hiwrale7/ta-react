;
import PropTypes from "prop-types";
import FixedBox from "./FixedBox";

const DataGrid = ({
  xLabels,
  yLabels,
  data,
  xLabelWidth,
  yLabelWidth,
  background,
  height,
  yLabelTextAlign,
  unit,
  displayYLabels,
  onClick,
  cursor,
  squares,
  cellRender,
  cellStyle,
  colorArray,
  moment
}) => {
  
  
 const tempflatArray = data.reduce((i, o) => [...o, ...i], []);
 const flatArray  = tempflatArray.filter(function( element ) {
  return element !== null;
});
  const max = 10;
  const min = 0;
  if(yLabels.length > 0){
    yLabels.map((data, index) => {
      let key = Object.keys(data)[0]
    })  
  }
  return (

    <div>
      {yLabels.map((y, yi) => {

              let key = Object.keys(y)[0]
              let operation = '='
              let showLabel = ''
              let newValue = [];
              if(moment !== null){
                if(y[key]['data']['name'] == 'start_date'){
                  y[key]['data']['value'].map((yValue,index)=>{
                    newValue.push(moment.unix(yValue).format('DD/MM/YYYY'))
                  })   
                }else{
                  newValue = y[key]['data']['value'];
                }
              }
              if(y[key]['data'].selectedStatus=="None"){
                showLabel = '!= ' +newValue.join(', ')
              }else{
                showLabel = newValue.join(', ')
              }

              if(y[key]['data']['operation'] == 'lt'){
                if(y[key]['data'].selectedStatus=="None"){
                  showLabel = '!< '+ newValue;
                }else{
                  showLabel = '< '+ newValue;
                }
              }else if(y[key]['data']['operation'] == 'gt'){
                if(y[key]['data'].selectedStatus=="None"){
                  showLabel = '!> '+ newValue;
                }else{
                  showLabel = '> '+ newValue;
                }
              }else if(y[key]['data']['operation'] == 'IB'){

                if(y[key]['data'].selectedStatus=="None"){
                  showLabel = '!= ' + newValue[0] + ' - ' + newValue[1];
                }else{
                  showLabel =  newValue[0] + ' - ' + newValue[1];
                }
              }

              if(y[key]['data']['name']=='business'){
                y[key]['data']['name']='Business Unit'
              }
              return(<div key={yi} style={{ display: "flex" }}>
                <FixedBox width={yLabelWidth}>
                  <div className="hmsdbrItm">
                  <div className={y[key]['data']['heirarchy']['data'].length > 0 ? 'keyRow hasChild' : 'keyRow'}>
                    <div className="hrvlwrpr">
                      <div className="operationName">{showLabel}</div>
                    </div>
                    <div className="keyNameUserCntWrpr clearfix">
                      <div className="keyName">{displayYLabels && y[key]['data']['name']}</div>
                      <div className="userCount"><i class="material-icons">people</i>{y[key]['data'].user_count}</div>
                    </div>
                    
                  </div>
                  {y[key]['data']['heirarchy']['data'].length > 0 ?
                    <ul className="heirarchyList">
                    
                        
                     {y[key]['data']['heirarchy']['data'].map((heirarchyData,index)=>{
                            let newValue = [];
                            if(moment !== null){
                              if(heirarchyData['name'] == 'start_date'){
                                heirarchyData['value'].map((yValue,index)=>{
                                  newValue.push(moment.unix(yValue).format('DD/MM/YYYY'))
                                })   
                              }else{
                                newValue = heirarchyData['value'];
                              }
                            }

                            if(heirarchyData['name']=='business'){
                              heirarchyData['name']='Business Unit'
                            }
                            let showLabelHeirarchy = '';
                            showLabelHeirarchy = heirarchyData['value'].join(', ')
                            let heirarchyOperation = '='
                            if(heirarchyData['operation'] == 'lt'){
                               heirarchyOperation = '<'
                               showLabelHeirarchy = '< '+ newValue;
                            }else if(heirarchyData['operation'] == 'gt'){
                               heirarchyOperation = '>'
                               showLabelHeirarchy = '< '+ newValue;
                            }else if(heirarchyData['operation'] == 'IB'){
                               heirarchyOperation = 'Between'
                               showLabelHeirarchy = newValue[0] + '-' + heirarchyData[1];
                            }

                            return(
                                <li key={index}>
                                  <div className="hrvlwrpr">
                                    <div className="operationName">{showLabelHeirarchy}</div>
                                  </div>
                                  <div className="keyNameUserCntWrpr clearfix">
                                    <div className="keyName">{heirarchyData['name']}</div>
                                    {/* <div className="userCount"><i class="material-icons">people</i>{heirarchyData.user_count}</div> */}
                                  </div>
                                </li>
                            )
                        })
                      }
                    
                    </ul>
                    :''}
                  </div>
                </FixedBox>
                {xLabels.map((x, xi) => {
                  
                  var value = data[yi][xi];
                  var background='';

                  if(value==null){
                    var colorCode = colorArray[2].code;
                    background=`rgb(`+colorCode[0]+`,`+ colorCode[1]+`,`+colorCode[2]+`,1)`
                    value = "-"
                  }
                  else if(value<5.0){
                    var colorCode = colorArray[0].code;
                    background=`rgb(`+colorCode[0]+`,`+ colorCode[1]+`,`+colorCode[2]+`, ${(max - value) / (max - min)})`
                  }else{
                    var colorCode =  colorArray[1].code;
                     background=`rgb(`+colorCode[0]+`,`+ colorCode[1]+`,`+colorCode[2]+`, ${1 - (max - value) / (max - min)})`
                  }
                  const style = Object.assign(
                    {
                   
                    },
                    {background:background,
                     fontSize:"14px"
                    }
                  );
                  return (
                    <div
                      onClick={onClick.bind(this, xi, yi)}
                      title={(value || value === 0) && `${value} ${unit}`}
                      key={`${xi}_${yi}`}
                      style={style}
                      className="hmclm"
                    >
                      <div>
                        {cellRender(value, x, y[key]['data']['name'])}
                      </div>
                    </div>
                  );
                })}
              </div>)
      })}
    </div>
  );
};

DataGrid.propTypes = {
  xLabels: PropTypes.arrayOf(
    PropTypes.oneOfType([PropTypes.string, PropTypes.number, PropTypes.object])
  ).isRequired,
  yLabels: PropTypes.arrayOf(
    PropTypes.oneOfType([PropTypes.string, PropTypes.number, PropTypes.object])
  ).isRequired,
  data: PropTypes.arrayOf(PropTypes.array).isRequired,
  height: PropTypes.number.isRequired,
  xLabelWidth: PropTypes.number.isRequired,
  yLabelWidth: PropTypes.number.isRequired,
  yLabelTextAlign: PropTypes.string.isRequired,
  unit: PropTypes.string.isRequired,
  displayYLabels: PropTypes.bool,
  onClick: PropTypes.func,
  cursor: PropTypes.string,
  squares: PropTypes.bool,
  cellRender: PropTypes.func.isRequired,
};

DataGrid.defaultProps = {
  displayYLabels: true,
  cursor: "",
  onClick: () => {},
  squares: false,
};

export default DataGrid;