
import HeatMap from './HeatMap';


// Display only even labels
const xLabelsVisibility = new Array(24)
  .fill(0)
  .map((_, i) => (i % 2 === 0 ? true : false));

class HeatMapDemo extends React.Component {
    constructor (props){
        super(props)
        this.state={
            data :[]
        }
    }

    
    render(){
        let xLabels = [];
        let yLabels = [];
        let data = [];

        if(this.props.allData.length > 0){
          if(this.props.allData[0].xlabel.length > 0){
            xLabels = this.props.allData[0].xlabel
          }
          if(this.props.allData[0].ylabel.length > 0){
            yLabels = this.props.allData[0].ylabel
          }
          if(this.props.allData[0].xlabel.length > 0){
            data = this.props.allData[0].data
          }

        }
        return(
            <div className='heatmapChart parent'>
                <HeatMap 
                     xLabels={xLabels}
                     yLabels={yLabels}
                     xLabelsLocation={"top"}
                     xLabelsVisibility={xLabelsVisibility}
                     xLabelWidth={50}
                     data={data}
                     squares
                     cellRender={value => value}
                     colorArray={this.props.colorArray}
                     moment={this.props.moment}
                     heatmapLoading={this.props.heatmapLoading}
                />
            </div>
        )}
}

export default HeatMapDemo;

