import PropTypes from "prop-types";
import XLabels from "./XLabels";
import DataGrid from "./DataGrid";

class HeatMap extends React.Component{
    constructor(props){
        super(props);
        this.state={

        }
    }
    datagridLoader(){
      return(
        <div className="hmdgLdrwrpr">
          {
            //loader-grey-line
          }
          <div className="hmdgLdrow" >
            <div className="hmdgLdclm" />
            <div className="hmdgLdclm" />
            <div className="hmdgLdclm" />
            <div className="hmdgLdclm" />
            <div className="hmdgLdclm" />
            <div className="hmdgLdclm" />
            <div className="hmdgLdclm" />
            <div className="hmdgLdclm" />
            <div className="hmdgLdclm" />
            <div className="hmdgLdclm" />
          </div>
          <div className="hmdgLdrow" >
            <div className="hmdgLdclm" />
            <div className="hmdgLdclm" />
            <div className="hmdgLdclm" />
            <div className="hmdgLdclm" />
            <div className="hmdgLdclm" />
            <div className="hmdgLdclm" />
            <div className="hmdgLdclm" />
            <div className="hmdgLdclm" />
            <div className="hmdgLdclm" />
            <div className="hmdgLdclm" />
          </div>
        </div>
      )
    }
    render(){
    let cursor = "";
      if (this.props.onClick !== undefined) {
        cursor = "pointer";
      }
      const xLabelsEle = (
        
        <XLabels
          labels={this.props.xLabels}
          width={this.props.xLabelWidth}
          labelsVisibility={this.props.xLabelsVisibility}
          height={this.props.height}
          squares={this.props.squares}
          yWidth={this.props.yLabelWidth}
        />
      );
        return(
            <div class="DataGrid">
            {
              this.props.heatmapLoading?
              this.datagridLoader():
            <div>
            {this.props.xLabelsLocation === "top" && xLabelsEle}
            <DataGrid
                xLabels = {this.props.xLabels}
                yLabels = {this.props.yLabels}
                data={this.props.data}
                // background = {this.props.background}
                height= {this.props.height}
                xLabelWidth = {this.props.xLabelWidth}
                yLabelWidth={this.props.yLabelWidth}
                yLabelTextAlign = {this.props.yLabelTextAlign}
                unit ={this.props.unit}
                xLabelsLocation = {this.props.xLabelsLocation}
                displayYLabels ={this.props.displayYLabels}
                onClick = {this.props.onClick}
                cursor = {cursor}
                squares = {this.props.squares}
                cellRender = {this.props.cellRender}
                colorArray={this.props.colorArray}
                moment={this.props.moment}

            />
            {this.props.xLabelsLocation === "bottom" && xLabelsEle}
            </div>
            }
          </div>
        )
        
    }
}

HeatMap.propTypes = {
  xLabels: PropTypes.arrayOf(
    PropTypes.oneOfType([PropTypes.string, PropTypes.number, PropTypes.object])
  ).isRequired,
  yLabels: PropTypes.arrayOf(
    PropTypes.oneOfType([PropTypes.string, PropTypes.number, PropTypes.object])
  ).isRequired,
  data: PropTypes.arrayOf(PropTypes.array).isRequired,
  background: PropTypes.string,
  height: PropTypes.number,
  xLabelWidth: PropTypes.number,
  yLabelWidth: PropTypes.number,
  xLabelsLocation: PropTypes.oneOf(["top", "bottom"]),
  xLabelsVisibility: PropTypes.arrayOf(PropTypes.bool),
  yLabelTextAlign: PropTypes.string,
  displayYLabels: PropTypes.bool,
  unit: PropTypes.string,
  onClick: PropTypes.func,
  squares: PropTypes.bool,
  cellRender: PropTypes.func,
  cellStyle: PropTypes.func,
};

HeatMap.defaultProps = {
  background: "#80ced6",
  height: 100,
  xLabelWidth: 100,
  yLabelWidth: 100,
  yLabelTextAlign: "right",
  unit: "",
  xLabelsLocation: "top",
  xLabelsVisibility: null,
  displayYLabels: true,
  onClick: undefined,
  squares: false,
  cellRender: () => null,
};

export default HeatMap;

