;
import PropTypes from "prop-types";

const FixedBox = ({ children, width }) => {
  return <div className="stmpSdbr"> {children==''?<div className='hmmnttl hmsdbrItm'>Segments</div> : children }</div>;
};

FixedBox.defaultProps = {
  children: "",
};

FixedBox.propTypes = {
  children: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
  width: PropTypes.number.isRequired,
};

export default FixedBox;

