import Collapse, { Panel } from 'rc-collapse' // collapse
// collapse css
import { IndexLink, Link } from 'react-router'
import * as utils from '../../utils/utils'

class LeaderboardNav extends React.Component {
  render(){
    let led_path_name = this.props.location.pathname;
    return (
      <nav className='nav-side'>
      <ul className="parent">
        <li>
          <Link className={ led_path_name == "/leaderboard" ? 'active' : ''} to={`/leaderboard`}><span className="text">All</span></Link>
        </li>
        <li>
          <Link activeClassName='active' to={`/leaderboard/mydepartment`}><span className="text">My Department</span></Link>
        </li>
      </ul>
      </nav>
    )
  }
}

module.exports = LeaderboardNav