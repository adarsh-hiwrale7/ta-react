import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Line, Circle } from 'rc-progress'
import notie from 'notie/dist/notie.js'
import * as utils from '../../utils/utils'
import Globals from '../../Globals'
import { browserHistory } from 'react-router'
import $ from '../Chunks/ChunkJquery'
import ChunkJqueryDropdown from '../Chunks/ChunkJqueryDropdown';
import * as profileSettings from "../../actions/settings/getSettings";
import LeaderboardNav from './LeaderboardNav';
import Header from '../Header/Header';
import PreLoader from '../PreLoader'
import GuestUserRestrictionPopup from '../GuestUserRestrictionPopup'
import StaticDataForGuest from '../StaticDataForGuest';
var showgraphs = null
class Leaderboard extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      rankings: [],
      departments: [],
      show_menu: true,
      isMyDepartmentCaling:false,
      loader:false,
      period:'month'
    }
  }
  componentDidMount() {
    var roles = JSON.parse(localStorage.getItem('roles'))
    // this.props.profileSettings.GetProfileSettings();
    var location = utils
      .removeTrailingSlash(this.props.location.pathname)
      .split('/')

    if (roles !== undefined && roles[0].role_name !== "guest") {
      this.fetchLeaderboard(location);
    }
  }
  componentWillMount() {
    $().then(jquery => {
      var $ = jquery.$

      ChunkJqueryDropdown().then(jqueryDropdown => { })
    })
  }
  componentWillUnmount(){
    window.scrollTo(0,0);
  }
  componentWillReceiveProps(nextProps) {
    var newLocation = utils
      .removeTrailingSlash(nextProps.location.pathname)
      .split('/')

    if (nextProps.location.pathname !== this.props.location.pathname) {

      if (nextProps.profile.profile.role !== undefined && nextProps.profile.profile.role.data[0].role_name !== "guest") {
        this.fetchLeaderboard(newLocation)
      }

    }
  }

  fetchLeaderboard(path) {
    this.setState({loader:true})
    let pathAll = Globals.API_ROOT_URL + `/leaderboard?timespan=${this.state.period}`;
    let pathMyDept = Globals.API_ROOT_URL + `/leaderboard?timespan=${this.state.period}&is_department=1`

    let pathToFetch = pathAll;
    this.setState({ isMyDepartmentCaling: false })
    if (typeof (path[2]) !== "undefined" && path[2] === "mydepartment") {
      this.setState({
        isMyDepartmentCaling: true
      })
      pathToFetch = pathMyDept;
    }
    fetch(pathToFetch, {
      headers: utils.setApiHeaders('get')
    })
      .then(response => response.json())
      .then(json => {
        if (Array.isArray(json.data) && json.data.length > 0) {
          this.setState({ rankings: json.data, loader: false })
        } else {
          this.setState({ loader: false, rankings: [] })
          utils.handleSessionError(json)
        }//'There was a problem fetching Leaderboard'
      })
      .catch(err => {
        notie.alert('warning', err, 5)
        throw err
      })
  }
  onRowClick(id) {
    let elInfo = document.querySelector(
      `#leaderboard [data-statsIdentity='${id}']`
    )
    if (elInfo.classList.contains('revealed')) {
      elInfo
        .classList
        .remove('revealed')
      return
    }
    elInfo.classList.add('revealed')
  }
  init(period){
    var roles = JSON.parse(localStorage.getItem('roles'))
    var location = utils
      .removeTrailingSlash(this.props.location.pathname)
      .split('/')
    this.setState({
      period:period
    },()=>{
      if(roles!==undefined &&roles[0].role_name!=="guest"){
        this.fetchLeaderboard(location);
      }
    })
  }

  renderLeaderboardTable() {
    return (
      <div id='leaderboard' >
       {this.state.loader ==true ? <PreLoader location="leaderboard-loader" /> :
       <div>
         <div className='header-analytics-wrapper clearfix'>
          <div className="filter-weekey-container clearfix">
            <div className="filter-container">
                <a className={`week-filter ${this.state.period == "week" ? 'active' : ''}`} onClick={this.init.bind(this, 'week')}> Week </a>
                <a className={`month-filter ${this.state.period == "month" ? 'active' : ''}`} onClick={this.init.bind(this, 'month')}> Month </a>
                <a className={`year-filter ${this.state.period == "year" ? 'active' : ''}`} onClick={this.init.bind(this, 'year')}>Year </a>
            </div>
          </div>
        </div>
        <h2 className='top-employee-title'>Top advocates</h2>
        
        <div className='clearfix'>


          {this.state.rankings.length>0 ?
          this.state.rankings.map((ranking, index) => {
            let userAvatarURL = ranking.avatar ?ranking.avatar :''
            var userAvatar = `${userAvatarURL !== '' ? userAvatarURL : '/img/user.jpg'}`
            let rank_diff = ranking.rank_diff
            let meterClass = ''
            let faMeterIcon = 'remove'
            showgraphs=utils.rankDiff(rank_diff,true)
            if(index<3){

              return(

                <div className="top-employees" key={index}>
                 <div
                  key={index}
                  data-statsIdentity={ranking.user_id}
                 // className='innerdiv-main clearfix '
                  className={this.props.profile.profile.identity == ranking.user_id ? 'innerdiv-main  clearfix' : 'innerdiv-main clearfix'}>
                  <div>
                  {/* puffIn */}
                    <div className={this.props.profile.profile.identity == ranking.user_id ? `index-property  currentuser` : 'index-property'} >
                      <div className='number-id'>
                        {/* {index + 1} */}
                        {ranking.rank}
                      </div>
                    </div>
                    {/* <div className='text-part'> */}
                    <div className={this.props.profile.profile.identity == ranking.user_id ? `text-part  currentuser-textprt` : 'text-part'}>
                      <div className='inner-text-part-container clearfix'>
                        <div className='flex-item-text-part clearfix leaderboard-name-col '>
                          {/* <div className='img' style={userAvatar} /> */}
                          <div className="leaderboard-avtar">
                          <img className='img' src={userAvatar} onError={(e) => { e.target.src = "https://app.visibly.io/img/default-avatar.png" }}></img>
                          </div>
                          <div className='text-header'>
                            <div className='name'>
                              {ranking.first_name}
                              {' '}
                              {ranking.last_name}
                            </div>
                            <div className='name2'>
                              {ranking.department_name?ranking.department_name:''}
                            </div>
                          </div>
                        </div>
                        <div className={`up-aero ${meterClass}`}>
                            {/* <i class='material-icons'>{faMeterIcon}</i> */}
                            {/* payal */}
                            <div className="leaderboard-level-image">
                            {showgraphs}

                            </div>
                            {' '}
                            <span className={`number ${rank_diff  == 0  ? 'zero-level-no' :''}`}>{rank_diff  > 0  ? <span>+</span> :''}{rank_diff}</span>
                          </div>

                        <div className='progressbar'>
                        <div className='start clearfix'>
                          <div className='pg-donet clearfix'>

                            <div className='donet-tx  donet-color-primary'>
                              <p className='tx-ld  donet-color-primary'>
                                {ranking.total_uploads}
                              </p>
                              <p className='tx-ld1 donet-color-primary'>Uploads</p>
                            </div>
                          </div>
                          {/* second denote  */}
                          <div className='pg-donet'>

                            <div className='donet-tx donet-color-theme'>
                              <p className='tx-ld   donet-color-theme'>
                                {ranking.total_share}
                              </p>
                              <p className='tx-ld1'>Shares</p>
                            </div>
                          </div>
                          {/* third denote  */}
                          <div className='pg-donet'>

                            <div className='donet-tx   donet-color-green'>
                              <p className='tx-ld   donet-color-green'>
                                {ranking.total_likes}
                              </p>
                              <p className='tx-ld1  donet-color-green'>Likes</p>
                            </div>
                          </div>
                          {/* fourth donet  */}
                          <div className='pg-donet'>

                            <div className='donet-tx donet-color-night'>
                              <p className='tx-ld donet-color-night'>{ranking.total_comments}</p>
                              <p className='tx-ld1 donet-color-night'>Comments</p>
                            </div>
                          </div>

                        </div>
                      </div>
                      <div className='flex-item-text-part clearfix leaderboard-number-col'>
                          <div className='postbox'>
                          <div className='edit'>
                            {ranking.total_post}
                            <span> Posts</span>
                            {/* <i class="material-icons edit-mt">&#xE254;</i> */}
                            {' '}
                          </div>
                          <div className='fav'>
                            {ranking.point}
                            <span> XP</span>
                            {/* <i class="material-icons">star_border</i> */}
                            {' '}
                          </div>
                          <div className='pg-donet pg-prog'>



                              <span className='sp donet-color-theme'>
                                  {ranking.approval_rate}%
                                  </span>
                                  <b>
                                  Approval rate
                                  </b>



                          </div>

                        </div>

                      </div>
                      {/* second part of  1st div */}
                    </div>{' '}
                    </div>
                  </div>
                </div>
                </div>
              )
            }else{
              return (
                // <div
                //   key={index}
                //   data-statsIdentity={ranking.identity}
                //  // className='innerdiv-main clearfix '
                //   className={this.props.profile.profile.identity == ranking.user_id ? 'innerdiv-main  current-user-wrapper clearfix' : 'innerdiv-main clearfix'}
                //   onClick={this.onRowClick.bind(this, ranking.identity)}>
                 <div className="other-employees">
                  {index == 3 ? <h2>Other Employees</h2> :''}
                  <div
                  key={index}
                  data-statsIdentity={ranking.user_id}
                 // className='innerdiv-main clearfix '
                  className={this.props.profile.profile.identity == ranking.user_id ? 'innerdiv-main  current-user-wrapper clearfix' : 'innerdiv-main clearfix'}>
                  <div className={this.props.profile.profile.identity == ranking.user_id ? `current  current-user` : 'current'}>
                  {/* puffIn */}
                    <div className={this.props.profile.profile.identity == ranking.user_id ? `index-property  currentuser` : 'index-property'} >
                      <div className='number-id'>
                        {/* {index + 1} */}
                        {ranking.rank}
                      </div>
                    </div>
                    {/* <div className='text-part'> */}
                    <div className={this.props.profile.profile.identity == ranking.user_id ? `text-part  currentuser-textprt` : 'text-part'}>
                      <div className='inner-text-part-container clearfix'>
                        <div className='flex-item-text-part clearfix leaderboard-name-col leaderboard-border'>
                          <img className='img' src={userAvatar} onError={(e) => { e.target.src = "https://app.visibly.io/img/default-avatar.png" }}></img>
                          <div className='text-header'>
                            <div className='name'>
                              {ranking.first_name}
                              {' '}
                              {ranking.last_name}
                            </div>
                            <div className='name2'>
                              {ranking.department_name?ranking.department_name:''}
                            </div>
                          </div>
                        </div>
                        <div className='flex-item-text-part clearfix leaderboard-number-col leaderboard-border'>
                          <div className='edit'>
                            {ranking.total_post}
                            <span> Posts</span>
                            {/* <i class="material-icons edit-mt">&#xE254;</i> */}
                            {' '}
                          </div>
                          <div className='fav'>
                            {ranking.point}
                            <span> XP</span>
                            {/* <i class="material-icons">star_border</i> */}
                            {' '}
                          </div>
                          <div className='pg-donet pg-prog'>
                              <span className='sp donet-color-theme'>
                                  {ranking.approval_rate}%
                                  </span>
                                  <b>
                                  Approval rate
                                  </b>

                          </div>

                        </div>
                        <div className='progressbar leaderboard-border' >
                        <div className='start clearfix'>
                          <div className='pg-donet clearfix'>

                            <div className='donet-tx  donet-color-primary'>
                              <p className='tx-ld  donet-color-primary'>
                                {ranking.total_uploads}
                              </p>
                              <p className='tx-ld1 donet-color-primary'>Uploads</p>
                            </div>
                          </div>
                          {/* second denote  */}
                          <div className='pg-donet pg-donetspace'>

                            <div className='donet-tx donet-color-theme'>
                              <p className='tx-ld   donet-color-theme'>
                                {ranking.total_share}
                              </p>
                              <p className='tx-ld1'>Shares</p>
                            </div>
                          </div>
                          {/* third denote  */}
                          <div className='pg-donet pg-donetspace'>

                            <div className='donet-tx   donet-color-green'>
                              <p className='tx-ld   donet-color-green'>
                                {ranking.total_likes}
                              </p>
                              <p className='tx-ld1  donet-color-green'>Likes</p>
                            </div>
                          </div>
                          {/* fourth donet  */}
                          <div className='pg-donet'>

                            <div className='donet-tx donet-color-night'>
                              <p className='tx-ld donet-color-night'>{ranking.total_comments}</p>
                              <p className='tx-ld1 donet-color-night'>Comments</p>
                            </div>
                          </div>

                        </div>
                      </div>
                      <div className={`up-aero ${meterClass}`}>
                            {/* <i class='material-icons'>{faMeterIcon}</i> */}
                            <div className="leaderboard-level-image"> {showgraphs}</div>

                            <span className='number'>{rank_diff  > 0  ? <span>+</span> :''}{rank_diff}</span>
                          </div>
                      </div>
                      {/* second part of  1st div */}

                    </div>{' '}
                    </div>
                  </div>
                  </div>
                  )
            }
          }
          ): <div className='no-data-block'>No user found.</div>}
        </div>
        </div>
       }
      </div>
    )
  }
  render() {
    let roleName = JSON.parse(localStorage.getItem('roles')) !== null ? JSON.parse(localStorage.getItem('roles'))[0].role_name : ''
    return (
      <div>
        <GuestUserRestrictionPopup setOverlay={true} />
        <section className='leaderboard'>
          <Header title="Leaderboard" nav={LeaderboardNav} location={this.props.location} />

          <div className='main-container'>
            <div id='content'>
              <div className='page'>
                <div className='esc-sm'>
                  <div className='content'>
                    <div className='leaderboard-wrap'>
                      {roleName == 'guest' ?
                        <StaticDataForGuest props={this.props} />
                        : this.renderLeaderboardTable()}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    )
  }
  open_main_menu() {
    this.setState({
      show_menu: !this.state.show_menu
    });
    var active_class = this.state.show_menu ? "in-view has-sib" : " has-sib";
    var handle_active_class = this.state.show_menu ? 'active inner_main_menu_handle' : 'inner_main_menu_handle';
    document.getElementById('navdraw').className = active_class;
    document.getElementById('inner_main_menu_handle').className = handle_active_class;

  }
}
function mapStateToProps(state) {
  return {
    profile: state.profile
  }
}
function mapDispatchToProps(dispatch) {
  return {
    profileSettings: bindActionCreators(profileSettings, dispatch)
  }
}
module.exports = connect(mapStateToProps, mapDispatchToProps)(Leaderboard)
