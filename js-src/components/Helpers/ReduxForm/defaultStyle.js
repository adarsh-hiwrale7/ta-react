export default ({
    control: {
      backgroundColor: '#fff',
  
      fontSize: 12,
      fontWeight: 'normal',
    },
  
    input: {
      margin: 0,
    },
  
    '&singleLine': {
      control: {
        border: '1px solid #e5e4e4',
        borderRadius: '.2rem',
      },
  
      highlighter: {
        padding: 9
      },
  
      input: {
        padding: 9,
        outline: 0,
        minHeight: 38,
        width:"100%",
        border: '1px solid transparent'
      },
    },
  
    '&multiLine': {
      control: {
        border: '1px solid #e5e4e4',
        borderRadius: '.2rem',
      },
  
      highlighter: {
        padding: 9,
      },
  
      input: {
        padding: 9,
        minHeight: 100,
        outline: 0,
        border: '1px solid transparent',
      },
    },
   
    suggestions: {
      list: {
        backgroundColor: 'white',
        border: '1px solid rgba(0,0,0,0.15)',
        fontSize: 10,
        maxHeight:241,
        overflowX:'hidden',
        overflowY:'auto'
      },
  
      item: {
        padding: '5px 15px',
        borderBottom: '1px solid rgba(0,0,0,0.15)',
  
        '&focused': {
          backgroundColor: '#cee4e5',
        },
      },
    },
  })
  