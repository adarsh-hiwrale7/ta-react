import { Component, PropTypes } from 'react'
import { MentionsInput, Mention } from 'react-mentions'
import defaultStyle from './defaultStyle'
class MentionTextArea extends Component {

    constructor(props) {
        super(props)
        this.state = {
            userTags: []
        }
    }

    userTags = [];

    handleOnChange(e) {
        this.props.input.onChange(e.target.value);
    }
    handleOnClick(e) {
        this.props.onClick(e.target.value);
    }


    /* 
    @purpose: To remove the selected usertag from the list
    @author: Akshay soni
    */

    handleOnItemClick(item) {
        this.userTags.forEach((obj) => {
            if (obj.id == item.id) {
                console.log('item1', item)
                this.userTags.splice(item, 1)
            }
        })
    }


    HandleOnKeyDown(e) {
        this.props.onKeyDown ? this.props.onKeyDown(e) : '';
    }
    render() {
        this.userTags = this.props.userTags
        // const { input, meta, ...rest } = this.props;
        const { touched, error, warning } = this.props.meta;
        return (
            <div className="form-row">
                <MentionsInput
                    id={this.props.id}
                    required={this.props.validatedata}
                    name={this.props.input.name}
                    value={this.props.input.value || ''}
                    onChange={event => this.props.input.onChange(event.target.value)}
                    singleLine={this.props.singleLine ? true : false}
                    style={defaultStyle}
                    markup="@[__display__](__type__:__id__)"
                    placeholder={this.props.placeholder}
                    disabled={this.props.disabled}
                    className="mention-input-wrapper"
                    onFocus={event => this.props.input.onFocus(event)}
                    onClick={this.props.onClick ? event => this.props.onClick(event.target.value) : ''}
                    onKeyPress={this.props.onKeyPress ? event => this.props.onKeyPress(event) : ''}
                    onKeyDown={event => this.HandleOnKeyDown(event)}
                >
                    <Mention trigger="@"
                        data={this.userTags}
                        type="user"

                        //    onAdd={ (id, display) => this.props.onAdd(id, display)}
                        // style={{ color: '#256eff' }}

                        renderSuggestion={(suggestion, search, highlightedDisplay) => (
                            <div className="user clearfix" onClick={() => ''}>
                                {/* {suggestion} */}
                                <div className="tag-avtar-wrapper">
                                    {
                                        suggestion.type == 'Company'
                                            ? <i class='material-icons'>account_balance</i>
                                            : suggestion.type == 'Department'
                                                ? <i className='material-icons'>business</i>
                                                : <img src={suggestion.profile_image} />
                                    }

                                </div>
                                <div className='tag-user-data'>
                                    <div className='tag-users-top-data'><span className='tag-user-name'>{highlightedDisplay}</span> <span className='tag-in-user-text'>in {suggestion.type}</span></div>
                                    <div className='tag-users-bottom-data'><span className='tag-users-email'>{suggestion.email}</span></div>
                                    {suggestion.office_name && suggestion.office_name.trim() !== '' ? <div className='tag-users-bottom-data'><span className='tag-users-email'>{suggestion.office_name}</span></div> : ''}
                                </div>
                            </div>
                        )}
                    />
                    <Mention trigger="#"
                        data={this.props.tags}
                        type="tags"
                        //style={{  color: '#256eff' }}
                        renderSuggestion={(suggestion, search, highlightedDisplay) => (
                            <div className="user clearfix">
                                <div className='tag-user-data'>
                                    <div className='tag-users-top-data'><span className='tag-user-name'>{highlightedDisplay}</span></div>
                                </div>
                            </div>
                        )}
                    />
                </MentionsInput>
                <div className='form-valid-message'>
                    {touched && ((error && <span className="error">* {error}</span>) || (warning && <span className="warning">* {warning}</span>))}
                </div>

            </div>
        );
    }
}

export default MentionTextArea

{/*
const MentionTextArea = ({ input, userTags, tags, valueField, textField }) =>
<MentionsInput 
                {...input}
                value={input.value || ''}
                onChange={event => input.onChange(event.target.value)}
                singleLine={false}
                style={ defaultStyle }
                //markup="@[__display__](__type__:__id__)"
                >
                   <Mention trigger="@"
                        data={userTags}
                       // type="userTags"
                        style={{ backgroundColor: '#d1c4e9' }}
                       renderSuggestion={ (suggestion, search, highlightedDisplay) => (
                            <div className="user">
                                { highlightedDisplay }
                            </div>
                        )}
                    />
                   <Mention trigger="#"
                        data={tags}
                        type="tags"
                        style={{ backgroundColor: '#d1c4e9' }}
                        renderSuggestion={ (suggestion, search, highlightedDisplay) => (
                            <div className="user">
                                { highlightedDisplay }
                            </div>
                        )}
                    /> 
            </MentionsInput>

        
export default MentionTextArea;
*/}