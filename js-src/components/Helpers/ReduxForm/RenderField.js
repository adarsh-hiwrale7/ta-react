const Line = require('rc-progress').Line;
import notie from 'notie/dist/notie.js'
import { MentionsInput, Mention } from 'react-mentions'
import * as utils from ' ../../../js-src/utils/utils';

var showDropzone = false;
export function renderField({ input, label,multiple, type, data, placeholder, value, cssClass, rows, disabled, meta: { touched, error, warning }, children,  maxLength}) {
  let fieldt = null;
  //if(this.props.disabled) disabled = true;
  switch(type) {
		case "textarea":
			fieldt = <textarea id={input.name} className={cssClass} rows={typeof(rows)=="undefined" ? "6" : rows} disabled={disabled} placeholder={placeholder} {...input} ></textarea>;
			break;
    case "text": case "checkbox": case "radio": case "file": case "password" :
      fieldt = <input id={input.name} {...input} className={cssClass} type={type} placeholder={placeholder} disabled={disabled} checked={input.checked} maxLength={maxLength}/>;
      break;
    case "select":
      fieldt = <select id={input.name} {...input} className={cssClass} disabled={disabled} multiple={multiple}> {children} </select>;
      break;
    case "hidden":
      fieldt = <input id={input.name} {...input} type={type} />;
      break;
    case "MentionsInput":
      fieldt = <MentionsInput value={input.value}>
    <Mention trigger="#"
        data={data}
        renderSuggestion={ (suggestion, search, highlightedDisplay) => (
          <div className="user">
            { highlightedDisplay }
          </div>
        )}
         />
</MentionsInput>;
      break;
		default:
			fieldt = <input id={input.name} {...input} type={type}  placeholder={placeholder} />;
	}

  let formRowClass = `form-row ${disabled ? 'disabled': ''} ${type}`;
  let formRowId = input.name;
  let inverse = (type=="checkbox" || type=="radio" ? true : false);
  if(!inverse) {
    return (
      <div className={formRowClass} >
        { label!=null ?  <label for={formRowId}>{label}</label> : null }
        { fieldt }
        <div className='form-valid-message'>
          {touched && ((error && <span className="error">* {error}</span>) || (warning && <span className="warning">* {warning}</span>))}
        </div>
      </div>
    )
  }
  else {
    return (
      <div className={formRowClass} >
        { fieldt }
        <label for={formRowId}>{label}</label>
        <div className='form-valid-message'>
          {touched && ((error && <span className="error">* {error}</span>) || (warning && <span className="warning">* {warning}</span>))}
        </div>
      </div>
    )
  }


}

/*export const renderTimePicker = (field) => {
  return (
    <div>
      <TimePicker
        name={field.name}
        style={{ width: 100 }}
        showSecond={true}
        defaultValue={"00:00:30"}
        className="timePicker"
        onChange={() => field.input.onChange()}
      />
      {field.meta.touched &&
        field.meta.error &&
        <span className="error">{field.meta.error}</span>}

    </div>
  );
}*/

export const renderDropzoneField = (field) => {

  const files = field.input.value;
 return (
    <div className="dropzoneUpload">
    {field.Dropzone?
      <field.Dropzone
        name={field.name}
        className = {field.cssClass}
        multiple = {field.multiple }
        style={field.style}
        onDrop={field.handleSubmit }
        //maxSize={2500000}
        accept={field.accept}

      >
        <div>{field.title}</div>
      </field.Dropzone>:''}
      {field.meta.touched &&
        field.meta.error &&
        <span className="error">* {field.meta.error}</span>}
      {files && Array.isArray(files) && (
        <ul className="dropzone-preview">

          {

            files.map(function(file, i) {
              if(file.type.substring(0, 5)=="image") {
                return (
                  <li key={i}>
                    <img src={file.preview} />
                  </li>
                )
              }
              else {
                return (
                  <li key={i}>{file.name}</li>
                )
              }
            })

           }

        </ul>
      )}
    </div>
  );
}

function assetBtnClick(selector){
  document.getElementById(selector).click();
}
export const renderDropzoneBox = (field) => {
  var path = utils.removeTrailingSlash(window.location.href).split('/')
  let filesList = [];
  var files=[];
  var selectedMediaSizeTotal=0
  if(field.input.value.length>0){
  var assetLimit = field.input.value.length;
  if(field.fromCreatePost!==true){
      assetLimit = field.input.value.length;
  }else{
      assetLimit = field.input.value.length < field.limit ? field.input.value.length : field.limit;
  }
  for(var i=0;i<assetLimit;i++){
    filesList.push(field.input.value[i])
    selectedMediaSizeTotal+=field.input.value[i].size
  }
}
  files = filesList;
  var freeStorage=field.freeStorage
  var isUploadingStart=field.isUploadingStart
  if(isUploadingStart ==true ){
    showDropzone=true;
  }

  if(field.uploadDone == true && field.isUploadingStart !==true){
    showDropzone=false
  }
//  var freeStorage=12500
  // var acceptfile = ['image/jpeg','image/gif','image/jpg','image/png' ,'video/mp4' , 'video/mp3' ,'image/jpeg']
  var acceptfile = ['image/jpeg','image/gif','application/x-msexcel','application/x-excel','application/vnd.ms-excel','image/jpg','application/msexcel','application/excel','application/x-dos_ms_excel','application/xls' ,'image/png' ,'video/mp4' , 'video/mp3' ,'application/doc' , 'application/pdf' ,'image/jpeg' ,'application/ppt','application/vnd.openxmlformats-officedocument.spreadsheetml.sheet','application/vnd.ms-excel' ,'application/msword','application/vnd.openxmlformats-officedocument.wordprocessingml.document' ,'application/vnd.ms-powerpoint' , 'application/vnd.openxmlformats-officedocument.presentationml.presentation']
  var percentArray = [];
  var createPostMainContainer = document.getElementsByClassName('createPostUploadAssetSection')[0];
  return (
    <div className="dropzoneUploadWrapper">

    <div className="dropzoneUpload">
    {field.Dropzone?
      <field.Dropzone
        name={field.name}
        accept = {acceptfile.toString()}
        className = {field.cssClass}
        multiple = {field.multiple }
        style={field.style}
        //maxSize={2500000}
        onDrop={( filesToUpload, e ) => {
          if(filesToUpload.length==0) {
            notie.alert("error", "This file type is not suppoted",3);
          }
            var filesData=[];
            var assetLimit = filesToUpload.length ;
            if(field.fromCreatePost!==true){
              assetLimit = filesToUpload.length;
            }else{
                assetLimit = filesToUpload.length < field.limit ? filesToUpload.length : field.limit;
            }
            if(assetLimit <  filesToUpload.length){
              notie.alert('error','You can select a maximum of 9 media assets.',3);
            }
            if(assetLimit!==0){
              for(var i=0;i<assetLimit;i++){
                filesData.push(filesToUpload[i]);
              }
              var filesToUpload= filesData;
              field.input.onChange(filesToUpload)
              field.submit(filesToUpload)
            }
        } }
      >
      <div className='dropzoneIconWrapper'><i className='material-icons'>cloud_upload</i></div><div className='dropzoneTitle'>
      {
          field.title=='Drag and drop'?
          <div>Drag & drop, or <span>upload</span></div>
          :
          field.title
        }
      </div>
      </field.Dropzone>:''}
      </div>
       
      
       {files && Array.isArray(files) && files.length>0 && (
        
          <ul className="dropzone-preview">   
            {createPostMainContainer?createPostMainContainer.scrollTo(0,createPostMainContainer.scrollHeight):''}
            { field.FromAsset !== true?
              
              files.map(function(file, i) {

                var strokeColor = "#ebeef0";
                var trailColor = "#256eff";

                if(field.uploadProgress.file!==null) {
                  strokeColor = "#256eff";
                  trailColor = "#ebeef0";
                }

                if(selectedMediaSizeTotal<freeStorage || isUploadingStart==true){
                  if(file.type.substring(0, 5)=="image") {
                      return (
                        <li className={`item-${i}`} rel={i} key={i}>
                              <div className='preview-wrapper'>
                                <img src={file.preview} />
                              {file.type!=="image/gif"?
                              <div class="edit-post-wrapper"><a href="javascript:void(0)" onClick={assetBtnClick.bind(this,'editAssetBtn'+i)} class="edit-post"><i class="material-icons"></i></a></div>
                              :''}
                              </div>
                            <div className="filename" title={file.name}>{file.name}</div>
                            {!showDropzone? <button
                              type="button"
                              className='btn-default right delete-image-btn'
                              onClick={()=>{
                                if(file.type.includes("gif")){
                                  field.handleGifCount(file);
                                }
                                files = files.filter(item => item.preview !== file.preview)
                                field.input.onChange(files)
                              }}
                              >
                              <i className='material-icons'>clear</i>
                            </button>:''}
                          {showDropzone == true ? <Line percent={typeof (field.uploadProgress[i]) !== "undefined" ? field.uploadProgress[i].percent : 0} strokeWidth="4" strokeColor={strokeColor} trailWidth="4" trailColor={trailColor} strokeLinecap={'square'} /> :'' }
                            {field.fromCreatePost!==true?
                            <div className='uploadAssetBtn'>
                              <a href="javascript:void(0)" onClick={assetBtnClick.bind(this,'btn-upload-post')} title="Upload asset" className='btn btn-primary'>Upload</a>
                            </div>:''}
                        </li>
                      )
                    }
                    else if(file.type.substring(0, 5)=="video") {
                      return (
                        <li className={`item-${i}`} rel={i} key={i}>
                        <div id= {`dropPreviewIconInner-${file.preview}`} className="preview-wrapper video-preview">
                          <div className="dropPreviewIcon">
                              <i className="material-icons asset-title-icon">videocam</i>
                            </div>
                        </div>

                          <div className="filename" title={file.name}>{file.name}</div>
                          {!showDropzone? <button
                              type="button"
                              className='btn-default right delete-image-btn'
                              onClick={()=>{
                                files = files.filter(item => item.preview !== file.preview)
                                field.input.onChange(files)
                              }}
                              >
                              <i className='material-icons'>clear</i>
                            </button>:''}
                          {showDropzone == true ? <Line percent={typeof (field.uploadProgress[i]) !== "undefined" ? field.uploadProgress[i].percent : 0} strokeWidth="4" strokeColor={strokeColor} trailWidth="4" trailColor={trailColor} strokeLinecap={'square'} /> :'' }
                          {field.fromCreatePost!==true?
                          <div className='uploadAssetBtn'>
                              <a href="javascript:void(0)" onClick={assetBtnClick.bind(this,'btn-upload-post')} title="Upload asset" className='btn btn-primary'>Upload</a>
                            </div>:''}
                        </li>

                      )
                    }
                    else {
                      return (
                        <li className={`item-${i}`} rel={i} key={i}>
                          <div class="dropPreviewIcon">
                          <i class="material-icons asset-title-icon">insert_drive_file</i>
                          </div>
                          <div className="filename" title={file.name}>{file.name}</div>
                          
                          {!showDropzone?<button
                              type="button"
                              className='btn-default right delete-image-btn'
                              onClick={()=>{
                                if(file.type.includes("application")){
                                  field.handleDocCount(file);
                                }
                                files = files.filter(item => item.preview !== file.preview)
                                field.input.onChange(files)
                              }}
                              >
                              <i className='material-icons'>clear</i>
                            </button>:''}
                          {showDropzone == true ? <Line percent={typeof (field.uploadProgress[i]) !== "undefined" ? field.uploadProgress[i].percent : 0} strokeWidth="4" strokeColor={strokeColor} trailWidth="4" trailColor={trailColor} strokeLinecap={'square'} /> :'' }
                          {field.fromCreatePost!==true?
                          <div className='uploadAssetBtn'>
                              <a href="javascript:void(0)" onClick={assetBtnClick.bind(this,'btn-upload-post')} title="Upload asset" className='btn btn-primary'>Upload</a>
                            </div>:''}
                        </li>

                      )
                    }
                  }else{
                      notie.alert('error', ` You have consumed your storage limit. Please ask your admin to upgrade your package to continue.`, 5)
                  }
              })
             :''}
            {showDropzone!==true && field.FromAsset !== true ?
              <div className='uploadAssetBtn'>
                <a href="javascript:void(0)" onClick={assetBtnClick.bind(this,'btn-upload-post')} title="Upload asset" className='btn btn-primary'>Upload</a>
              </div>:''} 

          </ul>
        )} 
            
      {field.meta.touched && field.meta.error &&
        <span className="error">* {field.meta.error}</span>}
    </div>
  );
}
