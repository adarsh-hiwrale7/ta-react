import { Component } from 'react'

// import TimePicker from 'rc-time-picker';
import TimePicker from '../../Chunks/ChunkTimePicker'

class ReactTimePicker extends Component {
  constructor (props) {
    super(props)
    var val = this.props.input.value
    var Moment = this.props.moment

    this.state = {
      TimePicker: null,
      allvalue:[],
      valInit: null
    }
  }

  componentWillMount () {
    TimePicker().then(TimePicker => {
      this.setState({ TimePicker: TimePicker })
    })
  }

  addTimePickerClass ({ open }) {
    if (open) {
      window.setTimeout(function () {
        let rcTimePickerEl = document.querySelector('.rc-time-picker-panel')
        if (rcTimePickerEl !== null) {
          rcTimePickerEl.parentNode.parentNode.classList.add(
            'rc-timepicker-popup'
          )
        }
      }, 200)
    }
  }

  onBlur (value) {
    this.props.input.onBlur(value)
  }

  onChange(value) {
    var changedValueObj = {}
    this.setState({
      value: value,
      valInit: value
    })
    if (this.state.allvalue.length > 0 && this.state.allvalue[0].name == this.props.input.name) {
      //if selected time is already in array then to remove that and add new value
      this.state.allvalue.splice(0, 1)
    }
    changedValueObj = {
      name: this.props.input.name,
      value: value
    }
    this.state.allvalue.push(changedValueObj)
    this.props.input.onChange(value)
  }

  render () {
    var moment = this.props.moment
    var TimePicker = this.state.TimePicker

    if (this.props.input.value == '' && this.state.valInit == null) {
      //if value is null and if filed has already value then it will set otherwise to set current time
      var timeVal = this.props.timeValue ? this.props.timeValue : moment !== null ? moment(moment(), 'hh:mm') : null
    } else if (this.state.allvalue.length > 0 && this.props.input.value == '' && this.state.valInit !== null) {
      // if date is changed then to display its value 
      var timeVal = this.state.allvalue[0].value
    } else {
      var timeVal = this.props.input.value || (moment !== null ? moment(moment(), 'hh:mm') : null)
    }
    return (
      <div className='form-row text'>
        { this.props.label!==null ? <label for={this.props.input.name}>{this.props.label}</label>: null}
        
        {!TimePicker
          ? <div className = "curated-header clearfix">
              <div className = "name-header">
                  <div className = "name-curated loader-line-height"> </div>
              </div>
          </div>
          : <TimePicker
            {...this.props}
            showSecond={false}
            onOpen={this.addTimePickerClass.bind(this)}
            onBlur={this.onBlur.bind(this)}
            onChange={this.onChange.bind(this)}
            value={timeVal}
            />}

        <div className='form-valid-message'>
          {this.props.touched &&
            ((this.props.error &&
              <span className='error'>* {this.props.error}</span>) ||
              (this.props.warning &&
                <span className='warning'>* {this.props.warning}</span>))}
        </div>
      </div>
    )
  }
}

export default ReactTimePicker