import {Component, PropTypes} from 'react'
import Globals from '../../../Globals';
class ReactDatePicker extends Component {

    constructor(props) {
        super(props);
        var val = this.props.input.value;
        var Moment = this.props.moment;

        this.state = {
            value: Moment!==null ? Moment() : val,
            allvalue:[],
            valInit: null
        }

    }


    onChange(value) {
        var changedValueObj={}
        this.setState({
            value: value,
            valInit: value
        })
        if(this.state.allvalue.length>0 && this.state.allvalue[0].name==this.props.input.name){
            //if selected date is already in array then to remove that and add new value
            this.state.allvalue.splice(0,1)
        }
        changedValueObj={
            name:this.props.input.name,
            value:value
        }
        if(typeof this.props.updateDateValue !== 'undefined'){
            document.getElementById('valueName'+this.props.indexVal).value = this.props.moment(value).unix()
            this.props.updateDateValue(this.props.moment(value).unix(),this.props.indexVal);    
        }
        if(this.props.fromProject==true){
            this.props.updateProjectDate(this.props.moment(value).unix()); 
        }
        //console.log('value',value,this.props.moment(value).format('DD-MM-YYYY'),this.props.indexVal)
        this.state.allvalue.push(changedValueObj)
        this.props.input.onChange(value/1000)
    }

    render() {

        var ReactDatepicker = this.props.reactDatePicker;
        var Moment = this.props.moment;
        if(this.props.input.value==''&& this.state.valInit==null ){
            //if value is null and if filed has already value then it will set otherwise to set current date
            if(this.props.callFrom !== undefined){
                //if reactdatepicker is calling from analytics page then to avoid adding today's date in textbox
                var selectedDate = this.props.dateValue ? this.props.dateValue : null
            }else{
                var selectedDate = this.props.dateValue ? this.props.dateValue: Moment!==null ? Moment() : null
            }
        }else if(this.state.allvalue.length>0 && this.props.input.value=='' && this.state.valInit !==null ){
            // if date is changed then to display its value 
            var selectedDate=this.state.allvalue[0].value
        }else{
            var selectedDate = this.props.input.value!=="" ? (Moment!==null ? Moment.unix(this.props.input.value) : null) : (Moment!==null ? Moment() : null)
      }
        return (
            <div className="form-row text" >
                { this.props.label!==null ? <label for={this.props.input.name}>{this.props.label}</label>: null}
                
                {
                (!ReactDatepicker ? <div>
                    <div className = "curated-header clearfix">
                    <div className = "name-header">
                        <div className = "name-curated loader-line-height"> </div>
                    </div>
                    </div>
                </div> : <div>
                    <ReactDatepicker  {...this.props}
                    selected= {selectedDate}
                    onChange={ this.onChange.bind(this) }
                    /></div>)
                }
                <div className='form-valid-message'>
                    {this.props.touched && ((this.props.error && <span className="error">* {this.props.error}</span>) || (this.props.warning && <span className="warning">* {this.props.warning}</span>))}
                </div>
            </div>
        );
    }
}

export default ReactDatePicker
