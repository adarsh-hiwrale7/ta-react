import {Component, PropTypes} from 'react'
// import Select from 'react-select' this plugin is uninstalled as we are not using this page anymore versio  was 1.0.0-rc.5

class SelectCreatable extends Component {
    
    handleOnChange (value) {
        if (this.props.input.onChange) {
            this.props.input.onChange(value); // <-- To be aligned with how redux-form publishes its CHANGE action payload. The event received is an object with 2 keys: "value" and "label"
        }
    }

    render() {
        return (
            // <Select.Creatable
            //     {...this.props}
            //     multi = {this.props.multi}
            //     autoSize = {false}
            //     placeholder={this.props.placeholder}
            //     menuContainerStyle={{ zIndex: 555 }}
            //     className={this.props.class}
            //     onBlur={() => this.props.input.onBlur(this.props.input.value)}
            //     onChange={this.handleOnChange.bind(this)}
            //     value = {this.props.input.value || ''}
            //     options={this.props.options} // <-- Receive options from the form
            // />
            <div></div>
        );
    }
}

export default SelectCreatable
