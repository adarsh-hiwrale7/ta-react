
export default function PreLoaderMaterial()  {
  	return (
      <div className="spinner-wrap">
        <svg className="spinner-svg" width="65px" height="65px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg">
         <circle className="path" fill="none" strokeWidth="4" strokeLinecap="round" cx="33" cy="33" r="30"></circle>
        </svg>
      </div>
		)
}
