import * as utils from '../utils/utils'
export default class SlackRedirection extends React.Component {
    componentDidMount() {
        //after authorizing and selecting channel from slack , slack will give code so to fetch code from url and pass it to api
        if (Object.keys(this.props.location.query).length > 0) {
            if (utils.detectmob() == "ios") {
                if (typeof window !== 'undefined') {
                    window.location.replace(`com.visibly.app://?code=${this.props.location.query.code}&state=${this.props.location.query.state}`);
                }
            } else if (utils.detectmob() == "android") {
                if (typeof window !== 'undefined') {
                    window.location.replace(`intent:#Intent;action=com.visibly.app;category=android.intent.category.DEFAULT;category=android.intent.category.BROWSABLE;S.msg_from_browser=Launched%20from%20Browser;package=com.visibly.app;S.code=${this.props.location.query.code};S.state=${this.props.location.query.state};end`);
                }
            }
        }
    }

    render() {
        return (
            <div></div>
        )
    }
}