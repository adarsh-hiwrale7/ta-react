import {connect} from 'react-redux'
import * as utils from '../../utils/utils'
import EventNav from "./EventNav"
import {bindActionCreators} from 'redux'
import reactable from '../Chunks/ChunkReactable'
class EventListView extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      reactable:null
    }
  }
  componentWillMount(){
    reactable().then(reactable=>{
      this.setState({reactable:reactable})
    })
  }
  render() {
    var media_url = '';
    if (this.props.Location == "all") {
      media_url = "/img/default-avatar.png"
    } else if (this.props.Location == "live") {
      media_url = "/img/default-avatar.png"
    } else if (this.props.Location == "expired") {
      media_url = "/img/default-avatar.png"
    } else {
      media_url = "/img/default-avatar.png"
    }
    return (
      this.state.reactable?
      <this.state.reactable.Table
        className='table responsive-table campaign-table'
        id='campaign-table'
        itemsPerPage={10}
        pageButtonLimit={5}>
        <this.state.reactable.Thead>
          <this.state.reactable.Th column='profile_event'>
            <strong className='name-header'></strong>
          </this.state.reactable.Th>

          <this.state.reactable.Th column='start_date'>
            <strong className='name-header'>From</strong>
          </this.state.reactable.Th>
          <this.state.reactable.Th column='end_date'>
            <strong className='name-header'>To</strong>
          </this.state.reactable.Th>
          <this.state.reactable.Th column='eventtype'>
            <strong className='name-header'>Type</strong>
          </this.state.reactable.Th>
          <this.state.reactable.Th column='eventtitle'>
            <strong className='name-header'>Title</strong>
          </this.state.reactable.Th>
          <this.state.reactable.Th column='eventaudience'>
            <strong className='name-header'>Audience</strong>
          </this.state.reactable.Th>
          <this.state.reactable.Th column='eventposts'>
            <strong className='name-header'>Posts</strong>
          </this.state.reactable.Th>
          <this.state.reactable.Th column='eventchannels'>
            <strong className='name-header'>Channels</strong>
          </this.state.reactable.Th>
        </this.state.reactable.Thead>

        <this.state.reactable.Tr className="table-body-text">
          <this.state.reactable.Td column='profile_event' className="fd clearfix">
            <img src={media_url} alt="no-img" className='list-avatar-event'/>
          </this.state.reactable.Td>
          {/* second row */}
          <this.state.reactable.Td column='start_date' className="fd" data-rwd-label="start_date">
            <strong className='name-header'>
              {this.props.Location == "all"
                ? "4-5-2014"
                : ''}
              {this.props.Location == "live"
                ? "4-5-2018"
                : ''}
              {this.props.Location == "expired"
                ? "4-5-2012"
                : ''}
            </strong>
          </this.state.reactable.Td>

          <this.state.reactable.Td column='end_date' className="fd" data-rwd-label="end_date">
            <strong className='name-header'>
              {this.props.Location == "all"
                ? "4-5-2018"
                : ''}
              {this.props.Location == "live"
                ? " 4-5-2018"
                : ''}
              {this.props.Location == "expired"
                ? "4-5-2018"
                : ''}
            </strong>
          </this.state.reactable.Td>
          {/* third row */}
          <this.state.reactable.Td column='eventtype' className="fd" data-rwd-label="eventtype">
            <strong className='name-header'>
              {this.props.Location == "all"
                ? " abc"
                : ''}
              {this.props.Location == "live"
                ? "cyt"
                : ''}
              {this.props.Location == "expired"
                ? "xyz"
                : ''}
            </strong>
          </this.state.reactable.Td>
          <this.state.reactable.Td column='eventtitle' className="fd" data-rwd-label="eventtitle">
            <a onClick={this.props.eventPopup}>
              <strong className='name-header'>
                {this.props.Location == "all"
                  ? "abc"
                  : ''}
                {this.props.Location == "live"
                  ? "xytx"
                  : ''}
                {this.props.Location == "expired"
                  ? "xyz"
                  : ''}
              </strong>
            </a>
          </this.state.reactable.Td>
          <this.state.reactable.Td column='eventaudience' className="fd" data-rwd-label="eventaudience">
            <strong className='name-header'>
              {this.props.Location == "all"
                ? "xyz"
                : ''}
              {this.props.Location == "live"
                ? "xytx"
                : ''}
              {this.props.Location == "expired"
                ? "xyz"
                : ''}

            </strong>
          </this.state.reactable.Td>
          <this.state.reactable.Td column='eventposts' className="fd" data-rwd-label="eventposts">
            <strong className='name-header'>
              {this.props.Location == "all"
                ? "xyz"
                : ''}
              {this.props.Location == "live"
                ? "xytx"
                : ''}
              {this.props.Location == "expired"
                ? "xyz"
                : ''}
            </strong>
          </this.state.reactable.Td>
          <this.state.reactable.Td column='eventchannels' className="fd" data-rwd-label="eventchannels">
            <strong className='name-header'>
              {this.props.Location == "all"
                ? "xyz"
                : ''}
              {this.props.Location == "live"
                ? "xytx"
                : ''}
              {this.props.Location == "expired"
                ? "xyz"
                : ''}
            </strong>
          </this.state.reactable.Td>
        </this.state.reactable.Tr>
      </this.state.reactable.Table>
      :''
    )
  }
}
function mapStateToProps(state) {
  return {}
}

function mapDispatchToProps(dispatch) {
  return {}
}
let connection = connect(mapStateToProps, mapDispatchToProps)
module.exports = connection(EventListView)
//module.exports = connection(EventListView)