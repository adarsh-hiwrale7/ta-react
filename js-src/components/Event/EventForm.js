import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import {Field, reduxForm} from 'redux-form';
import {renderField} from '../Helpers/ReduxForm/RenderField'
// import {WithContext as ReactTags} from 'react-tag-input'; this plugin is uninstalled bcz we are not using this file v 4.9.1
import ReactDatePicker from '../Helpers/ReduxForm/ReactDatePicker' // react datepicker field
import reactDatepickerChunk from '../Chunks/ChunkDatePicker' // datepicker chunk
import moment from '../Chunks/ChunkMoment'
const required = value => value
    ? undefined
    : 'Required';
class EventForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            show_menu: true,
            show_toggle: false,
            tags: [],
            Moment: null,
            showPostCreationButton: false,
            ReactDatePicker: null,
            mounted: true,
            //date: new Date(),
        };
        // onChange = date => this.setState({ date })
        this.handleDelete = this
            .handleDelete
            .bind(this);
        this.handleAddition = this
            .handleAddition
            .bind(this);
        this.handleDrag = this
            .handleDrag
            .bind(this);
    }

    componentWillMount() {
        var me = this
        moment().then(moment => {
            reactDatepickerChunk().then(reactDatepicker => {
                if (me.state.mounted) {
                    this.setState({
                        ReactDatePicker: reactDatepicker, Moment: moment, showPostCreationButton: true // show post creation button only after everything is loaded
                    })
                }
            }).catch(err => {
                throw err
            })
        })
    }
    componentWillUnmount() {
        this.setState({mounted: false})
    }

    handleDelete(i) {
        let tags = this.state.tags;
        tags.splice(i, 1);
        this.setState({tags: tags});
    }

    handleAddition(tag) {
        let tags = this.state.tags;
        tags.push({
            id: tags.length + 1,
            text: tag
        });
        this.setState({tags: tags});
    }
    handleDrag(tag, currPos, newPos) {
        let tags = this.state.tags;
        tags.splice(currPos, 1);
        tags.splice(newPos, 0, tag);
        this.setState({tags: tags});
    }
    render() {
        const {tags, suggestions} = this.state;
        return (
            <div>

                {this.props.Location_form == "all"
                    ? <form role="form">
                            <div className="form-row event-form clearfix">
                                <label>Event</label>
                                <Field
                                    placeholder='Name'
                                    component={renderField}
                                    type='text'
                                    name='event-fm'
                                    className='event-textbox'
                                    validate={required}/>
                            </div>

                            <div className="form-row event-form clearfix">
                                <label>Description
                                </label>
                                <Field
                                    placeholder='Description'
                                    component={renderField}
                                    type='textarea'
                                    name='event-fm'
                                    className='event-textbox'
                                    validate={required}/>
                            </div>
                            <div className="row clearfix">
                            
                                <div className="event-form-date">
                                    <Field
                                        name='startdate'
                                        component={props => {
                                        return (<ReactDatePicker
                                            {...props}
                                            label='Start Date '
                                            reactDatePicker={this.state.ReactDatePicker}
                                            moment={this.state.Moment}
                                            validate={[required]}/>)
                                    }}/>
                                </div>
                                <div className="event-form-date">
                                    <Field
                                        name='startdate'
                                        component={props => {
                                        return (<ReactDatePicker
                                            {...props}
                                            label='To Date '
                                            reactDatePicker={this.state.ReactDatePicker}
                                            moment={this.state.Moment}
                                            validate={[required]}/>)
                                    }}/>

                                </div>
                            </div>

                            <div className="form-row event-form clearfix">

                                {/* <div>
                                    <ReactTags
                                        tags={tags}
                                        handleDelete={this.handleDelete}
                                        handleAddition={this.handleAddition}
                                        handleDrag={this.handleDrag}/>
                                </div> */}

                            </div>
                            <div className="form-row btn-int">
                                <button type="submit" class="btn btn-primary btn-integration">
                                    Submit</button>
                            </div>

                        </form>
                    : ''}
                {this.props.Location_form == "live"
                    ? <form role="form">
                            <div className="form-row event-form clearfix">
                                <label>Event</label>
                                <Field
                                    placeholder='Name'
                                    component={renderField}
                                    type='text'
                                    name='event-fm'
                                    className='event-textbox'
                                    validate={required}/>
                            </div>

                            <div className="form-row event-form clearfix">
                                <label>Description
                                </label>
                                <Field
                                    placeholder='Description'
                                    component={renderField}
                                    type='textarea'
                                    name='event-fm'
                                    className='event-textbox'
                                    validate={required}/>
                            </div>
                            <div className="row clearfix">
                                <div className="event-form-date">
                                    <Field
                                        name='startdate'
                                        component={props => {
                                        return (<ReactDatePicker
                                            {...props}
                                            label='Start Date '
                                            reactDatePicker={this.state.ReactDatePicker}
                                            moment={this.state.Moment}
                                            validate={[required]}/>)
                                    }}/>
                                </div>
                                <div className="event-form-date">
                                    <Field
                                        name='To Date'
                                        component={props => {
                                        return (<ReactDatePicker
                                            {...props}
                                            label='To Date '
                                            reactDatePicker={this.state.ReactDatePicker}
                                            moment={this.state.Moment}
                                            validate={[required]}/>)
                                    }}/>
                                </div>
                            </div>

                            <div className="form-row event-form clearfix">

                                <div>
                                    <ReactTags
                                        tags={tags}
                                        handleDelete={this.handleDelete}
                                        handleAddition={this.handleAddition}
                                        handleDrag={this.handleDrag}/>
                                </div>

                            </div>
                            <div className="form-row btn-int">
                                <button type="submit" class="btn btn-primary btn-integration">
                                    Submit</button>
                            </div>

                        </form>
                    : ''}
                {this.props.Location_form == "expired"
                    ? <form role="form">
                            <div className="form-row event-form clearfix">
                                <label>Event</label>
                                <Field
                                    placeholder='Name'
                                    component={renderField}
                                    type='text'
                                    name='event-fm'
                                    className='event-textbox'
                                    validate={required}/>
                            </div>

                            <div className="form-row event-form clearfix">
                                <label>Description
                                </label>
                                <Field
                                    placeholder='Description'
                                    component={renderField}
                                    type='textarea'
                                    name='event-fm'
                                    className='event-textbox'
                                    validate={required}/>
                            </div>
                            <div className="row clearfix">
                                <div className="event-form-date">
                                    <Field
                                        name='startdate'
                                        component={props => {
                                        return (<ReactDatePicker
                                            {...props}
                                            label='Start Date '
                                            reactDatePicker={this.state.ReactDatePicker}
                                            moment={this.state.Moment}
                                            validate={[required]}/>)
                                    }}/>
                                </div>
                                <div className="event-form-date">
                                    <Field
                                        name='To Date'
                                        component={props => {
                                        return (<ReactDatePicker
                                            {...props}
                                            label='To Date '
                                            reactDatePicker={this.state.ReactDatePicker}
                                            moment={this.state.Moment}
                                            validate={[required]}/>)
                                    }}/>
                                </div>
                            </div>

                            <div className="form-row event-form clearfix">

                                <div>
                                    <ReactTags
                                        tags={tags}
                                        handleDelete={this.handleDelete}
                                        handleAddition={this.handleAddition}
                                        handleDrag={this.handleDrag}/>
                                </div>

                            </div>
                            <div className="form-row btn-int">
                                <button type="submit" class="btn btn-primary btn-integration">
                                    Submit</button>
                            </div>

                        </form>
                    : ''}
            </div>
        )
    }
}
export default reduxForm({
    form: 'EventForm', // a unique identifier for this form
})(EventForm)