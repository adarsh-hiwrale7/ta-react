import Collapse, {Panel} from 'rc-collapse' // collapse
// collapse css
import {IndexLink, Link} from 'react-router'
import * as utils from '../../utils/utils'

class EventNav extends React.Component {
  constructor(props) {
    super(props)
  }
  render() {
    var currentFilter = this.props.location;
    return (
      <nav className='nav-side'>
        <Collapse accordion={false} defaultKey={'1'} defaultActiveKey={['1']}>
          <Panel className='active' header='STATUS' key={'1'}>
            {(() => {
              return (
                <ul className='parent  clearfix'>
                  <li>
                    <Link
                      className={currentFilter == 'all'
                      ? ` active`
                      : ``}
                      to={'/event/all'}>
                      <span className='text'>All</span>
                    </Link>
                  </li>
                  <li>
                    <Link
                      className={currentFilter == 'live'
                      ? ` active`
                      : ``}
                      to={`/event`}>
                      <span className='text'>Live</span>
                    </Link>
                  </li>
                  <li>
                    <Link
                      className={currentFilter == 'expired'
                      ? ` active`
                      : ``}
                      to={`/event/expired`}>
                      <span className='text'>Expired</span>
                    </Link>
                  </li>
                  <li></li>
                </ul>
              )
            })()}
          </Panel>
        </Collapse>
      </nav>
    )
  }
}
module.exports = EventNav
