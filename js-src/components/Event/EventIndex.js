import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import {Link} from 'react-router'
import SidebarNarrow from '../Layout/Sidebar/SidebarNarrow'
import Header from '../Header/Header';
import * as utils from '../../utils/utils'
import EventNav from "./EventNav"
import SliderPopup from '../SliderPopup'
import EventForm from "./EventForm"
import EventListView from "./EventListView"
class EventIndex extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      currentFilter: '',
      eventPopup: false,
      editPopup: false

    }
  }
  componentWillMount() {
    this.setState({editPopup: false})
    document
      .body
      .classList
      .remove('overlay')
  }
  EditEventForm() {
    var route = this.props.route.path;
    return (
     <div>
     <EventForm Location_form={this.props.route.name}/>
     </div>
    )
  }
  editEventForm() {
    this.setState({
      editPopup: !this.state.editPopup
    })
  }
  ReadOnlytEventForm() {
    var route = this.props.route.name;
    return (
      <div>
        {route == "live"
          ? <div className="read_form">
              <div className="clearfix form-row">
                <label>
                  Name
                </label>
                <p>
                  kinjal Birare</p>
              </div>
              <div className="clearfix form-row">
                <label>
                  Description
                </label>
                <p>
                Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line
                </p>
              </div>
              <div className="clearfix form-row">
                <label>
                  To Date
                </label>
                <p>8-9-2017</p>
              </div>
              <div className="clearfix form-row">
                <label>
                  From Date
                </label>
                <p>
                  7-9-2018</p>
              </div>
              <div className="clearfix form-row">
                <label>
                  invitees
                </label>
                <p>
                Contrary to popular belief, Lorem Ipsum is roots in a piece of classical Latin literature.
                 </p>
              </div>
            </div>
          : ""}
        {route == "all"
          ? <div className="read_form">
              <div className="clearfix form-row">
                <label>
                  Name
                </label>
                <p>
                  kinjal Birare R.</p>
              </div>
              <div className="clearfix form-row">
                <label>
                  Description
                </label>
                <p>bcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabc</p>
              </div>
              <div className="clearfix form-row">
                <label>
                  To Date
                </label>
                <p>8-9-2017</p>
              </div>
              <div className="clearfix form-row">
                <label>
                  From Date
                </label>
                <p>
                  1-11-2018</p>
              </div>
              <div className="clearfix form-row">
                <label>
                  invitees
                </label>
                <p>
                  abc abc abc abc abc abc abc abc abc abc abc abc abc abc abc abc abc abc</p>
              </div>
            </div>
        : ''}
        {route == "expired"
          ? <div className="read_form">
              <div className="clearfix form-row">
                <label>
                  Name
                </label>
                <p>
                  kinjal Birare</p>
              </div>
              <div className="clearfix form-row">
                <label>
                  Description
                </label>
                <p>abcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabc</p>
              </div>
              <div className="clearfix form-row">
                <label>
                  To Date
                </label>
                <p>8-9-2017</p>
              </div>
              <div className="clearfix form-row">
                <label>
                  From Date
                </label>
                <p>
                  7-9-2018</p>
              </div>
              <div className="clearfix form-row">
                <label>
                  invitees
                </label>
                <p>
                  abc abc abc abc abc abc abc abc abc abc abc abc abc abc abc abc abc abc</p>
              </div>
            </div>
        : ''}
      </div>
    )
  }
  eventPopup() {
    this.setState({eventPopup: true})
    document
      .body
      .classList
      .add('overlay')
  }
  closeEventPopup() {
    this.setState({eventPopup: false})
    document
      .body
      .classList
      .remove('overlay')
  }
  renderEventPopup() {
    return (
      <SliderPopup wide>

        <button id='close-popup' className='btn-default'>
          <a>
            <i
              className='material-icons'
              onClick={this
              .closeEventPopup
              .bind(this)}>clear</i>
          </a>

        </button>
        <div className="header-menu">
          <ul className='dd-menu context-menu campaign_3dots'>
            <li className='button-dropdown'>
              <a className='dropdown-toggle '>
                <i className='material-icons'>more_vert</i>
              </a>
              <ul className='dropdown-menu'>

                <li class="dropdown-parent-li">
                  <ul>
                    <li>
                      <a
                        onClick={this
                        .editEventForm
                        .bind(this)}>
                        Edit
                      </a>
                    </li>
                  </ul>
                </li>
              </ul>
            </li>
          </ul>
        </div>
        <div className="event-edit-form-container">

          {this.state.editPopup
            ? this.EditEventForm()
            : this.ReadOnlytEventForm()}
        </div>
      </SliderPopup>
    )
  }
  render() {
    return (
      <section className='home-page' id='event'>
        {this.state.eventPopup
          ? this.renderEventPopup()
          : ''}
        <Header
          title="event"
          location={this.props.route.name}
          nav={EventNav}
          event_new_openpopup={this
          .eventPopup
          .bind(this)}
          ></Header>
        <div className='main-container'>
          <div id='content'>
            <div className='page '>
              <div className='full-container-event event-container clearfix'>
                {/* <div id="dropdown-asset-container">
                  <div className="dropdown-container clearfix">
                    <div className="clearfix">
                      <div className="filter-right-section">
                        <Link
                          to={'/campaigns/calendar/' + this.state.currentFilter}
                          className='btn btn-default'>
                          <i class="material-icons" title='Calendar View'>today</i> 
                        </Link>
                        <Link to={'/campaigns/' + this.state.currentFilter} className='btn btn-theme'>
                          <i className='material-icons' aria-hidden='true' title='List View'>view_headline</i>
                        </Link>
                      </div>
                    </div>
                  </div>
                </div> */}
                <div className="main-event-container">

                  <EventListView
                    eventPopup={this
                    .eventPopup
                    .bind(this)}
                    Location={this.props.route.name}
                    path= {this.props.location.pathname}/>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    )
  }
  componentDidMount() {}
}
function mapStateToProps(state) {
  return {}
}

function mapDispatchToProps(dispatch) {
  return {}
}

let connection = connect(mapStateToProps, mapDispatchToProps)
module.exports = connection(EventIndex)
