export default class PreLoader extends React.Component {
	render() {
        var index=2;
        if(this.props.isBottom==true){
            index = 1;
        }
		return (
	    <div>
           {
             this.props.bottomLoader?
            //  feed loader
             <div id="feed-loader">
                         <div className = "curated-loader-container Job-loader-container">
                           <div className="curated-loader-inner-container">
                               <div className = "curated-header clearfix">
                                   <div className = "img-avatar"> </div>
                                   <div className = "name-header">
                                       <div className = "name-curated loader-line-height"> </div>
                                       <div className = "date-curated loader-line-height"> </div>
                                   </div>
                                   </div>
                                 <div className = "clearfix curated-block-container">
                         
                                       <div className = "inner-curated-block">
                                           <div className = "curated-decripation">
                                             <div className = "clearfix date-and-share-block">
                                                   <div className ="cureted-date loader-grey-line loader-line-space loader-line-radius"> </div>
                                                   <div className ="cureted-share loader-grey-line loader-line-space loader-line-radius"> </div>
                                             </div>
                                               <div className = "cureted-decp-block4 loader-grey-line loader-line-space loader-line-radius loader-line-height"> </div>
                                               <div className = "cureted-decp-block3 loader-grey-line loader-line-space loader-line-radius loader-line-height"> </div>
                                               <div className = "cureted-decp-block1 loader-grey-line loader-line-space loader-line-radius loader-line-height"> </div>
                                               <div className="cureted-decp-line-padding"></div>
                                           </div>
                                           <div className = "curated-img-part loader-grey-line"></div>                                      
                                 </div> 
                         </div>
                         </div>
                       </div>
                     </div>
            :
            // moderation  asset loader
            this.props.location=='moderation-assets'?
            <div>
            {
            Array(index).fill(1).map((el, i) =>
            <div class="post modeartion-asset-loader" key={i}>
            <div class="clearfix postInner">
              <div class="moderationAssetThumb"><a>
                <div class="thumbWrap file-image type-image loader-grey-line">

                </div>
              </a></div>
              <div class="moderationAssetDetail">
                <div class="post-header">
                  <div class="user-profile loader-grey-line">

                  </div>
                  <div class="author-data">
                    <div class="author-name loader-grey-line loader-line-space loader-line-radius loader-line-height">
                      <span class="hidden-xs">

                      </span></div>
                    <div class="post-timing loader-grey-line loader-line-space loader-line-radius loader-line-height"></div>
                  </div>
                </div>
                <h4>
                  <div class="description loader-grey-line loader-line-space loader-line-radius loader-line-height"></div>

                </h4>
              </div>
            </div>
            <div class="moderationAssetApprovalActions">
              <div class="approval-actions clearfix">
                <div class="approval-box-loader">
                  <button class="loader-grey-line  loader-line-height moderation-loader-button approval-button">
                  </button>
                </div>
                <div class="unapproval-box">
                  <button class="loader-grey-line  loader-line-height moderation-loader-button unapproval-button">
                  </button>
                  <div>
                  </div>
                </div>
              </div>
            </div>
            </div>
            )}
            </div>
            : 
            this.props.feedLoader?
            <div id={`${this.props.isBottom==true ?'feed-loader' : 'feed-page-loader'}`}>
            {Array(index).fill(1).map((el, i) =>
                    <div className="feed-loader-wrapper" key={`feedloader-${i}`}>
                        <div className = "curated-loader-container Job-loader-container">
                          <div className="curated-loader-inner-container">
                              <div className = "curated-header clearfix">
                                  <div className = "img-avatar"> </div>
                                  <div className = "name-header">
                                      <div className = "name-curated loader-line-height"> </div>
                                      <div className = "date-curated loader-line-height"> </div>
                                  </div>
                                  </div>
                                <div className = "clearfix curated-block-container">
                        
                                      <div className = "inner-curated-block">
                                          <div className = "curated-decripation">
                                            <div className = "clearfix date-and-share-block">
                                                  <div className ="cureted-date loader-grey-line loader-line-space loader-line-radius"> </div>
                                                  <div className ="cureted-share loader-grey-line loader-line-space loader-line-radius"> </div>
                                            </div>
                                              <div className = "cureted-decp-block4 loader-grey-line loader-line-space loader-line-radius loader-line-height"> </div>
                                              <div className = "cureted-decp-block3 loader-grey-line loader-line-space loader-line-radius loader-line-height"> </div>
                                              <div className = "cureted-decp-block1 loader-grey-line loader-line-space loader-line-radius loader-line-height"> </div>
                                              <div className="cureted-decp-line-padding"></div>
                                          </div>
                                          <div className = "curated-img-part loader-grey-line"></div>                                      
                                </div> 
                        </div>
                        </div>
                      </div>
                      </div>
              )}
            </div>
            :
            // modearation post loader
            this.props.location == 'moderation-posts' ?
            <div >
            {
                Array(index).fill(1).map((el, i) =>
                <div className="moderation-loader" key={`moderation-${i}`}>
                <div className = "curated-loader-container Job-loader-container moderation-post-loader">
                  <div className="curated-loader-inner-container">
                      <div className = "curated-header clearfix">
                          <div className = "img-avatar"> </div>
                          <div className = "name-header">
                              <div className = "name-curated loader-line-height"> </div>
                              <div className = "date-curated loader-line-height"> </div>
                          </div>
                          </div>
                        <div className = "clearfix curated-block-container">
                
                              <div className = "inner-curated-block">
                                  <div className = "curated-decripation">
                                    <div className = "clearfix date-and-share-block">
                                          <div className ="cureted-date loader-grey-line loader-line-space loader-line-radius"> </div>
                                          <div className ="cureted-share loader-grey-line loader-line-space loader-line-radius"> </div>
                                    </div>
                                      <div className = "cureted-decp-block4 loader-grey-line loader-line-space loader-line-radius loader-line-height"> </div>
                                      <div className = "cureted-decp-block3 loader-grey-line loader-line-space loader-line-radius loader-line-height"> </div>
                                      <div className = "cureted-decp-block1 loader-grey-line loader-line-space loader-line-radius loader-line-height"> </div>
                                      <div className="cureted-decp-line-padding"></div>
                                  </div>
                                  <div className = "curated-img-part loader-grey-line"></div>                                      
                        </div>
                        
                </div>
                </div>
              </div>
                  <div class="moderationAssetApprovalActions">
                    <div class="approval-actions clearfix">
                            <div class="approval-box-loader">
                            <button class="loader-grey-line  loader-line-height moderation-loader-button approval-button">
                            </button>
                            </div>
                            <div class="unapproval-box">
                            <button class="loader-grey-line loader-line-height moderation-loader-button unapproval-button">
                            </button>
                            <div>
                            </div>
                            </div>

                        </div>
                        </div>
                        </div>
                  )} 
            </div>
            :
            // leaderboard loader
              this.props.location=="leaderboard-loader"?
              <div class="leaderboard-wrap">
                      <h2 class="top-employee-title loader-grey-line loader-line-space loader-line-radius loader-line-height loader-employee-heading"></h2>
                      <div class="clearfix">
                        <div class="top-employees">
                          <div class="inner-employee-loader clearfix">
                            <div>
                              <div class="index-property  currentuser">
          
                              </div>
                              <div class="text-part  currentuser-textprt leaderboard-box-wrapper">
                                <div class="inner-text-part-container clearfix">
                                  <div class="flex-item-text-part clearfix leaderboard-name-col ">
                                    <div class="loader-avtar-image loader-grey-line loader-line-space loader-line-radius loader-line-height loader-employee-heading">
                                    </div>
                                    <div class="text-header loader-text-header">
                                      <div class="name loader-top-avtar-name loader-grey-line loader-line-space loader-line-radius loader-line-height loader-employee-heading"></div>
                                      <div class="name2 loader-top-avtar-position loader-grey-line loader-line-space loader-line-radius loader-line-height loader-employee-heading loader-top-space"></div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="top-employees">
                          <div class="inner-employee-loader clearfix">
                            <div>
                              <div class="index-property  currentuser">
          
                              </div>
                              <div class="text-part  currentuser-textprt leaderboard-box-wrapper">
                                <div class="inner-text-part-container clearfix">
                                  <div class="flex-item-text-part clearfix leaderboard-name-col ">
                                    <div class="loader-avtar-image loader-grey-line loader-line-space loader-line-radius loader-line-height loader-employee-heading">
                                    </div>
                                    <div class="text-header loader-text-header">
                                      <div class="name loader-top-avtar-name loader-grey-line loader-line-space loader-line-radius loader-line-height loader-employee-heading"></div>
                                      <div class="name2 loader-top-avtar-position loader-grey-line loader-line-space loader-line-radius loader-line-height loader-employee-heading loader-top-space"></div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="top-employees">
                          <div class="inner-employee-loader clearfix">
                            <div>
                              <div class="index-property  currentuser">
          
                              </div>
                              <div class="text-part  currentuser-textprt leaderboard-box-wrapper">
                                <div class="inner-text-part-container clearfix">
                                  <div class="flex-item-text-part clearfix leaderboard-name-col ">
                                    <div class="loader-avtar-image loader-grey-line loader-line-space loader-line-radius loader-line-height loader-employee-heading">
                                    </div>
                                    <div class="text-header loader-text-header">
                                      <div class="name loader-top-avtar-name loader-grey-line loader-line-space loader-line-radius loader-line-height loader-employee-heading"></div>
                                      <div class="name2 loader-top-avtar-position loader-grey-line loader-line-space loader-line-radius loader-line-height loader-employee-heading loader-top-space"></div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="other-employees">
                          <h2 class="loader-grey-line loader-line-space loader-line-radius loader-line-height loader-employee-heading loader-heading-responsive"></h2>
                          <div class="inner-employee-loader clearfix">
                            <div class="current">
                              <div class="index-property">
          
                              </div>
                              <div class="text-part loader-text-wrapper">
                                <div class="inner-text-part-container clearfix">
                                  <div class="flex-item-text-part clearfix leaderboard-name-col">
                                    <div class="other-employee-avtar loader-grey-line  loader-line-radius loader-line-height loader-employee-heading">
                                    </div>
                                    <div class="text-header">
                                      <div class="name loader-avtar-name loader-grey-line  loader-line-radius loader-line-height loader-employee-heading loader-other-employee-name">
          
                                      </div>
                                      <div class="name2 loader-avtar-position loader-grey-line loader-line-space loader-line-radius loader-line-height loader-employee-heading loader-other-employee-name">
          
                                      </div>
                                    </div>
                                  </div>
                                  <div class="flex-item-text-part clearfix leaderboard-number-col">
          
          
          
                                  </div>
          
                                  <div class="up-aero ">
                                    <div class="leaderboard-level-image">
                                    </div>
                                    <span class="number">
                                    </span>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="inner-employee-loader clearfix">
                            <div class="current">
                              <div class="index-property">
          
                              </div>
                              <div class="text-part loader-text-wrapper">
                                <div class="inner-text-part-container clearfix">
                                  <div class="flex-item-text-part clearfix leaderboard-name-col">
                                    <div class="other-employee-avtar loader-grey-line  loader-line-radius loader-line-height loader-employee-heading">
                                    </div>
                                    <div class="text-header">
                                      <div class="name loader-avtar-name loader-grey-line  loader-line-radius loader-line-height loader-employee-heading loader-other-employee-name">
          
                                      </div>
                                      <div class="name2 loader-avtar-position loader-grey-line loader-line-space loader-line-radius loader-line-height loader-employee-heading loader-other-employee-name">
          
                                      </div>
                                    </div>
                                  </div>
                                  <div class="flex-item-text-part clearfix leaderboard-number-col">
          
          
          
                                  </div>
          
                                  <div class="up-aero ">
                                    <div class="leaderboard-level-image">
                                    </div>
                                    <span class="number">
                                    </span>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="inner-employee-loader clearfix">
                            <div class="current">
                              <div class="index-property">
          
                              </div>
                              <div class="text-part loader-text-wrapper">
                                <div class="inner-text-part-container clearfix">
                                  <div class="flex-item-text-part clearfix leaderboard-name-col">
                                    <div class="other-employee-avtar loader-grey-line  loader-line-radius loader-line-height loader-employee-heading">
                                    </div>
                                    <div class="text-header">
                                      <div class="name loader-avtar-name loader-grey-line  loader-line-radius loader-line-height loader-employee-heading loader-other-employee-name">
          
                                      </div>
                                      <div class="name2 loader-avtar-position loader-grey-line loader-line-space loader-line-radius loader-line-height loader-employee-heading loader-other-employee-name">
          
                                      </div>
                                    </div>
                                  </div>
                                  <div class="flex-item-text-part clearfix leaderboard-number-col">
          
          
          
                                  </div>
          
                                  <div class="up-aero ">
                                    <div class="leaderboard-level-image">
                                    </div>
                                    <span class="number">
                                    </span>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        
                      </div>
                  </div>
              :
            // rss loader
            this.props.feedtype=='rss'?
               <div className = "curated-loader-container">
                   <div className="curated-loader-inner-container">
                          <div className = "curated-header clearfix">
                             <div className = "img-avatar"> </div>
                             <div className = "name-header">
                                  <div className = "name-curated loader-line-height"> </div>
                                  <div className = "date-curated loader-line-height"> </div>
                              </div>
                           </div>
                           <div className = "clearfix curated-block-container">
                           {/* block1 */}
                               <div className = "curated-block one-curated-block">
                                 <div className = "inner-curated-block">
                                     <div className = "curated-img-part loader-grey-line"></div>
                                     <div className = "curated-decripation">
                                        <div className = "cureted-decp-block1 loader-grey-line loader-line-space loader-line-radius loader-line-height"> </div>
                                        <div className = "cureted-decp-block2 loader-grey-line loader-line-space loader-line-radius loader-line-height"> </div>
                                        <div className = "clearfix date-and-share-block">
                                             <div className ="cureted-date loader-grey-line loader-line-space loader-line-radius"> </div>
                                             <div className ="cureted-share loader-grey-line loader-line-space loader-line-radius"> </div>
                                        </div>
                                          <div className = "cureted-decp-block1 loader-grey-line loader-line-space loader-line-radius loader-line-height cureted-decp-line-padding"> </div>
                                          <div className = "cureted-decp-block3 loader-grey-line loader-line-space loader-line-radius loader-line-height "> </div>
                                          <div className = "cureted-decp-block4 loader-grey-line loader-line-space loader-line-radius loader-line-height"> </div>
                                      </div>
                                 </div>
                              </div> 

                              {/* blcok2 */}
                              <div className = "curated-block one-curated-block">
                                 <div className = "inner-curated-block">
                                     <div className = "curated-img-part loader-grey-line"></div>
                                     <div className = "curated-decripation">
                                        <div className = "cureted-decp-block1 loader-grey-line loader-line-space loader-line-radius loader-line-height"> </div>
                                        <div className = "cureted-decp-block2 loader-grey-line loader-line-space loader-line-radius loader-line-height"> </div>
                                        <div className = "clearfix date-and-share-block">
                                             <div className ="cureted-date loader-grey-line loader-line-space loader-line-radius"> </div>
                                             <div className ="cureted-share loader-grey-line loader-line-space loader-line-radius"> </div>
                                        </div>
                                          <div className = "cureted-decp-block1 loader-grey-line loader-line-space loader-line-radius loader-line-height cureted-decp-line-padding"> </div>
                                          <div className = "cureted-decp-block3 loader-grey-line loader-line-space loader-line-radius loader-line-height "> </div>
                                          <div className = "cureted-decp-block4 loader-grey-line loader-line-space loader-line-radius loader-line-height"> </div>
                                      </div>
                                 </div>
                              </div>

                            </div>
                      </div>
                   </div>
          :
          // jobs loader
          this.props.feedtype=='jobs'?
                    /* start div for job */
                     <div className = "curated-loader-container Job-loader-container">
                     <div className="curated-loader-inner-container">
                          <div className = "curated-header clearfix">
                             <div className = "img-avatar"> </div>
                             <div className = "name-header">
                                  <div className = "name-curated loader-line-height"> </div>
                                  <div className = "date-curated loader-line-height"> </div>
                              </div>
                           </div>
                           <div className = "clearfix curated-block-container">
                           {/* block1 */}
                               <div className = "curated-block one-curated-block">
                                 <div className = "inner-curated-block">
                                       <div className = "job-descripation-part">

                                          <div class="job-block1 "> </div>
                                          <div class="job-block2 loader-grey-line loader-line-radius loader-line-height"> </div>
                                          <div class="job-block3 "> </div>
                                          <div class="job-block4 loader-grey-line  loader-line-radius loader-line-height"> </div>
                                          <div class="job-block5"> </div>
                                          <div class="job-block6 loader-grey-line loader-line-radius loader-line-height"> </div>
                                          <div class="job-block7"> </div>
                                          <div class="job-block8 loader-grey-line loader-line-radius loader-line-height"> </div>
                                          <div class="job-block9"> </div>
                                          <div class="job-block10 loader-grey-line loader-line-radius loader-line-height"> </div>
                                          <div class="job-block11 loader-grey-line loader-line-space loader-line-radius loader-line-height"> </div>
                                          <div class="job-block12 loader-grey-line loader-line-space loader-line-radius loader-line-height"> </div>
                                          <div class="job-block13 loader-grey-line loader-line-space loader-line-radius loader-line-height"> </div>
                                          <div class="job-block1 "> </div>
                                          <div class="job-block2 loader-grey-line loader-line-radius loader-line-height"> </div>
                                          <div class = "job-last-div"> </div>
                                          </div>

                                 </div>
                              </div> 

                              {/* blcok2 */}
                               <div className = "curated-block one-curated-block">
                                 <div className = "inner-curated-block">
                                       <div className = "job-descripation-part">

                                          <div class="job-block1"> </div>
                                          <div class="job-block2 loader-grey-line loader-line-radius loader-line-height"> </div>
                                          <div class="job-block3"> </div>
                                          <div class="job-block4 loader-grey-line  loader-line-radius loader-line-height"> </div>
                                          <div class="job-block5"> </div>
                                          <div class="job-block6 loader-grey-line loader-line-radius loader-line-height"> </div>
                                          <div class="job-block7"> </div>
                                          <div class="job-block8 loader-grey-line loader-line-radius loader-line-height"> </div>
                                          <div class="job-block9"> </div>
                                          <div class="job-block10 loader-grey-line loader-line-radius loader-line-height"> </div>
                                          <div class="job-block11 loader-grey-line loader-line-space loader-line-radius loader-line-height"> </div>
                                          <div class="job-block12 loader-grey-line loader-line-space loader-line-radius loader-line-height"> </div>
                                          <div class="job-block13 loader-grey-line loader-line-space loader-line-radius loader-line-height"> </div>
                                          <div class="job-block1"> </div>
                                          <div class="job-block2 loader-grey-line loader-line-radius loader-line-height"> </div>
                                          <div class = "job-last-div"> </div>
                                          </div>

                                 </div>
                              </div>
                              {/* end blockj */}

                            </div>
                   </div>
                    {/* end div for jobs */}
            </div>
        :
        this.props.location=="assets-loader"?
              // <div id='content'>
              // <div className='page'>
              // <div className='full-container'>
              //   <div className='asset-container'>
              //   <div className='esc-2'>
                <div className='clearfix parent_assets'>
                <div className="render-assets">
                {/* folder loader */}
                {this.props.userAssets!==true?
                <div className='archive-container clearfix'>
                {
                  Array(3).fill(1).map((el, i) => 
                  <div class="archive asset-dir" key={`assetsLoader-${i}`}>
                    <a class="file-details file-details-loader-assets">
                        <span class="side">
                           <div className = "icon-loader loader-grey-line  loader-line-radius"> </div>
                        </span>
                        <p class="folder_name_container">
                            <span class=" foldername_assets">
                            <div className = "loader-grey-line  loader-line-radius loader-line-height"> </div>
                                <div className = "loader-grey-line  loader-line-space loader-line-radius loader-line-height"> </div>
                            </span>
                        </p>
                    </a>
                    </div> 
                  )}
                
                </div>
                :''}
                
                {/*  single asset loader */}
                <div id="container">
                <div className='archive-container asset-file-container clearfix'>
                {
                  Array(3).fill(1).map((el, i) => 
                
                <div class="vid file-image asset-file imgwrapper" key={`assetLoader-${i}`}>
                  <div class="inner_imgwrapper assetInnerWrapper">
                    <a class="type-image">
                        <span class="imageloader loaded thumbnail loader-assets-image">
                            <img src="/img/visiblyLoader.gif" />
                        </span>
                    </a>
                </div>
                <a class="file-details">
                    <span class="title">
                        <div className="loader-grey-line loader-line-height loader-line-radius loader-assets-title"> </div>
                    </span>
                </a>
              </div>
              )}
              </div>
              
              </div>
              {/* trending tag loader start */}
              <div className="render-trendng">
                <div className="asset-trending-contaner">
                  {/*start trending tag desing static */}
                  <div className="trending-xl-container">
                    <div className="trending-tag-container">
                          <div className="inner-trending-tag">
                                <div className="trending-header">
                                  <div className="loader-grey-line loader-line-height loader-line-radius"> </div>
                                </div>
                                <div className="tranding-body"> 
                                  <div className="inner-trending-body">
                                      <div className="text-part">
                                            <p className= "loader-grey-line loader-line-height loader-line-space loader-line-radius"> </p> 
                                            <p className= "loader-grey-line loader-line-height loader-line-space loader-line-radius"> </p>
                                            <p className= "loader-grey-line loader-line-height loader-line-space loader-line-radius"> </p>
                                            <p className= "loader-grey-line loader-line-height loader-line-space loader-line-radius"> </p>
                                            <p className= "loader-grey-line loader-line-height loader-line-space loader-line-radius"> </p>
                                            <p className= "loader-grey-line loader-line-height loader-line-space loader-line-radius"> </p>
                                      </div>
                                  </div>
                                </div>
                          </div>
                      </div>
              </div>
              </div>
              </div>
              {/* trending tag loader ends  */}
              </div>
              </div>
              // </div>
              // </div>
              // </div>
              // </div>
              // </div>
        :
        this.props.location=="campaign-loader" || this.props.location=="userList-Loader" ?
        <div className='page campaign-container page with-filters'>
        <div class='content'>
        <div className='table-white-container'>
            <div className="CampaignList">
            <div  className = " loader-culture-tableExpand"> 
            <div class="widget">
              <div class="table-anytlics-body">
                  <table class="table responsive-table">
                      <thead>
                          <tr class="reactable-column-header">
                              <th class="reactable-th-serialno reactable-header-sortable serialno  a-center"> 
                              <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                              </th>
                              <th class="reactable-th-serialno reactable-header-sortable serialno  a-center"> 
                              <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                              </th>
                              <th class="reactable-th-serialno reactable-header-sortable serialno  a-center"> 
                              <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                              </th>
                              <th class="reactable-th-serialno reactable-header-sortable serialno  a-center"> 
                              <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                              </th>
                              <th class="reactable-th-serialno reactable-header-sortable serialno  a-center"> 
                              <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                              </th>
                              <th class="reactable-th-serialno reactable-header-sortable serialno  a-center"> 
                              <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                              </th>
                              <th class="reactable-th-serialno reactable-header-sortable serialno  a-center"> 
                              <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                              </th>
                              <th class="reactable-th-serialno reactable-header-sortable serialno  a-center"> 
                              <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                              </th>
                              <th class="reactable-th-serialno reactable-header-sortable serialno  a-center"> 
                              <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                              </th>
                          </tr>
                      </thead>
                      <tbody class="reactable-data">
                        <tr class="table-body-text">
                          <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                          <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                          <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                          <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                          <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                          <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                          <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                          <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                          <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                          
                        </tr>
                        <tr class="table-body-text">
                          <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                          <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                          <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                          <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                          <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                          <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                          <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                          <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                          <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                          
                        </tr>
                        <tr class="table-body-text blanck-div-loader-totalpost">
                          <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                          <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                          <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                          <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                          <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                          <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                          <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                          <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                          <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                          
                        </tr>
                        <tr class="blanck-div-loader-totalpost table-body-text"><td></td> <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td> </tr>
                        <tr class="blanck-div-loader-totalpost table-body-text"><td></td> <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td> </tr>
                        <tr class="blanck-div-loader-totalpost table-body-text"><td></td> <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td> </tr>
                        <tr class="blanck-div-loader-totalpost table-body-text"><td></td> <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td> </tr>
                        <tr class="blanck-div-loader-totalpost table-body-text"><td></td> <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td> </tr>
                        <tr class="blanck-div-loader-totalpost table-body-text"><td></td> <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td> </tr>

                      </tbody>
                  </table>
              </div>
          </div>
                  </div>
          </div>
          </div>
          </div>
          </div>
        :
        (this.props.location=="cultureList-loader" || this.props.location=="department-loader")?
        <div id="content">
        <div className="page campaign-container">
        <div className="full-container">
        <div className="membersListing">
        <div className='table-white-container'>
        <div className="widget">
              <div  className = " loader-culture-tableExpand"> 
              <div>
              <div class="table-anytlics-body">
                  <table class="table responsive-table">
                      <thead>
                          <tr class="reactable-column-header">
                              <th class="reactable-th-serialno reactable-header-sortable serialno  a-center"> 
                              <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                              </th>
                              <th class="reactable-th-serialno reactable-header-sortable serialno  a-center"> 
                              <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                              </th>
                              <th class="reactable-th-serialno reactable-header-sortable serialno  a-center"> 
                              <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                              </th>
                              <th class="reactable-th-serialno reactable-header-sortable serialno  a-center"> 
                              <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                              </th>
                            
                          </tr>
                      </thead>
                      <tbody class="reactable-data">
                        <tr class="table-body-text">
                          <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                          <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                          <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                          <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                        </tr>
                        <tr class="table-body-text">
                          <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                          <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                          <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                          <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                        </tr>
                        <tr class="table-body-text blanck-div-loader-totalpost">
                          <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                          <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                          <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                          <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                        </tr>
                        <tr class="blanck-div-loader-totalpost table-body-text"><td></td> <td></td><td></td><td></td></tr>
                        <tr class="blanck-div-loader-totalpost table-body-text"><td></td> <td></td><td></td><td></td></tr>
                        <tr class="blanck-div-loader-totalpost table-body-text"><td></td> <td></td><td></td><td></td></tr>
                        <tr class="blanck-div-loader-totalpost table-body-text"><td></td> <td></td><td></td><td></td></tr>
                    
                      </tbody>
                  </table>
              </div>
              </div>
              </div>
              </div>
              </div>
              </div>
              </div>
              </div>
              </div>
        :
        this.props.location=="invoices-loader"?
            <div>
              <table class="table responsive-table">
                      <thead>
                          <tr class="reactable-column-header">
                              <th class="reactable-th-serialno reactable-header-sortable serialno  a-center"> 
                              <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                              </th>
                              <th class="reactable-th-serialno reactable-header-sortable serialno  a-center"> 
                              <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                              </th>
                              <th class="reactable-th-serialno reactable-header-sortable serialno  a-center"> 
                              <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                              </th>
                              <th class="reactable-th-serialno reactable-header-sortable serialno  a-center"> 
                              <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                              </th>
                              <th class="reactable-th-serialno reactable-header-sortable serialno  a-center"> 
                              <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                              </th>
                              <th class="reactable-th-serialno reactable-header-sortable serialno  a-center"> 
                              <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                              </th>
                            
                          </tr>
                      </thead>
                      <tbody class="reactable-data">
                        <tr class="table-body-text">
                          <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                          <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                          <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                          <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                          <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                          <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                        </tr>
                        <tr class="table-body-text">
                          <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                          <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                          <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                          <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                          <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                          <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                        </tr>
                        <tr class="table-body-text blanck-div-loader-totalpost">
                          <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                          <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                          <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                          <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                          <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                          <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                        </tr>
                        <tr class="blanck-div-loader-totalpost table-body-text"><td></td> <td></td><td></td><td></td><td></td><td></td></tr>
                        <tr class="blanck-div-loader-totalpost table-body-text"><td></td> <td></td><td></td><td></td><td></td><td></td></tr>
                        <tr class="blanck-div-loader-totalpost table-body-text"><td></td> <td></td><td></td><td></td><td></td><td></td></tr>
                        <tr class="blanck-div-loader-totalpost table-body-text"><td></td> <td></td><td></td><td></td><td></td><td></td></tr>
                    
                      </tbody>
                  </table>
            </div>
        :
        this.props.location=="teamChange-loader"|| this.props.location=="contactDetail-loader"?
        <div className="teamchange-loader">
        <table class="table responsive-table">
                <thead>
                    <tr class="reactable-column-header">
                        <th class="reactable-th-serialno reactable-header-sortable serialno  a-center"> 
                        <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                        </th>
                        <th class="reactable-th-serialno reactable-header-sortable serialno  a-center"> 
                        <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' />
                        </th>
                    </tr>
                </thead>
                <tbody class="reactable-data">
                  <tr class="table-body-text">
                    <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                    <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                  </tr>
                  <tr class="table-body-text">
                    <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                    <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                  </tr>
                  <tr class="table-body-text blanck-div-loader-totalpost">
                    <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                    <td>  <div class='rank-title loader-grey-line  loader-line-radius loader-line-height' /></td>
                  </tr>
                  <tr class="blanck-div-loader-totalpost table-body-text"><td></td> <td></td></tr>
                  <tr class="blanck-div-loader-totalpost table-body-text"><td></td> <td></td></tr>
                  <tr class="blanck-div-loader-totalpost table-body-text"><td></td> <td></td></tr>
                  <tr class="blanck-div-loader-totalpost table-body-text"><td></td> <td></td></tr>
              
                </tbody>
            </table>
      </div>
        :
        // page loader
				<div className={`preloaderWrap${this.props.className?this.props.className:''}`}>
					<div className="visibly-loader">
						<img src="/img/visibly-loader.gif" alt="Loader"/>
					</div>
				</div>
        }
			</div>
		)
	}
}