import PerfectScrollbar from '../components/Chunks/ChunkPerfectScrollbar'

class PopupWrapper extends React.Component {
  constructor(props) {
		super(props)
		this.state = {
			perfectScrollbar:null
		}
	}
  componentDidMount(){
    let scrollbarSelector;
    if(this.props.selector)
    {
      scrollbarSelector = `.${this.props.selector}`;
    }
    else{
      scrollbarSelector = '.popupDescSection';
    }
    PerfectScrollbar().then(scrollbar => {
      this.setState({ perfectScrollbar: scrollbar }, () => {
        if (document.querySelectorAll(scrollbarSelector).length > 0) {
          const ps = new this.state.perfectScrollbar.default(scrollbarSelector, {
            wheelSpeed: 2,
            wheelPropagation: true,
            minScrollbarLength: 20,
            suppressScrollX: true
          });
        }
      })
    })
  }
    render() {
        return (

            <div className={`popupDescSection ${this.props.className?this.props.className:''} ${this.props.selector?this.props.selector:''}`}>
              {this.props.children}
            </div>


        )
    }
}
module.exports = PopupWrapper