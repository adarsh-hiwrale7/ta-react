import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import Step3Form from './Step3Form';
import * as profileSettings from '../../actions/settings/getSettings'
import StepsNav from './StepsNav';
import PreLoader from '../PreLoader'
import * as settingsActions from "../../actions/settingsActions"

class RegisterStep3 extends React.Component {

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.profileSettings.GetProfileSettings()
  }

  handleFormSubmit(values){
    var objToAdd={
      stepNumber:3,
      isOnBoarding:true
    }
    this.props.settingsActions.settingsPostStep3(values, "/feed",objToAdd);
  }
  renderLoading() {
    return (
      <div className="preloader-wrap">
        <PreLoader></PreLoader>
      </div>
    )
  }

  render() {
    const {clientId,loading} = this.props.onboarding;
    return (

      <section className="register-page register-page-in-visibly">
        <div class = "illusration-ragister">
           <div className = "man-img"><img src = "../img/Login-walk-man.png"/></div>
           <div className = "msg-box"><img src = "../img/Login-msg-icon.png"/></div>
           <div className = "long-msg-box"><img src = "../img/Login-walk-shadow.png"/></div>
         </div>
      { (loading ? this.renderLoading() : <div></div> ) }
           <div className="content">
              <StepsNav active="step3" props={this.props}></StepsNav>
                 <div className="widget form-register-onboading">
                     <header className="heading clearfix" id="reg-profile">
                       <h3 className="heading-left">Company Profile</h3>
                       <h4 className="heading-right heading-left ">Step 3 of 3</h4>
                    </header>
               <Step3Form onSubmit={ this.handleFormSubmit.bind(this) } queryParams={this.props.location.query} profile={this.props.profile}> </Step3Form>

                 </div>
           </div>
     </section>
     );
  }
}

function mapStateToProps(state) {
  return {
    profile: state.profile,
    onboarding: state.settings.onboarding,
  }
}

function mapDispatchToProps(dispatch) {
  return {
    profileSettings: bindActionCreators(profileSettings, dispatch),
    settingsActions: bindActionCreators(settingsActions, dispatch)
  }
}

let connection = connect(mapStateToProps, mapDispatchToProps);

module.exports =  connection(RegisterStep3);