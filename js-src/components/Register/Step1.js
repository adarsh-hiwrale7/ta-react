import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import Step1Form from './Step1Form';
import Step2Form from './Step2Form';
import Step3Form from './Step3Form';
import PreLoader from '../PreLoader'
import StepsNav from './StepsNav';
import * as authActions from "../../actions/authActions"
//var Twit = require('twit');
import Globals from '../../Globals';
import * as profileSettings from '../../actions/settings/getSettings'
import * as settingsActions from "../../actions/settingsActions"
import * as s3functions from '../../utils/s3'


class RegisterStep1 extends React.Component {

  constructor(props) {
    super(props);
    this.state={
      verified:false
    }
  }

  componentDidMount() {
    this.props.profileSettings.GetProfileSettings()
    s3functions.loadAWSsdk()
  }
  handleSubmit(values){
    var objToAdd={
      stepNumber:1,
      isOnBoarding:true
    }
    this.props.settingsActions.settingsPostStep1(values, "/register/step2",false,objToAdd);
  }

  renderLoading() {
    return (
      <div className="preloader-wrap">
        <PreLoader></PreLoader>
      </div>
    )
  }


  render() {
    const {clientId,loading} = this.props.onboarding;
    return (
      <section className="register-page register-page-in-visibly 1">
       { (loading ? this.renderLoading() : <div></div> ) }
        <div class = "illusration-ragister">
           <div className = "man-img"><img src = "../img/Login-walk-man.png"/></div>
           <div className = "msg-box"><img src = "../img/Login-msg-icon.png"/></div>
           <div className = "long-msg-box"><img src = "../img/Login-walk-shadow.png"/></div>
         </div>
         <div className="content">
                    <StepsNav active="step1" props={this.props}></StepsNav>
                    <div className="widget form-register-onboading">
                        <header className="heading clearfix" id="reg-profile">
                          <h3 className="heading-left">Personal Profile</h3>
                          <h4 className="heading-right heading-left ">Step <strong> 1 </strong> of 2</h4>
                       </header>

                       <Step1Form onSubmit={ this.handleSubmit.bind(this) }> </Step1Form>
                    </div>
              </div>
        </section>
    );
  }
}

function mapStateToProps(state) {
  return {
    profile:state.profile,
    onboarding: state.settings.onboarding,
    auth: state.auth
  }
}

function mapDispatchToProps(dispatch) {
  return {
    profileSettings: bindActionCreators(profileSettings, dispatch),
    settingsActions: bindActionCreators(settingsActions, dispatch),
    authActions: bindActionCreators(authActions, dispatch)
  }
}

let connection = connect(mapStateToProps, mapDispatchToProps);

module.exports =  connection(RegisterStep1);
