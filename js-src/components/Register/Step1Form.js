import { Field, reduxForm } from 'redux-form';
import { renderField } from '../Helpers/ReduxForm/RenderField'
import { renderDropzoneField } from '../Helpers/ReduxForm/RenderField'
import { bindActionCreators } from 'redux'
import Globals from '../../Globals'
import * as utils from '../../utils/utils'
import * as settingsActions from "../../actions/settingsActions"
import * as countryActions from "../../actions/countryActions"
import { connect } from 'react-redux'
import PreLoaderMaterial from '../PreLoaderMaterial'
import PreLoader from '../PreLoader'
import ImageLoader from 'react-imageloader'
import notie from "notie/dist/notie.js"
import * as profileSettings from "../../actions/settings/getSettings"
import ReactDropzone from '../Chunks/ChunkReactDropzone';
const required = value => value ? undefined : 'Required';

const validEmail = value => value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value) ?
  'Invalid email address' : undefined;
var isHandleInitCall=false
class Step1Form extends React.Component {
  constructor() {
    super()

    this.state = {
      stateselected: false,
      cityselected: false,
      countryselected:false,
      isRequiredFieldFilled:false,
      reactDropzone:null
    }

  }
  componentWillMount() {
    ReactDropzone().then(dropzone=>{
      this.setState({
        reactDropzone:dropzone
      })
    })
  }
  componentDidMount() {
    isHandleInitCall=false
    this.props.countryActions.fetchAllCountry();
  }
  handleInitialize(nextProps = null) {
    // if(nextProps.country!=='' && nextProps.country!==undefined){
    //   //to fetch and initialize selected country's state list
    //   this.props.countryActions.fetchCountryState(nextProps.country.data.identity);
    // }if(nextProps.state!==undefined && nextProps.state!==''){
    //   this.props.countryActions.fetchStateCity(nextProps.state.data.identity)
    // }
    let initData = {}
    if (nextProps.email !== null ) {
      initData = {
        email: nextProps.email,
        firstname:nextProps.first_name ? nextProps.first_name : nextProps.firstname,
        lastname:nextProps.last_name? nextProps.last_name :nextProps.lastname,
      }
    }
    if(nextProps.department!==undefined){
      var objToSend={
      new_department: nextProps.department.name,
      user_position:nextProps.position?nextProps.position:nextProps.position
      }
      Object.assign(initData,objToSend)
    }
    // if(nextProps.country !== undefined && nextProps.state !== undefined){
    //   var objToSend={
    //     country:nextProps.country.data.identity,
    //     state:nextProps.state.data.identity
    //   }
    //   Object.assign(initData,objToSend)
    // }
    if (nextProps.email == null) {
      initData["email"] = null;
    }
    if(nextProps.email!==null && (nextProps.first_name!==null || nextProps.firstname!==null) ){
      this.setState({
        isRequiredFieldFilled:true
      })
    }
    this.props.initialize(initData)
  }
  componentWillReceiveProps(newProps){
    if(isHandleInitCall==false && (newProps.profile.profile.email !==null  ) ){
      isHandleInitCall=true
      //newProps.auth.userinfo!==undefined? newProps.auth.userinfo :
      var profile=newProps.profile.profile
        this.handleInitialize(profile)
    }
  }
  removeProfilePic() {
    var me = this

    let objToSend = {
      avatar: ''
    }

    fetch(
      Globals.API_ROOT_URL + `/user/profile`,
      {
        method: 'PUT',
        headers: utils.setApiHeaders('put'),
        body: JSON.stringify(objToSend)
      }
    )
      .then(response => {
        if (response.status != '200') {
          notie.alert(
            'error',
            'There was a problem removing your avatar. Please try later',
            5
          )
        }
        this.props.profileSettings.GetProfileSettings();
        return response.json()
      })
      .then(json => {
        if (json.hasOwnProperty('data')) {
          notie.alert('success', 'Your avatar removed', 3)

          me.setState({
            hideImgWrap: true
          })
        }else{
          utils.handleSessionError(json)
        }
      })
      .catch(err => {
        throw err
      })
  }
  selectedcountry(e) {
    var selectedCountryValue = e.target.value
    this.setState({
      countryselected: true
    })
    this.props.countryActions.fetchCountryState(selectedCountryValue);
  }
  selectedState(e) {
    this.setState({
      stateselected: true
    })
    var selectedStateValue = e.target.value
    this.props.countryActions.fetchStateCity(selectedStateValue)
  }
  selectedcity(){
    this.setState({
      cityselected: true
    })
  }
  renderLoading () {
    return (
      <div className='preloader-wrap'>
        <PreLoader className='Page' />
      </div>
    )
  }
  handleDropzoneSubmit(field) {
    this.props.settingsActions.settingsProfilePost({ files: field, photoUpload: true }, "");
  }
  render() {
    const { handleSubmit, pristine, submitting } = this.props;
    var me = this
    var activeFlag=''
    if(this.props.profile.profile.email !== null){
     // activeFlag=this.props.profile.profile.onboard_step
    }
    var defaultAvtar = "https://s3-us-west-2.amazonaws.com/talent-advocate/default-avatar.png";
    return (
      <form onSubmit={handleSubmit.bind(this)}>
      
        <div id="register-form">
          <div class="section">

            <div className='form-row profile-image-row'>
              <label className='block'>Avatar</label>
              <div className='profile-image-wrapper'>
                <div className="img-wrap">
                  <a href="javascript:void(0);" className='btn-remove-photo' onClick={this.removeProfilePic.bind(this)}><i className='fa fa-remove' /></a>
                  <img
                        src={this.props.aws.uploadStarted ==true ?'':this.props.profile.isProfileUpdated==true?this.props.profile.profile.avatar:''}
                        alt='visibly'
                        onError={e => {
                        e.target.src =
                        'https://app.visibly.io/img/default-avatar.png'
                     }}
                 />

                  <Field
                    label='Avatar'
                    name='files'
                    cssClass="upload-avatar btn"
                    handleSubmit={(fileToUpload, e) => {
                      me.handleDropzoneSubmit(fileToUpload)
                    }}
                    accept='.jpg, .png, .jpeg'
                    title='Change avatar'
                    Dropzone={this.state.reactDropzone}
                    component={renderDropzoneField}
                  />
                </div>
              </div>
            </div>

            <Field placeholder="First Name" label="First Name" component={renderField} type="text" name="firstname" validate={[required]} />
            <Field placeholder="Last Name" label="Last Name" component={renderField} type="text" name="lastname" />
            <Field placeholder="e.g HR, Marketing, Management, Communications " label="Which department do you work in?" component={renderField} type="text" name="new_department" validate={[required]} />
            <Field placeholder="e.g Founder, HR, Director, Marketing Director" label="What’s your position?" component={renderField} type="text" name="user_position" validate={[required]} />
            <div className="form-row  button-register">
              <button
                className='btn btn-primary right'
                type='submit'
                disabled={activeFlag=='' || activeFlag==0 ?pristine || submitting : !this.state.isRequiredFieldFilled}>
                Next
              </button>
            </div>
          </div>
        </div>
      </form>
    )
  }
}
function mapStateToProps(state) {
  return {
    aws: state.aws,
    auth:state.auth,
    profile: state.profile,
    settings: state.settings,
    allcountry: state.allcountry
  }
}

function mapDispatchToProps(dispatch) {
  return {
    profileSettings: bindActionCreators(profileSettings, dispatch),
    settingsActions: bindActionCreators(settingsActions, dispatch),
    countryActions: bindActionCreators(countryActions, dispatch)
  }
}
let connection = connect(mapStateToProps, mapDispatchToProps);
var reduxFormConnection = connection(Step1Form);
export default reduxForm({
  form: 'step1',  // a unique identifier for this form
})(reduxFormConnection)
