import { Field, reduxForm } from 'redux-form';
import { renderField } from '../Helpers/ReduxForm/RenderField'
import {browserHistory} from 'react-router';

const required = value => value ? undefined : 'Required';


class Step2Form extends React.Component {

  constructor() {
    super();
    this.state = {
      isRequiredFieldFilled:true
    }
  }
  handleInitialize(companyInfo) {
    var initData = {
      "Company_Name": "",
      "Company_Name_identity": "",
      "Location":"",
      "Location_identity":"",
      "Company_size":"",
      "Company_size_identity":"",
    };

    for(var i=0;i<companyInfo.length; i++) {
      for (var k in companyInfo[i]){
          if (companyInfo[i].hasOwnProperty(k)) {

            initData[companyInfo[i]["key"]] = companyInfo[i]["values"]
            initData[companyInfo[i]['key'] + '_identity'] =companyInfo[i]['identity']
          }
      }

    }
    if(initData.Company_Name !=='' && initData.Location!=='' && initData.Company_size!==''){
      this.setState({
        isRequiredFieldFilled:false
      })
    }
    this.props.initialize(initData);
  }
  componentDidMount() {
    this.handleInitialize(this.props.settings.companyInfo)

  }
  backStepButton(e){
    browserHistory.push('/register')
  }
componentWillReceiveProps(newProps){
  if(Object.keys(newProps.settings.companyInfo).length>0 && newProps.settings.companyInfo !== this.props.settings.companyInfo){
    this.handleInitialize(newProps.settings.companyInfo)
  }
}
  render() {
    var allowFinishButton = false
    //if 1st  step is not completed then finish button will not active
    var activeFlag=''
    if(this.props.profile.profile.email !== null){
      activeFlag=this.props.profile.profile.onboard_step
    }
    activeFlag < 1 ?
    allowFinishButton = true : allowFinishButton = false
    const { handleSubmit, departments, pristine, submitting } = this.props;
    return (
      <form onSubmit={handleSubmit}>


        <div class="section" id="page-step-2">
        <Field placeholder="What is your company called?" label="Company Name" component={renderField} type="text" name="Company_Name" validate={[required]} />

          <Field placeholder="e.g UK, USA" label="Which country are you based?" component={renderField} type="text" name="Location" validate={[required]} />
          <Field
            placeholder='Select company size '
            label='How many employees do you have currently?'
            component={renderField}
            type='select'
            name='Company_size'
            validate={[required]}
          >
            <option value=''>Select Company Size</option>
            <option value='1-10'>1-10</option>
            <option value='11-30'>11-30</option>
            <option value='31-50'>31-50</option>
            <option value='51-100'>51-100</option>
            <option value='101-200'>101-200</option>
            <option value='201-500'>201-500</option>
            <option value='501-1000'>501-1000</option>
            <option value='1001-5000'>1001-5000</option>
            <option value='5001-10000'>5001-10000</option>
            <option value='10000+'>10000+</option>

          </Field>
          <div className = "setp2-button">
          <div className="form-row step2-msg-for-finished">
              <span id="checkbox" name="agree">By clicking Finish, you agree to our <a href="https://visibly.io/term-of-use/" target="_blank">Terms</a>, <a href="https://visibly.io/privacy-policy/" target="_blank">Privacy Policy</a> and <a href="https://visibly.io/cookie-policy/" target="_blank">Cookie Policy</a></span>
          </div>
          <div className="form-row">
          <input type="submit" value="finish" class="btn btn-primary right btn-rgt" disabled={(activeFlag=='' || (activeFlag !== 0 )) ?pristine || submitting || allowFinishButton : this.state.isRequiredFieldFilled } ></input>
            <button class="btn btn-primary  btn-rgt" type="submit" onClick={this.backStepButton.bind(this)}>Back</button>
          </div>
          </div>
        </div>

      </form>
    )
  }

}

export default reduxForm({
  form: 'step2',  // a unique identifier for this form
})(Step2Form)
