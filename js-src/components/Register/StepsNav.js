import { connect } from 'react-redux'
import { Field, reduxForm } from 'redux-form'
import { Link } from 'react-router'
import VisiblyHeader from '../Layout/Header/VisiblyHeader';
import { browserHistory } from 'react-router';
let roleName=''
class StepsNav extends React.Component {
 constructor(props) {
		super(props);
		this.state = {
    }
  }
callpageloader(){
    return(
        <div class="initial-loading">
            
           <div class="visibly-splash-wrapper">
            <div class="visibly-splash">
              <img src="/img/visibly-loader.gif" alt="visibly-loader" class="visibly-splash-loader" />
            <svg class="nav__image image" xmlns="https://www.w3.org/2000/svg" viewBox="0 0 374.68 95.95">
                    <defs>
                       
                    </defs>
                    <g data-name="Layer 2">
                        <g data-name="Layer 1">
                            <path class="cls-1" d="M147.92,23.56H164.1V73.12H147.92ZM156,0a8.71,8.71,0,0,0-6.57,2.63,9,9,0,0,0-2.53,6.47,9,9,0,0,0,2.53,6.47,9.53,9.53,0,0,0,13.15,0,9,9,0,0,0,2.53-6.47,9,9,0,0,0-2.53-6.47A8.71,8.71,0,0,0,156,0Zm48,46.32a23.57,23.57,0,0,0-5.11-3q-2.73-1.16-5-2-2.93-1.11-4.8-1.87A2.58,2.58,0,0,1,187.31,37a2.2,2.2,0,0,1,1.57-2.28,14.11,14.11,0,0,1,4.5-.56,26.17,26.17,0,0,1,6.37.76,35.13,35.13,0,0,1,5.76,2l2.22-10.62a26.5,26.5,0,0,0-7.38-2.88,39,39,0,0,0-8.5-.86q-8.8,0-14.26,3.84t-5.46,11.93A13.46,13.46,0,0,0,173.25,44a13.78,13.78,0,0,0,3.08,4.3,19.3,19.3,0,0,0,4.7,3.24A51.86,51.86,0,0,0,186.91,54q3.84,1.42,5.76,2.17a2.65,2.65,0,0,1,1.92,2.58,3,3,0,0,1-2.17,2.88,13.84,13.84,0,0,1-5.11.86,39.37,39.37,0,0,1-6.62-.66,32,32,0,0,1-6.93-2L171.53,71.1a37.27,37.27,0,0,0,8.09,2.28,50.62,50.62,0,0,0,8.5.76,32,32,0,0,0,8.7-1.11,20.38,20.38,0,0,0,6.83-3.29,14.73,14.73,0,0,0,4.45-5.41,16.73,16.73,0,0,0,1.57-7.38A11.47,11.47,0,0,0,208,50.72,16.64,16.64,0,0,0,204,46.32Zm12.37,26.8H232.6V23.56H216.42ZM224.51,0a8.71,8.71,0,0,0-6.57,2.63A9,9,0,0,0,215.4,9.1a9,9,0,0,0,2.53,6.47,9.53,9.53,0,0,0,13.15,0,9,9,0,0,0,2.53-6.47,9,9,0,0,0-2.53-6.47A8.71,8.71,0,0,0,224.51,0Zm66.4,37.72a32.79,32.79,0,0,1,1.52,10.21,29.18,29.18,0,0,1-1.77,10.16,22.79,22.79,0,0,1-5.31,8.34,25.26,25.26,0,0,1-8.9,5.61,34.69,34.69,0,0,1-12.54,2.07,49.83,49.83,0,0,1-11.58-1.31,44.81,44.81,0,0,1-10.06-3.64V1.72h16.18V27.41a17.33,17.33,0,0,1,5.11-3.49,18.13,18.13,0,0,1,7.53-1.37,20.28,20.28,0,0,1,8.8,1.87,19.32,19.32,0,0,1,6.73,5.26A24.52,24.52,0,0,1,290.91,37.72ZM276.24,48.14q0-6.07-2.38-9.66t-7.43-3.59a12.47,12.47,0,0,0-8,2.73V61.08a12,12,0,0,0,2.63.81,16,16,0,0,0,3.14.3,12.53,12.53,0,0,0,5.46-1.11,9.8,9.8,0,0,0,3.74-3,13.62,13.62,0,0,0,2.12-4.45A20.23,20.23,0,0,0,276.24,48.14ZM319,62q-2.33,0-3.13-1.06a4.82,4.82,0,0,1-.81-3V1.72H298.9v58a19.93,19.93,0,0,0,.71,5.31,12.6,12.6,0,0,0,2.33,4.6,11.32,11.32,0,0,0,4.35,3.24,16.68,16.68,0,0,0,6.78,1.21,39.12,39.12,0,0,0,5.31-.35,30.72,30.72,0,0,0,4.6-1l-.51-11.12A23.8,23.8,0,0,1,319,62ZM358.4,23.56,334.93,96h16.28l23.46-72.39ZM98.09,22H81.3L99.2,72.75l6.55-21Zm4.24,50.77h16.28L142.07.37H125.79ZM345,53.29l-7.67-29.73H320.51l17.9,50.77Z"></path>
                            <path class="cls-2" d="M17.21,22V49.66H40.52V72.71H67.85V22ZM57.73,62.58H50.64V39.52H27.33v-7.4h30.4Z"></path>
                            <path class="cls-3" d="M50.64,90.25V62.57H27.33V39.52H0V90.25ZM10.12,49.66h7.09V72.71H40.52v7.4H10.12Z"></path>
                        </g>
                    </g>
                </svg>
            </div>
            </div>
        </div>
    )
}
componentDidMount() {
    if(roleName=='guest'){
        if(this.props.props.profile.profile.email !==null && this.props.props.profile.profile.onboard_step !==0){
            if(this.props.props.auth.feed_identity !==null){
                // alert('nooo4444')
                // browserHistory.push(`/feed/custom/live/${this.props.props.auth.feed_identity}`);
                browserHistory.push("/tour");
            }
        }
    }else {
        //when click on back button or trying to get back page then to redirect back to feed page if step is completed
        if(this.props.props.profile.profile.email !==null && this.props.props.profile.profile.onboard_step !== null && this.props.props.profile.profile.onboard_step !== undefined){
            if(this.props.props.profile.profile.onboard_step>=2){
                browserHistory.push("/tour");
            }
        }
    }
}
componentWillReceiveProps(newprops){
    if(roleName=='guest'){
        if(newprops.props.profile.profile.email !==null && newprops.props.profile.profile.onboard_step !==0 && newprops.props.profile.profile.email !==this.props.props.profile.profile.email ){
            if(newprops.props.auth.feed_identity !==null){
                // alert('nooo333')
                browserHistory.push("/tour");
                // browserHistory.push(`/feed/custom/live/${newprops.props.auth.feed_identity}`);
            }
        }
    }else {
        //when click on back button or trying to get back page then to redirect back to feed page if step is completed
        if(newprops.props.profile.profile.email !==null && newprops.props.profile.profile.onboard_step !== null && newprops.props.profile.profile.onboard_step !== undefined && newprops.props.profile.profile.email !==this.props.props.profile.profile.email ){
            if(newprops.props.profile.profile.onboard_step>=2){
                browserHistory.push("/tour");
            }
        }
    }
}
render() {
roleName =JSON.parse(localStorage.getItem('roles')) !== null? JSON.parse(localStorage.getItem('roles'))[0].role_name:''
var active = this.props.active;
var step1active=''
var step2active=''
var step3active=''
if(this.props.props!==undefined){
    var steps=this.props.props.profile.profile.onboard_step
    if(steps==1){
        step1active='active'
        step2active=''
        step3active=''
    } else if(steps==2){
        step1active='active'
        step2active='active'
        step3active=''
    }
}
	return (
    <div class = "register-step-navigation">
       {/* <div className = "sign-up-process-space-line-wrapper-container">
        <div className = 'sign-up-process-space-line-wrapper'>
            <div className = 'sign-up-process-space-line'>
               <div className = {`step-1-2 ${step1active}`}></div>
            </div>
        </div>
    </div> */}
        {this.props.props.profile.profile.email==null?this.callpageloader():''}
     <div class = "register-page">
     <VisiblyHeader onboard_step={this.props.props.profile.profile.onboard_step} props={this.props}/>
	</div>

    <div>

    {roleName!=='guest'?
		<div className="steps-nav ">

		<div className = "clearfix">
        {
           this.props.props.profile.profile.onboard_step==1?
           <div className = "step-nav-item"><Link className={`step ${step1active}`}><span ><i class="material-icons">done</i></span></Link>
                <span className="active"> Personal Profile </span>
            </div>
           :
           this.props.props.location.pathname=="/register"?
           <div className = "step-nav-item activeCurrentPage"><Link className={`step ${step1active}`}><span >1</span></Link>
               <span> Personal Profile </span>
           </div>
           :
           <div className = "step-nav-item"><Link className={`step ${step1active}`}><span >1</span></Link>
               <span > Personal Profile </span>
           </div>
        }
        {
           this.props.props.profile.profile.onboard_step==2?
           <div className = "step-nav-item"><Link className={`step ${step2active}`}><span ><i class="material-icons">done</i></span></Link>
                   <span className="active">  Company Profile </span>
            </div>
           :
           this.props.props.location.pathname=="/register/step2"?
           <div className = "step-nav-item activeCurrentPage"><Link className={`step ${step2active}`}><span >2</span></Link>
               <span>Company Profile </span>
           </div>:
           <div className = "step-nav-item"><Link className={`step ${step2active}`}><span >2</span></Link>
                <span>Company Profile </span>
            </div>
        }
			{/* <div className = "step-nav-item"><Link className={`step ${step3active}`}><span >3</span></Link></div> */}
	  </div>
    </div>:''}
    </div>
</div>
	)

  }
}

// module.exports =  StepsNav;
function mapStateToProps (state) {
    return {
    }
  }
  function mapDispatchToProps (dispatch) {
    return {
    }
  }
  let connection = connect(mapStateToProps, mapDispatchToProps)
  var reduxConnectedComponent = connection(StepsNav)

  export default reduxForm({
    form: 'StepsNav' // a unique identifier for this form
  })(reduxConnectedComponent)