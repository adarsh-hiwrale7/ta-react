import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { browserHistory } from 'react-router'
import PreLoader from '../PreLoader'
import notie from 'notie/dist/notie.js'
import * as authActions from "../../actions/authActions"
//var Twit = require('twit');
import Globals from '../../Globals';
import * as s3functions from '../../utils/s3'
import VisiblyHeader from '../Layout/Header/VisiblyHeader';

var showMenu = false;
class SignUp extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      forgotpass_popup: false,
      menu_handle_wrapper : false,
      show_menu: false,
    }
 }
  menuHandle(){
    showMenu = !showMenu;
    if(showMenu == true){
      document.body.classList.add("show-menu");
    }else{
      document.body.classList.remove('show-menu')
    }

  }
  closeMenu(){
    document.body.classList.remove('show-menu')
    showMenu = false;
  }

  renderPasswordCreation() {
    return (
      <SliderPopup>
        <button
          id='close-popup'
          className='btn-default'
          onClick={this.closeForgotpassPopup.bind(this)}
        > <i className='material-icons'>clear</i></button>
        <ForgotPassword></ForgotPassword>
      </SliderPopup>
    );
  }

  componentDidMount() {
    var queryParams = this.props.location.query;

    fetch(Globals.API_ROOT_URL_MASTER + `/verify?e=${queryParams.e}&t=${queryParams.t}`)
    .then(response => {
      return response.json()
    })
    .then((json) => {
        if(typeof json.success !== "undefined" && typeof json.error == "undefined"){
        // we got "data" in response, that means, it is success.
        // if we don't get "data", there will be error property in the response
            browserHistory.push('/thankyou')
        this.setState({
          verified: true
        });
      }
      else {
        if (Object.keys(json.error).length > 0) {
          if (json.error.Server_Issue !== undefined) {
            notie.alert('error', json.error.Server_Issue, 3);
          } else if (json.error.Invalid_request !== undefined) {
            notie.alert('error', json.error.Invalid_request, 3);
          } else {
            notie.alert('error', 'There is a problem in signup', 5);
          }
          this.setState({
            error: true
          })
        }
      }

    })
  }

  renderLoading() {
    return (
      <div className="preloader-wrap">
        <PreLoader></PreLoader>
      </div>
    )
  }


  render() {

    return (
      <section className="register-page" >
          <div id="content">
            <div className="page">
            <VisiblyHeader />
    <div class="LoadingBackground">
     <div className="LoadingHeading">
    <h2>Give us a minute...</h2><span>We're just setting up your account!</span>
    </div>

    <div className="LoadingImage">
    <img src="img/loadingillustration.jpg"/>
    </div>
    <div className="LoadingText">
    <h2>Loading...</h2>
    </div>
            </div>

            </div>
          </div>

</section>
    );
  }
}

function mapStateToProps(state) {
  return {
    onboarding: state.settings.onboarding,
    auth: state.auth
  }
}

function mapDispatchToProps(dispatch) {
  return {
    authActions: bindActionCreators(authActions, dispatch)
  }
}

let connection = connect(mapStateToProps, mapDispatchToProps);

module.exports =  connection(SignUp);
