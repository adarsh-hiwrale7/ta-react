import { connect } from 'react-redux'
import ThankYouPage from './ThankYouPage'
class RegisterStepLast extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {

    return (
      <section >
         <ThankYouPage stepLast={true}/>
      </section>
    );
  }
}

function mapStateToProps(state) {
  return {
  }
}

function mapDispatchToProps(dispatch) {
  return {

  }
}

let connection = connect(mapStateToProps, mapDispatchToProps);

module.exports =  connection(RegisterStepLast);