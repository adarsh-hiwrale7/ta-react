import notie from "notie/dist/notie.js";
import { connect } from "react-redux";
import { browserHistory } from "react-router";
import { bindActionCreators } from "redux";
import * as authActions from "../../actions/authActions";
import Globals from "../../Globals";
import VisiblyHeader from "../Layout/Header/VisiblyHeader";
import PreLoader from "../PreLoader";
class SSOSignUp extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    var queryParams = this.props.location.query;

    fetch(
      Globals.API_ROOT_URL_MASTER +
        `/verifySsoUser?e=${queryParams.e}&t=${queryParams.t}`
    )
    .then(response => response.json())
      .then((json) => {
        console.log(json, "json");
        if (json.code == 200) {
          browserHistory.push("/thankyou");
        } else {
          notie.alert("error", error.message, 5);
        }
      });
  }

  renderLoading() {
    return (
      <div className="preloader-wrap">
        <PreLoader></PreLoader>
      </div>
    );
  }

  render() {
    return (
      <section className="register-page">
        <div id="content">
          <div className="page">
            <VisiblyHeader />
            <div class="LoadingBackground">
              <div className="LoadingHeading">
                <h2>Give us a minute...</h2>
                <span>We're just setting up your account!</span>
              </div>

              <div className="LoadingImage">
                <img src="img/loadingillustration.jpg" />
              </div>
              <div className="LoadingText">
                <h2>Loading...</h2>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

function mapStateToProps(state) {
  return {
    onboarding: state.settings.onboarding,
    auth: state.auth,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    authActions: bindActionCreators(authActions, dispatch),
  };
}

let connection = connect(mapStateToProps, mapDispatchToProps);

module.exports = connection(SSOSignUp);
