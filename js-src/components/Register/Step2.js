import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import Step2Form from './Step2Form'
import Globals from '../../Globals'
import ReactCSSTransitionGroup from 'react/lib/ReactCSSTransitionGroup'
import PreLoader from '../PreLoader'
import StepsNav from './StepsNav'
import * as profileSettings from '../../actions/settings/getSettings'
import * as settingsActions from '../../actions/settingsActions'


class RegisterStep2 extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      departments: []
    }
  }

  componentDidMount() {
    this.props.profileSettings.GetProfileSettings(true)
    this.props.profileSettings.GetAllSettings()
  }
  handleFormSubmit(values) {
    var objToAdd = {
      stepNumber: 2,
      isOnBoarding: true
    }
    this.props.settingsActions.saveYourProfile_Step2(values, '/tour', objToAdd)
  }

  renderLoading() {
    return (
      <div className='preloader-wrap'>
        <PreLoader />
      </div>
    )
  }

  render() {
    const { clientId, loading } = this.props.onboarding
    return (

      <section className="register-page register-page-in-visibly">
       <div class = "illusration-ragister">
           <div className = "man-img"><img src = "../img/Login-walk-man.png"/></div>
           <div className = "msg-box"><img src = "../img/Login-msg-icon.png"/></div>
           <div className = "long-msg-box"><img src = "../img/Login-walk-shadow.png"/></div>
         </div>
        {(loading ? this.renderLoading() : <div></div>)}
        <div className="content">
          <StepsNav active='step2' props={this.props} />
          <div className="widget form-register-onboading">
            <header className="heading clearfix" id="reg-profile">
              <h3 className="heading-left">Company Profile</h3>
              <h4 className="heading-right heading-left ">Step <strong>2</strong> of 2</h4>
            </header>

            <Step2Form
              profile={this.props.profile}
              settings={this.props.settings}
              onSubmit={this.handleFormSubmit.bind(this)}
              departments={this.state.departments}>
            </Step2Form>

          </div>
        </div>
      </section>

    )
  }
}

function mapStateToProps(state) {
  return {
    settings: state.settings,
    profile:state.profile,
    onboarding: state.settings.onboarding,
    auth: state.auth
  }
}

function mapDispatchToProps(dispatch) {
  return {
    profileSettings: bindActionCreators(profileSettings, dispatch),
    settingsActions: bindActionCreators(settingsActions, dispatch)
  }
}

let connection = connect(mapStateToProps, mapDispatchToProps)

module.exports = connection(RegisterStep2)
