import { Field, reduxForm } from 'redux-form';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { renderField } from '../Helpers/ReduxForm/RenderField'
import * as utils from '../../utils/utils'
import Globals from "../../Globals";
import { Link } from "react-router";
import notie from "notie/dist/notie.js";
import {browserHistory} from 'react-router';
import MentionTextArea from '../Helpers/ReduxForm/MentionTextArea'
const required = value => value ? undefined : 'Required';
const validEmail = value => value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value) ?
  'Invalid email address' : undefined;
class Step3Form extends React.Component {

  constructor() {
    super();

    this.state = {
      fbConnected: false,
      twConnected: false,
      liConnected: false,
      isRequiredFieldFilled:true
    }
  }
  handleInitialize(data) {
    var tags = []
    if (data.email !== null) {
      const initData = {
        new_department: data.department.name,
        user_position: data.position

      }
      data.user_tag.length > 0 ? data.user_tag.map((tag, i) => {
        tags.push(tag.name)
      }) : ''
      var dataToAdd = {
        tags: tags.join(" ")
      }
      Object.assign(initData, dataToAdd)
      if(initData.new_department !=='' && initData.user_position!=='' && initData.tags!=='' ){
        this.setState({
          isRequiredFieldFilled:false
        })
      }
      this.props.initialize(initData)
    }
  }
  componentDidMount() {
    this.handleInitialize(this.props.profile.profile)

  }

  componentWillReceiveProps(newProps) {
    if (newProps.profile.profile.email !== null && newProps.profile.profile !== this.props.profile.profile) {
      this.handleInitialize(newProps.profile.profile)
    }
  }
  renderCheckedIcon() {
    return (
      <span>
        <i className="material-icons">check</i> &nbsp; Connected
      </span>
    )
  }

  renderNotConnectedSocial(socialAccount) {
    return (
      <span>
        {socialAccount}
      </span>
    )
  }
  backStepButton(e){
    browserHistory.push('/register/step2')
  }
  render() {
    let postTags = this.props.postTags;
    const { handleSubmit, pristine, submitting } = this.props;

    var allowFinishButton = false
    //if 1 and 2 step is not complete then finish button will not active

    var activeFlag=''
    if(this.props.profile.profile.email !== null){
      activeFlag=this.props.profile.profile.onboard_step
    }
    activeFlag < 2 ?
      allowFinishButton = true : allowFinishButton = false
    return (
      <form onSubmit={handleSubmit}>
        <div className="section" id="pagestep3">
          <Field placeholder="HR IT " label="Which department do you work in?" component={renderField} type="text" name="new_department" validate={[required]} />
          <Field placeholder="Position" label="What’s your position?" component={renderField} type="text" name="user_position" validate={[required]} />
          <Field
            label="What type of content you want to be notified about?"
            placeholder='#Technology #Science #Music #World #News'
            component={renderField}
            id='user_tags'
            validate={[required]}
            type="text"
            name="tags" />
          <div className="form-row">
              <span id="checkbox" name="agree">By clicking Finish, you agree to our <a href="https://visibly.io/term-of-use/" target="_blank">Terms</a>, <a href="https://visibly.io/privacy-policy/" target="_blank">Privacy Policy</a> and <a href="https://visibly.io/cookie-policy/" target="_blank">Cookie Policy</a></span></div>
          <div className="form-row">
          <button class="btn btn-primary" type="submit" onClick={this.backStepButton.bind(this)}>Back</button>
          <button class="btn btn-primary right btn-rgt" type="submit"  disabled={activeFlag=='' || (activeFlag!==0 && activeFlag!==1 )  ?pristine || submitting || allowFinishButton : this.state.isRequiredFieldFilled }>Finish</button>
          </div>
          <div>
          </div>
        </div>


      </form>
    )
  }

}
function mapStateToProps(state, ownProps) {
  return {
  }

}

function mapDispatchToProps(dispatch) {
  return {
  }
}
let connection = connect(mapStateToProps, mapDispatchToProps)

var reduxConnectedComponent = connection(Step3Form)

export default reduxForm({
  form: 'step3',  // a unique identifier for this form
})(reduxConnectedComponent)
