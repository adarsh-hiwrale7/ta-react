/*
** This is the form for adding team members. Don't delete it yet.
** It will be useful in settings page
*/

import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import Step5Form from './Step5Form';
import {reset} from 'redux-form';
import Globals from '../../Globals';
import PreLoader from '../PreLoader'
import notie from "notie/dist/notie.js"
import * as utils from '../../utils/utils'
import ReactCSSTransitionGroup from 'react/lib/ReactCSSTransitionGroup';

import * as settingsActions from "../../actions/settingsActions";


class RegisterStep5 extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      members: [],
      departments: [],
      loading:false
    }
  }

  componentDidMount() {
    let departments=[];
     fetch(Globals.API_ROOT_URL + `/department`,{
      headers: utils.setApiHeaders('get')
    })
    .then(response => response.json())
    .then((json) => {

      departments=json.data;
      for(var t=0; t<json.data.length; t++) {
          for (var prop in departments[t]) {
              if (json.data[t].hasOwnProperty("identity")) {
                  departments[t]["label"] = departments[t]["name"];
                  departments[t]["value"] = departments[t]["identity"];
              }
          }
      }
      utils.handleSessionError(json)
      this.setState({
        departments: departments
      });

    })
  }

  renderLoading() {
    return (
      <div className="loading">
        <PreLoader></PreLoader>
      </div>
    )
  }
  handleFormSubmit(values){
    var me = this;
    var arr = {
      'name': values.fname,
      'email': values.email,
      'department':values.department.values,
    }
    this.setState({loading:true});
    fetch(Globals.API_ROOT_URL + `/register-user`, {
      method: 'POST',
      //mode: 'no-cors',
      headers: utils.setApiHeaders('post'),
      body: JSON.stringify(arr)
    })
    .then(response => {
      this.setState({loading:false});
      if(response.status=="200") {
      }else if (response.status=="503"){
        notie.alert('error',  'User already exist.', 3)
      }else {
        notie.alert('error',  'There is some error adding user', 3)
      }
      return response.json()
    })
    .then(json => {
      if(!json.error)
      {
          var updatedMembers = [...me.state.members.slice(), json.data] ;
          this.setState({
            members: updatedMembers
          });
          //console.log(this.state.members);
      }else{
        utils.handleSessionError(json)
      }
    })
    .catch(err => { throw err; });

  }

  handleNext(values){
    this.props.settingsActions.settingsPostStep3(values, "/register/finish");
  }


  memberListing() {
    var members = this.state.members;
    return (
      <table className="table">
        <thead>
          <tr>
            <th>#</th>
            <th>Name</th>
            <th>Email</th>
            <th>Position</th>
            <th>Department</th>
          </tr>
        </thead>
        <tbody>
         {
           members.map((member, index) => {
            return (
              <tr key={index}>
                  <td>{ member.full_name}</td>
                  <td>{ member.email }</td>
                  <td>{ member.department }</td>
              </tr>
            )
          })
         }
         </tbody>
        </table>
      )

  }


  render() {

    return (
      <section className="register-page onboarding-step-addmembers" >


          <div id="content">
            <div className="page">

              <div className="esc narrow">

                { (this.state.loading ? this.renderLoading() : <div></div> ) }

                <header className="header">
                  <h1><i class="material-icons">insert_drive_file</i> <span className="breadcrumb">Registration </span> </h1>
                </header>

                <ReactCSSTransitionGroup transitionName="slide" transitionAppear={true} transitionEnterTimeout={500} transitionLeaveTimeout={500}  transitionAppearTimeout={1000}>

                  <div key={1} className="content">

                    <div className="widget form-register-onboading">

                      <header className="heading">
                        <h3>Team Members</h3>
                      </header>

                      <div className="membersListing">
                        {
                          this.memberListing()
                        }
                      </div>



                      <div className="alternate-bg">
                        <header className="heading">
                          <h3 className="subtitle">Add some team members</h3>
                        </header>

                        <Step5Form onSubmit={ this.handleFormSubmit.bind(this) } departments={this.state.departments} > </Step5Form>
                      </div>


                      <div className="section">
                        <div className="form-row">
                          <button className="btn btn-primary right" type="submit" onSubmit={ this.handleNext.bind(this) }>Next</button>
                        </div>
                      </div>


                    </div>


                  </div>

                </ReactCSSTransitionGroup>

              </div>
            </div>
          </div>

        </section>
    );
  }
}

function mapStateToProps(state) {
  return {
    profile: state.settings.profile,
  }
}

function mapDispatchToProps(dispatch) {
 /* return {
    settingsActions: bindActionCreators(settingsActions, dispatch)
  }*/
  /*return {
    dispatch,
    settingsActions:bindActionCreators(settingsActions, dispatch)
  }*/
  return {
    dispatch,
    settingsActions:
      bindActionCreators({
      ...settingsActions
    }, dispatch)
  }
}

let connection = connect(mapStateToProps, mapDispatchToProps);

module.exports =  connection(RegisterStep5);
