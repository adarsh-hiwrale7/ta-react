import notie from "notie/dist/notie.js";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as authActions from "../../../actions/authActions";
import * as profileSettings from "../../../actions/settings/getSettings";
import * as settingsActions from "../../../actions/settingsActions";
import * as s3functions from "../../../utils/s3";
import moment from "../../Chunks/ChunkMoment";
import VisiblyHeader from "../../Layout/Header/VisiblyHeader";
import PreLoader from "../../PreLoader";
import ProfileForm from "../../Settings/Account/ProfileForm";

class RegisterSsoUser extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      verified: false,
      Moment: null,
    };
  }
  componentWillMount() {
    moment().then((moment) => {
      this.setState({ Moment: moment });
    });
  }
  componentDidMount() {
    s3functions.loadAWSsdk();
  }

  renderLoading() {
    return (
      <div className="preloader-wrap">
        <PreLoader></PreLoader>
      </div>
    );
  }

  handleProfileSubmit(values) {
    var valToPass = values;
    valToPass = {
      ...valToPass,
      email: this.props.profile.profile.email,
    };
    var date = this.state.Moment.unix(values.birth_date).format("DD/MM/YYYY");
    var today_date = new Date();
    var birth_year = this.state.Moment(date, "DD/MM/YYYY").format("YYYY");
    var today_year = today_date.getFullYear();
    var age = today_year - birth_year;
    if (age < 16) {
      notie.alert("warning", "You cannot be below 16 years ", 3);
      return;
    }
    this.props.settingsActions.addSsoUser(valToPass);
  }
  handleDropzoneSubmit(field) {
    this.props.settingsActions.settingsProfilePost(
      { files: field, photoUpload: true },
      ""
    );
  }
  render() {
    const loading =
      this.props.settings.addSsoUserOnbaord.addSsoUserOnbaordLoader ||
      this.props.aws.uploadStarted ||
      this.props.country.loadCountry ||
      this.props.settings.companyLoader;
    let profile =
      typeof this.props.profile !== "undefined" ? this.props.profile : [];
    return (
      <section className="register-page register-page-in-visibly 1">
        <VisiblyHeader />
        {loading ? this.renderLoading() : <div></div>}
        <div class="illusration-ragister">
          <div className="man-img">
            <img src="../img/Login-walk-man.png" />
          </div>
          <div className="msg-box">
            <img src="../img/Login-msg-icon.png" />
          </div>
          <div className="long-msg-box">
            <img src="../img/Login-walk-shadow.png" />
          </div>
        </div>
        <div className="content">
          <div className="widget form-register-onboading">
            <header className="heading clearfix" id="reg-profile">
              <h3 className="heading-left">Personal Profile</h3>
            </header>
            <ProfileForm
              key={"ProfileForm"}
              onSubmit={this.handleProfileSubmit.bind(this)}
              departments={this.props.departments}
              renderEditor={this.handleDropzoneSubmit.bind(this)}
              location={this.props.location}
              profile={profile}
              Moment={this.state.Moment}
            />
          </div>
        </div>
      </section>
    );
  }
}

function mapStateToProps(state) {
  return {
    settings: state.settings,
    country: state.allcountry,
    aws: state.aws,
    profile: state.profile,
    auth: state.auth,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    profileSettings: bindActionCreators(profileSettings, dispatch),
    settingsActions: bindActionCreators(settingsActions, dispatch),
    authActions: bindActionCreators(authActions, dispatch),
  };
}

let connection = connect(mapStateToProps, mapDispatchToProps);

module.exports = connection(RegisterSsoUser);
