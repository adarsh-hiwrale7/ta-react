import { Field, reduxForm } from 'redux-form';
import { renderField } from '../Helpers/ReduxForm/RenderField'
// import SelectCreatable from '../Helpers/ReduxForm/SelectCreatable' this p;ugin is uninstalled as we are not using this page anymore versio  was 1.0.0-rc.5
import ReactDatePicker from '../Helpers/ReduxForm/ReactDatePicker';

const required = value => value ? undefined : 'Required';
const validEmail = value => value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value) ?
      'Invalid email address' : undefined;


function Step5Form (props) {
  const {handleSubmit,departments,pristine, submitting} = props;

  return (
    <form onSubmit={handleSubmit}>

      <div className="section">
        <div className="row">
          <div className="col-one-of-three">
            <Field placeholder="First Name" label="First Name" component={renderField} type="text" name="fname" validate={[ required ]} />
          </div>
          <div className="col-one-of-three">
            <Field placeholder="Last Name" label="Last Name" component={renderField} type="text" name="lname" />
          </div>
          <div className="col-one-of-three last">
            <Field placeholder="Email" label="Email" component={renderField} type="text" name="email" validate={[ required, validEmail ]} />
          </div>
        </div>


        <div className="row">

          <div className="col-one-of-three">
            <Field placeholder="Position" label="Position" component={renderField} type="text" name="position"  />
          </div>
          <div className="col-one-of-three">
            <Field
          name="tags"
          component = { props => {
            return (
                <ReactDatePicker
                  {...props}

                  //onChange={this.handleChange}
                ></ReactDatePicker>
              )
            }
          }></Field>
          </div>
          <div className="col-one-of-three">
            <label for="departments">Departments</label>
            {/* <Field
            name="departments"
            component = { props => {
              return (
                  <SelectCreatable
                    {...props}
                    selectType="creatable"
                    options={departments}
                  ></SelectCreatable>
                )
              }
            }></Field> */}
          </div>
          <div className="col-one-of-three last">
          </div>
        </div>

        <div className="form-row button-register">
          <button className="btn btn-secondary" type="submit">Add</button>
        </div>

      </div>

    </form>
  )
}

export default reduxForm({
  form: 'step5',  // a unique identifier for this form
})(Step5Form)
