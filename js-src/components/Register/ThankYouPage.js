import { connect } from 'react-redux'
import { browserHistory } from 'react-router'
import VisiblyHeader from '../Layout/Header/VisiblyHeader'
var showMenu = false
class ThankYouPage extends React.Component {
  constructor (props) {
    super(props)
    this.state = {}
  }
  menuHandle () {
    showMenu = !showMenu
    if (showMenu == true) {
      document.body.classList.add('show-menu')
    } else {
      document.body.classList.remove('show-menu')
    }
  }
  redirectOnFeed(){
    browserHistory.push('/feed')
  }
  render () {
    return (
      <section className='register-page'>

        <div id='content'>
          <div className='ThankyouMainPage'>
            <div className='page'>
              <VisiblyHeader />
              <div className='ThankyouImage'>
              <div className='ThankyouSqure item3'>
              <div className = "thankyou-img">
              <div className='ThankyouBox'>
                       <div className = "thank-you-text">
                         <h2>You’re all set!</h2>
                         <h3> Please check your email for next step instructions </h3>
                         </div>
                         <div className = "img-for-thankyou"> 
                        
                          <div className = "ellipse-img"> <img src = "img/ecllips.png"/></div>
                          </div>
                      </div>
                  {/* <div className='ThankyouText'>
                    {this.props.stepLast == undefined
                          ? <div>
                            
                            </div>
                         : <h2>Thanks for being awesome,
                      we hope you enjoy your <span>visibly</span></h2>}
                      </div> */}
                  </div>
              </div>
             </div>
            </div>
          </div>
        </div>

      </section>
    )
  }
}

function mapStateToProps (state) {
  return {}
}

function mapDispatchToProps (dispatch) {
  return {}
}

let connection = connect(mapStateToProps, mapDispatchToProps)

module.exports = connection(ThankYouPage)
