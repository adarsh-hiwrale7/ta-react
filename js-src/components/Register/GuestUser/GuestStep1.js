import { browserHistory } from 'react-router';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import GuestStep1Form from './GuestStep1Form';
import PreLoader from '../../PreLoader'
import StepsNav from '../StepsNav';
import * as authActions from "../../../actions/authActions"
import * as profileSettings from '../../../actions/settings/getSettings'
import * as settingsActions from "../../../actions/settingsActions"
import * as s3functions from '../../../utils/s3'


class GuestStep1 extends React.Component {

  constructor(props) {
    super(props);
    this.state={
    }
  }

  componentDidMount() {
    var props = this.props.profile.profile.email !== null ? this.props.profile.profile : this.props.auth.userinfo !== undefined ? this.props.auth.userinfo : ''
  var currAlbumName = localStorage.getItem('albumName')
  var userTourDetail = JSON.parse(localStorage.getItem(`${props.identity}_${currAlbumName}`))
    if(this.props.profile.profile.email !==null && this.props.profile.profile.onboard_step !==null && this.props.profile.profile.onboard_step !== 0 && this.props.profile.profile.email !=='' ){
      //to redirect back to feed page if signup is complete
      if(this.props.auth.feed_identity !==null){
        if(userTourDetail!== null){
          if(userTourDetail.isTourSkipped == false){
            browserHistory.push("/tour")
          }else{
            browserHistory.push(`/feed/custom/live/${this.props.auth.feed_identity}`);
          }
      }else{
        browserHistory.push("/tour")
      }
    }
    }
    this.props.profileSettings.GetProfileSettings()
    s3functions.loadAWSsdk()
  }
  handleSubmit(values){
    this.props.settingsActions.saveGuestSignupStep1(values);
  }
componentWillReceiveProps(newprops){
  var props = newprops.profile.profile.email !== null ? newprops.profile.profile : newprops.auth.userinfo !== undefined ? newprops.auth.userinfo : ''
  var currAlbumName = localStorage.getItem('albumName')
  var userTourDetail = JSON.parse(localStorage.getItem(`${props.identity}_${currAlbumName}`))
  if(newprops.profile.profile.email !==null && newprops.profile.profile.email !=='' && newprops.profile.profile.onboard_step !==null && newprops.profile.profile.onboard_step !==0){
    //to redirect back to feed page if signup is complete
    if(newprops.auth.feed_identity !==null){
      if(userTourDetail!== null){
        if(userTourDetail.isTourSkipped == false){
          browserHistory.push("/tour")
        }else{
          browserHistory.push(`/feed/custom/live/${newprops.auth.feed_identity}`);
        }
    }else{
      browserHistory.push("/tour")
    }
    }
  }
}
  renderLoading() {
    return (
      <div className="preloader-wrap">
        <PreLoader></PreLoader>
      </div>
    )
  }

  render() {
    const {clientId,loading} = this.props.onboarding;
    return (
      <section className="register-page register-page-in-visibly">
       <div class = "illusration-ragister">
           <div className = "man-img"><img src = "../img/Login-walk-man.png"/></div>
           <div className = "msg-box"><img src = "../img/Login-msg-icon.png"/></div>
           <div className = "long-msg-box"><img src = "../img/Login-walk-shadow.png"/></div>
         </div>
         { (loading ? this.renderLoading() : <div></div> ) }
              <div className="content">
                    <StepsNav active="step1" props={this.props}></StepsNav>
                    <div className="widget form-register-onboading">
                        <header className="heading clearfix" id="reg-profile">
                          <h3 className="heading-left">Guest Profile</h3>
                       </header>

                       <GuestStep1Form onSubmit={ this.handleSubmit.bind(this) }> </GuestStep1Form>

                    </div>
              </div>
        </section>
    );
  }
}

function mapStateToProps(state) {
  return {
    profile:state.profile,
    onboarding: state.settings.onboarding,
    auth: state.auth
  }
}

function mapDispatchToProps(dispatch) {
  return {
    profileSettings: bindActionCreators(profileSettings, dispatch),
    settingsActions: bindActionCreators(settingsActions, dispatch),
    authActions: bindActionCreators(authActions, dispatch)
  }
}

let connection = connect(mapStateToProps, mapDispatchToProps);

module.exports =  connection(GuestStep1);
