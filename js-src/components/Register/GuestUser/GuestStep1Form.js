import { Field, reduxForm } from 'redux-form';
import { renderField } from '../../Helpers/ReduxForm/RenderField'
import { renderDropzoneField } from '../../Helpers/ReduxForm/RenderField'
import { bindActionCreators } from 'redux'
import Globals from '../../../Globals'
import * as utils from '../../../utils/utils'
import * as settingsActions from "../../../actions/settingsActions"
import * as countryActions from "../../../actions/countryActions"
import { connect } from 'react-redux'
import PreLoaderMaterial from '../../PreLoaderMaterial'
import PreLoader from '../../PreLoader'
import ImageLoader from 'react-imageloader'
import notie from "notie/dist/notie.js"
import * as profileSettings from "../../../actions/settings/getSettings"
import ReactDropzone from '../../Chunks/ChunkReactDropzone';
const required = value => value ? undefined : 'Required';

const validEmail = value => value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value) ?
  'Invalid email address' : undefined;
var isHandleInitCall=false
class GuestStep1Form extends React.Component {
  constructor() {
    super()

    this.state = {
      reactDropzone:null
    }

  }
  componentWillMount() {
    ReactDropzone().then(dropzone=>{
      this.setState({
        reactDropzone:dropzone
      })
    })
  }
  componentDidMount() {
    isHandleInitCall=false
  }
  handleInitialize(nextProps = null) {
    let initData = {}
    if (nextProps.email !== null ) {
      initData = {
        email: nextProps.email,
        firstname:nextProps.first_name ? nextProps.first_name : nextProps.firstname,
        lastname:nextProps.last_name ? nextProps.last_name : nextProps.lastname,
      }
    }
    if (nextProps.email == null) {
      initData["email"] = null;
    }
    this.props.initialize(initData)
  }
  componentWillReceiveProps(newProps){
    if(isHandleInitCall==false && (newProps.profile.profile.email !==null  ) ){
      isHandleInitCall=true
      var profile=newProps.profile.profile
        this.handleInitialize(profile)
    }
  }
  removeProfilePic() {
    var me = this

    let objToSend = {
      avatar: ''
    }

    fetch(
      Globals.API_ROOT_URL + `/user/profile`,
      {
        method: 'PUT',
        headers: utils.setApiHeaders('put'),
        body: JSON.stringify(objToSend)
      }
    )
      .then(response => {
        if (response.status != '200') {
          notie.alert(
            'error',
            'There was a problem removing your avatar. Please try later',
            5
          )
        }
        this.props.profileSettings.GetProfileSettings();
        return response.json()
      })
      .then(json => {
        if (json.hasOwnProperty('data')) {
          notie.alert('success', 'Your avatar removed', 3)

          me.setState({
            hideImgWrap: true
          })
        }else{
          utils.handleSessionError(json)
        }
      })
      .catch(err => {
        throw err
      })
  }
  renderLoading () {
    return (
      <div className='preloader-wrap'>
        <PreLoader className='Page' />
      </div>
    )
  }
  handleDropzoneSubmit(field) {
    this.props.settingsActions.settingsProfilePost({ files: field, photoUpload: true }, "",'',true);
  }
  render() {
    const { handleSubmit, pristine, submitting } = this.props;
    var me = this
    var activeFlag=''
    if(this.props.profile.profile.email !== null){
      activeFlag=this.props.profile.profile.onboard_step
    }
    
    return (
      <form onSubmit={handleSubmit.bind(this)}>
      
        <div id="register-form">
          <div class="section">

            <div className='form-row profile-image-row'>
              <label className='block'>Avatar</label>
              <div className='profile-image-wrapper'>
                <div className="img-wrap">
                  <a href="javascript:void(0);" className='btn-remove-photo' onClick={this.removeProfilePic.bind(this)}><i className='fa fa-remove' /></a>
                  <ImageLoader
                    src={this.props.aws.uploadStarted ==true ?'':this.props.profile.isProfileUpdated==true?this.props.profile.profile.avatar:''}
                    preloader={PreLoaderMaterial}
                    onLoad={this.imageLoaded}
                     >
                    <div className="image-load-failed">
                      <i class="material-icons no-img-found-icon">&#xE001;</i>
                    </div>
                  </ImageLoader>

                  <Field
                    label='Avatar'
                    name='files'
                    cssClass="upload-avatar btn"
                    handleSubmit={(fileToUpload, e) => {
                      me.handleDropzoneSubmit(fileToUpload)
                    }}
                    accept='.jpg, .png, .jpeg'
                    title='Change avatar'
                    Dropzone={this.state.reactDropzone}
                    component={renderDropzoneField}
                  />
                </div>
              </div>
            </div>

            <Field placeholder="First Name" label="First Name" component={renderField} type="text" name="firstname" validate={[required]} />
            <Field placeholder="Last Name" label="Last Name" component={renderField} type="text" name="lastname" validate={[required]} />
            <div className='step1-mail form-row'>
              <Field
                placeholder='Email'
                label='Email'
                component={renderField}
                type='text'
                name="email"
                validate={[required]}
                disabled={
                  typeof this.props.auth !== 'undefined'
                }
              />
            </div>
            <div>
              <Field
                label='Designation'
                placeholder='Designation'
                component={renderField}
                type='text'
                name='designation'
                validate={[required]}
               />
            </div>
            <div>
                <Field placeholder="What is your company called?" label="Company Name" component={renderField} type="text" name="Company_Name" validate={[required]} />
            </div>
            
            <div className="form-row  button-register">
              <button
                className='btn btn-primary right'
                type='submit'
                disabled={pristine || submitting}>
                Finish
              </button>
            </div>
          </div>
        </div>
      </form>
    )
  }
}
function mapStateToProps(state) {
  return {
    aws: state.aws,
    auth:state.auth,
    profile: state.profile,
    settings: state.settings,
    allcountry: state.allcountry
  }
}

function mapDispatchToProps(dispatch) {
  return {
    profileSettings: bindActionCreators(profileSettings, dispatch),
    settingsActions: bindActionCreators(settingsActions, dispatch),
    countryActions: bindActionCreators(countryActions, dispatch)
  }
}
let connection = connect(mapStateToProps, mapDispatchToProps);
var reduxFormConnection = connection(GuestStep1Form);
export default reduxForm({
  form: 'GuestStep1Form',  // a unique identifier for this form
})(reduxFormConnection)
