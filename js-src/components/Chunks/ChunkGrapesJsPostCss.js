export default () => {
    return new Promise(resolve => {
      require.ensure(['grapesjs-parser-postcss'], function(require) {
        var GrapesJsPostCss = require('grapesjs-parser-postcss');
        resolve(GrapesJsPostCss);
      }, 'grapesjs-parser-postcss');
    });
  };