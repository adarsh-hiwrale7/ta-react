export default () => {
    return new Promise(resolve => {
      require.ensure(['react-autosuggest'], function(require) {
        var AutoSuggest = require('react-autosuggest');
        resolve(AutoSuggest);
      }, 'react-autosuggest');
    });
  };