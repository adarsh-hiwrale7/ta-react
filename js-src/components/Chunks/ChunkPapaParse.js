export default () => {  
    return new Promise(resolve => {

    	require.ensure(['papaparse'], function(require) {
    		var papaparse = require('papaparse');
			  resolve(papaparse);
      	}, 'papaparse');
    });
};