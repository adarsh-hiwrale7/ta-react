import Globals from '../../Globals'

export default () => {  
    return new Promise(resolve => {

    	require.ensure(['underscore'], function(require) {
            Globals.underscore = require('underscore')
            window._ = require('underscore')
            
      		resolve({
                _: require('underscore')
            });
      	}, 'underscore');
    });
};