export default () => {  
    return new Promise(resolve => {

    	require.ensure(['emojione-picker'], function(require) {
    		var emojipicker = require('emojione-picker');
			  resolve(emojipicker);
      	}, 'emojione-picker');
    });
};