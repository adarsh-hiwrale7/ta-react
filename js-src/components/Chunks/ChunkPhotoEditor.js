export default () => {
    return new Promise(resolve => {
      require.ensure(
        ['photoeditorsdk'],
        function (require) {
          var photoEditor = require('photoeditorsdk')
          require.ensure(
            ['photoeditorsdk/js/PhotoEditorSDK.UI.DesktopUI.js'],
            function (require) {
              var photoEditorJs = require('photoeditorsdk/js/PhotoEditorSDK.UI.DesktopUI.js')
            },
            'photoeditorsdk/js/PhotoEditorSDK.UI.DesktopUI.js'
          )
          resolve(photoEditor)
        },
        'photoeditorsdk'
      )
    })
  }
  