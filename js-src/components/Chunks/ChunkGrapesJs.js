export default () => {
  var GrapesJsBlockBasic = {};
  var GrapesJsBlockFlexbox = {};
  var GrapesJsCustomCode = {};
  var GrapesJsPluginActions = {};
  var GrapesJsParserPostcss = {};
  var GrapesJsPluginExport = {};
  var GrapesJsStyleFilter = {};
  var GrapesJsPresetNewsSeletter = {};
  var GrapesJsStyleGradient = {};
  var GrapesJsTouch = {};
  var GrapesJsImageEditor = {};
    return new Promise(resolve => {
      require.ensure(
        ['grapesjs'],
        function (require) {
          var Grapesjs = require('grapesjs')
          require.ensure(
            ['grapesjs-blocks-basic'],
            function (require) {
              GrapesJsBlockBasic= require('grapesjs-blocks-basic')
              // resolve(GrapesJsBlockBasic)
            },
            'grapesjs-blocks-basic'
          )
          
          require.ensure(
            ['grapesjs-blocks-flexbox'],
            function (require) {
              GrapesJsBlockFlexbox= require('grapesjs-blocks-flexbox')
            },
            'grapesjs-blocks-flexbox'
          )
          require.ensure(
            ['grapesjs-custom-code'],
            function (require) {
              GrapesJsCustomCode= require('grapesjs-custom-code')
            },
            'grapesjs-custom-code'
          )
          require.ensure(
            ['grapesjs-plugin-actions'],
            function (require) {
              GrapesJsPluginActions= require('grapesjs-plugin-actions')
            },
            'grapesjs-plugin-actions'
          )
          require.ensure(
            ['grapesjs-parser-postcss'],
            function (require) {
              GrapesJsParserPostcss= require('grapesjs-parser-postcss')
            },
            'grapesjs-parser-postcss'
          )
          require.ensure(
            ['grapesjs-plugin-export'],
            function (require) {
              GrapesJsPluginExport= require('grapesjs-plugin-export')
            },
            'grapesjs-plugin-export'
          )
          require.ensure(
            ['grapesjs-style-filter'],
            function (require) {
              GrapesJsStyleFilter= require('grapesjs-style-filter')
            },
            'grapesjs-style-filter'
          )
          require.ensure(
            ['grapesjs-preset-newsletter'],
            function (require) {
              GrapesJsPresetNewsSeletter= require('grapesjs-preset-newsletter')
            },
            'grapesjs-preset-newsletter'
          )
          require.ensure(
            ['grapesjs-style-gradient'],
            function (require) {
              GrapesJsStyleGradient= require('grapesjs-style-gradient')
            },
            'grapesjs-style-gradient'
          )
          require.ensure(
            ['grapesjs-touch'],
            function (require) {
              GrapesJsTouch= require('grapesjs-touch')
            },
            'grapesjs-touch'
          )
          require.ensure(
            ['grapesjs-tui-image-editor'],
            function (require) {
              GrapesJsImageEditor= require('grapesjs-tui-image-editor')
            },
            'grapesjs-tui-image-editor'
          )
          resolve(Grapesjs,GrapesJsBlockBasic,
            GrapesJsBlockFlexbox,
            GrapesJsCustomCode,
            GrapesJsPluginActions,
            GrapesJsParserPostcss,
            GrapesJsPluginExport,
            GrapesJsStyleFilter,
            GrapesJsPresetNewsSeletter,
            GrapesJsStyleGradient,
            GrapesJsTouch,
            GrapesJsImageEditor)
        },
        'grapesjs'
      )
    })
  }