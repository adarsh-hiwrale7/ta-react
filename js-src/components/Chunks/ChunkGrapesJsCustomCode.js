export default () => {
    return new Promise(resolve => {
      require.ensure(['grapesjs-custom-code'], function(require) {
        var GrapesJsCustom = require('grapesjs-custom-code');
        resolve(GrapesJsCustom);
      }, 'grapesjs-custom-code');
      
    });
  };