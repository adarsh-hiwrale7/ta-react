export default () => {
    return new Promise(resolve => {
      require.ensure(['react-select'], function(require) {
        var reactSelect = require('react-select');
        resolve(reactSelect);
      }, 'react-select');
    });
  };