export default () => {  
    return new Promise(resolve => {

    	require.ensure(['react-tooltip'], function(require) {
			var tooltip = require('react-tooltip');
			resolve(tooltip);
      	}, 'react-tooltip');
    });
};