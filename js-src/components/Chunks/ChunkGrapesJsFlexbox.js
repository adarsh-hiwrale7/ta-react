export default () => {
    return new Promise(resolve => {
      require.ensure(['grapesjs-blocks-flexbox'], function(require) {
        var GrapesJsFlexbox = require('grapesjs-blocks-flexbox');
        resolve(GrapesJsFlexbox);
      }, 'grapesjs-blocks-flexbox');
      
    });
  };