export default () => {  
    return new Promise(resolve => {

    	require.ensure(['react-throttle'], function(require) {
    		var reactthrottle = require('react-throttle');
			  resolve(reactthrottle);
      	}, 'react-throttle');
    });
};