export default () => {  
    return new Promise(resolve => {

    	require.ensure(['croppie'], function(require) {
			var croppie = require('croppie');
			resolve(croppie);
      	}, 'croppie');
    });
};