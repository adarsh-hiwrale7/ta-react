export default () => {  
    return new Promise(resolve => {

    	require.ensure(['react-slick'], function(require) {
			var reactSlick = require('react-slick');
			resolve(reactSlick);
      	}, 'react-slick');
    });
};