export default () => {  
    return new Promise(resolve => {

    	require.ensure(['bigCalendar'], function(require) {
    		var bigCalendar = require('bigCalendar');
			  resolve(bigCalendar);
      	}, 'bigCalendar');
    });
};