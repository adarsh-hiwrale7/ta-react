export default () => {  
    return new Promise(resolve => {

    	require.ensure(['perfect-scrollbar'], function(require) {
    		var perfectScrollbar = require('perfect-scrollbar');
			  resolve(perfectScrollbar);
      	}, 'perfect-scrollbar');
    });
};