export default () => {  
    return new Promise(resolve => {

    	require.ensure(['react-tag-cloud'], function(require) {
    		var tagcloud = require('react-tag-cloud');
			  resolve(tagcloud);
      	}, 'react-tag-cloud');
    });
};