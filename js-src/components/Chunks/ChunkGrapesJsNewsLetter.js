export default () => {
    return new Promise(resolve => {
      require.ensure(['grapesjs-preset-newsletter'], function(require) {
        var grapesjsNewsLetter = require('grapesjs-preset-newsletter');
        resolve(grapesjsNewsLetter);
      }, 'grapesjs-preset-newsletter');
      
    });
  };