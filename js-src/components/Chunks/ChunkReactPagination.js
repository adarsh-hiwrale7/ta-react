export default () => {  
    return new Promise(resolve => {

    	require.ensure(['react-js-pagination'], function(require) {
			var pagination = require('react-js-pagination');
			resolve(pagination);
      	}, 'react-js-pagination');
    });
};