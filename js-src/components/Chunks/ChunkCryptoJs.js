export default () => {  
    return new Promise(resolve => {

    	require.ensure(['crypto-js'], function(require) {
    		var cryptojs = require('crypto-js');
			  resolve(cryptojs);
      	}, 'crypto-js');
    });
};