export default () => {
    return new Promise(resolve => {
      require.ensure(['grapesjs-style-filter'], function(require) {
        var GrapesJsFilter = require('grapesjs-style-filter');
        resolve(GrapesJsFilter);
      }, 'grapesjs-style-filter');
    });
  };