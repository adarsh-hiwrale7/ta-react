export default () => {
    return new Promise(resolve => {
      require.ensure(['grapesjs-tui-image-editor'], function(require) {
        var GrapesJsImageEditor = require('grapesjs-tui-image-editor');
        resolve(GrapesJsImageEditor);
      }, 'grapesjs-tui-image-editor');
    });
  };