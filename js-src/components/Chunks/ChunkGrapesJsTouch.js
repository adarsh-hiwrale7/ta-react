export default () => {
    return new Promise(resolve => {
      require.ensure(['grapesjs-touch'], function(require) {
        var GrapesJsTouch = require('grapesjs-touch');
        resolve(GrapesJsTouch);
      }, 'grapesjs-touch');
    });
  };