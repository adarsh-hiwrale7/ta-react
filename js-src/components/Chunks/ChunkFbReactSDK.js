export default () => {
  return new Promise(resolve => {

    require.ensure(['fb-react-sdk'], function(require) {
      var FbReactSDK = require('fb-react-sdk');
      resolve(FbReactSDK);
    }, 'fb-react-sdk');
    
  });
};