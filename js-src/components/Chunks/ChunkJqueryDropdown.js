import Globals from '../../Globals'

export default () => {
  return new Promise(resolve => {
    require.ensure(
      ['jquery'],
      function (require) {
        
        Globals.jQuery = require('jquery')
        require.ensure(
          ['jquery-dropdown'],
          function (require) {
            resolve({
              jqueryDropdown: require('jquery-dropdown')
            })
          },
          'jquery-dropdown'
        )
      },
      'jquery'
    )
  })
}
