export default () => {
  return new Promise(resolve => {
    require.ensure(
      ['moment'],
      function (require) {
        var moment = require('moment')
        require.ensure(
          ['moment-timezone'],
          function (require) {
            var momentTimezone = require('moment-timezone')
          },
          'moment-timezone'
        )
        resolve(moment)
      },
      'moment'
    )
  })
}
