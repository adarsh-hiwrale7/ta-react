export default () => {
    return new Promise(resolve => {
      require.ensure(['grapesjs-plugin-actions'], function(require) {
        var GrapesJsActions = require('grapesjs-plugin-actions');
        resolve(GrapesJsActions);
      }, 'grapesjs-plugin-actions');
    });
  };