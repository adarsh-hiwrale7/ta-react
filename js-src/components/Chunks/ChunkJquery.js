import Globals from '../../Globals'

export default () => {  
    return new Promise(resolve => {
    	require.ensure(['jquery'], function(require) {
            Globals.jQuery = require('jquery')
            window.jQuery = require('jquery')
            window.$ = require('jquery')
            
      		resolve({
                $: require('jquery')
            });
      	}, 'jquery');
    });
};