export default () => {  
    return new Promise(resolve => {

    	require.ensure(['react-dropzone'], function(require) {
    		var reactDropzone = require('react-dropzone');
			  resolve(reactDropzone);
      	}, 'react-dropzone');
    });
};