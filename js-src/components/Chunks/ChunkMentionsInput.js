import Globals from '../../Globals';
export default () => {
    return new Promise(resolve => {
        require.ensure(
            ['jquery-events-input', 'jquery-elastic'],
            function(require)  {
                var jqueryEventsInput = require('jquery-events-input');
                var jqueryElastic = require('jquery-elastic');
                require.ensure(
                    ['jquery-mentionsInput'],
                    function(require) {
                        var jqueryMentionsInput = require('jquery-mentionsInput');

                        resolve(jqueryMentionsInput)
                    },
                    'jquery-mentionsInput'
                )
            },
            'jquery-mentionsInput-dependency'
        )
    })
}
