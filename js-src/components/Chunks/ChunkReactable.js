export default () => {  
    return new Promise(resolve => {

    	require.ensure(['reactable'], function(require) {
    		var reactable = require('reactable');
			  resolve(reactable);
      	}, 'reactable');
    });
};