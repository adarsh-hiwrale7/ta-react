export default () => {
    return new Promise(resolve => {
      require.ensure(['grapesjs-plugin-export'], function(require) {
        var GrapesJsExport = require('grapesjs-plugin-export');
        resolve(GrapesJsExport);
      }, 'grapesjs-plugin-export');
    });
  };