export default () => {  
    return new Promise(resolve => {

    	require.ensure(['react-router'], function(require) {
    		var reactRouter = require('react-router');
			  resolve(reactRouter);
      	}, 'react-router');
    });
};