export default () => {
    return new Promise(resolve => {
      require.ensure(['grapesjs-blocks-basic'], function(require) {
        var GrapesJsBlockBasic = require('grapesjs-blocks-basic');
        resolve(GrapesJsBlockBasic);
      }, 'grapesjs-blocks-basic');
      
    });
  };