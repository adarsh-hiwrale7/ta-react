export default () => {
    return new Promise(resolve => {
      require.ensure(['grapesjs-style-gradient'], function(require) {
        var GrapesJsGradient = require('grapesjs-style-gradient');
        resolve(GrapesJsGradient);
      }, 'grapesjs-style-gradient');
    });
  };