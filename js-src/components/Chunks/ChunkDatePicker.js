export default () => {
  return new Promise(resolve => {

    require.ensure(['react-datepicker'], function(require) {
      var reactDatepicker = require('react-datepicker');
      resolve(reactDatepicker);
    }, 'react-datepicker');
    
  });
};