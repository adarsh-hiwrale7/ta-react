export default () => {
  return new Promise(resolve => {

    require.ensure(['rc-time-picker'], function(require) {
      var reactTimepicker = require('rc-time-picker');
      
      resolve(reactTimepicker);
    }, 'rc-time-picker');
    
  });
};