export default () => {  
    return new Promise(resolve => {

    	require.ensure(['recharts'], function(require) {
			var recharts = require('recharts');
			resolve(recharts);
      	}, 'recharts');
    });
};