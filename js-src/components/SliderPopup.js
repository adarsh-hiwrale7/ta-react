
import ReactCSSTransitionGroup from 'react/lib/ReactCSSTransitionGroup';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import ReactDOM from 'react-dom';

import Globals from '../Globals'

class SliderPopup extends React.Component {
  render() {
    var me = this;
    const wide = this.props.wide;
    return (
      <ReactCSSTransitionGroup
        transitionName={this.props.fadePopup ? 'fade' : 'slide' }
        transitionAppear={true}
        transitionEnterTimeout={500}
        transitionLeaveTimeout={500}
        transitionAppearTimeout={1000}>
        <div className={` side-popup-wrap ${this.props.locationPopup ? "Create-post-wrap" : ''} ${this.props.sidePopupCenter ? this.props.sidePopupCenter : ''} ${this.props.updateCampaignPopup ? this.props.updateCampaignPopup : ''} `}>
        <span className='close-slider-popup' />
          <div id="side-popup-wrapper" className={`side-popup ${wide ? 'wide' : '' } ${this.props.className} ${this.props.locationPopup ? "Create-post-wrap-container" : ''}`}>
              <div className="scroller">
                {this.props.children} &nbsp;
              </div>

          </div>

        </div>

      </ReactCSSTransitionGroup>
    )


	}
}


function mapStateToProps(state) {
  return {
    general: state.general
  }
}

function mapDispatchToProps(dispatch) {
  return {
  }
}

module.exports = connect(mapStateToProps, mapDispatchToProps) (SliderPopup);
