import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  'in-container label': {
    'width': [{ 'unit': '%H', 'value': 0.3 }],
    'float': 'left',
    'marginLeft': [{ 'unit': 'px', 'value': 49 }]
  },
  'in-container input': {
    'width': [{ 'unit': '%H', 'value': 0.05 }],
    'float': 'right',
    'marginRight': [{ 'unit': '%H', 'value': 0.26 }],
    'marginTop': [{ 'unit': 'px', 'value': 1 }]
  },
  'in-container form-row': {
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }]
  },
  'in-container error': {
    'marginLeft': [{ 'unit': '%H', 'value': 0.26 }]
  },
  'points-tx button': {
    'background': '$blue !important',
    'color': 'white',
    'borderColor': '#fff',
    'width': [{ 'unit': '%H', 'value': 0.35 }],
    'marginRight': [{ 'unit': '%H', 'value': 0.52 }],
    'marginTop': [{ 'unit': 'px', 'value': 9 }],
    'marginBottom': [{ 'unit': 'px', 'value': 10 }]
  },
  'innercontainer': {
    'marginTop': [{ 'unit': '%V', 'value': 0.11 }]
  },
  'points-header': {
    'paddingLeft': [{ 'unit': 'px', 'value': 48 }]
  },
  'points-header p': {
    'fontSize': [{ 'unit': 'px', 'value': 16 }]
  },
  'h4': {
    'fontSize': [{ 'unit': 'px', 'value': 16 }]
  },
  'points-tx input': {
    'width': [{ 'unit': '%H', 'value': 0.2 }],
    'height': [{ 'unit': '%V', 'value': 0.02 }],
    'marginTop': [{ 'unit': 'px', 'value': 4 }]
  },
  'space': {
    'marginTop': [{ 'unit': '%V', 'value': 0.03 }]
  }
});
