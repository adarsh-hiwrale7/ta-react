import * as types from '../actionTypes';
import * as utils from '../../utils/utils'
import Globals from '../../Globals';

export function FetchUsersListSuccees(data) {
  return{
    type:types.USERS_LIST,
    userList:data
  }
}

export function fetchUsersList() {
    return (dispatch) => {
        fetch(Globals.API_ROOT_URL + `/user/profile/show`,{
          headers: utils.setApiHeaders('get')
        })
        .then(response => response.json())
        .then((json) => {
          dispatch(FetchUsersListSuccees(json))
        })
        .catch(err => { throw err;
          
        });
      }  
}