import * as types from './actionTypes';
import Globals from '../Globals'
import * as utils from '../utils/utils';
import notie from 'notie/dist/notie.js'

/** Get channel **/
export function getnotification(data) {
    return {
        type: types.FETCH_NOTIFICATION,
        notificationData: data
    }
}
export function fetchNotificationInit(){
  return {
    type: types.FETCH_NOTIFICATION_INIT,
}
}
export function fetchNotificationPostInit(){
    return {
      type: types.FETCH_NOTIFICATION_POST_INIT,
  }
}
export function getnotificationCount(data) {
    return {
        type: types.FETCH_NOTIFICATION_COUNT,
        notificationCount: data
    }
}
export function getPostdetail(data){
  return{
    type:types.FETCH_SELECTED_POST_DETAILS,
    postdata:data
  }
}
export function fetchNotification() {
  return (dispatch) => {
    dispatch(fetchNotificationInit())
    fetch(Globals.API_ROOT_URL + `/notification/user?notification_type=action`,{
      headers: utils.setApiHeaders('get'),
    })
      .then(response =>
           response.json())
      .then((json) => {
        if(json.data) {
          dispatch(fetchNotificationUnreadCount());
          dispatch(getnotification(json.data));
        }else{
          dispatch(getnotification([]));
          utils.handleSessionError(json)
        }
      })
      .catch(err => { throw err; });
  }
}

export function fetchNotificationUnreadCount() {
  return (dispatch) => {
    fetch(Globals.API_ROOT_URL + `/notification?notification_type=action`,{
      headers: utils.setApiHeaders('get'),
    })
      .then(response => response.json())
      .then((json) => {
        if(typeof json.count!=='undefined') {
          dispatch(getnotificationCount(json.count));
        }else{
          utils.handleSessionError(json)
        }
      })
      .catch(err => { throw err; });
  }
}
export function fetchSelectedPost(actionid) {
  return (dispatch) => {
    dispatch(fetchNotificationPostInit())
    fetch(Globals.API_ROOT_URL + `/post?post_identity=${actionid}`,{
      headers: utils.setApiHeaders('get'),
    })
      .then(response => response.json())
      .then((json) => {
        if(json.data) {
          dispatch(getPostdetail(json.data));
        }
        else{
          if(json.message){
            dispatch(postNotificationError(json.message));
          }else{
            utils.handleSessionError(json)
          }
        }
      })
      .catch(err => { throw err; });
  }
}

export function  postNotificationError(message){
    return{
      type: types.NOTIFICATION_ERROR,
      data: message
    }
}

export function sendReadPost(actionid) {
  return (dispatch) => {
    fetch(Globals.API_ROOT_URL + `/notification/user/${actionid}`, {
      method: 'PUT',
      headers: utils.setApiHeaders('put'),
    })
    .then(response =>  response.json())
      .then((json) => {
        if(json.code==200) {
          dispatch(fetchNotificationUnreadCount());
          dispatch(fetchNotification())
        }
        else{
          utils.handleSessionError(json)
          notie.alert('error', json.message, 3);
        }
      })
      .catch(err => { throw err; });
  }
}
export function markAllasRead(userid) {
  return (dispatch) => {
    fetch(Globals.API_ROOT_URL + `/notification/markread/${userid}`, {
      method: 'PUT',
      headers: utils.setApiHeaders('put'),
    })
    .then(response =>  response.json())
      .then((json) => {
        if(json.code==200) {
          dispatch(fetchNotificationUnreadCount());
          dispatch(fetchNotification())
        }
        else{
          utils.handleSessionError(json)
          notie.alert('error', json.message, 3);
        }
      })
      .catch(err => { throw err; });
  }
}
