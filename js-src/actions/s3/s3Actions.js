import * as types from '../actionTypes';
import Globals from '../../Globals';


export function uploadReset() {
 return {
    type: types.UPLOAD_RESET
  };
}

export function uploadingProgress(file, percent, index) {
  return {
    type: types.UPLOAD_PROGRESS,
    file: file,
    percent: percent,
    index: index
  }

}

export function uploadingDone() {
	return {
		type: types.UPLOAD_DONE
	}
}

export function uploadingStart() {
	return {
		type: types.UPLOAD_START
	}
}

export function uploadedItem(item) {
	return {
		type: types.UPLOAD_ITEM_SAVE,
		item: item
	}
}

export function storeFileDateTemp(data){
	return {
		type: types.STORE_FILE_DATA_TEMP,
		data: data
	}
}
