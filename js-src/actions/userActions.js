import * as types from './actionTypes';
import Globals from '../Globals';
import * as utils from '../utils/utils';
import notie from "notie/dist/notie.js"
import * as searchActions from "../actions/searchActions"

export function fetchUsersStart() {
    return {
        type: types.FETCH_USERS_START,
    };
}

export function fetchUsersSuccess(users) {
    return {
        type: types.FETCH_USERS_SUCCESS,
        users: users
    };
}

export function fetchUsersSuccessNoUsers() {
    return {
        type: types.USERS_NOT_FOUND,
    }
}

export function isFetching(status,isCallFromUserSidebarPopup=null) {
    if(isCallFromUserSidebarPopup==true){
        //if isfetching is calling from sidebar user profile popup then to seprate this popup and all user profile popup
        return{
            type:types.IS_FETCHING_FOR_SIDEBAR,
            status,
            isCallFromUserSidebarPopup
          };
    }else{
        return{
        type:types.IS_FETCHING,
        status
        };
    }
  }
  export function fetchList(data){
    return{
        type:types.FETCH_LIST,
        data:data
      };
  }
  export function sendCSV(data){
    return{
        type:types.SEND_CSV,
        data:data
      };
  }
export function fetchUsers(search=null,pageNo=1,sortingData=null, filterData=null, limit=10) {
    var encodeInput = encodeURIComponent(search);
    var user="user"
    var methodType = {}
    var arr={
        limit : limit,
        page :pageNo
    }
    if(search==null){
        if(sortingData !== null && Object.keys(sortingData).length>0){
            arr = {
                ...arr,
                sort : [sortingData],
            }
        }
        if(filterData !== null){
            arr = {
                ...arr,
                filterValues : filterData,
            }
        }
        var url = Globals.API_ROOT_URL + `/user-list`
        methodType ={
            headers: utils.setApiHeaders('post'),
            method: 'POST',
            body: JSON.stringify(arr)
        }
    }
    else
    {
        var url = Globals.API_ROOT_URL + `/globalSearch?search=${encodeInput}&type=${user}`
        methodType ={
            headers: utils.setApiHeaders('get'),
        }
    }
    return (dispatch) => {
        var me = this;
        dispatch(fetchUsersStart());
            fetch(url,methodType)
            .then(response => response.json())
            .then((json) => {
                if (typeof (json.data) !== 'undefined') {
                    dispatch(fetchUsersSuccess(json.data));
                    if(json.meta !== undefined ){
                        dispatch(fetchUsersMetaSuccess(json.meta));
                    }
                } else {
                    utils.handleSessionError(json)
                    dispatch(fetchUsersSuccessNoUsers())
                    dispatch(fetchUsersMetaSuccess());
                }
            }).catch(err => {
                throw err;
            });
    }
}
export function fetchUsersMetaSuccess(meta={}){
    return{
        type:types.FETCH_USERS_META_SUCCESS,
        data:meta
    }
}
export function fetchRolesInit(){
    return{
        type:types.FETCH_ROLES_INIT
    }
}
export function fetchRolesError(){
    return{
        type:types.FETCH_ROLES_ERROR
    }
}
export function fetchRoles() {
    return (dispatch) => {
        dispatch(fetchRolesInit());
        var me = this;
        fetch(Globals.API_ROOT_URL + `/role`,{
            headers: utils.setApiHeaders('get'),
        })
        .then(response => response.json())
        .then((json) => {
            if (typeof (json.data) !== 'undefined') {
                dispatch(fetchRolesSuccess(json.data));
            }else{
                dispatch(fetchRolesError());
                utils.handleSessionError(json)
            }
        }).catch(err => {
            throw err;
        });

    }
}
export function fetchUserDetailSuccess(userDetails,isCallFromUserSidebarPopup=null) {
    if(isCallFromUserSidebarPopup !== true){
        return {
            type: types.FETCH_USER_DETAILS_SUCCESS,
            userDetails
        };
    }else{
        return {
            type: types.FETCH_USER_DETAILS_SUCCESS_SIDEBAR_POPUP,
            userDetails
        };
    }
}
export function fetchUserDetail(id,isCallFromUserSidebarPopup=null) {
    return (dispatch) => {
        dispatch(isFetching(true,isCallFromUserSidebarPopup));
        var me = this;
        fetch(Globals.API_ROOT_URL + `/user/profile/show/${id}`,{
            headers: utils.setApiHeaders('get'),
        })
        .then(response => response.json())
        .then((json) => {
             if(json.message == "No user found"){
                dispatch(fetchUserDetailSuccess(json.message,isCallFromUserSidebarPopup));
            }
            else{
            if (typeof (json.data) !== 'undefined') {
                dispatch(isFetching(false,isCallFromUserSidebarPopup));
                dispatch(fetchUserDetailSuccess(json.data,isCallFromUserSidebarPopup));
             }else{
                dispatch(isFetching(false,isCallFromUserSidebarPopup));
                utils.handleSessionError(json)
             }
            }
        }).catch(err => {
            throw err;
            dispatch(isFetching(false,isCallFromUserSidebarPopup));
        });
      }
    }

export function fetchRolesSuccess(roles) {
    return {
        type: types.FETCH_ROLES_SUCCESS,
        roles
    };
}

export function AddUserInitiate() {
    return {
        type: types.ADD_USER_SAVING
    };
}

export function AddUserSuccess(data) {
    notie.alert('success',data, 3);
    return {
        type: types.ADD_USER_SUCCESS,
    };
}
export function AddcsvSuccess(data){
    notie.alert('success', data.message, 3);
    return {
        type: types.ADD_CSV_SUCCESS,
    };
}
export function AddcsvError() {
    notie.alert('error', 'There is some problem in adding users', 3)    
    return {
        type: types.ADD_CSV_ERROR,
        text: statusText,
    };
}

export function AddUserError(statusText = null) {
    statusText !== null ? notie.alert('error',statusText, 3) :''
    return {
        type: types.ADD_USER_ERROR,
        text: statusText,
    };
}

export function saveUser(fields,search=null,isFirstUserUpload=false) {
    return (dispatch) => {
        dispatch(AddUserInitiate());
         var arr = {
            'firstname': fields.name,
            'lastname': fields.surname,
            'email': fields.email_address,
            'role_id': fields.visibly_role,
            'mobile_country_code':fields.mobile_country_code,
            'mobile_no':fields.mobile_no,
            'department_identity': fields.department,
            'region': fields.region,
            'country':fields.country,
            'office': fields.office,
            'business_unit': fields.business_unit,
            'company_role': fields.company_role,
        }
        var content_preference = {
            'email':0,
            'SMS': 0
        }
        if(fields.email==true){
            content_preference.email=1;
        }
        if(fields.inapp==true){
            var objToAdd ={
                'in-app': 1
            }
            Object.assign(content_preference,objToAdd);
        }else{
            var objToAdd ={
                'in-app': 0
            }
            Object.assign(content_preference,objToAdd);
        }
        if(fields.enableSocial==false){
            var objToAdd ={
                'enable_social_posting': 0
            }
            Object.assign(arr,objToAdd);
        }else{
            var objToAdd ={
                'enable_social_posting': 1
            }
            Object.assign(arr,objToAdd);
        }
        if(fields.board==false){
            var objToAdd ={
                'board': 0
            }
            Object.assign(arr,objToAdd);
        }else{
            var objToAdd ={
                'board': 1
            }
            Object.assign(arr,objToAdd);
        }
        if(fields.sms==true){
            content_preference.SMS=1;
        }

        var objContent ={
            'content_preference':content_preference
        }
        Object.assign(arr,objContent);
        if(fields.birth_date!==undefined){
            var objToAdd ={
                'birth_date':fields.birth_date
            }
            Object.assign(arr,objToAdd);
        }
        if(fields.start_date!==undefined){
            var objToAdd ={
                'joining_date':fields.start_date
            }
            Object.assign(arr,objToAdd);
        }

        if(fields.level!==undefined){
            var objToAdd ={
                'level':fields.level
            }
            Object.assign(arr,objToAdd);
        }
        
        if(fields.gender!==undefined){
            var objToAdd ={
                'gender':fields.gender
            }
            Object.assign(arr,objToAdd);
        }
        fetch(Globals.API_ROOT_URL + `/user`, {
            method: 'POST',
            headers: utils.setApiHeaders('post'),
            body: JSON.stringify(arr)
        }).then(response =>response.json())
        .then((json) => {
            if (json.code == "200") {
                if(isFirstUserUpload==true){
                    var currUserEmail=localStorage.getItem('UserEmail') !== undefined ? localStorage.getItem('UserEmail') : ''
                    var meta = {URL: "/settings/users"};
                    userpilot.track("First user upload event", meta);
                    userpilot.identify(
                        currUserEmail,
                        {'isFirstUserCreated': true});
                }
                dispatch(AddUserSuccess(json.message));
                if(search==null)
                {
                    dispatch(fetchUsers(null,1));
                }
                else
                {
                    dispatch(fetchUsers(search));
                }
              } else {
                utils.handleSessionError(json)

                    if(json.message == undefined){   
                         var errorMessage = json.error.email? json.error.email[0]:'';
                    }
                    else{
                        var errorMessage = json.message 
                    }
                  dispatch(AddUserError(errorMessage));
              }
        })
        .catch(err => {
            throw err;
            
        });


    }

}
export function updateUser(fields,userAuthToken,search=null,pageNo=null,onlyPassPermissionField=null) {
    var identity = fields.identity;
    return (dispatch) => {

        dispatch(AddUserInitiate());
        if(onlyPassPermissionField==true){
            var arr = {}
        }else{
        var arr = {
            'firstname': fields.name,
            'lastname': fields.surname,
            // 'email': fields.email_address, email is not editable
            'role_id': fields.visibly_role,
            'department_identity': fields.department,
            'region': fields.region,
            'country':fields.country,
            'office': fields.office,
            'business_unit': fields.business_unit,
            'company_role': fields.company_role
        }
    }
        var content_preference = {
            'email':0,
            'SMS': 0
        }
        if(fields.email==true){
            content_preference.email=1;
        }
        if(fields.inapp==true){
            var objToAdd ={
                'in-app': 1
            }
            Object.assign(content_preference,objToAdd);
        }else{
            var objToAdd ={
                'in-app': 0
            }
            Object.assign(content_preference,objToAdd);
        }
        if(fields.sms==true){
            content_preference.SMS=1;
        }

        var objContent ={
            'content_preference':content_preference
        }
        Object.assign(arr,objContent);
        if(fields.birth_date!==undefined){
            var objToAdd ={
                'birth_date':fields.birth_date
            }
            Object.assign(arr,objToAdd);
        }
        if(fields.start_date!==undefined){
            var objToAdd ={
                'joining_date':fields.start_date
            }
            Object.assign(arr,objToAdd);
        }

        if(fields.level!==undefined){
            var objToAdd ={
                'level':fields.level
            }
            Object.assign(arr,objToAdd);
        }
        
        if(fields.gender!==undefined){
            var objToAdd ={
                'gender':fields.gender
            }
            Object.assign(arr,objToAdd);
        }
        if(fields.enableSocial==true){
            var objToAdd ={
                'enable_social_posting': 1
            }
            Object.assign(arr,objToAdd);
        }else{
            var objToAdd ={
                'enable_social_posting': 0
            }
            Object.assign(arr,objToAdd);
        }
        if(fields.board==true){
            var objToAdd ={
                'board': 1
            }
            Object.assign(arr,objToAdd);
        }else{
            var objToAdd ={
                'board': 0
            }
            Object.assign(arr,objToAdd);
        }
        fetch(Globals.API_ROOT_URL + `/user/profile/${identity}`, {
            method: 'PUT',
            headers: utils.setApiHeaders('put'),
            body: JSON.stringify(arr)
        }).then(response =>response.json())
        .then((json) => {
            if (json.data!==undefined) {
                notie.alert('success', 'User updated successfully.', 3);
                dispatch(AddUserSuccess());
                if(search!==null)
                dispatch(fetchUsers(search,pageNo));
                else
                dispatch(fetchUsers(null,pageNo));
              } else {
                utils.handleSessionError(json)
                  dispatch(AddUserError(json.statusText));
              }
        })
        .catch(err => {
            throw err;
        });
    }
}
export function pauseUser(userID,userAuthToken,search=null,pageNo=null) {
  return (dispatch) => {
    var arr = {
            'user_token': userAuthToken,
        }
    fetch(Globals.API_ROOT_URL + `/user/pause`, {
        method: 'PUT',
        headers: utils.setApiHeaders('put'),
        body: JSON.stringify(arr)
    }).then(response => response.json())
      .then((json) => {
        if (json.code== "200") {
            dispatch(fetchUsers(search,pageNo));          
          }else{
            utils.handleSessionError(json)
            notie.alert('error', json.message, 3);              
          }
    }).catch(err => {
      throw err;
    });
  }
}

export function trustedUser(userID,search=null,pageNo=null) {
    return (dispatch) => {
      var arr = {
              'user_id':userID
          }
      fetch(Globals.API_ROOT_URL + `/user/trust`, {
          method: 'PUT',
          headers: utils.setApiHeaders('put'),
          body: JSON.stringify(arr)
      }).then(response =>  response.json())
      .then((json) => {
        if (json.code!= "200") {
            utils.handleSessionError(json)
            notie.alert('error', json.message, 3);
          }else{
            notie.alert('success', json.message, 3);
            if(search==null)
            {
                dispatch(fetchUsers(search,pageNo));
            }
            else
            {
                dispatch(fetchUsers());
            }
          }
      }).catch(err => {
        throw err;
      });
    }
  }
  export function fecthFieldName() {
    return (dispatch) => {
      fetch(Globals.API_ROOT_URL + `/field`, {
          method: 'GET',
          headers: utils.setApiHeaders('get',
            {'Accept': 'application/json',
            'Content-Type': 'application/json'}),
      }).then(response => response.json())
        .then((json) => {
            if (json.code==200) {
                dispatch(fetchList(json.data));
            }else{
                utils.handleSessionError(json)
                notie.alert('error', json.statusText, 3);
            }
      }).catch(err => {
        throw err;
      });
    }
  }

  export function SendCsvData(csvdata,isFirstUserUpload=false) {
    return (dispatch) => {
      fetch(Globals.API_ROOT_URL + `/import`, {
            method: 'POST',
            headers: utils.setApiHeaders('post'),
            body: JSON.stringify(csvdata)
      }).then(response => response.json())
        .then((json) => {
            if (json.code == 200) {
                if(isFirstUserUpload==true){
                    var meta = {URL: "/settings/users"};
                    userpilot.track("First user upload event", meta);
                }
                dispatch(AddcsvSuccess(json));
                dispatch(fetchUsers(null,1));
              } else {
                utils.handleSessionError(json)
                  dispatch(AddcsvError(json));
              }
            if (json) {
            }

        })
        .catch(err => {
            throw err;
        });
    }
  }
