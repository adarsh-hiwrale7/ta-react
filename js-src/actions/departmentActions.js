import * as types from './actionTypes';
import Globals from '../Globals';
import * as utils from '../utils/utils';
import notie from "notie/dist/notie.js"

export function AddDepartmentInitiate() {
  return {type: types.ADD_DEPARTMENT_SAVING};
}

export function AddDepartmentSuccess(data) {
  return {type: types.ADD_DEPARTMENT_SUCCESS};
}

export function  deleteDepartmentInitilization(){
  return {type: types.DELETE_DEPARTMENT_INITILIZATION};
}
export function AddDepartmentError(statusText) {
  return {type: types.ADD_DEPARTMENT_ERROR, text: statusText};
}

export function saveDepartment(fields) {
  return (dispatch) => {

    dispatch(AddDepartmentInitiate());

    fetch(Globals.API_ROOT_URL + `/department`, {
      method: 'POST',
      headers: utils.setApiHeaders('post'),
      body: JSON.stringify(fields)
    }).then(response => {
      if (response.status == "200") {
        dispatch(AddDepartmentSuccess());
      } else {
        dispatch(AddDepartmentError(response.statusText));
      }
      return response.json();
    }).then((json) => {
      if (json.data) {
        notie.alert('success', json.message, 3);
        dispatch(AppendDepartmentSuccess(json.data, fields.name));
      }else{
        utils.handleSessionError(json)
      }

    }).catch(err => {
      throw err;
    });

  }

}

export function updateDepartment(fields,search=null) {
  return (dispatch) => {

    dispatch(AddDepartmentInitiate());

    fetch(Globals.API_ROOT_URL + `/department/${fields.identity}`, {
      method: 'PUT',
      headers: utils.setApiHeaders('put'),
      body: JSON.stringify({name: fields.name})
    }).then(response => {
      if (response.status == "200") {
        dispatch(AddDepartmentSuccess());
      } else {
        dispatch(AddDepartmentError(response.statusText));
      }
      return response.json();
    }).then((json) => {
      notie.alert('success', json.message, 3);
      if(search==null)
      {
        dispatch(fetchDepartments());
      }
      else{
      dispatch(fetchDepartments(search));
      }
      utils.handleSessionError(json)

    }).catch(err => {
      throw err;
    });

  }

}

export function deleteDepartment(deptID,search=null) {
  return (dispatch) => {
    dispatch(deleteDepartmentInitilization());
    fetch(Globals.API_ROOT_URL + `/department/${deptID}`, {
      method: 'DELETE',
      headers: utils.setApiHeaders('get')

    }).then(response => {
      return response.json();
    }).then((json) => {
      if (json.code ==  "200") {
        notie.alert('success', json.message, 3);
        if(search==null)
        {
          dispatch(fetchDepartments());
        }
        else
        {
          dispatch(fetchDepartments(search));
        }
       }else{

        utils.handleSessionError(json)
        dispatch(fetchNoDepartments())
         notie.alert('error', json.message, 3);
       }
    }).catch(err => {
      throw err;
    });
  }
}

export function AppendDepartmentSuccess(id, name) {
  var department = {
    identity: id,
    name: name
  };
  return {type: types.APPEND_DEPARTMENTS, added_department: department};
}
export function fetchNoDepartments(){
  return {type: types.FETCH_NO_DEPARTMENT};
}
export function fetchDepartmentsStart() {
  return {type: types.FETCH_DEPARTMENTS_START};
}

export function fetchDepartmentsSuccess(departments) {
  return {type: types.FETCH_DEPARTMENTS_SUCCESS, departments: departments};
}

export function fetchSuccessNoDepartments() {
  return {type: types.FETCH_SUCCESS_NO_DEPARTMENTS}
}

export function fetchDepartments(search=null) {
  if(search==null){
        return (dispatch) => {
          var me = this;
          dispatch(fetchDepartmentsStart());

          fetch(Globals.API_ROOT_URL + `/department`,{
            headers: utils.setApiHeaders('get'),
          })
          .then(response => response.json())
          .then((json) => {
            if (typeof(json.data) !== undefined) {
              dispatch(fetchDepartmentsSuccess(json.data));
            } else {
              utils.handleSessionError(json)
              dispatch(fetchNoDepartments())
            }
          }).catch(err => {
            throw err;
          });
        }
  }
  else
  {
    var encodeInput = encodeURIComponent(search);
    var dept="department"
    return (dispatch) => {
      var me = this;
      dispatch(fetchDepartmentsStart());

      fetch(Globals.API_ROOT_URL + `/globalSearch?search=${encodeInput}&type=${dept}`,{
        headers: utils.setApiHeaders('get'),
      })
      .then(response => response.json())
      .then((json) => {
        if (typeof(json.data) !== undefined) {
          dispatch(fetchDepartmentsSuccess(json.data));
        } else {
          utils.handleSessionError(json)
          dispatch(fetchNoDepartments())
        }
      }).catch(err => {
        throw err;
      });
    }
  }
}

/** Fetch department by id */
export function fetchDepartmentByIdSuccess(department) {
  return {type: types.FETCH_DEPARTMENT_BY_ID_SUCCESS, department: department};
}
export function fetchDepartmentById(id) {
  return (dispatch) => {
    var me = this;
    fetch(Globals.API_ROOT_URL + `/department/${id}`,{
      headers: utils.setApiHeaders('get'),
    }).then(response => response.json()).then((json) => {
      if (typeof(json.data) !== undefined) {
        dispatch(fetchDepartmentByIdSuccess(json.data));
      } else {
        utils.handleSessionError(json)
        dispatch(fetchNoDepartments())
      }
    }).catch(err => {
      throw err;
    });
  }
}
