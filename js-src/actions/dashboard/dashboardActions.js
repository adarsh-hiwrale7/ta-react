import * as types from '../actionTypes';
import Globals from '../../Globals'
import * as utils from '../../utils/utils';
import notie from "notie/dist/notie.js"


/** Get external post */
export function getExternalPost(externalPost) {
    return {
        type: types.FETCH_TOP_EXTERNAL_POST,
        dashboardExternalPost: externalPost
    }
}
export function getExternalPostError(){
  return{
    type:types.FETCH_TOP_EXTERNAL_POST_ERROR
  }
}
export function getPostsAssetsAnalytics(dashboardAnalytics) {
  return {
      type: types.FETCH_DASHBOARD_ANALYTICS,
      dashboardAnalytics
  }
}
export function getPostsAssetsAnalyticsError(){
  return{
    type:types.FETCH_DASHBOARD_ANALYTICS_ERROR
  }
}
export function getPostsAssetsAnalyticsInit() {
  return {
      type: types.FETCH_DASHBOARD_ANALYTICS_INIT
  }
}
export function fetchPostsAssetsAnalytics() {
  return (dispatch) => {
    dispatch(getPostsAssetsAnalyticsInit())
    fetch(Globals.API_ROOT_URL + `/dashboard/day-wise`,{
      headers: utils.setApiHeaders('get')
    })
      .then(response => response.json())
      .then((json) => {
        if(typeof json.data !== "undefined") {
          dispatch(getPostsAssetsAnalytics(json.data));
        }else{
          dispatch(getPostsAssetsAnalyticsError())
          utils.handleSessionError(json)
        }
        
      })
      .catch(err => { throw err; });
  }
}

export function modifyPostsAssetsAnalytics(newAnalytics) {
  return {
    type: types.MODIFY_DASHBOARD_ANALYTICS,
    dashboardAnalytics: newAnalytics
  }
}

export function getExternalPostInit() {
  return {
      type: types.FETCH_TOP_EXTERNAL_POST_INIT
  }
}
export function fetchTopExternalPost(type) {
  return (dispatch) => {
    dispatch(getExternalPostInit())
    fetch(Globals.API_ROOT_URL + `/dashboard/top-external-post?timespan=${type}&limit=1`,{
      headers: utils.setApiHeaders('get')
    })
      .then(response => response.json())
      .then((json) => {
          if(typeof json.data !== 'undefined') {
            dispatch(getExternalPost(json.data));
          }else{
            dispatch(getExternalPostError())
            utils.handleSessionError(json)
          }
      })
      .catch(err => { throw err; });
  }
}

/** Get company external post */
export function getCompanyExternalPost(companyExternalPost) {
    return {
        type: types.FETCH_TOP_COMPANY_EXTERNAL_POST,
        dashboardCompanyExternalPost: companyExternalPost
    }
}
export function getCompanyExternalPostInit() {
  return {
      type: types.FETCH_TOP_COMPANY_EXTERNAL_POST_INIT,
  }
}
export function getCompanyExternalPostError() {
  return {
      type: types.FETCH_TOP_COMPANY_EXTERNAL_POST_ERROR,
  }
}

export function fetchTopCompanyExternalPost(type) {
  return (dispatch) => {
    dispatch(getCompanyExternalPostInit())
    fetch(Globals.API_ROOT_URL + `/dashboard/top-external-post?timespan=${type}&limit=1&is_company=1`,{
      headers: utils.setApiHeaders('get')
    })
      .then(response => response.json())
      .then((json) => {
        if(typeof json.data !== 'undefined') {
          dispatch(getCompanyExternalPost(json.data));
        }else{
          dispatch(getCompanyExternalPostError())
          utils.handleSessionError(json)
        }
      })
      .catch(err => { throw err; });
  }
}


/** Get internal post **/
export function getTopInternalPost(internalPost) {
    return {
        type: types.FETCH_TOP_INTERNAL_POST,
        dashboardInternalPost: internalPost
    }
}
export function fetchTopInternalPostInit(){
  return {
    type: types.FETCH_TOP_INTERNAL_POST_INIT
  }
}
export function getTopInternalPostError(){
  return{
    type:types.FETCH_TOP_INTERNAL_POST_ERROR
  }
}
export function fetchTopInternalPost(type) {
  return (dispatch) => {
    dispatch(fetchTopInternalPostInit());
    fetch(Globals.API_ROOT_URL + `/dashboard/top-internal-post?timespan=${type}&limit=1`,{
      headers: utils.setApiHeaders('get')
    })
      .then(response => response.json())
      .then((json) => {
          if(typeof json.data !== 'undefined') {
            dispatch(getTopInternalPost(json.data));
          }else{
            dispatch(getTopInternalPostError())
          utils.handleSessionError(json)
        }
      })
      .catch(err => { throw err; });
  }
}

/** Get company internal post **/
export function getTopCompanyInternalPost(companyInternalPost) {
    return {
        type: types.FETCH_TOP_COMPANY_INTERNAL_POST,
        dashboardCompanyInternalPost: companyInternalPost
    }
}
export function fetchTopCompanyInternalPostInit(){
  return {
    type: types.FETCH_TOP_COMPANY_INTERNAL_POST_INIT
  }
}
export function fetchTopCompanyInternalPostError(){
  return {
    type: types.FETCH_TOP_COMPANY_INTERNAL_POST_ERROR
  }
}
export function fetchTopCompanyInternalPost(type) {
  return (dispatch) => {
    dispatch(fetchTopCompanyInternalPostInit())
    fetch(Globals.API_ROOT_URL + `/dashboard/top-internal-post?timespan=${type}&limit=1&is_company=1`,{
      headers: utils.setApiHeaders('get')
    })
      .then(response => response.json())
      .then((json) => {
        if(typeof json.data !== 'undefined') {
          dispatch(getTopCompanyInternalPost(json.data));
        }else{
          dispatch(fetchTopCompanyInternalPostError())
          utils.handleSessionError(json)
        }
      })
      .catch(err => { throw err; });
  }
}

/** Get recent assets **/
export function getMyRecentAssets(myRecentAssets) {
    return {
        type: types.FETCH_MY_RECENT_ASSETS,
        dashboardMyRecentAssets: myRecentAssets
    }
}
export function fetchMyRecentAssetsInit(){
  return {
    type: types.FETCH_MY_RECENT_ASSETS_INIT
  }
}
export function fetchMyRecentAssetsError(){
  return {
    type: types.FETCH_MY_RECENT_ASSETS_ERROR
  }
}
export function fetchMyRecentAssets(type) {
  return (dispatch) => {
    dispatch(fetchMyRecentAssetsInit())
    fetch(Globals.API_ROOT_URL + `/asset-user?timespan=${type}&limit=2`,{
      headers: utils.setApiHeaders('get')
    })
      .then(response => response.json())
      .then((json) => {
        if(typeof json.data !== 'undefined') {
          dispatch(getMyRecentAssets(json.data));
        }else{
          dispatch(fetchMyRecentAssetsError())
          utils.handleSessionError(json)
        }
      })
      .catch(err => { throw err; });
  }
}


/** Get post for map **/
export function getMapPost(mapPosts) {
    return {
        type: types.FETCH_MAP_POSTS,
        mapPosts: mapPosts
    }
}
export function getMapPostInit() {
  return {
      type: types.FETCH_MAP_POSTS_INIT
  }
}
export function getMapPostError(){
  return {
    type: types.FETCH_MAP_POSTS_ERROR
  }
}
export function fetchMapPosts(type) {
  return (dispatch) => {
    dispatch(getMapPostInit())
    fetch(Globals.API_ROOT_URL + `/widget?name=map&widget_secret=${localStorage.getItem('map_secret')}&timespan=${type}`,{
      headers: utils.setApiHeaders('get')
    })
      .then(response => response.json())
      .then((json) => {
        if(json.data) {
          dispatch(getMapPost(json.data));
        }else{
          dispatch(getMapPostError())
          utils.handleSessionError(json)
        }
      })
      .catch(err => { throw err; });
  }
}

//get storage price when you add more storage
export function getStoragePrice(){
  return (dispatch) => {
    dispatch(getStoragePriceInit());
    fetch(Globals.API_ROOT_URL + `/billing/current-plan`,{
      headers: utils.setApiHeaders('get')
    })
      .then(response => response.json())
      .then((json) => {
          if(typeof json.data !== "undefined"){
            dispatch(getStoragePriceSuccess(json.data));
          }else{
            dispatch(getStoragePriceFail());
          }
          
      })
      .catch(err => { throw err; });
  }
}

export function getStoragePriceInit(){
    return {
        type: types.FETCH_STORAGE_PRICE_INIT
    }
}

export function getStoragePriceFail(){
    return {
        type: types.FETCH_STORAGE_PRICE_FAIL
    }
}

export function getStoragePriceSuccess(planData){
  return {
        type: types.FETCH_STORAGE_PRICE,
        planData: planData
    }
}
/**
* Add more storage 
* @author Yamin
* @param storage
**/
export function addMoreStorage(storage){

                    return (dispatch) => {
                         notie.confirm(
                        `Are you sure you want to add storage to your account?`,
                        'Yes',
                        'No',
                        function () {
                    
                            var storageObj = {'storage_amount': storage*1024}
                                dispatch(addMoreStorageInit());
                                fetch(Globals.API_ROOT_URL + `/storage/add`, {
                                  method: 'POST',
                                  headers: utils.setApiHeaders('post'),
                                  body: JSON.stringify(storageObj)
                                }).then(response => {
                                  return response.json();
                                }).then((json) => {
                                   if(json.data.code == 200){
                                      notie.alert('success', json.data.message, 3);
                                      dispatch(addMoreStorageSuccess());
                                   }else{
                                      notie.alert('error', json.data.message, 3);
                                      dispatch(addMoreStorageError());
                                   }
                                }).catch(err => {
                                  throw err;
                                });
                        },
                        function () { }
                        )   
                    }  
}

export function addMoreStorageInit(){
    return {
        type: types.ADD_STORAGE_INIT
    }
}

export function addMoreStorageSuccess(){
    return {
        type: types.ADD_STORAGE_SUCCESS
    }
}

export function addMoreStorageError(){
    return {
        type: types.ADD_STORAGE_ERROR
    }
}