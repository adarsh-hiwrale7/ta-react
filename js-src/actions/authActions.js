import * as types from './actionTypes'
import Globals from '../Globals'
import { browserHistory } from 'react-router'
import notie from 'notie/dist/notie.js'
import { fetchSocialAccounts } from './socialAccountsActions'
import { fetchUsersList } from '../actions/fetchuser/fetchuserActions';
import { profileSuccessFlag } from './settingsActions'
import { settingsError } from './settingsActions'
import * as utils from '../utils/utils';
import { GetProfileSettings } from './settings/getSettings';
import { fetchDepartments } from './departmentActions';
import {CometChat} from "@cometchat-pro/chat";
import {initializeFirebase} from '../firebase';

var path = utils.removeTrailingSlash(window.location.href).split('/')
export function authUserInitiate() {
  return {
    type: types.AUTH_USER_INITIATE
  }
}
/**
 * @author disha
 * if cookie is set then to get role details 
 */
export function getRole(map_secret, widget_secret, albumName, sessionId) {
  var sessionid = Globals.SESSION_ID !== null ? Globals.SESSION_ID : sessionId
  return dispatch => {
    fetch(
      Globals.API_ROOT_URL +
      `/user/profile`,
      {
        method: 'GET',
        headers: utils.setApiHeaders('get', {
          Accept: 'application/json',
          'Content-Type': 'application/json'
        }, sessionid)
      }
    )
      .then(response => {
        if (response.status != '200') {
          dispatch(settingsError(response.statusText))
        }
        return response.json()
      })
      .then(json => {
        if (json.hasOwnProperty('data')) {
          var profile = json.data
          var objToAdd = {
            map_widget_secret: map_secret,
            wall_widget_secret: widget_secret,
            s3_folder_name: albumName
          }
          Object.assign(profile, objToAdd)
          dispatch(setLocalStorageFromCookie({ sessionId, profile }))
          dispatch(profileSuccessFlag(json.data))
        }
      })
      .catch(err => {
        throw err
      })
  }
}
/**
 * to set localstorage value from cookie 
 * @param {*} values 
 */
export function setLocalStorageFromCookie(values) {
  localStorage.setItem('session_id', values.sessionId)

  Globals.SESSION_ID = values.sessionId

  $session_id = values.sessionId

  if (values.hasOwnProperty('profile')) {
    localStorage.setItem('roles', JSON.stringify(values.profile.role.data))
    localStorage.setItem('map_secret', typeof values.profile !== undefined ? values.profile.map_widget_secret : null)
    localStorage.setItem('widget_secret', typeof values.profile !== undefined ? values.profile.wall_widget_secret : null)
    localStorage.setItem('albumName', values.profile.s3_folder_name)
    Globals.AWS_CONFIG.albumName = values.profile.s3_folder_name
  }
  return {
    type: types.AUTH_USER_SUCCESS,
    payload: {
      session_id: values.sessionId,
      userinfo: typeof values.profile !== undefined ? values.profile : null
    }
  }
}


export function guestUserData(data) {
  return {
    type: types.GUEST_USER_DATA,
    payload: data
  }
}
export function guestUserRedirection(sessionId) {
  return (dispatch) => {
    fetch(Globals.API_ROOT_URL + `/feed/guest`, {
      headers: utils.setApiHeaders('get', null, sessionId)
    })
      .then(response => {
        return response.json();
      }).then((json) => {
        if (json.code == 200) {
          dispatch(guestUserData(json.data))
        }
      })
  }
}

/**
 * @author disha
 * @param {*} data name of selected domain
 * @param {*} userEmailPassword email id and password
 * @param {*} ip_address ipaddress
 */
export function authUser(data = null, userEmailPassword = null, ip_address = null, ssoData = null) {
  console.log(ssoData,'ssoData1')
  var ipAddress = ''
  if (ip_address == '' || ip_address == undefined) {
    ipAddress = '192.168.0.109'
  } else {
    ipAddress = ip_address
  }
  var userAgent = navigator.userAgent

  if (ssoData !== null) {
    var objToSend = {
      "ip_address": ipAddress,
      "user_agent": userAgent,
      "sso_data": ssoData
    }
  } else {
    var objToSend = {
      "email": userEmailPassword.email,
      "password": userEmailPassword.password,
      "domain": data.domainList,
      "ip_address": ipAddress,
      "user_agent": userAgent
    }
  }

  /*console.log(objToSend,'objToSend');
  return;*/
  return dispatch => {
    dispatch(authUserInitiate())
    fetch(Globals.API_ROOT_URL_MASTER + `/setAccount`, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(objToSend)
    }).then(response => {
      return response.json();
    }).then((json) => {
      if (json.hasOwnProperty('data')) {
        var currDate = new Date().toLocaleString();
        var tempcurrDate = currDate.split(',');
        var dateSplit = tempcurrDate[0].split("/");
        var actualDate = dateSplit[2] + "/" + dateSplit[1] + "/" + dateSplit[0] + "" + tempcurrDate[1]

        //User pilot script after login successfully/
        userpilot.identify( 
          json.data.email, // Used to identify users 
            { 
                name: json.data.firstname+" "+json.data.lastname, // Full name 
                email: json.data.email, // Email address 
                created_at: typeof json.data.register_date ? json.data.register_date : actualDate.toString(), // Signup date as a Unix timestamp 
                role: json.data.role.data[0].role_display_name
            }
        );

        if (json.data.role.data[0].role_name == "guest") {
          dispatch(guestUserRedirection(json.data.session_id))
        }
        if(json.data.sso_onboarding == false && json.data.role.data[0].role_name == 'employee'){
          const fromSSOUser= true
          localStorage.setItem('ssoUser', fromSSOUser)
          dispatch(authUserSuccess({ session_id: json.data.session_id, profile: json.data }, true,fromSSOUser,ssoData))
        }else{
          dispatch(authUserSuccess({ session_id: json.data.session_id, profile: json.data }, true,false,ssoData))
        }
      } else {
        let error = json.error
        if (typeof error.password !== 'undefined') {
          dispatch(authUserError(error.password[0]))
        } else if (typeof error.email !== 'undefined') {
          dispatch(authUserError(error.email[0]))
        } else if (typeof error.domain !== 'undefined') {
          dispatch(authUserError(error.domain[0]))
        } else if (typeof error.secret !== 'undefined') {
          dispatch(authUserError(error.secret))
        } else {
          dispatch(authUserError(error))
        }
        if (ssoData !== null) {
          browserHistory.push('/login')
        }
      }
    }).catch(err => {
      throw err;
    });
  }
}

export function authUserSuccess(values, isCallFromLogin = null,fromSSOUser =false,ssoData=null) {
  console.log(ssoData,'ssoData')
   if(values.hasOwnProperty('profile'))
   {
    CometChat.init(values.profile.cometchat_app_id, new CometChat.AppSettingsBuilder().subscribePresenceForAllUsers().setRegion(values.profile.cometchat_app_region).build()); 
    initializeFirebase(values.profile.cometchat_app_id,values.profile.identity);
   //cometchat authentication
    CometChat.login(values.profile.identity, values.profile.cometchat_app_key).then(

      user => {
        localStorage.setItem('uid', user.uid)
        localStorage.setItem('chatToken', user.authToken)
        localStorage.setItem('chatKey', values.profile.cometchat_app_key)

      },
      error => {
        console.log('chat', error)
        localStorage.setItem('chatToken', null)
      });
   }

  var domainName = path !== undefined && path[2] !== undefined ? path[2].split(':')[0] : '' // get domain name from path to set domain name in cookie and if visibly is running on localhost:3030 then to get only localhost i split the string
  return (dispatch) => {
    if (isCallFromLogin == true && isCallFromLogin !== null) {

      //to delete all stored cookie on login(if any cookie is there)
      deleteCookie();

      //to get date after 5 years from now to set exipre date of cookie
      var expireDate = new Date();
      expireDate.setFullYear(expireDate.getFullYear() + 5);

      document.cookie = `session_id=${values.session_id};expires=${expireDate};domain=${domainName};`

      if (values.hasOwnProperty('profile')) {
        document.cookie = `map_secret=${typeof values.profile !== undefined ? values.profile.map_widget_secret : null};expires=${expireDate};domain=${domainName};`
        document.cookie = `widget_secret=${typeof values.profile !== undefined ? values.profile.wall_widget_secret : null};expires=${expireDate};domain=${domainName};`
        document.cookie = `albumName=${values.profile.s3_folder_name};expires=${expireDate};domain=${domainName};`  //secure;
      }
    }

    localStorage.setItem('session_id', values.session_id)
    Globals.SESSION_ID = values.session_id
    $session_id = values.session_id
    if (values.hasOwnProperty('profile')) {

        localStorage.setItem('roles', JSON.stringify(values.profile.role.data))
        localStorage.setItem('map_secret', typeof values.profile !== undefined ? values.profile.map_widget_secret : null)
        localStorage.setItem('widget_secret', typeof values.profile !== undefined ? values.profile.wall_widget_secret : null)
        localStorage.setItem('albumName', values.profile.s3_folder_name)
        Globals.AWS_CONFIG.albumName = values.profile.s3_folder_name
    }
    if (isCallFromLogin == true && isCallFromLogin !== null) {
      dispatch(GetProfileSettings());
      dispatch(fetchSocialAccounts());
      dispatch(fetchDepartments());
      dispatch(fetchUsersList());
      console.log(ssoData,'ssoDatassoData')
      ssoData !== null ? browserHistory.push('/tour') :''
    }
    dispatch(authUserSuccessData(values));
  }
}

export function authUserSuccessData(values) {
  return {
    type: types.AUTH_USER_SUCCESS,
    payload: {
      session_id: values.session_id,
      userinfo: typeof values.profile !== undefined ? values.profile : null
    }
  }

}

export function verifyUserSuccess(values) {
  localStorage.setItem('session_id', values.session_id)

  $session_id = values.session_id
  Globals.SESSION_ID = values.session_id
  return {
    type: types.VERIFY_USER_SUCCESS,
    payload: {
      auth_token: values.auth_token,
      api_secret: values.api_secret
    }
  }
}

export function authUserError(values) {
  deleteCookie();
  deleteLocalStorage();
  notie.alert('error', values, 3)
  return {
    type: types.AUTH_USER_ERROR
  }
}
/**
 * to destroy all cookie value
 * @author disha
 * @returns nothing
 */
export function deleteCookie() {

  var domainName = path !== undefined && path[2] !== undefined ? path[2].split(':')[0] : ''
  document.cookie = `map_secret=; expires=Thu, 01 Jan 1970 00:00:00 UTC;domain=${domainName};path=/;`
  document.cookie = `map_secret=; expires=Thu, 01 Jan 1970 00:00:00 UTC;domain=${domainName};path=/feed;`
  document.cookie = `map_secret=; expires=Thu, 01 Jan 1970 00:00:00 UTC;domain=${domainName};path=/feed/default;`

  document.cookie = `widget_secret=; expires=Thu, 01 Jan 1970 00:00:00 UTC;domain=${domainName};path=/;`
  document.cookie = `widget_secret=; expires=Thu, 01 Jan 1970 00:00:00 UTC;domain=${domainName};path=/feed;`
  document.cookie = `widget_secret=; expires=Thu, 01 Jan 1970 00:00:00 UTC;domain=${domainName};path=/feed/default;`

  document.cookie = `albumName=; expires=Thu, 01 Jan 1970 00:00:00 UTC;domain=${domainName};path=/;`
  document.cookie = `albumName=; expires=Thu, 01 Jan 1970 00:00:00 UTC;domain=${domainName};path=/feed;`
  document.cookie = `albumName=; expires=Thu, 01 Jan 1970 00:00:00 UTC;domain=${domainName};path=/feed/default;`

  document.cookie = `session_id=; expires=Thu, 01 Jan 1970 00:00:00 UTC;domain=${domainName};path=/;`
  document.cookie = `session_id=; expires=Thu, 01 Jan 1970 00:00:00 UTC;domain=${domainName};path=/feed;`
  document.cookie = `session_id=; expires=Thu, 01 Jan 1970 00:00:00 UTC;domain=${domainName};path=/feed/default;`
}
/**
 * to destroy all localstorage value
 * @author disha
 * @returns nothing
 */
export function deleteLocalStorage() {
  localStorage.removeItem('roles')
  localStorage.removeItem('map_secret')
  localStorage.removeItem('widget_secret')
  localStorage.removeItem('session_id')
  localStorage.removeItem('albumName')
  localStorage.removeItem('chatToken')
  localStorage.removeItem('uid')

}
export function logout(isCallFromStep1 = false) {
  deleteCookie();
  deleteLocalStorage();
  browserHistory.push('/login')
  return {
    type: types.AUTH_LOGOUT
  }
}

// to delete cookie and localstorage without redirection from signup page
export function logoutFromSignup() {
  deleteCookie();
  deleteLocalStorage();
  return {
    type: types.AUTH_LOGOUT
  }
}
