import * as types from './actionTypes';
import Globals from '../Globals';
import * as utils from '../utils/utils';

export function fetchTagsStart() {
    return {
        type: types.FETCH_TAGS_START,
    };
}

export function fetchTagsSuccess(tags) {
    return {
        type: types.FETCH_TAGS_SUCCESS,
        tags: tags
    };
}

export function fetchPostTagsSuccess(tags) {
    return {
        type: types.FETCH_POST_TAGS_SUCCESS,
        tags: tags
    };
}

export function fetchUserTagsSuccess(tags) {
    return {
        type: types.FETCH_USER_TAGS_SUCCESS,
        tags: tags
    };
}

export function fetchCompanyTags(tags) {
    return {
        type: types.FETCH_COMAPANY_TAGS_SUCCESS,
        tags: tags
    };
}

export function fetchHashTagsSuccess(tags) {
    return {
        type: types.FETCH_HASH_TAGS_SUCCESS,
        tags: tags
    };
}

export function fetchTagsSuccessNoTags() {
    return {
        type: types.TAGS_NOT_FOUND,
    }
}

export function fetchTags(type) {
    return (dispatch) => {
        var me = this;
        dispatch(fetchTagsStart());

        fetch(Globals.API_ROOT_URL + `/tags?param=${type}`,
            {headers: utils.setApiHeaders('get')}
        )
            .then(response => response.json())
            .then((json) => {
                if (typeof (json.data) !== 'undefined') {
                    var tagArray = [];
                    json.data.forEach(function(tag, index) {
                        tagArray.push({
                            'label': tag.name,
                            'value': tag.identity
                        })
                    });
                    dispatch(fetchTagsSuccess(tagArray));
                } else {
                    utils.handleSessionError(json)
                    dispatch(fetchTagsSuccessNoTags())
                }
            }).catch(err => {
            throw err;
        });
    }
}

export function fetchPostTags(type) {
    return (dispatch) => {
        var me = this;
        dispatch(fetchTagsStart());

        fetch(Globals.API_ROOT_URL + `/tags?param=${type}`,
            {headers: utils.setApiHeaders('get')}
            )
            .then(response => response.json())
            .then((json) => {
                if (typeof (json.data) !== 'undefined') {
                    var tagArray = [];
                    json.data.forEach(function(tag, index) {
                        tagArray.push({
                            'display': tag.name,
                            'id': tag.identity
                        })
                    });
                    dispatch(fetchPostTagsSuccess(tagArray));
                } else {
                    utils.handleSessionError(json)
                    dispatch(fetchTagsSuccessNoTags())
                }
            }).catch(err => {
            throw err;
        });
    }
}


export function fetchUserTags(newLocation,feedId=null,campID=null) {
    var APIString;
    var feed_identity;
    if(campID!==null){
        APIString=`?campaign_identity=${campID}`
    }
    else if(feedId!==null&&feedId!==undefined&&feedId!==''){
        APIString=`?feed_identity=${feedId}`
       
    }else if(newLocation !==undefined && newLocation[4] &&newLocation[4]!=='folder'){
        feed_identity=newLocation[4];
        APIString=`?feed_identity=${feed_identity}`
    }
    else{
        if(newLocation !==undefined && newLocation[2]=="default"){
            APIString=`?feed_identity=rpPDp`
        }
        else{
            if(newLocation !== undefined && newLocation[1] !== undefined && feedId==null && ( newLocation[1]=="assets" || newLocation[1]=="campaigns") ){
                APIString=`?tag_user_type=user`
            }
            else{
                APIString=``
            }
        }
    }
    return (dispatch) => {
        var me = this;
        dispatch(fetchTagsStart());

        fetch(Globals.API_ROOT_URL + `/user/tag${APIString}`,{
             headers: utils.setApiHeaders('get')})
        .then(response => response.json())
            .then((json) => {
                    var tagArray = [];
                    var company_tags = [];
                 if(json.users !==undefined){
                    json.users.forEach(function(tag, index) {
                        tagArray.push({
                            'display': tag.firstname + ' ' + tag.lastname,
                            'id': tag.identity,
                            'email': tag.email,
                            'profile_image':tag.profile_image,
                            'type': 'User'
                        })
                    });
                    json.departments.forEach(function(tag, index) {
                        tagArray.push({
                            'display': tag.name,
                            'id': tag.identity + '_dept',
                            'type': 'Department',
                            'office_name':tag.office_name
                        })
                    });
                    if(json.company!==undefined){
                        tagArray.push({
                            'display': json.company.name,
                            'id': json.company.identity + '_company',
                            'type': 'Company'
                        })
                        company_tags.push({
                            'tagid': json.company.identity ,
                            'tagName': json.company.name,
                            'type': 'company'
                        })
                    }
                    dispatch(fetchCompanyTags(company_tags))
                    dispatch(fetchUserTagsSuccess(tagArray));
                }else{
                    utils.handleSessionError(json)
                }
            }).catch(err => {
            throw err;
        });
    }
}

export function fetchUserTagsAll(userType = null, feedId =null) {
    console.log('get user tag ', userType)


    var APIString;
    APIString=Globals.API_ROOT_URL + `/user/tag`
    
    if(userType !== null){
        APIString = `${APIString}?tag_user_type=user`;
    }
    if(feedId !== null){
        APIString = `${APIString}&feed_identity=${feedId}`;
    }
        
    return (dispatch) => {
        var me = this;

        fetch(APIString,{
            headers: utils.setApiHeaders('get')
        })
            .then(response => response.json())
            .then((json) => {
                    var tagArray = [];
                 if(json.users !==undefined){
                    json.users.forEach(function(tag, index) {
                        tagArray.push({
                            'display': tag.firstname + ' ' + tag.lastname,
                            'id': tag.identity,
                            'email': tag.email,
                            'profile_image':tag.profile_image,
                            'type': 'User'
                        })
                    });
                    json.departments.forEach(function(tag, index) {
                        tagArray.push({
                            'display': tag.name,
                            'id': tag.identity + '_dept',
                            'type': 'Department',
                            'office_name':tag.office_name
                        })
                    });
                    if(json.company!==undefined){
                        tagArray.push({
                            'display': json.company.name,
                            'id': json.company.identity + '_company',
                            'type': 'Company'
                        })
                    }
                    dispatch(fetchUserTagsAllSuccess(tagArray));
                }else{
                    utils.handleSessionError(json)
                }
            }).catch(err => {
            throw err;
        });
    }
}



export function fetchHashTags() {
    return (dispatch) => {
        var me = this;
        dispatch(fetchTagsStart());

        fetch(Globals.API_ROOT_URL + `/tags/hashsearch?q=hash`,{
            headers: utils.setApiHeaders('get')
        })
            .then(response => response.json())
            .then((json) => {
                if (typeof (json.data) !== undefined) {
                    var tagArray = [];
                    /*json.data.forEach(function(tag, index) {
                        tagArray.push({
                            'display': tag.title,
                            'id': tag.identity
                        })
                    });*/
                    dispatch(fetchHashTagsSuccess(tagArray));
                } else {
                    utils.handleSessionError(json)
                    dispatch(fetchTagsSuccessNoTags())
                }
            }).catch(err => {
            throw err;
        });
    }
}
//fetch trending tags
export function fetchTrendingTags(location){
    var feedId;
    if(location!==undefined){
        if(location[4]!==undefined){
            feedId=location[4];
        }
        else if(location[2]=="default"){
            feedId="rpPDp"
        }
    }
    if(feedId!==undefined){
        return (dispatch) => {
            var me = this;
            dispatch(fetchTrendingTagsInit());
    
            fetch(Globals.API_ROOT_URL + `/tags/toptrending?feed_identity=${feedId}`,{
                headers: utils.setApiHeaders('get'),
            })
                .then(response => response.json())
                .then((json) => {
                    if (typeof (json.data) !== 'undefined') {
                        dispatch(fetchTrendingTagsSuccess(json.data));
                    } else {
                        utils.handleSessionError(json)
                        dispatch(fetchTrendingTagsError())
                    }
                }).catch(err => {
                throw err;
            });
        }
    }
    else{
        return (dispatch) => {
            var me = this;
            dispatch(fetchTrendingTagsInit());
    
            fetch(Globals.API_ROOT_URL + `/tags/toptrending`,{
                headers: utils.setApiHeaders('get'),
            })
                .then(response => response.json())
                .then((json) => {
                    if (typeof (json.data) !== 'undefined') {
                        dispatch(fetchTrendingTagsSuccess(json.data));
                    } else {
                        utils.handleSessionError(json)
                        dispatch(fetchTrendingTagsError())
                    }
                }).catch(err => {
                throw err;
            });
        }
    }
        
}

export function fetchTrendingTagsInit() {
    return {
        type: types.FETCH_TRENDING_TAGS_INIT
    };
}

export function fetchTrendingTagsSuccess(tags) {
    return {
        type: types.FETCH_TRENDING_TAGS_SUCCESS,
        trendingTags: tags
    };
}

export function fetchTrendingTagsError(tags) {
    return {
        type: types.FETCH_TRENDING_TAGS_FAIL
    };
}

export function fetchUserTagsAllSuccess(tags) {
    return {
        type: types.FETCH_ALL_USER_TAGS_SUCCESS,
        tags: tags
    };
}
