import * as types from '../actionTypes'
import Globals from '../../Globals'
import {
  browserHistory
} from 'react-router'
import notie from 'notie/dist/notie.js'
import * as utils from '../../utils/utils'
import * as message from '../../utils/Message'

// campaign create 
export function campaignsCreationInitiate() {
  return {
    type: types.CAMPAIGNS_CREATING
  }
}
export function creatingNewCampaign() {
  return {
    type: types.CAMPAIGNS_CREATING_NEW
  }
}

export function viewingCampaign() {
  return {
    type: types.VIEWING_CAMPAIGN
  }
}

export function campaignsCreationSuccess(createdCampaignInfo) {
  // browserHistory.push("/campaigns")
  return {
    type: types.CAMPAIGNS_CREATION_SUCCESS,
    createdCampaignInfo: createdCampaignInfo
  }
}

export function campaignsCreationError(statusText) {
  if (statusText !== 'undefined') {
    var KeyValue = Object.keys(statusText)[0];
    switch (KeyValue) {
      case 'to_date':
        statusText = statusText.to_date[0];
        break;
      case 'from_date':
        statusText = statusText.from_date[0];
        break;
      case 'title':
        statusText = statusText.title[0];
        break;
      case '0':
        statusText = statusText;
        break;
      default:
        statusText = "There is some error adding campaign!"
    }

  }
  message.Error(statusText,5)
  return {
    type: types.CAMPAIGNS_CREATION_ERROR,
    createdCampaignInfo: statusText
  }
}
export function fetchCampaign(campId) {
  return dispatch => {
    fetch(
        Globals.API_ROOT_URL +
        `/campaigns/${campId}`, {
          headers: utils.setApiHeaders('get')
        })
      .then(response => response.json())
      .then(json => {
        if (json.data !== undefined) {
          dispatch(fetchCampaignSuccess(json.data));
        }
      })
  }
}

export function fetchCampaignSuccess(data) {
  return {
    type: types.FETCH_CAMPAIGN_SUCCESS,
    campaign_data: data
  }
}

export function openCampaignFromNoti(){
  return{
    type: types.OPEN_CAMPAIGN_FROM_NOTI
  }
}

export function openContentProjectFromNotie(project_id){
  return{
    type:types.OPEN_CONTENT_PROJECT_FROM_NOTIE,
    notie_project_identity:project_id
  }
}

export function closeContentProjectFromNotie(){
  return{
    type: types.CLOSE_CONTENT_PROJECT_FROM_NOTIE
  }
}

export function closeCampaignFromNoti(){
  return{
    type: types.CLOSE_CAMPAIGN_FROM_NOTI
  }
}

export function  saveCampaignData(datatype,datavalue,campId){
return dispatch =>{
    dispatch(saveCampaignDataInit())
    if (datatype == 'title') {
      var jsonBody = {
        title: datavalue
      }
    } else if (datatype == 'campaignBrief') {
      var jsonBody = {
        description: datavalue
      }
    }
    fetch(
        Globals.API_ROOT_URL +
        `/campaigns/${campId}`, {
          method: 'PUT',
          headers: utils.setApiHeaders('PUT'),
          body: JSON.stringify(jsonBody)
        }
      )
      .then(response => {
        return response.json()
      })
      .then(json => {
          if (typeof json.error === 'undefined') {
            dispatch(saveCampaignDataSuccess(datatype, datavalue))
            dispatch(fetchCampaign(campId));
          } else {
            utils.handleSessionError(json)
            notie.error('error', 'There is some problem in updating camapign data.', 3)
            dispatch(saveCampaignDataError());
          }
        }

      )
  }
}
export function saveCampaignDataInit() {
  return {
    type: types.SAVE_CAMPAIGN_DATA_INIT
  }
}
export function saveCampaignDataSuccess(type, value) {
  return {
    type: types.SAVE_CAMPAIGN_DATA_SUCCESS,
    dataType: type,
    dataValue: value
  }
}
export function saveCampaignDataError() {
  return {
    type: types.SAVE_CAMPAIGN_DATA_ERROR
  }
}
// fetch tasks of particular campaign
export function fetchTasks(campId, taskType =null, roleName, userId = null, filterData = null, contentProjectId = null,from_planner=false,moment = null) {
  var jsonBody = {}
  if (filterData !== null) {
    if (filterData.status.length > 0) {
      var objToSend = {
        status: filterData.status
      }
      Object.assign(jsonBody, objToSend)
    }
    if (filterData.user.length > 0) {
      var objToSend = {
        user_id: filterData.user
      }
      Object.assign(jsonBody, objToSend)
    }
    if (filterData.creation_time.length > 0 && filterData.type !== 'all') {
      var objToSend = {
        timespan: filterData.creation_time
      }
      Object.assign(jsonBody, objToSend)
    }
    if (filterData.listFilter !== null && filterData.listFilter !== undefined) {
      var objToSend = {
        showTask: filterData.listFilter
      }
      Object.assign(jsonBody, objToSend)
    }
  }
  if (contentProjectId !== '' && contentProjectId !== null) {
    var objToAdd = {
      conent_project_id: contentProjectId
    }
    Object.assign(jsonBody, objToAdd);
  }
  if(taskType !== null){
    Object.assign(jsonBody, {
      task_filter: taskType,
    })
  }
  Object.assign(jsonBody, {
    campaign_id: campId,
    from_planner:from_planner
  })

  return dispatch => {
    var urlString = `/campaigns/listTask`

    dispatch(fetchTasksInit());
    fetch(
        Globals.API_ROOT_URL + urlString, {
          method: 'POST',
          headers: utils.setApiHeaders('post'),
          body: JSON.stringify(jsonBody)
        })
      .then(response => response.json())
      .then(json => {
        if (json !== undefined) {
          if (json.data instanceof Array && json.data.length > 0) {
            let taskArray = [];
            if(from_planner && moment!==null){
                taskArray = json.data.map((task, index) => {
                  return {
                    title: task.title,
                    start: moment.unix(task.due_date)._d,
                    end: moment.unix(task.due_date)._d,
                    feed_post: JSON.parse(JSON.stringify(task)),
                    type: "task"
                  }
      
                })
              }
            dispatch(fetchTasksSuccess(json.data,taskArray))
          } else {
            utils.handleSessionError(json)
            dispatch(fetchTasksError())
          }
        } else {
          dispatch(fetchTasksError())
        }
      })
      .catch(err => {
        throw err
      })
  }
}

export function fetchTasksInit() {
  return {
    type: types.FETCH_TASKS_INIT
  }
}
export function fetchTasksSuccess(data,taskArray) {
  return {
    type: types.FETCH_TASKS_SUCCESS,
    data: data,
    taskList: taskArray
  }
}

export function fetchTasksError() {
  return {
    type: types.FETCH_TASKS_ERROR,
  }
}

export function addTaskData(fields, campId=null, roleName, taskId = null, contentProjectId = null,callFrom=null,moment=null,from_global=null,event = false) {
  var method = '';
  var APIString = '';

  if (taskId !== null) {
    method = "PUT"
    APIString = `/campaigns/updateTask/${taskId}`

  } else {
    method = "POST"
    APIString = '/campaigns/addTask'
  }
  return dispatch => {
    var jsonBody = {
      "title": fields.title,
      "due_date": fields.DueDateTime,
      "notes": fields.notes,
    }
    if(campId !== null){
      jsonBody = {
        ...jsonBody,
        "campaign_id": campId
      }
    }
    if(fields.AssignedToIdentity !== undefined){
      jsonBody = {
        ...jsonBody,
        "assignee": fields.AssignedToIdentity
      }
    }
    if(fields.status !== undefined){
      jsonBody = {
        ...jsonBody,
        "status": fields.status
      }
    }
    if (fields.content_project_id) {
      Object.assign(jsonBody, {
        content_project_id: fields.content_project_id
      })
    }
    if (contentProjectId !== '' && contentProjectId !== null) {
      var objToAdd = {
        content_project_id: contentProjectId
      }
      Object.assign(jsonBody, objToAdd);
    }
    dispatch(addTaskDataInit());
    fetch(
        Globals.API_ROOT_URL + APIString, {
          method: method,
          headers: utils.setApiHeaders(method),
          body: JSON.stringify(jsonBody)
        }
      )
      .then(response => response.json())
      .then(json => {
        if (json !== undefined && json.code == 200) {
          dispatch(addTaskSuccess(true))
          notie.alert("success", json.message, 3);
          if(event == true){
            dispatch(fetchEventList(null,null,event,moment))
          }else if(from_global !== null && from_global == true){
            dispatch(fetchEventList(1,10))
          }else{
            dispatch(fetchTasks(campId, "openTask", roleName, null, null, contentProjectId,callFrom,moment))
          }
        } else {
          notie.error('error', 'There is some problem in adding task.', 3)
          dispatch(addTasksError())
        }
      })
      .catch(err => {
        throw err
      })


  }
}

export function addTaskDataInit() {
  return {
    type: types.ADD_TASK_DATA_INIT
  }
}

export function addTaskSuccess(flag= true) {
  return {
    type: types.ADD_TASK_DATA_SUCCESS,
    data:flag
  }
}

export function addTasksError() {
  return {
    type: types.ADD_TASK_ERROR
  }
}

export function deleteTask(id, campId, type, roleName, userId, contentProjectId = null,callFrom=null,moment =null,from_global=null,event= false) {
  var jsonBody = {
    task_identity: id
  }
  if(from_global == true){
    jsonBody={
      ...jsonBody,
      "from":"event"
    }
  }
  return dispatch => {
    dispatch(deleteTaskInit());
    fetch(`${Globals.API_ROOT_URL}/campaigns/deleteTask`, {
        method: 'DELETE',
        headers: utils.setApiHeaders('delete'),
        body: JSON.stringify(jsonBody)
      })
      .then(response => {
        return response.json()
      })
      .then(json => {
        if (json.code == 200) {
          notie.alert('success', json.message, 3);
          dispatch(deleteTaskSuccess());
          if(event == true){
            dispatch(fetchEventList(null,null,event,moment))
          }else if(from_global !== null && from_global == true){
            dispatch(fetchEventList(1,10))
          }else{
            dispatch(fetchTasks(campId, type, roleName, userId, null, contentProjectId,callFrom,moment,from_global));
          }
        } else {
          dispatch(deleteTaskError());
          notie.alert('error', json.error, 5);
        }
      })
      .catch(err => {
        throw err
      })
  }
}

export function deleteTaskInit() {
  return {
    type: types.DELETE_TASK_INIT
  }
}

export function deleteTaskSuccess() {
  return {
    type: types.DELETE_TASK_SUCCESS
  }
}

export function deleteTaskError() {
  return {
    type: types.DELETE_TASK_ERROR
  }
}
// segmentation 
export function fetchCampaignSegments(campId, tab) {
  var jsonBody = {
    campaign_id: campId,
  }
  if (tab !== "all") {
    Object.assign(jsonBody, {
      timespan: tab
    })
  }
  return dispatch => {

    dispatch(fetchCampaignSegmentsInit());
    fetch(`${Globals.API_ROOT_URL}/campaigns/campaignSegment`, {
        method: 'POST',
        headers: utils.setApiHeaders('post'),
        body: JSON.stringify(jsonBody)
      })
      .then(response => {
        return response.json()
      })
      .then(json => {
        if (json.code == 200) {
          if (json.data !== undefined) {
            dispatch(fetchCampaignSegmentsSuccess(json.data));
          } else {
            dispatch(fetchCampaignSegmentsSuccess([]));
          }
        } else {
          dispatch(fetchCampaignSegmentsError());
          notie.alert('error', json.message, 5);
        }
      })
      .catch(err => {
        throw err
      })
  }
}

export function fetchCampaignSegmentsSuccess(data) {
  return {
    type: types.FETCH_CAMPAIGN_SEGMENTS_SUCCESS,
    data: data
  }
}
export function fetchCampaignSegmentsInit() {
  return {
    type: types.FETCH_CAMPAIGN_SEGMENTS_INIT
  }
}

export function fetchCampaignSegmentsError() {
  return {
    type: types.FETCH_CAMPAIGN_SEGMENTS_ERROR
  }
}
/**
 * @author Kinjal
 * to fetch the quick user segments
 * @param {*} campId 
 * @param {*} userType - fetch the data  according to the usertype
 */
export function fetchCampaignSegmentsUser(campId,userType) {
  var jsonBody = {
    campaign_id: campId,
    user_type : userType
  }
 return dispatch => {

    dispatch(fetchCampaignSegmentsUserInit());
    fetch(`${Globals.API_ROOT_URL}/campaigns/quickUserList`, {
        method: 'POST',
        headers: utils.setApiHeaders('post'),
        body: JSON.stringify(jsonBody)
      })
      .then(response => {
        return response.json()
      })
     .then(json => {
        if (json.code == 200) {
          dispatch(fetchCampaignSegmentsUserSuccess(json.data));
          if (json.data !== undefined) {
            dispatch(fetchCampaignSegmentsUserSuccess(json.data));
          } else {
            dispatch(fetchCampaignSegmentsUserSuccess([]));
          }
        } else {
          dispatch(fetchCampaignSegmentsUserError());
          notie.alert('error', json.message, 5);
        }
     })
      .catch(err => {
        throw err
      })
  }
}
export function fetchCampaignSegmentsUserSuccess(data) {
  return {
    type: types.FETCH_CAMPAIGN_SEGMENTS_USER_SUCCESS,
    data: data
  }
}
export function fetchCampaignSegmentsUserInit() {
  return {
    type: types.FETCH_CAMPAIGN_SEGMENTS_USER_INIT
  }
}

export function fetchCampaignSegmentsUserError() {
  return {
    type: types.FETCH_CAMPAIGN_SEGMENTS_USER_ERROR
  }
}

export function fetchSegmentById(segmentId, campId) {
  var jsonBody = {
    campaign_id: campId,
    segment_id: segmentId
  }
  return dispatch => {

    dispatch(fetchSegmentByIdInit());
    fetch(`${Globals.API_ROOT_URL}/campaigns/campaignSegment`, {
        method: 'POST',
        headers: utils.setApiHeaders('post'),
        body: JSON.stringify(jsonBody)
      })
      .then(response => {
        return response.json();
      })
      .then(json => {
        if (json.code == 200) {
          dispatch(fetchSegmentByIdSuccess(json.data));
        } else {
          dispatch(fetchSegmentByIdError());
          notie.alert('error', json.message, 5);
        }
      })
      .catch(err => {
        throw err
      })
  }
}

export function fetchSegmentByIdInit() {
  return {
    type: types.FETCH_SEGMENTBYID_INIT
  }
}

export function fetchSegmentByIdSuccess(data) {
  return {
    type: types.FETCH_SEGMENTBYID_SUCCESS,
    data: data
  }

}
export function fetchSegmentByIdError() {
  return {
    type: types.FETCH_SEGMENTBYID_ERROR
  }
}

export function previewCollaborator(segment) {
  return dispatch => {
    dispatch(previewCollaboratorInit());
    fetch(`${Globals.API_ROOT_URL}/campaigns/previewCollaborator`, {
        method: 'POST',
        headers: utils.setApiHeaders('post'),
        body: JSON.stringify(segment)
      })
      .then(response => {
        return response.json()
      })
      .then(json => {
        if (json.code == 200) {
          if (json.data !== undefined) {
            dispatch((previewCollaboratorSuccess(json.data)));
          } else {
            dispatch(previewCollaboratorSuccess([]));
          }
        } else {
          dispatch(previewCollaboratorSuccess([]));
          notie.alert('error', json.error, 3);
        }
      })
      .catch(err => {
        throw err
      })
  }
}

export function previewCollaboratorInit() {
  return {
    type: types.PREVIEW_COLLABORATOR_INIT
  }
}

export function previewCollaboratorSuccess(data) {
  return {
    type: types.PREVIEW_COLLABORATOR_SUCCESS,
    data: data
  }
}

export function AddSegmentData(data, tab, type) {
  var apiString = '';
  if (type == 'quickSegment') {
    apiString = Globals.API_ROOT_URL + `/campaigns/addQuickSegment`
  } else {
    apiString = Globals.API_ROOT_URL + `/analytics/storeFilter`
  }
  return dispatch => {
    dispatch(addSegmentDataInit());
    fetch(apiString, {
        method: 'POST',
        headers: utils.setApiHeaders('post'),
        body: JSON.stringify(data)
      })
      .then(response => {
        return response.json()
      })
      .then(json => {
        if (json.code == 200) {
          notie.alert('success', json.message, 3);
          dispatch((AddSegmentDataSuccess()));
          dispatch((fetchCampaignSegmentsUser(data.action_id,"segment")));
          dispatch(fetchCampaignSegments(data.action_id, tab));
        } else {
          dispatch(AddSegmentDataError());
          notie.alert('error', json.message, 3);
        }
      })
      .catch(err => {
        throw err
      })
  }
}

export function addSegmentDataInit() {
  return {
    type: types.ADD_SEGMENTDATA_INIT
  }
}

export function AddSegmentDataSuccess() {
  return {
    type: types.ADD_SEGMENTDATA_SUCCESS
  }
}

export function AddSegmentDataError() {
  return {
    type: types.ADD_SEGMENTDATA_ERROR
  }
}

export function deleteSegment(segments, campId, tab) {
  var jsonBody = {
    campaign_id: campId,
    segment_id: segments
  }
  return (dispatch) => {
    dispatch(deleteSegmentInit());
    fetch(Globals.API_ROOT_URL + `/campaigns/deleteCampaignSegment`, {
      method: 'DELETE',
      headers: utils.setApiHeaders('delete'),
      body: JSON.stringify(jsonBody)
    }).then(response => {
      return response.json();
    }).then(json => {
      if (json.code == 200) {
        notie.alert('success', json.message, 3);
        dispatch(deleteSegmentSuccess());
        dispatch(fetchCampaignSegments(campId, tab));
      } else {
        dispatch(deleteSegmentError());
        utils.handleSessionError(json)
        notie.alert('error', json.message, 3);
      }


    }).catch(err => {
      throw err;
    });
  }
}

export function deleteSegmentInit() {
  return {
    type: types.DELETE_SEGMENT_INIT
  }
}

export function deleteSegmentSuccess() {
  return {
    type: types.DELETE_SEGMENT_SUCCESS
  }
}

export function deleteSegmentError() {
  return {
    type: types.DELETE_SEGMENT_ERROR
  }
}

// collaborator 
export function fetchAllCollaborators(campId, tab) {
  var jsonBody = {
    campaign_id: campId
  }
  if (tab !== "all") {
    Object.assign(jsonBody, {
      timespan: tab
    })
  }
  return dispatch => {

    dispatch(fetchCollaboratorInit());
    fetch(`${Globals.API_ROOT_URL}/campaigns/collaboratorSegment`, {
        method: 'POST',
        headers: utils.setApiHeaders('post'),
        body: JSON.stringify(jsonBody)
      })
      .then(response => {
        return response.json()
      })
      .then(json => {
        if (json.code == 200) {
          if (json.data !== undefined) {
            dispatch(fetchCollaboratorSuccess(json.data));
          } else {
            dispatch(fetchCollaboratorSuccess([]));
          }
        } else {
          dispatch(fetchCollaboratorError());
          notie.alert('error', json.message, 5);
        }
      })
      .catch(err => {
        throw err
      })
  }
}

export function fetchCollaboratorInit() {
  return {
    type: types.FETCH_COLLABORATOR_INIT
  }
}

export function fetchCollaboratorSuccess(data) {
  return {
    type: types.FETCH_COLLABORATOR_SUCCESS,
    data: data
  }
}

export function fetchCollaboratorError() {
  return {
    type: types.FETCH_COLLABORATOR_ERROR
  }
}
export function fetchCollaboratorByID(identity, campId) {
  var jsonBody = {
    campaign_id: campId,
    collaborator_id: identity
  }
  return dispatch => {

    dispatch(fetchCollaboratorByIdInit());
    fetch(`${Globals.API_ROOT_URL}/campaigns/collaboratorSegment`, {
        method: 'POST',
        headers: utils.setApiHeaders('post'),
        body: JSON.stringify(jsonBody)
      })
      .then(response => {
        return response.json()
      })
      .then(json => {
        if (json.code == 200) {
          dispatch(fetchCollaboratorByIdSuccess(json.data));
        } else {
          dispatch(fetchCollaboratorByError());
          notie.alert('error', json.message, 5);
        }
      })
      .catch(err => {
        throw err
      })
  }
}

export function fetchCollaboratorByIdInit() {
  return {
    type: types.FETCH_COLLABORATOR_BY_ID_INIT
  }
}
export function fetchCollaboratorByIdSuccess(data) {
  return {
    type: types.FETCH_COLLABORATOR_BY_ID_SUCCESS,
    data: data
  }
}

export function fetchCollaboratorByIdError() {
  return {
    type: types.FETCH_COLLABORATOR_BY_ID_ERROR
  }
}

export function deleteCollaborator(segments, campId, tab) {
  var jsonBody = {
    campaign_id: campId,
    collaborator_id: segments
  }
  return (dispatch) => {
    dispatch(deleteCollaboratorInit());
    fetch(Globals.API_ROOT_URL + `/campaigns/collaborator`, {
      method: 'DELETE',
      headers: utils.setApiHeaders('delete'),
      body: JSON.stringify(jsonBody)
    }).then(response => {
      return response.json();
    }).then(json => {
      if (json.code == 200) {
        notie.alert('success', json.message, 3);
        dispatch(deleteCollaboratorSuccess());
        dispatch(fetchAllCollaborators(campId, tab));
      } else {
        dispatch(deleteCollaboratorError());
        utils.handleSessionError(json)
        notie.alert('error', json.error, 3);
      }


    }).catch(err => {
      throw err;
    });
  }
}

export function deleteCollaboratorInit() {
  return {
    type: types.DELETE_COLLABORATOR_INIT
  }
}

export function deleteCollaboratorSuccess() {
  return {
    type: types.DELETE_COLLABORATOR_SUCCESS
  }
}

export function deleteCollaboratorError() {
  return {
    type: types.DELETE_COLLABORATOR_ERROR
  }
}

// collaborator add
export function addCollaborators(data, tab, type) {
  var apiString = '';
  apiString = Globals.API_ROOT_URL + `/campaigns/addCollaborator`
  return dispatch => {
    dispatch(addCollaboratorsInit());
    fetch(apiString, {
        method: 'POST',
        headers: utils.setApiHeaders('post'),
        body: JSON.stringify(data)
      })
      .then(response => {
        return response.json()
      })
      .then(json => {
        if (json.code == 200) {
          notie.alert('success', json.message, 3);
          dispatch((addCollaboratorsSuccess()));
          dispatch(fetchCampaign(data.action_id));
          dispatch(fetchCampaigAllUserList(data.action_id));
          dispatch(fetchAllCollaborators(data.action_id, tab));
          dispatch((fetchCampaignSegmentsUser(data.action_id,"collaborator")));
        } else {
          dispatch(addCollaboratorsError());
          notie.alert('error', json.message, 3);
        }
      })
      .catch(err => {
        throw err
      })
  }
}

export function addCollaboratorsInit() {
  return {
    type: types.ADD_COLLABORATORS_INIT
  }
}

export function addCollaboratorsSuccess() {
  return {
    type: types.ADD_COLLABORATORS_SUCCESS
  }
}

export function addCollaboratorsError() {
  return {
    type: types.ADD_COLLABORATORS_ERROR
  }
}

// fetch comments
export function fetchComments(campId, contentProjectId = null) {
  var jsonBody = {
    campaign_id: campId,
  }
  if (contentProjectId !== '' && contentProjectId !== null) {
    var objToAdd = {
      content_project_id: contentProjectId
    }
    Object.assign(jsonBody, objToAdd);
  }
  return dispatch => {
    dispatch(fetchCommentsInit());
    fetch(`${Globals.API_ROOT_URL}/campaigns/commentList`, {
        method: 'POST',
        headers: utils.setApiHeaders('post'),
        body: JSON.stringify(jsonBody)
      })
      .then(response => {
        return response.json()
      })
      .then(json => {
        if (json.code == 200) {
          dispatch(fetchCommentSuccess(json.data));
        } else {
          dispatch(fetchCommentSuccess([]));
          notie.alert('error', json.error, 3);
        }
      })
      .catch(err => {
        throw err
      })
  }
}

export function fetchCommentsInit() {
  return {
    type: types.FETCH_COMMENTS_INIT
  }
}

export function fetchCommentSuccess(data) {
  return {
    type: types.FETCH_COMMENTS_SUCCESS,
    data: data
  }
}
export function updateComment(values, campId, userTag, contentProjectId = null, editParent = null) {
  var jsonBody = {
    comment_id: values.CommentId,
    comment: values.updateComment,
    user_tag: userTag
  }
  if (contentProjectId !== '' && contentProjectId !== null) {
    var objToAdd = {
      project_id: contentProjectId
    }
    Object.assign(jsonBody, objToAdd);
  }
  return dispatch => {
    dispatch(addCommentInit());
    fetch(`${Globals.API_ROOT_URL}/campaigns/editComment`, {
        method: 'PUT',
        headers: utils.setApiHeaders('put'),
        body: JSON.stringify(jsonBody)
      })
      .then(response => {
        return response.json()
      })
      .then(json => {
        if (json.code == 200) {
          notie.alert('success', json.message, 3);
          dispatch(addCommentSuccess());
          dispatch(fetchComments(campId, contentProjectId));
          if (editParent !== null && editParent !== undefined) {
            dispatch(fetchCommentsReply(editParent, campId, contentProjectId));
          }
        } else {
          dispatch(addCommentError());
          dispatch(fetchCommentSuccess([]));
          notie.alert('error', json.error, 3);
        }
      })
      .catch(err => {
        throw err
      })
  }
}
export function submitComment(values, campId, userTag, contentProjectId = null) {
  var jsonBody = {
    campaign_id: campId,
    comment: values.submitComment,
    user_tag: userTag
  }
  if (contentProjectId !== '' && contentProjectId !== null) {
    var objToAdd = {
      content_project_id: contentProjectId
    }
    Object.assign(jsonBody, objToAdd);
  }
  return dispatch => {
    dispatch(addCommentInit())
    fetch(`${Globals.API_ROOT_URL}/campaigns/addComment`, {
        method: 'POST',
        headers: utils.setApiHeaders('post'),
        body: JSON.stringify(jsonBody)
      })
      .then(response => {
        return response.json()
      })
      .then(json => {
        if (json.code == 200) {
          notie.alert('success', json.message, 3);
          dispatch(addCommentSuccess());
          dispatch(fetchComments(campId, contentProjectId));
        } else {
          dispatch(addCommentError());
          dispatch(fetchCommentSuccess([]));
          if (json.message) {
            notie.alert('error', json.message, 3);
          }
        }
      })
      .catch(err => {
        throw err
      })
  }
}

export function addCommentInit() {
  return {
    type: types.ADD_COMMENT_INIT
  }
}
export function addCommentSuccess() {
  return {
    type: types.ADD_COMMENT_SUCCESS
  }
}
export function addCommentError() {
  return {
    type: types.ADD_COMMENT_ERROR
  }
}

export function deleteComment(commentId, campId, contentProjectId = null) {
  var jsonBody = {
    comment_id: commentId
  }
  if (contentProjectId !== '' && contentProjectId !== null) {
    var objToAdd = {
      project_id: contentProjectId
    }
    Object.assign(jsonBody, objToAdd);
  }
  return dispatch => {
    dispatch(deleteCommentInit())
    fetch(`${Globals.API_ROOT_URL}/campaigns/comment`, {
        method: 'DELETE',
        headers: utils.setApiHeaders('delete'),
        body: JSON.stringify(jsonBody)
      })
      .then(response => {
        return response.json()
      })
      .then(json => {
        if (json.code == 200) {
          notie.alert('success', json.message, 3);
          dispatch(deleteCommentSuccess());
          dispatch(fetchComments(campId, contentProjectId));
        } else {
          dispatch(deleteCommentError());
          notie.alert('error', json.error, 3);
        }
      })
      .catch(err => {
        throw err
      })
  }
}

export function deleteCommentInit() {
  return {
    type: types.DELETE_COMMENT_INIT
  }
}

export function deleteCommentSuccess() {
  return {
    type: types.DELETE_COMMENT_SUCCESS
  }
}

export function deleteCommentError() {
  return {
    type: types.DELETE_COMMENT_ERROR
  }
}

export function fetchCommentsReply(commentId, campId, contentProjectId = null) {
  var jsonBody = {
    comment_id: commentId,
    campaign_id: campId,
  }
  if (contentProjectId !== '' && contentProjectId !== null) {
    var objToAdd = {
      content_project_id: contentProjectId
    }
    Object.assign(jsonBody, objToAdd);
  }
  return dispatch => {
    dispatch(fetchReplyInit());
    fetch(`${Globals.API_ROOT_URL}/campaigns/commentList`, {
        method: 'POST',
        headers: utils.setApiHeaders('post'),
        body: JSON.stringify(jsonBody)
      })
      .then(response => {
        return response.json()
      })
      .then(json => {
        if (json.code == 200) {
          dispatch(fetchReplySuccess([json.data, commentId]));
        } else {
          dispatch(fetchReplySuccess([]));
          notie.alert('error', json.error, 3);
        }
      })
      .catch(err => {
        throw err
      })
  }
}

export function fetchReplyInit() {
  return {
    type: types.FETCH_REPLY_INIT
  }
}
export function fetchReplySuccess(data) {
  return {
    type: types.FETCH_REPLY_SUCCESS,
    data: data,
  }
}




export function submitCommentReply(values, campId, userTag, contentProjectId = null) {
  var jsonBody = {

    campaign_id: campId,
    parent_id: values.parentCommentID,
    comment: values.submitReply,
    user_tag: userTag
  }
  if (contentProjectId !== '' && contentProjectId !== null) {
    var objToAdd = {
      content_project_id: contentProjectId
    }
    Object.assign(jsonBody, objToAdd);
  }
  if (values.sub_parent_id !== undefined) {
    var objToAdd = {
      sub_parent_id: values.sub_parent_id
    }
    Object.assign(jsonBody, objToAdd);
  }
  return dispatch => {
    dispatch(addCommentInit())
    fetch(`${Globals.API_ROOT_URL}/campaigns/addComment`, {
        method: 'POST',
        headers: utils.setApiHeaders('post'),
        body: JSON.stringify(jsonBody)
      })
      .then(response => {
        return response.json()
      })
      .then(json => {
        if (json.code == 200) {
          notie.alert('success', json.message, 3);
          dispatch(addCommentSuccess());
          dispatch(fetchComments(campId, contentProjectId));
          dispatch(fetchCommentsReply(values.parentCommentID, campId, contentProjectId));
        } else {
          dispatch(addCommentError());
          dispatch(fetchCommentSuccess([]));
          notie.alert('error', json.error, 3);
        }
      })
      .catch(err => {
        throw err
      })
  }
}



export function campaignsCreate(values, identity = 0, location = null, moment = null, search = null) {
  var method = 'POST'
  var updateURL = ''
  var encodeInput = encodeURIComponent(search);
  if (identity != 0) {
    updateURL = `/${identity}`
    method = 'PUT'
  }
  //  if(values.userPermissions == '' || values.userPermissions == '@' || values.userPermissions === undefined){
  //       notie.alert('error','Please add collaborators for this campaign',5);
  //       return;
  //   }

  return dispatch => {
    var jsonBody = {
      title: values.title,
      targeted_audience: values.audience,
      campaign_objective: values.objective,
      campaign_type: values.campaignType,
      message: values.message,
      social_media: values.socialData,
      // description: values.description,
      tags: typeof values.campaignTags !== 'undefined' ?
        values.campaignTags :
        undefined,
      start_date: values.startdate,
      end_date: values.enddate,
      manager_identity: values.manager,
      // user_tag: values.userTag,
      // user_permissions:values.userPermissions
    }
    if(values.campaignHashtag){
      var ObjToAdd={
        hashtag:values.campaignHashtag
      }
      Object.assign(jsonBody,ObjToAdd)
    }

    dispatch(campaignsCreationInitiate())
    fetch(
        Globals.API_ROOT_URL +
        `/campaigns${updateURL}`, {
          method: method,
          headers: utils.setApiHeaders(method),
          body: JSON.stringify(jsonBody)
        }
      )
      .then(response => {
        return response.json()
      })
      .then(json => {
        if (typeof json.error === 'undefined') {
          dispatch(campaignsCreationSuccess(json))
          if (location != null) {
            let events = false
            if (location == 'calendar') events = true
            if (location == "search" && search !== null) {
              dispatch(fetchCampaigns(location, events, moment, true, search,1))
            } else {
              dispatch(fetchCampaigns(location, events, moment, true,null,1))
            }
            notie.alert('success', json.message, 3)
          }
        } else {
          utils.handleSessionError(json)
          dispatch(campaignsCreationError(json.error))
        }
      })
      .catch(err => {
        throw err
      })
  }
}

// campaign listing
export function fetchCampaignsStart() {
  return {
    type: types.FETCH_CAMPAIGNS_START
  }
}
export function ErrorFetchingCampaign() {
  notie.alert('error', 'There is problem in fetching data', 3)
  return {
    type: types.ERROR_FETCHING_CAMPAIGN
  }
}
export function fetchCampaignsSuccess(campaigns, createCampaign) {

  return {
    type: types.FETCH_CAMPAIGNS_SUCCESS,
    campaigns: campaigns,
    createCampaign: createCampaign
  }
}

export function fetchCampaignsSuccessNoCampaigns() {
  return {
    type: types.CAMPAIGNS_NOT_FOUND
  }
}
export function fetchCampaignsSuccessMeta (Meta={}) {
  return {
    type: types.CAMPAIGN_SUCCESS_META,
    data:Meta
  }
}

export function fetchCampaigns (filter, events = false, moment = null,createCampaign=false,search=null,pageNo=null,type=null) {
  var encodeInput = encodeURIComponent(search)
  if(search==null)
  {
    var queryToAppend='';
    if(pageNo !==null){
      queryToAppend= `page=${pageNo}&limit=10`
    }else{
      queryToAppend='limit=all';
    }
    if(type !== null){
      let typeString =''
      for (let index = 0; index < type.length; index++) {
        const element = type[index];
        typeString += `&campaign_type[]=${element}`
      }
      queryToAppend += typeString
    }
          return dispatch => {
            var me = this
            dispatch(fetchCampaignsStart())
            fetch(
              Globals.API_ROOT_URL +
                `/campaigns?campaign_status=${filter}&${queryToAppend}`,{
                  headers: utils.setApiHeaders('get')
                })
              .then(response => response.json())
              .then(json => {
                if(json !==undefined){
                if (json.data instanceof Array && json.data.length > 0) {
                  if (events == true) {
                    dispatch(fetchEvents(json.data, moment))}
                  else {
                    dispatch(fetchCampaignsSuccess(json.data,createCampaign))
                    if(json.meta !== undefined)
                      dispatch(fetchCampaignsSuccessMeta(json.meta))
                  }
                } else {
                  utils.handleSessionError(json)
                  if (events == true) dispatch(fetchEvents())
                  else dispatch(fetchCampaignsSuccessNoCampaigns())
                      dispatch(fetchCampaignsSuccessMeta())
                }
              }else{
                dispatch(ErrorFetchingCampaign())
              }
            })
        .catch(err => {
          throw err
        })
    }
  } else {
    return dispatch => {
      var me = this
      dispatch(fetchCampaignsStart())
      fetch(Globals.API_ROOT_URL + `/globalSearch?search=${encodeInput}&type=campaign`, {
          headers: utils.setApiHeaders('get')
        })
        .then(response => response.json())
        .then(json => {
          if(json !==undefined){
          if (json.data instanceof Array && json.data.length > 0) {
            if (events == true) {
              dispatch(fetchEvents(json.data, moment))}
            else {
              dispatch(fetchCampaignsSuccess(json.data,createCampaign))
              if(json.meta !== undefined)
                      dispatch(fetchCampaignsSuccessMeta(json.meta))
            }
          } else {
            utils.handleSessionError(json)
            if (events == true) dispatch(fetchEvents())
            else dispatch(fetchCampaignsSuccessNoCampaigns())
              dispatch(fetchCampaignsSuccessMeta())
          }
        }
        })
        .catch(err => {
          throw err
        })
    }
  }
}

export function fetchEvents(campaigns, moment) {
  var eventArray = []
  let campaignList = typeof campaigns !== undefined ? campaigns : [];
  if (campaignList) {
    campaignList.forEach(function (event, index) {
      eventArray.push({
        title: event.title,
        start: moment.unix(event.start_date).toDate(),
        end: moment.unix(event.end_date).toDate(),
        campaign: event,
        id:index
      })
    })
  }
  return {
    type: types.FETCH_CAMPAIGNS_SUCCESS,
    events: eventArray
  }

}

export function fetchCampaignPosts(campId, currview, moment) {
  return dispatch => {
    dispatch(fetchCampaignPostsInit())
    fetch(
        Globals.API_ROOT_URL +
        `/post/scheduled?campaign_identity=${campId}`, {
          headers: utils.setApiHeaders('get')
        })
      .then(response => response.json())
      .then(json => {
        if (json.data !== undefined) {
          var postsArray = [];
          var posts = json.data
          posts.length > 0 ?
            posts.forEach(function (post, index) {
              var dateString = moment.unix(post.scheduled_at)
              var CalendarFilter_object = {
                title: post.post.data.detail,
                start: dateString._d,
                end: dateString._d,
                feed_post: post
              }
              postsArray.push(CalendarFilter_object)
            }) : ''
          dispatch(fetchCampaignPostsSuccess(json.data, postsArray, currview))
        } else {
          if (json.code == 200) {
            if (currview !== 'calendar') {
              notie.alert('error', json.message, 3);
            }
            dispatch(fetchCampaignPostsSuccess([], [], currview))
          } else {
            notie.alert('error', 'Problem in fetching posts.', 3);
            dispatch(fetchCampaignPostsError())
            utils.handleSessionError(json)
          }

        }
      })
      .catch(err => {
        throw err
      })
  }
}

export function fetchCampaignPostsInit() {
  return {
    type: types.FETCH_CAMPAIGN_POST_INIT
  }
}

export function fetchCampaignPostsSuccess(posts, calendar, view) {
  return {
    type: types.FETCH_CAMPAIGN_POST_SUCCESS,
    postData: posts,
    calendarPosts: calendar,
    view: view
  }
}

export function fetchCampaignPostsError() {
  return {
    type: types.FETCH_CAMPAIGN_POST_ERROR
  }
}

export function campaignPostUpdateInit() {
  return {
    type: types.CAMPAIGN_POST_UPDATETING
  }
}

export function campaignpostUpdationInit() {
  return {
    type: types.CAMPAIGN_POST_UPDATE_INIT
  }
}

export function campaignPostUpdate(values, fromcalendar, moment, campId, tab) {
  var APIString = ''
  return dispatch => {
    dispatch(campaignpostUpdationInit())
    if (fromcalendar == true) {
      APIString = `${Globals.API_ROOT_URL}/post/publish/update-schedule-calendar`
    } else {
      APIString = `${Globals.API_ROOT_URL}/post/publish/update-schedule`
    }
    fetch(APIString, {
        method: 'PUT',
        headers: utils.setApiHeaders('put'),
        body: JSON.stringify(values)
      })
      .then(response => {
        return response.json()
      })
      .then(json => {
        if (json.code == 200) {
          if (fromcalendar == true) {
            dispatch(campaignPostUpdationSuccess(values))
            dispatch(fetchCampaignPosts(campId, tab, moment));
          } else {
            dispatch(fetchCampaignPosts(campId, tab, moment));
            dispatch(campaignPostUpdationSuccess(values))
          }
          var responseMessage = json.message;
          notie.alert(
            'success',
            responseMessage,
            3
          )
        }
        if (typeof json.error !== 'undefined') {
          utils.handleSessionError(json)
          dispatch(campaignPostUpdateError(json.error.detail[0]))
        }
      })
      .catch(err => {
        throw err
      })
  }
}

export function campaignPostUpdateError() {
  if (typeof statusText !== 'undefined') {
    notie.alert('error', statusText.toString(), 3)
  }
  return {
    type: types.CAMPAIGN_POST_UPDATE_ERROR
  }
}

export function campaignPostUpdationSuccess(values) {
  var message = "Your post was added successfully";
  return {
    type: types.CAMPAIGN_POST_UPDATION_SUCCESS,
    data: values
  }
}

export function getAllSchedulesInit() {
  return {
    type: types.GET_CAMPAIGN_SCHEDULES_INIT
  }
}
/**
 * get all the schedule for the post
 * @author Yamin
 * @param postId
 **/
export function getAllSchedules(postId) {
  return (dispatch) => {
    dispatch(getAllSchedulesInit());
    fetch(Globals.API_ROOT_URL + `/post/publish?post_identity=${postId}`, {
        headers: utils.setApiHeaders('get')
      })
      .then(response => {
        if (response.status != '200') {
          notie.alert(
            'error',
            'There is a problem in fetching schedule times.',
            5
          )
        }
        return response.json()
      })
      .then((json) => {
        if (Array.isArray(json.data) && json.data.length > 0) {
          dispatch(getAllSchedulesSuccess(json.data));
        } else {
          utils.handleSessionError(json)
          dispatch(getAllSchedulesError())
        }

      })
      .catch(err => {
        throw err;
      });
  }
}

export function getAllSchedulesSuccess(scheduleData) {
  return {
    type: types.GET_CAMPAIGN_SCHEDULES_SUCCESS,
    scheduleData: scheduleData
  }
}

export function getAllSchedulesError() {
  return {
    type: types.GET_CAMPAIGN_SCHEDULES_ERROR
  }
}

/**
 * fetch pending task
 * @author DISHA
 * @param campId
 **/
export function fetchTopMyPendingTask(campId) {
  return (dispatch) => {
    dispatch(fetchTopMyPendingTaskInit());
    fetch(Globals.API_ROOT_URL + `/campaigns/myPendingTask/${campId}`, {
        headers: utils.setApiHeaders('get')
      })
      .then(response => {
        if (response.status != '200') {
          notie.alert(
            'error',
            'There is a problem in fetching my pending task.',
            5
          )
        }
        return response.json()
      })
      .then((json) => {
        if (Array.isArray(json.data) && json.data.length > 0) {
          dispatch(fetchTopMyPendingTaskSuccess(json.data));
        } else {
          dispatch(fetchTopMyPendingTaskError())
          utils.handleSessionError(json)
        }
      })
      .catch(err => {
        throw err;
      });
  }
}
export function fetchTopMyPendingTaskInit() {
  return {
    type: types.FETCH_TOP_MYPENDING_TASK_INIT
  }
}
export function fetchTopMyPendingTaskSuccess(pendingData) {
  return {
    type: types.FETCH_TOP_MYPENDING_TASK_SUCCESS,
    pendingData: pendingData
  }
}

export function fetchTopMyPendingTaskError() {
  return {
    type: types.FETCH_TOP_MYPENDING_TASK_ERROR
  }
}
/**
 * get 4 pending content project list
 * @author DISHA
 * @param campId
 **/
export function fetchTopMyPendingContent(campId) {
  return (dispatch) => {
    dispatch(fetchTopMyPendingContentInit());
    fetch(Globals.API_ROOT_URL + `/campaigns/myPendingContent/${campId}`, {
        headers: utils.setApiHeaders('get')
      })
      .then(response => response.json())
      .then((json) => {
        if (json.code == 200) {
          if (Array.isArray(json.data) && json.data.length > 0) {
            dispatch(fetchTopMyPendingContentSuccess(json.data));
          } else {
            dispatch(fetchTopMyPendingContentSuccess([]));
          }
        } else {
          utils.handleSessionError(json)
          dispatch(fetchTopMyPendingContentError())
        }
      })
      .catch(err => {
        throw err;
      });
  }
}
export function fetchTopMyPendingContentInit() {
  return {
    type: types.FETCH_TOP_MYPENDING_CONTENT_INIT
  }
}
export function fetchTopMyPendingContentSuccess(data) {
  return {
    type: types.FETCH_TOP_MYPENDING_CONTENT_SUCCESS,
    data: data
  }
}

export function fetchTopMyPendingContentError() {
  notie.alert('error', 'There is a problem in fetching my pending Content.', 5)
  return {
    type: types.FETCH_TOP_MYPENDING_CONTENT_ERROR
  }
}

export function fetchPlannerPrints(campId, moment, taskType, filterData = null, fromCalendar = null, location) {
  var jsonBody = {}
  if (filterData !== null) {
    if (filterData.status.length > 0) {
      var objToSend = {
        status: filterData.status
      }
      Object.assign(jsonBody, objToSend)
    }
    if (filterData.user.length > 0) {
      var objToSend = {
        user_id: filterData.user
      }
      Object.assign(jsonBody, objToSend)
    }
    if (filterData.creation_time.length > 0) {
      var objToSend = {
        timespan: filterData.creation_time
      }
      Object.assign(jsonBody, objToSend)
    }
    if (filterData.default_status && filterData.default_status.length > 0) {
      var objToSend = {
        default_status: filterData.default_status
      }
      Object.assign(jsonBody, objToSend)
    }
    if (filterData.listFilter !== null && filterData.listFilter !== undefined) {
      var objToSend = {
        showTask: filterData.listFilter
      }
      Object.assign(jsonBody, objToSend)
    }
  }
  // Object.assign(jsonBody,{project_filter:taskType})
  if (location == "production") {
    Object.assign(jsonBody, {
      tab: "production",
      project_filter: taskType
    })
  }
  return dispatch => {
    dispatch(fetchPlannerDataInit());
    fetch(
        Globals.API_ROOT_URL +
        `/campaigns/contentPrint/${campId}`, {
          headers: utils.setApiHeaders('post'),
          body: JSON.stringify(jsonBody),
          method: 'POST'
        })
      .then(response => response.json())
      .then(json => {
        if (json.code == 200 && json.data) {
          let printArray = json.data.map((print, index) => {
            return {
              title: print.title,
              start: moment.unix(print.shipping_date)._d,
              end: moment.unix(print.shipping_date)._d,
              feed_post: JSON.parse(JSON.stringify(print)),
              type: "print"
            }

          })
          dispatch(fetchPlannerPrintsSuccess(json.data, printArray));
          if (fromCalendar == true) {
            dispatch(fetchPlannerPostSuccess([], []))
          }
        } else {
          dispatch(fetchPlannerPrintsError());
        }
      })
      .catch(err => {
        throw err
      })
  }
}
export function fetchPlannerEmails(campId, moment, taskType, filterData = null, fromCalendar = null, location) {
  var jsonBody = {}
  if (filterData !== null) {
    if (filterData.status.length > 0) {
      var objToSend = {
        status: filterData.status
      }
      Object.assign(jsonBody, objToSend)
    }
    if (filterData.user.length > 0) {
      var objToSend = {
        user_id: filterData.user
      }
      Object.assign(jsonBody, objToSend)
    }
    if (filterData.creation_time.length > 0) {
      var objToSend = {
        timespan: filterData.creation_time
      }
      Object.assign(jsonBody, objToSend)
    }
    if (filterData.default_status && filterData.default_status.length > 0) {
      var objToSend = {
        default_status: filterData.default_status
      }
      Object.assign(jsonBody, objToSend)
    }
    if (filterData.listFilter !== null && filterData.listFilter !== undefined) {
      var objToSend = {
        showTask: filterData.listFilter
      }
      Object.assign(jsonBody, objToSend)
    }
  }
  // Object.assign(jsonBody,{project_filter:taskType})
  if (location == "production") {
    Object.assign(jsonBody, {
      tab: 'production',
      project_filter: taskType
    })
  }
  return dispatch => {
    dispatch(fetchPlannerDataInit());
    fetch(
        Globals.API_ROOT_URL +
        `/campaigns/contentEmail/${campId}`, {
          headers: utils.setApiHeaders('post'),
          body: JSON.stringify(jsonBody),
          method: 'POST'
        })
      .then(response => response.json())
      .then(json => {
        if (json.code == 200 && json.data) {
          let emailArray = json.data.map((email, index) => {
            return {
              title: email.title,
              start: moment.unix(email.shipping_date)._d,
              end: moment.unix(email.shipping_date)._d,
              feed_post: JSON.parse(JSON.stringify(email)),
              type: "email"
            }

          })
          dispatch(fetchPlannerEmailsSuccess(json.data, emailArray));
          if (fromCalendar == true) {
            dispatch(fetchPlannerPostSuccess([], []))
          }
        } else {
          dispatch(fetchPlannerEmailsError());
        }
      })
      .catch(err => {
        throw err
      })
  }
}
export function fetchPlannerEmailsError() {
  return {
    type: types.FETCH_PLANNER_EMAILS_ERROR
  }
}
export function fetchPlannerEmailsSuccess(data, emailArray) {
  return {
    type: types.FETCH_PLANNER_EMAILS_SUCCESS,
    data: data,
    emailList: emailArray
  }
}

export function fetchPlannerDataInit() {
  return {
    type: types.FETCH_PLANNER_DATA_INIT
  }
}
export function fetchPlannerPrintsError() {
  return {
    type: types.FETCH_PLANNER_PRINTS_ERROR
  }
}
export function fetchPlannerPrintsSuccess(data, printArray) {
  return {
    type: types.FETCH_PLANNER_PRINTS_SUCCESS,
    data: data,
    printList: printArray
  }
}

export function fetchPlannerPosts(campId, moment, taskType, filterData = null, fromCalendar) {
  var jsonBody = {}
  if (filterData !== null) {
    if (filterData.status && filterData.status.length > 0) {
      var objToSend = {
        status: filterData.status
      }
      Object.assign(jsonBody, objToSend)
    }
    if (filterData.user && filterData.user.length > 0) {
      var objToSend = {
        user_id: filterData.user
      }
      Object.assign(jsonBody, objToSend)
    }
    if (filterData.creation_time && filterData.creation_time.length > 0) {
      var objToSend = {
        timespan: filterData.creation_time
      }
      Object.assign(jsonBody, objToSend)
    }
    if (filterData.listFilter !== null && filterData.listFilter !== undefined) {
      var objToSend = {
        showTask: filterData.listFilter
      }
      Object.assign(jsonBody, objToSend)
    }
  }
  // Object.assign(jsonBody,{task_filter:taskType})
  return dispatch => {
    dispatch(fetchPlannerDataInit());
    fetch(
        Globals.API_ROOT_URL +
        `/campaigns/contentPost/${campId}`, {
          headers: utils.setApiHeaders('post'),
          method: 'POST',
          body: JSON.stringify(jsonBody)
        })
      .then(response => response.json())
      .then(json => {
        if (json.data) {
          let postsArray = [];
          json.data.map((post, index) => {
            let dateString = moment.unix(post.scheduled_at)
            let CalendarFilter_object = {
              title: post.post.data.detail,
              start: dateString._d,
              end: dateString._d,
              feed_post: post,
              type: "post"
            }
            postsArray.push(CalendarFilter_object)
          })
          dispatch(fetchPlannerPostSuccess(json.data, postsArray));
          if (fromCalendar == true) {
            dispatch(fetchPlannerPrintsSuccess([], []))
          }
        } else {
          dispatch(fetchPlannerPostsError());
        }
      })
      .catch(err => {
        throw err
      })
  }
}

export function fetchPlannerPostsError() {
  return {
    type: types.FETCH_PLANNER_POST_ERROR
  }
}
export function fetchPlannerPostSuccess(data, postArray) {
  return {
    type: types.FETCH_PLANNER_POST_SUCCESS,
    data: data,
    postList: postArray
  }
}

export function fetchPlannerProjects(campId, moment, taskType, filterData = null, fromCalendar = null, location , rolename = null , profileId = null, callFromPlanner= false,) {


  return dispatch => {
    if (fromCalendar == true) {
      if (filterData && filterData.type == 'all') {
        dispatch(fetchPlannerPrints(campId, moment, taskType, filterData, false))
        dispatch(fetchPlannerPosts(campId, moment, taskType, filterData, false))
        dispatch(fetchTasks(campId,taskType,rolename ,profileId,filterData,null,callFromPlanner,moment))
      } else {
        dispatch(fetchPlannerPostSuccess([], []));
        dispatch(fetchPlannerPrints(campId, moment, taskType, filterData, fromCalendar))
        dispatch(fetchPlannerEmails(campId, moment, taskType, filterData, fromCalendar))

      }

    }
    // else if(filterData && filterData.type=="print"){
    //   dispatch(fetchPlannerPrints(campId,moment,taskType,filterData,null,location))
    // }else if(filterData && filterData.type=="email"){
    //   dispatch(fetchPlannerEmails(campId,moment,taskType,filterData,null,location))
    // }
    else {
      var jsonBody = {}
      if (filterData !== null) {
        if (filterData.status && filterData.status.length > 0) {
          var objToSend = {
            status: filterData.status
          }
          Object.assign(jsonBody, objToSend)
        }
        if (filterData.user && filterData.user.length > 0) {
          var objToSend = {
            user_id: filterData.user
          }
          Object.assign(jsonBody, objToSend)
        }
        if (filterData.creation_time && filterData.creation_time.length > 0) {
          var objToSend = {
            timespan: filterData.creation_time
          }
          Object.assign(jsonBody, objToSend)
        }
        if (filterData.default_status && filterData.default_status.length > 0) {
          var objToSend = {
            default_status: filterData.default_status
          }
          Object.assign(jsonBody, objToSend)
        }
        if (filterData.listFilter !== null && filterData.listFilter !== undefined) {
          var objToSend = {
            showTask: filterData.listFilter
          }
          Object.assign(jsonBody, objToSend)
        }


      }
      // Object.assign(jsonBody,{project_filter:taskType})
      if (location == "production") {
        Object.assign(jsonBody, {
          tab: "production",
          project_filter: taskType
        })
      } else if (location == "planner") {
        Object.assign(jsonBody, {
          tab: "planner"
        })
      }
      dispatch(fetchPlannerProjectsInit());
      fetch(
          Globals.API_ROOT_URL +
          `/campaigns/projectList/${campId}`, {
            headers: utils.setApiHeaders('post'),
            method: "POST",
            body: JSON.stringify(jsonBody)
          })
        .then(response => response.json())
        .then(json => {
          if (json.data !== undefined) {
            dispatch(fetchPlannerProjectsSuccess(json.data));
          } else {
            dispatch(fetchPlannerProjectsError());
          }
        })
        .catch(err => {
          throw err
        })
    }
  }
}

export function fetchPlannerProjectsInit() {
  return {
    type: types.FETCH_PLANNER_PROJECTS_INIT
  }
}

export function fetchPlannerProjectsSuccess(data) {
  return {
    type: types.FETCH_PLANNER_PROJECTS_SUCCESS,
    data: data
  }
}

export function fetchPlannerProjectsError() {
  return {
    type: types.FETCH_PLANNER_PROJECTS_ERROR,
  }
}

export function updateMetaData(meta) {
  return {
    type: types.UPDATE_META_DATA,
    data: meta
  }
}

/**
 * create content project
 * @author DISHA
 * @param data
 **/
export function createContentProject(data) {
  return (dispatch) => {
    dispatch(createContentProjectInit());
    fetch(Globals.API_ROOT_URL + `/campaigns/project`, {
        method: 'POST',
        headers: utils.setApiHeaders('post'),
        body: JSON.stringify(data)
      })
      .then(response => response.json())
      .then(json => {
        if (json !== undefined && json.code == 200) {
          notie.alert('success', json.message, 3)
          if (json.project_identity !== undefined) {
            dispatch(createContentProjectSuccess(json.project_identity));
            dispatch(fetchPlannerProjects(data.campaign_id));
            dispatch(getContentProjectById(data.type, json.project_identity, data.campaign_id))
          }
        } else {
          if (json.error !== undefined) {
            var error = json.error
            if (typeof error.title !== 'undefined') {
              notie.alert('error', error.title[0], 3)
            } else if(typeof error.asssignee !== 'undefined') {
              notie.alert('error', error.asssignee[0], 3)
            }else{
              notie.alert('error', error, 5)
            }
            utils.handleSessionError(json)
            dispatch(createContentProjectError())
          }
        }
      })
      .catch(err => {
        throw err;
      });
  }
}
export function handleContentCreationState() {
  return {
    type: types.HANDLE_CONTENT_CREATION_STATE
  }
}
export function createContentProjectInit() {
  return {
    type: types.CREATE_CONTENT_PROJECT_INIT
  }
}
export function createContentProjectSuccess(data) {

  return {
    type: types.CREATE_CONTENT_PROJECT_SUCCESS,
    data: data
  }
}

export function createContentProjectError() {
  return {
    type: types.CREATE_CONTENT_PROJECT_ERROR
  }
}
export function editContentProject(type,data,campId,contentProjectId,callForAssignee=false,isTemplateSelected=false,callAfterSaveTemplate=false,contentType = null){

  return dispatch =>{
    dispatch(editContentProjectInit());
    var objToSend = {
      campaign_id: campId,
      project_id: contentProjectId
    }

    if(type == "addUserPopup"){
      Object.assign(objToSend, data)
    }
    if (type == "editFromPlanner") {
      Object.assign(objToSend, data)
    }
   if (contentType) {
      Object.assign(objToSend, {
        "type": contentType
      })
    }
    if (type == "html") {
      Object.assign(objToSend, {
        html: data
      })
    }
    if (type == "html_preview") {
      Object.assign(objToSend, {
        html_preview: data
      })
    }
    if (type == "start_date") {
      Object.assign(objToSend, {
        start_date: data
      })
    }
    if (type == "end_date") {
      Object.assign(objToSend, {
        end_date:data
      })
    }
    if (type == "shipping_date") {
      Object.assign(objToSend, {
        shipping_date: data
      })
    }
    if (type == "title") {
      Object.assign(objToSend, {
        title: data
      })
    }
    if (type == "status") {
      Object.assign(objToSend, {
        status: data
      })
    }
    if (type == "notes") {
      Object.assign(objToSend, {
        notes: data
      })
    }
    if (type == "asssignee") {
      Object.assign(objToSend, {
        asssignee: [data]
      })
    }
    if (type == "remove_asssignee") {
      Object.assign(objToSend, {
        remove_asssignee: [data]
      })
    }
    if (type == 'signoff') {
      if(contentType == "email"){
          var obj = {
            status: 5,
            }
      }else{
        var obj = {
          pdf_url: data,
          status: 5,
        }
      }
   
      Object.assign(objToSend, obj);
    }
    fetch(Globals.API_ROOT_URL +
        `/campaigns/project`, {
          method: 'PUT',
          headers: utils.setApiHeaders('put'),
          body: JSON.stringify(objToSend)
        })
      .then(response => response.json())
      .then(json => {
        if (json.code == 200) {
          if (json.message && type !== "html_preview" && !(type == "status" && data == 5)) {
            notie.alert('success', json.message, 3);
          }
        if(callForAssignee==true || callAfterSaveTemplate== true){
          dispatch(getContentProjectById(type,contentProjectId,campId))
        }else{
          if(type!=='html' &&  type !=='html_preview'){
            dispatch(getContentProjectById(type,contentProjectId,campId))
          }
        }
        dispatch(editContentProjectSuccess(type))
       } else {
          if (json.code == 500) {
            if (json.error) {
              notie.alert('error', json.error, 3);
            } else if (json.message) {
              notie.alert('error', json.message, 3);
            }
            dispatch(editContentProjectError())
          }
        }
      })
      .catch(err => {
        throw err
      })
  }
}
export function editContentProjectInit() {
  return {
    type: types.EDIT_CONTENT_PROJECT_INIT
  }
}
export function editContentProjectSuccess(value) {
  return {
    type: types.EDIT_CONTENT_PROJECT_SUCCESS,
    value: value
  }
}

export function editContentProjectError() {
  return {
    type: types.EDIT_CONTENT_PROJECT_ERROR
  }
}

//fetch the data - disha
export function getContentProjectHtml(identity) {
  return dispatch => {
    fetch(Globals.API_ROOT_URL +
        `/campaigns/project?project_id=${identity}`, {
          headers: utils.setApiHeaders('get'),
        })
      .then(response => response.json())
      .then(json => {
        if (json.code == 200 && json.data !== undefined) {
          dispatch(getContentProjectHtmlSuccess(json.data));
        } else {
          dispatch(getContentProjectHtmlError());
        }
      })
      .catch(err => {
        throw err
      })
  }
}
export function getContentProjectHtmlSuccess(data) {
  return {
    type: types.GET_PROJECT_HTML_SUCCESS,
    data: data
  }
}
export function getContentProjectHtmlError() {
  return {
    type: types.GET_PROJECT_HTML_ERROR
  }
}
export function getContentProjectById(type, identity, campId, action) {
  return dispatch => {
    dispatch(getContentProjectByIdInit());
    fetch(Globals.API_ROOT_URL +
        `/campaigns/project?project_id=${identity}`, {
          headers: utils.setApiHeaders('get'),
        })
      .then(response => response.json())
      .then(json => {
        if (json.code == 200 && json.data !== undefined) {
          dispatch(getContentProjectByIdSuccess(json.data, action));

        } else {
          dispatch(getContentProjectByIdError());

        }
      })
      .catch(err => {
        throw err
      })
  }
}

export function getContentProjectByIdInit() {
  return {
    type: types.GET_CONTENT_PROJECT_BY_ID_INIT
  }
}

export function getContentProjectByIdSuccess(data, action) {
  return {
    type: types.GET_CONTENT_PROJECT_BY_ID_SUCCESS,
    data: data,
    event: action
  }
}

export function getContentProjectByIdError() {
  return {
    type: types.GET_CONTENT_PROJECT_BY_ID_ERROR
  }
}

export function deleteContentProject(type, identity, campId, moment, tab) {
  return dispatch => {
    dispatch(deleteContentProjectInit());
    fetch(Globals.API_ROOT_URL +
        `/campaigns/project?project_id=${identity}`, {
          headers: utils.setApiHeaders('delete'),
          method: 'DELETE',
        })
      .then(response => response.json())
      .then(json => {
        if (json.code == 200 && json.message) {
          notie.alert("success", json.message, 3);
          dispatch(deleteContentProjectSuccess())
          // if(tab=="production"){
          //   dispatch(fetchPlannerProjects(campId,moment,null,null,null,tab));
          // }else{
          //   dispatch(fetchPlannerPosts(campId,moment));
          //   dispatch(fetchPlannerPrints(campId,moment));
          // }
        } else {
          if (json.error) {
            notie.alert('error', json.error, 3);
            dispatch(deleteContentProjectError())
          }
        }
      })
      .catch(err => {
        throw err
      })
  }
}

export function deleteContentProjectInit() {
  return {
    type: types.DELETE_CONTENT_PROJECT_INIT
  }
}

export function deleteContentProjectSuccess() {
  return {
    type: types.DELETE_CONTENT_PROJECT_SUCCESS
  }
}
export function deleteContentProjectError() {
  return {
    type: types.DELETE_CONTENT_PROJECT_ERROR
  }
}
/**
 * get 4 pending content project list
 * @author DISHA
 * @param campId
 **/
export function fetchContentList(campId, taskType, roleName, userId = null, filterData = null) {
  return (dispatch) => {
    dispatch(fetchContentListInit());
    fetch(Globals.API_ROOT_URL + `/campaigns/myPendingContent/${campId}`, {
        headers: utils.setApiHeaders('get')
      })
      .then(response => response.json())
      .then((json) => {
        if (json.code == 200) {
          if (Array.isArray(json.data) && json.data.length > 0) {
            dispatch(fetchContentListSuccess(json.data));
          } else {
            dispatch(fetchContentListSuccess([]));
          }
        } else {
          utils.handleSessionError(json)
          dispatch(fetchContentListError())
        }
      })
      .catch(err => {
        throw err;
      });
  }
}
export function fetchContentListInit() {
  return {
    type: types.FETCH_CONTENT_LIST_INIT
  }
}
export function fetchContentListSuccess(data) {
  return {
    type: types.FETCH_CONTENT_LIST_SUCCESS,
    data: data
  }
}

export function fetchContentListError() {
  notie.alert('error', 'There is a problem in fetching Content List.', 5)
  return {
    type: types.FETCH_CONTENT_LIST_ERROR
  }
}

/**
 * fet review list for admin
 * @author DISHA
 * @param campId
 **/
export function fetchReviewContentList(campId) {
  return (dispatch) => {
    dispatch(fetchReviewContentListInit());
    fetch(Globals.API_ROOT_URL + `/campaigns/reviewContent/${campId}`, {
        headers: utils.setApiHeaders('get')
      })
      .then(response => response.json())
      .then((json) => {
        if (json.code == 200) {
          if (Array.isArray(json.data) && json.data.length > 0) {
            dispatch(fetchReviewContentListSuccess(json.data));
          } else {
            dispatch(fetchReviewContentListSuccess([]));
          }
        } else {
          utils.handleSessionError(json)
          dispatch(fetchReviewContentListError())
        }
      })
      .catch(err => {
        throw err;
      });
  }
}
export function fetchReviewContentListInit() {
  return {
    type: types.FETCH_REVIEW_CONTENT_LIST_INIT
  }
}
export function fetchReviewContentListSuccess(data) {
  return {
    type: types.FETCH_REVIEW_CONTENT_LIST_SUCCESS,
    data: data
  }
}

export function fetchReviewContentListError() {
  notie.alert('error', 'There is a problem in fetching review content List.', 5)
  return {
    type: types.FETCH_REVIEW_CONTENT_LIST_ERROR
  }
}
export function redirectToProductionForReviewContent(from) {
  return {
    type: types.REDIRECT_TO_PRODUCTION_FOR_REVIEW_CONTENT,
    data: from
  }
}
export function resetRedirectToProductionForReviewContent() {
  return {
    type: types.RESET_REDIRECT_TO_PRODUCTION_FOR_REVIEW_CONTENT
  }
}

export function redirectToProductionForPendingContent(from) {
  return {
    type: types.REDIRECT_TO_PRODUCTION_FOR_PENDING_CONTENT,
    data: from
  }
}
export function resetRedirectToProductionForPendingContent() {
  return {
    type: types.RESET_REDIRECT_TO_PRODUCTION_FOR_PENDING_CONTENT
  }
}

/**
 * fet review list for admin
 * @author DISHA
 * @param campId
 **/
export function fetchDefaultDraftTemplates(type) {
  return (dispatch) => {
    dispatch(fetchDefaultDraftTemplatesInit());
    fetch(Globals.API_ROOT_URL + `/campaigns/defaultDraft?type=${type}`, {
        headers: utils.setApiHeaders('get'),
      })
      .then(response => response.json())
      .then((json) => {
        if (Array.isArray(json.data) && json.data.length > 0) {
          dispatch(fetchDefaultDraftTemplatesSuccess(json.data));
          // dispatch(fetchDefaultDraftTemplatesSuccess(json.data));
        } else {
          utils.handleSessionError(json)
          dispatch(fetchDefaultDraftTemplatesError([]))
        }
      })
      .catch(err => {
        throw err;
      });
  }
}
export function fetchDefaultDraftTemplatesInit() {
  return {
    type: types.FETCH_DEFAULT_DRAFT_TEMPLAES_INIT
  }
}
export function fetchDefaultDraftTemplatesSuccess(data) {
  return {
    type: types.FETCH_DEFAULT_DRAFT_TEMPLAES_SUCCESS,
    data: data
  }
}

export function fetchDefaultDraftTemplatesError() {
  return {
    type: types.FETCH_DEFAULT_DRAFT_TEMPLAES_ERROR
  }
}

/**
 * fet review list for admin
 * @author DISHA
 * @param campId
 **/
export function fetchDraftFiles(type) {
  return (dispatch) => {
    // dispatch(fetchDraftFilesInit());
    fetch(Globals.API_ROOT_URL + `/campaigns/draft?type=${type}`, {
        headers: utils.setApiHeaders('post'),
      })
      .then(response => response.json())
      .then((json) => {
        if (Array.isArray(json.data) && json.data.length > 0) {
          dispatch(fetchDraftFilesSuccess(json.data));
        } else {
          utils.handleSessionError(json)
          dispatch(fetchDraftFilesError([]))
        }
      })
      .catch(err => {
        throw err;
      });
  }
}
export function fetchDraftFilesInit() {
  return {
    type: types.FETCH_DRAFT_FILES_INIT
  }
}
export function fetchDraftFilesSuccess(data) {
  return {
    type: types.FETCH_DRAFT_FILES_SUCCESS,
    data: data
  }
}

export function fetchDraftFilesError() {
  return {
    type: types.FETCH_DRAFT_FILES_ERROR
  }
}

export function addDraft(values, campId, project_id) {
  let objToSend = {
    content_html: values.html,
    title: values.title,
    html_preview: values.html_preview,
    type: values.type,
  }
  if(values.type == 'print'){
    Object.assign(objToSend, {
      "pdf_url": values.pdfUrl
    })
  }
  return dispatch => {
    dispatch(addDraftInit())
    fetch(Globals.API_ROOT_URL + `/campaigns/addDraft`, {
        headers: utils.setApiHeaders('post'),
        method: "POST",
        body: JSON.stringify(objToSend)
      })
      .then(response => response.json())
      .then((json) => {
        if (json.code == 200 && json.message) {
          notie.alert('success', json.message, 3);
          dispatch(addDraftSucesss());
        } else {
          notie.alert('success', 'Problem in adding template.', 3);
          dispatch(addDraftError());
        }
      })
      .catch(err => {
        throw err;
      });
  }
}
export function addDraftInit() {
  return {
    type: types.ADD_DRAFT_INIT
  }
}

export function addDraftSucesss() {
  return {
    type: types.ADD_DRAFT_SUCCESS
  }
}
export function addDraftError() {
  return {
    type: types.ADD_DRAFT_SUCCESS
  }
}
export function fetchCampaignAssets(campId) {
  return dispatch => {
    dispatch(fetchCampaignAssetInit());
    fetch(Globals.API_ROOT_URL + `/campaigns/assets/${campId}`, {
        headers: utils.setApiHeaders('get')
      })
      .then(response => response.json())
      .then((json) => {
        if (json.code == 200 && json.data) {
          json.data.map(asset => {
            asset.type = "image"
            asset.height == 0 ? asset.height = 350 : asset.height
            asset.width == 0 ? asset.width = 250 : asset.width
            asset.src = asset.media_url
          });
          dispatch(fetchCampaignAssetSuccess(json.data))
        } else {
          dispatch(fetchCampaignAssetSuccess([]))
        }
      })
      .catch(err => {
        throw err;
      });
  }
}

export function fetchCampaignAssetInit() {
  return {
    type: types.FETCH_CAMPAIGN_ASSET_INIT
  }
}

export function fetchCampaignAssetSuccess(data) {
  return {
    type: types.FETCH_CAMPAIGN_ASSET_SUCCESS,
    data: data
  }
}
export function fetchKanbanStatusList() {
  var data = [{
      name: 'Requested',
      stage: 0
    },
    {
      name: 'Draft',
      stage: 1
    },
    {
      name: 'In Process',
      stage: 2
    },
    {
      name: 'Done',
      stage: 3
    },
    {
      name: 'In Review',
      stage: 4
    },
    {
      name: 'Signed Off',
      stage: 5
    }
  ];
  return {
    type: types.FETCH_KANBAN_STATUS_LIST,
    data: data
  }
}
export function fetchCampaigAllUserList(campId) {
  return dispatch => {
    dispatch(fetchCampaigAllUserListInit());
    fetch(Globals.API_ROOT_URL + `/campaigns/userList/${campId}`, {
        headers: utils.setApiHeaders('get')
      })
      .then(response => response.json())
      .then((json) => {
        if (json.code == 200 && json.data) {
          var tagArray = [];
          if (json.data !== undefined && json.data.length > 0) {
            json.data.forEach(function (tag, index) {
              tagArray.push({
                'display': tag.first_name + ' ' + tag.last_name,
                'id': tag.id,
                'email': tag.email,
                'profile_image': tag.avatar,
                'type': 'User'
              })
            });
          }
          dispatch(fetchCampaigAllUserListSuccess(tagArray))
        } else {
          dispatch(fetchCampaigAllUserListError())
        }
      })
      .catch(err => {
        throw err;
      });
  }
}

export function fetchCampaigAllUserListInit() {
  return {
    type: types.FETCH_CAMPAIGN_ALL_USER_LIST_INIT
  }
}

export function fetchCampaigAllUserListSuccess(data) {
  return {
    type: types.FETCH_CAMPAIGN_ALL_USER_LIST_SUCCESS,
    data: data
  }
}
export function fetchCampaigAllUserListError() {
  return {
    type: types.FETCH_CAMPAIGN_ALL_USER_LIST_ERROR,
  }
}

//this method is used for the add user in edit tab
export function saveRecipientsData(RecipientsData) {
  return dispatch => {
    dispatch(saveRecipientsDataInit());
    fetch(`${Globals.API_ROOT_URL}/campaigns/recipients`, {
        method: 'POST',
        headers: utils.setApiHeaders('post'),
        body: JSON.stringify(RecipientsData)
      })
      .then(response => {
        return response.json()
      })
      .then(json => {
        if (json.code == 200) {
            notie.alert('success', json.message, 3);
            dispatch(saveRecipientsDataSuccess(true));
        } else {
          dispatch(saveRecipientsDataError(false));
          if(json.error.user_tag !== undefined){
            notie.alert('error', json.error.user_tag[0], 3);
          }else{
            notie.alert('error', json.error, 3);
          }
        }
      })
      .catch(err => {
        throw err
      })
  }
}
export function saveRecipientsDataInit(){
  return{
    type: types.SAVE_RECIPIENT_DATA_INIT,
  }
}
export function saveRecipientsDataSuccess(flag) {
  return {
    type: types.SAVE_RECIPIENT_DATA_SUCCESS,
    data:flag
  }
}
export function saveRecipientsDataError(flag) {
  return {
    type: types.SAVE_RECIPIENT_DATA_ERROR,
    data:flag
  }
}
export function fetchRecipientsData(projectId) {
  return dispatch => {
    dispatch(fetchRecipientsDataListInit());
    fetch(
      Globals.API_ROOT_URL +
        `/campaigns/recipients?project_id=${projectId}`, {
          headers: utils.setApiHeaders('get')
        })
      .then(response => response.json())
      .then(json => {
        if (json.data !== undefined) {
          dispatch(fetchRecipientsDataSuccess(json.data));
        }else{
          utils.handleSessionError(json);
          dispatch(fetchCampaignError());
        }
      })
  }
}

export function fetchRecipientsDataSuccess(data) {
  return {
    type: types.FETCH_RECIPIENT_DATA_LIST_SUCCESS,
    fetchdata : data
  }
}
export function fetchCampaignError() {
  return {
    type: types.FETCH_RECIPIENT_DATA_LIST_ERROR,
  }
}
export function fetchRecipientsDataListInit(){
  return {
    type: types.FETCH_RECIPIENT_DATA_LIST_INIT,
   
  }
}
//receipt delete

export function deleteRecipientUser(jsonBody){
  
  return dispatch => {
    dispatch(deleteRecipientUserInit());
    fetch(`${Globals.API_ROOT_URL}/campaigns/recipients`, {
        method: 'DELETE',
        headers: utils.setApiHeaders('delete'),
        body: JSON.stringify(jsonBody)
      })
      .then(response => {
        return response.json()
      })
      .then(json => {
        if (json.code == 200) {
          notie.alert('success', json.message, 3);
          dispatch(deleteRecipientUserSuccess());
          dispatch(fetchRecipientsData(jsonBody.project_id));
        } else {
          dispatch(deleteRecipientUserError());
          notie.alert('error', json.error, 5);
        }
      })
      .catch(err => {
        throw err
      })
  }
}

export function deleteRecipientUserInit() {
  return {
    type: types.DELETE_RECIPIENT_USER_INIT
  }
}

export function deleteRecipientUserSuccess() {
  return {
    type: types.DELETE_RECIPIENT_USER_SUCCESS
  }
}

export function deleteRecipientUserError() {
  return {
    type: types.DELETE_RECIPIENT_USER_ERROR
  }
}
/**
 * @author Kinjal
 * to delete the quick user segments
 * @param {*} campId 
 * @param {*} userId
 * @param {*} userType  - segments and collaborator
 */ 

export function deleteSegmentUser(campId,userId,userType) {
  var jsonBody ={
    campaign_id:campId,
    user_id :userId,
    user_type:userType
  }
  return (dispatch) => {
    dispatch(deleteSegmentUserInit());
    fetch(Globals.API_ROOT_URL + `/campaigns/deleteQuickUser`, {
      method: 'DELETE',
      headers: utils.setApiHeaders('delete'),
      body: JSON.stringify(jsonBody)
    }).then(response => {
      return response.json();
    }).then(json => {
      if (json.code == 200) {
        notie.alert('success', json.message, 3);
        dispatch(deleteSegmentUserSuccess());
        dispatch(fetchCampaigAllUserList(campId));
        dispatch(fetchCampaignSegmentsUser(campId,userType));
      } else {
        dispatch(deleteSegmentUserError());
        utils.handleSessionError(json)
        notie.alert('error', json.message, 3);
      }

   }).catch(err => {
      throw err;
    });
  }
}
export function deleteSegmentUserInit() {
  return {
    type: types.DELETE_SEGMENT_USER_INIT
  }
}

export function deleteSegmentUserSuccess() {
  return {
    type: types.DELETE_SEGMENT_USER_SUCCESS
  }
}

export function deleteSegmentUserError() {
  return {
    type: types.DELETE_SEGMENT_USER_ERROR
  }
}

/**
 * @author Kinjal
 * to fetch Campaign Recent Segments
 * @param {*} campId 
 */

//fetch recent segaments
export function fetchCampaignRecentSegments(campId) {
return dispatch => {
    dispatch(fetchCampaignRecentSegmentsInit());
    fetch(
      Globals.API_ROOT_URL +
        `/campaigns/recentSegment/${campId}`, {
          headers: utils.setApiHeaders('post')
        })
      .then(response => response.json())
     .then(json => {
        if (json.data !== undefined) {
          dispatch(fetchCampaignRecentSegmentsSuccess(json.data));
        }else{
           utils.handleSessionError(json);
          dispatch(fetchCampaignRecentSegmentsError());
        }
      })
  }
}
export function fetchCampaignRecentSegmentsInit() {
  return {
    type: types.FETCH_CAMPAIGN_RECENT_SEGMENTS_INIT
  }
}

export function fetchCampaignRecentSegmentsSuccess(recentSegment) {
  return {
    type: types.FETCH_CAMPAIGN_RECENT_SEGMENTS_SUCCESS,
    recentSegment : recentSegment
  }
}

export function fetchCampaignRecentSegmentsError() {
  return {
    type: types.FETCH_CAMPAIGN_RECENT_SEGMENTS_ERROR
  }
}
/**
 * @author disha 
 * set flag to handle whether to display task of content or not , on click of task count from kanban
 * @param {*} flag 
 */
export function displayTasksOfContent(flag) {
  return {
    type: types.DISPLAY_TASK_OF_CONTENT,
    data:flag
  }
}

export function fetchEventList(pageNo = 1,limit = 10,event = false, moment = null) {
  var apiUrl ='/campaigns/eventList'
  return dispatch => {
    dispatch(fetchEventListInit())
    if(event == false){
      apiUrl += `?page=${pageNo}&limit=${limit}`
    }else{
      apiUrl += `?limit=all`
    }
    fetch(
        Globals.API_ROOT_URL + apiUrl, {
          headers: utils.setApiHeaders('get')
        })
      .then(response => response.json())
      .then(json => {
        if (json.data !== undefined) {
          if(event == true){
            dispatch(fetchEventListForCalendar(json.data,moment));
          }else{
            dispatch(fetchEventListSuccess(json.data));
            dispatch(fetchEventListSuccessMeta(json.meta));
          }
        }else if(json.code == 200){
          if(event == true){
            dispatch(fetchEventListForCalendar([]));
          }else{
            dispatch(fetchEventListSuccess([]));
            dispatch(fetchEventListSuccessMeta({}));
          }
        }else{
          dispatch(fetchEventListError());
          utils.handleSessionError(json);
        }
      })
  }
}
export function fetchEventListForCalendar(data,moment){
  var eventArray = []
  let eventList = typeof data !== undefined ? data : [];
  if (eventList) {
    eventList.forEach(function (event, index) {
      eventArray.push({
        title: event.title,
        start: moment.unix(event.start_date).toDate(),
        end: moment.unix(event.end_date).toDate(),
        campaign: event,
        callFrom:'event',
        id:index
      })
    })
    return {
      type: types.FETCH_EVENT_LIST_SUCCESS,
      event: eventArray
    }
  }
}

export function fetchEventListSuccess(data) {
  return {
    type: types.FETCH_EVENT_LIST_SUCCESS,
    data: data
  }
}

export function fetchEventListInit(){
  return{
    type: types.FETCH_EVENT_LIST_INTI
  }
}
export function fetchEventListError(){
  return{
    type: types.FETCH_EVENT_LIST_ERROR
  }
}
export function fetchEventListSuccessMeta (Meta={}) {
  return {
    type: types.FETCH_EVENT_LIST_SUCCESS_META,
    data:Meta
  }
}