// import * as types from './actionTypes';
// import {browserHistory} from 'react-router';
// import Globals from '../Globals';
// import { getBase64 } from '../utils/utils';
// import notie from "notie/dist/notie.js"

// import * as s3functions from '../utils/s3'
// import * as s3actions from './s3/s3Actions';


// import { profilePost } from './settings/profile'
// import { companyInfoPost, companyInfoPut } from './settings/companyInfo'


// export function settingsPostInitiate() {
//   return {
//     type: types.SETTINGS_SAVING
//   };
// }

// export function profilePostInitiate() {
//   return {
//     type: types.PROFILE_SAVING
//   };
// }


// export function settingsPostStep1(fields, nextsteppath, isUpdate=false) {

//   return (dispatch) => {

//     dispatch(settingsPostInitiate());

//     if(typeof(fields["files"])!=="undefined") {

//       var me = this;

//       var s3Config = Globals.AWS_CONFIG;
//       var s3Manager = new s3functions.S3Manager(s3Config);

//       var filesToUpload = fields.files;

//       // we are uploading files to s3 now, set the flag in redux
//       dispatch(s3actions.uploadReset());
//       dispatch(s3actions.uploadingStart());

//       var fileToUpload = filesToUpload[0];


//       // upload starts here.
//       // it returns a promise whether succeeded or failed
//       // after you get the promise, proceed with db insertion
//       var s3MediaURL = s3Manager.s3uploadMedia(fileToUpload, 0, filesToUpload.length);

//       // On succession of uploading main asset on amazon, we will generate a thumbnail and
//       // send next request to amazon
//       s3MediaURL.then((obj) => {

//         var file = obj.data;
//         var originalFileObj = obj.fileObject;
//         var index = obj.index;

//         var generatedFileName = file.Location.split("/");
//           generatedFileName = generatedFileName[generatedFileName.length-1];
//           generatedFileName = generatedFileName.substring(0, generatedFileName.length-41);



//           let uploadedFile = {
//               fileUrl: file.Location,
//               thumbUrl: "",
//               uploadedFile: generatedFileName
//             }
//             dispatch(s3actions.uploadedItem(uploadedFile));

//             if(!isUpdate) {
//               companyInfoPost(fields, dispatch, nextsteppath, uploadedFile.fileUrl)
//             }
//             else {
//               companyInfoPut(fields, dispatch, uploadedFile.fileUrl)
//             }


//       })

//         .catch(err => {
//           notie.alert('error', 'There was a problem saving your data. Please try again!', 5);
//           throw err;
//         });
//     }

//     // user didn't upload image, simple post textual content
//     else {
//       if(!isUpdate) {
//         companyInfoPost(fields, dispatch)
//       }
//       else {
//         companyInfoPut(fields, dispatch)
//       }
//     }


//   }

// }

// export function settingsProfilePost(fields, nextsteppath) {
//   return (dispatch) => {

//     dispatch(profilePostInitiate());

//     if(typeof(fields["files"])!=="undefined") {

//       var me = this;

//       var s3Config = Globals.AWS_CONFIG;
//       var s3Manager = new s3functions.S3Manager(s3Config);

//       var filesToUpload = fields.files;

//       // we are uploading files to s3 now, set the flag in redux
//       dispatch(s3actions.uploadReset());
//       dispatch(s3actions.uploadingStart());

//       var fileToUpload = filesToUpload[0];


//       // upload starts here.
//       // it returns a promise whether succeeded or failed
//       // after you get the promise, proceed with db insertion
//       var s3MediaURL = s3Manager.s3uploadMedia(fileToUpload, 0, filesToUpload.length);

//       // On succession of uploading main asset on amazon, we will generate a thumbnail and
//       // send next request to amazon
//       s3MediaURL.then((obj) => {

//         var file = obj.data;
//         var originalFileObj = obj.fileObject;
//         var index = obj.index;

//         var generatedFileName = file.Location.split("/");
//           generatedFileName = generatedFileName[generatedFileName.length-1];
//           generatedFileName = generatedFileName.substring(0, generatedFileName.length-41);



//           let uploadedFile = {
//               fileUrl: file.Location,
//               thumbUrl: "",
//               uploadedFile: generatedFileName
//             }
//             dispatch(s3actions.uploadedItem(uploadedFile));

//             profilePost(uploadedFile.fileUrl)

//       })

//         .catch(err => {
//           notie.alert('error', 'There was a problem saving your data. Please try again!', 5);
//           throw err;
//         });
//     }

//     // user didn't upload image, simple post textual content
//     else {
//       profilePost(fields, dispatch, nextsteppath)
//     }
//   }
// }
// /*
// export function settingsPostStep3(fields, path) {

//   return (dispatch) => {

//     var arr = [];

//     Object.keys(fields).forEach(( key ) => {
//       var key = key;
//       var val = fields[key];

//       arr.push({
//         "key": key,
//         "values": val,
//         "order": "0",
//       });
//     });

//     var jsonBody = {
//         "data": arr
//     };

//     dispatch(settingsPostInitiate());

//     fetch(Globals.API_ROOT_URL + `/settings?api_secret=${Globals.API_SECRET}&access_token=${Globals.AUTH_TOKEN}`, {
//       method: 'POST',
//       headers: {
//         'Accept': 'application/json',
//         'Content-Type': 'application/json'
//       },
//       body: JSON.stringify(jsonBody)
//     })
//     .then(response => {
//       if(response.status=="200") {
//       }
//       else {
//         dispatch(settingsError(response.statusText));
//       }
//       return response.json();
//     })
//     .then(json=> {
//       dispatch(settingsSuccess(path));
//     })
//     .catch(err => { throw err; });
//   }
// }
// */
// export function settingsPostStep3(fields, path) {
//   return (dispatch) => {
//     dispatch(settingsSuccess(path));
//   }
// }


// export function settingsSuccessFlag() {
//   return {
//     type: types.SETTINGS_SUCCESS
//   };
// }

// export function settingsSuccess(path=null) {
//    return (dispatch) => {
//      if(path!=null){//it is on boarding
//        dispatch(settingsSuccessFlag());
//        browserHistory.push(path);
//      }
//      else//it is settings
//      {
//        dispatch(settingsSuccessFlag());
//        notie.alert('success', 'Settings updated!', 5);
//      }


//   }
// }


// export function profileSuccessFlag(profile) {
//   return {
//     type: types.PROFILE_SUCCESS,
//     profile: profile
//   };
// }


// export function companyInfoSuccessFlag(companyInfo) {
//   return {
//     type: types.SETTINGS_COMPANY_INFO_SUCCESS,
//     companyInfo
//   };
// }


// export function profileSuccess(path, profile) {
//    return (dispatch) => {
//     if(path!=''){//it is on boarding
//       dispatch(profileSuccessFlag(profile));
//       browserHistory.push(path);
//     }
//     else//it is settings
//     {
//       dispatch(profileSuccessFlag(profile.data));
//       notie.alert('success', 'Profile updated!', 5);
//     }

//   }
// }


// export function profileError(statusText) {
//   return {
//       type: types.SETTINGS_ERROR,
//       text: statusText,
//     };
// }

// export function settingsError(statusText) {
//   return {
//       type: types.SETTINGS_ERROR,
//       text: statusText,
//     };
// }
