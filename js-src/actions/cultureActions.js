import * as types from './actionTypes'
import Globals from '../Globals'
import * as utils from '../utils/utils'
import notie from 'notie/dist/notie.js'

export function resetPopupFlag(){
    return{
        type:types.RESET_POPUP_FLAG
    }
}
export function saveValueInit () {
  return {
    type: types.SAVE_VALUE_INIT
  }
}
export function saveValueSuccess (message) {
  notie.alert('success', message, 3)
  return {
    type: types.SAVE_VALUE_SUCCESS
  }
}
export function saveValueError (message) {
  if(message['data.0.title'] !== undefined){
      notie.alert('error', message['data.0.title'][0], 3)
  }else if(message['data.0.description'] !== undefined){
      notie.alert('error', message['data.0.description'][0], 3)
  }else{
    notie.alert('error' , 'Something went wrong!' , 3);
  }
  return {
    type: types.SAVE_VALUE_ERROR
  }
}

/**
 * to create new value
 * @param {*} fields array object
 * @author disha
 */
export function saveValues (fields) {
  return dispatch => {
    dispatch(saveValueInit())

    fetch(Globals.API_ROOT_URL + `/analytics/value`, {
      method: 'POST',
      headers: utils.setApiHeaders('post'),
      body: JSON.stringify({ data: fields })
    })
      .then(response => {
        return response.json()
      })
      .then(json => {
        if (json.code == 200) {
            dispatch(fetchValuesList())
            dispatch(saveValueSuccess(json.message))
        } else {
          dispatch(saveValueError(json.error))
          utils.handleSessionError(json)
        }
      })
      .catch(err => {
        throw err
      })
  }
}
export function fetchValuesListInit () {
  return {
    type: types.CULTURE_VALUE_LIST_INIT
  }
}
export function fetchValuesListSuccess (cultureValueList) {
  return {
    type: types.CULTURE_VALUE_LIST_SUCCESS,
    data: cultureValueList
  }
}
export function fetchValuesListError () {
  return {
    type: types.CULTURE_VALUE_LIST_ERROR
  }
}
/**
 * to fetch value list
 * @author disha
 */
export function fetchValuesList () {
  return dispatch => {
    dispatch(fetchValuesListInit())
    fetch(Globals.API_ROOT_URL + `/analytics/value`, {
      headers: utils.setApiHeaders('get')
    })
      .then(response => response.json())
      .then(json => {
        if (json.code == 200) {
          if (typeof json.data !== "undefined") {
            dispatch(fetchValuesListSuccess(json.data))
          }else{
            dispatch(fetchValuesListError())
          }
        } else {
          utils.handleSessionError(json)
          dispatch(fetchValuesListError())
        }
      })
      .catch(err => {
        throw err
      })
  }
}
export function updateValuesInit () {
  return {
    type: types.UPDATE_VALUE_INIT
  }
}
export function updateValuesSuccess (message) {
  notie.alert('success', message, 3)
  return {
    type: types.UPDATE_VALUE_SUCCESS
  }
}
export function updateValuesError (message) {
  notie.alert('error', message, 3)
  return {
    type: types.UPDATE_VALUE_ERROR
  }
}
/**
 * to update selected value detail
 * @param {*} fields object with data
 * @author disha
 */
export function updateValues (fields) {
  return dispatch => {
    dispatch(updateValuesInit())

    fetch(Globals.API_ROOT_URL + `/analytics/value`, {
      method: 'PUT',
      headers: utils.setApiHeaders('put'),
      body: JSON.stringify(fields)
    })
      .then(response => {
        return response.json()
      })
      .then(json => {
        if (json.code == 200) {
          dispatch(fetchValuesList())
          dispatch(updateValuesSuccess(json.message))
        } else {
          dispatch(updateValuesError(json.error))
          utils.handleSessionError(json)
        }
      })
      .catch(err => {
        throw err
      })
  }
}
export function deleteValueInit () {
  return {
    type: types.DELETE_VALUE_INIT
  }
}
export function deleteValueSuccess (message) {
  notie.alert('success', message, 3)
  return {
    type: types.DELETE_VALUE_SUCCESS
  }
}
export function deleteValueError (message) {
  notie.alert('error', message, 3)
  return {
    type: types.DELETE_VALUE_ERROR
  }
}
/**
 * to delete value form list
 * @param {*} valueId selected value id
 * @author disha
 */
export function deleteValue (valueId) {
  return dispatch => {
    dispatch(deleteValueInit())
    fetch(Globals.API_ROOT_URL + `/analytics/value/${valueId}`, {
      method: 'DELETE',
      headers: utils.setApiHeaders('get')
    })
      .then(response => {
        return response.json()
      })
      .then(json => {
        if (json.code == 200) {
          dispatch(fetchValuesList())
          dispatch(deleteValueSuccess(json.message))
        } else {
          dispatch(deleteValueError(json.error))
          utils.handleSessionError(json)
        }
      })
      .catch(err => {
        throw err
      })
  }
}
