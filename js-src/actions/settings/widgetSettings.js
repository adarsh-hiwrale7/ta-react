import Globals from '../../Globals';
import { settingsSuccess } from '../settingsActions'
import { settingsError } from '../settingsActions'
import notie from "notie/dist/notie.js"
import * as types from '../actionTypes';
import {GetAllSettings} from './getSettings';
import * as utils from '../../utils/utils';
/**
 * api to save setting widget data
 */
export function widgetInfoSave(fields) {
  return (dispatch) => {
  let arr = [];

  Object.keys(fields).forEach(( key ) => {
    let val = fields[key];
    let identity = fields[key+"_identity"];
    if(typeof(identity)!=="undefined" && identity!=="") {
        arr.push({
          "key": key,
          "values": val,
          "order": "1",
          "identity": identity
        });
      }
      else if(key.indexOf('_identity') == -1) {
          arr.push({
            "key": key,
            "values": val,
            "order": "1",
          });
      }

  });

  let jsonBody = {
      "data": arr
  };
  fetch(Globals.API_ROOT_URL + `/settings`, {
    method: 'PUT',
    headers: utils.setApiHeaders('put'),
    body: JSON.stringify(jsonBody)
  })
  .then(response => {
    if(response.status!="200") {
      dispatch(settingsError(response.statusText));
      notie.alert('error', 'Error while updating settings', 3);
    }
    return response.json();
  })
  .then(json=> {
    if(json.hasOwnProperty("data")) {
      dispatch(GetAllSettings());
      dispatch(settingsSuccess());
      notie.alert('success', 'Widget setting updated successfully', 3);
    }else{
      utils.handleSessionError(json)
    }

  })
  .catch(err => { throw err; });
}
}


export function getMapWidgetInfo() {
  return (dispatch) => {
    fetch(Globals.API_ROOT_URL + `/settings?param=googleKey`,{
      headers: utils.setApiHeaders('get')
    })
      .then(response => response.json())
      .then((json) => {
        if(Array.isArray(json.data) && json.data.length > 0) {
          dispatch(getMapWidgetInfoSuccess(json.data));
        }else{
          utils.handleSessionError(json)
        }
      })
      .catch(err => { throw err; });
  }
}

export function getMapWidgetInfoSuccess(mapWidgetInfo) {
  return {
    type: types.MAP_WIDGET_INFO,
    data: mapWidgetInfo
  };
}
