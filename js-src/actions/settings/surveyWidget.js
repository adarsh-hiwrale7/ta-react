import * as types from '../actionTypes';
import Globals from '../../Globals';
import * as utils from '../../utils/utils';
import notie from 'notie/dist/notie';
import { logout } from '../../actions/authActions';

export function createFunnel (data) {
  return dispatch => {
    dispatch(createFunnelInit());
    fetch(
      Globals.API_ROOT_URL +
        `/funnel`,{
          method: 'POST',
          headers: utils.setApiHeaders('post'),
          body: JSON.stringify(data)
        })
        .then(response => {
          return response.json();
        })
      .then(json => {
        if(json.code===200){
          notie.alert('success' , json.message , 3);
          dispatch(createFunnelSuccess());
          dispatch(fetchFunnelList());
        }else{
          if(json.error.feed_name!== undefined){
            notie.alert('error' , json.error.feed_name[0] , 3);
          }else{
            notie.alert('error' , 'Something went wrong!' , 3);
          }
          dispatch(createFunnelError());
        }

      })
  }
}

export function createFunnelInit (){
  return{
    type:types.CREATE_FUNNEL_INIT
  }
}

export function createFunnelSuccess (){
  return{
    type:types.CREATE_FUNNEL_SUCCESS
  }
}

export function createFunnelError (){
  return{
    type:types.CREATE_FUNNEL_ERROR
  }
}

export function editFunnel(data) {
  return dispatch => {
    dispatch(editFunnelInit());
    fetch(
      Globals.API_ROOT_URL +
        `/funnel`,{
          method: 'PUT',
          headers: utils.setApiHeaders('post'),
          body: JSON.stringify(data)
        })
        .then(response => {
          return response.json();
        })
      .then(json => {
        if(json.code===200){
          notie.alert('success' , 'Funnel updated successfully' , 3);
          dispatch(editFunnelSuccess());
          dispatch(fetchFunnelList());
        }else{
          notie.alert('error' , 'Something went wrong!' , 3);
          dispatch(editFunnelError());
        }

      })
  }
}

export function editFunnelInit (){
  return{
    type:types.EDIT_FUNNEL_INIT
  }
}

export function editFunnelSuccess (){
  return{
    type:types.EDIT_FUNNEL_SUCCESS
  }
}

export function editFunnelError (){
  return{
    type:types.EDIT_FUNNEL_ERROR
  }
}

export function fetchFunnelList () {
  return dispatch => {
    dispatch(fetchFunnelListInit())
    fetch(
      Globals.API_ROOT_URL +
        `/survey/funnel`,{
          headers: utils.setApiHeaders('get')
        })
      .then(response => {
          if(response.status == '401'){
            dispatch(logout());
          }
        return response.json()
      })
      .then(json => {
        if(json.code===200 && json.data){
            dispatch(fetchFunnelListSuccess(json.data))
        }else{
          dispatch(fetchFunnelListSuccess([]));
        }
      });
    }
  }

export function fetchFunnelListInit(){
  return{
    type: types.FETCH_FUNNEL_INIT
  }
}

export function fetchFunnelListSuccess(data){
  return{
    type: types.FETCH_FUNNEL_SUCCESS,
    data: data
  }
}

/*
Generate widget from the stage of funnel in NPS
@auther: Akshay soni
@param: data
@output: widgetSecret
*/
export function generateWidget (data) {
  return dispatch => {
    dispatch(WidgetGenerationinit())
    fetch(
      Globals.API_ROOT_URL +
        `/widget/survey`,{
          method: 'POST',
          headers: utils.setApiHeaders('post'),
          body: JSON.stringify(data)
        })
      .then(response => {
          if(response.status == '401'){
            dispatch(logout());
          }
        return response.json()
      })
      .then(json => {
        if(json.code===200 && json.data){
            dispatch(WidgetGenerationSuccess(json.data))
        }else{
          dispatch(initWidgetGenerationFail([]));
        }
      });
    }
  }

export function WidgetGenerationinit(){
  return{
    type: types.GENERATE_SURVEY_WIDGET_INIT
  }
}

export function WidgetGenerationSuccess(data){
  return{
    type: types.GENERATE_SURVEY_WIDGET_SUCCESS,
    data:data
  }
}

export function initWidgetGenerationFail(){
  return{
    type: types.GENERATE_SURVEY_WIDGET_FAIL,
  }
}

export function getFunnelData(id) {
  return dispatch => {
    dispatch(getFunnelInit());
    fetch(
      Globals.API_ROOT_URL +
        `/funnel?funnel_id=${id}`,{
          headers: utils.setApiHeaders('get')
        })
      .then(response => {
          if(response.status == '401'){
            dispatch(logout());
          }
          return response.json();
        })
      .then(json => {
        if(json.code === 200  && json.data){
          dispatch(getFunnelDataSuccess(json.data));
        }
      })
  }
}

export function getFunnelInit(){
  return{
    type: types.GET_FUNNEL_INIT,
  }
}

function addIdParam(data)
{
  data.stages.forEach(element=> {
      element.id = element.order
      // var object= {id:element.order}
      // element.Add(object)
   });
  
   return data;
}

export function getFunnelDataSuccess(data){
  return {
    type: types.GET_FUNNEL_DATA_SUCCESS,
    data: addIdParam(data),
  }
}


export function deleteFunnelAction(id){
  return dispatch => {
    dispatch(deleteFunnelInit());
    fetch(
      Globals.API_ROOT_URL +
        `/funnel?funnel_id=${id}`,{
          method: 'DELETE',
          headers: utils.setApiHeaders('delete')
        })
      .then(response => {
          if(response.status == '401'){
            dispatch(logout());
          }
          return response.json();
        })
      .then(json => {
        if(json.code === 200  && json.message){
          notie.alert('success',json.message,3);
          dispatch(deleteFunnelSuccess());
          dispatch(fetchFunnelList());
        }else{
          notie.alert('error',json.message,3);
          dispatch(deleteFunnelSuccess());
        }
      })
  }
}

export function deleteFunnelInit(){
  return{
    type: types.DELETE_FUNNEL_INIT
  }
}

export function deleteFunnelSuccess(){
  return{
    type: types.DELETE_FUNNEL_SUCCESS
  }
}
