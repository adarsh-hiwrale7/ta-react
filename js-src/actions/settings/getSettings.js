import Globals from '../../Globals'
import { profileSuccessFlag } from '../settingsActions'
import { companyInfoSuccessFlag } from '../settingsActions'
import { settingsError } from '../settingsActions'
import * as types from '../actionTypes'
import {logout} from '../authActions';
import * as utils from '../../utils/utils';
import { browserHistory } from 'react-router';

export function profileInit(){
  return{
    type:types.PROFILE_INIT
  }
}

export function GetProfileSettings (frompagrefreshed=null,isFromGuest=null) {
  return dispatch => {
    dispatch(profileInit())
    fetch(
      Globals.API_ROOT_URL +
        `/user/profile`,{
          headers: utils.setApiHeaders('get')
        })
      .then(response => {
        if (response.status != '200') {
          if(response.status == '401'){
            dispatch(logout());
          }
          dispatch(settingsError(response.statusText))
        }
        return response.json()
      })
      .then(json => {
        if (json.hasOwnProperty('data')) {
          if(json.data.onboard_step !==undefined ){
            //if onboarding steps are not completed then to redirect on that page
            if (json.data.role !== null && json.data.role !== undefined && json.data.role.data.length > 0) {
              var rolename= json.data.role.data[0].role_name
              var stepnumber=json.data.onboard_step
              const ssoUser = json.data.sso_onboarding
              var currAlbumName=localStorage.getItem('albumName')
              var userTourDetail=JSON.parse(localStorage.getItem(`${json.data.identity}_${currAlbumName}`))
              //frompagerefreshed flag indicate that this api is calling from main.js file on page refreshed 
              if (rolename == "super-admin"  && (frompagrefreshed ==true || (stepnumber == 0 && frompagrefreshed ==null ))) {
                if (stepnumber < 2) {
                  if (stepnumber == 0) {
                    browserHistory.push("/register");
                  } else if (stepnumber == 1) {    
                    browserHistory.push("/register/step2");
                  }
                }else{
                  //to check user's tour value is entered or not in localstorage 
                  if(userTourDetail!== null){
                    //to redirect on tour page is tour is not skipped or finished
                    if(userTourDetail.isTourSkipped == false){
                      browserHistory.push("/tour")
                    }
                  }else{
                    browserHistory.push("/tour")
                  }
                }
              } else if (rolename == "guest" && (frompagrefreshed ==true || (stepnumber == 0 && frompagrefreshed ==null ))) {
                if (stepnumber < 1) {
                  browserHistory.push("/guest-signup")
                }else{
                  if(userTourDetail!== null){
                    if(userTourDetail.isTourSkipped == false){
                      browserHistory.push("/tour")
                    }
                  }else{
                    browserHistory.push("/tour")
                  }
                }
              }else if(rolename == "employee" && (frompagrefreshed ==true || (ssoUser == false && frompagrefreshed ==null ))){
                if(ssoUser == false) {
                  browserHistory.push("/registerSsoUser")
                }else{
                  if(userTourDetail!== null){
                    if(userTourDetail.isTourSkipped == false){
                      browserHistory.push("/tour")
                    }else{
                      browserHistory.push("/feed/default/live");
                    }
                  }else{
                    browserHistory.push("/tour")
                  }
                }

              }
            }
          }
          dispatch(profileSuccessFlag(json.data))
        }else{
          utils.handleSessionError(json)
        }
      })
      .catch(err => {
        throw err
      })
  }
}


export function GetAllSettings () {
  return dispatch => {
    dispatch(getAllSettingInit())
    fetch(
      Globals.API_ROOT_URL +
        `/settings?param=all`,{
          headers: utils.setApiHeaders('get')
        })
      .then(response => {
        if (response.status != '200') {
          dispatch(settingsError(response.statusText))
        }
        return response.json()
      })
      .then(json => {
        if (json.hasOwnProperty('data')) {
          dispatch(companyInfoSuccessFlag(json.data))
        }else{
          dispatch(settingsError(json.message))
          utils.handleSessionError(json)
        }
      })
      .catch(err => {
        throw err
      })
  }
}
export function getAllSettingInit(){
  return{
    type:types.GET_ALL_SETTING_INIT
  }
}