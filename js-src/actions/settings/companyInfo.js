import Globals from '../../Globals';
import { settingsSuccess } from '../settingsActions'
import { settingsPostInitiate } from '../settingsActions'
import { settingsError } from '../settingsActions'
import notie from "notie/dist/notie.js"
import {GetAllSettings} from "./getSettings"
import * as utils from '../../utils/utils';
import * as types from '../actionTypes';
import configureStore from '../../store/store'
let store = configureStore()
export function companyInfoPost(fields,dispatch=null, path, imageResponse=null,OnBoardingDetail={}) {
  if(OnBoardingDetail !=={}){
    //to pass step number and flag which says that this api is calling for onboarding
    var addObj={
      is_onboarding:OnBoardingDetail.isOnBoarding==true? 1 : 0,
      step:OnBoardingDetail.stepNumber
    }
    OnBoardingDetail.isOnBoarding==true? Object.assign(fields,addObj):''
  }
  if(dispatch == null){
    return dispatch=>{
        dispatch(settingsPostInitiate());
        fetch(Globals.API_ROOT_URL + `/user/profile`, {
          method: 'PUT',
          headers: utils.setApiHeaders('put'),
          body: JSON.stringify(fields)
        })
      
        .then(response => {
          if(response.status!="200") {
            dispatch(settingsError(response.statusText));
          }
          return response.json();
        })
        .then(json=> {
          if(json.hasOwnProperty("data")) {
            notie.alert('success','Location updated!',3)
            dispatch(settingsSuccess(path));
          }else{
            utils.handleSessionError(json)
          }
      
        })
        .catch(err => { throw err; });
    }
  }else{
    dispatch(settingsPostInitiate());
    fetch(Globals.API_ROOT_URL + `/user/profile`, {
      method: 'PUT',
      headers: utils.setApiHeaders('put'),
      body: JSON.stringify(fields)
    })
  
    .then(response => {
      if(response.status!="200") {
        dispatch(settingsError(response.statusText));
      }
      return response.json();
    })
    .then(json=> {
      if(json.hasOwnProperty("data")) {
        dispatch(settingsSuccess(path));
      }else{
        utils.handleSessionError(json)
      }
  
    })
    .catch(err => { throw err; });
  }
}

export function companyInfoPut(fields,dispatch,imageResponse=null,path=null) {
  let arr = [];
  Object.keys(fields).forEach(( key ) => {

    let val = fields[key];
    let identity = fields[key+"_identity"];

    if(typeof(identity)!=="undefined") {
      if(key!=="files" && key!=="company_logo") {
        arr.push({
          "key": key,
          "values": val,
          "order": "1",
          "identity": identity
        });
      }
      else {
      	if(imageResponse!=null && key==="company_logo") {
      		arr.push({
  		        "key": "company_logo",
  		        "values": imageResponse,
  		        "order": "0",
              "identity": identity
  		    })
      	}
      }
    }
  });

  let jsonBody = {
      "data": arr
  };
  dispatch(settingsPostInitiate());
  fetch(Globals.API_ROOT_URL + `/settings`, {
    method: 'PUT',
    headers: utils.setApiHeaders('put'),
    body: JSON.stringify(jsonBody)
  })
  .then(response => {
    if(response.status!="200") {
      dispatch(settingsError(response.statusText));
      notie.alert('error', 'Error while updating settings', 3);
    }
    return response.json();
  })
  .then(json=> {
    if(json.hasOwnProperty("data")) {
      dispatch(settingsSuccess(path));
      dispatch(GetAllSettings());
      notie.alert('success', 'Settings updated!', 3);
    }else{
      utils.handleSessionError(json)
    }

  })
  .catch(err => { throw err; });
}
export function getCompanyLocationListInit(){
  return{
    type:types.GET_COMPANY_LOCAITON_INIT
  }
}
export function getCompanyLocationListSuccess(data){
  return{
    type:types.GET_COMPANY_LOCAITON_SUCCESS,
    data:data
  }
}
export function getCompanyLocationListError(){
  return{
    type:types.GET_COMPANY_LOCAITON_Error
  }
}
export function getCompanyLocationList () {
  return dispatch => {
    dispatch(getCompanyLocationListInit())
    fetch(Globals.API_ROOT_URL +`/location`,{
          headers: utils.setApiHeaders('get')
        })
      .then(response => {
        return response.json()
      })
      .then(json => {
        if(json.code==200){
          if (json.hasOwnProperty('data')) {
            dispatch(getCompanyLocationListSuccess(json.data))
          }else{
            dispatch(getCompanyLocationListSuccess([]))
          }  
        }else{
          dispatch(getCompanyLocationListError())
          utils.handleSessionError(json)
        }
      })
      .catch(err => {
        throw err
      })
  }
}
export function addCompanyLocationsInit(){
  return{
    type:types.ADD_COMPANY_LOCAITON_INIT
  }
}
export function addCompanyLocationsSuccess(data){
  return{
    type:types.ADD_COMPANY_LOCAITON_SUCCESS,
    data:data
  }
}
export function addCompanyLocationsError(message){
  notie.alert('error', message, 3);
  return{
    type:types.ADD_COMPANY_LOCAITON_ERROR
  }
}
export function addCompanyLocations(data) {
  var ObjtoSend={
    data:data
  }
  return dispatch => {
  dispatch(addCompanyLocationsInit());
  fetch(Globals.API_ROOT_URL + `/location`, {
    method: 'POST',
    headers: utils.setApiHeaders('post'),
    body: JSON.stringify(ObjtoSend)
  })
  .then(response => {
    return response.json();
  })
  .then(json=> {
    if(json.code==200){
        dispatch(getCompanyLocationList())
        dispatch(addCompanyLocationsSuccess());
      }else{
        dispatch(addCompanyLocationsError(error.message))  
        utils.handleSessionError(json)
    }

  })
  .catch(err => { throw err; });
}
}
export function updateCompanyLocationsInit(){
  return{
    type:types.UPDATE_COMPANY_LOCAITON_INIT
  }
}
export function updateCompanyLocationsSuccess(){
  return{
    type:types.UPDATE_COMPANY_LOCAITON_SUCCESS,
  }
}
export function updateCompanyLocationsError(message){
  notie.alert('error', message, 3);
  return{
    type:types.UPDATE_COMPANY_LOCAITON_ERROR
  }
}
export function updateCompanyLocations(data) {
  var arr={
    'office_location':data.newTitle
  }
  return dispatch => {
  dispatch(updateCompanyLocationsInit());
  fetch(Globals.API_ROOT_URL + `/location/${data.id}`, {
    method: 'PUT',
    headers: utils.setApiHeaders('put'),
    body: JSON.stringify(arr)
  })
  .then(response => {
    return response.json();
  })
  .then(json=> {
    if(json.code==200){
        dispatch(getCompanyLocationList());
        dispatch(updateCompanyLocationsSuccess());
      }else{
        dispatch(updateCompanyLocationsError(error.message))  
        utils.handleSessionError(json)
    }

  })
  .catch(err => { throw err; });
}
}

export function deleteCompanyLocationsInit(){
  return{
    type:types.UPDATE_COMPANY_LOCAITON_INIT
  }
}
export function deleteCompanyLocationsSuccess(data){
  return{
    type:types.UPDATE_COMPANY_LOCAITON_SUCCESS,
    data:data
  }
}
export function deleteCompanyLocationsError(message){
  notie.alert('error', message, 3);
  return{
    type:types.UPDATE_COMPANY_LOCAITON_ERROR
  }
}
export function deleteCompanyLocations(id) {
  return dispatch => {
  dispatch(deleteCompanyLocationsInit());
  fetch(Globals.API_ROOT_URL + `/location/${id}`, {
    method: 'DELETE',
    headers: utils.setApiHeaders('delete'),
  })
  .then(response => {
    return response.json();
  })
  .then(json=> {
    if(json.code==200){
        dispatch(getCompanyLocationList());
        dispatch(updateCompanyLocationsSuccess());
      }else{
        dispatch(deleteCompanyLocationsError(json.message))  
        utils.handleSessionError(json)
    }

  })
  .catch(err => { throw err; });
}
}

/**
* Save company information for company setting page
* @author Yamin
**/
export function saveCompanySettings(data) {
  let keyValPair = {}
  keyValPair[0] = {};
  keyValPair[0]['key'] = "company_no"
  keyValPair[0]['values'] = data.company_number
  keyValPair[0]['order'] = ""
  if(data.company_number_identity !== ''){
     keyValPair[0]['identity'] = data.company_number_identity
  }

  keyValPair[1] = {};
  keyValPair[1]['key'] = "company_country_id"
  keyValPair[1]['values'] = data.country
  keyValPair[1]['order'] = ""
  if(data.country_identity !== ''){
     keyValPair[1]['identity'] = data.country_identity
  }

  keyValPair[2] = {};
  keyValPair[2]['key'] = "company_head_office_add"
  keyValPair[2]['values'] = data.head_office_address
  keyValPair[2]['order'] = ""
  if(data.country_identity !== ''){
     keyValPair[2]['identity'] = data.head_office_address_identity
  }

  keyValPair[3] = {};
  keyValPair[3]['key'] = "company_head_office_add2"
  keyValPair[3]['values'] = data.head_office_address2
  keyValPair[3]['order'] = ""
  if(data.country_identity !== ''){
     keyValPair[3]['identity'] = data.head_office_address2_identity
  }

  keyValPair[4] = {};
  keyValPair[4]['key'] = "company_head_office_add3"
  keyValPair[4]['values'] = data.head_office_address3
  keyValPair[4]['order'] = ""
  if(data.country_identity !== ''){
     keyValPair[4]['identity'] = data.head_office_address3_identity
  }

  keyValPair[5] = {};
  keyValPair[5]['key'] = "company_legal_name"
  keyValPair[5]['values'] = data.legal_name
  keyValPair[5]['order'] = ""
  if(data.country_identity !== ''){
     keyValPair[5]['identity'] = data.legal_name_identity
  }

  keyValPair[6] = {};
  keyValPair[6]['key'] = "company_postcode"
  keyValPair[6]['values'] = data.postcode
  keyValPair[6]['order'] = ""
  if(data.country_identity !== ''){
     keyValPair[6]['identity'] = data.postcode_identity
  }

  keyValPair[7] = {};
  keyValPair[7]['key'] = "company_trading_name"
  keyValPair[7]['values'] = data.trading_name
  keyValPair[7]['order'] = ""
  if(data.country_identity !== ''){
     keyValPair[7]['identity'] = data.trading_name_identity
  }

 /* let keyValPair = [{
      key: "company_no",
      values: data.company_number,
      order:""
  },{
      key: "company_country_id",
      values: data.country,
      order:""
  },{
      key: "company_head_office_add",
      values: data.head_office_address,
      order:""
  },{
      key: "company_head_office_add2",
      values: data.head_office_address2,
      order:""
  },{
      key: "company_head_office_add3",
      values: data.head_office_address3,
      order:""
  },{
      key: "company_legal_name",
      values: data.legal_name,
      order:""
  },{
      key: "company_postcode",
      values: data.postcode,
      order:""
  },{
      key: "company_trading_name",
      values: data.trading_name,
      order:""
  }]*/
  /*console.log(keyValPair,'keyValPair');
  return*/

  var ObjtoSend={
    data:keyValPair
  }
 
  return dispatch => {
  dispatch(saveCompanySettingsInit());
  fetch(Globals.API_ROOT_URL + `/settings`, {
    method: data.operating_method,
    headers: utils.setApiHeaders('post'),
    body: JSON.stringify(ObjtoSend)
  })
  .then(response => {
    return response.json();
  })
  .then(json=> {
    if(json.code==200){
        dispatch(getCompanySettingInfo())
        dispatch(saveCompanySettingsSuccess());
        notie.alert('success', json.message, 3);
      }else{
        notie.alert('error', json.message, 3);
        dispatch(saveCompanySettingsError())  
        utils.handleSessionError(json)
    }

  })
  .catch(err => { throw err; });
}
}

export function saveCompanySettingsInit(){
  return{
    type:types.COMPANY_SETTING_SAVE_INIT
  }
}
export function saveCompanySettingsSuccess(){
  return{
    type:types.COMPANY_SETTING_SAVE_SUCCESS,
  }
}
export function saveCompanySettingsError(){
  return{
    type:types.COMPANY_SETTING_SAVE_ERROR
  }
}

/**
* Get company information for company setting page
* @author Yamin
**/
export function getCompanySettingInfo() {
 
  return dispatch => {
  dispatch(getCompanySettingInfoInit());
   fetch(Globals.API_ROOT_URL +`/settings?param=all`,{
          headers: utils.setApiHeaders('get')
    })
  .then(response => {
    return response.json();
  })
  .then(json=> {
    if(json.data.length > 0){
        dispatch(getCompanySettingInfoSuccess(json.data));
      }else{
        notie.alert('error', json.message, 3);
        dispatch(getCompanySettingInfoError())  
        utils.handleSessionError(json)
    }

  })
  .catch(err => { throw err; });
}
}

export function getCompanySettingInfoInit(){
  return{
    type:types.GET_COMPANY_SETTING_INFO_INIT
  }
}
export function getCompanySettingInfoSuccess(data){
  return{
    type:types.GET_COMPANY_SETTING_INFO_SUCCESS,
    data:data
  }
}
export function getCompanySettingInfoError(){
  return{
    type:types.GET_COMPANY_SETTING_INFO_ERROR
  }
}

export function updateCompanySettingFlagAction(){
  return{
    type:types.UPDATE_COMPANY_SETTING_FLAG
  }
}

/**
* Get master data accroding the data which is asked for in param
* @author Yamin
**/
export function showMasterData(dataFor,selectedData = null, isHierarchy=false,parentType = null,is_multiple = 1){
       let selectedParam = '';
       if(selectedData !== null){
          selectedParam = '&parent='+selectedData
       }
       if(parentType !== null && isHierarchy){
          selectedParam = selectedParam+'&type='+parentType
       }
       if(is_multiple == 0){
          selectedParam = selectedParam+'&is_multiple=0'
       }
      let apiUrl = '/hierarchy'
       if(!isHierarchy){
          apiUrl = '/master'
       }    

   return dispatch => {
      dispatch(showMasterDataInit(dataFor));
       fetch(Globals.API_ROOT_URL +`/company`+apiUrl+`?data=`+dataFor+selectedParam,{
              headers: utils.setApiHeaders('get')
        })
      .then(response => {
        return response.json();
      })
      .then(json=> {
        if(json.code==200 && typeof json.data !== 'undefined'){
            dispatch(showMasterDataSuccess(json.data, dataFor, selectedData));
          }else{
            dispatch(showMasterDataInitError(dataFor))  
            utils.handleSessionError(json)
        }
      })
      .catch(err => { throw err; });
    }
}

export function showMasterDataInit(dataFor){
  return{
    type:types.GET_MASTER_COMPANY_DATA_INIT,
    dataFor:dataFor
  }
}
export function showMasterDataSuccess(data, dataFor, parentId){
  return{
    type:types.GET_MASTER_COMPANY_DATA_SUCCESS,
    data:data,
    dataFor:dataFor,
    parentId: parentId
  }
}
export function resetMasterDataAddUser(role){
  return{
    type: types.RESET_MASTER_ADD_USER,
    data: role
  }
}
export function showMasterDataInitError(dataFor){
  return{
    type:types.GET_MASTER_COMPANY_DATA_ERROR,
    dataFor: dataFor
  }
}

export function addMasterData(data,isHierarchy=false){
    const dataArray = data.data_for;
    const parentId = data.parent_id
    delete data["data_for"]
    delete data["parent_id"]
    let manipulatedData = []
    Object.keys(data).map(function(key, index) {
      data[key]!==null && data[key]!== undefined?
      manipulatedData.push(data[key]):''
    });
    let apiUrl = 'hierarchy'
    if(!isHierarchy)
          apiUrl = 'master'
    var ObjtoSend={
      type:dataArray,
      data:manipulatedData,
      parent:parentId 
    }
  return dispatch => {
      /*dispatch(addMasterDataInit())
      setTimeout(function(){
        dispatch(addMasterDataSuccess())
        dispatch(showMasterData(dataArray, parentId,isHierarchy))
      },1000)*/
      dispatch(addMasterDataInit());
      fetch(Globals.API_ROOT_URL + `/company/`+apiUrl, {
        method: 'POST',
        headers: utils.setApiHeaders('post'),
        body: JSON.stringify(ObjtoSend)
      })
      .then(response => {
        return response.json();
      })
      .then(json=> {
        if(json.code==200){
            notie.alert('success', json.message, 3);
            dispatch(addMasterDataSuccess());
            dispatch(showMasterData(dataArray, parentId,isHierarchy))
            dispatch(getOfficeLocation())
          }else{
            if(json.message !== undefined ){
              notie.alert('error', json.message, 3); 
            }else{
              notie.alert('error','Something went wrong. Please try again later!',3)
            }
            dispatch(addMasterDataError())  
            utils.handleSessionError(json)
        }

      })
      .catch(err => { throw err; });
  }
}

export function addMasterDataInit(){
  return{
    type:types.ADD_MASTER_DATA_INIT
  }
}
export function addMasterDataSuccess(data){
  return{
    type:types.ADD_MASTER_DATA_SUCCESS,
    data:data
  }
}
export function addMasterDataError(){
  return{
    type:types.ADD_MASTER_DATA_ERROR
  }
}

export function fetchRegion(){
   return dispatch => {
 /*   dispatch(fetchRegionInit());
    let examArray = {}
    setTimeout(function(){
          examArray = {
              "code": 200,
              "data": [{
                  identity: 1,
                  name: "EMSA"
                },{
                  identity: 2,
                  name: "APEC"
                },{
                  identity: 3,
                  name: "ESSD"
                }]
              
          }
      dispatch(fetchRegionSuccess(examArray.data));

    }, 3000);*/
      dispatch(fetchRegionInit()); 
      fetch(Globals.API_ROOT_URL_MASTER +`/address/region`,{
              headers: utils.setApiHeaders('get')
        })
      .then(response => {
        return response.json();
      })
      .then(json=> {

        if(json.code==200){
            dispatch(fetchRegionSuccess(json.data));
          }else{
            notie.alert('error', json.message, 3);
           // dispatch(fetchRegionError())  
            dispatch(fetchRegionSuccess(json.data));
            utils.handleSessionError(json)
        }

      })
      .catch(err => { throw err; });
    }
}

export function fetchRegionInit(){
  return{
    type:types.FETCH_REGION_INIT
  }
}
export function fetchRegionSuccess(data){
  return{
    type:types.FETCH_REGION_SUCCESS,
    data:data
  }
}
export function fetchRegionError(){
  return{
    type:types.FETCH_REGION_ERROR
  }
}

export function getOfficeLocation(pageNo){
      
      return dispatch => {
            dispatch(getOfficeLocationInit())
            
            fetch(Globals.API_ROOT_URL +`/company/offices?page=${pageNo}`,{
                    headers: utils.setApiHeaders('get')
            })
            .then(response => {
              return response.json();
            })
            .then(json=> {
              if(json.code==200){
                  dispatch(getOfficeLocationSuccess(json));
                }else{
                  dispatch(getOfficeLocationError())  
                  utils.handleSessionError(json)
              }

            })
            .catch(err => { throw err; }) 
      }
}

export function getOfficeLocationInit(){
  return{
    type:types.GET_OFFICE_LOCATIONS_INIT
  }
}

export function getOfficeLocationSuccess(data){
  return{
    type:types.GET_OFFICE_LOCATIONS_SUCCESS,
    data: data
  }
}

export function getOfficeLocationError(){
  return{
    type:types.GET_OFFICE_LOCATIONS_ERROR
  }
}

export function deleteEntity(id,section,parentId,isHierarchy = false){
  
   var ObjtoSend={
      identity:id,
      type:section 
  }
  return dispatch => {
      dispatch(deleteEntityInit());
      fetch(Globals.API_ROOT_URL + `/company/removeEntity`, {
        method: 'DELETE',
        headers: utils.setApiHeaders('delete'),
        body: JSON.stringify(ObjtoSend)
      })
      .then(response => {
        return response.json();
      })
      .then(json=> {
        if(json.code==200){
            dispatch(deleteEntitySuccess(section));
            dispatch(showMasterData(section,parentId,isHierarchy))
            dispatch(getOfficeLocation());
             notie.alert('success', json.message, 3);
          }else{
            dispatch(deleteEntityError())  
            notie.alert('error', json.message, 3);
            utils.handleSessionError(json)
        }

      })
      .catch(err => { throw err; });
  }
}

export function deleteEntityInit(){
  return{
    type:types.DELETE_HIERARCHY_ENTITY_INIT
  }
}

export function deleteEntitySuccess(section){
  return{
    type:types.DELETE_HIERARCHY_ENTITY_SUCCESS,
    deleteFor: section
  }
}

export function deleteEntityError(){
  return{
    type:types.DELETE_HIERARCHY_ENTITY_ERROR
  }
}

export function deleteFlagUpdate(){
  return{
    type:types.DELETE_HIERARCHY_ENTITY_UPDATE_FLAG
  } 
}

export function updateHeirarchyProps(updateProps,section){
  return{
    type:types.UPDATE_HIERARCHY_PROPS,
    newProps: updateProps,
    section: section
  } 
}

export function updateHeirarchyPropsInit(){
  return{
    type:types.UPDATE_HIERARCHY_PROPS_INIT
  } 
}

