import * as types from '../actionTypes';
import Globals from '../../Globals'
import { settingsError } from '../settingsActions'
import notie from "notie/dist/notie.js"
import * as utils from '../../utils/utils';

/** Get Point **/
export function getPoint(points) {
    return {
        type: types.FETCH_POINT,
        points: points
    }
}

export function fetchPoint(type) {
  return (dispatch) => {
    fetch(Globals.API_ROOT_URL + `/point-table`,{
      headers: utils.setApiHeaders('get')
    })
      .then(response => response.json())
      .then((json) => {
        if(json.data) {
          dispatch(getPoint(json.data));
        }else{
          utils.handleSessionError(json)
        }
      })
      .catch(err => { throw err; });
  }
}

/** Update Point */
export function pointSave(fields) {
  return (dispatch) => {
    let arr = [];
    Object.keys(fields).forEach(( key ) => {

    let val = fields[key] ==''? 0 :fields[key];
    let identity = fields[key+"_identity"];
    if(typeof(identity)!=="undefined") {
        arr.push({
          "point": val,
          "action_id": identity
        });
      }
      else if(key.indexOf('_identity') == -1) {
          arr.push({
            "point": val,
            "action_id": ''
          });
      }

  });

  let jsonBody = {
      "data": arr
  };
  fetch(Globals.API_ROOT_URL + `/point-table`, {
    method: 'PUT',
    headers: utils.setApiHeaders('put'),
    body: JSON.stringify(jsonBody)
  })
  .then(response => {
    if(response.status!="200") {
      dispatch(settingsError(response.statusText));
      notie.alert('error', 'Error while updating settings', 3);
    }
    return response.json();
  })
  .then(json=> {
    if(json.code==200){
    notie.alert('success', json.message, 3);
    dispatch(fetchPoint());
    }else{
      utils.handleSessionError(json)
    }
  })
  .catch(err => { throw err; });
}
}

export function resetLeaderboardInit(){
  return{
    type:types.RESET_LEADERBOARD_INIT
  }
}
export function resetLeaderboardSuccess(){
  return{
    type:types.RESET_LEADERBOARD_SUCCESS
  }
}
export function resetLeaderboardError(){
  return{
    type:types.RESET_LEADERBOARD_ERROR
  }
}
/**
 * to reset points of leaderboard
 * @author disha 
 * @param time week, month, year
 */
export function resetLeaderboard(time){
  return (dispatch) => {
      dispatch(resetLeaderboardInit());
      fetch(Globals.API_ROOT_URL + `/leaderboard/reset?timespan=${time}`, {
        method: 'DELETE',
        headers: utils.setApiHeaders('delete'),
      }).then(response => {
        return response.json();
      }).then(json => {
        if (json.code == 200) {
          notie.alert('success', json.message, 3);
          dispatch(resetLeaderboardSuccess());
        }else{
          utils.handleSessionError(json)
          dispatch(resetLeaderboardError());
          notie.alert('error', json.message, 3);
        }

      }).catch(err => {
        throw err;
      });
    }
}