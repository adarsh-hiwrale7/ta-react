import Globals from '../../Globals';
import { profileSuccess } from '../settingsActions'
import { profileError } from '../settingsActions'
import * as utils from '../../utils/utils';
import {callLogoutApi} from '../../actions/loginActions';
import notie from "notie/dist/notie.js"

export function profilePost(fields, dispatch=null, path, imageResponse=null,callFrom=null) {
  var objToSend = {
    "firstname": fields.first_name,
    "lastname": fields.last_name,
    "business_unit":fields.business_unit,
    "company_role":fields.company_role,
    "country":fields.country,
    "department_identity":fields.department,
    "office":fields.office,
    "region":fields.region,
    "tags":fields.user_tags,
    'birth_date':fields.birth_date,
    'mobile_country_code':fields.mobile_country_code,
    'mobile_no':fields.mobile_no,
    'gender':fields.gender
  }
  if(imageResponse!=null) {
    objToSend["avatar"] = imageResponse;
  }
	fetch(Globals.API_ROOT_URL + `/user/profile`, {
      method: 'PUT',
      headers: utils.setApiHeaders('put'),
      body: JSON.stringify(objToSend)
      
    })
    .then(response => {
      if(response.status!="200") {
       dispatch(profileError(response.statusText));
      }
      return response.json();
    })
    .then(json=> {
      if(json.hasOwnProperty("data")) {
        if(fields.oldDepartmentName !== undefined && fields.oldDepartmentName !== fields.user_department ){
          //to logout if department is changed so that changes can reflect on leaderboard as they are taking department from session id
          dispatch(callLogoutApi())
        }else{
          dispatch(profileSuccess(path, json,callFrom));
        }
      }else{
        if(callFrom !== null && callFrom == 'content'){
          notie.alert('error' , "Problem in updating curated topics" , 3)
        }
        utils.handleSessionError(json)
        
      }

    })
    .catch(err => {
      throw err; 
    });
}
