import * as types from './actionTypes';
import Globals from '../Globals';
import notie from "notie/dist/notie.js"
import * as utils from '../utils/utils';
import {fetchSlackChannelList} from './integrationActions'
export function fetchChannelsStart() {
    return {
        type: types.FETCH_ACCOUNTS_START,
    };
}
export function socialAccountsInit(){
    return{

        type:types.SOCIAL_ACCOUNTS_INIT
    }
}
/**
 * @author disha
 * @use set flag to close popup when fb is disconnect
 */
export function disconnectFbSuccess(){
    return{
        type: types.DISCONNECT_FACEBOOK_SUCCESS
    }
}
export function sendPagesuccess(message){
    notie.alert('success', message, 3);
    return {
        type: types.SEND_PAGE_SUCCESS,
    };
}
export function sendPageError(data){
    notie.alert('error', json.message, 3);
    return{
        type: types.SEND_PAGE_ERROR
    }
}
export function sendpageInit(){
    return {
        type: types.SEND_PAGE_INIT,
    };
}
export function socialpageInit(){
    return {
        type: types.SOCIAL_PAGE_INIT,
    };
}
export function sendSelectedPage(data) {
    var arr={
        'social_account':data.type,
        "is_company" : data.iscompany,
        "api_token":data.accesstoken,
        "page_id":data.pageid,
        "name":data.pagename,
        "picture":data.picture
    }

    return (dispatch) => {
        dispatch(sendpageInit())
     fetch(Globals.API_ROOT_URL + `/pageSave`, {
            method: 'POST',
            headers: utils.setApiHeaders('post'),
            body: JSON.stringify(arr)
        })
     .then(response => response.json())
     .then((json) => {
        if (json.code == '200') {
             dispatch(sendPagesuccess(json.message));
        }else{
            utils.handleSessionError(json)
            dispatch(sendPageError(json.message))
        }
    }).catch(err => {
        throw err;
      });
    }
}
export function fetchSocialPagesSuccess(data){
    return{
        type: types.SOCIAL_PAGE_SUCCESS,
        data:data
    }
}
export function socialPagesInit(){
    return{
        type: types.SOCIAL_PAGES_INIT,
    }
}
export function fetchSocialNoPages(){
    return{
        type:types.SOCIAL_NO_PAGES
    }
}
/**
 * @use to fetch list of social page
 * @param {*} pagetype linkedin /facebook/twitter
 * @author disha 
 */
export function fetchSocialPages(pagetype){
    return (dispatch) => {
        dispatch(socialPagesInit())
        fetch(Globals.API_ROOT_URL + `/social/page?social_media=${pagetype}`,{
            headers: utils.setApiHeaders('get'),
        })
            .then(response => response.json())
            .then(json => {
            if (json.code == '200') {
                if (typeof json.data !== 'undefined') {
                    dispatch(fetchSocialPagesSuccess(json.data));
                } else {
                    dispatch(fetchSocialNoPages())
                    notie.alert('error', json.message, 3);
                }
            }
            else if(json.code==500){
                notie.alert('error', json.message, 3);
                dispatch(fetchSocialNoPages())

            }    
            else {
                utils.handleSessionError(json);
                notie.alert('error', json.error, 3);
            }
        }).catch(err => {
            throw err;
          });
   }
  }
  export function storeSlackChannelDetail(data){
      return{
          type:types.STORE_SLACK_CHANNEL_DETAIL,
          data:data
      }

  }
  /**
   * to fetch social accounts
   * @param {*} isCallingForSlack if flag is true then this api is calling for slack 
   */
export function fetchSocialAccounts(isCallingForSlack=false) {
    var channelId,
    channelName,
    access_token=''
    var channelDetail={}
    var socialMediaType=''
    if(isCallingForSlack==true){
        socialMediaType=`?social_media=slack`
    }
   return (dispatch) => {
    dispatch(socialAccountsInit())
    fetch(Globals.API_ROOT_URL + `/social-accounts${socialMediaType}`,{
        headers: utils.setApiHeaders('get'),
    })
    .then(response => response.json())
    .then((json) => {
       if (typeof (json.data) !== 'undefined') {
           //if api is calling for slack then to get data from api and call slack channel list api
            if (isCallingForSlack == true) {
                if (json.data !== null && json.data.length > 0) {
                    var allSocialAccount = json.data
                    for (var i = 0; i < allSocialAccount.length; i++) {
                        if (allSocialAccount[i].social_media == 'slack') {
                            var channelname = allSocialAccount[i].user_social.data.filter(function (v) {
                                return v['key'] == 'channel'
                            })
                            var accessToken = allSocialAccount[i].user_social.data.filter(function (v) {
                                return v['key'] == "access_token"
                            })
                            var channelid = allSocialAccount[i].user_social.data.filter(function (v) {
                                return v['key'] == "channel_id"
                            })
                            channelid.map(chid => {
                                channelId = chid.value
                            })
                            channelname.map(chName => {
                                channelName = chName.value
                            })
                            accessToken.map(at => {
                                access_token = at.value
                            })
                            channelDetail = {
                                'accessToken': access_token,
                                'channelId': channelId,
                                'userId': allSocialAccount[i].identity,
                                'channelName': channelName
                            }
                            dispatch(storeSlackChannelDetail(channelDetail))
                        }
                    }
                } 
            }
            dispatch(fetchSocialSuccess(json.data));
        } else {
            utils.handleSessionError(json)
            dispatch(fetchSocialNoChannels())
        }
    }).catch(err => {
          throw err;
        });
 }
}

export function fetchSocialSuccess(socialAccounts) {
    return {
        type: types.FETCH_SOCIAL_SUCCESS,
        socialAccounts: socialAccounts
    };
}

export function fetchSocialNoChannels() {
    return {
        type: types.SOCIAL_ACCOUNTS_NOT_FOUND,
    }
}


export function shareOnSocialMedia(data) {
    var arr={
        "resource":data.resource,
        "user_identity":data.userId,
        "social_data":[
            {
                'social_account_id':data.social_account_id,
                'local_post_id':data.Localid,
                'social_post_id':data.socialId,
                'social_platform':data.social_platform,
            }
        ],
        'type':'share'
    }
    return (dispatch) => {
     fetch(Globals.API_ROOT_URL + `/social/add-data`, {
            method: 'POST',
            headers: utils.setApiHeaders('post'),
            body: JSON.stringify(arr)
        })
     .then(response => response.json())
     .then((json) => {
        if (json.code !== 200) {
            notie.alert('error', json.message, 5);
        }else{
            utils.handleSessionError(json)
        }
    }).catch(err => {
        throw err;
      });
    }
}
