import * as types from './actionTypes';
import { browserHistory } from 'react-router';
import Globals from '../Globals';
import * as utils from '../utils/utils';
import { reset } from 'redux-form';
import notie from "notie/dist/notie.js"
import * as s3functions from '../utils/s3'
import * as s3actions from './s3/s3Actions';
import { profilePost } from './settings/profile'
import { companyInfoPost, companyInfoPut } from './settings/companyInfo'
import { GetProfileSettings } from './settings/getSettings'
export function settingsPostInitiate() {
  return {
    type: types.SETTINGS_SAVING
  };
}
export function profilePostInitiate(flag) {
  return {
    type: types.PROFILE_SAVING,
    flag: flag
  };
}


export function fetchRssDetailSuccess(rsslink) {
  return {
    type: types.FETCH_RSS_DETAILS_LINK,
    rsslink: rsslink
  }
}
export function addRss(rsslink,apiPoint=null) {
  let apiString = "addfeed";
  let method ="POST"
  let objToSend =  {};
  // add  utm to already added rss
  if(apiPoint=="addUtm"){
    apiString ="utm"
    method ="POST"
    Object.assign(objToSend,
      {"utm_link":rsslink.utm_link,
      "rss_identity":rsslink.rss_identity})

  }else if(apiPoint =="editUtm"){
    // edit utm of added rss
    apiString ="utm"
    method="PUT"
    Object.assign(objToSend,
      {"utm_link":rsslink.utm_link,
      "utm_identity":rsslink.utm_identity})
  }else{
    // new rss
    method="POST"
    apiString ="addfeed"
    objToSend=rsslink 
  }
  return (dispatch) => {
    dispatch(RsslinkInit());
    fetch(Globals.API_ROOT_URL + `/rssFeed/${apiString}`, {
      method: method,
      headers: utils.setApiHeaders(method),
      body: JSON.stringify(objToSend)
    }).then(response => {
      if (response.status == "200") {
        dispatch(RsslinkSuccess());

      } else {
        dispatch(RsslinkFail());
      }
      return response.json();
    }).then((json) => {
      if (json.code == 200) {
        dispatch(reset('RssForm'));
        dispatch(getrsslink());
        if(apiPoint=="addUtm"){
          notie.alert('success', "utm added succesfully.", 3);
        }else if(apiPoint=="editUtm"){
          notie.alert('success', "utm edited succesfully.", 3);
        }else{
        notie.alert('success', json.message, 3);}
      } else if (typeof json.error !== "undefined") {
        if(Object.keys(json.error).length > 0){
          var errorKey = Object.keys(json.error)[0];
          notie.alert('error',json.error[errorKey],5)
        }
      } else {
        notie.alert('error', json.message, 3);
      }
    }).catch(err => {
      throw err;
    });
  }
}

export function getUtmData(utm_id){
  return (dispatch) => {
    var me = this;

    fetch(Globals.API_ROOT_URL + `/rssFeed/utm?utm_identity=${utm_id}`,{
      headers: utils.setApiHeaders('get'),
    }).then(response => response.json()).then((json) => {
      if (typeof (json.data) !== 'undefined') {
        dispatch(getUtmDataSuccess(json.data));
      }else if(json.code == 200){
        dispatch(getUtmDataSuccess([]));
      }
    }).catch(err => {
      throw err;
    });
  }
}

export function getUtmDataSuccess(data){
  return{
    type:types.UTM_DATA_SUCCESS,
    data:data
  }
}

export function resetUtmData(){
  return{
    type:types.RESET_UTM_DATA,
  }
}


/*
performs manager survey
*/
export function performManagerSurvey(){
  return (dispatch) => {
    var me = this;

    fetch(Globals.API_ROOT_URL +"/survey/manager",{
      headers: utils.setApiHeaders('get'),
    }).then(response => response.json()).then((json) => {
      if(json.code == 200){
        if(json.hasOwnProperty("message"))
        {
          notie.alert('error', json.message, 3);
        }
        else{
          window.open(json.data);
        }
      }
      else if(json.code == 500)
      {
        notie.alert('error', json.error, 3);

      }
      else{
       //show data
      }
    }).catch(err => {
      throw err;
    });
  }
}


export function getrsslink() {
  return (dispatch) => {
    var me = this;

    fetch(Globals.API_ROOT_URL + `/rssFeed/rssuser`,{
      headers: utils.setApiHeaders('get'),
    }).then(response => response.json()).then((json) => {
      if (typeof (json.data) !== 'undefined') {
        dispatch(fetchRssDetailSuccess(json.data));
      }else if(json.code == 200){
        dispatch(fetchRssDetailSuccess([]));
      }
    }).catch(err => {
      throw err;
    });
  }
}
export function RsslinkInit() {
  return {
    type: types.RSSLINK_INIT
  }
}
export function RsslinkSuccess() {
  return {
    type: types.RSSLINK_SUCCESS,
  }
}
export function RsslinkFail() {
  return {
    type: types.RSSLINK_FAIL,
  }
}
export function fetchContactDetailSuccess(rsslink) {
  return {
    type: types.FETCH_BILL_RSSLINK,
    Rsslink: rsslink
  }
}

//remove rss url
export function removeRssurl(url_id) {
  return (dispatch) => {
    notie.confirm(
      `Are you sure you want to delete this RSS Link?`,
      'Yes',
      'No',
      function () {
        dispatch(removeRssURLApi(url_id));
      },
      function () { }
      
    )
  }

}
export function removeRssURLApi(data) {
  return (dispatch) => {
    dispatch(removerssUrlInit());
    fetch(Globals.API_ROOT_URL + `/rssFeed/addfeed`, {
      method: 'POST',
      headers: utils.setApiHeaders('post'),
      body: JSON.stringify(data)
    }).then(response => {
      if (response.status == "200") {
        dispatch(removerssUrlSuccess());
      } else {
        dispatch(removerssUrlFail());
      }
      return response.json();

    }).then(json => {
      if (json.code == 200) {
        notie.alert('success', json.message, 3);  // need to add note that data will be available in 15 minutes
        dispatch(getrsslink());
      } else {
        notie.alert('error', json.message, 3);
      }

    }).catch(err => {
      throw err;
    });
  }

}
export function removerssUrlInit() {
  return {
    type: types.REMOVE_RSS_URL_EMAIL_INIT,
  }
}

export function removerssUrlSuccess() {
  return {
    type: types.REMOVE_RSS_URL_SUCCESS,
  }
}

export function removerssUrlFail() {
  return {
    type: types.REMOVE_RSS_URL_FAIL,
  }
}
//=====
/**
 * @author disha
 * to save data of step 1 of registration
 * @param {*} fields 
 * @param {*} nextsteppath 
 * @param {*} isUpdate 
 * @param {*} isOnBoarding 
 */
export function settingsPostStep1(fields, nextsteppath, isUpdate = false, isOnBoarding = {}) {
  return (dispatch) => {

    dispatch(settingsPostInitiate());
    if (typeof (fields["files"]) !== "undefined" || fields.uploadedFile) {
      var me = this;
      Globals.AWS_CONFIG.albumName = localStorage.getItem('albumName')
      var s3Config = Globals.AWS_CONFIG;
      var s3Manager = new s3functions.S3Manager(s3Config);

      var filesToUpload = fields.files;

      // we are uploading files to s3 now, set the flag in redux
      dispatch(s3actions.uploadReset());
      dispatch(s3actions.uploadingStart());

      var fileToUpload = filesToUpload[0];

      // upload starts here.
      // it returns a promise whether succeeded or failed
      // after you get the promise, proceed with db insertion
      var s3MediaURL = s3Manager.s3uploadMedia(fileToUpload, 0, filesToUpload.length);
      // On succession of uploading main asset on amazon, we will generate a thumbnail and
      // send next request to amazon
      s3MediaURL.then((obj) => {

        var file = obj.data;
        var originalFileObj = obj.fileObject;
        var index = obj.index;

        var generatedFileName = file.Location.split("/");
        generatedFileName = generatedFileName[generatedFileName.length - 1];
        generatedFileName = generatedFileName.substring(0, generatedFileName.length - 41);



        var photokey=file.Location.split('amazonaws.com/')[1]
        let uploadedFile = {
          fileUrl: `https://s3.${Globals.AWS_BUCKET_REGION}.amazonaws.com/${file.Bucket}/${photokey}`,
          thumbUrl: "",
          uploadedFile: generatedFileName
        }
        dispatch(s3actions.uploadedItem(uploadedFile));
        if (!isUpdate) {
          companyInfoPost(fields,dispatch, nextsteppath, uploadedFile.fileUrl, isOnBoarding)
        }
        else {
          fields["company_logo"] = uploadedFile.fileUrl;
          companyInfoPut(fields, dispatch,uploadedFile.fileUrl, isOnBoarding)
        }


      })

        .catch(err => {
          notie.alert('error', 'There was a problem saving your data. Please try again!', 3);
          throw err;
        });
    }

    // user didn't upload image, simple post textual content
    else {

      if (!isUpdate) {
        companyInfoPost(fields,dispatch,nextsteppath, null, isOnBoarding)
      }
      else {
        companyInfoPut(fields,dispatch,isOnBoarding)
      }
    }


  }

}

export function shareCredits(shareFlag) {
  return (dispatch) => {
    fetch(Globals.API_ROOT_URL + `/link-share?sharelink=${shareFlag}`, {
      method: 'GET',
      // mode: 'no-cors',
      headers: utils.setApiHeaders('get',{
        'Access-Control-Allow-Origin': '*',
        Accept: 'application/json',
        'Content-Type': 'application/json'
      }),
    }).then(response => {
      if (response.status != "200") {
        notie.alert('error', 'There is some problem to enable sharelink.', 3);
      }
      return response.json();
    }).then(json => {
      if (json.code == "200") {
        localStorage.setItem("sharelink",shareFlag);
        // notie.alert('success', json.message, 3);
      }
      else {
        utils.handleSessionError(json)
        // notie.alert('error', json.message, 3);
      }
    })
      .catch(err => {
        throw err;
      });
  }
}
/**
 * @author disha
 * to save company data of step 2 of registration
 * @param {*} fields 
 * @param {*} nextsteppath 
 * @param {*} OnBoardingDetail 
 */
export function saveYourProfile_Step2(fields, nextsteppath, OnBoardingDetail = {}) {
  var companyUpdate=false
  let arr = [];
  if (OnBoardingDetail !== {}) {
    var addObj = {
      is_onboarding: OnBoardingDetail.isOnBoarding == true ? 1 : 0,
      step: OnBoardingDetail.stepNumber
    }
    OnBoardingDetail.isOnBoarding == true ? Object.assign(fields, addObj) : ''
  }
  if(fields.Company_Name_identity==''){
    //if id is blank then it mean user is saving data for first time
    companyUpdate=false
  }else{
    companyUpdate=true
  }
  Object.keys(fields).forEach((key) => {
    let val = fields[key];
    let identity = fields[key + "_identity"];
    if(typeof(identity)!=="undefined" && identity!=="") {
      arr.push({
        "key": key,
        "values": val,
        "order": "1",
        "identity": identity
      });
    }
    else if (key.indexOf('_identity') == -1) {
      arr.push({
        "key": key,
        "values": val,
        "order": "1",
      });
    }
  });
  let jsonBody = {
    "data": arr
  };
  return (dispatch) => {
    if(companyUpdate==false){
      dispatch(settingsPostInitiate());
    fetch(Globals.API_ROOT_URL + `/settings`, {
      method: 'POST',
      headers: utils.setApiHeaders('post'),
      body: JSON.stringify(jsonBody)
    })
      .then(response => {
        if (response.status == "200") {
        }
        else {
          dispatch(settingsError(response.statusText));
        }
        return response.json();
      })
      .then(json => {
        if(json.code==200){
          dispatch(finishAllOnBoardingStep());
          dispatch(settingsSuccess(nextsteppath));
          // dispatch(settingsSuccess(nextsteppath));
        }else{
          utils.handleSessionError(json)
          notie.alert('error', 'There is a problem saving your data. Please try again!', 3);
        }
      })
      .catch(err => { throw err; });
    }else{
      //if company detail is already filled and user want to update that detail then to call PUT method api
      companyInfoPut(fields,dispatch,null,nextsteppath)
    }
  }
}
export function settingsProfilePost(fields, nextsteppath, flag,isGuest=null,callFrom=null) {

  return (dispatch) => {
      dispatch(profilePostInitiate(flag));

    if (typeof (fields["files"]) !== "undefined" || fields.photoUpload) {

      var me = this;
      Globals.AWS_CONFIG.albumName = localStorage.getItem('albumName')
      var s3Config = Globals.AWS_CONFIG;
      var s3Manager = new s3functions.S3Manager(s3Config);

      var filesToUpload = fields.files;
      // to compress the profile image  
      var resized = utils.resizeImage(fields.files[0],'',800);
      
      resized.then(blob => {
      var temp= fields.files[0].name.split(".");
      var fName = temp[0];
      var type = temp[1];
      let newFile = new File([blob], fName + '_' + Date.now() + '.'+ type , { type: fields.files[0].type })
      var addMimeObj={
        mimetype:fields.files[0].mimetype
      }
      Object.assign(newFile,addMimeObj)
      var url = URL.createObjectURL(blob)
      newFile.preview = url
      
        // we are uploading files to s3 now, set the flag in redux
      dispatch(s3actions.uploadReset());
      dispatch(s3actions.uploadingStart());

      // var fileToUpload = filesToUpload[0];

      // upload starts here.
      // it returns a promise whether succeeded or failed
      // after you get the promise, proceed with db insertion
      var s3MediaURL = s3Manager.s3uploadMedia(newFile, 0, filesToUpload.length);

      // On succession of uploading main asset on amazon, we will generate a thumbnail and
      // send next request to amazon
      s3MediaURL.then((obj) => {

        var file = obj.data;
        var originalFileObj = obj.fileObject;
        var index = obj.index;

        var generatedFileName = file.Location.split("/");
        generatedFileName = generatedFileName[generatedFileName.length - 1];
        generatedFileName = generatedFileName.substring(0, generatedFileName.length - 41);


        var photokey=file.Location.split('amazonaws.com/')[1]
        let uploadedFile = {
          fileUrl: `https://s3.${Globals.AWS_BUCKET_REGION}.amazonaws.com/${file.Bucket}/${photokey}`,
          thumbUrl: "",
          uploadedFile: generatedFileName
        }
        dispatch(s3actions.uploadedItem(uploadedFile));
        isGuest==true?
          dispatch(saveGuestSignupStep1(fields, uploadedFile.fileUrl))
        : profilePost(fields, dispatch, nextsteppath, uploadedFile.fileUrl)

      })
      }) 
        .catch(err => {
          notie.alert('error', 'There was a problem saving your data. Please try again!', 3);
          throw err;
        });
    }

    // user didn't upload image, simple post textual content
    else {
      profilePost(fields, dispatch, nextsteppath,null,callFrom)
    }
  }
}
/**
 * @author disha
 * call api after completing all 3 step
 */
export function finishAllOnBoardingStep() {
  return (dispatch) => {
    fetch(Globals.API_ROOT_URL + `/settings/finish`,{
      headers: utils.setApiHeaders('get'),
    })
      .then(response => response.json())
      .then((json) => {
        if(json.code==200){
          // notie.alert('success', json.message, 3);
        }else{
          utils.handleSessionError(json)
          notie.alert('error', json.message, 3);
        }
      })
      .catch(err => {
        throw err;
      });
  }
}

/**
 * @author akshay
 * get countrycode
 */
export function getCountycode() {
  return (dispatch) => {
    fetch(Globals.API_ROOT_URL_MASTER + `/getCountryCode`,{
      headers: utils.setApiHeaders('get'),
      method: 'GET'
    })
      .then(response => response.json())
      .then((json) => {
        if(json.hasOwnProperty('data')){
         dispatch(getCountycodeSuccess(json.data)) 
          // notie.alert('success', json.message, 3);
        }else{
          notie.alert('error', 'Problem in fetching country codes.', 3);
        }
      })
      .catch(err => {
        throw err;
      });
  }
}

export function getCountycodeSuccess(data) {
  return {
    type: types.GET_COUNTRY_CODE_LIST_SUCCESS,
    data
  };

}

/**
 * @author disha
 * to save step 3 detail in register
 * @param {*} fields 
 * @param {*} path 
 * @param {*} OnBoardingDetail step number and flag that will indecate that this api is calling for onboarding
 */
export function settingsPostStep3(fields, path, OnBoardingDetail) {
  if (OnBoardingDetail !== {}) {
    var addObj = {
      is_onboarding: OnBoardingDetail.isOnBoarding == true ? 1 : 0,
      step: OnBoardingDetail.stepNumber
    }
    OnBoardingDetail.isOnBoarding == true ? Object.assign(fields, addObj) : ''
  }
  return (dispatch) => {

    dispatch(settingsPostInitiate());
    fetch(Globals.API_ROOT_URL + `/user/profile`, {
      method: 'PUT',
      headers: utils.setApiHeaders('put'),
      body: JSON.stringify(fields)
    })
      .then(response => {
        if (response.status == "200") {
        }
        else {
          dispatch(settingsError(response.statusText));
        }
        return response.json();
      })
      .then(json => {
        if(json.hasOwnProperty("data")) {
          dispatch(finishAllOnBoardingStep());
          dispatch(settingsSuccess(path));
        }else{
          utils.handleSessionError(json)
          notie.alert('error', 'There is a problem saving your data. Please try again!', 3);
        }
      })
      .catch(err => { throw err; });
  }
}

export function settingsSuccessFlag() {
  return {
    type: types.SETTINGS_SUCCESS
  };
}

export function settingsSuccess(path) {
  return (dispatch) => {
    if (path != null) {//it is on boarding
      dispatch(settingsSuccessFlag());
      if(path == '/tour'){
          if(utils.detectmob() == "ios"){
            if (typeof window !== 'undefined'){
              window.location.replace(`com.visibly.app://`);
            }
          }else if(utils.detectmob() == "android"){
            if (typeof window !== 'undefined'){
              window.location.replace(`intent:#Intent;action=visibly.appopenfromweb.io;category=android.intent.category.DEFAULT;category=android.intent.category.BROWSABLE;S.msg_from_browser=Launched%20from%20Browser;package=com.visibly.app;end`);
            }
          }else{
            browserHistory.push(path);  
          }
      }else{
        browserHistory.push(path);  
      }
    }
    else//it is settings
    {
      dispatch(settingsSuccessFlag());
    }
  }
}


export function profileSuccessFlag(profile) {
  localStorage.setItem("UserEmail",profile.email);
  localStorage.setItem("UserId",profile.identity);
  localStorage.setItem("UserName",profile.firstname + profile.lastname);
  return {
    type: types.PROFILE_SUCCESS,
    profile: profile
  };
}


export function companyInfoSuccessFlag(companyInfo) {
  return {
    type: types.SETTINGS_COMPANY_INFO_SUCCESS,
    companyInfo
  };

}


export function profileSuccess(path, profile,callFrom) {
  return (dispatch) => {
    if (path != '') {//it is on boarding
      dispatch(profileSuccessFlag(profile));
      browserHistory.push(path);
    }
    else//it is settings
    {
      dispatch(profileSuccessFlag(profile.data));
      if(callFrom !== null && callFrom == 'content'){
        notie.alert('success', 'Curated topics saved successfully', 3);
      }else{
        notie.alert('success', 'Profile updated!', 3);
      }
    }
  }
}


export function profileError(statusText) {
  return {
    type: types.SETTINGS_ERROR,
    text: statusText,
  };
}

export function settingsError(statusText) {
  return {
    type: types.SETTINGS_ERROR,
    text: statusText,
  };
}
export function GuestSignUpInit(){
  return{
    type:types.GUEST_SIGNUP_INIT
  }
}
export function successGuestSignUp(data){
  return{
    type : types.SUCCESS_GUEST_SIGNUP
  }
}
export function errorGuestSignUp(data){
  notie.alert('error', data.message !== undefined ?data.message:data, 3);
  return{
    type:types.ERROR_GUEST_SIGNUP
  }
}
/**
 * @author disha
 * @param {*} fields 
 * to save guest user signup detail
 */
export function saveGuestSignupStep1(fields,imageResponse=null) {
  
  var objToSend = {
    "firstname": fields.firstname,
    "lastname": fields.lastname,
    "Company_Name":fields.Company_Name,
    "designation":fields.designation,
  }
  if(imageResponse!=null) {
    var objToSend = {
    "avatar": imageResponse
    }
  }
  return (dispatch) => {
    dispatch(GuestSignUpInit());
    fetch(Globals.API_ROOT_URL + `/settings/guest-onboarding`, {
      method: 'POST',
      headers: utils.setApiHeaders('post'),
      body: JSON.stringify(objToSend)
    })
      .then(response => response.json())
      .then(json => {
        if (json.code == 200) {
          // dispatch(GetProfileSettings(null,true))
          dispatch(successGuestSignUp(json));
          if(utils.detectmob() == "ios"){
            if (typeof window !== 'undefined'){
              window.location.replace(`com.visibly.app://`);
            }
          }else if(utils.detectmob() == "android"){
            if (typeof window !== 'undefined'){
              window.location.replace(`intent:#Intent;action=visibly.appopenfromweb.io;category=android.intent.category.DEFAULT;category=android.intent.category.BROWSABLE;S.msg_from_browser=Launched%20from%20Browser;package=com.visibly.app;end`);
            }
          }else{
            browserHistory.push(`/tour`);
          }
        } else {
          if (json.error !== undefined) {
            dispatch(errorGuestSignUp(json.error));
          } else {
            dispatch(errorGuestSignUp(json));
          }
          utils.handleSessionError(json)
        }

      })
      .catch(err => { throw err; });
  }
}
export function curationTopicListInit(){
  return{
    type:types.CURATION_TOPIC_LIST_INIT
  }
}
export function curationTopicListSuccess(data){
  return{
    type:types.CURATION_TOPIC_LIST_SUCCESS,
    data:data
  }
}
export function curationTopicListError(){
  return{
    type:types.CURATION_TOPIC_LIST_ERROR
  }
}
/**
 * @author disha
 * 
 */
export function curationTopicList() {
  return (dispatch) => {
    dispatch(curationTopicListInit())
    fetch(Globals.API_ROOT_URL + `/rssFeed/rssCategory`,{
      headers: utils.setApiHeaders('get'),
    })
      .then(response => response.json())
      .then((json) => {
        if(json.code==200){
          if(json.hasOwnProperty("data")) {
            dispatch(curationTopicListSuccess(json.data))
            // notie.alert('success', json.message, 3);
            }else{
              dispatch(curationTopicListSuccess([]))
            }
        }else{
          dispatch(curationTopicListError())
          utils.handleSessionError(json)
          notie.alert('error', json.message, 3);
        }
      })
      .catch(err => {
        throw err;
      });
  }
}
/**
* Get survey setting data
* @author Yamin
**/
export function fetchSurveySetting(){
  return (dispatch) => {
    dispatch(fetchSurveySettingInit())
    fetch(Globals.API_ROOT_URL + `/survey/interval`,{
      headers: utils.setApiHeaders('get'),
    })
      .then(response => response.json())
      .then((json) => {
        if(json.code==200){
          dispatch(fetchSurveySettingSuccess(json.data))
        }else{
          dispatch(fetchSurveySettingFail())
        }
      })
      .catch(err => {
        throw err;
      });
  }
}

export function fetchSurveySettingInit(){
  return{
    type:types.GET_SURVEY_SETTING_INIT
  }
}
export function fetchSurveySettingSuccess(data){
  return{
    type:types.GET_SURVEY_SETTING_SUCCESS,
    data:data
  }
}
export function fetchSurveySettingFail(){
  return{
    type:types.GET_SURVEY_SETTING_FAIL
  }
}

/**
* Update suvey setting 
* @author Yamin
**/
export function updateSurveySetting(surveyData){

  if(surveyData.survey_interval == 0){
      
                    return (dispatch) => {
                      notie.confirm(
                        `Are you sure you want stop survey?`,
                        'Yes',
                        'No',
                        function () {
                          dispatch(updateSurveySettingAPI(surveyData))     
                      })
                    }
              
  }else{
      return (dispatch) => {
          dispatch(updateSurveySettingAPI(surveyData)) 
      }   
  }
  
}

export function updateSurveySettingAPI(surveyData){
                  var objToSend = {
                      "interval": surveyData.survey_interval,
                      "survey": 1,
                      "email_survey": 1,
                    }
                    return (dispatch) => {
                      dispatch(updateSurveySettingInit());
                      fetch(Globals.API_ROOT_URL + `/survey/addInterval`, {
                        method: 'POST',
                        headers: utils.setApiHeaders('post'),
                        body: JSON.stringify(objToSend)
                    })
                    .then(response => response.json())
                    .then(json => {
                    if (json.code == 200) {
                          notie.alert('success',json.message,3)
                    }else{
                          notie.alert('error',json.message,3)
                    }
                      dispatch(updateSurveySettingSuccess())

                    })
                    .catch(err => { throw err; });
         }
}

export function updateSurveySettingInit(){
  return{
    type:types.UPDATE_SURVEY_SETTING_INIT
  }
}

export function updateSurveySettingSuccess(){
  return{
    type:types.UPDATE_SURVEY_SETTING_SUCCESS
  } 
}
/**
* add hashtag into content settings
* @author Kinjal Birare
**/
export function AddHashTag(tags){
    return (dispatch) => {
      dispatch(addHashTagContentInit());
      fetch(Globals.API_ROOT_URL + `/hashtag`, {
        method: 'POST',
        headers: utils.setApiHeaders('post'),
        body: JSON.stringify(tags)
    })
    .then(response => response.json())
    .then(json => {
      if (json.code == 200) {
            notie.alert('success',json.message,3)
            dispatch(addHashTagContentSuccess())
            dispatch(fetchHashTagList());
      }else{
        if (typeof json.error !== "undefined") {
          if(json.error[`hashtag.0`][0] !== undefined ){
            notie.alert('error',json.error[`hashtag.0`][0],3)
          }else{
            notie.alert('error','Something went wrong. Please try again later!',3)
          }
        }else{
            notie.alert('error',json.message,3)
        }
        dispatch(addHashTagContentError())
        utils.handleSessionError(json)
      }  
    })
    .catch(err => { throw err; });
  }
}

export function addHashTagContentInit(){
  return{
  type:types.ADD_HASHTAG_CONTENT_INIT
  }
} 

export function addHashTagContentSuccess(){
  return{
  type:types.ADD_HASHTAG_CONTENT_SUCCESS
  } 
}
export function addHashTagContentError(){
  return{
  type:types.ADD_HASHTAG_CONTENT_ERROR
  } 
}

/**
* fetch the hashtag in content settings
* @author Kinjal Birare
**/

export function fetchHashTagList() {
  return dispatch => {
    dispatch(fetchHashTagListInit());
    fetch(Globals.API_ROOT_URL +
        `/hashtag`, {
          headers: utils.setApiHeaders('get'),
        })
      .then(response => response.json())
      .then(json => {
        if (json.code == 200 && json.data !== undefined) {
          dispatch(fetchHashTagListSuccess(json.data));
        } else {
          dispatch(fetchHashTagListError());
        }
      })
      .catch(err => {
        throw err
      })
  }
}
export function fetchHashTagListInit(){
  return {
    type: types.FETCH_HASHTAG_LIST_INIT
  }
}
export function fetchHashTagListSuccess(data) {
  return {
    type: types.FETCH_HASHTAG_LIST_SUCCESS,
    tags: data
  }
}
export function fetchHashTagListError() {
  return {
    type: types.FETCH_HASHTAG_LIST_ERROR
  }
}

/**
* delete hashtag in content settings
* @author Kinjal Birare
**/
export function deleteHashTag(hashTagId){
  return (dispatch) => {
    fetch(`${Globals.API_ROOT_URL}/hashtag`, {
          method: 'DELETE',
          headers: utils.setApiHeaders('delete'),
          body: JSON.stringify(hashTagId)
        })
      .then(response => response.json())
      .then((json) => {
        if(json.code==200){
          notie.alert('success', json.message, 5)
          dispatch(fetchHashTagList())
        }
        else{
          notie.alert('error', json.message, 5)
        }
      })
  }

}
//edit
export function editHashTag(tags) {
  return (dispatch) => {
    dispatch(editHashTagContentInit());
    fetch(Globals.API_ROOT_URL + `/hashtag`, {
      method: 'PUT',
      headers: utils.setApiHeaders('put'),
      body: JSON.stringify(tags)
    })
      .then(response => response.json())
      .then((json) => {
        if(json.code==200){
          notie.alert('success', json.message, 5)
          dispatch(fetchHashTagList())
          dispatch(editHashTagContentSuccess());
        }
        else{
          dispatch(editHashTagContentError());
          notie.alert('error', json.message, 5)
        }
      })
  }
}
export function editHashTagContentInit(){
  return{
  type:types.EDIT_HASHTAG_CONTENT_INIT
  }
} 

export function editHashTagContentSuccess(){
  return{
  type:types.EDIT_HASHTAG_CONTENT_SUCCESS
  } 
}
export function editHashTagContentError(){
  return{
  type:types.EDIT_HASHTAG_CONTENT_ERROR
  } 
}
export function autoHashTag(flag) {
  return (dispatch) => {
    fetch(Globals.API_ROOT_URL + `/auto-hashtag`, {
      method: 'PUT',
      headers: utils.setApiHeaders('put'),
      body: JSON.stringify(flag)
    })
      .then(response => response.json())
      .then((json) => {
        if(json.code==200){
          notie.alert('success', json.message, 5)
          // dispatch(fetchHashTagList())
        }
        else{
          notie.alert('error', json.message, 5)
        }
      })
  }
}
export function addSsoUserInit(){
  return{
  type:types.ADD_SSO_USER_INIT
  } 
}
export function addSsoUserSuccess(){
  return{
  type:types.ADD_SSO_USER_SUCCESS,
  } 
}
export function addSsoUserError(){
  return{
  type:types.ADD_SSO_USER_ERROR
  } 
}


export function addSsoUser(value) {
var arr ={
  "firstname": value.first_name,
  // "email":
  "lastname":value.last_name,
  "business_unit": value.business_unit,
  "company_role": value.company_role,
  "country": value.country,
  "department_identity": value.department,
  "office": value.office,
  "region": value.region,
  "mobile_country_code": value.mobile_country_code,
  "mobile_no":value.mobile_no,
  "gender": value.country,
  "role_id": value.company_role
}
if(value.birth_date !== null ){
   arr ={
    ...arr,
    "birth_date": value.birth_date,
  }
}
  return (dispatch) => {
    dispatch(addSsoUserInit());
    fetch(Globals.API_ROOT_URL + `/addUserSso`, {
      method: 'POST',
      headers: utils.setApiHeaders('post'),
      body: JSON.stringify(arr),
    })
      .then((response) => response.json())
      .then((json) => {
        if (typeof json.data !== 'undefined' && Object.keys(json.data).length>0) {
          var path = utils.removeTrailingSlash(window.location.href).split('/')
          var domainName = path !== undefined && path[2] !== undefined ? path[2].split(':')[0] : '' // get domain name from path to set domain name in cookie and if visibly is running on localhost:3030 then to get only localhost i split the string
          //to get date after 5 years from now to set exipre date of cookie
          var expireDate = new Date();
          expireDate.setFullYear(expireDate.getFullYear() + 5);

          document.cookie = `session_id=${json.data.session_id};expires=${expireDate};domain=${domainName};`
          localStorage.setItem('session_id', json.data.session_id)
          Globals.SESSION_ID = json.data.session_id
          dispatch(addSsoUserSuccess());
          browserHistory.push("/feed/default/live");
        } else {
          notie.alert("error", json.message, 3);
          dispatch(addSsoUserError());
          utils.handleSessionError(json);
        }
      })
      .catch((err) => {
        throw err;
      });
  };
}