import * as types from './actionTypes';
import Globals from '../Globals';
import * as utils from '../utils/utils';
import notie from "notie/dist/notie.js"
import { reset } from 'redux-form';
import { push } from 'react-router';
import { browserHistory } from 'react-router'

//getting payment detail for overview
export function getDetail() {
  return (dispatch) => {
    dispatch(fetchBillDetailInit());
    var me = this;

    fetch(Globals.API_ROOT_URL + `/billing/calcualtion`,{
      headers: utils.setApiHeaders('get')
    }).then(response => response.json()).then((json) => {
      if (typeof(json.data) !== undefined) {
        dispatch(fetchBillDetailSuccess(json.data));
      }else{
        utils.handleSessionError(json)
      }
    }).catch(err => {
      throw err;
    });
  }
}

export function fetchBillDetailSuccess(billDetail) {
  return {
    type: types.CURRENT_BILL_DETAIL,
    billDetail: billDetail
  }
}

export function fetchBillDetailInit() {
  return {
    type: types.CURRENT_BILL_DETAIL_INIT
  }
}
//getting emails
export function getContactEmail() {
  return (dispatch) => {
    var me = this;

    fetch(Globals.API_ROOT_URL + `/billing/contact`,{
      headers: utils.setApiHeaders('get')
    }).then(response => response.json()).then((json) => {
      if (typeof(json.data) !== undefined) {
        dispatch(fetchContactDetailSuccess(json.data));
      }else{
        utils.handleSessionError(json)
      }
    }).catch(err => {
      throw err;
    });
  }
}

export function fetchContactDetailSuccess(contactEmail) {
  return {
    type: types.FETCH_BILL_CONTACT_EMAIL,
    email: contactEmail
  }
}
//add emails
export function addContactEmailInit(){
    return {
    type: types.BILL_CONTACT_EMAIL_INIT
  }
}
export function addContactEmail(contactEmail){
    return (dispatch) => {
        dispatch(addContactEmailInit());
        fetch(Globals.API_ROOT_URL + `/billing/contact`, {
          method: 'POST',
          headers: utils.setApiHeaders('post'),
          body: JSON.stringify(contactEmail)
        }).then(response => {
          if (response.status == "200") {
            dispatch(AddContactEmailSuccess());
          }else{
            dispatch(AddContactEmailFail());
          }
          return response.json();
        }).then((json) => {
          if (json.code == 200) {
            dispatch(reset('SettingContactForm'));
            dispatch(getContactEmail());
            notie.alert('success', json.message, 3);
          }else if(typeof json.error !== "undefined"){
            utils.handleSessionError(json)
            notie.alert('error', json.error.contact_email, 3);
          }else{
            notie.alert('error', json.message, 3);
          }

        }).catch(err => {
          throw err;
        });
    }
}

export function AddContactEmailSuccess() {
  return {
    type: types.BILL_CONTACT_EMAIL_SUCCESS,
  }
}
export function AddContactEmailFail() {
  return {
    type: types.BILL_CONTACT_EMAIL_FAIL,
  }
}
//remove emails
export function removeContactEmail(emailId){
    return (dispatch) => {
            notie.confirm(
              `Are you sure you want to delete this email?`,
              'Yes',
              'No',
              function () {
                  dispatch(removeContactEmailApi(emailId));
              },
              function () { }
            )
            }

}

export function removeContactEmailApi(emailId){
    return (dispatch) => {
        dispatch(removeContactEmailInit());
        fetch(Globals.API_ROOT_URL + `/billing/contact/${emailId}`, {
          method: 'DELETE',
          headers: utils.setApiHeaders('delete'),
        }).then(response => {
          if (response.status == "200") {
            dispatch(removeContactEmailSuccess());
          }else{
            dispatch(removeContactEmailFail());
          }
          return response.json();
        }).then(json => {
          if (json.code == 200) {
            notie.alert('success', json.message, 3);
            dispatch(getContactEmail());
          }else{
            utils.handleSessionError(json)
            notie.alert('error', json.message, 3);
          }

        }).catch(err => {
          throw err;
        });
      }

}

export function removeContactEmailInit(){
  return {
    type: types.REMOVE_BILL_CONTACT_EMAIL_INIT,
  }
}

export function removeContactEmailSuccess() {
  return {
    type: types.REMOVE_BILL_CONTACT_EMAIL_SUCCESS,
  }
}

export function removeContactEmailFail(){
  return {
    type: types.REMOVE_BILL_CONTACT_EMAIL_FAIL,
  }
}
//for getting country
export function getCountry() {
  return (dispatch) => {
    //dispatch(fetchBillDetailInit());
    var me = this;

    fetch(Globals.API_ROOT_URL_MASTER + `/address/country`).then(response => response.json()).then((json) => {
      if (typeof(json.data) !== "undefined") {
        dispatch(fetchCountrySuccess(json.data));
      }else{
        utils.handleSessionError(json)
        dispatch(fetchCountryError());
      }
    }).catch(err => {
      throw err;
    });
  }
}

export function fetchCountrySuccess(countryDetail) {
  return {
    type: types.FETCH_BILL_COUNTRY,
    countryDetail: countryDetail
  }
}

export function fetchCountryError() {
  return {
    type: types.FETCH_BILL_COUNTRY_ERROR
  }
}
//for getting state from country
export function fetchState(country) {
  return (dispatch) => {
    dispatch(fetchStateInit());
    if(country != 0){

      var me = this;

      fetch(Globals.API_ROOT_URL_MASTER + `/address/state/`+country).then(response => response.json()).then((json) => {
        if (typeof(json.data) !== "undefined") {
          dispatch(fetchStateSuccess(json.data));
        }else{
          utils.handleSessionError(json)
          dispatch(fetchStateError());
        }
      }).catch(err => {
        throw err;
      });
    }else{
        dispatch(fetchStateError());
    }
  }
}

export function fetchStateSuccess(stateDetail) {
  return {
    type: types.FETCH_BILL_STATE,
    stateDetail: stateDetail
  }
}

export function fetchStateError() {
  return {
    type: types.FETCH_BILL_STATE_ERROR
  }
}

export function fetchStateInit() {
  return {
    type: types.FETCH_BILL_STATE_INIT
  }
}

//for getting city from state
export function fetchCity(state) {
  return (dispatch) => {
    dispatch(fetchCityInit());
    if(state != 0){
      var me = this;
       fetch(Globals.API_ROOT_URL_MASTER + `/address/city/`+state).then(response => response.json()).then((json) => {
        if (typeof(json.data) !== "undefined") {
          dispatch(fetchCitySuccess(json.data));
        }else{
          utils.handleSessionError(json)
          dispatch(fetchCityError());
        }
      }).catch(err => {
        throw err;
      });
    }else{
        dispatch(fetchCityError());
    }
  }
}

export function fetchCitySuccess(cityDetail) {
  return {
    type: types.FETCH_BILL_CITY,
    cityDetail: cityDetail
  }
}
export function fetchCityError() {
  return {
    type: types.FETCH_BILL_CITY_ERROR
  }
}

export function fetchCityInit() {
  return {
    type: types.FETCH_BILL_CITY_INIT
  }
}
//for getting contact details
export function getContactDetail() {
  return (dispatch) => {
    dispatch(fetchContactDetailInit());
    var me = this;

    fetch(Globals.API_ROOT_URL + `/billing/detail`,{
      headers: utils.setApiHeaders('get')
    }).then(response => response.json()).then((json) => {
      if (typeof(json.data) !== "undefined") {
        dispatch(getContactDetailSuccess(json.data));
      }else{
        utils.handleSessionError(json)
      }
    }).catch(err => {
      throw err;
    });
  }
}

export function fetchContactDetailInit(){
  return {
    type: types.FETCH_CONTACT_DETAIL_INIT
  }
}

export function getContactDetailSuccess(contactDetail) {
  return {
    type: types.FETCH_BILL_CONTACT_DETAIL,
    contactDetail: contactDetail
  }
}
//for update contacts detail
export function updateContactDetail(contactDetail){
      return (dispatch) => {
          dispatch(updateContactDetailInit());
          var arr = {
            'country_id': contactDetail.country,
            'company': contactDetail.company,
            'address': contactDetail.address,
            'state_id': contactDetail.state,
            'city_id': contactDetail.city,
            'postal': contactDetail.postal,
            'vat_no': contactDetail.vat_no

          }

          fetch(Globals.API_ROOT_URL + `/billing/detail`, {
              method: 'PUT',
              headers: utils.setApiHeaders('put'),
              body: JSON.stringify(arr)
          }).then(response => {
              dispatch(updateContactDetailSuccess());
              return response.json();
          })
          .then((json) => {
              if (json.code == 200) {
                notie.alert('success', json.message, 3);
              }else{
                utils.handleSessionError(json)
                notie.alert('error', json.message, 3);
              }

          })
          .catch(err => {
              throw err;
          });
        }
}

export function updateContactDetailInit() {
  return {
    type: types.UPDATE_CONTACT_DETAIL_INIT
  }
}

export function updateContactDetailSuccess() {
  return {
    type: types.UPDATE_CONTACT_DETAIL,
  }
}

//for getting cards
export function getCards() {
  return (dispatch) => {
    dispatch(getCardsInit());
    var me = this;
    fetch(Globals.API_ROOT_URL + `/payment/cards`,{
      headers: utils.setApiHeaders('get')
    })
    .then(response =>{
      if (response.status == "500") {
           dispatch(getCardsErrors());
      }
      return response.json();
    }).then((json) => {
      if (typeof(json.data) !== "undefined") {
        dispatch(getCardsErrorsSuccess(json.data));
      }else{
        utils.handleSessionError(json)
        dispatch(getCardsErrors());
      }
    }).catch(err => {
      throw err;
    });
  }
}

export function getCardsInit() {
  return {
    type: types.FETCH_BILL_CARD_INIT
  }
}

export function getCardsErrorsSuccess(cardDetail) {
  return {
    type: types.FETCH_BILL_CARD,
    cardDetail: cardDetail
  }
}

export function getCardsErrors() {
  return {
    type: types.FETCH_BILL_CARD_ERROR
  }
}

//add cards
export function addCard(cardData){
    return (dispatch) => {
          dispatch(getCardsInit());
          var arr = {
            'card_name': cardData.card_name,
            'card_number': cardData.card_number,
            'exp_month': cardData.exp_month,
            'exp_year': cardData.exp_year,
            'card_cvc': cardData.card_cvc,
            'zip_code': cardData.zip_code,

          }
          fetch(Globals.API_ROOT_URL + `/billing/card`, {
              method: 'POST',
              headers: utils.setApiHeaders('post'),
              body: JSON.stringify(arr)
          }).then(response => {

              return response.json();
          })
          .then((json) => {
              if(json.code == 422){
                notie.alert('warning', json.message, 3);
                dispatch(addCardError())
              }else if(json.code == 200){
                dispatch(getCards());
                dispatch(reset('PaymentMethodForm'));
                notie.alert('success', json.message, 3);
                dispatch(addCardSuccess())
                dispatch(changePayPopup('pay'));
              }else{
                utils.handleSessionError(json)
                notie.alert('error', json.message, 3);
                dispatch(addCardError())
              }


          })
          .catch(err => {
              throw err;
          });
        }
}

export function addCardSuccess() {
  return {
    type: types.ADD_BILL_CARD_SUCCESS
  }
}

export function addCardError() {
  return {
    type: types.ADD_BILL_CARD_ERROR
  }
}
export function removeCard(cardId){
    return (dispatch) => {
            notie.confirm(
              `Are you sure you want to delete this card?`,
              'Yes',
              'No',
              function () {
                  dispatch(removeCardApi(cardId));
              },
              function () { }
            )
            }

}
//remove card
export function removeCardApi(cardId){
      return (dispatch) => {
          dispatch(removeCardInit());

          fetch(Globals.API_ROOT_URL + `/billing/card/`+cardId, {
              method: 'DELETE',
              headers: utils.setApiHeaders('delete'),
              body: JSON.stringify()
          }).then(response => {
              if(response.status == 200){
                dispatch(getCards())
                dispatch(removeCardFinish())

              }else{
                utils.handleSessionError(json)
                dispatch(removeCardFinish())
              }
              return response.json();
          })
          .then((json) => {
              if(json.code == 200){
                notie.alert('success', json.message, 3);
              }else{
                utils.handleSessionError(json)
                notie.alert('error', json.message, 3);
              }

          })
          .catch(err => {
              throw err;
          });
      }
}

export function removeCardInit(){
    return {
      type: types.REMOVE_BILL_CARD_INIT
    }
}

export function removeCardFinish() {
  return {
    type: types.REMOVE_BILL_CARD
  }
}

export function updateCard(cardDetail){
    return (dispatch) => {
          dispatch(updateCardInit());
          var arr = {
            'card_name': cardDetail.card_name,
            'default': cardDetail.default
          }
          fetch(Globals.API_ROOT_URL + `/billing/card/`+cardDetail.card_id, {
              method: 'PUT',
              headers: utils.setApiHeaders('put'),
              body: JSON.stringify(arr)
          }).then(response => {
              dispatch(updateCardFinish())
              dispatch(getCards())
              return response.json();
          })
          .then((json) => {
              if(json.code == 200){
                notie.alert('success', json.message, 3);
              }else{
                utils.handleSessionError(json)
                notie.alert('error', json.message, 3);
              }

          })
          .catch(err => {
              throw err;
          });
      }
}

export function updateCardInit(){
    return {
      type: types.UPDATE_BILL_CARD_INIT
    }
}

export function updateCardFinish() {
  return {
    type: types.UPDATE_BILL_CARD
  }
}


export function changePayPopup(type){

  return {
    type: types.CHANGE_BILL_POPUP,
    popupScreen: type
  }
}

//for getting default
export function getDefaultCards() {
  return (dispatch) => {
    dispatch(getCardsInit());
    var me = this;


    fetch(Globals.API_ROOT_URL + `/billing/card?default=1`,{
      headers: utils.setApiHeaders('get')
    })
    .then(response =>{
      if (response.status == "500") {
           dispatch(getDefaultCardsErrors());
      }
      return response.json();
    }).then((json) => {
      if (typeof(json.data) !== "undefined") {
        dispatch(getDefaultCardsSuccess(json.data));
      }else if(json.code == 200 && typeof(json.message) !== "undefined"){
        dispatch(getDefaultCardsErrors());
      }else{
        utils.handleSessionError(json)
        notie.alert('error', 'Problem in fetching your cards', 3);
      }
    }).catch(err => {
      throw err;
    });
  }
}



export function getDefaultCardsSuccess(cardDetail) {
  return {
    type: types.FETCH_DEFAULT_BILL_CARD,
    cardDetail: cardDetail
  }
}

export function getDefaultCardsErrors() {
  return {
    type: types.FETCH_DEFAULT_BILL_CARD_ERROR
  }
}

export function payNow(payNowData){
    return (dispatch) => {
            notie.confirm(
              `Are you sure you want to pay the bill?`,
              'Yes',
              'No',
              function () {
                  dispatch(payNowAction(payNowData));
              },
              function () {  }
            )
            }

}

export function payNowAction(payNowData){
    return (dispatch) => {
          dispatch(payNowInit());
          var arr = {
            'card_identity': payNowData.card_identity,
            'amount': payNowData.amount,
          }
          fetch(Globals.API_ROOT_URL + `/billing/payment`, {
              method: 'POST',
              headers: utils.setApiHeaders('post'),
              body: JSON.stringify(arr)
          }).then(response => {
              if (response.status == "200") {
                 dispatch(paymentSuccess())

              }else{
                 dispatch(paymentError())
              }
              return response.json();
          })
          .then((json) => {
              if(json.code == 200){
                notie.alert('success', json.message, 3);
              }else{
                utils.handleSessionError(json)
                notie.alert('error', json.message, 3);
              }
          })
          .catch(err => {
              throw err;
          });
  }
}

export function payNowInit() {
  return {
    type: types.PAYMENT_INIT,
  }
}

export function paymentError() {
  return {
    type: types.PAYMENT_FAIL
  }
}

export function paymentSuccess() {
  return {
    type: types.PAYMENT_SUCCESS,
  }
}

//invoice

export function fetchInvoice() {

  return (dispatch) => {
    dispatch(fetchInvoiceInit());
    var me = this;

    fetch(Globals.API_ROOT_URL + `/billing/invoice/show`,{
      headers: utils.setApiHeaders('get')
    })
    .then(response =>{
      if (response.status != "200") {
           dispatch(fetchInvoiceError());
      }
      return response.json();
    }).then((json) => {
      if (typeof(json.data) !== "undefined") {
        dispatch(fetchInvoiceSuccess(json.data));
      }else if(json.code == 200 && typeof(json.message) !== "undefined"){
        dispatch(fetchInvoiceError());
      }else{
        utils.handleSessionError(json)
        notie.alert('error', 'Problem in fetching your invoice', 3);
      }
    }).catch(err => {
      throw err;
    });
  }
}



export function fetchInvoiceSuccess(invoiceDetail) {
  return {
    type: types.FETCH_INVOICE_LIST,
    invoiceDetail: invoiceDetail
  }
}

export function fetchInvoiceError() {
  return {
    type: types.FETCH_INVOICE_LIST_ERROR
  }
}

export function fetchInvoiceInit() {
  return {
    type: types.FETCH_INVOICE_INIT
  }
}

export function downloadInvoice(invoiceID) {

  return (dispatch) => {
    var me = this;

    fetch(Globals.API_ROOT_URL + `/billing/invoice/`+invoiceID,{
      headers: utils.setApiHeaders('get')
    })
    .then(response =>{
      
      return response.json();
    }).then((json) => {
      if(json.error!==undefined){
        utils.handleSessionError(json)
      }
    }).catch(err => {
      throw err;
    });
  }
}

export function checkDetailsOfBilling(){
  return (dispatch) => {
    dispatch(checkDetailsOfBillingInit());
    var me = this;

    fetch(Globals.API_ROOT_URL + `/billing/check-detail`,{
      headers: utils.setApiHeaders('get')
    })
    .then(response =>{
      if (response.status == "200") {
           dispatch(checkDetailsOfBillingSuccess());
      }else{
           dispatch(checkDetailsOfBillingFail());
      }
      return response.json();
    }).then((json) => {
      if(json.code !== 200) {
           notie.alert('warning', json.message, 3);
      }
    }).catch(err => {
      throw err;
    });
  }
}
export function checkDetailsOfBillingInit(){
    return {
      type: types.CHECK_BILLING_DETAIL_INIT
    }
}

export function checkDetailsOfBillingSuccess() {
  return {
    type: types.CHECK_BILLING_DETAIL_SUCCESS
  }
}

export function checkDetailsOfBillingFail() {
  return {
    type: types.CHECK_BILLING_DETAIL_FAIL
  }
}

export function getAddCardHostedPageUrl(){
    return (dispatch) => {
      dispatch(checkDetailsOfBillingInit());
      var me = this;

      fetch(Globals.API_ROOT_URL + `/payment/cards/add`,{
        headers: utils.setApiHeaders('get')
      })
      .then(response =>{
       
        return response.json();
      }).then((json) => {
        if (json.data.code == "200") {
             dispatch(getAddCardHostedPageUrlSuccess(json.data.hosted_link));
        }else{
          utils.handleSessionError(json)
             notie.alert('error', 'Problem in opening manage cards page', 3);
        }
    }).catch(err => {
      throw err;
    });
  } 
}

export function getAddCardHostedPageUrlSuccess(hostedLink){
     return {
      type: types.ADD_CARD_HOSTED_PAGE_LINK,
      hostedLink: hostedLink
  }
}

export function removeAddCardHostedPageUrl(){
    return {
      type: types.REMOVE_CARD_HOSTED_PAGE_LINK,
    }
}

export function getAllPlan(){
    return (dispatch) => {
        var me = this;

        fetch(Globals.API_ROOT_URL + `/billing/plan`,{
          headers: utils.setApiHeaders('get')
        })
        .then(response =>{
          if(response.status !== 200){
            notie.alert('error', 'Problem in fetching plans', 3);
          }
          return response.json();
        }).then((json) => {
          if(json.data!==undefined){
          dispatch(getAllPlanSuccess(json.data));
          }else{
            utils.handleSessionError(json)
          }
      }).catch(err => {
        throw err;
      });
    }
  }

  export function getAllPlanSuccess(planData){
     return {
      type: types.GET_ALL_PLAN,
      planData: planData
    }
  }


export function updatePlan(planValue){

    return (dispatch) => {
          dispatch(updatePlanInit());
          var arr = planValue
          fetch(Globals.API_ROOT_URL + `/payment/plan/update`, {
              method: 'POST',
              headers: utils.setApiHeaders('post'),
              body: JSON.stringify(arr)
          }).then(response => {
              if (response.status == "200") {
                 dispatch(updatePlanSuccess())
              }else{
                 dispatch(updatePlanError())
              }
              return response.json();
          })
          .then((json) => {
              if(json.data.code === 200){
                notie.alert('success', json.data.message, 3);
              }else{
                utils.handleSessionError(json)
                notie.alert('error', json.data.message, 3);
              }
          })
          .catch(err => {
              throw err;
          });
  }
}

export function updatePlanInit(){
    return {
      type: types.UPDATE_PLAN_INIT,
    }
}

export function updatePlanSuccess(){
    return {
      type: types.UPDATE_PLAN_SUCCESS,
    }
}

export function updatePlanError(){
    return {
      type: types.UPDATE_PLAN_ERROR,
    }
}



  
 
  


