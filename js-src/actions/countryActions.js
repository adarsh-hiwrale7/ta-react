import * as types from './actionTypes';
import Globals from '../Globals';
import {taBase64} from '../utils/utils';
import notie from "notie/dist/notie.js"

export function fetchAllCountryError(statusText) {
  return {type: types.ADD_DEPARTMENT_ERROR, text: statusText};
}

export function getAllCountry(data){
    return {
        type: types.FETCH_ALL_COUNTRY,
        country: data
    }
}
export function  getAllCountryError(){
  return{
    type: types.FETCH_ALL_COUNTRY_ERROR
  }
}

export function getRegionCountry(data){
    return {
        type: types.FETCH_REGION_COUNTRY,
        country: data
    }
}
export function  getRegionCountryError(){
  return{
    type: types.FETCH_REGION_COUNTRY_ERROR
  }
}
export function getStates(data,isCallFromSettings){
    return {
        type: types.FETCH_STATE,
        countrystate: data,
        isCallFromSettings
    }
}
export function getCities(data){
    return {
        type: types.FETCH_CITY,
        statecity: data
    }
}
export function getAllCountryInit(){
    return {
        type: types.FETCH_ALL_COUNTRY_INIT,
    }
}
export function fetchAllCountry(regionId = null,isRegion=false) {
    let regionUrl = '';
    if(isRegion && regionId !== null && regionId !== 0){
        regionUrl = '?region='+regionId
    }
    return (dispatch) => {
      dispatch(getAllCountryInit()); 
      fetch(Globals.API_ROOT_URL_MASTER  + `/address/country` + regionUrl)
      .then(response => response.json())
      .then((json) => {
        if (typeof(json.data) !== 'undefined') {
          if(regionId !== null){
            dispatch(getRegionCountry(json.data));  
          }else{
            dispatch(getAllCountry(json.data));  
          } 
        }
        else{
          if(regionId !== null){
            dispatch(getRegionCountryError());  
          }else{
            dispatch(getAllCountryError());  
          }  
        } 
      }).catch(err => {
        throw err;
      });
    }
  }
export function fetchCountryState(id,location=null) {
  var isCallFromSettings=false
  if(location !==null && location[1]!==undefined && location[1]=='settings'){
    isCallFromSettings=true
  }
  return (dispatch) => {
    var me = this;
    fetch(Globals.API_ROOT_URL_MASTER + `/address/state/${id}`)
    .then(response => response.json()).then((json) => {
      if (typeof(json.data) !== 'undefined') {
        dispatch(getStates(json.data,isCallFromSettings));
      }
    }).catch(err => {
      throw err;
    });
  }
}

export function fetchStateCity(id) {
    return (dispatch) => {
      var me = this;
      fetch(Globals.API_ROOT_URL_MASTER + `/address/city/${id}`)
      .then(response => response.json()).then((json) => {
        if (typeof(json.data) !== 'undefined') {
          dispatch(getCities(json.data));
        }
      }).catch(err => {
        throw err;
      });
    }
  }
  