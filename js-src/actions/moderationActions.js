import * as types from './actionTypes';
import Globals from '../Globals';
import * as utils from '../utils/utils';
import notie from 'notie/dist/notie.js'

export function handleUnapprovedFlag(){
    return {
        type: types.MODERATION_UNAPPROVEDFLAG,
       };
}

export function moderationSidebarAction(listingType) {
    return {
        type: types.MODERATION_SIDEBAR_ACTION,
        listingType
    };
}
export function successFetchAssets(){
    return{
        type:types.ASSET_FETCH_SUCCESS
    }
}
export function approveAssetInit(){
    return{
        type:types.APPROVE_ASSET_DATA
    }
}
export function approvePostInit(){
    return{
        type:types.APPROVE_POST_DATA
    }
}

export function approveAssetSuccess(){
    return{
        type:types.APPROVE_ASSET_SUCCESS
    }
}
export function CallFetchApiPostAssets(){
    return{
        type:types.CALL_FETCH_API_POST_ASSETS
    }
}
export function fetchfeedbackList(data) {
    return {
        type: types.FEEDBACK_LIST,
        data
    };
}
export function sendFeedbackSuccess(){
  return {
        type: types.SEND_FEEDBACK_SUCCESS,
    };
}
export function sendFeedbackInit(){
    return{
        type:types.SEND_FEEDBACK_INIT,
    }
}

export function feedbackLists (action_type) {
    return dispatch => {
      fetch(Globals.API_ROOT_URL + `/feedback?action=${action_type}`, {
        method: 'GET',
        headers: utils.setApiHeaders('get',{
            Accept: 'application/json',
            'Content-Type': 'application/json'
        })
        }).then(response => {
            if (response.status!= "200") {
            notie.alert('error', 'There is some problem to fetch feedback.', 3);
            }
            return response.json();
        }).then((json) => {
            utils.handleSessionError(json)
            dispatch(fetchfeedbackList(json.data));

        }).catch(err => {
            throw err;
        });
    }
  }

  export function sendUnapprovedFeedback (data) {
    return dispatch => {
      dispatch(sendFeedbackInit());
      fetch(Globals.API_ROOT_URL + `/asset/moderation`, {
        method: 'POST',
        headers: utils.setApiHeaders('post'),
        body: JSON.stringify(data)
        }).then(response => {
            return response.json();
        }).then((json) => {
            if(json.code==200)
            {
                notie.alert('success', json.message, 3);
                dispatch(sendFeedbackSuccess());
                document.body.classList.remove('overlay');
            }
            else {
                utils.handleSessionError(json)
                notie.alert('error', json.error.feedback, 3);
            }
        }).catch(err => {
            throw err;
        });
    }
  }
  export function sendUnapprovedFeedbackPost (data) {
    return dispatch => {
        dispatch(sendFeedbackInit());
      fetch(Globals.API_ROOT_URL + `/post/approve`, {
        method: 'POST',
        headers: utils.setApiHeaders('post'),
        body: JSON.stringify(data)
        }).then(response => {
            return response.json();
        }).then((json) => {
            if(json.code==200){
                notie.alert('success', json.message, 3);
                dispatch(sendFeedbackSuccess());
                document.body.classList.remove('overlay');
            }else{
                utils.handleSessionError(json)
                notie.alert('error', json.error.feedback ? json.error.feedback:json.error, 3);
            }
        }).catch(err => {
            throw err;
        });
    }
  }


// export function sendUnapprovedFeedback (data) {
//     return dispatch => {
//         dispatch(sendFeedbackSuccess());
//     }
// }
