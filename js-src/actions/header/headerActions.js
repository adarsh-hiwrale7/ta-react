import * as types from '../actionTypes';
import Globals from '../../Globals';

export function toggleFilter(filter_flag=true) {
  if(filter_flag)
  {
    return {
      type: types.ENABLE_FILTER_FLAG_ANALYTICS
    }
  }
  else
  {
    return {
      type: types.DISABLE_FILTER_FLAG_ANALYTICS
    }
  }
}

export function setFilterFlag(filter_flag){
  return{
    type : types.SET_FILTER_FLAG,
    flag : !filter_flag
  }
}
