import * as types from '../actionTypes';
import Globals from '../../Globals';
import notie from 'notie/dist/notie.js'
import * as utils from '../../utils/utils';
import {logout } from '../authActions';
var exportFlag
export function unsaveFilterAnalyticsInit(){
    return{
        type:types.UNSAVE_FILTER_ANALYTICS_INIT
    }
}
export function removeUnsaveFlag(){
    return {
        type:types.REMOVE_UNSAVE_FLAG
    }
}
export function saveAnalyticsFilterDataInit(){
    return {
        type: types.SAVE_ANALYTICS_FILTER_DATA_INIT
    }
}
export function fetchSavedAnalyticsFilterDataInit(){
    return {
        type: types.FETCH_SAVED_ANALYTICS_FILTER_DATA_INIT
    }
}
export function exportAnalyticsInit(){
    return {
        type: types.EXPORT_ANALYTICS_INIT
    }
}
export function fetchFeedAnalyticsInit() {
    return {
        type: types.FETCH_FEED_ANALYTICS_INIT
    }
}
export function fetchCampaignAnalyticsInit() {
    return {
        type: types.FETCH_CAMPAIGN_ANALYTICS_INIT
    }
}
export function fetchAssetAnalyticsInit() {
    return {
        type: types.FETCH_ASSET_ANALYTICS_INIT
    }
}

export function fetchFeedChartAnalyticsInit() {
    return {
        type: types.FETCH_FEED_CHART_ANALYTICS_INIT
    }
}
export function fetchCampaignChartAnalyticsInit() {
    return {
        type: types.FETCH_CAMPAIGN_CHART_ANALYTICS_INIT
    }
}
export function fetchAssetChartAnalyticsInit() {
    return {
        type: types.FETCH_ASSET_CHART_ANALYTICS_INIT
    }
}

export function ErrorfetchAssetAnalytics(){

    return {
        type: types.ERROR_FETCH_ASSET_ANALYTICS
    }
}

export function ErrorfetchAssetChartAnalytics(){

    return {
        type: types.ERROR_FETCH_ASSET_CHART_ANALYTICS
    }
}
export function ErrorfetchCampaignAnalytics(){

    return {
        type: types.ERROR_FETCH_CAMPAIGN_ANALYTICS
    }
}
export function ErrorfetchCampaignChartAnalytics(){
    return {
        type: types.ERROR_FETCH_CAMPAIGN_CHART_ANALYTICS
    }
}
export function ErrorfetchfeedAnalytics(){
    return{
        type:types.ERROR_FETCH_FEED_ANALYTICS
    }
}
export function ErrorfetchfeedChartAnalytics(){
    return{
        type:types.ERROR_FETCH_FEED_CHART_ANALYTICS
    }
}
export function ErrorExportAnalytics(message){
    notie.alert('error',message, 5);

    return{
        type:types.ERROR_EXPORT_ANALYTICS
    }
}
export function errorSaveAnalyticsFilterData(message){
    notie.alert('error',message , 5);

    return{
        type:types.ERROR_SAVE_ANALYTICS_FILTER_DATA
    }
}
export function errorFetchSavedFilterAnalytics(message){
    message?notie.alert('error',message, 5):''

    return{
        type:types.ERROR_FETCH_SAVED_FILTER_ANALYTICS
    }
}
export function errorUnsaveFilterAnalytics(message){
    notie.alert('error',message, 5);
    return{
        type:types.ERROR_UNSAVE_FILTER_ANALYTICS
    }
}
export function successUnsaveFilterAnalytics(message){
    message?notie.alert('success',message, 5):''
    return{
        type:types.SUCCESS_UNSAVE_FILTER_ANALYTICS
    }
}
export function clearUnsavedFilterData(){
    return{
        type:types.SUCCESS_UNSAVE_FILTER_ANALYTICS
    }
}
export function changeFlagOfsavedFilterAnalytics(){
    return{
        type:types.CHANGE_FLAG_OF_SAVED_FILTER_ANALYTICS
    }
}
export function successFetchSavedFilterAnalytics(data){
    return{
        type:types.SUCCESS_FETCH_SAVED_FILTER_ANALYTICS,
        data:data
    }
}
export function successSaveAnalyticsFilterData(jsondata){
    notie.alert('success',jsondata.message, 3);
    return{
        type:types.SUCCESS_SAVE_ANALYTICS_FILTER_DATA,
        data:jsondata.save_id
    }
}
export function successExportAnalytics(){
    return{
        type:types.SUCCESS_EXPORT_ANALYTICS
    }
}
export function successfetchFeedAnalytics(data){
    return {
        type: types.SUCCESS_FETCH_FEED_ANALYTICS,
        data: data
    }
}
export function successfetchFeedChartAnalytics(data){
    return {
        type: types.SUCCESS_FETCH_FEED_CHART_ANALYTICS,
        data: data
    }
}
export function successfetchAssetAnalytics(data){
    return {
        type: types.SUCCESS_FETCH_ASSET_ANALYTICS,
        data: data
    }
}
export function successfetchAssetChartAnalytics(data){
    return {
        type: types.SUCCESS_FETCH_ASSET_CHART_ANALYTICS,
        data: data
    }
}
export function successfetchCampaignAnalytics(data){
    return {
        type: types.SUCCESS_FETCH_CAMPAIGN_ANALYTICS,
        data: data
    }
}
export function successfetchCampaignChartAnalytics(data){
    return {
        type: types.SUCCESS_FETCH_CAMPAIGN_CHART_ANALYTICS,
        data: data
    }
}

/**
 * @author disha
 * get feed analytics data
 * @param {*} data
 */
export function fetchFeedAnalytics(data) {
    var apiString=Globals.API_ROOT_URL + `/analytics/feed?timezone=${data.timezone}`
    if(data.type!==undefined){
        apiString+=`&type=${data.type}`
    }
    if(data.tagtype!==undefined){
        apiString+=`&${data.tagtype}=${data.tagid}`
    }
    if(data.startDate!==undefined){
        apiString+=`&start_date=${data.startDate}`
    }
    if(data.endDate!==undefined){
        apiString+=`&end_date=${data.endDate}`
    }
    if(data.pageno!==undefined){
        apiString+=`&page=${data.pageno}`
    }
    if(data.timespan!==undefined){
        apiString+=`&timespan=${data.timespan}`
    }
    return (dispatch) => {
        dispatch(fetchFeedAnalyticsInit());
         fetch(apiString,{
            headers: utils.setApiHeaders('get'),
        })
        .then(response => response.json())
        .then((json) => {
                if(typeof json.data!=='undefined' && json.data.length == 0 ){
                    exportFlag = true
                }
                else{
                    exportFlag = false
                }
                if(typeof json.data!=='undefined' ) {
                    dispatch(successfetchFeedAnalytics(json));
                }else{
                    utils.handleSessionError(json)
                    dispatch(ErrorfetchfeedAnalytics())
                }
        })
        .catch(err => { throw err; });
    }
}
/**
 * @author disha
 * get Asset analytics data
 * @param {*} data
 */
export function fetchAssetAnalyticsPage(data) {
    var apiString=Globals.API_ROOT_URL + `/analytics/asset?timezone=${data.timezone}`
    if(data.tagtype!==''){
        apiString+=`&${data.tagtype}=${data.tagid}`
    }
    if(data.startDate!==''){
        apiString+=`&start_date=${data.startDate}`
    }
    if(data.endDate!==''){
        apiString+=`&end_date=${data.endDate}`
    }
    if(data.type!==''){
        apiString+=`&type=${data.type}`
    }
    if(data.pageno!==''){
        apiString+=`&page=${data.pageno}`
    }
    return (dispatch) => {
        dispatch(fetchAssetAnalyticsInit());
      fetch(apiString,{
        headers: utils.setApiHeaders('get'),
    })
        .then(response => response.json())
        .then((json) => {
                if(typeof json.data!=='undefined' && json.data.length===0){
                    exportFlag = true
                }
                else{
                    exportFlag = false
                }
                if(typeof json.data!=='undefined' ) {
                    dispatch(successfetchAssetAnalytics(json));
                }else{
                    utils.handleSessionError(json)
                    dispatch(ErrorfetchAssetAnalytics());
                }
        })
        .catch(err => { throw err; });
    }
}

/**
 * @author disha
 *get campaign analytics data
 * @param {*} data
 */
export function fetchCampaignAnalyticsPage(data) {
    var apiString=Globals.API_ROOT_URL + `/analytics/campaigns?timezone=${data.timezone}`
    if(data.tagtype!==''){
        apiString+=`&${data.tagtype}=${data.tagid}`
    }
    if(data.startDate!==''){
        apiString+=`&start_date=${data.startDate}`
    }
    if(data.endDate!==''){
        apiString+=`&end_date=${data.endDate}`
    }
    if(data.type!==''){
        apiString+=`&type=${data.type}`
    }
    if(data.socialmedia!==''){
        apiString+=`&social_media=${data.socialmedia}`
    }
    if(data.campStatus!==''){
        apiString+=`&campaign_status=${data.campStatus}`
    }
    if(data.pageno!==''){
        apiString+=`&page=${data.pageno}`
    }
    return (dispatch) => {
        dispatch(fetchCampaignAnalyticsInit());
      fetch(apiString,{
            headers: utils.setApiHeaders('get'),
        })
        .then(response => response.json())
        .then((json) => {
                if( typeof json.data!=='undefined'  && json.data.length===0){
                    exportFlag = true
                }
                else{
                    exportFlag = false
                }
                if(typeof json.data!=='undefined' ) {
                    dispatch(successfetchCampaignAnalytics(json));
                }else{
                    utils.handleSessionError(json)
                    dispatch(ErrorfetchCampaignAnalytics());
                }
        })
        .catch(err => { throw err; });
    }
}

/**
 * @author disha
 * get feed chart analytics data
 * @param {*} data
 */
export function fetchFeedChartAnalyticsPage(data,flag) {
    var apiString=Globals.API_ROOT_URL + `/analytics/feed-chart?limit=all&timezone=${data.timezone}`
    if(data.type!==''){
        apiString+=`&type=${data.type}`
    }
    if(data.tagtype!==''){
        apiString+=`&${data.tagtype}=${data.tagid}`
    }
    if(data.startDate!==''){
        apiString+=`&start_date=${data.startDate}`
    }
    if(data.endDate!==''){
        apiString+=`&end_date=${data.endDate}`
    }
    return (dispatch) => {
        dispatch(fetchFeedChartAnalyticsInit());
      fetch(apiString,{
            headers: utils.setApiHeaders('get'),
        })
        .then(response => response.json())
        .then((json) => {
                if(typeof json.data!=='undefined' ) {
                    dispatch(successfetchFeedChartAnalytics(json.data));
                }else{
                    utils.handleSessionError(json)
                    dispatch(ErrorfetchfeedChartAnalytics())
            }
        })
        .catch(err => { throw err; });
    }
}
/**
 * get Asset chart analytics data
 * @author disha
 * @param {*} data
 */
export function fetchAssetChartAnalyticsPage(data) {
    var apiString=Globals.API_ROOT_URL + `/analytics/asset-chart?limit=all&timezone=${data.timezone}`
    if(data.tagtype!==''){
        apiString+=`&${data.tagtype}=${data.tagid}`
    }
    if(data.startDate!==''){
        apiString+=`&start_date=${data.startDate}`
    }
    if(data.endDate!==''){
        apiString+=`&end_date=${data.endDate}`
    }
    if(data.type!==''){
        apiString+=`&type=${data.type}`
    }
    return (dispatch) => {
        dispatch(fetchAssetChartAnalyticsInit());
      fetch(apiString,{
            headers: utils.setApiHeaders('get'),
        })
        .then(response => response.json())
        .then((json) => {
                if(typeof json.data!=='undefined' ) {
                    dispatch(successfetchAssetChartAnalytics(json.data));
                }else{
                    utils.handleSessionError(json)
                    dispatch(ErrorfetchAssetChartAnalytics());
                }
        })
        .catch(err => { throw err; });
    }
}

/**
 * get campaign chart analytics data
 * @author disha
 * @param {*} data
 */
export function fetchCampaignChartAnalyticsPage(data) {
    var apiString=Globals.API_ROOT_URL + `/analytics/campaign-chart?limit=all&timezone=${data.timezone}`
    if(data.tagtype!==''){
        apiString+=`&${data.tagtype}=${data.tagid}`
    }
    if(data.startDate!==''){
        apiString+=`&start_date=${data.startDate}`
    }
    if(data.endDate!==''){
        apiString+=`&end_date=${data.endDate}`
    }
    if(data.type!==''){
        apiString+=`&type=${data.type}`
    }
    if(data.socialmedia!==''){
        apiString+=`&social_media=${data.socialmedia}`
    }
    if(data.campStatus!==''){
        apiString+=`&campaign_status=${data.campStatus}`
    }
    return (dispatch) => {
        dispatch(fetchCampaignChartAnalyticsInit());
      fetch(apiString,{
            headers: utils.setApiHeaders('get'),
        })
        .then(response => response.json())
        .then((json) => {
                if(typeof json.data!=='undefined' ) {
                    dispatch(successfetchCampaignChartAnalytics(json.data));
                }else{
                    utils.handleSessionError(json)
                    dispatch(ErrorfetchCampaignChartAnalytics());
                }
        })
        .catch(err => { throw err; });
    }
}

/**
 * @author disha
 * export analytics data for feed , asset, campaign
 * @param {*} data
 */
export function exportAnalytics(data) {
   var encryptedSessionId=utils.encodeSessionId(Globals.SESSION_ID);
    var apiString=Globals.API_ROOT_URL + `/analytics/csvAnalytics?csv_type=${data.analyticsType}&limit=all`
    if(data.tagtype!==''){
        apiString+=`&${data.tagtype}=${data.tagid}`
    }
    if(data.type!==''){
        apiString+=`&type=${data.type}`
    }
    if(data.startDate!==''){
        apiString+=`&start_date=${data.startDate}`
    }
    if(data.endDate!==''){
        apiString+=`&end_date=${data.endDate}`
    }
    if(data.socialmedia!=='' && typeof data.socialmedia!=='undefined'){
        apiString+=`&social_media=${data.socialmedia}`
    }
    if(data.campStatus!=='' && typeof data.campStatus!=='undefined'){
        apiString+=`&campaign_status=${data.campStatus}`
    }
    return (dispatch) => {
        dispatch(exportAnalyticsInit());
        fetch(apiString,{
            headers: utils.setApiHeaders('get',{
                'Access-Control-Allow-Origin':'*'
            })
        })
        .then((json) => {
            //this condition is for checking whether there is data after filter or not
            if(exportFlag == true){
                let message = 'No content found to download'
                dispatch(ErrorExportAnalytics(message));
            }
            else{
                if(json.code !== undefined ){
                    utils.handleSessionError(json)
                    dispatch(ErrorExportAnalytics(json.message));
                }else{
                    window.location.assign(apiString+`&session_id=${encryptedSessionId}`);  //to download api data
                    dispatch(successExportAnalytics());
                }
            }
        })
        .catch(err => { throw err; });
    }
}

/**
 * @author disha
 * to save filter data of analytics
 * @param {*} values
 */
export function saveAnalyticsFilterData(values) {
    return dispatch => {
        dispatch(saveAnalyticsFilterDataInit());
        fetch(`${Globals.API_ROOT_URL}/analytics/save `, {
          method: 'POST',
          headers: utils.setApiHeaders('post'),
          body: JSON.stringify(values)
        })
          .then(response => {
            return response.json()
          })
          .then(json => {
            if (json.code == 200) {
                dispatch(successSaveAnalyticsFilterData(json))
            }else{
                utils.handleSessionError(json)
                dispatch(errorSaveAnalyticsFilterData(json.message))
            }

          })
          .catch(err => {
            throw err
          })
      }
}

/**
 * @author disha
 * to fetch saved filter data from api
 * @param {*} userid
 * @param {*} pagetype feed/campaign/asset
 */
export function fetchSavedAnalyticsFilterData(userid,pagetype) {
    return (dispatch) => {
        dispatch(fetchSavedAnalyticsFilterDataInit());
        fetch(Globals.API_ROOT_URL + `/analytics/save/${userid}?type=${pagetype}`,{
            method: 'GET',
            headers: utils.setApiHeaders('get'),
        })
        .then(response => response.json())
        .then((json) => {
            if(json.code=='200'){
                if(typeof json.data!=='undefined'){
                    if(json.data.length>0){
                        dispatch(successFetchSavedFilterAnalytics(json.data));
                    }
                }else{
                    dispatch(errorFetchSavedFilterAnalytics());
                }
            }else{
                utils.handleSessionError(json)
                dispatch(errorFetchSavedFilterAnalytics(json.message));
            }
        })
        .catch(err => { throw err; });
    }
}
/**
 * @author disha
 * to remove filter data from api
 * @param {*} userid
 * @param {*} pagetype
 */
export function unsaveFilterAnalytics(userid,pagetype) {
    return (dispatch) => {
        dispatch(unsaveFilterAnalyticsInit());
        fetch(`${Globals.API_ROOT_URL}/analytics/delete?type=${pagetype}&user_id=${userid}`, {
            method: 'DELETE',
            headers: utils.setApiHeaders('delete'),
          })
        .then(response => response.json())
        .then((json) => {
            if(json.code==200){
                dispatch(successUnsaveFilterAnalytics(json.message));
            }else{
                utils.handleSessionError(json)
                dispatch(errorUnsaveFilterAnalytics(json.message));
            }
        })
        .catch(err => { throw err; });
    }
}

/**
 * @author kinjal
 * to remove filter data from api
 * @param {*} id : in this method pass the campaing id then give the analyitcs data of campaing popup
 */

export function campaignAnalyticsPopup(id) {
    return dispatch => {
        dispatch(campaignAnalyticsPopupInit());
        fetch(`${Globals.API_ROOT_URL}/analytics/campaignSocial?campaign_id=${id} `, {
          method: 'GET',
          headers: utils.setApiHeaders('get'),
        })
          .then(response => {
            return response.json()
          })
          .then(json => {
            if (json.code == 200) {
                dispatch(successCampaignAnalyticsPopup(json.data))
            }else{
                utils.handleSessionError(json)
            }
         })
          .catch(err => {
            throw err
          })
      }
}
export function  campaignAnalyticsPopupInit(){
    return{
        type:types.CAMPAIGN_ANALYTICS_POPUP_INIT
    }
}
export function  successCampaignAnalyticsPopup(data){
    return{
        type:types.CAMPAIGN_ANALYTICS_POPUP_SUCCESS,
        campaignPopupData:data
    }
}

export function  fetchAdoptionDataInit(){
    return{
        type:types.FETCH_ADOPTION_DATA_INIT
    }
}
export function  fetchAdoptionDataSuccess(data){
    return{
        type:types.FETCH_ADOPTION_DATA_SUCCESS,
        data:data
    }
}
export function  fetchAdoptionDataError(message){
    notie.alert('error',message, 3);
    return{
        type:types.FETCH_ADOPTION_DATA_ERROR
    }
}
export function modifyAdoptionData(data){
    return{
        type:types.MODIFY_ADOPTION_DATA_SUCCESS,
        data:data
    }
}
export function fetchAdoptionData(data) {
    var apiString=Globals.API_ROOT_URL + `/analytics/adoption`
    //  timespan=week&start_date=187873674676&end_time=27147673957
    return (dispatch) => {
        dispatch(fetchAdoptionDataInit());
      fetch(apiString,{
            headers: utils.setApiHeaders('post'),
            method: 'POST',
            body: JSON.stringify(data)
          })
        .then(response => response.json())
        .then((json) => {
            if(json.code==200){
                if(typeof json.data!=='undefined' ) {
                    dispatch(fetchAdoptionDataSuccess(json.data));
                }
            }else{
                utils.handleSessionError(json)
                dispatch(fetchAdoptionDataError(json.error));
            }
        })
        .catch(err => { throw err; });
    }
}
// ================
export function  fetchAssetAnalyticsDataInit(){
    return{
        type:types.FETCH_ASSET_ANALYTICS_DATA_INIT
    }
}
export function  fetchAssetAnalyticsDataSuccess(data){
    return{
        type:types.FETCH_ASSET_ANALYTICS_DATA_SUCCESS,
        data:data
    }
}
export function  fetchAssetAnalyticsDataError(data){
    return{
        type:types.FETCH_ASSET_ANALYTICS_DATA_ERROR
    }
}
export function fetchAssetAnalyticsData(data) {
    var apiString=Globals.API_ROOT_URL + `/analytics/assets`
    // pass timespan=week&start_date=187873674676&end_time=27147673957 
    return (dispatch) => {
        dispatch(fetchAssetAnalyticsDataInit());
      fetch(apiString,{
                headers: utils.setApiHeaders('post'),
                method: 'POST',
                body: JSON.stringify(data)
            })
        .then(response => response.json())
        .then((json) => {
            if(json.code==200){
                if(typeof json.data!=='undefined' ) {
                    dispatch(fetchAssetAnalyticsDataSuccess(json.data));
                }
            }else{
                utils.handleSessionError(json)
                dispatch(fetchAssetAnalyticsDataError());
            }
        })
        .catch(err => { throw err; });
    }
}

export function  fetchQualityAnalyticsDataInit(){
    return{
        type:types.FETCH_QUALITY_ANALYTICS_DATA_INIT
    }
}
export function  fetchQualityAnalyticsDataSuccess(data){
    return{
        type:types.FETCH_QUALITY_ANALYTICS_DATA_SUCCESS,
        data:data
    }
}
export function  fetchQualityAnalyticsDataError(data){
    return{
        type:types.FETCH_QUALITY_ANALYTICS_DATA_ERROR
    }
}
export function fetchQualityAnalyticsData(data) {
    var apiString=Globals.API_ROOT_URL + `/analytics/approval`
    // // pass timespan=week&start_date=187873674676&end_time=27147673957
    return (dispatch) => {
        dispatch(fetchQualityAnalyticsDataInit());
      fetch(apiString,{
            headers: utils.setApiHeaders('post'),
            method: 'POST',
            body: JSON.stringify(data)
        })
        .then(response => response.json())
        .then((json) => {
            if(json.code==200){
                if(typeof json.data!=='undefined' ) {
                    dispatch(fetchQualityAnalyticsDataSuccess(json.data));
                }
            }else{
                    utils.handleSessionError(json)
                    dispatch(fetchQualityAnalyticsDataError());
                }
        })
        .catch(err => { throw err; });
    }
}

// ================
export function  fetchInternalEngagementDataInit(){
    return{
        type:types.FETCH_INTERNAL_ENGAGEMENT_DATA_INIT
    }
}
export function  fetchInternalEngagementDataSuccess(data){
    return{
        type:types.FETCH_INTERNAL_ENGAGEMENT_DATA_SUCCESS,
        data:data
    }
}
export function  fetchInternalEngagementDataError(data){
    return{
        type:types.FETCH_INTERNAL_ENGAGEMENT_DATA_ERROR
    }
}
export function fetchInternalEngagementData(data) {
    // timespan=week&start_date=187873674676&end_time=27147673957
    var apiString=Globals.API_ROOT_URL + `/analytics/engagement`
    return (dispatch) => {
        dispatch(fetchInternalEngagementDataInit());
      fetch(apiString,{
                headers: utils.setApiHeaders('post'),
                method: 'POST',
                body: JSON.stringify(data)
            })
        .then(response => response.json())
        .then((json) => {
            if(json.code==200){
                if(typeof json.data!=='undefined' ) {
                    dispatch(fetchInternalEngagementDataSuccess(json.data));
                }
            }else{
                utils.handleSessionError(json)
                dispatch(fetchInternalEngagementDataError());
            }
        })
        .catch(err => { throw err; });
    }
}

// ================
export function  fetchTagCloudDataInit(){
    return{
        type:types.FETCH_TAG_CLOUD_DATA_INIT
    }
}
export function  fetchTagCloudDataSuccess(data){
    return{
        type:types.FETCH_TAG_CLOUD_DATA_SUCCESS,
        data:data
    }
}
export function  fetchTagCloudDataError(){
    return{
        type:types.FETCH_TAG_CLOUD_DATA_ERROR
    }
}
export function fetchTagCloudData(data) {
    // timespan=week&start_date=187873674676&end_time=27147673957
    var apiString=Globals.API_ROOT_URL + `/analytics/tag-list`
    return (dispatch) => {
        dispatch(fetchTagCloudDataInit());
      fetch(apiString,{
                headers: utils.setApiHeaders('post'),
                method: 'POST',
                body: JSON.stringify(data)
            })
        .then(response => response.json())
        .then((json) => {
            if(json.code==200){
                if(typeof json.data!=='undefined' ) {
                    dispatch(fetchTagCloudDataSuccess(json.data));
                }else{
                    dispatch(fetchTagCloudDataError());
                }
            }else{
                utils.handleSessionError(json)
                dispatch(fetchTagCloudDataError());
            }
        })
        .catch(err => { throw err; });
    }
}
export function  fetchTotalPostsChartDataInit(){
    return{
        type:types.FETCH_TOTAL_POSTS_CHARTS_DATA_INIT
    }
}
export function  fetchTotalPostsChartDataSuccess(data){
    return{
        type:types.FETCH_TOTAL_POSTS_CHARTS_DATA_SUCCESS,
        data:data
    }
}
export function  fetchTotalPostsChartDataError(){
    return{
        type:types.FETCH_TOTAL_POSTS_CHARTS_DATA_ERROR
    }
}
export function modifyTotalPostsChartData(data){
    return{
        type:types.MODIFY_TOTAL_POSTS_CHARTS_DATA_SUCCESS,
        data:data
    }
}
export function fetchTotalPostsChartData(data) {
    // timespan=week&start_date=187873674676&end_time=27147673957
    var apiString=Globals.API_ROOT_URL + `/analytics/posts`
    return (dispatch) => {
        dispatch(fetchTotalPostsChartDataInit());
      fetch(apiString,{
            headers: utils.setApiHeaders('post'),
            method: 'POST',
            body: JSON.stringify(data)
            })
        .then(response => response.json())
        .then((json) => {
            if(json.code==200){
                if(typeof json.data!=='undefined' ) {
                    dispatch(fetchTotalPostsChartDataSuccess(json.data));
                }
            }else{
                utils.handleSessionError(json)
                dispatch(fetchTotalPostsChartDataError());
            }
        })
        .catch(err => { throw err; });
    }
}
export function  fetchTotalSocialEngagementInit(){
    return{
        type:types.FETCH_TOTAL_SOCIAL_ENGAGEMENT_INIT
    }
}
export function  fetchTotalSocialEngagementSuccess(data){
    return{
        type:types.FETCH_TOTAL_SOCIAL_ENGAGEMENT_SUCCESS,
        data:data
    }
}
export function  fetchTotalSocialEngagementError(){
    return{
        type:types.FETCH_TOTAL_SOCIAL_ENGAGEMENT_ERROR
    }
}
export function fetchTotalSocialEngagementData(data) {
    // timespan=week&start_date=187873674676&end_time=27147673957
    //timespan=today&department_identity=BymMA&timezone=Asia/kolkata
    var apiString=Globals.API_ROOT_URL + `/analytics/social-engagement`
    return (dispatch) => {
        dispatch(fetchTotalSocialEngagementInit());
      fetch(apiString,{
            headers: utils.setApiHeaders('post'),
            method: 'POST',
            body: JSON.stringify(data)
        })
        .then(response => response.json())
        .then((json) => {
            if(json.code==200){
                if(typeof json.data!=='undefined' ) {
                    dispatch(fetchTotalSocialEngagementSuccess(json.data));
                }
            }else{
                utils.handleSessionError(json)
                dispatch(fetchTotalSocialEngagementError());
            }
        })
        .catch(err => { throw err; });
    }
}

export function  fetchLocationBaseAdoptionInit(){
    return{
        type:types.FETCH_LOCATION_BASE_ADOPTION_INIT
    }
}
export function  fetchLocationBaseAdoptionSuccess(data){
    return{
        type:types.FETCH_LOCATION_BASE_ADOPTION_SUCCESS,
        data:data
    }
}
export function  fetchLocationBaseAdoptionError(){
    return{
        type:types.FETCH_LOCATION_BASE_ADOPTION_ERROR
    }
}
export function fetchLocationBaseAdoption(data) {
    // timespan=week&start_date=187873674676&end_time=27147673957
    var apiString=Globals.API_ROOT_URL + `/analytics/heat-map`
    if(data.timespan !== undefined){
        apiString+=`?timespan=${data.timespan}`
    }
    return (dispatch) => {
        dispatch(fetchLocationBaseAdoptionInit());
      fetch(apiString,{
            headers: utils.setApiHeaders('get'),
        })
        .then(response => response.json())
        .then((json) => {
            // var json={'code':200,data:[{name: 'axis00', size: 12 },{name: 'controls',size: 328 },{name: 'data', size: 24 },{name: 'events', size: 73 },{name: 'legend', size: 129 },{name: 'operator',size: 401 }]};
            if(json.code==200){
                if(typeof json.data!=='undefined' ) {
                    dispatch(fetchLocationBaseAdoptionSuccess(json.data));
                }
            }else{
                utils.handleSessionError(json)
                dispatch(fetchLocationBaseAdoptionError());
            }
        })
        .catch(err => { throw err; });
    }
}

export function  fetchGuestUserChartInit(){
    return{
        type:types.FETCH_GUEST_USER_CHART_INIT
    }
}
export function  fetchGuestUserChartSuccess(data){
    return{
        type:types.FETCH_GUEST_USER_CHART_SUCCESS,
        data:data
    }
}
export function  fetchGuestUserChartError(){
    return{
        type:types.FETCH_GUEST_USER_CHART_ERROR
    }
}
export function fetchGuestUserChart(data) {
    // timespan=week&start_date=187873674676&end_time=27147673957
    var apiString=Globals.API_ROOT_URL + `/analytics/guest`
    if(data.timespan !== undefined){
        apiString+=`?timespan=${data.timespan}`
    }
    return (dispatch) => {
        dispatch(fetchGuestUserChartInit());
      fetch(apiString,{
            headers: utils.setApiHeaders('get'),
        })
        .then(response => response.json())
        .then((json) => {
            // var json={'code':200,data:[{name: 'axis00', size: 12 },{name: 'controls',size: 328 },{name: 'data', size: 24 },{name: 'events', size: 73 },{name: 'legend', size: 129 },{name: 'operator',size: 401 }]};
            if(json.code==200){
                if(typeof json.data!=='undefined' ) {
                    dispatch(fetchGuestUserChartSuccess(json.data));
                }
            }else{
                utils.handleSessionError(json)
                dispatch(fetchGuestUserChartError());
            }
        })
        .catch(err => { throw err; });
    }
}
export function  fetchGuestUserActivityInit(){
    return{
        type:types.FETCH_GUEST_USER_ACTIVITY_INIT
    }
}
export function  fetchGuestUserActivitySuccess(data){
    return{
        type:types.FETCH_GUEST_USER_ACTIVITY_SUCCESS,
        data:data
    }
}
export function  fetchGuestUserActivityError(){
    return{
        type:types.FETCH_GUEST_USER_ACTIVITY_ERROR
    }
}
export function fetchGuestUserActivity(data) {
    // timespan=week&start_date=187873674676&end_time=27147673957
    var apiString=Globals.API_ROOT_URL + `/analytics/activity`
    if(data.timespan !== undefined){
        apiString+=`?timespan=${data.timespan}`
    }
    return (dispatch) => {
        dispatch(fetchGuestUserActivityInit());
      fetch(apiString,{
            headers: utils.setApiHeaders('get'),
        })
        .then(response => response.json())
        .then((json) => {
            // var json={'code':200,data:[{name: 'axis00', size: 12 },{name: 'controls',size: 328 },{name: 'data', size: 24 },{name: 'events', size: 73 },{name: 'legend', size: 129 },{name: 'operator',size: 401 }]};
            if(json.code==200){
                if(typeof json.data!=='undefined' ) {
                    dispatch(fetchGuestUserActivitySuccess(json.data));
                }
            }else{
                utils.handleSessionError(json)
                dispatch(fetchGuestUserActivityError());
            }
        })
        .catch(err => { throw err; });
    }
}
export function fetchEngagementByChannelInit(){
    return{
        type:types.FETCH_ENGAGEMENT_BYCHANNEL_INIT
    }
}
export function  fetchEngagementByChannelSuccess(data){
    return{
        type:types.FETCH_ENGAGEMENT_BYCHANNEL_SUCCESS,
        data:data
    }
}
export function  fetchEngagementByChannelError(){
    return{
        type:types.FETCH_ENGAGEMENT_BYCHANNEL_ERROR
    }
}
export function fetchEngagementByChannel(data) {
    // timespan=week&start_date=187873674676&end_time=27147673957
    //timespan=today&timezone=${'Asia/kolkata'}
    var apiString=Globals.API_ROOT_URL + `/analytics/engagement-channel`
    return (dispatch) => {
        dispatch(fetchEngagementByChannelInit());
      fetch(apiString,{
            headers: utils.setApiHeaders('post'),
            method: 'POST',
            body: JSON.stringify(data)
        })
        .then(response => response.json())
        .then((json) => {
            if(json.code==200){
                if(typeof json.data!=='undefined' ) {
                    dispatch(fetchEngagementByChannelSuccess(json.data));
                }
            }else{
                utils.handleSessionError(json)
                dispatch(fetchEngagementByChannelError());
            }
        })
        .catch(err => { throw err; });
    }
}

export function fetchEngagementOverTimeInit(){
    return{
        type:types.FETCH_ENGAGEMENT_OVERTIME_INIT
    }
}
export function  fetchEngagementOverTimeSuccess(data){
    return{
        type:types.FETCH_ENGAGEMENT_OVERTIME_SUCCESS,
        data:data
    }
}
export function  fetchEngagementOverTimeError(){
    return{
        type:types.FETCH_ENGAGEMENT_OVERTIME_ERROR
    }
}
export function fetchEngagementOverTime(data) {
    // timespan=week&start_date=187873674676&end_time=27147673957
    //timespan=today&timezone=${'Asia/kolkata'}
    var apiString=Globals.API_ROOT_URL + `/analytics/engagement-overtime`
    return (dispatch) => {
        dispatch(fetchEngagementOverTimeInit());
      fetch(apiString,{
            headers: utils.setApiHeaders('post'),
            method: 'POST',
            body: JSON.stringify(data)
        })
        .then(response => response.json())
        .then((json) => {
            if(json.code==200){
                if(typeof json.data!=='undefined' ) {
                    dispatch(fetchEngagementOverTimeSuccess(json.data));
                }
            }else{
                utils.handleSessionError(json)
                dispatch(fetchEngagementOverTimeError());
            }
        })
        .catch(err => { throw err; });
    }
}

export function getPostByNetwork(data){

    var apiString=Globals.API_ROOT_URL + `/analytics/posts?expand_view=1`
    if(data.timespan !== undefined){
        apiString+=`&timespan=${data.timespan}`
    }
    
    return (dispatch) => {
      dispatch(getPostByNetworkInit());
      fetch(apiString,{
            headers: utils.setApiHeaders('post'),
            method: 'POST',
            body: JSON.stringify(data)
          })
        .then(response => response.json())
        .then((json) => {
            if(json.code==200){
                if(typeof json.data!=='undefined' ) {
                    dispatch(getPostByNetworkSuccess(json.data));
                }
            }else{
                utils.handleSessionError(json)
                dispatch(getPostByNetworkError());
            }
        })
        .catch(err => { throw err; });
    }
}

export function getPostByNetworkInit(){
    return{
        type:types.FETCH_POST_BY_NETWORK_INIT
    }
}

export function getPostByNetworkSuccess(data){
    return{
        type:types.FETCH_POST_BY_NETWORK_SUCCESS,
        data: data
    }

}
export function selectedFilterValues(data){
    return{
        type:types.SELECTED_FILTER_VALUES,
        data: data
    }
}
export function selectedFilterDetail(data){
    return{
        type:types.SELECTED_FILTER_DETAIL,
        data: data
    }
}
export function resetSelectedFilterValues(data){
    return{
        type:types.RESET_SELECTED_FILTER_DETAIL,
        data:data
    }
}
export function resetAllFilterValues(){
    return{
        type:types.RESET_ALL_FILTER_VALUES
    }
}
export function clearSelectedFilterData(data){
    return{
        type:types.CLEAR_SELECTED_FILTER_DATA,
        data:data
    }
}
/**
 * @author disha
 * @param {*} timespan 
 * get list of content v enagement data
 */
export function getContentVEngageData(timespan){

    var apiString=Globals.API_ROOT_URL + `/analytics/culture/graph`
    if(timespan !== undefined){
        apiString+=`?timespan=${timespan}`
    }
    return (dispatch) => {
      dispatch(getContentVEngageDataInit());
      fetch(apiString,{
            headers: utils.setApiHeaders('get'),
          })
        .then(response => response.json())
        .then((json) => {
            if(json.code==200){
                if(typeof json.data!=='undefined' ) {
                    dispatch(getContentVEngageDataSuccess(json.data));
                }
            }else{
                notie.alert('error',json.error, 5);
                utils.handleSessionError(json)
                dispatch(getContentVEngageDataError());
            }
        })
        .catch(err => { throw err; });
    }
}

export function getContentVEngageDataInit(){
    return{
        type:types.GET_CONTENT_V_ENGAGE_DATA_INIT
    }
}
export function getContentVEngageDataSuccess(data){
    return{
        type:types.GET_CONTENT_V_ENGAGE_DATA_SUCCESS,
        data: data
    }
}
export function getContentVEngageDataError(data){
    return{
        type:types.GET_CONTENT_V_ENGAGE_DATA_ERROR,
        data: data
    }
}
/**
 * @auhot disha
 * fetch detail of scatter chart
 * @param {*} data 
 */
export function fetchContentGraphDetail(data){
    return (dispatch) => {
      dispatch(fetchContentGraphDetailInit());
      fetch(Globals.API_ROOT_URL + `/analytics/culture/contentGraph`,{
            method: 'POST',
            headers: utils.setApiHeaders('post'),
            body: JSON.stringify(data)
        })
        .then(response => response.json())
        .then((json) => {
            if(json.code==200){
                if(typeof json.data!=='undefined' ) {
                    dispatch(fetchContentGraphDetailSuccess(json.data));
                }
            }else{
                let error = json.error
                if (typeof error.id !== 'undefined') {
                    notie.alert('error',error.id[0], 5);
                } else if (typeof error.type !== 'undefined') {
                    notie.alert('error',error.type[0], 5);
                }else{
                    notie.alert('error',error,3)
                }
                utils.handleSessionError(json)
                dispatch(fetchContentGraphDetailError());
            }
        })
        .catch(err => { throw err; });
    }
}

export function fetchContentGraphDetailInit(){
    return{
        type:types.FETCH_CONTENT_GRAPH_DETAIL_INIT
    }
}
export function fetchContentGraphDetailSuccess(data){
    return{
        type:types.FETCH_CONTENT_GRAPH_DETAIL_SUCCESS,
        data: data
    }
}
export function fetchContentGraphDetailError(){
    return{
        type:types.FETCH_CONTENT_GRAPH_DETAIL_ERROR,
    }
}
/**
 * @auhot disha
 * fetch detail of scatter chart
 * @param {*} data 
 */
export function fetchSelectedEmailPrint(type,id){
    return (dispatch) => {
      dispatch(fetchSelectedPrintEmailInit());
      fetch(Globals.API_ROOT_URL + `/analytics/culture/graph/${type}Data?${type}_identity=${id}`,{
            headers: utils.setApiHeaders('GET')
        })
        .then(response => response.json())
        .then((json) => {
            if(json.code==200){
                if(typeof json.data!=='undefined' ) {
                    dispatch(fetchSelectedPrintEmailSuccess(json.data));
                }
            }else{
                let error = json.error
                    notie.alert('error',error,3)
                utils.handleSessionError(json)
                dispatch(fetchSelectedPrintEmailError());
            }
        })
        .catch(err => { throw err; });
    }
}

export function fetchSelectedPrintEmailInit(){
    return{
        type:types.FETCH_SELECTED_PRINT_EMAIL_INIT
    }
}
export function fetchSelectedPrintEmailSuccess(data){
    return{
        type:types.FETCH_SELECTED_PRINT_EMAIL_SUCCESS,
        data: data
    }
}
export function fetchSelectedPrintEmailError(){
    return{
        type:types.FETCH_SELECTED_PRINT_EMAIL_ERROR,
    }
}