import * as types from '../actionTypes';
import Globals from '../../Globals';
import * as utils from '../../utils/utils';
import notie from "notie/dist/notie.js";

/**
To get the overall NPS score 
@author Akshay
@param timezone
@type GET
**/
export function getOverAllNPS(timezone) {
    return dispatch => {
        dispatch(getOverAllNPSInit());
        fetch(`${Globals.API_ROOT_URL}/analytics/funnel/nps`, {
            method: 'GET',
            headers: utils.setApiHeaders('get'),
        })
            .then(response => {
                return response.json()
            })
            .then(json => {
                if (json.code == 200) {
                    dispatch(getOverAllNPSSuccess(json.data));
                } else {
                    dispatch(getOverAllNPSError());
                    notie.alert('error', 'Problem in fetching culture index data', 5);
                }
            })
            .catch(err => {
                throw err
            })
    }
}

export function getOverAllNPSInit() {
    return {
        type: types.GET_OVERALL_NPS_INIT
    }
}

export function getOverAllNPSSuccess(data) {
    return {
        type: types.GET_OVERALL_NPS_SUCCESS,
        data: data
    }
}

export function getOverAllNPSError() {
    return {
        type: types.GET_OVERALL_NPS_ERROR,
    }
}


/**
 * @author Meet
 * Top Funnel API
 * @param timezone
 */

export function getNPSTopFunnel(timezone = 'month') {
    return dispatch => {
        dispatch(getTopFunnelInit());
        fetch(`${Globals.API_ROOT_URL}/analytics/funnel/topFunnel?timespan=${timezone}`, {
            method: 'GET',
            headers: utils.setApiHeaders('get'),
        })
            .then(response => {
                return response.json()
            })
            .then(json => {
                if (json.code == 200) {
                    dispatch(getTopFunnelSuccess(json.data));
                } else {
                    dispatch(getTopFunnelError());
                    notie.alert('error', 'Error in Fetching Data', 5);
                }
            })
            .catch(err => {
                throw err
            })
    }
}


export function getTopFunnelInit(){
    return{
        type: types.GET_NPS_TOP_FUNNEL_INIT,
    }
}

export function getTopFunnelSuccess(data){
    return{
        type:types.GET_NPS_TOP_FUNNEL_SUCCESS,
        data:data
    }
}

export function getTopFunnelError(){
    return{
        type:types.GET_NPS_TOP_FUNNEL_ERROR,
    }
}

/**
Api call to get the List of funnel and its data 
@author Akshay
@param timespan
@type GET
**/
export function getFunnelListAnalytics(timezone='month') {
    return dispatch => {
        dispatch(getFunnelListDataInit());
        fetch(`${Globals.API_ROOT_URL}/analytics/funnel/all-nps?timespan=${timezone}`, {
            method: 'GET',
            headers: utils.setApiHeaders('get'),

        })
            .then(response => {
                return response.json()
            })
            .then(json => {
                if (json.code == 200 && json.data !== undefined) {
                    dispatch(getFunnelListDataSuccess(json.data));
                }
                else {
                    dispatch(getFunnelListDataError());
                    notie.alert('error', 'Error fetching data', 5);
                }
            })
            .catch(err => {
                throw err
            })
    }
}

export function getFunnelListDataInit() {
    return {
        type: types.GET_FUNNEL_LIST_DATA_INIT
    }
}

export function getFunnelListDataSuccess(data) {
    return {
        type: types.GET_FUNNEL_LIST_DATA_SUCCESS,
        data: data
    }
}
export function getFunnelListDataError() {
    return {
        type: types.GET_FUNNEL_LIST_DATA_ERROR
    }
}

/**
Api call to get the detail of Particular funnel 
@author Akshay
@param  funnelID
@param timespan
@type GET
**/
export function getFunnelDetailAnalytics(data) {
    var apiString = Globals.API_ROOT_URL + `/analytics/funnel/stages`
        apiString += `?funnel_id=${data.funnelId}&timespan=${data.timezone}`
    return dispatch => {
        dispatch(getFunnelDetailDataInit());
        fetch(apiString, {
            headers: utils.setApiHeaders('get'),
        })
            .then(response => {
                return response.json()
            })
            .then(json => {
                if (json.code == 200 && json.data !== undefined) {
                    dispatch(getFunnelDetailDataSuccess(json.data));
                }
                else {
                    dispatch(getFunnelDetailDataError());
                    notie.alert('error', 'Problem in fetching Funnel Data', 5);
                }
            })
            .catch(err => {
                throw err
            })
    }
}


export function getFunnelDetailDataInit() {
    return {
        type: types.GET_FUNNEL_DETAIL_DATA_INIT
    }
}

export function getFunnelDetailDataSuccess(data) {
    return {
        type: types.GET_FUNNEL_DETAIL_DATA_SUCCESS,
        data: data
    }
}
export function getFunnelDetailDataError() {
    return {
        type: types.GET_FUNNEL_DETAIL_DATA_ERROR
    }
}


/**
Api call to get the commentList of Particular funnel
@author Akshay
@param  funnelID
@param timespan
@type GET
**/
export function getNPSCommentList(data) {
    var apiString = Globals.API_ROOT_URL + `/analytics/funnel/comments`
        apiString += `?funnel_id=${data.funnelId}&timespan=${data.timezone}`
    return dispatch => {
        dispatch(getNPSCommentInit());
        fetch(apiString, {
            headers: utils.setApiHeaders('get'),
        })
            .then(response => {
                return response.json()
            })
            .then(json => {
                if (json.code == 200 && json.data !== undefined) {
                    dispatch(getNPSCommentSuccess(json.data));
                }
                else {
                    dispatch(getNPSCommentError());
                    notie.alert('error', 'Problem in Comment List', 5);
                }
            })
            .catch(err => {
                throw err
            })
    }
}


export function getNPSCommentInit() {
    return {
        type: types.GET_FUNNEL_COMMENT_LIST_INIT
    }
}

export function getNPSCommentSuccess(data) {
    return {
        type: types.GET_FUNNEL_COMMENT_LIST_SUCCESS,
        data: data
    }
}
export function getNPSCommentError() {
    return {
        type: types.GET_FUNNEL_COMMENT_LIST_ERROR
    }
}



