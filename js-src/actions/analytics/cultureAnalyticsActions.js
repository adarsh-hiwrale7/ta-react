import * as types from '../actionTypes';
import Globals from '../../Globals';
import * as utils from '../../utils/utils';
import notie from "notie/dist/notie.js";

/**
to get the culter index data
@author Yamin
@param
**/
export function getIndexData(timezone) {
    return dispatch => {
        dispatch(getIndexDataInit());
        fetch(`${Globals.API_ROOT_URL}/analytics/cultural-index`, {
          method: 'GET',
          headers: utils.setApiHeaders('get'),
        })
          .then(response => {
            return response.json()
          })
          .then(json => {
            if (json.code == 200) {
                dispatch(getIndexDataSuccess(json.data));
             }else{
                dispatch(getIndexDataError());
                notie.alert('error','Problem in fetching culture index data', 5);
            }
         })
          .catch(err => {
            throw err
          })
      }
}

//to get enagement data
export function getEngagementData(timezone){
      return dispatch => {
        dispatch(getEngagementDataInit());
        fetch(`${Globals.API_ROOT_URL}/analytics/culture/score`, {
          method: 'GET',
          headers: utils.setApiHeaders('get'),
        })
          .then(response => {
            return response.json()
          })
          .then(json => {
            if(json.code==200 && json.data!==undefined){
              dispatch(getEngagementDataSuccess(json.data));
          }
          else{
            dispatch(getEngagementDataError());
            notie.alert('error','Error fetching data', 5);
          }
        })
          .catch(err => {
            throw err
          })
      }
}

export function getEngagementDataInit(){
    return{
        type:types.GET_ENGAGEMENTDATA_INIT
    }
}

export function getEngagementDataSuccess(data){
    return{
        type:types.GET_ENGAGEMENTDATA_SUCCESS,
        data : data
    }
}
export function  getEngagementDataError(){
  return{
    type:types.GET_ENGAGEMENTDATA_ERROR
  }
}

// drivers data
export function getAllDriversData(data){
var apiString=Globals.API_ROOT_URL + `/analytics/culture/factors`
    if(data.timespan !== undefined){
      apiString+=`?timespan=${data.timespan}`
    }
    return dispatch => {
      dispatch(getDriversDataInit());
      fetch(apiString, {
            headers: utils.setApiHeaders('get'),
      })
        .then(response => {
          return response.json()
        })
        .then(json => {
          if(json.code==200 && json.data!==undefined){
            dispatch(getDriversDataSuccess(json.data));
        }
        else{
          dispatch(getDriversDataError());
          notie.alert('error','Problem in fetching Drivers data', 5);
        }
      })
        .catch(err => {
          throw err
        })
    }
}


export function getDriversDataInit(){
  return{
      type:types.GET_DRIVERSDATA_INIT
  }
}

export function getDriversDataSuccess(data){
  return{
      type:types.GET_DRIVERSDATA_SUCCESS,
      data:data
  }
}
export function getDriversDataError(){
return{
  type:types.GET_DRIVERSDATA_ERROR
}
}


/**
to get the survey comparison data
@author Akshay
@param timespan
**/
export function getsurveyComparisonData(data){
  var apiString=Globals.API_ROOT_URL + `/analytics/survey/compare`
      if(data.timespan !== undefined){
        apiString+=`?timespan=${data.timespan}`
      }
      return dispatch => {
        dispatch(getsurveyComparisonInit());
        fetch(apiString, {
              headers: utils.setApiHeaders('get'),
        })
          .then(response => {
            return response.json()
          })
          .then(json => {
            if(json.code==200 && json.data!==undefined){
              dispatch(getsurveyComparisonSuccess(json.data));
          }
          else{
            dispatch(getsurveyComparisonError());
            notie.alert('error','Problem in survey data', 5);
          }
        })
          .catch(err => {
            throw err
          })
      }
  }



export function getsurveyComparisonInit(){
    return{
        type:types.GET_SURVEY_COMPARISON_INIT
    }
}

export function getsurveyComparisonSuccess(data){
    return{
        type:types.GET_SURVEY_COMPARISON_SUCCESS,
        data:data
    }
}
export function getsurveyComparisonError(){
  return{
    type:types.GET_SURVEY_COMPARISON_ERROR
  }
}

export function getCultureParticipationData(data){
  var apiString=Globals.API_ROOT_URL + `/analytics/culture/question`
    if(data.timespan !== undefined){
      apiString+=`?timespan=${data.timespan}`
    }
    return dispatch => {
      dispatch(getCultureParticipationInit());
      fetch(apiString, {
            headers: utils.setApiHeaders('get'),
      })
        .then(response => {
          return response.json()
        })
        .then(json => {
          if(json.code==200 && json.data!==undefined){
            dispatch(getCultureParticipationSuccess(json.data));
        }
        else{
          dispatch(getCultureParticipationError());
          notie.alert('error','Problem in fetching Participation data', 5);
        }
      })
        .catch(err => {
          throw err
        })
    }
}

export function getCultureParticipationInit(){
  return{
    type:types.GET_CULTURE_PARTICIPATION_INIT
  }
}
export function getCultureParticipationSuccess(data){
  return{
    type:types.GET_CULTURE_PARTICIPATION_SUCCESS,
    data: data
  }
}

export function getCultureParticipationError(){
  return{
    type:types.GET_CULTURE_PARTICIPATION_ERROR
  }
}


export function getIndexDataInit(){
    return{
        type:types.GET_CULTURE_INDEX_INIT
    }
}

export function getIndexDataSuccess(data){
    return{
        type:types.GET_CULTURE_INDEX_SUCCESS,
        data:data
    }
}

export function getIndexDataError(){
    return{
        type:types.GET_CULTURE_INDEX_ERROR,
    }
}

/**
* To get the participation data
* @author Yamin
**/
export function getParticipationData(data){
    var apiString=Globals.API_ROOT_URL + `/analytics/participation`
    return dispatch => {
        dispatch(getParticipationDataInit());
        fetch(apiString, {
          method: 'POST',
          headers: utils.setApiHeaders('post'),
          body: JSON.stringify(data)
        })
          .then(response => {
            return response.json()
          })
          .then(json => {
            if (json.code == 200) {
                dispatch(getParticipationDataSuccess(json.data));
             }else{
                dispatch(getParticipationDataError());
                notie.alert('error','Problem in fetching Participation data', 3);
            }
         })
          .catch(err => {
            throw err
          })
      }
}

export function getParticipationDataInit(){
    return{
        type:types.GET_PARTICIPATION_INIT
    }
}

export function getParticipationDataSuccess(data){
    return{
        type:types.GET_PARTICIPATION_SUCCESS,
        data:data
    }
}

export function getParticipationDataError(){
    return{
        type:types.GET_PARTICIPATION_ERROR,
    }
}

/**
* get the tagged values
* @author Yamin
**/
export function getTaggedValues(data){
      var apiString=Globals.API_ROOT_URL + `/analytics/tagged-value`
      return dispatch => {
        dispatch(getTaggedValuesInit());
        fetch(apiString, {
          method: 'POST',
          headers: utils.setApiHeaders('post'),
          body: JSON.stringify(data)
        })
          .then(response => {
            return response.json()
          })
          .then(json => {
            if (json.code == 200) {
                dispatch(getTaggedValuesSuccess(json.data));
             }else{
                dispatch(getTaggedValuesError());
                notie.alert('error','Problem in fetching tagged values data', 3);
            }
         })
          .catch(err => {
            throw err
          })
      }
}

export function getTaggedValuesInit(){
    return{
        type:types.GET_TAGGED_VALUE_INIT
    }
}

export function getTaggedValuesSuccess(data){
    return{
        type:types.GET_TAGGED_VALUE_SUCCESS,
        data:data
    }
}

export function getTaggedValuesError(){
    return{
        type:types.GET_TAGGED_VALUE_ERROR,
    }
}

/**
* get the voted values
* @author Yamin
**/
export function getVotedValues(data){
      var apiString=Globals.API_ROOT_URL + `/analytics/voted-value`
      return dispatch => {
        dispatch(getVotedValuesInit());
        fetch(apiString, {
          method: 'POST',
          headers: utils.setApiHeaders('post'),
          body: JSON.stringify(data)
        })
          .then(response => {
            return response.json()
          })
          .then(json => {
            if (json.code == 200) {
                dispatch(getVotedValuesSuccess(json.data));
             }else{
                dispatch(getVotedValuesError());
                notie.alert('error','Problem in fetching voted values data', 3);
            }
         })
          .catch(err => {
            throw err
          })
      }
}

export function getVotedValuesInit(){
    return{
        type:types.GET_VOTED_VALUE_INIT
    }
}

export function getVotedValuesSuccess(data){
    return{
        type:types.GET_VOTED_VALUE_SUCCESS,
        data:data
    }
}

export function getVotedValuesError(){
    return{
        type:types.GET_VOTED_VALUE_ERROR,
    }
}

/**
* get the level of user
* @author Yamin
**/
export function getLevelData(){
            
      return dispatch => {
        dispatch(getLevelDataInit());
        let levelData = {"data": [{value:'board',label:'Board'},{value:'employee',label:'Employee'},{value:'manager',label:'Manager'},{value:'senior manager',label:'Senior Manager'}]}
        
        dispatch(getLevelDataSuccess(levelData))
            
      }
}

export function getLevelDataInit(){
    return{
        type:types.GET_LEVEL_DATA_INIT
    }
}

export function getLevelDataSuccess(data){
    return{
        type:types.GET_LEVEL_DATA_SUCCESS,
        data:data
    }
}

/**
* get the voted values
* @author Yamin
**/
export function getGenderData(){
            
      return dispatch => {
        dispatch(getGenderDataInit());
        let genderData = {"data": [{value:'male',label:'Male'},{value:'female',label:'Female'},{value:'others',label:'Others'}]}
        dispatch(getGenderDataSuccess(genderData))
            
      }
}

export function getGenderDataInit(){
    return{
        type:types.GET_GENDER_DATA_INIT
    }
}

export function getGenderDataSuccess(data){
    return{
        type:types.GET_GENDER_DATA_SUCCESS,
        data:data
    }
}

/**
* get the visibly role
* @author Yamin
**/
export function getVisiblyRoleData(){
            
      return dispatch => {
        dispatch(getVisiblyRoleDataInit());
        let roleData = {"data": [{value:'employee',label:'employee'},{value:'female',label:'Female'},{value:'others',label:'Others'}]}
        dispatch(getVisiblyRoleDataSuccess(roleData))
            
      }
}

export function getVisiblyRoleDataInit(){
    return{
        type:types.GET_VISIBLY_ROLE_DATA_INIT
    }
}

export function getVisiblyRoleDataSuccess(data){
    return{
        type:types.GET_VISIBLY_ROLE_DATA_SUCCESS,
        data:data
    }
}

export function saveSegmentation(data){
      var apiString=Globals.API_ROOT_URL + `/analytics/storeFilter`
      return dispatch => {
        dispatch(saveSegmentationInit());
        fetch(apiString, {
          method: 'POST',
          headers: utils.setApiHeaders('post'),
          body: JSON.stringify(data)
        })
          .then(response => {
            return response.json()
          })
          .then(json => {
            if (json.code == 200) {
                dispatch(saveSegmentationSuccess());
                dispatch(heatMapChartData())
             }else{
                dispatch(saveSegmentationError());
                notie.alert('error',json.message, 3);
            }
         })
          .catch(err => {
            throw err
          })
      }
}

export function saveSegmentationInit(){
    return{
        type:types.SAVE_SEGMENTATION_INIT
    }
}

export function saveSegmentationSuccess(){
    return{
        type:types.SAVE_SEGMENTATION_SUCCESS
    }
}

export function saveSegmentationError(){
    return{
        type:types.SAVE_SEGMENTATION_ERROR
    }
}

export function resetSaveFlag(){
    return{
        type:types.RESET_SAVE_FLAG
    } 
}

export function fetchAllSegments(){
    return dispatch => {
        dispatch(fetchAllSegmentsInit());
        fetch(`${Globals.API_ROOT_URL}/analytics/segment`, {
          method: 'GET',
          headers: utils.setApiHeaders('get'),
        })
          .then(response => {
            return response.json()
          })
          .then(json => {
            if (json.code == 200) {
                dispatch(fetchAllSegmentsSuccess(json.data));
             }else{
                dispatch(fetchAllSegmentsError());
                notie.alert('error',json.message, 5);
            }
         })
          .catch(err => {
            throw err
          })
      }
}

export function fetchAllSegmentsInit(){
    return{
        type:types.FETCH_SEGMENTATION_INIT
    }
}

export function fetchAllSegmentsSuccess(data){
    return{
        type:types.FETCH_SEGMENTATION_SUCCESS,
        data:data
    }
}

export function fetchAllSegmentsError(){
    return{
        type:types.FETCH_SEGMENTATION_ERROR
    }
}

export function fetchPerticularSegment(segmentId){
    return dispatch => {
        dispatch(fetchPerticularSegmentInit());
        fetch(`${Globals.API_ROOT_URL}/analytics/segment?segment_id=`+segmentId, {
          method: 'GET',
          headers: utils.setApiHeaders('get'),
        })
          .then(response => {
            return response.json()
          })
          .then(json => {
            if (json.code == 200) {
                dispatch(fetchPerticularSegmentSuccess(json.data));
             }else{
                dispatch(fetchPerticularSegmentError());
                notie.alert('error',json.message, 5);
            }
         })
          .catch(err => {
            throw err
          })
      }
}

export function fetchPerticularSegmentInit(){
    return{
        type:types.FETCH_PERTICULAR_SEGMENTATION_INIT
    }
}

export function fetchPerticularSegmentSuccess(data){
    return{
        type:types.FETCH_PERTICULAR_SEGMENTATION_SUCCESS,
        data:data
    }
}

export function fetchPerticularSegmentError(){
    return{
        type:types.FETCH_PERTICULAR_SEGMENTATION_ERROR
    }
}

export function heatMapChartData(){
    return dispatch => {
        dispatch(heatMapChartDataInit());
        fetch(`${Globals.API_ROOT_URL}/analytics/culture/heatmap`, {
          method: 'GET',
          headers: utils.setApiHeaders('get'),
        })
          .then(response => {
            return response.json()
          })
          .then(json => {
            if (json.code == 200 && json.data!==undefined) {
                dispatch(heatMapChartDataSuccess(json.data));
             }else{
                dispatch(heatMapChartDataError());
                notie.alert('error',json.message, 5);
            }
         })
          .catch(err => {
            throw err
          })
      }
 
}

export function heatMapChartDataInit(){
    return{
        type:types.FETCH_HEATMAP_DATA_INIT
    }
}

export function heatMapChartDataSuccess(data){
    return{
        type:types.FETCH_HEATMAP_DATA_SUCCESS,
        data:data
    }
}

export function heatMapChartDataError(){
    return{
        type:types.FETCH_HEATMAP_DATA_ERROR
    }
}

export function removeSegmentation(segmentId){
  var jsonBody ={
    segment_id:[segmentId]
  }
    return (dispatch) => {
            notie.confirm(
              `Are you sure you want to delete this segment?`,
              'Yes',
              'No',
              function () {
                  dispatch(removeSegmentationInit());
                    fetch(Globals.API_ROOT_URL + `/analytics/segment`, {
                      method: 'DELETE',
                      headers: utils.setApiHeaders('delete'),
                      body: JSON.stringify(jsonBody)
                    }).then(response => {
                      return response.json();
                    }).then(json => {
                      if (json.code == 200) {
                        notie.alert('success', json.message, 3);
                        dispatch(fetchAllSegments());
                        dispatch(heatMapChartData())
                      }else{
                        utils.handleSessionError(json)
                        notie.alert('error', json.message, 3);
                      }
                      dispatch(removeSegmentationSuccess());

                    }).catch(err => {
                      throw err;
                    });
              },
              function () { }
            )
    }
}

export function removeSegmentationInit(){
    return{
        type:types.DELETE_SEGMENTATION_DATA_INIT
    }
}

export function removeSegmentationSuccess(){
    return{
        type:types.DELETE_SEGMENTATION_DATA_SUCCESS
    }
}










