import Globals from '../../Globals';
import * as types from '../actionTypes';
import * as utils from '../../utils/utils';
export function fetchTwitterSuccess(data) {
  return { 
  	type: types.FETCH_TWITTER_SUCCESS, 
  	tweetData: data
  };
}
export function fetchTwitter() {
	return {
		type: types.FETCH_TWITTER
	};
}

export function loadTwitterData() {
	return (dispatch) => {
		
		// start fetch so loading is set to true
		fetch(Globals.API_ROOT_URL + `/twitter/tweet/802166797067816961`, {
			method: 'GET',
			headers: utils.setApiHeaders('get'),
		  })
		.then(response => response.json())
		.then(json => dispatch(fetchTwitterSuccess(json)))
		.catch(err => { throw err; });
	}
}
