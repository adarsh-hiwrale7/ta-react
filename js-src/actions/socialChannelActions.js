import * as types from './actionTypes';
import Globals from '../Globals';
import * as utils from '../utils/utils';
import notie from 'notie/dist/notie.js'
import * as campaignsActions from './campaigns/campaignsActions';

export function fetchChannelsStart() {
    return {
        type: types.FETCH_CHANNELS_START,
    };
}

export function fetchChannelsSuccess(channels) {
    return {
        type: types.FETCH_CHANNELS_SUCCESS,
        channels: channels
    };
}

export function fetchSuccessNoChannels() {
    return {
        type: types.CHANNELS_NOT_FOUND,
    }
}

export function fetchChannels(channelsToFilter) {
    // the below condition is temporary, SHOULD BE REMOVED
    if(channelsToFilter==undefined) {
        channelsToFilter = {
            data: []
        }
    }

    let selectedChannels = channelsToFilter;
    return (dispatch) => {
        var me = this;
        dispatch(fetchChannelsStart());

        fetch(Globals.API_ROOT_URL + `/social/channel`,{
            headers: utils.setApiHeaders('get'),
        })
            .then(response => response.json())
            .then((json) => {
                if (typeof (json.data) !== 'undefined') {
                    dispatch(fetchChannelsSuccess(json.data))

                  if(typeof(selectedChannels)!=="undefined") {
                    dispatch(filterChannels(selectedChannels, json.data))
                  }

                } else {
                    utils.handleSessionError(json)
                    dispatch(fetchSuccessNoChannels())
                }
                if(json.message!==undefined){
                    notie.alert(
                        'success',
                        json.message
                        ,
                        5
                      )
                }
            }).catch(err => {
            throw err;
        });
    }
}


export function filterChannels(selectedChannels, channels) {
  return(dispatch)=> {
    var selectedSocialChannels = []
      if (typeof (selectedChannels.data) !== "undefined") {
        channels.map((social, i) => {
          selectedChannels.data.map(function(channel, index) {
            if (channel.identity == social.identity) {
              selectedSocialChannels.push(channel.media.toLowerCase());
            }
          });
        });

      }
      dispatch(filterChannelsSuccess(selectedSocialChannels))
  };
}


export function filterChannelsSuccess(selectedSocialChannels) {
    return {
        type: types.FILTER_CHANNELS_SUCCESS,
        selectedChannels: selectedSocialChannels
    };
}
