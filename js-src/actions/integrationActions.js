import * as types from './actionTypes';
import Globals from '../Globals';
import notie from "notie/dist/notie.js";
import { reset } from 'redux-form';
import {b64DecodeUnicode}  from '../utils/utils';
import * as utils  from '../utils/utils';
import {browserHistory} from 'react-router';
import { fetchSocialAccounts } from './socialAccountsActions'
/**
 * @author Kinjal
 * to add intergration key
 * @param {*} type
 */
export function intergrationKeyGenrater(type){
  return (dispatch) => {
      dispatch(IntegrationKeyInit());
      fetch(Globals.API_ROOT_URL + `/curation/secret?generate_key=${type}`, {
        method: 'GET',
        headers: utils.setApiHeaders('get',{
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        }),
       }).then(response => {
         return response.json();
      }).then((json) => {
        if (json.code == 200) {
         dispatch(fetchIntegrationSuccess(json))
         if(json.secret_key == ' ' || json.secret_key == 'undefined'){
          notie.alert('success', json.message, 3);
         }
         else{ if(typeof json.message !== "undefined"){
                notie.alert('success', json.message, 3);
              }
         }
        }else{
          notie.alert('error', json.message, 3);
          utils.handleSessionError(json)
           dispatch(fetchIntegrationError(json.message))
        }
        }).catch(err => {
        throw err;
      });
  }
}
export function IntegrationKeyInit(){
  return {
  type: types.INTEGRATION_KEY_INT
}
}
export function fetchIntegrationSuccess(data) {
   return {
          type:  types.INTEGRATION_KEY_SUCCESS,
          data: data.secret_key
        }

}
export function fetchIntegrationError(data) {
  return {
            type:  types.INTEGRATION_KEY_ERROR,
            data: data.secret_key
          }
 }




/**
 * @author kinjal
 * Workable Add Key
 * @param (*) data // data will include workable key and sub domain
 */


   export function workableAddKey(data){
    return (dispatch) => {
        dispatch(WorkableKeyInit());
        fetch(Globals.API_ROOT_URL + `/curation/addkey`, {
          method: 'POST',
          headers: utils.setApiHeaders('post'),
          body: JSON.stringify(data)

        }).then(response => {
          return response.json();
        }).then((json) => {
          if (json.code == 200) {
            dispatch(reset('WokableForm'));
            dispatch(workablerdata(json))

            notie.alert('success', json.data, 3);
          }else if(typeof json.error !== "undefined"){
            utils.handleSessionError(json)
            notie.alert('error', json.error.key, 3);
          }else{
            dispatch(fetchWorkableError(json.message))
          }
          }).catch(err => {
          throw err;
        });
    }
  }

/**
 * @author kinjal
 * fetch the key and sub domain
 */
  export function workablerdata(){
  return (dispatch) => {
    var me = this;
    dispatch(fetchWorkableDetailInit());
    fetch(Globals.API_ROOT_URL + `/curation/getkey`,{
      headers: utils.setApiHeaders('get'),
    }).then(response => response.json()).then((json) => {
      if (typeof(json.data) !== undefined) {
        dispatch(fetchWorkableDetailSuccess(json.data));
      }else{
        dispatch(fetchWorkableDetailSuccess([]));
        utils.handleSessionError(json)
      }
    }).catch(err => {
      throw err;
    });
  }
}
export function fetchWorkableDetailInit(){
  return {
    type:  types.FETCH_WORKABLE_DETAILS_INIT,
  }
}
export function fetchWorkableDetailSuccess(data){
  return {
    type:  types.FETCH_WORKABLE_DETAILS_LINK,
    data: data
  }
}
export function WorkableKeyInit(){
    return {
    type: types.WORKABLEKEY_INIT
  }
  }
  export function WorkableKeySuccess() {
    return {
      type: types.WORKABLEKEY_SUCCESS,
    }
  }
  export function WorkableKeyFail() {
    return {
      type: types.WORKABLEKEY_FAIL,
    }
  }
  export function fetchSlackClientInfoSuccess(data) {
    return {
      type: types.FETCH_SLACK_CLIENT_INFO_SUCCESS,
      data:data
    }
  }
  export function fetchSlackClientInfoError(data) {
    notie.alert('error', data.message, 3);
    return {
      type: types.FETCH_SLACK_CLIENT_INFO_ERROR
    }
  }
  export function fetchSlackClientInfoInit() {
    return {
      type: types.FETCH_SLACK_CLIENT_INFO_INIT
    }
  }
/**
 * fetch client id of slack
 * @author disha
 */
  export function fetchSlackClientInfo(){
  return (dispatch) => {
    dispatch(fetchSlackClientInfoInit());
    fetch(Globals.API_ROOT_URL + `/social-secret?social_media=slack`,{
      headers: utils.setApiHeaders('get'),
    })
    .then(response => response.json()).then((json) => {
      if (typeof(json.data) !== 'undefined') {
          dispatch(slackOuth(json.data))
          dispatch(fetchSlackClientInfoSuccess(json.data));
      }else{
        if(typeof(json.error) !== 'undefined')
        {
          dispatch(fetchSlackClientInfoError(json.error));
        }
        utils.handleSessionError(json)
      }
    }).catch(err => {
      throw err;
    });
  }
}

/**
 * redirect on this page with client id to select channel name and to authorize
 * @param {*} data client id
 * @author disha
 */
export function slackOuth(data){
  var decodedString = b64DecodeUnicode(data.slack_Client_id);
  return (dispatch)=> {
    if (typeof window !== 'undefined'){
      window.location =  `https://slack.com/oauth/authorize?client_id=${decodedString}&scope=channels:read,chat:write:user,im:write,incoming-webhook,chat:write:bot,groups:read,users:read,im:read&redirect_uri=${Globals.CLIENT_URL}/settings/integration&state=12345`
    }
  }
}

export function ConnenctSlackSuccess(data){
  notie.alert('success', data.message, 3);
  return{
    type:types.CONNECT_SLACK_SUCCESS
  }
}
export function ConnenctSlackError(data){
  notie.alert('error', data.message, 3);
  return{
    type:types.CONNECT_SLACK_ERROR
  }
}
export function ConnenctSlackInit(){
  return{
    type:types.CONNECT_SLACK_INIT
  }
}
/**
 * to connect slack with code (after authorizing on slack and selecting channel name slack will redirect to visibly page with code )
 * @param {*} slackcode slack code 
 * @author disha
 */
export function ConnenctSlack(slackcode){
  return (dispatch) => {
    dispatch(ConnenctSlackInit())
      fetch(Globals.API_ROOT_URL + `/slack-data?state=12345&redirect_uri=${Globals.CLIENT_URL}/settings/integration&code=${slackcode}`,{
        headers: utils.setApiHeaders('get'),
      })
        .then(response => {
        return response.json();
      }).then((json) => {
        if (json.code == 200) {
          dispatch(fetchSocialAccounts(true))
          dispatch(ConnenctSlackSuccess(json))
        }else{
          dispatch(ConnenctSlackError(json))
          utils.handleSessionError(json)
        }
        }).catch(err => {
        throw err;
      });
  }
}

export function fetchSlackChannelListSuccess(data){
  return{
    type:types.FETCH_SLACK_CHANNEL_LIST_SUCCESS,
    data:data
  }
}
export function fetchSlackChannelListError(data){
  notie.alert('error', data, 3);
  return{
    type:types.FETCH_SLACK_CHANNEL_LIST_ERROR
  }
}
export function fetchSlackChannelListInit(){
  return{
    type:types.FETCH_SLACK_CHANNEL_LIST_INIT
  }
}
/**
 * to get slack channel list
 * @param {*} token access token
 * @author disha
 */
export function fetchSlackChannelList(token){
  return (dispatch) => {
    dispatch(fetchSlackChannelListInit())
      fetch(`https://slack.com/api/channels.list?token=${token}&types=private_channel,public_channel&pretty=1`)
        .then(response => {
        return response.json();
      }).then((json) => {
        if (json.ok == true) {
          if(typeof json.channels !== 'undefined'){
            dispatch(fetchSlackUserList(token,json.channels))
          }
        }else{
          dispatch(fetchSlackChannelListError(json.error))
        }
        }).catch(err => {
        throw err;
      });
  }
}
// get list of users details 
export function fetchSlackUserList(token,allChannelJson){
  return (dispatch)=>{
    fetch(`https://slack.com/api/users.list?token=${token}&pretty=1`)
    .then(response=>{
      return response.json();
    }).then((userJson)=>{
      if(userJson.ok==true){
        if(typeof userJson.members !== 'undefined'){
          dispatch(fetchSlackPrivateChannelList(token,allChannelJson,userJson.members))
        }
      }else{
        dispatch(fetchSlackChannelListError(userJson.error))
      }
    }).catch(err => {
      throw err;
    });
  }
}
//get list of non archived private channel
export function fetchSlackPrivateChannelList(token,allChannelJson,userJson){
  return (dispatch)=>{
    fetch(`https://slack.com/api/groups.list?token=${token}&exclude_archived=true&pretty=1`)
    .then(response=>{
      return response.json();
    }).then((privateJson)=>{
      if(privateJson.ok==true){
        if(typeof privateJson.groups !== 'undefined'){
          dispatch(fetchSlackImList(token,allChannelJson,userJson,privateJson.groups))
        }
      }else{
        dispatch(fetchSlackChannelListError(privateJson.error))
      }
    }).catch(err => {
      throw err;
    });
  }
}
/**
 * @author disha
 * get all user id 
 * @param {*} token slack token from social account detail
 * @param {*} allChannelJson all public channel lists
 * @param {*} userJson user list 
 * @param {*} privateJson private channel list
 */
export function fetchSlackImList(token,allChannelJson,userJson,privateJson){
  return (dispatch)=>{
    fetch(`https://slack.com/api/im.list?token=${token}&pretty=1`)
    .then(response=>{
      return response.json();
    }).then((imJson)=>{
      if(imJson.ok==true){
        if(typeof imJson.ims !== 'undefined'){
          //store private channel list 
          for(var p=0;p<privateJson.length;p++){
            var ObjToAdd={
                'id':privateJson[p].id,
                'name':privateJson[p].name,
                'is_archived':privateJson[p].is_mpim
            }
            allChannelJson.push(ObjToAdd)
          }
          //get all users detail from userJson and get all user channel id from imJson  (as to reconnect other channel , the channel id is required to pass and userJson don't have channel id thats why imJson is used to fetch channel id according to its user id )
          for(var i=0;i<userJson.length;i++){
            for(var j=0;j<imJson.ims.length;j++){
              if(userJson[i].id==imJson.ims[j].user){
                var ObjToAdd={
                  'id':imJson.ims[j].id,
                  'name':userJson[i].name,
                  'is_archived':userJson[i].deleted
                }
                allChannelJson.push(ObjToAdd)
              }
            }
          }
          dispatch(fetchSlackChannelListSuccess(allChannelJson))
        }
      }else{
        dispatch(fetchSlackChannelListError(imJson.error))
      }
    }).catch(err => {
      throw err;
    });
  }
}

export function disconnectSlackSuccess(data){
  notie.alert('success', data.message, 3);
  return{
    type:types.DISCONNECT_SLACK_SUCCESS,
    data:data
  }
}
export function disconnectSlackError(data){
  notie.alert('error', data.message, 3);
  return{
    type:types.DISCONNECT_SLACK_ERROR
  }
}
export function disconnectSlackInit(){
  return{
    type:types.DISCONNECT_SLACK_INIT
  }
}
/**
 * to disconnect slack
 * @param {*} accountID account id(userid)
 * @author disha
 */
export function disconnectSlack(accountID) {
  return (dispatch) => {
    dispatch(disconnectSlackInit())
    fetch(Globals.API_ROOT_URL + `/social-accounts/${accountID}`, 
    { method: 'DELETE' ,
      headers: utils.setApiHeaders('get')}
    ).then(response => {
      if (response.status != "200") {
        notie.alert('error', response.statusText, 3);
      }
      return response.json();
    }).then((json) => {
      if (json.code == 200) {
        dispatch(disconnectSlackSuccess(json))
        dispatch(fetchSocialAccounts())
      } else {
        utils.handleSessionError(json);
        dispatch(disconnectSlackError(json))
        
      }
    }).catch(err => {
      throw err;
    });
  }
}
export function changeSlackChannelInit(){
  return{
    type:types.CHANGE_SLACK_CHANNEL_INIT
  }
}
export function changeSlackChannelSuccess(message){
  notie.alert('success', message, 3);
  return{
    type:types.CHANGE_SLACK_CHANNEL_SUCCESS
  }
}
export function changeSlackChannelError(message){
  notie.alert('error', message, 3);
  return{
    type:types.CHANGE_SLACK_CHANNEL_ERROR
  }
}
/**
 * to switch channel
 * @param {*} newChannelData object will have selected channel details
 * @author disha
 */
export function changeSlackChannel(newChannelData){
  return (dispatch) => {
      dispatch(changeSlackChannelInit());
      fetch(Globals.API_ROOT_URL + `/slack-data`, {
        method: 'PUT',
        headers: utils.setApiHeaders('put'),
        body: JSON.stringify(newChannelData)

      }).then(response => {
        return response.json();
      }).then((json) => {
        if (json.code == 200) {
          dispatch(fetchSocialAccounts(true))
          dispatch(changeSlackChannelSuccess(json.message))
        }else{
          utils.handleSessionError(json)
          dispatch(changeSlackChannelError(json.message))
        }
        }).catch(err => {
        throw err;
      });
  }
}
 /**
 * to check sso data is available or not
 * @author Yamin 
 **/
 export function getSSOData(){
    return (dispatch) => {
        dispatch(getSSODataInit())
        fetch(Globals.API_ROOT_URL + `/integration/sso`,{
          headers: utils.setApiHeaders('get'),
        })
          .then(response => {
          return response.json();
        }).then((json) => {
           if (json.code == 200) {
              var JsonResponseRef = [];
              JsonResponseRef = typeof json.data !== "undefined" ? json.data : []
              dispatch(getSSODataSucces(JsonResponseRef))
           }else{
              dispatch(getSSODataFail())
           } 
        }).catch(err => {
          throw err;
        });
    }
 }
 export function getSSODataInit(){
  return{
    type:types.GET_SSO_DATA_INIT
  }
 }
 export function getSSODataSucces(data){
  return{
    type:types.GET_SSO_DATA_SUCCESS,
    data:data
  }
 }
 export function getSSODataFail(){
  return{
    type:types.GET_SSO_DATA_FAIL
  }
 }
 /**
 * Handle sso connect api 
 * @author Yamin
 * @type Post
 **/
 export function ssoConnect(values){
    var methodType = values.hasOwnProperty('ApiType') ? values.ApiType : 'POST' 

    var objToSend = {
      "entity_id": values.sso_connect_entity_id,
      "login_url": values.sso_connect_url,
      "certificate": typeof values.sso_connect_certificate_upload !== 'undefined' && values.sso_connect_certificate_upload !== ''? values.sso_connect_certificate_upload : values.sso_connect_certificate,
      "sso_name": values.sso_connect_name,
      "required_single_sign_on" :typeof values.required_single_sign_on !=='undefined' ?values.required_single_sign_on :'optional',
      "allow_profile_edit" :typeof values.allow_profile_edit !== 'undefined' ? values.allow_profile_edit : 'Yes',
      "sso_secret_key" :values.sso_secret_key,
    }
    
    return (dispatch) => {
        dispatch(ssoConnectInit());
        fetch(Globals.API_ROOT_URL + `/integration/sso`, {
          method: methodType,
          headers: utils.setApiHeaders(methodType.toLowerCase()),
          body: JSON.stringify(objToSend)

        }).then(response => {
          return response.json();
        }).then((json) => {
            dispatch(ssoConnectSuccess(true))
            dispatch(getSSOData())
            if (json.code == 200) {
              notie.alert('success', json.message, 3);
            }else{
              if(typeof json.error == 'string'){
                notie.alert('error',json.error,5)
              }else{
                if(Object.keys(json.error).length > 0){
                  var errorKey = Object.keys(json.error)[0];
                  notie.alert('error',json.error[errorKey],5)
                }else{
                  notie.alert('error', json.error, 5); 
                }
              }
            }
          }).catch(err => {
          throw err;
        });
    }
 }
 export function ssoConnectInit(){
  return{
    type:types.SSO_CONNECT_INIT
  }
 }
 export function ssoConnectSuccess(flag){
  return{
    type:types.SSO_CONNECT_SUCCESS,
    data:flag
  }
 }
 /**
 * Handle sso disconnect api 
 * @author Yamin
 * @type DELETE
 **/
 export function ssoDisconnect(){
  return (dispatch) => {
     notie.confirm(
              `Are you sure you want to reset SSO details?`,
              'Yes',
              'No',
              function () {
                  dispatch(ssoDisconnectInit())
                  fetch(Globals.API_ROOT_URL + `/integration/sso`, 
                  { method: 'DELETE' ,
                    headers: utils.setApiHeaders('delete')}
                  ).then(response => {
                    /*if (response.status != "200") {
                      notie.alert('error', response.statusText, 3);
                    }*/
                    return response.json();
                  }).then((json) => {
                    dispatch(ssoDisconnectSuccess(json))
                    dispatch(getSSOData())
                   
                    if (json.code == 200) {
                      notie.alert('success', json.message, 3);
                     } else {
                      notie.alert('error', json.error, 3);
                    }
                  }).catch(err => {
                    throw err;
                  });
          })
        }
        
 }
 export function ssoDisconnectInit(){
  return{
    type:types.SSO_DISCONNECT_INIT
  }
 }
 export function ssoDisconnectSuccess(){
  return{
    type:types.SSO_DISCONNECT_SUCCESS
  }
 }
 export function resetDisconnect(){
    return{
    type:types.SSO_DISCONNECT_RESET
  }
 }