import * as types from './actionTypes';
import Globals from '../Globals';
import { taBase64 } from '../utils/utils';
import * as utils  from '../utils/utils';
import notie from "notie/dist/notie.js"
import { browserHistory } from 'react-router';
import {logout} from './authActions'
import {authUser} from './authActions'
import {resetSearchInput} from './searchActions';
export function AddPasswordInitiate() {
    return {
        type: types.ADD_PASS_SAVING
    };
}
export function storeStepNumber(number){
    return{
        type:types.STORE_STEP_NUMBER,
        data:number
    }
}
export function InitialLoadingForgot(){
    return{
        type:types.INITIAL_LOADING_FORGOT
    }
}
export function RequestForgotPassword() {
    return {
        type: types.REQUEST_FORGOT_PASSWORD
    };
}

export function AddPasswordSuccess(users=null) {
    return {
        type: types.ADD_PASS_SUCCESS,
    };
}


export function addPasswordError(statusText) {
    return {
        type: types.ADD_PASS_ERROR,
        text: statusText,
    };
}

export function savePassword(fields) {
    return (dispatch) => {

        dispatch(AddPasswordInitiate());

        if (fields["confirmpassword"] && fields["newpassword"]) {

            var arr = {
                'user_token': "QTdsGBcg4srQ",
                'secret': taBase64.encode(fields["newpassword"])
            }
            /*let jsonBody = {
            	"data": arr
            };*/

            fetch(Globals.API_ROOT_URL + `/settings/password-set`, {
                method: 'POST',
                headers: utils.setApiHeaders('post'),
                body: JSON.stringify(arr)
            })
                .then(response => {
                    if (response.status == "200") {
                        dispatch(ResetPasswordSuccess());
                    } else if (response.status == "500") {
                        dispatch(addPasswordError('You are not authorised to change password for this account'));
                    } else {
                        dispatch(addPasswordError(response.statusText));
                    }
                    return response.json();
                })
                .then(json=>{
                    utils.handleSessionError(json)
                })
                .catch(err => {
                    throw err;
                });
        }

        else {
        }


    }

}

export function resetPasswordSuccess(message) {
	if(typeof(message)=='undefined')
			notie.alert('success', 'Password changed successfully.', 3);
		else
			notie.alert('error', message, 3);
    return {
        type: types.RESET_PASS_SUCCESS,
    };
}
export function resetForgotPasswordSuccess(message) {
    if(typeof(message)=='undefined')
        {
        notie.alert('success', 'Email sent successfully', 3);
        }
    else
        notie.alert('error', message, 3);
    return {
        type: types.RESET_FORGOT_PASS_SUCCESS,
    }
}
export function setNewPasswordSuccess() {
    if(typeof(message)=='undefined')
    {
        notie.alert('success', 'Password changed successfully.', 3);
        browserHistory.push("/login");
    }
    else
        notie.alert('error', message, 3);
        return {
            type: types.SET_NEW_PASS_SUCCESS,
        }
    }
export function resetPasswordError() {
    return {
        type: types.RESET_PASS_ERROR,
    };
}
export function setnewPasswordError() {
    return {
        type: types.SET_NEW_PASS_ERROR,
    };
}
export function resetForgotPasswordError() {
    return {
        type: types.RESET_FORGOT_PASS_ERROR,
    };
}
export function resetPassword(fields) {
    var str = fields.newpassword;

    if(str.charAt(str.length-1)==' '){
        notie.alert("error","Please remove any spaces from the end of your password.",3)
        return;
    }
    if(str.charAt(0)==' '){
        notie.alert("error","Please remove any spaces from the beginning of your password.",3)
        return;
    }
    return (dispatch) => {
        dispatch(AddPasswordInitiate());

        if (fields["confirmpassword"] && fields["newpassword"] && fields["oldpassword"]) {

            var arr = {
                'old_secret': taBase64.encode(fields["oldpassword"]),
                'new_secret': taBase64.encode(fields["newpassword"]),
                'new_confirm_secret': taBase64.encode(fields["confirmpassword"]),
            }

            fetch(Globals.API_ROOT_URL + `/settings/change-password`, {
                method: 'POST',
                headers: utils.setApiHeaders('post'),
                body: JSON.stringify(arr)
            })
                .then(response => {
                    if (response.status == "200") {
                        dispatch(resetPasswordSuccess(response.message));
                    } else if (response.status == "500") {
                        dispatch(resetPasswordError('You are not authorised to change password for this account'));
                    } else {
                        dispatch(resetPasswordError(response.statusText));
                    }
                    return response.json();
                })
                .then(json=> {
                      dispatch(resetPasswordSuccess(json.message));
                      utils.handleSessionError(json)
			    })
                .catch(err => {
                    throw err;
                });
        } else {
        }


    }

}
export function resetForgotPassword(fields) {
    return (dispatch) => {
        dispatch(InitialLoadingForgot());
        var arr = {
            'email_id':fields["email"],
        }
        fetch(Globals.API_ROOT_URL_MASTER + `/forgot-password`, {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(arr)
            })
                .then(response => {
                    if (response.status == "200") {
                        dispatch(resetForgotPasswordSuccess(response.message));
                    } else if (response.status == "500") {
                        dispatch(resetForgotPasswordError('You are not authorised to change password for this account'));
                    } else {
                        dispatch(resetForgotPasswordError(response.statusText));
                    }
                    return response.json();
                })
                .then(json=> {
                    utils.handleSessionError(json)
		      		dispatch(resetForgotPasswordSuccess(json.message));
			    })
                .catch(err => {
                    throw err;
                });
    }

}

export function setNewPassword(fields,token,email) {
    var str = fields.newpassword
    if(str.charAt(str.length-1)==' '){
        notie.alert("error","Please remove any spaces from the end of your password.",3)
        return;
    }
    if(str.charAt(0)==' '){
        notie.alert("error","Please remove any spaces from the beginning of your password.",3)
        return;
    }
    return (dispatch) => {
        dispatch(InitialLoadingForgot());
        //dispatch(AddPasswordInitiate());
        var arr = {
            'email_id':email,
            'secret':taBase64.encode(fields["newpassword"]),
            'reset_token':token
        }
        fetch(Globals.API_ROOT_URL_MASTER + `/set-password`, {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(arr)
            })
                .then(response => {
                    if (response.status == "200") {
                        dispatch(setNewPasswordSuccess(response.message));
                    } else if (response.status == "500") {
                        dispatch(setnewPasswordError('Problem in updating password'));
                    } else {
                        dispatch(setnewPasswordError(response.statusText));
                    }
                    return response.json();
                })
                .then(json=> {
                    utils.handleSessionError(json)
		      		dispatch(setNewPasswordSuccess(json.message));
			    })
                .catch(err => {
                    throw err;
                });
    }

}
export function logoutError(){
    return{
        type:types.LOGOUT_ERROR
    }
}
export function logoutInit(){
  return{
      type:types.LOGOUT_INIT
  }
}
/**
 * @author disha
 * used to logout the visibly
 */
export function callLogoutApi() {
    return (dispatch) => {
        dispatch(logoutInit())
      fetch(Globals.API_ROOT_URL + `/user/logout`, {
        method: 'DELETE',
        headers: utils.setApiHeaders('get'),
      }).then(response => {
        return response.json();
      }).then((json) => {
        if (json.code ==  "200") {
            dispatch(resetSearchInput())
            dispatch(logout())
            userpilot.reload()

        //   notie.alert('success', json.message, 3);
         }else{
            dispatch(logoutError())
            utils.handleSessionError(json)
           notie.alert('error', json.message, 3);
         }
      }).catch(err => {
        throw err;
      });
    }
  }
  export function loginsuccess(data,userEmailPassword){
      console.log(data,'data')
      return{
          type:types.LOGIN_SUCCESS,
          data:data,
          userEmailPassword:userEmailPassword
      }
  }

  export function loginError(){
      return{
          type:types.LOGIN_ERROR
      }
  }
  export function loginInit(){
    return{
        type:types.LOGIN_INIT
    }
}
/**
 * @author disha
 * call when login button is clicked to login 
 * @param data email id and password
 * @param ip_address ip address
 */
  export function userLogin(data,ip_address){
    var newStr = data.password?data.password:'';
    if(newStr){
    while(newStr.charAt(newStr.length-1)==' '){
        newStr = newStr.substr(0, newStr.length-1);
    }
    while(newStr.charAt(0)==' '){
        newStr = newStr.substr(1);
    }
    }
    var arr = {
        email: data.email,
        password: taBase64.encode(newStr)
    }
    var domainList=[]
    return(dispatch)=>{
        dispatch(loginInit())
      fetch(Globals.API_ROOT_URL_MASTER + `/login`, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(arr)
    }).then(response => {
          return response.json();

        }).then((json) => {
          if (typeof json.data !==  'undefined') {
            //   to store the user credentials into cookie 
              var rememberme = utils.readCookie("rememberme");
              if(data.remebermecheck==true && rememberme==''){
                  utils.storeCredentials(data.email,data.password,data.remebermecheck)
              }
            //   delete the credentials from the cookie 
              else if(data.remebermecheck==false && rememberme!==''){
                utils.deleteCredentials();
              }
            if(json.data.length>0){
                for(var i=0;i<json.data.length;i++){
                  domainList.push(json.data[i].domain)
                }
              }
              dispatch(loginsuccess(domainList,arr))
              if(domainList.length ==1){
                  var ObjToSend={
                      "domainList":domainList[0],
                  }
                  console.log('111')
                  dispatch(authUser(ObjToSend,arr,ip_address))
              }
           }else{
              utils.handleSessionError(json)
              if(typeof json.error !==  'undefined'){
                  if(json.error.email!==undefined){
                    dispatch(loginError())
                    notie.alert('error', json.error.email, 3);
                  }else{
                    dispatch(loginError())
                    notie.alert('error', json.error, 3);
                  }
                }
                else{
                    dispatch(loginError())
                    notie.alert('error', json, 3); 
                }
              
           }
        }).catch(err => {
          throw err;
        });
    }

}
 /**
 * Handle SSO login form submit
 * @author Yamin
 * Post
 **/   
 export function ssoLogin(data){
    return dispatch => {
            dispatch(ssoLoginInit())
            fetch(
              Globals.API_ROOT_URL_MASTER +`/sso/url?sso_name=${data.sso_name}&device=web`,{
                  headers: utils.setApiHeaders('get')
             })
              .then(response => response.json())
              .then(json => {
                
                if(json.code == 200){
                    //console.log('json.url');
                    //window.location.replace(json.url);
                    window.location.href = json.url;
                    //browserHistory.push(json.url)
                   // dispatch(ssoLoginSuccess())
                }else{
                    dispatch(ssoLoginFail())
                    notie.alert('error',json.error,3)
                }
              })
              .catch(err => {
                throw err
              })
    }
}
export function ssoLoginInit(){
    return{
        type:types.SSO_LOGIN_INIT
    }
}
export function ssoLoginSuccess(){
    return{
        type:types.SSO_LOGIN_SUCCESS
    }
}
export function ssoLoginFail(){
    return{
        type:types.SSO_LOGIN_FAIL
    }
}
