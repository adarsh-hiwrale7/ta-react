import * as types from '../actionTypes'
import Globals from '../../Globals'
import { browserHistory } from 'react-router'
import * as utils from '../../utils/utils';
//fetch folder for category popup for unarchive and move asset
export function fetchPopupFolders (catID) {
  if (!catID) return

  return dispatch => {
    fetch(
      Globals.API_ROOT_URL +
        `/category/show/${catID}`, {
          headers: utils.setApiHeaders('get'),
        })
      .then(response => response.json())
      .then(json => {
        // if json.data consists of array, means we have got folders
        if (Array.isArray(json.data) && json.data.length > 0) {
          dispatch(fetchPopupFoldersSuccess(json.data))
        } else {
          utils.handleSessionError(json)
          dispatch(fetchPopupFoldersSuccess([]))
          
        }
      })
      .catch(err => {
        throw err
      })
  }
}

export function fetchPopupFoldersSuccess (folderData) {
  return {
    type: types.FETCH_FOLDER_POPUP,
    popupFolder: folderData
  }
}
