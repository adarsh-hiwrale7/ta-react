import * as types from '../actionTypes'
import Globals from '../../Globals'
import { browserHistory } from 'react-router'
import notie from 'notie/dist/notie.js'
import * as utils from '../../utils/utils';
import ReactRouter from '../../components/Chunks/ChunkReactRouter';
export function assetsCreationInitiate () {
  return {
    type: types.ASSETS_CREATING
  }
}

export function assetUploadingStart () {
  return {
    type: types.ASSETS_UPLOADING_START
  }

}

// export function assetsLoading () {
//   return {
//     type: types.ASSETS_LOADING
//   }
// }
export function fetchUsersFilesSuccessNoAssets(){
  return{
    type: types.NO_ASSETS
  }
}

export function creatingNewAsset () {
  return {
    type: types.ASSETS_CREATING_NEW_ASSET
  }
}

export function creatingNewAssetEditor () {
  return {
    type: types.ASSETS_CREATING_NEW_ASSET_EDITOR
  }
}


export function assetUploadProgress (file, percent) {
  return {
    type: types.ASSETS_UPLOAD_PROGRESS,
    file,
    percent
  }
}

export function fetchCats (shouldFetchAssets) {
  return dispatch => {
    fetch(Globals.API_ROOT_URL + `/category`, {
        headers: utils.setApiHeaders('get'),
		  })
      .then(response => response.json())
      .then(json => {
        if (Array.isArray(json.data) && json.data.length > 0) {
          var currCatIdentity = json.data[0].identity
          dispatch(fetchCatsSuccess(json.data))
          if(shouldFetchAssets) {
            dispatch(fetchFolders(currCatIdentity))
            dispatch(fetchFiles(currCatIdentity, null, 20, 1, true))
          }

        } else {
          utils.handleSessionError(json)
          dispatch(fetchCatsSuccess([]))
        }
      })
      .catch(err => {
        throw err
      })
  }
}

 export function fetchCatCampaignFolders() {
  return dispatch => {
    fetch(Globals.API_ROOT_URL + `/category/campaign-folders`, {
          headers: utils.setApiHeaders('get'),
        })
      .then(response => response.json())
      .then(json => {
        if (Array.isArray(json.data) && json.data.length > 0) {
          dispatch(fetchCatCampaignsSuccess(json.data))
        } else {
          utils.handleSessionError(json)
          dispatch(fetchCatCampaignsSuccess([]))
        }
      })
      .catch(err => {
        throw err
      })
  }
 }
/**
 * @author disha  
 * to fetch asset custom feed folder name
 */
 export function fetchCatCustomFeedFolders() {
  return dispatch => {
    fetch(Globals.API_ROOT_URL + `/category/custom-folders `, {
        headers: utils.setApiHeaders('get'),
      })
      .then(response => response.json())
      .then(json => {
        if (Array.isArray(json.data) && json.data.length > 0) {
          dispatch(fetchCatCustomFeedSuccess(json.data))
        } else {
          utils.handleSessionError(json)
          dispatch(fetchCatCustomFeedSuccess([]))
        }
      })
      .catch(err => {
        throw err
      })
  }
 }
 export function loadMoreAssets(){
   return{
     type:types.LOAD_MORE_ASSETS
   }
 }

export function fetchFolders (catID,searchValue=null) {
  //the below conditions are for checking the Category ID and search value So according to that API will be called
  if(catID==undefined){
    catID="rpPDp"
  }
  if (!catID && searchValue=="") {catID=""}
  else if (catID!=="" && (searchValue !== null && searchValue !== ""))
  {
    return dispatch => {
      dispatch(fetchFoldersInit());
      var encURL=encodeURIComponent(searchValue);
      fetch(
        Globals.API_ROOT_URL +
        `/category/show/${catID}?search=${encURL}`, {
          headers: utils.setApiHeaders('get')
        })
        .then(response => response.json())
        .then(json => {
          // if json.data consists of array, means we have got folders
          if (Array.isArray(json.data) && json.data.length > 0) {
            dispatch(fetchFoldersSuccess(json.data))
          } else {
            // dispatch(assetsLoading())
            utils.handleSessionError(json)
            dispatch(fetchFoldersSuccess([]))
  
          }
        })
        .catch(err => {
          throw err
        })
    }

  }
  else if((searchValue == null || searchValue == ""))
  {
      return dispatch => {
        dispatch(fetchFoldersInit());
        fetch(
          Globals.API_ROOT_URL +
            `/category/show/${catID}`, {
              headers: utils.setApiHeaders('get'),
            })
          .then(response => response.json())
          .then(json => {
            // if json.data consists of array, means we have got folders
            if (Array.isArray(json.data) && json.data.length > 0) {
              dispatch(fetchFoldersSuccess(json.data))
            } else {
              // dispatch(assetsLoading())
              utils.handleSessionError(json)
              dispatch(fetchFoldersSuccess([]))
            }
          })
          .catch(err => {
            throw err
          })
      }
    }
}

export function fetchFoldersInit(){
  return {
    type: types.FETCH_FOLDERS_INIT
  }
}

export function fetchAssetsByHashSuccess (assets) {
  return {
    type: types.FETCH_ASSETS_BY_HASH,
    assets
  }
}
export function fetchAssetsByHashNotFound (msg) {
  return {
    type: types.FETCH_ASSETS_BY_HASH_NOT_FOUND,
    msg
  }
}

export  function fetchHashAssetStart(){
  return{
    type: types.FETCH_HASH_INIT
  }
}

export function fetchAssetsByHash(hash) {
  return dispatch => {
    dispatch(fetchHashAssetStart());
    fetch(Globals.API_ROOT_URL +`/asset?hashtag=${hash}`, {
        headers: utils.setApiHeaders('get'),
      })
      .then(response => response.json())
      .then(json => {
        // if json.data consists of array, means we have got folders
        if (Array.isArray(json.data) && json.data.length > 0) {
          dispatch(fetchAssetsByHashSuccess(json.data))
        } else {
          // dispatch(assetsLoading())
          utils.handleSessionError(json)
          dispatch(fetchAssetsByHashNotFound(json.data.message))

        }
      })
      .catch(err => {
        throw err
      })
  }
}

export function fetchFiles (catID,folderID,maxItems,currPage,resetFilesArray = false,searchValue=null,gSearch=null,gSearchData=null) {
  var encodeInput = encodeURIComponent(gSearch);
  // no catID and folderID? get them and come back
  if(!catID){
    catID="rpPDp"
  }

  if (!catID && !folderID) return
  // If folderId/catID is null or falsey value, set it to empty string
  if (!folderID) {
    folderID = ''
  } else {
    folderID = '/' + folderID
  }
  if (!catID){ catID = '';}
  if((catID!==undefined || catID!=="") && (searchValue == null || searchValue == "") && gSearch==null)
  {
    var url = Globals.API_ROOT_URL + `/asset/${catID}${folderID}?limit=${maxItems}&page=${currPage}`
  }
  else if((searchValue!==null || searchValue!=="") && catID!==undefined && gSearch == null)
  {
    var encURL=encodeURIComponent(searchValue);
    // return
    var url = Globals.API_ROOT_URL + `/asset/${catID}${folderID}?limit=${maxItems}&page=${currPage}&search=${encURL}`
  }
  else if(gSearch !== null)
  {
    var url = Globals.API_ROOT_URL + `/globalSearch?search=${encodeInput}&type=asset&page=${currPage}&limit=20`
  }
  return (dispatch) => {
    dispatch(fetchFilesStart())
    
    fetch(url, {
        headers: utils.setApiHeaders('get'),
      })
      .then(response => response.json())
      .then(json => {
        if (resetFilesArray) {
          dispatch(resetFiles())
        }
        if (json.data instanceof Array && json.data.length > 0) {
          dispatch(fetchFilesSuccess(json,searchValue,gSearch,gSearchData)) 
          if(json.meta.pagination.current_page < json.meta.pagination.total_pages){
            dispatch(loadMoreAssets())
          }
        } else {
          utils.handleSessionError(json)
          dispatch(fetchFilesSuccessNoAssets())
        }
      })
      .catch(err => {
        throw err
      })
  }
}

export function fetchUsersFiles ( currPage, resetFilesArray = false) {
  let maxItems = 20

  return dispatch => {
    dispatch(fetchFilesStart())

    fetch(Globals.API_ROOT_URL +`/asset-user?limit=${maxItems}&page=${currPage}`, {
        headers: utils.setApiHeaders('get'),
      })
      .then(response => response.json())
      .then(json => {
        if (resetFilesArray) {
          dispatch(resetFiles())
        }

        if (json.data instanceof Array && json.data.length > 0) {
          dispatch(fetchUsersFilesSuccess(json))
          if(json.meta.pagination.current_page < json.meta.pagination.total_pages){
            dispatch(loadMoreAssets())
          }
        } else {
          utils.handleSessionError(json)
          dispatch(fetchFilesSuccessNoAssets())
        }
      })
      .catch(err => {
        throw err
      })
  }
}

export function assetsCreate (fields, opts) {
  if (!fields.catID) fields.catID = ''
  if (!fields.folderID) fields.folderID = ''

  return dispatch => {
    let tags = []
    var assetTags

    if (typeof fields.tags !== 'undefined') {
      for (var t = 0; t < fields.tags.length; t++) {
        tags.push(fields.tags[t].label)
      }
    }

    if (tags.length) {
      assetTags = {
        asset: tags
      }
    }

    var jsonBody = {
      title: fields.title,
      media_url: fields.fileUrl,
      thumbnail_url: fields.thumbUrl,
      folder_id: fields.folderID,
      category_id: fields.catID,
      campaign_id: fields.campID,
      // tags: assetTags
      detail:fields.description,
      user_tag:fields.user_tag
    }
    dispatch(creatingNewAssetEditor())

    fetch(Globals.API_ROOT_URL +`/asset`,{
        method: 'POST',
        headers: utils.setApiHeaders('post'),
        body: JSON.stringify(jsonBody)
      }
    )
      .then(response => {
        // if (response.status !== '200') {
        //   dispatch(assetsCreationError(response.statusText))
        // }
        return response.json()
      })
      .then(json => {
        if (json.code == 200) {
          notie.alert('success', json.success, 3);
      //    dispatch(resetCreateAsset())
          dispatch(assetsCreationSuccess(json.success))
          dispatch(
            fetchFiles(fields.catID, fields.folderID, opts.maxItems, 1, true)
          )
        }
        if(typeof json.error !== 'undefined'){
        //  dispatch(resetCreateAsset())
        utils.handleSessionError(json)
          dispatch(assetsCreationError(json.error.detail[0]))
        }
       // dispatch(assetsCreationSuccess(json))
        // dispatch(
        //   fetchFiles(fields.catID, fields.folderID, opts.maxItems, 1, true)
        // )
      })
      .catch(err => {
        throw err
      })
  }
}

// to upload the drive Asset to s3 via API
export function uploadDriveAsset(arrData){
  return dispatch => {
  dispatch(uploadDriveAssetInit());
  fetch(
    Globals.API_ROOT_URL +
      `/asset/download`,{
        method: 'POST',
        headers: utils.setApiHeaders('post'),
        body: JSON.stringify(arrData)
      }
  )
  .then(response => {
        return response.json();
      })
    .then(json => {
      if(json.code==200){
        dispatch(uploadDriveAssetsuccess(json.data,arrData));
      }else if(json.code==422){
        notie.alert('error','problem in uploading your file',3);
        dispatch(uploadDriveAssetError());
      }else{
      }
    })
    .catch(err => {
      throw err
    })
  }
}

export function uploadDriveAssetInit(){
  return{
    type: types.UPLOAD_DRIVE_ASSET_FILE_INIT
  }
}
export function uploadDriveAssetsuccess(data,driveFile){
  return{
    type: types.UPLOAD_DRIVE_ASSET_FILE_SUCCESS,
    assetS3URL: data.file_path,
    DriveFileData : driveFile
  }
}

export function uploadDriveAssetError(){
  return{
  type: types.UPLOAD_DRIVE_ASSET_FILE_ERROR
  }
}

export function uploadDriveAssetDone(){
  return{
    type: types.UPLOAD_DRIVE_ASSET_FILE_DONE
    }
}

// api for multiple assets selection and single asset selection
export function multiAssetCreate (fields, opts,gSearch=null,flag_gPopup=null,fromDrive=false,isFirstAsset=false,uploadForMyAsset= false) {
  return dispatch => {
      var data=[]
        fields.map((fvalue,i)=>{
          var assetData = fvalue;
          if (!fvalue.catID) fvalue.catID = ''
          if (!fvalue.folderID) fvalue.folderID = ''
          let tags = []
          var assetTags

          if (typeof fvalue.tags !== 'undefined') {
            for (var t = 0; t < fvalue.tags.length; t++) {
              tags.push(fvalue.tags[t].label)
            }
          }

          if (tags.length) {
            assetTags = {
              asset: tags
            }
          }
          var jsonBody = {
            title: fvalue.title,
            media_url: fvalue.fileUrl,
            thumbnail_url: fvalue.thumbUrl,       
            category_id: fvalue.catID,
            media_size:fvalue.filesize,
            mime_type:fvalue.fileData.mimetype,
            file_extension:fvalue.fileData.type.split('/')[1],
            // tags: assetTags
            detail:fvalue.description,
            user_tag:fvalue.user_tag
          }
          if(typeof fvalue.fileData.duration !== 'undefined'){
            var ObjToAddDuration={
              'media_duration': fvalue.fileData.duration
            }
            Object.assign(jsonBody,ObjToAddDuration)
          }
          if(fvalue.height!==undefined&&fvalue.width!==undefined){
            jsonBody.height = fvalue.height
            jsonBody.width = fvalue.width
          }
          if(fromDrive==true){
            jsonBody.mime_type = fvalue.mime_type,
            jsonBody.file_extension = fvalue.extension
          }
          if(fvalue.campID !=='' && fvalue.campID !== null){
              //if campaign id has value then and then to pass id in jsonbody
              var ObjToAddCampId={
                'campaign_id': fvalue.campID
              }
              Object.assign(jsonBody,ObjToAddCampId)
            }
            if(fvalue.folderID !=='' && fvalue.folderID !== null){
              var ObjToAddfolderId={
                'folder_id': fvalue.folderID
              }
              Object.assign(jsonBody,ObjToAddfolderId)
            }
            
          data.push(jsonBody)
        })
        var innerdata={
          data
        }
        dispatch(creatingNewAsset());
    fetch(
      Globals.API_ROOT_URL +
        `/asset/multiple`,
      {
        method: 'POST',
        headers: utils.setApiHeaders('post'),
        body: JSON.stringify(innerdata)
      }
    )
      .then(response => response.json())
      .then(json => {
        if (json.code == 200) {
          if(isFirstAsset == true){
            var meta = {URL: "/assets"};
            userpilot.track("First Asset creation event", meta);
            }
          notie.alert('success', json.success, 3);
         // dispatch(resetCreateAsset())
          dispatch(uploadDriveAssetDone())
          dispatch(assetsCreationSuccess(json.success))
          if(opts.campaignId !== "" && opts.campaignId !== null){
            if(flag_gPopup!==null)
           // browserHistory.push('/campaigns?asset=true&campaignid='+opts.campaignId)
              browserHistory.push('/campaigns')
            else
            {
            browserHistory.push('/search/campaign')
            }
          }
          if(gSearch !== null)
            if(uploadForMyAsset == true ){
              dispatch(fetchUsersFiles(1));
            }else{
              dispatch( fetchFiles(fields[0].catID, fields[0].folderID, opts.maxItems, 1, true, null, gSearch) )
            }
          else
          if(uploadForMyAsset == true ){
            dispatch(fetchUsersFiles(1));
          }else{
            dispatch( fetchFiles(fields[0].catID, fields[0].folderID, opts.maxItems, 1, true) )
          }
        }
        if(typeof json.error !== 'undefined'){
        //  dispatch(resetCreateAsset())
        document.body.classList.remove('overlay');
        utils.handleSessionError(json)
          //dispatch(assetsCreationError(json.error.detail[0]))
          dispatch(assetsCreationError('Problem in uploading asset'))
        }
       // dispatch(assetsCreationSuccess(json))
        // dispatch(
        //   fetchFiles(fields.catID, fields.folderID, opts.maxItems, 1, true)
        // )
      })
      .catch(err => {
        throw err
      })
  }
}


export function appendNewFolder(folder) {
  return {
    type: types.APPEND_NEW_FOLDER,
    folder
  }
}

export function appendNewCat(cat) {
  return {
    type: types.APPEND_NEW_CAT,
    cat
  }
}

export function assetsCreationSuccess (createdAssetInfo) {
  // var message = "Your post was added successfully";
  //   message = "Your asset was uploaded successfully and sent for moderation!"

  return {
    type: types.ASSETS_CREATION_SUCCESS,
    createdAssetInfo: createdAssetInfo
  }
}

export function fetchFoldersSuccess (folders) {
  return {
    type: types.ASSETS_FETCH_FOLDERS_SUCCESS,
    folders: folders
  }
}

export function fetchCatsSuccess (cats) {
  return {
    type: types.ASSETS_FETCH_CATS_SUCCESS,
    cats: cats
  }
}

export function fetchCatCampaignsSuccess (campaigns) {
  return {
    type: types.ASSETS_FETCH_CATS_CAMPAIGN_SUCCESS,
    campaigns: campaigns
  }
}
export function fetchCatCustomFeedSuccess(data){
  return{
    type:types.FETCH_CAT_CUSTOM_FEED_SUCCESS,
    customFeed:data
  }
}
export function fetchFilesStart () {
  return {
    type: types.ASSETS_FETCH_FILES_START
  }
}

export function resetFiles () {
  return {
    type: types.ASSETS_FILES_RESET
  }
}

export function resetFolders () {
  return {
    type: types.ASSETS_FOLDERS_RESET
  }
}

export function fetchInitialUserFiles(){
  return dispatch =>{
    fetch(Globals.API_ROOT_URL +`/asset-user?limit=10&page=1`, {
        headers: utils.setApiHeaders('get'),
      })
      .then(response => response.json())
      .then(json => {
        if (json.data instanceof Array && json.data.length > 0) {
          dispatch(assetsFetchInitialFiles(json.data))
          
        }else{
          dispatch(assetsInitialNoFiles())
          
        }

      })
    }
} 

export function assetsFetchInitialFiles(data){
  return{
    type: types.ASSETS_FETCH_INITIAL_FILES_SUCCESS,
    payload: data
  }
} 

export function assetsInitialNoFiles(){
  return{
    type: types.ASSETS_FETCH_INITIAL_FILES_NODATA
  }
}

export function fetchFilesSuccess (payload,search=null,gSearch=null,gSearchData) {
  if(search !== null)
  {
    return {
      type: types.ASSETS_FETCH_FILES_SUCCESS,
      payload : payload,
      search:search,
    }
  }
  else if(gSearch !==null)
  {
    return {
      type: types.ASSETS_FETCH_FILES_SUCCESS,
      payload : payload,
      gSearch:gSearch,
      gSearchData:gSearchData
    }
  }
  else
  {
    return {
      type: types.ASSETS_FETCH_FILES_SUCCESS,
      payload : payload
    }
  }
}

export function fetchUsersFilesSuccess (payload) {
  return {
    type: types.ASSETS_FETCH_USERS_FILES_SUCCESS,
    payload
  }
}

export function fetchFilesSuccessNoAssets () {
  return {
    type: types.ASSETS_NO_FILES_FOUND
  }
}

export function assetsCreationError (statusText) {
  if (typeof statusText !== 'undefined') {
    notie.alert('error', statusText.toString(), 3)
  }
  return {
    type: types.ASSETS_CREATION_ERROR,
    createdAssetInfo: statusText
  }
}

//archive asset
export function archiveAsset(selectedAssetArray,catID,folderID,maxItems){
    return dispatch => {
        notie.confirm(
              `Are you sure you want to archive asset?`,
              'Yes',
              'No',
              function () {
                  var assetIdArray = [];
                  selectedAssetArray.map(function (selectedAsset) {
                      assetIdArray.push(selectedAsset.identity)
                  });

                  dispatch(archiveAssetInit())
                  var arr = {
                      'asset_id': assetIdArray,
                      'action': 'archive',
                  }

                    fetch(
                      Globals.API_ROOT_URL +
                         `/asset/archive`,{
                          method: 'POST',
                          headers: utils.setApiHeaders('post'),
                          body: JSON.stringify(arr)
                        }
                    )
                    .then(response => {
                        if (response.status == "200") {
                          dispatch(archiveAssetSuccess());
                          dispatch(fetchFiles(catID, folderID, maxItems, 1, true));
                        }else{
                          dispatch(archiveAssetFail());
                        }
                        return response.json();
                    })
                    .then(json => {
                      if (typeof json.message !== "undefined") {
                        notie.alert('success', json.message, 3);
                      } else {
                        utils.handleSessionError(json)
                        notie.alert('error', json.error, 3);
                      }
                    })
                    .catch(err => {
                      throw err
                    })
              },
              function () { }
              )

  }
}
export function archiveAssetInit(){
  return {
    type: types.ARCHIVE_ASSET_INIT,
  }
}
export function resetArchiveUnarchiveFlag(){
  return {
    type: types.RESET_ARCHIVE_UNARCHIVE_FLAG
  }
}
export function archiveAssetSuccess() {
  return {
    type: types.ARCHIVE_ASSET_SUCCESS,
  }
}

export function archiveAssetFail() {
  return {
    type: types.ARCHIVE_ASSET_FAIL,
  }
}

//archive folder
export function archiveFolder(selectedFolderArray,catID,folderID,maxItems){
    return dispatch => {
          notie.confirm(
              `Are you sure you want to archive folder?`,
              'Yes',
              'No',
              function () {
                  var folderIdArray = [];
                  selectedFolderArray.map(function (selectedFolder) {
                      folderIdArray.push(selectedFolder)
                  });
                  var arr = {
                      'folder_identity': folderIdArray,
                      'action': 'archive',
                  }
                  fetch(
                    Globals.API_ROOT_URL +
                       `/category/archive`,{
                        method: 'POST',
                        headers: utils.setApiHeaders('post'),
                        body: JSON.stringify(arr)
                      }
                  )
                  .then(response => {
                      if (response.status == "200") {
                        dispatch(archiveFolderSuccess());
                        dispatch(fetchFolders(catID));
                      }else{
                        dispatch(archiveFolderFail());
                      }
                      return response.json();
                  })
                  .then(json => {

                    if (typeof json.message !== "undefined") {
                      notie.alert('success', json.message, 3);
                    } else {
                      utils.handleSessionError(json)
                      notie.alert('error', 'Problem in archive folder', 3);
                    }
                  })
                  .catch(err => {
                    throw err
                  })
              },
              function () { }
            )
  }
}
export function archiveFolderInit(){
  return {
    type: types.ARCHIVE_FOLDER_INIT,
  }
}

export function archiveFolderSuccess() {
  return {
    type: types.ARCHIVE_FOLDER_SUCCESS,
  }
}

export function archiveFolderFail() {
  return {
    type: types.ARCHIVE_FOLDER_FAIL,
  }
}

//get archive files
export function fetchArchiveFiles(){
    return dispatch => {

            fetch(
              Globals.API_ROOT_URL +
                `/asset/archiveList`, {
                  headers: utils.setApiHeaders('get')
             })
              .then(response => response.json())
              .then(json => {
                if (Array.isArray(json.data) && json.data.length > 0) {
                  dispatch(fetchArchiveFilesSuccess(json.data))
                } else {
                  utils.handleSessionError(json)
                  dispatch(fetchArchiveFilesFail())

                }
              })
              .catch(err => {
                throw err
              })
    }
}

export function fetchArchiveFilesSuccess(AssetData) {
  return {
    type: types.FETCH_ARCHIVE_FILE_SUCCESS,
    archiveAsset: AssetData
  }
}

export function fetchArchiveFilesFail() {
  return {
    type: types.FETCH_ARCHIVE_ASSET_FAIL,
  }
}

//get archive folder
export function fetchArchiveFolders(){
    return dispatch => {

            fetch(
              Globals.API_ROOT_URL +
                `/category/archive`, {
                  headers: utils.setApiHeaders('get'),
             })
              .then(response => response.json())
              .then(json => {
                if (typeof json.data !== "undefined") {
                  dispatch(fetchArchiveFoldersSuccess(json.data))
                } else {
                  utils.handleSessionError(json)
                  dispatch(fetchArchiveFoldersFail())

                }
              })
              .catch(err => {
                throw err
              })
    }
}

export function fetchArchiveFoldersSuccess(folderData) {
  return {
    type: types.FETCH_ARCHIVE_FOLDER_SUCCESS,
    archiveFolder: folderData
  }
}

export function fetchArchiveFoldersFail() {
  return {
    type: types.FETCH_ARCHIVE_FOLDER_FAIL,
  }
}

export function fetchArchiveCategory(){
    return dispatch => {
        fetch(
              Globals.API_ROOT_URL +
                `/category/fixed-category`, {
                  headers: utils.setApiHeaders('get'),
             })
              .then(response => response.json())
              .then(json => {
                if (typeof json.data !== "undefined") {
                  dispatch(fetchArchiveCategorySuccess(json.data))
                }else if(typeof json.message !== "undefined"){
                    dispatch(fetchArchiveCategoryFail())
                }else {
                  notie.alert('error', json.error, 3);
                  utils.handleSessionError(json)
                  dispatch(fetchArchiveCategoryFail())

                }
              })
              .catch(err => {
                throw err
              })
       }
}

export function fetchArchiveCategorySuccess(categoryData) {
  return {
    type: types.FETCH_ARCHIVE_CATEGORY_SUCCESS,
    archiveCategory: categoryData
  }
}

export function fetchArchiveCategoryFail() {
  return {
    type: types.FETCH_ARCHIVE_CATEGORY_FAIL,
  }
}
//unarchive asset
export function unArchiveAsset(selectedAssetArray,catID,folderID,maxItems,selectedMoveFolder){
    return dispatch => {
        notie.confirm(
              `Are you sure you want to unarchive asset?`,
              'Yes',
              'No',
              function () {
                  var assetIdArray = [];
                  selectedAssetArray.map(function (selectedAsset) {
                      assetIdArray.push(selectedAsset.identity)
                  });

                  dispatch(unArchiveAssetInit())
                  var arr = {
                      'asset_id': assetIdArray,
                      'action': 'unarchive',
                      'restore_category_id': selectedMoveFolder
                  }

                    fetch(
                      Globals.API_ROOT_URL +
                         `/asset/archive`,
                         {
                          method: 'POST',
                          headers: utils.setApiHeaders('post'),
                          body: JSON.stringify(arr)
                        }
                    )
                    .then(response => {
                        if (response.status == "200") {
                          dispatch(unArchiveAssetSuccess());
                        }else{
                          dispatch(unArchiveAssetFail());
                        }
                        dispatch(fetchFiles(catID, folderID, maxItems, 1, true));
                        return response.json();
                    })
                    .then(json => {
                      if (typeof json.message !== "undefined") {
                        notie.alert('success', json.message, 3);
                      } else {
                        utils.handleSessionError(json)
                        notie.alert('error', json.error, 3);
                      }
                    })
                    .catch(err => {
                      throw err
                    })
              },
              function () { }
              )

  }
}
export function unArchiveAssetInit(){
  return {
    type: types.UNARCHIVE_ASSET_INIT,
  }
}

export function unArchiveAssetSuccess() {
  return {
    type: types.UNARCHIVE_ASSET_SUCCESS,
  }
}

export function unArchiveAssetFail() {
  return {
    type: types.UNARCHIVE_ASSET_FAIL,
  }
}

//unArchive folder
export function unArchiveFolder(selectedFolderArray,catID,folderID,maxItems,selectedMoveFolder){
    return dispatch => {
          notie.confirm(
              `Are you sure you want to unarchive folder?`,
              'Yes',
              'No',
              function () {
                  var folderIdArray = [];
                  selectedFolderArray.map(function (selectedFolder) {
                      folderIdArray.push(selectedFolder)
                  });
                  var arr = {
                      'folder_identity': folderIdArray,
                      'action': 'unarchive',
                      'restore_category_id': selectedMoveFolder
                  }
                  fetch(
                    Globals.API_ROOT_URL +
                       `/category/archive`,{
                        method: 'POST',
                        headers: utils.setApiHeaders('post'),
                        body: JSON.stringify(arr)
                      }
                  )
                  .then(response => {
                      if (response.status == "200") {
                        dispatch(unArchiveFolderSuccess());
                      }else{
                        dispatch(unArchiveFolderFail());
                      }
                      dispatch(fetchFolders(catID));
                      return response.json();
                  })
                  .then(json => {

                    if (typeof json.message !== "undefined") {
                      notie.alert('success', json.message, 3);
                    } else {
                      utils.handleSessionError(json)
                      notie.alert('error', 'Problem in unarchive folder', 3);
                    }
                  })
                  .catch(err => {
                    throw err
                  })
              },
              function () { }
            )
  }
}
export function unArchiveFolderInit(){
  return {
    type: types.UNARCHIVE_FOLDER_INIT,
  }
}

export function unArchiveFolderSuccess() {
  return {
    type: types.UNARCHIVE_FOLDER_SUCCESS,
  }
}

export function unArchiveFolderFail() {
  return {
    type: types.UNARCHIVE_FOLDER_FAIL,
  }
}

export function moveAsset(selectedAssetArray,catID,folderID,maxItems,selectedMoveFolder){
    return dispatch => {

          notie.confirm(
              `Are you sure you want to move asset?`,
              'Yes',
              'No',
              function () {
                  dispatch(moveAssetFolderInit())
                  var assetIdArray = [];
                  selectedAssetArray.map(function (selectedAsset) {
                      assetIdArray.push(selectedAsset.identity)
                  });
                  var arr = {
                      'asset_ids': assetIdArray,
                      'target_category_id': selectedMoveFolder,
                  }
                  fetch(
                    Globals.API_ROOT_URL +
                       `/asset/moveasset`,{
                        method: 'POST',
                        headers: utils.setApiHeaders('post'),
                        body: JSON.stringify(arr)
                      }
                  )
                  .then(response => {
                      if (response.status == "200") {
                        dispatch(moveAssetFolderSuccess());
                      }else{
                        dispatch(moveAssetFolderFail());
                      }
                      dispatch(fetchFiles(catID, folderID, maxItems, 1, true));
                      return response.json();
                  })
                  .then(json => {

                    if (typeof json.message !== "undefined") {
                      notie.alert('success', json.message, 3);
                    } else {
                      utils.handleSessionError(json)
                      notie.alert('error', 'Problem in moving asset', 3);
                    }
                  })
                  .catch(err => {
                    throw err
                  })
              },
              function () { }
            )
  }
}
export function moveAssetFolderInit(){
  return {
    type: types.MOVE_ASSET_FOLDER_INIT,
  }
}

export function moveAssetFolderSuccess() {
  return {
    type: types.MOVE_ASSET_FOLDER_SUCCESS,
  }
}

export function moveAssetFolderFail() {
  return {
    type: types.MOVE_ASSET_FOLDER_FAIL,
  }
}

//move folders
export function moveFolder(selectedFolderArray,catID,folderID,maxItems,selectedMoveFolder){
    return dispatch => {

          notie.confirm(
              `Are you sure you want to move folder?`,
              'Yes',
              'No',
              function () {

                  dispatch(moveAssetFolderInit())
                  var folderIdArray = [];
                  selectedFolderArray.map(function (selectedFolder) {
                      folderIdArray.push(selectedFolder)
                  });
                  var arr = {
                      'folder_identity': folderIdArray,
                      'target_category_id': selectedMoveFolder,
                  }
                  fetch(
                    Globals.API_ROOT_URL +
                       `/category/movefolder`,{
                        method: 'POST',
                        headers: utils.setApiHeaders('post'),
                        body: JSON.stringify(arr)
                      }
                  )
                  .then(response => {
                      if (response.status == "200") {
                        dispatch(moveAssetFolderSuccess());
                      }else{
                        dispatch(moveAssetFolderFail());
                      }
                      dispatch(fetchFolders(catID));
                      return response.json();
                  })
                  .then(json => {

                    if (typeof json.message !== "undefined") {
                      notie.alert('success', json.message, 3);
                    } else {
                      utils.handleSessionError(json)
                      notie.alert('error', 'Problem in moving folder', 3);
                    }
                  })
                  .catch(err => {
                    throw err
                  })
              },
              function () { }
            )
  }
}


export function assetLinkClick(){
   return {
    type: types.ASSET_LINK_CLICK,
  }
}

//fetch breadcrums
export function fetchBreadCrums(currentPosition){ 
    return dispatch => {
        fetch(Globals.API_ROOT_URL +`/category/breadcrum?category_id=${currentPosition}`, {
              headers: utils.setApiHeaders('get'),
              })
              .then(response => response.json())
              .then(json => {
                if (typeof json.data !== "undefined") {
                  dispatch(fetchBreadCrumsSuccess(json.data))
                }else{
                  utils.handleSessionError(json)
                }
              })
              .catch(err => {
                throw err
              })
       }
}

export function fetchBreadCrumsSuccess(breadCrumData){
   return {
    type: types.FETCH_BREADCRUM_SUCCESS,
    breadCrumData: breadCrumData
  }
}

//reset breadcrums
export function resetBreadCrums(){
   return {
    type: types.RESET_BREADCRUM,
  }
}

export function uploadedAssetInBuffer(item) {
  return {
    type: types.UPLOAD_ASSET_IN_BUFFER,
    item: item
  }
}

export function editAssetInBuffer(item,indexCount) {
  return {
    type: types.Edit_ASSET_IN_BUFFER,
    item: item,
    indexCount: indexCount
  }
}

export function resetuploadedAssetInBuffer(){
  return {
    type: types.RESET_UPLOAD_ASSET_IN_BUFFER
  }
}

export function resetCreateAsset(resetAssetList =false, cancelUpload){
  return {
    type: types.RESET_CREATE_ASSET,
    resetAssetList: resetAssetList,
    cancelUpload : cancelUpload
  }
}

export function opendEditor(){
  return {
    type: types.OPEN_EDITOR
  }
}
export function hideEditor(){
  return {
    type: types.HIDE_EDITOR
  }
}

/*export function store(formData){
      return {
        type: types.STORE_ASSET_DATA_BEFORE_UPLOAD,
        item: formData
      }
}
*/
