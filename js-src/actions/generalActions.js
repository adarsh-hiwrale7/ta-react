import * as types from '../actions/actionTypes'
import Globals from '../Globals'
import notie from 'notie/dist/notie.js'
import * as utils from '../utils/utils';
export function GetEventInfo(e) {
  return {
    type: types.EVENT_INFO,
    ev: e.target
  };
}

export function successWorldTime(data){
  return{
    type:types.SUCCESS_FETCH_WORLD_DATE_TIME,
    data:data
  }
}
export function worldTimeInit(){
  return{
    type:types.WORLD_TIME_INIT
  }
}
export function clientIpInit(){
  return{
    type:types.CLIENT_IP_INIT
  }
}
export function successClientIp(data){
  return{
    type:types.SUCCESS_CLIENT_IP,
    data:data
  }
}
export function errorClientIp(){
  notie.alert('error', 'There is a problem fetching ip!', 3);
  return{
    type:types.ERROR_CLIENT_IP
  }
}
/**
 * @author disha
 * to fetch internet time from api
 */
export function fetchWorldTime() {
  return (dispatch) => {
    dispatch(worldTimeInit())
    fetch('http://worldclockapi.com/api/json/utc/now')
    .then(response => response.json())  
    .then((json) => {
        if(json!==undefined){
          dispatch(successWorldTime(json))
        }
      
    })
    .catch(err => { throw err;
    });
  }
}
export function fetchS3SizeInit(){
  return{
    type:types.FETCH_S3_SIZE_INIT
  }
}
export function successFetchS3Size(data) {
  return {
      type: types.SUCCESS_FETCH_S3_SIZE,
      data: data
  }
}
export function noStorageSpace(message){
  notie.alert('error',message, 5);
  return {
    type: types.NO_STORAGE_SPACE,
  }
}
export function errorFetchS3Size(message){
  notie.alert('error',message, 5);
  return{
    type:types.ERROR_FETCH_S3_SIZE
  }
}
export function resetS3SizeFlag(){
    return{
      type:types.RESET_S3_SIZE_FLAG
    }
}
export function fetchS3Size(values) {
  return (dispatch) => {
    dispatch(fetchS3SizeInit());
    fetch(Globals.API_ROOT_URL + `/user/storageCheck`,{
      method: 'POST',
      headers: utils.setApiHeaders('post'),
      body: values!==undefined?JSON.stringify(values):''
    })
      .then(response => response.json())
      .then((json) => {
        if(json.code==200){
            dispatch(successFetchS3Size(json));
            dispatch(resetS3SizeFlag())
        }else{
          dispatch(resetS3SizeFlag())
          dispatch(errorFetchS3Size(json.message));
        }
      })
      .catch(err => { throw err; });
    }
  }
/**
 * @author disha
 * to fetch ip address of client machine
 */
export function fetchClientIp() {
  return (dispatch) => {
    dispatch(clientIpInit())
    fetch('https://ipapi.co/json')
    .then(response => response.json())  
    .then((json) => {
        if(json!==undefined){
          dispatch(successClientIp(json))
        }else{
          dispatch(errorClientIp())
        }
      
    })
    .catch(err => { throw err;
    });
  }
}
