import * as types from '../actionTypes'
import Globals from '../../Globals'
import { browserHistory } from 'react-router'
import notie from 'notie/dist/notie.js'
import * as utils from '../../utils/utils'
import * as feedActions from './feedActions';
import * as campaignActions from '../campaigns/campaignsActions';

import {fetchCalendarScheduled} from './CalendarActions';
// post create
export function postCreationInitiate () {
  return {
    type: types.POST_CREATING
  }
}

export function assetInPostUploadingStart () {
  return {
    type: types.ASSETS_IN_POST_UPLOADING_START
  }
}
export function uploadingComplete(){
  return{
    type:types.UPLOADING_COMPLETE
  }
}

/**
 * @author disha
 * to reset flag to disable/ enable button of createpost 
 */
export function postCreationErrorInitiate(){
  return{
    type:types.ERROR_POST_CREATE_INIT
  }
}

export function creatingNewPost () {
  return {
    type: types.POST_CREATING_NEW
  }
}
export function hidepostSuccess(){
  return {
    type: types.HIDE_POST_SUCCESS
  }
}
export function postCreationError (statusText) {
  if (typeof statusText !== 'undefined') {
    if(statusText.detail==undefined){
      notie.alert('error',statusText,3)
    }else{
      notie.alert('error', statusText.detail[0].toString(), 3)
    }
  }
  return {
    type: types.POST_CREATION_ERROR
  }
}

export function postCreationSuccess (values) {
  var message = "Your post was added successfully";
  if(values.type=="External") {
    message = "Your post was added successfully and sent for moderation."
  }
  return {
    type: types.POST_CREATION_SUCCESS,
    data: values
  }
}

export function uploadDriveAssetabort(){
  return{
    type: types.DRIVE_ASSET_ABORT
  }
}

export function uploadDriveAsset(arrData,discard){
  if(discard=="discard"){
    return dispatch => {
      dispatch(uploadDriveAssetabort())
    }
  }
  else{
  return dispatch => {
  dispatch(uploadDriveAssetInit());
  fetch(
    Globals.API_ROOT_URL +
      `/asset/download`,{
        method: 'POST',
        headers: utils.setApiHeaders('post'),
        body: JSON.stringify(arrData)
      }
  )
  .then(response => {
        return response.json();
      })
    .then(json => {
      if(json.code==200){
        if(Globals.tempSelection == true){
          Globals.tempSelection=false;
          // Globals.assetSelectCancel=false
          // var jsonData={
          //   file_path:null
          // }
          // dispatch(uploadDriveAssetsuccess(jsonData));
        }else{ 
          if(Globals.assetSelectCancel!==true){
          dispatch(uploadDriveAssetsuccess(json.data,arrData));
          }
        }
      }else if(json.code==422){
        notie.alert('error','problem in uploading your file',3);
        dispatch(uploadDriveAssetError());
      }else{
      }
    })
    .catch(err => {
      throw err
    })
  }}
}

export function uploadDriveAssetInit(){
  return{
    type: types.UPLOAD_DRIVE_ASSET_INIT
  }
}
export function uploadDriveAssetsuccess(data,fileobj){
  return{
    type: types.UPLOAD_DRIVE_ASSET_SUCCESS,
    assetS3URL: data,
    uploadedDriveFile : fileobj
  }
}

export function uploadDriveAssetError(){
  return{
  type: types.UPLOAD_DRIVE_ASSET_ERROR
  }
}

export function uploadDriveAssetDone(){
  return{
    type: types.UPLOAD_DRIVE_ASSET_DONE
    }
}

export function generateShortLinkInit(){
  return{
    type : types.GENERATELINK_INIT
  }
}

// generate utm short link 
export function generateUtmShortLink(url){
  return dispatch =>{
    var jsonBody ={
      "link": url[0].link
  }
  dispatch(generateShortLinkInit());
  fetch(`${Globals.API_ROOT_URL}/utm/generateUtm`, {
    method: 'POST',
    headers: utils.setApiHeaders('post'),
    body: JSON.stringify(jsonBody)
  })
  .then(response => response.json())
        .then( json =>{
          if(json.code==200){
            dispatch(generateUtmLinksuccess(json.data));
          }else{
            notie.alert('error','UTM link generation failed.',3)
            dispatch(generateUtmLinkFailed());
          }
        })
    }
}

export function generateUtmLinkFailed(){
  return {
    type : types.UTM_LINK_FAILED
  }
}
export function generateUtmLinksuccess(utmData){
  return{
    type:types.UTM_LINK_SUCCESS,
    data : utmData
  }
}

//call api for AI value on create post
export function getAIValue(description){
  return dispatch =>{
    var jsonBody ={
      "sentence": description
  }
  dispatch(getAIValueInit());
  fetch(`https://values.visibly.io/`, {
    method: 'POST',
    headers: utils.setApiHeaders('post'),
    body: JSON.stringify(jsonBody)
  })
  .then(response => response.json())
        .then( json =>{
          if(json.Result){
            dispatch(getAIValueSuccess(json.Result));
          }else{
           // notie.alert('error',json.Error,3)
            dispatch(getAIValueFailed(json.Error));
          }
        })
    }
}


export function getAIValueFailed(error){
  return {
    type : types.AI_VALUE_FAILED,
    error:error
  }
}
export function getAIValueSuccess(aiValue){
  return{
    type:types.AI_VALUE_SUCCESS,
    data : aiValue
  }
}
export function getAIValueInit(){
  return{
    type : types.AI_VALUE_INIT
  }
}
export function postCreate (values,isFirstPost=false, user_role="admin") {
  var  arr={
        feed_identity: values.feed_identity,
          type: values.type,
          isCampaign: values.isCampaign,
          latitude: values.latitude,
          longitude: values.longitude,
          media: values.media,
          feedStatus: values.feedStatus,
          tags: values.tags,
          schedule: values.schedule,
          isScheduledPost: values.isScheduledPost,
          user_tag: values.user_tag,
          link_preview: values.link_preview
  }

  if(values.utm_identities!==undefined){
    var ObjToAddValue={
      utm_identities : values.utm_identities
    }
    Object.assign(arr, ObjToAddValue)
  }

  if(values.value_identity!==undefined){
    var ObjToAddValue={
      value_identity : values.value_identity
    }
    Object.assign(arr, ObjToAddValue)
  }
  if (values.detail.trim() !== '') {
    //if desc has value then and then to pass detail of post in fieldValue
    var ObjToAddDescription = {
      detail: values.detail.trim()
    }
    Object.assign(arr, ObjToAddDescription)
  }
  if (values.campaign_identity !== undefined) {
    //if campaign id has value then and then to pass its value in api
    var ObjToAddcampaignID = {
      campaign_identity: values.campaign_identity
    }
    Object.assign(arr, ObjToAddcampaignID)
  }
  return dispatch => {
    fetch(Globals.API_ROOT_URL + `/post`, {
      method: 'POST',
      headers: utils.setApiHeaders('post'),
      body: JSON.stringify(arr)
    })
      .then(response => {
        return response.json()
      })
      .then(json => {
        if (json.code == 200) {
          if(isFirstPost == true){
           
            if(user_role == "guest")
            {
              var meta = {URL: `/feed/custom/live/${values.feed_identity}`};
            }
            else
            {
              var meta = {URL: `/feed/default/live/${values.feed_identity}`};
            }
          userpilot.track("First post creation event", meta);
          }
        

          dispatch(postCreationSuccess(values))
          dispatch(uploadDriveAssetDone())
          var responseMessage = json.message;
          notie.alert('success',responseMessage,5)
          // will redirect to the page for which post has been created 
          if(values.isCampaign==false){
            //check if create post is for campaign or not
            if (values.isScheduledPost==true) {
              //if it is schedule
              if(values.feedStatus !=="scheduled" ){
                //redirect to my scheduled page if post is creating from other page
                browserHistory.push('/feed/my/scheduled')                
              }else{
                //call api if post is creating form scheduled page
                dispatch(feedActions.fetchScheduled(1,10,false,true))
              }
            }
          switch (values.feedtype) {
            case 'custom':
              if(values.feed_identity!== "rpPDp" && values.feed_identity!=='' && values.feed_identity!== null && values.feed_identity!==undefined ){
                dispatch(feedActions.fetchCustomLive(values.feed_identity,1,10,false,true))
              }
              break 
            case 'my':
                  if(values.feedStatus =="live"){
                    //if feed status is live then to call api
                    if(values.isFromFilter == true ){
                      //if feed is creating from my/live and filter is selected 
                      dispatch(feedActions.filterMyFeedData('my',values.feedStatus,values.feed_identity,1,10,false,true))    
                    }else{
                      //if feed is creating from my/live and filter is not selected 
                      dispatch(feedActions.fetchLive(1,10,false,true))
                    }
                 }else{
                   //if feed status is scheduled then to call api
                  if(values.isFromFilter == true ){
                    //if filter is selected
                     if(values.isScheduledPost==true){
                      //if filter is selected and it is post is scheduled 
                      dispatch(feedActions.filterMyFeedData('my',values.feedStatus,values.feed_identity,1,10,false,true))    
                     }else{
                      //if filter is selected and post is not scheduled means its live post then to redirect page on live
                      browserHistory.push('/feed/my/live')  
                     }
                  }else{
                    //no filter
                    if(values.isScheduledPost==false){
                      //if you are on my/scheduled page and create live post then to redirect it to my/live
                      browserHistory.push('/feed/my/live')  
                    }else{
                      //if you are on my/scheduled page page and create scheduled post 
                      dispatch(feedActions.fetchScheduled(1,10,false,true))
                    }
                  }
                 }
            break 
            case 'default':
                dispatch(feedActions.fetchDefaultLive(1,10,false,true))
                break 
              default:
              browserHistory.push('/feed/default/live')   
            }
          }
          
        }
        if(typeof json.error !== 'undefined'){
          utils.handleSessionError(json)
          dispatch(postCreationErrorInitiate())
          dispatch(postCreationError(json.error))
        }
      })
      .catch(err => {
        throw err
        dispatch(postCreationError(response.statusText))
      })
  }
}


// post update
export function postUpdateInitiate () {
  return {
    type: types.POST_UPDATING
  }
}

export function postUpdationError (statusText) {
  if (typeof statusText !== 'undefined') {
    notie.alert('error', statusText.toString(), 3)
  }
  return {
    type: types.POST_UPDATION_ERROR
  }
}

export function postUpdationSuccess (values) {
  var message = "Your post was added successfully";
  return {
    type: types.POST_UPDATION_SUCCESS,
    data: values
  }
}
export function resetCalendarSuccessFlag()
{
  return{
    type:types.RESET_CALENDAR_SUCCESS_FLAG
  }
}
export function postUpdationInit()
{
  return{
    type:types.POST_UPDATE_INIT
  }
}

export function postUpdate (values,fromcalendar,moment,firsttimestamp,lasttimestamp) {
   var APIString=''
  return dispatch => {
    dispatch(postUpdationInit())
    if(fromcalendar==true){
      APIString = `${Globals.API_ROOT_URL}/post/publish/update-schedule-calendar`
    }
    else{
      APIString = `${Globals.API_ROOT_URL}/post/publish/update-schedule`
    }
    fetch(APIString, {
      method: 'PUT',
      headers: utils.setApiHeaders('put'),
      body: JSON.stringify(values)
    })
      .then(response => {
        return response.json()
      })
      .then(json => {
        if (json.code == 200) {
          if(fromcalendar==true){
            dispatch(fetchCalendarScheduled(1,10,null,moment,firsttimestamp,lasttimestamp))
            dispatch(postUpdationSuccess(values))
          }else{
            dispatch(feedActions.fetchScheduled(1,10,false,true))
            dispatch(postUpdationSuccess(values))
          }
          var responseMessage = json.message;
          notie.alert(
            'success',
            responseMessage
            ,
            3
          )
        }
        if(typeof json.error !== 'undefined'){
          utils.handleSessionError(json)
          dispatch(postUpdationError(json.error.detail[0]))
        }
      })
      .catch(err => {
        throw err
        dispatch(postUpdationError(response.statusText))
      })
  }
}
/**
 * to store data of campaign and pass it in post creation page
 * @param {*} values 
 */
export function campaignCreatePostData (values) {
  return {
    type: types.CAMPAIGN_CREATE_POST_DATA,
    data: values
  }
}
/**
 * remove store data of campaign 
 * @param {*} values 
 */
export function removeCampaignCreatePostData (values) {
  return {
    type: types.REMOVE_CAMPAIGN_CREATE_POST_DATA
  }
}

/**
 * @author Tejal Kukadiya
 * @param {edit curation value}
 */
export function editCurationInit(fields){
  return{
    type:types.EDIT_CUREATION_INIT,
    editCurationData: fields
  }
}

export function resetCurationEdit(){
  return{
    type:types.RESET_CURATION_EDIT
  }
}