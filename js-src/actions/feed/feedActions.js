import * as types from '../actionTypes';
import Globals from '../../Globals';
import notie from 'notie/dist/notie.js'
import * as utils from '../../utils/utils'
import { browserHistory } from 'react-router';
import {callLogoutApi} from '../loginActions';
import {searchsuccess} from '../searchActions' ;
import {fetchHashAssetStart} from '../assets/assetsActions';
import { toggleFilter , setFilterFlag} from '../header/headerActions';
//import store from '../../store/store';
var page_limit,
pageno,
feed_event_type,
event_array_var
export function resetPosts() {
  return {
    type: types.RESET_POSTS
  }
}
export function fetchInitData() {
  return {
    type: types.INITIAL_LOADING_DATA
  }
}
export function fetchRSSInit() {
  return {
    type: types.INITIAL_LOADING_RSS
  }
}

export function fetchRssError(data) {
  notie.alert(
    'error',
    data,
    5
  )
  return {
    type: types.FETCH_ERROR_RSS_DATA
  }
}
export function fetchJobError() {

  return {
    type: types.FETCH_ERROR_JOB_DATA
  }
}
export function fetchJobDataSuccess(data,isCallFromJobsPage=false){
  return{
    type:types.FETCH_SUCCESS_JOB_DATA,
    data:data,
    isCallFromJobsPage
  }
}

export function fetchRssDataSuccess(data,isCallFromRssPage=false){
  return{
    type:types.FETCH_SUCCESS_RSS_DATA,
    data:data,
    isCallFromRssPage
  }
}
export function initiatefetchDefaultLive() {
 return {
    type: types.INITIATE_FETCH_INTERNAL_LIVE
  };
}

export function initiateFetchInternalScheduled() {
 return {
    type: types.INITIATE_FETCH_INTERNAL_SCHEDULED
  };
}

export function initiateFetchExternalLive() {
 return {
    type: types.INITIATE_FETCH_EXTERNAL_LIVE
  };
}

export function initiateFetchExternalScheduled() {
 return {
    type: types.INITIATE_FETCH_EXTERNAL_SCHEDULED
  };
}

export function updateMetaInformation(metaInfo) {
  return {
    type: types.UPDATE_META_INFO,
    payload: metaInfo
  };
}

export function fecthInternalScheduledError() {
 return {
    type: types.FETCH_INTERNAL_SCHEDULED_ERROR
  };
}

export function fetchDefaultLiveError() {
 return {
    type: types.FETCH_DEFAULT_LIVE_ERROR
  };
}

export function fetchNoPostDefault() {
 return {
    type: types.FETCH_NOPOST_DEFAULT
  };
}
export function userScrollLoader(){
  return{
    type:types.USER_SCROLL_LOADING
  };
}
export function fetchNoPostExternal() {
 return {
    type: types.FETCH_NOPOST_EXTERNAL
  };
}
export function userScrollLoaderExternal(){
  return{
    type:types.USER_SCROLL_LOADING_EXTERNAL
  };
}
export function loadPostDataExternal(){
  return{
    type:types.LOAD_POSTDATA_EXTERNAL
  };
}
export function loadPostDataDefault(){
  return{
    type:types.LOAD_POSTDATA_DEFAULT
  };
}
/**
 * to call api for get list of Custom feed
 */
export function getFeedList(fetchOnlyExternalData=false){
  var feedtype=''
  if(fetchOnlyExternalData==true){
    feedtype=`&feed_type=external`
  }
  return (dispatch) => {
    dispatch(userScrollLoader());
    dispatch(initiateFetchCustomLive());
    dispatch(fetchHashAssetStart());
    fetch(Globals.API_ROOT_URL + `/feed?user_data=false${feedtype}`,{
      headers: utils.setApiHeaders('get')
    })
    .then(response =>
      {
          return response.json();
      })
    .then((json) => {
     var roleName =JSON.parse(localStorage.getItem('roles')) !== null? JSON.parse(localStorage.getItem('roles'))[0].role_name:''
     if(roleName=="guest" && json.data[1]==undefined){
        dispatch(callLogoutApi());
     }
      dispatch(fetchCustomFeedSuccess(json));
    })
    .catch(err => { throw err;
    //  dispatch(fecthInternalScheduledError())
  });
}
}

export function exitFeed(feedId){
    var array={
      feed_identity:{feedId}
    }
    return (dispatch) => {
      fetch(`${Globals.API_ROOT_URL}/feed?feed_identity=${feedId}`, {
            method: 'DELETE',
            headers: utils.setApiHeaders('delete'),
          })
        .then(response => response.json())
        .then((json) => {
          if(json.code==200){
            notie.alert('success', json.message, 5)
            dispatch(getFeedList())
          }
          else{
            notie.alert('error', json.message, 5)
          }
        })
    }

}

/**
 * to get the all post data of custom feed
 * @param {*} Feed_identity of custom feed
 */
export function fetchCustomLive(Feed_identity,currPage, limit,isPin = false,createPostNewdata=false,lastPostId=null,lastFeedType,jobcount,job_display,curation_display,is_configured,isJobfinished, curationcount) {

  return (dispatch) => {
    dispatch(initiateFetchCustomLive())
      var lastPostParam = '';
      if(lastPostId !== null){
          lastPostParam = '&post_publisher_identity='+lastPostId+'&get_data=down';
      }else{
        lastPostParam = '&page='+currPage;
      }
      fetch(Globals.API_ROOT_URL + `/post?feed_identity=${Feed_identity}&limit=${limit}`+lastPostParam,{
        headers: utils.setApiHeaders('get')
      })
      .then(response => response.json())
      .then((json) => {
        if(Array.isArray(json.data) && json.data.length > 0) {
          dispatch(storeLastPostId(json.data[Object.keys(json.data)[Object.keys(json.data).length - 1]]['publisher_identity']))
          //dispatch(initiateFetchCustomLive());
          dispatch(fetchCustomLiveSuccess(json.data,isPin,createPostNewdata));
          dispatch(updateMetaInformation(json.meta));
          if(json.meta.pagination.current_page < json.meta.pagination.total_pages){
          dispatch(loadPostDataCustom());}
          dispatch(fetchJobCuration(currPage,limit,lastFeedType,jobcount,job_display,curation_display,is_configured,isJobfinished, curationcount));
        }
        else {
          utils.handleSessionError(json);
          dispatch(fetchCustomLiveSuccess([]))
          if(json.meta.pagination.total==0){
          dispatch(fetchNoPostCustom())}
        }
      })
      .catch(err => { throw err;
        dispatch(fetchCustomLiveError())
      });
    }
}

export function fetchCustomLiveHash(hash,Feed_identity,currPage, limit,onPostsScroll,lastPostId) {
  return (dispatch) => 
  {

    dispatch(initiateFetchCustomLive())
      var lastPostParam = '';
      if(lastPostId !== null){
          lastPostParam = '&post_publisher_identity='+lastPostId+'&get_data=down';
      }else{
        lastPostParam = '&page='+currPage;
      }
      fetch(Globals.API_ROOT_URL + `/post?feed_identity=${Feed_identity}&limit=${limit}&hashtag=${hash}`+lastPostParam,{
        headers: utils.setApiHeaders('get')
      })
      .then(response => response.json())
      .then((json) => {
        if(Array.isArray(json.data) && json.data.length > 0) {
          dispatch(storeLastPostId(json.data[Object.keys(json.data)[Object.keys(json.data).length - 1]]['publisher_identity']));
          dispatch(fetchCustomLiveSuccess(json.data,false,false,true,onPostsScroll));
          dispatch(updateMetaInformation(json.meta));
          if(json.meta.pagination.current_page < json.meta.pagination.total_pages){
          dispatch(loadPostDataCustom());}
        }
        else {
          utils.handleSessionError(json);
          if(onPostsScroll == true && hash!== '' ){
            dispatch(fetchCustomLiveSuccess([],false,false,true,onPostsScroll))
          }
          else{
            dispatch(fetchCustomLiveSuccess([]))
          }
          if(json.meta.pagination.total==0){
          dispatch(fetchNoPostCustom())}
        }
      })
      .catch(err => { throw err;
        dispatch(fetchCustomLiveError())
      });
    }
}

export function fetchNoPostCustom() {
  return {
     type: types.FETCH_NOPOST_CUSTOM
   };
}

export function loadPostDataCustom(){
  return{
    type:types.LOAD_POSTDATA_CUSTOM
  };
}

export function fetchCustomLiveSuccess(data,isPin=false,createPostNewdata=false,hash=false ,onPostsScroll){
    return{
      type: types.FETCH_CUSTOM_LIVE_SUCCESS,
      payload: data,
      isPin: isPin,
      createPostNewdata:createPostNewdata,
      isHash:hash,
      onPostsScroll : onPostsScroll
    }
}

export function fetchCustomFeedSuccess(customFeed){
  return{
    type: types.FETCH_CUSTOM_FEED_SUCCESS,
    payload:customFeed.data,
    is_configured:customFeed.is_configure
  }
}

export function initiateFetchCustomLive() {
  return {
     type: types.INITIATE_FETCH_CUSTOM_LIVE
   };
 }


 export function filterMyFeedData(PostType,type,FeedId,currPage,limit,isPin = false,createPostNewdata=false,Type=null,search=null,CallFromScrolling=false,hash=null){
  var fromFilter=true;
  var hashtag=''
  if(type=="live"){
      hashtag = hash!==null && hash!=='' ? `&hashtag=${hash}`:''
    return (dispatch) => {
      CallFromScrolling !==true ?dispatch(initiateFetchMyLive()):''
        fetch(Globals.API_ROOT_URL + `/post/user?feed_identity=${FeedId}&published_status=published&page=${currPage}&limit=${limit}${hashtag}`,{
          headers: utils.setApiHeaders('get')
        })
        .then(response => response.json())
        .then((json) => {
          if(Array.isArray(json.data) && json.data.length > 0) {
            dispatch(initiateFetchMyLive());
            dispatch(toggleFilter(false));
            dispatch(setFilterFlag(true));
            dispatch(fetchMyLiveSuccess(json.data, isPin,createPostNewdata,fromFilter,currPage));
            dispatch(updateMetaInformation(json.meta));
            if(json.meta.pagination.current_page < json.meta.pagination.total_pages){
            dispatch(loadPostDataMyLive());}
          }
          else {
            utils.handleSessionError(json)
            dispatch(toggleFilter(false))
            dispatch(setFilterFlag(true));
            dispatch(fetchMyLiveSuccess([]))
            // if(json.meta.pagination.total==0){
              if(json.meta.pagination.total==0){
            dispatch(fetchNoPostMyLive())}
          }

        })
        .catch(err => { throw err;
          dispatch(fecthMyLiveError())
        });
      }
  }
  else if(type=="scheduled"){
    return (dispatch) => {
      dispatch(initiateFetchMyScheduled())
      fetch(Globals.API_ROOT_URL + `/post/user?feed_identity=${FeedId}&published_status=scheduled&page=${currPage}&limit=${limit}`,{
        headers: utils.setApiHeaders('get')
      })
      .then(response => response.json())
      .then((json) => {
        if(Array.isArray(json.data) && json.data.length > 0) {
          dispatch(initiateFetchMyScheduled());
          if(createPostNewdata==false)
          dispatch(fetchMyScheduledSuccess(json.data));
          else
          dispatch(fetchMyScheduledSuccess(json.data,createPostNewdata));
          dispatch(updateMetaInformation(json.meta));
          if(json.meta.pagination.current_page < json.meta.pagination.total_pages){
           dispatch(loadPostDataMyScheduled());}
        }
        else {
          utils.handleSessionError(json)
          dispatch(fetchMyScheduledSuccess([]))
          if(json.meta.pagination.total==0){
          dispatch(fetchNoPostMyScheduled())}
        }
      })
      .catch(err => { throw err;
       dispatch(fecthMyScheduledError()) });
    }
  }
 }

 export function fetchLive(currPage,limit,isPin = false,createPostNewdata=false,Type=null,search=null,onPostsScroll=false){
  return (dispatch) => {
    onPostsScroll !==true ? dispatch(initiateFetchMyLive()) :''
      fetch(Globals.API_ROOT_URL + `/post/user?published_status=published&page=${currPage}&limit=${limit}`,{
        headers: utils.setApiHeaders('get')
      })
      .then(response => response.json())
      .then((json) => {
        if(Array.isArray(json.data) && json.data.length > 0) {
          dispatch(initiateFetchMyLive());
          dispatch(fetchMyLiveSuccess(json.data, isPin,createPostNewdata));
          dispatch(updateMetaInformation(json.meta));
          if(json.meta.pagination.current_page < json.meta.pagination.total_pages){
          dispatch(loadPostDataMyLive());}
        }
        else {
          utils.handleSessionError(json)
          dispatch(fetchMyLiveSuccess([]))
          // if(json.meta.pagination.total==0){
            if(json.meta.pagination.total==0){
          dispatch(fetchNoPostMyLive())}
        }

      })
      .catch(err => { throw err;
        dispatch(fecthMyLiveError())
      });
    }
 }
 export function fetchMyLiveSuccess(data,isPin=false,createPostNewdata=false,fromFilter=null,currPage=null,hash=false,onPostsScroll){
  return{
    type: types.FETCH_MY_LIVE_SUCCESS,
    payload: data,
    isPin: isPin,
    createPostNewdata:createPostNewdata,
    fromFilter:fromFilter,
    currPage:currPage,
    isHash:hash,
    onPostsScroll : onPostsScroll
  }
}

export function fetchLiveHash(hash, currPage, limit, onPostsScroll) {
  return (dispatch) => {
    dispatch(initiateFetchMyLive())
      fetch(Globals.API_ROOT_URL + `/post/user?published_status=published&page=${currPage}&limit=${limit}&hashtag=${hash}`,{
        headers: utils.setApiHeaders('get')
      })
      .then(response => response.json())
      .then((json) => {
        if(Array.isArray(json.data) && json.data.length > 0) {
          dispatch(initiateFetchMyLive());
          dispatch(fetchMyLiveSuccess(json.data,false,false,null,null,true,onPostsScroll));
          dispatch(updateMetaInformation(json.meta));
          if(json.meta.pagination.current_page < json.meta.pagination.total_pages){
          dispatch(loadPostDataMyLive());}
        }
        else {
          utils.handleSessionError(json)
          if(onPostsScroll == true && hash!== '' ){
            
            dispatch(fetchMyLiveSuccess([],false,false,null,null,true,onPostsScroll))
          }
          else{
            dispatch(fetchMyLiveSuccess([]))
          }
            if(json.meta.pagination.total==0){
              dispatch(fetchNoPostMyLive())}
            }
       })
      .catch(err => { throw err;
        dispatch(fecthMyLiveError())
      });
    }
}

export function loadPostDataMyLive(){
  return{
    type:types.LOAD_POSTDATA_MYLIVE
  };
}

export function fetchNoPostMyLive() {
  return {
     type: types.FETCH_NOPOST_MYLIVE
   };
}

export function initiateFetchMyLive() {
  return {
     type: types.INITIATE_FETCH_MY_LIVE
   };
 }

export function fetchScheduled(currPage,limit, isPin = false,createPostNewdata=false,Type=null,search=null) {
  return (dispatch) => {
    dispatch(initiateFetchMyScheduled())
    fetch(Globals.API_ROOT_URL + `/post/user?published_status=scheduled&page=${currPage}&limit=${limit}`,{
      headers: utils.setApiHeaders('get')
    })
    .then(response => response.json())
    .then((json) => {
      if(Array.isArray(json.data) && json.data.length > 0) {
        dispatch(initiateFetchMyScheduled());
        if(createPostNewdata==false)
        dispatch(fetchMyScheduledSuccess(json.data));
        else
        dispatch(fetchMyScheduledSuccess(json.data,createPostNewdata));
        dispatch(updateMetaInformation(json.meta));
        if(json.meta.pagination.current_page < json.meta.pagination.total_pages){
         dispatch(loadPostDataMyScheduled());}
      }
      else {
        utils.handleSessionError(json)
        dispatch(fetchMyScheduledSuccess([]))
        if(json.meta.pagination.total==0){
        dispatch(fetchNoPostMyScheduled())}
      }
    })
    .catch(err => { throw err;
     dispatch(fecthMyScheduledError()) });
  }

}

export function fetchMyScheduledSuccess(data,isPin,createPostNewdata){
 return{
   type: types.FETCH_MY_SCHEDULED_SUCCESS,
   payload: data,
   isPin: isPin,
   createPostNewdata:createPostNewdata,
   // isHash:hash,
   // onPostsScroll : onPostsScroll
 }
}

export function loadPostDataMyScheduled(){
 return{
   type:types.LOAD_POSTDATA_MYSCHEDULED
 };
}

export function fetchNoPostMyScheduled() {
 return {
    type: types.FETCH_NOPOST_MYSCHEDULED
  };
}

export function initiateFetchMyScheduled() {
 return {
    type: types.INITIATE_FETCH_MY_SCHEDULED
  };
}
/**
 *
 * @param {*} isPin true when function will call from the post pin
 * @param {*} createPostNewdata true when new post will create
 */
export function fetchDefaultLive(currPage, limit, isPin = false,createPostNewdata=false,Type=null,search=null, lastPostId = null,lastFeedType,jobcount,job_display,curation_display,is_configured,isJobfinished, curationcount) {
  var encodeInput = encodeURIComponent(search);
  page_limit=limit;
  pageno=currPage;
  if(Type==null)
  {
    if(search==null)
    {
      return (dispatch) => {

      dispatch(userScrollLoader())
      dispatch(fetchDefaultLiveInitiate())
        var lastPostParam = '';
        if(lastPostId !== null){
            lastPostParam = '&post_publisher_identity='+lastPostId+'&get_data=down';
        }else{
          lastPostParam = '&page='+currPage;
        }
        fetch(Globals.API_ROOT_URL + `/post?feed_identity=${Globals.defaultFeedId}&limit=${limit}`+lastPostParam,{
          headers: utils.setApiHeaders('get')
        })
        .then(response => response.json())
        .then((json) => {
          if(Array.isArray(json.data) && json.data.length > 0) {
            dispatch(storeLastPostId(json.data[Object.keys(json.data)[Object.keys(json.data).length - 1]]['publisher_identity']))
            dispatch(userScrollLoader());
            dispatch(fetchDefaultLiveSuccess(json.data, isPin,createPostNewdata));
            dispatch(updateMetaInformation(json.meta));
            if(json.meta.pagination.current_page < json.meta.pagination.total_pages){
            dispatch(loadPostDataDefault());}
            dispatch(fetchJobCuration(currPage,limit,lastFeedType,jobcount,job_display,curation_display,is_configured,isJobfinished, curationcount));

          }
          else {
            dispatch(fetchDefaultLiveSuccess([]))
            // if(json.meta.pagination.total==0){
              if(json.meta.pagination.total==0){
                dispatch(fetchNoPostDefault())}
              }
              if(json.error){
                utils.handleSessionError(json)
              }
        })
        .catch(err => { throw err;
          dispatch(fetchDefaultLiveError())
        });
      }
    }
    else
    {
      return (dispatch) => {
        dispatch(userScrollLoader())
          fetch(Globals.API_ROOT_URL + `/globalSearch?search=${encodeInput}&type=post&page=${currPage}&limit=${limit}`,{
            headers: utils.setApiHeaders('get')
          })
          .then(response => response.json())
          .then((json) => {
            if(Array.isArray(json.data) && json.data.length > 0) {
              dispatch(userScrollLoader());
              dispatch(fetchDefaultLiveSuccess(json.data, isPin,createPostNewdata));
              dispatch(searchsuccess());
              dispatch(updateMetaInformation(json.meta));
              if(json.meta.pagination.current_page < json.meta.pagination.total_pages){
              dispatch(loadPostDataDefault());
            }
            }
            else {
              dispatch(searchsuccess());
              dispatch(fetchDefaultLiveSuccess([]))
              // if(json.meta.pagination.total==0){
                if(json.meta.pagination.total==0){
              dispatch(fetchNoPostDefault())}
            }
            if(json.error){
              utils.handleSessionError(json)
            }
          })
          .catch(err => { throw err;
            dispatch(fetchDefaultLiveError())
          });
        }
    }
  }
  else
  {
    return (dispatch) => {
      dispatch(userScrollLoader())
        fetch(Globals.API_ROOT_URL + `/post/user?published_status=published&page=${currPage}&limit=${limit}`,{
          headers: utils.setApiHeaders('get')
        })
        .then(response => response.json())
        .then((json) => {
          if(Array.isArray(json.data) && json.data.length > 0) {
            dispatch(userScrollLoader());
            dispatch(fetchDefaultLiveSuccess(json.data, isPin,createPostNewdata));
            dispatch(updateMetaInformation(json.meta));
            if(json.meta.pagination.current_page < json.meta.pagination.total_pages){
            dispatch(loadPostDataDefault());}
          }
          else {
            dispatch(fetchDefaultLiveSuccess([]))
            // if(json.meta.pagination.total==0){
              if(json.meta.pagination.total==0){
            dispatch(fetchNoPostDefault())}
          }
          if(json.error){
            utils.handleSessionError(json)
          }
        })
        .catch(err => { throw err;
          dispatch(fetchDefaultLiveError())
        });
      }
  }
}
/**
 * to call api for selected hash data post
 * @param {*} hash hash name
 * @param {*} currPage
 * @param {*} limit
 */
export function fetchDefaultLiveHash(hash, currPage, limit,onPostsScroll, lastPostId=null) {
  return (dispatch) => {
   dispatch(userScrollLoader())
   dispatch(fetchDefaultLiveInitiate())
    var lastPostParam = '';
    if(lastPostId !== null){
        lastPostParam = '&post_publisher_identity='+lastPostId+'&get_data=down';
    }else{
      lastPostParam = '&page='+currPage;
    }
    fetch(Globals.API_ROOT_URL + `/post?feed_identity=${Globals.defaultFeedId}&limit=${limit}&hashtag=${hash}`+lastPostParam,{
      headers: utils.setApiHeaders('get')
    })
    .then(response => response.json())
    .then((json) => {
      if(Array.isArray(json.data) && json.data.length > 0) {
        dispatch(userScrollLoader());
         dispatch(storeLastPostId(json.data[Object.keys(json.data)[Object.keys(json.data).length - 1]]['publisher_identity']))
        dispatch(fetchDefaultLiveSuccess(json.data,false,false,true,onPostsScroll));
        dispatch(updateMetaInformation(json.meta));
        if(json.meta.pagination.current_page < json.meta.pagination.total_pages){
         dispatch(loadPostDataDefault());}
      }
      else {
          if(onPostsScroll == true && hash!== '' ){
            dispatch(fetchDefaultLiveSuccess([],false,false,true,onPostsScroll))
          }
          else{
          dispatch(fetchDefaultLiveSuccess([]))}
          if(json.meta.pagination.total==0)
          {
            dispatch(fetchNoPostDefault())
          }
          if(json.error){
            utils.handleSessionError(json)
          }
      }
    })
    .catch(err => { throw err;
      dispatch(fetchDefaultLiveError())
    });
  }
}
/**
 *
 * @param {*} data json data
 * @param {*} isPin
 * @param {*} createPostNewdata
 * @param {*} hash true if call from fetchInternalLiveHash
 */
export function fetchDefaultLiveSuccess(data,isPin=false,createPostNewdata=false,hash=false ,onPostsScroll) {
  return {
    type: types.FETCH_DEFAULT_LIVE_SUCCESS,
    payload: data,
    isPin: isPin,
    createPostNewdata:createPostNewdata,
    isHash:hash,
    onPostsScroll : onPostsScroll
  };
}
/**
 *
 * @param {*} createPostNewdata true when new post will create
 */
export function fetchInternalScheduled(currPage, limit, postStatus, events = false, moment = null,createPostNewdata=false) {
  return (dispatch) => {
    dispatch(userScrollLoader())
    fetch(Globals.API_ROOT_URL + `/post/user?published_status=scheduled&feed_identity=${Globals.defaultFeedId}&page=${currPage}&limit=${limit}`,{
      headers: utils.setApiHeaders('get')
    })
    .then(response => response.json())
    .then((json) => {
      if(Array.isArray(json.data) && json.data.length > 0) {
        dispatch(userScrollLoader());
        if(createPostNewdata==false)
        dispatch(fetchInternalScheduledSuccess(json.data));
        else
        dispatch(fetchInternalScheduledSuccess(json.data,createPostNewdata));
        dispatch(updateMetaInformation(json.meta));
        if(json.meta.pagination.total >=10){
         dispatch(loadPostDataInternal());}
      }
      else {
        dispatch(fetchInternalScheduledSuccess([]))
        if(json.meta.pagination.total==0){
        dispatch(fetchNoPostDefault())}
      }
      if(json.error){
        utils.handleSessionError(json)
      }
    })
    .catch(err => { throw err;
     dispatch(fecthInternalScheduledError()) });
  }

}

export function fetchInternalScheduledSuccess(data,newdata=null) {
   return {
    type: types.FETCH_INTERNAL_SCHEDULED_SUCCESS,
    payload: data,
    createPostNewdata:newdata
  };
}
/**
 *
 * @param {*} createPostNewdata true when new post will create
 */
export function fetchExternalScheduled(currPage, limit, postStatus, events = false, moment = null, social_channels = [],createPostNewdata=false) {
  page_limit=limit;
  pageno=currPage;
  return (dispatch) => {
    dispatch(userScrollLoaderExternal())
    fetch(Globals.API_ROOT_URL + `/post/user?published_status=scheduled&feed_identity=Opjmp&page=${currPage}&limit=${limit}`,{
      headers: utils.setApiHeaders('get')
    })
    .then(response => response.json())
    .then((json) => {
      if(Array.isArray(json.data) && json.data.length > 0) {
        dispatch(userScrollLoaderExternal());
        if(createPostNewdata==false)
        dispatch(fetchExternalScheduledSuccess(json.data));
        else
        dispatch(fetchExternalScheduledSuccess(json.data,createPostNewdata));
        dispatch(updateMetaInformation(json.meta));
        if(json.meta.pagination.total >=10){
          dispatch(loadPostDataExternal());}
      }
      else {
        dispatch(fetchExternalScheduledSuccess([]))
        if(json.meta.pagination.total==0){
          dispatch(fetchNoPostExternal())}
        }
        if(json.error){
          utils.handleSessionError(json)
        }
    })
    .catch(err => { throw err; });
  }
}


export function getGiphyDataSuccess (giphydata) {
  return {
    type: types.FETCH_GIPHY_DATA,
    data: giphydata,
  }

}

export function getGiphySearchDataSuccess (giphySearchdata) {
  return {
    type: types.FETCH_SEARCH_GIPHY_DATA,
    searchdata: giphySearchdata,
  }

}
export function fetchGiphy () {
  return dispatch => {
   // dispatch(getloadingGiphy())
    fetch('https://api.giphy.com/v1/gifs/trending?api_key=e7578fd528ef42c5a806427a3ed6552b&limit=25&rating=G')
      .then(response => response.json())
      .then(json => {
        if (Array.isArray(json.data) && json.data.length > 0) {
          dispatch(fetchsearchInit())
          dispatch(getGiphyDataSuccess(json.data))
        }
      })
      .catch(error => {
        console.error(error)
      })
  }
}
export function getloadingGiphy () {
  return {
    type: types.LOADING_GIPHY
  }
}
export function fetchsearchInit(){
  return{
    type:types.FETCH_SEARCH_GIPHY_INIT
  }
}
export function fetchSearchGiphy (searchdata) {
  return dispatch => {
    dispatch(getloadingGiphy())
    fetch(`https://api.giphy.com/v1/gifs/search?api_key=e7578fd528ef42c5a806427a3ed6552b&q=${searchdata}&limit=25&offset=0&rating=G&lang=en`)
      .then(response => response.json())
      .then(json => {
        if (Array.isArray(json.data) && json.data.length > 0) {
          dispatch(fetchsearchInit())
          dispatch(getGiphySearchDataSuccess(json.data))
        }
      })
      .catch(error => {
        console.error(error)
      })
  }
}

export function fetchExternalScheduledSuccess(data,newdata=null) {
 return {
    type: types.FETCH_EXTERNAL_SCHEDULED_SUCCESS,
    payload: data,
    createPostNewdata:newdata
  };
}

export function setHidePost(feedId,postid,sendmail, feed, feedType,search=null,scheduleTime) {
  return (dispatch) => {
    fetch(
      Globals.API_ROOT_URL +
      `/post/hide?post_identity=${postid}&send_mail=${sendmail}&schedule_time=${scheduleTime}`,
      {
        method:'GET',
        headers: utils.setApiHeaders('get',
          {Accept: 'application/json',
          'Content-Type': 'application/json'}
        )},
    )
    .then(response => {
      if (response.status != '200') {
        notie.alert(
          'error',
          'There was a problem hiding post.',
          5
        )
      }
      return response.json()
    })
    .then(json => {
      if (json.code == 200) {
        if(feed == "default"){
              dispatch(fetchDefaultLive(1, 10, true,false,null,search))
        }
        else if(feed=="custom"){
          if(feedType == "live"){
            dispatch(fetchCustomLive(feedId,1,10,true,false,null,search))
          }
        }
        else if(feed="my")
        {
          if(feedType=="live"){
            dispatch(fetchLive(1,10,true,false,null,search))
          }
        }
        notie.alert('success', json.message, 3)
      }else{
        utils.handleSessionError(json)
      }
    })
      .catch(err => { throw err; });
  }
}

export function pinOnTop(Feed_Identity=null,data, feed, feedType,search=null) {
   return (dispatch) => {
     fetch(
       Globals.API_ROOT_URL +
       `/post/pin`,
       {
        method: 'PUT',
        headers: utils.setApiHeaders('put'),
        body: JSON.stringify(data)
      })
     .then(response => {
       if (response.status != '200') {
         notie.alert(
           'error',
           'There was a problem to pin a post.',
           5
         )
       }
       return response.json()
     })
     .then(json => {
       if (json.code == 200) {
         if(feed == "default"){
              if(search==null)
              {
                dispatch(fetchDefaultLive(1, 10, true))
              }
              else
              {
                dispatch(fetchDefaultLive(1, 10, true,false,null,search))
              }
         }else if(feed == "custom"){
            if(feedType == "live"){
              dispatch(fetchCustomLive(Feed_Identity,1, 10, true))
            }
         }
         else if(feed=="my")
         {
          if(feedType == "live"){
            dispatch(fetchLive(1, 10, true,false,null,search))
          }
         }
         else if(typeof data.pin_message !== "undefined"){
            dispatch(updatePinMessageSuccess(data.pin_message,data.post_id))
         }

         notie.alert('success', json.message, 3)
         scroll(0,0)
       }else{
        utils.handleSessionError(json)
       }
     })
       .catch(err => { throw err; });
   }
 }
 export function updatePinMessageSuccess(message,postId){
    return {
      type: types.UPDATE_PIN_MESSAGE,
      message: message,
      postId: postId
    };
 }
 
 export function readPinedPost(feedId=null,postid, feed, feedType,publisherid) {
   return (dispatch) => {
     fetch(
       Globals.API_ROOT_URL +
       `/post/read?post_id=${postid}&publisher_id=${publisherid}`,
       {
        method: 'GET',
        headers: utils.setApiHeaders('get',
          {Accept: 'application/json',
          'Content-Type': 'application/json'}
        )},
     )
     .then(response => {
       if (response.status != '200') {
         notie.alert(
           'error',
           'There was a problem to pin a post.',
           5
         )
       }
       return response.json()
     })
     .then(json => {
       if (json.code == 200) {
          if(feed == "default"){
              dispatch(fetchDefaultLive(1, 10, true))
         }else if (feed == "custom"){
            if(feedType == "live"){
              dispatch(fetchCustomLive(feedId,1,10,true))
            }
         }
         notie.alert('success', json.message, 3)
       }else{
        utils.handleSessionError(json)
       }
     })
       .catch(err => { throw err; });
   }
 }
 /**
  * to start loader for job which is in feed page
  * @author disha
  */
export function jobFromFeedInit(){
  return{
    type:types.JOB_FROM_FEED_INIT
  }
}
/**
  * to start loader for curation which is in feed page
  * @author disha
  */
export function CurationFromFeedInit(){
  return{
    type:types.CURATION_FROM_FEED_INIT
  }
}

export function staticJobDisplay(){
  return{
    type:types.STATIC_JOB_DISPLAY
  }
}
//  fetch curation and Rss in merge feed
export function fetchJobCuration(currPage,limit,lastFeedType=null,jobcount,job_display,curation_display,is_configured,isJobfinished, curationcount){
  return (dispatch) => {
    if (job_display == 1 || curation_display == 1) {
      if (currPage == 1) {
        if (job_display == 1) {
          dispatch(JobAPIcall(0, 10, is_configured, currPage, curation_display, curationcount));
        }
        else if (curation_display == 1) {
          dispatch(RssAPIcall(0, 10));
        }
      }
      else {
        if (lastFeedType !== null) {
          if (job_display == 1 && curation_display == 1) {
            switch (lastFeedType) {

              case "curation":
                if (isJobfinished == false) {
                  if (job_display == 1) {
                    dispatch(JobAPIcall(jobcount, limit, is_configured, currPage, null, curationcount))
                  }
                } else if (curation_display == 1) {
                  dispatch(RssAPIcall(curationcount, limit));
                }
                break;

              case "job":
                if (curation_display == 1) {
                  dispatch(RssAPIcall(curationcount, limit));
                }
                break;

              default:
                // this condition is for call API in next page if response of Previous API is not loaded
                if (curation_display == 1) {
                  dispatch(RssAPIcall(curationcount, limit));
                }
                else if (job_display == 1) {
                  dispatch(JobAPIcall(jobcount, limit, is_configured, currPage, null,curationcount))
                }
                break;
            }
          }
          else if (job_display == 1 && isJobfinished == false) {
            dispatch(JobAPIcall(jobcount, limit, is_configured, currPage, null, curationcount));
          }
          else if (curation_display == 1) {
            dispatch(RssAPIcall(curationcount, limit));
          }
        }
      }
    }
  }
  }

export function JobAPIcall(jobcount,limit,is_configured,currPage,curation_display, curationcount){
  return (dispatch) => {
    if(is_configured==0){
      dispatch(staticJobDisplay());  
      if(currPage==1 && curation_display==1){
        dispatch(RssAPIcall(curationcount, limit));
      }
    }else{
      dispatch(jobFromFeedInit())
      fetch(Globals.API_ROOT_URL +`/rssFeed/curation-job?feed_type=job&page=${jobcount+1}&limit=${limit}`,{
        headers: utils.setApiHeaders('get')
      })
            .then(response => response.json())
            .then( json =>{
              if(Array.isArray(json.data) && json.data.length > 0) {
                dispatch(fetchJobDataSuccess(json));
                if(json.feed_type!=="curation" && curation_display==1){
                  dispatch(RssAPIcall(curationcount, limit));
                }
              }else{
                utils.handleSessionError(json)
                // dispatch(fetchJobError())
              }
            })
            .catch(err => { throw err;
            });
    }
  }
}
export function RssAPIcall(RSScount,limit){
  var rssCount = RSScount == undefined ? 0 : RSScount

  return (dispatch) => {
    dispatch(CurationFromFeedInit())
  fetch(Globals.API_ROOT_URL +`/rssFeed/curation-job?feed_type=curation&page=${rssCount+1}&limit=${limit}`,{
    headers: utils.setApiHeaders('get')
  })
  .then(response => response.json())
  .then( json =>{
    if(json.code==200){
      dispatch(fetchRssDataSuccess(json));
    }
    else{
          utils.handleSessionError(json);
          // dispatch(fetchRssError(json.message))
          }
    })
    .catch(err => { throw err;
    });
  }
}
export function fetchRssFeed(page=1) {
  var isCallFromRssPage=true
  return (dispatch) => {
    dispatch(fetchInitData());
    fetch(Globals.API_ROOT_URL + `/rssFeed/feeds?pagination=1&page=${page}`,{
      headers: utils.setApiHeaders('get')
    })
    .then(response => response.json())
    .then((json) => {
      if(json.code==200) {
        dispatch(fetchRssDataSuccess(json.data,isCallFromRssPage));
      }else{
        utils.handleSessionError(json)
        dispatch(fetchRssError(json.message))
      }
    })
    .catch(err => { throw err;
    });
  }
}

export function fetchJobList() {
  var isCallFromJobsPage=true
  return (dispatch) => {
    dispatch(fetchInitData());
    fetch(Globals.API_ROOT_URL + `/curation/getJob`,{
      headers: utils.setApiHeaders('get')
    })
    .then(response => {
       if (response.status != '200') {
         notie.alert(
           'error',
           'There is a problem in fetching jobs.',
           5
         )
       }
       return response.json()
     })
    .then((json) => {
        if (Array.isArray(json.data) && json.data.length > 0) {
          dispatch(fetchJobDataSuccess(json.data,isCallFromJobsPage));
        }else{
          utils.handleSessionError(json)
          dispatch(fetchJobError())
        }

    })
    .catch(err => { throw err;
    });
  }
}
export function getAllSchedulesInit(){
  return{
    type:types.GET_ALL_SCHEDULES_INIT
  }
}
/**
* get all the schedule for the post
* @author Yamin
* @param postId
**/
export function getAllSchedules(postId){
  return (dispatch) => {
    dispatch(getAllSchedulesInit());
    fetch(Globals.API_ROOT_URL + `/post/publish?post_identity=${postId}`,{
      headers: utils.setApiHeaders('get')
    })
    .then(response => {
       if (response.status != '200') {
         notie.alert(
           'error',
           'There is a problem in fetching schedule times.',
           5
         )
       }
       return response.json()
     })
    .then((json) => {
        if (Array.isArray(json.data) && json.data.length > 0) {
          dispatch(getAllSchedulesSuccess(json.data));
        }else{
          utils.handleSessionError(json)
          dispatch(getAllSchedulesError())
        }

    })
    .catch(err => { throw err;
    });
  }
}

export function getAllSchedulesSuccess(scheduleData){
    return {
      type: types.FETCH_SCHEDULE_DATA,
      scheduleData: scheduleData
    }
}

export function getAllSchedulesError(){
  return{
    type:types.FETCH_SCHEDULE_DATA_FAIL
  }
}

export function createCustomFeed(values){
  var formValues = [];
  //split giest user in array
  var trimUserIdArray = [];
  var chat = false;
  if(typeof values.guests !== "undefined" && values.guests.length > 0){
    trimUserIdArray = values.guests;
  }
  // to set the job and curation display status according to form value
  var job_display=0;
  var curation_display=0;
  if(values.jobRss!==undefined){
    if(values.jobRss=="jobs"){
      job_display = 1
      curation_display =0
    }
    else if(values.jobRss=="curation"){
      job_display = 0
      curation_display = 1
    }
    else if(values.jobRss=="both"){
      job_display = 1
      curation_display = 1
    }
    else if(values.jobRss=="none"){
      job_display = 0
      curation_display = 0
    }

  }
  if(values.chat == yes) {
    chat = true;
  }
  else{
    chat =false;
  }

  //fetch only user id to send in api
  var userArray = [];
  if(typeof values.users !== "undefined" && values.users.length > 0 ){
    userArray = values.users;
  }
   var jsonBody = {
      'name': values.title,
      'type': values.feedType,
      'job_display':job_display,
      'curation_display':curation_display,
      'chat':chat
    }
   if(userArray.length > 0){
      jsonBody['user_tag'] = userArray;
   }
   if(trimUserIdArray.length > 0){
      jsonBody['guest_user'] = trimUserIdArray;
   }
  return dispatch => {
    dispatch(createFeedInit());
    fetch(`${Globals.API_ROOT_URL}/feed`, {
      method: 'POST',
      headers: utils.setApiHeaders('post'),
      body: JSON.stringify(jsonBody)
    })
      .then(response => {
        return response.json()
      })
      .then(json => {

        if (json.code == 200 || json.code == 206) {
          dispatch(getFeedList());
          dispatch(createFeedSuccess());
          if(json.code == 206){
              var extraMessage = "";
              //if user added in guest list
              if(typeof json.data.invalid_users !== "undefined"){
                  extraMessage = extraMessage + "<br> you added user in guest : "
                  var emailList = "";
                  json.data.invalid_users.map((invalidUser)=>{
                    emailList = emailList + invalidUser + ","
                  });
                  emailList = emailList.substring(0, emailList.length - 1);
                  extraMessage = extraMessage + emailList;
              }

              // if suspended guest is added
              if(typeof json.data.suspend_users !== "undefined"){
                extraMessage = extraMessage + "<br> you added suspended guest : "
                var emailList = "";
                json.data.suspend_users.map((suspened_user)=>{
                  emailList = emailList + suspened_user + ","
                });
                emailList = emailList.substring(0, emailList.length - 1);
                extraMessage = extraMessage + emailList;
            }

              //if wrong user added
              if(typeof json.data.wrong_user !== "undefined"){
                  extraMessage = extraMessage + "<br> this are not user : "
                  var emailList = "";
                  json.data.wrong_user.map((wrongUser)=>{
                    emailList = emailList + wrongUser + ","
                  });
                  emailList = emailList.substring(0, emailList.length - 1);
                  extraMessage = extraMessage + emailList;
              }
              //if guest email is wrong
              if(typeof json.data.wrong_email !== "undefined"){
                  extraMessage = extraMessage + "<br> this email are not correct : "
                  var emailList = "";
                  json.data.wrong_email.map((wrongEmail)=>{
                    emailList = emailList + wrongEmail + ","
                  });
                  emailList = emailList.substring(0, emailList.length - 1);
                  extraMessage = extraMessage + emailList;
              }
              notie.alert('warning',json.message + extraMessage,10);
          }else{
              notie.alert('success',json.message,5);
          }


          browserHistory.push('/feed/custom/live/'+json.data.feed_id);

        }else{
          if(typeof json.error !== "undefined"){
            if(typeof json.error.name !== "undefined"){
                notie.alert('error',json.error.name,5);
            }
          }else{
            notie.alert('error',json.message,5);
          }

          dispatch(createFeedFail());

        }

      })
      .catch(err => {
        throw err
      })
  }
}

export function createFeedInit(){
    return {
      type: types.CREATE_FEED_INIT
    }
}

export function createFeedSuccess(){
    return {
      type: types.CREATE_FEED_SUCCESS
    }
}

export function createFeedFail(){
    return {
      type: types.CREATE_FEED_FAIL
    }
}

export function getUpdateFeedDetail(feedId){
    return (dispatch) => {
      dispatch(getUpdateFeedDetailInit());
      fetch(Globals.API_ROOT_URL + `/feed?feed_identity=${feedId}`,{
        headers: utils.setApiHeaders('get')
      })
      .then(response => {
         if (response.status != '200') {
            dispatch(getUpdateFeedDetailFail());
            if(response.status == '401')
            {
              notie.alert(
                'error',
                'you do not have permission to access this feed',
                3
              )
            }
            else
            {
              notie.alert(
                'error',
                'There is a problem in fetching feed detail.',
                3
              )
            }
         }
         return response.json()
       })
      .then((json) => {
          if(json.code == 200){ 
              dispatch(getUpdateFeedDetailSuccess(json.data));
              
          }
      })
      .catch(err => { throw err;
      });
    }
}

export function getUpdateFeedDetailInit(){
    return {
        type: types.GET_CUSTOM_FEED_DETAIL_INIT,
    }
}

export function getUpdateFeedDetailSuccess(feedDetail){
     return {
        type: types.GET_CUSTOM_FEED_DETAIL,
        feedDetail: feedDetail
    }
}

export function getUpdateFeedDetailFail(){
  return {
     type: types.GET_CUSTOM_FEED_DETAIL_FAIL,
 }
}
//remove user from feed by admin api
export function removeUserFromFeed(userObject,feedId){
                   var jsonBody = {
                      'delete_user': [userObject]
                    }
                  return dispatch => {
                     notie.confirm(
                    `Are you sure you want to remove user from this feed?`,
                    'Yes',
                    'No',
                    function () {
                    dispatch(removeUserFromFeedInit());
                    fetch(`${Globals.API_ROOT_URL}/feed/edit/${feedId}`, {
                      method: 'PUT',
                      headers: utils.setApiHeaders('put'),
                      body: JSON.stringify(jsonBody)
                    })
                      .then(response => {
                        return response.json()
                      })
                      .then(json => {

                        if (json.code == 200) {
                          notie.alert('success','User successfully removed from feed',5);
                          if(userObject.type=="company"){
                            dispatch(getFeedList());
                          }
                          dispatch(getUpdateFeedDetail(feedId));
                          dispatch(removeUserFromFeedFinish());

                        }else{
                          notie.alert('error','Problem in removing user from feed',5);
                          dispatch(removeUserFromFeedFinish());

                        }

                      })
                      .catch(err => {
                        throw err
                      })
              },
              function () { }
              )
       }

}

export function removeUserFromFeedInit(){
    return {
        type: types.REMOVE_USER_FROM_FEED_INIT
    }
}
export function removeUserFromFeedFinish(){
    return {
        type: types.REMOVE_USER_FROM_FEED_FINISH
    }
}

export function openFeedPopup(){
    return {
      type: types.OPEN_FEED_POPUP
    }
}


export function AddMoreUserCustomFeed(values){
  var formValues = [];
  //split giest user in array
  var trimUserIdArray = [];
  if(typeof values.guests !== "undefined" && values.guests.length > 0){
    trimUserIdArray = values.guests;
  }

  //fetch only user id to send in api
  var userArray = [];
  if(typeof values.users !== "undefined" && values.users.length > 0 ){
    userArray=values.users
  }
   var jsonBody = {};
   if(userArray.length > 0){
      jsonBody['user_tag'] = userArray;
   }
   if(trimUserIdArray.length > 0){
      jsonBody['guest_user'] = trimUserIdArray;
   }

   if(userArray.length == 0 && trimUserIdArray.length == 0){
      notie.alert('error','Please add User or guests to invite',5);
      return;
   }
  return dispatch => {
    dispatch(AddMoreUserCustomFeedInit());
    fetch(`${Globals.API_ROOT_URL}/feed/edit/${values.feedId}`, {
      method: 'PUT',
      headers: utils.setApiHeaders('put'),
      body: JSON.stringify(jsonBody)
    })
      .then(response => {
        return response.json()
      })
      .then(json => {
          
          if(json.code == 200 || json.code==201 )
          {
            dispatch(AddMoreUserCustomFeedFinish());
          }
          dispatch(getUpdateFeedDetail(values.feedId));
          if(json.code == 206 || json.code == 500){
              var extraMessage = "";
              //if user added in guest list
              if(typeof json.data.invalid_users !== "undefined"){
                  extraMessage = extraMessage + "<br> you added user in guest : "
                  var emailList = "";
                  json.data.invalid_users.map((invalidUser)=>{
                    emailList = emailList + invalidUser + ","
                  });
                  emailList = emailList.substring(0, emailList.length - 1);
                  extraMessage = extraMessage + emailList;
              }
              // if suspended guest is added 
              if(typeof json.data.suspend_users !== "undefined"){
                extraMessage = extraMessage + "<br> you added suspended guest : "
                var emailList = "";
                json.data.suspend_users.map((suspened_user)=>{
                  emailList = emailList + suspened_user + ","
                });
                emailList = emailList.substring(0, emailList.length - 1);
                extraMessage = extraMessage + emailList;
            }

              //if wrong user added
              if(typeof json.data.wrong_user !== "undefined"){
                  extraMessage = extraMessage + "<br> this are not user : "
                  var emailList = "";
                  json.data.wrong_user.map((wrongUser)=>{
                    emailList = emailList + wrongUser + ","
                  });
                  emailList = emailList.substring(0, emailList.length - 1);
                  extraMessage = extraMessage + emailList;
              }
              //if guest email is wrong
              if(typeof json.data.wrong_email !== "undefined"){
                  extraMessage = extraMessage + "<br> this email are not correct : "
                  var emailList = "";
                  json.data.wrong_email.map((wrongEmail)=>{
                    emailList = emailList + wrongEmail + ","
                  });
                  emailList = emailList.substring(0, emailList.length - 1);
                  extraMessage = extraMessage + emailList;
              }
              //if guest email is wrong
              if(typeof json.data.already_added_user !== "undefined"){
                  extraMessage = extraMessage + "<br> this user are already added : "
                  var emailList = "";
                  json.data.already_added_user.map((alreadyAdded)=>{
                    emailList = emailList + alreadyAdded + ","
                  });
                  emailList = emailList.substring(0, emailList.length - 1);
                  extraMessage = extraMessage + emailList;
              }
              if(json.code == 206){
                notie.alert('warning',json.message + extraMessage,10);
              }else{
                notie.alert('error',json.message + extraMessage,10);
              }
          }else{
            
              notie.alert('success',json.message,5 );
          }

      })
      .catch(err => {
        throw err
      })
  }
}

export function AddMoreUserCustomFeedInit(){
    return {
      type: types.ADD_MORE_USER_CUSTOM_FEED_INIT
    }
}

export function AddMoreUserCustomFeedFinish(){
    return {
      type: types.ADD_MORE_USER_CUSTOM_FEED_FINISH
    }
}

export function resetUpdateForm(){
    return {
      type: types.RESET_UPDATE_FEED_FORM
    }
}

export function updateFeedDetail(formValue){

  // to set the job and curation display status according to form value
  var job_display=0;
  var curation_display=0;
  var chat= false
  if(formValue.JobRss!==undefined){
    if(formValue.JobRss=="jobs"){
      job_display = 1
      curation_display =0
    }
    else if(formValue.JobRss=="curation"){
      job_display = 0
      curation_display = 1
    }
    else if(formValue.JobRss=="both"){
      job_display = 1
      curation_display = 1
    }
    else if(formValue.JobRss=="none"){
      job_display=0;
      curation_display=0;
    }
  }
  if(formValue.chat == yes){
    chat = true;
  }
   else{
    chat = false;
  }
   var jsonBody = {
        'name': formValue.feed_name,
        'type': formValue.feedType,
        'job_display':job_display,
        'curation_display':curation_display,
        'chat':chat      
   };
  return dispatch => {
    dispatch(updateFeedDetailInit());
    fetch(`${Globals.API_ROOT_URL}/feed/edit/${formValue.feedId}`, {
      method: 'PUT',
      headers: utils.setApiHeaders('put'),
      body: JSON.stringify(jsonBody)
    })
      .then(response => {
        return response.json()
      })
      .then(json => {
        if (json.code == 200) {
          notie.alert('success',json.message,5);
          dispatch(getFeedList());
          dispatch(getUpdateFeedDetail(formValue.feedId));
          dispatch(updateFeedDetailFinish());

        }else{
          notie.alert('error',json.message,5);
        }
      })
      .catch(err => {
        throw err
      })
  }
}

export function updateFeedDetailInit(){
    return {
      type: types.UPDATE_FEED_DETAIL_INIT
    }
}

export function updateFeedDetailFinish(){
    return {
      type: types.UPDATE_FEED_DETAIL_FINISH
    }
}
/**
To call the rate api
@author Yamin
**/
export function ratePost(points,postId,valueId){
  var jsonBody = {
    "post_identity": postId,
    "rate": points,
    "value_identity": valueId
  }
  return dispatch => {
    
    fetch(`${Globals.API_ROOT_URL}/post/rate`, {
      method: 'POST',
      headers: utils.setApiHeaders('post'),
      body: JSON.stringify(jsonBody)
    })
      .then(response => {
        return response.json()
      })
      .then(json => {
        if (json.code == 200) {
          dispatch(ratePostSuccess(postId));
        }else{
          notie.alert('error',json.message,5);
          dispatch(ratePostFail())
        } 
      })
      .catch(err => {
        throw err
      })
  }
}

export function ratePostSuccess(postId){
    return {
      type: types.RATE_POST_SUCCESS,
      ratedPost:postId
    }
}

export function ratePostFail(){
    return {
      type: types.RATE_POST_FAIL
    }
}

export function storeLastPostId(postPublisherId){
    return {
      type: types.STORE_LAST_POST_ID,
      postPublisherId:postPublisherId
    }
}

export function fetchDefaultLiveInitiate(){
   return {
      type: types.FETCH_DEFAULT_LIVE_INIT
    }
}

/**
fetch unapproved  posts
@author Tejal Kukadiya
**/
export function fetchMyUnapproved(currPage,limit,Type,onPostsScroll){
return (dispatch) => {
    dispatch(fetchMyUnapprovedInit())
    fetch(Globals.API_ROOT_URL + `/post/user?published_status=unapproved&page=${currPage}&limit=${limit}`,{
      headers: utils.setApiHeaders('get')
    })
    .then(response => response.json())
    .then((json) => {
      if(Array.isArray(json.data) && json.data.length > 0) {
        dispatch(fetchMyUnapprovedSuccess(json.data,onPostsScroll))
        dispatch(updateMetaInformation(json.meta));
            if(json.meta.pagination.current_page < json.meta.pagination.total_pages){
            dispatch(loadPostDataMyLive());}
      }
      else {
        utils.handleSessionError(json)
        dispatch(fetchMyUnapprovedSuccess([],onPostsScroll))
        if(json.meta.pagination.total==0){
          dispatch(fetchNoPostMyLive())
        }
        
      }
    })
    .catch(err => { throw err;
     dispatch(fetchMyUnapprovedError()) });
  }
}

export function fetchMyUnapprovedInit(){
  return {
    type:types.FETCH_MY_UNAPPROVED_INIT
  }
}

export function fetchMyUnapprovedSuccess(data,onPostsScroll){
  return {
    type:types.FETCH_MY_UNAPPROVED_SUCCESS,
    data : data,
    onPostsScroll:onPostsScroll
  }
}

export function fetchMyUnapprovedError(){
  return {
    type:types.FETCH_MY_UNAPPROVED_ERROR
  }
}
/**
fetch pending  posts
@author Tejal Kukadiya
**/

export function fetchMyPending(currPage,limit,Type,onPostsScroll){
  return (dispatch) => {
      dispatch(fetchMyPendingInit())
      fetch(Globals.API_ROOT_URL + `/post/user?published_status=pending&page=${currPage}&limit=${limit}`,{
        headers: utils.setApiHeaders('get')
      })
      .then(response => response.json())
      .then((json) => {
        if(Array.isArray(json.data) && json.data.length > 0) {
          dispatch(fetchMyPendingSuccess(json.data,onPostsScroll))
          dispatch(updateMetaInformation(json.meta));
            if(json.meta.pagination.current_page < json.meta.pagination.total_pages){
            dispatch(loadPostDataMyLive());}
        }
        else {
          utils.handleSessionError(json)
          dispatch(fetchMyPendingSuccess([],onPostsScroll))
          if(json.meta.pagination.total==0){
            dispatch(fetchNoPostMyLive())
          }
        }
      })
      .catch(err => { throw err;
       dispatch(fetchMyPendingError()) });
    }
  }
  
  export function fetchMyPendingInit(){
    return {
      type:types.FETCH_MY_PENDING_INIT
    }
  }
  
  export function fetchMyPendingSuccess(data,onPostsScroll){
    return {
      type:types.FETCH_MY_PENDING_SUCCESS,
      data : data,
      onPostsScroll:onPostsScroll
    }
  }
  
  export function fetchMyPendingError(){
    return {
      type:types.FETCH_MY_PENDING_ERROR
    }
  }
  /**
   * @author kinjal
   * fetch saved  hashtag list for create post
   */

  export function fetchHashTagInCreatePost(campId=null){
    var callForCampaign='';
    if(campId !==null){
      callForCampaign =`?campaign_identity=${campId}`
    }
    return dispatch => {
      
      dispatch(fetchHashTagInit());
      fetch(`${Globals.API_ROOT_URL}/post-hashtag${callForCampaign}`, {
        method: 'GET',
        headers: utils.setApiHeaders('get'),
      })
        .then(response => {
          return response.json()
        })
        .then(json => {
          if (json.code == 200) {
            if (typeof (json.data) !== 'undefined') {
              var tagArray = [];
              json.data.hashtag.forEach(function(tag, index) {
                  tagArray.push({
                      'display': tag.title,
                      'id': tag.hashtag_identity
                  })
              });
              var ObjToPass={
                detail:json.data.detail,
                hashtag:tagArray
              };
              dispatch(fetchHashTagSuccess(ObjToPass));
            }else{
              dispatch(fetchHashTagSuccess({}));
            }
          }else{
            notie.alert('error',json.message,5);
            dispatch(fetchHashTagFail())
          } 
        })
        .catch(err => {
          throw err
        })
    }
  }
  export function fetchHashTagInit(){
    return {
      type: types.FETCH_HASH_TAG_INIT_FLAG,
    }
  }
  export function fetchHashTagSuccess(data){
      return {
        type: types.FETCH_HASH_TAG_SUCCESS_FLAG,
        hashTag:data
      }
  }
  
  export function fetchHashTagFail(){
      return {
        type: types.FETCH_HASH_TAG_FAIL_FLAG
      }
  }
  

