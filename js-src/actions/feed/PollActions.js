import * as types from '../actionTypes';
import Globals from '../../Globals';
import notie from 'notie/dist/notie.js';
import * as utils from '../../utils/utils';


 export function fetchSurveyPollQuestion(timezone){
  return (dispatch) => {
   var apiString =   timezone ? `/survey?timezone=${timezone}`: '/survey';
    dispatch(fetchSurveyPollQuestionInit())
    fetch(Globals.API_ROOT_URL + apiString,{
      headers: utils.setApiHeaders('get')
    })
    .then(response => response.json())
    .then( json =>{
        if(json.code==200){
          dispatch(fetchSurveyPollQuestionSuccess(json.data));
        }else{
          dispatch(fetchSurveyPollQuestionFail());
          utils.handleSessionError(json);
        }
      })
      .catch(err => { throw err;
      });
    }
}


export function fetchSurveyPollQuestionInit(){
    return {
      type: types.GET_POLL_SURVEY_QUESTION_INIT
    }
}

export function fetchSurveyPollQuestionSuccess(data){
    return {
      type: types.GET_POLL_SURVEY_QUESTION_SUCCESS,
      data: data
    }
}

export function fetchSurveyPollQuestionFail(){
    return {
      type: types.GET_POLL_SURVEY_QUESTION_FAIL
    }
}

/**
* Update user seen poll
* @author Yamin
**/
export function updatePollSeenStatus(pollData){
    var data = {
        question_id: pollData.identity
    }
    return (dispatch) => {
        fetch(Globals.API_ROOT_URL + `/survey/questionSeen`, {
          method: 'POST',
          headers: utils.setApiHeaders('post'),
          body: JSON.stringify(data)
        }).then(response => {
          return response.json();
        }).then((json) => {
          dispatch(fetchSurveyPollQuestion());
        }).catch(err => {
          throw err;
        });
    }
}

/**
* Save survey result
* @author Yamin
**/
 export function saveSurveyResult(data,questionId){
      var data = {
          question_id: questionId,
          option_id: data.option_identity
      }
      return (dispatch) => {
          dispatch(saveSurveyResultInit())
          fetch(Globals.API_ROOT_URL + `/survey`, {
            method: 'POST',
            headers: utils.setApiHeaders('post'),
            body: JSON.stringify(data)
          }).then(response => {
            return response.json();
          }).then((json) => {
            if(json.code == 200){
              dispatch(saveSurveyResultSuccess())  
              // dispatch(fetchSurveyPollQuestion())
            }else{
              notie.alert('error', json.error, 3);
              dispatch(saveSurveyResultFail())   
            }
            
          }).catch(err => {
            throw err;
          });
      }
 }

export function saveSurveyResultInit(){
    return {
      type: types.SAVE_SURVEY_RESULT_INIT
    }
}

export function saveSurveyResultSuccess(){
    return {
      type: types.SAVE_SURVEY_RESULT_SUCCESS
    }
}

export function saveSurveyResultFail(){
    return {
      type: types.SAVE_SURVEY_RESULT_FAIL
    }
} 

