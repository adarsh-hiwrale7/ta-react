import * as types from '../actionTypes';
import Globals from '../../Globals';
import notie from 'notie/dist/notie.js';
import * as utils from '../../utils/utils';

export function fetchCalendarScheduledSuccess(data,newdata) {
  return {
   type: types.FETCH_CALENDAR_SCHEDULED_SUCCESS,
   payload: data,
 };
}
 export function fecthCalendarScheduledError() {
  return {
     type: types.FETCH_CALENDAR_SCHEDULED_ERROR
   };
 }
 export function fetchCalendarPosts (posts, moment = null) {
  var socialArray,
  CalendarFilterArray = [];
  socialArray = ['facebook','twitter','linkedin'];

  let postList = typeof posts !== undefined ? posts : []
  if (postList && posts!==null && moment !==null) {
    postList.forEach(function (post, index) {
      var socialFlag = false;
      var new_post_id = post.publisher_identity;
      var old_post_id;
      //var dateString = new Date(moment.tz(post.scheduled_at * 1000, Globals.SERVER_TZ).format('ddd MMM DD YYYY h:mm:ss'))
      var dateString = moment.unix(post.scheduled_at)
      var CalendarFilter_object =
      {
        title: post.post.data.detail,
        start: dateString._d,
        end: dateString._d,
        feed_post: post
      }
      if(post.post.data.type =='external')
      {
      post.SocialAccount.data.forEach(function(post_channel,index){
        if(post_channel.length !== 0)
        {
        socialArray.forEach(function(social_channel,inner_index){
          if(post_channel.SocialAccount.data.social_media === social_channel && old_post_id != new_post_id)
          {
            CalendarFilterArray.push(CalendarFilter_object);
            old_post_id = new_post_id;
          }
        });
      }
      })
    }
    else
    {
      CalendarFilterArray.push(CalendarFilter_object);
    }

    })
  }
  return {
    type: types.FETCH_FEED_CALENDAR_POSTS,
    feedCalendarPosts: CalendarFilterArray
  }
}

export function fetchCalendarScheduled(currPage, limit, postStatus, moment = null,firstDateTimestamp = null,lastDateTimestamp=null,feedId=null) {
  if(feedId!==null){
    var filterfeedId= `&feed_identity=${feedId}`
  }else{
    var filterfeedId=``
  }
  return (dispatch) => {
    fetch(Globals.API_ROOT_URL + `/post/user?published_status=scheduled&page=${currPage}&limit=${limit}&start_date=${firstDateTimestamp}&end_date=${lastDateTimestamp}${filterfeedId}`,{
      headers: utils.setApiHeaders('get')
    })
    .then(response => response.json())
    .then((json) => {
      if(Array.isArray(json.data) && json.data.length > 0) {
        // dispatch(fetchCalendarScheduledSuccess(json.data));
        dispatch(fetchCalendarPosts(json.data, moment))
      }
      else if(json.data.length == 0){
        dispatch(fetchCalendarPosts(json.data, moment))
      }
      else {  
        utils.handleSessionError(json)
      }
    })
    .catch(err => { throw err;
     dispatch(fecthScheduledError()) });
  }

}