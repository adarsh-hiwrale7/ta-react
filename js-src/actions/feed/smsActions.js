import * as types from "../actionTypes"
import Globals from "../../Globals"
import { browserHistory } from 'react-router'
import notie from 'notie/dist/notie.js'
import * as utils from '../../utils/utils'
import * as feedActions from './feedActions';
import * as campaignActions from '../campaigns/campaignsActions';
import {fetchCalendarScheduled} from './CalendarActions';


// call API for Sms/pop up----------------
export function getSmsValue(values){
    var sms=0
    var push=0
    if(values.messageType!==undefined){
      if(values.messageType=="sms"){
        sms = 1
        push =0
      }
      else if(values.messageType=="push"){
        sms = 0
        push = 1
      }
      else if(values.messageType=="both"){
        sms = 1
        push = 1
      }
    }
  
    
    return dispatch =>{ 
      var jsonBody ={
        "message": values.message,
        "push": push,
        "sms": sms
        }
    dispatch(getSmsValueInit());
    fetch(`${Globals.API_ROOT_URL}/broadcast`,{
          method: 'POST',
          headers: utils.setApiHeaders('post'),
          body: JSON.stringify(jsonBody)
        }
    )
    .then(response => response.json())
          .then( json =>{
            if(json.code == 200){
              dispatch(getSmsValueSuccess(json.data));
              var responseMessage = json.message
              notie.alert('success',responseMessage,5)
            }else{
             notie.alert('error',3)
              dispatch(getSmsValueFailed());
            }
          })
      }
    }
  
  export function getSmsValueFailed(error){
    return {
      type : types.SMS_VALUE_FAILED,
      //error:error
    }
  }
  export function getSmsValueSuccess(smsValue){
    return{
      type:types.SMS_VALUE_SUCCESS,
      data : smsValue
    }
  }
  export function getSmsValueInit(){
    return{
      type : types.SMS_LOADER
    }
  }
  //---------------
  