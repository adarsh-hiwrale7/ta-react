import * as types from './actionTypes';
import Globals from '../Globals';
import notie from "notie/dist/notie.js"
import * as utils from '../utils/utils';

// let searchInput;
export function globalSearchStart(inSearchSuggestFlag)
{
    var dom= document.querySelector(".react-autosuggest__container") ? document.querySelector(".react-autosuggest__container").classList:''
    dom.add("loading")
    return {
        type:types.GLOBAL_SEARCH_START,
        inSearchSuggestFlag:inSearchSuggestFlag
    };
}
export function resetSearchInput(){
    return{
        type:types.RESET_INPUT_SEARCH_TEXT
    }
}
export function seeAllClick(){
    return{
        type: types.SEE_ALL_CLICK,
    }
}
export function resetseeAllClick(){
    return{
        type: types.RESET_SEE_ALL_CLICK,
    }
}

export function isClickOnSuggestion(){
    return{
        type:types.IS_CLICK_ON_SUGGESTION
    }
}
export function resetSuggestionFlag(){
    return{
        type:types.RESET_SUGGESTION_FLAG
    }
}

export function  resetFlag(){
    return{
        type : types.RESET_FLAG
    }
}

export function fetchGlobalSuccess(data,input,suggestion=null,inSearchSuggestFlag){
    var flag_selected = false
    if(suggestion !== null)
        flag_selected = true
    return {
        type: types.GLOBAL_SEARCH_SUCCESS,
        payload:data,
        searchInput:input,
        suggestion:suggestion,
        flag_selected:flag_selected,
        inSearchSuggestFlag:inSearchSuggestFlag
    };

}

export function searchstart(){
    return{
        type:types.SEARCH_START
    }
}

export function searchsuccess(){
    return{
        type:types.SEARCH_SUCCESS
    }
}
export function globalSuccessLoader(data,input,suggestion=null,inSearchSuggestFlag)
{
    var dom=document.querySelector(".react-autosuggest__container")? document.querySelector(".react-autosuggest__container").classList:''
    dom.remove("loading")
    var flag_selected = false
    if(suggestion !== null)
        flag_selected = true
    return {
        type:types.GLOBAL_SEARCH_SUCCESS_LOADER,
        type: types.GLOBAL_SEARCH_SUCCESS,
        payload:data,
        searchInput:input,
        suggestion:suggestion,
        flag_selected:flag_selected,
        inSearchSuggestFlag:inSearchSuggestFlag
    };
}
/**
 * @function searchGlobal
 * @description fetches  global search results from the API depending on search input
 * @author Tejal Kukadiya
 * @param {*} input - Search String
 */
export function searchGlobal(input,suggestion_selected=null,auto_suggest=false,flag=false,inSearchSuggestFlag=false){

        var GS_API,flag=0
        var encodeInput = encodeURIComponent(input);
        if(auto_suggest==false)
        {
            GS_API = Globals.API_ROOT_URL + `/globalSearch?search=${encodeInput}`
        }
        else
        {
            GS_API = Globals.API_ROOT_URL + `/globalSearch?search=${encodeInput}&limit=2&auto_search=true`
        }
        return (dispatch) => {
                dispatch(searchstart())
            dispatch(globalSearchStart(inSearchSuggestFlag))
            fetch(GS_API,{
                headers: utils.setApiHeaders('get'),
            })
                .then(response => response.json())
                .then((json) => {
                    if (json.code!==200) {
                        dispatch(fetchGlobalSuccess(json,input,suggestion_selected,auto_suggest,inSearchSuggestFlag));
                        dispatch(globalSuccessLoader(json,input,suggestion_selected,auto_suggest,inSearchSuggestFlag));
                        dispatch(searchsuccess());
                        //dispatch({type:types.GLOBAL_SEARCH_SUCCESS,payload:json.data });
                    }
                    else if(json.code==200)
                    {
                        // notie.alert('error',json.message, 3);
                        dispatch({type:types.GLOBAL_SEARCH_ERROR,payload:json });
                    }
                }).catch(err => {
                throw err;
            });
        }
}