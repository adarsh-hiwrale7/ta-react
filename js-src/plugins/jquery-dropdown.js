import Globals from '../Globals';

Globals.jQuery(document).ready(function (e) {
function AssignWidth(){
        setTimeout(function(){
            e('.rbc-time-column').each(function(){
                classArray = [];
                e(this).find('.rbc-event').each(function(){
                    var top = parseInt(e(this)[0].style.top)
                    e(this).removeAttr('class')
                    e(this).addClass('rbc-event rbc-event-'+top)
                    if(classArray.indexOf(top)==-1)
                    {
                        classArray.push(top)
                    }
                })
                var _this = e(this)
                classArray.forEach(function(top){
                    var eleWidth = 100 / _this.find('.rbc-event-'+top).length;
                    var i = 0;
                    e('.rbc-event-'+top).each(function(){
                      e(this).css({'width':eleWidth+'%','left':i*eleWidth+'%'})
                      i++;
                    })
                 })
              })

        },1000)
    }

  function t(t) {
      e(t).bind("click", function (t) {
          t.preventDefault();
          e(this).parent().fadeOut()
      })
  }

/**
 * @auther : kinjal birare
 * This method for for createpost scroll
 */

 var lastScrollTop = 0;
 var delta = 100;
 var didScroll ;
 var createPostHeight = 78; 
 
// on scroll, let the interval function know the user has scrolled
$(window).scroll(function(event){
    hasScrolled();
    didScroll = true;
  });

 // run hasScrolled() and reset didScroll status
// setInterval(function() {
//     if (didScroll) {
//       hasScrolled();
//       didScroll = false;
//     }
//   }, 250);

function hasScrolled(){
  var st = e(window).scrollTop();
  if(Math.abs(lastScrollTop - st) <= delta && st !==0 && st !==78){
      return
  }
// If current position > last position AND scrolled past createpost...
if(st==0 || st ==78 || st < 95){
  e('.openPopupOnScroll').removeClass('scroll-up');
  e('.openPopupOnScroll').removeClass('scroll-down');
  lastScrollTop = st;
}else{
  if(st > lastScrollTop && st > createPostHeight){
    e('.openPopupOnScroll').removeClass('scroll-down').addClass('scroll-up');
 }else if(st + e(window).height()   < e(document).height()){
     if(window.pageYOffset + 55 >= createPostHeight){
        e('.openPopupOnScroll').removeClass('scroll-up').addClass('scroll-down');
     }else{
       
        e('.openPopupOnScroll').removeClass('scroll-up');
        e('.openPopupOnScroll').removeClass('scroll-down');
     }
 }else{}
  lastScrollTop = st;
}
}
e(document).on('fullscreenchange webkitfullscreenchange mozfullscreenchange MSFullscreenChange', function() {
    // visibly-widget-footer-poweredby
    e('.powerd-by-visibly-logo-container').toggleClass("go-to-fullscreen");
});

/**
 * @auther : kinjal birare
 * This method for the collapsible navigation bar
 */

var clearCollapsibleMouse ;
e(document).on('mouseenter', '.nd-sib', function(){ 
    clearTimeout(clearCollapsibleMouse);
    clearCollapsibleMouse = setTimeout(function(){
            e('body').addClass("collapsibleNavdrawOnHover"); 
          }, 1000);
     }
);
e(document).on('mouseleave', '.nd-sib', function(){ 
    clearTimeout(clearCollapsibleMouse);
    clearCollapsibleMouse = setTimeout(function(){
      e('body').removeClass("collapsibleNavdrawOnHover"); 
    }, 1000);
 }
);
e(document).on('click', '.nav-side .rc-collapse ul li a ,#navdraw .nav-side ul li a', function(e){ 
    $('body').removeClass("collapsibleNavdrawOnHover"); 
    $('body').removeClass("ShowCollapsibleForNavigation"); 
});



// for this method use for add the class of 3-dot menu
e(document).on('click', '.action-button .dropdown-toggle', function(e){ 
     $("tr.dropdownMenuContainer").removeClass("dropdownMenuContainer");
     $(this).closest('tr').addClass("dropdownMenuContainer");
});

//this jquery for open the url in open the newtab
$(document).on('click', '.text_readmore a', function(e){ 
    e.preventDefault(); 
    var url = $(this).attr('href'); 
    window.open(url, '_blank');
});

e(document).on('click', '.rc-collapse-header', function(){
    setTimeout(function(){
        e('.nd-sib').scrollTop(0);
    },200)

})

e(document).on('mouseenter', '.author-thumb .thumbnail', function(event){
    e('.author-thumb').removeClass('showtotop');
    if(e(this).offset().top+300>e(window).scrollTop()+e(window).height()){
        e(this).closest('.author-thumb').addClass('showtotop');
    }
});

e(document).on('click', '.author-thumb .thumbnail', function(event){
    e(this).closest('.post').addClass('showUserPopupMobile');
});
e(document).on('click', '.closeUserPopup', function(event){
    e(this).closest('.post').removeClass('showUserPopupMobile');
    e(this).closest('.user-popup-parent').removeClass('showUserPopupMobile');
});

e(document).on('click', '.user-popup-thumb', function(event){
    e(this).closest('.user-popup-parent').addClass('showUserPopupMobile');
});
e(document).on('click', '.custom-feed-toggle', function(event){
    e(this).toggleClass('active');
    e('.custom-sidebar-feed-wrapper .custom-feed-list').slideToggle();
});
e(document).on('mouseHover', '.multiple-assign .author-image', function(event){
  e(this).toggleClass('active');
  e('.custom-sidebar-feed-wrapper .custom-feed-list').slideToggle();
});
var resizeTimer;
e(document).on('keypress','.react-autosuggest__input',function(event){
    
    if (event.keyCode == 13) {
        event.preventDefault();
        e('.enterSuggestion').trigger('click')
    }
});
  e(window).resize(function(event){

    clearTimeout(resizeTimer);
  resizeTimer = setTimeout(function() {
    e('.setDates').trigger('click')
  }, 250);
if(e('#dropdown-asset-container .dropdown-container').length > 0 && e('#dropdown-asset-container .dropdown-container-inner').length > 0){
    if( e('.filter-li a').hasClass('filter_active') ){
        e('#dropdown-asset-container .dropdown-container').css('height',e('#dropdown-asset-container .dropdown-container-inner')[0].offsetHeight);
     }
}
if(e('#dropdown-asset-container .dropdown-container-inner').length>0 && e('.show-filter ').hasClass('active')){
       e('#dropdown-asset-container .dropdown-container').css('height',e('#dropdown-asset-container .dropdown-container-inner')[0].offsetHeight);
 }
});
e(document).on('click', '.btn-asset-submit', function(){
    var positionOfNullAssets = '';
    var heightOfNullAssets = '';
    positionOfNullAssets = e(".form-valid-message .error").closest(".asset-container-form-data").index();
    heightOfNullAssets = e(".asset-container-form-data").outerHeight();
    $('html, .ps').animate({ scrollTop: positionOfNullAssets * heightOfNullAssets - 20}, 'fast');
})
if(!e(".ps").scrollTop()){
    e('.ps').scrollTop(e('.ps').scrollTop()+1);
  }
  e(document).on('click', '.link-share-popup', function(){
  e(this).closest('.feed-post-item').siblings().find('.link-share-popup').next().addClass('hide');
  e(this).next().removeClass('hide');
  });


  //this trigger the close button of share popup
  e(document).click(function(event) {
    if(e(event.target).closest(".link-share-popup").length == 0  && e(event.target).closest(".sharePopup").length==0){
       e('.closeSharePopupPost').trigger('click');
     }
    //  if(e(event.target).closest(".sharePopupWrapper").length == 0){
    //     e('.share-popup-post').addClass('hide');
    //  }
  });
//to open or close share popup for job or rss
  e(document).on('click','.share-job',function(){
    e(this).closest('.slick-slide').siblings('.slick-slide').find('.share-popup-post').addClass('hide');
    e(this).siblings('.share-popup-post').toggleClass('hide');
    e(this).closest('.feed-job-item').siblings('.feed-job-item').find('.share-popup-post').addClass('hide');
    e(this).closest('.curatedParent').siblings('.curatedParent').find('.share-popup-post').addClass('hide');
  });
  
  e(document).on('click','.gSearchHandle',function(){
    e('.globalSearchForm').addClass('show');
  });
e(document).on('click','.closeGlobalSearch',function(){
    e('.globalSearchForm').removeClass('show');
});
/*e(document).on('click','.quick-survery-options ul li',function(){
    //e('.quick-survery-options ul li').removeClass('active');
    e(this).addClass('active');
    /*e('.inner-quick-suvery-content').addClass('Success-quick-survery');
    setTimeout(function(){
      e(".quick-survey-post").fadeOut(1000)
      }, 5000);
})*/
  e(document).on('click','.close-slider-popup',function(){
      e('body').removeClass('overlayForFeedPopup');
      e('#close-popup').trigger('click');
  })
 //** *this is add by @kinjal ,this method for click the userPopup in responsive */
e(document).on('click','.tags a ',function(){
     
      e('#pop-up-tooltip-holder').removeClass('hide');
      e('#pop-up-tooltip-wrapper').removeClass('hide');
})
 e(document).on('click', '.rbc-show-more', function(){
      var selected_date = new Date(e('.rbc-overlay-header').text());
      selected_date = selected_date.getDate()
      var row_number = e(this).closest('.rbc-month-row').index()-1
      var column_number;
      e('.rbc-month-row').eq(row_number).find('.rbc-date-cell').each(function(){
       if(e(this).text()==selected_date)
       {
        column_number = e(this).index();
       }
      })
      e('.rbc-day-bg').removeClass('selected_cell');
      e('.rbc-month-row').eq(row_number).find('.rbc-day-bg').eq(column_number).attr({'data-row-number':row_number,'data-column-number':column_number}).addClass('selected_cell')
  })
  var currentPage;
  e(document).on('click', '.cal-prev', function(){
    e('.rbc-btn-group:first-child button:nth-child(2)').trigger('click');
   });
  e(document).on('click', '.cal-next', function(){
    e('.rbc-btn-group:first-child button:nth-child(3)').trigger('click');
   });
   e(document).on('click', '.cal-current-month', function(){
    e('.rbc-btn-group:first-child button:nth-child(1)').trigger('click');
   });
   e(document).on('click', '.cal-month-view', function(){
    e('.calender-view-btn').removeClass('active');
    e(this).closest('.calender-view-btn').addClass('active');
    e('.rbc-btn-group:nth-child(3) button:nth-child(1)').trigger('click');
    e('.currentMonthTitle').html(e('.rbc-toolbar-label').text());
   });
   e(document).on('click', '.cal-week-view', function(){
    e('.calender-view-btn').removeClass('active');
    e(this).closest('.calender-view-btn').addClass('active');
    e('.rbc-btn-group:nth-child(3) button:nth-child(2)').trigger('click');
    e('.currentMonthTitle').html(e('.rbc-toolbar-label').text());
   });

  e(document).on('click', '#navdraw .nav-side ul.parent li > a', function(){
      e(this).closest('.rc-collapse-item').addClass('active');
      e(this).closest('.rc-collapse-item').siblings().removeClass('active');
  });


  e(document).click(function(event) {

    if (e(event.target).closest(".dashborard-popup").length==0) {
        e("body").find(".popup-wrapper-for-avtar").addClass("userpopup-notification");
    }
  });

  e(document).click(function(event) {
      if(e(event.target).closest(".notifaction-data").length==0){
          e("body").find(".notification-on-header").addClass("userpopup-notification");
          e("body").removeClass("notification-show");
      }
      if(e('.emojiPicker').length == 0){
        if(e(event.target).closest(".schedule-btn").length==0 && e(event.target).closest(".schedule-popup").length==0 && e(event.target).closest(".bottom-left-buttons").length==0  ){
           e('.schedule-close-btn').trigger('click');
         }

        if(e(event.target).closest(".gif-button-feed").length==0 && e(event.target).closest(".giphy-popup").length==0  && e(event.target).closest(".bottom-left-buttons").length==0){
           e('.giphy-close-btn').trigger('click');
         }
      }
});
  e(document).click(function(event) {
    //if you click on anything except the dropdown-menu  itself or the "dropdown-toggle" link, close the dropdown-menu
    if (e(event.target).closest(".dropdown-menu").length==0 && e(event.target).closest(".dropdown-toggle").length==0 && !e(event.target).hasClass('move-next-text') && (e(event.target).closest(".move-next-arrow").length==0)) {
        e("body").find(".dropdown-menu").addClass("hide");
        e("body").find(".dropdown-menu").removeClass("show");
        e("body").find(".dropdown-toggle").removeClass("active");
        e('.nd-parent').removeClass('removeOverflow');
    }
  });

  e(document).on('click', '.dropdown-toggle', function(){
    
    //if you click on the droupdown-toggle then add the active class and remove hide class
    if(e(this).closest('tr').length > 0){
        e(this).closest('tr').siblings().find(".dropdown-menu").removeClass("show");
    }
    if(e(this).closest('li').length > 0){
        e(this).closest('li').find(".dropdown-menu").removeClass("hide");
        e(this).closest('li').find(".dropdown-menu").toggleClass("show");
        e('.nd-parent').toggleClass('removeOverflow');
        e(this).closest('li').find(".dropdown-toggle").toggleClass("active");
    }
    if(e(this).closest('.calendarNavTitleWrapper').length > 0){
        e(this).closest('.checkbok-menu-button').find(".dropdown-menu").removeClass("hide");
        e(this).closest('.checkbok-menu-button').find(".dropdown-menu").addClass("show");
        e(this).closest('.checkbok-menu-button').find(".dropdown-toggle").addClass("active");
    }
})

e(document).on("click",".value-item-inner",function(){
    e(this).find('.value-list-descripation').slideDown(5000);
})

e(document).on('click', '.feed-job-share-links-wrapper a.rss-share-link', function(){
    e(this).closest('.col-four-of-twelve').siblings('.col-four-of-twelve').find('.sharePopup').removeClass('active');
    e(this).closest('.feed-job-share-item').siblings('.feed-job-share-item').find('.sharePopup').removeClass('active');
    e(this).closest('.feed-job-share-item').find('.sharePopup').toggleClass('active');
})

  e(document).on('click', '.dropdown-toggle', function(){
    if(e('#add-new-popup').hasClass('show')){
        e('.noPostNotiCloseBtn').trigger('click');
    }
    var ddmenu = e(this).parents(".button-dropdown").children(".dropdown-menu");
      var ddToggle = e(this).parents(".button-dropdown").children(".dropdown-toggle")
      var t = ddmenu.is(":hidden");
      //e(".button-dropdown .dropdown-menu").hide();
      e(".button-dropdown .dropdown-toggle").removeClass("active");
      if (t) {
          //ddmenu.toggle().parents(".button-dropdown").children(".dropdown-toggle").addClass("active")
          ddmenu.parents(".button-dropdown").children(".dropdown-toggle").addClass("active")
      }
  });
  e(document).on('click','.header .dd-menu .dropdown-menu li a',function(t){
    var n = e(t.target);
    if(!e(this).closest('.parent-li').length > 0 && n.closest('.campaignSegmentationTableWrapper').length==0){
       e(".button-dropdown .dropdown-menu").hide();
    }else{
      e(".button-dropdown .dropdown-menu").removeAttr('style');
    }
  });
  e(document).bind("click", function (t) {
    var n = e(t.target);
    if (n.closest('.button-dropdown').length==0 && n.closest('.campaignSegmentationTableWrapper').length==0)
    {
        e(".button-dropdown .dropdown-menu").hide();
        e(".button-dropdown .dropdown-toggle").removeClass("active");
    }else{
      e(".button-dropdown .dropdown-menu").removeAttr('style');
    }
    if( n.closest('.removeSelectedFolder').length==0 &&
    (e('.asset-dir.active').length>0 || e('.asset-file.selectedAsset').length>0) &&
    n.closest('.asset-dir .file-details').length==0 &&
    n.closest('.asset-file .assetInnerWrapper').length==0 &&
    n.closest('#assetActions').length==0 &&
    n.closest('#notie-confirm-no').length==0 &&
    n.closest('#notie-input-no').length==0 &&
    n.closest('.move-next-arrow').length==0 &&
    !n.hasClass('move-next-text') && !n.hasClass('move-back-arrow')
    ){
        e('.removeSelectedFolder').trigger('click');
    }
  });

  e(document).bind("click", function (t) {
    if(e(t.target).closest('.calendar-detail-popup').length==0 && e(t.target).closest('.calendar-event-title').length==0){
        e('.calendar-detail-popup #close-popup').trigger('click');
    }
  })

     var classArray;
     var rbc_event_length;
      e(document).on('click','.cal-week-view, .initializeStyle',function(){
        AssignWidth();

    });
    e(document).on('click','.calender-nav-btn',function(){
        AssignWidth();
    });

    e(document).on('click','#close-popup',function(){
        e(".disable-overlay ").removeClass('uploadedAssets');

      });
    e(document).on('click',' selectAssetTabs',function(){
        e(".uploadAssetBtn").removeClass('hide');

     });

    e(document).on('click','.uploadAssetBtn a',function(){

      //  e(".uploadAssetBtn").addClass('hide');
    });

    e(document).on('click','.uploadDriveAssetBtn a',function(){

      //  e(".uploadDriveAssetBtn").addClass('hide');
    });
/* this method used for when click on browser backbutton */
      if (window.history && window.history.pushState) {
            $(window).on('popstate', function() {
            e('body').removeClass('overlay');

            //this code for when open the create post popup and
            //click on back button then remove the state of openpopup
            //and remove the overlay of the popup

            e('body').removeClass('overlayForFeedPopup');
            e('#close-popup').trigger('click');
		});
     }
     e(document).on('click', '.nextTour', function(){ 
        e('.tour-tab').removeClass('active');
        var currBtnId = e(this).attr('id');
        var id = currBtnId.replace('next','');
        e(`#tab${id}`).addClass('active');
      });
    e(document).on('click', '.backTour', function(){ 
        e('.tour-tab').removeClass('active');
        var currBtnId = e(this).attr('id');
        var id = currBtnId.replace('back','');
        e(`#tab${id}`).addClass('active');
      });
      var newSegmentCount = 0;
      /*e(document).on('click', '.addSegmentButton', function(){ 
        var segmentRow = e('.newSegmentRow').html();
        e('.addSegementSectionInner').append(segmentRow);
        e('.addSegementSectionInner .newSegmentColumnWrapper').eq(newSegmentCount).find('input[type="checkbox"]').attr('id','segement-'+newSegmentCount);
        e('.addSegementSectionInner .newSegmentColumnWrapper').eq(newSegmentCount).find('.checkbox').find('label').attr('for','segement-'+newSegmentCount);
        e('.selectAllSegmentCheckboxWrapper').addClass('show');
        newSegmentCount++;
      });

      e(document).on('click', '.removeSegmentBtn', function(){ 
        e(this).closest('.newSegmentColumnWrapper').remove();
        if($(this).closest('.newSegmentColumnWrapper').length>0){
          newSegmentCount--
        }
        if(newSegmentCount<=0){
          e('.selectAllSegmentCheckboxWrapper').removeClass('show');
        }
        
      });

      e(document).on('click', '.removeAllSegments', function(){ 
        e('.addSegementSectionInner .newSegmentColumnWrapper').remove();
          newSegmentCount = 0
          e('.selectAllSegmentCheckboxWrapper').removeClass('show');
        
        
      });*/



      e(document).on('click', '.commentToggleBtn', function(){ 
        e('.campaignDetailRightColumn').toggleClass('moveOut');
      });
      
      e(document).on('click', '.closeCommentsBtn', function(event){
        event.preventDefault(); 
        e('.campaignDetailRightColumn').removeClass('moveOut');
      });
      e(document).on('change', '.firstSegmentDropdown select', function(){ 
        var selectSegmentVal = e(this).find('option:selected').val()
        if(selectSegmentVal=='Age' || selectSegmentVal=='Start date'){
          e(this).closest('.newSegmentColumnWrapper').addClass('numeric-segment');
        }else{
          e(this).closest('.newSegmentColumnWrapper').removeClass('numeric-segment');
        }
      });
      e(document).on('click', '.selectAllCheckbox', function(){ 
        var mainCheckboxVal = e(this).find('input[type="checkbox"]').prop('checked');
        if(mainCheckboxVal){
          e(this).closest('table').find('td').find('input[type="checkbox"]').prop('checked',true)
        }
        else{
          e(this).closest('table').find('td').find('input[type="checkbox"]').prop('checked',false)
        }
      });

      e(document).on('click', '.selectAllSegmentCheckboxWrapper', function(){ 
        var mainCheckboxVal = e(this).find('input[type="checkbox"]').prop('checked');
        if(mainCheckboxVal){
          e('.addSegementSectionInner input[type="checkbox"]').prop('checked',true);
        }
        else{
          e('.addSegementSectionInner input[type="checkbox"]').prop('checked',false);
        }
      });
      

      
    var lastClickedLi;
    e(document).on('click', '.master-data-list li', function(){ 
      lastClickedLi = $(this).closest('.master-column').attr('id');
      
    });
    e(document).on('click', '#notie-confirm-yes', function(){ 
      $('#'+lastClickedLi).find('.hierarchyNextButton').trigger('click');
      });
      e(document).on('click', '.hmsdbrItm', function(){
        e(this).toggleClass('active');
        e(this).find('.heirarchyList').slideToggle();
       })
     
    //campaign  accordion jquery
    e(document).on('click', '.accordion-tab-campaign .mainTitle', function(){ 
      e(this).parent('.accordion-tab-campaign').toggleClass('show-tab');
    });
   
  
  
});
