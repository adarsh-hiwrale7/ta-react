import notie from "notie/dist/notie.js"

export function Success(message,time=5) {
    return notie.alert('success', message, time);
}
export function Error(message,time=5) {
    return notie.alert('error', message, time);
}
export function Info(message,time=5) {
    return notie.alert('info', message, time);
}
export function Warning(message,time=5) {
    return notie.alert('warning', message, time);
}
export function Neutral(message,time=5) {
    return notie.alert('neutral', message, time);
}
export function Force(message,time=5) {
    return notie.alert('force', message, time);
}


