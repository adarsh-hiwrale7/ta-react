import Globals from '../Globals';
import {logout } from '../actions/authActions';
import CryptoJS from '../components/Chunks/ChunkCryptoJs';
export function initialsFromName(str) {
	let strArray = str.split(" ");
	let output = strArray.map(function(item) {
		return item.charAt(0).toUpperCase();
	})

	return output.join("");
}
export function mimetype(){
  var data;
  return(
    data = [
   'text/csv','application/vnd.google-apps.kix','application/vnd.ms-powerpoint',
    'application/vnd.openxmlformats-officedocument.presentationml.presentation','application/x-vnd.oasis.opendocument.presentation','application/vnd.google-apps.presentation',
    'application/vnd.google-apps.document','application/vnd.ms-excel',
    'application/vnd.oasis.opendocument.spreadsheet','application/vnd.google-apps.spreadsheet','application/pdf','application/vnd.google-apps.photo',
    'application/vnd.google-apps.presentation','image/jpeg','image/jpg','image/png','image/gif','application/msword','application/vnd.google-apps.folder'
    ]
  )
}

export function removeTrailingSlash(url) {
    return (
        url!==undefined?url.replace(/\/$/, ""):'');
}
export function index(selector)
  {
    var selectorClass = `.${(selector.getAttribute('class'))}`
    var nodes = Array.prototype.slice.call( document.querySelectorAll(selectorClass)[0].parentElement.children);
    return nodes.indexOf( selector );
  }
export function closest(el, clazz) {
  var root = document.getElementById('app')
  while(!(el.classList.contains(clazz))){
        el = el.parentNode;

        if (!el || el==root) {
            return null;
        }
    }
    return el;
}
export function pauseVideo(selector) {
  var videos = document.querySelectorAll(selector);
    if(videos){
      for(var i=0; i<videos.length; i++)
      {
        videos[i].pause();
      }
    }
}
export function wrapInner(parent, wrapper, attribute, attributevalue) {
  if (typeof wrapper === "string") {
      wrapper = document.createElement(wrapper);
  }
  var div = parent.appendChild(wrapper)
            .setAttribute(attribute, attributevalue);

  while (parent.firstChild !== wrapper) {
         wrapper.appendChild(parent.firstChild);
    }
}
export function getFaIcon(extension) {
	let faIcon = 'fa-file-o';
	switch(extension) {
		case 'xls': case 'xlsx':
			faIcon = 'fa-table';
			break;
		case 'doc': case 'docx': case 'txt':
			faIcon = 'fa-file-text-o';
			break;
		case 'pdf':
			faIcon = 'fa-file-pdf-o';
			break;
		case 'ppt': case 'pptx':
			faIcon = 'fa-file-powerpoint-o';
			break;
		default:
			faIcon = 'fa-file-o';
			break;
	}

	return faIcon;
}

export function resizeImage(file,fileLocation, expectedWidth=600) {
    // utilizing the generateThumbnail function and not renaming it to keep things separate
    return generateThumbnail(file,fileLocation, expectedWidth);
}

export function generateThumbnail(file, fileLocation, expectedWidth = 600, noPreview = false) {
  return new Promise(
      function (resolve, reject) {

        if(file.type.match('image')){

        var fileLoader = new FileReader(),
        canvas = document.createElement('canvas'),
        context = null,
        imageObj = new Image(),
        base64Thumb = null;

        var blobImage = new Image();
        if(noPreview === false){
          blobImage.src = file.preview;
        }else{
          blobImage.src = URL.createObjectURL(file);
        }
        blobImage.onload = function() {
          var imgWidth = blobImage.width;
          var imgHeight = blobImage.height;
          var aspect_ratio = imgWidth / imgHeight

          var w_ratio = expectedWidth / imgWidth;

          var thumbWidth = imgWidth * w_ratio;
          var thumbHeight = thumbWidth / aspect_ratio;
          //create a hidden canvas object we can use to create the new resized image data
          canvas.id     = "thumbCanvas";
          canvas.width  = thumbWidth;
          canvas.height = thumbHeight;
          canvas.style.visibility   = "hidden";
          document.body.appendChild(canvas);
          //get the context to use
          context = canvas.getContext('2d');

          // check for an image then
          //trigger the file loader to get the data from the image

          if (file.type.match('image.*')) {

              fileLoader.readAsDataURL(file);
          }
          else {
            reject("not an image");
          }
              // setup the file loader onload function
          // once the file loader has the data it passes it to the
          // image object which, once the image has loaded,
          // triggers the images onload function
          fileLoader.onload = function(val) {
            var data = this.result;
            imageObj.src = data;
        };

        fileLoader.onabort = function() {
            reject("uploaded aborted")
        };

        fileLoader.onerror = function() {
            reject("error in reading file")
        };


        // set up the images onload function which clears the hidden canvas context,
        // draws the new image then gets the blob data from it
        imageObj.onload = function() {

            // Check for empty images
            if(this.width == 0 || this.height == 0){
                reject("thumbnail size is zero")
            } else {
                context.clearRect(0,0,thumbWidth,thumbHeight);
                context.drawImage(imageObj, 0, 0, this.width, this.height, 0, 0, thumbWidth, thumbHeight);
                base64Thumb = canvas.toDataURL("image/jpeg");
                window.setTimeout(function() {
                  canvas.parentNode.removeChild(document.querySelector("canvas"));
                }, 1000)
                resolve(dataURItoBlob(base64Thumb,imgHeight,imgWidth))

            }
        };
      }
        imageObj.onabort = function() {
            reject("thumbnail not created")
        };

        imageObj.onerror = function() {
            reject("thumbnail not created")
        };

    }
    //else condition for thumbnail of video file
    else if(file.type.match('video')){
      let video = document.createElement('video');
      video.crossOrigin= 'Anonymous';
      video.addEventListener('loadeddata',()=>{
        var imgWidth = video.videoWidth;
        var imgHeight = video.videoHeight;
        var aspect_ratio = imgWidth / imgHeight

        //600 is expected width which is set statically bcz 1500 px size is generating 1 mb thumbnail
        var w_ratio = 600 / imgWidth;

        var thumbWidth = imgWidth * w_ratio;
        var thumbHeight = thumbWidth / aspect_ratio;
        let canvas = document.createElement('canvas');
        canvas.width=thumbWidth;
        canvas.height=thumbHeight;
        canvas
          .getContext('2d')
          .drawImage(video,0,0,canvas.width,canvas.height);
        let urlData= canvas.toDataURL();
        resolve(dataURItoBlob(urlData,imgHeight,imgWidth))
      })
      video.src= fileLocation;

      video.currentTime = "2";
      }
    }
  )
  }

//below taken from http://www.howtocreate.co.uk/tutorials/javascript/browserwindow
export function getScrollXY() {
    var scrOfX = 0, scrOfY = 0;
    if( typeof( window.pageYOffset ) == 'number' ) {
        //Netscape compliant
        scrOfY = window.pageYOffset;
        scrOfX = window.pageXOffset;
    } else if( document.body && ( document.body.scrollLeft || document.body.scrollTop ) ) {
        //DOM compliant
        scrOfY = document.body.scrollTop;
        scrOfX = document.body.scrollLeft;
    } else if( document.documentElement && ( document.documentElement.scrollLeft || document.documentElement.scrollTop ) ) {
        //IE6 standards compliant mode
        scrOfY = document.documentElement.scrollTop;
        scrOfX = document.documentElement.scrollLeft;
    }
    return [ scrOfX, scrOfY ];
}

//taken from http://james.padolsey.com/javascript/get-document-height-cross-browser/
export function getDocHeight() {
    var D = document;
    return Math.max(
        D.body.scrollHeight, D.documentElement.scrollHeight,
        D.body.offsetHeight, D.documentElement.offsetHeight,
        D.body.clientHeight, D.documentElement.clientHeight
    );
}
export function windowSize(){
  var w = window.innerWidth
|| document.documentElement.clientWidth
|| document.body.clientWidth;

var h = window.innerHeight
|| document.documentElement.clientHeight
|| document.body.clientHeight;
  return {w,h};
}
export function shortenURL(url) {
	//let output = url;
	// need to either call bitly or backend API
	//return output;
}


export function limitWords(textToLimit, wordLimit)
{
var finalText = "";
var text2 = textToLimit.replace(/\s+/g, ' ');
var text3 = text2.split(' ');
var numberOfWords = text3.length;
var i=0;
if(numberOfWords > wordLimit)
{
for(i=0; i< wordLimit; i++)
finalText = finalText+" "+ text3[i]+" ";
return finalText+"...";
}
else return textToLimit;
}

export function limitLetters(textToLimit ,wordLimit=10 ){
var length = textToLimit.length;
var finalText=''
var i=0;
if(length > wordLimit)
{
for(i=0; i< wordLimit; i++)
{
finalText = finalText + textToLimit[i] ;
}
return finalText+"...";
}
else return textToLimit;
}


export function forceDownload(content, fileName, mimeType) {
  var a = document.createElement('a');
  mimeType = mimeType || 'application/octet-stream';

  if (navigator.msSaveBlob) { // IE10
    return navigator.msSaveBlob(new Blob([content], { type: mimeType }),     fileName);
  } else if ('download' in a) { //html5 A[download]
    a.href = 'data:' + mimeType + ',' + encodeURIComponent(content);
    a.setAttribute('download', fileName);
    document.body.appendChild(a);
    setTimeout(function() {
      a.click();
      document.body.removeChild(a);
    }, 66);
    return true;
  } else { //do   iframe dataURL download (old ch+FF):
    var f = document.createElement('iframe');
    document.body.appendChild(f);
    f.src = 'data:' + mimeType + ',' + encodeURIComponent(content);

    setTimeout(function() {
      document.body.removeChild(f);
    }, 333);
    return true;
  }
}

export function dataURItoBlob(dataURI,height ,width) {
  // convert base64 to raw binary data held in a string
  // doesn't handle URLEncoded DataURIs - see SO answer #6850276 for code that does this
  if(dataURI!==undefined){
    var byteString = atob(dataURI.split(',')[1]);

  // separate out the mime component
  var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0]

  // write the bytes of the string to an ArrayBuffer
  var ab = new ArrayBuffer(byteString.length);
  var ia = new Uint8Array(ab);
  for (var i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
  }

  // write the ArrayBuffer to a blob, and you're done
  var blob = new Blob([ab], {type: mimeString});
  var obj = {
    "height": height,
    "width" : width 
  }
  Object.assign(blob,obj)
  return blob;
  }
  // Old code
  // var bb = new BlobBuilder();
  // bb.append(ab);
  // return bb.getBlob(mimeString);
}


export function guid() {
  // Not actually GUID, but random numbers that looks like GUID
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
    s4() + '-' + s4() + s4() + s4();
}

export const taBase64 = { _keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=", encode: function (e) { var t = ""; var n, r, i, s, o, u, a; var f = 0; e = taBase64._utf8_encode(e); while (f < e.length) { n = e.charCodeAt(f++); r = e.charCodeAt(f++); i = e.charCodeAt(f++); s = n >> 2; o = (n & 3) << 4 | r >> 4; u = (r & 15) << 2 | i >> 6; a = i & 63; if (isNaN(r)) { u = a = 64 } else if (isNaN(i)) { a = 64 } t = t + this._keyStr.charAt(s) + this._keyStr.charAt(o) + this._keyStr.charAt(u) + this._keyStr.charAt(a) } return t }, decode: function (e) { var t = ""; var n, r, i; var s, o, u, a,c2; var f = 0; e = e.replace(/[^A-Za-z0-9+/=]/g, ""); while (f < e.length) { s = this._keyStr.indexOf(e.charAt(f++)); o = this._keyStr.indexOf(e.charAt(f++)); u = this._keyStr.indexOf(e.charAt(f++)); a = this._keyStr.indexOf(e.charAt(f++)); n = s << 2 | o >> 4; r = (o & 15) << 4 | u >> 2; i = (u & 3) << 6 | a; t = t + String.fromCharCode(n); if (u != 64) { t = t + String.fromCharCode(r) } if (a != 64) { t = t + String.fromCharCode(i) } } t = taBase64._utf8_decode(t); return t }, _utf8_encode: function (e) { e = e.replace(/rn/g, "n"); var t = ""; for (var n = 0; n < e.length; n++) { var r = e.charCodeAt(n); if (r < 128) { t += String.fromCharCode(r) } else if (r > 127 && r < 2048) { t += String.fromCharCode(r >> 6 | 192); t += String.fromCharCode(r & 63 | 128) } else { t += String.fromCharCode(r >> 12 | 224); t += String.fromCharCode(r >> 6 & 63 | 128); t += String.fromCharCode(r & 63 | 128) } } return t }, _utf8_decode: function (e) { var t = ""; var n = 0; var r = c1 = c2 = 0; while (n < e.length) { r = e.charCodeAt(n); if (r < 128) { t += String.fromCharCode(r); n++ } else if (r > 191 && r < 224) { c2 = e.charCodeAt(n + 1); t += String.fromCharCode((r & 31) << 6 | c2 & 63); n += 2 } else { c2 = e.charCodeAt(n + 1); c3 = e.charCodeAt(n + 2); t += String.fromCharCode((r & 15) << 12 | (c2 & 63) << 6 | c3 & 63); n += 3 } } return t } }

export function b64DecodeUnicode(str) {
  // Going backwards: from bytestream, to percent-encoding, to original string.
  return decodeURIComponent(atob(str).split('').map(function(c) {
      return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
  }).join(''));
}

// you can do this once in a page, and this function will appear in all your files
File.prototype.convertToBase64 = function(callback){
        var reader = new FileReader();
        reader.onload = function(e) {
             callback(e.target.result)
        };
        reader.onerror = function(e) {
             callback(null);
        };
        reader.readAsDataURL(this);
};

export function cloneArr(arr) {
  return arr.slice();
}

export function strToCamel(str) {
  return str.split(' ').map(function(word){
    return word.charAt(0).toUpperCase() + word.slice(1);
  }).join('');
}

export function capitalizeFirstLetter(str){
  if(str !== undefined){
    return str.charAt(0).toUpperCase() + str.slice(1)
  }
}
export function unixToJStimestamp(timestamp) {

  return Math.floor(timestamp/1000);
}

export function animate(elem, style, unit, from, to, time, prop) {
    if (!elem) {
        return;
    }
    var start = new Date().getTime(),
        timer = setInterval(function () {
            var step = Math.min(1, (new Date().getTime() - start) / time);
            if (prop) {
                elem[style] = (from + step * (to - from))+unit;
            } else {
                elem.style[style] = (from + step * (to - from))+unit;
            }
            if (step === 1) {
                clearInterval(timer);
            }
        }, 25);
    if (prop) {
    	  elem[style] = from+unit;
    } else {
    	  elem.style[style] = from+unit;
    }
}
export function replaceTags1(str = "", usertags = [], tags = []) {
    for (var i = 0; i < usertags.length; i++) {
        str = str.replace('@' + usertags[i].name, "<a onClick='" +handleTag(usertags[i].identity)+"'>@" + usertags[i].name + "</a>");
    }
    for (var i = 0; i < tags.length; i++) {
        str = str.replace('#' + tags[i].name, "<a>#" + tags[i].name + "</a>");
    }
    return str;
}

export function replaceTags(str = "", usertags = [], tags = []) {
    let userTagCount = 0,
    reg = /[@|#](.*?) /g,
    result = '',
    test = str + " "
    while ((result = reg.exec(test)) !== null) {
      if (usertags.length > 0) {
        if (result[0].charAt(0) === "@") {
          if(usertags[userTagCount] !== undefined){
              let types = usertags[userTagCount].type === "department" ? "_dept" : "";
              test = test.replace(result[0], "@[" + result[1] + "]"
                + "(user:" + usertags[userTagCount].identity + types + ") ");
              userTagCount++
          }
        }
      }
      if (tags.length > 0) {
        for (var t = 0; t < tags.length; t++) {
          test = test.replace(result[0], "@[" + result[1] + "]"
            + "(tags:" + tags[t].identity + ") ");
        }
      }
    }
    return test.replace(/~+$/, '')
}
export function replacePermissionTags(str = "", usertags = [], tags = []) {
  let userTagCount = 0,
  reg = /[@|#](.*?) /g,
  result = '',
  test = str + " "
  while ((result = reg.exec(test)) !== null) {
    if (usertags.length > 0) {
      if (result[0].charAt(0) === "@") {
        let type = (usertags[userTagCount].type === "department" ? "_dept" : "")
        test = test.replace(result[0], "@[" + result[1] + "]"
          + "(user:" + usertags[userTagCount].id + type + ") ");
        userTagCount++
      }
    }
    if (tags.length > 0) {
      for (var t = 0; t < tags.length; t++) {
        test = test.replace(result[0], "@[" + result[1] + "]"
          + "(tags:" + tags[t].id + ") ");
      }
    }
  }
  return test.replace(/~+$/, '')
}
// to convert sortname into emoji face
export function toUnicode(code) {
    var codes = code.split('-').map(function (value, index) {
      return parseInt(value, 16);
    });
    return String.fromCodePoint.apply(null, codes);
  }
  //to convert emoji into unicode
  export function toUnicodetwo(str) {
    return str? str.split('').map(function (value, index, array) {
      var temp = value.charCodeAt(0).toString(16).toUpperCase();
      if (temp.length > 2) {
        return '\\u' + temp;
      }
      return value;
    }).join(''):'';
  }

  //to convert unicode into emoji
  export function convertUnicode (input) {
    if(input!==undefined || input!==''){
    return input.replace(/\\u(\w\w\w\w)/g, function (a, b) {
      var charcode = parseInt(b, 16)
      return String.fromCharCode(charcode)
    })
  }
  }
/**
   * @use to convert special character to unicode
   * @author disha
   * @param str string to convert in unicode
   */
  export function convertSpecialCharacterToUnicode(str){
    return str.replace(/[^\0-~]/g, function(ch) {
        return "\\u" + ("0000" + ch.charCodeAt().toString(16)).slice(-4);
    });
}
  export function blobToUrl(blob){
      var blobImage = new Image();
      blobImage.src = URL.createObjectURL(blob);
      return blobImage;
  }

	export function  updateText(id, type, e) {
	    this.setState({overlay_flag: true})
	    if (type === 'department') {
	      this
	        .props
	        .departmentActions
	        .fetchDepartmentById(id);
	    } else {
	      this
	        .props
	        .userActions
	        .fetchUserDetail(id);
	    }
	    const domNode = ReactDOM.findDOMNode(e.target);
	    let coords = domNode.getBoundingClientRect();
	    let coords_top = coords.top + pageYOffset;
	    let popup_wrapper = document.getElementById("pop-up-tooltip-wrapper");
	    let coords_left = 0,
	      coords_area = 0,
	      new_class_comb = '';
	    let pop_holder = document.getElementById("pop-up-tooltip-holder");
	    let popupparent = document.getElementById('department-popup');
	    if (popupparent != null) {
	      pop_holder.className = 'departmentpopup';
	    } else {
	      pop_holder.className = '';
	    }
	    if (screen.width >= 768) {
	      coords_left = coords.left + pageXOffset + (coords.width / 2);
	    } else {
	      coords_left = coords.left + pageXOffset;
	      coords_area = coords_left + 472;
	      if (coords_area < screen.width) {
	        new_class_comb = 'popup-left';
	      } else {
	        new_class_comb = 'popup-right';
	      }

	    }
	    if (e.nativeEvent.screenY < 400) {
	      new_class_comb += ' bottom';
	      if (e.target.className == "thumbnail") {
	        new_class_comb += ' thumbnai_popup';
	      }
	    } else {
	      new_class_comb += ' top';
	    }

	    popup_wrapper.className = new_class_comb;
	    this.setState({
	      popupTop: coords_top,
	      popupLeft: coords_left,
	      popupdisplay: 'block',
	      tagType: type,
	      tagTypeClass: (type === "department"
	        ? 'departmentpopup'
	        : '')
	    });
	    // let pop_overlay = document.getElementById("tag-popup-overlay");
	    // pop_overlay.className = 'show';

	  }


    /**
     * @author disha
     * to get url list from description
     * @param {*} text
     */
    export function checkLinkInPost(text){
      var source = (text || '').toString();
      var urlArrayObj = [];
      var url;
      var matchArray;
      var previousLinkArray=[]
      // Regular expression to find FTP, HTTP(S) and email URLs.
      var regexToken = /(((ftp|https?):\/\/|(www|visi.ly))[\-\w@:%_\+.~#?,&\/\/=]+)|((mailto:)?[_.\w-]+@([\w][\w\-]+\.)+[a-zA-Z]{2,3})/g;
      // Iterate through any URLs in the text.
      while( (matchArray = regexToken.exec( source )) !== null )
      {
        var appendStr='https://'
        //when url is start with www then to append https:// in url
          matchArray[0].startsWith('www.')?
              matchArray[0]=appendStr.concat(matchArray[0])
          :  matchArray[0].startsWith('visi.bly') ? 
              matchArray[0]=appendStr.concat(matchArray[0])  
          :''
          var token = matchArray[0];
          var link={
            link:token
          }
          if(previousLinkArray.indexOf(token)==-1){
              urlArrayObj.push( link );
          }
          previousLinkArray.push(token)
      }
      return urlArrayObj

    }
/**
 * @author disha 
 * to convert mb into different format according to their size
 * @param {*} size pass size  to convert
 */
export function sizeConverter(size) {
  var objToSend = {}
  switch (true) {
    case size >= 1073741824:
      objToSend = {
        storageType: 'PB',
        size: (size / (1024 * 1024 * 1024)).toFixed(2)
      }
      break;
    case size >= 1048576 && size < 1073741824:
      objToSend = {
        storageType: 'TB',
        size: (size / (1024 * 1024)).toFixed(2)
      }
      break;
    case size > 1024 && size < 1048576:
      objToSend = {
        storageType: 'GB',
        size: (size / 1024).toFixed(2)
      }
      break;
    case size <= 1024 && size >= 1:
      objToSend = {
        storageType: 'MB',
        size: size
      }
      break;
    case size < 1:
      objToSend = {
        storageType: 'KB',
        size: size * 1024
      }
      break;
    default:
      objToSend = {
        storageType: 'MB',
        size: size
      }
  }
  return objToSend;
}


    /**
     * @author disha
     * @use this function will encode data (sessionid) in random selected algorithm
     * @argument id pass session id or data which want to encode
     */
    export function encodeSessionId(id){
      // var secretKey=`DKVEuYqem8wp8Q2ru7Znq8oVGZG/F5KVQRxaFANBCZNdnKxSTK9QIDAQAB
      // AoGAdDhIV/+vbj3fY/EkUbcLULQw8pX8XLrZjOh7Di5LM6Wr1Io+7CLwIU8SuMdJ
      // mKFeUuT7h8rA3OFfxe4b/TuqFqtydiv4qAQmDlSQM76QFJN95kM/J7Y+VPRS1e2V
      // +QQGD+GIaCps4OPtX5daWBjIuV7d3HgtbpskH7NrNqx6u9ECQQDe5pOMnlh9J2ub
      // qOD7HLnPgEZoO8Hhz9N3LUhrsno3P+`
      // var allAlgorithms=['HS256','HS512','HS384']
      // var finalEncryptedString=''
      // var rand = allAlgorithms[Math.floor(Math.random() * allAlgorithms.length)];
      // var jwt = nJwt.create(id,secretKey,rand);  // njwt plugin is uninstalled so if you want to use this code you must install this plugin
      // var token = jwt.compact(); 
      // if(rand=='HS256'){
      //     finalEncryptedString=`${token}hdat4qEIFdSmQRzo`
      // }else if(rand=='HS512'){
      //     finalEncryptedString=`${token}UNENFcDvxAOfyXAh`
      // }else if(rand=='HS384'){
      //     finalEncryptedString=`${token}PKAA98tMWP18AwQQ`
      // }
      // return finalEncryptedString;
      return id
    }

/**
 * @author disha
 * @use to logout page if there is 401 error ( session)
 * @param data json data
 */
export function handleSessionError(data) {
  if (typeof data.error !== 'undefined') {
    if (data.error.error == "Your session id or key is wrong" || data.error.secret == "Unauthorized") {
      document.body.classList.remove('overlay');
      logout()
    }
  }
}
/**
 * @use to truncate text if its length is more than 280 char
 * @author disha
 * @param text string to truncate
 */
export function truncateTwitterText(text){
  var finalText=text.substring(0,Globals.TWITTER_LIMIT_WORD)
  return finalText
}

/**
 * @use to remember user credentials
 * @author Tejal Kukadiya
 * set  new cookie and store user credentials into it
 */
export function newCookie(name,value,days) {
  var path = this.removeTrailingSlash(window.location.href).split('/');
  var domainName=path !==undefined && path[2] !==undefined ?path[2].split(':')[0]:''
 var days = 1;   // the number at the left reflects the number of days for the cookie to last
 if (days) {
   var date = new Date();
   date.setTime(date.getTime()+(days*24*60*60*1000));
   var expires = "; expires="+date.toGMTString(); }
   else var expires = "";
   document.cookie = name+"="+value+expires+`; domain=${domainName};path=/ `; 
 }

 export function  storeCredentials(email,password,rememberme) {
  this.newCookie('email', email); 
  CryptoJS().then(crypto=>{
    var encryptedPassword = crypto.AES.encrypt(password, Globals.secretKey);
    this.newCookie('password', encryptedPassword.toString());
    this.newCookie("rememberme",rememberme)
  })
    // to encrypt the password  
  
}

export function  readCookie(name) {
  var nameSG = name + "=";
  var nuller = '';
 if (document.cookie.indexOf(nameSG) == -1)
   return nuller;

  var ca = document.cookie.split(';');
 for(var i=0; i<ca.length; i++) {
   var c = ca[i];
   while (c.charAt(0)==' ') c = c.substring(1,c.length);
 if (c.indexOf(nameSG) == 0) return c.substring(nameSG.length,c.length); }
   return null; }

export function deleteCredentials() {
    var path = this.removeTrailingSlash(window.location.href).split('/');
    var domainName=path !==undefined && path[2] !==undefined ?path[2].split(':')[0]:''
    document.cookie =`email=; expires=Thu, 01 Jan 1970 00:00:00 UTC;domain=${domainName};path=/;`
    document.cookie =`password=; expires=Thu, 01 Jan 1970 00:00:00 UTC;domain=${domainName};path=/;`
    document.cookie =`rememberme=; expires=Thu, 01 Jan 1970 00:00:00 UTC;domain=${domainName};path=/;`
}

export function setApiHeaders(methodType,headers=null,sessionId=null) {
    if(methodType=='get'){
      return {
        ...headers,
        'sessionId':sessionId !==null ? sessionId :Globals.SESSION_ID,
        'device':'web'
      }
    }else{
      return {
        ...headers,
        'sessionId':sessionId !==null ? sessionId :Globals.SESSION_ID,
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'device':'web'
      }
    } 
}
/**
* For checking user is on mobile device or web
* @author Yamin
* @return device type
**/
export function detectmob() { 
     if( navigator.userAgent.match(/Android/i)){
        return "android"        
     }else if(navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPad/i) || navigator.userAgent.match(/iPod/i)){
        return "ios";
      }
     else {
        return "web";
      }
}
/**
* For getting right rand diff image
* @author Yamin
* @return image
**/

export function rankDiff(count, fromgraph = false) {
  var showgraphs = null
  var graphSign = ''
  if (count > 0) {
      graphSign = '+'
      showgraphs = <img src="/img/up.png" />
  } else if (count < 0) {
      graphSign = '-'
      showgraphs = <img src="/img/down.png" />
  }
  else {
      graphSign = ''
      showgraphs = <img className='no-image-chart' src="/img/neutral.png" />
  }
  if (fromgraph == true) {
      return (showgraphs)
  } else {
      return (graphSign)
  }
}

/**
* For getting current date
* @author Yamin
**/
export function currentDate(){
    var d = new Date();
    d.setMonth(d.getMonth() - 1);
    return d;
}
/**
*Get client timezone 
* @author Yamin
* @return timezone GMT +5:30
**/
export function clientTimezone(){
  //  var offset = new Date().getTimezoneOffset(), o = Math.abs(offset);
  //  var timezone = ((offset < 0 ? "+" : "-") + ("00" + Math.floor(o / 60)).slice(-2) + ":" + ("00" + (o % 60)).slice(-2));
  //  return timezone; 
}

export function getOrdinalNum(n) {
  return n + (n > 0 ? ['th', 'st', 'nd', 'rd'][(n > 3 && n < 21) || n % 10 > 3 ? 0 : n % 10] : '');
}
/**
get culture chart dimesion as per screensize
*@author Yamin
**/
export function getChartDimension(screenSize, type) {
  
  var newWidth=0
  var divid = document.getElementById('dashboardLineChart') !== null && document.getElementById('dashboardLineChart') !== undefined ? document.getElementById('dashboardLineChart').clientWidth : ''
  var leftSideChart=document.getElementById('leftSideChart') !== null && document.getElementById('leftSideChart') !== undefined ? document.getElementById('leftSideChart').clientWidth : ''
  var rightSideChart=document.getElementById('rightSideChart') !== null && document.getElementById('rightSideChart') !== undefined ? document.getElementById('rightSideChart').clientWidth : ''
  var bottomRightSideChart=document.getElementById('bottomRightSideChart') !== null && document.getElementById('bottomRightSideChart') !== undefined ? document.getElementById('bottomRightSideChart').clientWidth : ''
  var expandTotalPost=document.getElementById('expandTotalPost') !== null && document.getElementById('expandTotalPost') !== undefined ? document.getElementById('expandTotalPost').clientWidth : ''
  var guestDonutDiv=document.getElementById('guestDonutDiv') !== null && document.getElementById('guestDonutDiv') !== undefined ? document.getElementById('guestDonutDiv').clientWidth : ''
  var driversView = document.getElementById('driversData') !== null && document.getElementById('driversData') !== undefined ? document.getElementById('driversData').clientWidth : ''
  switch (type) {
    case 'adoptionChart': case 'totalPostChart': 
      if(type=='totalPostChart'){
        newWidth=rightSideChart !== ''? rightSideChart:guestDonutDiv!==''? guestDonutDiv-30:expandTotalPost!==''?(expandTotalPost/2)-20:''
      }else if(type=='adoptionChart'){
        newWidth=leftSideChart!== ''?leftSideChart:(expandTotalPost/2)-20
      }
      var GraphDimention = { width: parseInt(newWidth), height: 387 }
      break;
    case 'fromDashboard':
      var GraphDimention = { width: divid, height: 700 }
      if (screenSize < 1500 && screenSize >= 1100) {
        GraphDimention = { width: divid, height: 530 }
      } else if (screenSize < 1099 && screenSize >= 1024) {
        GraphDimention = { width: divid, height: 450 }
      } else if (screenSize < 1024 && screenSize > 767) {
        GraphDimention = { width: divid, height: 380 }
      } else if (screenSize <= 768) {
        GraphDimention = { width: divid, height: 300 }
      }
      break;
    case 'EngagementByChannel':
      var GraphDimention = { width: 220, height: 250, cx: 105, cy: 105, innerRadius: 65, outerRadius: 100 }
      if (screenSize <= 1500) {
        GraphDimention = { width: 180, height: 200, cx: 85, cy: 85, innerRadius: 55, outerRadius: 90 }
      }
      break;
    case 'LocationGraph':
       var GraphDimention = { width: parseInt(leftSideChart), height: 550 }
        if(screenSize <1400 && screenSize >=1201){
          GraphDimention = { width: parseInt(leftSideChart), height: 550 }
        }
      break;  
    case 'engagementOverTime':
      var GraphDimention={width:parseInt(bottomRightSideChart),height:710}
      if (screenSize <= 1500) {
        GraphDimention = {width:parseInt(bottomRightSideChart),height:660}
      }
      break;
    case 'totalPostExpand':
      var GraphDimention={width:parseInt(expandTotalPost),height:550}
      break;  
    case 'driversView':
      var GraphDimention={width:parseInt(driversView),height:550}
      break; 
    case 'contentScatter':
      var GraphDimention={width:parseInt(driversView),height:670}
      break; 
    case 'cultureChart': case 'guestDonutGraph' :
       var GraphDimention = { width: 280, height: 280, cx: 135, cy: 135, innerRadius: 95, outerRadius: 140 }
      if (screenSize <= 1500) {
        GraphDimention = { width: 200, height: 200, cx: 95, cy: 95, innerRadius: 65, outerRadius: 100 }
      }
      break;
    default:
       var GraphDimention = { width: 280, height: 280, cx: 135, cy: 135, innerRadius: 95, outerRadius: 140 }
     
      break;
  }
  return GraphDimention;

}