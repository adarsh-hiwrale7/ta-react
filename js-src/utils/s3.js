import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import * as s3Actions from '../actions/s3/s3Actions';
import notie from "notie/dist/notie.js"

import * as utils from './utils';
import Globals from '../Globals';
import tagsReducer from '../reducers/tagsReducer';


export class S3Manager {

  constructor(s3Config) {

    var me = this;

    this.bucketName = s3Config.bucketName; // 'talent-advocate';
    this.bucketRegion = s3Config.bucketRegion; // 'us-west-2';
    this.IdentityPoolId = s3Config.IdentityPoolId; //'us-west-2:3513858c-1d24-4a69-82f1-7096fdfd703b';
    this.albumName = s3Config.albumName;

  AWS.config.update({
      region: me.bucketRegion,
      credentials: new AWS.CognitoIdentityCredentials({
        IdentityPoolId: me.IdentityPoolId
      })
    });

    this.s3 = new AWS.S3({
      apiVersion: '2006-03-01',
      params: {Bucket: me.bucketName}
    });

  }


  s3uploadMedia(fileObject, index=0, noOfFiles=1) {
    var me = this;

    // use a promise as this is a async operation.
    return new Promise(
      function (resolve, reject) {
        var name = fileObject.name.substr(0, fileObject.name.lastIndexOf('.'))
        var ext = fileObject.name.substr(fileObject.name.lastIndexOf('.')+1);

        var fileName = name + "-" + utils.guid() + "." + ext;
        var albumPhotosKey = me.albumName + '/';

        var photoKey = albumPhotosKey + 'W'+fileName;
        me.s3.upload({
          Key: photoKey,
          ACL: 'public-read',
          ContentDisposition: 'attachment',
          ContentType: fileObject.type,
          Body: fileObject,
          /*Metadata: {
            'og%3Atitle': 'video',
            'og%3Aurl' : 'https://s3-us-west-2.amazonaws.com/talent-advocate/uploads/dhadkan165409f2-46bc-10c2-49b1-6b4b1c142dd1.mp4',
          },*/
        }, function(err, data) {
          if (err) {
            reject(err); // failure
          }
          else {
            if(noOfFiles-1==index) {
              Globals.dispatch(s3Actions.uploadingDone())
            }
            resolve({data: data, fileObject: fileObject, index}); // success
          }

        }).on('httpUploadProgress',function(progress) {
          var percent = (progress.loaded / progress.total) * 100;
          Globals.dispatch(s3Actions.uploadingProgress(fileObject.name, percent, index))
        });



    });


  }

  s3uploadThumbnail(file, index=0, noOfFiles=1, fileLocation, noPreview = false,s3VideoThumbnailFail=null) {
    var me = this;
    return new Promise(
      function (resolve, reject) {
        if(noPreview == true){
          var thumb = utils.generateThumbnail(file,fileLocation,500,noPreview);
        }else{
          var thumb = utils.generateThumbnail(file,fileLocation,500);
        }

        var name = file.name.substr(0, file.name.lastIndexOf('.'))
        var ext = file.name.substr(file.name.lastIndexOf('.')+1);
        var size=file.size
        var fileName = name + "-thumb-" + utils.guid() + ".jpg";
        var albumPhotosKey = me.albumName + '/';

        var photoKey = albumPhotosKey + "W"+fileName;
        var newblob=utils.dataURItoBlob(file.thumbnailSrc)

        // generated thumbnail - resolved promise
        thumb.then(thumbnail => {
          me.s3.upload({
            Key: photoKey,
            ACL: 'public-read',
            ContentType: "image/jpeg",
            Body: s3VideoThumbnailFail==true?newblob:thumbnail,
          }, function(err, data) {
            if (err) {
              reject(err); // failure
              notie.alert('error',  'Problem while uploading file. Please try again', 3)
            }
            else {
              if(noOfFiles-1==index) {
                Globals.dispatch(s3Actions.uploadingDone())
              }
              // added thumbnail to get the height and width info
              resolve({data,thumbnail}); // success

            }

          })
        })
      }
    )

  }

  /**
 * Gets the combined file size of all the objects in a S3 folder.
 * @param  {string}   bucket   The bucket that the folder exists in.
 * @param  {string}   folder   The folder to calculate the size of.
 * @param  {function} callback Callback function that will be called once size
 * has been calculated.
 */
getFolderSize = function(bucket, folder, callback) {
  this._getFolderSize(bucket, folder, callback);
};

/**
 * Internal recursive get folder size function.
 * @param  {string}   bucket   The bucket that the folder exists in.
 * @param  {string}   folder   The folder to calculate the size of.
 * @param  {string}   [marker] The object key to start with on request.
 * @param  {function} callback Callback function that will be called when an
 * error occurs or size has been calculated.
 */
_getFolderSize = function(bucket, folder, marker, callback) {
  var self = this;
  var params = {
    Bucket : bucket,
    Prefix : folder + '/'
  };
  if(typeof marker === 'function') {
    callback = marker;
    marker = null;
  }
  if(marker !== null) {
    params.Marker = marker;
  }

  (this.s3.client || this.s3).listObjects(params, function(err, data) {
    if(err) {
      return callback(err, null);
    }
    var size = 0;
    if(data.hasOwnProperty('Contents')) {
      size = self._calculateObjectsSize(data.Contents);
    }
    if(!data.IsTruncated) {
      return callback(null, size);
    }
    marker = data.Contents[data.Contents.length - 1].Key;
    self._getFolderSize(bucket, folder, marker, function(err, nsize) {
      if(err) {
        return callback(err, null);
      }
      return callback(null, size + nsize);
    });
  });
};

/**
 * Calculates the size of the objects in the array passed in.
 * @param  {array} objects The objects to add up.
 * @return {number}        The total size of the objects.
 */
_calculateObjectsSize = function(objects) {
  var size = 0;
  for (var i = 0; i < objects.length; i++) {
    size += objects[i].Size;
  }
  return size;
};
}

/**
 * Delets the media from s3 bucket
 * @author Akshay soni
 * @param  {array} objects The objects to add up.
 * @return {void} 
 */
export function deleteObjectFromS3(media,s3Config)
{
  var me = this;
  me.bucketName = s3Config.bucketName;
  me.s3 = new AWS.S3({
    apiVersion: '2006-03-01',
    params: {Bucket: me.bucketName}
  });
  var uploadedMedia = media
  var arraytoDeletemedia =[];
  
  for (var i = 0 ; i < uploadedMedia.length; i++){ 
        var mediaURL ={
            Key : uploadedMedia[i].fileUrl
        }
        var thumbURL ={
          Key : uploadedMedia[i].thumbUrl
      }
      arraytoDeletemedia.push (mediaURL)
      arraytoDeletemedia.push (thumbURL)
   }
  var deleteParam = {
    Bucket: me.bucketName,
    Delete: {
      Objects: arraytoDeletemedia
    }
  };    
  me.s3.deleteObjects(deleteParam, function(err, data) {
    if (err) console.log(err, err.stack);
    else {
      // deleted object will come in data obj
      // console.log('delete', data);
    }
  });
}

export function loadAWSsdk() {
   return new Promise(
      function (resolve, reject) {
      (function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) {
            resolve()
          }
          js = d.createElement(s); js.id = id;
          js.src = "//sdk.amazonaws.com/js/aws-sdk-2.45.0.js";
          fjs.parentNode.insertBefore(js, fjs);
          js.onload = function() {
            resolve()
          }
        }(document, 'script', 'aws-jssdk'));
      });
}
