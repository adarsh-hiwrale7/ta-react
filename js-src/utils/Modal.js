
import PropTypes from 'prop-types';

class Modal extends React.Component {
  componentDidMount() {
     
  }
  render() {
    // Render nothing if the "show" prop is false
    if (!this.props.show) {
      return null;
    } else {
      document.body.classList.add('close-sccore');
    }
  
    // The gray background
    const backdropStyle = {
      position: 'fixed',
      top: 0,
      bottom: 0,
      left: 0,
      right: 0,
      backgroundColor: 'rgba(0,0,0,0.5)',
      zIndex: 80,
     };

   // The modal "window"
    const modalStyle = {
      backgroundColor: '#fff',
      borderRadius: 5,
      maxWidth: 600,
      margin: '0 auto',
      padding:'0',
    };
   
    return (
      <div id="backdrop" className="backdrop" style={backdropStyle}>
        <div id="show-pop-moderation">
        <div className='modal' style={modalStyle}>
          <div class="inner-modal-div">
            <div class="feed-popup-likes">
            {this.props.children}
              <button id="close-popup1" class="btn-default" onClick={this.props.onClose}><i class="material-icons">clear</i></button>
            </div>
           </div>
          </div>
          <div class="pop-close" onClick={this.props.onClose}> </div> 
         </div> 
   </div>
   );
  }
}
Modal.propTypes = {
  onClose: PropTypes.func.isRequired,
  show: PropTypes.bool,
  children: PropTypes.node
};

export default Modal;