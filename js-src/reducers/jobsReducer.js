import * as types from '../actions/actionTypes'

const initialState = {
  createdJobInfo: [],
  creation: {
    created: false,
    loading: false
  },
  jobs: [],
  events: [],
  files: [],
  listLoading:false,
  jobSelectedChannels: [],
  jobFilters:[]
};

let jobsReducer = function(state = initialState, action) {
  switch (action.type) {
    case types.JOBS_CREATING:
      return { ...state, creation: { loading: true, created: false }, createdJobInfo: [] };
    case types.JOBS_CREATING_NEW:
      return { ...state, creation: { loading: false, created: false } };
    case types.JOBS_CREATION_SUCCESS:
      return { ...state, creation: { loading: false, created: true }, createdJobInfo: action.createdJobInfo };
    case types.JOBS_CREATION_ERROR:
      return { ...state, creation: { loading: false, created: false }, createdJobInfo: [] };
    case types.FETCH_JOBS_SUCCESS:
      return { ...state, jobs: action.jobs,initialJobs:action.initialJobs, listLoading: false, events: action.events};
    case types.FETCH_JOBS_START:
      return { ...state, jobs: [] ,listLoading:true };
    case types.FETCH_JOBS_CHANNELS_SUCCESS:
      return { ...state, jobSelectedChannels: action.jobSelectedChannels };
    case types.FETCH_FILTER_COUNT_SUCCESS:
      return { ...state, jobFilters: action.jobFilters };

    default:
      return state;
  }
}

export default jobsReducer
