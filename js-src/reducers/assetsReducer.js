
import * as types from '../actions/actionTypes'

const initialState = {
  createdAssetInfo: [],
  creation: {
    created: false,
    loading: false, 
  },
  campaigns: [],
  cats: [],       // this will hardly change in the overall application once the cats are fetched
  folders: [],    // these will be depending on the parent cat
  files: [],       // these will be particular folder's files or cat's files
  userFiles:[],
  nofiles: false,   // no files found in asset listing
  meta: {},
  loadingasset:false,
  loadingFolder: false,
  hashAssets:[],
  hashMsg:null,
  filesArchive: [],
  folderArchive: [],
  archiveCat: [],
  popupFolder: [],
  assetLoading: false,
  breadCrum: [],
  uploadedAssetBuffer:[],
  driveAssetsURL:null,
  driveAssetsUploading:false,
  addAssetInDBInit:false,
  bufferUpdated:false,
  openEditor:false,
  isArchiveFolder:false,
  isArchiveAsset:false,
  isUnarchiveFolder:false,
  isUnarchiveAsset:false,
  customfeedAsset:[],
  isAssetUploadingStart:false,
  undoSavedMediaSize:false,
  hashloading:false,
  driveFileData:[],
  loadmore :false
  //assetDataBeforeUpload:[]
}; 

let assetsReducer = function(state = initialState, action) {
  switch (action.type) {
    case types.ASSETS_UPLOADING_START:
      return {...state,isAssetUploadingStart:true}
      //  Upload drive file to s3  from assets

      case types.UPLOAD_DRIVE_ASSET_FILE_INIT:
        return { ...state, driveAssetsUploading:true };
            
      case types.UPLOAD_DRIVE_ASSET_FILE_SUCCESS:
        return { ...state, driveAssetsURL:action.assetS3URL , driveAssetsUploading :false ,driveFileData:action.DriveFileData};
  
      case types.UPLOAD_DRIVE_ASSET_FILE_ERROR:
        return { ...state,  driveAssetError:true, driveAssetsUploading:false };
  
      case types.UPLOAD_DRIVE_ASSET_FILE_DONE:
        return { ...state, driveAssetsUploading:false ,driveAssetsURL:null};
 
    case types.ASSETS_CREATING:
      // ASSETS_CREATING resets all flags and set the loading indicator to true
      // this means user has filled the asset details. It is being processed
      return { ...state, creation: { loading: true, created: false }, createdAssetInfo: [] };

      // ASSETS_CREATING_NEW_ASSET indicates user has just started to created a new asset
    case types.ASSETS_CREATING_NEW_ASSET:
      return { ...state, creation: { loading: false, created: false } };
    case types.ASSETS_CREATING_NEW_ASSET_EDITOR:
      return { ...state, creation: { loading: true, created: false } };  
    case types.ASSETS_LOADING:
      return { ...state, creation: {...state.creation, loading: true} };
    case types.ASSETS_CREATION_SUCCESS:
      return { ...state, creation: { loading: false, created: true }, createdAssetInfo: action.createdAssetInfo ,isAssetUploadingStart:false,undoSavedMediaSize:false};
    case types.ASSETS_CREATION_ERROR:
      return { ...state, creation: { loading: false, created: false }, createdAssetInfo: [] ,isAssetUploadingStart:false};

    case types.ASSETS_FETCH_FOLDERS_SUCCESS:
      return { ...state, folders: action.folders,loadingFolder:false };
    case types.FETCH_FOLDERS_INIT:
      return { ...state, loadingFolder:true };
    case types.ASSETS_FETCH_CATS_SUCCESS:
      return { ...state, cats: action.cats };

    case types.ASSETS_FETCH_CATS_CAMPAIGN_SUCCESS:
      return { ...state, campaigns: action.campaigns };

    case types.FETCH_HASH_INIT:
        return{...state ,hashloading:true} 
    case types.ASSETS_FETCH_FILES_START:
      return { ...state, nofiles: false, assetLoading: true};
    case types.ASSETS_FETCH_FILES_SUCCESS:
    if(action.search !== undefined && action.search !== "")
    {
        return { ...state, files: action.payload.data, nofiles: false, meta: action.payload.meta, assetLoading: false ,loadmore:false};
    }
    else if(action.gSearch !== undefined)
    {
        if(action.gSearchData !== null)
        {
            return { ...state, files: action.gSearchData.concat(action.payload.data), nofiles: false, meta: action.payload.meta, assetLoading: false ,loadmore:false};
        }
        else
        {
            return { ...state, files: state.files.concat(action.payload.data), nofiles: false, meta: action.payload.meta, assetLoading: false ,loadmore:false};
        }
    }
    else
    {
        return { ...state, files: state.files.concat(action.payload.data), nofiles: false, meta: action.payload.meta, assetLoading: false ,loadmore:false};
    }

    case types.LOAD_MORE_ASSETS:
        return{
            ...state,loadmore:true
        }

    case types.ASSETS_FETCH_USERS_FILES_SUCCESS:
      return { ...state, files: state.files.concat(action.payload.data), nofiles: false, meta: action.payload.meta ,loadmore:false};

    case types.ASSETS_NO_FILES_FOUND:
      return { ...state, nofiles: true,loadingasset:false, assetLoading: false ,loadmore:false};

    case types.ASSETS_FETCH_INITIAL_FILES_SUCCESS:
        return{...state, userFiles:action.payload}
    case types.ASSETS_FETCH_INITIAL_FILES_NODATA:
        return{...state,userFiles:[]}

    case types.ASSETS_FILES_RESET:
      return { ...state, files: [], loadingasset:true ,loadmore : false};

      case types.ASSETS_FOLDERS_RESET:
      return { ...state ,folders: [], loadingasset:true ,loadmore : false};

    case types.APPEND_NEW_FOLDER:
      return { ...state, folders: state.folders.concat(action.folder)}

    case types.APPEND_NEW_CAT:
      return { ...state, cats: state.cats.concat(action.cat)}
    case types.FETCH_ASSETS_BY_HASH:
      return { ...state, hashAssets: action.assets,hashloading:false}

    case types.FETCH_ASSETS_BY_HASH_NOT_FOUND:
      return { ...state, hashMsg: action.msg,hashloading:false}

    case types.NO_ASSETS:
      return{...state,loading:false}
    case types.ARCHIVE_ASSET_INIT:
        return {
            ...state,
            loadingasset: true
        };   
    case types.ARCHIVE_ASSET_SUCCESS:
        return {
            ...state,
            loadingasset: false,
            isArchiveAsset:true
        };  
    case types.ARCHIVE_ASSET_FAIL:
        return {
            ...state,
            loadingasset: false,
            isArchiveAsset:false
        };  
    case types.ARCHIVE_FOLDER_INIT:
        return {
            ...state,
            loadingasset: true
        };   
    case types.ARCHIVE_FOLDER_SUCCESS:
        return {
            ...state,
            loadingasset: false,
            isArchiveFolder:true
        };  
    case types.ARCHIVE_FOLDER_FAIL:
        return {
            ...state,
            loadingasset: false,
            isArchiveFolder:false
        }; 
    case types.FETCH_ARCHIVE_FILE_SUCCESS:
        return {
            ...state,
            filesArchive: action.archiveAsset
        };
    case types.FETCH_ARCHIVE_FILE_FAIL:
        return {
            ...state,
            filesArchive: []
        }; 
    case types.FETCH_ARCHIVE_FOLDER_SUCCESS:
        return {
            ...state,
            folderArchive: action.archiveFolder
        };
    case types.FETCH_ARCHIVE_FOLDER_FAIL:
        return {
            ...state,
            folderArchive: []
        }; 
    case types.FETCH_ARCHIVE_CATEGORY_SUCCESS:
        return {
            ...state,
            archiveCat: action.archiveCategory
        };      
    case types.FETCH_ARCHIVE_CATEGORY_FAIL:
        return {
            ...state,
            archiveCat: []
        };    
    case types.UNARCHIVE_ASSET_INIT:
        return {
            ...state,
            loadingasset: true
        };   
    case types.UNARCHIVE_ASSET_SUCCESS:
        return {
            ...state,
            loadingasset: false,
            isUnarchiveAsset:true
        };  
    case types.UNARCHIVE_ASSET_FAIL:
        return {
            ...state,
            loadingasset: false,
            isUnarchiveAsset:false
        };
    case types.UNARCHIVE_FOLDER_INIT:
        return {
            ...state,
            loadingasset: true
        };   
    case types.UNARCHIVE_FOLDER_SUCCESS:
        return {
            ...state,
            loadingasset: false,
            isUnarchiveFolder:true
        };  
    case types.UNARCHIVE_FOLDER_FAIL:
        return {
            ...state,
            loadingasset: false,
            isUnarchiveFolder:false
        };  
     case types.MOVE_ASSET_FOLDER_INIT:
        return {
            ...state,
            loadingasset: true
        };     
     case types.MOVE_ASSET_FOLDER_SUCCESS:
        return {
            ...state,
            loadingasset: false
        };  
    case types.MOVE_ASSET_FOLDER_FAIL:
        return {
            ...state,
            loadingasset: false
        };  
    case types.ASSET_LINK_CLICK:
        return {
            ...state,
            assetLoading: true
        };   
    case types.FETCH_BREADCRUM_SUCCESS:
        return {
            ...state,
            breadCrum: action.breadCrumData
        };  
    case types.RESET_ARCHIVE_UNARCHIVE_FLAG:
        return {
            ...state,
            isArchiveFolder:false,
            isArchiveAsset:false,
            isUnarchiveFolder:false,
            isUnarchiveAsset:false
        }; 
    case types.UPLOAD_ASSET_IN_BUFFER:
      return { ...state, uploadedAssetBuffer: action.item}
    case types.RESET_UPLOAD_ASSET_IN_BUFFER:
      return { ...state, uploadedAssetBuffer: [], addAssetInDBInit: true} 
    case types.RESET_CREATE_ASSET:
      if(action.cancelUpload)
          return { ...state, uploadedAssetBuffer: [], bufferUpdated: [],openEditor:false,addAssetInDBInit: false, creation: { loading: false, created: false }, files: [],isAssetUploadingStart:false, resetUploadAsset: true}
      if(action.resetAssetList){
          return { ...state, uploadedAssetBuffer: [], bufferUpdated: [],openEditor:false,addAssetInDBInit: false, creation: { loading: false, created: false },isAssetUploadingStart:false, resetUploadAsset:false}
      }else{
          return { ...state, uploadedAssetBuffer: [], bufferUpdated: [],openEditor:false,addAssetInDBInit: false, creation: { loading: false, created: false },isAssetUploadingStart:false, resetUploadAsset: false}
      }
      
    case types.Edit_ASSET_IN_BUFFER:
      var newBuffer = state.uploadedAssetBuffer
      var indexCount = action.indexCount
      newBuffer.files[indexCount] = action.item
      return { ...state, uploadedAssetBuffer: newBuffer, bufferUpdated: true, openEditor:false} 
    /*case types.STORE_ASSET_DATA_BEFORE_UPLOAD:
      return { ...state, assetDataBeforeUpload: [...state.assetDataBeforeUpload, action.item]}    */
    case types.OPEN_EDITOR:
      return { ...state, openEditor:true}                          
    case types.HIDE_EDITOR:
      return { ...state, openEditor:false}    
    case types.RESET_BREADCRUM:
        return {
            ...state,
            breadCrum: []
        };            
    case types.FETCH_CAT_CUSTOM_FEED_SUCCESS:
        return{...state,customfeedAsset:action.customFeed}             
    default:
      return state;

  }
}

export default assetsReducer
