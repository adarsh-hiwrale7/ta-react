import auth from './authReducer';
import assets from './assetsReducer';
import aws from './awsReducer';
import allcountry from './countryReducer';
import analytics from './analyticsReducer';
import cultureAnalytics from './cultureAnalyticsReducer';
import NPSAnalytics from './NPSAnalyticsReducer';

import billing from './billingReducer';

import {combineReducers} from 'redux';
import campaigns from './campaignsReducer';
import channels from './channelReducer';

import departments from './departmentsReducer';
import dashboard from './dashboardReducer';

import feed from './feedReducer';
import feedCalendar from './feedCalendarReducer';
import folder from './folderReducer';

import general from './generalReducer';

import header from './headerReducer';

import intergration from './integrationReducer';

import jobs from './jobsReducer';

import smsValue from './smsReducer'

import message from './messageReducer';
import moderation from './moderationReducer';

import notification_data from './notificationReducer';

import posts from './postsReducer';
import profile from './profileReducer';
import points from './pointReducer';

import {reducer as form} from 'redux-form';

import settings from './settingsReducer';
import socialAccounts from './socialAccountsReducer';

import searchData from './searchReducer'

import tweet from './twitterReducer';
import tags from './tagsReducer';

import users from './usersReducer';
import usersList from './usersListReducer';

import widgets from './widgetsReducer';
import culture  from './cultureReducer';
import companyData from './companyReducer';
import polls from './pollReducer';

// The combineReducers helper function turns an object whose values are different reducing functions into a single reducing function you can pass to createStore.
const appReducer = combineReducers({
	auth,
	 
	allcountry,
	analytics,
	cultureAnalytics,
	NPSAnalytics,
	assets,
	aws,

	billing,

	campaigns,
	channels,

	dashboard,
	departments,

	form, // <---- reduxForm Mounted at 'form'
	feed,
	feedCalendar,
	folder,

	general,

	header,

	intergration,

	jobs,
	smsValue,
	
	message,
	moderation,
	
	notification_data,

	profile,
	posts,
	polls,
	points,

	settings,
	socialAccounts,

 	tweet,
 	tags,
 	
	 users,
	 usersList,
	 searchData,
	widgets,
	culture,
	companyData
});

const rootReducer = (state, action) => {
  if (action.type === 'AUTH_LOGOUT') {
    state = undefined
  }

  return appReducer(state, action)
}

export default rootReducer;

