
import * as types from '../actions/actionTypes'

const initialState = {
    dashboardAnalytics: {},
    dashboardAnalyticsInitial: {},
    dashboardCompanyExternalPost: {},
    dashboardExternalPost: {},
    dashboardInternalPost: {},
    dashboardCompanyInternalPost: {},
    dashboardMyRecentAssets: {},
    dashboardMapPosts: {},
    storagePrice: {},
    storagePopoupLoading:false,
    fetchStorageSuccess: false,
    internalPostLoader:false,
    companyInternalPostLoader:false,
    recentAssetLoader:false,
    companyExternalPostLoader:false,
    externalPostLoader:false,
    mapPostsLoader:false,
    chartLoader:false
};

let dashboardReducer = function(state = initialState, action) {
  switch (action.type) {
  
    case types.FETCH_TOP_EXTERNAL_POST:
      return { ...state, dashboardExternalPost: action.dashboardExternalPost ,externalPostLoader:false};
    case types.FETCH_TOP_EXTERNAL_POST_ERROR:
      return { ...state, externalPostLoader:false,dashboardExternalPost:[]};
    case types.FETCH_TOP_COMPANY_EXTERNAL_POST:
      return { ...state, dashboardCompanyExternalPost: action.dashboardCompanyExternalPost,companyExternalPostLoader:false };
    case types.FETCH_TOP_INTERNAL_POST:
      return { ...state, dashboardInternalPost: action.dashboardInternalPost,internalPostLoader:false };
    case types.FETCH_TOP_INTERNAL_POST_ERROR:
      return { ...state, internalPostLoader:false ,dashboardInternalPost:[]};
    case types.FETCH_TOP_COMPANY_INTERNAL_POST:
      return { ...state, dashboardCompanyInternalPost: action.dashboardCompanyInternalPost,companyInternalPostLoader:false };
    case types.FETCH_MY_RECENT_ASSETS:
      return { ...state, dashboardMyRecentAssets: action.dashboardMyRecentAssets,recentAssetLoader:false };
    case types.FETCH_MAP_POSTS:
      return { ...state, dashboardMapPosts: action.mapPosts,mapPostsLoader:false };
    case types.FETCH_DASHBOARD_ANALYTICS:
      return { ...state,  dashboardAnalyticsInitial: action.dashboardAnalytics, dashboardAnalytics: action.dashboardAnalytics,chartLoader:false }
    case types.MODIFY_DASHBOARD_ANALYTICS:
      return { ...state,  dashboardAnalytics: action.dashboardAnalytics }  
    case types.FETCH_STORAGE_PRICE_INIT:
      return { ...state,  storagePopoupLoading: true, fetchStorageSuccess:false}
    case types.FETCH_STORAGE_PRICE_FAIL:
      return { ...state,  storagePopoupLoading: false, fetchStorageSuccess:true}
    case types.FETCH_STORAGE_PRICE:
      return { ...state,  storagePrice: action.planData.plan.data, storagePopoupLoading: false}
    case types.ADD_STORAGE_INIT:
      return { ...state,  storagePopoupLoading: true} 
    case types.ADD_STORAGE_SUCCESS:
      return { ...state,  storagePopoupLoading: false, fetchStorageSuccess: true} 
    case types.ADD_STORAGE_ERROR:
      return { ...state,  storagePopoupLoading: false}       
      
    case types.FETCH_TOP_INTERNAL_POST_INIT:
      return {...state,internalPostLoader:true}
    case types.FETCH_TOP_COMPANY_INTERNAL_POST_INIT:
      return {...state,companyInternalPostLoader:true}
    case types.FETCH_MY_RECENT_ASSETS_INIT:
      return {...state,recentAssetLoader:true}
  
    case types.FETCH_TOP_COMPANY_EXTERNAL_POST_INIT:
      return {...state,companyExternalPostLoader:true}
      case types.FETCH_TOP_EXTERNAL_POST_INIT:
      return {...state,externalPostLoader:true}
    case types.FETCH_MAP_POSTS_INIT:
      return {...state,mapPostsLoader:true}
    case types.FETCH_DASHBOARD_ANALYTICS_INIT:
      return{...state,chartLoader:true}
    case types.FETCH_MAP_POSTS_ERROR:
      return {...state,mapPostsLoader:false}
    case types.FETCH_TOP_COMPANY_INTERNAL_POST_ERROR:
      return {...state,companyInternalPostLoader:false,dashboardCompanyInternalPost:[]}
    case types.FETCH_MY_RECENT_ASSETS_ERROR:
      return {...state,recentAssetLoader:false,dashboardMyRecentAssets:[]}

    case types.FETCH_TOP_COMPANY_EXTERNAL_POST_ERROR:
      return {...state,companyExternalPostLoader:false,dashboardCompanyExternalPost:[]}
    case types.FETCH_DASHBOARD_ANALYTICS_ERROR:
      return{...state,chartLoader:false}
    default:
      return state;
  }
}

export default dashboardReducer