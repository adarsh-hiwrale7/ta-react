import * as types from '../actions/actionTypes'

const initialState = {
  moderation: {
    sidebarListing: [],
    feedback_messageList:[],
    closeAllPopups:false,
    loading:false,
    newdata:false,
    unapprovedflag : false
  },
};

let moderationReducer = function(state = initialState, action) {
  switch (action.type) {
    case types.MODERATION_SIDEBAR_ACTION:
      return { ...state, moderation: {sidebarListing:  action.listingType} };

    case types.FEEDBACK_LIST:
      return { ...state, moderation: {feedback_messageList:  action.data},closeAllPopups: false };
      
    case types.CALL_FETCH_API_POST_ASSETS:
      return { ...state,newdata:false,};
      
    case types.ASSET_FETCH_SUCCESS:
      return { ...state, closeAllPopups: false,loading:false,};

    case types.SEND_FEEDBACK_SUCCESS: 
        return { ...state, loading: false, closeAllPopups: true,newdata:true,unapprovedflag:true }

      case types.SEND_FEEDBACK_INIT:{
        return{...state,loading:true}
      }
      case types.APPROVE_ASSET_DATA:{
        return{...state,loading:true}
      }

      case types.APPROVE_POST_DATA:{
        return{...state,loading:true}
      }

      case types.APPROVE_ASSET_SUCCESS:{
        return{...state,loading:false}
      }
      case types.MODERATION_UNAPPROVEDFLAG:{
         return{...state,unapprovedflag : false}
      }

    default:
      return state;
  }
}

export default moderationReducer
