import * as types from '../actions/actionTypes'

const initialState = {
    internalAnalytics:{},
    assetAnalytics:{},
    assetLineChart:{},
    feedAnalytics:{},
    loading:false,
    chartLoading:false,
    assetAnalytics:{},
    campaignAnalytics:{},
    feedChartAnalytics:{},
    assetChartAnalytics:{},
    campaignChartAnalytics:{},
    savedFilterAnalyticsId:'',
    savedFilterAnalyticsData:[],
    isFilterSaved:false,
    isFilterUnsave:false,
    campaingPopupData : null,
    //new analytics
    adoptionLoader:false,
    adoptionData:{},
    adoptionAnalytics:{},
    modifyAdoptionData:{},
    qualityAnalytics:{},
    qualityLoader:false,
    qualityData:{},
    assetsAnalytics:{},
    assetLoader:false,
    assetData:{},
    modifyAssetData:{},
    internalLoader:false,
    internalData:{},
    internalEngaementAnalytics:{},
    tagCloudData:[],
    tagCloudLoader:false,
    tagCloudAnalytics:{},
    totalPostAnalytics:{},
    totalPostLoader:false,
    totalPostData:[],
    modifyTotalPostData:{},
    locationBaseAdoption:{},
    locationBaseLoader:false,
    locationBaseData:{},
    guestUserChart:{},
    guestUserChartLoader:false,
    guestUserChartData:{},
    engagementByChannel:{},
    engagementByChannelLoader:false,
    engagementByChannelData:[],
    engagementOverTime:{},
    engagementOverTimeLoader:false,
    engagementOverTimeData:[],
    totalSocialAnalytics:{},
    totalSocialEngagementLoader:false,
    totalSocialEngagementData:[],
    postByNetworkLoader: false,
    postByNetworkData: [],
    analyticsPostTableLoader: false,
    guestUserActivity:{},
    guestUserActivityLoader:false,
    guestUserActivityData:{},
    selectedFilterValues:{},
    adoptionFilter:{Department:[],Country:[],Region:[],'User role':[],Age:[],Gender:[],'Office location':[],selectedFilterDetail:{}},
    totalpostFilter:{Department:[],'User role':[],Age:[],Gender:[],'Office location':[],selectedFilterDetail:{}},
    totalSocialFilter:{Department:[],'User role':[],Age:[],Gender:[],'Office location':[],selectedFilterDetail:{}},
    assetsFilter:{Department:[],'User role':[],Age:[],Gender:[],'Office location':[],selectedFilterDetail:{}},
    qualityFilter:{Department:[],'User role':[],Age:[],Gender:[],'Office location':[],selectedFilterDetail:{}},
    tagcloudFilter:{Department:[],'User role':[],Age:[],Gender:[],'Office location':[],selectedFilterDetail:{}},
    byChannelFilter:{Department:[],'User role':[],Age:[],Gender:[],'Office location':[],selectedFilterDetail:{}},
    overTimeFilter:{Department:[],'User role':[],Age:[],Gender:[],'Office location':[],selectedFilterDetail:{}},
    networkFilter:{Department:[],'User role':[],Age:[],Gender:[],'Office location':[],selectedFilterDetail:{}},
    participationFilter:{Department:[],'User role':[],Age:[],Gender:[],'Office location':[],selectedFilterDetail:{}},
    votedFilter:{Department:[],'User role':[],Age:[],Gender:[],'Office location':[],selectedFilterDetail:{}},
    taggedFilter:{Department:[],'User role':[],Age:[],Gender:[],'Office location':[],selectedFilterDetail:{}},
    contentVengageDetail:{ loading : false,contentVengageData:[]},
    contentGraphDetail:{ loading : false,contentGraphDetailData:[]},
    printEmailAnalyticsCount:{ loading : false,printEmailAnalyticsCount:{}},
};

let analyticsReducer = function(state = initialState, action) {
  switch (action.type) {
        
    case types.UNSAVE_FILTER_ANALYTICS_INIT:
        return{...state,loading:true}

    case types.FETCH_SAVED_ANALYTICS_FILTER_DATA_INIT:
        return{...state,loading:true}

    case types.SAVE_ANALYTICS_FILTER_DATA_INIT:
        return {...state,loading:true}

    case types.EXPORT_ANALYTICS_INIT:
        return{...state,loading:true}

    case types.FETCH_FEED_ANALYTICS_INIT:
      return { ...state, loading:true, analyticsPostTableLoader: true };

    case types.FETCH_ASSET_ANALYTICS_INIT:
      return { ...state, loading:true };

    case types.FETCH_CAMPAIGN_ANALYTICS_INIT:
      return { ...state, loading:true };

    case types.FETCH_FEED_CHART_ANALYTICS_INIT:
      return { ...state, chartLoading:true };

    case types.FETCH_ASSET_CHART_ANALYTICS_INIT:
      return { ...state, chartLoading:true };

    case types.FETCH_CAMPAIGN_CHART_ANALYTICS_INIT:
      return { ...state, chartLoading:true };

    case types.FETCH_CAMPAIGNS_ANALYTICS:
        return { ...state, campaignsAnalytics: action.campaign };

    case types.FETCH_INTERNAL_ANALYTICS:
        return { ...state, internalAnalytics: action.internal_analytics };

    case types.SUCCESS_FETCH_FEED_ANALYTICS:
        return{...state,feedAnalytics:action.data,loading:false, analyticsPostTableLoader: false}

    case types.ERROR_FETCH_ASSET_ANALYTICS:
        return{...state,loading:false,assetAnalytics:{}}

    case types.SUCCESS_FETCH_ASSET_ANALYTICS:
        return{...state,assetAnalytics:action.data,loading:false}

    case types.SUCCESS_FETCH_CAMPAIGN_ANALYTICS:
        return{...state,campaignAnalytics:action.data,loading:false}

    case types.ERROR_FETCH_FEED_ANALYTICS:
        return{...state,loading:false,feedAnalytics:[], analyticsPostTableLoader: false}

    case types.ERROR_FETCH_CAMPAIGN_ANALYTICS:
        return{...state,loading:false,campaignAnalytics:[]}

    case types.SUCCESS_FETCH_FEED_CHART_ANALYTICS:
        return{...state,chartLoading:false,feedChartAnalytics:action.data}

    case types.ERROR_FETCH_FEED_CHART_ANALYTICS:
        return{...state,chartLoading:false,feedChartAnalytics:{}}

    case types.SUCCESS_FETCH_ASSET_CHART_ANALYTICS:
        return{...state,chartLoading:false,assetChartAnalytics:action.data}

    case types.ERROR_FETCH_ASSET_CHART_ANALYTICS:
        return{...state,chartLoading:false,assetChartAnalytics:{}}

    case types.SUCCESS_FETCH_CAMPAIGN_CHART_ANALYTICS:
        return{...state,chartLoading:false,campaignChartAnalytics:action.data}

    case types.ERROR_FETCH_CAMPAIGN_CHART_ANALYTICS:
        return{...state,chartLoading:false,campaignChartAnalytics:{}}

    case types.ERROR_EXPORT_ANALYTICS:
        return{...state,loading:false}

    case types.SUCCESS_EXPORT_ANALYTICS:
        return{...state,loading:false}

    case types.ERROR_SAVE_ANALYTICS_FILTER_DATA:
        return{...state,loading:false,savedFilterAnalyticsId:'',isFilterSaved:false}

    case types.SUCCESS_SAVE_ANALYTICS_FILTER_DATA:
        return{...state,loading:false,savedFilterAnalyticsId:action.data ,isFilterSaved:true}

    case types.SUCCESS_FETCH_SAVED_FILTER_ANALYTICS:
        return {...state,loading:false,savedFilterAnalyticsData:action.data}

    case types.ERROR_FETCH_SAVED_FILTER_ANALYTICS:
        return{...state,loading:false,savedFilterAnalyticsData:['no data']}

    case types.CHANGE_FLAG_OF_SAVED_FILTER_ANALYTICS:
        return{...state,isFilterSaved:false}

    case types.ERROR_UNSAVE_FILTER_ANALYTICS:
        return{...state,loading:false,isFilterUnsave:false}

    case types.SUCCESS_UNSAVE_FILTER_ANALYTICS:
        return{...state,loading:false,savedFilterAnalyticsData:['no data'],isFilterUnsave:true}

    case types.REMOVE_UNSAVE_FLAG:
        return{...state,isFilterUnsave:false}

    case types.CAMPAIGN_ANALYTICS_POPUP_INIT:
    return{...state,loading:true}

    case types.CAMPAIGN_ANALYTICS_POPUP_SUCCESS:{
        return{...state,loading:false ,campaingPopupData : action.campaignPopupData}
    }
    //new analytics
    case types.FETCH_ADOPTION_DATA_INIT:
        return{...state,adoptionAnalytics:{adoptionLoader:true}}
    case types.FETCH_ADOPTION_DATA_SUCCESS:
        return{...state,adoptionAnalytics:{adoptionLoader:false,adoptionData:action.data}}
    case types.FETCH_ADOPTION_DATA_ERROR:
        return{...state,adoptionAnalytics:{adoptionLoader:false}}
    case types.MODIFY_ADOPTION_DATA_SUCCESS:
        return{...state,modifyAdoptionData:action.data}

    case types.FETCH_ASSET_ANALYTICS_DATA_INIT:
        return{...state, assetsAnalytics :{assetLoader:true}}
    case types.FETCH_ASSET_ANALYTICS_DATA_SUCCESS:
        return{...state, assetsAnalytics :{assetLoader:false, assetData:action.data}}
    case types.FETCH_ASSET_ANALYTICS_DATA_ERROR:
        return{...state, assetsAnalytics :{assetLoader:false}}

    case types.FETCH_QUALITY_ANALYTICS_DATA_INIT:
        return{...state, qualityAnalytics :{qualityLoader:true}}
    case types.FETCH_QUALITY_ANALYTICS_DATA_SUCCESS:
        return{...state, qualityAnalytics :{qualityLoader:false,qualityData:action.data}}
    case types.FETCH_QUALITY_ANALYTICS_DATA_ERROR:
        return{...state,qualityAnalytics:{qualityLoader:false}}
        
    case types.FETCH_INTERNAL_ENGAGEMENT_DATA_INIT:
        return{...state , internalEngaementAnalytics:{internalLoader:true}}
    case types.FETCH_INTERNAL_ENGAGEMENT_DATA_SUCCESS:
        return{...state, internalEngaementAnalytics:{internalLoader:false,internalData:action.data}}
    case types.FETCH_INTERNAL_ENGAGEMENT_DATA_ERROR:
        return{...state, internalEngaementAnalytics:{internalLoader:false}}

    case types.FETCH_TAG_CLOUD_DATA_INIT:
        return{...state , tagCloudAnalytics:{tagCloudLoader:true}}
    case types.FETCH_TAG_CLOUD_DATA_SUCCESS:
        return{...state, tagCloudAnalytics:{tagCloudLoader:false,tagCloudData:action.data}}
    case types.FETCH_TAG_CLOUD_DATA_ERROR:
        return{...state, tagCloudAnalytics:{tagCloudLoader:false}}

    case types.FETCH_TOTAL_POSTS_CHARTS_DATA_INIT:
        return{...state , totalPostAnalytics:{totalPostLoader:true}}
    case types.FETCH_TOTAL_POSTS_CHARTS_DATA_SUCCESS:
        return{...state, totalPostAnalytics:{totalPostLoader:false,totalPostData:action.data}}
    case types.FETCH_TOTAL_POSTS_CHARTS_DATA_ERROR:
        return{...state, totalPostAnalytics:{totalPostLoader:false}}

    case types.MODIFY_TOTAL_POSTS_CHARTS_DATA_SUCCESS:
        return{...state,modifyTotalPostData:action.data}

    case types.FETCH_LOCATION_BASE_ADOPTION_INIT:
        return{...state , locationBaseAdoption:{locationBaseLoader:true}}
    case types.FETCH_LOCATION_BASE_ADOPTION_SUCCESS:
        return{...state, locationBaseAdoption:{locationBaseLoader:false,locationBaseData:action.data}}
    case types.FETCH_LOCATION_BASE_ADOPTION_ERROR:
        return{...state, locationBaseAdoption:{locationBaseLoader:false}}

    case types.FETCH_GUEST_USER_CHART_INIT:
        return{...state , guestUserChart:{guestUserChartLoader:true}}
    case types.FETCH_GUEST_USER_CHART_SUCCESS:
        return{...state, guestUserChart:{guestUserChartLoader:false,guestUserChartData:action.data}}
    case types.FETCH_GUEST_USER_CHART_ERROR:
        return{...state, guestUserChart:{guestUserChartLoader:false}}

    case types.FETCH_ENGAGEMENT_BYCHANNEL_INIT:
        return{...state , engagementByChannel:{engagementByChannelLoader:true}}
    case types.FETCH_ENGAGEMENT_BYCHANNEL_SUCCESS:
        return{...state, engagementByChannel:{engagementByChannelLoader:false,engagementByChannelData:action.data}}
    case types.FETCH_ENGAGEMENT_BYCHANNEL_ERROR:
        return{...state, engagementByChannel:{engagementByChannelLoader:false}}

    case types.FETCH_ENGAGEMENT_OVERTIME_INIT:
        return{...state , engagementOverTime:{engagementOverTimeLoader:true}}
    case types.FETCH_ENGAGEMENT_OVERTIME_SUCCESS:
        return{...state, engagementOverTime:{engagementOverTimeLoader:false,engagementOverTimeData:action.data}}
    case types.FETCH_ENGAGEMENT_OVERTIME_ERROR:
        return{...state, engagementOverTime:{engagementOverTimeLoader:false}}

    case types.FETCH_TOTAL_SOCIAL_ENGAGEMENT_INIT:
        return{...state , totalSocialAnalytics:{totalSocialEngagementLoader:true}}
    case types.FETCH_TOTAL_SOCIAL_ENGAGEMENT_SUCCESS:
        return{...state, totalSocialAnalytics:{totalSocialEngagementLoader:false,totalSocialEngagementData:action.data}}
    case types.FETCH_TOTAL_SOCIAL_ENGAGEMENT_ERROR:
        return{...state, totalSocialAnalytics:{totalSocialEngagementLoader:false}}
    case types.FETCH_POST_BY_NETWORK_INIT:
        return{...state, postByNetworkLoader: true}
    case types.FETCH_POST_BY_NETWORK_SUCCESS:
        return{...state, postByNetworkLoader: false, postByNetworkData: action.data}  
        
    case types.FETCH_GUEST_USER_ACTIVITY_INIT:
        return{...state , guestUserActivity:{guestUserActivityLoader:true}}
    case types.FETCH_GUEST_USER_ACTIVITY_SUCCESS:
        return{...state, guestUserActivity:{guestUserActivityLoader:false,guestUserActivityData:action.data}}
    case types.FETCH_GUEST_USER_ACTIVITY_ERROR:
        return{...state, guestUserActivity:{guestUserActivityLoader:false}}

    case types.SELECTED_FILTER_VALUES:
        if(state[`${[action.data.callFrom]}Filter`][action.data.selectedFrom].includes(action.data.value)){
            var index=state[`${[action.data.callFrom]}Filter`][action.data.selectedFrom].indexOf(action.data.value)
            state[`${[action.data.callFrom]}Filter`][action.data.selectedFrom].splice(index,1)
        }else{
            state[`${[action.data.callFrom]}Filter`][action.data.selectedFrom].push(action.data.value)
        }
        return{...state,...state[`${[action.data.callFrom]}Filter`]}
        
    case types.SELECTED_FILTER_DETAIL:
        state[`${[action.data.callFrom]}Filter`].selectedFilterDetail=action.data
        return{...state,...state[`${[action.data.callFrom]}Filter`]}

    case types.RESET_SELECTED_FILTER_DETAIL:
        if(state[`${[action.data]}Filter`].selectedFilterDetail!== undefined && Object.keys(state[`${[action.data]}Filter`].selectedFilterDetail).length>0){
            state[`${[action.data]}Filter`].selectedFilterDetail={}
        }
        return{...state,...state[`${[action.data]}Filter`]}
    case types.CLEAR_SELECTED_FILTER_DATA:
        if(state[`${[action.data.callFrom]}Filter`][action.data.filtername]!== undefined ){
            state[`${[action.data.callFrom]}Filter`][action.data.filtername]=[]
        }
        return{...state,...state[`${[action.data.callFrom]}Filter`]}

    case types.RESET_ALL_FILTER_VALUES:
        return{...state,adoptionFilter:{Department:[],Country:[],Region:[],'User role':[],Age:[],Gender:[],'Office location':[],selectedFilterDetail:{}},
        totalpostFilter:{Department:[],'User role':[],Age:[],Gender:[],'Office location':[],selectedFilterDetail:{}},
        totalSocialFilter:{Department:[],'User role':[],Age:[],Gender:[],'Office location':[],selectedFilterDetail:{}},
        assetsFilter:{Department:[],'User role':[],Age:[],Gender:[],'Office location':[],selectedFilterDetail:{}},
        qualityFilter:{Department:[],'User role':[],Age:[],Gender:[],'Office location':[],selectedFilterDetail:{}},
        tagcloudFilter:{Department:[],'User role':[],Age:[],Gender:[],'Office location':[],selectedFilterDetail:{}},
        byChannelFilter:{Department:[],'User role':[],Age:[],Gender:[],'Office location':[],selectedFilterDetail:{}},
        overTimeFilter:{Department:[],'User role':[],Age:[],Gender:[],'Office location':[],selectedFilterDetail:{}},
        networkFilter:{Department:[],'User role':[],Age:[],Gender:[],'Office location':[],selectedFilterDetail:{}},
        participationFilter:{Department:[],'User role':[],Age:[],Gender:[],'Office location':[],selectedFilterDetail:{}},
        votedFilter:{Department:[],'User role':[],Age:[],Gender:[],'Office location':[],selectedFilterDetail:{}},
        taggedFilter:{Department:[],'User role':[],Age:[],Gender:[],'Office location':[],selectedFilterDetail:{}}}

    case types.GET_CONTENT_V_ENGAGE_DATA_INIT:
        return{ ...state,contentVengageDetail:{ loading : true}}
    case types.GET_CONTENT_V_ENGAGE_DATA_SUCCESS:
        return{ ...state,contentVengageDetail:{ loading : false,contentVengageData:action.data}}
    case types.GET_CONTENT_V_ENGAGE_DATA_ERROR:
        return{ ...state,contentVengageDetail:{ loading : false}}

    case types.FETCH_CONTENT_GRAPH_DETAIL_INIT:
        return{ ...state,contentGraphDetail:{ loading : true}}
    case types.FETCH_CONTENT_GRAPH_DETAIL_SUCCESS:
        return{ ...state,contentGraphDetail:{ loading : false,contentGraphDetailData:action.data}}
    case types.FETCH_CONTENT_GRAPH_DETAIL_ERROR:
        return{ ...state,contentGraphDetail:{ loading : false}}

    case types.FETCH_SELECTED_PRINT_EMAIL_INIT:
        return{ ...state,printEmailAnalyticsCount:{ loading : true}}
    case types.FETCH_SELECTED_PRINT_EMAIL_SUCCESS:
        return{ ...state,printEmailAnalyticsCount:{ loading : false,printEmailAnalyticsCountData:action.data}}
    case types.FETCH_SELECTED_PRINT_EMAIL_ERROR:
        return{ ...state,printEmailAnalyticsCount:{ loading : false}}

   default:
      return state;
  }
}
export default analyticsReducer
