
import * as types from '../actions/actionTypes'

const initialState = {
  widgets: {
    googleMapInfo: [],
  },
  funnelWidget: {
    createFunnelLoading: false,
    funnelListLoading: false,
    funnelList: [],
    currFunnelData: {},
    currFunnelLoading: false,
    deleteFunnelLoading: false,
    generatedWidget: null,
    generateWidgetLoading: false,

  }
};

let widgetsReducer = function (state = initialState, action) {
  switch (action.type) {
    case types.MAP_WIDGET_INFO:
      return { ...state, widgets: { googleMapInfo: action.data } };
    case types.CREATE_FUNNEL_INIT:
      return { ...state, funnelWidget: { ...state.funnelWidget, createFunnelLoading: true } }
    case types.CREATE_FUNNEL_SUCCESS:
      return { ...state, funnelWidget: { ...state.funnelWidget, createFunnelLoading: false } }
    case types.CREATE_FUNNEL_ERROR:
      return { ...state, funnelWidget: { ...state.funnelWidget, createFunnelLoading: false } }
    case types.EDIT_FUNNEL_INIT:
      return { ...state, funnelWidget: { ...state.funnelWidget, createFunnelLoading: true } }
    case types.EDIT_FUNNEL_SUCCESS:
      return { ...state, funnelWidget: { ...state.funnelWidget, createFunnelLoading: false } }
    case types.EDIT_FUNNEL_ERROR:
      return { ...state, funnelWidget: { ...state.funnelWidget, createFunnelLoading: false } }
    case types.GENERATE_SURVEY_WIDGET_INIT:
      return { ...state, funnelWidget: { ...state.funnelWidget, createFunnelLoading: false }, generateWidgetLoading: true }
    case types.GENERATE_SURVEY_WIDGET_SUCCESS:
      return { ...state, generatedWidget: action.data, funnelWidget: { ...state.funnelWidget, createFunnelLoading: false }, generateWidgetLoading: false }
    case types.GENERATE_SURVEY_WIDGET_FAIL:
      return { ...state, funnelWidget: { ...state.funnelWidget, createFunnelLoading: false }, generateWidgetLoading: false }
    case types.FETCH_FUNNEL_INIT:
      return { ...state, funnelWidget: { ...state.funnelWidget, funnelListLoading: true, } }
    case types.FETCH_FUNNEL_SUCCESS:
      return { ...state, funnelWidget: { ...state.funnelWidget, funnelListLoading: false, funnelList: action.data } }
    case types.GET_FUNNEL_INIT:
      return { ...state, currFunnelData: {}, currFunnelLoading: true }
    case types.GET_FUNNEL_DATA_SUCCESS:
      return { ...state, currFunnelData: action.data, currFunnelLoading: false }
    case types.DELETE_FUNNEL_INIT:
      return { ...state, deleteFunnelLoading: true }
    case types.DELETE_FUNNEL_SUCCESS:
      return { ...state, deleteFunnelLoading: false }
    default:
      return state;
  }
}

export default widgetsReducer