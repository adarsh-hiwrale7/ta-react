import * as types from '../actions/actionTypes'

const initialState = {
  accounts: [],
  loading: false,
  pageCreated:false,
  popupclose:false,
  accountsFetched:false,
  socialLoader:false,
  pageList:[],
  pageLoader:false
};

let socialAccountsReducer = function(state = initialState, action) {
  
  switch (action.type) {
    case types.SOCIAL_ACCOUNTS_INIT:
        return{...state,accountsFetched:false,socialLoader:true}
    case types.FETCH_SOCIAL_SUCCESS:
      var socialAccounts=action.socialAccounts;
      let accounts = [];
      if(socialAccounts !== null) {
        for(let s=0; s < socialAccounts.length; s++) {
            if(socialAccounts[s].social_media.toLowerCase() == "twitter" && socialAccounts[s].active) {
              accounts.push({
                social_media: "twitter",
                active:socialAccounts[s].active,
                identity: socialAccounts[s].identity,
                iscompany:socialAccounts[s].is_company,
                user_social:socialAccounts[s].user_social.data
              })
            }
            if(socialAccounts[s].social_media.toLowerCase() == "linkedin" && socialAccounts[s].active) {
              accounts.push({
                social_media: "linkedin",
                active:socialAccounts[s].active,
                identity: socialAccounts[s].identity,
                iscompany:socialAccounts[s].is_company,
                user_social:socialAccounts[s].user_social.data
              })
            }
            if(socialAccounts[s].social_media.toLowerCase() == "facebook" && socialAccounts[s].active) {
              accounts.push({
                social_media: "facebook",
                active:socialAccounts[s].active,
                identity: socialAccounts[s].identity,
                iscompany:socialAccounts[s].is_company,
                user_social:socialAccounts[s].user_social.data
                
              })
            }
            if(socialAccounts[s].social_media.toLowerCase() == "slack" && socialAccounts[s].active) {
              accounts.push({
                social_media: "slack",
                active:socialAccounts[s].active,
                identity: socialAccounts[s].identity,
                iscompany:socialAccounts[s].is_company,
                site_identity:socialAccounts[s].site_identity,
                user_social:socialAccounts[s].user_social.data
              })
            }
          }
      }
        return { ...state,accounts: accounts,loading: false,accountsFetched:true,socialLoader:false};

    case types.FETCH_ACCOUNTS_START:
      return {...state,accounts: [],loading: false};

    case types.SOCIAL_ACCOUNTS_NOT_FOUND:
      return { ...state,accounts : [] ,loading : false,accountsFetched:false };

    case types.SEND_PAGE_INIT:
         return{...state,loading:true}

    case types.SEND_PAGE_SUCCESS:
         return{...state,loading:false,popupclose:true,pageCreated:true}
         
    case types.DISCONNECT_FACEBOOK_SUCCESS:
        return {...state,loading:false,popupclose:true}

    case types.SEND_PAGE_ERROR:
      return{...state,popupclose:true,loading:false,pageCreated:false}

    case types.SOCIAL_PAGE_INIT:
      return{...state,popupclose:false}

    case types.SOCIAL_PAGES_INIT:
      return{...state,pageLoader:true}

    case types.SOCIAL_PAGE_SUCCESS:
      return{...state,pageList:action.data,pageLoader:false}

    case types.SOCIAL_NO_PAGES:
      return{...state,pageLoader:false}
         
    default:
      return state;
  }
}

export default socialAccountsReducer
