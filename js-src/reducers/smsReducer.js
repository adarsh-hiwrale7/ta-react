import * as types from "../actions/actionTypes";

const initialState = {
  smsValue: {},
  smsLoading: false,
  smsValueFailed: false,
};

let smsReducer = function (state = initialState, action) {
  switch (action.type) {
    //get Sms/popup value
    case types.SMS_LOADER:
      return { ...state, smsLoading: true };

    case types.SMS_VALUE_SUCCESS:
      return { ...state, smsValue: action.response, smsLoading: false };

    case types.SMS_VALUE_FAILED:
      return { ...state, smsValue: {}, smsLoading: false };

    default:
      return state;
  }
};
//file added

export default smsReducer;
