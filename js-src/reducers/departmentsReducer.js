import * as types from '../actions/actionTypes'

const initialState = {
    loading: false,
    list: [],
    added_department:{},
    closeAllPopups: false,
    department:{}
};

let departmentsReducer = function(state = initialState, action) {
    switch (action.type) {
    case types.FETCH_DEPARTMENTS_SUCCESS:
        return {
            ...state,
            list: action.departments,
            loading: false,
            closeAllPopups: true
        };
    case types.FETCH_DEPARTMENT_BY_ID_SUCCESS:
        return {
            ...state,
            department: action.department,
        };
    case types.FETCH_DEPARTMENTS_START:
        return {
            ...state,
            list: [],
            loading:true,
            closeAllPopups: false
        };
    case types.ADD_DEPARTMENT_SAVING:
        return {
            ...state,
            loading: true,
            closeAllPopups: false
        }
    case types.ADD_DEPARTMENT_SUCCESS:
        return {
            ...state,
            loading: false,
            closeAllPopups: true
        }
    case types.APPEND_DEPARTMENTS:
        return {
            ...state,
            list: state.list.concat(action.added_department),
            loading: false,
            closeAllPopups: true
        }
    case types.DELETE_DEPARTMENT_INITILIZATION:
        return {
            ...state,
            loading: true,
          }
    case types.FETCH_NO_DEPARTMENT:
          return {
              ...state,
              loading:false,
            }
    default:
        return state;
    }
}

export default departmentsReducer
