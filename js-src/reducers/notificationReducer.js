import * as types from '../actions/actionTypes'

const initialState = {
    notification_data:[],
    noticount:0,
    postSelectedData:[],
    loading:false,
    loading_post:false,
    notification_error:false
};

let notificationReducer = function(state = initialState, action) {
  switch (action.type) {
    case types.FETCH_NOTIFICATION:
        return { ...state, notification_data: action.notificationData,loading:false };
    case types.FETCH_NOTIFICATION_COUNT:
        return { ...state, noticount: action.notificationCount };
    case types.FETCH_SELECTED_POST_DETAILS:
        return{...state,postSelectedData:action.postdata,loading_post:false,notification_error:false}
    case types.FETCH_NOTIFICATION_INIT:
        return{...state,loading:true}
    case types.NOTIFICATION_ERROR:
        return{...state,postSelectedData:action.data,notification_error:true}   
    case types.FETCH_NOTIFICATION_POST_INIT:
        return{...state,loading_post:true}
    // case types.FETCH_SELECTED_POST_DETAILS:
    //     return{...state,postSelectedData:action.postdata}
    default:
      return state;
  }
}
export default notificationReducer
