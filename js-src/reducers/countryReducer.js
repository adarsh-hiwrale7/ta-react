import * as types from '../actions/actionTypes'

const initialState = {
    allcountry:[],
    countrystates:[],
    statecities:[],
    regioncountry:[],
    loadCountry:false
};

let analyticsReducer = function(state = initialState, action) {
  switch (action.type) {
    case types.FETCH_ALL_COUNTRY_INIT:
        return { ...state, loadCountry: true};
    case types.FETCH_ALL_COUNTRY:
        return { ...state, allcountry: action.country, loadCountry: false };
    case types.FETCH_REGION_COUNTRY:
        return { ...state, regioncountry: action.country ,loadCountry: false};  
    case types.FETCH_REGION_COUNTRY_ERROR:
        return{...state , regioncountry:[],loadCountry: false};
    case types.FETCH_ALL_COUNTRY_ERROR: 
        return{...state,allcountry:[],loadCountry: false};  
    case types.FETCH_STATE:
    if(action.isCallFromSettings==true){
        return { ...state, countrystates: action.countrystate};
    }else{
        //this will call while api is calling from onboarding and to remove old selected city if country is changed
        return { ...state, countrystates: action.countrystate ,statecities:[]};
    }
    case types.FETCH_CITY: 
        return { ...state, statecities: action.statecity };
    default:
      return state;
  }
}
export default analyticsReducer
