import * as types from '../actions/actionTypes'

const initialState = {
    
    detail:'',
    loading: false,
    email:'',
    country:[],
    billState:[],
    city:[],
    stateLoading: false,
    cityLoading: false,
    contactDetail: '',
    contactDetailLoading: false,
    updateDetailLoading: false,
    paymentCard: [],
    fetchCardLoading: false,
    addCardHostedPageLink: '',
    planData: [],
    planUpdated: false,
    invoiceLoading: false,
    invoiceList: []
};

let billingReducer = function(state = initialState, action) {
    switch (action.type) {
    case types.CURRENT_BILL_DETAIL:
        return {
            ...state,
            detail: action.billDetail,
            loading:false
        };
    case types.CURRENT_BILL_DETAIL_INIT:
        return {
            ...state,
            loading:true
        };
    case types.FETCH_BILL_CONTACT_EMAIL:
        return {
            ...state,
            email: action.email,
        };   
    case types.BILL_CONTACT_EMAIL_SUCCESS:
      return { ...state};  
    case types.REMOVE_BILL_CONTACT_EMAIL_SUCCESS:
      return { ...state};  
    case types.FETCH_BILL_COUNTRY:
        return {
            ...state,
            country: action.countryDetail,
        }; 
    case types.FETCH_BILL_COUNTRY_ERROR:
        return {
            ...state,
            country: []
        };   
    case types.FETCH_BILL_STATE:
        return {
            ...state,
            billState: action.stateDetail,
            stateLoading:false
        };
    case types.FETCH_BILL_STATE_ERROR:
        return {
            ...state,
            billState: [],
            stateLoading:false
        };      
    case types.FETCH_BILL_STATE_INIT:
        return {
            ...state,
            billState: [],
            city:[],
            stateLoading:true
        };    
    case types.FETCH_BILL_CITY:
        return {
            ...state,
            city: action.cityDetail,
            cityLoading:false
        }; 
    case types.FETCH_BILL_CITY_ERROR:
        return {
            ...state,
            city: [],
            cityLoading:false
        };     
    case types.FETCH_BILL_CITY_INIT:
        return {
            ...state,
            cityLoading:true
        }; 
    case types.FETCH_CONTACT_DETAIL_INIT:
        return{
            ...state,
            contactDetailLoading:true
        }
    case types.FETCH_BILL_CONTACT_DETAIL:
        return {
            ...state,
            contactDetailLoading:false,
            contactDetail: action.contactDetail
        };
    case types.UPDATE_CONTACT_DETAIL_INIT:
        return {
            ...state,
            updateDetailLoading:true
        }; 
    case types.UPDATE_CONTACT_DETAIL:
        return {
            ...state,
            updateDetailLoading:false
        };
    case types.FETCH_BILL_CARD_INIT:
        return {
            ...state,
            fetchCardLoading: true
        };    
    case types.FETCH_BILL_CARD:
        return {
            ...state,
            paymentCard: action.cardDetail,
            fetchCardLoading: false
        };   
         
    case types.FETCH_BILL_CARD_ERROR:
        return {
            ...state,
            fetchCardLoading: false
        };     
    case types.FETCH_BILL_CARD_ERROR:
        return {
            ...state,
            fetchCardLoading: false
        };
    case types.ADD_BILL_CARD_SUCCESS:
        return {
            ...state,
            fetchCardLoading: false
        };
    case types.ADD_BILL_CARD_ERROR:
        return {
            ...state,
            fetchCardLoading: false
        };
    case types.REMOVE_BILL_CARD_INIT:
        return {
            ...state,
            fetchCardLoading: true
        };    
    case types.REMOVE_BILL_CARD:
        return {
            ...state,
            fetchCardLoading: false
        };
     case types.UPDATE_BILL_CARD_INIT:
        return {
            ...state,
            fetchCardLoading: true
        };    
    case types.UPDATE_BILL_CARD:
        return {
            ...state,
            fetchCardLoading: false
        };
     case types.ADD_CARD_HOSTED_PAGE_LINK:  
    return {
            ...state,
            addCardHostedPageLink: action.hostedLink
    }; 
    case types.REMOVE_CARD_HOSTED_PAGE_LINK:
    return {
            ...state,
            addCardHostedPageLink: ''
    };     
    case types.GET_ALL_PLAN:
    return {
            ...state,
            planData: action.planData
    }; 
    case types.UPDATE_PLAN_INIT:
    return {
            ...state,
            loading:true,
            planUpdated:false
    }; 
    case types.UPDATE_PLAN_SUCCESS:
    return {
            ...state,
            loading:false,
            planUpdated: true
    };
    case types.UPDATE_PLAN_ERROR:
    return {
            ...state,
            loading:false
    };
    case types.FETCH_INVOICE_INIT:
    return {
            ...state,
            invoiceLoading:true,
            invoiceList:[]
    };
    case types.FETCH_INVOICE_LIST:
    return {
            ...state,
            invoiceLoading:false,
            invoiceList: action.invoiceDetail
        };
    case types.FETCH_INVOICE_LIST_ERROR:
    return {
            ...state,
            invoiceLoading:false,
            invoiceList: []
    };
    default:
        return state;
    }
}

export default billingReducer
