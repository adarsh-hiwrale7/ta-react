import * as types from '../actions/actionTypes'

const initialState = {
    intdata:[],
    loading:false,
    data : [] ,      // data is include the workable key and sub domain
    slackLoader:false,
    slackClientData:{},
    slackChannelList:[],
    isSlackDisconnect:false,
    slackChannelData:{},
    ssoLoader:false,
    ssoDataList: {},
    ssoConnectModifed: false,
    ssoSaved :false,
    workableLoader:false
};

let integrationReducer = function(state = initialState, action) {
  switch (action.type) {
    case types.INTEGRATION_KEY_INTL:
        return { ...state,loading:true };
    case types.INTEGRATION_KEY_SUCCESS:
        return { ...state, intdata: action.data ,loading:false};
    case types.INTEGRATION_KEY_ERROR:
        return { ...state,loading:false};
    case types.WORKABLEKEY_INIT:
        return { ...state,loading:true};
    case types.WORKABLEKEY_SUCCESS:
        return { ...state, intdata: action.data ,loading:false};
    case types.WORKABLEKEY_FAIL:
        return { ...state,loading:false};
    case types.FETCH_WORKABLE_DETAILS_LINK:
        return { ...state, data:action.data,workableLoader:false
        };
    case types.FETCH_WORKABLE_DETAILS_INIT:
        return { ...state, workableLoader:true,
        };
        
    case types.FETCH_SLACK_CLIENT_INFO_INIT:
        return {...state,slackLoader:true}
    case types.FETCH_SLACK_CLIENT_INFO_ERROR:
        return{...state,slackLoader:false}
    case types.FETCH_SLACK_CLIENT_INFO_SUCCESS:
        return{...state,slackLoader:false,slackClientData:action.data}

    case types.CONNECT_SLACK_SUCCESS:
        return{...state,slackLoader:false}
    case types.CONNECT_SLACK_ERROR:
        return{...state,slackLoader:false}
    case types.CONNECT_SLACK_INIT:
        return{...state,slackLoader:true}

    case types.FETCH_SLACK_CHANNEL_LIST_INIT:
        return{ ...state,slackLoader:true}
    case types.FETCH_SLACK_CHANNEL_LIST_SUCCESS:
        return{ ...state,slackLoader:false,slackChannelList:action.data }
    case types.FETCH_SLACK_CHANNEL_LIST_ERROR:
        return{ ...state,slackLoader:false }

    case types.DISCONNECT_SLACK_SUCCESS:
        return{...state,slackLoader:false ,slackClientData:{} , slackChannelList:[],isSlackDisconnect:true,slackChannelData:{}}
    case types.DISCONNECT_SLACK_ERROR:
        return{...state,slackLoader:false,isSlackDisconnect:false}
    case types.DISCONNECT_SLACK_INIT:
        return{...state,slackLoader:true,isSlackDisconnect:false}

    case types.STORE_SLACK_CHANNEL_DETAIL:
        return{...state,slackLoader:false,slackChannelData:action.data}
    
    case types.CHANGE_SLACK_CHANNEL_INIT:
        return{...state,slackLoader:true}
    case types.CHANGE_SLACK_CHANNEL_SUCCESS:
        return{...state,slackLoader:false}
    case types.CHANGE_SLACK_CHANNEL_ERROR:
        return{...state,slackLoader:false}
    case types.GET_SSO_DATA_INIT:
        return{...state,ssoLoader:true}
    case types.GET_SSO_DATA_SUCCESS:
        return{...state,ssoLoader:false, ssoDataList: action.data}
    case types.GET_SSO_DATA_FAIL:
        return{...state,ssoLoader:false}      
    case types.SSO_CONNECT_INIT:
        return { ...state,loading:true,ssoSaved:false};
    case types.SSO_CONNECT_SUCCESS:
        return { ...state,loading:false,ssoSaved:action.data};
    case types.SSO_DISCONNECT_INIT:
        return { ...state,loading:true};
    case types.SSO_DISCONNECT_SUCCESS:
        return { ...state,loading:false, ssoConnectModifed: true};  
    case types.SSO_DISCONNECT_RESET:
        return { ...state,loading:false, ssoConnectModifed: false};                  

    default:
      return state;
  }
}
export default integrationReducer