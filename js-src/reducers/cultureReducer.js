import * as types from '../actions/actionTypes'

const initialState = {
    cultureLoader: false,
    cultureValueList:[],
    closeCreateValuePopup:false
};

let cultureReducer = function(state = initialState, action) {
    switch (action.type) {
        
    case types.RESET_POPUP_FLAG:
        return{...state,closeCreateValuePopup:false}
    case types.SAVE_VALUE_INIT:
        return {...state, cultureLoader: true};

    case types.SAVE_VALUE_SUCCESS:
        return {...state, cultureLoader: false,closeCreateValuePopup:true};

    case types.SAVE_VALUE_ERROR:
        return {...state, cultureLoader: false};

    case types.CULTURE_VALUE_LIST_INIT:
        return{...state, cultureLoader:true}

    case types.CULTURE_VALUE_LIST_SUCCESS: 
        return{...state, cultureLoader:false, cultureValueList:action.data}

    case types.CULTURE_VALUE_LIST_ERROR: 
        return{...state, cultureLoader:false}

    case types.UPDATE_VALUE_INIT:
        return{...state,cultureLoader:true}
    
    case types.UPDATE_VALUE_SUCCESS:
        return{...state,cultureLoader:false,closeCreateValuePopup:true}
    
    case types.UPDATE_VALUE_ERROR:
        return{...state,cultureLoader:false}

    case types.DELETE_VALUE_INIT:
        return{...state,cultureLoader:true}
    
    case types.DELETE_VALUE_SUCCESS:
        return{...state,cultureLoader:false}
    
    case types.DELETE_VALUE_ERROR:
        return{...state,cultureLoader:false}

    default:
        return state;
    }
}

export default cultureReducer
