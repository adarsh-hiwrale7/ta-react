import * as types from '../actions/actionTypes'

const initialState = {
  tags: [],
  loading: false,
  listing:[], 
  trendinglist:{loading : true,noData : false},
  userTags: [],
  companyTags:[],
  allUserTags: [],
  postTags:[],
  trendingTags: []
};

let tagsReducer = function(state = initialState, action) {
  switch (action.type) {
    case types.FETCH_TAGS_SUCCESS:
      return {
        ...state,
        tags: action.tags,
        listing: {
          loading: false
        }
      };
    case types.FETCH_POST_TAGS_SUCCESS:
      return {
        ...state,
        postTags: action.tags,
        listing: {
          loading: false
        }
      };
    case types.FETCH_USER_TAGS_SUCCESS:
      return {...state,userTags: action.tags,listing: {loading: false}};

    case types.FETCH_COMAPANY_TAGS_SUCCESS:
        return {...state,companyTags: action.tags};
    case types.FETCH_ALL_USER_TAGS_SUCCESS:
      return {...state,allUserTags: action.tags};  
    case types.FETCH_HASH_TAGS_SUCCESS:
      return {...state,hashTags: action.tags,listing: {loading: false}};

    case types.FETCH_TRENDING_TAGS_INIT:
      return {...state,trendinglist: {loading: true , noData: false}};  

    case types.FETCH_TRENDING_TAGS_SUCCESS:
      return {...state,trendingTags: action.trendingTags,trendinglist: {loading: false,noData:false}};

    case types.FETCH_TRENDING_TAGS_FAIL:
      return {...state,trendingTags: [],trendinglist: {loading: false,noData:true}};  
    
    case types.FETCH_TAGS_START:
      return {
        ...state,
        tags: [],
        listing: {
          loading: true
        }
      };
    default:
      return state;
  }
}

export default tagsReducer