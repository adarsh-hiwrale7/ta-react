import * as types from '../actions/actionTypes'

const initialState = {
    pollQuestionLoader: false,
    pollQuestions:{},
    saveSurveyLoading: false,
    surveySaveSuccess: false
};

let pollReducer = function(state = initialState, action) {
    switch (action.type) {
        
    case types.GET_POLL_SURVEY_QUESTION_INIT:
        return{...state,pollQuestionLoader:true}
    case types.GET_POLL_SURVEY_QUESTION_SUCCESS:
        return{...state,pollQuestionLoader:false, pollQuestions: action.data}    
    case types.GET_POLL_SURVEY_QUESTION_FAIL:  
        return{...state,pollQuestionLoader:false, pollQuestions:{}} 
    case types.SAVE_SURVEY_RESULT_INIT:
        return{...state,saveSurveyLoading:true,surveySaveSuccess:false}
    case types.SAVE_SURVEY_RESULT_SUCCESS:
        return{...state,saveSurveyLoading:false,surveySaveSuccess:true}    
    case types.SAVE_SURVEY_RESULT_FAIL:  
        return{...state,saveSurveyLoading:false}     


    default:
        return state;
    }
}

export default pollReducer
