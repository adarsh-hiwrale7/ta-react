import * as types from '../actions/actionTypes'

const initialState = {
  internal: { 
    loading: false,
    live: [],
    scheduled: [],
    nopost:false,
    loadmore:false,
    isPin: false
  },
  default: { 
    loading: false,
    live: [],
    scheduled: [],
    nopost:false,
    loadmore:false,
    isPin: false
  },
  external: {
    loading: false,
    live: [],
    scheduled: [],
    nopost:false,
    loadmore:false
  },
  custom : {
    loading: false,
    live: [],
    scheduled: [],
    nopost:false,
    loadmore:false,
    isPin: false,
    createLoading:false
  },
  mypost:{
    loading: false,
    live: [],
    scheduled: [],
    unapproved:[],
    pending:[],
    nopost:false,
    loadmore:false,
    isPin: false
  },
  feedList:{},
  is_configured:'',
  postGiphy:[],
  postsearchGiphy:[],
  loadinggiphy:false,
  meta: {},
  feedEvents:[],
  RssData:[],
  JobData:[],
  jobcount :0,
  curationcount:0,
  lastFeedType:'',
  isJobfinished:false,
  isResponseChanged:false,
  pinMessage:{
    message:null,
    postId:null
  },
  scheduleDates:[],
  feedCreated: false,
  feedDetail:[],
  finishFeedUpdate: false,
  scheduleLoading:false,
  JobsFromFeedLoading:false,
  RssFromFeedLoading:false,
  ratedPost: [],
  feedLoading:false,
  lastPostId:null,
  savedHashTagListInPost:{
    hashTagListInCreatePost:{},
    loading:false
  }
};

let feedReducer = function(state = initialState, action) {
  
  switch (action.type) {
// custom feed 
  case types.JOB_FROM_FEED_INIT:
    return{...state,JobsFromFeedLoading:true}

  case types.CURATION_FROM_FEED_INIT:
    return{...state,RssFromFeedLoading:true}
  // this is to set pagination for  static job disp[lay when is_configure is set to 0
  case types.STATIC_JOB_DISPLAY:
    return{...state,lastFeedType:'job'}

   case types.FETCH_CUSTOM_FEED_SUCCESS:
      return{...state, feedList: action.payload ,is_configured:action.is_configured};
      
   case types.FETCH_NOPOST_CUSTOM:
      return{...state,custom:{...state.custom,nopost:true}};

   case types.FETCH_CUSTOM_LIVE_SUCCESS:
    if(action.isPin || action.createPostNewdata || (action.isHash && action.onPostsScroll !== true)){
        //if post is pinned or new post has been created or hash is has been selected
        return { ...state, custom: { loading: false, live: action.payload, scheduled: [], isPin:true,loadmore:false}, meta: {},ratedPost:[] ,feedLoading:false}
      } 
    else 
      {
        return { ...state, custom: { loading: false, live: state.custom.live.concat(action.payload), scheduled: [], isPin:false,loadmore:false}, meta: {},ratedPost:[],feedLoading:false };
      }
    case types.LOAD_POSTDATA_CUSTOM:
      return { ...state, custom: {  ...state.custom, loadmore:true } };

    case types.INITIATE_FETCH_CUSTOM_LIVE:
      return { ...state, custom: {  ...state.custom, loading:true } ,feedLoading:true, lastPostId:null};

    case types.FETCH_MY_LIVE_SUCCESS:

      if(action.isPin || action.createPostNewdata || (action.isHash && action.onPostsScroll !== true) || action.fromFilter){
          //if post is pinned or new post has been created or hash is has been selected
          if(action.fromFilter==true && action.currPage!==1){
            return { ...state, mypost: { loading: false, live: state.mypost.live.concat(action.payload), scheduled: [], pending :[],unapproved:[],isPin:false,loadmore:false}, meta: {} ,feedLoading:false};
          }
          else{
            return { ...state, mypost: { loading: false, live: action.payload, scheduled: [],pending :[],unapproved:[], isPin:true,loadmore:false}, meta: {} ,feedLoading:false}
          } 
        } 
      else 
        {
          return { ...state, mypost: { loading: false, live: state.mypost.live.concat(action.payload), scheduled: [],pending :[],unapproved:[], isPin:false,loadmore:false}, meta: {} ,feedLoading:false};
        }
    
    case types.LOAD_POSTDATA_MYLIVE:
      return { ...state, mypost: {  ...state.mypost, loadmore:true } };
    
    case types.FETCH_NOPOST_MYLIVE:
      return{...state,mypost:{...state.mypost,nopost:true}};
    
    case types.INITIATE_FETCH_MY_LIVE:
      return { ...state, mypost: {  ...state.mypost, loading:true } ,feedLoading:true};

    case types.GET_ALL_SCHEDULES_INIT:
      return{...state,scheduleLoading:true}

    case types.FETCH_MY_SCHEDULED_SUCCESS:
      if(action.isPin || action.createPostNewdata || (action.isHash && action.onPostsScroll !== true)){
          //if post is pinned or new post has been created or hash is has been selected
          return { ...state, mypost: { loading: false, scheduled: action.payload, live: [],pending :[],unapproved:[], isPin:true,loadmore:false}, meta: {} ,feedLoading:false}
        } 
      else 
        {
          return { ...state, mypost: { loading: false, scheduled: state.mypost.scheduled.concat(action.payload), live: [],pending :[],unapproved:[], isPin:false,loadmore:false}, meta: {} ,feedLoading:false};
        }
    
    case types.LOAD_POSTDATA_MYSCHEDULED:
      return { ...state, mypost: {  ...state.mypost, loadmore:true } };
    
    case types.FETCH_NOPOST_MYSCHEDULED:
      return{...state,mypost:{...state.mypost,nopost:true},feedLoading:true};
    
    case types.INITIATE_FETCH_MY_SCHEDULED:
      return { ...state, mypost: {  ...state.mypost, loading:true } ,feedLoading:true};
  
    case types.FETCH_NOPOST_DEFAULT:
      return{...state,default:{...state.default,nopost:true}};

    case types.USER_SCROLL_LOADING:
      return { ...state, default: { ...state.default, loading:true } ,feedLoading:true};

    case types.INITIAL_LOADING_DATA:
      return { ...state, loading:true };

    case types.INITIAL_LOADING_RSS:
      return { ...state, loading:true };

    case types.LOAD_POSTDATA_DEFAULT:
      return { ...state, default: { ...state.default, loadmore:true } };
      
    case types.LOAD_POSTDATA_EXTERNAL:
      return { ...state, external: { ...state.external, loadmore:true } };

    case types.FETCH_NOPOST_EXTERNAL:
      return{...state,external:{...state.external,nopost:true}};

    case types.USER_SCROLL_LOADING_EXTERNAL:
      return { ...state, external: { ...state.external, loading:true } ,feedLoading:true};

    case types.INITIATE_FETCH_INTERNAL_LIVE:
      return { ...state, internal: { loading: true, live: [], scheduled: [], isPin: false} };

    case types.INITIATE_FETCH_INTERNAL_SCHEDULED:
      return { ...state, internal: { loading: true, live: [], scheduled: [], isPin:false} };

    case types.INITIATE_FETCH_EXTERNAL_LIVE:
      return { ...state, external: { loading: true, live: [], scheduled: [], isPin:false} };

    case types.INITIATE_FETCH_EXTERNAL_SCHEDULED:
      return { ...state, external: { loading: true, live: [], scheduled: [], isPin:false} };

    // case types.FETCH_INTERNAL_LIVE_SUCCESS:
    //   if(action.isPin || action.createPostNewdata || (action.isHash && action.onPostsScroll !== true)){
    //     //if post is pinned or new post has been created or hash is has been selected
    //     return { ...state, internal: { loading: false, live: action.payload, scheduled: [], isPin:true,loadmore:false}, meta: {} };  
    //   }else{
    //     return { ...state, internal: { loading: false, live: state.internal.live.concat(action.payload), scheduled: [], isPin:false,loadmore:false}, meta: {} };
    //   }

    case types.FETCH_DEFAULT_LIVE_SUCCESS:
      
      if(action.isPin || action.createPostNewdata || (action.isHash && action.onPostsScroll !== true)){
        //if post is pinned or new post has been created or hash is has been selected
        return { ...state, default: { loading: false, live: action.payload, scheduled: [], isPin:true,loadmore:false}, meta: {},ratedPost:[] ,feedLoading:false};  
      }else{
        return { ...state, default: { loading: false, live: state.default.live.concat(action.payload), scheduled: [], isPin:false,loadmore:false}, meta: {},ratedPost:[] ,feedLoading:false};
      }
      
    case types.FETCH_DEFAULT_LIVE_ERROR:
      // In error cases, no need to store in redux, just show a notification
      return { ...state, default: { loading: false, isPin:false } }

    case types.FETCH_INTERNAL_SCHEDULED_SUCCESS:
        if(action.createPostNewdata==true){
          //if new post has been created
          return { ...state, internal: { loading: false, live: [], scheduled: action.payload}, meta: {}};
        }else{
          if(state.internal.scheduled.length>0)
           return { ...state, internal: { loading: false, live: [], scheduled: state.internal.scheduled.concat(action.payload) }, meta: {}};
          else if(action.createPostNewdata==null)
           return { ...state, internal: { loading: false, live: [], scheduled: action.payload}, meta: {}};
        }
      
    case types.FETCH_INTERNAL_SCHEDULED_ERROR:
      return { ...state, internal: { loading: false,  } }

      case types.FETCH_INTERNAL_FEED_EVENTS:
      return { ...state, feedEvents: action.feedEvents };

    case types.FETCH_EXTERNAL_LIVE_SUCCESS:
      if(action.isPin || action.createPostNewdata || (action.isHash && action.onPostsScroll !== true)){
        //if post is pinned or create a new post or hash  has been selected
        return { ...state, external: { loading: false, live: action.payload, scheduled: [], isPin:true ,loadmore:false}, meta: {},ratedPost:[] };
      }else{
        return { ...state, external: { loading: false, live: state.external.live.concat(action.payload), scheduled: [], isPin:false ,loadmore:false}, meta: {},ratedPost:[] };
        }
      

    case types.FETCH_EXTERNAL_LIVE_ERROR:
      return { ...state, external: { loading: false,  } }

    case types.FETCH_EXTERNAL_SCHEDULED_SUCCESS:
      if(action.createPostNewdata==true)
      {
        //if new post has been created
        return { ...state, external: { loading: false, live: [], scheduled: action.payload, isPin:false}, meta: {} };
      }
      else{
        if (state.external.scheduled.length>0)
        {
          return { ...state, external: { loading: false, live: [], scheduled: state.external.scheduled.concat(action.payload), isPin:false}, meta: {} };
        }
        if(action.createPostNewdata==null)
        {
          return { ...state, external: { loading: false, live: [], scheduled:action.payload, isPin:false}, meta: {} };
        }
      }
      

    case types.FETCH_EXTERNAL_SCHEDULED_ERROR:
      return { ...state, external: { loading: false  }}

      case types.FETCH_EXTERNAL_FEED_EVENTS:
      return { ...state, feedEvents: action.feedEvents };

    case types.RESET_POSTS:
      return { ...state, default: { live: [], scheduled: [], isPin:false ,loading:true, loadmore : false} ,custom: { live: [], scheduled: [], meta: {}, isPin:false , loading : true,loadmore : false} ,mypost: { live: [], scheduled: [],  meta: {}, isPin:false ,loading : true,loadmore : false},JobData:[],RssData:[],  jobcount :0, curationcount :0,lastFeedType:''}

    case types.UPDATE_META_INFO:
      return { ...state, meta: action.payload }


    case types.FETCH_GIPHY_DATA:
      return {...state,postGiphy:action.data,loadinggiphy:false}

    case types.FETCH_SEARCH_GIPHY_INIT:
      return{...state,postGiphy:[],postsearchGiphy:[],loadinggiphy:true}

    case types.FETCH_SEARCH_GIPHY_DATA:
      return {...state,postGiphy:[],postsearchGiphy:action.searchdata,loadinggiphy:false}
    
    case types.LOADING_GIPHY:
      return {...state,loadinggiphy:true}

    case types.FETCH_SUCCESS_RSS_DATA:
    if(action.isCallFromRssPage==true){
      //to display only rss data on rss page
      return{...state,RssFromFeedLoading:false, loading: false , RssData:action.data}
    }else{
      //to display merged rss data if scrolling is done in feed page
      return{...state, RssFromFeedLoading:false,loading: false , RssData:state.RssData.concat(action.data.data), curationcount :state.curationcount+1, lastFeedType:action.data.feed_type}
    }
      

    case types.FETCH_ERROR_RSS_DATA:
      return{...state, loading:false,RssFromFeedLoading:false}

    case types.FETCH_ERROR_JOB_DATA:
      return{...state, loading:false,JobsFromFeedLoading:false}

    case types.FETCH_SUCCESS_JOB_DATA:
    if(action.isCallFromJobsPage==true){
      //to display only job data in job page
      return{...state,JobsFromFeedLoading:false,JobData:action.data, loading:false}
    }else{
      if(action.data.feed_type=="curation"){
        return{...state,JobsFromFeedLoading:false,RssData:state.RssData.concat(action.data.data), loading:false,lastFeedType:action.data.feed_type,isJobfinished:true, curationcount:state.curationcount+1, isResponseChanged:true}
      }else if(action.data.feed_type=="job"){
        return{...state,JobsFromFeedLoading:false,JobData:state.JobData.concat(action.data.data), loading:false,lastFeedType:action.data.feed_type, jobcount:state.jobcount+1, isJobfinished:false,isResponseChanged:false}
      }
    }

    case types.UPDATE_PIN_MESSAGE:  
      return{...state,pinMessage: { message: action.message, postId: action.postId }}

    case types.FETCH_SCHEDULE_DATA:
        return{...state,scheduleDates:action.scheduleData,scheduleLoading:false}

    case types.FETCH_SCHEDULE_DATA_FAIL:
        return{...state,scheduleDates:[],scheduleLoading:false}    
    case types.CREATE_FEED_INIT:
        return{...state,custom: {  ...state.custom, createLoading: true }, feedCreated: false}
    case types.CREATE_FEED_SUCCESS:
        return{...state,custom: { ...state.custom, createLoading: false }, feedCreated: true}
    case types.CREATE_FEED_FAIL:
        return{...state,custom: { ...state.custom, createLoading: false }}       
    case types.OPEN_FEED_POPUP:
        return{...state,feedCreated:false}   
    case types.REMOVE_USER_FROM_FEED_INIT:
        return{...state,custom: { ...state.custom, createLoading: true }} 
    case types.REMOVE_USER_FROM_FEED_FINISH:
        return{...state,custom: { ...state.custom, createLoading: false }}     
    case types.GET_CUSTOM_FEED_DETAIL_INIT:
        return{...state,custom: { ...state.custom, createLoading: true }, feedDetail: []}
    case types.GET_CUSTOM_FEED_DETAIL:
        return{...state,feedDetail:action.feedDetail, custom: { ...state.custom, createLoading: false }}  
    case types.GET_CUSTOM_FEED_DETAIL_FAIL:
        return{...state, custom: { ...state.custom, createLoading: false }}      
     case types.ADD_MORE_USER_CUSTOM_FEED_INIT:
        return{...state,custom: { ...state.custom, createLoading: true }}   
     case types.ADD_MORE_USER_CUSTOM_FEED_FINISH:
        return{...state,custom: { ...state.custom, createLoading: false }, finishFeedUpdate: true}
     case types.RESET_UPDATE_FEED_FORM:
        return{...state, finishFeedUpdate: false}  
     case types.UPDATE_FEED_DETAIL_INIT:
        return{...state,custom: { ...state.custom, createLoading: true }}   
     case types.UPDATE_FEED_DETAIL_FINISH:
        return{...state,custom: { ...state.custom, createLoading: false },finishFeedUpdate: true}    
     case types.RATE_POST_SUCCESS:
        var uniqueItems = [...new Set(state.ratedPost.concat(action.ratedPost))]
        return{...state, ratedPost:uniqueItems }    
     case types.STORE_LAST_POST_ID:
        return{...state, lastPostId: action.postPublisherId}     
     case types.FETCH_DEFAULT_LIVE_INIT:
        return{...state, lastPostId: null} 

    case types.FETCH_MY_UNAPPROVED_INIT:
        return { ...state, mypost: {  ...state.mypost, loading:true } ,feedLoading:true};
    case types.FETCH_MY_UNAPPROVED_SUCCESS:
        if( action.onPostsScroll !== true){
          //if post is pinned or new post has been created or hash is has been selected
          return { ...state, mypost: { loading: false,scheduled:[] ,live: [],unapproved:action.data,loadmore:false}, meta: {} ,feedLoading:false}
        } 
      else 
        {
          return { ...state, mypost: { loading: false, scheduled: [], live: [],unapproved : state.mypost.unapproved.concat(action.data) ,loadmore:false}, meta: {} ,feedLoading:false};
        }
    case types.FETCH_MY_UNAPPROVED_ERROR:
      return { ...state, mypost: { loading: false, feedLoading:false } }

    case types.FETCH_MY_PENDING_INIT:
        return { ...state, mypost: {  ...state.mypost, loading:true } ,feedLoading:true};

    case types.FETCH_MY_PENDING_SUCCESS:
        if( action.onPostsScroll !== true){
          //if post is pinned or new post has been created or hash is has been selected
          return { ...state, mypost: { loading: false,scheduled:[] ,live: [],unapproved:[], pending:action.data,loadmore:false}, meta: {} ,feedLoading:false}
        } 
      else 
        {
          return { ...state, mypost: { loading: false, scheduled: [], live: [],unapproved :[],pending: state.mypost.pending.concat(action.data) ,loadmore:false}, meta: {} ,feedLoading:false};
        }
    case types.FETCH_MY_PENDING_ERROR:
      return { ...state, mypost: { loading: false, feedLoading:false } }
    case types.FETCH_HASH_TAG_INIT_FLAG:
      return { ...state, savedHashTagListInPost: { loading: true, hashTagListInCreatePost:{} } }
    case types.FETCH_HASH_TAG_SUCCESS_FLAG:
      return { ...state, savedHashTagListInPost: { loading: false, hashTagListInCreatePost:action.hashTag } }
    case types.FETCH_HASH_TAG_FAIL_FLAG:
      return { ...state, savedHashTagListInPost: { loading: false, hashTagListInCreatePost:{} } }
    default:
      return state;
  }
}
export default feedReducer
