import * as types from '../actions/actionTypes'

const initialState = {
    analytic:{
    filter_visibility:false
    },
    filterFlag : false
}

let headerReducer = function(state = initialState, action) {
    switch (action.type) {
        case types.ENABLE_FILTER_FLAG_ANALYTICS:
           return{...state,analytic:{filter_visibility:true}};
        case types.DISABLE_FILTER_FLAG_ANALYTICS:
           return{...state,analytic:{filter_visibility:false}};
        case types.SET_FILTER_FLAG: 
            return{...state,filterFlag : action.flag}
        default:
           return state;
    }
}

export default headerReducer