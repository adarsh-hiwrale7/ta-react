import * as types from '../actions/actionTypes'

const initialState = {
  ev: null,
  scroll: null,
  WorldDateTime:{},
  worldTimeLoader:false,
  s3SizeData:'',
  storageLoading:false,
  isFetchS3Size:false,
  clientIpDetail:{},
  iploading:false
};

let generalReducer = function(state = initialState, action) {
  switch (action.type) {
    case types.EVENT_INFO:
      return { ...state, ev: action.ev };

    case types.WORLD_TIME_INIT:
      return {...state,worldTimeLoader:true}

    case types.FETCH_S3_SIZE_INIT:
      return {...state,storageLoading:true,isFetchS3Size:false}

    case types.SUCCESS_FETCH_WORLD_DATE_TIME:
      return{...state,WorldDateTime:action.data,worldTimeLoader:false};

    case types.SUCCESS_FETCH_S3_SIZE:
      return{...state,s3SizeData:action.data,storageLoading:false,isFetchS3Size:true}

    case types.RESET_S3_SIZE_FLAG:
      return {...state,isFetchS3Size:false}

    case types.ERROR_FETCH_S3_SIZE:
      return {...state,s3SizeData:'',storageLoading:true,isFetchS3Size:false}

    case types.CLIENT_IP_INIT:
      return{...state,iploading:true}

    case types.SUCCESS_CLIENT_IP:
      return{...state,clientIpDetail:action.data,iploading:false}

    case types.ERROR_CLIENT_IP:
      return{...state,iploading:false,clientIpDetail:{}}
    default:
      return state;
  }
}

export default generalReducer
