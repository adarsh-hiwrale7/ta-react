import * as types from '../actions/actionTypes'

const initialState = {
  channels: [],             // all social channels
  loading: false,
  selectedChannels: []      // post's or campaign's selected channel
};

let channelsReducer = function(state = initialState, action) {
  switch (action.type) {
    case types.FETCH_CHANNELS_SUCCESS:
      return {
        ...state,
        channels: action.channels,
        loading: false,
      };
    case types.FETCH_CHANNELS_START:
      return {
        ...state,
        channels: [],
        loading: false,
      };
    case types.FILTER_CHANNELS_SUCCESS:
      return { ...state, selectedChannels: action.selectedChannels };
    default:
      return state;
  }
}

export default channelsReducer
