import * as types from '../actions/actionTypes'

const initialState = {
    companyLocationList:[],
    companyLoader:false
};

let companyReducer = function(state = initialState, action) {
    switch (action.type) {
    case types.GET_COMPANY_LOCAITON_INIT:
        return { ...state, companyLoader:true};
    case types.GET_COMPANY_LOCAITON_SUCCESS:
        return {...state, companyLoader:false,companyLocationList:action.data}
    case types.GET_COMPANY_LOCAITON_Error:
        return { ...state, companyLoader:false};

    case types.ADD_COMPANY_LOCAITON_INIT:
        return { ...state, companyLoader:true};
    case types.ADD_COMPANY_LOCAITON_SUCCESS:
        return {...state, companyLoader:false}
    case types.ADD_COMPANY_LOCAITON_ERROR:
        return { ...state, companyLoader:false};

    case types.UPDATE_COMPANY_LOCAITON_INIT:
        return { ...state, companyLoader:true};
    case types.UPDATE_COMPANY_LOCAITON_SUCCESS:
        return {...state, companyLoader:false}
    case types.UPDATE_COMPANY_LOCAITON_ERROR:
        return { ...state, companyLoader:false};
        default:
        return state;
    }
}

export default companyReducer
