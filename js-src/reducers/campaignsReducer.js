import * as types from '../actions/actionTypes'

const initialState = {
  createdCampaignInfo: [],
  creation: {
    created: false,
    loading: false
  },
  tasks:[],
  taskLoading : false,
  campaigns: [],
  segmentsData:[],
  segmentloading:false,
  addSegmentLoading:false,
  addCollaboratorLoading:false,
  collaboratorLoading:false,
  particularCollaborator:{},
  previewCollaborator:[],
  collaboratorsData:[],
  updated_Campaign:{},
  commentsData:[],
  commentLoading:false,
  replyData:[],
  updation: {
    updated: false,
    loading: false,
  },
  editProjectLoading:false,
  contentSaved:false,
  scheduleDates:[],
  scheduleLoading:false,
  campaignPosts:[],
  calendarPosts:[],
  calendarTask:[],
  postLoading:false,
  deleteCommentLoading:false,
  particularSegment:{},
  events: [],
  files: [],
  editValueType:null,
  listLoading:false,
  noCampaigns: false,
  createCampaignFlag:false,
  dataLoading: false,
  addTaskLoading: false,
  taskSaved:false,
  myPendingTask:{
    myPendingTaskData:[],
    myPendingTaskLoading:false
  },
  myPendingContent:{
    myPendingContentData:[],
    myPendingContentLoading:false
  },
  createContentProject:{
    createContentProjectData:'',
    createContentProjectLoading:false
  },
  ContentProjectLoading : false,
  currContentProject : null,
  contentLists:{
    contentListsData:[],
    contentListsLoading:false
  },
  contentProjectListLoading:false,
  contentProjectList:[],
  campaignAssetList:[],
  campaignAssetLoading:false,
  reviewContent: {reviewContentData:[],reviewContentLoading:false},
  defaultDraftTemplates: {defaultDraftTemplatesData:[],defaultDraftTemplatesLoading:false},
  draftFiles: {draftFilesData:[],draftFilesLoading:false},
  projectAction:null,
  redirectToProductionForReview:false,
  plannerData:{loading : false , posts:[],print:[],email:[],SMS:[], calendarPosts :[],calendarPrints:[],calendarEmails:[],calendarSMS:[]},
  kanbanStatusList:[],
  deleteProjectLoading:false,
  redirectFor:'',
  projectHtmlData:null,
  htmlFetchError:false,
  campaignAllUserList:{campaignAllUserListLoading:false,campaignAllUserListData:[]},
  redirectToProductionForPending:false,
  recipientData : {
    recipientDataLoader :false
  },
  fetchRecipientData :{
    fetchRecipientDataList : {},
    fetchRecipientLoading : false
  },
  deleteRecipientData:{
    fetchRecipientDataList :'',
    fetchRecipientLoading : false
  },
  openCamapignFromNoti:false,
  opencontentProjectNotie : false,
  notie_project_identity:null,
  campaignMeta:{},
  segmentsUserList:{
    segmentsUserListLoading:false,
    segmentsUserListData:[]
  },
  deleteSegmentUserLoader:false,
  fetchCampaignRecentSegments:{
      fetchCampaignRecentSegmentsLoading:false ,
      fetchCampaignRecentSegmentsData:{}
   },
   displayTaskOfContent:false,
   typeEvent:{typeEventLoading:false ,typeEventData:[],typeEventMetaData:{},eventCalendar:[]}
};

let campaignsReducer = function(state = initialState, action) {
  switch (action.type) {
    case types.CAMPAIGNS_CREATING:
      return { ...state, creation: { loading: true, created: false }, createdCampaignInfo: [] };
    case types.CAMPAIGNS_CREATING_NEW:
      return { ...state, creation: { loading: false, created: false } };
    case types.CAMPAIGNS_CREATION_SUCCESS:
      return { ...state, creation: { loading: false, created: true }, createdCampaignInfo: action.createdCampaignInfo };
    case types.CAMPAIGNS_CREATION_ERROR:
      return { ...state, creation: { loading: false, created: false }, createdCampaignInfo: [] };
    case types.FETCH_CAMPAIGNS_SUCCESS:
      return { ...state, campaigns: action.campaigns, listLoading: false, events: action.events,creation: { loading: false},createCampaignFlag:action.createCampaign};
    case types.FETCH_CAMPAIGNS_START:
      return { ...state, campaigns: [] ,listLoading:true,creation: { created: false ,loading: true},createCampaignFlag:false };
    
      // campaign posts
    case types.FETCH_CAMPAIGN_POST_ERROR:
      return{...state,campaignPosts:[],postLoading:true}

    case types.FETCH_CAMPAIGN_POST_SUCCESS:
        return{...state,campaignPosts:action.postData,calendarPosts:action.calendarPosts,postLoading:false}

    case types.FETCH_CAMPAIGN_POST_ERROR:
      return{...state,campaignPosts:[],calendarPosts:[],postLoading:false}

    case types.CAMPAIGN_POST_UPDATETING:
        return{...state,updation: { loading: true, updated:false }}
    case types.CAMPAIGN_POST_UPDATE_INIT:
        return{...state,updation:{loading:true,updated: false}}
    case types.CAMPAIGN_POST_UPDATION_SUCCESS:
        return{...state,updation: { loading: false, updated:true}}
    case types.CAMPAIGN_POST_UPDATE_ERROR:
        return{...state,updation: { loading: false, updated:false}}

    case types.GET_CAMPAIGN_SCHEDULES_INIT:
        return{...state,scheduleLoading:true}
    case types.GET_CAMPAIGN_SCHEDULES_SUCCESS:
        return{...state,scheduleDates:action.scheduleData,scheduleLoading:false}
    case types.GET_CAMPAIGN_SCHEDULES_ERROR:
        return{...state,scheduleDates:[],scheduleLoading:false}


    case types.SAVE_CAMPAIGN_DATA_INIT:
      return{...state,dataLoading: true}
    case types.SAVE_CAMPAIGN_DATA_SUCCESS:
      if(action.dataType =="title"){
          return{...state,dataLoading:false}
      }else if(action.dataType == "campaignBrief"){
        return{...state , dataLoading: false}
      }
    case types.SAVE_CAMPAIGN_DATA_ERROR:
      return{...state,dataLoading:false}
    case types.FETCH_CAMPAIGN_SUCCESS:
        return{...state,updated_Campaign:action.campaign_data}
    // FETCH TASKS
    case types.FETCH_TASKS_SUCCESS:
      return{...state,taskLoading: false,tasks:action.data,calendarTask:action.taskList}
    case types.FETCH_TASKS_INIT:
      return{...state,taskLoading:true}
    case types.FETCH_TASKS_ERROR:
      return{...state,taskLoading:false,tasks:[],calendarTask:[]}

    // add task
    case types.ADD_TASK_DATA_SUCCESS:
      return{...state,addTaskLoading: false, taskSaved : action.data}
    case types.ADD_TASK_DATA_INIT:
      return{...state,addTaskLoading: true}
    case types.ADD_TASK_ERROR:
        return{...state,addTaskLoading:false, taskSaved : false}
    
    case types.DELETE_TASK_INIT:
        return{...state,deleteTaskLoading:true}
    case types.DELETE_TASK_SUCCESS:
        return{...state,deleteTaskLoading:false}
    case types.DELETE_TASK_ERROR:
        return{...state,deleteTaskLoading:false}
    // segments 
    case types.FETCH_CAMPAIGN_SEGMENTS_SUCCESS:
      return{...state, segmentsData :action.data,segmentloading:false}
    case types.FETCH_CAMPAIGN_SEGMENTS_INIT:
      return{...state, segmentloading:true}
    case types.FETCH_CAMPAIGN_SEGMENTS_ERROR:
        return{...state,segmentloading:false}
    case types.FETCH_CAMPAIGN_SEGMENTS_USER_INIT:
        return{...state,segmentsUserList:{segmentsUserListLoading : true,segmentsUserListData:[]}}
    case types.FETCH_CAMPAIGN_SEGMENTS_USER_SUCCESS:
        return{...state,segmentsUserList:{segmentsUserListLoading :false ,segmentsUserListData:action.data}}
    case types.FETCH_CAMPAIGN_SEGMENTS_USER_ERROR:
        return{...state,segmentsUserList:{segmentsUserListLoading :false,segmentsUserListData:[]}}
   case types.FETCH_SEGMENTBYID_INIT:
        return{...state,segmentloading:true,particularSegment:{}}
    case types.FETCH_SEGMENTBYID_SUCCESS:
       return{...state,segmentloading:false,particularSegment:action.data}
    case types.FETCH_SEGMENTBYID_ERROR:
        return{...state,segmentloading:false,particularSegment:{}}
    case types.ADD_SEGMENTDATA_INIT:
        return{...state,addSegmentLoading:true}
    
    case types.ADD_SEGMENTDATA_SUCCESS:
        return{...state,addSegmentLoading:false}

    case types.ADD_SEGMENTDATA_ERROR:
        return{...state,addSegmentLoading:false}

    case types.ADD_COLLABORATORS_ERROR:
        return{...state,  addCollaboratorLoading:false}
    case types.ADD_COLLABORATORS_INIT:
        return{...state, addCollaboratorLoading:true}
    case types.ADD_COLLABORATORS_SUCCESS:
        return{...state, addCollaboratorLoading:false}

    case types.DELETE_COLLABORATOR_INIT:{
        return{...state,collaboratorLoading:true}
    }
    case types.DELETE_COLLABORATOR_SUCCESS:{
        return{...state,collaboratorLoading:false}
    }
    case types.DELETE_COLLABORATOR_ERROR:
        return{...state,collaboratorLoading:false}

    case types.FETCH_COLLABORATOR_INIT:
        return{...state,collaboratorLoading:true}
    case types.FETCH_COLLABORATOR_SUCCESS:
        return{...state,collaboratorLoading:false,collaboratorsData:action.data}
    case types.FETCH_COLLABORATOR_ERROR:
        return{...state,collaboratorLoading:false}
    case types.FETCH_COLLABORATOR_BY_ID_INIT:
        return{...state,collaboratorLoading:true,particularCollaborator:{}}
    case types.FETCH_COLLABORATOR_BY_ID_SUCCESS:
        return{...state,collaboratorLoading:false,particularCollaborator:action.data}
    case types.FETCH_COLLABORATOR_BY_ID_ERROR:
        return{...state,collaboratorLoading:false,particularCollaborator:{}}

    case types.PREVIEW_COLLABORATOR_SUCCESS:
        return{...state,previewCollaboratorLoading :false,previewCollaborator:action.data}
    case types.PREVIEW_COLLABORATOR_INIT:
        return{...state,previewCollaboratorLoading :true,previewCollaborator:[]}

    case types.DELETE_SEGMENT_INIT:
      return{...state, segmentloading:true}
    case types.DELETE_SEGMENT_SUCCESS:
      return{...state, segmentloading:false}
    case types.DELETE_SEGMENT_ERROR:
      return{...state,segmentloading:false}
    // comments 
    case types.FETCH_COMMENTS_SUCCESS:
      return{...state,commentLoading : false,commentsData:action.data}
    case types.FETCH_COMMENTS_INIT:
      return{...state,commentLoading:true}
    
    case types.ADD_COMMENT_INIT:
        return{...state,commentLoading:true}
    case types.ADD_COMMENT_SUCCESS:
        return{...state,commentLoading:false}
    case types.ADD_COMMENT_ERROR:
        return{...state,commentLoading:false}

    case types.DELETE_COMMENT_INIT:
        return{...state,commentLoading:true,deleteCommentLoading : true}
    case types.DELETE_COMMENT_SUCCESS:
        return{...state,commentLoading:false,deleteCommentLoading : false}
    case types.DELETE_COMMENT_ERROR:
      return{...state,commentLoading:false,deleteCommentLoading : false}
    case types.FETCH_REPLY_INIT:
        return{...state}
    case types.FETCH_REPLY_SUCCESS:
        return{...state,replyData : action.data}

    case types.CAMPAIGNS_NOT_FOUND:
      return { ...state, campaigns: [] ,listLoading:false, noCampaigns: false };
    case types.VIEWING_CAMPAIGN:
      return { ...state, creation: {loading: false, created: false}}
     case types.ERROR_FETCHING_CAMPAIGN:
      return { ...state, creation: {loading: false, created: false}}

    case types.FETCH_TOP_MYPENDING_TASK_INIT:
      return{...state,myPendingTask: {myPendingTaskData:[],myPendingTaskLoading:true}}
    case types.FETCH_TOP_MYPENDING_TASK_SUCCESS:
          return{...state,myPendingTask: { myPendingTaskData :action.pendingData,myPendingTaskLoading:false}}
    case types.FETCH_TOP_MYPENDING_TASK_ERROR:
      return{...state,myPendingTask:{myPendingTaskData:[],myPendingTaskLoading:false}}

    case types.FETCH_TOP_MYPENDING_CONTENT_INIT:
      return{...state,myPendingContent: {myPendingContentData:[],myPendingContentLoading:true}}
    case types.FETCH_TOP_MYPENDING_CONTENT_SUCCESS:
          return{...state,myPendingContent: { myPendingContentData :action.data,myPendingContentLoading:false}}
    case types.FETCH_TOP_MYPENDING_CONTENT_ERROR:
      return{...state,myPendingContent:{myPendingContentData:[],myPendingContentLoading:false}}

    case types.FETCH_CONTENT_LIST_INIT:
      return{...state,contentLists: {contentListsData:[],contentListsLoading:true}}
    case types.FETCH_CONTENT_LIST_SUCCESS:
          return{...state,contentLists: { contentListsData :action.data,contentListsLoading:false}}
    case types.FETCH_CONTENT_LIST_ERROR:
      return{...state,contentLists:{contentListsData:[],contentListsLoading:false}}

    case types.CREATE_CONTENT_PROJECT_INIT:
      return{...state,createContentProject: {createContentProjectData:'',createContentProjectLoading:true}}
    case types.CREATE_CONTENT_PROJECT_SUCCESS:
      return{...state,createContentProject: { createContentProjectData :action.data,createContentProjectLoading:false}}
    case types.CREATE_CONTENT_PROJECT_ERROR:
      return{...state,createContentProject:{createContentProjectData:'',createContentProjectLoading:false}}

    case types.FETCH_REVIEW_CONTENT_LIST_INIT:
      return{...state,reviewContent: {reviewContentData:[],reviewContentLoading:true}}
    case types.FETCH_REVIEW_CONTENT_LIST_SUCCESS:
          return{...state,reviewContent: { reviewContentData :action.data,reviewContentLoading:false}}
    case types.FETCH_REVIEW_CONTENT_LIST_ERROR:
      return{...state,reviewContent:{reviewContentData:[],reviewContentLoading:false}}

    case types.FETCH_DEFAULT_DRAFT_TEMPLAES_INIT:
      return{...state,defaultDraftTemplates: {defaultDraftTemplatesData:[],defaultDraftTemplatesLoading:true}}
    case types.FETCH_DEFAULT_DRAFT_TEMPLAES_SUCCESS:
          return{...state,defaultDraftTemplates: { defaultDraftTemplatesData :action.data,defaultDraftTemplatesLoading:false}}
    case types.FETCH_DEFAULT_DRAFT_TEMPLAES_ERROR:
      return{...state,defaultDraftTemplates:{defaultDraftTemplatesData:[],defaultDraftTemplatesLoading:false}}

    case types.FETCH_DRAFT_FILES_INIT:
      return{...state,draftFiles: {draftFilesData:[],draftFilesLoading:true}}
    case types.FETCH_DRAFT_FILES_SUCCESS:
          return{...state,draftFiles: { draftFilesData :action.data,draftFilesLoading:false}}
    case types.FETCH_DRAFT_FILES_ERROR:
      return{...state,draftFiles:{draftFilesData:[],draftFilesLoading:false}}

    // campaign print reducers 
    case types.FETCH_ALL_PLANNER_DATA_INIT:
      return{...state, plannerData :{...state.plannerData,loading:true}}

    case types.FETCH_ALL_PLANNER_DATA_SUCCESS:
        var postsArray= action.data.Posts;
        var printArray = action.data.Print;
        var EmailArray = action.data.Email;
        var SMSArray = action.data.SMS;
      return{...state, plannerData :{loading:false, posts :postsArray,print : printArray , email : EmailArray ,SMS : SMSArray,calendarPosts :action.posts, calendarPrints:action.printArray, calendarEmails:action.emailArray,calendarSMS:action.SMSArray}}

    case types.FETCH_ALL_PLANNER_DATA_ERROR:
      return{...state, plannerData :{loading:false,posts:[],print:[],email:[],SMS:[]}}


    case types.EDIT_CONTENT_PROJECT_INIT:
      return{...state,editProjectLoading:true}

    case types.EDIT_CONTENT_PROJECT_SUCCESS:
      return{...state,editProjectLoading:false, editValueType : action.value, contentSaved : true}
    
    case types.EDIT_CONTENT_PROJECT_ERROR:
      return{...state,editProjectLoading:false,contentSaved:false}

    case types.FETCH_PLANNER_PRINTS_ERROR:
        return{...state,plannerData:{...state.plannerData,loading:false,print :[],calendarPrints:[]}}
    
    case types.FETCH_PLANNER_PRINTS_SUCCESS:
      return{...state,plannerData:{...state.plannerData,loading:false,print :action.data,calendarPrints:action.printList}}

    case types.FETCH_PLANNER_EMAILS_SUCCESS:
        return{...state,plannerData:{...state.plannerData,loading:false,email :action.data,calendarEmails:action.emailList}}
    
    case types.FETCH_PLANNER_EMAILS_ERROR:
        return{...state,plannerData:{...state.plannerData,loading:false,email :[],calendarEmails:[]}}

    case types.FETCH_PLANNER_DATA_INIT:
        return{...state,plannerData:{...state.plannerData,loading:true}}
    
    case types.FETCH_PLANNER_POST_ERROR:
        return{...state,plannerData:{...state.plannerData,loading:false,posts :[],calendarPosts:[]}}
    
    case types.FETCH_PLANNER_POST_SUCCESS:
      return{...state,plannerData:{...state.plannerData,loading:false,posts :action.data,calendarPosts:action.postList}}

    case types.GET_CONTENT_PROJECT_BY_ID_INIT:
      return{...state,ContentProjectLoading:true,projectAction:null}

    case types.GET_CONTENT_PROJECT_BY_ID_SUCCESS:
      return{...state,ContentProjectLoading:false, currContentProject: action.data,projectAction:action.event}
    
    case types.GET_CONTENT_PROJECT_BY_ID_ERROR:
        return{...state,ContentProjectLoading:false, currContentProject: null,projectAction:null}

    case types.FETCH_CAMPAIGN_ALL_USER_LIST_INIT:
      return{...state,campaignAllUserList:{campaignAllUserListLoading:true,campaignAllUserListData:[]}}

    case types.FETCH_CAMPAIGN_ALL_USER_LIST_SUCCESS:
        return{...state,campaignAllUserList:{campaignAllUserListLoading:false,campaignAllUserListData:action.data}}
    
    case types.FETCH_CAMPAIGN_ALL_USER_LIST_ERROR:
        return{...state,campaignAllUserList:{campaignAllUserListLoading:false,campaignAllUserListData:[]}}

    case types.DELETE_CONTENT_PROJECT_SUCCESS:
        return{...state,deleteProjectLoading:false}

    case types.DELETE_CONTENT_PROJECT_INIT:
      return{...state,deleteProjectLoading:true}

    case types.DELETE_CONTENT_PROJECT_ERROR:
      return{...state,deleteProjectLoading:false}
    case types.FETCH_PLANNER_PROJECTS_INIT:
        return{...state,contentProjectListLoading:true}
 
    case types.FETCH_PLANNER_PROJECTS_SUCCESS:
        return{...state,contentProjectListLoading:false,contentProjectList:action.data}
      
    case types.FETCH_PLANNER_PROJECTS_ERROR:
        return{...state,contentProjectListLoading:false}
    
    case types.UPDATE_META_DATA:
        return{...state,meta:action.data}

    case types.REDIRECT_TO_PRODUCTION_FOR_REVIEW_CONTENT:
        return{...state,redirectToProductionForReview:true ,redirectFor:action.data}

    case types.RESET_REDIRECT_TO_PRODUCTION_FOR_REVIEW_CONTENT:
        return{...state,redirectToProductionForReview:false,redirectFor:''}

        case types.REDIRECT_TO_PRODUCTION_FOR_PENDING_CONTENT:
        return{...state,redirectToProductionForPending:true ,redirectFor:action.data}

    case types.RESET_REDIRECT_TO_PRODUCTION_FOR_PENDING_CONTENT:
        return{...state,redirectToProductionForPending:false,redirectFor:''}
    
    case types.ADD_DRAFT_INIT:
        return{...state,addDraftLoading  : true}
    
    case types.ADD_DRAFT_SUCCESS:
      return{...state,addDraftLoading  : false}
    
    case types.ADD_DRAFT_ERROR:
        return{...state,addDraftLoading  : false}

    case types.FETCH_KANBAN_STATUS_LIST:
        return{...state,kanbanStatusList:action.data}
        
    case types.FETCH_CAMPAIGN_ASSET_SUCCESS:
        return{...state,campaignAssetLoading:false , campaignAssetList : action.data}
    
    case types.FETCH_CAMPAIGN_ASSET_INIT:
        return{...state,campaignAssetLoading : true}

    case types.HANDLE_CONTENT_CREATION_STATE:
        return{...state,createContentProject: {createContentProjectData:'',createContentProjectLoading:false}}
    
    case types.GET_PROJECT_HTML_SUCCESS:
        return{...state,projectHtmlData : action.data , htmlFetchError:false}
    case types.GET_PROJECT_HTML_ERROR:
        return{...state, projectHtmlData:null, htmlFetchError : true}
    //receipt
    case types.SAVE_RECIPIENT_DATA_INIT:
        return{...state, recipientData:{recipientDataLoader : true}}
    case types.SAVE_RECIPIENT_DATA_SUCCESS:
        return{...state, recipientData:{recipientDataLoader:false}} 
    case types.SAVE_RECIPIENT_DATA_ERROR:
        return{...state,recipientData:{recipientDataLoader:false}}
    case types.FETCH_RECIPIENT_DATA_LIST_INIT:
        return{...state,fetchRecipientData:{fetchRecipientLoading:true,fetchRecipientDataList:{}}}
    case types.FETCH_RECIPIENT_DATA_LIST_SUCCESS:
        return{...state,fetchRecipientData:{fetchRecipientDataList:action.fetchdata ,fetchRecipientLoading:false}}
    case types.FETCH_RECIPIENT_DATA_LIST_ERROR:
        return{...state,fetchRecipientData:{fetchRecipientDataList:{} ,fetchRecipientLoading:false}}

    case types.DELETE_RECIPIENT_USER_INIT:
        return{...state,deleteRecipientUser:{deleteRecipientLoading:true}}
    case types.DELETE_RECIPIENT_USER_SUCCESS:
        return{...state,deleteRecipientUser:{deleteRecipientDataUser:action.data ,deleteRecipientLoading:true}}
    case types.DELETE_RECIPIENT_USER_ERROR:
        return{...state,deleteRecipientData:{fetchRecipientDataList:action.data ,fetchRecipientLoading:false}}
    case types.OPEN_CAMPAIGN_FROM_NOTI:
        return{...state,openCamapignFromNoti:true}
    case types.CLOSE_CAMPAIGN_FROM_NOTI:
        return{...state,openCamapignFromNoti:false}
    
    case types.OPEN_CONTENT_PROJECT_FROM_NOTIE:
        return{...state,opencontentProjectNotie:true,notie_project_identity:action.notie_project_identity}
    
    case types.CLOSE_CONTENT_PROJECT_FROM_NOTIE:
        return{...state,opencontentProjectNotie:false,notie_project_identity:null}

    case types.CAMPAIGN_SUCCESS_META:
        return{...state,campaignMeta:action.data}
    case types.DELETE_SEGMENT_USER_INIT:
        return{...state,deleteSegmentUserLoader:true}
    case types.DELETE_SEGMENT_USER_SUCCESS:
        return{...state,deleteSegmentUserLoader:false}
    case types.DELETE_SEGMENT_USER_ERROR:
        return{...state,deleteSegmentUserLoader:false}

    case types.FETCH_CAMPAIGN_RECENT_SEGMENTS_INIT:
        return{...state,fetchCampaignRecentSegments:{fetchCampaignRecentSegmentsLoading:true ,fetchCampaignRecentSegmentsData:{}}}
    case types.FETCH_CAMPAIGN_RECENT_SEGMENTS_SUCCESS:
         return{...state,fetchCampaignRecentSegments:{fetchCampaignRecentSegmentsLoading:false ,fetchCampaignRecentSegmentsData:action.recentSegment}}
    case types.FETCH_CAMPAIGN_RECENT_SEGMENTS_ERROR :
         return{...state,fetchCampaignRecentSegments:{fetchCampaignRecentSegmentsLoading:false ,fetchCampaignRecentSegmentsData:{}}}
         
    case types.DISPLAY_TASK_OF_CONTENT :
        return{...state,displayTaskOfContent:action.data}
           
    case types.FETCH_EVENT_LIST_SUCCESS:
        return{...state,typeEvent:{...state.typeEvent,typeEventLoading:false ,typeEventData:action.data, eventCalendar :action.event}}
    case types.FETCH_EVENT_LIST_INTI:
        return{...state,typeEvent:{typeEventLoading:true }}
    case types.FETCH_EVENT_LIST_ERROR :
        return{...state,typeEvent:{typeEventLoading:false ,typeEventData:[],typeEventMetaData:{},eventCalendar:[]}}
    case types.FETCH_EVENT_LIST_SUCCESS_META :
        return{...state,typeEvent:{...state.typeEvent,typeEventLoading:false ,typeEventMetaData:action.data}}
         
    default:
      return state;
  }
}
export default campaignsReducer
