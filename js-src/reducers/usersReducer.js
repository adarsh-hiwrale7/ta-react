import * as types from '../actions/actionTypes'

const initialState = {
  users: [],
  loading: false,
  closeAllPopups: false,
  socialAccounts :{},
  roles: [],
  userDetails: {},
  userDetailsSidebar:{},
  isFetching:false,
  fieldlist:[],
  isFetchingForSidebar:false,
  rolesLoader:false,
  usersListMeta:{}
};

let usersReducer = function(state = initialState, action) {
  switch (action.type) {
    case types.FETCH_USERS_START:
        return { ...state,users : [],loading: true };

    case types.FETCH_USERS:
      return { ...state, users : [],loading: true };

    case types.FETCH_USERS_SUCCESS:
      return { ...state, users : action.users,  loading: false };

    case types.FETCH_ROLES_INIT:
      return{...state,rolesLoader:true}

    case types.FETCH_ROLES_SUCCESS:
      return { ...state, roles : action.roles ,rolesLoader:false};
    
    case types.FETCH_ROLES_ERROR:
      return {...state,rolesLoader:false}

      case types.ADD_USER_SAVING: {
      return { ...state, loading: true, closeAllPopups: false }
    }
    case types.ADD_USER_ERROR: {
      return { ...state, loading: false, closeAllPopups: false  }
    }

    case types.ADD_USER_SUCCESS: {
      return { ...state, loading: false, closeAllPopups: true }
    }

    case types.FETCH_USER_DETAILS_SUCCESS:
      return { ...state, userDetails : action.userDetails,isFetching:false };

    case types.FETCH_USER_DETAILS_SUCCESS_SIDEBAR_POPUP:
      return {...state,userDetailsSidebar : action.userDetails,isFetching:false}
    
    case types.IS_FETCHING:
      return { ...state, isFetching : action.status };

    case types.IS_FETCHING_FOR_SIDEBAR:
      return {...state,isFetchingForSidebar : action.status}

    case types.FETCH_LIST:
      return{...state,fieldlist:action.data}
    
    case types.ADD_CSV_SUCCESS:
      return{...state,loading:true,closeAllPopups: true }
      
    case types.ADD_CSV_ERROR: {
        return { ...state, loading: false, closeAllPopups: false  }
      }

    case types.FETCH_USERS_META_SUCCESS:
        return{...state,usersListMeta:action.data}
    default:
      return state;
  }
}

export default usersReducer
