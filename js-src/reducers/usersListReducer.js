import * as types from '../actions/actionTypes'

const initialState = {
   userList:[]
}

let usersListReducer = function(state = initialState, action) {
    switch (action.type) {
        case types.USERS_LIST:
           return{...state, userList:action.userList};
        default:
           return state;
    }
}

export default usersListReducer