import * as types from '../actions/actionTypes'

const initialState = {
    onboarding: {
      loading: false,
      statusText: "",
    },
    companyInfo: {},
    countrycode:{},
    loading: false,
    rsslink:[],
    rsslinkDetailLoading:false,
    curationTopicList:[],
    curationListLoader:false,
    surveyData: {},
    companySettingLoader: false,
    companyLocationLoader: false,
    companySettingUpdateFlag: false,
    companySettingData:{},
    companyMasterData:{
      companyMasterDataLoader: false,
      department: {},
      business:{},
      role:{},
      office:{},
      companyDataSaveLoader: false,
      departmentMaster: 0,
      businessMaster: 0,
      roleMaster: 0,
      officeMaster: 0,
      parentId:{office: 0, department: 0, office: 0, role: 0},
      deleteEntity:null

    },
    companyHierarchyData:{
      regions: {}
    },
   
    officeLocations:{},
    utmData:{},
    hashTagContent:{
             loading:false,
             isHashTagSaved:false,
             hashTagList:[]
       
    },
    companyLoader:false,
    addSsoUserOnbaord :{addSsoUserOnbaordLoader: false,addSsoUserOnbaordData:{}}
};


let settingsReducer = function(state = initialState, action) {

  switch (action.type) {
    case types.GET_ALL_SETTING_INIT:
       return{...state,loading: true}
    case types.SETTINGS_SAVING:
      return { ...state, onboarding: {loading: true}, loading: true };

    case types.SETTINGS_SUCCESS:
      return { ...state, onboarding: {loading: false}, loading: false };

    case types.SETTINGS_ERROR:
      return { ...state, onboarding: {loading: false, statusText: action.text}, loading: false };

    case types.SETTINGS_COMPANY_INFO_SUCCESS:
      return { ...state, onboarding: {loading: false}, companyInfo: action.companyInfo, loading: false };
    
    case types.GET_COUNTRY_CODE_LIST_SUCCESS:
       return { ...state, onboarding: {loading: false}, companyInfo: action.companyInfo, countrycode: action.data, loading: false };

    case types.FETCH_RSS_DETAILS_LINK:
    return {
        ...state,rsslink: action.rsslink,
    }; 
    case types.RSSLINK_INIT:
    return { 
          ...state,rsslinkDetailLoading:true
    };  
    case types.RSSLINK_SUCCESS:
    return { 
              ...state, rsslinkDetailLoading:false
           };
  case types.RSSLINK_FAIL:
    return { 
              ...state, rsslinkDetailLoading:false
           };
   case types.UTM_DATA_SUCCESS:
       return{
              ...state,utmData:action.data
       }
   case types.RESET_UTM_DATA:
       return{...state,utmData:{}}
   case types.REMOVE_RSS_URL_EMAIL_INIT:
     return {
              ...state, contactDetailLoading:true
           }         
    case types.REMOVE_RSS_URL_SUCCESS:
      return {
               ...state, contactDetailLoading:false
              }   
    case types.REMOVE_RSS_URL_FAIL:
           return{
             ...state,contactDetailLoading:false
           }        
    
    case types.SUCCESS_GUEST_SIGNUP:
           return{...state,onboarding: {loading: false}}

    case types.ERROR_GUEST_SIGNUP:
           return{...state,onboarding: {loading: false}}

    case types.GUEST_SIGNUP_INIT:
           return{...state,onboarding: {loading: true}}

    case types.CURATION_TOPIC_LIST_INIT:
           return{...state,curationListLoader:true}
    case types.CURATION_TOPIC_LIST_SUCCESS:
           return{...state,curationListLoader:false,curationTopicList:action.data}
    case types.CURATION_TOPIC_LIST_ERROR:
           return{...state,curationListLoader:false}
    case types.GET_SURVEY_SETTING_INIT:
           return{...state,loading:true}
    case types.GET_SURVEY_SETTING_SUCCESS:
           return{...state,loading:false,surveyData:action.data}
    case types.GET_SURVEY_SETTING_FAIL:
           return{...state,loading:false}    
    case types.UPDATE_SURVEY_SETTING_INIT:
           return{...state,loading:true}
    case types.UPDATE_SURVEY_SETTING_SUCCESS:
           return{...state,loading:false}
    case types.GET_COMPANY_SETTING_INFO_INIT:
           return{...state,companySettingLoader:true,companySettingData:{}}
    case types.GET_COMPANY_SETTING_INFO_SUCCESS:
           return{...state,companySettingLoader:false,companySettingData:action.data}    
    case types.GET_COMPANY_SETTING_INFO_ERROR:
           return{...state,companySettingLoader:false,companySettingData:{}}       
    case types.COMPANY_SETTING_SAVE_INIT:
           return{...state,companySettingLoader:true,commpanySettingUpdateFlag:false}
    case types.COMPANY_SETTING_SAVE_SUCCESS:
           return{...state,companySettingLoader:false,companySettingUpdateFlag:true}       
    case types.COMPANY_SETTING_SAVE_ERROR:
           return{...state,companySettingLoader:false,commpanySettingUpdateFlag:false}
    case types.UPDATE_COMPANY_SETTING_FLAG:
           return{...state,commpanySettingUpdateFlag:false}       
    case types.GET_MASTER_COMPANY_DATA_INIT:
           if(action.dataFor == 'office'){
                return{...state,companyLoader:true,companyMasterData:{...state.companyMasterData,companyMasterDataLoader:action.dataFor, office:{}}}
           }else if(action.dataFor == 'role'){
                return{...state,companyLoader:true,companyMasterData:{...state.companyMasterData,companyMasterDataLoader:action.dataFor, role:{}}}
           }else if(action.dataFor == 'business'){
                return{...state,companyLoader:true,companyMasterData:{...state.companyMasterData,companyMasterDataLoader:action.dataFor, business:{}}}
           }
           return{...state,companyLoader:true,companyMasterData:{...state.companyMasterData,companyMasterDataLoader:action.dataFor, department:{}}}
    case types.GET_MASTER_COMPANY_DATA_SUCCESS:
           if(action.dataFor == 'business'){
              return{...state,companyLoader:false,companyMasterData:{...state.companyMasterData,companyMasterDataLoader:false, business:action.data.business, businessMaster: action.data.is_master, parentId: {...state.companyMasterData.parentId, business:action.parentId}}}
           }else if(action.dataFor == 'role'){
              return{...state,companyLoader:false,companyMasterData:{...state.companyMasterData,companyMasterDataLoader:false, role:action.data.role, roleMaster: action.data.is_master, parentId: {...state.companyMasterData.parentId, role:action.parentId}}}
           }else if(action.dataFor == 'office'){
              return{...state,companyLoader:false,companyMasterData:{...state.companyMasterData,companyMasterDataLoader:false, office:action.data.office, officeMaster: action.data.is_master, parentId: {...state.companyMasterData.parentId, office:action.parentId}}}
           } 
           return{...state,companyLoader:false,companyMasterData:{...state.companyMasterData,companyMasterDataLoader:false, department:action.data.department,departmentMaster: action.data.is_master, parentId: {...state.companyMasterData.parentId, department:action.parentId}}}     
           
    case types.RESET_MASTER_ADD_USER:
           switch(action.data){
              case "role":
                     return{...state,companyMasterData:{...state.companyMasterData,role:{}}}
              case "business":
                     return{...state,companyMasterData:{...state.companyMasterData,role:{},business:{}}}
              case "department":
                     return{...state,companyMasterData:{...state.companyMasterData,role:{},business:{},department:{}}}
              case "office": 
                     return{...state,companyMasterData:{...state.companyMasterData,role:{},business:{},department:{},office:{}}}
           }

    case types.GET_MASTER_COMPANY_DATA_ERROR:
           if(action.dataFor == 'office'){
                return{...state,companyLoader:false,companyMasterData:{...state.companyMasterData,companyMasterDataLoader:false, office:{}}}
           }else if(action.dataFor == 'role'){
                return{...state,companyLoader:false,companyMasterData:{...state.companyMasterData,companyMasterDataLoader:false, role:{}}}
           }else if(action.dataFor == 'business'){
                return{...state,companyLoader:false,companyMasterData:{...state.companyMasterData,companyMasterDataLoader:false, business:{}}}
           }
           return{...state,companyLoader:false,companyMasterData:{...state.companyMasterData,companyMasterDataLoader:false, department:{}}}
                             
    case types.ADD_MASTER_DATA_INIT:
           return{...state,companyMasterData:{...state.companyMasterData,companyDataSaveLoader:true}}
    case types.ADD_MASTER_DATA_SUCCESS:
           return{...state,companyMasterData:{...state.companyMasterData,companyDataSaveLoader:false}}       
    case types.ADD_MASTER_DATA_ERROR:
           return{...state,companyMasterData:{...state.companyMasterData,companyDataSaveLoader:false}}   
    case types.FETCH_REGION_INIT:
           return{...state,companySettingLoader:true}
    case types.FETCH_REGION_SUCCESS:
           return{...state, companyHierarchyData:{...state.companyHierarchyData,regions:action.data}, companySettingLoader:false}       
    case types.COMPANY_SETTING_SAVE_ERROR:
           return{...state,companySettingLoader:false, companyHierarchyData:{...state.companyHierarchyData,regions:{}}}      
    case types.GET_OFFICE_LOCATIONS_INIT:
           return{...state,companyLocationLoader:true}
    case types.GET_OFFICE_LOCATIONS_SUCCESS:
           return{...state,companyLocationLoader:false,officeLocations:action.data}       
    case types.GET_OFFICE_LOCATIONS_ERROR:
           return{...state,companyLocationLoader:false} 
    case types.DELETE_HIERARCHY_ENTITY_INIT:
           return{...state,companySettingLoader:true}
    case types.DELETE_HIERARCHY_ENTITY_SUCCESS:
           return{...state,companySettingLoader:false,companyMasterData:{...state.companyMasterData,deleteEntity:action.deleteFor}}       
    case types.DELETE_HIERARCHY_ENTITY_ERROR:
           return{...state,companySettingLoader:false}     
    case types.DELETE_HIERARCHY_ENTITY_UPDATE_FLAG:
           return{...state,companyMasterData:{...state.companyMasterData,deleteEntity:null}} 
    case types.UPDATE_HIERARCHY_PROPS_INIT:
           return{...state,companySettingLoader:true}       
    case types.UPDATE_HIERARCHY_PROPS:
           if(action.section == 'business'){
              return{...state,companySettingLoader:false,companyMasterData:{...state.companyMasterData, business:action.newProps}} 
           }else if(action.section == 'role'){
              return{...state,companySettingLoader:false,companyMasterData:{...state.companyMasterData, role:action.newProps}} 
           }
              return{...state,companySettingLoader:false,companyMasterData:{...state.companyMasterData, department:action.newProps}} 
    case types.ADD_HASHTAG_CONTENT_INIT:
          return{...state,hashTagContent:{...state.hashTagContent,loading:true,isHashTagSaved:false}}                           
    case types.ADD_HASHTAG_CONTENT_SUCCESS:
          return{...state,hashTagContent:{...state.hashTagContent,loading:false,isHashTagSaved:true}}  
    case types.ADD_HASHTAG_CONTENT_ERROR:
          return{...state,hashTagContent:{...state.hashTagContent,loading:false,isHashTagSaved:false}}  
          
    case types.EDIT_HASHTAG_CONTENT_INIT:
          return{...state,hashTagContent:{...state.hashTagContent,loading:true,isHashTagSaved:false}}                           
    case types.EDIT_HASHTAG_CONTENT_SUCCESS:
          return{...state,hashTagContent:{...state.hashTagContent,loading:false,isHashTagSaved:true}} 
    case types.EDIT_HASHTAG_CONTENT_ERROR:
          return{...state,hashTagContent:{...state.hashTagContent,loading:false,isHashTagSaved:false}} 

    case types.FETCH_HASHTAG_LIST_INIT:
          return{...state,hashTagContent:{...state.hashTagContent,loading:true,hashTagList:[]}}                           
    case types.FETCH_HASHTAG_LIST_SUCCESS:
          return{...state,hashTagContent:{...state.hashTagContent,loading:false,hashTagList:action.tags}} 
    case types.FETCH_HASHTAG_LIST_ERROR:
          return{...state,hashTagContent:{...state.hashTagContent,loading:false,hashTagList:[]}} 

    case types.ADD_SSO_USER_INIT:
          return{...state,addSsoUserOnbaord : {addSsoUserOnbaordLoader: true}}
    case types.ADD_SSO_USER_SUCCESS:
          return{...state,addSsoUserOnbaord : {addSsoUserOnbaordLoader: false}} 
    case types.ADD_SSO_USER_ERROR:
          return{...state,addSsoUserOnbaord : {addSsoUserOnbaordLoader: false}} 
    default:
          return state;
     }
}

export default settingsReducer
