import * as types from '../actions/actionTypes';

const initialState = {
    points: null,
    resetLeaderboardLoader:false
};

let pointReducer = function(state = initialState, action) {
    switch (action.type) {
      case types.FETCH_POINT:
        return { ...state, points: action.points };

      case types.RESET_LEADERBOARD_INIT:
        return { ...state, resetLeaderboardLoader:true };
      case types.RESET_LEADERBOARD_SUCCESS:
        return { ...state, resetLeaderboardLoader:false };
      case types.RESET_LEADERBOARD_ERROR:
        return { ...state, resetLeaderboardLoader:false };
      default: 
        return state;
    }
  };
  
  export default pointReducer;