import * as types from '../actions/actionTypes'

const initialState = {
    overAllNPSDataLoading:false,
    overAllNPSData:{},
    funnelListDataLoading:false,
    funnelListData:{},
    funnelDetailDataLoading: false,
    funnelDetailData:null,
    funnelCommentListLoading: false,
    funnelCommentList:null,
    topFunnelLoading: false,
    topFunnel:{}
};

let NPSAnalyticsReducer = function(state = initialState, action) {
  switch (action.type) {

    case types.GET_OVERALL_NPS_INIT:
        return{...state,overAllNPSDataLoading:true}
    case types.GET_OVERALL_NPS_SUCCESS:
        return{...state,overAllNPSDataLoading:false, overAllNPSData: action.data}
    case types.GET_OVERALL_NPS_ERROR:
        return{...state,overAllNPSDataLoading:false} 

    case types.GET_FUNNEL_LIST_DATA_INIT:
        return{...state,funnelListDataLoading:true}    
    case types.GET_FUNNEL_LIST_DATA_SUCCESS:
        return{...state,funnelListDataLoading:false, funnelListData: action.data}
    case types.GET_FUNNEL_LIST_DATA_ERROR:
        return{...state,funnelListDataLoading:false} 

     case types.GET_FUNNEL_DETAIL_DATA_INIT:
        return{...state,funnelDetailDataLoading:true}    
    case types.GET_FUNNEL_DETAIL_DATA_SUCCESS:
        return{...state,funnelDetailDataLoading:false, funnelDetailData: action.data}
    case types.GET_FUNNEL_DETAIL_DATA_ERROR:
        return{...state,funnelDetailDataLoading:false}

    case types.GET_FUNNEL_COMMENT_LIST_INIT:
        return{...state,funnelCommentListLoading:true}    
    case types.GET_FUNNEL_COMMENT_LIST_SUCCESS:
        return{...state,funnelCommentListLoading:false, funnelCommentList: action.data}
    case types.GET_FUNNEL_COMMENT_LIST_ERROR:
        return{...state,funnelCommentListLoading:false}  

    case types.GET_NPS_TOP_FUNNEL_INIT:
        return{...state,topFunnelLoading:true}
    case types.GET_NPS_TOP_FUNNEL_SUCCESS:
        return{...state, topFunnelLoading:false, topFunnel:action.data}
    case types.GET_NPS_TOP_FUNNEL_ERROR:
        return{...state, topFunnelLoading:false}    
        
   default:
      return state;
  }
}
export default NPSAnalyticsReducer
