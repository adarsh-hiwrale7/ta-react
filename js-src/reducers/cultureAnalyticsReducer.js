import * as types from '../actions/actionTypes'

const initialState = {
    indexLoading:false,
    surveyComparisonLoading:false,
    surveyComparison:{},
    cultureIndex:{},
    participationLoading: false,
    participationData:{},
    taggedValueLoading: false,
    taggedValueData:{},
    votedValueLoading: false,
    votedValueData:{},
    levelData:{},
    engagementDataLoading:false,
    engagementData:{},
    allDriversData:{},
    allDriversDataLoading:false,
    cultureParticipationLoading : false,
    cultureParticipation:{}, 
    genderData:{},
    visiblyRoleData:{},
    heatmapLoader: false,
    saveHeatMap: false,
    segmentLoader: false,
    segmentData: {},
    perticularSegment:{},
    heatmapData:{}
};

let cultureAnalyticsReducer = function(state = initialState, action) {
  switch (action.type) {
    case types.GET_CULTURE_INDEX_INIT:
        return{...state,indexLoading:true}
    case types.GET_CULTURE_INDEX_SUCCESS:
        return{...state,indexLoading:false, cultureIndex: action.data}
    case types.GET_CULTURE_INDEX_ERROR:
        return{...state,indexLoading:false}   
    case types.GET_PARTICIPATION_INIT:
        return{...state,participationLoading:true}    
    case types.GET_PARTICIPATION_SUCCESS:
        return{...state,participationLoading:false, participationData: action.data}
    case types.GET_PARTICIPATION_ERROR:
        return{...state,participationLoading:false} 
     case types.GET_TAGGED_VALUE_INIT:
        return{...state,taggedValueLoading:true}    
    case types.GET_TAGGED_VALUE_SUCCESS:
        return{...state,taggedValueLoading:false, taggedValueData: action.data}
    case types.GET_TAGGED_VALUE_ERROR:
        return{...state,taggedValueLoading:false}
    case types.GET_VOTED_VALUE_INIT:
        return{...state,votedValueLoading:true}    
    case types.GET_VOTED_VALUE_SUCCESS:
        return{...state,votedValueLoading:false, votedValueData: action.data}
    case types.GET_VOTED_VALUE_ERROR:
        return{...state,votedValueLoading:false}
    case types.GET_LEVEL_DATA_INIT:
        return{...state,heatmapLoader:true}
    case types.GET_LEVEL_DATA_SUCCESS:
        return{...state,heatmapLoader:false, levelData: action.data}   
    case types.GET_GENDER_DATA_INIT:
        return{...state,heatmapLoader:true}
    case types.GET_GENDER_DATA_SUCCESS:
        return{...state,heatmapLoader:false, genderData: action.data}
    case types.GET_VISIBLY_ROLE_DATA_INIT:
        return{...state,heatmapLoader:true}
    case types.GET_VISIBLY_ROLE_DATA_SUCCESS:
        return{...state,heatmapLoader:false, visiblyRoleData: action.data}                                
    case types.GET_ENGAGEMENTDATA_INIT:
        return{...state,engagementDataLoading:true}
    case types.GET_ENGAGEMENTDATA_SUCCESS:
        return{...state,engagementDataLoading:false,engagementData:action.data}
    
    case types.GET_ENGAGEMENTDATA_ERROR:
        return{...state,engagementDataLoading:false,engagementData:{}}

    case types.GET_DRIVERSDATA_SUCCESS:
        return{
            ...state,
            allDriversDataLoading:false,
            allDriversData:action.data
        }
    case types.GET_DRIVERSDATA_INIT:
        return{...state,allDriversDataLoading:true}   
    case types.GET_DRIVERSDATA_ERROR:
        return{...state,allDriversDataLoading:false,allDriversData:{}}

    // surveyComparison
    case types.GET_SURVEY_COMPARISON_INIT:
        return{...state,surveyComparisonLoading:true}   
    case types.GET_SURVEY_COMPARISON_SUCCESS:
        return{...state,surveyComparisonLoading:false,surveyComparison:action.data}
    case types.GET_SURVEY_COMPARISON_ERROR:
        return{...state,allDriversDataLoading:false,surveyComparison:{}}
        
    // culture particiaption 
    case types.GET_CULTURE_PARTICIPATION_INIT:
        return{...state,cultureParticipationLoading:true}
    case types.GET_CULTURE_PARTICIPATION_SUCCESS:
        return{...state,cultureParticipation:action.data,cultureParticipationLoading:false}
    case types.GET_CULTURE_PARTICIPATION_ERROR:
        return{...state,cultureParticipationLoading:false, cultureParticipation:{}}

    case types.SAVE_SEGMENTATION_INIT:
        return{...state,heatmapLoader:true}    
    case types.SAVE_SEGMENTATION_SUCCESS:
        return{...state,heatmapLoader:false, saveHeatMap: true}
    case types.SAVE_SEGMENTATION_ERROR:
        return{...state,heatmapLoader:false}
    case types.RESET_SAVE_FLAG:
        return{...state,saveHeatMap:false}  
    case types.FETCH_SEGMENTATION_INIT:
        return{...state,segmentLoader:true}    
    case types.FETCH_SEGMENTATION_SUCCESS:
        return{...state,segmentLoader:false, segmentData: action.data}
    case types.FETCH_SEGMENTATION_ERROR:
        return{...state,segmentLoader:false, segmentData:{}}   
    case types.FETCH_PERTICULAR_SEGMENTATION_INIT:
        return{...state,segmentLoader:true,  perticularSegment:{}}    
    case types.FETCH_PERTICULAR_SEGMENTATION_SUCCESS:
        return{...state,segmentLoader:false, perticularSegment: action.data}
    case types.FETCH_PERTICULAR_SEGMENTATION_ERROR:
        return{...state,segmentLoader:false, perticularSegment:{}}      
    case types.FETCH_HEATMAP_DATA_INIT:
        return{...state,heatmapLoader:true}    
    case types.FETCH_HEATMAP_DATA_SUCCESS:
        return{...state,heatmapLoader:false, heatmapData: action.data}
    case types.FETCH_HEATMAP_DATA_ERROR:
        return{...state,heatmapLoader:false, heatmapData:{}}    
    case types.DELETE_SEGMENTATION_DATA_INIT:
        return{...state,heatmapLoader:true}    
    case types.DELETE_SEGMENTATION_DATA_SUCCESS:
        return{...state,heatmapLoader:false}                       
        
   default:
      return state;
  }
}
export default cultureAnalyticsReducer
