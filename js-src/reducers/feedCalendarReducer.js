import * as types from '../actions/actionTypes'

const initialState = {
  feedPosts:[]
};

let feedCalendarReducer = function(state = initialState, action) {
  
  switch (action.type) {
      case types.FETCH_EXTERNAL_FEED_CALENDAR_POSTS:
      return { ...state, feedPosts: action.feedCalendarPosts };
      case types.FETCH_INTERNAL_FEED_CALENDAR_POSTS:
      return { ...state, feedPosts: action.feedCalendarPosts };
      case types.FETCH_FEED_CALENDAR_POSTS:
      return { ...state, feedPosts: action.feedCalendarPosts };

    default:
      return state;


  }
}

export default feedCalendarReducer
