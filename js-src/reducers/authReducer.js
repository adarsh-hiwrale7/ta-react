import * as types from '../actions/actionTypes'

const initialState = {
  userinfo: {},
  authenticated: false,
  auth_token: null,
  api_secret: null,
  passwordSet:false,
  passwordError:false,
  passwordReset:false,
  loading: false,
  forgotpasswordReset:false,
  setnewpasswordReset:false,
  initloading: false,
  isOnboardingDone:true,
  domainList:[],
  LoginLoading:false,
  guestData:false,
  isLoginDone:false,
  userEmailPassword:{},
  feed_identity:null,
  category_identity:null,
  isLogoutClicked:false
};

let authReducer = function(state = initialState, action) {
  switch (action.type) {
    case types.VERIFY_USER_SUCCESS:
      return { ...state, authenticated: true, auth_token: action.payload.auth_token, api_secret: action.payload.api_secret };
    case types.ADD_PASS_SUCCESS:
      return { ...state, passwordSet:true};
    case types.ADD_PASS_ERROR:
      return { ...state, passwordError:true};
    case types.AUTH_USER_INITIATE:
      return { ...state, loading: true }
    case types.STORE_STEP_NUMBER:
      return {...state,isOnboardingDone:false}

    case types.AUTH_USER_SUCCESS:
      return { ...state, authenticated: true,session_id:action.payload.session_id, userinfo: action.payload.userinfo, loading: false };
    case types.AUTH_USER_ERROR:
      return { ...state, authenticated: false, auth_token: null, api_secret: null, loading: false };
    case types.LOGOUT_INIT:
      return {...state,isLogoutClicked:true}
    case types.LOGOUT_ERROR:
      return {...state,isLogoutClicked:false}
    case types.AUTH_LOGOUT:
      return { ...state, authenticated:false, auth_token: null, api_secret: null,isLogoutClicked:true}; 
    case types.ADD_PASS_SAVING:
      return {...state,loading:true}   
    case types.RESET_PASS_SUCCESS:
      return { ...state, passwordReset:true,loading:false};
    case types.RESET_PASS_ERROR:
      return { ...state, passwordReset:false};
      //forgot password
    case types.INITIAL_LOADING_FORGOT:
      return {...state,initloading:true}
    case types.RESET_FORGOT_PASS_SUCCESS:
      return { ...state, forgotpasswordReset:true,initloading:false};
    case types.RESET_FORGOT_PASS_ERROR:
      return { ...state, forgotpasswordReset:false,initloading:false};
    case types.SET_NEW_PASS_SUCCESS:
      return { ...state, setnewpasswordReset:true,initloading:false};
    case types.SET_NEW_PASS_ERROR:
      return { ...state, setnewpasswordReset:false,initloading:false};

    case types.LOGIN_SUCCESS:
      return{...state,domainList:action.data,LoginLoading:false,isLoginDone:true,userEmailPassword:action.userEmailPassword,isLogoutClicked:false}

    case types.LOGIN_ERROR:
      return{...state,domainList:[],LoginLoading:false,isLoginDone:false}

    case types.LOGIN_INIT:
      return {...state,LoginLoading:true,domainList:[],isLoginDone:false}
    
    case types.GUEST_USER_DATA:
      return {...state,guestData:true,feed_identity:action.payload.feed_identity,category_identity:action.payload.category_identity}
    case types.SSO_LOGIN_INIT:
      return {...state,LoginLoading:true}
    case types.SSO_LOGIN_SUCCESS:
      return {...state,LoginLoading:false} 
    case types.SSO_LOGIN_FAIL:
      return {...state,LoginLoading:false}   
    default: 
      return state;
  }
}

export default authReducer