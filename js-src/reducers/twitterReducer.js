import * as types from '../actions/actionTypes'

const initialState = {
  tweet: {
  	tweetData:[],
  	loading: false,
  },
};

let tweeterReducer = function(state = initialState, action) {
  switch (action.type) {
    case types.FETCH_TWITTER:
      return { ...state, tweet: {tweetData:  [], loading: true} };
      
    case types.FETCH_TWITTER_SUCCESS:
      return { ...state, tweet: {tweetData:  action.tweetData.data, loading: false} };


    default: 
      return state;
  }
}

export default tweeterReducer