import * as types from '../actions/actionTypes'


const initialState = {
    profile: {
      name: null,
      first_name: null,
      last_name: null,
      identity: null,
      email: null,
      avatar: 'https://s3-us-west-2.amazonaws.com/talent-advocate/default-avatar.png',
      department: null,
      city:null,
      country:null,
      state:null,
      position: null,
      loading: false,
      isAdmin: true,
      user_tag:[],
      isProfileUpdated:null,
     // profileAvatar:{}
     profileImageLoading:false,
     profileLoader:false,
     enable_social_posting:null,
     profile_update : true,
    },

};

let profileReducer = function(state = initialState, action) {
  switch (action.type) {
    // case types.FETCH_PROFILE_PHOTO:
    //   return { ...state, profileAvatar: action.profileAvatar };
    case types.PROFILE_INIT:
      return { ...state, profileLoader:true}
    case types.PROFILE_SAVING:
      if(action.flag==1){
        return { ...state, profile: {...state.profile,loading: false, profileImageLoading:true} };
      }
      else{
        return { ...state, profile: {...state.profile,loading: true},isProfileUpdated:false };
      }

    case types.PROFILE_SUCCESS:
      return { ...state,profileLoader:false, profile: { ...state.profile,
        first_name: action.profile.firstname,
        last_name: action.profile.lastname,
        email: action.profile.email,
        default_feed: action.profile.default_feed,
        feed: action.profile.feed,
        department: typeof(action.profile.Department)!=="undefined" ? action.profile.Department.data : "",
        BusinessUnit: typeof(action.profile.BusinessUnit)!=="undefined" ? action.profile.BusinessUnit.data : "",
        CompanyRole: typeof(action.profile.CompanyRole)!=="undefined" ? action.profile.CompanyRole.data : "",
        office: typeof(action.profile.office)!=="undefined" ? action.profile.office.data : "",
        identity: action.profile.identity,
        position: action.profile.user_position,
        avatar: action.profile.avatar,
        city:action.profile.city,
        country:action.profile.country,
        state:action.profile.state,
        domain_name:action.profile.domain_name,
        loading: false,
        user_tag:action.profile.tags.data,
        sharelink: action.profile.sharelink,
        onboard_step:action.profile.onboard_step,
        role:action.profile.role,
        gender:action.profile.gender,
        birth_date:action.profile.birth_date,
        mobile_country_code: action.profile.mobile_country_code,
        board:action.profile.board,
        mobile_no: action.profile.mobile_no,
        office_location:action.profile.office_location,
        profileImageLoading:false,
        profile_update:action.profile.profile_update,
        cometchat_app_id:action.profile.cometchat_app_id,
        cometchat_app_key:action.profile.cometchat_app_key,
        cometchat_app_region:action.profile.cometchat_app_region,
        enable_social_posting:action.profile.enable_social_posting,
        sso_onboarding: action.profile.sso_onboarding
        }, isProfileUpdated:true
      };


    default:
      return state;
  }
}

export default profileReducer
