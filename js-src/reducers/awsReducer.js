import * as types from '../actions/actionTypes'

const initialState = { 
	uploadProgress: [],
	uploadStarted: false,
	uploadDone: false,
	uploadedItems: [],
	isUploadedItemSave: false,
	fileTemp:null
};

let awsReducer = function (state = initialState, action) {
	switch (action.type) {
		case types.UPLOAD_RESET:
			return { ...state, uploadProgress: [], uploadStarted: false, uploadDone: false, uploadedItems: [], isUploadedItemSave:false, uploadedItemsBuffer: [], fileTemp:null }
			
		case types.UPLOAD_PROGRESS:
			let progress = {
				file: action.file,
				percent: action.percent
			}
			state.uploadProgress[action.index] = progress;
			let newUploadProgress = state.uploadProgress.slice();
			return { ...state, uploadProgress: newUploadProgress }
		case types.UPLOAD_DONE:
			return { ...state, uploadDone: true, uploadStarted: false }
		case types.UPLOAD_START:
			return { ...state, uploadDone: false, uploadStarted: true }
		case types.UPLOAD_ITEM_SAVE:
			return { ...state, uploadedItems: [...state.uploadedItems, action.item], isUploadedItemSave: true }
		case types.STORE_FILE_DATA_TEMP:
			return { ...state, fileTemp: action.data}	
			
		default:
			return state;
	}
}

export default awsReducer
