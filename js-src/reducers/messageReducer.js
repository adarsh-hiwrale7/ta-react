import * as types from '../actions/actionTypes'

let messageReducer = function(state = null, action) {
    const filter=null;
    if (filter !== action.filter) {
      return state;
    }
    switch (action.type) {
      case 'FETCH_TODOS_FAILURE':
        return action.message;
      case 'FETCH_TODOS_REQUEST':
      case 'FETCH_TODOS_SUCCESS':
        return null;
      default:
        return state;
    }
}

export default messageReducer
