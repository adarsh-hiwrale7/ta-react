import * as types from '../actions/actionTypes';

const initialState = {
    searchData:{},
    suggestion:{},
    searchInput:"",
    flag_selected:false,
    loading:false,
    auto_suggest:false,
    flag_suggested:false,
    flag_see_all:false
};

let searchReducer = function(state = initialState, action) {
  switch (action.type) {

    case types.SEARCH_START : 
      return { ...state , loading : true}
    
    case types.SEE_ALL_CLICK:
      return{...state , flag_see_all:true}
    
    case types.RESET_SEE_ALL_CLICK:
      return{...state , flag_see_all:false}
    
    case types.SEARCH_SUCCESS : 
      return {...state , loading : false}

    case types.GLOBAL_SEARCH_START : 
      return {...state , loading : true,inSearchSuggestFlag:action.inSearchSuggestFlag} 

    case types.IS_CLICK_ON_SUGGESTION:
      return{...state,flag_suggested:true}

    case types.RESET_SUGGESTION_FLAG:
      return{...state,flag_suggested:false}

    case types.RESET_FLAG : 
        return{...state , flag_selected:false}

    case types.GLOBAL_SEARCH_SUCCESS:
      return { ...state, searchData : action.payload , searchInput:action.searchInput , suggestion:action.suggestion , flag_selected:action.flag_selected, loading:false,auto_suggest:action.auto_suggest,inSearchSuggestFlag:action.inSearchSuggestFlag}

    case types.GLOBAL_SEARCH_SUCCESS_LOADER:
      return {...state , loading : false,inSearchSuggestFlag:action.inSearchSuggestFlag} 

    case types.GLOBAL_SEARCH_ERROR:
      return { ...state, SearchError : action.payload };
    
    case types.RESET_INPUT_SEARCH_TEXT:
      return{...state,searchInput :'',searchData:{},suggestion:{}};

          
    default:
      return state;
  }
}

export default searchReducer
