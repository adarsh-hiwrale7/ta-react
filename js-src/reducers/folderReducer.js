import * as types from '../actions/actionTypes'

const initialState = {
  popupFolder: []
};

let folderReducer = function(state = initialState, action) {
  switch (action.type) {
    case types.FETCH_FOLDER_POPUP:
        return {
            ...state,
            popupFolder: action.popupFolder
        };                        
    default:
      return state;
  }
}

export default folderReducer
