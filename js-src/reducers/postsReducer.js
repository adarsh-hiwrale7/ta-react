import * as types from '../actions/actionTypes'

const initialState = {
  creation: {
    created: false,
    loading: false,
    buttondisabled:false,
    campaignId:"",
    disblePopup: false
  },
  updation: {
    updated: false,
    loading: false,
  },
  driveAssetsURL:null,
  uploadedDriveFile:undefined,
  driveAssetsUploading:false,
  newpostdata:false,
  campaignCreatePostdata:{},
  calendar_success:false,
  isUploadingStart:false,
  utmData :{},
  utmLinkLoading:false,
  aiValue : null,
  aiValueLoading:false,
  aiValueError:null,
  utmlinkFailed:false,
  editCurationData : null
};

let postsReducer = function(state = initialState, action) {
  switch (action.type) {

    /* Post creation starts*/

    case types.POST_CREATING:
      return { ...state, creation: { loading: true, created: false,buttondisabled:true, disblePopup: false } };

    case types.ASSETS_IN_POST_UPLOADING_START:
      return {...state,isUploadingStart:true}

    case types.UPLOADING_COMPLETE:
      return{...state,isUploadingStart:false}

      // POST_CREATING_NEW first time call thse jyre popup load thse
    case types.POST_CREATING_NEW:
      return { ...state, creation: { loading: false, created: false,buttondisabled:false,disblePopup: false } };

    case types.POST_CREATION_SUCCESS:
        var campaignId = "";
          if(action.data.hasOwnProperty('campaign_identity')) {
            campaignId = action.data.campaign_identity;
          }
      return { ...state, creation: { loading: false, created: true ,buttondisabled:true,  campaignId: campaignId, disblePopup: true}};

    case types.POST_CREATION_ERROR:
      return { ...state, creation: { loading: false, created: false,buttondisabled:false,disblePopup: true } };

    case types.ERROR_POST_CREATE_INIT:
      return { ...state, creation: { loading: false, created: false,buttondisabled:false,disblePopup: false} };

    /* Post creation ends*/

    case types.UTM_LINK_SUCCESS: 
          return{...state, utmData : action.data ,utmLinkLoading:false , utmlinkFailed:false}
    
    case types.UTM_LINK_FAILED:
          return{...state ,utmData:{},utmLinkLoading :false , utmlinkFailed:true}

    case types.GENERATELINK_INIT:
          return{...state ,utmLinkLoading:true}
    //get Ai value in post create
    case types.AI_VALUE_SUCCESS: 
          return{...state, aiValue : action.data ,aiValueLoading:false,aiValueError:null}
    
    case types.AI_VALUE_FAILED:
          return{...state,aiValueLoading :false,aiValueError:action.error,aiValue:null}

    case types.AI_VALUE_INIT:
          return{...state ,aiValueLoading:true}


    //  Upload drive file to s3 

    case types.UPLOAD_DRIVE_ASSET_INIT:
      return { ...state, driveAssetsUploading:true };
          
    case types.UPLOAD_DRIVE_ASSET_SUCCESS:
      return { ...state, driveAssetsURL:action.assetS3URL ,uploadedDriveFile:action.uploadedDriveFile, driveAssetsUploading :false };

    case types.UPLOAD_DRIVE_ASSET_ERROR:
      return { ...state,  driveAssetError:true, driveAssetsUploading:false };

    case types.UPLOAD_DRIVE_ASSET_DONE:
      return { ...state, driveAssetsUploading:false ,driveAssetsURL:null};

    case types.DRIVE_ASSET_ABORT:
          return{...state , driveAssetsUploading:false,driveAssetsURL:null}
    /* Post updation starts*/
      
    case types.POST_UPDATING:
      return { ...state, updation: { loading: true, updated:false } };
      // POST_CREATING_NEW first time call thse jyre popup load thse
    case types.RESET_CALENDAR_SUCCESS_FLAG:
      return {...state,calendar_success:false}; 

    case types.POST_UPDATE_INIT:
          return{...state,updation:{loading:true,updated: false}}

    case types.POST_UPDATION_SUCCESS:
      return { ...state, updation: { loading: false, updated:true,feedEvents:action.data},calendar_success:true };
      
    case types.POST_UPDATION_ERROR:
      return { ...state, updation: { loading: false, updated: false},calendar_success:false };

    /* Post updation ends*/

    case types.HIDE_POST_SUCCESS:
      return{...state,newpostdata:true}

    case types.CAMPAIGN_CREATE_POST_DATA:
      return{...state,campaignCreatePostdata:action.data}

    case types.REMOVE_CAMPAIGN_CREATE_POST_DATA:
      return{...state,campaignCreatePostdata:{}}
    
    case types.EDIT_CUREATION_INIT:
      return{...state , editCurationData : action.editCurationData}
    
    case types.RESET_CURATION_EDIT:
      return{...state, editCurationData:null}
    default:
      return state;
  }
}

export default postsReducer
