import * as types from '../actions/actionTypes'

const initialState = {
  todos: {
  	todosList: [],
  	loading: false,
  },
};

let todosReducer = function(state = initialState, action) {
  switch (action.type) {
    case types.FETCH_TODOS:
      return { ...state, todos: {todosList:  [], loading: true} };
      
    case types.FETCH_TODOS_SUCCESS:
      return { ...state, todos: {todosList:  action.todos, loading: false} };


    default: 
      return state;
  }
}

export default todosReducer