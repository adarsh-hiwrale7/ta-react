import firebase from "firebase/app";
import "firebase/messaging";
import { CometChat } from "@cometchat-pro/chat";

let meUid = "";

export function initializeFirebase(cometchatId, userId) {
  console.log(" firebase==-", firebase.apps, firebase.apps.length);

  if (firebase.messaging.isSupported()) {
    console.log("inside firebase==-");

    const config = {
      apiKey: "AIzaSyBvP0Wjh6Cv3F9NOA6QCl8lh_oBdpZ8ZOA",
      authDomain: "visibly-2fb5f.firebaseapp.com",
      databaseURL: "https://visibly-2fb5f.firebaseio.com",
      projectId: "visibly-2fb5f",
      storageBucket: "visibly-2fb5f.appspot.com",
      messagingSenderId: "242104348586",
      appId: "1:242104348586:web:f0fa39b7555c5b750145d3",
    };
    if (!firebase.apps.length) {
      firebase.initializeApp(config);
    }

    const messaging = firebase.messaging();
    messaging
      .requestPermission()
      .then(() => {
        console.log("Have Permission", messaging.getToken());
        return messaging.getToken();
      })
      .then((token) => {
        console.log("FCM Token:", token);
        CometChat.getAppSettings().then((settings) => {
          var appToken;
          console.log(settings.extensions, "ext==--");
          settings.extensions.forEach((ext) => {
            if (ext.id == "push-notification") {
              appToken = ext.appToken;
            }
          });
          var userType = "user";
          var UID = userId;
          var appId = cometchatId;
          var region = "us";
          var topic = appId + "_" + userType + "_" + UID;
          var url =
            "https://push-notification-" +
            region +
            ".cometchat.io/v1/subscribe?appToken=" +
            appToken +
            "";
          fetch(url, {
            method: "POST",
            headers: new Headers({
              "Content-Type": "application/json",
            }),
            body: JSON.stringify({
              appId: appId,
              fcmToken: token,
              topic: topic,
            }),
          })
            .then((response) => {
              if (response.status < 200 || response.status >= 400) {
                console.log(
                  "Error subscribing to topic: " +
                    response.status +
                    " - " +
                    response.text()
                );
              }
              console.log('Subscribed to "' + topic + '"');
            })
            .catch((error) => {
              console.error(error);
            });
        });
      })
      .catch((error) => {
        console.log(error, "error1222==-");
        if (error.code === "messaging/permission-blocked") {
          console.log("Please Unblock Notification Request Manually");
        } else {
          console.log("Error Occurred123", error);
        }
      });

    messaging.onMessage(function (payload) {
      var sender = JSON.parse(payload.data.message);
      console.log("Receiving foreground message", sender, payload);
      // Customize notification here
      if (sender.data.entities.sender.entity.uid !== meUid) {
        console.log(
          sender.data.entities.sender.entity.uid,
          meUid,
          "event123notification==-"
        );

        var notificationTitle = "New CometChat message";
        var notificationOptions = {
          body: payload.data.alert,
          icon: sender.data.entities.sender.entity.avatar,
        };

        var notification = new Notification(
          notificationTitle,
          notificationOptions
        );
        notification.onclick = function (event) {
          console.log(event, "event==-");
          //handle click event onClick on Web Push Notification
          notification.close();
        };
      }
    });
  }
}

export function updateFirebaseLoggedInUser(uid) {
  if (firebase.messaging.isSupported()) {
    meUid = uid;
  }
}
