
// use hashHistory if you want older browsers support
// import ResetPassword from "../components/Settings/ResetPassword/ResetPassword";
// import EventIndex from "../components/Event/EventIndex";
import loadable from 'react-loadable';
import { browserHistory, IndexRoute, Route } from 'react-router';
import Authentication from '../components/Auth/Authentication';
//chat
import Chat from '../components/Chat/Chat';
import {NotificationContainer} from 'react-notifications';
// import RegisterStep3 from "../components/Register/Step3";
import VisiblyTour from "../components/VisiblyTour";
import ConnectFB from "../components/FB/ConnectFB";
//import Curated from "../components/Feed/Curated";
import Jobs from "../components/Feed/CuratedPages/JobsPages/Jobs";
import Rss from "../components/Feed/CuratedPages/RssPages/Rss";
import Feed from "../components/Feed/Feed";
// import Calendar from "../components/Feed/Calendar";
import PostNotification from "../components/Feed/PostNotification";
import Four0Four from "../components/Four0Four";
import LayoutBasic from "../components/Layout/LayoutBasic";
// import App from "../components/App";
import LayoutMain from "../components/Layout/LayoutMain";
import LayoutPlain from "../components/Layout/LayoutPlain";
import Login from "../components/Login/Login";
// import ForgotPassword from "../components/Login/ForgotPassword";
import PasswordReset from "../components/Login/PasswordReset";
import Resetpassword from "../components/Login/Resetpassword";
import SsoRedirection from "../components/Login/SsoRedirection";
import Verify from "../components/Login/Verify";
import GuestSignUp from '../components/Register/GuestUser/GuestStep1';
import SignUp from "../components/Register/SignUp";
import RegisterStep1 from "../components/Register/Step1";
import RegisterStep2 from "../components/Register/Step2";
import ThankYouPage from '../components/Register/ThankYouPage';
import GlobalSearch from "../components/Search/GlobalSearch";
import RegisterSsoUser from '../components/Register/SsoUser/RegisterSsoUser'
// import AssetHash from "../components/Assets/AssetHash";
import SlackRedirection from '../components/SlackRedirection';

// import DepartmentSettings from "../components/Settings/Department/DepartmentSettings";
const AsyncSettingsComponents =loadable.Map({
	loader: {
		SocialSettings: () => import( '../components/Settings/Social/SocialSettings' ),
		ProfileSettings: () => import("../components/Settings/Account/SettingsProfile"),
		CompanySettings : () => import("../components/Settings/Company/CompanyIndex"),
		ContentSettings : () => import("../components/Settings/Content/ContentSettings"),
		ValueSettings : () => import("../components/Settings/Culture/ValueSettings"),
		IntegrationIndex : () => import("../components/Settings/Integration/IntegrationIndex"),
		WidgetSettings : () => import("../components/Settings/Widget/WidgetSettings"),
		UsersSettings : () => import("../components/Settings/User/UsersSettings"),
		Points : () => import("../components/Settings/Points/PointsSettings"),
		BillingSettings : () => import("../components/Settings/billing/BillingSettings"),
		InVoice : () => import("../components/Settings/billing/InVoice"),
		PaymentMethod : () => import("../components/Settings/billing/PaymentMethod"),
		SettingContact : () => import("../components/Settings/billing/SettingContact"),
		TeamChange : () => import("../components/Settings/billing/TeamChange"),
		MapWidgetSettings : () => import("../components/Settings/Widget/MapWidgetSettings"),
		SurveyWidgetSettings : () => import("../components/Settings/Widget/surveyWidget/surveyWidgetContainer"),
	},
	loading:LoadingRoutes,
	delay: 300, // 0.3 seconds
	timeout: 10000,
	render(loaded, props) {
		let Component = '';
		const currPath = props.location.pathname
		switch (currPath) {
			case '/settings/content':
				Component = loaded.ContentSettings.default
				break;
			case '/settings/company':
				Component = loaded.CompanySettings
				break;
			case '/settings/social':
				Component = loaded.SocialSettings
				break;
			case '/settings/culture':
				Component = loaded.ValueSettings
				break;
			case '/settings/integration':
				Component = loaded.IntegrationIndex
				break;
			case '/settings/widgets':
				Component = loaded.WidgetSettings
				break;
			case '/settings/users':
				Component = loaded.UsersSettings
				break;
			case '/settings/Points':
				Component = loaded.Points
				break;
			case '/settings/billing/':
				Component = loaded.BillingSettings.default
				break;
			case '/settings/billing/invoice':
				Component = loaded.InVoice.default
				break;
			case '/settings/billing/settingcontact':
				Component = loaded.PaymentMethod.default
				break;
			case '/settings/billing/paymentmethod':
				Component = loaded.SettingContact.default
				break;
			case '/settings/billing/teamchange':
				Component = loaded.TeamChange.default
				break;
			case '/settings/widgets/map':
				Component = loaded.MapWidgetSettings
				break;
			case '/settings/widgets/survey':
				Component = loaded.SurveyWidgetSettings
				break;
			default:
				Component = loaded.ProfileSettings
				break;
		}
	  return <Component {...props}/>;
	},
  });
const AsyncAnalyticsComponents =loadable.Map({
	loader: {
		CultureIndex : () => import("../components/Analytics/CultureScore/CultureIndex"),
		ActivityIndex : () => import("../components/Analytics/AnalyticsDashboard/Activity/ActivityIndex"),
		EngagementIndex : () => import("../components/Analytics/AnalyticsDashboard/Engagement/EngagementIndex"),
		ValueScoreIndex : () => import("../components/Analytics/ValueScore/ValueScoreIndex"),
		NPSAnalytics : () => import("../components/Analytics/NPSAnalytics/NPSAnalyticsIndex"),
		ComparisonIndex : () => import("../components/Analytics/ComparisonAnalytics/ComparisonIndex"),
		AnalyticsIndex : () => import("../components/Analytics/AnalyticsDashboard/AnalyticsDashboardIndex"),
	},
	loading:LoadingRoutes,
	delay: 100, // 0.3 seconds
	timeout: 10000,
	render(loaded, props) {
		let Component = '';
		const currPath = props.location.pathname
		switch (currPath) {
			case '/analytics/culture':
				Component = loaded.ValueScoreIndex
				break;
			case '/analytics/activity':
				Component = loaded.ActivityIndex
				break;
			case '/analytics/engagement':
				Component = loaded.EngagementIndex
				break;
			case '/analytics/values':
				Component = loaded.CultureIndex
				break;
			case '/analytics/NPS':
				Component = loaded.NPSAnalytics
				break;
			case '/analytics/resonance':
				Component = loaded.ComparisonIndex
				break;
			case '/analytics/adoption':
					Component = loaded.AnalyticsIndex
					break;
			default:
				Component = loaded.AnalyticsIndex
				break;
		}
	  return <Component {...props}/>;
	},
  });
const AsyncLeaderboardComponents =loadable({
	loader: () => import("../components/Leaderboard/Index"),
	loading:LoadingRoutes,
	delay: 100, // 0.1 seconds
	timeout: 10000,
  });
const AsyncSSoSignUpComponents =loadable({
	loader: () => import("../components/Register/SSOSignUp"),
	loading:LoadingRoutes,
	delay: 100, // 0.1 seconds
	timeout: 10000,
  });
const AsyncModerationComponents =loadable({
	loader: () => import("../components/Moderation/Index"),
	loading:LoadingRoutes,
	delay: 100, // 0.1 seconds
	timeout: 10000,
  });
const AsyncCampaignComponents =loadable({
	loader: () => import("../components/Campaigns/Index"),
	loading:LoadingRoutes,
	delay: 100, // 0.1 seconds
	timeout: 10000,
  });
const AsyncAssetsComponents =loadable({
	loader: () => import("../components/Assets/Index"),
	loading:LoadingRoutes,
	delay: 100, // 0.1 seconds
	timeout: 10000,
  });
const AsyncDashboardComponents =loadable({
	loader: () => import("../components/Dashboard/Dashboard"),
	loading:LoadingRoutes,
	delay: 100, // 0.1 seconds
	timeout: 10000,
  });
// const AsyncVisiblyTourComponents =loadable({
// 	loader: () => import("../components/VisiblyTour"),
// 	loading:LoadingRoutes,
// 	delay: 100, // 0.1 seconds
// 	timeout: 10000,
//   });
export const Moderator = ['moderator', 'super-admin', 'admin','guest'];
export const Employee = ['employee', 'moderator', 'super-admin', 'admin','guest'];
export const Admin = ['super-admin', 'admin','guest'];
export const SuperAdmin = ['guest','super-admin'];
var url = null;
function LoadingRoutes(props) {
	if (props.error) {
	  return <div><label>Error!</label> <button className ="btn btn-theme" onClick={ props.retry }>Retry</button></div>;
	} else if (props.timedOut) {
	  return <div><label>Taking a long time...</label> <button className ="btn btn-theme" onClick={ props.retry }>Retry</button></div>;
	} else if (props.pastDelay || props.isLoading) {
	  return <div className={`preloaderWrap`}>
					<div className="visibly-loader">
						<img src="/img/visibly-loader.gif" alt="Loader"/>
					</div>
				</div>;
	} else {
	  return null;
	}
  }
export default (
	/* when using nested routes, the parent has to have this.props.children written in its component. In this example, App component will have this.props.children in main content area */
	<Route path="/" >

		<Route component={LayoutPlain}>
			<IndexRoute component={Login}></IndexRoute>
			<Route path="login" component={Login}></Route>
			<Route path="/Reset-password" name="Resetpassword" component={Resetpassword}></Route>
			<Route path="/sign-up" name="SignUp" component={SignUp}></Route>
			<Route path="/sso-sign-up" name="SsoSignUp" component={AsyncSSoSignUpComponents}></Route>
			<Route path="/thankyou" name="Thankyou" component={ThankYouPage}></Route>
			<Route path='/tour' name='VisiblyTour' component={Authentication(VisiblyTour, Employee)} ></Route>
			<Route path='/slack-redirect' name='SlackRedirection' component={SlackRedirection}></Route>
			<Route path='/sso-redirect' name='SsoRedirection' component={SsoRedirection}></Route>
		</Route>

		{ /* Routes that use layout 1 */ }
		<Route component={Authentication(LayoutMain, Employee)}>
			{/* Authentication is a wrapper component which performs auth check when component gets rendered */}
			<Route path="dashboard" name="dashboard" component={AsyncDashboardComponents}></Route>


		</Route>

		{ /* Routes that use layout 2 */ }

		<Route component={LayoutBasic}>
			<Route path="verify" name="verify" component={Verify}></Route>
			<Route path="passwordreset" name="passwordreset" component={PasswordReset}></Route>
			<Route path="connectfb" name="connectfb" component={ConnectFB}></Route>
			<Route path="register" component={Authentication(LayoutBasic,SuperAdmin)}>
			{/* <Route path="verify" component={LayoutBasic}> */}
				<IndexRoute component={Authentication(RegisterStep1,SuperAdmin)}></IndexRoute>
				<Route path="step2" component={Authentication(RegisterStep2,SuperAdmin)}></Route>
				{/* <Route path="step3" component={Authentication(RegisterStep3,SuperAdmin)}></Route> */}
			</Route>
			<Route path="guest-signup" component={LayoutBasic}>
				<IndexRoute component={GuestSignUp}></IndexRoute>
			</Route>
			<Route path="registerSsoUser" component={LayoutBasic}>
				<IndexRoute component={RegisterSsoUser}></IndexRoute>
			</Route>

			<NotificationContainer />
			<Route path="chat" name="chat" >
			    <IndexRoute component={Chat}></IndexRoute>
			</Route>

			
			<Route path="analytics" name="analytics">
			<IndexRoute  component={Authentication(AsyncAnalyticsComponents, Admin)}></IndexRoute>
				<Route path="adoption" name="adoption" component={Authentication(AsyncAnalyticsComponents, Admin)}></Route>
				<Route path="activity" name="activity" component={Authentication(AsyncAnalyticsComponents,Admin)}></Route>
				<Route path="values" name="values" component={Authentication(AsyncAnalyticsComponents,Admin)}></Route>
				<Route path="engagement" name="engagement" component={Authentication(AsyncAnalyticsComponents,Admin)}></Route>
				<Route path="culture" name="culture" component={Authentication(AsyncAnalyticsComponents,Admin)}></Route>
				<Route path="Nps" name="Nps" component={Authentication(AsyncAnalyticsComponents,Admin)}></Route>
				<Route path="resonance" name="resonance" component={Authentication(AsyncAnalyticsComponents,Admin)}></Route>
			</Route>
			{/* <Route path= "analytics" component = {LayoutBasic}>
			<Route path="feed" name="feed" component={Authentication(AnalyticsContainer,Admin)}/>
			 <Route path="asset" name="asset" component={Authentication(AnalyticsContainer,Admin)}/>
			 <Route path="campaign" name="campaign" component={Authentication(AnalyticsContainer,Admin)}/>
		      </Route> */}
		</Route>
		
       
		{ /* Routes that use layout 2 */ }
		<Route component={LayoutBasic}>
			<Route path="post(/:actionid)" name="post" component={PostNotification}>
			</Route>

			


			<Route path="feed" name="feed" component={Authentication(Feed, Employee)}>
				{/* <Route path="external" name="external" component={Feed}>
					<Route path="live(/:createpost)" name="live" component={Feed}>
					</Route>
					<Route path="scheduled(/:createpost)" name="scheduled" component={Feed}></Route>
				</Route>
				<Route path="internal" name="internal" component={Feed}>
					<Route path="live(/:createpost)" name="live" component={Feed}></Route>
					<Route path="scheduled(/:createpost)" name="scheduled" component={Feed}></Route>
				</Route> */}
				<Route path="default" name="default" component={Feed}>
					<Route path="live(/:createpost)" name="live" component={Feed}></Route>
					<Route path="scheduled(/:createpost)" name="scheduled" component={Feed}></Route>
				</Route>
				<Route path="custom" name="custom" component={Feed}>
					<Route path="live(/:actionid)" name="live" component={Feed}></Route>
				</Route>
				<Route path="assets" name="internal" component={Feed}>
					<Route path="hash" name="hash" component={Feed}></Route>
				</Route>
			</Route>

			<Route path="feed" name="feed">
				{/* <Route path="internal" name="internal" >
					<Route path="calendar" name="calendar">
						<IndexRoute name="calendar" component={Authentication(Calendar, Employee)}></IndexRoute>
							<Route path="scheduled" name="scheduled" component={Calendar}></Route>
					</Route>
				</Route>
				<Route path="external" name="internal" >
					<Route path="calendar" name="calendar">
						   <IndexRoute name="calendar" component={Authentication(Calendar, Employee)}></IndexRoute>
							<Route path="scheduled" name="scheduled" component={Calendar}></Route>
					</Route>
				</Route> */}
				<Route name="curated">
					<Route path="rss" name="rss" component={Rss}></Route>
					<Route path="jobs" name="jobs" component={Jobs}></Route>
				</Route>
				<Route path="my" name="my">
						<Route path="live" name="live" component={Feed}></Route>
						<Route path="scheduled" name="scheduled" component={Feed}></Route>
						<Route path="unapproved" name="unapproved" component={Feed}></Route>
						<Route path="failed" name="failed" component={Feed}></Route>
						<Route path="pending" name="pending" component={Feed}></Route>
				</Route>
			</Route>
			<Route path="moderation" name="moderation">
				<IndexRoute component={Authentication(AsyncModerationComponents, Moderator)}></IndexRoute>
				<Route path="posts" name="unmoderatedposts" component={Authentication(AsyncModerationComponents, Moderator)}>
					<Route path="undecided" name="moderationPostsUndecided" component={Authentication(AsyncModerationComponents, Moderator)}></Route>
					<Route path="unapproved" name="moderationPostsUnapproved" component={Authentication(AsyncModerationComponents, Moderator)}></Route>
				</Route>
				<Route path="assets" name="unmoderatedassets" component={Authentication(AsyncModerationComponents, Moderator)}>
					<Route path="undecided" name="moderationAssetsUndecided" component={Authentication(AsyncModerationComponents, Moderator)}></Route>
					<Route path="unapproved" name="moderationAssetsUnapproved" component={Authentication(AsyncModerationComponents, Moderator)}></Route>
				</Route>

			</Route>

			<Route path="assets" name="assets">
				<Route component={Authentication(AsyncAssetsComponents, Employee)}>
					<IndexRoute name="assetsindex" component={Authentication(AsyncAssetsComponents, Employee)}></IndexRoute>
					<Route path="my" name="myAssets" component={Authentication(AsyncAssetsComponents, Employee)}></Route>
					<Route path="archive" name="archiveAssets" component={Authentication(AsyncAssetsComponents, Employee)}></Route>
					<Route path="cat/:catId" name="assetsDir" component={Authentication(AsyncAssetsComponents, Employee)}></Route>
					<Route path="cat/:catId/folder/:folderId" name="assetsDirFolder" component={Authentication(AsyncAssetsComponents, Employee)}></Route>
					<Route path="custom/:catId" name="customAssetsDir" component={Authentication(AsyncAssetsComponents, Employee)}></Route>
				</Route>

			</Route>

			{/*<Route path="assets" name="assets" component={Authentication(Assets)}></Route>
			<Route path="assets/my" name="myAssets" component={Authentication(Assets)}></Route>
			<Route path="assets/cat/:catId" name="assetsDir" component={Authentication(Assets)}></Route>
			<Route path="assets/cat/:catId/folder/:folderId" name="assetsDirFolder" component={Authentication(Assets)}></Route>*/}

			<Route path="campaigns" name="campaigns">
				<Route component={Authentication(AsyncCampaignComponents, Employee)}>
					<IndexRoute name="index" component={Authentication(AsyncCampaignComponents, Employee)}></IndexRoute>
					<Route path="all" name="all" component={AsyncCampaignComponents}></Route>
					<Route path="live" name="live" component={AsyncCampaignComponents}></Route>
					<Route path="expired" name="expired" component={AsyncCampaignComponents}></Route>
					<Route path="terminated" name="terminated" component={AsyncCampaignComponents}></Route>
				</Route>
				<Route path="calendar">
						 <IndexRoute name="index" component={Authentication(AsyncCampaignComponents, Employee)}></IndexRoute>
						<Route path="all" name="all" component={AsyncCampaignComponents}></Route>
						 <Route path="live" name="live" component={AsyncCampaignComponents}></Route>
						 <Route path="expired" name="expired" component={AsyncCampaignComponents}></Route>
						 <Route path="terminated" name="terminated" component={AsyncCampaignComponents}></Route>
						<IndexRoute name="index" component={Authentication(AsyncCampaignComponents, Employee)}></IndexRoute>
						<Route path="all" name="all" component={AsyncCampaignComponents}></Route>
						<Route path="live" name="live" component={AsyncCampaignComponents}></Route>
						<Route path="expired" name="expired" component={AsyncCampaignComponents}></Route>
						<Route path="terminated" name="terminated" component={AsyncCampaignComponents}></Route>
				</Route>
			</Route>

			<Route path="leaderboard" name="leaderboard">
				<Route component={Authentication(AsyncLeaderboardComponents, Employee)}>
					<IndexRoute name="all" component={AsyncLeaderboardComponents}></IndexRoute>
					<Route path="mydepartment" name="dept" component={AsyncLeaderboardComponents}></Route>
				</Route>
			</Route>

			<Route path="settings" name="settings"  component ={AsyncSettingsComponents}>
				<IndexRoute name="profile" component={Authentication(AsyncSettingsComponents, Employee)}></IndexRoute>
				<Route path="company" name="company" component={Authentication(AsyncSettingsComponents,Admin)}></Route>
				<Route path="social" name="social" component={Authentication(AsyncSettingsComponents,Employee)}></Route>
				<Route path="Integration" name="integration" component={Authentication(AsyncSettingsComponents, Employee)}></Route>
				<Route path='content' name='content' component={Authentication(AsyncSettingsComponents,Employee)}></Route>
				<Route path='culture' name='ValueSettings' component={Authentication(AsyncSettingsComponents,Admin)}></Route>
				{/* <Route path="departments" name="departments" component={Authentication(DepartmentSettings, Admin)}></Route> */}
				<Route path="billing" name="billing">
					<IndexRoute name="overview" component={Authentication(AsyncSettingsComponents,SuperAdmin)}></IndexRoute>
					<Route path="invoice" name="invoice" component={Authentication(AsyncSettingsComponents,SuperAdmin)}></Route>
					<Route path="settingcontact" name="settingcontact" component={Authentication(AsyncSettingsComponents,SuperAdmin)}></Route>
					<Route path="paymentmethod" name="paymentmethod" component={Authentication(AsyncSettingsComponents,SuperAdmin)}></Route>
					<Route path="teamchange" name="teamchange" component={Authentication(AsyncSettingsComponents,SuperAdmin)}></Route>
				</Route>


				 <Route path="Points" name="Points" component={Authentication(AsyncSettingsComponents, Admin)}></Route>
				<Route path="users" name="users" component={Authentication(AsyncSettingsComponents, Admin)}></Route>
				<Route path="widgets" name="widgets">
					<IndexRoute name="wall" component={Authentication(AsyncSettingsComponents, Admin)}></IndexRoute>
					<Route path="map" name="map" component={Authentication(AsyncSettingsComponents, Admin)}></Route>
					<Route path="survey" name="survey" component={Authentication(AsyncSettingsComponents, Admin)}></Route>
				</Route>



			</Route>

		</Route>


		<Route component={LayoutBasic}>
			<Route path="search" component = {GlobalSearch}>
				<Route path="default" name="default" component={GlobalSearch}>
					<Route path="live" name="live" component={GlobalSearch}>
					</Route>
				</Route>
				<Route path="assets" name="assets" component={GlobalSearch}>
				</Route>
				<Route path="campaign" name="campaign" component={GlobalSearch}>
				</Route>
				<Route path="user" name="user" component={GlobalSearch}>
				</Route>
				<Route path="department" name="department" component={GlobalSearch}>
				</Route>
			</Route>
		</Route>

		<Route path="*" component={Four0Four} />

	</Route>


)

//When page change and user login we have to call user pilot screen
browserHistory.listen( location =>  {
 			//Do your stuff here
 			var splitLocation = location.pathname.split('/')
 			var unAuthPageArray = ['reset-password','sign-up','thankyou','slack-redirect','sso-redirect','login']
 			if(unAuthPageArray.indexOf(splitLocation[1]) == -1 && url !== splitLocation[1]){
 				userpilot.identify(
				    localStorage.getItem('UserEmail')
				)
				url = splitLocation[1]
 			}

	});
